<?php

/**
 * @file
 * Hooks related to cloud_launch_template module.
 */

use Drupal\cloud\Entity\CloudLaunchTemplateInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the route array after a template is launched.
 *
 * @param array $route
 *   Associate array with route_name, params.
 * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template
 *   The cloud launch template entity.
 */
function hook_cloud_launch_template_post_launch_redirect_alter(array &$route, CloudLaunchTemplateInterface $cloud_launch_template) {

}

/**
 * @} End of "addtogroup hooks".
 */
