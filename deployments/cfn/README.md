USAGE INSTRUCTIONS
==================

The following Cloud Formation templates are provided.

- `nested/cloud_orchestrator_full.yaml`
  - Builds a fully distributed Cloud Orchestrator distribution.
    - Database: **RDS (MariaDB)**
    - Memcache: **ElastiCache (Memcached)**
    - Runtime: **EC2** with LAMP stack
    - VPC: **Automatic**
- `nested/cloud_orchestrator_full_manual_vpc.yaml`
  - Builds a fully distributed Cloud Orchestrator distribution. This template
    lets a user choose a VPC and Subnet for the EC2 instance.
    - Runtime: **EC2** with LAMP stack
    - Database: **RDS** (MariaDB)
    - Memcache: **ElastiCache (Memcached)**
    - VPC: **User-specified VPC and Subnet**
- `nested/cloud_orchestrator_single.yaml`
  - Builds a Cloud Orchestrator distribution fully contained in a single EC2
    instance.
    - Runtime: **EC2** with LAMP stack
    - Database: **MariaDB**
    - Memcache: **Memcached**
    - VPC: **Automatic**
- `nested/cloud_orchestrator_single_manual_vpc.yaml`
  - Fully contained in a single EC2 instance.  This template lets a user choose
    a VPC and Subnet for the EC2 instance. Builds a Cloud Orchestrator
    distribution.
    - Runtime: **EC2** with LAMP stack
    - Database: **MariaDB**
    - Memcache: **Memcached**
    - VPC: **User-specified VPC and Subnet**
- `nested/cloud_orchestrator_docker.yaml`
  - Build a Cloud Orchestrator distribution running individual Docker containers
    in a single EC2 instance.
    - Runtime: **Docker on EC2** with LAMP stack
    - Database: **MariaDB**
    - Memcache: **Memcached**
    - VPC: Automatic
- `nested/cloud_orchestrator_docker_manual_vpc.yaml`
  - Builds a Cloud Orchestrator distribution running individual Docker
    containers in a single EC2 instance. This template lets a user choose a VPC
    and Subnet for the EC2 instance.
    - Runtime: **Docker** on EC2 with LAMP stack
    - Database: **MariaDB**
    - Memcache: **Memcached**
    - VPC: **User-specified VPC and Subnet**
- `nested/cloud_orchestrator_docker_rds_manual_vpc.yaml`
  - Builds a Cloud Orchestrator distribution running individual Docker
    containers in a single EC2 instance. The database is running on RDS.  This
    template lets a user choose a VPC and Subnet for the EC2 instance.
    - Runtime: **Docker on EC2** with LAMP stack
    - Database: **RDS (MySQL)**
    - Memcache: **Memcached**
    - VPC: **User-specified VPC and Subnet**

The packaged version of these templates are hosted on S3.
- https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_full.yaml
- https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_full_manual_vpc.yaml
- https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_single.yaml
- https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_single_manual_vpc.yaml
- https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_docker.yaml
- https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_docker_manual_vpc.yaml
- https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_docker_rds_manual_vpc.yaml

To package the templates for your own usage, follow these steps:

1. Prep and upload the nested templates.  The command also replaces the
   Template URLs with publicly accessible S3 urls:
   `aws cloudformation package --template-file <TEMPLATE_NAME.yaml>
   --output-template <output_template_name.yaml> --s3-bucket
   <Accessible S3 Bucket>`
2. Log into AWS management console and navigate to CloudFormation.  Make sure
   your account has permissions for IAM, VPC and Networking, Security groups,
   RDS, ElastiCache and EC2.
3. Use the `<output_template_name.yaml>` to launch the Cloud Orchestrator stack.
4. Wait for the stack to launch.  Then follow the instructions under
   `Outputs` > `Instructions`.

Technical Details
=================

The nested CFn templates are described here.  These templates are
functional as standalone templates, but it is recommended to compile them
into the Cloud Formation templates.

- `cfn/nested/cache/template.yaml`
  - Creates ElastiCache cluster.
- `cfn/nested/database/template.yaml`
  - Creates RDS instance using MariaDB `10.5.12`.
- `cfn/nested/ec2/docker/template.yaml`
  - Creates EC2 instance.
  - Installs MariaDB Docker image, Memcached Docker image, and PHP 8.0 based
  Docker image.
  - Cloud Orchestrator distribution is installed on EC2 instance and accessed
  via Docker LAMP stack.
  - **MariaDB only listens for connections from the PHP 8.0
  Docker image.**
  - The following custom PHP parameter are added.
```
  memory_limit = -1
  max_execution_time = 600
  max_input_time = 600
  max_input_vars = 100000
```
- `cfn/nested/ec2/docker_rds/template.yaml`
  - Creates EC2 instance.
  - Installs Memcache Docker image, and PHP 8.0 based Apache image.
  - Connects to RDS database.
  - Cloud Orchestrator distribution is installed on EC2 instance and
  accessed via Docker LAMP stack.
  - **ElastiCache and RDS IP and port are only available from the preconfigured
  VPC Subnets.**
  - The following custom PHP parameters are added.
```
  memory_limit = -1
  max_execution_time = 600
  max_input_time = 600
  max_input_vars = 100000
```
- `cfn/nested/ec2/full/template.yaml`
  - Creates EC2 instance.
  - Installs Apache and PHP 8.0.
  - Connects to MariaDB(`10.5.12`) on RDS and memcache via ElastiCache.
  - Cloud Orchestrator distribution is installed on EC2.
  - **ElastiCache and RDS IP and port are only available from the preconfigured
  VPC Subnets.**
  - The following custom PHP parameters are added.
```
  memory_limit = -1
  max_execution_time = 600
  max_input_time = 600
  max_input_vars = 100000
```
- `cfn/nested/ec2/single/template.yaml`
  - Creates EC2 instance.
  - Installs Apache, PHP8.0, MariaDB 10.5.12 and Memcached.
  - Cloud Orchestrator distribution is installed on EC2.
  - **MariaDB only listens for connections from `127.0.0.1`.**
  - The following custom PHP parameters are added.
```
  memory_limit = -1
  max_execution_time = 600
  max_input_time = 600
  max_input_vars = 100000
```
- `cfn/nested/iam/template.yaml`
  - Creates IAM profile with the following permissions:
    - ec2:AcceptVpcPeeringConnection
    - ec2:AllocateAddress
    - ec2:AssociateAddress
    - ec2:AssociateSubnetCidrBlock
    - ec2:AssociateIamInstanceProfile
    - ec2:AssociateVpcCidrBlock
    - ec2:AttachInternetGateway
    - ec2:AuthorizeSecurityGroup*
    - ec2:AttachVolume
    - ec2:CreateDefaultSubnet
    - ec2:CreateDefaultVpc
    - ec2:CreateEgressOnlyInternetGateway
    - ec2:CreateFlowLogs
    - ec2:CreateImage
    - ec2:CreateInternetGateway
    - ec2:CreateKeyPair
    - ec2:CreateLaunchTemplate
    - ec2:CreateLaunchTemplateVersion
    - ec2:CreateNetworkInterface
    - ec2:CreateNetworkInterfacePermission
    - ec2:CreateSecurityGroup
    - ec2:CreateSnapshot
    - ec2:CreateSnapshots
    - ec2:CreateSubnet
    - ec2:CreateTags
    - ec2:CreateVolume
    - ec2:CreateVpc
    - ec2:CreateVpcPeeringConnection
    - ec2:DisassociateAddress
    - ec2:DisassociateIamInstanceProfile
    - ec2:DisassociateVpcCidrBlock
    - ec2:Describe*
    - ec2:DeleteEgressOnlyInternetGateway
    - ec2:DeleteFlowLogs
    - ec2:DeleteInternetGateway
    - ec2:DeleteKeyPair
    - ec2:DeleteLaunchTemplate
    - ec2:DeleteLaunchTemplateVersions
    - ec2:DeleteNetworkInterface
    - ec2:DeleteNetworkInterfacePermission
    - ec2:DeleteSecurityGroup
    - ec2:DeleteSnapshot
    - ec2:DeleteSubnet
    - ec2:DeleteTags
    - ec2:DeleteVpcPeeringConnection
    - ec2:DeleteVolume
    - ec2:DeleteVpc
    - ec2:DeregisterImage
    - ec2:DetachInternetGateway
    - ec2:DetachNetworkInterface
    - ec2:DetachVolume
    - ec2:ImportKeyPair
    - ec2:ModifyInstanceAttribute
    - ec2:ModifyImageAttribute
    - ec2:ModifyLaunchTemplate
    - ec2:ModifyVolume
    - ec2:RebootInstances
    - ec2:RegisterImage
    - ec2:ReleaseAddress
    - ec2:ReplaceIamInstanceProfileAssociation
    - ec2:RevokeSecurityGroup*
    - ec2:RunInstances
    - ec2:StartInstances
    - ec2:StopInstances
    - ec2:TerminateInstances
    - ecr:BatchCheckLayerAvailability
    - ecr:BatchGetImage
    - ecr:CompleteLayerUpload
    - ecr:CreateRepository
    - ecr:DeleteRepository
    - ecr:DescribeImages
    - ecr:DescribeRepositories
    - ecr:GetAuthorizationToken
    - ecr:GetDownloadUrlForLayer
    - ecr:ListImages
    - ecr:PutImage
    - ecr:StartImageScan
    - ecr:TagResource
    - ecr:UploadLayerPart
    - ecr:UntagResource
    - s3:DeleteObject
    - s3:DeleteObjectVersion
    - s3:GetObject
    - s3:ListAllMyBuckets
    - s3:ListBucket
    - s3:PutObject
    - iam:ListInstanceProfiles
    - iam:PassRole
- `cfn/nested/vpc_attributes/template.yaml`
  - Helper template that launches a Lambda function used for looking up
  VPC attributes such as `VPC Cidr Block`, `Subnet Availability Zone`,
  `Subnet CIDR Block` and `Subnet VPC ID`.
- `cfn/nested/networking/template.yaml`
  - Creates a VPC using `10.0.0.0/24` as the public subnet block, `10.0.1.0/24`
  as private subnet 1 and `10.0.2.0/24` as private subnet 2.
- `cfn/nested/security/ec2/template.yaml`
  - Creates a security group with port `22` and port `80` open to `0.0.0.0/0`.
  - There are no restrictions on outgoing ports.
- `cfn/nested/security/subnet/template.yaml`
  - Creates a security group for RDS and ElastiCache communications.
  - Port `3306` and `11211` are open to the public subnet `10.0.0.0/24`.

Troubleshooting
===============

- If you encounter the following error, try to build the CFn template again.

``
PHP Fatal error:
Uncaught Drupal\Component\Plugin\Exception\PluginNotFoundException:
The "page" entity type does not exist. in
/var/www/cloud_orchestrator/docroot/core/
lib/Drupal/Core/Entity/EntityTypeManager.php:143
``
