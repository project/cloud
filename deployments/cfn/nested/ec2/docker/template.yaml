##
# Cloud Orchestrator cloud formation template.
# This template will create an EC2 instance and install
# Docker.  Cloud Orchestrator runs inside Docker.
##
AWSTemplateFormatVersion: '2010-09-09'
Description: 'Cloud Orchestrator EC2 - Docker'
Fn::Transform:
  Name: 'AWS::Include'
  Parameters:
    Location: '../../includes/amis.yaml'

Parameters:
  StackPrefix:
    Type: String
    Description: >-
      A prefix to append to resource names/IDs. For example, ${StackPrefix}-IAM-Role,
      ${StackPrefix}-Drupal-RDS for RDS DB Identifier.
      Must be between 1 and 20 characters and only contain alphanumeric characters
      and hyphens.
    MinLength: '1'
    MaxLength: '20'
    AllowedPattern: '[a-zA-Z0-9\\-]*'
  IAMInstanceProfile:
    Description: IAM instance profile name
    Type: String
  KeyName:
    Description: Name of an existing EC2 key pair to SSH access into the EC2 instance.
    Type: String
    ConstraintDescription: Must be the name of an existing EC2 KeyPair.
  InstanceType:
    Description: The EC2 instance type.
    Type: String
    Default: t3.medium
  DrupalUserName:
    Default: cloud_admin
    Description: >-
      The Drupal admin account username. Must be between 5 and 16 characters and
      only contain alphanumeric characters and underscores.
    Type: String
    MinLength: '5'
    MaxLength: '16'
    AllowedPattern: "[a-zA-Z][a-zA-Z0-9_]*"
    NoEcho: 'false'
    ConstraintDescription: >-
      Must only contain alphanumeric characters, underscores
      and must be between 5 and 16 characters long.
  DrupalPassword:
    Type: String
    Description: >-
      The Drupal admin account password. Must be between 6 and 32 characters and
      only contain alphanumeric characters and these special
      characters ` ~ ! # $ % ^ & * ( ) _ + , . \ -
    NoEcho: 'true'
    MinLength: '6'
    MaxLength: '32'
    AllowedPattern: '^[\w`~!#$%^&*()_+,.\\-]+$'
    ConstraintDescription: >-
      Your Drupal password must be between 6 and 32 characters and be
      letters (upper or lower), numbers or these special characters ` ~ ! # $ % ^ & * ( ) _ + , . \ -
  DrupalEmail:
    Description: Drupal site administrator email.
    Type: String
    AllowedPattern: '^[\w_.+-]+@[\w-]+\.[-.\w]+$'
    ConstraintDescription: Must be a valid email address.
  DrupalTimezone:
    Default: America/Los_Angeles
    Description: Default time zone
    Type: String
  PublicSubnet1:
    Description: Public Subnet 1
    Type: String
  EC2SecurityGroup:
    Description: EC2 security group
    Type: String
  MySQLUserName:
    Description: >-
      Username for the MariaDB. Must be between 5 and 16 characters and only
      contain alphanumeric characters and underscores.
    Default: administrator
    Type: String
    NoEcho: 'false'
    MinLength: '5'
    MaxLength: '16'
    AllowedPattern: "[a-zA-Z0-9_]*"
    ConstraintDescription: >-
      Must contain only alphanumeric characters, underscores
      and must be between 5 and 16 characters long.
  MySQLPassword:
    Description: >-
      Password for the MariaDB Username.  Must be between 6 and 32 characters
      and only contain alphanumeric characters and these special characters
      ` ~ ! # $ % ^ & * ( ) _ + , . \ -
    Type: String
    MinLength: '6'
    MaxLength: '32'
    AllowedPattern: '^[\w`~!#$%^&*()_+,.\\-]+$'
    ConstraintDescription: >-
      Your MySQLPassword must be between 6 and 32 characters and be letters (upper or lower),
      numbers or these special characters ` ~ ! # $ % ^ & * ( ) _ + , . \ -
    NoEcho: 'true'
  DatabaseName:
    Description: >-
      The name of the database. Must be between 4 and 32 characters and
      only contain alphanumeric characters and underscores.
    Type: String
    Default: cloud_orchestrator
    AllowedPattern: '[a-zA-Z0-9_]*'
    MinLength: '4'
    MaxLength: '32'
    ConstraintDescription: >-
      Your database name must be between 4 and 32 characters and
      contain alphanumeric characters and underscores.
  DatabaseEndpointAddress:
    Description: Database Endpoint
    Type: String
    Default: 'mysql'
  DatabasePort:
    Description: Database Port
    Type: String
    Default: '3306'
  ElastiCacheAddress:
    Description: ElastiCache endpoint address
    Type: String
    Default: 'memcache'
  MemcachePort:
    Description: Memcache port
    Type: String
    Default: '11211'
  InstallDB:
    Description: TRUE or FALSE for installing local database
    Type: String
    Default: 'TRUE'
  InstallMemcache:
    Description: TRUE or FALSE for installing local memcache
    Type: String
    Default: 'TRUE'
  CloudOrchestratorVersion:
    Description: Version of the Cloud Orchestrator to install.
    Type: String
  GitHubToken:
    Description: GitHub authentication token
    Type: String
Resources:
  EC2:
    Type: 'AWS::EC2::Instance'
    Properties:
      DisableApiTermination: false
      EbsOptimized: false
      IamInstanceProfile: !Ref IAMInstanceProfile
      ImageId: !FindInMap
        - CloudOrchestrator
        - !Ref 'AWS::Region'
        - AMI
      BlockDeviceMappings:
        - DeviceName: /dev/sda1
          Ebs:
            VolumeType: gp3
            VolumeSize: '15'
            DeleteOnTermination: 'true'
            Encrypted: 'true'
      InstanceInitiatedShutdownBehavior: stop
      InstanceType: !Ref InstanceType
      KeyName: !Ref KeyName
      Monitoring: false
      NetworkInterfaces:
        - AssociatePublicIpAddress: true
          DeviceIndex: "0"
          SubnetId: !Ref PublicSubnet1
          GroupSet:
            - !Ref EC2SecurityGroup
      Tenancy: default
      UserData:
        Fn::Transform:
          Name: 'AWS::Include'
          Parameters:
            Location: '../../includes/docker.yaml'
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Name
          Value: !Sub
            - '${StackPrefix} Drupal Instance'
            - StackPrefix: !Ref StackPrefix
    CreationPolicy:
      ResourceSignal:
        Timeout: PT30M  # 30min
        Count: 1
Outputs:
  EC2InstanceName:
    Value: !Sub
      - '${StackPrefix} Drupal Instance'
      - StackPrefix: !Ref StackPrefix
  DrupalUrl:
    Value:
      !Sub
        - 'http://${PublicIp}'
        - { PublicIp: !GetAtt EC2.PublicIp }
  DrupalUserName:
    Value: !Ref DrupalUserName
  DrupalPassword:
    Value: !Ref DrupalPassword
  DrupalEmail:
    Value: !Ref DrupalEmail
  SSHAccess:
    Value:
      !Sub
        - 'ssh ubuntu@${PublicIp}'
        - { PublicIp: !GetAtt EC2.PublicIp }
