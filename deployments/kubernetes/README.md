USAGE INSTRUCTIONS
==================

### Deploy Cloud orchestrator to your K8s cluster

Deploy Cloud orchestrator to your K8s cluster by executing the following
command. Note that
[default usernames and passwords](amd64/cloud_orchestrator.yml#L18-24)
will be set.

```
$ kubectl apply -f https://git.drupalcode.org/project/cloud/-/raw/8.x/deployments/kubernetes/amd64/cloud_orchestrator.yml
```

If your K8s cluster consists ARM64-based nodes, use
[`arm64/cloud_orchestrator.yml`](arm64/cloud_orchestrator.yml) instead.

```
$ kubectl apply -f https://git.drupalcode.org/project/cloud/-/raw/8.x/deployments/kubernetes/arm64/cloud_orchestrator.yml
```

#### Alternative procedure

If you want to set your usernames and passwords, or limit which client IP's can
access your Cloud Orchestrator, follow these commands instead.

Download the Cloud Orchestrator manifest file.

```
$ curl -LO https://git.drupalcode.org/project/cloud/-/raw/8.x/deployments/kubernetes/amd64/cloud_orchestrator.yml
```

Update the following lines.

* Line 18- 24: Set your username, password, etc.
* Line 45: Limit which clients can access your Cloud Orchestrator

```
$ vi cloud_orchestrator.yml
```

Apply the manifest file to your K8s cluster.

```
$ kubectl apply -f cloud_orchestrator.yml
```


### Access your Cloud Orchestrator

Check `EXTERNAL-IP` of the Cloud Orchestrator service by using the following
command. For example, if you are using AWS EKS, that is the hostname of a
classic load balancer.

```
$ kubectl get services  \
    --namespace cloud-orchestrator  \
    --field-selector metadata.name=cloud-orchestrator
```

You can access your Cloud Orchestrator by opening `EXTERNAL-IP` with the prefix
`http://` in your web browser.


### Remove Cloud Orchestrator from your K8s cluster

```
$ kubectl delete -f https://git.drupalcode.org/project/cloud/-/raw/8.x/deployments/kubernetes/amd64/cloud_orchestrator.yml
```

`*/cloud_orchestrator.yml` installs Cloud Orchestrator and stores MariaDB
database on a filesystem of one of the K8s nodes via PersistentVolumes of the
type hostPath. When these PersistentVolumes are deleted, files stored in the
node's filesystem are NOT deleted. If you want to delete them, log in to the
node and execute the following command:

* Cloud Orchestrator
  - `rm -r /var/www/cloud_orchestrator`
* MariaDB
  - `rm -r /var/cloud_orchestrator/mysql`
