#!/bin/bash

CO_DIR='/var/www/cloud_orchestrator'
SETTINGS_FILE="${CO_DIR}/docroot/sites/default/settings.php"

cd "${CO_DIR}"
if ! [ -f "${SETTINGS_FILE}" ]; then
  while true; do
    if mysql -u "${MYSQL_USER}" -p"${MYSQL_PASSWORD}" -h "${MYSQL_HOST}" -e 'show databases;' > /dev/null; then
      break
    fi
    echo 'Waiting for the database to respond...'
    sleep 10
  done

  rm -rf "${CO_DIR}/"*
  git config --global url.'https://github.com/'.insteadOf 'git@github.com:'
  git config --global url.'https://'.insteadOf 'git://'
  composer create-project "docomoinnovations/cloud_orchestrator:${CLOUD_ORCHESTRATOR_VERSION}" "${CO_DIR}"
  composer -d "${CO_DIR}" update
  chown -R www-data:www-data "${CO_DIR}"

  # Setup private directories.
  PRIVATE_FILE_DIR="${CO_DIR}/files/private"
  CONFIG_DIR="${CO_DIR}/files/config/sync"
  mkdir -p "${PRIVATE_FILE_DIR}"
  mkdir -p "${CONFIG_DIR}"
  chown -R www-data:www-data "${CO_DIR}/files"
  chmod -R g+w "${CO_DIR}/files"

  FILES_DIR="${CO_DIR}/docroot/sites/default/files"
  DEFAULT_SETTINGS_FILE="${CO_DIR}/docroot/sites/default/default.settings.php"

  mkdir "${FILES_DIR}"
  cp "${DEFAULT_SETTINGS_FILE}" "${SETTINGS_FILE}"
  chown -R www-data:www-data "${SETTINGS_FILE}"
  chmod -R g+w "${SETTINGS_FILE}"

  chown -R www-data:www-data "${FILES_DIR}"
  chmod -R g+w "${FILES_DIR}"

  tee -a "${SETTINGS_FILE}" > /dev/null <<EOF
\$settings['file_private_path'] = '${PRIVATE_FILE_DIR}';
\$settings['config_sync_directory'] = '${CONFIG_DIR}';
EOF

  # Set up a drush command.
  ln -s "${CO_DIR}/vendor/bin/drush" /usr/local/bin/

  # Install Cloud Orchestrator using Drush.
  drush si -y \
    --db-url="mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@${MYSQL_HOST}:${MYSQL_PORT}/${MYSQL_DATABASE}" \
    --account-name="${DRUPAL_USER}" \
    --account-pass="${DRUPAL_PASSWORD}" \
    --site-name='Cloud Orchestrator' \
    --account-mail="${DRUPAL_EMAIL}" \
    cloud_orchestrator \
    cloud_orchestrator_module_configure_form.cloud_service_providers.terraform=terraform \
    cloud_orchestrator_module_configure_form.cloud_service_providers.openstack=openstack \
    cloud_orchestrator_module_configure_form.cloud_service_providers.vmware=vmware

  chown -R www-data:www-data "${CO_DIR}/files"
  chown -R www-data:www-data "${CO_DIR}/docroot/sites/default/files"

  # Set timezone.
  drush -y config:set system.date timezone.default "${DRUPAL_TIMEZONE}"

  # Switch to Claro Admin.
  drush then -y claro
  drush cset -y system.theme admin claro

  drush en -y memcache memcache_admin

  tee -a "${SETTINGS_FILE}" > /dev/null << EOF
\$databases['default']['default']['init_commands'] = [
  'isolation_level' => 'SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED',
];
\$settings['memcache']['servers'] = ['${MEMCACHED_HOST}:${MEMCACHED_PORT}' => 'default'];
\$settings['memcache']['bins'] = ['default' => 'default'];
\$settings['memcache']['key_prefix'] = 'cloud_orchestrator';
\$settings['memcache']['options'] = [
  Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_CONSISTENT,
];
\$settings['cache']['default'] = 'cache.backend.memcache';
EOF

  DRUSH_QUEUE_RUN_SCRIPT="${CO_DIR}/docroot/modules/contrib/cloud/scripts/drush_queue_run.sh"
  chmod +x "${DRUSH_QUEUE_RUN_SCRIPT}"
  tee /etc/crontab > /dev/null <<EOF
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=''

*/1 * * * * www-data cd '${CO_DIR}'; drush cron > /dev/null 2>&1
*/15 * * * * www-data cd '${CO_DIR}'; '${DRUSH_QUEUE_RUN_SCRIPT}' > /dev/null 2>&1
EOF
fi

drush -y cr
drush -y updb

cron
apache2-foreground
