(function (Drupal) {
  'use strict';

  // Init the tables.
  var dataTableSelector = document.querySelectorAll('.simple-datatable table');
  var dataTables = [];
  dataTableSelector.forEach(function (dataTable, index) {
    dataTables[index] = new simpleDatatables.DataTable(dataTable, {
      fixedHeight: false,
      labels: {
        info: '',
      },
      footer: dataTable.classList.contains('show-footer') ? true : false,
      paging: false,
      perPage: 9999,
      perPageSelect: false,
      searchable: false,
      // Set the first column to sort ascending by default.
      // User then clicks on which column they want to sort.
      columns: [
        {
          select: 0,
          sort: 'asc',
        }
      ]
    });
  });

  dataTables.forEach(function (dataTable, index) {
    // Add glyphicon.
    let tableHeaders = dataTableSelector[index].querySelectorAll('th');

    dataTable.on('datatable.init', function () {
      tableHeaders.forEach(function (tableHeader) {
        // Create Icon element
        let datatableArrowIcon = document.createElement('span');

        datatableArrowIcon.classList.add('icon', 'glyphicon', 'icon-after');
        // Add up icon if column is sorted ascending.
        if (tableHeader.classList.contains('asc')) {
          datatableArrowIcon.classList.add('glyphicon-chevron-up');
        }
        else if (tableHeader.classList.contains('desc')) {
          datatableArrowIcon.classList.add('glyphicon-chevron-down');
        }

        // Append Icon element
        tableHeader.querySelector('a').appendChild(datatableArrowIcon);
      });
    });

    dataTable.on('datatable.sort', function (column, direction) {
      // Be more specific with th selector.
      let headers = dataTable.table.querySelectorAll('thead tr th');
      // Remove both classes since we have no way of keeping track.
      headers.forEach(function (tableHeader) {
        let span = tableHeader.querySelector('span');
        span.classList.remove(span.classList.contains('glyphicon-chevron-down')
          ? 'glyphicon-chevron-down'
          : 'glyphicon-chevron-up'
        );
      });
      // Add the corresponding class.
      headers[column].querySelector('span')
        .classList.add(direction == 'asc'
        ? 'glyphicon-chevron-up'
        : 'glyphicon-chevron-down');
    });

  });

})(Drupal);
