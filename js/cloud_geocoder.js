(function (drupalSettings) {
  'use strict';

  let countryId = 'edit-field-location-country';
  let cityId = 'edit-field-location-city-0-value';
  let latitudeId = 'edit-field-location-latitude-0-value';
  let longitudeId = 'edit-field-location-longitude-0-value';

  if (drupalSettings.cloud.country_id) {
    countryId = drupalSettings.cloud.country_id;
  }

  if (drupalSettings.cloud.city_id) {
    cityId = drupalSettings.cloud.city_id;
  }

  if (drupalSettings.cloud.latitude_id) {
    latitudeId = drupalSettings.cloud.latitude_id;
  }

  if (drupalSettings.cloud.longitude_id) {
    longitudeId = drupalSettings.cloud.longitude_id;
  }

  let setGeolocation = function () {

    if (!drupalSettings.cloud || !drupalSettings.cloud.geocoder_url) {
      return;
    }

    let countryInput = document.getElementById(countryId);
    let cityInput = document.getElementById(cityId);
    if (countryInput.value === '_none' || cityInput.value.trim().length === 0) {
      return;
    }
    let url = drupalSettings.cloud.geocoder_url;

    url = url.replace('country', countryInput.value);
    url = url.replace('city', cityInput.value);

    let xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function() {
      if (xhr.status === 200) {
        let data = JSON.parse(xhr.responseText);
        if (data && data.latitude && data.longitude) {
          let latitudeInput = document.getElementById(latitudeId);
          let longitudeInput = document.getElementById(longitudeId);

          let step = latitudeInput.getAttribute('step');
          if (step && !isNaN(step)) {
            step = parseFloat(step);
          }
          else {
            step = 0.000001;
          }
          step = 1 / step;
          let latitude = Math.round(data.latitude * step) / step;
          let longitude = Math.round(data.longitude * step) / step;
          latitudeInput.value = latitude;
          longitudeInput.value = longitude;
        }
      }
    };
    xhr.send();
  };

  let countryInput = document.getElementById(countryId);
  if (countryInput) {
    countryInput.addEventListener('change', setGeolocation);
    countryInput.addEventListener('blur', setGeolocation);
  }

  let cityInput = document.getElementById(cityId);
  if (cityInput) {
    cityInput.addEventListener('change', setGeolocation);
    cityInput.addEventListener('blur', setGeolocation);
  }

  let form = document.querySelector('form');
  if (form) {
    form.addEventListener('submit', setGeolocation);
  }

}(drupalSettings));
