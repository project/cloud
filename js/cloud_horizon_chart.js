(function () {
  'use strict';

  Drupal.Cloud = {};

  const colors = [
    ['#edf8e9', '#bae4b3', '#74c476', '#31a354', '#006d2c'],
    ['#e9f8f4', '#b3e4dc', '#74c1c4', '#317fa3', '#003f6d'],
  ];

  const AXIS_HEIGHT = 10;
  const AXIS_FONT_SIZE = '8';
  const CHART_HEIGHT = 20;
  const TITLE_FONT_SIZE = 10;
  const TITLE_POSITION_X = 1;
  const TITLE_POSITION_Y = 70;
  const BACKGROUND_DEFAULT_COLOR = 'rgba(0, 0, 0, 0)';

  const drawXAxisLabels = (firstRowContainer, values) => {
    // Reset the <div> for the current x-axis.
    const prevXAxisDiv = document.querySelector('#x-axis');
    if (prevXAxisDiv) {
      prevXAxisDiv.remove();
    }
    const xAxisDiv = document.createElement('div');
    xAxisDiv.id = 'x-axis';
    firstRowContainer.before(xAxisDiv);

    const axis = new HorizonChart();
    axis.setHeight(AXIS_HEIGHT);
    axis.setDataSet(values);
    axis.setSelector('#x-axis');
    axis.draw();

    const g = xAxisDiv.querySelector('g:first-child:not(.axis):not(.tick)');
    if (g) {
      g.style.display = 'none';
    }
    for (const axisg of xAxisDiv.querySelectorAll('g.axis')) {
      axisg.style.fontSize = AXIS_FONT_SIZE;
    }
  };

  // Draw Chart Function.
  Drupal.Cloud.drawChart = (data, first) => {
    data.forEach(({ id, selector, values }, idx) => {
      const rowContainer = document.querySelector(selector);
      if (!(rowContainer instanceof Element)) {
        return;
      }
      // Delete current chart.
      rowContainer.innerHTML = '';

      // Draw labels on the x-axis.
      if (first && idx === 0) {
        drawXAxisLabels(rowContainer, values);
      }

      // Draw charts.
      const horizonChart = new HorizonChart();
      const fontColor = getComputedStyle(rowContainer).color;
      let bgColor = '';
      for (let container = rowContainer; container; container = container.parentElement) {
        const color = getComputedStyle(container).backgroundColor;
        if (color && color !== BACKGROUND_DEFAULT_COLOR) {
          bgColor = color;
          break;
        }
      }
      horizonChart.setTitle(id);
      horizonChart.setTitleFontSize(TITLE_FONT_SIZE);
      horizonChart.setTitlePosition(TITLE_POSITION_X, TITLE_POSITION_Y);
      horizonChart.setTitleColer(fontColor);
      horizonChart.setHeight(CHART_HEIGHT);
      horizonChart.setDataSet(values);
      horizonChart.setSelector(selector);
      horizonChart.setColors(colors[idx % 2]);
      horizonChart.setShowXScale(false);
      horizonChart.draw();

      const title = rowContainer.querySelector('svg g text');
      const bbox = title.getBBox();
      const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
      rect.setAttribute('x', bbox.x - 1);
      rect.setAttribute('y', bbox.y);
      rect.setAttribute('width', bbox.width + 2);
      rect.setAttribute('height', bbox.height);
      rect.setAttribute('fill', bgColor);
      rect.style.opacity = '0.8';
      title.before(rect);

      rowContainer.style.marginBottom = '0';

      if (!first || idx > 0) {
        rowContainer.style.borderTop = '1px solid #ddd';
      }
    });
  };
})();
