INTRODUCTION
============

- Cloud Budget enables to manage budgets / credits in each logged-in user.
- This is a supplemental module of AWS Cloud and Kubernetes (K8s) module,
  therefore the module works with AWS Cloud or K8s module.

- See also [Cloud](https://drupal.org/project/cloud/) module.

REQUIREMENTS
============

- Drupal `10.x` or higher (The latest version of Drupal `10.x`)
- Cloud `8.x` or higher

INSTALLATION
============

- `composer require drupal/cloud`
