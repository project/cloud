<?php

/**
 * @file
 * Cloud Budget module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\user\Entity\User;

/**
 * Implements hook_help().
 */
function cloud_budget_help($route_name, RouteMatchInterface $route_match): string {
  switch ($route_name) {
    case 'help.page.cloud_budget':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<ul>';
      $output .= '<li>' . \Drupal::service('cloud')->getVersion() . '</li>';
      $output .= '<li>' . t('The cloud budget module allows users to manage Cloud Budget.') . '</li>';
      $output .= '</ul>';
      $output .= '<p>' . t('For more information, see the <a href=":cloud_documentation">online documentation for the Cloud Budget module</a>.', [':cloud_documentation' => 'https://www.drupal.org/docs/8/modules/cloud']) . '</p>';
      return $output;

    default:
      return '';
  }
}

/**
 * Implements hook_form_alter().
 */
function cloud_budget_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  if (strpos($form_id, 'views_form_cloud_credit_') === 0) {
    $form['#submit'][] = 'cloud_views_bulk_form_submit';
  }
}

/**
 * Implements hook_cron().
 */
function cloud_budget_cron() {
  // Update credits.
  cloud_budget_update_credits();
}

/**
 * Implements hook_ENTITY_TYPE_predelete().
 */
function cloud_budget_user_predelete($account): void {
  // Reassign cloud store entities.
  $cloud_store_ids = \Drupal::entityTypeManager()
    ->getStorage('cloud_credit')
    ->getQuery()
    ->accessCheck(TRUE)
    ->condition('uid', $account->id())
    ->execute();

  \Drupal::service('cloud')->reassignUids($cloud_store_ids, [
    'uid' => 0,
  ], 'cloud_credit', FALSE, $account, [
    // Do additional processing specific to cloud_credit entities.
    'cloud_budget_delete_cloud_credit_user_callback',
  ]);
}

/**
 * Additional callback during user delete operation.
 *
 * Callback queries the cloud_credit table and deletes any entities
 * where the user field = user being deleted.
 *
 * @param array $entities
 *   Array of entity ids.
 * @param array $updates
 *   Array of key/values to update.
 * @param string $entity_type
 *   The entity type.
 * @param bool $revision
 *   TRUE if the entities support revisions.
 * @param \Drupal\user\Entity\User $account
 *   User entity.
 * @param array|\ArrayAccess $context
 *   An array of contextual key/values.
 */
function cloud_budget_delete_cloud_credit_user_callback(array $entities, array $updates, string $entity_type, bool $revision, User $account, &$context): void {
  if (!isset($context['sandbox']['progress'])) {
    // Load cloud_credit entities.
    $cloud_store_entities = [];
    try {
      $cloud_store_entities = \Drupal::entityTypeManager()
        ->getStorage('cloud_credit')
        ->loadByProperties([
          'user' => $account->id(),
        ]);
    }
    catch (\Exception $e) {
      \Drupal::service('cloud')->handleException($e);
    }
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($cloud_store_entities);
    $context['sandbox']['entities'] = $cloud_store_entities;
  }

  try {
    $count = min(5, count($context['sandbox']['entities']));
    for ($i = 1; $i <= $count; $i++) {
      /** @var \Drupal\cloud_budget\Entity\CloudCredit $entity */
      $entity = array_shift($context['sandbox']['entities']);
      $entity->delete();

      // Update our progress information.
      $context['sandbox']['progress']++;
    }
  }
  catch (\Exception $e) {
    \Drupal::service('cloud')->handleException($e);
    // Set progress value to max so batch processing completes.
    $context['sandbox']['progress'] = $context['sandbox']['max'];
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] !== $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

}

/**
 * Update credits.
 */
function cloud_budget_update_credits() {
  $cloud_credits = \Drupal::entityTypeManager()
    ->getStorage('cloud_credit')
    ->loadByProperties([]);

  $now = time();
  foreach ($cloud_credits ?: [] as $cloud_credit) {
    $results = \Drupal::service('plugin.manager.cloud_cost_calculator')->calculateAll(
      $cloud_credit->getUser()->id(),
      $cloud_credit->getCloudContext(),
      $cloud_credit->getRefreshed(),
      $now
    );

    $cost = array_sum($results);
    $cloud_credit->setAmount(max($cloud_credit->getAmount() - $cost, 0));
    $cloud_credit->setRefreshed($now);
    $cloud_credit->save();

    \Drupal::logger('cloud_budget')->info(t('@type: updated %label.', [
      '@type' => $cloud_credit->bundle(),
      '%label' => $cloud_credit->label(),
    ]), [
      'link' => $cloud_credit->toLink(t('View'))->toString(),
    ]);
  }
}
