<?php

namespace Drupal\Tests\cloud_budget\Traits;

use Drupal\Tests\cloud\Traits\CloudTestEntityTrait;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud_budget\Entity\CloudCredit;

/**
 * The trait creating test entity for cloud budget testing.
 */
trait CloudBudgetTestEntityTrait {

  use CloudTestEntityTrait;

  /**
   * Create a Cloud Credit test entity.
   *
   * @param array $cloud_credit
   *   The cloud credit data.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The cloud credit entity.
   */
  protected function createCloudCreditTestEntity(array $cloud_credit): CloudContentEntityBase {
    return $this->createTestEntity(CloudCredit::class, [
      'cloud_context' => $cloud_credit['cloud_context'],
      'user' => $cloud_credit['user'],
      'amount' => $cloud_credit['amount'],
      'refreshed' => time(),
    ]);
  }

}
