<?php

namespace Drupal\cloud_cluster_worker\Form\Config;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Routing\RouteProvider;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Traits\CloudFormTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Cloud Orchestrator Worker Admin Settings.
 */
class CloudClusterWorkerAdminSettings extends ConfigFormBase {

  use CloudFormTrait;

  const API_PATH = '/entity/cloud_cluster_worker?_format=json';

  /**
   * The cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloud;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProvider
   */
  protected $routeProvider;

  /**
   * Guzzle Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * The Messenger Service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * CloudClusterWorkerAdminSettings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud
   *   The cloud service.
   * @param \Drupal\Core\Routing\RouteProvider $route_provider
   *   The route provider.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The uuid service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CloudServiceInterface $cloud,
    RouteProvider $route_provider,
    ClientInterface $http_client,
    UuidInterface $uuid_service,
    Messenger $messenger,
    TypedConfigManagerInterface $typed_config_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->cloud = $cloud;
    $this->routeProvider = $route_provider;
    $this->httpClient = $http_client;
    $this->uuidService = $uuid_service;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cloud'),
      $container->get('router.route_provider'),
      $container->get('http_client'),
      $container->get('uuid'),
      $container->get('messenger'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cloud_cluster_worker_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['cloud_cluster_worker.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('cloud_cluster_worker.settings');

    $form['cloud_cluster_worker'] = [
      '#type' => 'details',
      '#title' => $this->t('Cloud Orchestrator'),
      '#open' => TRUE,
    ];

    $worker_id = $config->get('cloud_cluster_worker_id');
    if (empty($worker_id)) {
      $worker_id = $this->config('system.site')->get('uuid');
    }

    $form['cloud_cluster_worker']['cloud_cluster_worker_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID'),
      '#description' => $this->t('The ID of worker.'),
      '#disabled' => TRUE,
      '#default_value' => $worker_id,
    ];

    $form['cloud_cluster_worker']['cloud_cluster_worker_api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Endpoint'),
      '#description' => $this->t('API Endpoint of Cloud Orchestrator. e.g. https://example.com/'),
      '#required' => TRUE,
      '#default_value' => $config->get('cloud_cluster_worker_api_endpoint'),
    ];

    $form['cloud_cluster_worker']['cloud_cluster_worker_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token'),
      '#description' => $this->t('Token of Cloud Orchestrator.'),
      '#required' => TRUE,
      '#default_value' => $config->get('cloud_cluster_worker_token'),
    ];

    $form['cloud_cluster_worker']['cloud_cluster_worker_tags'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tags'),
      '#description' => $this->t('Tags of worker separated by comma. For example, "tag1, tag2".'),
      '#default_value' => $config->get('cloud_cluster_worker_tags'),
    ];

    $form['location'] = [
      '#type' => 'details',
      '#title' => $this->t('Location'),
      '#open' => TRUE,
    ];

    $form['location']['cloud_cluster_worker_location_country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => CountryManager::getStandardList(),
      '#empty_value' => '',
      '#default_value' => $config->get('cloud_cluster_worker_location_country'),
    ];

    $form['location']['cloud_cluster_worker_location_city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#default_value' => $config->get('cloud_cluster_worker_location_city'),
    ];

    $form['location']['cloud_cluster_worker_location_latitude'] = [
      '#type' => 'number',
      '#title' => $this->t('Latitude'),
      '#step' => 0.000001,
      '#default_value' => $config->get('cloud_cluster_worker_location_latitude'),
    ];

    $form['location']['cloud_cluster_worker_location_longitude'] = [
      '#type' => 'number',
      '#title' => $this->t('Longitude'),
      '#step' => 0.000001,
      '#default_value' => $config->get('cloud_cluster_worker_location_longitude'),
    ];

    if ($this->cloud->isGeocoderAvailable()) {
      $path = $this->routeProvider->getRouteByName('entity.cloud_config.geocoder')->getPath();
      $path = str_replace('{country}', 'country', $path);
      $path = str_replace('{city}', 'city', $path);
      $url = Url::fromUri('internal:' . $path);
      $form['#attached']['library'] = 'cloud/cloud_geocoder';
      $form['#attached']['drupalSettings']['cloud']['geocoder_url'] = $url->toString();
      $form['#attached']['drupalSettings']['cloud']['country_id'] = 'edit-cloud-cluster-worker-location-country';
      $form['#attached']['drupalSettings']['cloud']['city_id'] = 'edit-cloud-cluster-worker-location-city';
      $form['#attached']['drupalSettings']['cloud']['latitude_id'] = 'edit-cloud-cluster-worker-location-latitude';
      $form['#attached']['drupalSettings']['cloud']['longitude_id'] = 'edit-cloud-cluster-worker-location-longitude';
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('cloud_cluster_worker.settings');

    try {
      $this->registerWorker($form_state);
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      $this->messenger->addError($this->t('Failed to register the worker.'));
      return;
    }
    $this->messenger->addStatus('The worker is registered.');

    $config->set('cloud_cluster_worker_id', $form_state->getValue('cloud_cluster_worker_id'));
    $config->set('cloud_cluster_worker_api_endpoint', $form_state->getValue('cloud_cluster_worker_api_endpoint'));
    $config->set('cloud_cluster_worker_token', $form_state->getValue('cloud_cluster_worker_token'));
    $config->set('cloud_cluster_worker_tags', $form_state->getValue('cloud_cluster_worker_tags'));
    $config->set('cloud_cluster_worker_location_country', $form_state->getValue('cloud_cluster_worker_location_country'));
    $config->set('cloud_cluster_worker_location_city', $form_state->getValue('cloud_cluster_worker_location_city'));
    $config->set('cloud_cluster_worker_location_latitude', $form_state->getValue('cloud_cluster_worker_location_latitude'));
    $config->set('cloud_cluster_worker_location_longitude', $form_state->getValue('cloud_cluster_worker_location_longitude'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Log errors to watchdog and throw an exception.
   *
   * @param string $type
   *   The error type.
   * @param \Exception $exception
   *   The exception object.
   * @param string $message
   *   The message to log.
   * @param bool $throw
   *   TRUE to throw exception.
   *
   * @throws \Exception
   */
  private function logError($type, \Exception $exception, $message, $throw = TRUE): void {
    // See also: https://www.drupal.org/node/2932520.
    Error::logException($this->logger($type), $exception, $message);
    if ($throw === TRUE) {
      throw new \Exception($exception->getMessage());
    }
  }

  /**
   * Register a worker.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @throws \Exception
   */
  private function registerWorker(FormStateInterface $form_state): void {
    try {
      $headers = [
        'Authorization' => 'Bearer ' . base64_encode($form_state->getValue('cloud_cluster_worker_token') ?? ''),
        'Content-Type' => 'application/json',
      ];

      $endpoint_url = $form_state->getValue('cloud_cluster_worker_api_endpoint') ?? '';

      // Remove the '/' if the endpoint url ends with '/'.
      if (substr($endpoint_url, -1) === '/') {
        $endpoint_url = substr($endpoint_url, 0, strlen($endpoint_url) - 1);
      }

      // @todo Add field tags.
      $response = $this->httpClient->request(
        'POST',
        $endpoint_url . self::API_PATH,
        [
          'body' => json_encode([
            'name' => ['value' => $form_state->getValue('cloud_cluster_worker_id')],
            'location_country' => ['value' => $form_state->getValue('cloud_cluster_worker_location_country')],
            'location_city' => ['value' => $form_state->getValue('cloud_cluster_worker_location_city')],
            'location_latitude' => ['value' => $form_state->getValue('cloud_cluster_worker_location_latitude')],
            'location_longitude' => ['value' => $form_state->getValue('cloud_cluster_worker_location_longitude')],
          ]),
          'headers' => $headers,
        ]
      );

      $output = json_decode($response->getBody()->getContents(), TRUE);
      if ($response->getStatusCode() !== 201) {
        throw new \Exception($output['error'] ?? 'Unknown error.');
      }
    }
    catch (ClientException $error) {
      $response = $error->getResponse();
      $response_info = $response->getBody()->getContents();
      $message = new FormattableMarkup(
        'Cloud Orchestrator API error. Error details are as follows: <pre>@response</pre>',
        [
          '@response' => json_decode($response_info)->message,
        ]
      );

      $this->logError('Remote API Connection', $error, $message);
    }
    catch (\Exception $error) {
      $this->logError('Remote API Connection', $error, $this->t('An unknown error
      occurred while trying to connect to the remote API. This is not a Guzzle
      error, nor an error in the remote API, rather a generic local error
      occurred. The reported error was @error',
        ['@error' => $error->getMessage()]
      ));
    }
  }

}
