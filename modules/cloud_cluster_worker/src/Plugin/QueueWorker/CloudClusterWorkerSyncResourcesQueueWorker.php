<?php

namespace Drupal\cloud_cluster_worker\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\cloud_cluster_worker\Service\CloudClusterWorkerServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes for Cloud Cluster Worker Sync Resources Queue.
 *
 * @QueueWorker(
 *   id = "cloud_cluster_worker_sync_resources_queue",
 *   title = @Translation("Cloud Cluster Worker Sync Resources Queue"),
 *   cron = {"time" = 60}
 * )
 */
class CloudClusterWorkerSyncResourcesQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The resources synchronization service.
   *
   * @var \Drupal\cloud_cluster_worker\Service\CloudClusterWorkerService
   */
  protected $clusterWorker;

  /**
   * Constructs a new LocaleTranslation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\cloud_cluster_worker\Service\CloudClusterWorkerServiceInterface $cluster_worker
   *   The resources synchronization service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    CloudClusterWorkerServiceInterface $cluster_worker,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->clusterWorker = $cluster_worker;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('cloud_cluster_worker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $this->clusterWorker->sendSyncRequest($data['cloud_context'], $data['entity_type'], $data['resource_jsons']);
  }

}
