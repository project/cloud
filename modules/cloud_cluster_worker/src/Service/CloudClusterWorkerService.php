<?php

namespace Drupal\cloud_cluster_worker\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Provides the service for the cloud_cluster_worker.
 */
class CloudClusterWorkerService extends CloudServiceBase implements CloudClusterWorkerServiceInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The serializer object.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new CloudClusterWorkerService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle Http client.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   The serializer object.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory instance.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    QueueFactory $queue_factory,
    Serializer $serializer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    LoggerChannelFactoryInterface $logger_factory,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    // Set up the entity type manager for querying entities.
    $this->entityTypeManager = $entity_type_manager;

    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->queueFactory = $queue_factory;
    $this->serializer = $serializer;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->logger = $logger_factory->get('cloud_cluster_worker');
  }

  /**
   * {@inheritdoc}
   */
  public function receiveOperations(): void {

    $this->logger->info($this->t('Starting execution of CloudClusterWorkerService->receiveOperations()'));

    if (!cloud_cluster_worker_is_configured()) {
      $this->logger->warning($this->t('The cloud cluster worker is not configured.'));
      return;
    }

    $config = $this->configFactory->get('cloud_cluster_worker.settings');

    try {
      $headers = [
        'Authorization' => 'Bearer ' . base64_encode($config->get('cloud_cluster_worker_token') ?? ''),
        'Content-Type' => 'application/json',
      ];

      $endpoint_url = $config->get('cloud_cluster_worker_api_endpoint');
      // Remove the '/' if the endpoint url ends with '/'.
      if (substr($endpoint_url, -1) === '/') {
        $endpoint_url = substr($endpoint_url, 0, -1);
      }

      $cloud_cluster_worker_name = $config->get('cloud_cluster_worker_id');

      $response = $this->httpClient->request(
        'POST',
        $endpoint_url . '/cloud_cluster/operation',
        [
          'body' => json_encode([
            'cloud_cluster_worker_name' => $cloud_cluster_worker_name,
          ], JSON_THROW_ON_ERROR),
          'headers' => $headers,
        ]
      );

      $output = json_decode($response->getBody()
        ->getContents(), TRUE, 512, JSON_THROW_ON_ERROR);
      if ($response->getStatusCode() !== 200) {
        $this->logger->error(!empty($output['error']) ? $output['error'] : $this->t('Unknown error.'));
        return;
      }
      $this->logger->info('Operations: ' . $response->getBody());

      $results = [];
      foreach ($output ?: [] as $item) {

        $cloud_context = $item['cloud_context'];

        if (($pos = strrpos($cloud_context, '_')) !== FALSE) {
          $cloud_context = substr($cloud_context, 0, $pos);
        }

        // Run operation.
        $service_name = $item['service_name'];
        // phpcs:ignore
        $service = \Drupal::service($service_name); // @phpstan-ignore-line
        $service->setCloudContext($cloud_context);

        $method_name = lcfirst($item['operation']);
        if (!method_exists($service, $method_name)) {
          $this->logger->error($this->t('The method @method_name does not exist.', [
            '@method_name' => $method_name,
          ]));
          continue;
        }

        // Resolve reference in parameters.
        $item['params'] = $this->resolveRefs($item['params'], $item['refs'] ?? [], $results);

        $result = call_user_func_array(
          [$service, $method_name],
          $item['params']
        );

        $results[$method_name] = $result;

        if (empty($item['update_entities_method'])) {
          continue;
        }

        $update_entities_method = $item['update_entities_method'];
        if (!method_exists($service, $update_entities_method)) {
          continue;
        }

        // Update entities.
        $item['update_entities_method_params'][0]['result'] = $result;
        call_user_func_array(
          [$service, $update_entities_method],
          $item['update_entities_method_params']
        );
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function syncResources(): void {
    $this->logger->info($this->t('Starting execution of CloudClusterWorkerService->syncResources().'));

    if (!cloud_cluster_worker_is_configured()) {
      $this->logger->warning($this->t('The cloud cluster worker is not configured.'));
      return;
    }

    $entity_type_definitions = $this->entityTypeManager->getDefinitions();
    $resources_queue = $this->queueFactory->get('cloud_cluster_worker_sync_resources_queue');
    $definitions = $this->cloudConfigPluginManager->getDefinitions();
    foreach ($definitions ?: [] as $key => $definition) {
      // Ignore definitions like "aws_cloud.cloud_config:1" whose base_plugin
      // is undefined. And because the value of $definition['base_plugin'] is
      // empty string, the "empty" function cannot be used in the condition.
      if (!isset($definition['base_plugin'])) {
        continue;
      }
      $configs = $this->cloudConfigPluginManager->createInstance($key)->loadConfigEntities();
      $cloud_name = substr($key, 0, strlen($key) - strlen('.cloud_config'));
      // Skip cloud cluster.
      if ($cloud_name === 'cloud_cluster') {
        continue;
      }

      foreach ($configs ?: [] as $config) {
        if ($config->isRemote()) {
          continue;
        }

        $resources_queue->createItem([
          'cloud_context' => $config->getCloudContext(),
          'resource_jsons' => [$this->serializer->serialize($config, 'json')],
          'entity_type' => 'cloud_config',
        ]);

        // Loop for entity types, such as aws_cloud_instance, aws_cloud_image.
        foreach ($entity_type_definitions ?: [] as $entity_type_key => $entity_type_definition) {
          if ($entity_type_key !== 'cloud_launch_template' && strpos($entity_type_key, $cloud_name) !== 0) {
            continue;
          }

          $resources = $this->entityTypeManager
            ->getStorage($entity_type_key)
            ->loadByProperties([
              'cloud_context' => $config->getCloudContext(),
            ]);

          $resource_jsons = [];
          foreach ($resources ?: [] as $resource) {
            $resource_jsons[] = $this->serializer->serialize($resource, 'json');
          }

          $resources_queue->createItem([
            'cloud_context' => $config->getCloudContext(),
            'resource_jsons' => $resource_jsons,
            'entity_type' => $entity_type_key,
          ]);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendSyncRequest($cloud_context, $entity_type, array $resource_jsons, $clear = TRUE): void {
    $config = $this->configFactory
      ->get('cloud_cluster_worker.settings');

    try {
      $headers = [
        'Authorization' => 'Bearer ' . base64_encode($config->get('cloud_cluster_worker_token') ?? ''),
        'Content-Type' => 'application/json',
      ];

      $endpoint_url = $config->get('cloud_cluster_worker_api_endpoint');
      if (empty($endpoint_url)) {
        return;
      }

      // Remove the '/' if the endpoint url ends with '/'.
      if (substr($endpoint_url, -1) === '/') {
        $endpoint_url = substr($endpoint_url, 0, -1);
      }

      $cloud_cluster_worker_name = $config->get('cloud_cluster_worker_id');
      $response = $this->httpClient->request(
        'POST',
        $endpoint_url . self::API_PATH,
        [
          'body' => json_encode([
            'cloud_cluster_worker_name' => $cloud_cluster_worker_name,
            'cloud_context' => $cloud_context . '_' . substr($cloud_cluster_worker_name, -self::CLOUD_CONTEXT_UUID_LENGTH),
            'entity_type' => $entity_type,
            'resource_jsons' => $resource_jsons,
            'clear' => $clear,
          ], JSON_THROW_ON_ERROR),
          'headers' => $headers,
        ]
      );

      $output = json_decode($response->getBody()
        ->getContents(), TRUE, 512, JSON_THROW_ON_ERROR);
      if ($response->getStatusCode() !== 200) {
        $this->logger->error($output['error'] ?? $this->t('Unknown error.'));
      }
    }
    catch (\Exception $error) {
      $this->logger->error($error->getMessage());
    }
  }

  /**
   * Resolve references in parameters.
   *
   * @param array $params
   *   The parameters.
   * @param array $refs
   *   The reference information.
   * @param array $results
   *   The results of method call.
   *
   * @return array
   *   The parameters processed.
   */
  private function resolveRefs(array $params, array $refs, array $results): array {
    $param_refs = [];
    foreach ($refs as $key => $ref) {
      $parts = explode('#', $ref);
      if (!empty($results[$parts[0]])) {
        $param_refs[$key] = $results[$parts[0]][$parts[1]];
      }
    }

    array_walk_recursive($params, static function (&$value, $key) use ($param_refs, $refs) {
      if (!empty($param_refs[$key]) && $value === $refs[$key]) {
        $value = $param_refs[$key];
      }
    });

    return $params;
  }

}
