<?php

namespace Drupal\cloud_cluster_worker\Service;

/**
 * Interface for the CloudClusterWorkerService.
 */
interface CloudClusterWorkerServiceInterface {

  const API_PATH = '/cloud_cluster/sync';

  const CLOUD_CONTEXT_UUID_LENGTH = 12;

  /**
   * Receive operations from the master site.
   */
  public function receiveOperations(): void;

  /**
   * Synchronize entity resources.
   */
  public function syncResources(): void;

  /**
   * Send synchronization request.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type
   *   The entity type.
   * @param array $resource_jsons
   *   The resource jsons.
   * @param bool $clear
   *   Whether clear other resources not included in $resource_jsons or not.
   */
  public function sendSyncRequest($cloud_context, $entity_type, array $resource_jsons, $clear = TRUE): void;

}
