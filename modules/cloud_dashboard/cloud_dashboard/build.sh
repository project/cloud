#!/bin/bash
# Read the .po file and convert it to translation code.
sed '/^#/d;/^$/d' ../../../translations/ja.po |
  tr '\n' ' ' |
  sed 's/ msgid /\nmsgid /g' |
  sed 's/ msgstr /\nmsgstr /g' |
  sed 's/ msgstr\[0\] /\nmsgstr /g' |
  sed 's/ msgstr\[1\] /\nmsgstr /g' |
  sed 's/ msgid_plural /\nmsgid /g' |
  sed '/^msgstr/d' |
  sed 's/msgid "" "/msgid "/g' |
  sed 's/" "/\\n/g' |
  sed '/^msgid ""/d' |
  sort |
  uniq |
  sed -n 's/^msgid "\(.*\)"/Drupal.t("\1");/p' > src/constant/translations/translation.ts

# Build cloud_dashboard
npx vite build

# Remove old files
rm -f ../js/index.js
rm -f ../css/index.css

# Copy new files from build directory
cp build/assets/index.*.js ../js/index.js
cp build/assets/index.*.css ../css/index.css

# Add csslint ignore to bypass DrupalCI checking
# of the compiled css file.
tee ../css/index.css >/dev/null << EOF
/* stylelint-disable */
/* csslint ignore:start */
$(cat ../css/index.css)
/* csslint ignore:end */
EOF
