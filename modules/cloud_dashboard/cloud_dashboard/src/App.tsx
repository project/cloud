import 'leaflet/dist/leaflet.css';

import { BypassAnimationContext, useBypassAnimation } from 'hooks/bypass_animation';
import { CloudContextListContext, useCloudContextList } from 'hooks/cloud_context_list';
import {
    OpenStackHeatFormTemplateContext, useOpenStackHeatFormTemplate
} from 'hooks/openstack_heat_form_template';
import { StatusMessageContext, useStatusMessage } from 'hooks/status_message';
import { UserInformationContext, useUserInformation } from 'hooks/user_information';
import CallbackPage from 'pages/CallbackPage';
import CloudProjectPage from 'pages/CloudProjectPage';
import CloudServerTemplatePage from 'pages/CloudServerTemplatePage';
import CloudStorePage from 'pages/CloudStorePage';
import EntityCreatePage from 'pages/EntityCreatePage';
import EntityDetailPage from 'pages/EntityDetailPage';
import EntitySubDetailPage from 'pages/EntitySubDetailPage';
import EntityViewPage from 'pages/EntityViewPage';
import EntityXxudPage from 'pages/EntityXxudPage';
import LoginPage from 'pages/LoginPage';
import OpenStackHeatInputExtraPage from 'pages/openstack/heat/OpenStackHeatInputExtraPage';
import OpenStackHeatInputTemplatePage from 'pages/openstack/heat/OpenStackHeatInputTemplatePage';
import OpenStackInstanceActionLogPage from 'pages/openstack/instance/OpenStackInstanceActionLogPage';
import OpenStackInstanceConsoleOutputPage from 'pages/openstack/instance/OpenStackInstanceConsoleOutputPage';
import OpenStackInstanceConsolePage from 'pages/openstack/instance/OpenStackInstanceConsolePage';
import ProviderPage from 'pages/ProviderPage';
import TemplateCreatePage from 'pages/TemplateCreatePage';
import { useEffect } from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';
import { DataTableSelectedRowsContext, useDataTableSelectedRows } from 'hooks/data_table_selected_row';

/**
 * Application routes.
 */
const routes = [
  { path: '/', component: LoginPage, exact: true },
  { path: '/callback', component: CallbackPage },
  { path: '/providers', component: ProviderPage },
  { path: '/k8s_cost_store', component: () => <CloudStorePage bundleId="k8s_cost_store" /> },
  { path: '/k8s_namespace_resource_store', component: () => <CloudStorePage bundleId="k8s_namespace_resource_store" /> },
  { path: '/k8s_node_resource_store', component: () => <CloudStorePage bundleId="k8s_node_resource_store" /> },
  { path: '/k8s_pod_resource_store', component: () => <CloudStorePage bundleId="k8s_pod_resource_store" /> },
  { path: '/design/:cloudServiceProvider/server_template_list', component: CloudServerTemplatePage },
  { path: '/design/server_template/:cloudContext/:cloudServiceProvider/add', component: TemplateCreatePage },
  { path: '/design/server_template/:cloudContext/:entityId/:action', component: () => <EntityXxudPage entityName="cloud_launch_template" /> },
  { path: '/design/server_template/:cloudContext/:entityId', component: () => <EntityDetailPage entityName="cloud_launch_template" /> },
  { path: '/design/server_template/:cloudContext', component: CloudServerTemplatePage },
  { path: '/:cloudServiceProvider/cloud_project', component: CloudProjectPage },
  { path: '/:cloudServiceProvider/:cloudContext/cloud_project', component: CloudProjectPage },
  { path: '/:cloudServiceProvider/:cloudContext/:entityName/:entityId/remove_interface/:subEntityId', component: () => <EntityXxudPage entityName="port" anotherEntityName="interface" action="delete" entityId="subEntityId" /> },
  { path: '/:cloudServiceProvider/:cloudContext/:entityName/:entityId/:subType/:subEntityId', component: EntitySubDetailPage },
  { path: '/openstack/:cloudContext/stack/:entityId/edit', component: () => <OpenStackHeatInputTemplatePage action="edit" titleText="Edit @name" />, exact: true },
  { path: '/openstack/:cloudContext/stack/:entityId/edit_extra', component: () => <OpenStackHeatInputExtraPage action="edit" titleText="Edit @name" />, exact: true },
  { path: '/openstack/:cloudContext/instance/:entityId/console_output', component: OpenStackInstanceConsoleOutputPage, exact: true },
  { path: '/openstack/:cloudContext/instance/:entityId/console', component: OpenStackInstanceConsolePage, exact: true },
  { path: '/openstack/:cloudContext/instance/:entityId/action_log', component: OpenStackInstanceActionLogPage, exact: true },
  { path: '/:cloudServiceProvider/:cloudContext/:entityName/:entityId/:action', component: EntityXxudPage },
  { path: '/openstack/:cloudContext/stack/add', component: () => <OpenStackHeatInputTemplatePage action="create" titleText="Add stack - template" />, exact: true },
  { path: '/openstack/:cloudContext/stack/add_extra', component: () => <OpenStackHeatInputExtraPage action="create" titleText="Add stack - template" />, exact: true },
  { path: '/openstack/:cloudContext/stack/preview', component: () => <OpenStackHeatInputTemplatePage action="preview" titleText="Preview stack - template" />, exact: true },
  { path: '/openstack/:cloudContext/stack/preview_extra', component: () => <OpenStackHeatInputExtraPage action="preview" titleText="Preview stack - template" />, exact: true },
  { path: '/:cloudServiceProvider/:cloudContext/:entityName/add', component: () => <EntityCreatePage action="create" />, exact: true },
  { path: '/:cloudServiceProvider/:cloudContext/:entityName/import', component: () => <EntityCreatePage action="import" />, exact: true },
  { path: '/:cloudServiceProvider/:cloudContext/:entityName/:entityId', component: EntityDetailPage },
  { path: '/:cloudServiceProvider/:cloudContext/:entityName', component: EntityViewPage },
  { path: '/:cloudServiceProvider/:entityName', component: EntityViewPage },
];

/**
 * Component for application route.
 */
const App = () => {

  const statusMessage = useStatusMessage();
  const cloudContextList = useCloudContextList();
  const bypassAnimation = useBypassAnimation();
  const userInformation = useUserInformation();
  const heatTemplate = useOpenStackHeatFormTemplate();
  const dataTableSelectedRows = useDataTableSelectedRows();
  const location = useLocation();
  useEffect(() => {
    statusMessage.removeAllMessage();
  }, [location]);

  return <CloudContextListContext.Provider value={cloudContextList}>
    <BypassAnimationContext.Provider value={bypassAnimation}>
      <StatusMessageContext.Provider value={statusMessage}>
        <UserInformationContext.Provider value={userInformation}>
          <OpenStackHeatFormTemplateContext.Provider value={heatTemplate}>
            <DataTableSelectedRowsContext.Provider value={dataTableSelectedRows}>
              <Switch>
                {routes.map((route, i) => (
                  <Route key={i} exact={route.exact} path={route.path} component={route.component} />
                ))}
              </Switch>
            </DataTableSelectedRowsContext.Provider>
          </OpenStackHeatFormTemplateContext.Provider>
        </UserInformationContext.Provider>
      </StatusMessageContext.Provider>
    </BypassAnimationContext.Provider>
  </CloudContextListContext.Provider>;

}

export default App;
