import { Form } from 'react-bootstrap';

/**
 * Label parts in form.
 *
 * @param children Children Node.
 */
const FormSelect = ({ value, setvalue, dataList, className }: {
  value: string,
  setvalue: (s: string) => void,
  dataList: string[],
  className?: string
}) => {

  return <Form.Select className={className} value={value}
    onChange={(e) => {
      setvalue(e.currentTarget.value);
    }}>
    <option value="">- {Drupal.t('All')} -</option>
    {dataList.map((data) => {
      return <option value={data} key={data}>{data}</option>
    })}
  </Form.Select>;

}

export default FormSelect;
