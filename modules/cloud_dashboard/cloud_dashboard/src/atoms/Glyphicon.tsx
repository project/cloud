/**
 * The wrapper of glyphicon.
 * @param type The type of glyphicon.
 * @param unMargin Whether to remove the right margin.
 */
const Glyphicon = ({ type, unMargin}: {
  type: string,
  unMargin?: boolean,
}) => {
  return <span className={`glyphicon glyphicon-${type}`} style={
    unMargin ? {'marginRight': 0} : {}
  }></span>;
}
export default Glyphicon;
