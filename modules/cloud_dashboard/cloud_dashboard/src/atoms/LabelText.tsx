import Glyphicon from 'atoms/Glyphicon';
import { LABEL_ICON_LIST } from 'constant/others/other';

/**
 * Return the translated text with icon.
 * @param text The text to be translated.
 */
const LabelText = ({ text, args }: {
  text: string;
  args?: Record<string, string>;
}) => {
  let iconInfo = LABEL_ICON_LIST.matches.find((record) => {
    return record.label === text;
  });
  if (!iconInfo) {
    iconInfo = LABEL_ICON_LIST.contains.find((record) => {
      return text.includes(record.label);
    });
  }

  return (
    <>
      {
        iconInfo
          ? <Glyphicon type={iconInfo.icon} />
          : <></>
      } {Drupal.t(text, args)}
    </>
  );
};

export default LabelText;
