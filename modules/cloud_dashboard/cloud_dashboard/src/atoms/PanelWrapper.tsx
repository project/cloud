import useDrupalTranslation from 'hooks/drupal_translation';
import { Card } from 'react-bootstrap';

const PanelWrapper = ({ children, title }: {
  children: React.ReactNode,
  title: string
}) => {

  const { t } = useDrupalTranslation();

  return <details className="card" open>
    <summary role="button" aria-expanded="true" area-pressed="true"
      className="card-header">{t(title)}</summary>
    <Card.Body className="details-wrapper">
    {children}
    </Card.Body>
  </details>;

}

export default PanelWrapper;
