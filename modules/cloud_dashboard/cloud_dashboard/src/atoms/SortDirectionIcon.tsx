/**
 * Icon glyph for showing sort direction.
 *
 * @param direction Sort direction.
 */
const SortDirectionIcon = ({ direction }: { direction: 'ASC' | 'DESC' }) => {

  switch (direction) {
    case 'ASC':
      return <span className='caret downarrow'></span>;
    case 'DESC':
      return <span className='caret'></span>;
  }

};

export default SortDirectionIcon;
