import CloudContext from 'model/CloudContext';
import CloudServiceProvider from 'model/CloudServiceProvider';

// The name of the client to be used for OAuth2 authentication,
// corresponding to the description in INSTALL.md.
export const OAUTH2_CLIENT_LABEL = 'Cloud Dashboard';

// Client secret used when performing OAuth2 authentication,
// corresponding to the description in INSTALL.md.
export const OAUTH2_CLIENT_SECRET = 'cloud_dashboard';

// The URL where the Cloud Dashboard will be placed,
// corresponding to the description in cloud_dashboard.routing.yml.
export const ROUTE_URL = '/clouds/dashboard';

// Fetch API timeout (in milliseconds), as referenced by drupal_jsonapi.ts.
export const FETCH_TIMEOUT_MS = 60 * 1000;

// Expiration time (in milliseconds) of the cache for the GET method
// in the Fetch API, as referenced by drupal_jsonapi.ts.
export const CACHE_EXPIRED_UNIXTIME = 1000 * 60 * 60 * 24;

// Initial list of Cloud Context (Cloud Service Provider).
export const DEFAULT_CLOUD_CONTEXTS: CloudContext[] = [
  { cloudServiceProvider: 'aws_cloud', name: 'ALL', labelName: 'AWS Cloud (ALL)' },
  { cloudServiceProvider: 'k8s', name: 'ALL', labelName: 'K8s (ALL)' },
  { cloudServiceProvider: 'openstack', name: 'ALL', labelName: 'OpenStack (ALL)' },
  { cloudServiceProvider: 'vmware', name: 'ALL', labelName: 'VMware (ALL)' },
];

export const CLOUD_CONTEXT_LABEL_NAMES = [
  { cloudServiceProvider: 'aws_cloud', labelName: 'AWS Cloud' },
  { cloudServiceProvider: 'k8s', labelName: 'K8s' },
  { cloudServiceProvider: 'openstack', labelName: 'OpenStack' },
  { cloudServiceProvider: 'vmware', labelName: 'VMware' },
];

/**
 * Entity type that does not display action buttons in ListView.
 */
export const DONT_SHOW_ACTION_ENTITY_TYPE_LIST = [
  { cloudServiceProvider: 'k8s', entityName: 'node', },
  { cloudServiceProvider: 'k8s', entityName: 'event', },
  { cloudServiceProvider: 'k8s', entityName: 'schedule', },
  { cloudServiceProvider: 'vmware', entityName: 'host', },
];

/**
 * Entity type that does not display operation links in ListView.
 */
export const DONT_SHOW_OPERATIONS_ENTITY_TYPE_LIST = [
  'k8s_node', 'k8s_event', 'openstack_template_version', 'openstack_flavor'
];

/**
 * Entity type that displays only Delete in operation links in ListView.
 */
export const DELETE_ONLY_OPERATIONS_ENTITY_TYPE_LIST = [
  'openstack_server_group'
];

/**
 * The list of actions in the ListView.
 */
export const ACTION_BUTTON_LIST = [
  { actionType: 'create', label: 'Add' },
  { actionType: 'import', label: 'Import' },
  { actionType: 'preview', label: 'Preview' },
];

const drupalSettings = (window as any).drupalSettings || {};

export const CLOUD_SERVICE_PROVIDER_LIST: CloudServiceProvider[] = drupalSettings.cloud_configs || [];

export const CLOUD_SERVICE_PROVIDER_INFO: Record<CloudServiceProvider, {
  entityTypeId: string,
  label: string,
}> = drupalSettings.cloud_config_infos || {};

export const VMWARE_OS_DICT: Record<string, string> = {
  'WINDOWS_HYPERV': 'Windows Hyper-V',
  'WINDOWS_9_SERVER_64': 'Windows 10 Server (64 bit)',
  'WINDOWS_9_64': 'Windows 10 (64 bit)',
  'WINDOWS_9': 'Windows 10',
  'WINDOWS_8_SERVER_64': 'Windows 8 Server (64 bit)',
  'WINDOWS_8_64': 'Windows 8 (64 bit)',
  'WINDOWS_8': 'Windows 8',
  'WINDOWS_7_SERVER_64': 'Windows Server 2008 R2 (64 bit)',
  'WINDOWS_7_64': 'Windows 7 (64 bit)',
  'WINDOWS_7': 'Windows 7',
  'WIN_VISTA_64': 'Windows Vista (64 bit)',
  'WIN_VISTA': 'Windows Vista',
  'WIN_NET_DATACENTER_64': 'Windows Server 2003, Datacenter Edition (64 bit) (experimental)',
  'WIN_LONGHORN_64': 'Windows Longhorn (64 bit) (experimental)',
  'WIN_LONGHORN': 'Windows Longhorn (experimental)',
  'WIN_NET_ENTERPRISE_64': 'Windows Server 2003, Enterprise Edition (64 bit)',
  'WIN_NET_STANDARD_64': 'Windows Server 2003, Standard Edition (64 bit)',
  'WIN_NET_BUSINESS': 'Windows Small Business Server 2003',
  'WIN_NET_DATACENTER': 'Windows Server 2003, Datacenter Edition',
  'WIN_NET_ENTERPRISE': 'Windows Server 2003, Enterprise Edition',
  'WIN_NET_STANDARD': 'Windows Server 2003, Standard Edition',
  'WIN_NET_WEB': 'Windows Server 2003, Web Edition',
  'WIN_XP_PRO_64': 'Windows XP Professional Edition (64 bit)',
  'WIN_XP_PRO': 'Windows XP Professional',
  'WIN_XP_HOME': 'Windows XP Home Edition',
  'WIN_2000_ADV_SERV': 'Windows 2000 Advanced Server',
  'WIN_2000_SERV': 'Windows 2000 Server',
  'WIN_2000_PRO': 'Windows 2000 Professional',
  'WIN_NT': 'Windows NT 4',
  'WIN_ME': 'Windows Millennium Edition',
  'WIN_98': 'Windows 98',
  'WIN_95': 'Windows 95',
  'WIN_31': 'Windows 3.1',
  'DOS': 'MS-DOS',
  'AMAZONLINUX2_64': 'Amazon Linux 2 (64 bit)',
  'VMWARE_PHOTON_64': 'VMware Photon (64 bit)',
  'RHEL_8_64': 'Red Hat Enterprise Linux 8 (64 bit)',
  'RHEL_7_64': 'Red Hat Enterprise Linux 7 (64 bit)',
  'RHEL_7': 'Red Hat Enterprise Linux 7',
  'RHEL_6_64': 'Red Hat Enterprise Linux 6 (64 bit)',
  'RHEL_6': 'Red Hat Enterprise Linux 6',
  'RHEL_5_64': 'Red Hat Enterprise Linux 5 (64 bit) (experimental)',
  'RHEL_5': 'Red Hat Enterprise Linux 5',
  'RHEL_4_64': 'Red Hat Enterprise Linux 4 (64 bit)',
  'RHEL_4': 'Red Hat Enterprise Linux 4',
  'RHEL_3_64': 'Red Hat Enterprise Linux 3 (64 bit)',
  'RHEL_3': 'Red Hat Enterprise Linux 3',
  'RHEL_2': 'Red Hat Enterprise Linux 2',
  'REDHAT': 'Red Hat Linux 2.1',
  'SLES_15_64': 'Suse Linux Enterprise Server 15 (64 bit)',
  'SLES_12_64': 'Suse Linux Enterprise Server 12 (64 bit)',
  'SLES_12': 'Suse linux Enterprise Server 12',
  'SLES_11_64': 'Suse Linux Enterprise Server 11 (64 bit)',
  'SLES_11': 'Suse linux Enterprise Server 11',
  'SLES_10_64': 'Suse Linux Enterprise Server 10 (64 bit) (experimental)',
  'SLES_10': 'Suse linux Enterprise Server 10',
  'SLES_64': 'Suse Linux Enterprise Server 9 (64 bit)',
  'SLES': 'Suse Linux Enterprise Server 9',
  'SUSE_64': 'Suse Linux (64 bit)',
  'SUSE': 'Suse Linux',
  'CENTOS_8_64': 'CentOS 8 (64-bit)',
  'CENTOS_7_64': 'CentOS 7 (64-bit)',
  'CENTOS_7': 'CentOS 7',
  'CENTOS_6_64': 'CentOS 6 (64-bit)',
  'CENTOS_6': 'CentOS 6',
  'CENTOS_64': 'CentOS 4/5 (64-bit)',
  'CENTOS': 'CentOS 4/5',
  'DEBIAN_10_64': 'Debian GNU/Linux 10 (64 bit)',
  'DEBIAN_10': 'Debian GNU/Linux 10',
  'DEBIAN_9_64': 'Debian GNU/Linux 9 (64 bit)',
  'DEBIAN_9': 'Debian GNU/Linux 9',
  'DEBIAN_8_64': 'Debian GNU/Linux 8 (64 bit)',
  'DEBIAN_8': 'Debian GNU/Linux 8',
  'DEBIAN_7_64': 'Debian GNU/Linux 7 (64 bit)',
  'DEBIAN_7': 'Debian GNU/Linux 7',
  'DEBIAN_6_64': 'Debian GNU/Linux 6 (64 bit)',
  'DEBIAN_6': 'Debian GNU/Linux 6',
  'DEBIAN_5_64': 'Debian GNU/Linux 5 (64 bit)',
  'DEBIAN_5': 'Debian GNU/Linux 5',
  'DEBIAN_4_64': 'Debian GNU/Linux 4 (64 bit)',
  'DEBIAN_4': 'Debian GNU/Linux 4',
  'OPENSUSE_64': 'OpenSUSE Linux (64 bit)',
  'OPENSUSE': 'OpenSUSE Linux',
  'ASIANUX_8_64': 'Asianux Server 8 (64 bit)',
  'ASIANUX_7_64': 'Asianux Server 7 (64 bit)',
  'ASIANUX_5_64': 'Asianux Server 5 (64 bit)',
  'ASIANUX_4_64': 'Asianux Server 4 (64 bit)',
  'ASIANUX_4': 'Asianux Server 4',
  'ASIANUX_3_64': 'Asianux Server 3 (64 bit)',
  'ASIANUX_3': 'Asianux Server 3',
  'FEDORA_64': 'Fedora Linux (64 bit)',
  'FEDORA': 'Fedora Linux',
  'ORACLE_LINUX_8_64': 'Oracle Linux 8 (64-bit)',
  'ORACLE_LINUX_7_64': 'Oracle Linux 7 (64-bit)',
  'ORACLE_LINUX_7': 'Oracle Linux 7',
  'ORACLE_LINUX_6_64': 'Oracle Linux 6 (64-bit)',
  'ORACLE_LINUX_6': 'Oracle Linux 6',
  'ORACLE_LINUX_64': 'Oracle Linux 4/5 (64-bit)',
  'ORACLE_LINUX': 'Oracle Linux 4/5',
  'UBUNTU_64': 'Ubuntu Linux (64 bit)',
  'UBUNTU': 'Ubuntu Linux',
  'COREOS_64': 'CoreOS Linux (64 bit)',
  'OTHER_LINUX': 'Linux 2.2x Kernel',
  'OTHER_4X_LINUX_64': 'Linux 4.x Kernel (64 bit)',
  'OTHER_4X_LINUX': 'Linux 4.x Kernel',
  'OTHER_3X_LINUX_64': 'Linux 3.x Kernel (64 bit)',
  'OTHER_3X_LINUX': 'Linux 3.x Kernel',
  'OTHER_26X_LINUX_64': 'Linux 2.6x Kernel (64 bit) (experimental)',
  'OTHER_26X_LINUX': 'Linux 2.6x Kernel',
  'OTHER_24X_LINUX_64': 'Linux 2.4x Kernel (64 bit) (experimental)',
  'OTHER_24X_LINUX': 'Linux 2.4x Kernel',
  'OTHER_LINUX_64': 'Linux (64 bit) (experimental)',
  'GENERIC_LINUX': 'Other Linux',
  'DARWIN_18_64': 'Mac OS 10.14 (64 bit)',
  'DARWIN_17_64': 'Mac OS 10.13 (64 bit)',
  'DARWIN_16_64': 'Mac OS 10.12 (64 bit)',
  'DARWIN_15_64': 'Mac OS 10.11 (64 bit)',
  'DARWIN_14_64': 'Mac OS 10.10 (64 bit)',
  'DARWIN_13_64': 'Mac OS 10.9 (64 bit)',
  'DARWIN_12_64': 'Mac OS 10.8 (64 bit)',
  'DARWIN_11_64': 'Mac OS 10.7 (64 bit)',
  'DARWIN_11': 'Mac OS 10.7',
  'DARWIN_10_64': 'Mac OS 10.6 (64 bit)',
  'DARWIN_10': 'Mac OS 10.6',
  'DARWIN_64': 'Mac OS 10.5 (64 bit)',
  'DARWIN': 'Mac OS 10.5',
  'FREEBSD_12_64': 'FreeBSD 12 x64 or later',
  'FREEBSD_11_64': 'FreeBSD 11 x64',
  'FREEBSD_12': 'FreeBSD 12 or later',
  'FREEBSD_11': 'FreeBSD 11',
  'FREEBSD_64': 'FreeBSD 10 x64 or earlier',
  'FREEBSD': 'FreeBSD 10 or earlier',
  'OS2': 'OS/2',
  'NETWARE_6': 'Novell NetWare 6.x',
  'NETWARE_5': 'Novell NetWare 5.1',
  'NETWARE_4': 'Novell NetWare 4',
  'SOLARIS_11_64': 'Solaris 11 (64 bit)',
  'SOLARIS_10_64': 'Solaris 10 (64 bit) (experimental)',
  'SOLARIS_10': 'Solaris 10 (32 bit) (experimental)',
  'SOLARIS_9': 'Solaris 9',
  'SOLARIS_8': 'Solaris 8',
  'SOLARIS_7': 'Solaris 7',
  'SOLARIS_6': 'Solaris 6',
  'UNIXWARE_7': 'SCO UnixWare 7',
  'OPENSERVER_6': 'SCO OpenServer 6',
  'OPENSERVER_5': 'SCO OpenServer 5',
  'ECOMSTATION_2': 'eComStation 2.0',
  'ECOMSTATION': 'eComStation 1.x',
  'VMKERNEL_65': 'VMware ESX 6.5',
  'VMKERNEL_6': 'VMware ESX 6',
  'VMKERNEL_5': 'VMware ESX 5',
  'VMKERNEL': 'VMware ESX 4',
};

/**
 * The list of icons and labels.
 */
export const LABEL_ICON_LIST = {
  // Text that match these specific strings are checked first.
  'matches': [
    { label: 'Disassociate', icon: 'remove' },
    { label: 'Associate', icon: 'ok' },
    { label: 'Add', icon: 'plus' },
    { label: 'Start', icon: 'play' },
    { label: 'Reboot', icon: 'repeat' },
    { label: 'Stop', icon: 'stop' },
    { label: 'Terminate', icon: 'remove' },
    { label: 'Delete', icon: 'trash' },
    { label: 'Delete | Terminate', icon: 'trash' },
    { label: 'Create', icon: 'plus' },
    { label: 'Copy', icon: 'duplicate' },
    { label: 'List', icon: 'th-list' },
    { label: 'Launch', icon: 'play' },
    { label: 'Refresh All', icon: 'refresh' },
    { label: 'Refresh', icon: 'repeat' },
    { label: 'Log out', icon: 'log-out' },
    { label: 'Apply', icon: 'ok' },
    { label: 'Attach', icon: 'tag' },
    { label: 'Detach', icon: 'tags' },
    { label: 'Accept', icon: 'ok' },
    { label: 'Approve', icon: 'ok' },
    { label: 'Review', icon: 'comment' },
    { label: 'Devel', icon: 'devel' },

    { label: 'Manage', icon: 'cog' },
    { label: 'Configure', icon: 'cog' },
    { label: 'Settings', icon: 'cog' },
    { label: 'Download', icon: 'download' },
    { label: 'Export', icon: 'export' },
    { label: 'Filter', icon: 'filter' },
    { label: 'Import', icon: 'download-alt' },
    { label: 'Save', icon: 'ok' },
    { label: 'Update', icon: 'ok' },
    { label: 'Edit', icon: 'wrench' },
    { label: 'Uninstall', icon: 'trash' },
    { label: 'Install', icon: 'plus' },
    { label: 'Write', icon: 'plus' },
    { label: 'Cancel', icon: 'remove' },
    { label: 'Remove', icon: 'trash' },
    { label: 'Reset', icon: 'remove-circle' },
    { label: 'Search', icon: 'search' },
    { label: 'Upload', icon: 'upload' },
    { label: 'Preview', icon: 'eye-open' },
    { label: 'Log in', icon: 'log-in' },
    { label: 'Console Output', icon: 'modal-window' }
  ],

  // Text containing these words anywhere in the string are checked last.
  'contains': [
    { label: 'Confirm', icon: 'ok' },
    { label: 'Restore', icon: 'ok' },
    { label: 'Rebuild', icon: 'ok' },
    { label: 'Disassociate', icon: 'remove' },
    { label: 'Disassociate Address', icon: 'remove' },
    { label: 'Associate', icon: 'ok' },
    { label: 'Add', icon: 'plus' },
    { label: 'Start', icon: 'play' },
    { label: 'Reboot', icon: 'repeat' },
    { label: 'Stop', icon: 'stop' },
    { label: 'Terminate', icon: 'remove' },
    { label: 'Delete', icon: 'trash' },
    { label: 'Delete | Terminate', icon: 'trash' },
    { label: 'Create', icon: 'plus' },
    { label: 'Copy', icon: 'duplicate' },
    { label: 'List', icon: 'th-list' },
    { label: 'Launch', icon: 'play' },
    { label: 'Refresh All', icon: 'refresh' },
    { label: 'Refresh', icon: 'repeat' },
    { label: 'Log out', icon: 'log-out' },
    { label: 'Apply', icon: 'ok' },
    { label: 'Attach', icon: 'tag' },
    { label: 'Detach', icon: 'tags' },
    { label: 'Accept', icon: 'ok' },
    { label: 'Approve', icon: 'ok' },
    { label: 'Review', icon: 'comment' },
    { label: 'Devel', icon: 'devel' },

    { label: 'Manage', icon: 'cog' },
    { label: 'Configure', icon: 'cog' },
    { label: 'Settings', icon: 'cog' },
    { label: 'Download', icon: 'download' },
    { label: 'Export', icon: 'export' },
    { label: 'Filter', icon: 'filter' },
    { label: 'Import', icon: 'download-alt' },
    { label: 'Save', icon: 'ok' },
    { label: 'Update', icon: 'ok' },
    { label: 'Edit', icon: 'wrench' },
    { label: 'Uninstall', icon: 'trash' },
    { label: 'Install', icon: 'plus' },
    { label: 'Write', icon: 'plus' },
    { label: 'Cancel', icon: 'remove' },
    { label: 'Remove', icon: 'trash' },
    { label: 'Reset', icon: 'remove-circle' },
    { label: 'Search', icon: 'search' },
    { label: 'Upload', icon: 'upload' },
    { label: 'Preview', icon: 'eye-open' },
    { label: 'Log in', icon: 'log-in' },
  ],
};

// The URL of the configuration information about the location map.
export const LOCATION_MAP_CONFIG_URI = drupalSettings.location_map_config_uri || '';

// The list of visibility type for OpensStack image.
export const IMAGE_VISIBILITY_LIST = [
  { labelName: 'Private', key: 'private', name: '0' },
  { labelName: 'Public', key: 'public', name: '1' },
  { labelName: 'Shared', key: 'shared', name: '2' },
];
