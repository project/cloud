import K8S_CLOUD_STORE_LIST from 'constant/templates/cloud_store/k8s/k8s_resource';

const CLOUD_STORE_LIST = [
  ...K8S_CLOUD_STORE_LIST,
];

export default CLOUD_STORE_LIST;
