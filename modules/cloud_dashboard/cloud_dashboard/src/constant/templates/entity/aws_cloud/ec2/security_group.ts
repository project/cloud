import { ROUTE_URL } from 'constant/others/other';
import EntityColumn from 'model/EntityColumn';
import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying the Launch template in K8s.
const COLUMN_RULES: EntityColumn[] = [
  { labelName: 'IP protocol', 'name': 'ip_protocol', type: 'conditions', value: ['-1', 'All Traffic']},
  { labelName: 'From port', 'name': 'from_port', type: 'default' },
  { labelName: 'To port', 'name': 'to_port', type: 'default' },
  { labelName: 'CIDR IP', 'name': 'cidr_ip', type: 'default' },
  { labelName: 'CIDR IP V6 ', 'name': 'cidr_ip_v6', type: 'default' },
  { labelName: 'Prefix list ID', 'name': 'prefix_list_id', type: 'default' },
  { labelName: 'Group ID', 'name': 'group_id', type: 'default' },
  { labelName: 'Group name', 'name': 'group_name', type: 'default' },
  { labelName: 'Peering status', 'name': 'peering_status', type: 'default' },
  { labelName: 'Group user ID', 'name': 'user_id', type: 'default' },
  {
    labelName: 'VPC ID', name: 'vpc_id', type: 'join', info: {
      entityTypeId: 'aws_cloud_vpc',
      keyColumn: 'vpc_id',
      valueColumn: 'name',
    }
  },
  { labelName: 'Peering connection ID', 'name': 'peering_connection_id', type: 'default' },
  { labelName: 'Description', 'name': 'description', type: 'default' },
];

const INBOUND_COLUMN_RULES: EntityColumn[] = COLUMN_RULES.concat([
  { labelName: 'Operation', name: 'operation', type: 'link',
    label: 'Revoke',
    url: `${ROUTE_URL}/{cloudServiceProvider}/{cloudContext}/{entityName}/{entityId}/revoke?type=ip_permission&position={index}`,
  }
]);

const OUTBOUND_COLUMN_RULES: EntityColumn[] = COLUMN_RULES.concat([
  { labelName: 'Operation', name: 'operation', type: 'link',
    label: 'Revoke',
    url: `${ROUTE_URL}/{cloudServiceProvider}/{cloudContext}/{entityName}/{entityId}/revoke?type=outbound_permission&position={index}`,
  }
]);

const AWS_CLOUD_SECURITY_GROUP_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'security_group',
  entityRecords: [
    {
      panelName: 'Security group',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Security group name', name: 'group_name', type: 'default' },
        { labelName: 'ID', name: 'group_id', type: 'default' },
        { labelName: 'Description', name: 'description', type: 'default' },
        {
          labelName: 'VPC ID', name: 'vpc_id', type: 'join', info: {
            entityTypeId: 'aws_cloud_vpc',
            keyColumn: 'vpc_id',
            valueColumn: 'name',
          }
        },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Rules',
      tableRecordList: ['outbound_permission'],
      keyValueRecords: [
        { labelName: 'Inbound rules', name: 'ip_permission', type: 'custom-table', column: INBOUND_COLUMN_RULES },
        { labelName: 'Outbound rules', name: 'outbound_permission', type: 'custom-table', column: OUTBOUND_COLUMN_RULES },
      ]
    },
  ]
};

export default AWS_CLOUD_SECURITY_GROUP_TEMPLATE;
