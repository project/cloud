import OPENSTACK_HEAT_EVENT_TEMPLATE from 'constant/templates/entity/openstack/heat_event';
import OPENSTACK_HEAT_RESOURCE_TEMPLATE from 'constant/templates/entity/openstack/heat_resource';

const ENTITY_INFO_SUB_LIST = [
  OPENSTACK_HEAT_RESOURCE_TEMPLATE,
  OPENSTACK_HEAT_EVENT_TEMPLATE,
];

export default ENTITY_INFO_SUB_LIST;
