import OPENSTACK_HEAT_EVENT_TEMPLATE from 'constant/templates/entity/openstack/heat_event';
import OPENSTACK_HEAT_RESOURCE_TEMPLATE from 'constant/templates/entity/openstack/heat_resource';
import OPENSTACK_PORT_TEMPLATE from 'constant/templates/entity/openstack/port';
import EntityInfoTabTemplate from 'model/EntityInfoTabTemplate';

const ENTITY_INFO_TAB_LIST: EntityInfoTabTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'instance',
    tabs: [
      { type: 'default', name: 'edit', labelName: 'Edit' },
      { type: 'default', name: 'console_output', labelName: 'Log' },
      { type: 'default', name: 'console', labelName: 'Console' },
      { type: 'default', name: 'action_log', labelName: 'Action log' },
      { type: 'default', name: 'terminate', labelName: 'Delete' },
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'image',
    tabs: [
      { type: 'default', name: 'edit', labelName: 'Edit' },
      { type: 'default', name: 'delete', labelName: 'Delete' },
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'router',
    tabs: [
      {
        type: 'table', name: 'interface', labelName: 'Interfaces', columns: [
          { name: 'name', labelName: 'Name', type: 'default' },
          { name: 'fixed_ips', labelName: 'Fixed IPs', type: 'comma', index: 0 },
          { name: 'status', labelName: 'Status', type: 'default' },
          { name: 'device_owner', labelName: 'Type', type: 'replace', list: {
            'network:router_interface': 'Internal Interface',
            'network:router_gateway': 'External Gateway'
          } },
          {
            name: 'admin_state_up', labelName: 'Admin state', type: 'boolean', value: [
              'up', 'down'
            ]
          },
        ],
        path: '/openstack/port',
        entityTypeId: 'openstack_port',
        filter: [
          { key: 'device_id', value: '{router_id}' },
        ],
        detail: OPENSTACK_PORT_TEMPLATE,
        operationsLinks: [
          { label: 'Remove interface', path: '/openstack/{cloud_context}/router/{entityId}/remove_interface/{subEntityId}' },
        ],
        actionButtons: [
          { label: 'Add interface', action: 'add_interface' }
        ]
      },
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'stack',
    tabs: [
      {
        type: 'table', name: 'resource', labelName: 'Resources', columns: [
          { name: 'name', labelName: 'Name', type: 'default' },
          { name: 'resource_id', labelName: 'Resource ID', type: 'default' },
          { name: 'resource_type', labelName: 'Resource Type', type: 'default' },
          { name: 'changed', labelName: 'Date Updated', type: 'datetime' },
          { name: 'resource_status', labelName: 'Status', type: 'default' },
          { name: 'resource_status_reason', labelName: 'Status Reason', type: 'default' },
        ],
        path: '/openstack/stack/{entityId}/resource',
        entityTypeId: 'openstack_stack_resource',
        filter: [
          { key: 'stack_entity_id', value: '{entityId}' },
        ],
        detail: OPENSTACK_HEAT_RESOURCE_TEMPLATE,
        actionButtons: []
      },
      {
        type: 'table', name: 'event', labelName: 'Events', columns: [
          { name: 'name', labelName: 'Name', type: 'default' },
          { name: 'resource_name', labelName: 'Stack Resource', type: 'default' },
          { name: 'resource_id', labelName: 'Resource', type: 'default' },
          { name: 'changed', labelName: 'Time Since Event', type: 'datetime' },
          { name: 'resource_status', labelName: 'Status', type: 'default' },
          { name: 'resource_status_reason', labelName: 'Status Reason', type: 'default' },
        ],
        path: '/openstack/stack/{entityId}/event',
        entityTypeId: 'openstack_stack_event',
        filter: [
          { key: 'stack_entity_id', value: '{entityId}' },
        ],
        detail: OPENSTACK_HEAT_EVENT_TEMPLATE,
        actionButtons: []
      },
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'server_group',
    tabs: [
      { type: 'default', name: 'delete', labelName: 'Delete' },
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'cloud_launch_template',
    tabs: [
      { type: 'default', name: 'edit', labelName: 'Edit' },
      { type: 'default', name: 'delete', labelName: 'Delete' },
      { type: 'default', name: 'launch', labelName: 'Launch' },
      { type: 'default', name: 'copy', labelName: 'Copy' },
      { type: 'default', name: 'revisions', labelName: 'Revisions' },
    ]
  },
]

export default ENTITY_INFO_TAB_LIST;
