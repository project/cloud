import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_FLAVOR_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'flavor',
  entityRecords: [
    {
      panelName: 'Flavor',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Flavor ID', name: 'flavor_id', type: 'default' },
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'vCPUs', name: 'vcpus', type: 'default' },
        { labelName: 'RAM', name: 'ram', type: 'storage_mb' },
        { labelName: 'Root Disk', name: 'disk', type: 'storage_gb' },
        { labelName: 'Ephemeral Disk', name: 'ephemeral', type: 'storage_gb' },
        { labelName: 'Swap Disk', name: 'swap', type: 'storage_mb' },
        { labelName: 'RX/TX Factor', name: 'rxtx_factor', type: 'float_number' },
        { labelName: 'Public', name: 'is_public', type: 'boolean', value: ['Yes', 'No'] },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default OPENSTACK_FLAVOR_TEMPLATE;
