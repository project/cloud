import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_FLOATING_IP_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'floating_ip',
  entityRecords: [
    {
      panelName: 'IP address',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Floating IP', name: 'public_ip', type: 'default' },
        { labelName: 'Private IP address', name: 'private_ip_address', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Assign',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Instance ID', name: 'instance_id', type: 'join', info: {
          entityTypeId: 'openstack_instance', keyColumn: 'instance_id', valueColumn: 'name' } },
        { labelName: 'Network ID', name: 'network_id', type: 'join', info: {
          entityTypeId: 'openstack_network', keyColumn: 'network_id', valueColumn: 'name' } },
        { labelName: 'Allocation ID', name: 'allocation_id', type: 'default' },
        { labelName: 'Association ID', name: 'association_id', type: 'default' },
        { labelName: 'Network owner', name: 'network_owner', type: 'default' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
   ]
};

export default OPENSTACK_FLOATING_IP_TEMPLATE;
