import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_HEAT_RESOURCE_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'stack_resource',
  entityRecords: [
    {
      panelName: 'Stack Resource',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Project ID', name: 'project_id', type: 'default' },
        { labelName: 'Stack ID', name: 'stack_id', type: 'default' },
        { labelName: 'Resource ID', name: 'resource_id', type: 'default' },
        { labelName: 'Status', name: 'resource_status', type: 'default' },
        { labelName: 'Status Reason', name: 'resource_status_reason', type: 'default' },
        { labelName: 'Resource Type', name: 'resource_type', type: 'default', id: 'field--name-resource-type', class: 'field--name-resource-type' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    }
  ]
};

export default OPENSTACK_HEAT_RESOURCE_TEMPLATE;
