import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_IMAGE_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'image',
  entityRecords: [
    {
      panelName: 'Image',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Image ID', name: 'image_id', type: 'default' },
        { labelName: 'Owner', name: 'account_id', type: 'default' },
        { labelName: 'Status', name: 'status', type: 'default' },
        { labelName: 'Image type', name: 'image_type', type: 'default' },
        { labelName: 'Root device type', name: 'root_device_type', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Security',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Visibility', name: 'image_visibility', type: 'default', id: 'field--name-image-visibility', class: 'field--name-image-visibility' },
        { labelName: 'Shared projects', name: 'shared_projects', type: 'custom-table', column: [
          { labelName: 'Cloud context', name: 'cloud_context', type: 'default' },
          { labelName: 'Project ID', name: 'project_id', type: 'default' },
          { labelName: 'Project ID', name: 'project_id', type: 'join', info: {
            entityTypeId: 'openstack_project',
            keyColumn: 'project_id',
            valueColumn: 'name',
          }},
          { labelName: 'Status', name: 'status', type: 'default' },
        ] },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default OPENSTACK_IMAGE_TEMPLATE;
