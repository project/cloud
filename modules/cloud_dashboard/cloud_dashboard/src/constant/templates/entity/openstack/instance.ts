import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_INSTANCE_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'instance',
  entityRecords: [
    {
      panelName: 'Instance',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Instance ID', name: 'instance_id', type: 'default' },
        { labelName: 'Instance State', name: 'instance_state', type: 'default', id: 'field--name-instance-state', class: 'field--name-instance-state' },
        { labelName: 'Power state', name: 'power_state', type: 'select-local', id: 'field--name-power-state', class: 'field--name-power-state', value: [
          { labelName: 'nostate', name: '0' },
          { labelName: 'running', name: '1' },
          { labelName: 'paused', name: '3' },
          { labelName: 'shutdown', name: '4' },
          { labelName: 'crashed', name: '6' },
          { labelName: 'suspended', name: '7' },
        ] },
        { labelName: 'Flavor', name: 'instance_type', type: 'default', id: 'field--name-instance-type', class: 'field--name-instance-type' },
        { labelName: 'Cost', name: 'cost', type: 'cost' },
        { labelName: 'OpenStack image name', name: 'image_id', type: 'join', id: 'field--name-image-id', class: 'field--name-image-id', info: {
            entityTypeId: 'openstack_image',
            keyColumn: 'image_id',
            valueColumn: 'name',
          }
        },
        { labelName: 'Virtualization', name: 'virtualization', type: 'default' },
        { labelName: 'Reservation', name: 'reservation', type: 'default' },
        { labelName: 'Launch Time', name: 'launch_time', type: 'datetime' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Network',
      tableRecordList: [],
      keyValueRecords: [
        {
          labelName: 'Public IP', name: 'public_ip', type: 'join', info: {
            entityTypeId: 'openstack_floating_ip',
            keyColumn: 'public_ip',
            valueColumn: 'name',
          }
        },
        { labelName: 'Private IPs', name: 'private_ips', type: 'default' },
        { labelName: 'Public DNS', name: 'public_dns', type: 'default' },
        { labelName: 'Security groups', name: 'security_groups', type: 'default', id: 'field--name-security-groups', class: 'field--name-security-groups' },
        { labelName: 'Key pair name', name: 'key_pair_name', type: 'join', id: 'field--name-key-pair-name', class: 'field--name-key-pair-name', info: {
            entityTypeId: 'openstack_key_pair',
            keyColumn: 'key_pair_name',
            valueColumn: 'name',
          }
        },
        {
          labelName: 'VPC ID', name: 'vpc_id', type: 'join', info: {
            entityTypeId: 'openstack_security_group',
            keyColumn: 'vpc_id',
            valueColumn: 'name',
          }
        },
        {
          labelName: 'Subnet ID', name: 'subnet_id', type: 'join', info: {
            entityTypeId: 'aws_cloud_subnet',
            keyColumn: 'subnet_id',
            valueColumn: 'name',
          }
        },
        { labelName: 'Availability Zone', name: 'availability_zone', type: 'default', id: 'field--name-field-os-availability-zone', class: 'field--name-field-os-availability-zone' },
        { labelName: 'Network interfaces', name: 'network_interfaces', type: 'array' },
        { labelName: 'Port ID', name: 'port_id', type: 'array' },
      ]
    },
    {
      panelName: 'Storage',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Root device type', name: 'root_device_type', type: 'default' },
        { labelName: 'Root device', name: 'root_device', type: 'default' },
        { labelName: 'EBS optimized', name: 'ebs_optimized', type: 'boolean', value: ['On', 'Off'] },
        { labelName: 'Volume', name: 'block_devices', type: 'default' },
      ]
    },
    {
      panelName: 'Tags',
      tableRecordList: ['tags'],
      keyValueRecords: [
        { labelName: 'Tags', name: 'tags', type: 'default' },
      ]
    },
    {
      panelName: 'Option',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Termination protection', name: 'termination_protection', type: 'boolean', value: ['On', 'Off'] },
        { labelName: 'AMI Launch Index', name: 'ami_launch_index', type: 'default' },
        { labelName: 'Tenancy', name: 'tenancy', type: 'default' },
      ]
    },
  ]
};

export default OPENSTACK_INSTANCE_TEMPLATE;
