import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_KEY_PAIR_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'key_pair',
  entityRecords: [
    {
      panelName: 'Key pair',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Key pair name', name: 'key_pair_name', type: 'default', id: 'field--name-key-pair-name', class: 'field--name-key-pair-name' },
        { labelName: 'Fingerprint', name: 'key_fingerprint', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ],
};

export default OPENSTACK_KEY_PAIR_TEMPLATE;
