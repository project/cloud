import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_SERVER_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'cloud_launch_template',
  entityRecords: [
    {
      panelName: 'Instance',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Description', name: 'field_description', type: 'default', id: 'field--name-field-description', class: 'field--name-field-description' },
        { labelName: 'Flavor', name: 'field_flavor', type: 'select',
          id: 'field--name-field-flavor', class: 'field--name-field-flavor',
          url: '/cloud_dashboard/openstack/{cloud_context}/cloud_launch_template/{entity_id}/flavors',
          readOnly: true
        },
        { labelName: 'Min count', name: 'field_min_count', type: 'number' },
        { labelName: 'Max count', name: 'field_max_count', type: 'number' },
        { labelName: 'Test only', name: 'field_test_only', type: 'boolean', value: ['On', 'Off'] },
      ],
    },
    {
      panelName: 'Image',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Image ID', name: 'field_openstack_image_id', type: 'join', info: {
            entityTypeId: 'openstack_image',
            keyColumn: 'image_id',
            valueColumn: 'name',
          }, id: 'field--name-field-openstack-image-id', class: 'field--name-field-openstack-image-id'
        },
      ],
    },
    {
      panelName: 'Network',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Availability Zone', name: 'field_os_availability_zone', type: 'default', id: 'field--name-field-os-availability-zone', class: 'field--name-field-os-availability-zone' },
        { labelName: 'Security groups', name: 'field_openstack_security_group', type: 'relationship', info: {
          entityTypeId: 'openstack_security_group',
          keyColumn1: 'drupal_internal__target_id',
          keyColumn2: 'drupal_internal__id',
          valueColumn: 'name',
        }, id: 'field--name-field-openstack-security-group', class: 'field--name-field-openstack-security-group' },
        { labelName: 'SSH key', name: 'field_openstack_ssh_key', type: 'relationship', info: {
          entityTypeId: 'openstack_key_pair',
          keyColumn1: 'drupal_internal__target_id',
          keyColumn2: 'drupal_internal__id',
          valueColumn: 'key_pair_name',
        }, id: 'field--name-field-openstack-ssh-key', class: 'field--name-field-openstack-ssh-key' },
        { labelName: 'Network', name: 'field_openstack_network', type: 'relationship', info: {
          entityTypeId: 'openstack_network',
          keyColumn1: 'drupal_internal__target_id',
          keyColumn2: 'drupal_internal__id',
          valueColumn: 'name',
        }, id: 'field--name-field-openstack-network', class: 'field--name-field-openstack-network' },
      ],
    },
    {
      panelName: 'Server group',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Server group', name: 'field_openstack_server_group', type: 'relationship', info: {
          entityTypeId: 'openstack_server_group',
          keyColumn1: 'drupal_internal__target_id',
          keyColumn2: 'drupal_internal__id',
          valueColumn: 'name',
        } },
      ],
    },
    {
      panelName: 'Tags',
      tableRecordList: ['field_tags'],
      keyValueRecords: [
        { labelName: 'Tags', name: 'field_tags', type: 'default' },
      ],
    },
    {
      panelName: 'Options',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Instance shutdown behavior', name: 'field_instance_shutdown_behavior', type: 'default' },
        { labelName: 'Termination protection', name: 'field_termination_protection', type: 'boolean', value: ['On', 'Off'] },
        { labelName: 'Monitoring', name: 'field_monitoring', type: 'boolean', value: ['On', 'Off'] },
        { labelName: 'Customization script', name: 'field_user_data', type: 'default', id: 'field--name-field-user-data', class: 'field--name-field-user-data' },
      ],
    },
  ],
};

export default OPENSTACK_SERVER_TEMPLATE;
