import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_NETWORK_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'network',
  entityRecords: [
    {
      panelName: 'Network',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Network ID', name: 'network_id', type: 'default' },
        { labelName: 'Project ID', name: 'project_id', type: 'default' },
        { labelName: 'Status', name: 'status', type: 'default' },
        { labelName: 'Admin state', name: 'admin_state_up', type: 'boolean', value: ['up', 'down'] },
        { labelName: 'Shared', name: 'shared', type: 'boolean', value: ['yes', 'no'] },
        { labelName: 'External', name: 'external', type: 'boolean', value: ['yes', 'no'] },
        { labelName: 'MTU', name: 'mtu', type: 'number' },
        { labelName: 'Network type', name: 'network_type', type: 'default' },
        { labelName: 'Segmentation ID', name: 'segmentation_id', type: 'number' },
        { labelName: 'Availability Zones', name: 'availability_zones', type: 'array' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    }
  ]
};

export default OPENSTACK_NETWORK_TEMPLATE;
