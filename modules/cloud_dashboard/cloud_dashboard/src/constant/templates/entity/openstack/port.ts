import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_PORT_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'port',
  entityRecords: [
    {
      panelName: 'Port',
      tableRecordList: ['allowed_address_pairs', 'binding_vif_details'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Port ID', name: 'port_id', type: 'default' },
        { labelName: 'Project ID', name: 'project_id', type: 'default' },
        { labelName: 'Network', name: 'network_id', id: 'field--name-network-id', class: 'field--name-network-id', type: 'join', info: {
          entityTypeId: 'openstack_network', keyColumn: 'network_id', valueColumn: 'name' } },
        { labelName: 'MAC address', name: 'mac_address', type: 'default' },
        { labelName: 'Status', name: 'status', type: 'default' },
        { labelName: 'Admin state', name: 'admin_stateup', type: 'boolean', value: ['up', 'down']  },
        { labelName: 'Port security', name: 'port_security_enabled', type: 'boolean', value: ['enabled', 'disabled'] },
        { labelName: 'DNS name', name: 'dns_name', type: 'default' },
        { labelName: 'Dns Assignment', name: 'dns_assignment', type: 'default' },
        { labelName: 'Fixed IPs', name: 'fixed_ips', type: 'array-table', column: [
          { labelName: 'IP Address', name: 'ip_address', type: 'default' },
          { labelName: 'Subnet ID', name: 'subnet_id', type: 'join', info: {
            entityTypeId: 'openstack_subnet', keyColumn: 'subnet_id', valueColumn: 'name' }  },
        ] },
        { labelName: 'Allowed Address Pairs', name: 'allowed_address_pairs', type: 'default' },
        { labelName: 'Device owner', name: 'device_owner', type: 'default' },
        { labelName: 'Device ID', name: 'device_id', type: 'default' },
        { labelName: 'Security groups', name: 'security_groups', id: 'field--name-security-groups', class: 'field--name-security-groups', type: 'join', info: {
          entityTypeId: 'openstack_security_group', keyColumn: 'group_id', valueColumn: 'name' } },
        { labelName: 'VNIC type', name: 'binding_vnic_type', type: 'default' },
        { labelName: 'Host', name: 'binding_host_id', type: 'default' },
        { labelName: 'Profile', name: 'binding_profile', type: 'default' },
        { labelName: 'VIF type', name: 'binding_vif_type', type: 'default' },
        { labelName: 'VIF details', name: 'binding_vif_details', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default OPENSTACK_PORT_TEMPLATE;
