import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_PROJECT_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'project',
  entityRecords: [
    {
      panelName: 'Project',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Project ID', name: 'project_id', type: 'default' },
        { labelName: 'Description', name: 'description', type: 'default', id: 'field--name-field-description', class: 'field--name-field-description' },
        { labelName: 'Is domain', name: 'is_domain', type: 'boolean', value: ['yes', 'no'] },
        { labelName: 'Domain ID', name: 'domain_id', type: 'default' },
        { labelName: 'Enabled', name: 'enabled', type: 'boolean', value: ['yes', 'no'] },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Users',
      tableRecordList: [],
      keyValueRecords: [
        {
          labelName: 'User Roles',
          name: 'user_roles',
          type: 'custom-table',
          column: [
            { labelName: 'User', name: 'user', type: 'join', hiddenId: true, info: {
              entityTypeId: 'openstack_user', keyColumn: 'drupal_internal__id', valueColumn: 'name' } },
            { labelName: 'Roles', name: 'roles', type: 'multi-join', hiddenId: true, info: {
              entityTypeId: 'openstack_role', keyColumn: 'drupal_internal__id', valueColumn: 'name' } },
          ],
         id: 'field--name-user-roles', class: 'field--name-user-roles'
        },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default OPENSTACK_PROJECT_TEMPLATE;
