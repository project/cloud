import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_QUOTA_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'quota',
  entityRecords: [
    {
      panelName: 'Quota',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Compute',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Instances usage', name: 'instances_usage', type: 'fraction', denominator_name: 'instances', id: 'field--name-instances-usage', class: 'field--name-instances-usage' },
        { labelName: 'vCPUs usage', name: 'cores_usage', type: 'fraction', denominator_name: 'cores', },
        { labelName: 'RAM(MiB) Usage', name: 'ram_usage', type: 'fraction', denominator_name: 'ram', },
        { labelName: 'Metadata Items Usage', name: 'metadata_items_usage', type: 'fraction', denominator_name: 'metadata_items', },
        { labelName: 'Key Pairs Usage', name: 'key_pairs_usage', type: 'fraction', denominator_name: 'key_pairs', },
        { labelName: 'Server Groups Usage', name: 'server_groups_usage', type: 'fraction', denominator_name: 'server_groups', },
        { labelName: 'Server Group Members Usage', name: 'server_group_members_usage', type: 'fraction', denominator_name: 'server_group_members', },
        { labelName: 'Injected Files Usage', name: 'injected_files_usage', type: 'fraction', denominator_name: 'injected_files', },
        { labelName: 'Injected File Content(Bytes) Usage', name: 'injected_file_content_bytes_usage', type: 'fraction', denominator_name: 'injected_file_content_bytes', },
        { labelName: 'Length of Injected File Path Usage', name: 'injected_file_path_bytes_usage', type: 'fraction', denominator_name: 'injected_file_path_bytes', },
      ]
    },
    {
      panelName: 'Volume',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Volumes usage', name: 'volumes_usage', type: 'fraction', denominator_name: 'volumes', },
        { labelName: 'Volume snapshots Usage', name: 'snapshots_usage', type: 'fraction', denominator_name: 'snapshots', },
        { labelName: 'Total used size of volumes and snapshots (GiB)', name: 'gigabytes_usage', type: 'fraction', denominator_name: 'gigabytes', },
      ]
    },
    {
      panelName: 'Network',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Networks Usage', name: 'network_usage', type: 'fraction', denominator_name: 'network', },
        { labelName: 'Subnets Usage', name: 'subnet_usage', type: 'fraction', denominator_name: 'subnet', },
        { labelName: 'Ports Usage', name: 'port_usage', type: 'fraction', denominator_name: 'port', },
        { labelName: 'Routers Usage', name: 'router_usage', type: 'fraction', denominator_name: 'router', },
        { labelName: 'Floating IPs Usage', name: 'floatingip_usage', type: 'fraction', denominator_name: 'floatingip', },
        { labelName: 'Security groups usage', name: 'security_group_usage', type: 'fraction', denominator_name: 'security_group', },
        { labelName: 'Security group rules usage', name: 'security_group_rule_usage', type: 'fraction', denominator_name: 'security_group_rule', },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    }
  ]
};

export default OPENSTACK_QUOTA_TEMPLATE;
