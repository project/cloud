import EntityColumn from 'model/EntityColumn';
import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const EXTERNAL_FIXED_IPS: EntityColumn[] = [
  { labelName: 'IP address', name: 'ip_address', type: 'default' },
  { labelName: 'Subnet ID', name: 'subnet_id', type: 'default' },
];

const OPENSTACK_ROUTER_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'router',
  entityRecords: [
    {
      panelName: 'Router',
      tableRecordList: ['routes'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Router ID', name: 'router_id', type: 'default' },
        { labelName: 'Project ID', name: 'project_id', type: 'default' },
        { labelName: 'Status', name: 'status', type: 'default' },
        { labelName: 'Admin state', name: 'admin_state_up', type: 'boolean', value: ['up', 'down'] },
        { labelName: 'Static Routes', name: 'routes', type: 'custom-table', column: [
            { labelName: 'Destination CIDR', name: 'item_key', type: 'default' },
            { labelName: 'Next Hop', name: 'item_value', type: 'default' },
          ] },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'External Gateway',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Network ID', name: 'external_gateway_network_id', type: 'default' },
        { labelName: 'SNAT', name: 'external_gateway_enable_snat', type: 'boolean', value: ['enabled', 'disabled'] },
        { labelName: 'External fixed IPs', name: 'external_gateway_external_fixed_ips', type: 'array-table', column: EXTERNAL_FIXED_IPS },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    }
  ]
};

export default OPENSTACK_ROUTER_TEMPLATE;
