import { ROUTE_URL } from 'constant/others/other';
import EntityColumn from 'model/EntityColumn';
import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const COLUMN_RULES: EntityColumn[] = [
  { labelName: 'IP protocol', name: 'ip_protocol', type: 'conditions', value: ['-1', 'All Traffic'] },
  { labelName: 'From port', name: 'from_port', type: 'default' },
  { labelName: 'To port', name: 'to_port', type: 'default' },
  { labelName: 'CIDR IP', name: 'cidr_ip', type: 'default' },
  { labelName: 'CIDR IP V6', name: 'cidr_ip_v6', type: 'default' },
  { labelName: 'Prefix list ID', name: 'prefix_list_id', type: 'default' },
  { labelName: 'Group ID', name: 'group_id', type: 'default' },
  { labelName: 'Group name', name: 'group_name', type: 'default' },
  { labelName: 'Peering status', name: 'peering_status', type: 'default' },
  { labelName: 'Group user ID', name: 'user_id', type: 'default' },
  { labelName: 'VPC ID', name: 'vpc_id', type: 'default' },
  { labelName: 'Peering connection ID', name: 'peering_connection_id', type: 'default' },
  { labelName: 'Description', name: 'description', type: 'default' },
];

const INBOUND_COLUMN_RULES: EntityColumn[] = COLUMN_RULES.concat([
  { labelName: 'Operation', name: 'operation', type: 'link',
    label: 'Revoke',
    url: `${ROUTE_URL}/{cloudServiceProvider}/{cloudContext}/{entityName}/{entityId}/revoke?type=ip_permission&position={index}`,
  }
]);

const OUTBOUND_COLUMN_RULES: EntityColumn[] = COLUMN_RULES.concat([
  { labelName: 'Operation', name: 'operation', type: 'link',
    label: 'Revoke',
    url: `${ROUTE_URL}/{cloudServiceProvider}/{cloudContext}/{entityName}/{entityId}/revoke?type=outbound_permission&position={index}`,
  }
]);

const OPENSTACK_SECURITY_GROUP_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'security_group',
  entityRecords: [
    {
      panelName: 'Security group',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Security group name', name: 'group_name', type: 'default', id: 'field--name-group-name', class: 'field--name-group-name' },
        { labelName: 'ID', name: 'group_id', type: 'default', class: 'field--name-group-id' },
        { labelName: 'Description', name: 'description', type: 'default', id: 'field--name-description', class: 'field--name-description' },
        { labelName: 'Created', name: 'created', type: 'datetime', class: 'field--name-created' },
      ]
    },
    {
      panelName: 'Rules',
      tableRecordList: ['ip_permission', 'outbound_permission'],
      keyValueRecords: [
        { labelName: 'Inbound rules', name: 'ip_permission', type: 'custom-table', column: INBOUND_COLUMN_RULES, id: 'field--name-ip-permission', class: 'field--name-ip-permission' },
        { labelName: 'Outbound rules', name: 'outbound_permission', type: 'custom-table', column: OUTBOUND_COLUMN_RULES, id: 'field--name-outbound-permission', class: 'field--name-outbound-permission' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default', class: 'field--name-cloud-context' },
      ]
    },
  ]
};

export default OPENSTACK_SECURITY_GROUP_TEMPLATE;
