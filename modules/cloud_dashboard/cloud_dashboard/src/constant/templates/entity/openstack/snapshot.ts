import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_SNAPSHOT_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'snapshot',
  entityRecords: [
    {
      panelName: 'Snapshot',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Description', name: 'description', type: 'default', id: 'field--name-field-description', class: 'field--name-field-description' },
        { labelName: 'Snapshot ID', name: 'snapshot_id', type: 'default' },
        { labelName: 'Volume ID', name: 'volume_id', type: 'default' },
        { labelName: 'Size (GB)', name: 'size', type: 'number', id: 'field--name-size', class: 'field--name-size' },
        { labelName: 'Status', name: 'status', type: 'default', id: 'field--name-state', class: 'field--name-state' },
        { labelName: 'Progress', name: 'progress', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default OPENSTACK_SNAPSHOT_TEMPLATE;
