import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_STACK_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'stack',
  entityRecords: [
    {
      panelName: 'Stack',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Stack ID', name: 'stack_id', type: 'default' },
        { labelName: 'Project ID', name: 'project_id', type: 'default' },
        { labelName: 'Stack Status', name: 'stack_status', type: 'default' },
        { labelName: 'Stack Status Reason', name: 'stack_status_reason', type: 'default' },
        { labelName: 'Timeout(minutes)', name: 'timeout_mins', type: 'number' },
        { labelName: 'Rollback', name: 'rollback', type: 'boolean', value: [
          'Enabled', 'Disabled'
        ] },
        { labelName: 'Created', name: 'created', type: 'datetime' },
        { labelName: 'Refreshed', name: 'refreshed', type: 'datetime' },
      ]
    },
    {
      panelName: 'Outputs',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Outputs', name: 'outputs', type: 'custom-table', column: [
          { labelName: 'Name', name: 'item_key', type: 'default' },
          { labelName: 'Value', name: 'item_value', type: 'default' },
        ] },
      ]
    },
    {
      panelName: 'Parameters',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Stack Parameters', name: 'parameters', type: 'custom-table', column: [
          { labelName: 'Name', name: 'item_key', type: 'default' },
          { labelName: 'Value', name: 'item_value', type: 'default' },
        ] },
      ]
    },
    {
      panelName: 'Template',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Template', name: 'template', type: 'default' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    }
  ]
};

export default OPENSTACK_STACK_TEMPLATE;
