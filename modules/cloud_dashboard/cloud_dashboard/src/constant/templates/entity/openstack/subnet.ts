import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_SUBNET_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'subnet',
  entityRecords: [
    {
      panelName: 'Subnet',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Subnet ID', name: 'subnet_id', type: 'default' },
        { labelName: 'Project ID', name: 'project_id', type: 'default' },
        { labelName: 'Network', name: 'network_id', type: 'join', id: 'field--name-network-id', class: 'field--name-network-id', info: {
          entityTypeId: 'openstack_network', keyColumn: 'network_id', valueColumn: 'name' } },
        { labelName: 'IP version', name: 'ip_version', type: 'default', id: 'field--name-ip-version', class: 'field--name-ip-version' },
        { labelName: 'CIDR', name: 'cidr', type: 'default', id: 'field--name-cidr', class: 'field--name-cidr' },
        { labelName: 'Allocation pools', name: 'allocation_pools', type: 'default' },
        { labelName: 'Gateway IP', name: 'gateway_ip', type: 'default', id: 'field--name-gateway-ip', class: 'field--name-gateway-ip' },
        { labelName: 'Enable DHCP', name: 'enable_dhcp', type: 'boolean', value: ['yes', 'no'] },
        { labelName: 'Host Routes', name: 'host_routes', type: 'array' },
        { labelName: 'DNS name servers', name: 'dns_name_servers', type: 'array' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default OPENSTACK_SUBNET_TEMPLATE;
