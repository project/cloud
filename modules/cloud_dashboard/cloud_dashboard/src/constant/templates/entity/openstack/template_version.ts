import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_TEMPLATE_VERSION_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'template_version',
  entityRecords: [
    {
      panelName: 'Template version',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'Type', name: 'openstack_template_version_type', type: 'default' },
        { labelName: 'Functions', name: 'functions', type: 'custom-table', id: 'field--name-functions', class: 'field--name-functions', column: [
          { labelName: 'Function', name: 'item_key', type: 'default' },
          { labelName: 'Description', name: 'item_value', type: 'default' },
        ] },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default OPENSTACK_TEMPLATE_VERSION_TEMPLATE;
