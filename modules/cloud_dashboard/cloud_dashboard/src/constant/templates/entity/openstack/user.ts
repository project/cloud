import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_USER_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'user',
  entityRecords: [
    {
      panelName: 'User',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default', id: 'field--name-name', class: 'field--name-name' },
        { labelName: 'User ID', name: 'user_id', type: 'default' },
        { labelName: 'Description', name: 'description', type: 'default', id: 'field--name-field-description', class: 'field--name-field-description' },
        { labelName: 'Email', name: 'email', type: 'default' },
        { labelName: 'Default Project ID', name: 'default_project_id', type: 'join', id: 'field--name-default-project-id', class: 'field--name-default-project-id', info: {
          entityTypeId: 'openstack_project',
          keyColumn: 'project_id',
          valueColumn: 'name',
        } },
        { labelName: 'Domain ID', name: 'domain_id', type: 'default' },
        { labelName: 'Enabled', name: 'enabled', type: 'boolean', value: ['yes', 'no'] },
        { labelName: 'Password Expires At', name: 'password_expires_at', type: 'datetime' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default OPENSTACK_USER_TEMPLATE;
