import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in OpenStack.
const OPENSTACK_VOLUME_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'openstack',
  entityName: 'volume',
  entityRecords: [
    {
      panelName: 'Volume',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Volume ID', name: 'volume_id', type: 'default' },
        { labelName: 'Instance ID', name: 'attachment_information', type: 'join', info: {
          entityTypeId: 'openstack_instance', keyColumn: 'instance_id', valueColumn: 'name' } },
        { labelName: 'Snapshot ID', name: 'snapshot_id', type: 'default' },
        { labelName: 'Snapshot name', name: 'snapshot_name', type: 'default' },
        { labelName: 'Size (GB)', name: 'size', type: 'number', id: 'field--name-size', class: 'field--name-size' },
        { labelName: 'Volume type', name: 'volume_type', type: 'default' },
        { labelName: 'Availability Zone', name: 'availability_zone', type: 'default', id: 'field--name-field-os-availability-zone', class: 'field--name-field-os-availability-zone' },
        { labelName: 'Status', name: 'state', type: 'default', id: 'field--name-state', class: 'field--name-state' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default OPENSTACK_VOLUME_TEMPLATE;
