import EntityInfoTemplate from 'model/EntityInfoTemplate';

const VMWARE_HOST_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'vmware',
  entityName: 'host',
  entityRecords: [
    {
      panelName: 'Host',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Host', name: 'host', type: 'default' },
        { labelName: 'Power State', name: 'power_state', type: 'default' },
        { labelName: 'Connection State', name: 'connection_state', type: 'default' },
      ]
    },
    {
      panelName: 'Tag Information',
      tableRecordList: ['tag_categories', 'tags'],
      keyValueRecords: [
        { labelName: 'Tag Categories', name: 'tag_categories', type: 'json-table', column: [
          { labelName: 'ID', name: 'id', type: 'default' },
          { labelName: 'Associable Types', name: 'associable_types', type: 'default' },
          { labelName: 'Name', name: 'name', type: 'default' },
          { labelName: 'Description', name: 'description', type: 'default' },
          { labelName: 'Used By', name: 'used_by', type: 'default' },
          { labelName: 'Cardinality', name: 'cardinality', type: 'default' },
        ] },
        { labelName: 'Tags', name: 'tags', type: 'json-table', column: [
          { labelName: 'ID', name: 'id', type: 'default' },
          { labelName: 'Category Id', name: 'category_id', type: 'default' },
          { labelName: 'Name', name: 'name', type: 'default' },
          { labelName: 'Description', name: 'description', type: 'default' },
          { labelName: 'Used By', name: 'used_by', type: 'default' },
        ] },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default VMWARE_HOST_TEMPLATE;
