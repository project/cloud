import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in VMware.
const VMWARE_VM_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'vmware',
  entityName: 'vm',
  entityRecords: [
    {
      panelName: 'VM',
      tableRecordList: ['cdroms', 'networks'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Power State', name: 'power_state', type: 'default' },
        { labelName: 'CPU Count', name: 'cpu_count', type: 'number' },
        { labelName: 'Memory Size (MiB)', name: 'memory_size', type: 'number' },
        { labelName: 'Disk Size (GiB)', name: 'disk_size', type: 'array' },
        { labelName: 'Guest OS', name: 'guest_os', type: 'vmware_os' },
        { labelName: 'CD-ROMS', name: 'cdroms', type: 'default' },
        { labelName: 'Networks', name: 'networks', type: 'default' },
      ]
    },
    {
      panelName: 'Tag Information',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Tag Categories', name: 'tag_categories', type: 'json-table', column: [
          { labelName: 'ID', name: 'id', type: 'default' },
          { labelName: 'Associable Types', name: 'associable_types', type: 'array' },
          { labelName: 'Name', name: 'name', type: 'default' },
          { labelName: 'Description', name: 'description', type: 'default' },
          { labelName: 'Used By', name: 'used_by', type: 'array' },
          { labelName: 'Cardinality', name: 'cardinality', type: 'default' },
        ] },
        { labelName: 'Tags', name: 'tags', type: 'json-table', column: [
          { labelName: 'ID', name: 'id', type: 'default' },
          { labelName: 'Category Id', name: 'category_id', type: 'default' },
          { labelName: 'Name', name: 'name', type: 'default' },
          { labelName: 'Description', name: 'description', type: 'default' },
          { labelName: 'Used By', name: 'used_by', type: 'array' },
        ] },
      ]
    },
  ]
};

export default VMWARE_VM_TEMPLATE;
