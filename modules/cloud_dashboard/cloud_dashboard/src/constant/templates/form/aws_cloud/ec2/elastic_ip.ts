import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_ELASTIC_IP_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'elastic_ip',
    actionType: 'associate',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Select the instance OR network interface',
        keyValueRecords: [
          { type: 'select-local', name: 'resource_type', labelName: 'Resource type', defaultValue: 'instance', value: [
            { name: 'instance', labelName: 'Instance' },
            { name: 'network_interface', labelName: 'Network interface' },
          ] },
          { type: 'select', labelName: 'Instance', name: 'instance_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_elastic_ip/{entity_id}/unassociated_instance_ids',
            defaultValue: ''
          },
          { type: 'select', labelName: 'Private IP', name: 'instance_private_ip',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_elastic_ip/{entity_id}/instance_private_ips/{instance_id}',
            defaultValue: ''
          },
          { type: 'select', labelName: 'Network interface', name: 'network_interface_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_elastic_ip/{entity_id}/unassociated_network_interface_ids',
            defaultValue: ''
          },
          { type: 'select', labelName: 'Private IP', name: 'network_interface_private_ip',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_elastic_ip/{entity_id}/network_interface_private_ips/{network_interface_id}',
            defaultValue: ''
          },
        ]
      }
    ],
    submitButtonLabel: 'Associate Address'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'elastic_ip',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Elastic IP',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '' },
          { type: 'select-local', labelName: 'Domain (Standard | VPC)', name: 'domain', defaultValue: 'standard', value: [
            { labelName: 'Standard', name: 'standard' },
            { labelName: 'VPC', name: 'vpc' },
          ], required: true },
          { type: 'select', labelName: 'Network border group', name: 'network_border_group',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/network_border_groups',
            defaultValue: '', required: true
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'elastic_ip',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Elastic IP',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '' },
          { type: 'default', labelName: 'Type', name: 'elastic_ip_type', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Elastic IP', name: 'public_ip', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Private IP address', name: 'private_ip_address', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Assign',
        keyValueRecords: [
          {
            type: 'join', labelName: 'Instance ID', name: 'instance_id', info: {
              entityTypeId: 'aws_cloud_instance',
              keyColumn: 'name',
            }, defaultValue: '', readOnly: true
          },
          {
            type: 'join', labelName: 'Network interface ID', name: 'network_interface_id', info: {
              entityTypeId: 'aws_cloud_network_interface',
              keyColumn: 'name',
            }, defaultValue: '', readOnly: true
          },
          { type: 'default', labelName: 'Allocation ID', name: 'allocation_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Association ID', name: 'association_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Domain (Standard | VPC)', name: 'domain', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Network interface owner', name: 'network_interface_owner', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Network border group', name: 'network_border_group', defaultValue: '', readOnly: true },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'elastic_ip',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'elastic_ip',
    actionType: 'disassociate',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to disassociate this {{entityName}}({{name}})?'
      },
      {
        type: 'panel',
        panelName: 'Elastic IP Information',
        keyValueRecords: [
          {
            type: 'join', labelName: 'Instance ID', name: 'instance_id', info: {
              entityTypeId: 'aws_cloud_instance',
              keyColumn: 'name',
            }, defaultValue: '', readOnly: true
          },
          {
            type: 'join', labelName: 'Network ID', name: 'network_interface_id', info: {
              entityTypeId: 'aws_cloud_network_interface',
              keyColumn: 'name',
            }, defaultValue: '', readOnly: true
          }
        ]
      }
    ],
    submitButtonLabel: 'Disassociate Address'
  },
]

export default AWS_CLOUD_ELASTIC_IP_TEMPLATE;
