import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_IMAGE_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'image',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Image',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'edit-name', class: 'form-item-name' },
          { type: 'default', labelName: 'Instance ID', name: 'instance_id', defaultValue: '', required: true, id: 'edit-instance-id' ,class: 'form-item-instance-id' },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '', id: 'edit-description', class: 'form-item-description' },
          { type: 'array', labelName: 'AWS account IDs', name: 'launch_permission_account_ids', defaultValue: [''], id: 'edit-launch-permission-account-ids', class: 'form-item-launch-permission-account-ids' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'image',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'image',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Image',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'edit-name', class: 'form-item-name' },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '', required: true, id: 'edit-description', class: 'form-item-description' },
          { type: 'default', labelName: 'AMI name', name: 'ami_name', defaultValue: '', readOnly: true, id: 'edit-ami-name', class: 'form-item-ami-name' },
          { type: 'default', labelName: 'Image ID', name: 'image_id', defaultValue: '', readOnly: true, id: 'edit-image-id', class: 'form-item-image-id' },
          { type: 'default', labelName: 'Owner', name: 'account_id', defaultValue: '', readOnly: true, id: 'edit-account-id', class: 'form-item-account-id' },
          { type: 'default', labelName: 'Source', name: 'source', defaultValue: '', readOnly: true, id: 'edit-source', class: 'form-item-source' },
          { type: 'default', labelName: 'Status', name: 'status', defaultValue: '', readOnly: true, id: 'edit-status', class: 'form-item-status' },
          { type: 'default', labelName: 'State Reason', name: 'state_reason', defaultValue: '', readOnly: true, id: 'edit-state-reason', class: 'form-item-state-reason' },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true, id: 'edit-created', class: 'form-item-created' },
        ]
      },
      {
        type: 'panel',
        panelName: 'Launch Permission:',
        keyValueRecords: [
          { type: 'radio', labelName: 'Visibility', name: 'visibility', defaultValue: '0',
          value: [
            { labelName: 'Private', name: '0' },
            { labelName: 'Public', name: '1' },
          ], orientation: 'horizontal', id: 'edit-visibility', class: 'form-item-visibility' },
          { type: 'array', labelName: 'AWS account IDs', name: 'launch_permission_account_ids', defaultValue: [''], id: 'edit-launch-permission-account-ids', class: 'form-item-launch-permission-account-ids' },
        ]
      },
      {
        type: 'panel',
        panelName: 'Type:',
        keyValueRecords: [
          { type: 'default', labelName: 'Platform', name: 'platform', defaultValue: '', readOnly: true, id: 'edit-platform', class: 'form-item-platform' },
          { type: 'default', labelName: 'Architecture', name: 'architecture', defaultValue: '', readOnly: true, id: 'edit-architecture', class: 'form-item-architecture' },
          { type: 'default', labelName: 'Visualization Type', name: 'visualization_type', defaultValue: '', readOnly: true, id: 'edit-visualization-type', class: 'form-item-visualization-type' },
          { type: 'default', labelName: 'Product Code', name: 'product_code', defaultValue: '', readOnly: true, id: 'edit-product-code', class: 'form-item-product-code' },
          { type: 'default', labelName: 'Image type', name: 'image_type', defaultValue: '', readOnly: true, id: 'edit-image-type', class: 'form-item-image-type' },
        ]
      },
      {
        type: 'panel',
        panelName: 'Device:',
        keyValueRecords: [
          { type: 'default', labelName: 'Root device name', name: 'root_device_name', defaultValue: '', readOnly: true, id: 'edit-root-device-name', class: 'form-item-root-device-name' },
          { type: 'default', labelName: 'Root device type', name: 'root_device_type', defaultValue: '', readOnly: true, id: 'edit-root-device-type', class: 'form-item-root-device-type' },
          { type: 'default', labelName: 'Kernel ID', name: 'kernel_id', defaultValue: '', readOnly: true, id: 'edit-kernel-id', class: 'form-item-kernel-id' },
          { type: 'default', labelName: 'Ramdisk ID', name: 'ramdisk_id', defaultValue: '', readOnly: true, id: 'edit-ramdisk-id', class: 'form-item-ramdisk-id' },
        ]
      },
    ]
  },
]

export default AWS_CLOUD_IMAGE_TEMPLATE;
