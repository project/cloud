import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_INSTANCE_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'instance',
    actionType: 'associate_elastic_ip',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Select Elastic IP',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Elastic IP', name: 'allocation_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/elastic_ips',
            defaultValue: ''
          },
          {
            type: 'select', labelName: 'Private IP', name: 'network_interface_id',
            url: '/cloud_dashboard/aws_cloud/aws_cloud_instance/{entity_id}/private_ips',
            defaultValue: ''
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'instance',
    actionType: 'create_image',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Image',
        keyValueRecords: [
          { labelName: 'Image name', name: 'image_name', type: 'default', defaultValue: '', required: true },
          { labelName: 'No Reboot', name: 'no_reboot', type: 'boolean', defaultValue: false },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'instance',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      }
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'instance',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Instance',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'edit-name', class: 'form-item-name' },
          { type: 'default', labelName: 'Instance ID', name: 'instance_id', defaultValue: '', readOnly: true, id: 'edit-instance-id', class: 'form-item-instance-id' },
          { type: 'default', labelName: 'Instance State', name: 'instance_state', defaultValue: '', readOnly: true, id: 'edit-instance-state', class: 'form-item-instance-state' },
          {
            type: 'select', labelName: 'Instance type', name: 'instance_type',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/instance_types',
            defaultValue: '', readOnly: true, id: 'edit-instance-type', class: 'form-item-instance-type'
          },
          { type: 'cost', labelName: 'Cost', name: 'cost', id: 'edit-cost', class: 'form-item-cost' },
          {
            type: 'select', labelName: 'IAM role', name: 'iam_role',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/iam_role_options',
            defaultValue: '', id: 'edit-iam-role', class: 'form-item-iam-role'
          },
          { type: 'default', labelName: 'AMI image', name: 'image_id', defaultValue: '', readOnly: true, id: 'edit-image-id', class: 'form-item-image-id' },
          { type: 'default', labelName: 'Kernel image', name: 'kernel_id', defaultValue: '', readOnly: true, id: 'edit-kernel-id', class: 'form-item-kernel-id' },
          { type: 'default', labelName: 'Ramdisk image', name: 'ramdisk_id', defaultValue: '', readOnly: true, id: 'edit-ramdisk-id', class: 'form-item-ramdisk-id' },
          { type: 'default', labelName: 'Virtualization', name: 'virtualization', defaultValue: '', readOnly: true, id: 'edit-virtualization', class: 'form-item-virtualization' },
          { type: 'default', labelName: 'Reservation', name: 'reservation', defaultValue: '', readOnly: true, id: 'edit-reservation', class: 'form-item-reservation' },
          { type: 'default', labelName: 'AWS account ID', name: 'account_id', defaultValue: '', readOnly: true, id: 'edit-account-id', class: 'form-item-account-id' },
          { type: 'datetime', labelName: 'Launch time', name: 'launch_time', defaultValue: 0, readOnly: true, id: 'edit-launch-time', class: 'form-item-launch-time' },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true, id: 'edit-created', class: 'form-item-created' },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          {
            type: 'join', labelName: 'Elastic IP', name: 'public_ip', info: {
              entityTypeId: 'aws_cloud_elastic_ip',
              keyColumn: 'public_ip',
            }, defaultValue: '', readOnly: true, id: 'edit-public-ip', class: 'form-item-public-ip'
          },
          { type: 'default', labelName: 'Private IPs', name: 'private_ips', defaultValue: '', readOnly: true, id: 'edit-private-ips', class: 'form-item-private-ips' },
          { type: 'default', labelName: 'Public DNS', name: 'public_dns', defaultValue: '', readOnly: true, id: 'edit-public-dns', class: 'form-item-public-dns' },
          {
            type: 'multi-select', labelName: 'Security groups', name: 'security_groups',
            url: '/cloud_dashboard/aws_cloud/aws_cloud_instance/{entity_id}/security_groups_options',
            defaultValue: [], id: 'edit-security-groups', class: 'form-item-security-groups'
          },
          {
            type: 'join', labelName: 'Key pair name', name: 'key_pair_name', info: {
              entityTypeId: 'aws_cloud_key_pair',
              keyColumn: 'key_pair_name',
            }, defaultValue: '', readOnly: true, id: 'edit-key-pair-name', class: 'form-item-key-pair-name'
          },
          {
            type: 'join', labelName: 'VPC ID', name: 'vpc_id', info: {
              entityTypeId: 'aws_cloud_vpc',
              keyColumn: 'vpc_id',
            }, defaultValue: '', readOnly: true, id: 'edit-vpc-id', class: 'form-item-vpc-id'
          },
          {
            type: 'join', labelName: 'Subnet ID', name: 'subnet_id', info: {
              entityTypeId: 'aws_cloud_subnet',
              keyColumn: 'subnet_id',
            }, defaultValue: '', readOnly: true, id: 'edit-subnet-id', class: 'form-item-subnet-id'
          },
          { type: 'array', labelName: 'Network interfaces', name: 'network_interfaces', defaultValue: [], readOnly: true, id: 'edit-network-interfaces', class: 'form-item-network-interfaces' },
          { type: 'default', labelName: 'Availability Zone', name: 'availability_zone', defaultValue: '', readOnly: true, id: 'edit-availability-zone', class: 'form-item-availability-zone' },
        ]
      },
      {
        type: 'panel',
        panelName: 'Storage',
        keyValueRecords: [
          { type: 'default', labelName: 'Root device type', name: 'root_device_type', defaultValue: '', readOnly: true, id: 'edit-root-device-type', class: 'form-item-root-device-type' },
          { type: 'default', labelName: 'Root device', name: 'root_device', defaultValue: '', readOnly: true, id: 'edit-root-device', class: 'form-item-root-device' },
          { type: 'boolean', labelName: 'EBS optimized', name: 'ebs_optimized', defaultValue: false, labels: ['On', 'Off'], readOnly: true, id: 'edit-ebs-optimized', class: 'form-item-ebs-optimized' },
          { type: 'default', labelName: 'Volume', name: 'block_devices', defaultValue: '', readOnly: true, id: 'edit-block-devices', class: 'form-item-block-devices' },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          {
            type: 'key-value', labelName: 'Tags', name: 'tags',
            defaultValue: [], id: 'edit-tags', class: 'form-item-tags'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Option',
        keyValueRecords: [
          { type: 'datetime', labelName: 'Termination Date', name: 'termination_timestamp', defaultValue: 0, id: 'edit-termination-timestamp', class: 'form-item-termination-timestamp' },
          { type: 'textarea', labelName: 'User data', name: 'user_data', defaultValue: '', id: 'edit-user-data', class: 'form-item-user-data' },
          { type: 'boolean', labelName: 'User data is base64 encoded', name: 'is_user_data_binary', defaultValue: false, id: 'edit-user-data-base64-encoded', class: 'form-item-user-data-base64-encoded' },
          { type: 'boolean', labelName: 'Termination protection', name: 'termination_protection', defaultValue: false, id: 'edit-termination-protection', class: 'form-item-termination-protection' },
          { type: 'boolean', labelName: 'Monitoring Enabled', name: 'monitoring', defaultValue: false, labels: ['Enabled', 'Disabled'], readOnly: true, id: 'edit-is-monitoring', class: 'form-item-is-monitoring' },
          { type: 'default', labelName: 'AMI Launch Index', name: 'ami_launch_index', defaultValue: '', readOnly: true, id: 'edit-ami-launch-index', class: 'form-item-ami-launch-index' },
          { type: 'default', labelName: 'Tenancy', name: 'tenancy', defaultValue: '', readOnly: true, id: 'edit-tenancy', class: 'form-item-tenancy' },
          {
            type: 'select', labelName: 'Schedule', name: 'schedule',
            url: '/cloud_dashboard/schedule_options',
            defaultValue: '', id: 'edit-schedule', class: 'form-item-schedule'
          },
          { type: 'default', labelName: 'Login Username', name: 'login_username', defaultValue: '', readOnly: true, id: 'edit-login-username', class: 'form-item-login-username' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'instance',
    actionType: 'reboot',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to reboot the {{name}} {{entityName}}?',
      }
    ],
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'instance',
    actionType: 'start',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to start the {{name}} {{entityName}}?',
      }
    ],
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'instance',
    actionType: 'stop',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to stop {{name}} {{entityName}}?',
      }
    ],
  },
]

export default AWS_CLOUD_INSTANCE_TEMPLATE;
