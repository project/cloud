import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_KEY_PAIR_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'key_pair',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Key pair',
        keyValueRecords: [
          { type: 'default', labelName: 'Key pair name', name: 'key_pair_name', defaultValue: '' }
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'key_pair',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'key_pair',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Key Pair',
        keyValueRecords: [
          { type: 'default', labelName: 'Key pair name', name: 'key_pair_name', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Key pair ID', name: 'key_pair_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Fingerprint', name: 'key_fingerprint', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true },
        ]
      }
    ],
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'key_pair',
    actionType: 'import',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Key pair',
        keyValueRecords: [
          { type: 'file', labelName: 'Public Key', name: 'key_pair_public_key', id: 'edit-key-pair-public-key', class: 'form-item-files-key-pair-public-key'},
          { type: 'default', labelName: 'Key pair name', name: 'key_pair_name', defaultValue: '', required: true, id: 'edit-key-pair-name', class: 'form-item-key-pair-name' },
        ]
      }
    ]
  },
]

export default AWS_CLOUD_KEY_PAIR_TEMPLATE;
