import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_NETWORK_INTERFACE_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'network_interface',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Network interface',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '' },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '' },
          {
            type: 'select', labelName: 'Subnet', name: 'subnet_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_network_interface/subnet_options',
            defaultValue: ''
          },
          {
            type: 'multi-select', labelName: 'Security groups', name: 'security_groups',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/security_group_options/{subnet_id}',
            defaultValue: []
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'network_interface',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'network_interface',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Network interface',
        keyValueRecords: [
          { type: 'default', labelName: 'Network interface name', name: 'name', defaultValue: '', required: true },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '' },
          { type: 'default', labelName: 'Network interface ID', name: 'network_interface_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Instance ID', name: 'instance_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Allocation ID', name: 'allocation_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Mac Address', name: 'mac_address', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Device Index', name: 'device_index', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Status', name: 'status', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Delete on Termination', name: 'delete_on_termination', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', readOnly: true, defaultValue: 0 },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          { type: 'default', labelName: 'Security group', name: 'security_groups', defaultValue: '', readOnly: true },
          {
            labelName: 'VPC ID', name: 'vpc_id', type: 'join', info: {
              entityTypeId: 'aws_cloud_vpc',
              keyColumn: 'vpc_id',
            }, defaultValue: '', readOnly: true
          },
          { type: 'default', labelName: 'CIDR Block', name: 'cidr_block', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Subnet ID', name: 'subnet_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Public IPs', name: 'public_ips', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Primary private IP', name: 'primary_private_ip', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Secondary private IPs', name: 'secondary_private_ips', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Private DNS', name: 'private_dns', defaultValue: '', readOnly: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Attachment',
        keyValueRecords: [
          { type: 'default', labelName: 'Attachment ID', name: 'attachment_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Attachment Owner', name: 'attachment_owner', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Attachment Status', name: 'attachment_status', defaultValue: '', readOnly: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Owner',
        keyValueRecords: [
          { type: 'default', labelName: 'AWS account ID', name: 'account_id', defaultValue: '', readOnly: true },
        ]
      }

    ]
  },
]

export default AWS_CLOUD_NETWORK_INTERFACE_TEMPLATE;
