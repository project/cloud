import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_SECURITY_GROUP_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'security_group',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Security group',
        keyValueRecords: [
          { type: 'default', labelName: 'Security group name', name: 'group_name', defaultValue: '', required: true },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '', required: true },
          {
            type: 'select', labelName: 'VPC CIDR (ID)', name: 'vpc_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/vpc_options',
            defaultValue: ''
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'security_group',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'security_group',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Security group',
        keyValueRecords: [
          { labelName: 'Name', name: 'group_name', type: 'default', defaultValue: '', required: true, id: 'edit-name', class: 'edit-name' },
          { labelName: 'Security group name', name: 'group_name', type: 'default', defaultValue: '', readOnly: true, id: 'edit-group-name--2', class: 'form-item-group-name' },
          { labelName: 'ID', name: 'group_id', type: 'default', defaultValue: '', readOnly: true, id: 'edit-group-id', class: 'form-item-group-id' },
          { labelName: 'Description', name: 'description', type: 'default', defaultValue: '', readOnly: true, id: 'edit-description', class: 'form-item-description' },
          { labelName: 'Created', name: 'created', type: 'datetime', defaultValue: 0, readOnly: true, id: 'edit-created', class: 'form-item-created' },
          {
            labelName: 'VPC ID', name: 'vpc_id', type: 'join', info: {
              entityTypeId: 'aws_cloud_vpc',
              keyColumn: 'vpc_id',
            }, defaultValue: '', readOnly: true, id: 'edit-vpc-id', class: 'form-item-vpc-id'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Rules',
        keyValueRecords: [
          { labelName: 'Inbound rules', name: 'ip_permission', type: 'sg_permission', defaultValue: [], id: 'edit-ip-permission', class: 'form-item-ip-permission' },
          { labelName: 'Outbound rules', name: 'outbound_permission', type: 'sg_permission', defaultValue: [], id: 'edit-outbound-permission', class: 'form-item-outbound-permission' },
        ]
      }
    ],
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'security_group',
    actionType: 'revoke',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to revoke the following permission?'
      },
    ],
    submitButtonLabel: 'Revoke'
  }
]

export default AWS_CLOUD_SECURITY_GROUP_TEMPLATE;
