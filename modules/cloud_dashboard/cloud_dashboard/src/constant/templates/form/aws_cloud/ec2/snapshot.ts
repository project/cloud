import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_SNAPSHOT_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'snapshot',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Snapshot',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '' },
          { type: 'select', labelName: 'Volume ID', name: 'volume_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/snapshot_volume_ids',
            defaultValue: '', required: true
          },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'snapshot',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'snapshot',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Snapshot',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'edit-name', class: 'form-item-name' },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '', readOnly: true, id: 'edit-description', class: 'form-item-description' },
          { type: 'default', labelName: 'Snapshot ID', name: 'snapshot_id', defaultValue: '', readOnly: true, id: 'edit-snapshot-id', class: 'form-item-snapshot-id' },
          { type: 'default', labelName: 'Volume ID', name: 'volume_id', defaultValue: '', readOnly: true, id: 'edit-volume-id', class: 'form-item-volume-id' },
          { type: 'default', labelName: 'Size (GB)', name: 'size', defaultValue: '', readOnly: true, id: 'edit-size', class: 'form-item-size' },
          { type: 'default', labelName: 'Status', name: 'status', defaultValue: '', readOnly: true, id: 'edit-status', class: 'form-item-status' },
          { type: 'default', labelName: 'Progress', name: 'Progress', defaultValue: '', readOnly: true, id: 'edit-progress', class: 'form-item-progress' },
          { type: 'boolean', labelName: 'Encrypted', name: 'encrypted_value', defaultValue: false, id: 'edit-encrypted', class: 'form-item-encrypted' },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true, id: 'edit-created', class: 'form-item-created' },
        ]
      }
    ]
  },
]

export default AWS_CLOUD_SNAPSHOT_TEMPLATE;
