import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_VOLUME_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'volume',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Volume',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'edit-name', class: 'form-item-name' },
          { type: 'select', labelName: 'Snapshot ID', name: 'snapshot_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/volume_snapshot_ids',
            defaultValue: '', required: true, id: 'edit-snapshot-id', class: 'form-item-snapshot-id'
          },
          { type: 'number', labelName: 'Size (GiB)', name: 'size', defaultValue: 1, required: true, id: 'edit-size', class: 'form-item-size' },
          { type: 'select', labelName: 'Volume type', name: 'volume_type',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/volume_types',
            defaultValue: '', required: true, id: 'edit-volume-type', class: 'form-item-volume-type'
          },
          { type: 'number', labelName: 'IOPS', name: 'iop', defaultValue: 1, id: 'edit-iops', class: 'form-item-iops' },
          { type: 'select', labelName: 'Availability Zone', name: 'availability_zone',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/availability_zones',
            defaultValue: '', required: true, id: 'edit-availability-zone', class: 'form-item-availability-zone'
          },
          { type: 'default', labelName: 'KMS key ID', name: 'kms_key_id', defaultValue: '', id: 'edit-kms-key-id', class: 'form-item-kms-key-id' },
          { type: 'boolean', labelName: 'Encrypted', name: 'encrypted', defaultValue: true, id: 'edit-encrypted', class: 'form-item-encrypted' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'volume',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'volume',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Volume',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '' },
          { type: 'default', labelName: 'Volume ID', name: 'volume_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Instance ID', name: 'attachment_information', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Snapshot ID', name: 'snapshot_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Snapshot name', name: 'snapshot_name', defaultValue: '', readOnly: true },
          { type: 'number', labelName: 'Size (GiB)', name: 'size', defaultValue: 1 },
          { type: 'select', labelName: 'Volume type', name: 'volume_type',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/volume_types',
            defaultValue: '', required: true
          },
          { type: 'number', labelName: 'IOPS', name: 'iop', defaultValue: 1 },
          { type: 'default', labelName: 'Availability Zone', name: 'availability_zone', defaultValue: '', readOnly: true },
          { type: 'boolean', labelName: 'Encrypted', name: 'encrypted', defaultValue: false, labels: ['On', 'Off'], readOnly: true },
          { type: 'default', labelName: 'Status', name: 'state', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'volume',
    actionType: 'attach',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to attach {{entityName}}: {{name}}?'
      },
      {
        type: 'panel',
        panelName: 'Volume Information',
        keyValueRecords: [
          { type: 'default', labelName: 'Volume id', name: 'volume_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Volume name', name: 'name', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Device name', name: 'device_name', defaultValue: '' },
          { type: 'select', labelName: 'Instance ID', name: 'instance_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_volume/{entity_id}/instances',
            defaultValue: '', required: true
          },
        ]
      },
    ],
    submitButtonLabel: 'Attach'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'volume',
    actionType: 'detach',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to detach {{entityName}}: {{name}}?'
      },
      {
        type: 'panel',
        panelName: 'Volume Information',
        keyValueRecords: [
          { type: 'default', labelName: 'Volume ID', name: 'volume_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Volume name', name: 'name', defaultValue: '', readOnly: true },
          {
            type: 'join', labelName: 'Attached to instance', name: 'attachment_information', info: {
              entityTypeId: 'aws_cloud_instance',
              keyColumn: 'instance_id',
            }, defaultValue: '', readOnly: true
          },
        ]
      },
    ],
    submitButtonLabel: 'Detach'
  },
]

export default AWS_CLOUD_VOLUME_TEMPLATE;
