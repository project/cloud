import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_CARRIER_GATEWAY_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'carrier_gateway',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Carrier gateway',
        keyValueRecords: [
          {
            type: 'default',
            labelName: 'Name',
            name: 'name',
            defaultValue: '',
            required: true
          },
          {
            type: 'select', labelName: 'VPC CIDR (ID)', name: 'vpc_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/vpc_options',
            defaultValue: ''
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'carrier_gateway',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Carrier gateway',
        keyValueRecords: [
          {
            type: 'default',
            labelName: 'Name',
            name: 'name',
            defaultValue: '',
            required: true
          },
          {
            type: 'default',
            labelName: 'Carrier gateway ID',
            name: 'carrier_gateway_id',
            defaultValue: '',
            readOnly: true
          },
          {
            type: 'default',
            labelName: 'State',
            name: 'state',
            defaultValue: '',
            readOnly: true
          },
          {
            labelName: 'VPC CIDR (ID)', name: 'vpc_id', type: 'join', info: {
              entityTypeId: 'aws_cloud_vpc',
              keyColumn: 'vpc_id',
            }, defaultValue: '', readOnly: true
          },
          {
            type: 'datetime',
            labelName: 'Created',
            name: 'created',
            defaultValue: 0,
            readOnly: true
          },
          {
            type: 'key-value', labelName: 'Tags', name: 'tags',
            defaultValue: []
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'carrier_gateway',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
]

export default AWS_CLOUD_CARRIER_GATEWAY_TEMPLATE;
