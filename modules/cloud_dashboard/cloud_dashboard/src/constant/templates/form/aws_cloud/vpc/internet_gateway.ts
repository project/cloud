import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_INTERNET_GATEWAY_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'internet_gateway',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Internet gateway',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'internet_gateway',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Internet gateway',
        keyValueRecords: [
          {
            type: 'default',
            labelName: 'Name',
            name: 'name',
            defaultValue: '',
            required: true
          },
          {
            type: 'default',
            labelName: 'Internet gateway ID',
            name: 'internet_gateway_id',
            defaultValue: '',
            readOnly: true
          },
          {
            type: 'default',
            labelName: 'State',
            name: 'state',
            defaultValue: '',
            readOnly: true
          },
          {
            labelName: 'VPC CIDR (ID)', name: 'vpc_id', type: 'join', info: {
              entityTypeId: 'aws_cloud_vpc',
              keyColumn: 'vpc_id',
            }, defaultValue: '', readOnly: true
          },
          {
            type: 'datetime',
            labelName: 'Created',
            name: 'created',
            defaultValue: 0,
            readOnly: true
          },
          {
            type: 'key-value', labelName: 'Tags', name: 'tags',
            defaultValue: []
          },
        ]
      },
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'internet_gateway',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'internet_gateway',
    actionType: 'attach',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to attach {{entityName}}: {{name}}?'
      },
      {
        type: 'panel',
        panelName: 'Internet gateway Information',
        keyValueRecords: [
          { type: 'default', labelName: 'Internet gateway', name: 'name', defaultValue: '', readOnly: true },
		      { type: 'select', labelName: 'VPC CIDR (ID)', name: 'vpc_id',
		        url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_internet_gateway/{entity_id}/vpcs',
		        defaultValue: '', required: true
		      },
        ]
      },
    ],
    submitButtonLabel: 'Attach'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'internet_gateway',
    actionType: 'detach',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to detach {{entityName}}: {{name}}?'
      },
      {
        type: 'panel',
        panelName: 'Volume Information',
        keyValueRecords: [
          { type: 'default', labelName: 'Internet gateway', name: 'internet_gateway_id', defaultValue: '', readOnly: true },
          {
            type: 'join', labelName: 'Attached to VPC', name: 'vpc_id', info: {
              entityTypeId: 'aws_cloud_vpc',
              keyColumn: 'vpc_id',
            }, defaultValue: '', readOnly: true
          },
        ]
      },
    ],
    submitButtonLabel: 'Detach'
  },
]

export default AWS_CLOUD_INTERNET_GATEWAY_TEMPLATE;
