import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_SUBNET_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'subnet',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Subnet',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true },
          {
            type: 'select', labelName: 'VPC CIDR (ID)', name: 'vpc_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/vpc_options',
            defaultValue: '',
            required: true,
          },
          {
            type: 'select', labelName: 'Availability Zone', name: 'availability_zone',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/availability_zones',
            defaultValue: ''
          },
          { type: 'default', labelName: 'IPv4 CIDR block', name: 'cidr_block', defaultValue: '', required: true },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'subnet',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Subnet',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true },
          { type: 'default', labelName: 'CIDR Block', name: 'cidr_block', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Subnet ID', name: 'subnet_id', defaultValue: '', readOnly: true },
          {
            labelName: 'VPC ID', name: 'vpc_id', type: 'join', info: {
              entityTypeId: 'aws_cloud_vpc',
              keyColumn: 'vpc_id',
            }, defaultValue: '', readOnly: true
          },
          { type: 'default', labelName: 'Availability Zone', name: 'availability_zone', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'State', name: 'state', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'AWS account ID', name: 'account_id', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true },
          {type: 'key-value', labelName: 'Tags', name: 'tags', defaultValue: [] },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'subnet',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
]

export default AWS_CLOUD_SUBNET_TEMPLATE;
