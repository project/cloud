import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_TRANSIT_GATEWAY_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'transit_gateway',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Transit gateway',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '', required: true },
          { type: 'default', labelName: 'Amazon side ASN', name: 'amazon_side_asn', defaultValue: '', required: true },
          { type: 'boolean', labelName: 'DNS support', name: 'dns_support', defaultValue: true },
          { type: 'boolean', labelName: 'VPN ECMP support', name: 'vpn_ecmp_support', defaultValue: true },
          { type: 'boolean', labelName: 'Default route table association', name: 'default_route_table_association', defaultValue: true },
          { type: 'boolean', labelName: 'Default route table propagation', name: 'default_route_table_propagation', defaultValue: true },
          { type: 'boolean', labelName: 'Multicast support', name: 'multicast_support', defaultValue: false },
          { type: 'boolean', labelName: 'Auto accept shared attachments', name: 'auto_accept_shared_attachments', defaultValue: false },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'transit_gateway',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Transit gateway',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '', required: false },
          { type: 'default', labelName: 'Transit gateway ID', name: 'transit_gateway_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'State', name: 'state', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Amazon side ASN', name: 'amazon_side_asn', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Multicast support', name: 'multicast_support', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true },
          { type: 'boolean', labelName: 'DNS support', name: 'dns_support', defaultValue: true },
          { type: 'boolean', labelName: 'VPN ECMP support', name: 'vpn_ecmp_support', defaultValue: true },
          { type: 'boolean', labelName: 'Default route table association', name: 'default_route_table_association', defaultValue: true },
          {
            type: 'select',
            labelName: 'Association Default Route Table ID',
            name: 'association_default_route_table_id',
            defaultValue: '',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_transit_gateway/{entity_id}/route_tables',
            defaultValueUrl: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_transit_gateway/{entity_id}/association_default_route_table_id',
          },
          { type: 'boolean', labelName: 'Default route table propagation', name: 'default_route_table_propagation', defaultValue: true },
          {
            type: 'select',
            labelName: 'Propagation Default Route Table ID',
            name: 'propagation_default_route_table_id',
            defaultValue: '',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_transit_gateway/{entity_id}/route_tables',
            defaultValueUrl: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_transit_gateway/{entity_id}/propagation_default_route_table_id',
          },
          { type: 'boolean', labelName: 'Multicast support', name: 'multicast_support', defaultValue: false },
          { type: 'boolean', labelName: 'Auto accept shared attachments', name: 'auto_accept_shared_attachments', defaultValue: false },
          { type: 'key-value', labelName: 'Tags', name: 'tags', defaultValue: [] },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'transit_gateway',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
]

export default AWS_CLOUD_TRANSIT_GATEWAY_TEMPLATE;
