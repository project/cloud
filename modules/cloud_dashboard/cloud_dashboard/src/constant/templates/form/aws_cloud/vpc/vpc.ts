import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_VPC_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'vpc',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'VPC',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true },
          { type: 'default', labelName: 'IPv4 CIDR block', name: 'cidr_block', defaultValue: '', required: true },
          { type: 'select-local', labelName: 'IPv6 CIDR block', name: 'amazon_provided_ipv6_cidr_block', defaultValue: '0', value: [
            { name: '0', labelName: 'No IPv6 CIDR Block' },
            { name: '1', labelName: 'Amazon provided IPv6 CIDR block' },
          ] },
          { type: 'select-local', labelName: 'Tenancy', name: 'instance_tenancy', defaultValue: 'default', value: [
            { name: 'default', labelName: 'Default' },
            { name: 'dedicated', labelName: 'Dedicated' },
          ] },
          { type: 'boolean', labelName: 'Flow log', name: 'flow_log', defaultValue: false }
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'vpc',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'VPC',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'edit-name', class: 'form-item-name' },
          { type: 'default', labelName: 'VPC ID', name: 'vpc_id', defaultValue: '', readOnly: true, id: 'edit-vpc-id', class: 'form-item-vpc-id' },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true, id: 'edit-created', class: 'form-item-created' },
        ]
      },
      {
        type: 'panel',
        panelName: 'Flow logs',
        keyValueRecords: [
          { type: 'boolean', labelName: 'Flow log', name: 'flow_log', defaultValue: true, id: 'edit-flow-log', class: 'form-item-flow-log' },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          { type: 'key-value', labelName: 'Tags', name: 'tags', defaultValue: [], id: 'edit-tags', class: 'form-item-tags' },
        ]
      },
      {
        type: 'panel',
        panelName: 'CIDR Blocks',
        keyValueRecords: [
          { type: 'item-array', labelName: 'IPv4 CIDR', name: 'cidr_blocks', defaultValue: [], info: [
            { labelName: 'CIDR', name: 'cidr', type: 'default' },
            { labelName: 'State', name: 'state', type: 'default', readOnly: true },
            { labelName: 'Status reason', name: 'status_message', type: 'default', readOnly: true },
            { labelName: 'Association ID', name: 'association_id', type: 'default', readOnly: true },
          ], id: 'edit-cidr-blocks', class: 'form-item-cidr-blocks' },
          { type: 'item-array', labelName: 'IPv6 CIDR', name: 'ipv6_cidr_blocks', defaultValue: [], info: [
            { labelName: 'CIDR', name: 'cidr', type: 'default' },
            { labelName: 'State', name: 'state', type: 'default', readOnly: true },
            { labelName: 'Status reason', name: 'status_message', type: 'default', readOnly: true },
            { labelName: 'Association ID', name: 'association_id', type: 'default', readOnly: true },
          ], id: 'edit-ipv6-cidr-blocks', class: 'form-item-ipv6-cidr-blocks' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'vpc',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
]

export default AWS_CLOUD_VPC_TEMPLATE;
