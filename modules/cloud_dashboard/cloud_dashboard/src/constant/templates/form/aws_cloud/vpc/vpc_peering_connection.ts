import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_VPC_PEERING_CONNECTION: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'vpc_peering_connection',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'VPC peering connection',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'edit-name', class: 'form-item-name' },
          {
            type: 'select', labelName: 'Requester VPC ID', name: 'requester_vpc_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/vpc_options',
            defaultValue: '',
            required: true, id: 'edit-requester-vpc-id', class: 'form-item-requester-vpc-id'
          },
          {
            type: 'default',
            labelName: 'Accepter AWS account ID',
            name: 'accepter_account_id',
            defaultValue: '',
            defaultValueUrl: '/cloud_dashboard/aws_cloud/{cloud_context}/account_id',
            id: 'edit-accepter-account-id',
            class: 'form-item-accepter-account-id',
          },
          {
            type: 'select',
            labelName: 'Accepter region',
            name: 'accepter_region',
            defaultValue: '',
            defaultValueUrl: '/cloud_dashboard/aws_cloud/{cloud_context}/defaultRegion',
            required: true,
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/regions',
            id: 'edit-accepter-region',
            class: 'form-item-accepter-region',
          },
          {
            type: 'select',
            labelName: 'Accepter VPC ID',
            name: 'accepter_vpc_id',
            defaultValue: '',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/vpc_options',
            required: true,
            id: 'edit-accepter-vpc-id',
            class: 'form-item-accepter-vpc-id',
          },
          {
            type: 'default',
            labelName: 'Accepter VPC ID',
            name: 'accepter_vpc_id_text',
            defaultValue: '',
            required: true,
          },
          {
            type: 'hidden',
            labelName: '',
            name: 'default_account_id',
            defaultValue: '',
            defaultValueUrl: '/cloud_dashboard/aws_cloud/{cloud_context}/account_id',
          },
          {
            type: 'hidden',
            labelName: '',
            name: 'default_region',
            defaultValue: '',
            defaultValueUrl: '/cloud_dashboard/aws_cloud/{cloud_context}/defaultRegion',
          }
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'vpc_peering_connection',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'VPC peering connection',
        keyValueRecords: [
          {
            type: 'default',
            labelName: 'Name',
            name: 'name',
            defaultValue: '',
            required: true
          },
          {
            type: 'default',
            labelName: 'VPC peering connection ID',
            name: 'vpc_peering_connection_id',
            defaultValue: '',
            readOnly: true,
          },
          {
            type: 'default',
            labelName: 'Status Code',
            name: 'status_code',
            defaultValue: '',
            readOnly: true,
          },
          {
            type: 'default',
            labelName: 'Status Message',
            name: 'status_message',
            defaultValue: '',
            readOnly: true,
          },
          {
            type: 'datetime',
            labelName: 'Created',
            name: 'created',
            defaultValue: 0,
            readOnly: true
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Requester',
        keyValueRecords: [
          {
            type: 'default',
            labelName: 'Requester VPC ID',
            name: 'requester_vpc_id',
            defaultValue: '',
            readOnly: true,
          },
          {
            type: 'default',
            labelName: 'Requester CIDR Block',
            name: 'requester_cidr_block',
            defaultValue: '',
            readOnly: true,
          },
          {
            type: 'default',
            labelName: 'Requester AWS account ID',
            name: 'requester_account_id',
            defaultValue: '',
            readOnly: true,
          },
          {
            type: 'default',
            labelName: 'Requester region',
            name: 'requester_region',
            defaultValue: '',
            readOnly: true,
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Accepter',
        keyValueRecords: [
          {
            type: 'default',
            labelName: 'Accepter VPC ID',
            name: 'accepter_vpc_id',
            defaultValue: '',
            readOnly: true,
          },
          {
            type: 'default',
            labelName: 'Accepter CIDR Block',
            name: 'accepter_cidr_block',
            defaultValue: '',
            readOnly: true,
          },
          {
            type: 'default',
            labelName: 'Accepter AWS account ID',
            name: 'accepter_account_id',
            defaultValue: '',
            readOnly: true,
          },
          {
            type: 'default',
            labelName: 'Accepter region',
            name: 'accepter_region',
            defaultValue: '',
            readOnly: true,
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          {
            type: 'key-value', labelName: 'Tags', name: 'tags',
            defaultValue: []
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'vpc_peering_connection',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
]

export default AWS_CLOUD_VPC_PEERING_CONNECTION;
