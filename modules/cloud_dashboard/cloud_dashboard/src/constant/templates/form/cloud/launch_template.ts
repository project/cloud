import CloudContext from 'model/CloudContext';
import EntityColumn from 'model/EntityColumn';

// Template for displaying the Launch template in AWS Cloud.
const AWS_LAUNCH_TEMPLATE_LIST: EntityColumn[] = [
  { labelName: 'Name', 'name': 'name', type: 'default' },
  { labelName: 'AMI name', 'name': 'field_image_id', type: 'default' },
  { labelName: 'Instance type', 'name': 'field_instance_type', type: 'default' },
  { labelName: 'Security group', 'name': 'field_security_group', type: 'relationship', info: {
    entityTypeId: 'aws_cloud_security_group',
    keyColumn1: 'drupal_internal__target_id',
    keyColumn2: 'drupal_internal__id',
    valueColumn: 'name',
  } },
  { labelName: 'Key pair', 'name': 'field_ssh_key', type: 'relationship', info: {
    entityTypeId: 'aws_cloud_key_pair',
    keyColumn1: 'drupal_internal__target_id',
    keyColumn2: 'drupal_internal__id',
    valueColumn: 'key_pair_name',
  } },
  {
    labelName: 'VPC', 'name': 'field_vpc', type: 'join', info: {
      entityTypeId: 'aws_cloud_vpc',
      keyColumn: 'vpc_id',
      valueColumn: 'name',
    }
  },
  { labelName: 'Max count', 'name': 'field_max_count', type: 'default' },
  { labelName: 'Status', 'name': 'field_workflow_status', type: 'default' },
];

const DEFAULT_AWS_LAUNCH_TEMPLATE: EntityColumn = { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' };

// Template for displaying the Launch template in K8s.
const K8S_LAUNCH_TEMPLATE_LIST: EntityColumn[] = [
  { labelName: 'Name', 'name': 'name', type: 'default' },
  { labelName: 'Namespace', 'name': 'field_namespace', type: 'default' },
  { labelName: 'Object', 'name': 'field_object', type: 'array' },
  { labelName: 'Enable time scheduler', 'name': 'field_enable_time_scheduler', type: 'boolean', value: ['On', 'Off'] },
  { labelName: 'Workflow status', 'name': 'field_workflow_status', type: 'default' },
];

const DEFAULT_K8S_LAUNCH_TEMPLATE: EntityColumn = { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' };

// Template for displaying the Launch template in OpenStack.
const OPENSTACK_LAUNCH_TEMPLATE_LIST: EntityColumn[] = [
  { labelName: 'Name', 'name': 'name', type: 'default' },
  { labelName: 'Image name', 'name': 'field_openstack_image_id', type: 'join', info: {
      entityTypeId: 'openstack_image',
      keyColumn: 'image_id',
      valueColumn: 'name',
    }
  },
  { labelName: 'Flavor', 'name': 'field_flavor', type: 'join', info: {
      entityTypeId: 'openstack_flavor',
      keyColumn: 'flavor_id',
      valueColumn: 'name',
    }
  },
  {
    labelName: 'Security group', 'name': 'field_openstack_security_group', type: 'relationship', info: {
      entityTypeId: 'openstack_security_group',
      keyColumn1: 'drupal_internal__target_id',
      keyColumn2: 'drupal_internal__id',
      valueColumn: 'name',
    }
  },
  {
    labelName: 'Key pair', 'name': 'field_openstack_ssh_key', type: 'relationship', info: {
      entityTypeId: 'openstack_key_pair',
      keyColumn1: 'drupal_internal__target_id',
      keyColumn2: 'drupal_internal__id',
      valueColumn: 'key_pair_name',
    }
  },
  { labelName: 'VPC', 'name': 'field_openstack_vpc', type: 'default' },
  { labelName: 'Max count', 'name': 'field_max_count', type: 'default' },
];

const DEFAULT_OPENSTACK_LAUNCH_TEMPLATE: EntityColumn = { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' };

// Template for displaying the Launch template in VMware.
const VMWARE_LAUNCH_TEMPLATE_LIST: EntityColumn[] = [];

const DEFAULT_VMWARE_LAUNCH_TEMPLATE: EntityColumn = { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' };

/**
 * Get LaunchTemplateColumnList by cloud_context.
 *
 * @param cloudContext cloud_context.
 * @returns LaunchTemplateColumnList.
 */
const getLaunchTemplateColumnList = (cloudContext: CloudContext): EntityColumn[] => {
  switch (cloudContext.cloudServiceProvider) {
    case 'aws_cloud':
      return cloudContext.name !== 'ALL'
        ? AWS_LAUNCH_TEMPLATE_LIST
        : [
          DEFAULT_AWS_LAUNCH_TEMPLATE,
          ...AWS_LAUNCH_TEMPLATE_LIST
        ];
    case 'k8s':
      return cloudContext.name !== 'ALL'
        ? K8S_LAUNCH_TEMPLATE_LIST
        : [
          DEFAULT_K8S_LAUNCH_TEMPLATE,
          ...K8S_LAUNCH_TEMPLATE_LIST
        ];
    case 'openstack':
      return cloudContext.name !== 'ALL'
        ? OPENSTACK_LAUNCH_TEMPLATE_LIST
        : [
          DEFAULT_OPENSTACK_LAUNCH_TEMPLATE,
          ...OPENSTACK_LAUNCH_TEMPLATE_LIST
        ];
    case 'vmware':
      return cloudContext.name !== 'ALL'
        ? VMWARE_LAUNCH_TEMPLATE_LIST
        : [
          DEFAULT_VMWARE_LAUNCH_TEMPLATE,
          ...VMWARE_LAUNCH_TEMPLATE_LIST
        ];
  }
};

export default getLaunchTemplateColumnList;
