import AWS_CLOUD_ELASTIC_IP_TEMPLATE from 'constant/templates/form/aws_cloud/ec2/elastic_ip';
import AWS_CLOUD_IMAGE_TEMPLATE from 'constant/templates/form/aws_cloud/ec2/image';
import AWS_CLOUD_INSTANCE_TEMPLATE from 'constant/templates/form/aws_cloud/ec2/instance';
import AWS_CLOUD_KEY_PAIR_TEMPLATE from 'constant/templates/form/aws_cloud/ec2/key_pair';
import AWS_CLOUD_NETWORK_INTERFACE_TEMPLATE from 'constant/templates/form/aws_cloud/ec2/network_interface';
import AWS_CLOUD_SECURITY_GROUP_TEMPLATE from 'constant/templates/form/aws_cloud/ec2/security_group';
import AWS_CLOUD_SNAPSHOT from 'constant/templates/form/aws_cloud/ec2/snapshot';
import AWS_CLOUD_VOLUME_TEMPLATE from 'constant/templates/form/aws_cloud/ec2/volume';
import AWS_CLOUD_CARRIER_GATEWAY_TEMPLATE from 'constant/templates/form/aws_cloud/vpc/carrier_gateway';
import AWS_CLOUD_INTERNET_GATEWAY_TEMPLATE from 'constant/templates/form/aws_cloud/vpc/internet_gateway';
import AWS_CLOUD_SUBNET_TEMPLATE from 'constant/templates/form/aws_cloud/vpc/subnet';
import AWS_CLOUD_TRANSIT_GATEWAY_TEMPLATE from 'constant/templates/form/aws_cloud/vpc/transit_gateway';
import AWS_CLOUD_VPC_TEMPLATE from 'constant/templates/form/aws_cloud/vpc/vpc';
import AWS_CLOUD_VPC_PEERING_CONNECTION from 'constant/templates/form/aws_cloud/vpc/vpc_peering_connection';
import K8S_DEPLOYMENT_TEMPLATE from 'constant/templates/form/k8s/deployment';
import K8S_NAMESPACE_TEMPLATE from 'constant/templates/form/k8s/namespace';
import K8S_OTHER_TEMPLATE from 'constant/templates/form/k8s/other';
import K8S_POD_TEMPLATE from 'constant/templates/form/k8s/pod';
import K8S_SCHEDULE_TEMPLATE from 'constant/templates/form/k8s/schedule';
import AWS_CLOUD_SERVER_TEMPLATE from 'constant/templates/form/launch_template/aws_cloud';
import OPENSTACK_SERVER_TEMPLATE from 'constant/templates/form/launch_template/openstack';
import OPENSTACK_FLOATING_IP_TEMPLATE from 'constant/templates/form/openstack/floating_ip';
import OPENSTACK_IMAGE_TEMPLATE from 'constant/templates/form/openstack/image';
import OPENSTACK_INSTANCE_TEMPLATE from 'constant/templates/form/openstack/instance';
import OPENSTACK_KEY_PAIR_TEMPLATE from 'constant/templates/form/openstack/key_pair';
import OPENSTACK_NETWORK_TEMPLATE from 'constant/templates/form/openstack/network';
import OPENSTACK_PORT_TEMPLATE from 'constant/templates/form/openstack/port';
import OPENSTACK_PROJECT_TEMPLATE from 'constant/templates/form/openstack/project';
import OPENSTACK_QUOTA_TEMPLATE from 'constant/templates/form/openstack/quota';
import OPENSTACK_ROLE_TEMPLATE from 'constant/templates/form/openstack/role';
import OPENSTACK_ROUTER_TEMPLATE from 'constant/templates/form/openstack/router';
import OPENSTACK_SECURITY_GROUP_TEMPLATE from 'constant/templates/form/openstack/security_group';
import OPENSTACK_SERVER_GROUP_TEMPLATE from 'constant/templates/form/openstack/server_group';
import OPENSTACK_SNAPSHOT_TEMPLATE from 'constant/templates/form/openstack/snapshot';
import OPENSTACK_STACK_TEMPLATE from 'constant/templates/form/openstack/stack';
import OPENSTACK_SUBNET_TEMPLATE from 'constant/templates/form/openstack/subnet';
import OPENSTACK_USER_TEMPLATE from 'constant/templates/form/openstack/user';
import OPENSTACK_VOLUME_TEMPLATE from 'constant/templates/form/openstack/volume';
import VMWARE_VM_TEMPLATE from 'constant/templates/form/vmware/vm';

const ENTITY_FORM_LIST = [
  ...AWS_CLOUD_ELASTIC_IP_TEMPLATE,
  ...AWS_CLOUD_IMAGE_TEMPLATE,
  ...AWS_CLOUD_INSTANCE_TEMPLATE,
  ...AWS_CLOUD_NETWORK_INTERFACE_TEMPLATE,
  ...AWS_CLOUD_KEY_PAIR_TEMPLATE,
  ...AWS_CLOUD_SECURITY_GROUP_TEMPLATE,
  ...AWS_CLOUD_CARRIER_GATEWAY_TEMPLATE,
  ...AWS_CLOUD_INTERNET_GATEWAY_TEMPLATE,
  ...AWS_CLOUD_TRANSIT_GATEWAY_TEMPLATE,
  ...AWS_CLOUD_VPC_TEMPLATE,
  ...AWS_CLOUD_VPC_PEERING_CONNECTION,
  ...AWS_CLOUD_SUBNET_TEMPLATE,
  ...AWS_CLOUD_SNAPSHOT,
  ...AWS_CLOUD_VOLUME_TEMPLATE,
  ...AWS_CLOUD_SERVER_TEMPLATE,
  ...K8S_DEPLOYMENT_TEMPLATE,
  ...K8S_NAMESPACE_TEMPLATE,
  ...K8S_POD_TEMPLATE,
  ...K8S_SCHEDULE_TEMPLATE,
  // K8S_OTHER_TEMPLATE must be registered after the other K8S_XXX_TEMPLATE.
  ...K8S_OTHER_TEMPLATE,
  ...OPENSTACK_FLOATING_IP_TEMPLATE,
  ...OPENSTACK_INSTANCE_TEMPLATE,
  ...OPENSTACK_IMAGE_TEMPLATE,
  ...OPENSTACK_SECURITY_GROUP_TEMPLATE,
  ...OPENSTACK_KEY_PAIR_TEMPLATE,
  ...OPENSTACK_SNAPSHOT_TEMPLATE,
  ...OPENSTACK_VOLUME_TEMPLATE,
  ...OPENSTACK_NETWORK_TEMPLATE,
  ...OPENSTACK_SUBNET_TEMPLATE,
  ...OPENSTACK_PORT_TEMPLATE,
  ...OPENSTACK_ROUTER_TEMPLATE,
  ...OPENSTACK_QUOTA_TEMPLATE,
  ...OPENSTACK_SERVER_GROUP_TEMPLATE,
  ...OPENSTACK_PROJECT_TEMPLATE,
  ...OPENSTACK_STACK_TEMPLATE,
  ...OPENSTACK_ROLE_TEMPLATE,
  ...OPENSTACK_USER_TEMPLATE,
  ...OPENSTACK_SERVER_TEMPLATE,
  ...VMWARE_VM_TEMPLATE,
];

export default ENTITY_FORM_LIST;
