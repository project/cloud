import EntityFormTemplate from 'model/EntityFormTemplate';

const K8S_DEPLOYMENT_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'k8s',
    entityName: 'deployment',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Deployment',
        keyValueRecords: [
          { type: 'textarea', labelName: 'Detail', name: 'detail', defaultValue: '', },
          { type: 'default', labelName: 'YAML URL', name: 'yaml_url', defaultValue: '' },
        ]
      },
      {
        type: 'panel',
        panelName: 'Time scheduler',
        keyValueRecords: [
          {
            type: 'boolean', labelName: 'Enable time scheduler', name: 'enableTimeScheduler',
            defaultValue: false
          },
          {
            type: 'radio', labelName: 'Scheduler Use Type', name: 'time_scheduler_option',
            value: [
              { labelName: 'Use Cloud Orchestrator', name: 'cloud_orchestrator_scheduler' },
              { labelName: 'Use CronJob', name: 'cronjob_scheduler' }
            ], defaultValue: 'cloud_orchestrator_scheduler',
            orientation: 'vertical'
          },
          {
            type: 'time', labelName: 'Start-up Time', hourName: 'startHour', minuteName: 'startMinute',
            defaultValue: ['6', '0']
          },
          {
            type: 'time', labelName: 'Stop Time', hourName: 'stopHour', minuteName: 'stopMinute',
            defaultValue: ['18', '0']
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'k8s',
    entityName: 'deployment',
    actionType: 'scale',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Deployment',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Current ready replicas', name: 'ready_replicas', defaultValue: '', readOnly: true
          },
          {
            type: 'number', labelName: 'Desire number of pods', name: 'replicas', defaultValue: 1, required: true
          },
        ]
      }
    ]
  }
]

export default K8S_DEPLOYMENT_TEMPLATE;
