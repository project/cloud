import EntityFormTemplate from 'model/EntityFormTemplate';

const K8S_NAMESPACE_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'k8s',
    entityName: 'namespace',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Namespace',
        keyValueRecords: [
          {
            type: 'default', labelName: 'name', name: 'name',
            defaultValue: '', required: true
          },
          {
            type: 'key-value', labelName: 'Labels', name: 'labels',
            defaultValue: []
          },
          {
            type: 'key-value', labelName: 'Annotations', name: 'annotations',
            defaultValue: []
          }
        ],
      },
    ]
  },
  {
    cloudServiceProvider: 'k8s',
    entityName: 'namespace',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Namespace',
        keyValueRecords: [
          {
            type: 'default', labelName: 'name', name: 'name',
            defaultValue: '', readOnly: true
          },
          {
            type: 'key-value', labelName: 'Labels', name: 'labels',
            defaultValue: []
          },
          {
            type: 'key-value', labelName: 'Annotations', name: 'annotations',
            defaultValue: []
          }
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'k8s',
    entityName: 'namespace',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
      {
        type: 'label',
        text: 'CAUTION: The role "{{name}}" is also going to be deleted.'
      }
    ],
    submitButtonLabel: 'Delete'
  }
]

export default K8S_NAMESPACE_TEMPLATE;
