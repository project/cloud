import EntityFormTemplate from 'model/EntityFormTemplate';

const K8S_OTHER_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'k8s',
    entityName: '',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: '',
        keyValueRecords: [
          {
            type: 'join', labelName: 'Namespace', name: 'namespace', info: {
              entityTypeId: 'k8s_namespace',
              keyColumn: 'name'
            }, defaultValue: '', required: true
          },
          {
            type: 'textarea', labelName: 'Detail', name: 'detail',
            defaultValue: '', required: true
          }
        ],
      },
    ]
  },
  {
    cloudServiceProvider: 'k8s',
    entityName: '',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: '',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Name', name: 'name',
            defaultValue: '', readOnly: true
          },
          {
            type: 'join', labelName: 'Namespace', name: 'namespace', info: {
              entityTypeId: 'k8s_namespace',
              keyColumn: 'name'
            }, defaultValue: '', readOnly: true
          },
          {
            type: 'textarea', labelName: 'Detail', name: 'detail',
            defaultValue: '', required: true
          }
        ],
      },
    ]
  },
  {
    cloudServiceProvider: 'k8s',
    entityName: '',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  }
]

export default K8S_OTHER_TEMPLATE;
