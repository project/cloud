import EntityFormTemplate from 'model/EntityFormTemplate';

const K8S_POD_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'k8s',
    entityName: 'pod',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Pod',
        keyValueRecords: [
          {
            type: 'join', labelName: 'Namespace', name: 'namespace', info: {
              entityTypeId: 'k8s_namespace',
              keyColumn: 'name'
            }, defaultValue: '', required: true
          },
          {
            type: 'textarea', labelName: 'Detail', name: 'detail', defaultValue: ''
          },
          {
            type: 'default', labelName: 'YAML URL', name: 'yaml_url', defaultValue: ''
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Time scheduler',
        keyValueRecords: [
          {
            type: 'boolean', labelName: 'Enable time scheduler', name: 'enableTimeScheduler',
            defaultValue: false
          },
          {
            type: 'radio', labelName: 'Scheduler Use Type', name: 'time_scheduler_option',
            value: [
              { labelName: 'Use Cloud Orchestrator', name: 'cloud_orchestrator_scheduler' },
              { labelName: 'Use CronJob', name: 'cronjob_scheduler' }
            ], defaultValue: 'cloud_orchestrator_scheduler',
            orientation: 'vertical'
          },
          {
            type: 'time', labelName: 'Start-up Time',
            hourName: 'startHour', minuteName: 'startMinute', defaultValue: ['6', '0']
          },
          {
            type: 'time', labelName: 'Stop Time',
            hourName: 'stopHour', minuteName: 'stopMinute', defaultValue: ['18', '0']
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'k8s',
    entityName: 'pod',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Pod',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Name', name: 'name', defaultValue: '', readOnly: true
          },
          {
            type: 'default', labelName: 'Namespace', name: 'namespace', defaultValue: '', readOnly: true
          },
          {
            type: 'textarea', labelName: 'Detail', name: 'detail', defaultValue: ''
          },
        ]
      }
    ]
  }
]

export default K8S_POD_TEMPLATE;
