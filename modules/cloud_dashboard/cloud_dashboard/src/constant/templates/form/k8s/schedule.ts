import EntityFormTemplate from 'model/EntityFormTemplate';

const K8S_SCHEDULE_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'k8s',
    entityName: 'schedule',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Schedule',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Name', name: 'name', defaultValue: '', readOnly: true
          },
          {
            type: 'default', labelName: 'Namespace name', name: 'namespace_name', defaultValue: '', readOnly: true
          },
          {
            type: 'default', labelName: 'Resource name', name: 'resource_name', defaultValue: '', readOnly: true
          },
          {
            type: 'default', labelName: 'Launch template name', name: 'launch_template_name', defaultValue: 'N/A', readOnly: true
          },
          {
            type: 'time', labelName: 'Start Time:',
            hourName: 'startHour', minuteName: 'startMinute', defaultValue: ['6', '0']
          },
          {
            type: 'time', labelName: 'Stop Time:',
            hourName: 'stopHour', minuteName: 'stopMinute', defaultValue: ['18', '0']
          },
        ]
      }
    ]
  }
]

export default K8S_SCHEDULE_TEMPLATE;
