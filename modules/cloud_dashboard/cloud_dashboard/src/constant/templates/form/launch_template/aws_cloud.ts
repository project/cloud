import EntityFormTemplate from 'model/EntityFormTemplate';

const AWS_CLOUD_SERVER_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'cloud_launch_template',
    actionType: 'copy',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Instance',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Name', name: 'name',
            defaultValue: '', required: true
          },
          {
            type: 'textarea', labelName: 'Description', name: 'field_description',
            defaultValue: ''
          },
          {
            type: 'select', labelName: 'Instance type', name: 'field_instance_type',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/instance_types',
            defaultValue: '', readOnly: true
          },
          {
            type: 'select', labelName: 'IAM role', name: 'field_iam_role',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/iam_role_options',
            defaultValue: ''
          },
          {
            type: 'number', labelName: 'Min count', name: 'field_min_count',
            defaultValue: 1, required: true
          },
          {
            type: 'number', labelName: 'Max count', name: 'field_max_count',
            defaultValue: 1
          },
          {
            type: 'boolean', labelName: 'Test only', name: 'field_test_only',
            defaultValue: false
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'AMI',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Image ID', name: 'field_image_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/cloud_launch_template/image_options',
            defaultValue: '', required: true
          },
          {
            type: 'default', labelName: 'Kernel ID', name: 'field_kernel_id',
            defaultValue: '',
          },
          {
            type: 'default', labelName: 'Ramdisk ID', name: 'field_ram',
            defaultValue: '',
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Availability Zone', name: 'field_availability_zone',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/availability_zones',
            defaultValue: '', required: true
          },
          {
            labelName: 'VPC', 'name': 'field_vpc', type: 'join', info: {
              entityTypeId: 'aws_cloud_vpc',
              keyColumn: 'vpc_id',
              label: '{name} ({vpc_id} | {cidr_block})',
            }, defaultValue: '',
          },
          {
            type: 'select', labelName: 'Subnet', name: 'field_subnet',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_network_interface/subnet_options',
            defaultValue: ''
          },
          {
            type: 'multi-select', labelName: 'Security groups', name: 'security_groups',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/security_group_options',
            defaultValue: ["{tryNotNull(getValue(aws_cloud_security_group,relationship(field_security_group,drupal_internal__target_id),drupal_internal__id,name),'')}"]
          },
          {
            type: 'join', labelName: 'SSH key', name: 'ssh_key', info: {
              entityTypeId: 'aws_cloud_key_pair',
              keyColumn: 'key_pair_name',
              label: '{key_pair_name}'
            },
            defaultValue: "{tryNotNull(getValue(aws_cloud_key_pair,relationship(field_ssh_key,drupal_internal__target_id),drupal_internal__id,key_pair_name),'')}"
          },
          {
            type: 'select', labelName: 'Network interface', name: 'network_interface_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_elastic_ip/{entity_id}/unassociated_network_interface_ids',
            defaultValue: "{tryNotNull(relationship(field_security_group,drupal_internal__target_id), '')}"
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          {
            type: 'key-value', labelName: 'Tags', name: 'field_tags',
            defaultValue: []
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Workflow',
        keyValueRecords: [
          { type: 'select-local', name: 'field_workflow_status', labelName: 'Status', defaultValue: 'Draft', value: [
            { name: 'Draft', labelName: 'Draft' },
            { name: 'Review', labelName: 'Review' },
            { name: 'Approved', labelName: 'Approved' },
          ], required: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Option',
        keyValueRecords: [
          { type: 'select-local', name: 'field_instance_shutdown_behavior', labelName: 'Instance shutdown behavior', defaultValue: 'stop', value: [
            { name: 'stop', labelName: 'Stop' },
            { name: 'terminate', labelName: 'Terminate' },
          ]},
          {
            type: 'boolean', labelName: 'Termination protection', name: 'field_termination_protection',
            defaultValue: false
          },
          {
            type: 'boolean', labelName: 'Monitoring', name: 'field_monitoring',
            defaultValue: false
          },
          {
            type: 'select', labelName: 'Schedule', name: 'field_schedule',
            url: '/cloud_dashboard/schedule_options',
            defaultValue: '',
          },
          {
            type: 'textarea', labelName: 'User data', name: 'field_user_data',
            defaultValue: ''
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Others',
        keyValueRecords: [
          {
            type: 'textarea', labelName: 'Revision log message', name: 'revision_log_message',
            defaultValue: ''
          },
        ]
      },
    ],
    submitButtonLabel: 'Copy',
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'server_template',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Instance',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'edit-name', class: 'form-item-name' },
          { type: 'textarea', labelName: 'Description', name: 'description', defaultValue: '', id: 'edit-description', class: 'form-item-description' },
          {
            type: 'select', labelName: 'Instance type', name: 'instance_type',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/instance_types',
            defaultValue: '', id: 'edit-instance-type', class: 'form-item-instance-type'
          },
          {
            type: 'number', labelName: 'Min count', name: 'field_min_count',
            defaultValue: 1, required: true, id: 'edit-field-min-count', class: 'form-item-field-min-count-0-value'
          },
          {
            type: 'number', labelName: 'Max count', name: 'field_max_count',
            defaultValue: 1, id: 'edit-field-max-count-0-value', class: 'form-item-field-max-count-0-value'
          },
          {
            type: 'boolean', labelName: 'Test only', name: 'field_test_only',
            defaultValue: false, id: 'edit-field-test-only-value', class: 'form-item-field-test-only-value'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'AMI',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Image ID', name: 'field_image_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/cloud_launch_template/image_options',
            defaultValue: '', required: true
          },
          {
            type: 'default', labelName: 'Kernel ID', name: 'field_kernel_id',
            defaultValue: '',
          },
          {
            type: 'default', labelName: 'Ramdisk ID', name: 'field_ram',
            defaultValue: '',
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Availability Zone', name: 'field_availability_zone',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/availability_zones',
            defaultValue: '', required: true
          },
          {
            labelName: 'VPC', 'name': 'field_vpc', type: 'join', info: {
              entityTypeId: 'aws_cloud_vpc',
              keyColumn: 'vpc_id',
              label: '{name} ({vpc_id} | {cidr_block})',
            }, defaultValue: '', required: true
          },
          {
            type: 'select', labelName: 'Subnet', name: 'field_subnet',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_network_interface/subnet_options',
            defaultValue: '', required: true
          },
          {
            type: 'multi-select', labelName: 'Security groups', name: 'security_groups',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/security_group_options',
            defaultValue: [],
            required: true
          },
          {
            type: 'join', labelName: 'SSH key', name: 'ssh_key', info: {
              entityTypeId: 'aws_cloud_key_pair',
              keyColumn: 'key_pair_name',
              label: '{key_pair_name}'
            },
            defaultValue: '',
            required: true
          },
          {
            type: 'select', labelName: 'Network interface', name: 'network_interface_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/cloud_launch_template/available_network_interface_ids',
            defaultValue: ''
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          {
            type: 'key-value', labelName: 'Tags', name: 'field_tags',
            defaultValue: []
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Workflow',
        keyValueRecords: [
          { type: 'select-local', name: 'field_workflow_status', labelName: 'Status', defaultValue: 'Draft', value: [
            { name: 'Draft', labelName: 'Draft' },
            { name: 'Review', labelName: 'Review' },
            { name: 'Approved', labelName: 'Approved' },
          ], required: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Option',
        keyValueRecords: [
          { type: 'select-local', name: 'field_instance_shutdown_behavior', labelName: 'Instance shutdown behavior', defaultValue: 'stop', value: [
            { name: 'stop', labelName: 'Stop' },
            { name: 'terminate', labelName: 'Terminate' },
          ]},
          {
            type: 'boolean', labelName: 'Termination protection', name: 'field_termination_protection',
            defaultValue: false
          },
          {
            type: 'boolean', labelName: 'Monitoring', name: 'field_monitoring',
            defaultValue: false
          },
          {
            type: 'select', labelName: 'Schedule', name: 'field_schedule',
            url: '/cloud_dashboard/schedule_options',
            defaultValue: '',
          },
          {
            type: 'textarea', labelName: 'User data', name: 'field_user_data',
            defaultValue: ''
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Others',
        keyValueRecords: [
          {
            type: 'textarea', labelName: 'Revision log message', name: 'revision_log_message',
            defaultValue: ''
          },
        ]
      },
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'cloud_launch_template',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'cloud_launch_template',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Instance',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Name', name: 'name',
            defaultValue: '', readOnly: true, required: true
          },
          {
            type: 'textarea', labelName: 'Description', name: 'field_description',
            defaultValue: ''
          },
          {
            type: 'select', labelName: 'Instance type', name: 'field_instance_type',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/instance_types',
            defaultValue: '', required: true
          },
          {
            type: 'select', labelName: 'IAM role', name: 'field_iam_role',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/iam_role_options',
            defaultValue: ''
          },
          {
            type: 'number', labelName: 'Min count', name: 'field_min_count',
            defaultValue: 1, required: true
          },
          {
            type: 'number', labelName: 'Max count', name: 'field_max_count',
            defaultValue: 1
          },
          {
            type: 'boolean', labelName: 'Test only', name: 'field_test_only',
            defaultValue: false
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'AMI',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Image ID', name: 'field_image_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/cloud_launch_template/image_options',
            defaultValue: '', required: true
          },
          {
            type: 'default', labelName: 'Kernel ID', name: 'field_kernel_id',
            defaultValue: '',
          },
          {
            type: 'default', labelName: 'Ramdisk ID', name: 'field_ram',
            defaultValue: '',
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Availability Zone', name: 'field_availability_zone',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/availability_zones',
            defaultValue: '', required: true
          },
          {
            labelName: 'VPC', 'name': 'field_vpc', type: 'join', info: {
              entityTypeId: 'aws_cloud_vpc',
              keyColumn: 'vpc_id',
              label: '{name} ({vpc_id} | {cidr_block})',
            }, defaultValue: '', required: true
          },
          {
            type: 'select', labelName: 'Subnet', name: 'field_subnet',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/aws_cloud_network_interface/subnet_options',
            defaultValue: '', required: true
          },
          {
            type: 'multi-select', labelName: 'Security groups', name: 'security_groups',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/security_group_options',
            defaultValue: ["{tryNotNull(getValue(aws_cloud_security_group,relationship(field_security_group,drupal_internal__target_id),drupal_internal__id,name),'')}"],
            required: true
          },
          {
            type: 'select', labelName: 'SSH key', name: 'ssh_key',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/key_pair_options',
            defaultValue: "{tryNotNull(relationship(field_ssh_key,drupal_internal__target_id), '_none')}",
            required: true
          },
          {
            type: 'select', labelName: 'Network interface', name: 'network_interface_id',
            url: '/cloud_dashboard/aws_cloud/{cloud_context}/cloud_launch_template/available_network_interface_ids',
            defaultValue: "{tryNotNull(relationship(field_network,drupal_internal__target_id), '')}"
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          {
            type: 'key-value', labelName: 'Tags', name: 'field_tags',
            defaultValue: []
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Workflow',
        keyValueRecords: [
          { type: 'select-local', name: 'field_workflow_status', labelName: 'Status', defaultValue: 'Draft', value: [
            { name: 'Draft', labelName: 'Draft' },
            { name: 'Review', labelName: 'Review' },
            { name: 'Approved', labelName: 'Approved' },
          ], required: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Option',
        keyValueRecords: [
          { type: 'select-local', name: 'field_instance_shutdown_behavior', labelName: 'Instance shutdown behavior', defaultValue: 'stop', value: [
            { name: 'stop', labelName: 'Stop' },
            { name: 'terminate', labelName: 'Terminate' },
          ]},
          {
            type: 'boolean', labelName: 'Termination protection', name: 'field_termination_protection',
            defaultValue: false
          },
          {
            type: 'boolean', labelName: 'Monitoring', name: 'field_monitoring',
            defaultValue: false
          },
          {
            type: 'select', labelName: 'Schedule', name: 'field_schedule',
            url: '/cloud_dashboard/schedule_options',
            defaultValue: '',
          },
          {
            type: 'textarea', labelName: 'User data', name: 'field_user_data',
            defaultValue: ''
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Others',
        keyValueRecords: [
          {
            type: 'textarea', labelName: 'Revision log message', name: 'revision_log_message',
            defaultValue: ''
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'aws_cloud',
    entityName: 'cloud_launch_template',
    actionType: 'review',
    entityRecords: [
      {
        type: 'label',
        text: 'Workflow status will be changed from Draft to Review.'
      },
    ],
    submitButtonLabel: 'Review'
  },
];

export default AWS_CLOUD_SERVER_TEMPLATE;
