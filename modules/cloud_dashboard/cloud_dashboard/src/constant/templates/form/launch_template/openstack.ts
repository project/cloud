import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_SERVER_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'cloud_launch_template',
    actionType: 'copy',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Instance',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Name', name: 'name',
            defaultValue: 'copy_of_{name}', required: true
          },
          {
            type: 'textarea', labelName: 'Description', name: 'field_description',
            defaultValue: '', id: 'field--name-field-description', class: 'field--name-field-description'
          },
          {
            type: 'select-table', labelName: 'Flavor', name: 'field_flavor',
            url: '/cloud_dashboard/openstack/{cloud_context}/flavor_table_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_flavor,drupal_internal__target_id), '_none')}",
            required: true,
            id: 'edit-field-flavor',
            class: 'form-item-field-flavor',
            recordKey: 'id',
            sortKey: 'name',
            column: [
              { labelName: 'Name', name: 'name' },
              { labelName: 'vCPUs', name: 'vcpus' },
              { labelName: 'RAM', name: 'ram' },
            ]
          },
          {
            type: 'boolean', labelName: 'Test only', name: 'field_test_only',
            defaultValue: false
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Image',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Image ID', name: 'field_openstack_image_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/cloud_launch_template/image_options',
            defaultValue: '', required: true
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Availability Zone', name: 'field_os_availability_zone',
            url: '/cloud_dashboard/openstack/{cloud_context}/availability_zones',
            defaultValue: '', required: true
          },
          {
            type: 'multi-check', labelName: 'Security groups', name: 'security_groups',
            url: '/cloud_dashboard/openstack/{cloud_context}/security_group_options',
            defaultValue: ["{tryNotNull(getValue(openstack_security_group,relationship(field_openstack_security_group,drupal_internal__target_id),drupal_internal__id,drupal_internal__id),'')}"],
            id: 'field--name-security-groups', class: 'field--name-security-groups',
            keyType: 'value',
          },
          {
            type: 'select', labelName: 'SSH key', name: 'ssh_key',
            url: '/cloud_dashboard/openstack/{cloud_context}/key_pair_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_ssh_key,drupal_internal__target_id),'_none')}",
            required: true
          },
          {
            type: 'select', labelName: 'Network', name: 'network_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/network_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_network,drupal_internal__target_id), '_none')}",
            id: 'field--name-field-openstack-network', class: 'field--name-field-openstack-network',
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Server group',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Server group name', name: 'field_openstack_server_group',
            url: '/cloud_dashboard/openstack/{cloud_context}/server_group_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_server_group,drupal_internal__target_id), '_none')}",
            id: 'field--name-field-openstack-server-group', class: 'field--name-field-openstack-server-group'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          {
            type: 'key-value', labelName: 'Tags', name: 'field_tags',
            defaultValue: []
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Others',
        keyValueRecords: [
          {
            type: 'textarea', labelName: 'Revision log message', name: 'revision_log_message',
            defaultValue: ''
          },
        ]
      },
    ],
    submitButtonLabel: 'Copy'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'server_template',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Instance',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', required: true, defaultValue: '', id: 'field--name-name', class: 'field--name-name' },
          { type: 'textarea', labelName: 'Description', name: 'description', defaultValue: '', id: 'field--name-field-description', class: 'form-item-description' },
          {
            type: 'select-table', labelName: 'Flavor', name: 'field_flavor',
            url: '/cloud_dashboard/openstack/{cloud_context}/flavor_table_options',
            defaultValue: '', required: true,
            id: 'edit-field-flavor',
            class: 'form-item-field-flavor',
            recordKey: 'id',
            sortKey: 'name',
            column: [
              { labelName: 'Name', name: 'name' },
              { labelName: 'vCPUs', name: 'vcpus' },
              { labelName: 'RAM', name: 'ram' },
            ]
          },
          {
            type: 'boolean', labelName: 'Test only', name: 'field_test_only',
            defaultValue: false, id: 'edit-field-test-only-value', class: 'form-item-field-test-only-value'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Image',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Image ID', name: 'field_openstack_image_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/cloud_launch_template/image_options',
            defaultValue: '', required: true, id: 'field--name-field-openstack-image-id', class: 'field--name-field-openstack-image-id'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Availability Zone', name: 'field_os_availability_zone',
            url: '/cloud_dashboard/openstack/{cloud_context}/availability_zones',
            defaultValue: '', required: true, id: 'field--name-field-os-availability-zone', class: 'field--name-field-os-availability-zone'
          },
          {
            type: 'multi-check', labelName: 'Security groups', name: 'security_groups',
            url: '/cloud_dashboard/openstack/{cloud_context}/security_group_options',
            defaultValue: [],
            id: 'field--name-security-groups', class: 'field--name-security-groups',
            keyType: 'value',
          },
          {
            type: 'select', labelName: 'SSH key', name: 'ssh_key',
            url: '/cloud_dashboard/openstack/{cloud_context}/key_pair_options',
            defaultValue: '', id: 'field--name-field-openstack-ssh-key', class: 'field--name-field-openstack-ssh-key'
          },
          {
            type: 'select', labelName: 'Network', name: 'network_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/network_options',
            defaultValue: '', id: 'field--name-field-openstack-network', class: 'field--name-field-openstack-network'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Server group',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Server group name', name: 'field_openstack_server_group',
            url: '/cloud_dashboard/openstack/{cloud_context}/server_group_options',
            defaultValue: '', id: 'field--name-field-openstack-server-group', class: 'field--name-field-openstack-server-group'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          {
            type: 'key-value', labelName: 'Tags', name: 'field_tags',
            defaultValue: []
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Options',
        keyValueRecords: [
          { type: 'boolean', labelName: 'Termination protection', name: 'termination_protection', defaultValue: false, id: 'edit-termination-protection', class: 'form-item-termination-protection' },
          {
            type: 'textarea', labelName: 'Customization script', name: 'field_user_data',
            defaultValue: '', id: 'edit-field-user-data', class: 'form-item-field-user-data'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Others',
        keyValueRecords: [
          {
            type: 'textarea', labelName: 'Revision log message', name: 'revision_log_message',
            defaultValue: ''
          },
        ]
      },
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'cloud_launch_template',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'cloud_launch_template',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Instance',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Name', name: 'name',
            defaultValue: '', readOnly: true, required: true
          },
          {
            type: 'textarea', labelName: 'Description', name: 'field_description',
            defaultValue: '', id: 'field--name-field-description', class: 'field--name-field-description'
          },
          {
            type: 'select-table', labelName: 'Flavor', name: 'field_flavor',
            url: '/cloud_dashboard/openstack/{cloud_context}/flavor_table_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_flavor,drupal_internal__target_id), '_none')}",
            required: true,
            id: 'field--name-field-flavor',
            class: 'field--name-field-flavor',
            recordKey: 'id',
            sortKey: 'name',
            column: [
              { labelName: 'Name', name: 'name' },
              { labelName: 'vCPUs', name: 'vcpus' },
              { labelName: 'RAM', name: 'ram' },
            ]
          },
          {
            type: 'boolean', labelName: 'Test only', name: 'field_test_only',
            defaultValue: false
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'AMI',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Image ID', name: 'field_openstack_image_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/cloud_launch_template/image_options',
            defaultValue: '', required: true
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Availability Zone', name: 'field_os_availability_zone',
            url: '/cloud_dashboard/openstack/{cloud_context}/availability_zones',
            defaultValue: '', required: true
          },
          {
            type: 'multi-check', labelName: 'Security groups', name: 'security_groups',
            url: '/cloud_dashboard/openstack/{cloud_context}/security_group_options',
            defaultValue: ["{tryNotNull(getValue(openstack_security_group,relationship(field_openstack_security_group,drupal_internal__target_id),drupal_internal__id,drupal_internal__id),'')}"],
            required: true,
            id: 'field--name-security-groups', class: 'field--name-security-groups',
            keyType: 'value',
          },
          {
            type: 'select', labelName: 'SSH key', name: 'ssh_key',
            url: '/cloud_dashboard/openstack/{cloud_context}/key_pair_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_ssh_key,drupal_internal__target_id), '_none')}",
            required: true
          },
          {
            type: 'select', labelName: 'Network', name: 'network_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/network_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_network,drupal_internal__target_id), '_none')}",
            id: 'field--name-field-openstack-network', class: 'field--name-field-openstack-network'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Server group',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Server group name', name: 'field_openstack_server_group',
            url: '/cloud_dashboard/openstack/{cloud_context}/server_group_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_server_group,drupal_internal__target_id), '_none')}",
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          {
            type: 'key-value', labelName: 'Tags', name: 'field_tags',
            defaultValue: []
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Others',
        keyValueRecords: [
          {
            type: 'textarea', labelName: 'Revision log message', name: 'revision_log_message',
            defaultValue: ''
          },
        ]
      },
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'cloud_launch_template',
    actionType: 'launch',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Cost',
        keyValueRecords: [
          { type: 'table-with-sort',
          url: '/cloud_dashboard/openstack/{cloud_context}/flavor_table_options',
          sortKey: 'name',
          entityName: 'flavor',
          column: [
            { labelName: 'Name', name: 'name', type: 'default' },
            { labelName: 'vCPUs', name: 'vcpus', type: 'default' },
            { labelName: 'RAM', name: 'ram', type: 'storage_mb' },
            { labelName: 'Root Disk', name: 'disk', type: 'storage_gb' },
            { labelName: 'Ephemeral Disk', name: 'ephemeral', type: 'storage_gb' },
            { labelName: 'Swap Disk', name: 'swap', type: 'storage_mb' },
            { labelName: 'RX/TX Factor', name: 'rxtx_factor', type: 'float_number' },
            { labelName: 'Public', name: 'is_public', type: 'boolean', value: ['Yes', 'No'] },
          ] }
        ]
      },
      {
        type: 'panel',
        panelName: 'Automation',
        keyValueRecords: [
          { type: 'boolean', labelName: 'Termination protection', name: 'termination_protection', defaultValue: false, id: 'edit-termination-protection', class: 'form-item-termination-protection' },
        ]
      },
      {
        type: 'panel',
        panelName: 'Instance',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Name', name: 'name',
            defaultValue: '', readOnly: true, required: true
          },
          {
            type: 'select-table', labelName: 'Flavor', name: 'field_flavor',
            url: '/cloud_dashboard/openstack/{cloud_context}/flavor_table_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_flavor,drupal_internal__target_id), '_none')}",
            required: true,
            id: 'edit-field-flavor',
            class: 'form-item-field-flavor',
            recordKey: 'id',
            sortKey: 'name',
            column: [
              { labelName: 'Name', name: 'name' },
              { labelName: 'vCPUs', name: 'vcpus' },
              { labelName: 'RAM', name: 'ram' },
            ], readOnly: true
          },
          { labelName: 'Min count', name: 'field_min_count', type: 'number', defaultValue: 0, readOnly: true },
          { labelName: 'Max count', name: 'field_max_count', type: 'number', defaultValue: 0, readOnly: true },
          {
            type: 'boolean', labelName: 'Test only', name: 'field_test_only',
            defaultValue: false, readOnly: true, labels: ['On', 'Off']
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Image',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Image ID', name: 'field_openstack_image_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/cloud_launch_template/image_options',
            defaultValue: '', required: true, readOnly: true
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Availability Zone', name: 'field_os_availability_zone',
            url: '/cloud_dashboard/openstack/{cloud_context}/availability_zones',
            defaultValue: '', readOnly: true
          },
          {
            type: 'multi-check', labelName: 'Security groups', name: 'security_groups',
            url: '/cloud_dashboard/openstack/{cloud_context}/security_group_options',
            defaultValue: ["{tryNotNull(getValue(openstack_security_group,relationship(field_openstack_security_group,drupal_internal__target_id),drupal_internal__id,drupal_internal__id),'')}"],
            readOnly: true,
            id: 'field--name-security-groups', class: 'field--name-security-groups',
            keyType: 'value',
          },
          {
            type: 'select', labelName: 'SSH key', name: 'ssh_key',
            url: '/cloud_dashboard/openstack/{cloud_context}/key_pair_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_ssh_key,drupal_internal__target_id), '_none')}",
            readOnly: true
          },
          {
            type: 'select', labelName: 'Network', name: 'network_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/network_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_network,drupal_internal__target_id), '_none')}",
            readOnly: true
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Server group',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Server group name', name: 'field_openstack_server_group',
            url: '/cloud_dashboard/openstack/{cloud_context}/server_group_options',
            defaultValue: "{tryNotNull(relationship(field_openstack_server_group,drupal_internal__target_id), '_none')}",
            readOnly: true
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          {
            type: 'key-value', labelName: 'Tags', name: 'field_tags',
            defaultValue: [], readOnly: true,
            id: 'field--name-field-tags', class: 'form-item-tags'
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Options',
        keyValueRecords: [
          { labelName: 'Instance shutdown behavior', name: 'field_instance_shutdown_behavior', type: 'default', defaultValue: '', readOnly: true },
          { labelName: 'Termination protection', name: 'field_termination_protection', type: 'boolean', defaultValue: false, labels: ['On', 'Off'], readOnly: true },
          { labelName: 'Monitoring', name: 'field_monitoring', type: 'boolean', defaultValue: false,labels: ['On', 'Off'], readOnly: true },
        ],
      },
    ],
    submitButtonLabel: 'Launch'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'cloud_launch_template',
    actionType: 'review',
    entityRecords: [
      {
        type: 'label',
        text: 'Workflow status will be changed from Draft to Review.'
      },
    ],
    submitButtonLabel: 'Review'
  },
];

export default OPENSTACK_SERVER_TEMPLATE;
