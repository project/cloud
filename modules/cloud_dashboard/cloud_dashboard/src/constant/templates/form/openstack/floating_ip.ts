import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_FLOATING_IP_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'floating_ip',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Floating IP',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'field--name-name', class: 'field--name-name' },
          { type: 'select', labelName: 'Floating network ID', name: 'floating_network_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/floating_network_ids',
            defaultValue: '', required: true, id: 'form-item-floating-network-id', class: 'form-item-floating-network-id'
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'floating_ip',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Floating IP',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'field--name-name', class: 'field--name-name' },
          { type: 'default', labelName: 'Type', name: 'elastic_ip_type', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Floating IP', name: 'public_ip', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Private IP address', name: 'private_ip_address', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Assign',
        keyValueRecords: [
          {
            type: 'join', labelName: 'Instance ID', name: 'instance_id', info: {
              entityTypeId: 'openstack_instance',
              keyColumn: 'instance_id',
              label: 'name',
            }, defaultValue: '', readOnly: true
          },
          {
            type: 'join', labelName: 'Network ID', name: 'network_id', info: {
              entityTypeId: 'openstack_network',
              keyColumn: 'network_id',
              label: 'name'
            }, defaultValue: '', readOnly: true
          },
          { type: 'default', labelName: 'Allocation ID', name: 'allocation_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Association ID', name: 'association_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Network owner', name: 'network_owner', defaultValue: '', readOnly: true },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'floating_ip',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'floating_ip',
    actionType: 'associate',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Select the instance OR port to which you want to associate this Floating IP address',
        keyValueRecords: [
          { type: 'select', labelName: 'Port to be associated', name: 'port_id_ip',
            url: '/cloud_dashboard/openstack/{cloud_context}/openstack_floating_ip/{entity_id}/associated_port_ids',
            defaultValue: '', id: 'form-item-port-id-ip', class: 'form-item-port-id-ip'
          },
        ]
      }
    ],
    submitButtonLabel: 'Associate Address'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'floating_ip',
    actionType: 'disassociate',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to disassociate this {{entityName}}({{name}})?'
      },
      {
        type: 'panel',
        panelName: 'Floating IP Information',
        keyValueRecords: [
          {
            type: 'join', labelName: 'Instance ID', name: 'instance_id', info: {
              entityTypeId: 'openstack_instance',
              keyColumn: 'instance_id',
              label: 'name',
            }, defaultValue: '', readOnly: true
          },
          {
            type: 'join', labelName: 'Network ID', name: 'network_id', info: {
              entityTypeId: 'openstack_network',
              keyColumn: 'network_id',
              label: 'name',
            }, defaultValue: '', readOnly: true
          }
        ]
      }
    ],
    submitButtonLabel: 'Disassociate Address'
  },
]

export default OPENSTACK_FLOATING_IP_TEMPLATE;
