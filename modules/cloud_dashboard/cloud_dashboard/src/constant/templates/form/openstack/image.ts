import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_IMAGE_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'image',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Image',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'field--name-name', class: 'field--name-name' },
          { type: 'radio', labelName: 'Visibility', name: 'image_visibility', defaultValue: '0',
            value: [
              { labelName: 'Private', name: '0' },
              { labelName: 'Public', name: '1' },
              { labelName: 'Shared', name: '2' },
            ], orientation: 'horizontal' },
          { type: 'image-shared-projects', labelName: 'Shared projects', name: 'shared_projects', defaultValue: [] },
        ]
      },
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'image',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Image',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'default', labelName: 'Image ID', name: 'image_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Owner', name: 'account_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Source', name: 'source', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Status', name: 'status', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'State Reason', name: 'state_reason', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true },
          { type: 'radio', labelName: 'Visibility', name: 'image_visibility', defaultValue: '0',
            value: [
              { labelName: 'Private', name: '0' },
              { labelName: 'Public', name: '1' },
              { labelName: 'Shared', name: '2' },
            ], orientation: 'horizontal' },
          { type: 'image-shared-projects', labelName: 'Shared projects', name: 'shared_projects', defaultValue: [] },
        ]
      },
      {
        type: 'panel',
        panelName: 'Type',
        keyValueRecords: [
          { type: 'default', labelName: 'Platform', name: 'platform', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Architecture', name: 'architecture', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Visualization Type', name: 'visualization_type', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Product Code', name: 'product_code', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Image type', name: 'image_type', defaultValue: '', readOnly: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Device',
        keyValueRecords: [
          { type: 'default', labelName: 'Root device name', name: 'root_device_name', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Root device type', name: 'root_device_type', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Kernel ID', name: 'kernel_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Ramdisk ID', name: 'ramdisk_id', defaultValue: '', readOnly: true },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'image',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  }
]

export default OPENSTACK_IMAGE_TEMPLATE;
