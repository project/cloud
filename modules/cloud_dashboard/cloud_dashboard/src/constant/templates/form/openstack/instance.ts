import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_INSTANCE_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'instance',
    actionType: 'create_image',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Image',
        keyValueRecords: [
          { type: 'default', labelName: 'Image name', name: 'image_name', defaultValue: '', required: true, id: 'form-item-image-name', class: 'form-item-image-name' },
        ]
      },
    ],
    submitButtonLabel: 'Create image',
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'instance',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Instance',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { labelName: 'Instance ID', name: 'instance_id', type: 'default', defaultValue: '', readOnly: true },
          { labelName: 'Instance state', name: 'instance_state', type: 'default', defaultValue: '', readOnly: true },
          { labelName: 'AMI image', name: 'image_id', type: 'default', defaultValue: '', readOnly: true },
          { labelName: 'AWS account ID', name: 'account_id', type: 'default', defaultValue: '', readOnly: true },
          { labelName: 'Launch time', name: 'launch_time', type: 'datetime', defaultValue: 0, readOnly: true },
          { labelName: 'Created', name: 'created', type: 'datetime', defaultValue: 0, readOnly: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          { labelName: 'Private IPs', name: 'private_ips', type: 'default', defaultValue: '', readOnly: true },
          { labelName: 'Public DNS', name: 'public_dns', type: 'default', defaultValue: '', readOnly: true },
          {
            labelName: 'Key pair name', name: 'key_pair_name', type: 'join', info: {
              entityTypeId: 'openstack_key_pair',
              keyColumn: 'key_pair_name',
            }, defaultValue: '', readOnly: true
          },
          {
            type: 'multi-check', labelName: 'Security groups', name: 'security_groups',
            url: '/cloud_dashboard/openstack/{cloud_context}/security_groups',
            defaultValue: [],
            id: 'field--name-field-openstack-security-group',
            class: 'field--name-field-openstack-security-group',
            required: true,
            keyType: 'label',
          },
          { labelName: 'Availability Zone', name: 'availability_zone', type: 'default', defaultValue: '', readOnly: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Storage',
        keyValueRecords: [
          { labelName: 'Volume', name: 'block_devices', type: 'default', defaultValue: '', readOnly: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Tags',
        keyValueRecords: [
          {
            type: 'key-value', labelName: 'Tags', name: 'tags',
            defaultValue: []
          },
        ]
      },
      {
        type: 'panel',
        panelName: 'Option',
        keyValueRecords: [
          { labelName: 'Termination protection', name: 'termination_protection', type: 'boolean', labels: ['On', 'Off'], defaultValue: false },
        ]
      },
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'instance',
    actionType: 'terminate',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete | Terminate'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'instance',
    actionType: 'start',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to start the {{name}} {{entityName}}?'
      },
    ],
    submitButtonLabel: 'Start'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'instance',
    actionType: 'stop',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to stop the {{name}} {{entityName}}?'
      },
    ],
    submitButtonLabel: 'Stop'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'instance',
    actionType: 'reboot',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to reboot the {{name}} {{entityName}}?'
      },
      {
        type: 'panel',
        panelName: 'Reboot option',
        keyValueRecords: [
          { labelName: 'Soft reboot', name: 'type', type: 'boolean', defaultValue: false, id: 'form-item-type', class: 'form-item-type' }
        ]
      }
    ],
    submitButtonLabel: 'Reboot'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'instance',
    actionType: 'attach_network',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Options',
        keyValueRecords: [
          {
            type: 'select-local', labelName: 'Attach Type', name: 'type', defaultValue: 'network', value: [
              { labelName: 'Network', name: 'network' },
              { labelName: 'Port', name: 'port' },
            ], required: true, id: 'edit-attach-type', class: 'form-item-attach-type'
          },
          {
            type: 'select', labelName: 'Network', name: 'network',
            url: '/cloud_dashboard/openstack/{cloud_context}/attach_networks',
            defaultValue: '',
            required: true, id: 'edit-network', class: 'form-item-network'
          },
          { type: 'default', labelName: 'Fixed IP address', name: 'fixed_ip', defaultValue: '',
            id: 'edit-fixed-ip', class: 'form-item-ip-address-or-subnet' },
          {
            type: 'select', labelName: 'Port', name: 'port',
            url: '/cloud_dashboard/openstack/{cloud_context}/attach_ports',
            defaultValue: '',
            required: true, id: 'edit-port', class: 'form-item-port'
          }
        ]
      }
    ],
    submitButtonLabel: 'Attach Interface'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'instance',
    actionType: 'detach_network',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Options',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Port', name: 'port_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/openstack_instance/{entity_id}/attached_ports',
            defaultValue: '',
            required: true,
            id: 'edit-port-id',
            class: 'form-item-port-id'
          }
        ]
      }
    ],
    submitButtonLabel: 'Detach Interface'
  }
]

export default OPENSTACK_INSTANCE_TEMPLATE;
