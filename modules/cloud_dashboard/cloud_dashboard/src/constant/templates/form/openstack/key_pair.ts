import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_KEY_PAIR_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'key_pair',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Key pair',
        keyValueRecords: [
          { type: 'default', labelName: 'Key pair name', name: 'key_pair_name', defaultValue: '', id: 'field--name-key-pair-name', class: 'field--name-key-pair-name' }
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'key_pair',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Key pair',
        keyValueRecords: [
          { type: 'default', labelName: 'Key pair name', name: 'key_pair_name', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Key pair ID', name: 'key_pair_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Fingerprint', name: 'key_fingerprint', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'key_pair',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'key_pair',
    actionType: 'import',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Key pair',
        keyValueRecords: [
          { type: 'file', labelName: 'Public Key', name: 'key_pair_public_key' },
          { type: 'default', labelName: 'Key pair name', name: 'key_pair_name', defaultValue: '', required: true },
        ]
      }
    ]
  },
]

export default OPENSTACK_KEY_PAIR_TEMPLATE;
