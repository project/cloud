import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_NETWORK_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'network',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'boolean', labelName: 'Admin state up', name: 'admin_state_up', defaultValue: true },
          { type: 'boolean', labelName: 'Shared', name: 'shared', defaultValue: false },
          { type: 'boolean', labelName: 'External network', name: 'external', defaultValue: false, id: 'form-item-external', class: 'form-item-external' },
          { type: 'multi-select', labelName: 'Availability Zone', name: 'availability_zone', id: 'field--name-availability-zone',class: 'field--name-availability-zone',
            url: '/cloud_dashboard/openstack/{cloud_context}/availability_zones?component_name=network&zone_resource=network',
            defaultValue: []
          }
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'network',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'boolean', labelName: 'Admin state up', name: 'admin_state_up', defaultValue: true },
          { type: 'boolean', labelName: 'Shared', name: 'shared', defaultValue: false },
          { type: 'boolean', labelName: 'External network', name: 'external', defaultValue: false, id: 'form-item-external', class: 'form-item-external' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'network',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
]

export default OPENSTACK_NETWORK_TEMPLATE;
