import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_PORT_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'port',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Port',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          {
            type: 'select', labelName: 'Network', name: 'network_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/networks',
            defaultValue: '',
            required: true, id: 'field--name-network-id', class: 'field--name-network-id'
          },
          { type: 'default', labelName: 'Device ID', name: 'device_id', defaultValue: '' },
          { type: 'default', labelName: 'Device owner', name: 'device_owner', defaultValue: '' },
          { type: 'select-local', name: 'ip_address_or_subnet', labelName: 'Specify IP address or subnet', defaultValue: '', required: true, value: [
            { name: 'unspecified', labelName: 'Unspecified' },
            { name: 'subnet', labelName: 'Subnet' },
            { name: 'fixed_ip', labelName: 'Fixed IP address' },
          ], id: 'form-item-ip-address-or-subnet', class: 'form-item-ip-address-or-subnet' },
          {
            type: 'select', labelName: 'Subnet', name: 'subnet',
            url: '/cloud_dashboard/openstack/{cloud_context}/subnets/{network_id}',
            defaultValue: '',
          },
          { type: 'default', labelName: 'Fixed IP address', name: 'fixed_ips', defaultValue: '', id: 'form-item-fixed-ips', class: 'form-item-fixed-ips' },
          { type: 'default', labelName: 'MAC address', name: 'mac_address', defaultValue: '' },
          {
            type: 'boolean', labelName: 'Port security', name: 'port_security_enabled',
            defaultValue: true
          },
          {
            type: 'multi-select', labelName: 'Security group', name: 'security_groups',
            url: '/cloud_dashboard/openstack/{cloud_context}/security_groups',
            defaultValue: [],
            id: 'field--name-field-openstack-security-group', class: 'field--name-field-openstack-security-group'
          },
          { type: 'select-local', name: 'binding_vnic_type', labelName: 'VNIC type', defaultValue: '', required: true, value: [
            { name: 'normal', labelName: 'Normal' },
            { name: 'direct', labelName: 'Direct' },
            { name: 'macvtap', labelName: 'MacVTap' },
            { name: 'baremetal', labelName: 'Bare Metal' },
            { name: 'virtio-forwarder', labelName: 'Virtio Forwarder' },
          ] },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'port',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Port',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          {
            type: 'boolean', labelName: 'Enable admin state', name: 'admin_state_up',
            defaultValue: false
          },
          {
            type: 'join', labelName: 'Network', name: 'network_id', info: {
              entityTypeId: 'openstack_network',
              keyColumn: 'network_id',
            }, defaultValue: '', readOnly: true
          },
          { type: 'select-local', name: 'binding_vnic_type', labelName: 'VNIC type', defaultValue: '', required: true, value: [
            { name: 'normal', labelName: 'Normal' },
            { name: 'direct', labelName: 'Direct' },
            { name: 'macvtap', labelName: 'MacVTap' },
            { name: 'baremetal', labelName: 'Bare Metal' },
            { name: 'virtio-forwarder', labelName: 'Virtio Forwarder' },
          ] },
          {
            type: 'boolean', labelName: 'Port security', name: 'port_security_enabled',
            defaultValue: true
          },
          {
            type: 'multi-select', labelName: 'Security group', name: 'security_groups',
            url: '/cloud_dashboard/openstack/{cloud_context}/security_groups',
            defaultValue: [],
          },
          { type: 'item-array', labelName: 'Allowed Address Pairs', name: 'allowed_address_pairs', defaultValue: [], info: [
            { labelName: 'IP address or CIDR', name: 'item_key', type: 'default' },
            { labelName: 'MAC address', name: 'item_value', type: 'default' },
          ], id: 'form-item-allowed-address-pairs', class: 'form-item-allowed-address-pairs' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'port',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
]

export default OPENSTACK_PORT_TEMPLATE;
