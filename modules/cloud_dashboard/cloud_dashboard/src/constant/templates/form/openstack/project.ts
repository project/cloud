import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_PROJECT_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'project',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Project',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'textarea', labelName: 'Description', name: 'description', defaultValue: '', id: 'field--name-field-description', class: 'field--name-field-description' },
          { type: 'boolean', labelName: 'Enabled', name: 'enabled', defaultValue: true },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'project',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Project',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'textarea', labelName: 'Description', name: 'description', defaultValue: '', id: 'field--name-field-description', class: 'field--name-field-description' },
          { type: 'boolean', labelName: 'Enabled', name: 'enabled', defaultValue: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'User Roles',
        keyValueRecords: [
          { type: 'item-array', labelName: 'User Roles', name: 'user_roles', defaultValue: [], info: [
            { labelName: 'User', name: 'user', type: 'join', entityTypeId: 'openstack_user' },
            { labelName: 'Roles', name: 'roles', type: 'multi-select', entityTypeId: 'openstack_role' },
            ], id: 'form-item-user-roles', class: 'form-item-user-roles' }
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'project',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  }
]

export default OPENSTACK_PROJECT_TEMPLATE;
