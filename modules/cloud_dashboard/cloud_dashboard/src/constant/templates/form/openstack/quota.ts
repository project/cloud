import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_QUOTA_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'quota',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Quota',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', readOnly: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Compute',
        keyValueRecords: [
          { type: 'number', labelName: 'Instances', name: 'instances', defaultValue: 0, id: 'form-item-instances', class: 'form-item-instances' },
          { type: 'number', labelName: 'vCPUs', name: 'cores', defaultValue: 0 },
          { type: 'number', labelName: 'RAM(MiB)', name: 'ram', defaultValue: 0 },
          { type: 'number', labelName: 'Metadata Items', name: 'metadata_items', defaultValue: 0 },
          { type: 'number', labelName: 'Key Pairs', name: 'key_pairs', defaultValue: 0 },
          { type: 'number', labelName: 'Server Groups', name: 'server_groups', defaultValue: 0 },
          { type: 'number', labelName: 'Server Group Members', name: 'server_group_members', defaultValue: 0 },
          { type: 'number', labelName: 'Injected Files', name: 'injected_files', defaultValue: 0 },
          { type: 'number', labelName: 'Injected File Content(Bytes)', name: 'injected_file_content_bytes', defaultValue: 0 },
          { type: 'number', labelName: 'Length of Injected File Path', name: 'injected_file_path_bytes', defaultValue: 0 },
        ]
      },
      {
        type: 'panel',
        panelName: 'Volume',
        keyValueRecords: [
          { type: 'number', labelName: 'Volumes', name: 'volumes', defaultValue: 0 },
          { type: 'number', labelName: 'Volume snapshots', name: 'snapshots', defaultValue: 0 },
          { type: 'number', labelName: 'Total size of volumes and snapshots (GiB)', name: 'gigabytes', defaultValue: 0 },
        ]
      },
      {
        type: 'panel',
        panelName: 'Network',
        keyValueRecords: [
          { type: 'number', labelName: 'Networks', name: 'network', defaultValue: 0 },
          { type: 'number', labelName: 'Subnets', name: 'subnet', defaultValue: 0 },
          { type: 'number', labelName: 'Ports', name: 'port', defaultValue: 0 },
          { type: 'number', labelName: 'Routers', name: 'router', defaultValue: 0 },
          { type: 'number', labelName: 'Floating IPs', name: 'floatingip', defaultValue: 0 },
          { type: 'number', labelName: 'Security groups', name: 'security_group', defaultValue: 0 },
          { type: 'number', labelName: 'Security group rules', name: 'security_group_rule', defaultValue: 0 },
        ]
      },
      {
        type: 'panel',
        panelName: 'Workflow',
        keyValueRecords: [
          { type: 'select-local', name: 'status', labelName: 'Status', defaultValue: 'Draft', value: [
            { name: 'Draft', labelName: 'Draft' },
            { name: 'Review', labelName: 'Review' },
            { name: 'Approved', labelName: 'Approved' },
          ], required: true },
        ]
      },
    ]
  },
]

export default OPENSTACK_QUOTA_TEMPLATE;
