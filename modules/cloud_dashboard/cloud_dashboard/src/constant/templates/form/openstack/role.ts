import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_ROLE_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'role',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Role',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'textarea', labelName: 'Description', name: 'description', defaultValue: '', id: 'field--name-field-description', class: 'field--name-field-description' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'role',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Role',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'textarea', labelName: 'Description', name: 'description', defaultValue: '', id: 'field--name-field-description', class: 'field--name-field-description' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'role',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  }
]

export default OPENSTACK_ROLE_TEMPLATE;
