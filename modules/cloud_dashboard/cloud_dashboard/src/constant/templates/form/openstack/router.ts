import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_NETWORK_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'router',
    actionType: 'add_interface',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Interface',
        keyValueRecords: [
          {
            type: 'select', labelName: 'Subnet', name: 'subnet_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/subnets_router/{entity_id}',
            defaultValue: '', id: 'edit-subnet-id', class: 'form-item-subnet-id'
          },
          { type: 'default', labelName: 'IP address', name: 'ip_address', defaultValue: '', id: 'edit-ip-address', class: 'form-item-ip-address' },
        ]
      }
    ],
    submitButtonLabel: 'Add interface'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'router',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Router',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'boolean', labelName: 'Admin state up', name: 'admin_state_up', defaultValue: true },
          { type: 'select', labelName: 'External network', name: 'external_gateway_network_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/external_gateway_network_ids',
            defaultValue: '', id: 'form-item-external-gateway-network-id', class: 'form-item-external-gateway-network-id'
          },
          { type: 'boolean', labelName: 'Enable SNAT', name: 'external_gateway_enable_snat', defaultValue: true },
          { type: 'multi-select', labelName: 'Availability Zone', name: 'availability_zone',
            url: '/cloud_dashboard/openstack/{cloud_context}/availability_zones?component_name=network&zone_resource=router',
            defaultValue: [],
            id: 'field--name-availability-zone', class: 'field--name-availability-zone'
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'router',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Router',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'boolean', labelName: 'Admin state up', name: 'admin_state_up', defaultValue: true },
          { type: 'select', labelName: 'External network', name: 'external_gateway_network_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/external_gateway_network_ids',
            defaultValue: '', id: 'form-item-external-gateway-network-id', class: 'form-item-external-gateway-network-id'
          },
          { type: 'boolean', labelName: 'Enable SNAT', name: 'external_gateway_enable_snat', defaultValue: true },
          { type: 'key-value', labelName: 'Static Routes', name: 'routes', defaultValue: [],
            columnLabel: { keyLabelName: 'Destination CIDR', valueLabelName: 'Next Hop'}
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'router',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  }
]

export default OPENSTACK_NETWORK_TEMPLATE;
