import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_SECURITY_GROUP_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'security_group',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Security group',
        keyValueRecords: [
          { type: 'default', labelName: 'Security group name', name: 'group_name', defaultValue: '', required: true, id: 'field--name-group-name', class: 'field--name-group-name' },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '', required: true, id: 'field--name-field-description', class: 'field--name-field-description' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'security_group',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'security_group',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Security group',
        keyValueRecords: [
          { labelName: 'Name', name: 'name', type: 'default', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { labelName: 'Security group name', name: 'group_name', type: 'default', defaultValue: '', readOnly: true },
          { labelName: 'ID', name: 'group_id', type: 'default', defaultValue: '', readOnly: true },
          { labelName: 'Description', name: 'description', type: 'default', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'VPC ID', name: 'vpc_id', defaultValue: '', readOnly: true },
          { labelName: 'Created', name: 'created', type: 'datetime', defaultValue: 0, readOnly: true },
        ]
      },
      {
        type: 'panel',
        panelName: 'Rules',
        keyValueRecords: [
          { labelName: 'Inbound rules', name: 'ip_permission', type: 'sg_permission', defaultValue: [] , id: 'edit-ip-permission', class: 'field--name-ip-permission' },
          { labelName: 'Outbound rules', name: 'outbound_permission', type: 'sg_permission', defaultValue: [], id: 'edit-outbound-permission', class: 'field--name-outbound-permission' },
        ]
      }
    ],
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'security_group',
    actionType: 'revoke',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to revoke the following permission?'
      },
    ],
    submitButtonLabel: 'Revoke'
  }
]

export default OPENSTACK_SECURITY_GROUP_TEMPLATE;
