import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_SERVER_GROUP_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'server_group',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Server group',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'select', labelName: 'Policy', name: 'policy', id: 'field--name-policy', class: 'field--name-policy',
            url: '/cloud_dashboard/openstack/{cloud_context}/server_group_policy',
            defaultValue: '', required: true
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'server_group',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  }
]

export default OPENSTACK_SERVER_GROUP_TEMPLATE;
