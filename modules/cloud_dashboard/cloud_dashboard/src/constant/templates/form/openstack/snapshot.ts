import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_SNAPSHOT_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'snapshot',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Snapshot',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'field--name-name', class: 'field--name-name' },
          { type: 'select', labelName: 'Volume ID', name: 'volume_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/snapshot_volume_ids',
            defaultValue: '', required: true, id: 'form-item-volume-id', class: 'form-item-volume-id'
          },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '', id: 'field--name-field-description', class: 'field--name-field-description' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'snapshot',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'snapshot',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Snapshot',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'field--name-name', class: 'field--name-name' },
          { type: 'default', labelName: 'Description', name: 'description', defaultValue: '', id: 'field--name-field-description', class: 'field--name-field-description' },
          { type: 'default', labelName: 'Snapshot ID', name: 'snapshot_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Volume ID', name: 'volume_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Size (GB)', name: 'size', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Status', name: 'status', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Progress', name: 'Progress', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true },
        ]
      }
    ]
  },
]

export default OPENSTACK_SNAPSHOT_TEMPLATE;
