import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_STACK_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'stack',
    actionType: 'create',
    entityRecords: []
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'stack',
    actionType: 'edit',
    entityRecords: []
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'stack',
    actionType: 'preview',
    entityRecords: []
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'stack',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'stack',
    actionType: 'check',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to check stack: {{entityName}} {{name}}?'
      },
    ],
    submitButtonLabel: 'Check'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'stack',
    actionType: 'resume',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to resume stack: {{entityName}} {{name}}?'
      },
    ],
    submitButtonLabel: 'Resume'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'stack',
    actionType: 'suspend',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to suspend stack: {{entityName}} {{name}}?'
      },
    ],
    submitButtonLabel: 'Suspend'
  }
]

export default OPENSTACK_STACK_TEMPLATE;
