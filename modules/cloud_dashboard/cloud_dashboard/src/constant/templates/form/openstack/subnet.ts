import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_SUBNET_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'subnet',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Subnet',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          {
            type: 'select', labelName: 'Network', name: 'network_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/networks',
            defaultValue: '',
            required: true, id: 'field--name-network-id', class: 'field--name-network-id'
          },
          { type: 'default', labelName: 'CIDR', name: 'cidr', defaultValue: '', required: true, id: 'field--name-cidr', class: 'field--name-cidr' },
          { type: 'select-local', name: 'ip_version', labelName: 'IP version', defaultValue: '', required: true, value: [
            { name: 'IPv4', labelName: 'IPv4' },
            { name: 'IPv6', labelName: 'IPv6' },
          ], id: 'field--name-ip-version', class: 'field--name-ip-version' },
          { type: 'default', labelName: 'Gateway IP', name: 'gateway_ip', defaultValue: '', id: 'field--name-gateway-ip', class: 'field--name-gateway-ip' },
          {
            type: 'boolean', labelName: 'Disable Gateway', name: 'disable_gateway',
            defaultValue: false
          },
          {
            type: 'boolean', labelName: 'Enable DHCP', name: 'enable_dhcp',
            defaultValue: true
          },
          {
            type: 'textarea', labelName: 'Allocation pools', name: 'allocation_pools',
            defaultValue: '', id: 'form-item-allocation-pools', class: 'form-item-allocation-pools'
          },
          {
            type: 'textarea', labelName: 'DNS name servers', name: 'dns_name_servers',
            defaultValue: '', id: 'form-item-dns-name-servers', class: 'form-item-dns-name-servers'
          },
          {
            type: 'textarea', labelName: 'Host Routes', name: 'host_routes',
            defaultValue: '', id: 'form-item-host-routes', class: 'form-item-host-routes'
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'subnet',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Subnet',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          {
            type: 'join', labelName: 'Network', name: 'network_id', info: {
              entityTypeId: 'openstack_network',
              keyColumn: 'network_id',
            }, defaultValue: '', readOnly: true
          },
          { type: 'default', labelName: 'CIDR', name: 'cidr', defaultValue: '', required: true },
          { type: 'default', labelName: 'Gateway IP', name: 'gateway_ip', defaultValue: '', id: 'field--name-gateway-ip', class: 'field--name-gateway-ip' },
          {
            type: 'boolean', labelName: 'Disable Gateway', name: 'disable_gateway',
            defaultValue: false
          },
          {
            type: 'boolean', labelName: 'Enable DHCP', name: 'enable_dhcp',
            defaultValue: true
          },
          {
            type: 'textarea', labelName: 'Allocation pools', name: 'allocation_pools',
            defaultValue: '', id: 'form-item-allocation-pools', class: 'form-item-allocation-pools'
          },
          {
            type: 'textarea', labelName: 'DNS name servers', name: 'dns_name_servers',
            defaultValue: '', id: 'form-item-dns-name-servers', class: 'form-item-dns-name-servers'
          },
          {
            type: 'textarea', labelName: 'Host Routes', name: 'host_routes',
            defaultValue: '', id: 'form-item-host-routes', class: 'form-item-host-routes'
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'subnet',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
]

export default OPENSTACK_SUBNET_TEMPLATE;
