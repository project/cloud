import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_USER_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'user',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'User',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'textarea', labelName: 'Description', name: 'description', defaultValue: '', id: 'field--name-field-description', class: 'field--name-field-description' },
          { type: 'default', labelName: 'Email', name: 'email', defaultValue: '', id: 'edit-email', class: 'form-item-email' },
          { type: 'password', labelName: 'Password', name: 'password', confirmLabelName: 'Confirm password', defaultValue: '', required: true, id: 'edit-password', class: 'form-item-password' },
          { type: 'select', labelName: 'Primary Project', name: 'default_project_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/projects',
            defaultValue: '', id: 'edit-default-project-id', class: 'form-item-default-project-id'
          },
          { type: 'select', labelName: 'Role', name: 'role',
            url: '/cloud_dashboard/openstack/{cloud_context}/roles',
            defaultValue: '', id: 'edit-role', class: 'form-item-role'
          },
          { type: 'boolean', labelName: 'Enabled', name: 'enabled', defaultValue: true, id: 'edit-enabled', class: 'form-item-enabled' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'user',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'User',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', required: true, id: 'field--name-name', class: 'field--name-name' },
          { type: 'textarea', labelName: 'Description', name: 'description', defaultValue: '', id: 'field--name-field-description', class: 'field--name-field-description' },
          { type: 'default', labelName: 'Email', name: 'email', defaultValue: '' },
          { type: 'select', labelName: 'Primary Project', name: 'default_project_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/projects',
            defaultValue: ''
          },
          { type: 'boolean', labelName: 'Enabled', name: 'enabled', defaultValue: true },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'user',
    actionType: 'change_password',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'User',
        keyValueRecords: [
          { type: 'password', labelName: 'Password', name: 'password', confirmLabelName: 'Confirm password', defaultValue: '', required: true, id: 'edit-password', class: 'form-item-password' },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'user',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  }
]

export default OPENSTACK_USER_TEMPLATE;
