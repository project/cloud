import EntityFormTemplate from 'model/EntityFormTemplate';

const OPENSTACK_VOLUME_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'openstack',
    entityName: 'volume',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Volume',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'field--name-name', class: 'field--name-name' },
          { type: 'select', labelName: 'Snapshot ID', name: 'snapshot_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/volume_snapshot_ids',
            defaultValue: '', required: true
          },
          { type: 'number', labelName: 'Size (GiB)', name: 'size', defaultValue: 1, required: true, id: 'field--name-size', class: 'field--name-size' },
          { type: 'select', labelName: 'Volume type', name: 'volume_type',
            url: '/cloud_dashboard/openstack/{cloud_context}/volume_types',
            defaultValue: '',
            defaultValueUrl: '/cloud_dashboard/openstack/{cloud_context}/default_volume_type',
            required: true
          },
          { type: 'select', labelName: 'Availability Zone', name: 'availability_zone',
            url: '/cloud_dashboard/openstack/{cloud_context}/availability_zones?component_name=volume',
            defaultValue: '',
            defaultValueUrl: '/cloud_dashboard/openstack/{cloud_context}/default_availability_zone',
            required: true, id: 'field--name-field-os-availability-zone', class: 'field--name-field-os-availability-zone'
          },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'volume',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      },
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'volume',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'Volume',
        keyValueRecords: [
          { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'field--name-name', class: 'field--name-name' },
          { type: 'default', labelName: 'Volume ID', name: 'volume_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Instance ID', name: 'attachment_information', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Snapshot ID', name: 'snapshot_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Snapshot name', name: 'snapshot_name', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Size (GiB)', name: 'size', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Volume type', name: 'volume_type', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Availability Zone', name: 'availability_zone', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Status', name: 'state', defaultValue: '', readOnly: true },
          { type: 'datetime', labelName: 'Created', name: 'created', defaultValue: 0, readOnly: true },
        ]
      }
    ]
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'volume',
    actionType: 'attach',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to attach {{entityName}}: {{name}}?'
      },
      {
        type: 'panel',
        panelName: 'Volume Information',
        keyValueRecords: [
          { type: 'default', labelName: 'Volume id', name: 'volume_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Volume name', name: 'name', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Device name', name: 'device_name', defaultValue: '', id: 'form-item-device-name', class: 'form-item-device-name' },
          { type: 'select', labelName: 'Instance ID', name: 'instance_id',
            url: '/cloud_dashboard/openstack/{cloud_context}/openstack_volume/{entity_id}/instances',
            defaultValue: '', required: true, id: 'form-item-instance-id', class: 'form-item-instance-id'
          },
        ]
      },
    ],
    submitButtonLabel: 'Attach'
  },
  {
    cloudServiceProvider: 'openstack',
    entityName: 'volume',
    actionType: 'detach',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to detach {{entityName}}: {{name}}?'
      },
      {
        type: 'panel',
        panelName: 'Volume Information',
        keyValueRecords: [
          { type: 'default', labelName: 'Volume ID', name: 'volume_id', defaultValue: '', readOnly: true },
          { type: 'default', labelName: 'Volume name', name: 'name', defaultValue: '', readOnly: true },
          {
            type: 'join', labelName: 'Attached to instance', name: 'attachment_information', info: {
              entityTypeId: 'openstack_instance',
              keyColumn: 'instance_id',
            }, defaultValue: '', readOnly: true
          },
        ]
      },
    ],
    submitButtonLabel: 'Detach'
  },
]

export default OPENSTACK_VOLUME_TEMPLATE;
