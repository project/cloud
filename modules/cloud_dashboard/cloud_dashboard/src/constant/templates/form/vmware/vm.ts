import EntityFormTemplate from 'model/EntityFormTemplate';

const VMWARE_VM_TEMPLATE: EntityFormTemplate[] = [
  {
    cloudServiceProvider: 'vmware',
    entityName: 'vm',
    actionType: 'create',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'VM',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Name', name: 'name',
            defaultValue: 'vm1', required: true
          },
          {
            type: 'join', labelName: 'Host', name: 'host', info: {
              entityTypeId: 'vmware_host',
              keyColumn: 'name'
            }, defaultValue: '', required: true
          },
          {
            type: 'select', labelName: 'Folder', name: 'folder',
            url: '/cloud_dashboard/vmware/{cloud_context}/folders',
            defaultValue: '', required: true
          },
          {
            type: 'select', labelName: 'Datastore', name: 'datastore',
            url: '/cloud_dashboard/vmware/{cloud_context}/datastores',
            defaultValue: '', required: true
          },
          {
            type: 'select', labelName: 'Guest OS', name: 'guest_os',
            url: '/cloud_dashboard/vmware/guest_oses',
            defaultValue: '', required: true
          },
          {
            type: 'number', labelName: 'CPU Count', name: 'cpu_count',
            defaultValue: 1, required: true
          },
          {
            type: 'number', labelName: 'Memory Size (MiB)', name: 'memory_size',
            defaultValue: 4096, required: true
          },
          {
            type: 'number-array', labelName: 'Disk Size (GiB)', name: 'disk_size',
            defaultValue: [10], required: true
          },
        ]
      },
    ]
  },
  {
    cloudServiceProvider: 'vmware',
    entityName: 'vm',
    actionType: 'edit',
    entityRecords: [
      {
        type: 'panel',
        panelName: 'VM',
        keyValueRecords: [
          {
            type: 'default', labelName: 'Name', name: 'name',
            defaultValue: '', readOnly: true
          },
          {
            type: 'default', labelName: 'Power State', name: 'power_state',
            defaultValue: '', readOnly: true
          },
          {
            type: 'number', labelName: 'CPU Count', name: 'cpu_count',
            defaultValue: 0, required: true
          },
          {
            type: 'number', labelName: 'Memory Size (MiB)', name: 'memory_size',
            defaultValue: 0
          },
          {
            type: 'select', labelName: 'Guest OS', name: 'guest_os',
            url: '/cloud_dashboard/vmware/guest_oses',
            defaultValue: '', readOnly: true
          },
          {
            type: 'number-array', labelName: 'Disk Size (GiB)', name: 'disk_size',
            defaultValue: [0],
          },
        ]
      },
    ]
  },
  {
    cloudServiceProvider: 'vmware',
    entityName: 'vm',
    actionType: 'reboot',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to reboot VM: {{name}}?'
      }
    ]
  },
  {
    cloudServiceProvider: 'vmware',
    entityName: 'vm',
    actionType: 'delete',
    entityRecords: [
      {
        type: 'label',
        text: 'This action cannot be undone.'
      }
    ],
    submitButtonLabel: 'Delete'
  },
  {
    cloudServiceProvider: 'vmware',
    entityName: 'vm',
    actionType: 'start',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to start VM: {{name}}?'
      }
    ],
    submitButtonLabel: 'Start'
  },
  {
    cloudServiceProvider: 'vmware',
    entityName: 'vm',
    actionType: 'stop',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to stop VM: {{name}}?'
      }
    ],
    submitButtonLabel: 'Stop'
  },
  {
    cloudServiceProvider: 'vmware',
    entityName: 'vm',
    actionType: 'suspend',
    entityRecords: [
      {
        type: 'label',
        text: 'Are you sure you want to suspend VM: {{name}}?'
      }
    ],
    submitButtonLabel: 'Suspend'
  }
]

export default VMWARE_VM_TEMPLATE;
