import LimitSummary from 'model/LimitSummary';

const OPENSTACK_LIMIT_SUMMARY_TEMPLATES: LimitSummary[] = [
  {
    panelTitle: 'Compute',
    plots: [
      { label: 'Instances', usedKey: 'instances_usage', totalKey: 'instances' },
      { label: 'vCPUs', usedKey: 'cores_usage', totalKey: 'cores' },
      { label: 'RAM', usedKey: 'ram_usage', totalKey: 'ram', scale: 1024, suffix: 'GB' },
    ]
  },
  {
    panelTitle: 'Volume',
    plots: [
      { label: 'Volumes', usedKey: 'volumes_usage', totalKey: 'volumes' },
      { label: 'Volume snapshots', usedKey: 'snapshots_usage', totalKey: 'snapshots' },
      { label: 'Volume storage', usedKey: 'gigabytes_usage', totalKey: 'gigabytes', suffix: 'GB' },
    ],
  },
  {
    panelTitle: 'Network',
    plots: [
      { label: 'Floating IPs', usedKey: 'floatingip_usage', totalKey: 'floatingip' },
      { label: 'Security groups', usedKey: 'security_group_usage', totalKey: 'security_group' },
      { label: 'Security group rules', usedKey: 'security_group_rule_usage', totalKey: 'security_group_rule' },
      { label: 'Networks', usedKey: 'network_usage', totalKey: 'network' },
      { label: 'Ports', usedKey: 'port_usage', totalKey: 'port' },
      { label: 'Routers', usedKey: 'router_usage', totalKey: 'router' },
    ],
  },
];

export default OPENSTACK_LIMIT_SUMMARY_TEMPLATES;
