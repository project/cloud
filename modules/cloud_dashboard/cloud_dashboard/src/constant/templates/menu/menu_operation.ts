import MenuOperation from 'model/MenuOperation';

/**
 * The list of operations.
 */
const MENU_OPERATIONS: MenuOperation[] = [
  { typeId: 'aws_cloud_instance', action: 'stop', label: 'Stop', when: [{ key: 'instance_state', value: 'running' }] },
  { typeId: 'aws_cloud_instance', action: 'reboot', label: 'Reboot', when: [{ key: 'instance_state', value: 'running' }] },
  { typeId: 'aws_cloud_instance', action: 'start', label: 'Start', when: [{ key: 'instance_state', value: 'stopped' }] },
  { typeId: 'aws_cloud_instance', action: 'associate_elastic_ip', label: 'Associate Elastic IP', when: [{ key: 'instance_state', value: 'stopped' }] },
  { typeId: 'aws_cloud_instance', action: 'create_image', label: 'Create image', when: [{ key: 'instance_state', value: 'pending' }] },
  { typeId: 'aws_cloud_instance', action: 'create_image', label: 'Create image', when: [{ key: 'instance_state', value: 'running' }] },
  { typeId: 'aws_cloud_instance', action: 'create_image', label: 'Create image', when: [{ key: 'instance_state', value: 'stopping' }] },
  { typeId: 'aws_cloud_instance', action: 'create_image', label: 'Create image', when: [{ key: 'instance_state', value: 'stopped' }] },
  { typeId: 'aws_cloud_volume', action: 'create_snapshot', label: 'Create snapshot' },
  { typeId: 'aws_cloud_volume', action: 'attach', label: 'Attach', when: [{ key: 'state', value: 'available' }] },
  { typeId: 'aws_cloud_volume', action: 'detach', label: 'Detach', when: [{ key: 'state', value: 'in-use' }] },
  { typeId: 'aws_cloud_elastic_ip', action: 'associate', label: 'Associate Elastic IP', when: [{ key: 'association_id', value: '' }] },
  { typeId: 'aws_cloud_elastic_ip', action: 'associate', label: 'Associate Elastic IP', when: [{ key: 'association_id', value: null }] },
  { typeId: 'aws_cloud_elastic_ip', action: 'associate', label: 'Associate Elastic IP', when: [{ key: 'association_id', value: undefined }] },
  { typeId: 'aws_cloud_elastic_ip', action: 'disassociate', label: 'Disassociate Elastic IP', not: [{ key: 'association_id', value: '' }, { key: 'association_id', value: null }, { key: 'association_id', value: undefined }] },
  { typeId: 'aws_cloud_snapshot', action: 'create_volume', label: 'Create Volume' },
  { typeId: 'aws_cloud_security_group', action: 'copy', label: 'Copy' },
  { typeId: 'aws_cloud_vpc_peering_connection', action: 'accept', label: 'Accept', when: [{ key: 'status_code', value: 'pending-acceptance' }] },
  { typeId: 'aws_cloud_internet_gateway', action: 'attach', label: 'Attach', when: [{ key: 'vpc_id', value: '' }] },
  { typeId: 'aws_cloud_internet_gateway', action: 'attach', label: 'Attach', when: [{ key: 'vpc_id', value: null }] },
  { typeId: 'aws_cloud_internet_gateway', action: 'detach', label: 'Detach', not: [{ key: 'vpc_id', value: '' }, { key: 'vpc_id', value: null }] },

  { typeId: 'k8s_deployment', action: 'scale', label: 'Scale' },

  { typeId: 'openstack_instance', action: 'stop', label: 'Stop', when: [{ key: 'instance_state', value: 'running' }] },
  { typeId: 'openstack_instance', action: 'reboot', label: 'Reboot', when: [{ key: 'instance_state', value: 'running' }] },
  { typeId: 'openstack_instance', action: 'console_output', label: 'Console output', when: [{ key: 'instance_state', value: 'running' }] },
  { typeId: 'openstack_instance', action: 'start', label: 'Start', when: [{ key: 'instance_state', value: 'stopped' }] },
  { typeId: 'openstack_instance', action: 'create_image', label: 'Create an OpenStack image' },
  { typeId: 'openstack_instance', action: 'attach_network', label: 'Attach interface' },
  { typeId: 'openstack_instance', action: 'detach_network', label: 'Detach interface' },
  { typeId: 'openstack_instance', action: 'terminate', label: 'Delete' },
  { typeId: 'openstack_floating_ip', action: 'associate', label: 'Associate Floating IP', when: [{ key: 'association_id', value: '' }] },
  { typeId: 'openstack_floating_ip', action: 'associate', label: 'Associate Floating IP', when: [{ key: 'association_id', value: null }] },
  { typeId: 'openstack_floating_ip', action: 'associate', label: 'Associate Floating IP', when: [{ key: 'association_id', value: undefined }] },
  { typeId: 'openstack_floating_ip', action: 'disassociate', label: 'Disassociate Floating IP', not: [{ key: 'association_id', value: '' }, { key: 'association_id', value: null }, { key: 'association_id', value: undefined }] },
  { typeId: 'openstack_volume', action: 'attach', label: 'Attach', when: [{ key: 'attachment_information', value: '' }] },
  { typeId: 'openstack_volume', action: 'attach', label: 'Attach', when: [{ key: 'attachment_information', value: null }] },
  { typeId: 'openstack_volume', action: 'attach', label: 'Attach', when: [{ key: 'attachment_information', value: undefined }] },
  { typeId: 'openstack_volume', action: 'detach', label: 'Detach', not: [{ key: 'attachment_information', value: '' }, { key: 'attachment_information', value: null }, { key: 'attachment_information', value: undefined }] },
  { typeId: 'openstack_stack', action: 'check', label: 'Check' },
  { typeId: 'openstack_stack', action: 'suspend', label: 'Suspend' },
  { typeId: 'openstack_stack', action: 'resume', label: 'Resume' },
  { typeId: 'openstack_user', action: 'change_password', label: 'Change Password' },

  { typeId: 'vmware_vm', action: 'start', label: 'Start', when: [{ key: 'power_state', value: 'POWERED_OFF' }] },
  { typeId: 'vmware_vm', action: 'stop', label: 'Stop', when: [{ key: 'power_state', value: 'POWERED_ON' }] },
  { typeId: 'vmware_vm', action: 'suspend', label: 'Suspend', when: [{ key: 'power_state', value: 'POWERED_ON' }] },
  { typeId: 'vmware_vm', action: 'reboot', label: 'Reboot', when: [{ key: 'power_state', value: 'POWERED_ON' }] },
  { typeId: 'vmware_vm', action: 'start', label: 'Start', when: [{ key: 'power_state', value: 'SUSPENDED' }] },
  { typeId: 'vmware_vm', action: 'stop', label: 'Stop', when: [{ key: 'power_state', value: 'SUSPENDED' }] },

  { typeId: 'cloud_launch_template', action: 'launch', label: 'Launch', when: [{ key: 'field_workflow_status', value: 'Approved' }] },
  { typeId: 'cloud_launch_template', action: 'copy', label: 'Copy' },
  { typeId: 'cloud_launch_template', action: 'approve', label: 'Approve', when: [{ key: 'field_workflow_status', value: 'Review' }] },
  { typeId: 'cloud_launch_template', action: 'review', label: 'Review', when: [{ key: 'field_workflow_status', value: 'Draft' }] },

  { typeId: 'cloud_project', action: 'launch', label: 'Launch' },
  { typeId: 'cloud_project', action: 'copy', label: 'Copy' },

  { typeId: 'k8s_cost_store', action: 'copy', label: 'Copy' },
  { typeId: 'k8s_namespace_resource_store', action: 'copy', label: 'Copy' },
  { typeId: 'k8s_node_resource_store', action: 'copy', label: 'Copy' },
  { typeId: 'k8s_pod_resource_store', action: 'copy', label: 'Copy' },
];

export default MENU_OPERATIONS;
