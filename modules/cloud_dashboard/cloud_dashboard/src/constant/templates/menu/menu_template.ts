import menu_template from 'constant/templates/menu/menu_template.json';
import CloudServiceProvider from 'model/CloudServiceProvider';
import MenuTemplate from 'model/MenuTemplate';

// Template for displaying a list of entities in AWS Cloud.
const AWS_MENU_LIST = menu_template[0] as MenuTemplate[];

// Template for displaying a list of entities in K8s.
const K8S_MENU_LIST = menu_template[1] as MenuTemplate[];

// Template for displaying a list of entities in OpenStack.
const OPENSTACK_MENU_LIST = menu_template[2] as MenuTemplate[];

// Template for displaying a list of entities in VMware.
const VMWARE_MENU_LIST = menu_template[3] as MenuTemplate[];

export const MENU_TEMPLATE_LIST = [
  ...AWS_MENU_LIST,
  ...K8S_MENU_LIST,
  ...OPENSTACK_MENU_LIST,
  ...VMWARE_MENU_LIST
];

export const getMenuTemplateList = (cloudServiceProvider: CloudServiceProvider) => {
  switch (cloudServiceProvider) {
    case 'aws_cloud':
      return AWS_MENU_LIST;
    case 'k8s':
      return K8S_MENU_LIST;
    case 'openstack':
      return OPENSTACK_MENU_LIST;
    case 'vmware':
      return VMWARE_MENU_LIST;
  }
}
