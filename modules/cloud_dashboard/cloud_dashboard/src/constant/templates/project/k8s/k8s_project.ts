import EntityColumn from 'model/EntityColumn';

// List of projects in K8s.
export const K8S_PROJECT_TEMPLATE_LIST: EntityColumn[] = [
  { labelName: 'Name', name: 'name', type: 'default' },
  { labelName: 'K8s cluster', name: 'field_k8s_clusters', type: 'default' },
  { labelName: 'Enable resource scheduler', name: 'field_enable_resource_scheduler', type: 'default' },
];

// Default template of projects in K8s.
export const DEFAULT_K8S_PROJECT_TEMPLATE: EntityColumn = {
  labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default'
};
