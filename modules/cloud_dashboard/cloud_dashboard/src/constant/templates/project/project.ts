import {
    DEFAULT_K8S_PROJECT_TEMPLATE, K8S_PROJECT_TEMPLATE_LIST
} from 'constant/templates/project/k8s/k8s_project';
import CloudContext from 'model/CloudContext';
import CloudServiceProvider from 'model/CloudServiceProvider';
import EntityColumn from 'model/EntityColumn';

/**
 * Get ProjectColumnList by cloud_context.
 *
 * @param cloudContext cloud_context.
 * @returns ProjectColumnList.
 */
export const getProjectColumnList = (cloudContext: CloudContext): EntityColumn[] => {
  switch (cloudContext.cloudServiceProvider) {
    case 'aws_cloud':
      return [];
    case 'openstack':
      return [];
    case 'vmware':
      return [];
    case 'k8s':
      return cloudContext.name !== 'ALL'
        ? K8S_PROJECT_TEMPLATE_LIST
        : [DEFAULT_K8S_PROJECT_TEMPLATE,
          ...K8S_PROJECT_TEMPLATE_LIST];
  }
};

// List of CloudServiceProvider where the Project resides.
export const PROJECT_CLOUD_CONTEXT_LIST: CloudServiceProvider[] = [
  'k8s'
];
