import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { createContext, useState } from 'react';
import { getLocalStorageItem, setLocalStorageItem } from 'service/storage';

interface BypassAnimation {
  bypassAnimation: boolean;
  isLoading: boolean;
}

/**
 * Get the animation type.
 * @returns The animation type.
 */
export const useBypassAnimation = (): BypassAnimation => {
  const { getJsonData } = useDrupalJsonApi();
  const [bypassAnimation, setBypassAnimation] = useState(
    getLocalStorageItem('bypassAnimation', 'false') === 'true'
  );
  const [isLoading, setLoading] = useState(true);

  if (isLoading) {
    setLoading(false);
    const init = async () => {
      const data = await getJsonData<{
        bypass_animation: 'true' | 'false'
      }>('/clouds/cloud_dashboard/config/bypass_animation',
      { bypass_animation: 'true' });
      setLocalStorageItem('bypassAnimation',
        data.bypass_animation === 'true' ? 'true' : 'false'
      );
      setBypassAnimation(data.bypass_animation === 'true');
      console.group('Bypass animation');
      console.log(data);
      console.groupEnd();
    }
    init();
  }

  return {
    bypassAnimation,
    isLoading,
  };
};

export const BypassAnimationContext = createContext<BypassAnimation>({
  bypassAnimation: true,
  isLoading: true,
});
