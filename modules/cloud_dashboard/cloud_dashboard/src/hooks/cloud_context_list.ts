import { CLOUD_SERVICE_PROVIDER_LIST, DEFAULT_CLOUD_CONTEXTS } from 'constant/others/other';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import CloudContext from 'model/CloudContext';
import CloudServiceProvider from 'model/CloudServiceProvider';
import EntityData from 'model/EntityData';
import { createContext, useState } from 'react';

interface CloudContextList {
  cloudContextList: CloudContext[];
  isLoading: boolean;
}

/**
 * Get cloud context list.
 * @returns The cloud context list.
 */
export const useCloudContextList = (): CloudContextList => {
  const { getEntityListAll } = useDrupalJsonApi();
  const [cloudContextList, setCloudContextList] = useState<CloudContext[]>([...DEFAULT_CLOUD_CONTEXTS]);
  const [isLoading, setLoading] = useState(true);

  if (isLoading) {
    setLoading(false);
    const init = async () => {
      let newCloudContextList = [...DEFAULT_CLOUD_CONTEXTS];
      for (const cloudServiceProvider of CLOUD_SERVICE_PROVIDER_LIST) {
        const data = (await getEntityListAll('cloud_config', {}, cloudServiceProvider))
          .map((record: EntityData) => {
            return {
              cloudServiceProvider: cloudServiceProvider as CloudServiceProvider,
              name: record.attributes['cloud_context'],
              labelName: record.attributes['name'],
            };
          });
        newCloudContextList = [...newCloudContextList, ...data];
      }
      setCloudContextList(newCloudContextList);
    };
    init();
  }

  return {
    cloudContextList,
    isLoading,
  };
};

export const CloudContextListContext = createContext<CloudContextList>({
  cloudContextList: [],
  isLoading: true,
});
