import useDrupalJsonApi, { GetJsonDataType } from 'hooks/drupal_jsonapi';
import EntityFormTemplate from 'model/EntityFormTemplate';
import { useEffect, useState } from 'react';

/**
 * Generate initial values for form input.
 */
const calcDefaultFormFata = async (
  cloudContext: string,
  entityFormTemplate: EntityFormTemplate,
  getJsonData: GetJsonDataType
) => {
  /**
   * Create a list of key strings and default values.
   *
   * Note: Pretreatment to facilitate the second half of the process.
   */
  const formInfoList: { key: string, type: string, defaultValue: any }[] = [];
  for (const entityRecord of entityFormTemplate.entityRecords) {
    // Process only forms contained in Panel.
    if (entityRecord.type !== 'panel') {
      continue;
    }

    // Load the form inside each Panel.
    for (const keyValueRecord of entityRecord.keyValueRecords) {
      /**
       * In the case of a time input form,
       * the hour and minute are each given an initial value.
       */
      if (keyValueRecord.type === 'time') {
        formInfoList.push({ key: keyValueRecord.hourName, type: keyValueRecord.type, defaultValue: keyValueRecord.defaultValue[0] });
        formInfoList.push({ key: keyValueRecord.minuteName, type: keyValueRecord.type, defaultValue: keyValueRecord.defaultValue[1] });
        continue;
      }

      /**
       * The initial value is null in the case of a form for a file,
       * otherwise the branch is branched depending on
       * whether an initial value is given.
       */
      formInfoList.push({
        key: keyValueRecord.type !== 'table-with-sort'
          ? keyValueRecord.name : '',
        type: keyValueRecord.type,
        defaultValue: keyValueRecord.type === 'file'
          ? null
          : 'defaultValue' in keyValueRecord
            ? keyValueRecord.defaultValue
            : undefined
      });
    }
  }

  // Set initial values.
  const newFormData: Record<string, any> = {};
  for (const formInfo of formInfoList) {
    // Handling of types that are not entered in the form, such as 'cost' types.
    if (formInfo.defaultValue === undefined) {
      continue;
    }

    newFormData[formInfo.key] = formInfo.defaultValue;
  }

  /**
   * If the initial value is specified by URL,
   * the read operation is performed.
   */
  for (const entityRecord of entityFormTemplate.entityRecords) {
    // Process only forms contained in Panel.
    if (entityRecord.type !== 'panel') {
      continue;
    }

    /**
     * Check to see if the initial value is given by URL,
     * and if so, perform the load process.
     */
    for (const keyValueRecord of entityRecord.keyValueRecords) {
      if (keyValueRecord.type !== 'default'
        && keyValueRecord.type !== 'textarea'
        && keyValueRecord.type !== 'hidden'
        && keyValueRecord.type !== 'select') {
        continue;
      }
      if (keyValueRecord.defaultValueUrl === undefined) {
        continue;
      }
      const replacedUrl = keyValueRecord.defaultValueUrl.replaceAll('{cloud_context}', cloudContext);
      if (replacedUrl.endsWith('/')) {
        continue;
      }
      const jsonData = await getJsonData<string>(replacedUrl, '');
      if (jsonData.length > 0) {
        newFormData[keyValueRecord.name] = jsonData;
      }
    }
  }

  return newFormData;
}

/**
 * Custom Hooks for form data.
 */
const useCreatingFormData = (
  cloudContext: string,
  entityFormTemplate: EntityFormTemplate,
) => {

  const [formData, setFormData] = useState<Record<string, any>>({});
  const { getJsonData } = useDrupalJsonApi();

  // Initialize form data.
  useEffect(() => {
    calcDefaultFormFata(cloudContext, entityFormTemplate, getJsonData).then((defaultFormData) => {
      setFormData(defaultFormData);
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    formData,
    setFormData
  };

}

export default useCreatingFormData;
