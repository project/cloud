import { createContext, useEffect, useState } from "react";

interface DataId {
  id: string,
  life: number,
}

type DataTableSelectedRows = {
  selectedIdList: string[],
  addId: (id: string, life?: number) => void,
  addIds: (ids: string[], life?: number) => void,
  removeId: (id: string) => void,
  hasId: (id: string) => boolean,
  clearIdList: () => void,
}

export const useDataTableSelectedRows = (): DataTableSelectedRows => {
  const [idList, setIdList] = useState<DataId[]>([]);

  const addId = (id: string, life = 1) => {
    const newIdList = JSON.parse(JSON.stringify(idList));
    if (!hasId(id)) {
      newIdList.push({ id, life });
    }
    setIdList(newIdList);
  }

  const addIds = (ids: string[], life = 1) => {
    const newIdList = JSON.parse(JSON.stringify(idList));
    const idSet = new Set(idList.map((dataId) => dataId.id));
    for (const id of ids) {
      if (!idSet.has(id)) {
        newIdList.push({ id, life });
        idSet.add(id);
      }
    }
    setIdList(newIdList);
  }

  const removeId = (id: string) => {
    const newIdList = [];
    for (const dataId of idList) {
      if (dataId.id !== id) {
        newIdList.push({
          id: dataId.id,
          life: dataId.life,
        });
      }
    }
    setIdList(newIdList);
  }

  const hasId = (id: string) => {
    return idList.filter((dataId) => dataId.id === id).length >= 1;
  }

  const clearIdList = () => {
    const newIdList = [];
    for (const dataId of idList) {
      if (dataId.life <= 1) {
        continue;
      }

      newIdList.push({
        id: dataId.id,
        life: dataId.life - 1,
      });
    }
    setIdList(newIdList);
  }

  return {
    selectedIdList: idList.map((dataId) => dataId.id),
    addId,
    addIds,
    removeId,
    hasId,
    clearIdList,
  }
}

export const DataTableSelectedRowsContext = createContext<DataTableSelectedRows>({
  selectedIdList: [],
  addId: () => {},
  addIds: () => {},
  removeId: () => {},
  hasId: () => false,
  clearIdList: () => {},
});
