import { CACHE_EXPIRED_UNIXTIME, FETCH_TIMEOUT_MS } from 'constant/others/other';
import EntityColumn from 'model/EntityColumn';
import EntityData from 'model/EntityData';
import SortInfo from 'model/SortInfo';
import { useState } from 'react';
import { fetchWrapper } from 'service/fetch';
import { getLocalStorageItem } from 'service/storage';

type CacheType = Record<string, { response: any, unixtime: number }>;
export type GetEntityListAllType = (entityTypeId: string, filter?: { [key: string]: string }, bundleId?: string) => Promise<EntityData[]>;
export type GetJsonDataType = <T>(url: string, defaultValue: T, filter?: { [key: string]: string; }) => Promise<T>;
export type ReadDataCacheType = (entityColumnList: EntityColumn[], cloudContext?: string) => Promise<Record<string, EntityData[]>>;

let cache: CacheType = {};

type GetEntityListOption = {
  // Maximum number of data to be retrieved.
  limit: number,
  // Offset at which to retrieve the data (0 origin)
  offset: number,
  // Filters when retrieving data.
  filter: { [key: string]: string },
  // Keys and directions to sort.
  sort: SortInfo,
};

/**
 * Download entity data by JSON:API.
 *
 * @param The getJson() function.
 * @param entityTypeId Entity type ID.
 * @param option Options of downloading.
 * @param bundleId Bundle ID.
 */
const getEntityListImpl = async (
  getJson: <T>(url: string, defaultValue: T, parameter?: RequestInit) => Promise<T>,
  entityTypeId: string,
  option: GetEntityListOption,
  bundleId: string
) => {
  // Create a GET parameter.
  const parameters: { key: string, value: string }[] = [];
  parameters.push({ key: 'page[limit]', value: `${option.limit}` });
  parameters.push({ key: 'page[offset]', value: `${option.offset}` });
  for (const key in option.filter) {
    parameters.push({ key: `filter[${key}]`, value: option.filter[key] });
  }
  if (option.sort.key !== '') {
    parameters.push(
      option.sort.direction === 'ASC'
        ? { key: 'sort', value: option.sort.key }
        : { key: 'sort', value: '-' + option.sort.key }
    );
  }

  // Create the downloading URL.
  let url = `/jsonapi/${entityTypeId}/${bundleId}`;
  if (parameters.length > 0) {
    url += '?' + parameters.map((r) => r.key + '=' + r.value).join('&');
  }

  // Download Action.
  const res = await getJson<{ data: EntityData[] }>(url, {
    data: []
  });
  return res.data;
}

/**
 * Download ALL entity data by JSON:API.
 *
 * @param The getJson() function.
 * @param entityTypeId Entity type ID.
 * @param filter Filter for searching by keyword.
 * @param bundleId Bundle ID.
 */
const getEntityListAllImpl = async (
  getJson: <T>(url: string, defaultValue: T, parameter?: RequestInit) => Promise<T>,
  entityTypeId: string,
  filter: { [key: string]: string },
  bundleId: string
) => {
  // Create a GET parameter.
  const parameters: { key: string, value: string }[] = [];
  for (const key in filter) {
    parameters.push({ key, value: filter[key] });
  }

  // Create the downloading URL.
  let url = `/jsonapi/${entityTypeId}/${bundleId}`;
  if (parameters.length > 0) {
    url += '?' + parameters.map((r) => r.key + '=' + r.value).join('&');
  }

  // Download Action.
  let output: EntityData[] = [];
  while (true) {
    const res = await getJson<{
      data: EntityData[],
      links: {
        next?: {
          href: string
        }
      }
    }>(url, {
      data: [],
      links: {}
    });
    output = [...output, ...res.data];
    if (res.links.next !== undefined) {
      url = res.links.next!.href;
    } else {
      break;
    }
  }

  return output;
}

const useDrupalJsonApi = () => {
  const [jsonApiServerUri] = useState(
    getLocalStorageItem('jsonApiServerUri', '')
  );

  /**
   * Get JSON data.
   *
   * @param url URL.
   * @param parameter An object containing any custom settings that you want to apply to the request.
   * @param defaultValue The default value to return if the response is not OK.
   * @param addJsonApiServerUriFlg If this is true, give the server address of JSON:API before the URL.
   * @returns JSON data with type T.
   */
  const getJson = async <T>(url: string, defaultValue: T|undefined, parameter?: RequestInit): Promise<T> => {
    const fixedUrl = url.includes('http://') || url.includes('https://')
      ? url : jsonApiServerUri + url;

    // If the data exists in the cache and is not expired, return it.
    if (fixedUrl in cache) {
      const cachedResponse = cache[fixedUrl];
      const nowUnixtime = (new Date()).getTime();
      if (nowUnixtime >= cachedResponse.unixtime && nowUnixtime - cachedResponse.unixtime < CACHE_EXPIRED_UNIXTIME) {
        return cachedResponse.response as T;
      }
    }

    // Controller for timeout in Fetch API.
    const controller = new AbortController();
    const timeout = setTimeout(() => { controller.abort() }, FETCH_TIMEOUT_MS);

    // Fetch API with timeout.
    try {
      const result = await fetchWrapper(url, {
        signal: controller.signal,
        ...parameter,
      });
      if (!result.ok) {
        console.group('JSON:API');
        console.error('URL:', fixedUrl);
        let jsonResponse = undefined;
        if (result.error instanceof Error) {
          console.error('Reason:', result.error.name === 'AbortError' ? 'Timeout' : result.error.message);
          jsonResponse = defaultValue;
        } else {
          console.error('Reason:');
          try {
            jsonResponse = await result.error.json();
            console.error(jsonResponse);
          } catch {
            console.error(result.error);
          }
        }
        console.groupEnd();
        return defaultValue === undefined ? jsonResponse as T : defaultValue;
      }
      const jsonResponse = await (result.response.json());
      const unixtime = (new Date()).getTime();
      cache[fixedUrl] = {response: jsonResponse, unixtime};
      return jsonResponse as T;
    } finally {
      clearTimeout(timeout);
    }
  }

  /**
   * Download entity data by JSON:API.
   *
   * @param entityTypeId Entity type ID.
   * @param option Options of downloading.
   */
  const getEntityList = async (entityTypeId: string, option: GetEntityListOption, bundleId: string = '') => {
    return getEntityListImpl(getJson, entityTypeId, option, bundleId === '' ? entityTypeId : bundleId);
  }

  /**
   * Download ALL entity data by JSON:API.
   *
   * @param entityTypeId Entity type ID.
   * @param filter Filter for searching by keyword.
   */
  const getEntityListAll = async (entityTypeId: string, filter: { [key: string]: string } = {}, bundleId: string = '') => {
    return getEntityListAllImpl(getJson, entityTypeId, filter, bundleId === '' ? entityTypeId : bundleId);
  }

  /**
   * Remove JSON:API server URI data for logout.
   */
  const removeJsonapiServerUri = () => {
    window.localStorage.removeItem('jsonapiServerUri');
  }

  /**
   * Get JSON data with filter.
   *
   * @param url URL.
   * @param filter Filter for searching by keyword.
   * @returns JSON data with type T.
   */
  const getJsonData = async <T>(url: string, defaultValue: T|undefined, filter: { [key: string]: string } = {}): Promise<T> => {
    // Create a GET parameter.
    const parameters: { key: string, value: string }[] = [];
    for (const key in filter) {
      parameters.push({ key, value: filter[key] });
    }

    // Create the downloading URL.
    let downloadUrl = url;
    if (parameters.length > 0) {
      downloadUrl += '?' + parameters.map((r) => r.key + '=' + r.value).join('&');
    }
    return await getJson<T>(downloadUrl, defaultValue);
  }

  /**
   * Read entity's data for convert entity's data by JSON:API.
   *
   * @param entityColumnList Information about the entities that will be loaded in advance.
   * @returns Data cache.
   */
  const readDataCache = async (
    entityColumnList: EntityColumn[],
    cloudContext?: string,
  ) => {
    const dataCache: Record<string, EntityData[]> = {};
    const flattenEntityColumnList: EntityColumn[] = [];
    for (const entityColumn of entityColumnList) {
      switch (entityColumn.type) {
        case 'join': {
          flattenEntityColumnList.push(entityColumn);
          break;
        }
        case 'multi-join': {
          flattenEntityColumnList.push(entityColumn);
          break;
        }
        case 'array-table': {
          entityColumn.column.forEach((ec) => {
            flattenEntityColumnList.push(ec);
          });
          break;
        }
        case 'custom-table': {
          entityColumn.column.forEach((ec) => {
            flattenEntityColumnList.push(ec);
          });
          break;
        }
        case 'relationship':
          flattenEntityColumnList.push(entityColumn);
          break;
      }
    }
    for (const entityColumn of flattenEntityColumnList) {
      switch (entityColumn.type) {
        case 'join': {
          const entityTypeId = entityColumn.info.entityTypeId;
          if (!(entityTypeId in dataCache)) {
            dataCache[entityTypeId] = cloudContext !== undefined
              ? await getEntityListAll(entityTypeId, {'filter[cloud_context]': cloudContext})
              : await getEntityListAll(entityTypeId);
          }
          break;
        }
        case 'multi-join': {
          const entityTypeId = entityColumn.info.entityTypeId;
          if (!(entityTypeId in dataCache)) {
            dataCache[entityTypeId] = cloudContext !== undefined
              ? await getEntityListAll(entityTypeId, {'filter[cloud_context]': cloudContext})
              : await getEntityListAll(entityTypeId);
          }
          break;
        }
        case 'relationship':
          const entityTypeId = entityColumn.info.entityTypeId;
          if (!(entityTypeId in dataCache)) {
            dataCache[entityTypeId] = cloudContext !== undefined
              ? await getEntityListAll(entityTypeId, {'filter[cloud_context]': cloudContext})
              : await getEntityListAll(entityTypeId);
          }
          break;
      }
    }
    return dataCache;
  };

  const resetFetchCache = () => {
    cache = {};
  }

  return {
    getEntityList,
    getEntityListAll,
    removeJsonapiServerUri,
    getJsonData,
    readDataCache,
    resetFetchCache,
  };
}

export default useDrupalJsonApi;
