const useDrupalTranslation = () => {
  const t = (word: string, args?: Record<string, string>) => {
    return Drupal.t(word, args);
  }

  return {
    t,
  }
}

export default useDrupalTranslation;
