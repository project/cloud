import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import EntityData from 'model/EntityData';
import { useState } from 'react';
import { useParams } from 'react-router-dom';

/**
 * Custom Hooks for entity data.
 *
 * @param cloudServiceProvider The cloud service provider.
 * @param entityName The entity's name.
 * @param entityId The entity's ID.
 */
export const useEntityData = (
  cloudServiceProvider: string,
  entityName: string,
  entityId?: string
) => {

  const { getJsonData } = useDrupalJsonApi();
  const [entityData, setEntityData] = useState<EntityData | undefined>();
  const [isLoading, setLoading] = useState(true);

  if (isLoading && entityId) {
    setLoading(false);

    let url = '';
    if (entityName !== "cloud_launch_template") {
      const entityTypeId = `${cloudServiceProvider}_${entityName}`;
      url = `/jsonapi/${entityTypeId}/${entityTypeId}?filter[drupal_internal__id]=${entityId}`;
    } else {
      url = `/jsonapi/cloud_launch_template/${cloudServiceProvider}?filter[drupal_internal__id]=${entityId}`;
    }

    getJsonData<{ data: EntityData[] }>(url, { data: [] }).then((result) => {
      if (result.data.length >= 1) {
        setEntityData(result.data[0]);
      }
    });
  }

  return {
    entityData,
  }

}

/**
 * Custom Hooks for sub entity data.
 *
 * @param cloudServiceProvider The cloud service provider.
 * @param entityName The entity's name.
 * @param entityId The entity's ID.
 * @param subEntityId The sub entity's ID.
 */
export const useSubEntityData = (
  cloudServiceProvider: string,
  entityName: string,
  entityId: string,
  subEntityId: string,
) => {

  const { getJsonData } = useDrupalJsonApi();
  const [entityData, setEntityData] = useState<EntityData | undefined>();
  const [isLoading, setLoading] = useState(true);
  const params: {
    entityName: string,
  } = useParams();

  if (isLoading) {
    setLoading(false);

    let url = '';
    if (entityName !== "cloud_launch_template") {
      const entityTypeId = `${cloudServiceProvider}_${entityName}`;
      url = `/jsonapi/${entityTypeId}/${entityTypeId}?filter[${params.entityName}_entity_id]=${entityId}&filter[drupal_internal__id]=${subEntityId}`;
    } else {
      url = `/jsonapi/cloud_launch_template/${cloudServiceProvider}?filter[${params.entityName}_entity_id]=${entityId}&filter[drupal_internal__id]=${subEntityId}`;
    }

    getJsonData<{ data: EntityData[] }>(url, { data: [] }).then((result) => {
      if (result.data.length >= 1) {
        setEntityData(result.data[0]);
      }
    });
  }

  return {
    entityData,
  }

}
