import EntityFormRecord from 'model/EntityFormRecord';
import FormParameters from 'model/FormParameters';
import { createContext, useState } from 'react';
import { formParametersToFormRecord } from 'service/convert';

type OpenStackHeatFormTemplate = {
  parameterPanel: EntityFormRecord,
  parameterDefaultValue: Record<string, any>,
  templateUrl: string,
  setDataResponse: (templateData: FormParameters, template: string, environment: string) => void,
  setTemplateUrl: (templateUrl: string) => void,
}

export const useOpenStackHeatFormTemplate = (): OpenStackHeatFormTemplate => {

  const [parameterPanel, setParameterPanel] = useState<EntityFormRecord>({
    type: 'panel',
    panelName: 'Template parameters',
    keyValueRecords: []
  });
  const [parameterDefaultValue, setParameterDefaultValue] = useState<Record<string, any>>({
    template: '',
    environment: '',
  });
  const [templateUrl, setTemplateUrl] = useState<string>('');

  const setDataResponse = (templateData: FormParameters, template: string, environment: string) => {
    const formRecordData = formParametersToFormRecord(templateData);
    setParameterDefaultValue({
      ...formRecordData.parameterDefaultValue,
      'template': template,
      'environment': environment,
    });
    setParameterPanel(formRecordData.parameterPanel);
  }

  return {
    parameterPanel,
    parameterDefaultValue,
    templateUrl,
    setDataResponse,
    setTemplateUrl,
  }

}

export const OpenStackHeatFormTemplateContext = createContext<OpenStackHeatFormTemplate>({
  parameterPanel: {
    type: 'panel',
    panelName: 'Template parameters',
    keyValueRecords: []
  },
  parameterDefaultValue: {},
  templateUrl: '',
  setDataResponse: () => {},
  setTemplateUrl: () => {},
});
