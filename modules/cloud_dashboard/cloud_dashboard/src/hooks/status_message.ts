import BootstrapColor from 'model/BootstrapColor';
import Message from 'model/Message';
import StatusMessageResponse from 'model/StatusMessageResponse';
import { createContext, useState } from 'react';
import { fetchWrapper, printErrorResult } from 'service/fetch';
import { setLocalStorageItem } from 'service/storage';

/** The interface of message option. */
interface MessageOption {
  color?: BootstrapColor,
  args?: Record<string, string>,
  links?: Record<string, string>,
  life?: number,
}

/**
 * The interface of status message.
 */
interface StatusMessage {
  messages: Message[];
  addMessages: (appendMessages: {
    text: string, option?: MessageOption
  }[]) => void;
  addMessage: (text: string, option?: MessageOption) => void;
  addErrorMessage: (text: string, option?: MessageOption) => void;
  addMessagesByResponse:(response: StatusMessageResponse, option?: MessageOption) => void;
  removeMessage: (messageIndex: number) => void;
  removeAllMessage: () => void;
}

/**
 * Get the state of status message.
 * @returns The state of status message.
 */
export const useStatusMessage = (): StatusMessage => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [isLoading, setLoading] = useState(true);

  // Set JSON:API server URI.
  if (isLoading) {
    setLoading(false);
    const init = async () => {
      const result = await fetchWrapper('/clouds/cloud_dashboard/config/jsonapi_server_uri');
      if (!result.ok) {
        console.group('JSON:API server URI');
        await printErrorResult(result, 'warn');
        console.groupEnd();
        return;
      }

      setLocalStorageItem('jsonapiServerUri', (await result.response.json()).uri);
    };
    init();
  }

  /**
   * Add messages for display.
   * @param appendMessages The messages.
   */
  const addMessages = (appendMessages: {
    text: string,
    option?: MessageOption,
  }[]) => {
    setMessages((prevMessages) => [
      ...prevMessages,
      ...appendMessages.map((appendMessage) => {
        return {
          text: appendMessage.text,
          backgroundColor: appendMessage.option?.color ?? 'success',
          life: appendMessage.option?.life ?? 1,
          args: appendMessage.option?.args ? appendMessage.option.args : {},
          links: appendMessage.option?.links ? appendMessage.option.links : {},
        };
      }),
    ]);
  }

  /**
   * Add message for display.
   * @param text The message.
   * @param option The option of message.
   */
  const addMessage = (text: string, option?: MessageOption) => {
    addMessages([{
      text,
      option: {
        ...option,
        color: 'success',
      }
    }]);
  }

  /**
   * Add error message for display.
   * @param text The message.
   * @param option The option of message.
   */
  const addErrorMessage = (text: string, option?: MessageOption) => {
    addMessages([{
      text,
      option: {
        ...option,
        color: 'danger',
      }
    }]);
  }

  const addMessagesByResponse = (response: StatusMessageResponse, option?: MessageOption) => {
    const messages: Message[] = [];
    if (response.status) {
      messages.push({
        text: response.status.filter((t) => !t.includes('<script>')).join('\n'),
        backgroundColor: 'success',
        life: option?.life ?? 1,
        args: option?.args ? option.args : {},
        links: option?.links ? option.links : {},
      });
    }
    if (response.error) {
      messages.push({
        text: response.error.filter((t) => !t.includes('<script>')).join('\n'),
        backgroundColor: 'danger',
        life: option?.life ?? 1,
        args: option?.args ? option.args : {},
        links: option?.links ? option.links : {},
      });
    }
    setMessages((prev) => [
      ...prev,
      ...messages
    ]);
  }

  /**
   * Remove message for display.
   * @param messageIndex The message index.
   */
  const removeMessage = (messageIndex: number) => {
    setMessages((prevMessages) => {
      const newMessages = [...prevMessages];
      if (newMessages[messageIndex].life >= 2) {
        newMessages[messageIndex] = { ...newMessages[messageIndex], life: newMessages[messageIndex].life - 1 };
      } else {
        newMessages.splice(messageIndex, 1);
      }
      return newMessages;
    });
  }

  /**
   * Remove all messages for display.
   */
  const removeAllMessage = () => {
    setMessages((prevMessages) => {
      return prevMessages.filter((message) => {
        return message.life >= 2;
      }).map((message) => {
        return { ...message, life: message.life - 1 };
      });
    });
  }

  return {
    messages,
    addMessages,
    addMessage,
    addErrorMessage,
    addMessagesByResponse,
    removeMessage,
    removeAllMessage,
  };
};

export const StatusMessageContext = createContext<StatusMessage>({
  messages: [],
  addMessages: () => {},
  addMessage: () => {},
  addErrorMessage: () => {},
  addMessagesByResponse: () => {},
  removeMessage: () => {},
  removeAllMessage: () => {},
});
