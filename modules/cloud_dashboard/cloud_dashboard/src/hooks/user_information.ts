import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { createContext, useState } from 'react';

interface UserInformation {
  hasAdminRole: boolean;
}

/**
 * Get user information.
 */
export const useUserInformation = (): UserInformation => {
  const { getJsonData } = useDrupalJsonApi();
  const [hasAdminRole, setHasAdminRole] = useState(false);
  const [isLoading, setLoading] = useState(true);

  if (isLoading) {
    setLoading(false);
    const init = async () => {
      const info = await getJsonData<{
        has_admin_role: boolean;
      }>('/cloud_dashboard/openstack/user/has_admin_role', { has_admin_role: false });
      setHasAdminRole(info.has_admin_role);
    };
    init();
  }

  return {
    hasAdminRole,
  };
};

export const UserInformationContext = createContext<UserInformation>({
  hasAdminRole: false,
});
