import 'index.css';
import 'constant/translations/translation';

import App from 'App';
import { ROUTE_URL } from 'constant/others/other';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root')!!);

root.render(
  <React.StrictMode>
    <BrowserRouter basename={ROUTE_URL}>
      <App />
    </BrowserRouter>
  </React.StrictMode>
);
