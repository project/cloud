/** The type of color type. */
type BootstrapColor = 'primary' | 'secondary' | 'success' | 'danger' | 'warning' | 'info' | 'light' | 'dark' | 'hidden';

export default BootstrapColor;
