import { LatLngTuple } from 'leaflet';
import CloudContextItemlabel from 'model/CloudContextItemLabel';

interface CloudContextItem {
  icon: L.Icon | undefined,
  cloudServiceProvider: string,
  position: LatLngTuple,
  item: CloudContextItemlabel[]
}

export default CloudContextItem;
