interface CloudContextItemlabel {
  iconUrl: string;
  entityViewUrl: string;
  name: string;
  positionLabel: string;
}

export default CloudContextItemlabel;
