type CloudServiceProvider = 'aws_cloud' | 'k8s' | 'openstack' | 'vmware';

export default CloudServiceProvider;
