import EntityColumn from 'model/EntityColumn';

interface CloudStoreTemplate {
  bundleId: string;
  column: EntityColumn[];
  title: string;
}

export default CloudStoreTemplate;
