interface DataRecord {
  id: string,
  entityTypeId: string,
  value: { [key: string]: string },
  cloudContext: string
};

export default DataRecord;
