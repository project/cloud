/**
 * Label name and data attribute's key of entity.
 */
type EntityColumn = {
  labelName: string,
  name: string,
  type: 'default' | 'datetime' | 'cpu' | 'memory' | 'key-value' | 'cost' | 'array' | 'number-array' | 'key-value-crlf' | 'cidr' | 'number' | 'vmware_os' | 'storage_mb' | 'storage_gb' | 'float_number',
  readOnly?: boolean,
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  type: 'array-table',
  column: EntityColumn[],
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  type: 'boolean',
  value: [string, string],
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  type: 'join' | 'multi-join',
  info: {
    entityTypeId: string,
    keyColumn: string,
    valueColumn: string,
  },
  hiddenId?: boolean,
  id?: string,
  class?: string,
} | {
  labelName: '',
  name: '',
  type: 'metrics',
  column: {
    title: string,
    name: string,
    yLabel: string,
    type: 'default' | 'memory'
  }[],
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  type: 'custom-table',
  column: EntityColumn[],
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  type: 'conditions',
  value: [string, string],
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  type: 'json-table',
  column: EntityColumn[],
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  type: 'select',
  url: string,
  readOnly?: boolean,
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  type: 'link',
  url: string,
  label: string,
  readOnly?: boolean,
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  type: 'relationship',
  info: {
    entityTypeId: string,
    keyColumn1: string,
    keyColumn2: string,
    valueColumn: string,
  },
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  denominator_name: string,
  type: 'fraction',
  id?: string,
  class?: string,
} | {
  labelName: string,
  name: string,
  value: { labelName: string, name: string }[],
  type: 'select-local',
  id?: string,
  class?: string,
} | {
  name: string,
  labelName: string,
  type: 'comma',
  index: number,
  id?: string,
  class?: string,
} | {
  name: string,
  labelName: string,
  type: 'replace',
  list: Record<string, string>,
  id?: string,
  class?: string,
};

export default EntityColumn;
