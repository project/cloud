interface EntityRelationshipsData {
  id: string;
  type: string;
  meta: {
    [key: string]: any;
  }
}

/**
 * Entity data of JSON:API.
 *
 * Note: Originally, in the relationships' property,
 * the "type" key has a value of type `string` and
 * the "[key: string]" key has a value of type `object`.
 * However, according to the TypeScript specification,
 * the two cannot be used together (the value types must be aligned).
 * Therefore, the "[key: string]" key is defined as
 * having a value of type `object | string`.
 */
interface EntityData {
  attributes: {
    [key: string]: any;
  };
  relationships: {
    [key: string]: {
      data: EntityRelationshipsData | EntityRelationshipsData[];
      links: {
        related: {
          href: string;
        };
        self: {
          href: string;
        };
      }
    };
  } | string;
  id: string;
}

export default EntityData;
