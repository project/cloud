/**
 * Label name and data attribute's key of entity.
 */

import EntityColumn from 'model/EntityColumn';
import KeyValueItem from 'model/KeyValueItem';
import SecurityGroupPermission from 'model/SecurityGroupPermission';
import ImageSharedProjectsItem from 'model/ImageSharedProjectsItem';

type EntityFormColumnBase = {
  labelName: string,
  name: string,
  required?: boolean,
  readOnly?: boolean,
  id?: string,
  class?: string,
}

export type InfoArraytype = {
  labelName: string, name: string, type: 'default', readOnly?: boolean
} | {
  labelName: string, name: string, type: 'join', entityTypeId: string
} | {
  labelName: string, name: string, type: 'multi-select', entityTypeId: string
};

type EntityFormColumnInfo = {
  type: 'default' | 'textarea' | 'hidden',
  defaultValue: string,
  defaultValueUrl?: string,
} | {
  type: 'number-array',
  defaultValue: number[],
} | {
  type: 'array',
  defaultValue: string[],
} | {
  type: 'number' | 'datetime',
  defaultValue: number,
} | {
  type: 'key-value',
  defaultValue: KeyValueItem[],
  columnLabel?: { keyLabelName: string, valueLabelName: string }
} | {
  type: 'join',
  info: {
    entityTypeId: string,
    keyColumn: string,
    label?: string,
  },
  defaultValue: string,
} | {
  type: 'select',
  url: string,
  defaultValue: string,
  defaultValueUrl?: string,
} | {
  type: 'select-local',
  value: { labelName: string, name: string }[],
  defaultValue: string,
} | {
  type: 'multi-select',
  url: string,
  defaultValue: string[],
} | {
  type: 'multi-check',
  url: string,
  defaultValue: string[],
  keyType: 'value' | 'label',
} | {
  type: 'radio',
  value: { labelName: string, name: string }[],
  defaultValue: string,
  orientation: 'horizontal' | 'vertical'
} | {
  type: 'boolean',
  defaultValue: boolean,
  labels?: [string, string],
} | {
  type: 'item-array',
  info: InfoArraytype[],
  defaultValue: { labelName: string, name: string, }[],
} | {
  type: 'password',
  confirmLabelName: string,
  defaultValue: string,
} | {
  type: 'select-table',
  url: string,
  defaultValue: string,
  recordKey: string,
  sortKey: string,
  column: {
    labelName: string,
    name: string,
  }[]
};

type EntityFormColumn = (EntityFormColumnBase & EntityFormColumnInfo) | {
  type: 'time',
  labelName: string,
  hourName: string,
  minuteName: string,
  defaultValue: [string, string],
  required?: boolean,
  readOnly?: boolean,
  id?: string,
  class?: string,
} | {
  type: 'cost',
  labelName: string,
  name: string,
  id?: string,
  class?: string,
} | {
  type: 'sg_permission',
  labelName: string,
  name: string,
  defaultValue: SecurityGroupPermission[],
  id?: string,
  class?: string,
} | {
  type: 'file',
  labelName: string,
  name: string,
  id?: string,
  class?: string,
} | {
  type: 'table-with-sort',
  url: string,
  sortKey: string,
  entityName: string,
  column: EntityColumn[]
} | {
  type: 'image-shared-projects',
  defaultValue: ImageSharedProjectsItem[],
  labelName: string,
  name: string,
  required?: boolean,
  readOnly?: boolean,
  id?: string,
  class?: string,
};

export default EntityFormColumn;
