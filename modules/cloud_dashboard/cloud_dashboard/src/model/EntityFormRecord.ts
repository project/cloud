import EntityFormColumn from 'model/EntityFormColumn';

type EntityFormRecord = {
  type: 'panel',
  panelName: string,
  keyValueRecords: EntityFormColumn[],
} | {
  type: 'label',
  text: string,
};

export default EntityFormRecord;
