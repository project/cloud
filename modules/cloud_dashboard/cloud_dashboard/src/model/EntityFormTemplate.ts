import EntityFormRecord from 'model/EntityFormRecord';

type EntityFormTemplate = {
  cloudServiceProvider: string,
  entityName: string,
  actionType: string,
  entityRecords: EntityFormRecord[],
  submitButtonLabel?: string,
}

export default EntityFormTemplate;
