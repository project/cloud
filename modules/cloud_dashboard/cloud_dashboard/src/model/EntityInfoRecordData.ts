type EntityInfoRecordData = {
  type: 'div',
  key: string,
  value: string,
  id?: string,
  class?: string,
} | {
  type: 'table',
  title: string,
  record: Record<string, string>[],
  label?: Record<string, string>,
  id?: string,
  class?: string,
} | {
  type: 'metrics',
  record: {
    title: string,
    yLabel: string,
    record: { x: number, y: number }[]
  }[],
  id?: string,
  class?: string,
};

export default EntityInfoRecordData;
