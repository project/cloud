import EntityInfoTemplate from 'model/EntityInfoTemplate';

type EntityInfoTabTemplateColumn = {
  name: string,
  labelName: string,
  type: 'default' | 'datetime'
} | {
  name: string,
  labelName: string,
  type: 'boolean',
  value: [string, string],
} | {
  name: string,
  labelName: string,
  type: 'comma',
  index: number,
} | {
  name: string,
  labelName: string,
  type: 'replace',
  list: Record<string, string>,
};

type EntityInfoTabTemplateFilterInfo = {
  key: string,
  value: string,
}

type EntityInfoTabTemplateTab = {
  type: 'default',
  name: string,
  labelName: string,
} | {
  type: 'table',
  name: string,
  labelName: string,
  columns: EntityInfoTabTemplateColumn[],
  path: string,
  entityTypeId: string,
  filter: EntityInfoTabTemplateFilterInfo[],
  detail: EntityInfoTemplate,
  operationsLinks?: {
    label: string,
    path: string,
  }[],
  actionButtons: { label: string, action: string }[]
}

type EntityInfoTabTemplate = {
  cloudServiceProvider: string,
  entityName: string,
  tabs: EntityInfoTabTemplateTab[],
};

export default EntityInfoTabTemplate;
