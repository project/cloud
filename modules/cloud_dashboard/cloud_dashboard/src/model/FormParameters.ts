/**
 * Type to define the template parameter form in the second step of the form.
 */
type FormParameters = {
  parameters: Record<string, {
    default: any,
    description: string,
    type: string,
  }>
}

export default FormParameters;
