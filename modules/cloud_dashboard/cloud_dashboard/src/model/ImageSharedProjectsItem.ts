interface ImageSharedProjectsItem {
  cloud_context: string;
  project_id: string;
  status: string;
}

export default ImageSharedProjectsItem;
