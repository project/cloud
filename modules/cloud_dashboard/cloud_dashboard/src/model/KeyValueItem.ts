interface KeyValueItem {
  item_key: string;
  item_value: string;
}

export default KeyValueItem;
