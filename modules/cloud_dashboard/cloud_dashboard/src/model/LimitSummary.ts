import LimitSummaryPlot from 'model/LimitSummaryPlot';

interface LimitSummary {
  panelTitle: string;
  plots: LimitSummaryPlot[];
}

export default LimitSummary;
