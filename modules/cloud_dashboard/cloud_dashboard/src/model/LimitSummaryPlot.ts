interface LimitSummaryPlot {
  label: string;
  usedKey: string;
  totalKey: string;
  scale?: number;
  suffix?: string;
}

export default LimitSummaryPlot;
