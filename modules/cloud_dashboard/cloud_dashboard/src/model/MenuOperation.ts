interface MenuOperation {
  /**
   * The type ID of entity.
   */
  typeId: string;

  /**
   * The action of operation.
   */
  action: string;

  /**
   * The label of operation.
   */
  label: string;

  /**
   * The list of conditions.
   *   - key: The key of attribute.
   *   - value: If it is equal, it is true.
   */
  when?: {
    key: string,
    value: string | number | boolean | null | undefined,
  }[];

  /**
   * The list of conditions.
   *   - key: The key of attribute.
   *   - value: If it is not equal, it is true.
   */
  not?: {
    key: string,
    value: string | number | boolean | null | undefined,
  }[];
}

export default MenuOperation;
