import CloudServiceProvider from 'model/CloudServiceProvider';
import EntityColumn from 'model/EntityColumn';

interface MenuTemplate {
  cloudServiceProvider: CloudServiceProvider;
  labelName: string;
  entityName: string;
  entityColumn: EntityColumn[];
  multiActions?: { label: string, urlSuffix: string }[];
}

export default MenuTemplate;
