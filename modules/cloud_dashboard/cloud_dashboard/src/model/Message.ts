import BootstrapColor from 'model/BootstrapColor';

/**
 * The status message for the dashboard.
 *
 * i.e.
 * - {text: 'Hello, world.', backgroundColor: 'success', life: 1, args: {}, links: {}}
 * - {text: 'Username is @user.', backgroundColor: 'info', life: 2, args: {'@user': 'John Doe'}, links: {}}
 * - {text: 'Click @here.', backgroundColor: 'success', life: 1, args: {'@here': 'this URL'}, links: {'@here': 'https://example.com'}}
 */
interface Message {
  /** The message text. */
  text: string;
  /** The background color of the message. */
  backgroundColor: BootstrapColor;
  /** The life of ths message. If 0, the message will be removed. */
  life: number;
  /** The translation arguments for replacement. */
  args: Record<string, string>;
  /** The link keyword for replacement. */
  links: Record<string, string>;
}

export default Message;
