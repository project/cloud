type ScriptToken = {
  type: 'keyword' | 'text',
  value: string
} | {
  type: 'array',
  value: string[]
} | {
  type: 'right-bracket'
} | {
  type: 'left-bracket'
} | {
  type: 'comma'
} | {
  type: 'null'
}

export default ScriptToken;
