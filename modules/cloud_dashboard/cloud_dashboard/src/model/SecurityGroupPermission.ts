type SecurityGroupPermission = {
  cidr_ip: string | null,
  cidr_ip_v6: string | null,
  description: string | null,
  from_port: string | null,
  group_id: string | null,
  group_name: string | null,
  ip_protocol: '-1' | 'tcp' | 'udp' | 'icmp' | 'icmpv6',
  peering_connection_id: string | null,
  peering_status: string | null,
  prefix_list_id: string | null,
  rule_id: string | null,
  source: 'ip4' | 'ip6' | 'group',
  to_port: string | null,
  user_id: string | null,
  vpc_id: string | null,
}

export default SecurityGroupPermission;
