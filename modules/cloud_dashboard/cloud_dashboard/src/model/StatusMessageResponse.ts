interface StatusMessageResponse {
  status?: string[],
  error?: string[],
}

export  default StatusMessageResponse;
