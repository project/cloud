import LabelText from 'atoms/LabelText';
import { ACTION_BUTTON_LIST, DONT_SHOW_ACTION_ENTITY_TYPE_LIST } from 'constant/others/other';
import ENTITY_FORM_LIST from 'constant/templates/form/entity_form_template';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { StatusMessageContext } from 'hooks/status_message';
import MenuTemplate from 'model/MenuTemplate';
import StatusMessageResponse from 'model/StatusMessageResponse';
import { useContext, useState } from 'react';
import { Form } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import { fetchWrapper, printErrorResult } from 'service/fetch';
import { cspToLabel, entityNameToLabel } from 'service/string';

/**
 * Calculate the list of action buttons to display.
 *
 * @param menuTemplate The template of form menu.
 * @param cloudContext The Cloud Context.
 */
const calcActionButtonList = (menuTemplate: MenuTemplate, cloudContext: string) => {
  const actionTypeList = ENTITY_FORM_LIST.filter((r) => {
    return r.cloudServiceProvider === menuTemplate.cloudServiceProvider
      && r.entityName === menuTemplate.entityName;
  }).map((r) => r.actionType);

  const buttonList = ACTION_BUTTON_LIST
  .filter((r) => {
    return actionTypeList.includes(r.actionType);
  })
  .map((r) => {
    const to = `/${menuTemplate.cloudServiceProvider}/${cloudContext}/${menuTemplate.entityName}/${
      r.actionType === 'create' ? 'add' : r.actionType
    }`;
    const actionLabel = r.label;
    const cloudServiceProviderLabel = cspToLabel(menuTemplate.cloudServiceProvider);
    const entitytype = menuTemplate.entityName;
    return {
      to,
      action: actionLabel,
      cloudServiceProvider: cloudServiceProviderLabel,
      type: entityNameToLabel(entitytype),
    };
  });

  return menuTemplate.entityName === 'instance'
    ? [{
      to: `/design/server_template/${cloudContext}`,
      action: 'Add | Launch',
      cloudServiceProvider: cspToLabel(menuTemplate.cloudServiceProvider),
      type: entityNameToLabel('instance'),
    }, ...buttonList]
    : buttonList;
}

/**
 * The button group of action buttons.
 * @param menuTemplate The template of form menu.
 * @param cloudContext The Cloud Context.
 */
const ActionButtonList = ({ menuTemplate, cloudContext }: {
  menuTemplate: MenuTemplate,
  cloudContext: string
}) => {

  const actionButtonList = calcActionButtonList(menuTemplate, cloudContext);

  return (
    <>
      {actionButtonList.map((r) => (
        <Link className="btn btn-primary" key={r.to} to={r.to}>
          {`${r.action} ${r.cloudServiceProvider} ${r.type}`}
        </Link>
      ))}
    </>
  );

}

/**
 * The button of refreshing entity list.
 * @param menuTemplate The template of form menu.
 * @param cloudContext The Cloud Context.
 */
const EntityListRefreshButton = ({ menuTemplate, cloudContext }: {
  menuTemplate: MenuTemplate,
  cloudContext: string
}) => {

  const { addMessage, addErrorMessage, addMessagesByResponse } = useContext(StatusMessageContext);
  const [isRefreshing, setRefreshing] = useState(false);
  const history = useHistory();
  const { resetFetchCache } = useDrupalJsonApi();

  /**
   * Function to update the entity list.
   */
  const refresh = async () => {
    if (isRefreshing) {
      return;
    }

    setRefreshing(true);

    const url = `/cloud_dashboard/${menuTemplate.cloudServiceProvider}/${cloudContext}/${menuTemplate.entityName}/update`;

    const result = await fetchWrapper(url, {
      method: 'POST',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
      }
    });
    setRefreshing(false);

    const entityNameLabel = menuTemplate.labelName;

    if (!result.ok) {
      console.group('Refresh entity list');
      await printErrorResult(result, 'error');
      console.groupEnd();
      addErrorMessage('Unable to update @entityName.', {
        args: {
          '@entityName': entityNameLabel,
        }
      });
      return;
    }

    // Console log.
    console.group('Refresh Request');
    console.log('Response:');
    const responseJson = await result.response.json();
    console.log(responseJson);
    console.groupEnd();

    // Status message.
    const messages: StatusMessageResponse = (responseJson)['messages'];
    if (messages) {
      addMessagesByResponse(messages, {
        life: 2,
      });
    } else {
      addMessage('Updated @entityName.', {
        args: {
          '@entityName': entityNameLabel,
        },
        life: 2,
      });
    }
    resetFetchCache();

    history.push(`/${menuTemplate.cloudServiceProvider}/${cloudContext}/${menuTemplate.entityName}`);
  }

  return <a className="btn btn-outline ripple-effect"
    onClick={refresh} href="#">
    <LabelText text={
      isRefreshing ? 'Refreshing...' : 'Refresh'
    } />
  </a>;

}

/**
 * The button of creating cloud entity.
 *
 * @param menuTemplate The template of form menu.
 * @param cloudContext The Cloud Context.
 */
const ActionButtonGroup = ({ menuTemplate, cloudContext }: {
  menuTemplate: MenuTemplate,
  cloudContext: string
}) => {

  /**
   * Entity type that does not display Action buttons in ListView.
   */
  if (DONT_SHOW_ACTION_ENTITY_TYPE_LIST.filter((r) => {
    return r.cloudServiceProvider === menuTemplate.cloudServiceProvider
      && r.entityName === menuTemplate.entityName;
  }).length >= 1) {
    return <></>;
  }

  return <Form.Group>
    <ActionButtonList menuTemplate={menuTemplate} cloudContext={cloudContext} />
    <EntityListRefreshButton menuTemplate={menuTemplate} cloudContext={cloudContext} />
  </Form.Group>;

}

export default ActionButtonGroup;
