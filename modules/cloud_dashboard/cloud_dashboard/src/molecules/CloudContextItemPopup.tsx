import CloudContextItem from 'model/CloudContextItem';
import CloudContextItemPopupLabel from 'molecules/CloudContextItemPopupLabel';
import { Marker, Popup } from 'react-leaflet';

/**
 * Popup on CloudServiceProviderMap.
 *
 * @param item Information of CloudContext.
 */
const CloudContextItemPopup = ({ item }: {
  item: CloudContextItem
}) => {

  return <Marker position={item.position} icon={item.icon}>
    <Popup>
      {
        item.item.map((item2, index) => {
          return <CloudContextItemPopupLabel
            label={item2}
            key={index} />;
        })
      }
    </Popup>
  </Marker>;

};

export default CloudContextItemPopup;
