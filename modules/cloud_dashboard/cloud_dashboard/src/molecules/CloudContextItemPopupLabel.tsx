import Glyphicon from 'atoms/Glyphicon';
import CloudContextItemlabel from 'model/CloudContextItemLabel';
import { Link } from 'react-router-dom';

/**
 * Label on CloudContextItemPopup.
 *
 * @param label Labeling data of CloudContextItem.
 */
const CloudContextItemPopupLabel = ({ label }: {
  label: CloudContextItemlabel
}) => {

  return <div>
    <div>
      <Link to={label.entityViewUrl} target="_blank" rel="noreferrer">
        <img src={label.iconUrl} alt={label.name} />
      </Link>
    </div>
    <div>
      <strong>
        <Link to={label.entityViewUrl} target="_blank" rel="noreferrer">
          {label.name}
        </Link>
      </strong><br />
      <span className="location">
        <Glyphicon type="map-marker" />
        {label.positionLabel}
      </span>
    </div>
  </div>;

};

export default CloudContextItemPopupLabel;
