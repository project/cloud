import useDrupalJsonApi, { GetJsonDataType } from 'hooks/drupal_jsonapi';
import { useEffect } from 'react';
import { Form } from 'react-bootstrap';

/**
 * Get entity item's count.
 *
 * @param cloudContext Cloud context.
 * @param entityTypeId Entity type ID.
 * @param namespace Value of namespace.
 * @param namespaceName Value of namespace.
 * @returns Entity item's count.
 */
const getItemCount = async (
  getJsonData: GetJsonDataType,
  bundleId: string
) => {

  // Create URL for REST API.
  const url = `/cloud_dashboard/cloud_store/${bundleId}/count`;

  // Download data.
  return (await getJsonData<{ count: number }>(url, { count: 0 })).count;

};

/**
 * Label of cloud store item's count.
 * @param bundleId Bundle type ID.
 * @param itemCount resource item's count.
 * @param setItemCount Setter of itemCount.
 */
const CloudStoreCountLabel = ({ bundleId, itemCount, setItemCount }: {
  bundleId: string,
  itemCount: number,
  setItemCount: (n: number) => void
}) => {

  const { getJsonData } = useDrupalJsonApi();

  // Set entity item's count.
  useEffect(() => {
    getItemCount(getJsonData, bundleId).then((count) => {
      setItemCount(count);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <Form.Label>{
    Drupal.t('ItemCount: @itemCount', {'@itemCount': itemCount})
  }</Form.Label>;

};

export default CloudStoreCountLabel;
