import { Link } from 'react-router-dom';

/**
 * TD tag of DataTable.
 *
 * @param text Message.
 * @param link URL link for react-router-dom.
 */
const DataTableData = ({ text, link, dataId }: {
  text: string | number,
  link?: string,
  dataId?: string,
}) => {
  // For the text to be displayed, split the text at the line break position beforehand
  // so that any text is always processed to be enclosed in a "one line at a time" tag.
  const records = typeof text === 'string' && text.includes('\n')
    ? text.split('\n')
    : [`${text}`];

  // If you need to set a link, enclose the entire group of <span> tags in an <a> tag.
  // If not, do not use the <a> tag.
  const textSpanTagList = records.map((r, index) => {
    return index === 0
      ? <span key={index}>{r}</span>
      : <span key={index}><br />{r}</span>;
  });

  return typeof link === 'string'
    ? <td className="word-break-all">
      <Link to={link} id={`edit-entities-${dataId}-name-data`}>
        {textSpanTagList}
      </Link>
    </td>
    : <td className="word-break-all">
      {textSpanTagList}
    </td>;
}

export default DataTableData;
