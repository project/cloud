import { DataTableSelectedRowsContext } from 'hooks/data_table_selected_row';
import DataColumn from 'model/DataColumn';
import DataRecord from 'model/DataRecord';
import DataTableData from 'molecules/DataTableData';
import { useContext } from 'react';

/**
 * Row data of DataTable.
 *
 * @param dataRecord Record of data.
 * @param dataColumn List of DataColumn.
 * @param className Parameter of className.
 * @param detailInfo Information required to create a link to more information.
*/
const DataTableRow = ({ dataRecord, dataColumnList, className, detailInfo, isShowCheckBox }: {
  dataRecord: DataRecord,
  dataColumnList: DataColumn[],
  className?: string,
  detailInfo?: {
    column: string,
    path: string,
  },
  isShowCheckBox?: boolean,
}) => {

  const { selectedIdList, addId, removeId } = useContext(DataTableSelectedRowsContext);

  return <tr key={dataRecord.id} className={className}>
    {
      isShowCheckBox
        ? <td className="table-select">
          <div className="form-item form-type-checkbox">
            <input type="checkbox" className="form-checkbox form-check-input"
              checked={selectedIdList.includes(dataRecord.id)}
              onChange={() => {
                if (selectedIdList.includes(dataRecord.id)) {
                  removeId(dataRecord.id);
                } else {
                  addId(dataRecord.id);
                }
              }} />
          </div>
        </td>
        : <></>
    }
    {dataColumnList.map((dataColumn) => {
      let link = undefined;
      if (dataColumn.key === detailInfo?.column) {
        if ('url' in dataRecord.value) {
          link = dataRecord.value['url'];
        } else {
          const temp = detailInfo.path.split('/');
          if (temp.length >= 5) {
            link = `/${temp[1]}/${dataRecord.cloudContext}/${temp[2]}/${temp[3]}/${temp[4]}/${dataRecord.id}`;
          } else {
            link = temp[2] !== 'server_template'
              ? `/${temp[1]}/${dataRecord.cloudContext}/${temp[2]}/${dataRecord.id}`
              : `/design/server_template/${dataRecord.cloudContext}/${dataRecord.id}`;
          }
        }
      }

      return <DataTableData key={dataColumn.key} text={
        dataColumn.key in dataRecord.value
          ? dataRecord.value[dataColumn.key]
          : ''
      } link={link} dataId={dataRecord.id} />;
    })}
  </tr>;

}

export default DataTableRow;
