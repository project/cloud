import LabelText from 'atoms/LabelText';
import {
  DELETE_ONLY_OPERATIONS_ENTITY_TYPE_LIST, DONT_SHOW_OPERATIONS_ENTITY_TYPE_LIST, ROUTE_URL
} from 'constant/others/other';
import { DataTableSelectedRowsContext } from 'hooks/data_table_selected_row';
import useDrupalTranslation from 'hooks/drupal_translation';
import DataColumn from 'model/DataColumn';
import DataRecord from 'model/DataRecord';
import DataTableData from 'molecules/DataTableData';
import { useContext } from 'react';
import { ButtonGroup, Dropdown } from 'react-bootstrap';
import { getOperations } from 'service/convert';

/**
 * Wrapper of DataTableData.
 * @param dataColumn Column data of the element type.
 * @param dataRecord Data record of the element.
 * @param detailInfo Element type information.
 */
const DataTableDataWrapper = ({ dataColumn, dataRecord, detailInfo }: {
  dataColumn: DataColumn,
  dataRecord: DataRecord,
  detailInfo?: {
    column: string,
    path: string,
  },
}) => {
  let link = undefined;
  if (dataColumn.key === detailInfo?.column) {
    const temp = detailInfo.path.split('/');
    link = temp[2] !== 'server_template'
      ? `/${temp[1]}/${dataRecord.cloudContext}/${temp[2]}/${dataRecord.id}`
      : `/design/server_template/${dataRecord.cloudContext}/${dataRecord.id}`;
  }

  return <DataTableData key={dataColumn.key} text={
    dataColumn.key in dataRecord.value
      ? dataRecord.value[dataColumn.key]
      : ''
  } link={link} dataId={dataRecord.id} />;
}

/**
 * Action logic of updating and deleting.
 *
 * @param dataRecord Data record of the element.
 * @param action Type of CxUD.
 */
const updateAndDeleteData = (dataRecord: DataRecord, action: string) => {
  let urlParts = dataRecord.entityTypeId;
  const modules = ['aws_cloud', 'k8s', 'openstack', 'vmware'];
  for (const module of modules) {
    urlParts = urlParts.replaceAll(`${module}_`, `${module}/${dataRecord.cloudContext}/`);
  }
  window.location.href = dataRecord.entityTypeId !== 'cloud_launch_template'
    ? `${ROUTE_URL}/${urlParts}/${dataRecord.id}/${action}`
    : `${ROUTE_URL}/design/server_template/${dataRecord.cloudContext}/${dataRecord.id}/${action}`;
}

/**
 * Wrapper of Dropdown default item.
 * @param dataRecord Data record of the element.
 * @param entityId Entity id.
 * @param cloudContextList CloudContext list.
 */
const DropdownDefaultItem = (dataRecord: DataRecord, entityId: string) => {
  const { t } = useDrupalTranslation();

  // Entity that does not display Edit on Dropdown displays Delete by default.
  const defaultItems = !DELETE_ONLY_OPERATIONS_ENTITY_TYPE_LIST.includes(entityId)
    ? { label: 'Edit', action: 'edit' }
    : { label: 'Delete', action: 'delete' };

  return <button type="button" className="dropdown-toggle links ripple-effect" onClick={(e) => {
    e.preventDefault();
    updateAndDeleteData(dataRecord, defaultItems.action);
  }}>{t(defaultItems.label)}</button>;
}

/**
 * Row data of DataTable.
 * Note: The OperationLinks column is added.
 *
 * @param dataRecord Record of data.
 * @param dataColumn List of DataColumn.
 * @param className Parameter of className.
 * @param detailInfo Information required to create a link to more information.
*/
const DataTableRowWithOperationLinks = ({ dataRecord, dataColumnList, className, detailInfo, operationsLinks, isShowCheckBox }: {
  dataRecord: DataRecord,
  dataColumnList: DataColumn[],
  className?: string,
  detailInfo?: {
    column: string,
    path: string,
  },
  operationsLinks?: {
    label: string,
    path: string,
  }[],
  isShowCheckBox?: boolean,
}) => {

  const { t } = useDrupalTranslation();
  const { selectedIdList, addId, removeId } = useContext(DataTableSelectedRowsContext);

  if (DONT_SHOW_OPERATIONS_ENTITY_TYPE_LIST.includes(dataRecord.entityTypeId)) {
    return <tr key={dataRecord.id} className={className}>
      {
        isShowCheckBox
          ? <td className="table-select">
            <div className="form-item form-type-checkbox">
              <input type="checkbox" className="form-checkbox form-check-input"
                checked={selectedIdList.includes(dataRecord.id)}
                onChange={() => {
                  if (selectedIdList.includes(dataRecord.id)) {
                    removeId(dataRecord.id);
                  } else {
                    addId(dataRecord.id);
                  }
                }} />
            </div>
          </td>
          : <></>
      }
      {
        dataColumnList.map((dataColumn) => {
          return <DataTableDataWrapper
            dataColumn={dataColumn}
            dataRecord={dataRecord}
            detailInfo={detailInfo} />;
        })
      }
    </tr>;
  }

  return <tr key={dataRecord.id} className={className}>
    {
      isShowCheckBox
        ? <td className="table-select">
          <div className="form-item form-type-checkbox">
            <input type="checkbox" className="form-checkbox form-check-input"
              checked={selectedIdList.includes(dataRecord.id)}
              onChange={() => {
                if (selectedIdList.includes(dataRecord.id)) {
                  removeId(dataRecord.id);
                } else {
                  addId(dataRecord.id);
                }
              }} />
          </div>
        </td>
        : <></>
    }
    {
      dataColumnList.map((dataColumn) => {
        return <DataTableDataWrapper
          dataColumn={dataColumn}
          dataRecord={dataRecord}
          detailInfo={detailInfo} />;
      })
    }
    <td>
      <div className="dropbutton-wrapper dropbutton-multiple">
        <div className="dropbutton-widget">
          <Dropdown as={ButtonGroup} className="custom-dropdown">
            {
              operationsLinks && operationsLinks.length > 0
                ? <ul style={{ 'paddingLeft': 0, 'marginBottom': 0 }}>
                  {
                    operationsLinks.map((menuInfo) => {
                      const url = `${ROUTE_URL}${menuInfo.path.replace('{entityId}', dataRecord.id)}`;
                      return <li className="dropbutton-action secondary-action" key={url}>
                        <button className="dropdown-toggle links ripple-effect" type="button">
                          <a href={url}>
                            <LabelText text={menuInfo.label} />
                          </a>
                        </button>
                      </li>;
                    })
                  }
                </ul>
                : <></>
            }
            {
              operationsLinks === undefined
                ? DropdownDefaultItem(dataRecord, dataRecord.entityTypeId)
                : <></>
            }

            {operationsLinks === undefined && !DELETE_ONLY_OPERATIONS_ENTITY_TYPE_LIST.includes(dataRecord.entityTypeId) &&
              <button type="button" className="uparrow" data-bs-toggle="dropdown" aria-expanded="false">
                <span className="caret"></span>
              </button>
            }

            {
              operationsLinks === undefined
                ? <ul data-drupal-selector="edit-entities-1-operations-data"
                  className="dropbutton dropdown-menu"
                  style={{
                    position: 'absolute',
                    inset: '0px auto auto 0px',
                    margin: 0,
                    transform: 'translate3d(1424px, 444px, 0px)',
                  }}
                  data-popper-placement="bottom-start"
                >
                  <li className="dropbutton-action">
                    {/* eslint-disable-next-line  */}
                    <a href="#" onClick={(e) => {
                      e.preventDefault();
                      updateAndDeleteData(dataRecord, 'delete');
                    }}>
                      <LabelText text="Delete" />
                    </a>
                  </li>
                  <li className="dropbutton-toggle">
                    <button type="button"></button>
                  </li>
                  {
                    getOperations(dataRecord).map((menuInfo) => {
                      return <li className="dropbutton-action secondary-action" key={menuInfo.action}>
                        {/* eslint-disable-next-line  */}
                        <a href="#" onClick={(e) => {
                          e.preventDefault();
                          updateAndDeleteData(dataRecord, menuInfo.action);
                        }}>
                          {t(menuInfo.label)}
                        </a>
                      </li>;
                    })
                  }
                </ul>
                : <></>
            }
          </Dropdown>
        </div>
      </div>
    </td>
  </tr>;

}

export default DataTableRowWithOperationLinks;
