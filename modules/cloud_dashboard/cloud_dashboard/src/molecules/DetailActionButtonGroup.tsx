import Glyphicon from 'atoms/Glyphicon';
import LabelText from 'atoms/LabelText';
import ENTITY_FORM_LIST from 'constant/templates/form/entity_form_template';
import EntityData from 'model/EntityData';
import EntityInfoTemplate from 'model/EntityInfoTemplate';
import { Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { getOperationsImpl } from 'service/convert';
import { cspToLabel, entityNameToLabel } from 'service/string';

const ActionLinks = ({ cloudServiceProvider, cloudContext, entityName, entityId, operationActions }: {
  cloudServiceProvider: string,
  cloudContext: string,
  entityName: string,
  entityId: string,
  operationActions: { action: string, label: string, }[],
}) => {

  return <>
    {
      operationActions.map((record) => {
        const to = `/${cloudServiceProvider}/${cloudContext}/${entityName}/${entityId}/${record.action}`;
        const buttonColor = (record.action !== 'delete' && record.action !== 'terminate') ? 'primary' : 'danger';

        return <Link className={`btn btn-${buttonColor} mb-3`} key={record.action} to={to}>
          <LabelText text={record.label} />
        </Link>;
      })
    }
  </>;

}

const DetailActionButtonGroup = ({ detailTemplate, cloudContext, entityData, entityId }: {
  detailTemplate: EntityInfoTemplate,
  cloudContext: string,
  entityData?: EntityData,
  entityId: string
}) => {

  const operations = entityData
    ? getOperationsImpl(
        `${detailTemplate.cloudServiceProvider}_${detailTemplate.entityName}`,
        entityData.attributes
      )
    : [
      { action: 'edit', label: 'Edit' },
      { action: 'delete', label: 'Delete' },
    ];

  const validActionTypes = ENTITY_FORM_LIST.filter(
    (template) =>
      template.cloudServiceProvider === detailTemplate.cloudServiceProvider &&
      (template.entityName === detailTemplate.entityName ||
        template.entityName === '')
  ).map((template) => template.actionType);

  const operationActions = operations.filter((record) => validActionTypes.includes(record.action));

  return <Form.Group className="mb-4 local-actions">
    <Link className="btn btn-primary mb-3" to={
      detailTemplate.entityName !== 'cloud_launch_template'
        ? `/${detailTemplate.cloudServiceProvider}/${cloudContext}/${detailTemplate.entityName}`
        : `/design/server_template/${cloudContext}`
    }>
      <Glyphicon type="th-list" />
      {
        Drupal.t('List @cloudServiceProvider @entityName', {
          '@cloudServiceProvider': cspToLabel(detailTemplate.cloudServiceProvider),
          '@entityName': entityNameToLabel(detailTemplate.entityName),
        })
      }
    </Link>
    {
      entityData
        ? <ActionLinks cloudServiceProvider={detailTemplate.cloudServiceProvider}
          cloudContext={cloudContext}
          entityName={detailTemplate.entityName}
          entityId={entityId}
          operationActions={operationActions} />
        : <></>
    }
  </Form.Group>;

}

export default DetailActionButtonGroup;
