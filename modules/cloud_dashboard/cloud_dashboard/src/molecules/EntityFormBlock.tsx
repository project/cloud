import EntityFormColumn from 'model/EntityFormColumn';
import BooleanInputBlock from 'molecules/formblocks/BooleanInputBlock';
import DateTimeInputBlock from 'molecules/formblocks/DateTimeInputBlock';
import FileInputBlock from 'molecules/formblocks/FileInputBlock';
import HiddenBlock from 'molecules/formblocks/HiddenBlock';
import ImageSharedProjectsBlock from 'molecules/formblocks/ImageSharedProjectsBlock';
import JoinSelectBlock from 'molecules/formblocks/JoinSelectBlock';
import KeyValueTableBlock from 'molecules/formblocks/KeyValueTableBlock';
import LabelBlock from 'molecules/formblocks/LabelBlock';
import MultiItemInputBlock from 'molecules/formblocks/MultiItemInputBlock';
import MultiNumberInputBlock from 'molecules/formblocks/MultiNumberInputBlock';
import MultiStringInputBlock from 'molecules/formblocks/MultiStringInputBlock';
import NumberInputBlock from 'molecules/formblocks/NumberInputBlock';
import PasswordInputBlock from 'molecules/formblocks/PasswordInputBlock';
import PermissionInputBlock from 'molecules/formblocks/PermissionInputBlock';
import RadioSelectBlock from 'molecules/formblocks/RadioSelectBlock';
import SelectBlock from 'molecules/formblocks/SelectBlock';
import SelectTableBlock from 'molecules/formblocks/SelectTableBlock';
import StringInputBlock from 'molecules/formblocks/StringInputBlock';
import TableWithSortBlock from 'molecules/formblocks/TableWithSortBlock';
import TextareaBlock from 'molecules/formblocks/TextareaBlock';
import TimeSelectBlock from 'molecules/formblocks/TimeSelectBlock';
import UrlMultiCheckBox from 'molecules/formblocks/UrlMultiCheckBox';
import UrlMultiSelectBlock from 'molecules/formblocks/UrlMultiSelectBlock';
import UrlSelectBlock from 'molecules/formblocks/UrlSelectBlock';
import { convertDateString } from 'service/string';

const EntityFormBlock = ({ keyValueRecord, cloudContext, formData, setFormData, cloudServiceProvider }: {
  keyValueRecord: EntityFormColumn,
  cloudContext: string,
  formData: Record<string, any>,
  setFormData: (v: Record<string, any>) => void,
  cloudServiceProvider: string,
}) => {

  if (keyValueRecord.type === 'time') {
    const setHour = <T,>(v: T) => {
      const newFormData = JSON.parse(JSON.stringify(formData));
      newFormData[keyValueRecord.hourName] = v;
      setFormData(newFormData);
    }

    const setMinute = <T,>(v: T) => {
      const newFormData = JSON.parse(JSON.stringify(formData));
      newFormData[keyValueRecord.minuteName] = v;
      setFormData(newFormData);
    }

    return <TimeSelectBlock
      label={keyValueRecord.labelName}
      hour={formData[keyValueRecord.hourName]}
      setHour={setHour}
      minute={formData[keyValueRecord.minuteName]}
      setMinute={setMinute}
      required={!!keyValueRecord.required}
      readOnly={!!keyValueRecord.readOnly} />;
  }

  const name = keyValueRecord.type !== 'table-with-sort'
    ? keyValueRecord.name
    : '';
  const value = formData[name];
  if (value === undefined && keyValueRecord.type !== 'table-with-sort') {
    return <></>;
  }

  const setValue = <T,>(v: T) => {
    const newFormData: Record<string, any> = {...formData};
    newFormData[name] = v;
    setFormData(newFormData);
  }

  switch (keyValueRecord.type) {
    case 'default':
      return !!keyValueRecord.readOnly
        ? <LabelBlock
          name={keyValueRecord.labelName}
          value={value}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />
        : <StringInputBlock
          label={keyValueRecord.labelName}
          value={value}
          setValue={setValue}
          required={!!keyValueRecord.required}
          readOnly={!!keyValueRecord.readOnly}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />;
    case 'number':
      return !!keyValueRecord.readOnly
        ? <LabelBlock
          name={keyValueRecord.labelName}
          value={`${value}`}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />
        : <NumberInputBlock
          label={keyValueRecord.labelName}
          value={value}
          setValue={setValue}
          required={!!keyValueRecord.required}
          readOnly={!!keyValueRecord.readOnly}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />;
    case 'number-array':
      return <MultiNumberInputBlock
        label={keyValueRecord.labelName}
        value={value}
        setValue={setValue}
        defaultValue={keyValueRecord.defaultValue ?? []}
        required={!!keyValueRecord.required}
        readOnly={!!keyValueRecord.readOnly}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'join':
      return <JoinSelectBlock
        label={keyValueRecord.labelName}
        value={value}
        defaultValue={keyValueRecord.defaultValue ?? ''}
        setValue={setValue}
        entityTypeId={keyValueRecord.info.entityTypeId}
        keyColumn={keyValueRecord.info.keyColumn}
        optionLabel={keyValueRecord.info.label ?? '{name}'}
        required={!!keyValueRecord.required}
        readOnly={!!keyValueRecord.readOnly}
        cloudContext={cloudContext}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'select':
      return <UrlSelectBlock
        label={keyValueRecord.labelName}
        value={value}
        setValue={setValue}
        formData={formData}
        url={keyValueRecord.url}
        cloudContext={cloudContext}
        defaultValue={keyValueRecord.defaultValue ?? ''}
        required={!!keyValueRecord.required}
        readOnly={!!keyValueRecord.readOnly}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'select-local':
      return <SelectBlock
        label={keyValueRecord.labelName}
        value={value}
        defaultValue={keyValueRecord.defaultValue ?? ''}
        setValue={setValue}
        recordList={keyValueRecord.value.map((pair) => {
          return { label: pair.labelName, value: pair.name };
        })}
        required={!!keyValueRecord.required}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'select-table':
      return <SelectTableBlock
        label={keyValueRecord.labelName}
        value={value}
        setValue={setValue}
        url={keyValueRecord.url}
        cloudContext={cloudContext}
        recordKey={keyValueRecord.recordKey}
        sortKey={keyValueRecord.sortKey}
        column={keyValueRecord.column}
        defaultValue={keyValueRecord.defaultValue ?? ''}
        required={!!keyValueRecord.required}
        readOnly={!!keyValueRecord.readOnly}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'multi-select':
      return <UrlMultiSelectBlock
        label={keyValueRecord.labelName}
        value={value}
        setValue={setValue}
        formData={formData}
        url={keyValueRecord.url}
        cloudContext={cloudContext}
        required={!!keyValueRecord.required}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'multi-check':
      return <UrlMultiCheckBox
        label={keyValueRecord.labelName}
        value={value}
        setValue={setValue}
        formData={formData}
        url={keyValueRecord.url}
        cloudContext={cloudContext}
        required={!!keyValueRecord.required}
        keyType={keyValueRecord.keyType}
        readOnly={!!keyValueRecord.readOnly}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'textarea':
      return <TextareaBlock
        label={keyValueRecord.labelName}
        value={value}
        setValue={setValue}
        required={!!keyValueRecord.required}
        readOnly={!!keyValueRecord.readOnly}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'key-value':
      return <KeyValueTableBlock
        label={keyValueRecord.labelName}
        value={value}
        setValue={setValue}
        columnLabel={keyValueRecord.columnLabel}
        required={!!keyValueRecord.required}
        readOnly={!!keyValueRecord.readOnly}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'radio':
      return <RadioSelectBlock
        label={keyValueRecord.labelName}
        value={value}
        setValue={(s: string) => {
          setValue(s);
        }}
        valueList={keyValueRecord.value}
        orientation={keyValueRecord.orientation}
        required={!!keyValueRecord.required}
        readOnly={!!keyValueRecord.readOnly}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'datetime':
      return !!keyValueRecord.readOnly
        ? <LabelBlock
          name={keyValueRecord.labelName}
          value={convertDateString(value)}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />
        : <DateTimeInputBlock
          label={keyValueRecord.labelName}
          value={value}
          setValue={setValue}
          required={!!keyValueRecord.required}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />;
    case 'cost':
      return <LabelBlock
        name={keyValueRecord.labelName}
        value={`$${value}`}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'boolean':
      return !!keyValueRecord.readOnly
        ? <LabelBlock
          name={keyValueRecord.labelName}
          value={
            (typeof keyValueRecord.labels === 'undefined'
              ? ['True', 'False']
              : keyValueRecord.labels)[
            !!(value) ? 0 : 1
            ]
          } id={keyValueRecord.id}
          className={keyValueRecord.class} />
        : <BooleanInputBlock
          label={keyValueRecord.labelName}
          value={value}
          setValue={setValue}
          required={!!keyValueRecord.required}
          readOnly={!!keyValueRecord.readOnly}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />;
    case 'array':
      return !!keyValueRecord.readOnly
        ? <LabelBlock
          name={keyValueRecord.labelName}
          value={(Array.isArray(value) ? value : []).join(', ')}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />
        : <MultiStringInputBlock
          label={keyValueRecord.labelName}
          value={value}
          setValue={setValue}
          required={!!keyValueRecord.required}
          readOnly={!!keyValueRecord.readOnly}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />;
    case 'sg_permission':
      return <PermissionInputBlock
        label={keyValueRecord.labelName}
        value={value}
        setValue={setValue}
        cloudServiceProvider={cloudServiceProvider}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'file':
      return <FileInputBlock
        label={keyValueRecord.labelName}
        value={value}
        setValue={setValue}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'hidden':
      return <HiddenBlock
        name={keyValueRecord.name}
        value={value}
        defaultValue={keyValueRecord.defaultValue ?? ''}
        setValue={setValue}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    case 'item-array':
      return <MultiItemInputBlock
        label={keyValueRecord.labelName}
        setValue={setValue}
        value={Array.isArray(value) ? value : []}
        info={keyValueRecord.info}
        cloudContext={cloudContext}
        id={keyValueRecord.id}
        className={keyValueRecord.class}
      />;
    case 'password':
      return !!keyValueRecord.readOnly
        ? <LabelBlock
          name={keyValueRecord.labelName}
          value={value}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />
        : <PasswordInputBlock
          label={keyValueRecord.labelName}
          confirmLabel={keyValueRecord.confirmLabelName}
          value={value}
          setValue={setValue}
          required={!!keyValueRecord.required}
          readOnly={!!keyValueRecord.readOnly}
          id={keyValueRecord.id}
          className={keyValueRecord.class} />;
    case 'table-with-sort':
      return <TableWithSortBlock url={keyValueRecord.url}
        sortKey={keyValueRecord.sortKey}
        cloudServiceProvider={cloudServiceProvider}
        cloudContext={cloudContext}
        entityName={keyValueRecord.entityName}
        column={keyValueRecord.column} />;
    case 'image-shared-projects':
      return <ImageSharedProjectsBlock
        label={keyValueRecord.labelName}
        value={value}
        setValue={setValue}
        required={!!keyValueRecord.required}
        readOnly={!!keyValueRecord.readOnly}
        id={keyValueRecord.id}
        className={keyValueRecord.class} />;
    default:
      return <></>;
  }
}

export default EntityFormBlock;
