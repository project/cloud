import EntityData from 'model/EntityData';
import { Form } from 'react-bootstrap';
import { entityNameToLabel } from 'service/string';

const EntityFormLabel = ({ label, entityName, entityData }: {
  label: string,
  entityName: string,
  entityData?: EntityData
}) => {
  let outputLabel = label;
  while (true) {
    const result = outputLabel.match(/{{.+?}}/);
    if (result === null) {
      break;
    }
    if (result.length === 0) {
      continue;
    }
    const key = result[0].replace('{{', '').replace('}}', '');
    const data = entityData !== undefined && key in entityData.attributes
      ? `${entityData.attributes[key]}`
      : key === 'entityName'
        ? entityNameToLabel(entityName).replace(' ', '')
        : '';
    outputLabel = outputLabel.replace(result[0], `${data}`);
  }

  return <Form.Group className="form-item js-form-item">
    <Form.Label className="control-label">{outputLabel}</Form.Label>
  </Form.Group>;
}

export default EntityFormLabel;
