import Glyphicon from 'atoms/Glyphicon';
import { getMenuTemplateList } from 'constant/templates/menu/menu_template';
import CloudServiceProvider from 'model/CloudServiceProvider';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getEntityListViewUrl } from 'service/string';

/**
 * Tab elements for entity view.
 *
 * @param menuType Cloud service provider.
 * @param menuName Active tab name.
 */
const EntityTabs = ({ menuType, entityName, cloudContext }: {
  menuType: CloudServiceProvider,
  entityName: string,
  cloudContext?: string
}) => {

  const [tabColumnList, setTabColumnList] = useState<{
    location: string;
    entityName: string;
    title: string;
  }[]>([]);
  const [foldFlg, setFoldFlg] = useState(false);

  useEffect(() => {
    setTabColumnList(
      getMenuTemplateList(menuType)
        .map((menu) => {
          return {
            location: getEntityListViewUrl(menu, cloudContext),
            entityName: menu.entityName,
            title: menu.labelName,
          };
        })
    );
  }, [menuType, entityName, cloudContext]);

  return <nav className="tabs">
    <div className={'tabs-wrap' + (foldFlg ? '' : ' wrapflex')}>
      <ul className="nav nav-tabs" id="tab_wrap">
        {
          tabColumnList.map((tabColumn) => {

            return <li className={entityName === tabColumn.entityName ? 'active' : ''}>
              <Link to={tabColumn.location}
                className="active is-active ripple-effect">
                {tabColumn.title}
              </Link>
            </li>;
          })
        }
      </ul>
      { /* eslint-disable-next-line */ }
      <a href="#" className="tab-icon" id="right-button" onClick={(e) => {
        e.preventDefault();
        setFoldFlg(f => !f);
      }}>
        {
          foldFlg
            ? <Glyphicon type="chevron-down" unMargin />
            : <Glyphicon type="chevron-up" unMargin />
        }
      </a>
    </div>
  </nav>;

}

export default EntityTabs;
