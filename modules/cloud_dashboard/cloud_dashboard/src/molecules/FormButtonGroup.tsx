import React from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';

/**
 * Groups of button.
 *
 * @param buttonList List of buttons.
 */
const FormButtonGroup = ({ buttonList }: {
  buttonList: {
    disabled: boolean,
    onClick: React.MouseEventHandler<HTMLButtonElement>,
    value: string
  }[]
}) => {

  return <ButtonGroup>
    {
      buttonList.map((button, index) => {
        return <Button key={index} disabled={button.disabled} onClick={button.onClick}>
          {button.value}
        </Button>;
      })
    }
  </ButtonGroup>;

}

export default FormButtonGroup;
