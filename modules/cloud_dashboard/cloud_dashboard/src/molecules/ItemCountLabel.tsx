import useDrupalJsonApi, { GetJsonDataType } from 'hooks/drupal_jsonapi';
import { useEffect } from 'react';
import { Form } from 'react-bootstrap';

/**
 * Get entity item's count.
 *
 * @param cloudServiceProvider Cloud Service Provider.
 * @param cloudContext Cloud context.
 * @param entityTypeId Entity type ID.
 * @param namespace Value of namespace.
 * @param namespaceName Value of namespace.
 * @returns Entity item's count.
 */
const getItemCount = async (
  getJsonData: GetJsonDataType,
  cloudServiceProvider: string,
  cloudContext: string | undefined,
  entityTypeId: string,
  namespace: string,
  namespaceName: string
) => {

  // Create URL for REST API.
  const url = cloudContext === undefined
    ? `/cloud_dashboard/${cloudServiceProvider}/${entityTypeId}/count`
    : `/cloud_dashboard/${cloudServiceProvider}/${cloudContext}/${entityTypeId}/count`;
  const filter: { [key: string]: string; } = {};
  if (namespace !== '') {
    filter['namespace'] = namespace;
  }
  if (namespaceName !== '') {
    filter['namespace'] = namespaceName;
  }

  // Download data.
  return (await getJsonData<{ count: number }>(url, { count: 0 }, filter)).count;

};

/**
 * Label of entity item's count.
 * @param entityTypeId Entity type ID.
 * @param namespace Value of namespace.
 * @param namespaceName Value of namespace.
 * @param itemCount Entity item's count.
 * @param setItemCount Setter of itemCount.
 */
const ItemCountLabel = ({
  cloudServiceProvider,
  cloudContext,
  entityTypeId,
  namespace,
  namespaceName,
  itemCount,
  setItemCount
}: {
  cloudServiceProvider: string,
  cloudContext: string | undefined,
  entityTypeId: string,
  namespace: string,
  namespaceName: string,
  itemCount: number,
  setItemCount: (n: number) => void
}) => {

  const { getJsonData } = useDrupalJsonApi();

  // Set entity item's count.
  useEffect(() => {
    getItemCount(getJsonData, cloudServiceProvider, cloudContext, entityTypeId, namespace, namespaceName).then((count) => {
      setItemCount(count);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cloudServiceProvider, cloudContext, entityTypeId, namespace, namespaceName]);

  return <Form.Label>{
    Drupal.t('ItemCount: @itemCount', {'@itemCount': itemCount})
  }</Form.Label>;

};

export default ItemCountLabel;
