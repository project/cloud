import { Form } from 'react-bootstrap';

const ItemPerPageSelector = ({itemPerPage, setItemPerPage}: {
  itemPerPage: number,
  setItemPerPage: (n: number) => void,
}) => {

  return <Form.Group className="d-flex" style={{ marginTop: '2rem' }}>
    <Form.Label className="text-nowrap mt-1">{Drupal.t('Items per page')}</Form.Label>
    <Form.Select className="ms-3 w-auto" value={`${itemPerPage}`}
      onChange={(e) => {
      setItemPerPage(parseInt(e.currentTarget.value, 10));
    }}>
      <option value="10">10</option>
      <option value="15">15</option>
      <option value="20">20</option>
      <option value="25">25</option>
      <option value="50">50</option>
    </Form.Select>
  </Form.Group>;

}

export default ItemPerPageSelector;
