import { BypassAnimationContext } from 'hooks/bypass_animation';
import { useContext } from 'react';
import { Form } from 'react-bootstrap';
import { Grid } from 'react-loader-spinner';

const LoadingSpinner = ({ message }: {
  message?: string
}) => {

  const { bypassAnimation } = useContext(BypassAnimationContext);

  return <Form>
    <div className="d-flex">
      {
        bypassAnimation
          ? <></>
          : <Grid
            color="#00BFFF"
            height="2rem"
            width="2rem"
          />
      }
      <span style={{
        fontSize: '1.5rem',
        marginLeft: '1.5rem'
      }}>{
          message ? message : Drupal.t('Loading...')
        }</span>
    </div>
  </Form>;
}

export default LoadingSpinner;
