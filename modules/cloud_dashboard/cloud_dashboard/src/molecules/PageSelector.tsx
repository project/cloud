import { Pagination } from 'react-bootstrap';

/**
 * Page selector for EntityForm.
 * @param pageIndex Entity item's page index.
 * @param setPageIndex Setter of pageIndex.
 * @param itemCount Entity item's count.
 */
const PageSelector = ({ pageIndex, setPageIndex, itemCount, itemPerPage }: {
  pageIndex: number,
  setPageIndex: (n: number) => void,
  itemCount: number,
  itemPerPage: number
}) => {

  const pageCount = Math.floor(1.0 * (itemCount + itemPerPage - 1) / itemPerPage);
  const disabledPageBack = pageIndex === 0;
  const disabledPageForward = itemCount === 0 || pageIndex === pageCount - 1;

  if (pageCount <= 1) {
    return <></>;
  }

  return <Pagination>
    <Pagination.First className={disabledPageBack ? 'disabled' : ''} onClick={() => {
      setPageIndex(0);
    }} />
    <Pagination.Prev className={disabledPageBack ? 'disabled' : ''} onClick={() => {
      setPageIndex(Math.max(0, pageIndex - 1));
    }} />
    <Pagination.Item>{pageIndex + 1}</Pagination.Item>
    <Pagination.Next className={disabledPageForward ? 'disabled' : ''}  onClick={() => {
      setPageIndex(Math.min(pageIndex + 1, pageCount - 1));
    }} />
    <Pagination.Last className={disabledPageForward ? 'disabled' : ''}  onClick={() => {
      setPageIndex(pageCount - 1);
    }} />
  </Pagination>;

}

export default PageSelector;
