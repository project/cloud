import * as d3 from 'd3';

/**
 * The PieChart component using d3.js v7.
 * @param param0 
 */
const PieChart = ({ size, data, color }: {
  size: number,
  data: number[],
  color: string[],
}) => {

  const pie = d3.pie().sort(null);
  const arc = d3.arc().innerRadius(0).outerRadius(size / 2);

  const arcs = pie(data);

  return (
    <svg width={size} height={size}>
      <g transform={`translate(${size / 2},${size / 2})`}>
        {arcs.map((d, i) => (
          <path
            key={i}
            fill={color[i]}
            d={arc(d as any) as string}
          />
        ))}
      </g>
    </svg>
  );

}

export default PieChart;
