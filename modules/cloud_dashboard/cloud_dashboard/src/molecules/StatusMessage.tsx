import { StatusMessageContext } from 'hooks/status_message';
import Message from 'model/Message';
import { useContext } from 'react';
import { Link } from 'react-router-dom';

type MessageBlockType = {
  type: 'normal';
  text: string;
} | {
  type: 'link';
  text: string;
  link: string;
  'link-index': number;
}

/**
 * Calculate the message blocks for displaying with links.
 * @param message the message data.
 * @returns the message blocks.
 */
const createMessageBlocks = (message: Message) => {
  // Translate arguments for translation process.
  // i.e. {'@here': 'this URL'} => {'@here': '@@this URL@@'}
  const newArgs: Record<string, string> = {};
  for (const key in message.args) {
    newArgs[key] = `@@${message.args[key]}@@`;
  }

  // Translate message text.
  // i.e. 'This is @here' => 'This is @@this URL@@'
  const newText = Drupal.t(message.text, newArgs);

  // Split text to blocks.
  // i.e. 'This is @@this URL@@', {'@here': '@@this URL@@'}, {'@here': 'https://example.com'} => [
  //        { 'type': 'normal', 'text': 'This is ' },
  //        { 'type': 'link', 'text': 'this URL', 'link': 'https://example.com', 'link-index': 0 },
  //      ]
  let blocks: MessageBlockType[] = [
    {
      type: 'normal',
      text: newText,
    }
  ];
  for (const argPair of Object.entries(newArgs)) {
    let linkIndex = 0;
    const newBlocks: MessageBlockType[] = [];
    for (const block of blocks) {
      switch (block.type) {
        case 'normal': {
          const splitText = block.text.split(argPair[1]);
          for (let i = 0; i < splitText.length; i++) {
            newBlocks.push({
              type: 'normal',
              text: splitText[i],
            });
            if (i >= splitText.length - 1) {
              continue;
            }
            if (!(argPair[0] in message.links)) {
              newBlocks.push({
                type: 'normal',
                text: argPair[1].replace(/@@(.+)@@/, '$1'),
              });
              continue;
            }
            newBlocks.push({
              type: 'link',
              text: argPair[1].replace(/@@(.+)@@/, '$1'),
              link: message.links[argPair[0]],
              'link-index': linkIndex,
            });
            linkIndex += 1;
          }
          break;
        }
        case 'link': {
          newBlocks.push({
            type: 'link',
            text: block.text,
            link: block.link,
            'link-index': linkIndex,
          });
          linkIndex += 1;
          break;
        }
      }
    }
    blocks = newBlocks;
  }

  return blocks;
}

const MessageBlock = ({block}: {
  block: MessageBlockType
}) => {

  const text = block.text.replaceAll('&quot;', '"')
    .replace('<a href="/clouds/', '<a href="/clouds/dashboard/');

  switch (block.type) {
    case 'normal': {
      return <span dangerouslySetInnerHTML={{'__html': text}}></span>;
    }
    case 'link': {
      return text.includes('Download') || block.link.startsWith('http')
        ? <a style={{'textDecorationLine': 'underline'}}
            href={block.link}
            id={`link-${block['link-index'] + 1}`}
            download>
            {text}
          </a>
        : <Link style={{'textDecorationLine': 'underline'}}
            to={block.link}
            id={`link-${block['link-index'] + 1}`}>
            {text}
          </Link>;
    }
  }

}

/**
 * Calculate the message blocks for displaying with links.
 * @param message the message data.
 */
const createMultipleMessageBlocks = (message: Message) => {
  // Split the message text according to the line break character.
  const splitTexts = message.text.split('\n');
  const splitMessages: Message[] = splitTexts.map((splitText) => {
    return {
      ...message,
      text: splitText,
      };
  });

  // For each message, perform the conversion process.
  return splitMessages.map((splitMessage) => {
    return createMessageBlocks(splitMessage);
  });
}

const MessageBlocks = ({blocks}: { blocks: MessageBlockType[][] }) => {

  if (blocks.length === 0) {
    return <></>;
  }

  if (blocks.length === 1) {
    return <>
      {
        blocks[0].filter((block) => {
          return block.text !== '';
        }).map((block, index) => {
          return <MessageBlock key={index} block={block} />;
        })
      }
    </>;
  }

  return <ul>
    {
      blocks.map((blocks, index) => {
        return <li key={index}>
          {
            blocks.filter((block) => {
              return block.text !== '';
            }).map((block, index2) => {
              return <MessageBlock key={index2} block={block} />;
            })
          }
        </li>;
      })
    }
  </ul>;

}

const StatusMessage = ({message, removeMessage}: {
  message: Message,
  removeMessage: () => void,
}) => {

  // Calculate message blocks for displaying with links.
  const blocks = createMultipleMessageBlocks(message);

  // Calculate class text for displaying with background color.
  const classText = message.backgroundColor !== 'hidden'
    ? `alert alert-${message.backgroundColor} alert-dismissible`
    : 'alert alert-primary alert-dismissible d-none';

  // Display message.
  return <div className={classText} role="status" aria-label="status message">
    <button type="button" role="button" className="close" data-dismiss="alert" aria-label="close" onClick={() => {
      removeMessage();
    }}>
      <span aria-hidden="true">×</span>
    </button>
      <h2 className="sr-only">status message</h2>
      <p>
        <MessageBlocks blocks={blocks} />
      </p>
  </div>;

}

const StatusMessages = () => {

  const { messages, removeMessage } = useContext(StatusMessageContext);

  return <>
    {
      messages.map((message, index) => {
        return <StatusMessage key={index} message={message} removeMessage={() => {
          removeMessage(index);
        }} />;
      })
    }
  </>;

}

export default StatusMessages;
