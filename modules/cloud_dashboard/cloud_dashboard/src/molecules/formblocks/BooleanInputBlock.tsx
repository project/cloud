import { Form } from 'react-bootstrap';

/**
 * Block of key-value input view.
 *
 * @param
 */
const BooleanInputBlock = ({ label, value, setValue, required, readOnly, id, className }: {
  label: string,
  value: boolean,
  setValue: (v: boolean) => void,
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {
  return <div className={"form-item js-form-item " + ((className) ? className : '')}>
    <input className="form-check-input" type="checkbox" checked={value} id={id}
    onChange={() => {
      setValue(!value);
    }} readOnly={readOnly} />
    <Form.Label htmlFor={id} className={'option' + (required ? ' form-required' : '')}>{label}</Form.Label>
  </div>;
}

export default BooleanInputBlock;
