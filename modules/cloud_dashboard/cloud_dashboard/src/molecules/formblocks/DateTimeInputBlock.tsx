import { useEffect, useState } from 'react';
import { Form } from 'react-bootstrap';

/**
 * Block of key-value input view.
 *
 * @param
 */
const DateTimeInputBlock = ({ label, value, setValue, required, id, className }: {
  label: string,
  value: number,
  setValue: (s: number) => void,
  required: boolean,
  id?: string,
  className?: string,
}) => {

  const [date, setDate] = useState('');
  const [time, setTime] = useState('');

  useEffect(() => {
    if (value > 0) {
      const dateValue = new Date(value);
      setDate(`${dateValue.getFullYear()}-${dateValue.getMonth() + 1}-${dateValue.getDate()}`);
      setTime(`${dateValue.getHours()}:${dateValue.getMinutes()}`);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const divId = id ? `${id}-0-value` : undefined;
  const dateClass = className
    ? `form-item form-type-date js-form-item ${className}-0-value-date`
    : 'form-item form-type-date js-form-item';
  const timeClass = className
    ? `form-item form-type-date js-form-item ${className}-0-value-time`
    : 'form-item form-type-date js-form-item';
  const dateId = id ? `${id}-0-value-date` : undefined;
  const timeId = id ? `${id}-0-value-time` : undefined;

  return <Form.Group>
    <div id={divId}>
      <Form.Label className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
      <div className="form-inline container-inline">
        <div className={dateClass}>
          <input className="form-date form-control" type="date" value={date} id={dateId}
          onChange={(e) => {
            setDate(e.currentTarget.value);
            if (e.currentTarget.value !== '') {
              const temp = e.currentTarget.value.split('-');

              const dateValue = new Date(value);
              dateValue.setFullYear(
                parseInt(temp[0], 10),
                parseInt(temp[1], 10) - 1,
                parseInt(temp[2], 10)
              );
              setValue(dateValue.getTime());
            } else {
              setValue(0);
            }
          }} />
        </div>
        <div className={timeClass}>
          <input className="form-time form-control" type="time" value={time} id={timeId}
          onChange={(e) => {
            setTime(e.currentTarget.value);
            if (e.currentTarget.value !== '') {
              const temp = e.currentTarget.value.split(':');

              const dateValue = new Date(value);
              dateValue.setHours(parseInt(temp[0], 10));
              dateValue.setMinutes(parseInt(temp[1], 10));
              setValue(dateValue.getTime());
            } else {
              setValue(0);
            }
          }} />
        </div>
      </div>
    </div>
  </Form.Group>;

}

export default DateTimeInputBlock;
