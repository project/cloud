import { Form } from 'react-bootstrap';

/**
 * Block of key-value input view.
 *
 * @param
 */
const FileInputBlock = ({ label, value, setValue, id, className }: {
  label: string,
  value: File | null,
  setValue: (s: File) => void,
  id?: string,
  className?: string,
}) => {

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileList = e.target.files;
    if (fileList === null || fileList.length === 0) {
      return;
    }
    const file = fileList.item(0);
    if (file === null) {
      return;
    }
    setValue(file);
  };

  return <Form.Group className={'form-item js-form-item' + (className ? ' ' + className : '')}>
    <Form.Label htmlFor={id} className="control-label">{label}</Form.Label>
    <input type="file" className="js-form-file form-file" size={60} id={id} onChange={onChange} />
  </Form.Group>;

}

export default FileInputBlock;
