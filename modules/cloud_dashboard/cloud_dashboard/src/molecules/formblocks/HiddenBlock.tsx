import { Form } from 'react-bootstrap';

const HiddenBlock = ({name, value, defaultValue, setValue, id, className} : {
  name: string,
  value: string,
  defaultValue: string,
  setValue: (s: string) => void
  id?: string,
  className?: string,
}) => {

  return <Form.Group className={"form-item js-form-item" + (className ? ' ' + className : '')}>
    <Form.Control type="hidden" name={name} value={value || defaultValue} id={id} onChange={(e) => {
    setValue(e.currentTarget.value);
  }}/>
  </Form.Group>;
}

export default HiddenBlock;
