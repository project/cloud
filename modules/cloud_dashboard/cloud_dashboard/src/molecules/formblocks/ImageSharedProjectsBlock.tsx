import Glyphicon from 'atoms/Glyphicon';
import ImageSharedProjectsItem from 'model/ImageSharedProjectsItem';
import { createContext, useContext } from 'react';
import { Button, ButtonGroup, Form, Table } from 'react-bootstrap';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import EntityData from 'model/EntityData';
import { useEffect, useState } from 'react';

interface ImageSharedProjectsStore {
  ImageSharedProjectsList: ImageSharedProjectsItem[];
  changeItemCloudContext: (s: string, index: number) => void;
  changeItemProjectId: (s: string, index: number) => void;
  orderForward: (index: number) => void;
  orderBackward: (index: number) => void;
  addRecord: (index: number) => void;
  deleteRecord: (index: number) => void;
}

const useImageSharedProjects = (
  value: ImageSharedProjectsItem[],
  setValue: (l: ImageSharedProjectsItem[]) => void
): ImageSharedProjectsStore => {

  const getValue = (): ImageSharedProjectsItem[] => {
    if (!Array.isArray(value)) {
      return [];
    }

    return value.length >= 1
      ? value
      : [{ cloud_context: '', project_id: '', status: '' }];
  }

  const changeItemCloudContext = (s: string, index: number) => {
    const newImageSharedProjectsList: ImageSharedProjectsItem[] = JSON.parse(JSON.stringify(getValue()));
    newImageSharedProjectsList[index].cloud_context = s;
    setValue(newImageSharedProjectsList);
  }

  const changeItemProjectId = (s: string, index: number) => {
    const newImageSharedProjectsList: ImageSharedProjectsItem[] = JSON.parse(JSON.stringify(getValue()));
    newImageSharedProjectsList[index].project_id = s;
    setValue(newImageSharedProjectsList);
  }

  const orderForward = (index: number) => {
    const newImageSharedProjectsList: ImageSharedProjectsItem[] = JSON.parse(JSON.stringify(value));
    const tempKey = newImageSharedProjectsList[index].cloud_context;
    const tempValue = newImageSharedProjectsList[index].project_id;
    newImageSharedProjectsList[index].cloud_context = newImageSharedProjectsList[index + 1].cloud_context;
    newImageSharedProjectsList[index].project_id = newImageSharedProjectsList[index + 1].project_id;
    newImageSharedProjectsList[index + 1].cloud_context = tempKey;
    newImageSharedProjectsList[index + 1].project_id = tempValue;
    setValue(newImageSharedProjectsList);
  }

  const orderBackward = (index: number) => {
    const newImageSharedProjectsList: ImageSharedProjectsItem[] = JSON.parse(JSON.stringify(value));
    const tempKey = newImageSharedProjectsList[index].cloud_context;
    const tempValue = newImageSharedProjectsList[index].project_id;
    newImageSharedProjectsList[index].cloud_context = newImageSharedProjectsList[index - 1].cloud_context;
    newImageSharedProjectsList[index].project_id = newImageSharedProjectsList[index - 1].project_id;
    newImageSharedProjectsList[index - 1].cloud_context = tempKey;
    newImageSharedProjectsList[index - 1].project_id = tempValue;
    setValue(newImageSharedProjectsList);
  }

  const addRecord = (index: number) => {
    const newImageSharedProjectsList: ImageSharedProjectsItem[] = [];
    for (let i = 0; i < getValue().length; i += 1) {
      newImageSharedProjectsList.push(JSON.parse(JSON.stringify(getValue()[i])));
      if (i === index) {
        newImageSharedProjectsList.push({
          cloud_context: '',
          project_id: '',
          status: '',
        });
      }
    }
    setValue(newImageSharedProjectsList);
  }

  const deleteRecord = (index: number) => {
    const newImageSharedProjectsList: ImageSharedProjectsItem[] = [];
    for (let i = 0; i < value.length; i += 1) {
      if (i !== index) {
        newImageSharedProjectsList.push(JSON.parse(JSON.stringify(value[i])));
      }
    }
    setValue(newImageSharedProjectsList);
  }

  return {
    ImageSharedProjectsList: getValue(),
    changeItemCloudContext,
    changeItemProjectId,
    orderForward,
    orderBackward,
    addRecord,
    deleteRecord,
  }
}

const ImageSharedProjectsContext = createContext<ImageSharedProjectsStore>({
  ImageSharedProjectsList: [],
  changeItemCloudContext: () => { },
  changeItemProjectId: () => { },
  orderForward: () => { },
  orderBackward: () => { },
  addRecord: () => { },
  deleteRecord: () => { },
});

const ImageSharedProjectsTableRow = ({ ImageSharedProjects, index, readOnly, id, className }: {
  ImageSharedProjects: ImageSharedProjectsItem,
  index: number,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  const {
    ImageSharedProjectsList,
    changeItemCloudContext,
    changeItemProjectId,
    orderForward,
    orderBackward,
    addRecord,
    deleteRecord,
  } = useContext(ImageSharedProjectsContext);

  const cloudContextId = `${id}-${index}-cloud-context`;
  const projectId = `${id}-${index}-project-id`;
  const { getEntityListAll } = useDrupalJsonApi();
  const [dataList, setDataList] = useState<EntityData[]>([]);
  const cloudConfigEntityTypeId = 'cloud_config';
  const [fieldImageApiEndpoint, setFieldImageApiEndpoint] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        const entityDataList = await getEntityListAll(cloudConfigEntityTypeId, {}, 'openstack');
        setDataList(entityDataList);

        const CloudConfigEntityDataList = await getEntityListAll(cloudConfigEntityTypeId, {'filter[cloud_context]': ImageSharedProjects.cloud_context}, 'openstack');
        const fieldValue = CloudConfigEntityDataList.length > 0 ? CloudConfigEntityDataList[0].attributes['field_image_api_endpoint'] : '';
        setFieldImageApiEndpoint(fieldValue);
      } catch (error) {
        console.error(error);
        setFieldImageApiEndpoint('');
      }
    };

    fetchData();
  }, [ImageSharedProjects.cloud_context]);

  useEffect(() => {
    if (dataList.length >= 1 && ImageSharedProjects.cloud_context === '') {
      changeItemCloudContext(dataList[0].attributes['field_image_api_endpoint'], index);
    }
  }, [dataList]);

  return <tr>
    <td>
    <div className={"form-item js-form-item p-0 w-100" + (className ? ' ' + className : '')}>
      <Form.Label htmlFor={cloudContextId}>{Drupal.t('Cloud context')}</Form.Label>
        <Form.Select className="form-control" value={fieldImageApiEndpoint.length !== 0 ? fieldImageApiEndpoint : ImageSharedProjects.cloud_context} id={id}
        onChange={(e) => {
          changeItemCloudContext(e.currentTarget.value, index);
        }}>
          <option value="">{'- Select -'}</option>
          {
            dataList.map((d) => {
              return <option key={d.attributes['field_image_api_endpoint']} value={d.attributes['field_image_api_endpoint']}>{d.attributes['cloud_context']}</option>;
            })
          }
        </Form.Select>
    </div>

    </td>
    <td>
      <div className={"form-item js-form-item " + ((className) ? className : '')}>
        <Form.Label htmlFor={projectId}>{Drupal.t('Project ID')}</Form.Label>
        <Form.Control value={ImageSharedProjects.project_id} id={projectId}
          onChange={(e) => {
            changeItemProjectId(e.currentTarget.value, index);
          }} readOnly={readOnly} />
      </div>
    </td>
    <td>
      <ButtonGroup>
        <Button onClick={() => {
          orderBackward(index);
        }} disabled={index === 0 || readOnly}>
          <Glyphicon type="chevron-up" unMargin />
        </Button>
        <Button onClick={() => {
          orderForward(index);
        }} disabled={index === ImageSharedProjectsList.length - 1 || readOnly}>
          <Glyphicon type="chevron-down" unMargin />
        </Button>
        <Button onClick={() => {
          addRecord(index);
        }} disabled={readOnly}>
          <Glyphicon type="plus" unMargin />
        </Button>
        <Button onClick={() => {
          deleteRecord(index);
        }} disabled={ImageSharedProjectsList.length === 0 || readOnly} >
          <Glyphicon type="trash" unMargin />
        </Button>
      </ButtonGroup>
    </td>
  </tr>;
}

const ImageSharedProjectsBlock = ({ label, value, setValue, required, readOnly, id, className }: {
  label: string,
  value: ImageSharedProjectsItem[],
  setValue: (l: ImageSharedProjectsItem[]) => void,
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  const ImageSharedProjectsStore = useImageSharedProjects(value, setValue);

  if (!Array.isArray(value)) {
    return <></>;
  }

  return <div className="form-item js-form-item">
    <Table hover={true} striped={true} responsive={true}>
      <thead>
        <tr>
          <th colSpan={2} className={required ? 'form-required field-label' : 'field-label'}>{label}</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <ImageSharedProjectsContext.Provider value={ImageSharedProjectsStore}>
          {
            ImageSharedProjectsStore.ImageSharedProjectsList.map((ImageSharedProjects, index) => {
              return <ImageSharedProjectsTableRow ImageSharedProjects={ImageSharedProjects} index={index} readOnly={readOnly}
                id={id} className={className}
                />;
            })
          }
        </ImageSharedProjectsContext.Provider>
      </tbody>
    </Table>
  </div>;
}

export default ImageSharedProjectsBlock;
