import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import EntityData from 'model/EntityData';
import LabelBlock from 'molecules/formblocks/LabelBlock';
import SelectBlock from 'molecules/formblocks/SelectBlock';
import { useEffect, useState } from 'react';
import { getEntityDataNameLabel } from 'service/convert';

const convertLabel = (entityData: EntityData, labelFormat: string): string => {
  let output = labelFormat;
  for (const key in entityData.attributes) {
    output = output.replaceAll(`{${key}}`, entityData.attributes[key]);
  }
  return output;
}

/**
 * Block of select view.
 *
 * @param
 */
const JoinSelectBlock = ({ label, value, defaultValue, setValue, entityTypeId, keyColumn, optionLabel, required, readOnly, cloudContext, id, className }: {
  label: string,
  value: string,
  defaultValue: string,
  setValue: (s: string) => void,
  entityTypeId: string,
  keyColumn: string,
  optionLabel: string,
  required: boolean,
  readOnly: boolean,
  cloudContext: string,
  id?: string,
  className?: string,
}) => {

  const { getEntityListAll } = useDrupalJsonApi();
  const [dataList, setDataList] = useState<EntityData[]>([]);

  useEffect(() => {
    getEntityListAll(entityTypeId, {'filter[cloud_context]': cloudContext}).then((entityDataList) => {
      setDataList(entityDataList);
    })
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (readOnly) {
    // If the corresponding data exists in dataList,
    // use a notation such as "label name (key name)".
    // Otherwise, only the key name is used, e.g., "key name".
    const labelName = dataList
      .filter(
        (d) => {
          return d.attributes[keyColumn] === value;
        }
      )
      .map(
        (d) => getEntityDataNameLabel(d)
      )
      .find(
        (d) => d !== ''
      ) ?? value;

    return <LabelBlock
      name={label}
      value={labelName !== value ? `${labelName} (${value})` : value}
      id={id}
      className={className} />;
  }

  if (readOnly) {
    const filtered = dataList.filter((r) => r.attributes[keyColumn] === value);
    return <LabelBlock
      name={label}
      value={filtered.length >= 1 ? convertLabel(filtered[0], optionLabel) : value}
      id={id}
      className={className} />;
  }

  return <SelectBlock
    label={label}
    value={value}
    setValue={setValue}
    defaultValue={defaultValue}
    recordList={dataList.map((d) => {
      return { value: d.attributes[keyColumn], label: convertLabel(d, optionLabel) };
    })}
    required={required}
    id={id}
    className={className} />;

}

export default JoinSelectBlock;
