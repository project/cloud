import Glyphicon from 'atoms/Glyphicon';
import KeyValueItem from 'model/KeyValueItem';
import { createContext, useContext } from 'react';
import { Button, ButtonGroup, Form, Table } from 'react-bootstrap';

interface KeyValueStore {
  keyValueList: KeyValueItem[];
  changeItemKey: (s: string, index: number) => void;
  changeItemValue: (s: string, index: number) => void;
  orderForward: (index: number) => void;
  orderBackward: (index: number) => void;
  addRecord: (index: number) => void;
  deleteRecord: (index: number) => void;
}

const useKeyValue = (
  value: KeyValueItem[],
  setValue: (l: KeyValueItem[]) => void
): KeyValueStore => {

  const getValue = (): KeyValueItem[] => {
    if (!Array.isArray(value)) {
      return [];
    }

    return value.length >= 1
      ? value
      : [{ item_key: '', item_value: '' }];
  }

  const changeItemKey = (s: string, index: number) => {
    const newKeyValueList: KeyValueItem[] = JSON.parse(JSON.stringify(getValue()));
    newKeyValueList[index].item_key = s;
    setValue(newKeyValueList);
  }

  const changeItemValue = (s: string, index: number) => {
    const newKeyValueList: KeyValueItem[] = JSON.parse(JSON.stringify(getValue()));
    newKeyValueList[index].item_value = s;
    setValue(newKeyValueList);
  }

  const orderForward = (index: number) => {
    const newKeyValueList: KeyValueItem[] = JSON.parse(JSON.stringify(value));
    const tempKey = newKeyValueList[index].item_key;
    const tempValue = newKeyValueList[index].item_value;
    newKeyValueList[index].item_key = newKeyValueList[index + 1].item_key;
    newKeyValueList[index].item_value = newKeyValueList[index + 1].item_value;
    newKeyValueList[index + 1].item_key = tempKey;
    newKeyValueList[index + 1].item_value = tempValue;
    setValue(newKeyValueList);
  }

  const orderBackward = (index: number) => {
    const newKeyValueList: KeyValueItem[] = JSON.parse(JSON.stringify(value));
    const tempKey = newKeyValueList[index].item_key;
    const tempValue = newKeyValueList[index].item_value;
    newKeyValueList[index].item_key = newKeyValueList[index - 1].item_key;
    newKeyValueList[index].item_value = newKeyValueList[index - 1].item_value;
    newKeyValueList[index - 1].item_key = tempKey;
    newKeyValueList[index - 1].item_value = tempValue;
    setValue(newKeyValueList);
  }

  const addRecord = (index: number) => {
    const newKeyValueList: KeyValueItem[] = [];
    for (let i = 0; i < getValue().length; i += 1) {
      newKeyValueList.push(JSON.parse(JSON.stringify(getValue()[i])));
      if (i === index) {
        newKeyValueList.push({
          item_key: '',
          item_value: '',
        });
      }
    }
    setValue(newKeyValueList);
  }

  const deleteRecord = (index: number) => {
    const newKeyValueList: KeyValueItem[] = [];
    for (let i = 0; i < value.length; i += 1) {
      if (i !== index) {
        newKeyValueList.push(JSON.parse(JSON.stringify(value[i])));
      }
    }
    setValue(newKeyValueList);
  }

  return {
    keyValueList: getValue(),
    changeItemKey,
    changeItemValue,
    orderForward,
    orderBackward,
    addRecord,
    deleteRecord,
  }
}

const KeyValueContext = createContext<KeyValueStore>({
  keyValueList: [],
  changeItemKey: () => { },
  changeItemValue: () => { },
  orderForward: () => { },
  orderBackward: () => { },
  addRecord: () => { },
  deleteRecord: () => { },
});

const KeyValueTableRow = ({ keyValue, index, readOnly, id, className, keyLabel, valueLabel }: {
  keyValue: KeyValueItem,
  index: number,
  readOnly: boolean,
  id?: string,
  className?: string,
  keyLabel: string,
  valueLabel: string,
}) => {

  const {
    keyValueList,
    changeItemKey,
    changeItemValue,
    orderForward,
    orderBackward,
    addRecord,
    deleteRecord,
  } = useContext(KeyValueContext);

  const keyId = `${id}-${index}-item-key`;
  const valueId = `${id}-${index}-item-value`;

  return <tr>
    <td>
      <div className={"form-item js-form-item " + ((className) ? className : '')}>
        <Form.Label htmlFor={keyId}>{keyLabel}</Form.Label>
        <Form.Control value={keyValue.item_key} id={keyId}
          onChange={(e) => {
            changeItemKey(e.currentTarget.value, index);
          }} readOnly={readOnly} />
      </div>
    </td>
    <td>
      <div className={"form-item js-form-item " + ((className) ? className : '')}>
        <Form.Label htmlFor={valueId}>{valueLabel}</Form.Label>
        <Form.Control value={keyValue.item_value} id={valueId}
          onChange={(e) => {
            changeItemValue(e.currentTarget.value, index);
          }} readOnly={readOnly} />
      </div>
    </td>
    <td>
      <ButtonGroup>
        <Button onClick={() => {
          orderBackward(index);
        }} disabled={index === 0 || readOnly}>
          <Glyphicon type="chevron-up" unMargin />
        </Button>
        <Button onClick={() => {
          orderForward(index);
        }} disabled={index === keyValueList.length - 1 || readOnly}>
          <Glyphicon type="chevron-down" unMargin />
        </Button>
        <Button onClick={() => {
          addRecord(index);
        }} disabled={readOnly}>
          <Glyphicon type="plus" unMargin />
        </Button>
        <Button onClick={() => {
          deleteRecord(index);
        }} disabled={keyValueList.length === 0 || readOnly} >
          <Glyphicon type="trash" unMargin />
        </Button>
      </ButtonGroup>
    </td>
  </tr>;
}

const KeyValueTableBlock = ({ label, value, setValue, columnLabel, required, readOnly, id, className }: {
  label: string,
  value: KeyValueItem[],
  setValue: (l: KeyValueItem[]) => void,
  columnLabel?: { keyLabelName: string, valueLabelName: string },
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  const keyValueStore = useKeyValue(value, setValue);

  if (!Array.isArray(value)) {
    return <></>;
  }

  if (readOnly) {
    return <div className={`field ${className ?? ''} field--type-key-value field--label-hidden field--items`}>
      <div className="field--item">
        <div className="table-responsive">
          <table data-striping="1" className="table table-hover table-striped">
            <thead>
              <tr>
                <th>Key</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              {
                keyValueStore.keyValueList.map((keyValue, index) => {
                  return <tr key={index} className={index % 2 === 0 ? 'odd' : 'even'}>
                    <td>{keyValue.item_key}</td>
                    <td>{keyValue.item_value}</td>
                  </tr>;
                })
              }
            </tbody>
          </table>
        </div>
      </div>
    </div>;
  }

  return <div className="form-item js-form-item">
    <Table hover={true} striped={true} responsive={true}>
      <thead>
        <tr>
          <th colSpan={2} className={required ? 'form-required field-label' : 'field-label'}>{label}</th>
          {columnLabel !== undefined ? <th></th> : <th>Action</th>}
        </tr>
      </thead>
      <tbody>
        <KeyValueContext.Provider value={keyValueStore}>
          {
            keyValueStore.keyValueList.map((keyValue, index) => {
              return <KeyValueTableRow keyValue={keyValue} index={index} readOnly={readOnly}
                id={id} className={className}
                keyLabel={columnLabel !== undefined ? columnLabel.keyLabelName : Drupal.t('Key')}
                valueLabel={columnLabel !== undefined ? columnLabel.valueLabelName : Drupal.t('Value')}/>;
            })
          }
        </KeyValueContext.Provider>
      </tbody>
    </Table>
  </div>;
}

export default KeyValueTableBlock;
