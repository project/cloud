import { Form } from 'react-bootstrap';

const LabelBlock = ({name, value, id, className} : {
  name: string,
  value: string,
  id?: string,
  className?: string,
}) => {
  if (value === "'_none'") {
    return <></>;
  }

  return <Form.Group id={id} className={"form-item form-type-item " + ((className) ? className : '')}>
    <Form.Label htmlFor={id} className="control-label">{`${name} `}</Form.Label>
    {value === null ? '' : ` ${value}`}
  </Form.Group>;
}

export default LabelBlock;
