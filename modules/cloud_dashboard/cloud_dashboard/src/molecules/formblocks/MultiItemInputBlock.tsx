import Glyphicon from 'atoms/Glyphicon';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { InfoArraytype } from 'model/EntityFormColumn';
import { useState } from 'react';
import { Button, Col, Form, Row, Table } from 'react-bootstrap';
import { getEntityDataNameLabel } from 'service/convert';

type ItemBlock = Record<string, string>;

const StringInput = ({ label, placeholder, value, setValue, readOnly, id, className }: {
  label: string,
  placeholder?: string,
  value: string,
  setValue: (s: string) => void,
  readOnly?: boolean,
  id?: string,
  className?: string,
}) => {
  return <Col sm={3}>
    <div className={"form-item js-form-item p-0 w-100" + (className ? ' ' + className : '')}>
      <label htmlFor={id} className="control-label">{label}</label>
      <input className="form-text form-control" value={value} id={id} placeholder={placeholder}
        onChange={(e) => {
          setValue(e.currentTarget.value);
        }} readOnly={readOnly} />
    </div>
  </Col>;
}

const JoinInput = ({ label, value, setValue, entityTypeId, cloudContext, id, className }: {
  label: string,
  value: string,
  setValue: (s: string) => void,
  entityTypeId: string,
  cloudContext: string,
  id?: string,
  className?: string,
}) => {

  const [recordList, setRecordList] = useState<{ value: string, label: string }[]>([]);
  const [isLoading, setLoading] = useState(true);
  const { getEntityListAll } = useDrupalJsonApi();

  if (isLoading) {
    getEntityListAll(entityTypeId, {'filter[cloud_context]': cloudContext}).then((list) => {
      setRecordList(list.map((record) => {
        return {
          value: record.attributes['drupal_internal__id'],
          label: getEntityDataNameLabel(record),
        };
      }));
    });
    setLoading(false);
  }

  return <Col sm={3}>
    <div className={"form-item js-form-item p-0 w-100" + (className ? ' ' + className : '')}>
      <label htmlFor={id} className="control-label">{label}</label>
      <div className="select-wrapper">
        <Form.Select className="form-control" value={value} id={id} onChange={(e) => {
          setValue(e.currentTarget.value);
        }}>
          <option value="">{'- Select -'}</option>
          {
            recordList.map((d) => {
              return <option key={d.value} value={d.value}>{d.label}</option>;
            })
          }
        </Form.Select>
      </div>
    </div>
  </Col>;

}

const MultiSelectInput = ({ label, value, setValue, entityTypeId, cloudContext, id, className }: {
  label: string,
  value: string,
  setValue: (s: string[]) => void,
  entityTypeId: string,
  cloudContext: string,
  id?: string,
  className?: string,
}) => {

  const [recordList, setRecordList] = useState<{ value: string, label: string }[]>([]);
  const [isLoading, setLoading] = useState(true);
  const { getEntityListAll } = useDrupalJsonApi();
  const valueArray = value.split(",");

  if (isLoading) {
    getEntityListAll(entityTypeId, {'filter[cloud_context]': cloudContext}).then((list) => {
      setRecordList(list.map((record) => {
        return {
          value: record.attributes['drupal_internal__id'],
          label: getEntityDataNameLabel(record),
        };
      }));
    });
    setLoading(false);
  }

  return <Col sm={3}>
    <div className={"form-item js-form-item p-0 w-100" + (className ? ' ' + className : '')}>
      <label htmlFor={id} className="control-label">{label}</label>
      <div className="select-wrapper">
        <Form.Select style={{height: 142, backgroundImage: 'none'}} id={id}
                    multiple={true} className="form-control" value={valueArray} onChange={(e) => {
          const newValue: string[] = [];

          for (let i = 0; i < e.currentTarget.options.length; i++) {
            if (e.currentTarget.options[i].selected) {
              newValue.push(recordList[i].value);
            }
          }
          setValue(newValue);

        }}>
          {
            recordList.map((d) => {
              return <option key={d.value} value={d.value}>{d.label}</option>;
            })
          }
        </Form.Select>
      </div>
    </div>
  </Col>;

}

const ItemForm = ({ value, setValue, info, cloudContext, blockIndex, id, className }: {
  value: ItemBlock,
  setValue: (s: ItemBlock) => void,
  info: InfoArraytype[],
  cloudContext: string,
  blockIndex: number,
  id?: string,
  className?: string,
}) => {

  const setSingleValue = (key: string, s: string) => {
    const newValue: { [key: string]: any } = JSON.parse(JSON.stringify(value));
    newValue[key] = s;
    setValue(newValue as ItemBlock);
  }

  const setMultipleValue = (key: string, selectedItems: string[]) => {
    const newValue: { [key: string]: any } = JSON.parse(JSON.stringify(value));
    newValue[key] = selectedItems.join(',');
    setValue(newValue);
  }

  return <>
    {
      info.map((itemInfo, index) => {
        const formId = id ? `${id}-${blockIndex}-${itemInfo.name.replaceAll('_', '-')}` : undefined;
        const formClass = className ? `${className}-${blockIndex}-${itemInfo.name.replaceAll('_', '-')}` : undefined;
        switch (itemInfo.type) {
          case 'default':
            return <StringInput key={itemInfo.name}
              label={itemInfo.labelName}
              value={value[itemInfo.name]}
              setValue={(s) => setSingleValue(itemInfo.name, s)}
              readOnly={itemInfo.readOnly}
              id={formId}
              className={formClass}
            />;
          case 'join':
            return <JoinInput key={itemInfo.name}
              label={itemInfo.labelName}
              value={value[itemInfo.name]}
              setValue={(s) => setSingleValue(itemInfo.name, s)}
              entityTypeId={itemInfo.entityTypeId}
              cloudContext={cloudContext}
              id={formId}
              className={formClass}
            />;
          case 'multi-select':
            return <MultiSelectInput key={itemInfo.name}
              label={itemInfo.labelName}
              value={value[itemInfo.name]}
              setValue={(s) => setMultipleValue(itemInfo.name, s)}
              entityTypeId={itemInfo.entityTypeId}
              cloudContext={cloudContext}
              id={formId}
              className={formClass}
            />;
        }
      })
    }
  </>;

}

/**
 * Block of key-value input view.
 */
const MultiItemInputBlock = ({ label, value, setValue, info, cloudContext, id, className }: {
  label: string,
  value: ItemBlock[],
  setValue: (s: ItemBlock[]) => void,
  info: InfoArraytype[],
  cloudContext: string,
  id?: string,
  className?: string,
}) => {

  const setSingleValue = (item: ItemBlock, index: number) => {
    const newValue: ItemBlock[] = JSON.parse(JSON.stringify(value));
    newValue[index] = item;
    setValue(newValue);
  }

  const createNewItem = () => {
    const temp: Record<string, string> = {};
    for (const itemInfo of info) {
      temp[itemInfo.name] = '';
    }
    return temp;
  }

  const addItem = (index: number) => {
    const oldValue: ItemBlock[] = JSON.parse(JSON.stringify(value));
    if (oldValue.length === 0) {
      setValue([createNewItem()]);
      return;
    }
    const newValue: ItemBlock[] = [];
    for (let i = 0; i < oldValue.length; i += 1) {
      newValue.push(oldValue[i]);
      if (i === index) {
        newValue.push(createNewItem());
      }
    }
    setValue(newValue);
  }

  const moveUpItem = (index: number) => {
    const newValue: ItemBlock[] = JSON.parse(JSON.stringify(value));
    const value1 = JSON.parse(JSON.stringify(newValue[index - 1]));
    const value2 = JSON.parse(JSON.stringify(newValue[index]));
    newValue[index] = value1;
    newValue[index - 1] = value2;
    setValue(newValue);
  }

  const moveDownItem = (index: number) => {
    const newValue: ItemBlock[] = JSON.parse(JSON.stringify(value));
    const value1 = JSON.parse(JSON.stringify(newValue[index + 1]));
    const value2 = JSON.parse(JSON.stringify(newValue[index]));
    newValue[index] = value1;
    newValue[index + 1] = value2;
    setValue(newValue);
  }

  const deleteItem = (index: number) => {
    const newValue: ItemBlock[] = JSON.parse(JSON.stringify(value));
    const newValue2: ItemBlock[] = [];
    for (let i = 0; i < newValue.length; i++) {
      if (i !== index) {
        newValue2.push(newValue[i]);
      }
    }
    setValue(newValue2);
  }

  // If there are no item objects, initialize a blank one.
  // Otherwise, the "+" and up/down arrows do not show up.
  if (value.length === 0) {
    addItem(0);
  }

  return <div className="field--type-cidr-block mb-3">
    <div>
      <div>
        <Table responsive hover striped>
          <thead>
            <tr>
              <th className="field-label">
                <h4 className="label">{label}</h4>
              </th>
              <th>Operation</th>
            </tr>
          </thead>
          <tbody>
            {
              value.map((item, index) => {
                return <tr key={index}>
                  <div className="field-multiple-drag"></div>
                  <td className="d-block">
                    <Row key={index} className="mb-3">
                      <ItemForm value={item} info={info} cloudContext={cloudContext}
                        setValue={(data) => setSingleValue(data, index)}
                        id={id} className={className} blockIndex={index} />
                    </Row>
                  </td>
                  <td className="text-nowrap">
                    <Button className="mx-1"
                      onClick={() => addItem(index)}>
                      <Glyphicon type="plus" unMargin />
                    </Button>
                    <Button className="mx-1" disabled={index <= 0}
                      onClick={() => moveUpItem(index)}>
                      <Glyphicon type="chevron-up" unMargin />
                    </Button>
                    <Button className="mx-1"
                      disabled={index >= value.length - 1}
                      onClick={() => moveDownItem(index)}>
                      <Glyphicon type="chevron-down" unMargin />
                    </Button>
                    <Button className="mx-1"
                      onClick={() => deleteItem(index)}>
                      <Glyphicon type="trash" unMargin />
                      </Button>
                  </td>
                </tr>;
              })
            }
          </tbody>
        </Table>
      </div>
    </div>
  </div>
}

export default MultiItemInputBlock;
