import { Form } from 'react-bootstrap';

const MultiSelectBlock = ({
  label, value, setValue, recordList, required, id, className
}: {
  label: string,
  value: string[],
  setValue: (s: string[]) => void,
  recordList: { value: number, label: string }[],
  required: boolean,
  id?: string,
  className?: string
}) => {

  const divClassName = 'form-item js-form-item form-type-select' + ((className) ? ' ' + className : '');

  return <Form.Group className={divClassName}>
    <Form.Label htmlFor={id} className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
    {/** This is a dirty hack for problems specifying CSS */}
    <Form.Select style={{height: 142, backgroundImage: 'none'}}
      multiple={true} className="form-control" value={value} id={id}
      onChange={(e) => {
      const newValue: string[] = [];
      for (let i = 0; i < e.currentTarget.options.length; i++) {
        if (e.currentTarget.options[i].selected) {
          newValue.push(`${recordList[i].value}`);
        }
      }
      setValue(newValue);
    }}>
      {
        recordList.map((d) => {
          return <option key={d.value} value={d.value}>{d.label}</option>;
        })
      }
    </Form.Select>
  </Form.Group>;
}

export default MultiSelectBlock;
