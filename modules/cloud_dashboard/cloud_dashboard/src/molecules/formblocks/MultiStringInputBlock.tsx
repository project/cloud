import LabelText from 'atoms/LabelText';
import { Button, ButtonGroup, Form, Table } from 'react-bootstrap';

/**
 * Block of key-value input view.
 *
 * @param
 */
const MultiStringInputBlock = ({ label, value, setValue, required, readOnly, id, className }: {
  label: string,
  value: string[],
  setValue: (n: string[]) => void,
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  if (!Array.isArray(value)) {
    return <></>;
  }

  return <Form.Group className="form-item js-form-item">
    <Form.Label htmlFor={id} className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
    <Table striped>
      <tbody>
        {
          value.map((v, index) => {
            const indexedClass = className ? `form-item js-form-item ${className}-${index}-value` : 'form-item js-form-item';
            const indexedId = id ? `${id}-${index}-value` : undefined;
            return <tr key={index}>
              <td>
                <div className={indexedClass}>
                  <input className="form-text form-control" type="text" value={v} id={indexedId}
                  onChange={(e) => {
                    const newValue: string[] = JSON.parse(JSON.stringify(value));
                    newValue[index] = e.currentTarget.value;
                    setValue(newValue);
                  }} readOnly={readOnly} />
                </div>
              </td>
              <td>
                <ButtonGroup>
                  <Button onClick={() => {
                    const newValue: string[] = [];
                    for (let index2 = 0; index2 < value.length; index2 += 1) {
                      newValue.push(value[index2]);
                      if (index2 === index) {
                        newValue.push('');
                      }
                    }
                    setValue(newValue);
                  }} disabled={readOnly}>＋</Button>
                  <Button onClick={() => {
                    if (index === 0) {
                      return;
                    }
                    const newValue: string[] = JSON.parse(JSON.stringify(value));
                    newValue[index] = value[index - 1];
                    newValue[index - 1] = value[index];
                    setValue(newValue);
                  }} disabled={readOnly}>↑</Button>
                  <Button onClick={() => {
                    if (index === value.length - 1) {
                      return;
                    }
                    const newValue: string[] = JSON.parse(JSON.stringify(value));
                    newValue[index] = value[index + 1];
                    newValue[index + 1] = value[index];
                    setValue(newValue);
                  }} disabled={readOnly}>↓</Button>
                  <Button onClick={() => {
                    if (value.length === 1) {
                      setValue(['']);
                      return;
                    }
                    const newValue: string[] = [];
                    for (let index2 = 0; index2 < value.length; index2 += 1) {
                      if (index2 !== index) {
                        newValue.push(value[index2]);
                      }
                    }
                    setValue(newValue);
                  }} disabled={readOnly}>
                    <LabelText text="Delete" />
                  </Button>
                </ButtonGroup>
              </td>
            </tr>;
          })
        }
      </tbody>
    </Table>
  </Form.Group>;

}

export default MultiStringInputBlock;
