import { Form } from 'react-bootstrap';

/**
 * Block of key-value input view.
 *
 * @param
 */
const NumberInputBlock = ({ label, value, setValue, required, readOnly, id, className }: {
  label: string,
  value: number,
  setValue: (s: number) => void,
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  return <Form.Group className={'form-item js-form-item' + (className ? ' ' + className : '')}>
    <Form.Label htmlFor={id} className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
    <input className="form-text form-control" type="number" value={value} id={id}
      onChange={(e) => {
      setValue(parseInt(e.currentTarget.value, 10));
    }} readOnly={readOnly} />
  </Form.Group>;

}

export default NumberInputBlock;
