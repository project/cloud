import { useState } from 'react';
import { Form } from 'react-bootstrap';

/**
 * Validate password text.
 *
 * @param value The password text.
 * @param reValue The re-inputed password text.
 * @param required The required flag.
 * @returns If there are no problems, an empty string is returned.
 */
const validateText = (value: string, reValue: string, required: boolean) => {
  if ((value.length > 0 || reValue.length > 0) && (value !== reValue)) {
    return 'The specified passwords do not match.';
  }
  if (required && value.length === 0) {
    return 'Password field is required.';
  }

  return '';
}

/**
 * Block of key-value input view.
 */
const PasswordInputBlock = ({ label, confirmLabel, value, setValue, required, readOnly, id, className }: {
  label: string,
  confirmLabel: string,
  value: string,
  setValue: (s: string) => void,
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  const [reInputValue, setInputValue] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const setValueWrapper = (v: string) => {
    setValue(v);

    setErrorMessage(validateText(v, reInputValue, required));
  }

  const setInputValueWrapper = (v: string) => {
    setInputValue(v);

    setErrorMessage(validateText(value, v, required));
  }

  const id1 = id ? id + '-pass1' : '';
  const id2 = id ? id + '-pass2' : '';

  return <Form.Group id={id} className={'form-item js-form-item' + (className ? ' ' + className : '')}>
    <Form.Group className={'form-item js-form-item' + (className ? ' ' + className + '-pass1' : '')}>
      <Form.Label htmlFor={id1} className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
      <input className="form-text form-control" type="password" value={value} id={id1} onChange={(e) => {
        setValueWrapper(e.currentTarget.value);
      }} readOnly={readOnly} />
    </Form.Group>
    <Form.Group className={'form-item js-form-item' + (className ? ' ' + className + '-pass2' : '')}>
      <Form.Label htmlFor={id2} className={'control-label' + (required ? ' form-required' : '')}>{confirmLabel}</Form.Label>
      <input className="form-text form-control" type="password" value={reInputValue} id={id2}  onChange={(e) => {
        setInputValueWrapper(e.currentTarget.value);
      }} readOnly={readOnly} />
      {
        errorMessage !== ''
          ? <Form.Text className="text-danger fw-bold">
            Error: {errorMessage}
          </Form.Text>
          : <></>
      }
    </Form.Group>
  </Form.Group>;

}

export default PasswordInputBlock;
