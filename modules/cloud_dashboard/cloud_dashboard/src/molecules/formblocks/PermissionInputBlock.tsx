import Glyphicon from 'atoms/Glyphicon';
import SecurityGroupPermission from 'model/SecurityGroupPermission';
import { Button, Col, Row, Table } from 'react-bootstrap';
import {useState} from "react";
import useDrupalJsonApi from 'hooks/drupal_jsonapi';

type FormItemInfo = {
  type: 'string',
  label: string,
  placeholder?: string,
  key: string,
} | {
  type: 'select',
  label: string,
  itemList: { value: string, label: string }[],
  key: string,
}

type FormInfo = {
  source: 'ip4' | 'ip6' | 'group' | 'prefix',
  itemList: FormItemInfo[]
}

type WellknownProtocolItem = {
  name: string,
  ip_protocol: string,
  from_port: string,
  to_port: string
}

type WellknownProtocolItems = {
  [key: string]: WellknownProtocolItem;
}

const FORM_INFO_LIST: FormInfo[] = [
  {
    source: 'ip4',
    itemList: [
      { type: 'select', label: 'IP protocol', key: 'ip_protocol', itemList: [
        { value: '-1', label: 'All' },
        { value: 'tcp', label: 'TCP' },
        { value: 'udp', label: 'UDP' },
        { value: 'icmp', label: 'ICMP' },
        { value: 'icmpv6', label: 'ICMPv6' },
      ] },
      { type: 'string', label: 'From port', placeholder: '1', key: 'from_port' },
      { type: 'string', label: 'To port', placeholder: '65535', key: 'to_port' },
      { type: 'select', label: 'Source', key: 'source', itemList: [
        { value: 'ip4', label: 'IP' },
        { value: 'ip6', label: 'IPv6' },
        { value: 'group', label: 'Group' },
        { value: 'prefix', label: 'Prefix list Id' },
      ] },
      { type: 'string', label: 'CIDR IP', placeholder: '0.0.0.0/0', key: 'cidr_ip' },
      { type: 'string', label: 'Description', key: 'description' },
      { type: 'string', label: 'Rule ID', placeholder: '0', key: 'rule_id' },
    ]
  },
  {
    source: 'ip6',
    itemList: [
      { type: 'select', label: 'IP protocol', key: 'ip_protocol', itemList: [
        { value: '-1', label: 'All' },
        { value: 'tcp', label: 'TCP' },
        { value: 'udp', label: 'UDP' },
        { value: 'icmp', label: 'ICMP' },
        { value: 'icmpv6', label: 'ICMPv6' },
      ] },
      { type: 'string', label: 'From port', placeholder: '1', key: 'from_port' },
      { type: 'string', label: 'To port', placeholder: '65535', key: 'to_port' },
      { type: 'select', label: 'Source', key: 'source', itemList: [
        { value: 'ip4', label: 'IP' },
        { value: 'ip6', label: 'IPv6' },
        { value: 'group', label: 'Group' },
        { value: 'prefix', label: 'Prefix list Id' },
      ] },
      { type: 'string', label: 'CIDR IPv6', placeholder: '0.0.0.0/0', key: 'cidr_ip_v6' },
      { type: 'string', label: 'Description', key: 'description' },
      { type: 'string', label: 'Rule ID', placeholder: '0', key: 'rule_id' },
    ]
  },
  {
    source: 'group',
    itemList: [
      { type: 'select', label: 'IP protocol', key: 'ip_protocol', itemList: [
        { value: '-1', label: 'All' },
        { value: 'tcp', label: 'TCP' },
        { value: 'udp', label: 'UDP' },
        { value: 'icmp', label: 'ICMP' },
        { value: 'icmpv6', label: 'ICMPv6' },
      ] },
      { type: 'string', label: 'From port', placeholder: '1', key: 'from_port' },
      { type: 'string', label: 'To port', placeholder: '65535', key: 'to_port' },
      { type: 'select', label: 'Source', key: 'source', itemList: [
        { value: 'ip4', label: 'IP' },
        { value: 'ip6', label: 'IPv6' },
        { value: 'group', label: 'Group' },
        { value: 'prefix', label: 'Prefix list Id' },
      ] },
      { type: 'string', label: 'Group ID', key: 'group_id' },
      { type: 'string', label: 'Peering status', key: 'peering_status' },
      { type: 'string', label: 'Group user ID', key: 'user_id' },
      { type: 'string', label: 'VPC ID', key: 'vpc_id' },
      { type: 'string', label: 'Peering connection ID', key: 'peering_connection_id' },
      { type: 'string', label: 'Description', key: 'description' },
      { type: 'string', label: 'Rule ID', placeholder: '0', key: 'rule_id' },
    ]
  },
  {
    source: 'prefix',
    itemList: [
      { type: 'select', label: 'IP protocol', key: 'ip_protocol', itemList: [
        { value: '-1', label: 'All' },
        { value: 'tcp', label: 'TCP' },
        { value: 'udp', label: 'UDP' },
        { value: 'icmp', label: 'ICMP' },
        { value: 'icmpv6', label: 'ICMPv6' },
      ] },
      { type: 'string', label: 'From port', placeholder: '1', key: 'from_port' },
      { type: 'string', label: 'To port', placeholder: '65535', key: 'to_port' },
      { type: 'select', label: 'Source', key: 'source', itemList: [
        { value: 'ip4', label: 'IP' },
        { value: 'ip6', label: 'IPv6' },
        { value: 'group', label: 'Group' },
        { value: 'prefix', label: 'Prefix list Id' },
      ] },
      { type: 'string', label: 'Prefix list ID', placeholder: '', key: 'prefix_list_id' },
      { type: 'string', label: 'Description', key: 'description' },
      { type: 'string', label: 'Rule ID', placeholder: '0', key: 'rule_id' },
    ]
  }
]

const StringInput = ({ label, placeholder, value, setValue, id, className, name }: {
  label: string,
  placeholder?: string,
  value: string,
  setValue: (s: string) => void,
  id?: string,
  className?: string,
  name?: string,
}) => {
  return <div className={"form-item js-form-item" + (className ? ' ' + className : '')}>
    <label htmlFor={id} className="control-label">{label}</label>
    <input className="form-text form-control" value={value} id={id} placeholder={placeholder}
      onChange={(e) => {
      setValue(e.currentTarget.value);
    }} name={name} />
  </div>;
}

const SelectInput = ({ label, itemList, value, setValue, id, className, name }: {
  label: string,
  itemList: { value: string, label: string}[],
  value: string,
  setValue: (s: string) => void,
  id?: string,
  className?: string,
  name?: string,
}) => {
  return <div className={"form-item js-form-item" + (className ? ' ' + className : '')}>
    <label htmlFor={id} className="control-label">{label}</label>
    <div className="select-wrapper">
      <select className="form-select form-control" id={id} value={value}
        onChange={(e) => {
          setValue(e.currentTarget.value);
        }} name={name}>
          {
            itemList.map((item) => {
              return <option key={item.value} value={item.value}>{item.label}</option>;
            })
          }
      </select>
    </div>
  </div>;
}

const PermissionForm = ({ value, setValue, cloudServiceProvider, blockIndex, id, className }: {
  value: SecurityGroupPermission,
  setValue: (s: SecurityGroupPermission) => void,
  cloudServiceProvider: string,
  blockIndex: number,
  id?: string,
  className?: string,
}) => {
  const template = FORM_INFO_LIST.filter((info) => info.source === value.source);
  if (template.length === 0) {
    return <></>;
  }

  const itemCount = template[0].itemList.length;
  const width = 2;
  const rowCount = Math.ceil((itemCount + width - 1) / width);
  const setSingleValue = (key: string, s: string) => {
    const newValue: {[key: string]: any} = JSON.parse(JSON.stringify(value));
    newValue[key] = s;
    setValue(newValue as SecurityGroupPermission);
  }

  // Get a list of well-known protocols from the JSON API.
  const { getJsonData } = useDrupalJsonApi();
  const [isLoaded, setIsLoaded] = useState(false);
  const [wellknownProtocolList, setWellknownProtocolList] = useState<WellknownProtocolItems>({});
  const wellknownProtocolApiUrl = '/cloud_dashboard/openstack/wellknown_protocols';
  if (!isLoaded) {
    setIsLoaded(true);
    getJsonData<WellknownProtocolItems>(wellknownProtocolApiUrl, {}).then((jsonData) => {
      setWellknownProtocolList(jsonData);
    });
  }

  // Create a set of labels and values from the list of well-known protocols.
  const getWellknownProtocolLabels = () => {
    let protocolOptions: { value: string; label: string; }[] = [];
    Object.entries(wellknownProtocolList).map((item) => {
      protocolOptions.push({ value: item[0], label: item[1].name });
    })
    return protocolOptions;
  }

  // Manipulate PermissionForm.
  const WellknownProtocolInput = (inputValues: SecurityGroupPermission) => {
    let result: {isWellknown: boolean} = { isWellknown: false};
    const wellknownProtocolLabels = getWellknownProtocolLabels();

    wellknownProtocolLabels.map(protocol => {
      // Well-known protocol is selected.
      if (inputValues.ip_protocol === protocol.value) {
        const protocolItem = wellknownProtocolList[protocol.value];
        result = {isWellknown: true};

        // Change the selected port.
        inputValues.to_port = protocolItem.to_port
        inputValues.from_port = protocolItem.from_port
      }
    });

    if (!result.isWellknown && inputValues.ip_protocol !== 'icmp') {
      inputValues.to_port = inputValues.to_port === '-1' ? '' : inputValues.to_port;
      inputValues.from_port = inputValues.from_port === '-1' ? '' : inputValues.from_port;
    }

    return result;
  }

  return <>
    {
      [...Array(rowCount)].map((_, i) => i).map((row) => {
        return <>
          {
            template[0].itemList.map((item, index) => {
              if ((index < row * width) || ((row + 1) * width <= index)) {
                return <></>;
              }
              const formId = id ? `${id}-${blockIndex}-${item.key.replaceAll('_', '-')}` : undefined;
              const formClass = className ? `${className}-${blockIndex}-${item.key.replaceAll('_', '-')}` : undefined;
              const formName = id ? `${id.replace('edit-', '').replace('-', '_')}[${blockIndex}][${item.key}]` : undefined;
              const wellknownProtocolInfo = WellknownProtocolInput(value);
              switch (item.type) {
                case 'string':
                  if (cloudServiceProvider === 'openstack' && (item.key === 'to_port' || item.key === 'from_port') && wellknownProtocolInfo.isWellknown) {
                    return <></>;
                  }
                  return <StringInput label={item.label} placeholder={item.placeholder}
                    value={(value as {[key: string]: any})[item.key]}
                    setValue={(s) => setSingleValue(item.key, s)}
                    id={formId} className={formClass} name={formName} />;
                case 'select':
                  const itemList = cloudServiceProvider === 'openstack' && item.key === 'ip_protocol'
                    ? item.itemList.filter((column) => column.value !== '-1').concat(getWellknownProtocolLabels())
                    : item.itemList;
                  return <SelectInput label={item.label} itemList={itemList}
                    value={(value as {[key: string]: any})[item.key]}
                    setValue={(s) => setSingleValue(item.key, s)}
                    id={formId} className={formClass} name={formName} />;
                default:
                  return <></>;
              }
            })
          }
        </>;
      })
    }
  </>;
}

/**
 * Block of key-value input view.
 *
 * @param
 */
const PermissionInputBlock = ({ label, value, setValue, cloudServiceProvider, id, className }: {
  label: string,
  value: SecurityGroupPermission[],
  setValue: (s: SecurityGroupPermission[]) => void,
  cloudServiceProvider: string,
  id?: string,
  className?: string,
}) => {

  const setSingleValue = (sg: SecurityGroupPermission, index: number) => {
    const newValue: SecurityGroupPermission[] = JSON.parse(JSON.stringify(value));
    newValue[index] = sg;
    setValue(newValue);
  }

  const addPermission = (index: number) => {
    const newValue: SecurityGroupPermission[] = JSON.parse(JSON.stringify(value));
    newValue.push({
      cidr_ip: '',
      cidr_ip_v6: '',
      description: '',
      from_port: '',
      group_id: '',
      group_name: '',
      ip_protocol: '-1',
      peering_connection_id: '',
      peering_status: '',
      prefix_list_id: '',
      rule_id: '',
      source: 'ip4',
      to_port: '',
      user_id: '',
      vpc_id: '',
    });
    setValue(newValue);
  }

  const moveUpPermission = (index: number) => {
    const newValue: SecurityGroupPermission[] = JSON.parse(JSON.stringify(value));
    const value1 = JSON.parse(JSON.stringify(newValue[index - 1]));
    const value2 = JSON.parse(JSON.stringify(newValue[index]));
    newValue[index] = value1;
    newValue[index - 1] = value2;
    setValue(newValue);
  }

  const moveDownPermission = (index: number) => {
    const newValue: SecurityGroupPermission[] = JSON.parse(JSON.stringify(value));
    const value1 = JSON.parse(JSON.stringify(newValue[index + 1]));
    const value2 = JSON.parse(JSON.stringify(newValue[index]));
    newValue[index] = value1;
    newValue[index + 1] = value2;
    setValue(newValue);
  }

  const deletePermission = (index: number) => {
    const newValue: SecurityGroupPermission[] = JSON.parse(JSON.stringify(value));
    const newValue2: SecurityGroupPermission[] = [];
    for (let i = 0; i < newValue.length; i++) {
      if (i !== index) {
        newValue2.push(newValue[i]);
      }
    }
    setValue(newValue2);
  }

  // If there are no item objects, initialize a blank one.
  // Otherwise, the "+" and up/down arrows do not show up.
  if (value.length === 0) {
    addPermission(0);
  }

  return <div className={"field--type-ip-permission mb-3" + (className ? ' ' + className : '')} id={id ? id + '-wrapper' : ''}>
    <div>
      <div>
        <Table responsive hover striped>
          <thead>
            <tr>
              <th className="field-label">
                <h4 className="label">{label}</h4>
              </th>
              <th>Operation</th>
            </tr>
          </thead>
          <tbody>
            {
              value && value.map((sg, index) => {
                return <tr className={`draggable row-${index} ${index % 2 === 0 ? 'odd' : 'even'}`} key={index}>
                  <div className="field-multiple-drag"></div>
                  <td>
                    <PermissionForm value={sg} setValue={(data) => {
                      setSingleValue(data, index);
                    }} cloudServiceProvider={cloudServiceProvider} blockIndex={index} id={id} className={className} />
                  </td>
                  <td className="delta-order">
                    <div className="select-wrapper">
                      <select className="form-select form-control"
                        id={`${id}-${index}-weight`}
                        name={`${id}[0][_weight]`}>
                        <option value={`${index}`}>{index}</option>
                      </select>
                    </div>
                  </td>
                  <td>
                    <Button className="mx-1 my-1" onClick={() => addPermission(index)}>
                      <Glyphicon type="plus" unMargin />
                    </Button>
                    <Button className="mx-1 my-1" disabled={index <= 0} onClick={() => moveUpPermission(index)}>
                      <Glyphicon type="chevron-up" unMargin />
                    </Button>
                    <Button className="mx-1 my-1" disabled={index >= value.length - 1} onClick={() => moveDownPermission(index)}>
                      <Glyphicon type="chevron-down" unMargin />
                    </Button>
                    <a href="#remove-rule" className="remove-rule-button mx-1 my-1" onClick={() => deletePermission(index)}>
                      Remove rule
                    </a>
                  </td>
                </tr>;
              })
            }
          </tbody>
        </Table>
      </div>
    </div>
  </div>;

}

export default PermissionInputBlock;
