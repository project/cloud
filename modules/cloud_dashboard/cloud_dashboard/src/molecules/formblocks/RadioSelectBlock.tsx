import { Form } from 'react-bootstrap';

const VerticalRadioSelectBlock = ({ label, value, setValue, valueList, required, readOnly, id, className }: {
  label: string,
  value: string,
  setValue: (s: string) => void,
  valueList: { labelName: string, name: string }[],
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  return <Form.Group className="form-item js-form-item">
    <Form.Label className={required ? 'form-required' : ''}>{label}</Form.Label>
    <div className="form-radios" id={id}>
      {
        valueList.map((valueRecord, index) => {
          const radioClassName='form-item js-form-item form-type-radio' + (className ? ' ' + className : '');
          const radioId = id + '-' + index;
          return <div className={radioClassName} key={valueRecord.name}>
            <input className="form-radio" name={valueRecord.name} type="radio" value={valueRecord.name}
              checked={value === valueRecord.name}
              onChange={() => {
                setValue(valueRecord.name);
              }} readOnly={readOnly} id={radioId} />
            <Form.Label htmlFor={radioId} className="option" onClick={() => {
              setValue(valueRecord.name);
            }}>{Drupal.t(valueRecord.labelName)}</Form.Label>
          </div>;
        })
      }
    </div>
  </Form.Group>;

}

const HorizontalRadioSelectBlock = ({ label, value, setValue, valueList, required, readOnly, id, className }: {
  label: string,
  value: string,
  setValue: (s: string) => void,
  valueList: { labelName: string, name: string }[],
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  return <div className="container-inline">
    <b className={required ? 'form-required' : ''}>{`${label}: `}</b>
    <div className="form-radios" id={id}>
      {
        valueList.map((valueRecord, index) => {
          const radioClassName='form-item js-form-item form-type-radio' + (className ? ' ' + className : '');
          const radioId = id + '-' + index;
          return <div className={radioClassName} key={valueRecord.name}>
            <input className="form-radio" name={valueRecord.name} type="radio" value={valueRecord.name}
              checked={value === valueRecord.name}
              onChange={() => {
                setValue(valueRecord.name);
              }} readOnly={readOnly} id={radioId} />
            <Form.Label htmlFor={radioId} className="control-label option" onClick={() => {
              setValue(valueRecord.name);
            }}>
              {Drupal.t(valueRecord.labelName)}
            </Form.Label>
          </div>;
        })
      }
    </div>
  </div>;

}

/**
 * Block of key-value input view.
 *
 * @param
 */
const RadioSelectBlock = ({ label, value, setValue, valueList, orientation, required, readOnly, id, className }: {
  label: string,
  value: string,
  setValue: (s: string) => void,
  valueList: { labelName: string, name: string }[],
  orientation: 'horizontal' | 'vertical',
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  switch (orientation) {
    case 'vertical':
      return <VerticalRadioSelectBlock
        label={label}
        value={value}
        setValue={setValue}
        valueList={valueList}
        required={required}
        readOnly={readOnly}
        id={id}
        className={className} />;
    case 'horizontal':
      return <HorizontalRadioSelectBlock
        label={label}
        value={value}
        setValue={setValue}
        valueList={valueList}
        required={required}
        readOnly={readOnly}
        id={id}
        className={className} />;
  }

}

export default RadioSelectBlock;
