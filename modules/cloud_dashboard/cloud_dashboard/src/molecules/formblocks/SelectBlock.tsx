import { Form } from 'react-bootstrap';

const SelectBlock = ({
  label, value, defaultValue, setValue, recordList, required, id, className
}: {
  label: string,
  value: string,
  defaultValue: string,
  setValue: (s: string) => void,
  recordList: { value: string, label: string, group?: string }[],
  required: boolean,
  id?: string,
  className?: string,
}) => {
  // If the contents of the select box are not yet available.
  if (recordList.length === 0) {
    return <Form.Group className={"form-item js-form-item " + ((className) ? className : '')}>
      <Form.Label htmlFor={id} className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
      <div className="select-wrapper">
        <Form.Select className="form-control" id={id}>
          <option value="">{'- Select -'}</option>
        </Form.Select>
      </div>
    </Form.Group>;
  }

  // If the select box is not grouped.
  if (typeof recordList[0].group === 'undefined') {
    return <Form.Group className={"form-item js-form-item " + ((className) ? className : '')}>
      <Form.Label htmlFor={id} className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
      <div className="select-wrapper">
        <Form.Select className="form-control" value={value} id={id}
        onChange={(e) => {
          setValue(e.currentTarget.value);
        }}>
          <option value="">{'- Select -'}</option>
          {
            recordList.map((d) => {
              return <option key={d.value} value={d.value}>{d.label}</option>;
            })
          }
        </Form.Select>
      </div>
    </Form.Group>;
  }

  // If the select box is grouped.
  const groups: string[] = [];
  for (const record of recordList) {
    const group = record.group!!;
    if (!groups.includes(group)) {
      groups.push(group);
    }
  }

  return <Form.Group className={"form-item js-form-item " + ((className) ? className : '')}>
    <Form.Label htmlFor={id} className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
    <div className="select-wrapper">
      <Form.Select className="form-control" value={value || defaultValue} id={id}
      onChange={(e) => {
        setValue(e.currentTarget.value);
      }}>
        <option value="">{'- Select -'}</option>
        {
          groups.map((group) => {
            return <optgroup key={group} label={group}>
              {
                recordList
                  .filter((r) => r.group === group)
                  .map((d) => {
                    return <option key={d.value} value={d.value}>{d.label}</option>;
                  })
              }
            </optgroup>;
          })
        }
      </Form.Select>
    </div>
  </Form.Group>;
}

export default SelectBlock;
