import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import LabelBlock from 'molecules/formblocks/LabelBlock';
import { useState } from 'react';
import { Form, Table } from 'react-bootstrap';

const SelectTableBlock = ({
  label, value, setValue, url, cloudContext, recordKey, sortKey, column,
  defaultValue, required, readOnly, id, className
}: {
  label: string,
  value: string,
  setValue: (value: string) => void,
  url: string,
  cloudContext: string,
  recordKey: string,
  sortKey: string,
  column: {
    labelName: string,
    name: string,
  }[],
  defaultValue: string,
  required?: boolean,
  readOnly?: boolean,
  id?: string,
  className?: string,
}) => {

  const { getJsonData } = useDrupalJsonApi();
  const [recordList, serRecordList] = useState<Record<string, any>[]>([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [filterString, setFilterString] = useState('');
  const [showTable, setShowTable] = useState(false);

  if (!isLoaded) {
    setIsLoaded(true);
    const replacedUrl = url
      .replaceAll('{cloud_context}', cloudContext);

    getJsonData<Record<string, any>[]>(replacedUrl, []).then((jsonData) => {
      jsonData.sort((a, b) => {
        if (a[sortKey] < b[sortKey]) {
          return -1;
        }
        if (a[sortKey] > b[sortKey]) {
          return 1;
        }
        return 0;
      });

      serRecordList(jsonData);
    });
  }

  const getSelectedValue = () => {
    const selectedRecord = recordList.find((d) => {
      return d[recordKey] === value;
    });

    if (selectedRecord) {
      return selectedRecord['name'];
    }

    return '- Select a value -';
  }

  if (readOnly) {
    const filtered = recordList.filter((r) => r.id === `${value}`);
    return <LabelBlock
      name={label}
      value={filtered.length >= 1 ? filtered[0].name : value}
      id={id}
      className={className} />;
  }

  return <>
    <Form.Group className={"form-item js-form-item form-type-select" + ((className) ? ' ' + className : '') + ((showTable) ? '' : ' mb-3')}>
      <Form.Label htmlFor={id} className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
      <div className="select-wrapper d-none">
        <Form.Select className={`form-select ${required ? 'required' : ''} form-control mb-0`}
          value={value || defaultValue} id={id}
          onChange={(e) => {
            setValue(e.currentTarget.value);
          }}
          required={required}>
          <option value="">{'- Select a value -'}</option>
          {
            recordList.map((d) => {
              return <option key={d[recordKey]} value={d[recordKey]}>{d['name']}</option>;
            })
          }
        </Form.Select>
      </div>
      <Form.Control className="mb-0" value={getSelectedValue()} onClick={() => {
        setShowTable((s: boolean) => !s);
      }} readOnly />
    </Form.Group>
    {
      showTable
        ? <Form.Group className="border border-secondary p-1 mb-3">
          <input className="form-text form-control" type="text" value={filterString} id={id}
            onChange={(e) => {
              setFilterString(e.currentTarget.value);
            }} readOnly={readOnly} />
          <Table>
            <thead>
              <tr>
                {
                  column.map((c) => {
                    return <th key={c.name}>{c.labelName}</th>;
                  })
                }
              </tr>
            </thead>
            <tbody>
              <tr onClick={() => {
                setValue('');
                setShowTable(false);
              }}>
                <td colSpan={column.length}>
                  - Select a value -
                </td>
              </tr>
              {
                recordList.filter((d) => {
                  return d['name'].includes(filterString);
                }).map((d) => {
                  return <tr key={d[recordKey]} onClick={() => {
                    setValue(d[recordKey]);
                    setShowTable(false);
                  }}>
                    {
                      column.map((c) => {
                        return <td key={c.name}>{d[c.name]}</td>;
                      })
                    }
                  </tr>;
                })
              }
            </tbody>
          </Table>
        </Form.Group>
        : <></>
    }
  </>;

}

export default SelectTableBlock;
