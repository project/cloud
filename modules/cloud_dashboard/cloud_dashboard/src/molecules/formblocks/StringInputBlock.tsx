import { Form } from 'react-bootstrap';

/**
 * Block of key-value input view.
 *
 * @param
 */
const StringInputBlock = ({ label, value, setValue, required, readOnly, id, className }: {
  label: string,
  value: string,
  setValue: (s: string) => void,
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {
  return <Form.Group className={"form-item js-form-item " + ((className) ? className : '')}>
    <Form.Label htmlFor={id} className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
    <input className="form-text form-control" type="text" value={value} id={id}
    onChange={(e) => {
      setValue(e.currentTarget.value);
    }} readOnly={readOnly} />
  </Form.Group>;

}

export default StringInputBlock;
