import { CloudContextListContext } from 'hooks/cloud_context_list';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import DataColumn from 'model/DataColumn';
import DataRecord from 'model/DataRecord';
import EntityColumn from 'model/EntityColumn';
import SortInfo from 'model/SortInfo';
import LoadingSpinner from 'molecules/LoadingSpinner';
import DataTable from 'organisms/DataTable';
import { useContext, useEffect, useState } from 'react';
import { convertEntityDataToDataRecord } from 'service/convert';

const TableWithSortBlock = ({url, sortKey, cloudServiceProvider, cloudContext, entityName, column}: {
  url: string,
  sortKey: string,
  cloudServiceProvider: string,
  cloudContext: string,
  entityName: string,
  column: EntityColumn[]
}) => {

  const { cloudContextList } = useContext(CloudContextListContext);
  const { getJsonData, readDataCache } = useDrupalJsonApi();
  const [dataColumnList, setDataColumnList] = useState<DataColumn[]>([]);
  const [dataRecordList, setDataRecordList] = useState<DataRecord[]>([]);
  const [sortInfo, setSortInfo] = useState<SortInfo>({
    key: sortKey, direction: 'ASC'
  });
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    const init = async () => {
      // Load column data.
      let newDataColumnList: DataColumn[] = column.map((entityColumn) => {
        return { key: entityColumn.name, label: entityColumn.labelName };
      });
      setDataColumnList(newDataColumnList);

      // Cache the data you need.
      const dataCache = await readDataCache(column, cloudContext);

      // Load raw data.
      const replacedUrl = url
        .replaceAll('{cloud_context}', cloudContext);
      const rawData = await getJsonData<Record<string, any>[]>(replacedUrl, []);
      rawData.sort((a, b) => {
        if (a[sortInfo.key] < b[sortInfo.key]) {
          return sortInfo.direction === 'ASC' ? -1 : 1;
        }
        if (a[sortInfo.key] > b[sortInfo.key]) {
          return sortInfo.direction === 'ASC' ? 1 : -1;
        }
        return 0;
      });

      // Create data record's data.
      setDataRecordList(convertEntityDataToDataRecord('', rawData.map((record) => {
        return {
          id: 'id' in record ? record.id : '',
          attributes: record,
          relationships: {}
        }
      }), column, cloudContextList, dataCache));

      setLoading(false);
    }
    setLoading(true);
    init();
  }, [cloudContext, sortInfo, url, cloudContextList]);

  return isLoading
    ? <LoadingSpinner />
    : <DataTable
      dataColumnList={dataColumnList}
      dataRecordList={dataRecordList}
      sortInfo={sortInfo}
      setSortInfo={setSortInfo}
      hasOperationLinks={false}
      operationLinksName=""
      isShowCheckBox={false}
      detailInfo={{
        column: 'name',
        path: `/${cloudServiceProvider}/${cloudContext}/${entityName}`
      }} />;

}

export default TableWithSortBlock;
