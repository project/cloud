import { Form } from 'react-bootstrap';

/**
 * Block of key-value input view.
 *
 * @param
 */
const TextareaBlock = ({ label, value, setValue, required, readOnly, id, className }: {
  label: string,
  value: string,
  setValue: (s: string) => void,
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string
}) => {

  return <div className={"form-item js-form-item " + ((className) ? className : '')}>
    <Form.Label for={id} id={id} className={'control-label' + (required ? ' form-required' : '')}>{label}</Form.Label>
    <div className="form-textarea-wrapper">
      <textarea className={'resize-vertical form-textarea form-control' + (required ? ' required' : '')}
        rows={20} cols={60} value={value} id={id} onChange={(e) => {
          setValue(e.currentTarget.value);
        }} readOnly={readOnly} />
    </div>
    {
      label === 'Detail'
        ? <div className="description help-block">Enter entity detail.</div>
        : <></>
    }
  </div>;

}

export default TextareaBlock;
