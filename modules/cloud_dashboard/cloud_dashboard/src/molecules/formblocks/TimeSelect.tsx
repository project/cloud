import { Form } from 'react-bootstrap';
import { paddingZero } from 'service/string';

const TimeSelect = ({ time, setTime, size, readOnly }: {
  time: string,
  setTime: (s: string) => void,
  size: number,
  readOnly: boolean
}) => {

  return <Form.Select value={time} onChange={(e) => {
    setTime(e.currentTarget.value);
  }} disabled={readOnly}>
    {
      [...Array(size)]
        .map((_, i) => {
          return <option key={i} value={`${i}`}>{paddingZero(i, 2)}</option>
        })
    }
  </Form.Select>;

}

export default TimeSelect;
