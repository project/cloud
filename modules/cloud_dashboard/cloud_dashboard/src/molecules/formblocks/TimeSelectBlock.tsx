import TimeSelect from 'molecules/formblocks/TimeSelect';

/**
 * Block of select view.
 *
 * @param
 */
const TimeSelectBlock = ({ label, hour, setHour, minute, setMinute, required, readOnly }: {
  label: string,
  hour: string,
  setHour: (s: string) => void,
  minute: string,
  setMinute: (s: string) => void,
  required: boolean,
  readOnly: boolean,
}) => {

  return <>
    <div className="form-item js-form-item form-type-item">
      <span className={'control-label' + (required ? ' form-required' : '')}>{label}</span>
    </div>
    <div className="container-inline mb-4">
      <div className="form-item js-form-item">
        <TimeSelect time={hour} setTime={setHour} size={24} readOnly={readOnly} />
      </div>
      <div className="mx-1 mt-1">{' : '}</div>
      <div className="form-item js-form-item">
        <TimeSelect time={minute} setTime={setMinute} size={60} readOnly={readOnly} />
      </div>
    </div>
  </>;

}

export default TimeSelectBlock;
