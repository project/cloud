import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import LabelBlock from 'molecules/formblocks/LabelBlock';
import { useMemo, useState } from 'react';
import { Form } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

/**
 * Get the record data from the URL.
 * @param cloudContext
 * @param entityId
 * @param formData
 * @param url
 * @returns recordList, isLoading
 */
const useRecordData = (
  cloudContext: string,
  entityId: string,
  formData: Record<string, any>,
  url: string,
) => {

  const { getJsonData } = useDrupalJsonApi();
  const [recordList, setRecordList] = useState<{ value: number | string, label: string, group?: string }[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  // Replace the part of the URL that corresponds to the keyword.
  const replacedUrl = useMemo(() => {
    let newUrl = url
      .replaceAll('{cloud_context}', cloudContext)
      .replaceAll('{entity_id}', entityId);

    const result = newUrl.match(/\{[^{}]+\}/g);
    if (result !== null) {
      for (const pattern of result) {
        const key = pattern.substring(1, pattern.length - 1);
        newUrl = newUrl.replaceAll(pattern, `${formData[key]}`);
      }
    }

    return newUrl.endsWith('/') ? newUrl.slice(0, -1) : newUrl;
  }, [cloudContext, entityId, url, formData]);

  if (isLoading) {
    getJsonData<{ value: number | string, label: string, group?: string }[]>(replacedUrl, []).then((jsonData) => {
      setRecordList(jsonData);
      setIsLoading(false);
    })
  }

  return {
    recordList,
    isLoading,
  }

}

/**
 * Block of select view implementation.
 */
const UrlMultiCheckBoxImpl = ({
  label, value, setValue, recordList, required, keyType, readOnly, id, className
}: {
  label: string,
  value: string[],
  setValue: (s: string[]) => void,
  recordList: { value: number | string, label: string, group?: string }[],
  required: boolean,
  keyType: 'value' | 'label',
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  if (readOnly) {
    const valuesForLabel = recordList
      .filter(r => keyType === 'value'
        ? value.includes(r.value.toString())
        : value.includes(r.label))
      .map(r => r.label);

    return <LabelBlock
      name={label}
      value={
        valuesForLabel.length >= 1
          ? valuesForLabel.join(', ')
          : value.join(', ')
      }
      id={id}
      className={className} />;
  }

  const divClassName = `form-item js-form-item form-type-item ${className || ''}`;

  return <>
    <div className={divClassName}>
      <Form.Label htmlFor={id} className={`control-label ${required ? 'form-required' : ''}`}>{label}</Form.Label>
    </div>
    <div className="form-group js-form-wrapper form-wrapper">
      <fieldset className={`fieldgroup form-composite form-item js-form-item form-wrapper ${required ? 'required' : ''}`}>
        <legend className="d-none">
          <span className={`fieldset-legend ${required ? 'form-required' : ''}`}>{label}</span>
        </legend>
        <div className="fieldset-wrapper">
          <div className="form-checkboxes">
            {
              recordList.map((d, index) => {
                const idText = `${id || 'multi-select'}-${index}`;
                const keyValue = keyType === 'label' ? d.label : d.value.toString();
                const isChecked = value.includes(keyValue);
                const updatedValue = isChecked
                  ? value.filter(v => v !== keyValue)
                  : [...value, keyValue];

                return <div key={d.label} className="form-item form-type-checkbox">
                  <input type="checkbox" className="form-checkbox form-check-input" id={idText}
                    checked={isChecked}
                    onClick={() => {setValue(updatedValue)}} />
                  <Form.Label className="control-label option"
                    htmlFor={idText}>
                    <span className="views-field views-field-name">
                      <span className="field-content">
                        {d.label}
                      </span>
                    </span>
                  </Form.Label>
                </div>;
              })
            }
          </div>
        </div>
      </fieldset>
    </div>
  </>;

}

/**
 * Block of select view.
 *
 * @param
 */
const UrlMultiCheckBox = ({ label, value, setValue, formData, url, cloudContext, required, readOnly = false, keyType, id, className }: {
  label: string,
  value: string[],
  setValue: (s: string[]) => void,
  formData: Record<string, any>,
  url: string,
  cloudContext: string,
  required: boolean,
  keyType: 'value' | 'label',
  readOnly?: boolean,
  id?: string,
  className?: string
}) => {

  const params = useParams<{ entityId: string }>();

  const { recordList, isLoading } = useRecordData(cloudContext, params.entityId, formData, url);

  return !isLoading
    ? <UrlMultiCheckBoxImpl
      label={label}
      value={value}
      setValue={setValue}
      recordList={recordList}
      required={required}
      keyType={keyType}
      readOnly={readOnly}
      id={id}
      className={className} />
    : <></>;

}

export default UrlMultiCheckBox;
