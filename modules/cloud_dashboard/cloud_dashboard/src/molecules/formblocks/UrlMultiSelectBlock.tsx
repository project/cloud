import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import MultiSelectBlock from 'molecules/formblocks/MultiSelectBlock';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

/**
 * Block of select view.
 *
 * @param
 */
const UrlMultiSelectBlock = ({ label, value, setValue, formData, url, cloudContext, required, id, className }: {
  label: string,
  value: string[],
  setValue: (s: string[]) => void,
  formData: Record<string, any>,
  url: string,
  cloudContext: string,
  required: boolean,
  id?: string,
  className?: string
}) => {

  const params: {
    cloudServiceProvider: string,
    cloudContext: string,
    entityName: string,
    entityId: string,
    action: string
  } = useParams();

  const { getJsonData } = useDrupalJsonApi();
  const [recordList, setRecordList] = useState<{ value: number, label: string, group?: string }[]>([]);

  useEffect(() => {
    // Replace the part of the URL that corresponds to the keyword.
    let replacedUrl = url
      .replaceAll('{cloud_context}', cloudContext)
      .replaceAll('{entity_id}', params.entityId);
    const result = replacedUrl.match(/\{[^{}]+\}/g);
    if (result !== null) {
      for (const pattern of result) {
        const key = pattern.substring(1, pattern.length - 1);
        replacedUrl = replacedUrl.replaceAll(pattern, `${formData[key]}`);
      }
    }
    if (replacedUrl.endsWith('/')) {
      replacedUrl = replacedUrl.substring(0, replacedUrl.length - 1);
    }
    try {
      getJsonData<{ value: number, label: string, group?: string }[]>(replacedUrl, []).then((jsonData) => {
        setRecordList(jsonData);
        if (value.length !== 0) {
          const valueSet = new Set();
          for (const r of jsonData) {
            valueSet.add(r.value);
          }
          const newValue: string[] = [];
          for (const v of value) {
            if (valueSet.has(v)) {
              newValue.push(v);
            }
          }
          if (value.length !== newValue.length) {
            setValue(newValue);
          }
        }
      })
    } catch {
      console.group('UrlMultiSelectBlock');
      console.error('URL : ' + replacedUrl);
      console.error('Error:', 'Can\'t read data by URL');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cloudContext, formData]);

  return <MultiSelectBlock
    label={label}
    value={value}
    setValue={setValue}
    recordList={recordList}
    required={required}
    id={id}
    className={className} />;
}

export default UrlMultiSelectBlock;
