import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import LabelBlock from 'molecules/formblocks/LabelBlock';
import SelectBlock from 'molecules/formblocks/SelectBlock';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

/**
 * Block of select view.
 *
 * @param
 */
const UrlSelectBlock = ({ label, value, defaultValue, setValue, formData, url, cloudContext, required, readOnly, id, className }: {
  label: string,
  value: string,
  defaultValue: string,
  setValue: (s: string) => void,
  formData: Record<string, any>,
  url: string,
  cloudContext: string,
  required: boolean,
  readOnly: boolean,
  id?: string,
  className?: string,
}) => {

  const params: {
    cloudServiceProvider: string,
    cloudContext: string,
    entityName: string,
    entityId: string,
    action: string
  } = useParams();

  const { getJsonData } = useDrupalJsonApi();
  const [recordList, setRecordList] = useState<{ value: string, label: string, group?: string }[]>([]);

  useEffect(() => {
    // Replace the part of the URL that corresponds to the keyword.
    let replacedUrl = url
      .replaceAll('{cloud_context}', cloudContext)
      .replaceAll('{entity_id}', params.entityId);
    const result = replacedUrl.match(/\{[^{}]+\}/g);
    if (result !== null) {
      for (const pattern of result) {
        const key = pattern.substring(1, pattern.length - 1);
        replacedUrl = replacedUrl.replaceAll(pattern, `${formData[key]}`);
      }
    }

    if (!replacedUrl.endsWith('/')) {
      try {
        getJsonData<{ value: string, label: string, group?: string }[]>(replacedUrl, []).then((jsonData) => {
          setRecordList(jsonData);

          // Baldwin: commented out because setValue resets the formData.
          // if (value !== '' && jsonData.filter(record => record.value === value).length === 0) {
          //   setValue('');
          // }
        })
      } catch {
        console.group('UrlSelectBlock');
        console.error('URL : ' + replacedUrl);
        console.error('Error:', 'Can\'t read data by URL');
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cloudContext, formData]);

  if (readOnly) {
    const filtered = recordList.filter((r) => `${r.value}` === `${value}`);
    return <LabelBlock
      name={label}
      value={filtered.length >= 1 ? filtered[0].label : value}
      id={id}
      className={className} />;
  }

  return <SelectBlock
    label={label}
    value={value}
    setValue={setValue}
    recordList={recordList}
    defaultValue={defaultValue}
    required={required}
    id={id}
    className={className} />;
}

export default UrlSelectBlock;
