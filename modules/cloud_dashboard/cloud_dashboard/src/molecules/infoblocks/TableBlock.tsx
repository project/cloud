import { UPPERCASE_WORD_SET } from 'constant/templates/entity/entity_info_template';
import useDrupalTranslation from 'hooks/drupal_translation';
import EntityInfoRecordData from 'model/EntityInfoRecordData';
import { Table } from 'react-bootstrap';

const TdBlock = ({ text }: { text: string }) => {
  try {
    // For strings that can be parsed as JSON.
    const temp = JSON.parse(text);
    if (('type' in temp) && (temp['type'] === 'link')) {
      const url: string = temp['url'];
      const label: string = temp['label'];

      return <td className="word-break-all">
        <a href={url}>{label}</a>
      </td>;
    }

    return <td className="word-break-all">
      {text}
    </td>;
  } catch {
    if (text.includes('\n')) {
      return <td className="word-break-all">
        <pre>{text}</pre>
      </td>;
    }

    return <td className="word-break-all">
      {text}
    </td>;
  }
}

/**
 * Block of table view.
 *
 * @param record Record data fo table.
 */
const TableBlock = ({ record }: { record: EntityInfoRecordData }) => {

  const { t } = useDrupalTranslation();

  if (record.type !== 'table' || record.record.length === 0) {
    return <></>;
  }

  const keys = Object.keys(record.record[0]);

  const convert = (keyName: string) => {
    if (record.label !== undefined) {
      if (keyName in record.label) {
        return record.label[keyName];
      }
    }
    switch (keyName) {
      case 'item_key':
        return 'Key';
      case 'item_value':
        return 'Value';
      default: {
        return keyName.split(('_')).map((word, index) => {
          if (UPPERCASE_WORD_SET.has((word))) {
            return word.toUpperCase();
          }
          if (index === 0) {
            return word.slice(0, 1).toUpperCase() + word.slice((1));
          }
          return word;
        }).join(' ');
      }
    }
  }

  return <div className={`field ${record.class ?? ''} field--type-key-value field--label-above`}>
    <div className="field--label">{record.title}</div>
    <div className="field--items">
      <div className="field--item">
        <Table hover={true} striped={true} responsive={true}>
          <thead>
            <tr>
              {
                keys.map((key, index) => {
                  return <th key={index}>{t(convert(key))}</th>;
                })
              }
            </tr>
          </thead>
          <tbody>
            {
              record.record.map((r, index) => {
                return <tr className={index % 2 === 0 ? 'odd' : 'even'}>
                  {
                    keys.map((key, index2) => {
                      return <TdBlock text={r[key]} />;
                    })
                  }
                </tr>;
              })
            }
          </tbody>
        </Table>
      </div>
    </div>
  </div>;

}

export default TableBlock;
