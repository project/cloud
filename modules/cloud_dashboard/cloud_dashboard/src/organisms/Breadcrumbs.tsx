import EntityData from 'model/EntityData';
import { useLocation, useParams } from 'react-router-dom';
import { getEntityDataNameLabel } from 'service/convert';
import { capitalize, cspToLabel, entityNameToLabel } from 'service/string';

const Breadcrumbs = ({ action, designLabel, entityData }: {
  action?: string,
  designLabel?: string,
  entityData?: EntityData
}) => {

  /**
   * URL parameters.
   */
  const params: {
    cloudServiceProvider?: string,
    cloudContext?: string,
    entityName?: string,
    entityId?: string,
    action?: string
  } = useParams();
  const pathname = useLocation().pathname;

  /**
   * If the existing breadcrumb list is not displayed,
   * do not display by SPA.
   */
  const nodeList = document.querySelectorAll('div.breadcrumb-wrapper');
  if (nodeList.length === 0) {
    return <></>;
  }

  /**
   * If the parent node for an existing breadcrumb cannot be obtained,
   * do not display by SPA.
   */
  const parentNode = nodeList[0].parentNode;
  if (parentNode === null) {
    return <></>;
  }

  /**
   * Delete existing breadcrumbs.
   */
  for (const node of nodeList) {
    node.remove();
  }

  /**
   * If you cannot get the node that exists immediately
   * after the existing breadcrumb list,
   * do not display by SPA.
   */
  const reference = parentNode.querySelector('.contextual-region');
  if (reference === null) {
    return <></>;
  }

  /**
   * Create new breadcrumbs.
   */
  const breadcrumbTextList: string[] = ['Cloud service providers'];

  const actionType = action !== undefined
    ? action : params.action;
  if (actionType !== undefined) {
    // Create labels for adding.
    const cloudServiceProviderLabel = cspToLabel(params.cloudServiceProvider ? params.cloudServiceProvider : '');
    const entityNameLabel = entityNameToLabel(params.entityName ? params.entityName : '');
    const ActionLabel = actionType === 'create'
      ? 'Add' : actionType === 'import'
      ? 'Import' : capitalize(actionType);

    // Add the text in breadcrumbTextList(1).
    breadcrumbTextList.push(`${cloudServiceProviderLabel} ${entityNameLabel}`);

    // Add the text in breadcrumbTextList(2).
    if (entityData !== undefined) {
      const name = getEntityDataNameLabel(entityData);
      if (name !== '') {
        breadcrumbTextList.push(name);
      }
    }

    // Add the text in breadcrumbTextList(3).
    breadcrumbTextList.push(`${ActionLabel} ${cloudServiceProviderLabel} ${entityNameLabel}`);
  }

  if (designLabel !== undefined) {
    breadcrumbTextList.push('Design');
    breadcrumbTextList.push(`${designLabel} list`);
  }

  /**
   * Create the list of hyperlinks to set in breadcrumb.
   */
  const path_array = pathname.split("/");
  const path_prefix = '/clouds/dashboard';

  // If display "Launch template" view, change the path.
  path_array.forEach((item, index) => {
    if (item === 'cloud_launch_template') {
      path_array[index] = 'server_template';
    }
  });

  const breadcrumbs = path_array.map((item, index) => {
    if (index === 1 || index === 2) {
      return null;
    }

    let path = path_prefix + path_array.slice(0, index + 1).join('/');
    if (index === 0) {
      path = path_prefix + '/providers';
    }
    return path;
  }).filter(Boolean);

  /**
   * Create a <div> tag for adding breadcrumbs.
   */
  const newDiv = document.createElement('div');
  newDiv.className = 'breadcrumb-wrapper';

  const ol = document.createElement('ol');
  ol.className = 'breadcrumb';

  breadcrumbTextList.forEach((label, index) => {
    const li = document.createElement('li');
    if (index === breadcrumbTextList.length -1 && index > 0) {
      li.className = 'active';
      li.textContent = label;
    } else {
      const a = document.createElement('a');
      a.href = breadcrumbs[index] ?? '#';
      a.textContent = label;
      li.appendChild(a);
    }

    ol.appendChild(li);
  });

  newDiv.appendChild(ol);

  /**
   * Add the created breadcrumbs in place.
   */
  parentNode.insertBefore(newDiv, reference);

  return <></>;

}

export default Breadcrumbs;
