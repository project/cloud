import useDrupalOAuth2 from 'hooks/drupal_oauth2';
import LoadingSpinner from 'molecules/LoadingSpinner';
import { useEffect } from 'react';

/**
 * Callback form for Authorization Code Grant.
 */
const CallbackForm = () => {

  const { getTokenByAuthorizationCodeGrant } = useDrupalOAuth2();

  useEffect(() => {
    getTokenByAuthorizationCodeGrant();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <LoadingSpinner message={Drupal.t('Orchestrating clouds...')} />;

}

export default CallbackForm;
