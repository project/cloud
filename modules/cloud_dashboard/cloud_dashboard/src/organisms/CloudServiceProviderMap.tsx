import { LOCATION_MAP_CONFIG_URI } from 'constant/others/other';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import L from 'leaflet';
import CloudContextItem from 'model/CloudContextItem';
import { MapData } from 'model/MapData';
import RawCloudContextItem from 'model/RawCloudContextItem';
import CloudContextItemPopup from 'molecules/CloudContextItemPopup';
import LoadingSpinner from 'molecules/LoadingSpinner';
import MapPolygonLayer from 'molecules/mapblocks/MapPolygonLayer';
import { useEffect, useState } from 'react';
import { MapContainer } from 'react-leaflet';

/**
 * Type that represents the center coordinates
 * and zoom factor of the location map.
 */
type MapConfig = {
  latitude: number,
  longitude: number,
  zoomLevel: number,
};

/**
 * The custom hooks to get the location map's config
 * that represents the center coordinates
 * and zoom factor of the location map.
 */
const useMapConfig = () => {

  const [config, setConfig] = useState<MapConfig | undefined>(undefined);
  const { getJsonData } = useDrupalJsonApi();

  useEffect(() => {
    const fetchMapConfig = async () => {
      const response = await getJsonData<{
        latitude: string,
        longitude: string,
        zoom_level: string,
      }>(LOCATION_MAP_CONFIG_URI, { latitude: '0.0', longitude: '0.0', zoom_level: '1' });
      setConfig({
        latitude: parseFloat(response.latitude),
        longitude: parseFloat(response.longitude),
        zoomLevel: parseInt(response.zoom_level, 10),
      });
    };
    fetchMapConfig();
  }, []);

  return {
    config,
  };

}

/**
 * Convert a list of RawCloudContextItem to a format more suitable for display.
 *
 * @param rawCloudContextItemList List of RawCloudContextItem.
 * @returns List of CloudContextItem.
 */
const convertCloudContextItemList = (
  rawCloudContextItemList: RawCloudContextItem[],
  defaultIconUri: string
): CloudContextItem[] => {
  const hash: Record<string, CloudContextItem> = {};
  for (const rawCloudContextItem of rawCloudContextItemList) {
    const hashKey = `${rawCloudContextItem.Latitude},${rawCloudContextItem.Longitude}`;
    if (hashKey in hash) {
      hash[hashKey].icon = new L.Icon({
        iconUrl: defaultIconUri
      });
    } else {
      hash[hashKey] = {
        icon: new L.Icon({
          iconUrl: rawCloudContextItem.Items[0].Image
        }),
        cloudServiceProvider: rawCloudContextItem.Type,
        position: [
          parseFloat(rawCloudContextItem.Latitude),
          parseFloat(rawCloudContextItem.Longitude),
        ],
        item: []
      };
    }

    const itemList = rawCloudContextItem.Items.map((rawItem) => {
      return {
        iconUrl: rawItem.Image,
        entityViewUrl: rawItem.Url.replace(/^\/clouds/, ''),
        name: rawItem.Name,
        positionLabel: `${rawCloudContextItem.City}, ${rawCloudContextItem.Country}`,
      };
    });
    hash[hashKey].item = [...hash[hashKey].item, ...itemList];
  }
  return Object.values(hash);
}

/**
 * The custom hooks to get the list of CloudContextItem.
 */
const useCloudContextItemList = (): { cloudContextItemList: CloudContextItem[] | undefined } => {
  const [cloudContextItemList, setCloudContextItemList] = useState<CloudContextItem[] | undefined>(undefined);
  const { getJsonData } = useDrupalJsonApi();
  const [isLoading, setLoading] = useState<boolean>(true);

  const fetchCloudContextItemList = async () => {
    const { uri: defaultIconUri } = await getJsonData<{ uri: string }>('/clouds/cloud_dashboard/config/marker_icon_uri', { uri: '' });
    const rawCloudContextItemList = await getJsonData<RawCloudContextItem[]>('/clouds/cloud_config_location', []);
    return defaultIconUri.length > 0 && rawCloudContextItemList.length > 0
      ? convertCloudContextItemList(rawCloudContextItemList, defaultIconUri)
      : [];
  }

  if (isLoading) {
    setLoading(false);
    fetchCloudContextItemList().then((itemList) => {
      setCloudContextItemList(itemList);
    });
  }

  return {
    cloudContextItemList,
  };
};

/**
 * The custom hooks to get the MapData.
 */
const useMapData = (): { mapData: MapData | undefined } => {
  const [mapData, setMapData] = useState<MapData | undefined>(undefined);
  const { getJsonData } = useDrupalJsonApi();

  useEffect(() => {
    const fetchMapData = async () => {
      const response = await getJsonData<{ uri: string }>('/clouds/cloud_dashboard/config/map_geojson_uri', { uri: '' });
      if (response.uri === '') {
        return;
      }

      const mapData = await getJsonData<MapData>(response.uri, { features: [] });
      if (mapData.features.length > 0) {
        setMapData(mapData);
      }
    };
    fetchMapData();
  }, []);

  return {
    mapData,
  };
};

/**
 * Map for cloud service providers.
 */
const CloudServiceProviderMap = () => {

  const { config } = useMapConfig();
  const { cloudContextItemList } = useCloudContextItemList();
  const { mapData } = useMapData();

  return !config || !cloudContextItemList || !mapData
    ? <LoadingSpinner />
    : <MapContainer
      center={[config.latitude, config.longitude]}
      zoom={config.zoomLevel}
      scrollWheelZoom={false}
      style={{ height: 500, backgroundColor: '#4e5d6c' }}>
      <MapPolygonLayer mapData={mapData} />
      {
        cloudContextItemList.map((item, index) => {
          return <CloudContextItemPopup item={item} key={index} />;
        })
      }
    </MapContainer>;

};

export default CloudServiceProviderMap;
