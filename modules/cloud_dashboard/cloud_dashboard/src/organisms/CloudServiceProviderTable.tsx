import { CLOUD_SERVICE_PROVIDER_INFO } from 'constant/others/other';
import { CloudContextListContext } from 'hooks/cloud_context_list';
import SortInfo from 'model/SortInfo';
import LoadingSpinner from 'molecules/LoadingSpinner';
import DataTable from 'organisms/DataTable';
import { useContext, useState } from 'react';
import { Form } from 'react-bootstrap';

/**
 * Table of cloud service providers.
 */
const CloudServiceProviderTable = () => {

  const { cloudContextList, isLoading } = useContext(CloudContextListContext);
  const [sortInfo, setSortInfo] = useState<SortInfo>({
    key: '', direction: 'ASC'
  });

  return <Form>
    <Form.Group>
      <h1 className="page-header">{Drupal.t('Cloud Service Providers')}</h1>
    </Form.Group>
    {
      isLoading
        ? <LoadingSpinner />
        : <DataTable
          dataColumnList={[{ key: 'labelName', label: 'Name' }]}
          dataRecordList={
            cloudContextList
              .sort((a, b) => {
                const keyA = a.labelName;
                const keyB = b.labelName;
                if (sortInfo.direction === 'ASC') {
                  return keyA > keyB ? 1 : -1;
                } else {
                  return keyA > keyB ? -1 : 1;
                }
              })
              .filter((r) => r.name !== 'ALL')
              .map((r) => {
                const temp = CLOUD_SERVICE_PROVIDER_INFO[r.cloudServiceProvider];
                return {
                  id: `${r.cloudServiceProvider}_${r.name}`,
                  entityTypeId: '',
                  value: {
                    'labelName': r.labelName,
                    'url': `/${r.cloudServiceProvider}/${r.name}/${temp.entityTypeId}`
                  },
                  cloudContext: r.name
                };
              })
          }
          sortInfo={sortInfo}
          setSortInfo={setSortInfo}
          hasOperationLinks={false}
          operationLinksName="Operations"
          isShowCheckBox={false}
          detailInfo={{
            column: 'labelName',
            path: 'test'
          }}
        />
    }
  </Form>;

}

export default CloudServiceProviderTable;
