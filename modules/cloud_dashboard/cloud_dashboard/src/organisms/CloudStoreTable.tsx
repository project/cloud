import CLOUD_STORE_LIST from 'constant/templates/cloud_store/resource';
import { CloudContextListContext } from 'hooks/cloud_context_list';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import DataColumn from 'model/DataColumn';
import DataRecord from 'model/DataRecord';
import SortInfo from 'model/SortInfo';
import LoadingSpinner from 'molecules/LoadingSpinner';
import DataTable from 'organisms/DataTable';
import { useContext, useEffect, useState } from 'react';
import { convertEntityDataToDataRecord } from 'service/convert';

/**
 * ListView of data.
 *
 * @returns JSX of CloudStoreTable.
 */
const CloudStoreTable = ({ bundleId, itemPerPage, pageIndex }: {
  bundleId: string,
  itemPerPage: number,
  pageIndex: number,
}) => {

  const { cloudContextList } = useContext(CloudContextListContext);
  const { getEntityList } = useDrupalJsonApi();
  const [dataColumnList, setDataColumnList] = useState<DataColumn[]>([]);
  const [dataRecordList, setDataRecordList] = useState<DataRecord[]>([]);
  const [sortInfo, setSortInfo] = useState<SortInfo>({
    key: '', direction: 'ASC'
  });
  const [isLoading, setLoading] = useState(true);

  const template = CLOUD_STORE_LIST.filter((r) => r.bundleId === bundleId)[0];

  /** Update dataColumnList and dataRecordList */
  useEffect(() => {
    const init = async () => {
      // Load launch template's column data.
      const columnList = template.column;
      let newDataColumnList: DataColumn[] = columnList.map((column) => {
        return { key: column.name, label: column.labelName };
      });
      setDataColumnList(newDataColumnList);

      // Create function parameter.
      const parameter = {
        limit: itemPerPage,
        offset: pageIndex * itemPerPage,
        filter: {},
        sort: sortInfo
      };

      // Load launch template's data.
      const rawData = await getEntityList('cloud_store', parameter, template.bundleId);
      setDataRecordList(convertEntityDataToDataRecord(template.bundleId, rawData, columnList, cloudContextList, {}));
      setLoading(false);
    };
    setLoading(true);
    init();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cloudContextList, sortInfo, itemPerPage, pageIndex]);

  return isLoading
    ? <LoadingSpinner />
    : <DataTable
      dataColumnList={dataColumnList}
      dataRecordList={dataRecordList}
      sortInfo={sortInfo}
      setSortInfo={setSortInfo}
      hasOperationLinks={true}
      isShowCheckBox={false}
      operationLinksName="Operations" />;
}

export default CloudStoreTable;
