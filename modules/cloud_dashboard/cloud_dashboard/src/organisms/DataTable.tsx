import { DataTableSelectedRowsContext } from 'hooks/data_table_selected_row';
import useDrupalTranslation from 'hooks/drupal_translation';
import DataColumn from 'model/DataColumn';
import DataRecord from 'model/DataRecord';
import SortInfo from 'model/SortInfo';
import DataTableHeader from 'molecules/DataTableHeader';
import DataTableRow from 'molecules/DataTableRow';
import DataTableRowWithOperationLinks from 'molecules/DataTableRowWithOperationLinks';
import { useContext, useState } from 'react';
import { Table } from 'react-bootstrap';

/**
 * Table for listing data.
 *
 * @param dataColumnList List of DataColumn.
 * @param dataRecordList List of DataRecord.
 * @param sortInfo Information of soring parameter.
 * @param setSortInfo Setter of sortInfo.
 * @param detailInfo Information required to create a link to more information.
*/
const DataTable = ({ dataColumnList, dataRecordList, sortInfo, setSortInfo, hasOperationLinks, operationLinksName, operationsLinks, detailInfo, noItemMessage, isShowCheckBox = true }: {
  dataColumnList: DataColumn[],
  dataRecordList: DataRecord[],
  sortInfo: SortInfo,
  setSortInfo: (s: SortInfo) => void,
  hasOperationLinks: boolean,
  operationLinksName: string,
  operationsLinks?: {
    label: string,
    path: string,
  }[],
  detailInfo?: {
    column: string,
    path: string,
  },
  noItemMessage?: string,
  isShowCheckBox?: boolean,
}) => {

  const { t } = useDrupalTranslation();
  const [allChecked, setAllChecked] = useState(false);
  const { clearIdList, addIds } = useContext(DataTableSelectedRowsContext);

  return <Table hover={true} striped={true} responsive={true}>
    <thead>
      <tr>
      {
        isShowCheckBox
          ? <th className="select-all">
            <div className="form-item form-type-checkbox">
              <input type="checkbox" className="form-checkbox form-check-input" checked={allChecked}
                onChange={() => {
                  if (allChecked) {
                    setAllChecked(false);
                    clearIdList();
                  } else {
                    setAllChecked(true);
                    addIds(dataRecordList.map((dataRecord) => dataRecord.id));
                  }
                }} />
            </div>
          </th>
          : <></>
      }
        {dataColumnList.map((dataColumn) => {
          return <DataTableHeader dataColumn={dataColumn} sortInfo={sortInfo} setSortInfo={setSortInfo} />
        })}
        {
          hasOperationLinks
            ? <th className="th-style">
              {t(operationLinksName)}
            </th>
            : <></>
        }
      </tr>
    </thead>
    <tbody>
      {
        dataRecordList.length === 0
        ? <tr>
          <td colSpan={dataColumnList.length + (hasOperationLinks ? 1 : 0)}>
            {
              noItemMessage
                ? t(noItemMessage)
                : t('No items.')
            }
          </td>
        </tr>
        : dataRecordList.map((dataRecord, index) => {
          return hasOperationLinks
            ? <DataTableRowWithOperationLinks
              dataRecord={dataRecord}
              dataColumnList={dataColumnList}
              className={index % 2 === 0 ? 'odd' : 'even'}
              detailInfo={detailInfo}
              operationsLinks={operationsLinks}
              isShowCheckBox={isShowCheckBox}
            />
            : <DataTableRow
              dataRecord={dataRecord}
              dataColumnList={dataColumnList}
              className={index % 2 === 0 ? 'odd' : 'even'}
              detailInfo={detailInfo}
              isShowCheckBox={isShowCheckBox}
            />;
        })
      }
    </tbody>
  </Table>

};

export default DataTable;
