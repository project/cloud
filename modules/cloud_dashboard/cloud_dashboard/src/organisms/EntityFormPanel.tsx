import PanelWrapper from 'atoms/PanelWrapper';
import { UserInformationContext } from 'hooks/user_information';
import EntityFormColumn from 'model/EntityFormColumn';
import EntityFormRecord from 'model/EntityFormRecord';
import EntityFormBlock from 'molecules/EntityFormBlock';
import { useContext } from 'react';
import { showEntityFormBlockFlg } from 'service/convert';
import { entityNameToLabel } from 'service/string';

const changeEntityFormFields = (
  cloudServiceProvider: string,
  entityName: string,
  actionType: string,
  entityFormColumn: EntityFormColumn,
  formData: Record<string, any>,
) => {
  // Edit OpenStack security group
  if (actionType === 'edit'
    && cloudServiceProvider === 'openstack'
    && entityName === 'security_group') {
    if (entityFormColumn.type === 'sg_permission') {
      let rules = formData[entityFormColumn.name];
      if (rules !== undefined) {
        rules.forEach((rule: any) => {
          // If "-1"(Any) is selected, "Replace with "tcp"(TCP)
          if (rule.ip_protocol === '-1') {
            rule.ip_protocol = 'tcp';
          }
        });
      }
    }
  }

  return;
}

const EntityFormPanel = ({
  cloudServiceProvider,
  cloudContext,
  entityName,
  actionType,
  entityRecord,
  formData,
  setFormData
}: {
  cloudServiceProvider: string,
  cloudContext: string,
  entityName: string,
  actionType: string,
  entityRecord: EntityFormRecord,
  formData: Record<string, any>,
  setFormData: (f: Record<string, any>) => void,
}) => {
  const { hasAdminRole } = useContext(UserInformationContext);

  if (entityRecord.type !== 'panel') {
    return <></>;
  }

  return <PanelWrapper title={entityRecord.panelName !== ''
    ? entityRecord.panelName : entityNameToLabel(entityName)}>
    {
      entityRecord.keyValueRecords.map((keyValueRecord, index) => {
        changeEntityFormFields(
          cloudServiceProvider,
          entityName,
          actionType,
          keyValueRecord,
          formData,
        );
        return (showEntityFormBlockFlg(
          cloudServiceProvider,
          entityName,
          actionType,
          keyValueRecord,
          hasAdminRole,
          formData
        ))
          ? <EntityFormBlock key={index}
            keyValueRecord={keyValueRecord}
            cloudContext={cloudContext}
            formData={formData}
            setFormData={setFormData}
            cloudServiceProvider={cloudServiceProvider} />
          : <></>;
      })
    }
  </PanelWrapper>;
}

export default EntityFormPanel;
