import useDrupalTranslation from 'hooks/drupal_translation';
import EntityInfoPanelData from 'model/EntityInfoPanelData';
import D3MetricsBlock from 'molecules/infoblocks/D3MetricsBlock';
import KeyValueBlock from 'molecules/infoblocks/KeyValueBlock';
import TableBlock from 'molecules/infoblocks/TableBlock';
import { Card } from 'react-bootstrap';

/**
 * Panel of the entity detail info.
 *
 * @param index Index for panel accordion logic.
 * @param panelData Entity info.
 */
const EntityInfoPanel = ({ panelData }: {
  panelData: EntityInfoPanelData
}) => {

  const { t } = useDrupalTranslation();

  return <details className="card" open>
    <summary role="button" aria-expanded="true" area-pressed="true"
      className="card-header">{t(panelData.title)}</summary>
    <Card.Body className="details-wrapper">
      {
        panelData.records.map((record) => {
          return <>
            <D3MetricsBlock record={record} />
            <KeyValueBlock record={record} />
            <TableBlock record={record} />
          </>;
        })
      }
    </Card.Body>
  </details>;

}

export default EntityInfoPanel;
