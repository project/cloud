import EntityFormRecord from 'model/EntityFormRecord';
import EntityFormLabel from 'molecules/EntityFormLabel';
import EntityFormPanel from 'organisms/EntityFormPanel';

/**
 * A block of forms to be displayed based on the form template.
 */
const EntityRecordForm = ({
  cloudServiceProvider,
  cloudContext,
  entityName,
  action,
  formData,
  setFormData,
  entityRecord,
}: {
  cloudServiceProvider: string,
  cloudContext: string,
  entityName: string,
  action: string,
  formData: Record<string, any>,
  setFormData: (f: Record<string, any>) => void,
  entityRecord: EntityFormRecord,
}) => {

  switch (entityRecord.type) {
    case 'panel':
      return <EntityFormPanel
        cloudServiceProvider={cloudServiceProvider}
        cloudContext={cloudContext}
        entityName={entityName}
        actionType={action}
        entityRecord={entityRecord}
        formData={formData}
        setFormData={setFormData} />;
    case 'label':
      return <EntityFormLabel label={entityRecord.text} entityName={entityName} />;
    default:
      return <></>;
  };

}

export default EntityRecordForm;
