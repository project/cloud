import { DONT_SHOW_OPERATIONS_ENTITY_TYPE_LIST } from 'constant/others/other';
import { CloudContextListContext } from 'hooks/cloud_context_list';
import { DataTableSelectedRowsContext } from 'hooks/data_table_selected_row';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import DataColumn from 'model/DataColumn';
import DataRecord from 'model/DataRecord';
import EntityColumn from 'model/EntityColumn';
import SortInfo from 'model/SortInfo';
import LoadingSpinner from 'molecules/LoadingSpinner';
import DataTable from 'organisms/DataTable';
import { useContext, useEffect, useState } from 'react';
import { convertEntityDataToDataRecord } from 'service/convert';

/**
 * Table for entity data.
 *
 * @param entityTypeId Entity type ID.
 * @param entityColumnList List of EntityColumn.
 * @param namespace Value of namespace.
 * @param namespaceName Value of namespace.
 * @param pageIndex Entity item's page index.
 * @param detailInfo Information required to create a link to more information.
*/
const EntityTable = ({ entityTypeId, entityColumnList, namespace, namespaceName, pageIndex, itemPerPage, cloudContext, detailInfo }: {
  entityTypeId: string,
  entityColumnList: EntityColumn[],
  namespace: string,
  namespaceName: string,
  pageIndex: number,
  itemPerPage: number,
  cloudContext?: string,
  detailInfo?: {
    column: string,
    path: string,
  },
}) => {

  const { cloudContextList } = useContext(CloudContextListContext);
  const { getEntityList, readDataCache } = useDrupalJsonApi();
  const [dataColumnList, setDataColumnList] = useState<DataColumn[]>([]);
  const [dataRecordList, setDataRecordList] = useState<DataRecord[]>([]);
  const [sortInfo, setSortInfo] = useState<SortInfo>({
    key: '', direction: 'ASC'
  });
  const [isLoading, setLoading] = useState(true);
  const { clearIdList } = useContext(DataTableSelectedRowsContext);

  /** Update dataColumnList and dataRecordList */
  useEffect(() => {
    const init = async () => {
      // Load column data.
      let newDataColumnList: DataColumn[] = entityColumnList.map((entityColumn) => {
        return { key: entityColumn.name, label: entityColumn.labelName };
      });
      setDataColumnList(newDataColumnList);

      // Cache the data you need.
      const dataCache = await readDataCache(entityColumnList, cloudContext);

      // Create function parameter.
      const filter: { [key: string]: string } = {};
      if (namespace !== '') {
        filter['namespace'] = namespace;
      }
      if (namespaceName !== '') {
        filter['namespaceName'] = namespaceName;
      }
      if (cloudContext !== undefined) {
        filter['cloud_context'] = cloudContext;
      }
      const parameter = {
        limit: itemPerPage,
        offset: pageIndex * itemPerPage,
        filter,
        sort: sortInfo
      };

      // Load entity data.
      const rawData = await getEntityList(entityTypeId, parameter);
      setDataRecordList(convertEntityDataToDataRecord(entityTypeId, rawData, entityColumnList, cloudContextList, dataCache));

      clearIdList();

      setLoading(false);
    };
    setLoading(true);
    init();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cloudContext, cloudContextList, entityTypeId, entityColumnList, sortInfo, namespace, namespaceName, pageIndex, itemPerPage]);

  return isLoading
    ? <LoadingSpinner />
    : <DataTable
      dataColumnList={dataColumnList}
      dataRecordList={dataRecordList}
      sortInfo={sortInfo}
      setSortInfo={setSortInfo}
      hasOperationLinks={!DONT_SHOW_OPERATIONS_ENTITY_TYPE_LIST.includes(entityTypeId)}
      operationLinksName="Operations links"
      detailInfo={detailInfo} />;

}

export default EntityTable;
