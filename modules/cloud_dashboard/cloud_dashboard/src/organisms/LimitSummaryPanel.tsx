import LimitSummary from 'model/LimitSummary';
import LimitSummaryPanelBlock from 'organisms/LimitSummaryPanelBlock';
import { Card, Row } from 'react-bootstrap';

const LimitSummaryPanel = ({ template, getGraphData } : {
  template: LimitSummary,
  getGraphData: (dataKey: string) => number,
}) => {

  const { panelTitle, plots } = template;
  return <details className="card" open>
    <summary role="button" aria-expanded="true" area-pressed="true" className="card-header bg-light">
      {panelTitle}
    </summary>
    <Card.Body className="details-wrapper">
      <Row className="form-group">
        {
          plots.map(({ label, usedKey, totalKey, scale, suffix }) => {
            return <LimitSummaryPanelBlock key={label} label={label}
              used={getGraphData(usedKey)}
              total={getGraphData(totalKey)}
              scale={scale}
              suffix={suffix} />;
          })
        }
      </Row>
    </Card.Body>
  </details>;

}

export default LimitSummaryPanel;
