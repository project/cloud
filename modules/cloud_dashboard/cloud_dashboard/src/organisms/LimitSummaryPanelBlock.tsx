import PieChart from 'molecules/PieChart';

const LimitSummaryPanelBlock = ({ label, used, total, scale, suffix }: {
  label: string,
  used: number,
  total: number,
  scale?: number,
  suffix?: string,
}) => {

  if (used === 0 && total === 0) {
    return null;
  }

  const usedText = scale ? `${Math.trunc(used / scale)}` : `${used}`;
  const totalText = scale ? `${Math.trunc(total / scale)}` : `${total}`;

  return <div className="col-auto text-center">
    <PieChart size={75}
      data={[used, total - used]}
      color={[
        '#428bca', '#ddd'
      ]} />
    <div>
      <div>{label}</div>
      <div>Used {usedText}{suffix} of {totalText}{suffix}</div>
    </div>
  </div>;

}

export default LimitSummaryPanelBlock;
