import OPENSTACK_LIMIT_SUMMARY_TEMPLATES from 'constant/templates/limit_summary/openstack';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import EntityData from 'model/EntityData';
import LimitSummaryPanel from 'organisms/LimitSummaryPanel';
import { useCallback, useState } from 'react';

const LimitSummaryPanels = () => {

  const { getEntityListAll } = useDrupalJsonApi();
  const [quotaData, setQuotaData] = useState<EntityData[]>([]);
  const [isLoading, setLoading] = useState(true);

  if (isLoading) {
    setLoading(false);
    getEntityListAll('openstack_quota').then((data) => {
      setQuotaData(data);
    });
  }

  const getGraphData = useCallback((dataKey: string) => {
    return (quotaData.length === 0
      || !quotaData[0].attributes
      || !(dataKey in quotaData[0].attributes)
      || typeof quotaData[0].attributes[dataKey] !== 'number')
      ? 0
      : quotaData[0].attributes[dataKey];
  }, [quotaData]);

  return <>
    {
      OPENSTACK_LIMIT_SUMMARY_TEMPLATES.map((template) => {
        return <LimitSummaryPanel key={template.panelTitle} template={template}
          getGraphData={getGraphData} />;
      })
    }
  </>;

}

export default LimitSummaryPanels;
