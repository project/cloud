import useDrupalOAuth2 from 'hooks/drupal_oauth2';
import { Button, Form } from 'react-bootstrap';

/**
 * Login form component.
 */
const LoginForm = () => {
  const { toCallbackUrl } = useDrupalOAuth2();

  return <Form>
    <Button onClick={toCallbackUrl}>{Drupal.t('Login')}</Button>
  </Form>;

}

export default LoginForm;
