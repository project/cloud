import { getMenuTemplateList } from 'constant/templates/menu/menu_template';
import CloudServiceProvider from 'model/CloudServiceProvider';
import EntityData from 'model/EntityData';
import { useParams } from 'react-router-dom';
import { getEntityDataNameLabel } from 'service/convert';
import { capitalize, cspToLabel, entityNameToLabel } from 'service/string';

const PageTitle = ({ action, designLabel, bundleId, entityData, text }: {
  action?: string,
  designLabel?: string,
  bundleId?: string,
  entityData?: EntityData,
  text?: string
}) => {
  /**
   * URL parameters.
   */
  const params: {
    cloudServiceProvider?: string,
    cloudContext?: string,
    entityName?: string,
    entityId?: string,
    action?: string
  } = useParams();

  /**
   * If the existing page title is not displayed,
   * do not display by SPA.
   */
  const pageTitleElements = document.querySelectorAll('h1.page-header');
  if (pageTitleElements.length !== 1) {
    return <></>;
  }
  const pageTitle = pageTitleElements[0];

  /**
   * Get Entity Type Name Label.
   */
  const entityTypeNameLabel = entityNameToLabel(params.entityName ? params.entityName : '');

  /**
   * Get Entity Data Name.
   */
  const entityDataNameLabel = getEntityDataNameLabel(entityData);

  /**
   * Get Action Type.
   */
  const actionType = action !== undefined
    ? action : params.action;

  /**
   * Get Action Label.
   */
  const actionLabel = actionType !== undefined
    ? actionType === 'create'
      ? 'Add' : capitalize(actionType.replace('_', ' ')) : undefined;

  /**
   * Create new title text.
   */
  let pageTitleText = "Cloud Dashboard";

  // List
  if (params.cloudServiceProvider !== undefined && actionType === undefined) {
    const menuTemplateList = getMenuTemplateList(params.cloudServiceProvider as CloudServiceProvider)
      .filter((menu) => {
        return menu.entityName === params.entityName;
      });
    pageTitleText = menuTemplateList.length >= 1
      ? menuTemplateList[0].labelName
      : `${capitalize(entityTypeNameLabel)}s`;
    if (params.cloudContext === undefined) {
      pageTitleText = `All ${cspToLabel(params.cloudServiceProvider)} ${pageTitleText}`;
    }
  }

  // Add, Import
  if (actionLabel !== undefined && entityData === undefined) {
    pageTitleText = actionLabel + ' ' + entityTypeNameLabel;
  }

  // Edit
  if (actionLabel !== undefined && entityData !== undefined) {
    pageTitleText = actionLabel + ' ' + entityDataNameLabel;
  }

  // Delete
  if (actionLabel !== undefined && actionType === 'delete') {
    pageTitleText = 'Are you sure you want to delete the ' + entityTypeNameLabel + ' ' + entityDataNameLabel + '?';
  }

  // Detailed View
  if (actionLabel === undefined && entityData !== undefined) {
    pageTitleText = 'Name: ' + entityDataNameLabel;
  }

  // Template List, Project List
  if (designLabel !== undefined) {
    pageTitleText = capitalize(designLabel) + ' list';
  }

  // Store
  if (actionType === undefined && designLabel !== undefined && designLabel === 'store' && bundleId !== undefined) {
    pageTitleText = entityNameToLabel(bundleId);
  }

  // Special action
  if (actionType === 'console_output') {
    pageTitleText = `Console log of ${entityTypeNameLabel}`;
  } else if (actionType === 'console') {
    pageTitleText = `Console of ${entityTypeNameLabel}`;
  } else if (actionType === 'action_log') {
    pageTitleText = `Action log of ${entityTypeNameLabel}`;
  } else if (actionType === 'launch') {
    pageTitleText = `Are you sure you want to launch instance(s) or resource(s) from ${entityDataNameLabel}?`;
  } else if (actionType === 'create_image') {
    pageTitleText = `Create an image for instance ${entityDataNameLabel}${entityData ? ' (' + entityData.attributes['instance_id'] + ')' : ''}`;
  }

  if (text) {
    pageTitleText = text;
  }

  /**
   * Rewrite the page title.
   */
  const divTag = document.createElement('div');
  divTag.className = 'field field--name-name field--type-string field--label-hidden field--item';
  divTag.innerText = pageTitleText;
  pageTitle.innerHTML = '';
  pageTitle.appendChild(divTag);

  const siteNameElement =  document.querySelectorAll('#navbar > nav > div.navbar-header > div > a.name.navbar-brand');
  if (siteNameElement.length === 1) {
    const siteName = siteNameElement[0].textContent;
    document.title = pageTitleText + ' | ' + siteName;
  }

  return <></>;

}

export default PageTitle;
