import { getProjectColumnList } from 'constant/templates/project/project';
import { CloudContextListContext } from 'hooks/cloud_context_list';
import useDrupalJsonApi, { GetEntityListAllType } from 'hooks/drupal_jsonapi';
import CloudContext from 'model/CloudContext';
import DataColumn from 'model/DataColumn';
import DataRecord from 'model/DataRecord';
import SortInfo from 'model/SortInfo';
import LoadingSpinner from 'molecules/LoadingSpinner';
import DataTable from 'organisms/DataTable';
import { useContext, useEffect, useState } from 'react';
import { convertEntityDataToDataRecord } from 'service/convert';

/**
 * Read ProjectList by JSON:API.
 *
 * @param cloudContext cloud_context.
 * @param sortInfo Information of soring parameter.
 * @returns ProjectList.
 */
const readProjectList = async (
  getEntityListAll: GetEntityListAllType,
  cloudContext: CloudContext,
  sortInfo: SortInfo
) => {

  const filter: { [key: string]: string } = {};
  if (cloudContext.name !== 'ALL') {
    filter['filter[cloud_context]'] = cloudContext.name;
  }
  if (sortInfo.key !== '') {
    filter['sort'] = sortInfo.direction === 'ASC' ? sortInfo.key : '-' + sortInfo.key;
  }
  return await getEntityListAll('cloud_project', filter, cloudContext.cloudServiceProvider);

};

/**
 * ListView of Project.
 *
 * @param cloudContext cloud_context.
 * @returns JSX of LaunchTemplateView.
 */
const ProjectTable = ({ cloudContext }: {
  cloudContext: CloudContext
}) => {

  const { cloudContextList } = useContext(CloudContextListContext);
  const { getEntityListAll } = useDrupalJsonApi();
  const [dataColumnList, setDataColumnList] = useState<DataColumn[]>([]);
  const [dataRecordList, setDataRecordList] = useState<DataRecord[]>([]);
  const [sortInfo, setSortInfo] = useState<SortInfo>({
    key: '', direction: 'ASC'
  });
  const [isLoading, setLoading] = useState(true);

  /** Update dataColumnList and dataRecordList */
  useEffect(() => {
    const init = async () => {
      // Load launch template's column data.
      const columnList = getProjectColumnList(cloudContext);
      let newDataColumnList: DataColumn[] = columnList.map((column) => {
        return { key: column.name, label: column.labelName };
      });
      setDataColumnList(newDataColumnList);

      // Load launch template's data.
      const rawData = await readProjectList(getEntityListAll, cloudContext, sortInfo);
      setDataRecordList(convertEntityDataToDataRecord('cloud_project', rawData, columnList, cloudContextList, {}));
      setLoading(false);
    };
    setLoading(true);
    init();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cloudContext, cloudContextList, sortInfo]);

  return isLoading
    ? <LoadingSpinner />
    : <DataTable
      dataColumnList={dataColumnList}
      dataRecordList={dataRecordList}
      sortInfo={sortInfo}
      setSortInfo={setSortInfo}
      hasOperationLinks={true}
      isShowCheckBox={false}
      operationLinksName="Operations" />;

}

export default ProjectTable;
