import {
    CLOUD_SERVICE_PROVIDER_INFO, CLOUD_SERVICE_PROVIDER_LIST, ROUTE_URL
} from 'constant/others/other';
import { CloudContextListContext } from 'hooks/cloud_context_list';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import CloudServiceProvider from 'model/CloudServiceProvider';
import { useContext, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';

/**
 * Item that can be displayed in the "Manage" menu.
 */
type MenuItem = {
  label: string;
  url: string;
};

const DEFAULT_CLOUD_CONTEXT_NAME = 'Cloud service providers';

const HeaderLogo = () => {
  return <div className="navbar-header">
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
      <svg focusable="false" aria-hidden="true" viewBox="0 0 24 24" tabIndex={-1} fill="#fff"><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path></svg>
    </button>
    <div className="region region-navigation">
      <a className="logo navbar-btn pull-left m-md-auto text-md-center" href={`${ROUTE_URL}/providers`} title="Home" rel="home">
        <img src="/themes/contrib/rigel/logo.svg" alt="Home" />
      </a>
      <a className="name navbar-brand d-none" href={`${ROUTE_URL}/providers`} title="Home" rel="home">sample-site</a>
    </div>
  </div>;
}

const HeaderLogo2 = () => {
  return <div className="region region-navigation">
    <a className="logo navbar-btn pull-left m-md-auto text-md-center" href={`${ROUTE_URL}/providers`} title="Home" rel="home">
      <img src="/themes/contrib/rigel/logo.svg" alt="Home" />
    </a>
    <a className="name navbar-brand d-none" href={`${ROUTE_URL}/providers`} title="Home" rel="home">sample-site</a>
  </div>;
}

const MenuLink = ({ url, label, cssClass }: {
  url: string,
  label: string,
  cssClass?: string
}) => {
  if (url.startsWith(ROUTE_URL)) {
    const url2 = url.replaceAll(`${ROUTE_URL}/`, '/');
    return <li>
      <Link to={url2}>
        {label}
      </Link>
    </li>;
  }

  return <li>
    <a href={url} className={cssClass}>
      {label}
    </a>
  </li>;
}

const SubMenu = ({ isOpen, label, menuLink }: {
  isOpen: boolean,
  label: string,
  menuLink: { url: string, label: string, cssClass?: String }[]
}) => {
  const [isOpenLocal, setIsOpenLocal] = useState(isOpen);

  return <li className={"dropdown-submenu" + (isOpenLocal ? ' open' : '')}>
    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
    <div className="dropdown-submenu-toggle dropdown-toggle menu" data-toggle="dropdown"
      onClick={() => {
        setIsOpenLocal(!isOpenLocal);
      }}>
      {label}<span className="caret"></span>
    </div>
    <ul className="dropdown-menu" role="menu">
      {
        menuLink.map((pair) => {
          return <MenuLink key={pair.label} url={pair.url} label={pair.label} />;
        })
      }
    </ul>
  </li>;
}

const AllCspSubMenu = ({ isOpen }: {
  isOpen: boolean
}) => {
  const { cloudContextList } = useContext(CloudContextListContext);

  const getFilteredList = (cloudServiceProvider: CloudServiceProvider) => {
    return cloudContextList.filter((c) => {
      return c.name !== 'ALL' && c.cloudServiceProvider === cloudServiceProvider;
    })
  }

  return <SubMenu label="All" isOpen={isOpen}
    menuLink={[
      { url: `${ROUTE_URL}/providers`, label: 'Cloud Service Providers' },
      ...CLOUD_SERVICE_PROVIDER_LIST
        .filter((csp) => {
          const list = getFilteredList(csp);
          return list.length > 0;
        })
        .map((csp) => {
          const pair = CLOUD_SERVICE_PROVIDER_INFO[csp];
          return { url: `${ROUTE_URL}/${csp}/${pair.entityTypeId}`, label: pair.label };
        })
    ]} />;
}

const CspSubMenu = ({ csp, isOpen }: {
  csp: CloudServiceProvider,
  isOpen: boolean
}) => {
  const { cloudContextList } = useContext(CloudContextListContext);

  const getFilteredList = (cloudServiceProvider: CloudServiceProvider) => {
    return cloudContextList.filter((c) => {
      return c.name !== 'ALL' && c.cloudServiceProvider === cloudServiceProvider;
    })
  }

  const list = getFilteredList(csp);
  const pair = CLOUD_SERVICE_PROVIDER_INFO[csp];
  if (list.length === 0) {
    return <></>;
  }

  return <SubMenu label={pair.label} isOpen={isOpen}
    menuLink={list.map((c) => {
      return { url: `${ROUTE_URL}/${c.cloudServiceProvider}/${c.name}/${pair.entityTypeId}`, label: c.labelName };
    })} />;
}

const AllCltSubMenu = ({ isOpen }: {
  isOpen: boolean
}) => {
  const { cloudContextList } = useContext(CloudContextListContext);

  const getFilteredList = (cloudServiceProvider: CloudServiceProvider) => {
    return cloudContextList.filter((c) => {
      return c.name !== 'ALL' && c.cloudServiceProvider === cloudServiceProvider;
    })
  }

  const menuLinks = [];
  if (getFilteredList('aws_cloud').length > 0) {
    menuLinks.push({ url: `${ROUTE_URL}/design/aws_cloud/server_template_list`, label: 'AWS launch templates' });
  }
  if (getFilteredList('k8s').length > 0) {
    menuLinks.push({ url: `${ROUTE_URL}/design/k8s/server_template_list`, label: 'K8s launch templates' });
    menuLinks.push({ url: `${ROUTE_URL}/k8s/project`, label: 'K8s cloud projects' });
  }

  return <SubMenu isOpen={isOpen} label="All" menuLink={menuLinks} />;
}

const CltSubMenu = ({ isOpen }: {
  isOpen: boolean
}) => {
  const { cloudContextList } = useContext(CloudContextListContext);

  return <SubMenu label="Launch templates" isOpen={isOpen} menuLink={
    cloudContextList.filter((c) => {
      return c.name !== 'ALL';
    }).map((c) => {
      return { url: `${ROUTE_URL}/design/server_template/${c.name}`, label: c.labelName };
    })
  } />;
}

const ProjectSubMenu = ({ isOpen }: {
  isOpen: boolean
}) => {
  const { cloudContextList } = useContext(CloudContextListContext);

  const getFilteredList = (cloudServiceProvider: CloudServiceProvider) => {
    return cloudContextList.filter((c) => {
      return c.name !== 'ALL' && c.cloudServiceProvider === cloudServiceProvider;
    })
  }

  return <SubMenu label="Projects" isOpen={isOpen} menuLink={
    getFilteredList('k8s').map((c) => {
      return { url: `${ROUTE_URL}/${c.cloudServiceProvider}/${c.name}/cloud_project`, label: c.labelName };
    })} />;
}

const StoreSubMenu = ({ isOpen }: {
  isOpen: boolean
}) => {
  return <SubMenu label="Stores" isOpen={isOpen} menuLink={[
    { url: `${ROUTE_URL}/k8s_cost_store`, label: 'K8s cost store' },
    { url: `${ROUTE_URL}/k8s_namespace_resource_store`, label: 'K8s namespace resource store' },
    { url: `${ROUTE_URL}/k8s_node_resource_store`, label: 'K8s node resource store' },
    { url: `${ROUTE_URL}/k8s_pod_resource_store`, label: 'K8s pod resource store' },
  ]} />;
}

const DropdownMenu = ({ isOpen, label, children }: {
  isOpen: boolean,
  label: string,
  children: React.ReactNode
}) => {
  const [isOpenLocal, setIsOpenLocal] = useState(isOpen);

  return <li className={"dropdown" + (isOpenLocal ? ' open' : '')}>
    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
    <div className="dropdown-toggle menu" data-toggle="dropdown"
      onClick={() => {
        setIsOpenLocal(!isOpenLocal);
      }}>
      {label}<span className="caret"></span>
    </div>
    <ul className="dropdown-menu" role="menu">
      {children}
    </ul>
  </li>;
}

const CspMenu = ({ isOpen, cloudContextName }: {
  isOpen: boolean,
  cloudContextName: string
}) => {
  return <DropdownMenu isOpen={isOpen} label={cloudContextName}>
    <AllCspSubMenu isOpen={false} />
    {
      CLOUD_SERVICE_PROVIDER_LIST.map((csp) => {
        return <CspSubMenu key={csp} csp={csp} isOpen={false} />;
      })
    }
  </DropdownMenu>;
}

const DesignMenu = ({ isOpen }: {
  isOpen: boolean
}) => {
  return <DropdownMenu isOpen={isOpen} label="Design">
    <AllCltSubMenu isOpen={false} />
    <CltSubMenu isOpen={false} />
    <ProjectSubMenu isOpen={false} />
    <StoreSubMenu isOpen={false} />
  </DropdownMenu>;
}

const ConfigMenu = ({ isOpen }: {
  isOpen: boolean
}) => {
  const [menuItems, setMenuItems] = useState<MenuItem[]>([]);
  const { getJsonData } = useDrupalJsonApi();
  useEffect(() => {
    const fetchMenuLinks = async () => {
      const response = await getJsonData<MenuItem[]>('/cloud_dashboard/manage_menu/visible', []);
      setMenuItems(response);
    };
    fetchMenuLinks();
  }, []);

  return menuItems.length > 0
    ? <DropdownMenu isOpen={isOpen} label="Manage">
      {menuItems?.map(item => (
        <MenuLink url={item.url} label={item.label} cssClass="dropdown-submenu-toggle"/>
      ))}
    </DropdownMenu>
    : <></>;
}

const SideBar = ({ isOpenCsp, isOpenDesign, isOpenConfig }: {
  isOpenCsp?: boolean,
  isOpenDesign?: boolean,
  isOpenConfig?: boolean,
}) => {

  const params: {
    cloudServiceProvider?: string,
    cloudContext?: string,
    entityName?: string,
    entityId?: string,
    action?: string
  } = useParams();
  const { cloudContextList } = useContext(CloudContextListContext);
  const [cloudContextName, setCloudContextName] = useState(DEFAULT_CLOUD_CONTEXT_NAME);

  useEffect(() => {
    const temp = cloudContextList.filter((c) => c.name === params.cloudContext);
    setCloudContextName(
      temp.length > 0
        ? temp[0].labelName
        : DEFAULT_CLOUD_CONTEXT_NAME
    );
  }, [params, cloudContextList]);

  // Note: The magic number 78.9843px comes from the style set in the body tag.
  return <div className="col-lg-3 col-xl-3 col-xxl-2 left-section" style={{ top: '78.9843px' }}>
    <header className="navbar-default">
      <nav className="navbar navbar-expand-lg navbar-light d-block">
        <HeaderLogo />
        <div className="navbar-collapse collapse">
          <HeaderLogo2 />
          <div className="region region-navigation-collapsible">
            <nav role="navigation" aria-labelledby="block-rigel-main-menu-menu" className="contextual-region open">
              <h2 className="sr-only" >Main navigation</h2>
              <ul className="nav navbar-nav" role="menu">
                <MenuLink url={`${ROUTE_URL}/providers`} label={Drupal.t('Home')} />
                <CspMenu isOpen={
                  isOpenCsp !== undefined && isOpenCsp
                } cloudContextName={cloudContextName} />
                <DesignMenu isOpen={
                  isOpenDesign !== undefined && isOpenDesign
                } />
                <ConfigMenu isOpen={
                  isOpenConfig !== undefined && isOpenConfig
                } />
              </ul>
            </nav>
          </div>
        </div>
      </nav>
    </header>
  </div>;
}

export default SideBar;
