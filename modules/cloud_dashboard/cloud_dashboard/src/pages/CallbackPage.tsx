import PageTitle from 'organisms/PageTitle';
import Breadcrumbs from 'organisms/Breadcrumbs';
import CallbackForm from 'organisms/CallbackForm';
import { Col, Container, Row } from 'react-bootstrap';

/**
 * Callback page.
 */
const CallbackPage = () => {

  return <>
    <PageTitle />
    <Breadcrumbs />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <CallbackForm />
        </Col>
      </Row>
    </Container>
  </>;

}

export default CallbackPage;
