import CloudServiceProvider from 'model/CloudServiceProvider';
import Breadcrumbs from 'organisms/Breadcrumbs';
import PageTitle from 'organisms/PageTitle';
import ProjectTable from 'organisms/ProjectTable';
import SideBar from 'organisms/SideBar';
import { Col, Container, Row } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

const CloudProjectPage = () => {
  const params: {
    cloudServiceProvider: string,
    cloudContext?: string,
  } = useParams();

  return <>
    <PageTitle designLabel='project' />
    <Breadcrumbs designLabel='Project' />
    <SideBar isOpenDesign />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <ProjectTable cloudContext={{
            cloudServiceProvider: params.cloudServiceProvider as CloudServiceProvider,
            name: params.cloudContext === undefined ? 'ALL' : params.cloudContext,
            labelName: ''
          }} />
        </Col>
      </Row>
    </Container>
  </>;

}

export default CloudProjectPage;
