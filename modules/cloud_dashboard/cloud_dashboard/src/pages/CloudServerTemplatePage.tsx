import LabelText from 'atoms/LabelText';
import { CloudContextListContext } from 'hooks/cloud_context_list';
import { DataTableSelectedRowsContext } from 'hooks/data_table_selected_row';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { StatusMessageContext } from 'hooks/status_message';
import CloudServiceProvider from 'model/CloudServiceProvider';
import StatusMessageResponse from 'model/StatusMessageResponse';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import LaunchTemplateTable from 'organisms/LaunchTemplateTable';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext, useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { Link, useHistory, useParams } from 'react-router-dom';
import { fetchWrapper, printErrorResult } from 'service/fetch';

const CloudServerTemplatePageImpl = ({ cloudServiceProvider }: {
  cloudServiceProvider: string,
}) => {

  const params: {
    cloudContext?: string,
  } = useParams();

  const { addMessage, addErrorMessage, addMessagesByResponse } = useContext(StatusMessageContext);
  const [isRefreshing, setRefreshing] = useState(false);
  const history = useHistory();
  const { resetFetchCache } = useDrupalJsonApi();
  const { selectedIdList, clearIdList } = useContext(DataTableSelectedRowsContext);
  const [multiActionType, setMultiActionType] = useState('delete_multiple');

  useEffect(() => {
    clearIdList();
  }, []);

  useEffect(() => {
    console.group('CloudServerTemplatePage');
    console.log('selectedIdList', selectedIdList);
    console.groupEnd();
  }, [selectedIdList]);

  const refresh = async () => {
    if (isRefreshing) {
      return;
    }

    setRefreshing(true);

    const url = params.cloudContext !== undefined
      ? `/clouds/design/server_template/${params.cloudContext}/update`
      : `/clouds/design/${cloudServiceProvider}/server_template/update`;

    const result = await fetchWrapper(url, {
      method: 'POST',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
      }
    });
    setRefreshing(false);

    if (!result.ok) {
      console.group('Refresh template list');
      await printErrorResult(result, 'error');
      console.groupEnd();
      addErrorMessage('Unable to update Server Template.');
      return;
    }

    // Console log.
    console.group('Refresh Request');
    console.log('Response:');
    const responseJson = await result.response.json();
    console.log(responseJson);
    console.groupEnd();

    // Status message.
    const messages: StatusMessageResponse = (responseJson)['messages'];
    if (messages) {
      addMessagesByResponse(messages, {
        life: 2,
      });
    } else {
      addMessage('Updated Server Template.', {
        life: 2,
      });
    }
    resetFetchCache();

    history.push(
      params.cloudContext !== undefined
        ? `/design/server_template/${params.cloudContext}`
        : `/design/${cloudServiceProvider}/server_template_list`
    );
  }

  const multiAction = () => {
    if (multiActionType !== '' && selectedIdList.length > 0) {
      history.push(`/design/server_template/${params.cloudContext}/${multiActionType}`);
    }
  }

  return <>
    <StatusMessages />
    <PageTitle designLabel='launch template' />
    <Breadcrumbs designLabel='Launch template' />
    <SideBar isOpenDesign />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <Form>
            <Form.Group style={{ marginBottom: '2rem' }}>
              {
                params.cloudContext
                  ? <Link className="btn btn-primary" to={
                    `/design/server_template/${params.cloudContext}/${cloudServiceProvider}/add`
                  }>
                    <LabelText text="Add launch template" />
                  </Link>
                  : <></>
              }
              <a className="btn btn-outline ripple-effect"
                onClick={refresh} href="#">
                <LabelText text={
                  isRefreshing ? 'Refreshing...' : 'Refresh'
                } />
              </a>
            </Form.Group>
            {
              <Form.Group className="form-item">
                <Form.Label className="control-label" htmlFor="edit-action">Action</Form.Label>
                <div className="select-wrapper">
                  <Form.Select id="edit-action" className="form-control" value={multiActionType}
                    onChange={(e) => {
                      setMultiActionType(e.currentTarget.value);
                    }}>
                    <option value="delete_multiple">{Drupal.t('Delete launch template(s)')}</option>
                  </Form.Select>
                </div>
                <div className="form-actions">
                  <Button variant="primary" onClick={multiAction}>
                    <LabelText text="Apply to selected items" />
                  </Button>
                </div>
              </Form.Group>
            }
            <LaunchTemplateTable cloudContext={{
              cloudServiceProvider: cloudServiceProvider as CloudServiceProvider,
              name: params.cloudContext === undefined ? 'ALL' : params.cloudContext,
              labelName: ''
            }} />
          </Form>
        </Col>
      </Row>
    </Container>
  </>;

}

const CloudServerTemplatePage = () => {

  const params: {
    cloudServiceProvider?: string,
    cloudContext?: string,
  } = useParams();
  const { cloudContextList } = useContext(CloudContextListContext);

  // Guess the cloud service provider.
  const cloudServiceProvider = params.cloudServiceProvider ??
    cloudContextList.find(context => context.name === params.cloudContext)?.cloudServiceProvider;

  return cloudServiceProvider
    ? <CloudServerTemplatePageImpl cloudServiceProvider={cloudServiceProvider} />
    : <></>;

}

export default CloudServerTemplatePage;
