import CloudStoreCountLabel from 'molecules/CloudStoreCountLabel';
import ItemPerPageSelector from 'molecules/ItemPerPageSelector';
import PageSelector from 'molecules/PageSelector';
import Breadcrumbs from 'organisms/Breadcrumbs';
import CloudStoreTable from 'organisms/CloudStoreTable';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useState } from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';

/**
 * The Page of Cloud Store.
 *
 * @param bundleId The bundle ID, such as k8s_cost_store.
 */
const CloudStorePage = ({ bundleId }: {
  bundleId: string
}) => {
  const [itemCount, setItemCount] = useState(0);
  const [itemPerPage, setItemPerPageImpl] = useState(50);
  const [pageIndex, setPageIndex] = useState(0);

  const setItemPerPage = (n: number) => {
    setItemPerPageImpl(n);
    setPageIndex(0);
  }

  return <>
    <PageTitle designLabel='store' bundleId={bundleId} />
    <Breadcrumbs designLabel='Store' />
    <SideBar isOpenDesign/>
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <Form>
            {
              itemCount > 0
                ? <ItemPerPageSelector
                  itemPerPage={itemPerPage}
                  setItemPerPage={setItemPerPage}
                />
                : <></>
            }
            <Form.Group style={{ marginTop: '2rem' }}>
              <CloudStoreCountLabel
                bundleId={bundleId}
                itemCount={itemCount}
                setItemCount={setItemCount} />
            </Form.Group>
          </Form>
        </Col>
      </Row>
    </Container>
    <PageSelector
      pageIndex={pageIndex}
      setPageIndex={setPageIndex}
      itemCount={itemCount}
      itemPerPage={itemPerPage} />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <Form>
            <CloudStoreTable bundleId={bundleId} itemPerPage={itemPerPage} pageIndex={pageIndex} />
          </Form>
        </Col>
      </Row>
    </Container>
    <PageSelector
      pageIndex={pageIndex}
      setPageIndex={setPageIndex}
      itemCount={itemCount}
      itemPerPage={itemPerPage} />
  </>;
}

export default CloudStorePage;
