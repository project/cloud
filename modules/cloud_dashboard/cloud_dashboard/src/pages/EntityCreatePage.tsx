import LabelText from 'atoms/LabelText';
import ENTITY_FORM_LIST from 'constant/templates/form/entity_form_template';
import useCreatingFormData from 'hooks/creating_form_data';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { StatusMessageContext } from 'hooks/status_message';
import EntityFormTemplate from 'model/EntityFormTemplate';
import StatusMessageResponse from 'model/StatusMessageResponse';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import EntityRecordForm from 'organisms/EntityRecordForm';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext, useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import { convertFormDataToSendData, getFormDataNameLabel } from 'service/convert';
import { fetchWrapper, printErrorResult } from 'service/fetch';
import { capitalize } from 'service/string';

/**
 * Custom Hooks for form template.
 */
const useFormTemplate = (action: string) => {

  const params: {
    cloudServiceProvider: string,
    cloudContext: string,
    entityName: string,
  } = useParams();

  const entityFormTemplateList = ENTITY_FORM_LIST.filter((template) => {
    return template.cloudServiceProvider === params.cloudServiceProvider
      && (template.entityName === params.entityName || template.entityName === '')
      && template.actionType === action
  });

  return {
    formTemplate: entityFormTemplateList.length >= 1
      ? entityFormTemplateList[0] : undefined
  };

}

/**
 * Default page if form data cannot be retrieved.
 */
const DefaultPage = () => {

  const params: {
    cloudServiceProvider: string,
    cloudContext: string,
    entityName: string,
  } = useParams();

  return <>
    <PageTitle />
    <Breadcrumbs />
    <SideBar />
    <Container fluid className="px-0">
      <Row>
        <Col>
          <span>EntityCreatePage</span><br />
          <span>cloudServiceProvider={params.cloudServiceProvider}</span><br />
          <span>cloudContext={params.cloudContext}</span><br />
          <span>entityName={params.entityName}</span><br />
          <span>action=create</span>
        </Col>
      </Row>
    </Container>
  </>;

}

/**
 * Page of the form to create an entity.
 *
 * @param cloudContext The cloud context.
 * @param entityFormTemplate The form template for entity.
 * @param entityName The entity type.
 * @param action The CxUD action type.
 */
const EntityCreatePageImpl = ({ cloudContext, entityFormTemplate, entityName, action }: {
  cloudContext: string,
  entityFormTemplate: EntityFormTemplate,
  entityName: string,
  action: string,
}) => {

  const { formData, setFormData } = useCreatingFormData(cloudContext, entityFormTemplate);
  const { addMessage, addMessages, addErrorMessage, addMessagesByResponse } = useContext(StatusMessageContext);
  const [isRefreshing, setRefreshing] = useState(false);
  const history = useHistory();
  const { resetFetchCache } = useDrupalJsonApi();

  useEffect(() => {
    console.group('FormData (EntityCreatePage)');
    console.log(formData);
    console.groupEnd();
  }, [formData]);

  /**
   * Save entity's data.
   */
  const save = async () => {
    setRefreshing(true);
    const url = `/cloud_dashboard/${entityFormTemplate.cloudServiceProvider}/${cloudContext}/${entityFormTemplate.cloudServiceProvider}_${entityName}/${action}`;

    const result = await fetchWrapper(url, {
      method: 'POST',
      body: convertFormDataToSendData(formData, entityFormTemplate),
    });
    setRefreshing(false);

    if (!result.ok) {
      console.group('Create Request');
      await printErrorResult(result, 'error');
      console.groupEnd();
      addErrorMessage('The @type @label could not be created.',
        {
          args: {
            '@type': capitalize(entityName.replace('_', ' ')),
            '@label': getFormDataNameLabel(formData),
          },
        }
      );
      return;
    }

    console.group('Create Request');
    console.log('Response:');
    const responseJson = await result.response.json();
    console.log(responseJson);
    console.groupEnd();

    const messages: StatusMessageResponse = (responseJson)['messages'];
    if (messages) {
      addMessagesByResponse(messages, {
        life: 2,
      });
    }
    resetFetchCache();

    history.push(`/${entityFormTemplate.cloudServiceProvider}/${cloudContext}/${entityName}`);
  }

  const saveButtonLabel = entityFormTemplate.submitButtonLabel !== undefined
    ? (entityFormTemplate.submitButtonLabel ?? 'Save')
    : 'Save';

  return <>
    <StatusMessages />
    <PageTitle action={action} />
    <Breadcrumbs action={action} />
    <SideBar />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <Form>
            {
              entityFormTemplate.entityRecords.map((entityRecord, index) => {
                return <EntityRecordForm key={index}
                  cloudServiceProvider={entityFormTemplate.cloudServiceProvider}
                  cloudContext={cloudContext}
                  entityName={entityName}
                  action={entityFormTemplate.actionType}
                  formData={formData}
                  setFormData={setFormData}
                  entityRecord={entityRecord} />;
              })
            }
            <Button variant={isRefreshing ? 'secondary' : 'primary'}
              disabled={isRefreshing}
              className="ripple-effect" onClick={save}>
              <LabelText text={saveButtonLabel} />
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  </>;

}

/**
 * Page of the form to create an entity.
 *
 * @param action The CxUD action type.
 */
const EntityCreatePage = ({ action }: {
  action: string
}) => {

  const params: {
    cloudServiceProvider: string,
    cloudContext: string,
    entityName: string,
  } = useParams();
  const { formTemplate } = useFormTemplate(action);

  if (!formTemplate) {
    return <DefaultPage />;
  }

  return <EntityCreatePageImpl
    cloudContext={params.cloudContext}
    entityFormTemplate={formTemplate}
    entityName={params.entityName}
    action={action} />;

}

export default EntityCreatePage;
