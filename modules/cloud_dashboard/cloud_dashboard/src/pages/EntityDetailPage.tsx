import LabelText from 'atoms/LabelText';
import ENTITY_INFO_TAB_LIST from 'constant/templates/entity/entity_info_tab';
import ENTITY_INFO_LIST from 'constant/templates/entity/entity_info_template';
import { CloudContextListContext } from 'hooks/cloud_context_list';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import useDrupalTranslation from 'hooks/drupal_translation';
import { useEntityData } from 'hooks/entity_data';
import { StatusMessageContext } from 'hooks/status_message';
import EntityColumn from 'model/EntityColumn';
import EntityData from 'model/EntityData';
import EntityInfoPanelData from 'model/EntityInfoPanelData';
import EntityInfoRecord from 'model/EntityInfoRecord';
import EntityInfoRecordData from 'model/EntityInfoRecordData';
import EntityInfoTemplate from 'model/EntityInfoTemplate';
import StatusMessageResponse from 'model/StatusMessageResponse';
import DetailActionButtonGroup from 'molecules/DetailActionButtonGroup';
import LoadingSpinner from 'molecules/LoadingSpinner';
import Breadcrumbs from 'organisms/Breadcrumbs';
import EntityInfoPanel from 'organisms/EntityInfoPanel';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext, useEffect, useState } from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import { Link, useHistory, useParams } from 'react-router-dom';
import { convertDataToTextForUI } from 'service/convert';
import { fetchWrapper, printErrorResult } from 'service/fetch';
import { getReplacedUrl } from 'service/string';
import EntityMultiXxudPage from './EntityMultiXxudPage';

/**
 * Custom Hooks for detail page's template.
 */
const useDetailTemplate = (
  cloudServiceProvider: string,
  entityName: string
) => {

  const entityDetailTemplateList = ENTITY_INFO_LIST.filter((template) => {
    return template.cloudServiceProvider === cloudServiceProvider
      && template.entityName === entityName;
  });

  return {
    detailTemplate: entityDetailTemplateList.length >= 1
      ? entityDetailTemplateList[0] : undefined
  };

}

/**
 * Custom Hooks for detail page tab's template.
 */
const useTabTemplate = (
  cloudServiceProvider: string,
  entityName: string,
) => {

  const tabTemplateList = ENTITY_INFO_TAB_LIST.filter((template) => {
    return template.cloudServiceProvider === cloudServiceProvider
      && template.entityName === entityName;
  });

  return {
    tabTemplate: tabTemplateList.length >= 1
      ? tabTemplateList[0] : undefined
  };

}

/**
 * Custom Hooks for panel data.
 *
 * @param entityData The entity's data.
 * @param cloudContext The cloud context.
 * @param detailTemplate The detail info template for entity.
 * @param entityId The entity ID.
 */
const usePanelData = (
  entityData: EntityData,
  cloudContext: string,
  detailTemplate: EntityInfoTemplate,
  entityId: string,
) => {

  const { readDataCache, getJsonData } = useDrupalJsonApi();
  const [panelDataList, setPanelDataList] = useState<EntityInfoPanelData[]>([]);

  /**
   * Calculate the information on the panel
   * to be displayed from the entity's information.
   *
   * @param dataCache The data cache.
   * @param tableRecordList The record list of table.
   * @param entityColumn The form record's information.
   */
  const getEntityInfoRecordData = async (
    dataCache: Record<string, EntityData[]>,
    tableRecordList: string[],
    entityColumn: EntityColumn
  ) => {
    switch (entityColumn.type) {
      case 'metrics': {
        const url = `/clouds/${detailTemplate.cloudServiceProvider}/${entityData.attributes['cloud_context']}/${detailTemplate.entityName}/${entityData.attributes['drupal_internal__id']}/metrics`;
        const metricsData = await getJsonData<Record<string, number>[]>(url, []);
        return {
          type: 'metrics',
          record: entityColumn.column.map((column) => {
            return {
              title: column.title,
              yLabel: column.yLabel,
              record: metricsData.map((metrics) => {
                return { x: metrics['timestamp'], y: metrics[column.name] };
              })
            }
          }),
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;
      }
      case 'custom-table': {
        const keyVal: Record<string, string>[] = [];
        const labelData: Record<string, string> = {};
        entityData.attributes[entityColumn.name].forEach((record: any, index: number) => {
          const temp: Record<string, string> = {};
          for (const column of entityColumn.column) {
            labelData[column.name] = column.labelName;
            const convertedText = convertDataToTextForUI(
              record[column.name],
              column,
              dataCache
            );
            const convertedText2 = column.type === 'link'
              ? getReplacedUrl(
                convertedText,
                entityData.attributes['cloud_context'],
                detailTemplate.entityName,
                entityData.attributes['drupal_internal__id'],
                detailTemplate.cloudServiceProvider,
                index
              ) : convertedText;
            temp[column.name] = convertedText2;
          }
          keyVal.push(temp);
        });
        return {
          type: 'table',
          title: entityColumn.labelName,
          record: keyVal,
          label: labelData,
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;
      }
      case 'json-table': {
        const entityColumnList = entityColumn.column;
        const keyVal: Record<string, string>[] = [];

        for (const record of entityData.attributes[entityColumn.name]) {
          const record2: {
            item_key: string,
            item_value: string
          } = record;
          const record3: Record<string, any> = JSON.parse(record2.item_value);

          const temp: Record<string, string> = {};
          for (const entityColumn of entityColumnList) {
            if (entityColumn.name in record3) {
              temp[entityColumn.name] = convertDataToTextForUI(
                record3[entityColumn.name],
                entityColumn,
                dataCache
              );
            }
          }
          keyVal.push(temp);
        }
        return {
          type: 'table',
          title: entityColumn.labelName,
          record: keyVal,
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;
      }
      case 'array-table': {
        const keyVal: Record<string, string>[] = [];
        const rows = entityData.attributes[entityColumn.name];
        for (const row of rows) {
          const values: string[] = row.split(',');
          const temp: Record<string, string> = {};
          entityColumn.column.forEach((column, index) => {
            temp[column.name] = convertDataToTextForUI(
              values[index],
              column,
              dataCache
            );
          });
          keyVal.push(temp);
        }
        return {
          type: 'table',
          title: entityColumn.labelName,
          record: keyVal,
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;
      }
      case 'relationship': {
        const emptyDiv = {
          type: 'div',
          key: entityColumn.labelName,
          value: '',
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;

        // If the relationship field is empty, return an empty div.
        if (typeof entityData.relationships === 'string') {
          return emptyDiv;
        }

        // If the relationship's data is empty, return an empty div.
        // Note: Since the data contained in the relevant key of the
        // relationships can be either an array or a single map,
        // the process is written to accommodate both cases.
        const rawValue = entityData.relationships[entityColumn.name];
        const relationshipsData = Array.isArray(rawValue.data)
          ? rawValue.data : [rawValue.data];

        const filteredRelationshipsData = relationshipsData.filter((record) => {
          return (record !== undefined && record !== null && ('meta' in record));
        });
        if (filteredRelationshipsData.length === 0) {
          return emptyDiv;
        }

        // Replace each element of the target (filteredRelationshipsData)
        // with an array of strings.
        const relationshipsDataList = filteredRelationshipsData.map((record) => {
          const entityId = record.meta[entityColumn.info.keyColumn1];
          const entityList = dataCache[entityColumn.info.entityTypeId];
          const selectedEntity = entityList.find((e) => {
            return e.attributes[entityColumn.info.keyColumn2] === entityId;
          });
          return selectedEntity
            ? selectedEntity.attributes[entityColumn.info.valueColumn] as string
            : '';
        });

        // Return the data to be displayed.
        return {
          type: 'div',
          key: entityColumn.labelName,
          value: relationshipsDataList.join(' '),
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;
      }
      case 'select': {
        const emptyDiv = {
          type: 'div',
          key: entityColumn.labelName,
          value: '{}',
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;

        let replacedUrl = entityColumn.url
          .replaceAll('{cloud_context}', cloudContext)
          .replaceAll('{entity_id}', entityId);
        const result = replacedUrl.match(/\{[^{}]+\}/g);
        if (result !== null) {
          for (const pattern of result) {
            const key = pattern.substring(1, pattern.length - 1);
            replacedUrl = replacedUrl.replaceAll(pattern, `${entityData.attributes[key]}`);
          }
        }

        try {
          const jsonData = await getJsonData<{ value: string, label: string, group?: string }[]>(replacedUrl, []);
          const dataKey = entityData.attributes[entityColumn.name];
          const filteredJsonData = jsonData.filter((r) => r.value === dataKey);
          return filteredJsonData.length >= 1
            ? {
              type: 'div',
              key: entityColumn.labelName,
              value: filteredJsonData[0].label,
              id: entityColumn.id,
              class: entityColumn.class
            } as EntityInfoRecordData
            : emptyDiv;
        } catch {
          return emptyDiv;
        }
      }
      default:
        if (tableRecordList.includes(entityColumn.name)) {
          const keyVal: Record<string, string>[] = [];
          for (const record of entityData.attributes[entityColumn.name]) {
            const temp: Record<string, string> = {};
            for (const key of Object.keys((record))) {
              temp[key] = record[key] && (typeof record[key] === 'string') ? `${record[key].replace(/(\r\n)|\n/, '')}` : '';
            }
            keyVal.push(temp);
          }
          return {
            type: 'table',
            title: entityColumn.labelName,
            record: keyVal,
            id: entityColumn.id,
            class: entityColumn.class
          } as EntityInfoRecordData;
        } else {
          const data = entityColumn.type !== 'fraction'
            ? entityData.attributes[entityColumn.name]
            : [
              entityData.attributes[entityColumn.name],
              entityData.attributes[entityColumn.denominator_name]
            ];
          const convertedText = convertDataToTextForUI(
            data,
            entityColumn,
            dataCache
          );
          return {
            type: 'div',
            key: entityColumn.labelName,
            value: convertedText,
            id: entityColumn.id,
            class: entityColumn.class
          } as EntityInfoRecordData;
        }
    }
  }

  /**
   * Calculate the information on the panel
   * to be displayed from the entity's information.
   *
   * @param entityInfoRecord The information of the form.
   */
  const getPanelData = async (entityInfoRecord: EntityInfoRecord) => {
    const panelData: EntityInfoPanelData = {
      title: entityInfoRecord.panelName,
      records: []
    };

    const dataCache = await (readDataCache(entityInfoRecord.keyValueRecords, cloudContext));
    for (const entityColumn of entityInfoRecord.keyValueRecords) {
      panelData.records.push(await getEntityInfoRecordData(
        dataCache,
        entityInfoRecord.tableRecordList,
        entityColumn,
      ));
    }

    return panelData;
  }

  /**
   * Calculate the information on the panel
   * to be displayed from the entity's information.
   */
  const getPanelDataList = async () => {
    const panelDataList: EntityInfoPanelData[] = [];
    for (const entityInfoRecord of detailTemplate.entityRecords) {
      panelDataList.push(await getPanelData(entityInfoRecord));
    }
    return panelDataList;
  }

  useEffect(() => {
    getPanelDataList().then((data) => {
      setPanelDataList(data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [entityData]);

  return {
    panelDataList
  };

}

/**
 * Tab buttons for action.
 */
const InfoTabs = ({cloudServiceProvider, entityName}: {
  cloudServiceProvider: string,
  entityName: string,
}) => {
  const params: {
    cloudContext: string,
    entityId: string,
  } = useParams();
  const { tabTemplate } = useTabTemplate(cloudServiceProvider, entityName);
  const { t } = useDrupalTranslation();

  if (!tabTemplate) {
    return <></>;
  }

  const url = tabTemplate.entityName !== 'cloud_launch_template'
    ? `/${tabTemplate.cloudServiceProvider}/${params.cloudContext}/${tabTemplate.entityName}/${params.entityId}`
    : `/design/server_template/${params.cloudContext}/${params.entityId}`;

  return <nav className="tabs">
    <div>
      <ul className="nav nav-tabs" id="tab_wrap">
        <li className="active">
          <Link to={url}
            className="active is-active ripple-effect">
            {Drupal.t('View')}
          </Link>
        </li>
        {
          tabTemplate.tabs.map((tabColumn) => {
            return <li>
              <Link to={`${url}/${tabColumn.name}`}
                className="active is-active ripple-effect">
                {t(tabColumn.labelName)}
              </Link>
            </li>;
          })
        }
      </ul>
    </div>
  </nav>;

}

const CloudLaunchTemplateButtonGroup = ({cloudContext}: {
  cloudContext: string,
}) => {

  const { addMessage, addErrorMessage, addMessagesByResponse } = useContext(StatusMessageContext);
  const [isRefreshing, setRefreshing] = useState(false);
  const history = useHistory();
  const { resetFetchCache } = useDrupalJsonApi();

  /**
   * Function to update the entity list.
   */
  const refresh = async () => {
    if (isRefreshing) {
      return;
    }

    setRefreshing(true);

    const url = `/clouds/design/server_template/${cloudContext}/update`;

    const result = await fetchWrapper(url, {
      method: 'POST',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
      }
    });
    setRefreshing(false);

    if (!result.ok) {
      console.group('Refresh entity list');
      await printErrorResult(result, 'error');
      console.groupEnd();
      addErrorMessage('Unable to update Sever template.');
      return;
    }

    // Console log.
    console.group('Refresh Request');
    console.log('Response:');
    const responseJson = await result.response.json();
    console.log(responseJson);
    console.groupEnd();

    // Status message.
    const messages: StatusMessageResponse = (responseJson)['messages'];
    if (messages) {
      addMessagesByResponse(messages, {
        life: 2,
      });
    } else {
      addMessage('Updated Server Template.', {
        life: 2,
      });
    }
    resetFetchCache();

    history.push(`/design/server_template/${cloudContext}`);
  }

  return <Form.Group className="mb-4">
    <a className="btn btn-outline ripple-effect"
      onClick={refresh} href="#">
      <LabelText text={
        isRefreshing ? 'Refreshing...' : 'Refresh'
      } />
    </a>
  </Form.Group>;

}

/**
 * Default page if form data cannot be retrieved.
 */
const DefaultPage = ({cloudServiceProvider, entityName}: {
  cloudServiceProvider: string,
  entityName: string,
}) => {

  const params: {
    cloudContext: string,
    entityId: string
  } = useParams();

  return <>
    <PageTitle />
    <Breadcrumbs />
    <SideBar />
    <Container fluid className="px-0">
      <Row>
        <Col>
          <span>EntityDetailPage</span><br />
          <span>cloudServiceProvider={cloudServiceProvider}</span><br />
          <span>cloudContext={params.cloudContext}</span><br />
          <span>entityName={entityName}</span><br />
          <span>entityId={params.entityId}</span><br />
        </Col>
      </Row>
    </Container>
  </>;

}

/**
 * Page of the view of detail info an entity.
 *
 * @param entityData The entity's data.
 * @param cloudContext The cloud context.
 * @param detailTemplate The detail info template for entity.
 * @param entityId The entity's ID.
 */
const EntityDetailPageImpl = ({ entityData, cloudContext, detailTemplate, entityId }: {
  entityData: EntityData,
  cloudContext: string,
  detailTemplate: EntityInfoTemplate,
  entityId: string,
}) => {

  const { panelDataList } = usePanelData(entityData, cloudContext, detailTemplate, entityId);

  return <>
    <PageTitle entityData={entityData} text={
      detailTemplate.entityName === 'cloud_launch_template'
        ? entityData.attributes['name']
        : undefined
    } />
    <Breadcrumbs entityData={entityData} />
    <SideBar />
    <InfoTabs cloudServiceProvider={detailTemplate.cloudServiceProvider} entityName={detailTemplate.entityName} />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          {
            detailTemplate.entityName === 'cloud_launch_template'
              ? <CloudLaunchTemplateButtonGroup cloudContext={cloudContext} />
              : <DetailActionButtonGroup detailTemplate={detailTemplate} cloudContext={cloudContext}
              entityData={entityData} entityId={entityId} />
          }
          {
            panelDataList.map((panelData, index) => {
              return <EntityInfoPanel key={index} panelData={panelData} />;
            })
          }
        </Col>
      </Row>
    </Container>
  </>;

}

const CushionPage = ({ cloudServiceProvider, entityName }: {
  cloudServiceProvider: string,
  entityName: string,
}) => {

  const params: {
    cloudContext: string,
    entityId: string
  } = useParams();
  const { entityData } = useEntityData(
    cloudServiceProvider,
    entityName,
    params.entityId
  );
  const { detailTemplate } = useDetailTemplate(cloudServiceProvider, entityName);

  if (!detailTemplate) {
    return <DefaultPage cloudServiceProvider={cloudServiceProvider} entityName={entityName} />;
  }

  if (!entityData) {
    return <>
      <PageTitle />
      <Breadcrumbs />
      <SideBar />
      <InfoTabs cloudServiceProvider={cloudServiceProvider} entityName={entityName} />
      <Container fluid className="px-0">
        <Row className="mx-0">
          <Col>
            <LoadingSpinner />
          </Col>
        </Row>
      </Container>
    </>;
  }

  return <EntityDetailPageImpl
    entityData={entityData}
    cloudContext={params.cloudContext}
    detailTemplate={detailTemplate}
    entityId={params.entityId} />;

}

/**
 * Page of the view of detail info an entity.
 */
const EntityDetailPage = ({entityName}: {
  entityName?: string,
}) => {
  const params: {
    cloudServiceProvider?: string,
    cloudContext: string,
    entityName?: string,
    entityId: string
  } = useParams();

  const { cloudContextList } = useContext(CloudContextListContext);

  // Guess the cloud service provider.
  const cloudServiceProvider = params.cloudServiceProvider ??
    cloudContextList.find(context => context.name === params.cloudContext)?.cloudServiceProvider;

  // Guess the entity type name.
  const newEntityName = entityName ?? params.entityName;

  // If the page is about multi-action, the screen transfers to that page.
  if (params.entityId.includes('_multiple') && cloudServiceProvider && newEntityName) {
    return <EntityMultiXxudPage cloudServiceProvider={cloudServiceProvider} cloudContext={params.cloudContext}
      entityName={newEntityName} action={params.entityId} />;
  }

  return (!cloudServiceProvider || !newEntityName)
    ? <DefaultPage cloudServiceProvider={cloudServiceProvider ?? 'undefined'}
      entityName={newEntityName ?? 'undefined'} />
    : <CushionPage cloudServiceProvider={cloudServiceProvider}
      entityName={newEntityName} />;

}

export default EntityDetailPage;
