import Glyphicon from "atoms/Glyphicon";
import LabelText from "atoms/LabelText";
import { DataTableSelectedRowsContext } from "hooks/data_table_selected_row";
import EntityFormLabel from "molecules/EntityFormLabel";
import StatusMessages from "molecules/StatusMessage";
import Breadcrumbs from "organisms/Breadcrumbs";
import PageTitle from "organisms/PageTitle";
import SideBar from "organisms/SideBar";
import { useContext, useEffect, useState } from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import LoadingDataPage from "./LoadingDataPage";
import useDrupalJsonApi from "hooks/drupal_jsonapi";
import EntityData from "model/EntityData";
import { convertFormDataToSendData, getEntityDataNameLabel } from "service/convert";
import { fetchWrapper } from "service/fetch";
import StatusMessageResponse from "model/StatusMessageResponse";
import { StatusMessageContext } from "hooks/status_message";
import { capitalize, cspToLabel, entityNameToLabel } from "service/string";
import EntityFormPanel from "organisms/EntityFormPanel";
import EntityFormRecord from "model/EntityFormRecord";

const rebootOptionPanel: EntityFormRecord = {
  type: 'panel',
  panelName: 'Reboot option',
  keyValueRecords: [
    { type: 'boolean', labelName: 'soft reboot', name: 'type', defaultValue: false, id: 'edit-type', class: 'form-edit-type' },
  ]
}

const EntityMultiXxudPage = ({ cloudServiceProvider, cloudContext, entityName, action }: {
  cloudServiceProvider: string,
  cloudContext: string,
  entityName: string,
  action: string,
}) => {

  const { selectedIdList } = useContext(DataTableSelectedRowsContext);
  const [isLoading, setLoading] = useState(true);
  const [isRefreshing, setRefreshing] = useState(false);
  const { addMessagesByResponse } = useContext(StatusMessageContext);
  const { getJsonData, resetFetchCache } = useDrupalJsonApi();
  const [entityNameList, setEntityNameList] = useState<string[]>([]);
  const [formData, setFormData] = useState<Record<string, any>>({});
  const history = useHistory();

  useEffect(() => {
    setLoading(true);
    const init = async () => {
      const newEntityNameList = [];
      for (const id of selectedIdList) {
        let url = '';
        if (entityName !== "cloud_launch_template") {
          const entityTypeId = `${cloudServiceProvider}_${entityName}`;
          url = `/jsonapi/${entityTypeId}/${entityTypeId}?filter[drupal_internal__id]=${id}`;
        } else {
          url = `/jsonapi/cloud_launch_template/${cloudServiceProvider}?filter[drupal_internal__id]=${id}`;
        }

        const data = await getJsonData<{ data: EntityData[] }>(url, { data: [] });
        if (data.data.length >= 1) {
          newEntityNameList.push(getEntityDataNameLabel(data.data[0]));
        }
      }
      setEntityNameList(newEntityNameList);
      setLoading(false);
    };
    init();

    setFormData({...formData, type: false});
  }, [selectedIdList]);

  useEffect(() => {
    console.group('FormData (EntityMultiXxudPage)');
    console.log(formData);
    console.groupEnd();
  }, [formData]);

  if (selectedIdList.length < 1) {
    history.push(
      entityName !== "cloud_launch_template"
        ? `/${cloudServiceProvider}/${cloudContext}/${entityName}`
        : `/design/server_template/${cloudContext}`);
  }

  const save = async () => {
    // Send request.
    setRefreshing(true);

    const entityNamePath = entityName !== 'cloud_launch_template'
      ? `${cloudServiceProvider}_${entityName}`
      : entityName;
    const url = `/cloud_dashboard/${cloudServiceProvider}/${cloudContext}/${entityNamePath}/${action}`;
    const requestBody = new FormData();
    requestBody.append('idList', JSON.stringify(selectedIdList));
    for (const pair of Object.entries(formData)) {
      requestBody.append(pair[0], `${pair[1]}`);
    }
    const result = await fetchWrapper(url, { method: 'POST', body: requestBody });
    setRefreshing(false);

    // Sequence when the request fails.
    if (!result.ok) {
      // Status message.
      if (result.error instanceof Response) {
        const responseJson = await result.error.json();
        const messages: StatusMessageResponse = (responseJson)['messages'];
        if (messages) {
          addMessagesByResponse(messages);
          return;
        }
      }
      return;
    }

    // Console log.
    console.group(`${capitalize(action)} Request`);
    console.log('Response:');
    const responseJson = await result.response.json();
    console.log(responseJson);
    console.groupEnd();

    // Status message.
    const messages: StatusMessageResponse = (responseJson)['messages'];
    if (messages) {
      addMessagesByResponse(messages, {
        life: 2,
      });
    }
    resetFetchCache();

    // Redirect.
    history.push(
      entityName !== "cloud_launch_template"
        ? `/${cloudServiceProvider}/${cloudContext}/${entityName}`
        : `/design/server_template/${cloudContext}`);
  }

  return entityNameList.length < 1 || isLoading
    ? <LoadingDataPage />
    : <>
      <StatusMessages />
      <PageTitle text={`Are you sure you want to ${action.replace('_multiple', '')} these ${capitalize(entityNameToLabel(entityName))}s?`} />
      <Breadcrumbs />
      <SideBar />
      <Container fluid className="px-0">
        <Row className="mx-0">
          <Col>
            <Form>
              <EntityFormLabel label="This action cannot be undone." entityName={entityName} />
              <div className="item-list">
                <ul data-drupal-selector="edit-entities">
                  {
                    entityNameList.map((name) => {
                      return <li key={name}>{name}</li>
                    })
                  }
                </ul>
              </div>
              {
                action === 'reboot_multiple'
                  ? <EntityFormPanel
                    cloudServiceProvider={cloudServiceProvider}
                    cloudContext={cloudContext}
                    entityName={entityName}
                    actionType={action}
                    entityRecord={rebootOptionPanel}
                    formData={formData}
                    setFormData={setFormData} />
                  : <></>
              }
              <Button variant={
                action.includes('delete') ? 'danger' : 'primary'
              } disabled={isRefreshing} onClick={save}>
                <LabelText text={capitalize(action.replace('_multiple', ''))} />
              </Button>
              <Link className="btn btn-danger" to={
                entityName !== "cloud_launch_template"
                ? `/${cloudServiceProvider}/${cloudContext}/${entityName}`
                : `/design/server_template/${cloudContext}`
              }>
                <Glyphicon type="remove" />{Drupal.t('Cancel')}
              </Link>
            </Form>
          </Col>
        </Row>
      </Container>
    </>;

}

export default EntityMultiXxudPage;
