import ENTITY_INFO_SUB_LIST from 'constant/templates/entity/entity_info_sub_template';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { useSubEntityData } from 'hooks/entity_data';
import EntityColumn from 'model/EntityColumn';
import EntityData from 'model/EntityData';
import EntityInfoPanelData from 'model/EntityInfoPanelData';
import EntityInfoRecord from 'model/EntityInfoRecord';
import EntityInfoRecordData from 'model/EntityInfoRecordData';
import EntityInfoTemplate from 'model/EntityInfoTemplate';
import LoadingSpinner from 'molecules/LoadingSpinner';
import Breadcrumbs from 'organisms/Breadcrumbs';
import EntityInfoPanel from 'organisms/EntityInfoPanel';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { convertDataToTextForUI } from 'service/convert';
import { getReplacedUrl } from 'service/string';

/**
 * Custom Hooks for detail page's template.
 */
const useSubDetailTemplate = () => {

  const params: {
    cloudServiceProvider: string,
    cloudContext: string,
    entityName: string,
    entityId: string,
    subType: string,
    subEntityId: string,
  } = useParams();

  const entitySubDetailTemplateList = ENTITY_INFO_SUB_LIST.filter((template) => {
    return template.cloudServiceProvider === params.cloudServiceProvider
      && template.entityName === params.entityName + '_' + params.subType;
  });

  return {
    subDetailTemplate: entitySubDetailTemplateList.length >= 1
      ? entitySubDetailTemplateList[0] : undefined
  };

}

/**
 * Custom Hooks for panel data.
 *
 * @param cloudContext The cloud context.
 * @param subDetailTemplate The detail info template for entity.
 * @param entityId The entity's ID.
 * @param subEntityId The sub entity's ID.
 */
const useSubPanelData = (
  entityData: EntityData | undefined,
  cloudContext: string,
  subDetailTemplate: EntityInfoTemplate,
) => {

  const params: {
    entityName: string,
  } = useParams();
  const { readDataCache, getJsonData } = useDrupalJsonApi();
  const [panelDataList, setPanelDataList] = useState<EntityInfoPanelData[]>([]);

  /**
   * Calculate the information on the panel
   * to be displayed from the entity's information.
   *
   * @param dataCache The data cache.
   * @param tableRecordList The record list of table.
   * @param entityColumn The form record's information.
   */
  const getEntityInfoRecordData = async (
    dataCache: Record<string, EntityData[]>,
    tableRecordList: string[],
    entityColumn: EntityColumn
  ) => {
    // Keep the panel from displaying
    // while the entity's information is being read.
    if (!entityData) {
      return {
        type: 'div',
        key: entityColumn.labelName,
        value: '',
        id: entityColumn.id,
        class: entityColumn.class
      } as EntityInfoRecordData;
    }

    switch (entityColumn.type) {
      case 'metrics': {
        const url = `/clouds/${subDetailTemplate.cloudServiceProvider}/${entityData.attributes['cloud_context']}/${params.entityName}/${entityData.attributes['drupal_internal__id']}/metrics`;
        const metricsData = await getJsonData<Record<string, number>[]>(url, []);
        return {
          type: 'metrics',
          record: entityColumn.column.map((column) => {
            return {
              title: column.title,
              yLabel: column.yLabel,
              record: metricsData.map((metrics) => {
                return { x: metrics['timestamp'], y: metrics[column.name] };
              })
            }
          }),
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;
      }
      case 'custom-table': {
        const keyVal: Record<string, string>[] = [];
        const labelData: Record<string, string> = {};
        entityData.attributes[entityColumn.name].forEach((record: any, index: number) => {
          const temp: Record<string, string> = {};
          for (const column of entityColumn.column) {
            labelData[column.name] = column.labelName;
            const convertedText = convertDataToTextForUI(
              record[column.name],
              column,
              dataCache
            );
            const convertedText2 = column.type === 'link'
              ? getReplacedUrl(
                convertedText,
                entityData.attributes['cloud_context'],
                params.entityName,
                entityData.attributes['drupal_internal__id'],
                subDetailTemplate.cloudServiceProvider,
                index
              ) : convertedText;
            temp[column.name] = convertedText2;
          }
          keyVal.push(temp);
        });
        return {
          type: 'table',
          title: entityColumn.labelName,
          record: keyVal,
          label: labelData,
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;
      }
      case 'json-table': {
        const entityColumnList = entityColumn.column;
        const keyVal: Record<string, string>[] = [];

        for (const record of entityData.attributes[entityColumn.name]) {
          const record2: {
            item_key: string,
            item_value: string
          } = record;
          const record3: Record<string, any> = JSON.parse(record2.item_value);

          const temp: Record<string, string> = {};
          for (const entityColumn of entityColumnList) {
            if (entityColumn.name in record3) {
              temp[entityColumn.name] = convertDataToTextForUI(
                record3[entityColumn.name],
                entityColumn,
                dataCache
              );
            }
          }
          keyVal.push(temp);
        }
        return {
          type: 'table',
          title: entityColumn.labelName,
          record: keyVal,
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;
      }
      case 'array-table': {
        const keyVal: Record<string, string>[] = [];
        const rows = entityData.attributes[entityColumn.name];
        for (const row of rows) {
          const values: string[] = row.split(',');
          const temp: Record<string, string> = {};
          entityColumn.column.forEach((column, index) => {
            temp[column.name] = convertDataToTextForUI(
              values[index],
              column,
              dataCache
            );
          });
          keyVal.push(temp);
        }
        return {
          type: 'table',
          title: entityColumn.labelName,
          record: keyVal,
          id: entityColumn.id,
          class: entityColumn.class
        } as EntityInfoRecordData;
      }
      default:
        if (tableRecordList.includes(entityColumn.name)) {
          const keyVal: Record<string, string>[] = [];
          for (const record of entityData.attributes[entityColumn.name]) {
            const temp: Record<string, string> = {};
            for (const key of Object.keys((record))) {
              temp[key] = record[key] ? `${record[key].replace(/(\r\n)|\n/, '')}` : '';
            }
            keyVal.push(temp);
          }
          return {
            type: 'table',
            title: entityColumn.labelName,
            record: keyVal,
            id: entityColumn.id,
            class: entityColumn.class
          } as EntityInfoRecordData;
        } else {
          const data = entityColumn.type !== 'fraction'
            ? entityData.attributes[entityColumn.name]
            : [
              entityData.attributes[entityColumn.name],
              entityData.attributes[entityColumn.denominator_name]
            ];
          const convertedText = convertDataToTextForUI(
            data,
            entityColumn,
            dataCache
          );
          return {
            type: 'div',
            key: entityColumn.labelName,
            value: convertedText,
            id: entityColumn.id,
            class: entityColumn.class
          } as EntityInfoRecordData;
        }
    }
  }

  /**
   * Calculate the information on the panel
   * to be displayed from the entity's information.
   *
   * @param entityInfoRecord The information of the form.
   */
  const getPanelData = async (entityInfoRecord: EntityInfoRecord) => {
    // Keep the panel from displaying
    // while the entity's information is being read.
    const panelData: EntityInfoPanelData = {
      title: entityInfoRecord.panelName,
      records: []
    };
    if (!entityData) {
      return panelData;
    }

    const dataCache = await (readDataCache(entityInfoRecord.keyValueRecords, cloudContext));
    for (const entityColumn of entityInfoRecord.keyValueRecords) {
      panelData.records.push(await getEntityInfoRecordData(
        dataCache,
        entityInfoRecord.tableRecordList,
        entityColumn,
      ));
    }

    return panelData;
  }

  /**
   * Calculate the information on the panel
   * to be displayed from the entity's information.
   */
  const getPanelDataList = async () => {
    // Keep the panel from displaying
    // while the entity's information is being read.
    if (!entityData) {
      return [];
    }

    const panelDataList: EntityInfoPanelData[] = [];
    for (const entityInfoRecord of subDetailTemplate.entityRecords) {
      panelDataList.push(await getPanelData(entityInfoRecord));
    }
    return panelDataList;
  }

  useEffect(() => {
    getPanelDataList().then((data) => {
      setPanelDataList(data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [entityData]);

  return {
    panelDataList
  };

}

/**
 * Default page if form data cannot be retrieved.
 */
const DefaultPage = () => {

  const params: {
    cloudServiceProvider: string,
    cloudContext: string,
    entityName: string,
    entityId: string,
    subType: string,
    subEntityId: string,
  } = useParams();

  return <>
    <PageTitle />
    <Breadcrumbs />
    <SideBar />
    <Container fluid className="px-0">
      <Row>
        <Col>
          <span>EntityDetailPage</span><br />
          <span>cloudServiceProvider={params.cloudServiceProvider}</span><br />
          <span>cloudContext={params.cloudContext}</span><br />
          <span>entityName={params.entityName}</span><br />
          <span>entityId={params.entityId}</span><br />
          <span>subType={params.subType}</span><br />
          <span>subEntityId={params.subEntityId}</span><br />
        </Col>
      </Row>
    </Container>
  </>;

}

/**
 * Page of the view of detail info an entity.
 *
 * @param cloudContext The cloud context.
 * @param detailTemplate The detail info template for entity.
 * @param entityId The entity's ID.
 */
const EntitySubDetailPageImpl = ({ cloudContext, subDetailTemplate, entityId, subEntityId }: {
  cloudContext: string,
  subDetailTemplate: EntityInfoTemplate,
  entityId: string,
  subEntityId: string,
}) => {

  const { entityData } = useSubEntityData(
    subDetailTemplate.cloudServiceProvider,
    subDetailTemplate.entityName,
    entityId,
    subEntityId,
  );
  const { panelDataList } = useSubPanelData(entityData, cloudContext, subDetailTemplate);

  return <>
    <PageTitle entityData={entityData} />
    <Breadcrumbs entityData={entityData} />
    <SideBar />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          {
            panelDataList.length > 0
              ? panelDataList.map((panelData, index) => {
                return <EntityInfoPanel key={index} panelData={panelData} />;
              })
              : <LoadingSpinner />

          }
        </Col>
      </Row>
    </Container>
  </>;

}

/**
 * Page of the view of detail info an entity.
 */
const EntitySubDetailPage = () => {

  const params: {
    cloudContext: string,
    entityId: string,
    subEntityId: string,
  } = useParams();
  const { subDetailTemplate } = useSubDetailTemplate();

  if (!subDetailTemplate) {
    return <DefaultPage />;
  }

  return <EntitySubDetailPageImpl
    cloudContext={params.cloudContext}
    subDetailTemplate={subDetailTemplate}
    entityId={params.entityId}
    subEntityId={params.subEntityId} />;

}

export default EntitySubDetailPage;
