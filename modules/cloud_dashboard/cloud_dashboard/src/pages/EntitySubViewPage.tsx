import LabelText from 'atoms/LabelText';
import { CloudContextListContext } from 'hooks/cloud_context_list';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { useEntityData } from 'hooks/entity_data';
import DataColumn from 'model/DataColumn';
import DataRecord from 'model/DataRecord';
import EntityColumn from 'model/EntityColumn';
import EntityData from 'model/EntityData';
import EntityInfoTabTemplate from 'model/EntityInfoTabTemplate';
import SortInfo from 'model/SortInfo';
import LoadingSpinner from 'molecules/LoadingSpinner';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import DataTable from 'organisms/DataTable';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext, useEffect, useState } from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { convertEntityDataToDataRecord } from 'service/convert';
import { cspToLabel } from 'service/string';

const InfoTabs = ({ cloudContext, tabTemplate, entityId, subType }: {
  cloudContext: string,
  tabTemplate: EntityInfoTabTemplate,
  entityId: string,
  subType: string
}) => {

  const url = `/${tabTemplate.cloudServiceProvider}/${cloudContext}/${tabTemplate.entityName}/${entityId}`;

  return <nav className="tabs">
    <div>
      <ul className="nav nav-tabs" id="tab_wrap">
        <li>
          <Link to={url}
            className="active is-active ripple-effect">
            View
          </Link>
        </li>
        {
          tabTemplate.tabs.map((tabColumn) => {
            return <li className={subType === tabColumn.name ? 'active' : ''} key={tabColumn.name}>
              <Link to={`${url}/${tabColumn.name}`}
                className="active is-active ripple-effect">
                {tabColumn.labelName}
              </Link>
            </li>;
          })
        }
      </ul>
    </div>
  </nav>;

}

const EntitySubViewPage = ({
  cloudContext,
  tabTemplate,
  entityId,
  subType,
}: {
  cloudContext: string,
  tabTemplate: EntityInfoTabTemplate,
  entityId: string,
  subType: string,
}) => {

  const { cloudContextList } = useContext(CloudContextListContext);
  const { getEntityListAll } = useDrupalJsonApi();
  const { entityData } = useEntityData(tabTemplate.cloudServiceProvider, tabTemplate.entityName, entityId);
  const [dataColumnList, setDataColumnList] = useState<DataColumn[]>([]);
  const [dataRecordList, setDataRecordList] = useState<DataRecord[]>([]);
  const [sortInfo, setSortInfo] = useState<SortInfo>({
    key: '', direction: 'ASC'
  });
  const [isLoading, setLoading] = useState(true);

  const getSubTemplate = () => {
    const subTemplates = tabTemplate.tabs.filter((template) => {
      return template.name === subType;
    });

    return subTemplates.length >= 1 ? subTemplates[0] : undefined;
  }

  const getPath = () => {
    const subTemplate = getSubTemplate();
    return subTemplate && subTemplate.type === 'table'
      ? subTemplate.path : '';
  }

  useEffect(() => {
    const init = async () => {
      // Load column data.
      const subTemplate = getSubTemplate();
      if (subTemplate && subTemplate.type === 'table') {
        // Load column list.
        let newDataColumnList: DataColumn[] = subTemplate.columns.map((column) => {
          return { key: column.name, label: column.labelName };
        });
        setDataColumnList(newDataColumnList);

        // Load filter list.
        let filterList: {
          [key: string]: string;
        }[] = [{
          'filter[cloud_context]': cloudContext,
        }];
        for (const filterInfo of subTemplate.filter) {
          /* Create the key and value for the filter settings. */
          const filterKey = `filter[${filterInfo.key}]`;
          let filterValue = filterInfo.value;
          filterValue = filterValue.replace('{entityId}', entityId);
          // Regular expression to extract 'foo' from the string '{foo}'.
          const reg = new RegExp('{(.+?)}');
          const match = filterValue.match(reg);
          if (match && entityData && match[1] in entityData.attributes) {
            filterValue = filterValue.replace(`{${match[1]}}`, entityData.attributes[match[1]]);
          }

          /* Create the filter list. */
          const newFilterList = [];
          for (const filterRecord of filterList) {
            if (filterKey in filterRecord) {
              newFilterList.push({
                ...filterRecord
              });
              newFilterList.push({
                ...filterRecord,
                [filterKey]: filterValue
              });
            } else {
              newFilterList.push({
                ...filterRecord,
                [filterKey]: filterValue
              });
            }
          }
          filterList = newFilterList;
        }

        // Load entity data.
        const entityTypeId = subTemplate.entityTypeId;
        const rawData: EntityData[] = [];
        for (const filter of filterList) {
          const rawDataPart = await getEntityListAll(entityTypeId, filter);
          rawData.push(...rawDataPart);
        }
        setDataRecordList(convertEntityDataToDataRecord(entityTypeId, rawData, subTemplate.columns as EntityColumn[], cloudContextList, {}));
        setLoading(false);
      }
    }

    init();
  }, [cloudContext, tabTemplate, entityId, subType, entityData]);

  const hasOperationLinks = () => {
    const subTemplate = getSubTemplate();
    return (subTemplate !== undefined) && subTemplate.type === 'table' && (subTemplate.operationsLinks !== undefined);
  }

  const getOperationLinks = () => {
    const subTemplate = getSubTemplate();
    if (subTemplate === undefined || subTemplate.type !== 'table' || subTemplate.operationsLinks === undefined) {
      return undefined;
    }

    return subTemplate.operationsLinks.map((operationLink) => {
      return {
        label: operationLink.label,
        path: operationLink.path
          .replaceAll('{entityId}', entityId)
          .replaceAll('{cloud_context}', cloudContext)
          .replaceAll('{subEntityId}', '{entityId}'),
      }
    });
  }

  const getActionButtons = () => {
    const subTemplate = getSubTemplate();
    if (subTemplate === undefined || subTemplate.type !== 'table') {
      return [];
    }

    return subTemplate.actionButtons;
  }

  return <>
    <StatusMessages />
    <PageTitle text={`${cspToLabel(tabTemplate.cloudServiceProvider)} ${tabTemplate.entityName} ${subType}s`} />
    <Breadcrumbs />
    <SideBar isOpenCsp />
    <InfoTabs cloudContext={cloudContext} tabTemplate={tabTemplate} entityId={entityId} subType={subType} />
    <Container fluid className="px-0">
      <Row>
        <Col>
          <Form>
            {
              isLoading
                ? <LoadingSpinner />
                : <>
                  <Form.Group>
                    <ul className="list-inline local-actions">
                      {
                        getActionButtons().map((actionButton, index) => {
                          return <li key={index}>
                            <Link
                              key={actionButton.label}
                              to={`/openstack/${cloudContext}/${tabTemplate.entityName}/${entityId}/${actionButton.action}`}
                              className="btn btn-primary">
                              <LabelText text={actionButton.label} />
                            </Link>
                          </li>;
                        })
                      }
                    </ul>
                  </Form.Group>
                  <DataTable
                    dataColumnList={dataColumnList}
                    dataRecordList={dataRecordList}
                    sortInfo={sortInfo}
                    setSortInfo={setSortInfo}
                    hasOperationLinks={hasOperationLinks()}
                    operationLinksName="Operations links"
                    operationsLinks={getOperationLinks()}
                    isShowCheckBox={false}
                    detailInfo={
                      typeof getSubTemplate() !== 'undefined'
                        ? {
                          column: 'name',
                          path: getPath().replace('{entityId}', entityId)
                        }
                        : undefined
                    } />
                </>
            }
          </Form>
        </Col>
      </Row>
    </Container>
  </>;

}

export default EntitySubViewPage;
