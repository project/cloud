import LabelText from 'atoms/LabelText';
import ENTITY_INFO_LIST from 'constant/templates/entity/entity_info_template';
import { MENU_TEMPLATE_LIST } from 'constant/templates/menu/menu_template';
import { DataTableSelectedRowsContext } from 'hooks/data_table_selected_row';
import { StatusMessageContext } from 'hooks/status_message';
import CloudServiceProvider from 'model/CloudServiceProvider';
import MenuTemplate from 'model/MenuTemplate';
import ActionButtonGroup from 'molecules/ActionButtonGroup';
import EntityTabs from 'molecules/EntityTabs';
import FieldSelect from 'molecules/FieldSelect';
import ItemCountLabel from 'molecules/ItemCountLabel';
import ItemPerPageSelector from 'molecules/ItemPerPageSelector';
import PageSelector from 'molecules/PageSelector';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import EntityTable from 'organisms/EntityTable';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext, useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import { getEntityListViewUrl, getEntityTypeId } from 'service/string';

const calcDetailInfoColumn = (menuTemplate: MenuTemplate) => {
  // If the entity_info_template is not registered, it will not be detected.
  const registeredTemplateList = ENTITY_INFO_LIST.filter((e) => {
    return e.cloudServiceProvider === menuTemplate.cloudServiceProvider
      && e.entityName === menuTemplate.entityName;
  });
  if (registeredTemplateList.length < 1) {
    return undefined;
  }

  // Detect a property whose name is 'name' and sets a link to it.
  const nameColumnList = menuTemplate.entityColumn.filter((e) => {
    return e.name === 'name';
  })
  if (nameColumnList.length > 0) {
    return nameColumnList[0].name;
  }

  // Detect a property whose name contains 'name' and set links to it.
  const likeNameColumnList = menuTemplate.entityColumn.filter((e) => {
    return e.name.includes('name');
  })
  if (likeNameColumnList.length > 0) {
    return likeNameColumnList[0].name;
  }

  // Default value when automatic detection is not possible.
  return undefined;
}

const EntityViewPage = () => {
  const params: {
    cloudServiceProvider: string,
    cloudContext?: string,
    entityName: string
  } = useParams();

  const [namespace, setNamespace] = useState<string>('');
  const [namespaceName, setNamespaceName] = useState<string>('');
  const [itemCount, setItemCountImpl] = useState(0);
  const [itemPerPage, setItemPerPageImpl] = useState(50);
  const [pageIndex, setPageIndex] = useState(0);
  const { messages, removeMessage } = useContext(StatusMessageContext);
  const { selectedIdList } = useContext(DataTableSelectedRowsContext);
  const [multiActionType, setMultiActionType] = useState('');
  const history = useHistory();

  useEffect(() => {
    console.group('EntityViewPage');
    console.log('selectedIdList', selectedIdList);
    console.groupEnd();
  },  [selectedIdList]);

  useEffect(() => {
    if (namespace !== '') {
      setNamespaceName('');
    }
  }, [namespace]);

  useEffect(() => {
    if (namespaceName !== '') {
      setNamespace('');
    }
  }, [namespaceName]);

  useEffect(() => {
    window.scrollBy(0, -window.pageYOffset);
  }, [pageIndex]);

  const menuTemplate = MENU_TEMPLATE_LIST.filter((m) => {
    return m.cloudServiceProvider === params.cloudServiceProvider
      && m.entityName === params.entityName;
  })[0];
  const namespaceFlg = menuTemplate.entityColumn.map((c) => c.labelName).includes('Namespace');
  const namespaceNameFlg = menuTemplate.entityColumn.map((c) => c.labelName).includes('Namespace name');
  const entityTypeId = getEntityTypeId(menuTemplate);
  const detailInfoColumn = calcDetailInfoColumn(menuTemplate);

  const setItemCount = (n: number) => {
    setItemCountImpl(n);
    setPageIndex(0);
  }

  const setItemPerPage = (n: number) => {
    setItemPerPageImpl(n);
    setPageIndex(0);
  }

  useEffect(() => {
    if (messages.length >= 1) {
      for (let i = 0; i < messages.length; i++) {
        if (!messages[i].text.includes('Download')) {
          continue;
        }
        const element = document.getElementById('link-1');
        if (element === null) {
          continue;
        }
        element.click();
        removeMessage(i);
        break;
      }
    }
  }, [messages]);

  const multiAction = () => {
    if (multiActionType !== '' && selectedIdList.length > 0) {
      history.push(`/${menuTemplate.cloudServiceProvider}/${params.cloudContext}/${menuTemplate.entityName}/${multiActionType}`);
    }
  }

  return <>
    <StatusMessages />
    <PageTitle />
    <Breadcrumbs />
    <SideBar isOpenCsp/>
    <EntityTabs menuType={menuTemplate.cloudServiceProvider} entityName={menuTemplate.entityName} cloudContext={params.cloudContext} />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <Form>
            {
              params.cloudContext !== undefined
                ? <ActionButtonGroup menuTemplate={menuTemplate} cloudContext={params.cloudContext} />
                : <></>
            }
            {
              namespaceFlg
                ? <Form.Group className="d-flex" style={{ marginTop: '2rem' }}>
                  <Form.Label className="text-nowrap mt-1">Namespace</Form.Label>
                  <FieldSelect
                    columnKey="namespace"
                    columnName={namespace}
                    setColumnName={setNamespace}
                    cloudContext={{
                      cloudServiceProvider: params.cloudServiceProvider as CloudServiceProvider,
                      name: params.cloudContext ? params.cloudContext : 'ALL',
                      labelName: ''
                    }}
                  />
                </Form.Group>
                : <></>
            }
            {
              namespaceNameFlg
                ? <Form.Group className="d-flex" style={{ marginTop: '2rem' }}>
                  <Form.Label className="text-nowrap mt-1">Namespace Name</Form.Label>
                  <FieldSelect
                    columnKey="namespace"
                    columnName={namespaceName}
                    setColumnName={setNamespaceName}
                    cloudContext={{
                      cloudServiceProvider: params.cloudServiceProvider as CloudServiceProvider,
                      name: params.cloudContext ? params.cloudContext : 'ALL',
                      labelName: ''
                    }}
                  />
                </Form.Group>
                : <></>
            }
            {
              itemCount > 0
                ? <ItemPerPageSelector
                  itemPerPage={itemPerPage}
                  setItemPerPage={setItemPerPage}
                />
                : <></>
            }
            {
              menuTemplate.multiActions
                ? <Form.Group className="d-flex" style={{ marginTop: '2rem' }}>
                    <Form.Label htmlFor="edit-action" className="text-nowrap mt-1">Action</Form.Label>
                      <Form.Select id="edit-action" className="ms-3 w-auto" value={multiActionType}
                      onChange={(e) => {
                        setMultiActionType(e.currentTarget.value);
                      }}>
                      <option value="">- {Drupal.t('Select')} -</option>
                      {menuTemplate.multiActions.map((action) => {
                        return <option value={action.urlSuffix} key={action.urlSuffix}>{action.label}</option>
                      })}
                    </Form.Select>
                    <Button variant="primary" onClick={multiAction}>
                      <LabelText text="Apply to selected items" />
                    </Button>
                  </Form.Group>
              : <></>
            }
            <Form.Group style={{ marginTop: '2rem' }}>
              <ItemCountLabel
                cloudServiceProvider={params.cloudServiceProvider}
                cloudContext={params.cloudContext}
                entityTypeId={entityTypeId}
                namespace={namespace}
                namespaceName={namespaceName}
                itemCount={itemCount}
                setItemCount={setItemCount} />
            </Form.Group>
          </Form>
        </Col>
      </Row>
    </Container>
    <PageSelector
      pageIndex={pageIndex}
      setPageIndex={setPageIndex}
      itemCount={itemCount}
      itemPerPage={itemPerPage} />
    <Container fluid className="px-0">
      <Row>
        <Col>
          <Form>
            <EntityTable
              entityTypeId={entityTypeId}
              entityColumnList={menuTemplate.entityColumn}
              namespace={namespace}
              namespaceName={namespaceName}
              pageIndex={pageIndex}
              cloudContext={params.cloudContext}
              itemPerPage={itemPerPage}
              detailInfo={
                typeof detailInfoColumn !== 'undefined'
                  ? {
                    column: detailInfoColumn,
                    path: getEntityListViewUrl(menuTemplate)
                  }
                  : undefined} />
          </Form>
        </Col>
      </Row>
    </Container>
    <PageSelector
      pageIndex={pageIndex}
      setPageIndex={setPageIndex}
      itemCount={itemCount}
      itemPerPage={itemPerPage} />
  </>;
}

export default EntityViewPage;
