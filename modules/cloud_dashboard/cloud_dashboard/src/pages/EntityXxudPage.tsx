import Glyphicon from 'atoms/Glyphicon';
import LabelText from 'atoms/LabelText';
import { IMAGE_VISIBILITY_LIST } from 'constant/others/other';
import ENTITY_INFO_TAB_LIST from 'constant/templates/entity/entity_info_tab';
import ENTITY_FORM_LIST from 'constant/templates/form/entity_form_template';
import { CloudContextListContext } from 'hooks/cloud_context_list';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import useDrupalTranslation from 'hooks/drupal_translation';
import { useEntityData } from 'hooks/entity_data';
import { StatusMessageContext } from 'hooks/status_message';
import EntityData from 'model/EntityData';
import EntityFormColumn from 'model/EntityFormColumn';
import EntityFormTemplate from 'model/EntityFormTemplate';
import ScriptToken from 'model/ScriptToken';
import StatusMessageResponse from 'model/StatusMessageResponse';
import EntityFormLabel from 'molecules/EntityFormLabel';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import EntityFormPanel from 'organisms/EntityFormPanel';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import EntitySubViewPage from 'pages/EntitySubViewPage';
import LoadingDataPage from 'pages/LoadingDataPage';
import { useContext, useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { Link, useHistory, useParams } from 'react-router-dom';
import { convertFormDataToSendData, getEntityDataNameLabel } from 'service/convert';
import { fetchWrapper } from 'service/fetch';
import { capitalize, cspToLabel, entityNameToLabel } from 'service/string';

const getValue = (token: ScriptToken) => {
  return token.type === 'text' || token.type === 'keyword' || token.type === 'array'
    ? token.value : '';
}

const getValueOrNull = (token: ScriptToken) => {
  return token.type === 'text' || token.type === 'keyword' || token.type === 'array'
    ? token.value : null;
}

const getStringValueOrNull = (token: ScriptToken) => {
  return token.type === 'text' || token.type === 'keyword'
    ? token.value : null;
}

const useFormScript = (cloudContext: string) => {
  const [entity, setEntity] = useState<EntityData | undefined>(undefined);
  const { getJsonData, getEntityListAll } = useDrupalJsonApi();

  /**
   * Split pattern text into separate tokens.
   * i.e. 'aaa(bbb, ccc)' =>
   *   [1] type=keyword, value='aaa'
   *   [2] type=bracket, value='('
   *   [3] type=keyword, value='bbb'
   *   [4] type=comma, value=','
   *   [5] type=keyword, value='ccc'
   *   [6] type=bracket, value=')'
   *
   * @param script The pattern text.
   * @returns The separate tokens.
   */
  const convertScriptToToken = (script: string) => {
    const output: ScriptToken[] = [];
    for (let p = 0; p < script.length; p += 1) {
      const character = script[p];
      if (character.match(/[a-zA-Z._]/) !== null) {
        const keyword = script.slice(p).match(/[a-zA-Z._]+/g);
        if (keyword !== null) {
          output.push({ type: 'keyword', value: keyword[0] });
          p += keyword[0].length - 1;
        }
        continue;
      }

      // Refactored repeated code into a function.
      const processCharacter = (regex: RegExp) => {
        const keyword = script.slice(p).match(regex);
        if (keyword !== null) {
          output.push({ type: 'text', value: keyword[0] });
          p += keyword[0].length - 1;
        }
      }
      if (character === '"') {
        processCharacter(/"[a-zA-Z._]*"/g);
        continue;
      }
      if (character === '\'') {
        processCharacter(/'[a-zA-Z._]*'/g);
        continue;
      }

      // Special characters.
      const SPECIAL_CHARACTERS: Record<string, 'left-bracket' | 'right-bracket' | 'comma'> = {
        '(': 'left-bracket',
        ')': 'right-bracket',
        ',': 'comma',
      };
      if (character in SPECIAL_CHARACTERS) {
        output.push({ type: SPECIAL_CHARACTERS[character] });
        continue;
      }
    }
    return output;
  }

  /**
   * Join tokens to text.
   *
   * @param tokens The separate tokens.
   * @param mode The mode of the output text.
   */
  const convertTokenToScript = (tokens: ScriptToken[], mode: 'script' | 'value') => {
    if (mode === 'value') {
      if (tokens.length === 0) {
        return '';
      }
      return getValue(tokens[0]);
    }

    return tokens.map((token) => {
      switch (token.type) {
        case 'text':
          return token.value;
        case 'keyword':
          return token.value;
        case 'array':
          return token.value.join(',');
        case 'left-bracket':
          return '(';
        case 'right-bracket':
          return ')';
        case 'comma':
          return ',';
        case 'null':
          return 'null';
        default:
          return '';
      }
    }).join();
  }

  /**
   * Search for a pattern of tokens that can be processed.
   * @param tokens The separate tokens.
   * @returns If not detected, null is returned.
   */
  const findTokenPattern = (tokens: ScriptToken[]) => {
    // Replace the token type with a symbol for ease of determination.
    const TYPE_THUMBNAIL = {
      'keyword': 'K',
      'text': 'T',
      'array': 'A',
      'right-bracket': 'R',
      'left-bracket': 'L',
      'comma': 'C',
      'null': 'N'
    }
    const thumbnail = tokens.map((t) => {
      return TYPE_THUMBNAIL[t.type];
    }).join('');
    const matches = thumbnail.match(/KL((K|T|A|N)(C(K|T|A|N))*)R/g);
    console.group('TokenThumbnail');
    console.debug(thumbnail);
    console.debug(matches);
    console.groupEnd();
    if (matches === null) {
      return null;
    }
    const match = matches[0];
    const index = thumbnail.indexOf(match);
    return {
      index,
      length: match.length
    };
  }

  /**
   * Script tokens to shrink tokens.
   *
   * @param tokens The separate tokens.
   */
  const convertTokenToToken = async (tokens: ScriptToken[]): Promise<ScriptToken> => {
    const functionName = tokens[0].type === 'keyword' ? tokens[0].value : '';
    if (tokens[0].type !== 'keyword') {
      return { type: 'null' };
    }
    const parameters: ScriptToken[] = tokens.filter(((_, i) => {
      return i >= 2 && i % 2 === 0;
    }));
    console.group('function');
    console.debug(functionName);
    console.debug(parameters);
    console.groupEnd();
    switch (functionName) {
      case 'relationship': {
        if (entity === undefined || typeof entity.relationships === 'string') {
          return { type: 'null' };
        }

        const relationshipKey1 = getStringValueOrNull(parameters[0]);
        const relationshipKey2 = getStringValueOrNull(parameters[1]);
        if (relationshipKey1 === null || relationshipKey2 === null) {
          return { type: 'null' };
        }

        const rawValue = entity.relationships[relationshipKey1];
        if (rawValue === undefined || rawValue.data === null) {
          return { type: 'null' };
        }

        if (Array.isArray(rawValue.data)) {
          return {
            type: 'array', value: rawValue.data.map((d) => {
              return `${(d !== undefined && 'meta' in d)
                ? d.meta[relationshipKey2]
                : ''
                }`;
            })
          };
        }

        return {
          type: 'text', value: `${(rawValue.data !== undefined && 'meta' in rawValue.data)
            ? rawValue.data.meta[relationshipKey2]
            : ''
            }`
        };
      }
      case 'getValue': {
        const entityTypeId = getStringValueOrNull(parameters[0]);
        const entityValue1 = getValueOrNull(parameters[1]);
        const entityKey1 = getStringValueOrNull(parameters[2]);
        const entityKey2 = getStringValueOrNull(parameters[3]);
        if (entityTypeId === null || entityValue1 === null
          || entityKey1 === null || entityKey2 === null) {
          return { type: 'null' };
        }

        const valueList = [];
        for (const entityValue of Array.isArray(entityValue1) ? entityValue1 : [entityValue1]) {
          const param: Record<string, any> = {};
          param[`filter[${entityKey1}]`] = entityValue;
          param['filter[cloud_context]'] = cloudContext;
          const entityList = await getEntityListAll(entityTypeId, param);
          for (const e of entityList) {
            if (e.attributes[entityKey2] !== null) {
              valueList.push(`${e.attributes[entityKey2]}`);
            }
          }
        }

        if (valueList.length >= 2) {
          return { type: 'array', value: valueList };
        } else if (valueList.length === 1) {
          return { type: 'text', value: valueList[0] };
        } else {
          return { type: 'null' };
        }
      }
      case 'tryNotNull': {
        const value = getValueOrNull(parameters[0]);
        const defaultValue = getValueOrNull(parameters[1]);
        const value2 = value !== null ? value : defaultValue;

        if (value2 === null) {
          return { type: 'null' };
        }
        if (Array.isArray(value2)) {
          return { type: 'array', value: value2 };
        }
        return { type: 'text', value: value2 };
      }
    }

    return { type: 'null' };
  }

  /**
   * Convert default value script to string literal by interpreting them.
   * i.e. 'foo(bar, baz)' => 'xyz'
   *
   * @param script The default value script.
   */
  const calcScript = async (script: string) => {
    // Convert default value script to script tokens.
    const tokens = convertScriptToToken(script);
    console.group('Token');
    console.debug(tokens);
    console.groupEnd();

    // Convert script tokens to string;
    let tokensTemp: ScriptToken[] = JSON.parse(JSON.stringify(tokens));
    while (true) {
      // Search for a pattern of tokens that can be processed.
      const result = findTokenPattern(tokensTemp);
      console.group('TokenPattern');
      console.debug(result);
      console.groupEnd();
      if (result === null) {
        break;
      }

      // Interpret token, convert to string, and replace tokensTemp.
      const result2 = await convertTokenToToken(tokensTemp.slice(result.index, result.index + result.length));
      tokensTemp = [
        ...tokensTemp.slice(0, result.index),
        result2,
        ...tokensTemp.slice(result.index + result.length)
      ];
      console.group('NewToken');
      console.debug(tokensTemp);
      console.groupEnd();
    }
    return convertTokenToScript(tokensTemp, 'value');
  }

  /**
   * Convert default value scripts to string literals by interpreting them.
   * i.e. '{foo}{bar}{baz}' => 'xxxyyyzzz'
   *
   * @param script The default value script.
   */
  const convertDefaultValueTextFromScript = async (script: string) => {
    // If script_string don't contain '{}', it returns script_string.
    const patterns = script.match(/\{[^{}]+?\}/g);
    if (patterns === null || patterns.length === 0) {
      return script;
    }

    // Replace '{keyword}' to 'result_string' from script_string.
    console.group('Script');
    console.debug(script);
    console.debug(patterns);
    let output: string | string[] = script;
    if (patterns[0] === script) {
      output = await calcScript(patterns[0].slice(1, patterns[0].length - 1));
      console.debug(output);
      console.groupEnd();
    } else {
      for (const pattern of patterns) {
        const result = await calcScript(pattern.slice(1, pattern.length - 1));
        output = output.replace(pattern,
          Array.isArray(result) ? result.join(', ') : result
        );
      }
      console.debug(output);
      console.groupEnd();
    }

    return output;
  }

  /**
   * Calculate information to determine default value in form.
   * @param keyValueRecord KeyValueRecord.
   * @param cloudContext The cloud context.
   * @param entityId The entity ID.
   */
  const convertToFormInfoList = async (
    keyValueRecord: EntityFormColumn,
    cloudContext: string,
    entityId: string
  ) => {
    if (keyValueRecord.type === 'time') {
      return [
        { key: keyValueRecord.hourName, type: keyValueRecord.type, defaultValue: keyValueRecord.defaultValue[0] },
        { key: keyValueRecord.minuteName, type: keyValueRecord.type, defaultValue: keyValueRecord.defaultValue[1] }
      ];
    }

    if (keyValueRecord.type === 'file') {
      return [{
        key: keyValueRecord.name,
        type: keyValueRecord.type,
        defaultValue: null
      }];
    }

    if (!('defaultValue' in keyValueRecord)) {
      return [{
        key: keyValueRecord.type !== 'table-with-sort'
          ? keyValueRecord.name : '',
        type: keyValueRecord.type,
        defaultValue: undefined
      }];
    }

    if ('defaultValueUrl' in keyValueRecord && keyValueRecord.defaultValueUrl !== undefined) {
      const replacedUrl = keyValueRecord.defaultValueUrl
        .replaceAll('{cloud_context}', cloudContext)
        .replaceAll('{entity_id}', entityId);
      if (!replacedUrl.endsWith('/')) {
        const jsonData = await getJsonData<string>(replacedUrl, '');
        if (jsonData.length > 0) {
          return [{
            key: keyValueRecord.name,
            type: keyValueRecord.type,
            defaultValue: jsonData,
          }];
        }
      }
    }

    if (Array.isArray(keyValueRecord.defaultValue)) {
      const defaultValueArray = [];
      for (const record of keyValueRecord.defaultValue) {
        if (typeof record === 'string') {
          const defaultValueText = await convertDefaultValueTextFromScript(record);
          for (const defaultValueTextRecord of Array.isArray(defaultValueText) ? defaultValueText : [defaultValueText]) {
            defaultValueArray.push(defaultValueTextRecord);
          }
        } else {
          defaultValueArray.push(record);
        }
      }
      return [{
        key: keyValueRecord.name,
        type: keyValueRecord.type,
        defaultValue: defaultValueArray
      }];
    }

    return [{
      key: keyValueRecord.name,
      type: keyValueRecord.type,
      defaultValue: typeof keyValueRecord.defaultValue === 'string'
        ? await convertDefaultValueTextFromScript(keyValueRecord.defaultValue)
        : keyValueRecord.defaultValue
    }];
  }

  return {
    entity,
    setEntity,
    convertToFormInfoList
  };
}

/**
 * Custom Hooks for detail page tab's template.
 */
const useTabTemplate = (
  cloudServiceProvider: string,
  entityName: string,
) => {

  const tabTemplateList = ENTITY_INFO_TAB_LIST.filter((template) => {
    return template.cloudServiceProvider === cloudServiceProvider
      && template.entityName === entityName;
  });

  return {
    tabTemplate: tabTemplateList.length >= 1
      ? tabTemplateList[0] : undefined
  };

}

const InfoTabs = ({ cloudServiceProvider, entityName }: {
  cloudServiceProvider: string,
  entityName: string,
}) => {
  const params: {
    cloudContext: string,
    entityId: string,
    action: string,
  } = useParams();
  const { tabTemplate } = useTabTemplate(cloudServiceProvider, entityName);
  const { t } = useDrupalTranslation();

  if (!tabTemplate) {
    return <></>;
  }

  const url = `/${tabTemplate.cloudServiceProvider}/${params.cloudContext}/${tabTemplate.entityName}/${params.entityId}`;

  return <nav className="tabs">
    <div>
      <ul className="nav nav-tabs" id="tab_wrap">
        <li>
          <Link to={url}
            className="active is-active ripple-effect">
            {Drupal.t('View')}
          </Link>
        </li>
        {
          tabTemplate.tabs.map((tabColumn) => {
            return <li className={tabColumn.name === params.action ? 'active' : ''} key={tabColumn.name}>
              <Link to={`${url}/${tabColumn.name}`}
                className="active is-active ripple-effect">
                {t(tabColumn.labelName)}
              </Link>
            </li>;
          })
        }
      </ul>
    </div>
  </nav>;

}

/**
 * Calculate the redirect URL.
 *
 * @param cloudServiceProvider The cloud service provider.
 * @param cloudContext The cloud context.
 * @param entityName The entity name.
 * @param entityId The entity ID.
 * @param actionType The action type.
 * @param anotherEntityName The another entity name.
 *
 * @returns The redirect URL.
 */
const calcRedirectUrl = (cloudServiceProvider: string, cloudContext: string, entityName: string, entityId: string, actionType: string, anotherEntityName?: string) => {

  // Redirect URL when launched from Cloud Launch Template.
  if (entityName === 'cloud_launch_template' && actionType === 'launch') {
    return `/${cloudServiceProvider}/${cloudContext}/instance`;
  }
  // Redirect URL when created image from Instance.
  if (entityName === 'instance' && actionType === 'create_image') {
    return `/${cloudServiceProvider}/${cloudContext}/image`;
  }
  // Redirect URL for adding or removing interface from OpenStack Router.
  if (cloudServiceProvider === 'openstack' && entityName === 'router' && actionType === 'add_interface') {
    return `/${cloudServiceProvider}/${cloudContext}/${entityName}/${entityId}/interface`;
  }
  if (cloudServiceProvider === 'openstack' && entityName === 'port' && anotherEntityName === 'interface') {
    return `/${cloudServiceProvider}/${cloudContext}/router/${entityId}/interface`;
  }
  // Redirect URL (others).
  return entityName === 'cloud_launch_template'
    ? `/design/server_template/${cloudContext}`
    : `/${cloudServiceProvider}/${cloudContext}/${entityName}`;

}

const calcMessageText = (actionType: string, anotherEntityName?: string) => {
  // The special case.
  if (anotherEntityName === 'interface' && actionType === 'delete') {
    return '@label has been removed an interface';
  }

  // The other case.
  const actionTypeLabel = actionType !== 'edit' ? actionType : 'update';
  let actionTypeLabelPassive;
  if (actionTypeLabel === 'stop') {
    actionTypeLabelPassive = 'stopped';
  } else {
    actionTypeLabelPassive = actionTypeLabel.endsWith('e')
      ? `${actionTypeLabel}d`
      : `${actionTypeLabel}ed`;
  }
  return `The @type @label has been ${actionTypeLabelPassive}.`;
}

const EntityXxudPageImpl = ({ cloudContext, entityFormTemplate, entityName, entityId, anotherEntityName }: {
  cloudContext: string,
  entityFormTemplate: EntityFormTemplate,
  entityName: string,
  entityId: string,
  anotherEntityName?: string,
}) => {
  const [formData, setFormData] = useState<Record<string, any>>({});
  const { entityData } = useEntityData(entityFormTemplate.cloudServiceProvider, entityName, entityId);
  const { addMessage, addErrorMessage, addMessagesByResponse } = useContext(StatusMessageContext);
  const [isRefreshing, setRefreshing] = useState(false);
  const { entity, setEntity, convertToFormInfoList } = useFormScript(cloudContext);
  const { resetFetchCache } = useDrupalJsonApi();
  const params: {
    entityId: string,
  } = useParams();
  const history = useHistory();

  useEffect(() => {
    if (entityData !== undefined) {
      setEntity(entityData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [entityData]);

  useEffect(() => {
    if (entity !== undefined) {
      initForm(entity);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [entity]);

  useEffect(() => {
    console.group('FormData (EntityXxudPage)');
    console.log(formData);
    console.groupEnd();
  }, [formData]);

  const initForm = async (entityData: EntityData) => {
    // Create a list of key strings and default values.
    const formInfoList: { key: string, type: string, defaultValue: any }[] = [];
    for (const entityRecord of entityFormTemplate.entityRecords) {
      // Process only forms contained in Panel.
      if (entityRecord.type !== 'panel') {
        continue;
      }

      // Load the form inside each Panel.
      for (const keyValueRecord of entityRecord.keyValueRecords) {
        const newFormInfoList = await convertToFormInfoList(
          keyValueRecord,
          cloudContext,
          entityId
        );

        for (const formInfo of newFormInfoList) {
          formInfoList.push(formInfo);
        }
      }
    }

    // Load entity data.
    const attributes = entityData.attributes;
    const newFormData: Record<string, any> = {};
    for (const formInfo of formInfoList) {
      // Handling of types that are not entered in the form, such as 'cost' types.
      if (formInfo.defaultValue === undefined) {
        continue;
      }

      // Processing when the value corresponding to a key cannot be read.
      if (!(formInfo.key in attributes)) {
        newFormData[formInfo.key] = formInfo.defaultValue;
        continue;
      }
      const value = attributes[formInfo.key];
      if (value === null || (Array.isArray(value) && value.length === 0)) {
        newFormData[formInfo.key] = formInfo.defaultValue;
        continue;
      }

      // Processing in 'multi-select' type forms.
      if (formInfo.type === 'multi-select'
        || formInfo.type === 'multi-check'
        || formInfo.type === 'array') {
        newFormData[formInfo.key] = typeof value === 'string'
          ? value.split(', ') : value;
        continue;
      }

      // Processing in 'radio' type forms.
      if (typeof value === 'boolean' && formInfo.type === 'radio') {
        newFormData[formInfo.key] = value ? '1' : '0';
        continue;
      }

      // Processing when the raw data is an array type and the input form is different.
      if (Array.isArray(value)) {
        // Processing special handling of allocation pools.
        if (formInfo.key === 'allocation_pools') {
          newFormData[formInfo.key] = value.map((v) => {
            return v.replace(' - ', ',');
          }).join("\n");
          continue;
        }
        // Processing the shared project from an object to an array.
        if (formInfo.key === 'shared_projects') {
          newFormData[formInfo.key] = [...value];
          continue;
        }

        // Processing the Host Routes form.
        if (formInfo.type === 'textarea'
          && formInfo.key === 'host_routes') {
          const items = [];
          for (const line of (value as string[])) {
            const regex = /nexthop: ([0-9.]+), destination: ([0-9.\/]+)/;
            const matches = line.match(regex);
            if (!matches || matches.length !== 3) {
              continue;
            }
            items.push(`${matches[2]}, ${matches[1]}`);
          }
          newFormData[formInfo.key] = items.join("\n");
          continue;
        }

        if (formInfo.type === 'key-value' || formInfo.type === 'sg_permission') {
          newFormData[formInfo.key] = value;
          continue;
        }

        newFormData[formInfo.key] = value.join("\n");
        continue;
      }

      // Processing visibility of Image from value to index.
      if (formInfo.type === 'radio'
        && entityFormTemplate.cloudServiceProvider === 'openstack'
        && entityName === 'image') {
        const name = IMAGE_VISIBILITY_LIST.find((r) => r.key === value)?.name;
        newFormData[formInfo.key] = name ?? '2';
        continue;
      }

      // Processing when copying entities.
      if (typeof value === 'string' && formInfo.key === 'name' && entityFormTemplate.actionType === 'copy') {
        newFormData[formInfo.key] = `copy_of_${value}`;
        continue;
      }

      // Other type.
      newFormData[formInfo.key] = value;
    }

    setFormData(newFormData);
  }

  const save = async () => {
    // Check entity.
    if (!entity) {
      return;
    }

    // Send request.
    setRefreshing(true);

    const entityNameLabel = getEntityDataNameLabel(entity);
    const { cloudServiceProvider, actionType } = entityFormTemplate;
    const entityNamePath = entityName !== 'cloud_launch_template'
      ? `${cloudServiceProvider}_${entityName}`
      : entityName;
    const url = anotherEntityName && entityFormTemplate.actionType === 'delete'
      ? `/cloud_dashboard/${cloudServiceProvider}/${cloudContext}/openstack_router/${params.entityId}/remove_${anotherEntityName}`
      : `/cloud_dashboard/${cloudServiceProvider}/${cloudContext}/${entityNamePath}/${entityId}/${actionType}`;

    const requestBody = convertFormDataToSendData(formData, entityFormTemplate);
    if (anotherEntityName && entityFormTemplate.actionType === 'delete') {
      requestBody.append('port_id', entity.attributes['port_id']);
    }
    const result = await fetchWrapper(url, { method: 'POST', body: requestBody });
    setRefreshing(false);

    // Preparation before sequence processing.
    const actionTypeC = capitalize(actionType);
    const actionTypeLabel = actionType !== 'edit' ? actionType : 'update';
    const entityTypeC = capitalize(entityName.replace('_', ' '));

    // Sequence when the request fails.
    if (!result.ok) {
      // Status message.
      if (result.error instanceof Response) {
        const responseJson = await result.error.json();
        const messages: StatusMessageResponse = (responseJson)['messages'];
        if (messages) {
          addMessagesByResponse(messages);
          return;
        }
      }
      const messageText = `Unable to ${actionTypeLabel} @type @label.`;
      const messageOption = {
        args: {
          '@type': entityTypeC,
          '@label': entityNameLabel,
        },
      };
      addErrorMessage(messageText, messageOption);
      return;
    }

    // Console log.
    console.group(`${actionTypeC} Request`);
    console.log('Response:');
    const responseJson = await result.response.json();
    console.log(responseJson);
    console.groupEnd();

    // Status message.
    const messages: StatusMessageResponse = (responseJson)['messages'];
    if (messages) {
      addMessagesByResponse(messages, {
        life: 2,
      });
    } else {
      const messageOption = {
        args: {
          '@type': entityTypeC,
          '@label': entityNameLabel,
        },
        life: 2,
      };
      addMessage(calcMessageText(actionType, anotherEntityName), messageOption);
    }
    resetFetchCache();

    const redirectUrl = calcRedirectUrl(
      cloudServiceProvider,
      cloudContext,
      entityName,
      anotherEntityName && entityFormTemplate.actionType === 'delete' ? params.entityId : entityId,
      actionType,
      anotherEntityName);

    history.push(redirectUrl);
  }

  const saveButtonLabel = anotherEntityName && entityFormTemplate.actionType === 'delete'
    ? `Remove ${anotherEntityName}`
      : entityFormTemplate.submitButtonLabel
        ? entityFormTemplate.submitButtonLabel
        : 'Save';

  // OpenStack instance create_image page (failed)
  if (entityFormTemplate.cloudServiceProvider === 'openstack'
    && entityName === 'instance'
    && entityFormTemplate.actionType === 'create_image'
    && entityData !== undefined
    && entityData.attributes['instance_state'] !== 'stopped') {
    return <>
      <StatusMessages />
      <PageTitle entityData={entityData} />
      <Breadcrumbs entityData={entityData} />
      <SideBar />
      <Container fluid className="px-0">
        <Row className="mx-0">
          <Col>
            <Form>
              <EntityFormLabel label="For performance reasons, instance needs to be stopped before creating image."
                entityName={entityName} entityData={entityData} />
              <Link className="btn btn-danger"
                to={`/${entityFormTemplate.cloudServiceProvider}/${cloudContext}/${entityFormTemplate.entityName}/${entityId}`}>
                <Glyphicon type="remove" />{Drupal.t('Cancel')}
              </Link>
            </Form>
          </Col>
        </Row>
      </Container>
    </>;
  }

  return entityData === undefined
    ? <LoadingDataPage />
    : <>
      <StatusMessages />
      <PageTitle entityData={entityData} action={entityFormTemplate.actionType} />
      <Breadcrumbs entityData={entityData} />
      <SideBar />
      <InfoTabs cloudServiceProvider={entityFormTemplate.cloudServiceProvider} entityName={entityName} />
      <Container fluid className="px-0">
        <Row className="mx-0">
          <Col>
            <Form>
              {
                entityFormTemplate.actionType !== 'launch'
                  ? <Form.Group className="mb-4">
                    <Link className="btn btn-primary" to={
                      entityFormTemplate.entityName !== 'cloud_launch_template'
                        ? `/${entityFormTemplate.cloudServiceProvider}/${cloudContext}/${entityFormTemplate.entityName}`
                        : `/design/server_template/${cloudContext}`
                    }>
                      <Glyphicon type="th-list" />
                      {
                        Drupal.t('List @cloudServiceProvider @entityName', {
                          '@cloudServiceProvider': cspToLabel(entityFormTemplate.cloudServiceProvider),
                          '@entityName': entityNameToLabel(entityFormTemplate.entityName),
                        })
                      }
                    </Link>
                  </Form.Group>
                  : <></>
              }
              {
                entityFormTemplate.entityRecords.map((entityRecord) => {
                  switch (entityRecord.type) {
                    case 'panel':
                      return <EntityFormPanel
                        cloudServiceProvider={entityFormTemplate.cloudServiceProvider}
                        cloudContext={cloudContext}
                        entityName={entityName}
                        actionType={entityFormTemplate.actionType}
                        entityRecord={entityRecord}
                        formData={formData}
                        setFormData={setFormData} />;
                    case 'label':
                      return <EntityFormLabel label={entityRecord.text} entityName={entityName} entityData={entityData} />;
                    default:
                      return <></>;
                  }
                })
              }
              {
                entityFormTemplate.actionType !== 'delete'
                  ? <Button variant={isRefreshing ? 'secondary' : 'primary'}
                    disabled={isRefreshing} onClick={save}>
                    <LabelText text={saveButtonLabel} />
                  </Button>
                  : <Button variant={isRefreshing ? 'secondary' : 'danger'}
                    disabled={isRefreshing} onClick={save}>
                    <LabelText text={saveButtonLabel} />
                  </Button>
              }
              {
                entityFormTemplate.actionType === 'edit'
                  ? <Link className="btn btn-danger" to={
                    entityFormTemplate.entityName !== 'cloud_launch_template'
                      ? entityFormTemplate.cloudServiceProvider === 'openstack' && entityFormTemplate.entityName === 'instance'
                        ? `/${entityFormTemplate.cloudServiceProvider}/${cloudContext}/${entityFormTemplate.entityName}/${entityId}/terminate`
                        : `/${entityFormTemplate.cloudServiceProvider}/${cloudContext}/${entityFormTemplate.entityName}/${entityId}/delete`
                      : `/design/server_template/${cloudContext}/${entityId}/delete`
                  }>
                    <Glyphicon type="trash" />{Drupal.t('Delete')}
                  </Link>
                  : <Link className="btn btn-danger" to={
                    entityFormTemplate.entityName !== 'cloud_launch_template'
                      ? `/${entityFormTemplate.cloudServiceProvider}/${cloudContext}/${entityFormTemplate.entityName}/${entityId}`
                      : `/design/server_template/${cloudContext}/${entityId}`
                  }>
                    <Glyphicon type="remove" />{Drupal.t('Cancel')}
                  </Link>
              }
            </Form>
          </Col>
        </Row>
      </Container>
    </>;
}

const DefaultPage = ({ cloudServiceProvider, entityName, entityId, action }: {
  cloudServiceProvider: string,
  entityName: string,
  entityId: string,
  action: string,
}) => {

  const params: {
    cloudContext: string,
  } = useParams();

  return <>
    <PageTitle />
    <Breadcrumbs />
    <SideBar />
    <Container fluid className="px-0">
      <Row>
        <Col>
          <span>EntityXxudPage</span><br />
          <span>cloudServiceProvider={cloudServiceProvider}</span><br />
          <span>cloudContext={params.cloudContext}</span><br />
          <span>entityName={entityName}</span><br />
          <span>entityId={entityId}</span><br />
          <span>action={action}</span>
        </Col>
      </Row>
    </Container>
  </>;

}

const CushionPage = ({ cloudServiceProvider, entityName, entityId, action, anotherEntityName }: {
  cloudServiceProvider: string,
  entityName: string,
  entityId: string,
  action: string,
  anotherEntityName?: string
}) => {

  const params: {
    cloudContext: string,
  } = useParams();

  // Entity Update/Delete page
  const entityFormTemplate = ENTITY_FORM_LIST.filter((template) => {
    return template.cloudServiceProvider === cloudServiceProvider
      && (template.entityName === entityName || template.entityName === '')
      && template.actionType === action
  });

  if (entityFormTemplate.length >= 1) {
    return <EntityXxudPageImpl
      cloudContext={params.cloudContext}
      entityFormTemplate={entityFormTemplate[0]}
      entityName={entityName}
      entityId={entityId}
      anotherEntityName={anotherEntityName} />;
  }

  // Entity SubView Page
  const tabTemplateList = ENTITY_INFO_TAB_LIST.filter((template) => {
    return template.cloudServiceProvider === cloudServiceProvider
      && template.entityName === entityName;
  });

  if (tabTemplateList.length >= 1) {
    const subTemplatelist = tabTemplateList[0].tabs.filter((template) => {
      return template.name === action;
    });
    if (subTemplatelist.length >= 1) {
      return <EntitySubViewPage
        cloudContext={params.cloudContext}
        tabTemplate={tabTemplateList[0]}
        entityId={entityId}
        subType={action} />;
    }
  }

  return <DefaultPage cloudServiceProvider={cloudServiceProvider} entityName={entityName} entityId={entityId} action={action} />;

}

const EntityXxudPage = ({ entityName, entityId, action, anotherEntityName }: {
  entityId?: string,
  entityName?: string,
  action?: string,
  anotherEntityName?: string,
}) => {

  const params: {
    cloudServiceProvider?: string,
    cloudContext: string,
    entityName?: string,
    entityId?: string,
    action?: string,
    subEntityId?: string,
  } = useParams();

  const { cloudContextList } = useContext(CloudContextListContext);

  // Guess the cloud service provider.
  const cloudServiceProvider = params.cloudServiceProvider ??
    cloudContextList.find(context => context.name === params.cloudContext)?.cloudServiceProvider;

  // Guess the entity type name.
  const newEntityName = entityName ?? params.entityName;

  // Guess the entity ID.
  const newEntityId = entityId === 'subEntityId'
    ? params.subEntityId : params.entityId;

  // Guess the action.
  const newAction = action ?? params.action;

  return (!cloudServiceProvider || !newEntityName)
    ? <DefaultPage cloudServiceProvider={cloudServiceProvider ?? 'undefined'}
      entityName={newEntityName ?? 'undefined'}
      entityId={newEntityId ?? 'undefined'}
      action={newAction ?? 'undefined'} />
    : <CushionPage cloudServiceProvider={cloudServiceProvider}
      entityName={newEntityName}
      entityId={newEntityId ?? ''}
      action={newAction ?? ''}
      anotherEntityName={anotherEntityName} />;

}

export default EntityXxudPage;
