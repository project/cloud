import { BypassAnimationContext } from 'hooks/bypass_animation';
import Breadcrumbs from 'organisms/Breadcrumbs';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext } from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import { Grid } from 'react-loader-spinner';

/**
 * LoadingDataPage.
 */
const LoadingDataPage = () => {

  const { bypassAnimation } = useContext(BypassAnimationContext);

  return <>
    <PageTitle />
    <Breadcrumbs />
    <SideBar />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <Form>
            <div style={{ display: 'flex' }}>
              {
                bypassAnimation
                  ? <></>
                  : <Grid
                    color="#00BFFF"
                    height="2rem"
                    width="2rem"
                  />
              }
              <span style={{
                fontSize: '1.5rem',
                marginLeft: '1.5rem'
              }}>{Drupal.t('Loading...')}</span>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  </>;

}

export default LoadingDataPage;
