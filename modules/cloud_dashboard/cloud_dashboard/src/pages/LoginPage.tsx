import PageTitle from 'organisms/PageTitle';
import Breadcrumbs from 'organisms/Breadcrumbs';
import LoginForm from 'organisms/LoginForm';
import { Col, Container, Row } from 'react-bootstrap';

/**
 * Page for login.
 */
const LoginPage = () => {

  return <>
    <PageTitle />
    <Breadcrumbs />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <LoginForm />
        </Col>
      </Row>
    </Container>
  </>;

}

export default LoginPage;
