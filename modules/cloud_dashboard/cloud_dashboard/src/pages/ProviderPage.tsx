import { CloudContextListContext } from 'hooks/cloud_context_list';
import Breadcrumbs from 'organisms/Breadcrumbs';
import CloudServiceProviderMap from 'organisms/CloudServiceProviderMap';
import CloudServiceProviderTable from 'organisms/CloudServiceProviderTable';
import LimitSummaryPanels from 'organisms/LimitSummaryPanels';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext } from 'react';
import { Card, Col, Container, Row } from 'react-bootstrap';

/**
 * Page of cloud service provider info.
 */
const ProviderPage = () => {

  const { cloudContextList } = useContext(CloudContextListContext);

  const hasOpenStack = cloudContextList.filter((cloudContext) => {
    return cloudContext.cloudServiceProvider === 'openstack' && cloudContext.name !== 'ALL';
  }).length > 0;

  return <>
    <PageTitle />
    <Breadcrumbs />
    <SideBar />
    <Container fluid className="px-0">
      <Row className="mx-0 mb-3">
        <Col>
          {
            hasOpenStack
              ? <LimitSummaryPanels />
              : <></>
          }
        </Col>
      </Row>
      <Row className="mx-0 mb-3">
        <Col>
          <details className="card" open>
            <summary role="button" aria-expanded="true" area-pressed="true"
              className="card-header">{Drupal.t('Location map')}</summary>
            <Card.Body>
              <CloudServiceProviderMap />
            </Card.Body>
          </details>
        </Col>
      </Row>
      <Row className="mx-0 mb-3">
        <Col>
          <CloudServiceProviderTable />
        </Col>
      </Row>
    </Container>
  </>;

}

export default ProviderPage;
