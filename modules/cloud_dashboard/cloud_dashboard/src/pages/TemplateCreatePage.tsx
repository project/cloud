import LabelText from 'atoms/LabelText';
import ENTITY_FORM_LIST from 'constant/templates/form/entity_form_template';
import useCreatingFormData from 'hooks/creating_form_data';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { StatusMessageContext } from 'hooks/status_message';
import EntityFormTemplate from 'model/EntityFormTemplate';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import EntityRecordForm from 'organisms/EntityRecordForm';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext, useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import { convertFormDataToSendData, getFormDataNameLabel } from 'service/convert';
import { fetchWrapper, printErrorResult } from 'service/fetch';
import { capitalize } from 'service/string';

const TemplateCreatePageImpl = ({cloudContext, entityFormTemplate}: {
  cloudContext: string,
  entityFormTemplate: EntityFormTemplate
}) => {
  const { formData, setFormData } = useCreatingFormData(cloudContext, entityFormTemplate);
  const { addMessage, addErrorMessage } = useContext(StatusMessageContext);
  const [isRefreshing, setRefreshing] = useState(false);
  const history = useHistory();
  const { resetFetchCache } = useDrupalJsonApi();

  useEffect(() => {
    console.group('FormData (TemplateCreatePage)');
    console.log(formData);
    console.groupEnd();
  }, [formData]);

  /**
   * Save entity's data.
   */
  const save = async () => {
    // Send request.
    setRefreshing(true);

    const cloudServiceProvider = entityFormTemplate.cloudServiceProvider;
    const url = `/cloud_dashboard/${cloudServiceProvider}/${cloudContext}/cloud_launch_template/create`;

    const requestBody = convertFormDataToSendData(formData, entityFormTemplate);
    const result = await fetchWrapper(url, { method: 'POST', body: requestBody });
    setRefreshing(false);

    // Preparation before sequence processing.
    const entityNameLabel = getFormDataNameLabel(formData);
    const entityTypeC = capitalize('cloud_launch_template'.replace('_', ' '));

    // Sequence when the request fails.
    // Note: console.group is written separately from the normal case for readability.
    if (!result.ok) {
      // Console log.
      console.group('Create Request');
      await printErrorResult(result, 'error');
      console.groupEnd();

      // Status message.
      const messageOption = {
        args: {
          '@type': entityTypeC,
          '@label': entityNameLabel,
        },
      };
      addErrorMessage('The @type @label could not be created.', messageOption);
      return;
    }

    // Console log.
    console.group('Create Request');
    console.log('Response:');
    const responseJson = await result.response.json();
    console.log(responseJson);
    console.groupEnd();

    // Status message.
    const id = responseJson['id'];
    const messageOption = {
      args: {
        '@type': entityTypeC,
        '@label': entityNameLabel,
      },
      links: {
        '@label': `/${cloudServiceProvider}/${cloudContext}/cloud_launch_template/${id}`,
      },
      life: 2,
    };
    addMessage('The @type @label has been created.', messageOption);
    resetFetchCache();

    history.push(`/design/server_template/${cloudContext}`);
  }

  return <>
    <StatusMessages />
    <PageTitle text="Add launch template" />
    <Breadcrumbs />
    <SideBar />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <Form>
            {
              entityFormTemplate.entityRecords.map((entityRecord, index) => {
                return <EntityRecordForm key={index}
                  cloudServiceProvider={entityFormTemplate.cloudServiceProvider}
                  cloudContext={cloudContext}
                  entityName="server_template"
                  action={entityFormTemplate.actionType}
                  formData={formData}
                  setFormData={setFormData}
                  entityRecord={entityRecord} />;
              })
            }
            <Button variant={isRefreshing ? 'secondary' : 'primary'}
              disabled={isRefreshing}
              className="ripple-effect" onClick={save}>
              <LabelText text="Save" />
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  </>;
}

const TemplateCreatePage = () => {

  const params: {
    cloudServiceProvider: string,
    cloudContext: string,
    entityName: string,
  } = useParams();

  const entityFormTemplateList = ENTITY_FORM_LIST.filter((template) => {
    return template.cloudServiceProvider === params.cloudServiceProvider
      && template.entityName === 'server_template'
      && template.actionType === 'create'
  });
  if (entityFormTemplateList.length === 0) {
    return <>
      <PageTitle />
      <Breadcrumbs />
      <SideBar />
      <Container fluid className="px-0">
        <Row>
          <Col>
            <span>TemplateCreatePage</span><br />
            <span>cloudServiceProvider={params.cloudServiceProvider}</span><br />
            <span>cloudContext={params.cloudContext}</span><br />
            <span>entityName=server_template</span><br />
            <span>action=create</span>
          </Col>
        </Row>
      </Container>
    </>;
  }
  const entityFormTemplate = entityFormTemplateList[0];

  return <TemplateCreatePageImpl cloudContext={params.cloudContext} entityFormTemplate={entityFormTemplate} />;
}

export default TemplateCreatePage;
