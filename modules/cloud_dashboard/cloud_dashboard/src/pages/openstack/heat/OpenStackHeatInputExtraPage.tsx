import LabelText from 'atoms/LabelText';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { useEntityData } from 'hooks/entity_data';
import { OpenStackHeatFormTemplateContext } from 'hooks/openstack_heat_form_template';
import { StatusMessageContext } from 'hooks/status_message';
import EntityFormRecord from 'model/EntityFormRecord';
import StatusMessageResponse from 'model/StatusMessageResponse';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import EntityFormPanel from 'organisms/EntityFormPanel';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import LoadingDataPage from 'pages/LoadingDataPage';
import { useContext, useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import { getFormDataNameLabel } from 'service/convert';
import { fetchWrapper } from 'service/fetch';
import { capitalize } from 'service/string';

/**
 * Information on the second step form (Stack panel).
 */
const stackPanelA: EntityFormRecord = {
  type: 'panel',
  panelName: 'Stack',
  keyValueRecords: [
    { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', id: 'field--name-name', class: 'field--name-name' },
    { type: 'number', labelName: 'Creation timeout (minutes)', name: 'timeout_mins', defaultValue: 60 },
    { type: 'boolean', labelName: 'Rollback on failure', name: 'rollback', defaultValue: false },
  ]
}
const stackPanelB: EntityFormRecord = {
  type: 'panel',
  panelName: 'Stack',
  keyValueRecords: [
    { type: 'default', labelName: 'Name', name: 'name', defaultValue: '', readOnly: true, id: 'field--name-name', class: 'field--name-name' },
    { type: 'number', labelName: 'Creation timeout (minutes)', name: 'timeout_mins', defaultValue: 60 },
    { type: 'boolean', labelName: 'Rollback on failure', name: 'rollback', defaultValue: false },
  ]
}

const getStackPanel = (readOnlyName: boolean) => {
  return readOnlyName ? stackPanelB : stackPanelA;
}

const OpenStackHeatInputExtraPage = ({ action, titleText }: {
  action: string,
  titleText: string,
}) => {

  const params: {
    cloudContext: string,
    entityId?: string,
  } = useParams();
  const { entityData } = useEntityData('openstack', 'stack', params.entityId);
  const [formData, setFormData] = useState<Record<string, any>>({
    name: '',
    timeout_mins: 60,
    rollback: false,
  });
  const history = useHistory();
  const { parameterPanel, parameterDefaultValue, templateUrl } = useContext(OpenStackHeatFormTemplateContext);
  const [isRefreshing, setRefreshing] = useState(false);
  const { resetFetchCache } = useDrupalJsonApi();
  const { addMessage, addErrorMessage, addMessagesByResponse } = useContext(StatusMessageContext);

  useEffect(() => {
    setFormData({
      ...formData,
      ...parameterDefaultValue,
      'name': entityData ? entityData.attributes['name'] : '',
    });
  }, [entityData]);

  useEffect(() => {
    console.group('FormData (OpenStackHeatInputExtraPage)');
    console.log(formData);
    console.groupEnd();
  }, [formData]);

  const calcTitleText = () => {
    return titleText.replace('@name',
      entityData ? entityData.attributes['name'] : 'stack');
  }

  /**
   * Save data.
   */
  const save = async () => {
    setRefreshing(true);
    const url = entityData
      ? `/cloud_dashboard/openstack/${params.cloudContext}/openstack_stack/${params.entityId}/${action}`
      : `/cloud_dashboard/openstack/${params.cloudContext}/openstack_stack/${action}`;

    const formDataForSubmit = new FormData();
    for (const key in formData) {
      formDataForSubmit.append(key, `${formData[key]}`);
    }
    formDataForSubmit.append('template_url', templateUrl);

    const result = await fetchWrapper(url, {
      method: 'POST',
      body: formDataForSubmit,
    });
    setRefreshing(false);

    if (!result.ok) {
      if (result.error instanceof Error) {
        const errorReason = result.error.name === 'AbortError'
          ? 'Timeout'
          : result.error.message;
        console.group('HTTP Request');
        console.error('Reason:', errorReason);
        console.groupEnd();
      } else {
        const errorReason = await result.error.json();
        const messages: StatusMessageResponse = (errorReason)['messages'];
        if (messages) {
          addMessagesByResponse(messages, {
            life: 2,
          });
        } else {
          addErrorMessage('The @type @label could not be created.',
            {
              args: {
                '@type': capitalize('stack'.replace('_', ' ')),
                '@label': getFormDataNameLabel(formData),
              },
            }
          );
        }
      }
      return;
    }

    console.group('Create Request');
    console.log('Response:');
    const responseJson = await result.response.json();
    console.log(responseJson);
    console.groupEnd();

    const messages: StatusMessageResponse = (responseJson)['messages'];
    if (messages) {
      addMessagesByResponse(messages, {
        life: 2,
      });
    } else {
      const id = responseJson['id'];
      addMessage(action !== 'edit'
        ? 'The @type @label has been created.'
        : 'The @type @label has been updated.',
        {
          args: {
            '@type': capitalize('stack'.replace('_', ' ')),
            '@label': getFormDataNameLabel(formData),
          },
          links: {
            '@label': `/openstack/${params.cloudContext}/stack/${id}`,
          },
          life: 2,
        }
      );
    }
    resetFetchCache();

    history.push(`/openstack/${params.cloudContext}/stack`);
  }

  return params.entityId && entityData === undefined
    ? <LoadingDataPage />
    : <>
      <StatusMessages />
      <PageTitle action={action} text={calcTitleText()} />
      <Breadcrumbs action={action} />
      <SideBar />
      <Container fluid className="px-0">
        <Row className="mx-0">
          <Col>
            <Form>
              <EntityFormPanel
                cloudServiceProvider="openstack"
                cloudContext={params.cloudContext}
                entityName="stack"
                actionType={action}
                entityRecord={getStackPanel(!!entityData)}
                formData={formData}
                setFormData={setFormData} />
              {
                action !== 'preview'
                  ? <EntityFormPanel
                    cloudServiceProvider="openstack"
                    cloudContext={params.cloudContext}
                    entityName="stack"
                    actionType={action}
                    entityRecord={parameterPanel}
                    formData={formData}
                    setFormData={setFormData} />
                  : <></>
              }
              <Button variant={isRefreshing ? 'secondary' : 'primary'}
                disabled={isRefreshing}
                className="ripple-effect" onClick={save}>
                <LabelText text="Save" />
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </>;

}

export default OpenStackHeatInputExtraPage;
