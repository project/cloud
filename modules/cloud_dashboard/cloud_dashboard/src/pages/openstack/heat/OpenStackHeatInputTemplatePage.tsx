import LabelText from 'atoms/LabelText';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { useEntityData } from 'hooks/entity_data';
import { OpenStackHeatFormTemplateContext } from 'hooks/openstack_heat_form_template';
import { StatusMessageContext } from 'hooks/status_message';
import EntityFormRecord from 'model/EntityFormRecord';
import FormParameters from 'model/FormParameters';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import EntityFormPanel from 'organisms/EntityFormPanel';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext, useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import { getFormDataNameLabel } from 'service/convert';
import { fetchWrapper, printErrorResult } from 'service/fetch';
import { capitalize } from 'service/string';

/**
 * Information on the first step form.
 */
const stackPanel: EntityFormRecord = {
  type: 'panel',
  panelName: 'Stack',
  keyValueRecords: [
    {
      type: 'select-local', labelName: 'Template source', name: 'template_source', defaultValue: '', value: [
        { name: 'file', labelName: 'File' },
        { name: 'data', labelName: 'Direct Input' },
        { name: 'url', labelName: 'URL' },
      ], required: true, id: 'form-item-template-source', class: 'form-item-template-source'
    },
    { type: 'file', labelName: 'Template File', name: 'template_file' },
    { type: 'textarea', labelName: 'Template Data', name: 'template_data', defaultValue: '', id: 'form-item-template-data', class: 'form-item-template-data' },
    { type: 'default', labelName: 'Template URL', name: 'template_url', defaultValue: '' },
    {
      type: 'select-local', labelName: 'Environment source', name: 'environment_source', defaultValue: '', value: [
        { name: 'file', labelName: 'File' },
        { name: 'data', labelName: 'Direct Input' },
      ]
    },
    { type: 'file', labelName: 'Environment File', name: 'environment_file' },
    { type: 'textarea', labelName: 'Environment Data', name: 'environment_data', defaultValue: '' },
  ]
}

/**
 * Result of the first step form submission.
 */
type PreFormResult = {
  templateData: FormParameters,
  template: string,
  environment: string,
};

const OpenStackHeatInputTemplatePage = ({action, titleText}: {
  action: string,
  titleText: string,
}) => {

  const params: {
    cloudContext: string,
    entityId?: string,
  } = useParams();
  const { entityData } = useEntityData('openstack', 'stack', params.entityId);
  const [formData, setFormData] = useState<Record<string, any>>({
    template_source: '',
    template_file: null,
    template_data: '',
    template_url: '',
    environment_source: '',
    environment_file: null,
    environment_data: '',
  });
  const [isRefreshing, setRefreshing] = useState(false);
  const history = useHistory();
  const { addErrorMessage } = useContext(StatusMessageContext);
  const { setDataResponse, setTemplateUrl } = useContext(OpenStackHeatFormTemplateContext);
  const { resetFetchCache } = useDrupalJsonApi();

  useEffect(() => {
    console.group('FormData (OpenStackHeatInputTemplatePage)');
    console.log(formData);
    console.groupEnd();
  }, [formData]);

  const calcTitleText = () => {
    return titleText.replace('@name',
      entityData ? entityData.attributes['name'] : 'stack');
  }

  const toNext = async () => {
    setRefreshing(true);
    const url = `/cloud_dashboard/openstack/${params.cloudContext}/openstack_stack/pre_create`;

    const formDataForSubmit = new FormData();
    formDataForSubmit.append('template_source', formData.template_source);
    formDataForSubmit.append('template_file', formData.template_file);
    formDataForSubmit.append('template_data', formData.template_data);
    formDataForSubmit.append('template_url', formData.template_url);
    formDataForSubmit.append('environment_source', formData.environment_source);
    formDataForSubmit.append('environment_file', formData.environment_file);
    formDataForSubmit.append('environment_data', formData.environment_data);

    const result = await fetchWrapper(url, {
      method: 'POST',
      body: formDataForSubmit,
    });
    setRefreshing(false);

    if (!result.ok) {
      addErrorMessage('The @type @label could not be created.',
        {
          args: {
            '@type': capitalize('stack'.replace('_', ' ')),
            '@label': getFormDataNameLabel(formData),
          },
        }
      );
      console.group('Create Request');
      await printErrorResult(result, 'error');
      console.groupEnd();
      return;
    }

    console.group('Create Request');
    console.log('Response:');
    const responseJson = await result.response.json();
    console.log(responseJson);
    console.groupEnd();

    setDataResponse(responseJson.templateData, responseJson.template, responseJson.environment);
    setTemplateUrl(formData.template_url);
    resetFetchCache();

    const urlSuffix = action === 'create' ? 'add_extra' : `${action}_extra`;
    history.push(params.entityId
        ? `/openstack/${params.cloudContext}/stack/${params.entityId}/${urlSuffix}`
        : `/openstack/${params.cloudContext}/stack/${urlSuffix}`);
  }

  return <>
    <StatusMessages />
    <PageTitle action={action} text={calcTitleText()} />
    <Breadcrumbs action={action} />
    <SideBar />
    <Container fluid className="px-0">
      <Row className="mx-0">
        <Col>
          <Form>
            <EntityFormPanel
              cloudServiceProvider="openstack"
              cloudContext={params.cloudContext}
              entityName="stack"
              actionType={action}
              entityRecord={stackPanel}
              formData={formData}
              setFormData={setFormData} />
            <Button variant={isRefreshing ? 'secondary' : 'primary'}
              disabled={isRefreshing}
              className="ripple-effect" onClick={toNext}>
              <LabelText text="Next" />
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  </>;

}

export default OpenStackHeatInputTemplatePage;
