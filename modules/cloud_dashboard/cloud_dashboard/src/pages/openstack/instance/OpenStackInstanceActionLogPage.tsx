import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { useEntityData } from 'hooks/entity_data';
import { StatusMessageContext } from 'hooks/status_message';
import DataRecord from 'model/DataRecord';
import SortInfo from 'model/SortInfo';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import DataTable from 'organisms/DataTable';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext, useState } from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import { convertDateString } from 'service/string';

/**
 * The raw action log of OpenStack instance.
 */
interface RawActionLog {
  action: string,
  instance_uuid: string,
  message: string | null,
  project_id: string | null,
  request_id: string,
  start_time: string,
  user_id: string | null,
}

/**
 * The action log page of OpenStack instance.
 */
const OpenStackInstanceActionLogPage = () => {

  const params: {
    cloudContext: string,
    entityId: string,
  } = useParams();
  const { getJsonData } = useDrupalJsonApi();
  const { entityData } = useEntityData('openstack', 'instance', params.entityId);
  const [dataRecordList, setDataRecordList] = useState<DataRecord[]>([]);
  const [sortInfo, setSortInfo] = useState<SortInfo>({
    key: '', direction: 'ASC'
  });
  const [isLoading, setLoading] = useState(true);
  const { addErrorMessage } = useContext(StatusMessageContext);

  if (isLoading) {
    setLoading(false);
    getJsonData<{ result: string, action_log: RawActionLog[] }>(`/cloud_dashboard/openstack/${params.cloudContext}/instance/${params.entityId}/action_log`, { result: '', action_log: [] }).then((res) => {

      if (res.action_log.length === 0) {
        addErrorMessage('Action log could not be read.');
        return;
      }

      setDataRecordList(res.action_log.map((actionLog) => {
        return {
          id: actionLog.request_id,
          entityTypeId: '',
          value: {
            requestId: actionLog.request_id,
            action: actionLog.action,
            startTime: convertDateString(actionLog.start_time),
            userId: actionLog.user_id || '',
            message: actionLog.message || '',
          },
          cloudContext: '',
        } as DataRecord;
      }));
    });
  }

  const sortedDataRecordList = () => {
    if (sortInfo.key === '') {
      return dataRecordList;
    }

    return dataRecordList.sort((a, b) => {
      const aValue = a.value[sortInfo.key];
      const bValue = b.value[sortInfo.key];
      if (aValue < bValue) {
        return sortInfo.direction === 'ASC' ? -1 : 1;
      }
      if (aValue > bValue) {
        return sortInfo.direction === 'ASC' ? 1 : -1;
      }
      return 0;
    });
  }

  const baseUrl = `/openstack/${params.cloudContext}/instance/${params.entityId}`;

  return <>
    <StatusMessages />
    <PageTitle text="Action log of instance" />
    <Breadcrumbs entityData={entityData} />
    <SideBar />
    <nav className="tabs">
      <div>
        <ul className="nav nav-tabs" id="tab_wrap">
          <li>
            <Link to={baseUrl}
              className="active is-active ripple-effect">
              View
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/edit`}
              className="active is-active ripple-effect">
              Edit
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/console_output`}
              className="active is-active ripple-effect">
              Log
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/console`}
              className="active is-active ripple-effect">
              Console
            </Link>
          </li>
          <li className="active">
            <Link to={`${baseUrl}/action_log`}
              className="active is-active ripple-effect">
              Action log
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/delete`}
              className="active is-active ripple-effect">
              Delete
            </Link>
          </li>
        </ul>
      </div>
    </nav>
    <Container fluid className="px-0">
      <Row>
        <Col>
          <Form>
            <Form.Group>
              <DataTable
                dataColumnList={[
                  { key: 'requestId', label: 'Request ID' },
                  { key: 'action', label: 'Action' },
                  { key: 'startTime', label: 'Start Time' },
                  { key: 'userId', label: 'User ID' },
                  { key: 'message', label: 'Message' },
                ]}
                dataRecordList={sortedDataRecordList()}
                sortInfo={sortInfo}
                setSortInfo={setSortInfo}
                hasOperationLinks={false}
                operationLinksName=""
                noItemMessage="No data available."
                isShowCheckBox={false}
                />
            </Form.Group>
          </Form>
        </Col>
      </Row>
    </Container>
  </>;

}

export default OpenStackInstanceActionLogPage;
