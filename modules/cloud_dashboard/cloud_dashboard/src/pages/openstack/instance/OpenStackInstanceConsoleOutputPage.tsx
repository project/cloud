import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { useEntityData } from 'hooks/entity_data';
import { StatusMessageContext } from 'hooks/status_message';
import EntityFormRecord from 'model/EntityFormRecord';
import LoadingSpinner from 'molecules/LoadingSpinner';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import EntityFormPanel from 'organisms/EntityFormPanel';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext, useEffect, useState } from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';

/**
 * Information on the action panel.
 */
const actionPanel: EntityFormRecord = {
  type: 'panel',
  panelName: 'Actions',
  keyValueRecords: [
    {
      type: 'select-local', labelName: 'Number of lines', name: 'lines', defaultValue: '150', value: [
        { name: '50', labelName: '50' },
        { name: '100', labelName: '100' },
        { name: '150', labelName: '150' },
        { name: 'ALL', labelName: 'ALL Lines' },
      ], required: true, id: 'form-item-lines', class: 'form-item-lines'
    },
  ]
}

/**
 * The console output page of OpenStack instance.
 */
const OpenStackInstanceConsoleOutputPage = () => {

  const params: {
    cloudContext: string,
    entityId: string,
  } = useParams();

  const { getJsonData } = useDrupalJsonApi();
  const { entityData } = useEntityData('openstack', 'instance', params.entityId);
  const [formData, setFormData] = useState<Record<string, any>>({
    lines: '150',
  });
  const [consoleOutput, setConsoleOutput] = useState('');
  const [isLoading, setLoading] = useState(true);
  const { addErrorMessage } = useContext(StatusMessageContext);

  if (isLoading) {
    getJsonData<{ result: string, log: string }>(`/cloud_dashboard/openstack/${params.cloudContext}/instance/${params.entityId}/console_output`, { result: '', log: '' }).then((res) => {
      setConsoleOutput(res.log);
      setLoading(false);
    }).catch(() => {
      addErrorMessage('Console output could not be read.');
    });
  }

  const filteredConsoleOutput = formData['lines'] === 'ALL'
    ? consoleOutput
    : consoleOutput.split('\n').slice(-parseInt(formData['lines'])).join('\n');

  useEffect(() => {
    console.group('FormData (OpenStackInstanceConsoleOutputPage)');
    console.log(formData);
    console.groupEnd();
  }, [formData]);

  const baseUrl = `/openstack/${params.cloudContext}/instance/${params.entityId}`;

  return <>
    <StatusMessages />
    <PageTitle text="Console log of instance" />
    <Breadcrumbs entityData={entityData} />
    <SideBar />
    <nav className="tabs">
      <div>
        <ul className="nav nav-tabs" id="tab_wrap">
          <li>
            <Link to={baseUrl}
              className="active is-active ripple-effect">
              View
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/edit`}
              className="active is-active ripple-effect">
              Edit
            </Link>
          </li>
          <li className="active">
            <Link to={`${baseUrl}/console_output`}
              className="active is-active ripple-effect">
              Log
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/console`}
              className="active is-active ripple-effect">
              Console
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/action_log`}
              className="active is-active ripple-effect">
              Action log
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/delete`}
              className="active is-active ripple-effect">
              Delete
            </Link>
          </li>
        </ul>
      </div>
    </nav>
    <Container fluid className="px-0">
      <Row>
        <Col>
          <Form>
            {
              entityData !== undefined && entityData.attributes['instance_state'] !== 'running'
                ? <Form.Group>
                  <Form.Label>
                    Console output cannot be displayed when an instance is stopped.
                  </Form.Label>
                </Form.Group>
                : <>
                  <EntityFormPanel
                    cloudServiceProvider="openstack"
                    cloudContext={params.cloudContext}
                    entityName="instance"
                    actionType="console_output"
                    entityRecord={actionPanel}
                    formData={formData}
                    setFormData={setFormData} />
                  {
                    filteredConsoleOutput === ''
                    ? <LoadingSpinner />
                    : <pre className="ansi-block" style={{ 'backgroundColor': 'black', 'color': 'white' }}>
                      <span>
                        {filteredConsoleOutput}
                      </span>
                    </pre>
                  }
                </>
            }
          </Form>
        </Col>
      </Row>
    </Container>
  </>;

}

export default OpenStackInstanceConsoleOutputPage;
