import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import { useEntityData } from 'hooks/entity_data';
import { StatusMessageContext } from 'hooks/status_message';
import StatusMessages from 'molecules/StatusMessage';
import Breadcrumbs from 'organisms/Breadcrumbs';
import PageTitle from 'organisms/PageTitle';
import SideBar from 'organisms/SideBar';
import { useContext, useState } from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';

/**
 * The console page of OpenStack instance.
 */
const OpenStackInstanceConsolePage = () => {

  const params: {
    cloudContext: string,
    entityId: string,
  } = useParams();
  const { messages, addErrorMessage } = useContext(StatusMessageContext);
  const { getJsonData } = useDrupalJsonApi();
  const { entityData } = useEntityData('openstack', 'instance', params.entityId);
  const [isLoading, setLoading] = useState(true);
  const [url, setUrl] = useState('');

  const baseUrl = `/openstack/${params.cloudContext}/instance/${params.entityId}`;

  if (entityData !== undefined && entityData.attributes['instance_state'] !== 'running') {
    const messageText = 'Cannot display the console because there is no available console.';
    if (messages.filter((message) => {
      return message.text.includes(messageText);
    }).length === 0) {
      addErrorMessage('Cannot display the console because there is no available console.');
    }
  }

  if (isLoading && entityData !== undefined) {
    setLoading(false);
    getJsonData<{ result: string, console: Record<string, any> }>(`/cloud_dashboard/openstack/${params.cloudContext}/instance/${params.entityId}/console`, { result: '', console: {} }).then((res) => {
      console.group('Console');
      console.log(res);
      console.groupEnd();
      if (res.result === 'OK') {
        setUrl(res.console['url']);
      } else {
        addErrorMessage('Cannot display the console because there is no available console.');
      }
    });
  }

  return <>
    <StatusMessages />
    <PageTitle text="Console of instance" />
    <Breadcrumbs entityData={entityData} />
    <SideBar />
    <nav className="tabs">
      <div>
        <ul className="nav nav-tabs" id="tab_wrap">
          <li>
            <Link to={baseUrl}
              className="active is-active ripple-effect">
              View
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/edit`}
              className="active is-active ripple-effect">
              Edit
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/console_output`}
              className="active is-active ripple-effect">
              Log
            </Link>
          </li>
          <li className="active">
            <Link to={`${baseUrl}/console`}
              className="active is-active ripple-effect">
              Console
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/action_log`}
              className="active is-active ripple-effect">
              Action log
            </Link>
          </li>
          <li>
            <Link to={`${baseUrl}/delete`}
              className="active is-active ripple-effect">
              Delete
            </Link>
          </li>
        </ul>
      </div>
    </nav>
    {
      url !== ''
      ? <Container fluid className="px-0">
          <Row>
            <Col>
              <Form>
                <Form.Group>
                  <Form.Label>
                    If console is not responding to keyboard input: click the grey status bar below. <a href={url}>Click here to show only console</a>
                    <br />To exit the fullscreen mode, click the browser's back button.
                  </Form.Label>
                </Form.Group>
                <Form.Group>
                  <iframe src={url} style={{
                    'width': '100%',
                    'aspectRatio': '4/3'
                  }} sandbox="allow-same-origin allow-forms allow-scripts"></iframe>
                </Form.Group>
              </Form>
            </Col>
          </Row>
        </Container>
      : <></>
    }
  </>;

}

export default OpenStackInstanceConsolePage;
