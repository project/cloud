import { VMWARE_OS_DICT } from 'constant/others/other';
import MENU_OPERATIONS from 'constant/templates/menu/menu_operation';
import CloudContext from 'model/CloudContext';
import DataRecord from 'model/DataRecord';
import EntityColumn from 'model/EntityColumn';
import EntityData from 'model/EntityData';
import EntityFormColumn from 'model/EntityFormColumn';
import EntityFormRecord from 'model/EntityFormRecord';
import EntityFormTemplate from 'model/EntityFormTemplate';
import FormParameters from 'model/FormParameters';
import { addCommasToNumber, convertDateString } from 'service/string';

/**
 * Record<string, any> to FormData.
 * This method is used the template of entity form for formatting data.
 *
 * @param formData Record<string, any>.
 * @param entityFormTemplate Template of entity form.
 *
 * @returns FormData type.
 */
export const convertFormDataToSendData = (formData: Record<string, any>, entityFormTemplate: EntityFormTemplate) => {
  // Create a name property's list in the file type records.
  // Note: To avoid errors, write for/if statements instead of filter/map.
  const fileTypeRecordNameList: string[] = [];
  for (const record1 of entityFormTemplate.entityRecords) {
    if (record1.type === 'panel') {
      for (const record2 of record1.keyValueRecords) {
        if (record2.type === 'file') {
          fileTypeRecordNameList.push(record2.name);
        }
      }
    }
  }

  // Create a name property's list in the key-value type records.
  // Note: To increase readability, the for/if blocks were written separately.
  const keyValueTypeRecordList: string[] = [];
  for (const record1 of entityFormTemplate.entityRecords) {
    if (record1.type === 'panel') {
      for (const record2 of record1.keyValueRecords) {
        if (record2.type === 'key-value') {
          keyValueTypeRecordList.push(record2.name);
        }
      }
    }
  }

  // Create FormData from formData.
  const sendData = new FormData();
  for (const pair of Object.entries(formData)) {
    // Guard clause.
    if (typeof pair[1] !== 'object') {
      sendData.append(pair[0], `${pair[1]}`);
      continue;
    }

    // If the value type is file, append it to FormData without JSON.stringify().
    if (fileTypeRecordNameList.includes(pair[0])){
      sendData.append(pair[0], pair[1]);
      continue;
    }

    // If the value type is key-value, append it to FormData with _weight property.
    if (keyValueTypeRecordList.includes(pair[0])) {
      const keyValuePairs: Record<string, any>[] = pair[1];
      const keyValuePairForSend: Record<string, any>[] = [];
      let weight = 0;
      for (const keyValuePair of keyValuePairs) {
        keyValuePairForSend.push({
          ...keyValuePair,
          _weight: weight,
        });
        weight += 1;
      }
      sendData.append(pair[0], JSON.stringify(keyValuePairForSend));
      continue;
    }

    // If the value type is object, append it to FormData with JSON.stringify().
    sendData.append(pair[0], JSON.stringify(pair[1]));
  }

  return sendData;
}

/**
 * Round number method.
 * @param value number
 * @param digit digit
 * @returns fixed value string
 */
const roundNumber = (value: number, digit: number) => {
  // This code means "x = 10 ^ digit".
  let x = 1.0;
  for (let i = 0; i < digit; i += 1) {
    x *= 10;
  }

  // Use guard clause.
  if (Math.floor(value) !== value) {
    return `${Math.round(value * x) / x}`;
  }

  let textBuffer = `${value}`;
  if (digit !== 0) {
    textBuffer += '.';
    for (let i = 0; i < digit; i += 1) {
      textBuffer += '0';
    }
  }
  return textBuffer;
};

const convertKeyValueCrLfData = (data: string) => {
  if (data === '{  }') {
    return '';
  }

  const records = data.split('\n');
  const CONVERT_KEY_VALUE_CRLF_DICT: Record<string, string> = {
    'on_demand_hourly: ': 'On-demand hourly ($): ',
    'on_demand_daily: ': 'On-demand daily ($): ',
    'on_demand_monthly: ': 'On-demand monthly ($): ',
    'on_demand_yearly: ': 'On-demand yearly ($): ',
    'ri_one_year: ': 'RI 1 year ($): ',
    'ri_three_year: ': 'RI 3 year ($): ',
    'cpu: ': 'CPU (Usage): ',
    'memory: !!float ': 'Memory (Usage): ',
    'memory: ': 'Memory (Usage): ',
    'pod_allocation: ': 'Pods (Allocation): ',
    'cpu_capacity: !!float ': 'CPU (Capacity): ',
    'memory_capacity: !!float ': 'Memory (Capacity): ',
    'pod_capacity: ': 'Pods (Capacity): ',
  };
  const fixedRecords: string[] = [];
  for (const record of records) {
    if (record === '') {
      continue;
    }
    let fixedRecord = record;
    for (const key in CONVERT_KEY_VALUE_CRLF_DICT) {
      if (record.includes(key)) {
        const value = record.replaceAll(key, '');
        const valueFloat = parseFloat(value);
        if (key.includes('!!float') || key.includes('memory: ')) {
          if (valueFloat >= 1024 * 1024 * 1024) {
            fixedRecord = `${CONVERT_KEY_VALUE_CRLF_DICT[key]}${roundNumber(valueFloat / (1024 * 1024 * 1024), 2)}Gi`;
          } else if (valueFloat >= 1024 * 1024) {
            fixedRecord = `${CONVERT_KEY_VALUE_CRLF_DICT[key]}${roundNumber(valueFloat / (1024 * 1024), 2)}Mi`;
          } else if (valueFloat >= 1024) {
            fixedRecord = `${CONVERT_KEY_VALUE_CRLF_DICT[key]}${roundNumber(valueFloat / (1024), 2)}Ki`;
          } else {
            fixedRecord = `${CONVERT_KEY_VALUE_CRLF_DICT[key]}${roundNumber(valueFloat, 2)}`;
          }
        } else {
          fixedRecord = CONVERT_KEY_VALUE_CRLF_DICT[key].includes('Pods ')
            ? CONVERT_KEY_VALUE_CRLF_DICT[key] + value
            : CONVERT_KEY_VALUE_CRLF_DICT[key] + roundNumber(valueFloat, 2);
        }
        break;
      }
    }
    fixedRecords.push(fixedRecord);
  }
  return fixedRecords.sort((a, b) => {
    return a < b ? -1 : a > b ? 1 : 0;
  }).join('\n');
}

/**
 * Converter of entity's data for UI.
 * @param data Entity's data
 * @param ec Information of Entity Column
 * @param dataCache Data cache for 'join' type
 */
export const convertDataToTextForUI = (data: any, ec: EntityColumn, dataCache: { [key: string]: EntityData[] }): string => {
  // Null check.
  if (data === null) {
    return '';
  }

  switch (ec.type) {
    case 'datetime':
      return convertDateString(data);
    case 'cpu':
      return roundNumber(data, 2);
    case 'memory': {
      if (data >= 1024 * 1024 * 1024) {
        return `${roundNumber(data / (1024 * 1024 * 1024), 2)}Gi`;
      }
      if (data >= 1024 * 1024) {
        return `${roundNumber(data / (1024 * 1024), 2)}Mi`;
      }
      if (data >= 1024) {
        return `${roundNumber(data / (1024), 2)}Ki`;
      }
      return roundNumber(data, 2);
    }
    case 'storage_mb': {
      return data !== undefined ? `${addCommasToNumber(data)}MB` : '0MB';
    }
    case 'storage_gb': {
      return data !== undefined ? `${addCommasToNumber(data)}GB` : '0GB';
    }
    case 'key-value': {
      let records: string[] = [];
      for (const record of data) {
        records.push(`${record['item_key']}:${record['item_value']}`);
      }
      return records.join(', ');
    }
    case 'cost':
      return `$${data}`;
    case 'boolean':
      return data ? ec.value[0] : ec.value[1];
    case 'array':
      if (!Array.isArray(data)) {
        return data;
      }
      if (data.length === 0) {
        return '';
      }
      return data.map((r: any) => `${r}`).join(', ');
    case 'join': {
      const arrayData = Array.isArray(data)
        ? data : [data];
      const output: string[] = [];
      for (const datum of arrayData) {
        let flg = false;
        for (const record of dataCache[ec.info.entityTypeId]) {
          const recordData: EntityData = record;
          if (`${recordData.attributes[ec.info.keyColumn]}` === `${datum}`) {
            output.push(
              ec.hiddenId
              ? `${recordData.attributes[ec.info.valueColumn]}`
              : `${recordData.attributes[ec.info.valueColumn]} (${datum})`
            );
            flg = true;
            break;
          }
        }
        if (!flg) {
          output.push(`${datum}`);
        }
      }
      return output.join(', ');
    }
    case 'multi-join': {
      const arrayData = Array.isArray(data)
        ? data
        : data.includes(',')
          ? data.split(',')
          : [data];
      const output: string[] = [];
      for (const datum of arrayData) {
        let flg = false;
        for (const record of dataCache[ec.info.entityTypeId]) {
          const recordData: EntityData = record;
          if (`${recordData.attributes[ec.info.keyColumn]}` === `${datum}`) {
            output.push(
              ec.hiddenId
              ? `${recordData.attributes[ec.info.valueColumn]}`
              : `${recordData.attributes[ec.info.valueColumn]} (${datum})`
            );
            flg = true;
            break;
          }
        }
        if (!flg) {
          output.push(`${datum}`);
        }
      }
      return output.join(', ');
    }
    case 'key-value-crlf':
      return convertKeyValueCrLfData(data);
    case 'conditions':
      return data !== ec.value[0] ? data : ec.value[1];
    case 'cidr':
      return data.length > 0 ? data[0]['cidr'] : '';
    case 'number':
      return data.toString();
    case 'float_number': {
      return data !== undefined
        ? Number.isInteger(data) ? `${data}.0` : data.toString()
        : '0.0';
    }
    case 'vmware_os':
      // source: VmwareVmGuestOsDataProvider.php
      return data in VMWARE_OS_DICT ? VMWARE_OS_DICT[data] : data;
    case 'link':
      return JSON.stringify({
        'url': ec.url,
        'label': ec.label,
        'type': ec.type,
      });
    case 'fraction': {
      const numeratorValue = data[0];
      const denominatorValue = data[1];
      if (typeof numeratorValue !== 'number' || typeof denominatorValue !== 'number') {
        return '-1 (N/A)';
      }
      if (denominatorValue === 0) {
        return '-1 (N/A)';
      }
      if (denominatorValue < 0) {
        return `${numeratorValue} (No limit)`;
      }
      const percent = 100.0 * numeratorValue / denominatorValue;
      return `${numeratorValue} of ${denominatorValue} (${Math.round(percent * 10) / 10}%)`;
    }
    case 'select-local':
      const filteredItems = ec.value.filter((record) => {
        return record.name === `${data}`;
      });
      return filteredItems.length >= 1
        ? filteredItems[0].labelName
        : `${data}`;
    case 'comma': {
      const func = (d: string) => {
        const temp = `${d}`.split(',');
        return temp.length > ec.index ? temp[ec.index] : `${d}`;
      }
      if (Array.isArray(data)) {
        return data.map((dataRecord) => func(dataRecord)).join(', ');
      }
      return func(data);
    }
    case 'replace':
      return `${data}` in ec.list ? ec.list[`${data}`] : `${data}`;
    default:
      return `${data}`;
  }
};

/**
 * Convert EntityData's list to DataRecord's list.
 *
 * @param entityTypeId ID of entity type.
 * @param entityDataList List of EntityData.
 * @param entityColumnList List of EntityColumn.
 * @param cloudContextList List of CloudContext.
 * @param dataCache Data cache of EntityData.
 * @returns List of DataRecord.
 */
export const convertEntityDataToDataRecord = (
  entityTypeId: string,
  entityDataList: EntityData[],
  entityColumnList: EntityColumn[],
  cloudContextList: CloudContext[],
  dataCache: { [key: string]: EntityData[] }) => {
  const newDataRecordList: DataRecord[] = [];

  // Convert EntityData to DataRecord.
  for (const rawRecord of entityDataList) {
    const dataRecord: DataRecord = {
      id: `${rawRecord.attributes['drupal_internal__id']}`,
      entityTypeId,
      value: {},
      cloudContext: rawRecord.attributes['cloud_context'],
    };

    // Read data column by launchTemplateColumn and convert it.
    for (const launchTemplateColumn of entityColumnList) {
      if (!(launchTemplateColumn.name in rawRecord.attributes)) {
        /**
         * Handling of cases where data cannot be retrieved
         * from rawRecord.attributes.
         */
        if (launchTemplateColumn.type !== 'relationship') {
          continue;
        }
        if (typeof rawRecord.relationships === 'string') {
          continue;
        }
        if (!(launchTemplateColumn.name in rawRecord.relationships)) {
          continue;
        }

        // Read data column by launchTemplateColumn.
        const rawValue = rawRecord.relationships[launchTemplateColumn.name];
        const relationshipsData = Array.isArray(rawValue.data)
          ? rawValue.data[0] : rawValue.data;
        if (relationshipsData === undefined || !('meta' in relationshipsData)) {
          dataRecord.value[launchTemplateColumn.name] = '';
          continue;
        }

        const entityId = relationshipsData.meta[launchTemplateColumn.info.keyColumn1];
        const entityList = dataCache[launchTemplateColumn.info.entityTypeId];
        const selectedEntity = entityList.filter((e) => {
          return e.attributes[launchTemplateColumn.info.keyColumn2] === entityId;
        });
        dataRecord.value[launchTemplateColumn.name] = selectedEntity.length >= 1
          ? selectedEntity[0].attributes[launchTemplateColumn.info.valueColumn]
          : '';
        continue;
      }

      // Read data column by launchTemplateColumn.
      const rawValue = launchTemplateColumn.type !== 'fraction'
        ? rawRecord.attributes[launchTemplateColumn.name]
        : [
          rawRecord.attributes[launchTemplateColumn.name],
          rawRecord.attributes[launchTemplateColumn.denominator_name],
        ];

      // Convert as appropriate.
      if (launchTemplateColumn.name !== 'cloud_context') {
        // If it is not 'cloud_context', convert it by convertDataToTextForUI().
        const newValue = convertDataToTextForUI(rawValue, launchTemplateColumn, dataCache);
        dataRecord.value[launchTemplateColumn.name] = newValue;
        continue;
      }

      // Search for possible candidates.
      const candidate = cloudContextList.filter((r) => {
        return r.name !== 'ALL' && r.name === rawValue;
      });

      // Branching depending on the number of candidates.
      dataRecord.value[launchTemplateColumn.name] = candidate.length >= 1
        ? candidate[0].labelName
        : rawValue;
    }
    // Write it out.
    newDataRecordList.push(dataRecord);
  }
  return newDataRecordList;
};

/**
 * Find and return the list of items to operate.
 *
 * @param entityTypeId The entity type ID.
 * @param attributes The attributes of the entity.
 */
export const getOperationsImpl = (entityTypeId: string, attributes: Record<string, any>) => {

  const output = [];
  // Calc the list of operations.
  for (const record of MENU_OPERATIONS) {
    // If the type ID is not matched, skip it.
    if (record.typeId !== entityTypeId) {
      continue;
    }

    // If the conditions are not matched, skip it.
    const when = record.when ?? [];
    if (when.find((condition) => {
      return attributes[condition.key] !== condition.value
    })) {
      // Skipped because none of the conditions were met in the clause.
      continue;
    }
    const whenNot = record.not ?? [];
    if (whenNot.find((condition) => {
      return attributes[condition.key] === condition.value
    })) {
      // Skipped because none of the conditions in the clause were met.
      continue;
    }
    output.push({
      action: record.action,
      label: record.label,
    });
  }

  if (entityTypeId === 'openstack_instance') {
    return [
      { action: 'edit', label: 'Edit' },
      ...output,
    ];
  }

  return [
    { action: 'edit', label: 'Edit' },
    ...output,
    { action: 'delete', label: 'Delete' },
  ];

}

/**
 * Find and return the list of items to be added to Operation links.
 *
 * @param dataRecord Record of data.
 * @returns The list of items.
 */
export const getOperations = (dataRecord: DataRecord) => {
  return getOperationsImpl(dataRecord.entityTypeId, dataRecord.value);
}

/**
 * Determines whether to display the form or not.
 *
 * Some pages require fine control over the display of forms. Therefore, this
 * function determines whether it should display the form or not.
 * @param cloudServiceProvider The cloud service provider.
 * @param entityName The entity's type name.
 * @param actionType The CxUD action Type.
 * @param entityFormColumn The entity form column.
 * @param hasAdminRole If TRUE, the user has an admin role.
 * @param formData The data of form values.
 *
 * @returns If TRUE, the block is displayed.
 */
export const showEntityFormBlockFlg = (
  cloudServiceProvider: string,
  entityName: string,
  actionType: string,
  entityFormColumn: EntityFormColumn,
  hasAdminRole: boolean,
  formData: Record<string, any>
) => {
  // Associate AWS Cloud Elastic IP
  if (actionType === 'associate'
    && cloudServiceProvider === 'aws_cloud'
    && entityName === 'elastic_ip') {
    if (entityFormColumn.type === 'select') {
      switch (entityFormColumn.name) {
        case 'instance_id':
          return formData['resource_type'] === 'instance';
        case 'instance_private_ip':
          return formData['resource_type'] === 'instance';
        case 'network_interface_id':
          return formData['resource_type'] === 'network_interface';
        case 'network_interface_private_ip':
          return formData['resource_type'] === 'network_interface';
      }
    }
  }

  // Create K8s deployment
  if (actionType === 'create'
    && cloudServiceProvider === 'k8s'
    && entityName === 'deployment'
    && entityFormColumn.type !== 'table-with-sort') {
    switch (entityFormColumn.labelName) {
      case 'Detail':
        return formData['time_scheduler_option'] === 'cloud_orchestrator_scheduler';
      case 'YAML URL':
        return formData['time_scheduler_option'] === 'cronjob_scheduler';
      case 'Scheduler Use Type':
        return formData['enableTimeScheduler'];
      case 'Start-up Time':
        return formData['enableTimeScheduler'];
      case 'Stop Time':
        return formData['enableTimeScheduler'];
    }
  }

  // Create K8s pod
  if (actionType === 'create'
    && cloudServiceProvider === 'k8s'
    && entityName === 'pod'
    && entityFormColumn.type !== 'table-with-sort') {
    switch (entityFormColumn.labelName) {
      case 'Detail':
        return formData['time_scheduler_option'] === 'cloud_orchestrator_scheduler';
      case 'YAML URL':
        return formData['time_scheduler_option'] === 'cronjob_scheduler';
      case 'Scheduler Use Type':
        return formData['enableTimeScheduler'];
      case 'Start-up Time':
        return formData['enableTimeScheduler'];
      case 'Stop Time':
        return formData['enableTimeScheduler'];
    }
  }

  // Associate OpenStack Floating IP
  if (actionType === 'associate'
    && cloudServiceProvider === 'openstack'
    && entityName === 'floating_ip') {
    if (entityFormColumn.type === 'select') {
      switch (entityFormColumn.name) {
        case 'instance_id':
          return formData['resource_type'] === 'instance';
        case 'instance_private_ip':
          return formData['resource_type'] === 'instance';
        case 'network_id':
          return formData['resource_type'] === 'network';
      }
    }
  }

  // Create AWS Cloud VPC Peering Connection
  if (actionType === 'create'
    && cloudServiceProvider === 'aws_cloud'
    && entityName === 'vpc_peering_connection') {

    if (entityFormColumn.type === 'default'
      || entityFormColumn.type === 'select'
      || entityFormColumn.type === 'hidden') {

      // If the regions and account_ids are different, display the textfield
      // `accepter_vpc_id_text` so users can enter a freeform VPC ID belonging
      // to a different region.
      if (entityFormColumn.name === 'accepter_vpc_id') {
        if (formData['accepter_region'] !== formData['default_region'] ||
          formData['accepter_account_id'] !== formData['default_account_id']
        ) {
          formData['accepter_vpc_id'] = '';
          return false;
        }
      }

      if (entityFormColumn.name === 'accepter_vpc_id_text') {
        if (formData['accepter_region'] === formData['default_region'] &&
          formData['accepter_account_id'] === formData['default_account_id']
        ) {
          formData['accepter_vpc_id_text'] = '';
          return false;
        }
      }
    }
  }

  // Edit AWS Cloud Transit Gateway
  if (actionType === 'edit'
    && cloudServiceProvider === 'aws_cloud'
    && entityName === 'transit_gateway'
  ) {
    if (entityFormColumn.type === 'boolean' || entityFormColumn.type === 'select') {
      switch (entityFormColumn.name) {
        case 'association_default_route_table_id':
          return formData['default_route_table_association'];
        case 'propagation_default_route_table_id':
          return formData['default_route_table_propagation'];
      }
    }
  }

  // Create/Edit Openstack Subnet
  if ((actionType === 'create' || actionType === 'edit')
    && cloudServiceProvider === 'openstack'
    && entityName === 'subnet') {
    if (entityFormColumn.type === 'default') {
      if (entityFormColumn.name === 'gateway_ip') {
        return formData['disable_gateway'] === false;
      }
    }
  }

  // Create/Edit Openstack Router
  if ((actionType === 'create' || actionType === 'edit')
    && cloudServiceProvider === 'openstack'
    && entityName === 'router') {
    if (entityFormColumn.type === 'boolean') {
      if (entityFormColumn.name === 'external_gateway_enable_snat') {
        return formData['external_gateway_network_id'] !== '' && hasAdminRole;
      }
    }
  }

  // Create/Edit Openstack Port
  if ((actionType === 'create' || actionType === 'edit')
    && cloudServiceProvider === 'openstack'
    && entityName === 'port') {
    if (entityFormColumn.type === 'default'
      || entityFormColumn.type === 'select'
      || entityFormColumn.type === 'multi-select'
      || entityFormColumn.type === 'multi-check'
      || entityFormColumn.type === 'item-array') {
      switch (entityFormColumn.name) {
        case 'subnet':
          return formData['ip_address_or_subnet'] === 'subnet';
        case 'fixed_ips':
          return formData['ip_address_or_subnet'] === 'fixed_ip';
        case 'security_groups':
          return formData['port_security_enabled'] === true;
        case 'allowed_address_pairs':
          return formData['port_security_enabled'] === true;
      }
    }
  }

  // Create Openstack Heat
  if (actionType === 'create'
    && cloudServiceProvider === 'openstack'
    && entityName === 'stack') {
    if (entityFormColumn.type === 'file'
      || entityFormColumn.type === 'default'
      || entityFormColumn.type === 'textarea') {
      switch (entityFormColumn.name) {
        case 'template_file':
          return formData['template_source'] === 'file';
        case 'template_data':
          return formData['template_source'] === 'data';
        case 'template_url':
          return formData['template_source'] === 'url';
        case 'environment_file':
          return formData['environment_source'] === 'file';
        case 'environment_data':
          return formData['environment_source'] === 'data';
      }
    }
  }

  // Edit Openstack Heat
  if (actionType === 'edit'
    && cloudServiceProvider === 'openstack'
    && entityName === 'stack') {
    if (entityFormColumn.type === 'file'
      || entityFormColumn.type === 'default'
      || entityFormColumn.type === 'textarea') {
      switch (entityFormColumn.name) {
        case 'template_file':
          return formData['template_source'] === 'file';
        case 'template_data':
          return formData['template_source'] === 'data';
        case 'template_url':
          return formData['template_source'] === 'url';
        case 'environment_file':
          return formData['environment_source'] === 'file';
        case 'environment_data':
          return formData['environment_source'] === 'data';
      }
    }
  }

  // Preview Openstack Heat
  if (actionType === 'preview'
    && cloudServiceProvider === 'openstack'
    && entityName === 'stack') {
    if (entityFormColumn.type === 'file'
      || entityFormColumn.type === 'default'
      || entityFormColumn.type === 'textarea') {
      switch (entityFormColumn.name) {
        case 'template_file':
          return formData['template_source'] === 'file';
        case 'template_data':
          return formData['template_source'] === 'data';
        case 'template_url':
          return formData['template_source'] === 'url';
        case 'environment_file':
          return formData['environment_source'] === 'file';
        case 'environment_data':
          return formData['environment_source'] === 'data';
      }
    }
  }

  // Attach port in OpenStack instance
  if (actionType === 'attach_network'
    && cloudServiceProvider === 'openstack'
    && entityName === 'instance') {
      if (entityFormColumn.type === 'select'
      || entityFormColumn.type === 'default') {
        switch (entityFormColumn.name) {
          case 'network':
            return formData['type'] === 'network';
          case 'fixed_ip':
            return formData['type'] === 'network';
          case 'port':
            return formData['type'] === 'port';
        }
      }
  }

  // Create/Edit Openstack Image
  if ((actionType === 'create' || actionType === 'edit')
    && cloudServiceProvider === 'openstack'
    && entityName === 'image') {
    if (entityFormColumn.type === 'image-shared-projects') {
      switch (entityFormColumn.name) {
        case 'shared_projects':
          // Display only if image_visibility is Shared.
          return formData['image_visibility'] === '2';
      }
    }
  }

  return true;
}

/**
 * Get the entity data's name label.
 * @param entityData The entity data.
 * @returns The entity data's name label.
 */
export const getEntityDataNameLabel = (entityData: EntityData | undefined): string => {
  // If the entity data is not set, return an empty string.
  // If the entity data is set but the attributes are not set, return an empty string.
  if (!entityData || !entityData.attributes) {
    return '';
  }

  // If the entity data is set and the attributes are set, return the name.
  // Note: The entity name's key is 'name' or 'key_pair_name'.
  return entityData.attributes['name'] || entityData.attributes['key_pair_name'] || '';
}

/**
 * Get the form data's name label.
 * @param formData The form data.
 * @returns The form data's name label.
 */
export const getFormDataNameLabel = (formData: Record<string ,any>): string => {
  // If the entity data is set and the attributes are set, return the name.
  // Note: The entity name's key is 'name' or 'key_pair_name'.
  return formData['name'] as string || formData['group_name'] as string || formData['key_pair_name'] as string || '';
}

/**
 * Convert the results of the first step form submission
 * into a template parameter form definition for the second step form.
 *
 * @param template The template data.
 */
export const formParametersToFormRecord = (template: FormParameters) => {
  const output: EntityFormRecord = {
    type: 'panel',
    panelName: 'Template parameters',
    keyValueRecords: []
  };
  const defaultValue: Record<string, any> = {};

  for (const formKey of Object.keys(template.parameters)) {
    const formParameter = template.parameters[formKey];
    let formDefaultValue = formParameter.default ?? '';
    const formId = `edit-template-parameters-${formKey.replaceAll('_', '-').toLowerCase()}`;
    const formClass = `form-item-template-parameters-${formKey.replaceAll('_', '-').toLowerCase()}`;
    switch (formParameter.type) {
      case 'number':
        formDefaultValue = formParameter.default ?? 0;
        output.keyValueRecords.push({
          type: 'number',
          labelName: formKey,
          name: 'parameter_' + formKey,
          defaultValue: formDefaultValue,
          id: formId,
          class: formClass,
        } as EntityFormColumn);
        break;

      case 'boolean':
        formDefaultValue = ['t', 'true', 'on', 'y', 'yes', '1'].includes(`${formParameter.default}`.toLowerCase());
        output.keyValueRecords.push({
          type: 'boolean',
          labelName: formKey,
          name: 'parameter_' + formKey,
          defaultValue: formDefaultValue,
          id: formId,
          class: formClass,
        } as EntityFormColumn);
        break;

      case 'json':
        formDefaultValue = JSON.stringify(formParameter.default ?? '');
        output.keyValueRecords.push({
          type: 'default',
          labelName: formKey,
          name: 'parameter_' + formKey,
          defaultValue: formDefaultValue,
          id: formId,
          class: formClass,
        } as EntityFormColumn);
        break;

      case 'comma_delimited_list':
        formDefaultValue = JSON.stringify(formParameter.default ?? '');
        output.keyValueRecords.push({
          type: 'default',
          labelName: formKey,
          name: 'parameter_' + formKey,
          defaultValue: formDefaultValue,
          id: formId,
          class: formClass,
        } as EntityFormColumn);
        break;

      default:
        output.keyValueRecords.push({
          type: 'default',
          labelName: formKey,
          name: 'parameter_' + formKey,
          defaultValue: formDefaultValue,
          id: formId,
          class: formClass,
        } as EntityFormColumn);
        break;
    }
    defaultValue['parameter_' + formKey] = formDefaultValue;
  }

  return {
    parameterPanel: output as EntityFormRecord,
    parameterDefaultValue: defaultValue
  };
}
