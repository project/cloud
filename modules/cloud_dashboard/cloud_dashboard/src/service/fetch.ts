type Result = {
  ok: true,
  response: Response,
} | {
  ok: false,
  error: DOMException | TypeError | Response,
}

/**
 * The wrapper function for the fetch API.
 * @param input This defines the resource that you wish to fetch.
 * @param init An object containing any custom settings that you want to apply to the request.
 * @returns A promise that resolves to a Result object.
 */
export const fetchWrapper = async (input: RequestInfo | URL, init?: RequestInit | undefined): Promise<Result> => {
  try {
    const response = await fetch(input, init);
    return response.ok
      ? {
        ok: true,
        response,
      }
      : {
        ok: false,
        error: response,
      };
  } catch (error: any) {
    return {
      ok: false,
      error,
    };
  }
}

/**
 * The wrapper function for debug print.
 *
 * @param result A promise that resolves to a Result object.
 * @param type The type of the debug print.
 */
export const printErrorResult = async (result: Result, type: 'error' | 'warn') => {
  if (result.ok) {
    return;
  }

  const errorReason = result.error instanceof Error
    ? result.error.name === 'AbortError'
      ? 'Timeout'
      : result.error.message
    : await result.error.json();

  switch (type) {
    case 'error':
      console.error('Reason:', errorReason);
      break;
    case 'warn':
      console.warn('Reason:', errorReason);
      break;
  }
}
