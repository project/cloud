/**
 * Wrapper of window.localStorage.getItem();
 *
 * @param key Key.
 * @param defaultValue Default value.
 * @returns Item value.
 */
export const getLocalStorageItem = (key: string, defaultValue: string) => {
  const temp = window.localStorage.getItem(key);
  return temp !== null ? temp : defaultValue;
}

/**
 * Wrapper of window.localStorage.setItem();
 *
 * @param key Key.
 * @param value Item value.
 */
export const setLocalStorageItem = (key: string, value: string) => {
  window.localStorage.setItem(key, value);
}
