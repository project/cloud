import { CLOUD_CONTEXT_LABEL_NAMES } from 'constant/others/other';
import { UPPERCASE_WORD_SET } from 'constant/templates/entity/entity_info_template';
import MenuTemplate from 'model/MenuTemplate';

/**
 * Padding Zero String.
 * @param data data
 * @param length data length
 * @returns Padded value
 */
export const paddingZero = (data: any, length: number) => {
  return `000000${data}`.slice(-length);
}

/**
 * Getter of EntityListView's URL for MenuTemplate.
 * @param menu MenuTemplate
 * @param cloudContext cloudContext
 * @returns URL
 */
export const getEntityListViewUrl = (menu: MenuTemplate, cloudContext?: string) => {
  return cloudContext === undefined
    ? `/${menu.cloudServiceProvider as string}/${menu.entityName}`
    : `/${menu.cloudServiceProvider as string}/${cloudContext}/${menu.entityName}`;
};

/**
 * Getter of EntityTypeId for MenuTemplate.
 * @param menu MenuTemplate
 * @returns EntityTypeId
 */
export const getEntityTypeId = (menu: MenuTemplate) => {
  return `${menu.cloudServiceProvider as string}_${menu.entityName}`;
};

/**
 * Capitalize text.
 * @param text The input text string.
 */
export const capitalize = (text: string) => {
  if (text.length === 0) {
    return '';
  }
  return text.slice(0, 1).toUpperCase() + text.slice(1);
}

/**
 * Convert to datetime string for UI.
 * @param dateString datetime string
 * @returns datetime string for UI
 */
export const convertDateString = (dateString: string) => {
  const date = new Date(dateString);
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  let output = `${year}/${paddingZero(month, 2)}/${paddingZero(day, 2)}`;
  output += ` - ${paddingZero(hour, 2)}:${paddingZero(minute, 2)}`;
  return output;
}

/**
 * Getter of replaced URL for EntityDetailPage.
 * @param url url before replace
 * @param cloudContext cloudContext
 * @param entityName entityName
 * @param entityId entityId
 * @param cloudServiceProvider cloudServiceProvider
 * @param index index
 * @returns URL
 */
export const getReplacedUrl = (
  url: string,
  cloudContext: string,
  entityName: string,
  entityId: string,
  cloudServiceProvider: string,
  index: number
) => {
  return  url
    .replaceAll('{cloudContext}', cloudContext)
    .replaceAll('{entityName}', entityName)
    .replaceAll('{entityId}', entityId)
    .replaceAll('{cloudServiceProvider}', cloudServiceProvider)
    .replaceAll('{index}', `${index}`)
}

export const cspToLabel = (cloudServiceProvider: string) => {
  const temp = CLOUD_CONTEXT_LABEL_NAMES.filter((r) => {
    return r.cloudServiceProvider === cloudServiceProvider;
  });
  return temp.length >= 1 ? temp[0].labelName : capitalize(cloudServiceProvider);
};

export const entityNameToLabel = (entityName: string) => {
  return entityName.split('_').map((word) => {
    return UPPERCASE_WORD_SET.has(word) ? word.toUpperCase() : word;
  }).join(' ');
}

/**
 * Add commas to number.
 *
 * @param num number.
 */
export const addCommasToNumber = (num: number) => {
  const strNumber = num.toString();

  const length = strNumber.length;
  const remainder = length % 3;
  let result = (remainder > 0) ? strNumber.slice(0, remainder) : '';

  for (let i = remainder; i < length; i += 3) {
    result += (result.length > 0 ? ',' : '') + strNumber.slice(i, i + 3);
  }

  return result;
}
