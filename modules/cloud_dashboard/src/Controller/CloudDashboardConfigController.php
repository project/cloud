<?php

namespace Drupal\cloud_dashboard\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class CloudConfigController.
 *
 * Returns responses for cloud service provider (CloudConfig) routes.
 */
class CloudDashboardConfigController extends ControllerBase {

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a AwsCloudAdminNotificationSettings object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   */
  public function __construct(
    EntityTypeManager $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    RequestStack $request_stack,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * Get callback URI for OAuth2.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of callback URI.
   */
  public function getCallbackUri(): JsonResponse {
    // If the URL has been entered in the settings form, follow it.
    $config = $this->configFactory->get('cloud_dashboard.settings');
    $callback_uri = $config->get('cloud_dashboard_oauth2_callback_uri');
    if (!empty($callback_uri)) {
      return new JsonResponse(['uri' => $callback_uri]);
    }

    // If a callback URL has been set in
    // the Consumer configuration form, follow it.
    /** @var \Drupal\consumers\Entity\Consumer[] $entities */
    $entities = $this->entityTypeManager
      ->getStorage('consumer')
      ->loadByProperties([
        'label' => 'Cloud Dashboard',
      ]);
    if (!empty($entities)) {
      return new JsonResponse(['uri' => current($entities)->get('redirect')->value]);
    }

    // If none of the above, return an error.
    return new JsonResponse([
      'result' => 'NG',
      'reason' => 'The callback URL is not set.',
    ], 404);
  }

  /**
   * Get client ID for OAuth2.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of callback URI.
   */
  public function getClientId(): JsonResponse {
    // If the URL has been entered in the settings form, follow it.
    $config = $this->configFactory->get('cloud_dashboard.settings');
    $client_id = $config->get('cloud_dashboard_oauth2_client_id');
    if (!empty($client_id)) {
      return new JsonResponse(['id' => $client_id]);
    }

    // If a client ID has been set in
    // the Consumer configuration form, follow it.
    /** @var \Drupal\consumers\Entity\Consumer[] $entities */
    $entities = $this->entityTypeManager
      ->getStorage('consumer')
      ->loadByProperties([
        'label' => 'Cloud Dashboard',
      ]);
    if (!empty($entities)) {
      return new JsonResponse(['id' => current($entities)->get('client_id')->value]);
    }

    // If none of the above, return an error.
    return new JsonResponse([
      'result' => 'NG',
      'reason' => 'The client ID can not load.',
    ], 404);
  }

  /**
   * Get Server URI for JSON:API.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of callback URI.
   */
  public function getJsonApiServerUri(): JsonResponse {
    // If the URL has been entered in the settings form, follow it.
    $config = $this->configFactory->get('cloud_dashboard.settings');
    $server_uri = $config->get('cloud_dashboard_json_api_server_uri');
    if (!empty($server_uri)) {
      return new JsonResponse(['uri' => $server_uri]);
    }

    return new JsonResponse(['uri' => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost()]);
  }

  /**
   * Get URL of default marker icon for Leaflet.js.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of callback URI.
   */
  public function getMakerIconUri(): JsonResponse {
    // If the URL has been entered in the settings form, follow it.
    $config = $this->configFactory->get('cloud_dashboard.settings');
    $server_uri = $config->get('cloud_dashboard_marker_icon_uri');
    if (!empty($server_uri)) {
      return new JsonResponse(['uri' => $server_uri]);
    }

    return new JsonResponse(['uri' => 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-icon.png']);
  }

  /**
   * Get URL of coordinate data for drawing world map (GeoJson format).
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of callback URI.
   */
  public function getMapGeoJsonUri(): JsonResponse {
    // If the URL has been entered in the settings form, follow it.
    $config = $this->configFactory->get('cloud_dashboard.settings');
    $server_uri = $config->get('cloud_dashboard_map_geojson_uri');
    if (!empty($server_uri)) {
      return new JsonResponse(['uri' => $server_uri]);
    }

    return new JsonResponse(['uri' => 'https://enjalot.github.io/wwsd/data/world/ne_50m_admin_0_countries.geojson']);
  }

  /**
   * Returns configuration information about the location map.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of config.
   */
  public function getLocationMapConfig(): JsonResponse {
    $config = $this->configFactory->get('cloud_dashboard.settings');
    $latitude = $config->get('cloud_dashboard_location_map_latitude');
    $longitude = $config->get('cloud_dashboard_location_map_longitude');
    $zoom_level = $config->get('cloud_dashboard_location_map_zoom_level');

    return new JsonResponse([
      'latitude' => !empty($latitude) ? $latitude : '0.0',
      'longitude' => !empty($longitude) ? $longitude : '0.0',
      'zoom_level' => !empty($zoom_level) ? $zoom_level : '5',
    ]);
  }

  /**
   * Returns configuration information about the bypass animation.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of config.
   */
  public function getBypassAnimation(): JsonResponse {
    $config = $this->configFactory->get('cloud_dashboard.settings');
    $bypass_animation = $config->get('cloud_dashboard_bypass_animation');

    return new JsonResponse([
      'bypass_animation' => !empty($bypass_animation) ? 'true' : 'false',
    ]);
  }

}
