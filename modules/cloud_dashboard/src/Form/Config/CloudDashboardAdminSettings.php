<?php

namespace Drupal\cloud_dashboard\Form\Config;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManagerInterface;
use Drupal\Core\Routing\RouteProvider;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\cloud\Service\CloudServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CloudDashboard Admin Settings.
 */
class CloudDashboardAdminSettings extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The default value of location map's latitude.
   */
  public const DEFAULT_LATITUDE = '0.0';

  /**
   * The default value of location map's longitude.
   */
  public const DEFAULT_LONGITUDE = '0.0';

  /**
   * The default value of location map's zoom level.
   */
  public const DEFAULT_ZOOM_LEVEL = '5';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Locale\CountryManager
   */
  protected $countryManager;

  /**
   * The cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloud;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProvider
   */
  protected $routeProvider;

  /**
   * Constructs a AwsCloudAdminSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Locale\CountryManagerInterface $country_manager
   *   Country Manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud
   *   The cloud service.
   * @param \Drupal\Core\Routing\RouteProvider $route_provider
   *   The route provider.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CountryManagerInterface $country_manager,
    EntityTypeManagerInterface $entity_type_manager,
    CloudServiceInterface $cloud,
    RouteProvider $route_provider,
    TypedConfigManagerInterface $typed_config_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->countryManager = $country_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->cloud = $cloud;
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('country_manager'),
      $container->get('entity_type.manager'),
      $container->get('cloud'),
      $container->get('router.route_provider'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cloud_dashboard_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['cloud_dashboard.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('cloud_dashboard.settings');

    $form['location_map'] = [
      '#type' => 'details',
      '#title' => $this->t('Location map'),
      '#open' => TRUE,
    ];

    $form['location_map']['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => $this->countryManager->getStandardList(),
      '#required' => TRUE,
      '#default_value' => $config->get('cloud_dashboard_location_map_country'),
    ];

    $form['location_map']['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#required' => TRUE,
      '#default_value' => $config->get('cloud_dashboard_location_map_city'),
    ];

    $form['location_map']['latitude'] = [
      '#type' => 'number',
      '#title' => $this->t('Latitude'),
      '#max' => '90',
      '#min' => '-90',
      '#step' => 0.000001,
      '#required' => TRUE,
      '#default_value' => !empty($config->get('cloud_dashboard_location_map_latitude'))
        ? $config->get('cloud_dashboard_location_map_latitude') : self::DEFAULT_LATITUDE,
    ];

    $form['location_map']['longitude'] = [
      '#type' => 'number',
      '#title' => $this->t('Longitude'),
      '#max' => '180',
      '#min' => '-180',
      '#step' => 0.000001,
      '#required' => TRUE,
      '#default_value' => !empty($config->get('cloud_dashboard_location_map_longitude'))
        ? $config->get('cloud_dashboard_location_map_longitude') : self::DEFAULT_LONGITUDE,
    ];

    $form['location_map']['zoom_level'] = [
      '#type' => 'select',
      '#title' => $this->t('Zoom level'),
      '#required' => TRUE,
      '#default_value' => !empty($config->get('cloud_dashboard_location_map_zoom_level'))
        ? $config->get('cloud_dashboard_location_map_zoom_level') : self::DEFAULT_ZOOM_LEVEL,
      '#options' => [
        '1' => $this->t('1'),
        '2' => $this->t('2'),
        '3' => $this->t('3'),
        '4' => $this->t('4'),
        '5' => $this->t('5'),
        '6' => $this->t('6'),
        '7' => $this->t('7'),
        '8' => $this->t('8'),
        '9' => $this->t('9'),
      ],
    ];

    $form['custom_urls'] = [
      '#type' => 'details',
      '#title' => $this->t('Custom URLs'),
      '#open' => FALSE,
    ];

    $form['custom_urls']['oauth2_callback_uri'] = [
      '#type' => 'url',
      '#title' => $this->t('Callback URI for OAuth2'),
      '#default_value' => $config->get('cloud_dashboard_oauth2_callback_uri'),
      '#description' => $this->t('If you leave this field blank, the callback URI will be the same as the consumer plugin at the server of Cloud Dashboard.'),
      '#attributes' => ['placeholder' => 'e.g. https://example.com/clouds/dashboard/callback'],
    ];

    $form['custom_urls']['oauth2_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID for OAuth2'),
      '#default_value' => $config->get('cloud_dashboard_oauth2_client_id'),
      '#description' => $this->t('If you leave this field blank, the client ID will be the same as the consumer plugin at the server of Cloud Dashboard.'),
      '#attributes' => ['placeholder' => 'e.g. e0a08590-4a13-419d-a64d-f859806c21a9'],
    ];

    $form['custom_urls']['json_api_server_uri'] = [
      '#type' => 'url',
      '#title' => $this->t('Server URL to get data by JSON:API'),
      '#default_value' => $config->get('cloud_dashboard_json_api_server_uri'),
      '#description' => $this->t('If you leave this field blank, it will access the same server of Cloud Dashboard to retrieve the data.'),
      '#attributes' => ['placeholder' => 'e.g. https://example.com'],
    ];

    $form['custom_urls']['marker_icon_uri'] = [
      '#type' => 'url',
      '#title' => $this->t('URL of default marker icon for Leaflet.js'),
      '#default_value' => $config->get('cloud_dashboard_marker_icon_uri'),
      '#description' => $this->t('If you leave this field blank, it will access data from CDN'),
      '#attributes' => ['placeholder' => 'e.g. https://example.com/marker-icon.png'],
    ];

    $form['custom_urls']['map_geojson_uri'] = [
      '#type' => 'url',
      '#title' => $this->t('URL of coordinate data for drawing world map (GeoJson format)'),
      '#default_value' => $config->get('cloud_dashboard_map_geojson_uri'),
      '#description' => $this->t('If you leave this field blank, it will access data from GitHub'),
      '#attributes' => ['placeholder' => 'e.g. https://example.com/world_map.geojson'],
    ];

    $form['custom_urls']['location_map_config_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL of the configuration information about the location map'),
      '#default_value' => !empty($config->get('cloud_dashboard_location_map_config_uri'))
        ? Url::fromUri($config->get('cloud_dashboard_location_map_config_uri'))->toString() : '',
      '#description' => $this->t('If you leave this field blank, it will be /clouds/cloud_dashboard/config/location_map_config'),
      '#attributes' => ['placeholder' => 'e.g. /clouds/cloud_dashboard/config/location_map_config'],
      '#element_validate' => [
        [
          'Drupal\link\Plugin\Field\FieldWidget\LinkWidget',
          'validateUriElement',
        ],
      ],
    ];

    $form['debug'] = [
      '#type' => 'details',
      '#title' => $this->t('Debug'),
      '#open' => FALSE,
    ];

    $form['debug']['bypass_animation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bypass the <em>animated</em> loading spinner icon in SPA'),
      '#description' => $this->t('By turning on this option, the loading spinner icon is disabled for the debug purpose.'),
      '#default_value' => $config->get('cloud_dashboard_bypass_animation'),
    ];

    if ($this->cloud->isGeocoderAvailable()) {
      $path = $this->routeProvider->getRouteByName('entity.cloud_config.geocoder')->getPath();
      $path = str_replace('{country}', 'country', $path);
      $path = str_replace('{city}', 'city', $path);
      $url = Url::fromUri('internal:' . $path);
      $form['#attached']['library'] = 'cloud/cloud_geocoder';
      $form['#attached']['drupalSettings']['cloud']['geocoder_url'] = $url->toString();
      $form['#attached']['drupalSettings']['cloud']['country_id'] = 'edit-country';
      $form['#attached']['drupalSettings']['cloud']['city_id'] = 'edit-city';
      $form['#attached']['drupalSettings']['cloud']['latitude_id'] = 'edit-latitude';
      $form['#attached']['drupalSettings']['cloud']['longitude_id'] = 'edit-longitude';
    }

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $country = $form_state->getValue('country');
    $city = $form_state->getValue('city');

    if (!empty($country) && empty($city)) {
      $form_state->setErrorByName('city', $this->t("Please specify the city name."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    // Save the settings entered in the form.
    $config = $this->configFactory()->getEditable('cloud_dashboard.settings');

    $config->set('cloud_dashboard_oauth2_callback_uri', $form_state->getValue('oauth2_callback_uri'));
    $config->set('cloud_dashboard_oauth2_client_id', $form_state->getValue('oauth2_client_id'));
    $config->set('cloud_dashboard_json_api_server_uri', $form_state->getValue('json_api_server_uri'));
    $config->set('cloud_dashboard_marker_icon_uri', $form_state->getValue('marker_icon_uri'));
    $config->set('cloud_dashboard_map_geojson_uri', $form_state->getValue('map_geojson_uri'));
    $config->set('cloud_dashboard_location_map_config_uri', $form_state->getValue('location_map_config_uri'));
    $config->set('cloud_dashboard_location_map_country', $form_state->getValue('country'));
    $config->set('cloud_dashboard_location_map_city', $form_state->getValue('city'));
    $config->set('cloud_dashboard_location_map_latitude', $form_state->getValue('latitude'));
    $config->set('cloud_dashboard_location_map_longitude', $form_state->getValue('longitude'));
    $config->set('cloud_dashboard_location_map_zoom_level', $form_state->getValue('zoom_level'));
    $config->set('cloud_dashboard_bypass_animation', $form_state->getValue('bypass_animation'));

    $config->save();

    parent::submitForm($form, $form_state);

  }

}
