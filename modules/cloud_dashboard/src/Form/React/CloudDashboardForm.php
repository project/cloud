<?php

namespace Drupal\cloud_dashboard\Form\React;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CloudReactForm.
 *
 * @package Drupal\cloud_dashboard\Form\React
 */
class CloudDashboardForm extends FormBase {

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The cloud service provider plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs new CloudLocalTasks.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityTypeManagerInterface $entity_type_manager,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    ConfigFactoryInterface $config_factory,
  ) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityTypeManager = $entity_type_manager;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloud_dashboard';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['html'] = [
      '#markup' => '<div id="root"></div>',
      '#attached' => [
        'library' => 'cloud_dashboard/react_app',
      ],
    ];

    // Get cloud configs.
    /** @var \Drupal\cloud\Entity\CloudConfigInterface[] $cloud_configs */
    $cloud_configs = $this->entityTypeManager->getStorage('cloud_config')->loadMultiple();

    $bundles = [];
    foreach ($cloud_configs ?: [] as $cloud_config) {
      /** @var \Drupal\cloud\Entity\CloudConfigInterface $cloud_config */
      if (array_key_exists($cloud_config->bundle(), $bundles)) {
        continue;
      }
      $this->cloudConfigPluginManager->setCloudContext($cloud_config->getCloudContext());
      $route = $this->cloudConfigPluginManager->getInstanceCollectionTemplateName();

      $matches = [];
      preg_match("/view\.{$cloud_config->bundle()}_(\w+)\.list/", $route, $matches);

      $bundles[$cloud_config->bundle()] = [
        'entityTypeId' => count($matches) > 1 ? $matches[1] : '',
        'label' => $this->entityTypeBundleInfo->getBundleInfo('cloud_config')[$cloud_config->bundle()]['label'],
      ];
    }

    $form['#attached']['drupalSettings']['cloud_configs'] = array_keys($bundles);
    $form['#attached']['drupalSettings']['cloud_config_infos'] = $bundles;

    $config = $this->configFactory()->getEditable('cloud_dashboard.settings');
    $location_map_config_uri = $config->get('cloud_dashboard_location_map_config_uri');
    $form['#attached']['drupalSettings']['location_map_config_uri'] = !empty($location_map_config_uri)
      ? Url::fromUri($location_map_config_uri)->toString()
      : Url::fromRoute('cloud_dashboard.location_map_config')->toString();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
