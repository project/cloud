<?php

/**
 * @file
 * Builds placeholder replacement tokens for aws-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function aws_cloud_token_info(): array {
  $types['aws_cloud_instance'] = [
    'name' => t('EC2 instances'),
    'description' => t('Tokens related to individual EC2 instances.'),
    'needs-data' => 'aws_cloud_instance',
  ];
  $instance['name'] = [
    'name' => t('EC2 instance name'),
    'description' => t('The name of the EC2 instance entity.'),
  ];
  $instance['id'] = [
    'name' => t('EC2 instance ID'),
    'description' => t('EC2 instance ID.'),
  ];
  $instance['launch_time'] = [
    'name' => t('Launch time'),
    'description' => t('The time the EC2 instance launched.'),
  ];
  $instance['instance_state'] = [
    'name' => t('Instance state'),
    'description' => t('The current state of the instance.'),
  ];
  $instance['availability_zone'] = [
    'name' => t('Availability Zone'),
    'description' => t('Zone the instance is in.'),
  ];
  $instance['private_ip'] = [
    'name' => t('Private IP address'),
    'description' => t('The EC2 instance private IP.'),
  ];
  $instance['public_ip'] = [
    'name' => t('Public IP address'),
    'description' => t('The EC2 instance public IP.'),
  ];
  $instance['elastic_ip'] = [
    'name' => t('Elastic IP address'),
    'description' => t('The instance Elastic IP address.'),
  ];
  $instance['instance_link'] = [
    'name' => t('EC2 instance Link'),
    'description' => t('Absolute link to the EC2 instance.'),
  ];
  $instance['instance_edit_link'] = [
    'name' => t('Edit EC2 instance link'),
    'description' => t('Absolute link to edit the EC2 instance.'),
  ];

  $types['aws_cloud_volume'] = [
    'name' => t('EBS volume'),
    'description' => t('Tokens related to individual EBS volumes.'),
    'needs-data' => 'aws_cloud_volume',
  ];
  $volume['name'] = [
    'name' => t('EBS volume name'),
    'description' => t('The name of the EBS volume entity.'),
  ];
  $volume['volume_link'] = [
    'name' => t('EBS volume Link'),
    'description' => t('Absolute link to EBS volume.'),
  ];
  $volume['volume_edit_link'] = [
    'name' => t('EBS volume edit link'),
    'description' => t('Absolute link to edit the EBS volume.'),
  ];
  $volume['created'] = [
    'name' => t('Create date'),
    'description' => t('The EBS volume create date.'),
  ];

  $types['aws_cloud_snapshot'] = [
    'name' => t('EBS snapshot'),
    'description' => t('Tokens related to individual EBS snapshots.'),
    'needs-data' => 'aws_cloud_snapshot',
  ];
  $snapshot['name'] = [
    'name' => t('EBS snapshot name'),
    'description' => t('The name of the EBS snapshot entity.'),
  ];
  $snapshot['snapshot_link'] = [
    'name' => t('EBS snapshot Link'),
    'description' => t('Absolute link to EBS snapshot.'),
  ];
  $snapshot['snapshot_edit_link'] = [
    'name' => t('EBS snapshot Edit Link'),
    'description' => t('Absolute link to edit the EBS snapshot.'),
  ];
  $snapshot['created'] = [
    'name' => t('Create date'),
    'description' => t('The EBS snapshot create date.'),
  ];

  $types['aws_cloud_elastic_ip'] = [
    'name' => t('Elastic IP'),
    'description' => t('Tokens related to individual Elastic IPs.'),
    'needs-data' => 'aws_cloud_elastic_ip',
  ];
  $elastic_ip['name'] = [
    'name' => t('Elastic IP name'),
    'description' => t('The name of the Elastic IP entity.'),
  ];
  $elastic_ip['elastic_ip_link'] = [
    'name' => t('Elastic IP Link'),
    'description' => t('Absolute link to Elastic IP.'),
  ];
  $elastic_ip['elastic_ip_edit_link'] = [
    'name' => t('Elastic IP Edit Link'),
    'description' => t('Absolute link to edit the Elastic IP.'),
  ];
  $elastic_ip['created'] = [
    'name' => t('Create date'),
    'description' => t('The Elastic IP create date.'),
  ];
  $types['aws_cloud_launch_template'] = [
    'name' => t('Launch Template'),
    'description' => t('Tokens related to individual launch template.'),
    'needs-data' => 'aws_cloud_launch_template',
  ];
  $launch_template['name'] = [
    'name' => t('List of launch template name'),
    'description' => t('Enter the name of the launch template entity.'),
  ];
  $launch_template['launch_template_link'] = [
    'name' => t('Launch template link'),
    'description' => t('An absolute link to launch template.'),
  ];
  $launch_template['launch_template_edit_link'] = [
    'name' => t('Launch template edit link'),
    'description' => t('An absolute link to edit the launch template.'),
  ];
  $launch_template['changed'] = [
    'name' => t('Change date'),
    'description' => t('The launch template change date.'),
  ];
  $types['aws_cloud_instance_email'] = [
    'name' => t('EC2 instance email'),
    'description' => t('Tokens related to individual EC2 instance email.'),
    'needs-data' => 'aws_cloud_instance_email',
  ];
  $instance_email['instances'] = [
    'name' => t('List of EC2 instances'),
    'description' => t('List of EC2 instances to display to user.'),
  ];

  $types['aws_cloud_volume_email'] = [
    'name' => t('EBS volume email'),
    'description' => t('Tokens related to individual EBS volume email.'),
    'needs-data' => 'aws_cloud_volume_email',
  ];
  $volume_email['volumes'] = [
    'name' => t('List of EBS volumes'),
    'description' => t('List of EBS volumes to display to user.'),
  ];

  $types['aws_cloud_snapshot_email'] = [
    'name' => t('EBS snapshot email'),
    'description' => t('Tokens related to individual EBS snapshot email.'),
    'needs-data' => 'aws_cloud_snapshot_email',
  ];
  $snapshot_email['snapshots'] = [
    'name' => t('List of EBS snapshots'),
    'description' => t('List of EBS snapshots to display to user.'),
  ];

  $types['aws_cloud_elastic_ip_email'] = [
    'name' => t('Elastic IP email'),
    'description' => t('Tokens related to individual Elastic IP email.'),
    'needs-data' => 'aws_cloud_elastic_ip_email',
  ];
  $elastic_ip_email['elastic_ips'] = [
    'name' => t('List of Elastic IPs'),
    'description' => t('List of Elastic IPs to display to a user.'),
  ];
  $types['aws_cloud_launch_template_email'] = [
    'name' => t('Launch template email'),
    'description' => t('Tokens related to individual launch template email.'),
    'needs-data' => 'aws_cloud_launch_template_email',
  ];
  $launch_template_email['launch_templates'] = [
    'name' => t('List of launch templates'),
    'description' => t('List of launch templates to display to a user.'),
  ];
  $types['aws_cloud_launch_template_request_email'] = [
    'name' => t('Launch template request email'),
    'description' => t('Tokens related to individual launch template request email.'),
    'needs-data' => 'aws_cloud_launch_template_request_email',
  ];
  $launch_template_request_email['launch_templates_request'] = [
    'name' => t('List of request launch templates'),
    'description' => t('List of request launch templates to display to a user.'),
  ];

  $types['aws_cloud_launch_template_requext'] = [
    'name' => t('Launch template request'),
    'description' => t('Tokens related to individual launch template.'),
    'needs-data' => 'aws_cloud_launch_template_request',
  ];
  $launch_template_request['name'] = [
    'name' => t('List of launch template name'),
    'description' => t('Enter the name of the launch template entity.'),
  ];
  $launch_template_request['launch_template_link'] = [
    'name' => t('Launch template link'),
    'description' => t('An absolute link to launch template.'),
  ];
  $launch_template_request['launch_template_edit_link'] = [
    'name' => t('Launch template edit link'),
    'description' => t('An absolute link to edit the launch template.'),
  ];
  $launch_template_request['changed'] = [
    'name' => t('Change date'),
    'description' => t('The launch template change date.'),
  ];
  $launch_template_request['launch_template_button_approve'] = [
    'name' => t('Approve button'),
    'description' => t('The launch template approve button.'),
  ];

  return [
    'types' => $types,
    'tokens' => [
      'aws_cloud_instance' => $instance,
      'aws_cloud_volume' => $volume,
      'aws_cloud_snapshot' => $snapshot,
      'aws_cloud_elastic_ip' => $elastic_ip,
      'aws_cloud_launch_template' => $launch_template,
      'aws_cloud_instance_email' => $instance_email,
      'aws_cloud_volume_email' => $volume_email,
      'aws_cloud_snapshot_email' => $snapshot_email,
      'aws_cloud_elastic_ip_email' => $elastic_ip_email,
      'aws_cloud_launch_template_email' => $launch_template_email,
      'aws_cloud_launch_template_request' => $launch_template_request,
      'aws_cloud_launch_template_request_email' => $launch_template_request_email,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function aws_cloud_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {

  $replacements = [];
  if ($type === 'aws_cloud_instance' && !empty($data['aws_cloud_instance'])) {
    $replacements = aws_cloud_instance_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_volume' && !empty($data['aws_cloud_volume'])) {
    $replacements = aws_cloud_volume_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_snapshot' && !empty($data['aws_cloud_snapshot'])) {
    $replacements = aws_cloud_snapshot_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_elastic_ip' && !empty($data['aws_cloud_elastic_ip'])) {
    $replacements = aws_cloud_elastic_ip_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_launch_template' && !empty($data['aws_cloud_launch_template'])) {
    $replacements = aws_cloud_launch_template_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_launch_template_request' && !empty($data['aws_cloud_launch_template_request'])) {
    $replacements = aws_cloud_launch_template_request_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_instance_email') {
    $replacements = aws_cloud_instance_email_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_volume_email') {
    $replacements = aws_cloud_volume_email_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_snapshot_email') {
    $replacements = aws_cloud_snapshot_email_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_elastic_ip_email') {
    $replacements = aws_cloud_elastic_ip_email_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_launch_template_email') {
    $replacements = aws_cloud_launch_template_email_tokens($tokens, $data);
  }
  elseif ($type === 'aws_cloud_launch_template_request_email') {
    $replacements = aws_cloud_launch_template_request_email_tokens($tokens, $data);
  }
  return $replacements;
}

/**
 * Setup instance email token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_instance_email_tokens(array $tokens, array $data): array {
  $replacements = [];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'instances':
        $replacements[$original] = $data['instances'];
        break;
    }
  }
  return $replacements;
}

/**
 * Setup volume email token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_volume_email_tokens(array $tokens, array $data): array {
  $replacements = [];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'volumes':
        $replacements[$original] = $data['volumes'];
        break;
    }
  }
  return $replacements;
}

/**
 * Setup snapshot email token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_snapshot_email_tokens(array $tokens, array $data): array {
  $replacements = [];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'snapshots':
        $replacements[$original] = $data['snapshots'];
        break;
    }
  }
  return $replacements;
}

/**
 * Setup Elastic IP email token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_elastic_ip_email_tokens(array $tokens, array $data): array {
  $replacements = [];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'elastic_ips':
        $replacements[$original] = $data['elastic_ips'];
        break;
    }
  }
  return $replacements;
}

/**
 * Setup launch template email token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_launch_template_email_tokens(array $tokens, array $data): array {
  $replacements = [];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'launch_templates':
        $replacements[$original] = $data['launch_templates'];
        break;
    }
  }
  return $replacements;
}

/**
 * Setup launch template request email token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_launch_template_request_email_tokens(array $tokens, array $data): array {
  $replacements = [];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'launch_templates_request':
        $replacements[$original] = $data['launch_templates_request'];
        break;

      case 'site_url':
        global $base_url;
        $replacements[$original] = t('<a href=":url">:url</a>', [
          ':url' => $base_url,
        ]);
    }
  }
  return $replacements;
}

/**
 * Setup volume token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_volume_tokens(array $tokens, array $data): array {
  $replacements = [];
  /** @var \Drupal\aws_cloud\Entity\Ec2\Volume $volume */
  $volume = $data['aws_cloud_volume'];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'name':
        $replacements[$original] = $volume->getName();
        break;

      case 'volume_link':
        $replacements[$original] = $volume->toUrl('canonical', ['absolute' => TRUE])->toString();
        break;

      case 'volume_link_edit':
        $replacements[$original] = $volume->toUrl('edit-form', ['absolute' => TRUE])
          ->toString();
        break;

      case 'created':
        $replacements[$original] = \Drupal::service('date.formatter')->format($volume->created->value, 'custom', 'Y-m-d H:i:s O');
        break;
    }
  }
  return $replacements;
}

/**
 * Setup snapshot token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_snapshot_tokens(array $tokens, array $data): array {
  $replacements = [];
  /** @var \Drupal\aws_cloud\Entity\Ec2\Snapshot $snapshot */
  $snapshot = $data['aws_cloud_snapshot'];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'name':
        $replacements[$original] = $snapshot->getName();
        break;

      case 'snapshot_link':
        $replacements[$original] = $snapshot->toUrl('canonical', ['absolute' => TRUE])->toString();
        break;

      case 'snapshot_link_edit':
        $replacements[$original] = $snapshot->toUrl('edit-form', ['absolute' => TRUE])
          ->toString();
        break;

      case 'created':
        $replacements[$original] = \Drupal::service('date.formatter')->format($snapshot->created->value, 'custom', 'Y-m-d H:i:s O');
        break;
    }
  }
  return $replacements;
}

/**
 * Setup Elastic IP token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_elastic_ip_tokens(array $tokens, array $data): array {
  $replacements = [];
  /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIp $elastic_ip */
  $elastic_ip = $data['aws_cloud_elastic_ip'];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'name':
        $replacements[$original] = $elastic_ip->getName();
        break;

      case 'elastic_ip_link':
        $replacements[$original] = $elastic_ip->toUrl('canonical', ['absolute' => TRUE])->toString();
        break;

      case 'elastic_ip_link_edit':
        $replacements[$original] = $elastic_ip->toUrl('edit-form', ['absolute' => TRUE])
          ->toString();
        break;

      case 'created':
        $replacements[$original] = \Drupal::service('date.formatter')->format($elastic_ip->created->value, 'custom', 'Y-m-d H:i:s O');
        break;
    }
  }
  return $replacements;
}

/**
 * Setup launch template token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_launch_template_tokens(array $tokens, array $data): array {
  $replacements = [];
  /** @var Drupal\cloud\Entity\CloudLaunchTemplate $launch_template */
  $launch_template = $data['aws_cloud_launch_template'];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'name':
        $replacements[$original] = $launch_template->getName();
        break;

      case 'launch_template_link':
        $replacements[$original] = $launch_template->toUrl('canonical', ['absolute' => TRUE])->toString();
        break;

      case 'launch_template_link_edit':
        $replacements[$original] = $launch_template->toUrl('edit-form', ['absolute' => TRUE])
          ->toString();
        break;

      case 'changed':
        $replacements[$original] = \Drupal::service('date.formatter')->format($launch_template->changed->value, 'custom', 'Y-m-d H:i:s O');
        break;
    }
  }
  return $replacements;
}

/**
 * Setup launch template token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_launch_template_request_tokens(array $tokens, array $data): array {
  $replacements = [];
  /** @var Drupal\cloud\Entity\CloudLaunchTemplate $launch_template */
  $launch_template = $data['aws_cloud_launch_template_request'];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'name':
        $replacements[$original] = $launch_template->getName();
        break;

      case 'launch_template_link':
        $replacements[$original] = t('<a href=":url">:url</a>', [
          ':url' => $launch_template->toUrl('canonical', ['absolute' => TRUE])->toString(),
        ]);
        break;

      case 'launch_template_link_edit':
        $replacements[$original] = t('<a href=":url">:url</a>', [
          ':url' => $launch_template->toUrl('edit-form', ['absolute' => TRUE])->toString(),
        ]);
        break;

      case 'launch_template_button_approve':
        $replacements[$original] = $launch_template->toUrl('approve', ['absolute' => TRUE])->toString();
        break;

      case 'changed':
        $replacements[$original] = \Drupal::service('date.formatter')->format($launch_template->changed->value, 'custom', 'Y-m-d H:i:s O');
        break;
    }
  }
  return $replacements;
}

/**
 * Setup instance token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function aws_cloud_instance_tokens(array $tokens, array $data): array {
  $replacements = [];
  /** @var \Drupal\aws_cloud\Entity\Ec2\Instance $instance */
  $instance = $data['aws_cloud_instance'];

  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'name':
        $replacements[$original] = $instance->getName();
        break;

      case 'id':
        $replacements[$original] = $instance->getInstanceId();
        break;

      case 'launch_time':
        $replacements[$original] = \Drupal::service('date.formatter')
          ->format($instance->getLaunchTime(), 'custom', 'Y-m-d H:i:s O');
        break;

      case 'instance_state':
        $replacements[$original] = $instance->getInstanceState();
        break;

      case 'availability_zone':
        $replacements[$original] = $instance->getAvailabilityZone();
        break;

      case 'private_ip':
        $replacements[$original] = $instance->getPrivateIps();
        break;

      case 'public_ip':
        $replacements[$original] = $instance->getPublicIp();
        break;

      case 'elastic_ip':
        $replacements[$original] = $instance->getElasticIp();
        break;

      case 'instance_link':
        $replacements[$original] = $instance->toUrl('canonical', ['absolute' => TRUE])
          ->toString();
        break;

      case 'instance_edit_link':
        $replacements[$original] = $instance->toUrl('edit-form', ['absolute' => TRUE])
          ->toString();
        break;

    }
  }
  return $replacements;
}
