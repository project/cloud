(function () {
  'use strict';

  // The interval to update a view's content.
  let refresh_in_sec = drupalSettings.aws_cloud_view_refresh_interval || 10;

  // The function to get a view's content in an Ajax way.
  let auto_update = function () {

    // The URL of the callback for a view.
    let url = document.querySelector('div.views-form form').getAttribute('action');
    let query_str = '';
    let pos = url.indexOf('?');
    if (pos !== -1) {
      query_str = url.substr(pos);
      url = url.substr(0, pos);
    }
    url += '/callback';

    // Update .view-content element, which is the content of view, excluding
    // exposed filter and pager.
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url + query_str, true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        let data = xhr.responseText;
        let parser = new DOMParser();
        let doc = parser.parseFromString(data, 'text/html');
        let form_token = doc.querySelector('.view-content input[name=form_token]').value;
        let checkbox_ids = Array.from(doc.querySelectorAll('.view-content .form-checkbox:checked')).map(function (checkbox) {
          return checkbox.id;
        });

        // Replace table.
        let oldTable = document.querySelector('.views-element-container .view-content div.table-responsive');
        let newTable = doc.querySelector('.view-content div.table-responsive');
        oldTable.parentNode.replaceChild(newTable, oldTable);

        // Replace pager.
        let oldPager = document.querySelector('.views-element-container nav.pager-nav');
        let newPager = doc.querySelector('nav.pager-nav');

        if (oldPager && oldPager.parentNode) {
          oldPager.parentNode.replaceChild(newPager, oldPager);
        }

        // Restore form token.
        // If form token changes, the form validation will fail.
        if (form_token) {
          document.querySelector('.view-content input[name=form_token]').value = form_token;
        }

        // Fix for the theme Bartik, which need to initialize drop buttons.
        if (Drupal.behaviors.dropButton) {
          Drupal.behaviors.dropButton.attach(document, drupalSettings);
        }

        if (Drupal.behaviors.tableSelect) {
          Drupal.behaviors.tableSelect.attach(document, drupalSettings);
        }

        // Fix button initialization for bootstrap theme.
        if (Drupal.behaviors.bootstrapDropdown) {
          Drupal.behaviors.bootstrapDropdown.attach(document, drupalSettings);
        }

        // Restore checkbox selection.
        checkbox_ids.forEach(function (val) {
          if (val) {
            document.getElementById(val).checked = true;
          }
          else {
            document.querySelector('.select-all .form-checkbox').checked = true;
          }
        });
      }
    };
    xhr.send();
  };

  // Update a view's content every "refresh_in_sec" seconds.
  setInterval(auto_update, refresh_in_sec * 1000);
})();
