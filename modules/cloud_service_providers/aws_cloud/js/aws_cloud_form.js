(function (Drupal, drupalSettings) {
  'use strict';

  // Image id and instance type fields are TomSelect fields.  Because these
  // fields are AJAX fields, they will reinitialize during onChange.
  // During AJAX, the TomSelects need to be destroyed and reinitialized.
  // Declare these elements outside the behavior, so they can be checked and
  // destroyed.
  let image_id;
  let instance_type;

  // Store the user selected values for disabling and enabling of the
  // select elements.
  let user_selected_instance_type;
  let user_selected_image_id;

  /**
   * Helper to remove options from fields.
   */
  const removeOptions = function (selectElement, valuesToRemove) {
    selectElement.querySelectorAll('option').forEach(function (option) {
      if (!valuesToRemove.includes(option.value)) {
        option.remove();
      }
    });
  }

  /**
   * Initialize network fields.
   */
  Drupal.behaviors.networkFieldInit = {
    attach(context, settings) {
      removeOptions(document.querySelector('select[name=field_subnet]'), drupalSettings.aws_cloud.field_subnet_default_values);
      removeOptions(document.querySelector("select[name='field_security_group[]']"), drupalSettings.aws_cloud.field_security_group_default_values);
    }
  }

  /**
   * Initialize ssh fields.
   */
  Drupal.behaviors.sshFieldInit = {
    attach(context, settings) {
      // Remove "- Select a value -" option if there is only one SSH key.
      let field_ssh_key_options = document.querySelectorAll('select[name=field_ssh_key] option');
      if (field_ssh_key_options.length === 2) {
        field_ssh_key_options.forEach(function (option) {
          if (option.value === '_none') {
            option.remove();
          }
        });
      }
    }
  }

  /**
   * Initialize TomSelect.
   */
  Drupal.behaviors.tomSelectInit = {
    attach(context, settings) {
      // Initialize TomSelect.
      let prepare_instance_type = function () {
        // Do not display cost information in the dropdown of instance type,
        // if the configuration is false.
        if (!drupalSettings.aws_cloud.aws_cloud_instance_type_cost) {
          return;
        }
        // Grab all the <option> elements and reformat it for TomSelect.
        let select_element = document.getElementById('edit-field-instance-type');
        let reformatted_options = Array.from(select_element.options).map(function (option) {
          let parts = option.text.split(':').slice(0, 5);
          // Keep the default _none element the same.
          if (parts.length === 1) {
            return {
              id: '_none',
              type: '- Select a value -'
            }
          }
          return {
            id: option.value,
            type: parts[0],
            vcpu: parts[1],
            ecu: parts[2],
            memory: parts[3],
            rate: parts[4]
          }
        })

        // Destroy the existing TomSelect and reinitialize because of
        // AJAX form refresh.
        if (image_id) {
          image_id.destroy();
        }
        image_id = new TomSelect('#edit-field-image-id', {
          plugins: ['dropdown_input'],
          onInitialize: function () {
            // Store the user selected value.
            user_selected_image_id = document.getElementById('edit-field-image-id').value;
          },
        });

        if (instance_type) {
          // Destroy the existing TomSelect and reinitialize.
          instance_type.destroy();
        }
        instance_type = new TomSelect('#edit-field-instance-type', {
          plugins: ['dropdown_input'],
          options: reformatted_options,
          valueField: 'id',
          searchField: ['type', 'vcpu', 'memory'],
          render: {
            item: function (item, escape) {
              return `<div class="instance-info"><span class="instance-type">${item.type}</span></div>`;
            },
            option: function (item, escape) {
              if (item.id == '_none') {
                return '<div><span>- Select a value -</span></div>';
              }
              return `<div class="instance-info">
                <span class="instance-type">${item.type}</span>
                <span class="instance-vcpu">${item.vcpu} vCPU</span>
                <span class="instance-mem"> ${item.memory} Memory</span>
                <span class="instance-ecu"> ${item.ecu} ECUs</span>
                <span class="instance-cost"> ${item.rate} USD per hour</span>
                </div>`;
            },
          },
          onInitialize: function () {
            // Store the user selected value.
            user_selected_instance_type = document.getElementById('edit-field-instance-type').value;
          },
        });

        // Add onBlur functions after both TomSelects dropdowns have been
        // initialized.  Don't disable if the user didn't change the value.
        instance_type.on('blur', function (name, argument) {
          if (user_selected_instance_type === instance_type.getValue()) {
            return;
          }
          image_id.disable();
        });
        image_id.on('blur', function () {
          if (user_selected_image_id === image_id.getValue()) {
            return;
          }
          instance_type.disable();
        });

        // Re-enable the TomSelect fields at the very end, to make sure
        // none of the fields are disabled.
        image_id.enable();
        instance_type.enable();
      }
      prepare_instance_type();
    }
  }
})(Drupal, drupalSettings);
