(function () {
  'use strict';

  const mySelect = new TomSelect('#edit-name', {
    valueField: 'id',
    labelField: 'name',
    searchField: [],
    placeholder: 'Search for images',
    shouldLoad: function (query, callback) {
      if (query.length > 3) {//if search has at least 4 chars
        return true;
      }
    },
    // fetch remote data
    load: function (query, callback) {
      // Clear existing options before adding new ones
      mySelect.clearOptions();
      const url = 'search?q=' + encodeURIComponent(query);
      fetch(url)
        .then(response => response.json())
        .then(json => {
          callback(json);
        }).catch(() => {
          callback();
        });

    },
    render: {
      option: function (item, escape) {
        return `<div><div style="font-weight: bold;">${escape(item.name)}</div></div>`;
      },

      item: function (item, escape) {
        return '<div title="' + item.name + '">' + item.name + '</div>';
      }
    }

  });

})();
