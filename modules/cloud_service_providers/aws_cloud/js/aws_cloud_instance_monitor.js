(function () {
  'use strict';

  // Metrics Url.
  // Needed as this js shares functionality with the InstanceMonitorBlock and InstanceMonitorForm.
  const metrics_url = drupalSettings.aws_cloud_monitor_metrics_url ? drupalSettings.aws_cloud_monitor_metrics_url : 'metrics';

  let generateChart = function (bindTo) {
    return c3.generate({
      bindto: bindTo,
      data: {
        x: 'x',
        xFormat: '%Y-%m-%dT%H:%M:%S+00:00',
        xLocaltime: false,
        columns: []
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%m-%d %H:%M:%S'
          }
        }
      }
    });
  };

  const updateCharts = function () {
    fetch(metrics_url)
      .then(res => res.json())
      .then(json => {
        cpu_chart.load({
          columns: [
            ['x'].concat(json.cpu.timestamps),
            ['cpu utilization'].concat(json.cpu.values),
          ],
        });

        network_chart.load({
          columns: [
            ['x'].concat(json.network_in.timestamps),
            ['network in'].concat(json.network_in.values),
            ['network out'].concat(json.network_out.values),
          ],
        });

        disk_chart.load({
          columns: [
            ['x'].concat(json.disk_read.timestamps),
            ['disk read'].concat(json.disk_read.values),
            ['disk write'].concat(json.disk_write.values),
          ],
        });

        disk_operation_chart.load({
          columns: [
            ['x'].concat(json.disk_read_operation.timestamps),
            ['disk read operation'].concat(json.disk_read_operation.values),
            ['disk write operation'].concat(json.disk_write_operation.values),
          ],
        });
      });
  };

  const cpu_chart = generateChart('#edit-cpu-chart');
  const network_chart = generateChart('#edit-network-chart');
  const disk_chart = generateChart('#edit-disk-chart');
  const disk_operation_chart = generateChart('#edit-disk-operation-chart');

  updateCharts();

  let interval = drupalSettings.aws_cloud_monitor_refresh_interval || 10;
  setInterval(function () {
    updateCharts();
  }, interval * 1000);

  const radio_btn = document.querySelectorAll(`input[id='edit-chart-type-0']`);
  if (!radio_btn || !radio_btn[0]) {
    return;
  }
  radio_btn[0].addEventListener('change', function (event) {
    let count = 0;
    const interval = setInterval(function () {
      count++;
      let chart = document.querySelectorAll('#edit-disk-operation-chart');
      if (!chart || !chart[0]) {
        // count 20 is almost 2 sec.
        if (count < 20) {
          return;
        }
        clearInterval(interval);
      }
      cpu_chart = generateChart('#edit-cpu-chart');
      network_chart = generateChart('#edit-network-chart');
      disk_chart = generateChart('#edit-disk-chart');
      disk_operation_chart = generateChart('#edit-disk-operation-chart');

      updateCharts();
      clearInterval(interval);
    }, 100);
  });
})();
