(function () {
  'use strict';

  // Metrics Url.
  if (!drupalSettings.aws_cloud_monitor_metrics_url) {
    return;
  }

  let chart_count = 0;
  // Init Chart.
  const initCharts = function () {
    fetch(drupalSettings.aws_cloud_monitor_metrics_url)
      .then(response => response.json())
      .then(json => {
        let chart_data = [];
        Object.entries(json).forEach(([name, obj]) => {
          const selector = 'edit-' + name.replaceAll('_' , '-') + '-horizon-chart';
          const title = document.getElementById(selector).getAttribute('chart-name');
          let data = [];
          obj.timestamps.forEach((element, index) => {
            data.push([
              d3.timeParse('%Y-%m-%dT%H:%M:%S+00:00')(element),
              obj.values[index],
            ]);
          });
          chart_data.push({
            id: title,
            values: data,
            selector: '#' + selector,
          });
          chart_count++;
        });
        Drupal.Cloud.drawChart(chart_data, true);
      });
  };

  // Init Charts.
  initCharts();

  // Refresh charts.
  const interval = drupalSettings.aws_cloud_monitor_refresh_interval || 10;
  setInterval(function () {
    initCharts();
  }, interval * 1000);

  const radio_btn = document.querySelectorAll(`input[id='edit-chart-type-1']`);
  if (!radio_btn || !radio_btn[0]) {
    return;
  }

  radio_btn[0].addEventListener('change', function (event) {
    let count = 0;
    const interval = setInterval(function () {
      count++;
      const chart_parent = document.querySelectorAll('#aws-cloud-instance-monitor-charts .card-body');
      if (!chart_parent || !chart_parent[0] || chart_parent[0].childNodes.length < chart_count) {
        // count 20 is almost 2 sec.
        if (count < 20) {
          return;
        }
        clearInterval(interval);
      }
      initCharts();
      clearInterval(interval);
    }, 100);
  });
})();
