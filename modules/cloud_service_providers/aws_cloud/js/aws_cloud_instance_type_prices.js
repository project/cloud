(function () {
  'use strict';

  if (!window.location.hash) {
    return;
  }

  let instance_type = window.location.hash.substr(1);
  let instance_type_underscore = instance_type.replace('.', '_');

  // Find the column with instance type.
  let trList = document.querySelectorAll('tr.' + instance_type_underscore);
  let tr = trList[0];
  let tdList = tr.children;
  let td = tdList[0];
  if (!td.innerHTML) {
    return;
  }

  // Highlight the row.
  tr.classList.add('highlight');

  // Calculate position.
  let navbar = document.querySelector('header');
  let navbar_top = navbar.offsetTop;
  let header_height = document.querySelector('table.aws_cloud_instance_type_prices thead').clientHeight;
  let top = td.offsetTop - header_height - navbar_top;

  setTimeout(function () {
    window.scrollTo(0, top);
  }, 500);
})();
