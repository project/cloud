(function (drupalSettings) {
  'use strict';

  // Function to set the maximum width based on the window width and the provided maximum width.
  function setMaxWidth() {
    const maxWidth = Math.min(window.innerWidth, drupalSettings.aws_cloud.maxWidth);
    document.querySelectorAll('.horizontal-scroll').forEach(function (element) {
      element.style.maxWidth = maxWidth + 'px';
    });
  }

  // Event listener for when the DOM content is fully loaded.
  document.addEventListener('DOMContentLoaded', function () {
    // Initial setup to set the maximum width.
    setMaxWidth();

    // Event listener for window resize to update the maximum width.
    window.addEventListener('resize', setMaxWidth);
  });

}(drupalSettings));
