(function (drupalSettings) {
  'use strict';

  window.addEventListener('load', function () {
    if (!drupalSettings.download_url) {
      return;
    }
    let download_url = drupalSettings.download_url;
    location.href = download_url;
  });

}(drupalSettings));
