(function (Drupal) {
  'use strict';

  Drupal.SecurityGroup = Drupal.SecurityGroup || {};

  Drupal.SecurityGroup.autoPopulate = {
    populateAllPorts: function (protocol, field, key) {
      if (protocol === '-1') {
        document.querySelector('.form-item-' + field + '-' + key + '-from-port input').value = '0';
        document.querySelector('.form-item-' + field + '-' + key + '-to-port input').value = '65535';
      }
    }
  };

  Drupal.SecurityGroup.showHide = {
    hideRow: function (field, row_count, table_id) {
      const from_port = document.querySelector('.' + table_id + ' tr.row-' + row_count + ' .form-item-' + field + '-' + row_count + '-from-port input').value;
      const to_port = document.querySelector('.' + table_id + ' tr.row-' + row_count + ' .form-item-' + field + '-' + row_count + '-to-port input').value;

      // If these fields are blank, they qualify as an empty row. Remove them
      if (from_port !== '' && to_port !== '') {
        document.querySelectorAll('.' + table_id + ' tr.row-' + row_count + ' input').forEach(input => input.value = '');
        document.querySelectorAll('.' + table_id + ' tr.row-' + row_count + ' select').forEach(select => select.value = '');

        document.querySelector('.' + table_id + ' tr.row-' + row_count).classList.add('hide');

        // Display a message telling user to save the page before the rule change is applied.
        if (!document.querySelector('.messages--warning')) {
          const warningDiv = document.createElement('div');
          warningDiv.classList.add('messages', 'messages--warning');
          warningDiv.setAttribute('role', 'alert');
          warningDiv.innerHTML = '<abbr class="warning">*</abbr> Click "Save" to apply rule changes.';

          document.querySelector('#edit-rules .details-wrapper').prepend(warningDiv);
        }
      }
    },
  };

  Drupal.behaviors.securityPermissions = {
    attach: function (context, settings) {
      Array.from(document.querySelectorAll('.field--name-ip-permission .ip-protocol-select')).forEach((el, k) => {
        el.addEventListener('change', () => {
          // Populate From-To ports for "all traffic" option.
          Drupal.SecurityGroup.autoPopulate.populateAllPorts(el.value, 'ip-permission', k);
        });
      });

      Array.from(document.querySelectorAll('.field--name-outbound-permission .ip-protocol-select')).forEach((el, k) => {
        el.addEventListener('change', () => {
          // Populate From-To ports for "all traffic" option.
          Drupal.SecurityGroup.autoPopulate.populateAllPorts(el.value, 'outbound-permission', k);
        });
      });

      // When the link is clicked, clear out the 'to' and 'from' port.
      // Hide the row, and add a message for the user.
      Array.from(document.querySelectorAll('.ip-permission-values .remove-rule')).forEach((el, k) => {
        el.addEventListener('click', () => {
          const row_count = el.getAttribute('data-row');
          const table_id = el.getAttribute('data-table-id');
          Drupal.SecurityGroup.showHide.hideRow('ip-permission', row_count, table_id);
        });
      });

      Array.from(document.querySelectorAll('.outbound-permission-values .remove-rule')).forEach((el, k) => {
        el.addEventListener('click', () => {
          const row_count = el.getAttribute('data-row');
          const table_id = el.getAttribute('data-table-id');
          Drupal.SecurityGroup.showHide.hideRow('outbound-permission', row_count, table_id);
        });
      });

    }
  };
})(Drupal);
