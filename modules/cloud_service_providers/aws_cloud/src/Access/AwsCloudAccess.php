<?php

namespace Drupal\aws_cloud\Access;

use Aws\Exception\AwsException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Service\Iam\IamServiceInterface;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Service\CloudCacheServiceInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Access validation for Aws resource operations/forms.
 *
 * AwsCloudAccess checks user access against the database table
 * cloud_config_iam_permissions instead of a realtime API call. The permissions
 * in cloud_config_iam_permissions table are retrieved using a queue called
 * AwsCloudUpdateIamPermissionsQueueWorker.
 *
 * The CRUD operations use a raw $database(\Drupal\Core\Database\Connection)
 * object to access the database instead of a ConfigEntity or custom
 * entities to reduce any overhead and extraneous method/db calls from
 * those APIs.
 *
 * @package Drupal\aws_cloud\Access
 */
class AwsCloudAccess implements AwsCloudAccessInterface {

  use CloudContentEntityTrait;

  /**
   * EC2 service.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  protected $ec2Service;

  /**
   * IAM service.
   *
   * @var \Drupal\aws_cloud\Service\Iam\IamServiceInterface
   */
  protected $iamService;

  /**
   * The Cloud cache service.
   *
   * @var \Drupal\cloud\Service\CloudCacheServiceInterface
   */
  protected $cloudCacheService;

  /**
   * Database object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * AwsCloudAccess constructor.
   *
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The EC2 service.
   * @param \Drupal\aws_cloud\Service\Iam\IamServiceInterface $iam_service
   *   The IAM service.
   * @param \Drupal\cloud\Service\CloudCacheServiceInterface $cloud_cache_service
   *   The Cloud cache service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   */
  public function __construct(
    Ec2ServiceInterface $ec2_service,
    IamServiceInterface $iam_service,
    CloudCacheServiceInterface $cloud_cache_service,
    Connection $database,
    ConfigFactoryInterface $config_factory,
  ) {
    $this->ec2Service = $ec2_service;
    $this->iamService = $iam_service;
    $this->cloudCacheService = $cloud_cache_service;
    $this->database = $database;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): AwsCloudAccess {
    return new static(
      $container->get('aws_cloud.ec2'),
      $container->get('aws_cloud.iam'),
      $container->get('cloud.cache'),
      $container->get('database'),
      $container->get('config.factory')
    );
  }

  /**
   * Validate IAM permissions by invoking AWS APIs.
   *
   * @param string $perm
   *   Permission.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface|\Drupal\aws_cloud\Service\Iam\IamServiceInterface $service
   *   Service object, EC2 or IAM.
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access result.
   */
  private function validateIamPermissions(string $perm, $service, string $cloud_context): AccessResultInterface {

    // 1. Check arguments.
    if (empty($perm)
      || empty($cloud_context)) {
      return AccessResult::allowed();
    }

    // 2. Check if $service is Ec2ServiceInterface or IamServiceInterface.
    if (!($service instanceof Ec2ServiceInterface)
      && !($service instanceof IamServiceInterface)) {
      return AccessResult::allowed();
    }

    // 3. Check if $service::PERMISSION_TO_AWS_API[$perm] is valid or not.
    if (is_array($service::PERMISSION_TO_AWS_API)
      && !array_key_exists($perm, $service::PERMISSION_TO_AWS_API)) {
      return AccessResult::allowed();
    }

    // 4. Set $cloud_context.
    $service->setCloudContext($cloud_context);

    if (method_exists($service, 'isWorkerResource') && $service->isWorkerResource()) {
      return AccessResult::allowed();
    }

    // 5. Validate IAM permissions for AWS APIs.
    $apis = (array) $service::PERMISSION_TO_AWS_API[$perm];
    foreach ($apis as $api) {
      try {
        $method = lcfirst($api);
        $service->$method(['DryRun' => TRUE]);
      }
      catch (AwsException $e) {
        // If permission is granted, the error code is DryRunOperation.
        // If not, the error code is one of permissionErrorCodes.
        $error_code = $e->getAwsErrorCode();

        if (in_array($error_code, $service::PERMISSION_ERROR_CODES, TRUE)) {
          $this->logger('aws_cloud')->error($e->getMessage());
          return AccessResult::forbidden();
        }
        // Other error code means an unexpected error occurs.
        // @FIXME This condition will be cleaned once AWS-service-related
        // constants are better defined.
        if ($service instanceof IamServiceInterface
          || $error_code !== Ec2ServiceInterface::DRY_RUN_OPERATION) {
          $this->logger('aws_cloud')->error($e->getMessage());
        }
      }
      catch (\Exception $e) {
        // Continue processing even if unexpected exceptions occur.
        $this->logger('aws_cloud')->error($e->getMessage());
      }
    }
    return AccessResult::allowed();
  }

  /**
   * {@inheritDoc}
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface {
    $perm = $route->getOption('perm');
    $ec2_perms = $this->ec2Service::PERMISSION_TO_AWS_API;
    $iam_perms = $this->iamService::PERMISSION_TO_AWS_API;
    if (!array_key_exists($perm, $ec2_perms) && !array_key_exists($perm, $iam_perms)) {
      return AccessResult::allowed();
    }
    $ec2_operations = array_key_exists($perm, $ec2_perms) ? (array) $ec2_perms[$perm] : [];
    $iam_operations = array_key_exists($perm, $iam_perms) ? (array) $iam_perms[$perm] : [];
    $operations = array_merge($ec2_operations, $iam_operations);

    if (empty($operations)) {
      return AccessResult::allowed();
    }

    foreach ($operations as $operation) {
      $access = $this->cloudCacheService->getCachedOperationAccess($cloud_context, $operation);
      if (empty($access) || !($access instanceof AccessResult)) {
        return $this->hasPermission($cloud_context, $perm, $operation);
      }
      if ($access instanceof AccessResult && $access->isAllowed()) {
        continue;
      }
      return $access;
    }
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function hasPermission(string $cloud_context, string $perm, string $operation): AccessResultInterface {
    $rows = $this->database->select(AwsCloudAccessInterface::IAM_PERMISSIONS_DB_TABLE)
      ->condition('cloud_context', $cloud_context)
      ->condition('drupal_permission', $perm)
      ->condition('iam_permission', $operation)
      ->countQuery()
      ->execute()
      ->fetchField();
    if (!empty($rows)) {
      $this->cloudCacheService->setCachedOperationAccess($cloud_context, $operation);
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePermissions(string $cloud_context): void {
    // Delete all permissions before updating.
    $this->deletePermissions($cloud_context);

    // Using recursive merge because of associative arrays.
    $operations = array_merge_recursive($this->ec2Service::PERMISSION_TO_AWS_API, $this->iamService::PERMISSION_TO_AWS_API);

    foreach ($operations as $perm => $operation) {
      $access = $this->validateIamPermissions($perm, $this->ec2Service, $cloud_context)
        ->andIf($this->validateIamPermissions($perm, $this->iamService, $cloud_context));

      if ($access->isAllowed() === FALSE) {
        continue;
      }

      if (is_array($operation) === TRUE) {
        foreach ($operation ?: [] as $op) {
          $this->insertPermission($cloud_context, $op, $perm);
        }
        continue;
      }

      $this->insertPermission($cloud_context, $operation, $perm);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deletePermissions(string $cloud_context, $perm = ''): void {
    $query = $this->database
      ->delete(AwsCloudAccessInterface::IAM_PERMISSIONS_DB_TABLE)
      ->condition('cloud_context', $cloud_context);
    if (!empty($perm)) {
      $query->condition('iam_permission', $perm);
    }
    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function insertPermission(string $cloud_context, string $op, string $perm): void {
    $this->database
      ->insert(AwsCloudAccessInterface::IAM_PERMISSIONS_DB_TABLE)
      ->fields([
        'cloud_context' => $cloud_context,
        'iam_permission' => $op,
        'drupal_permission' => $perm,
        'created' => time(),
      ])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function createResourceQueueItems(CloudConfigInterface $cloud_config): void {
    $queue_limit = $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_queue_limit');
    $method_names = [
      'updatePermissions',
    ];
    self::updateResourceQueue($cloud_config, $method_names, $queue_limit, self::getIamPermissionsQueueName($cloud_config));
  }

}
