<?php

namespace Drupal\aws_cloud\Access;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Entity\CloudConfigInterface;
use Symfony\Component\Routing\Route;

/**
 * Interface AwsCloudAccessInterface for access control.
 *
 * @package Drupal\aws_cloud\Controller
 */
interface AwsCloudAccessInterface {

  /**
   * IAM permissions database table name.
   *
   * @var string
   */
  public const IAM_PERMISSIONS_DB_TABLE = 'cloud_config_iam_permissions';

  /**
   * Checks user access by both Drupal and IAM permissions.
   *
   * @param string $cloud_context
   *   Cloud context to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface;

  /**
   * Checks if a certain IAM permission exists.
   *
   * @param string $cloud_context
   *   Cloud context to check.
   * @param string $perm
   *   Permission to check.
   * @param string $operation
   *   IAM operation to check.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function hasPermission(string $cloud_context, string $perm, string $operation): AccessResultInterface;

  /**
   * Update permissions from Ec2 and Iam.
   *
   * @param string $cloud_context
   *   The cloud context to update.
   */
  public function updatePermissions(string $cloud_context): void;

  /**
   * Delete IAM permissions.
   *
   * @param string $cloud_context
   *   Cloud context to delete.
   * @param string $perm
   *   Optional permission variable.  If passed, only delete the
   *   specific permission.
   */
  public function deletePermissions(string $cloud_context, $perm = ''): void;

  /**
   * Insert a IAM permission record into the database.
   *
   * @param string $cloud_context
   *   Cloud context for the permission.
   * @param string $op
   *   IAM operation.
   * @param string $perm
   *   Drupal's permission.
   */
  public function insertPermission(string $cloud_context, string $op, string $perm): void;

  /**
   * Create queue items for update resources queue.
   *
   * @param \Drupal\cloud\Entity\CloudConfigInterface $cloud_config
   *   The Cloud Config entity.
   */
  public function createResourceQueueItems(CloudConfigInterface $cloud_config): void;

}
