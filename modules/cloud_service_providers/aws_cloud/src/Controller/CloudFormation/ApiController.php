<?php

namespace Drupal\aws_cloud\Controller\CloudFormation;

use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Render\RendererInterface;
use Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceInterface;
use Drupal\cloud\Controller\CloudApiControllerBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller responsible for "update" URLs.
 *
 * This class is mainly responsible for
 * updating the aws entities from URLs.
 */
class ApiController extends CloudApiControllerBase implements ApiControllerInterface {

  /**
   * The CloudFormationService.
   *
   * @var \Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceInterface
   */
  private $cfnService;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Cloud service interface.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * ApiController constructor.
   *
   * @param \Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceInterface $cfn_service
   *   Object for interfacing with AWS API.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger Object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   Cloud service.
   */
  public function __construct(
    CloudFormationServiceInterface $cfn_service,
    Messenger $messenger,
    RequestStack $request_stack,
    RendererInterface $renderer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    CloudServiceInterface $cloud_service,
  ) {
    $this->cfnService = $cfn_service;
    $this->messenger = $messenger;
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->cloudService = $cloud_service;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return ApiController
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.cloud_formation'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('cloud')
    );
  }

  /**
   * Update message after refresh resources.
   *
   * @param string $resource_type
   *   Entity type name.
   * @param bool $updated
   *   Updated statuses.
   */
  protected function updateMessages($resource_type, bool $updated): void {
    $labels = $this->getDisplayLabels($resource_type);
    $updated
      ? $this->messenger->addStatus($this->t('Updated @resource_name.', ['@resource_name' => $labels['plural']]))
      : $this->messenger->addError($this->t('Unable to update @resource_name.', ['@resource_name' => $labels['plural']]));

    $this->cloudService->invalidateCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function updateStackList($cloud_context): RedirectResponse {

    $this->cfnService->setCloudContext($cloud_context);
    $updated = $this->cfnService->updateStacks();

    $this->updateMessages('aws_cloud_stack', $updated);

    return $this->redirect('view.aws_cloud_stack.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAllStackList(): RedirectResponse {
    return $this->cfnService->updateAllResourceList('aws_cloud_stack');
  }

}
