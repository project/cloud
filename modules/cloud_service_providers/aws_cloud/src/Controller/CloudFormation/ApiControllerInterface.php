<?php

namespace Drupal\aws_cloud\Controller\CloudFormation;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * {@inheritdoc}
 */
interface ApiControllerInterface {

  /**
   * Update all stacks in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateStackList($cloud_context): RedirectResponse;

  /**
   * Update all stacks of all cloud region.
   */
  public function updateAllStackList(): RedirectResponse;

}
