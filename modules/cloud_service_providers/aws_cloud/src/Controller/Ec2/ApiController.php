<?php

namespace Drupal\aws_cloud\Controller\Ec2;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Link;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Entity\Ec2\ElasticIp;
use Drupal\aws_cloud\Entity\Ec2\InstanceInterface;
use Drupal\aws_cloud\Entity\Ec2\NetworkInterface;
use Drupal\aws_cloud\Entity\Vpc\TransitGateway;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\CloudWatch\CloudWatchServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Traits\AwsCloudEntityCheckTrait;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;
use Drupal\cloud\Controller\CloudApiControllerBase;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Entity\IntermediateFormState;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Traits\CloudResourceTrait;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Route;

/**
 * Controller responsible for "update" URLs.
 *
 * This class is mainly responsible for
 * updating the AWS entities from URLs.
 */
class ApiController extends CloudApiControllerBase implements ApiControllerInterface {

  use AwsCloudEntityCheckTrait;
  use CloudResourceTrait;
  use AwsCloudFormTrait;

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * The AWS Cloud EC2 service.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  private $ec2Service;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * The AWS Cloud CloudWatch service.
   *
   * @var \Drupal\aws_cloud\Service\CloudWatch\CloudWatchServiceInterface
   */
  private $cloudWatchService;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Cloud service interface.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The configuration data.
   *
   * @var array
   */
  protected $configuration = ['cloud_context' => ''];

  /**
   * ApiController constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   Object for interfacing with AWS API.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger Object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\aws_cloud\Service\CloudWatch\CloudWatchServiceInterface $cloud_watch_service
   *   The AWS Cloud CloudWatch service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   Cloud service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    Messenger $messenger,
    RequestStack $request_stack,
    RendererInterface $renderer,
    CloudWatchServiceInterface $cloud_watch_service,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    CloudServiceInterface $cloud_service,
    EntityTypeManagerInterface $entity_type_manager,
    AccountInterface $current_user,
  ) {

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
    $this->ec2Service = $ec2_service;
    $this->messenger = $messenger;
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->cloudWatchService = $cloud_watch_service;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->cloudService = $cloud_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return ApiController
   *   return created object.
   */
  public static function create(ContainerInterface $container): ApiController {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('aws_cloud.cloud_watch'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('cloud'),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Checks user access for a specific request based on the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   * @param string $entity_type_id
   *   The entity type.
   * @param string $entity_id
   *   The entity id.
   * @param string $command
   *   The command name.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route, string $entity_type_id, string $entity_id, string $command): AccessResultInterface {
    $entity = $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->load($entity_id);

    if (!empty($entity) && $entity instanceof EntityInterface) {
      return $entity->access($command, $account, TRUE);
    }
    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirects to the cloud_config collection.
   */
  public function updateAll(): RedirectResponse {
    $regions = $this->requestStack->getCurrentRequest()->query->get('regions');
    if ($regions === NULL) {
      $this->messageUser($this->t('No region specified'), 'error');
    }
    else {
      $regions_array = explode(',', $regions);

      foreach ($regions_array ?: [] as $region) {
        $entity = $this->entityTypeManager()->getStorage('cloud_config')
          ->loadByProperties(
            [
              'cloud_context' => $region,
            ]);
        if ($entity) {
          aws_cloud_update_ec2_resources(array_shift($entity));
        }
      }

      $this->messageUser($this->t('Creating cloud service provider was performed successfully.'));
      drupal_flush_all_caches();
    }
    return $this->redirect('entity.cloud_config.collection');
  }

  /**
   * Update message after refresh resources.
   *
   * @param string $resource_type
   *   Entity type name.
   * @param bool $updated
   *   Updated statuses.
   */
  protected function updateMessages(string $resource_type, bool $updated): void {
    $labels = $this->getDisplayLabels($resource_type);
    if ($updated) {
      $this->messageUser($this->t('Updated @resource_name.', ['@resource_name' => $labels['plural']]));
    }
    else {
      $this->messageUser($this->t('Unable to update @resource_name.', ['@resource_name' => $labels['plural']]), 'error');
    }
    $this->cloudService->invalidateCacheTags();
  }

  /**
   * Update all instances in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to instance list of particular cloud region.
   */
  public function updateInstanceList($cloud_context): RedirectResponse {
    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateInstances();

    $this->updateMessages('aws_cloud_instance', $updated);

    return $this->redirect('view.aws_cloud_instance.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Update all instances of all cloud region.
   */
  public function updateAllInstanceList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_instance');
  }

  /**
   * Update all images in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to all image list.
   */
  public function updateImageList($cloud_context): RedirectResponse {
    $cloud_config_entities = $this->entityTypeManager()->getStorage('cloud_config')->loadByProperties(
      ['cloud_context' => [$cloud_context]]
    );

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = !empty($cloud_config_entities)
      ? reset($cloud_config_entities)
      : CloudConfig::create([
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\aws_cloud\Service\Ec2\Ec2Service */
    $ec2Service = $this->ec2Service;
    $account_id = $ec2Service->getAccountId($cloud_config_entities);

    if ($account_id) {
      $this->ec2Service->setCloudContext($cloud_context);
      $updated = $this->ec2Service->updateImages([
        'Owners' => [
          $account_id,
        ],
      ], TRUE);

      $this->updateMessages('aws_cloud_image', $updated);
    }
    else {
      $message = $this->t('AWS User ID is not specified.');
      $account = $this->currentUser();
      if ($account->hasPermission('edit cloud service providers')) {
        $message = Link::createFromRoute($message, 'entity.cloud_config.edit_form', ['cloud_config' => $cloud_config->id()])->toString();
      }
      $this->messageUser($message, 'error');
    }

    return $this->redirect('view.aws_cloud_image.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Update all images of all cloud region.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to all image list.
   */
  public function updateAllImageList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_image');
  }

  /**
   * Update all security groups in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to all security group list.
   */
  public function updateSecurityGroupList($cloud_context): RedirectResponse {
    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateSecurityGroups();

    $this->updateMessages('aws_cloud_security_group', $updated);

    return $this->redirect('view.aws_cloud_security_group.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Update all security groups of all cloud region.
   */
  public function updateAllSecurityGroupList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_security_group');
  }

  /**
   * Update all network interfaces in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to network interface list of particular cloud region.
   */
  public function updateNetworkInterfaceList($cloud_context): RedirectResponse {
    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateNetworkInterfaces();

    $this->updateMessages('aws_cloud_network_interface', $updated);

    return $this->redirect('view.aws_cloud_network_interface.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Update all network interfaces of all cloud region.
   */
  public function updateAllNetworkInterfaceList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_network_interface');
  }

  /**
   * Update all elastic ips in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to elastic ip list of particular cloud region.
   */
  public function updateElasticIpList($cloud_context): RedirectResponse {

    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateElasticIps();

    if ($updated === TRUE) {
      // Also update network interfaces.
      $updated = $this->ec2Service->updateNetworkInterfaces();
      if ($updated !== FALSE) {
        $this->messageUser($this->t('Updated Elastic IPs and network interfaces.'));
      }
      else {
        $this->messageUser(
          $this->t('Unable to update network interfaces while updating Elastic IPs.'),
          'error');
      }
      $this->cloudService->invalidateCacheTags();
    }
    else {
      $this->messageUser($this->t('Unable to update Elastic IPs.'), 'error');
    }

    return $this->redirect('view.aws_cloud_elastic_ip.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Update all elastic ips of all cloud region.
   */
  public function updateAllElasticIpList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_elastic_ip');
  }

  /**
   * Update all key pairs in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to keypair list of particular cloud region.
   */
  public function updateKeyPairList($cloud_context): RedirectResponse {

    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateKeyPairs();

    $this->updateMessages('aws_cloud_key_pair', $updated);
    return $this->redirect('view.aws_cloud_key_pair.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Update all key pairs of all cloud region.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to all instance list.
   */
  public function updateAllKeyPairList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_key_pair');
  }

  /**
   * Update all volumes in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to volume list of particular cloud region.
   */
  public function updateVolumeList($cloud_context): RedirectResponse {
    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateVolumes();

    $this->updateMessages('aws_cloud_volume', $updated);

    return $this->redirect('view.aws_cloud_volume.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Update all volumes of all cloud region.
   */
  public function updateAllVolumeList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_volume');
  }

  /**
   * Update all snapshots in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to snapshot list of particular cloud region.
   */
  public function updateSnapshotList($cloud_context): RedirectResponse {

    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateSnapshots();

    $this->updateMessages('aws_cloud_snapshot', $updated);

    return $this->redirect('view.aws_cloud_snapshot.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Update all snapshots of all cloud region.
   */
  public function updateAllSnapshotList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_snapshot');
  }

  /**
   * {@inheritdoc}
   */
  public function listInstanceCallback(): Response {
    return $this->getViewResponse('aws_cloud_instance');
  }

  /**
   * {@inheritdoc}
   */
  public function listImageCallback(): Response {
    return $this->getViewResponse('aws_cloud_image');
  }

  /**
   * {@inheritdoc}
   */
  public function listSnapshotCallback(): Response {
    return $this->getViewResponse('aws_cloud_snapshot');
  }

  /**
   * {@inheritdoc}
   */
  public function listVolumeCallback(): Response {
    return $this->getViewResponse('aws_cloud_volume');
  }

  /**
   * {@inheritdoc}
   */
  public function searchImages($cloud_context): Response {
    $this->ec2Service->setCloudContext($cloud_context);
    $name = $this->requestStack->getCurrentRequest()->query->get('q');
    $result = $this->ec2Service->describeImages([
      'Filters' => [
        [
          'Name' => 'name',
          'Values' => [$name],
        ],
        [
          'Name' => 'state',
          'Values' => ['available'],
        ],
      ],
    ]);

    $limit = 50;
    $count = 0;
    $images = [];
    foreach ($result['Images'] ?: [] as $image) {
      $images[] = [
        'name' => $image['Name'],
        'id' => $image['Name'],
        'created' => strtotime($image['CreationDate']),
      ];
      if ($count++ >= $limit) {
        break;
      }
    }

    // Sort by latest.
    usort($images, static function ($a, $b) {
      return ($a['created'] < $b['created']) ? 1 : -1;
    });

    $response = new Response('[]');
    try {
      $response = new Response(json_encode($images, JSON_THROW_ON_ERROR));
    }
    catch (\JsonException $e) {
      $this->handleException($e);
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceMetrics($cloud_context, InstanceInterface $aws_cloud_instance): JsonResponse {
    $this->cloudWatchService->setCloudContext($cloud_context);
    $metric_names = [
      'cpu' => 'CPUUtilization',
      'network_in' => 'NetworkIn',
      'network_out' => 'NetworkOut',
      'disk_read' => 'DiskReadBytes',
      'disk_write' => 'DiskWriteBytes',
      'disk_read_operation' => 'DiskReadOps',
      'disk_write_operation' => 'DiskWriteOps',
    ];
    $queries = [];
    foreach ($metric_names as $key => $name) {
      $queries[] = [
        'Id' => $key,
        'MetricStat' => [
          'Metric' => [
            'Namespace' => 'AWS/EC2',
            'MetricName' => $name,
            'Dimensions' => [
              [
                'Name' => 'InstanceId',
                'Value' => $aws_cloud_instance->getInstanceId(),
              ],
            ],
          ],
          'Period' => 300,
          'Stat' => 'Average',
        ],
      ];
    }

    $result = $this->cloudWatchService->getMetricData([
      'StartTime' => strtotime('-1 days'),
      'EndTime' => strtotime('now'),
      'MetricDataQueries' => $queries,
    ]);

    $data = [];
    foreach (array_keys($metric_names) as $index => $key) {
      $timestamps = [];
      foreach ($result['MetricDataResults'][$index]['Timestamps'] as $timestamp) {
        $timestamps[] = $timestamp->__toString();
      }

      $data[$key] = [
        'timestamps' => $timestamps,
        'values' => $result['MetricDataResults'][$index]['Values'],
      ];

      if ($key === 'network_in' || $key === 'network_out') {
        // Convert Byte to MB.
        $data[$key]['values'] = array_map(static function ($value) {
          return $value / 1024 / 1024;
        }, $data[$key]['values']);
      }
    }

    return new JsonResponse($data);
  }

  /**
   * Helper method to get views output.
   *
   * @param string $view_id
   *   The ID of list view.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response of list view.
   */
  private function getViewResponse(string $view_id): Response {
    $view = Views::getView($view_id);

    // Set the display machine name.
    $view->setDisplay('list');

    // Render the view as html, and return it as a response object.
    $build = $view->executeDisplay();
    return new Response($this->renderer->render($build));
  }

  /**
   * Helper method to add messages for the end user.
   *
   * @param string $message
   *   The message.
   * @param string $type
   *   The message type: error or message.
   */
  private function messageUser(string $message, string $type = 'message'): void {
    switch ($type) {
      case 'error':
        $this->messenger->addError($message);
        break;

      case 'message':
        $this->messenger->addStatus($message);
        break;

      default:
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityCount(string $cloud_context, string $entity_type_id): JsonResponse {
    $params = !empty($cloud_context)
      ? ['cloud_context' => $cloud_context]
      : [];

    if (!empty($cloud_context)) {
      $this->configuration['cloud_context'] = $cloud_context;
    }
    $count = $this->getResourceCountWithAccessCheck(
      $entity_type_id,
      $params,
    );

    return new JsonResponse(['count' => $count]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsoleOutput(string $cloud_context, string $entity_id): JsonResponse {
    // Get entity data.
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity */
    $entity = $this->entityTypeManager
      ->getStorage('aws_cloud_instance')
      ->load($entity_id);
    $entity = $this->updateEntitySubmitForm(new FormState(), $entity)
      ? $entity : NULL;

    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The instance has already been deleted.',
      ], 404);
    }

    // Execute AWS API.
    $result = $this->awsCloudOperationsService->getConsoleOutput($entity);

    return !empty($result)
      ? new JsonResponse([
        'result' => 'OK',
        'log' => $result,
      ], 200)
      : new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
      ], 500);
  }

  /**
   * {@inheritdoc}
   */
  public function refreshEntity(string $cloud_context, string $entity_type_id, string $entity_id): JsonResponse {
    $entity = $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->load($entity_id);

    $this->ec2Service->setCloudContext($cloud_context);
    $this->ec2Service->updateSingleEntity($entity, FALSE, TRUE);

    return new JsonResponse(['result' => 'OK']);
  }

  /**
   * {@inheritdoc}
   */
  public function operateEntity(Request $request, string $cloud_context, string $entity_type_id, string $entity_id, string $command): JsonResponse {
    // Create an instance of the entity.
    // The process splits between adding a new entity
    // and when reading an existing entity.
    $entity = $command !== 'create'
      ? $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->load($entity_id)
      : $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->create([
          'cloud_context' => $cloud_context,
        ]);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The entity has already been deleted.',
      ], 404);
    }

    // Depending on the content of the process,
    // branching is performed with a switch statement.
    $form_state = new IntermediateFormState();
    $method_name = '';
    $parameters = [];
    try {
      switch ($command . '_' . $entity_type_id) {
        case 'associate_aws_cloud_elastic_ip':
          $form_state->setValue('resource_type', $request->get('resource_type', ''));
          $form_state->setValue('instance_id', $request->get('instance_id', '-1'));
          $form_state->setValue('instance_private_ip', $request->get('instance_private_ip', ''));
          $form_state->setValue('network_interface_id', $request->get('network_interface_id', '-1'));
          $form_state->setValue('network_private_ip', $request->get('network_private_ip', ''));
          $method_name = 'associateElasticIp';
          break;

        case 'associate_elastic_ip_aws_cloud_instance':
          $form_state->set('allocation_id', $request->get('allocation_id', ''));
          $form_state->set('network_interface_id', $request->get('network_interface_id', ''));

          $method_name = 'associateElasticIpForInstance';
          break;

        case 'create_aws_cloud_elastic_ip':
          /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setDomain($request->get('domain', ''));
          $entity->setNetworkBorderGroup($request->get('network_border_group', ''));
          $method_name = 'createElasticIp';
          break;

        case 'create_aws_cloud_image':
          /** @var \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->set('instance_id', $request->get('instance_id', ''));
          $entity->setDescription($request->get('description', ''));
          $entity->setLaunchPermissionAccountIds(json_decode($request->get('launch_permission_account_ids', '[]'), TRUE));
          $method_name = 'createImage';
          break;

        case 'create_aws_cloud_key_pair':
          /** @var \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity */
          $entity->set('key_pair_name', $request->get('key_pair_name', ''));
          $method_name = 'createKeyPair';
          break;

        case 'create_image_aws_cloud_instance':
          $form_state->set('image_name', $request->get('image_name', ''));
          $form_state->set('no_reboot', $request->get('no_reboot', 'false') === 'true');

          $method_name = 'createImageFromInstance';
          break;

        case 'create_aws_cloud_security_group':
          /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
          $entity->setGroupName($request->get('group_name', ''));
          $entity->setDescription($request->get('description', ''));
          $vpc_id = $request->get('vpc_id', '');
          if (empty($vpc_id)) {
            $vpc_id = $entity->getVpcId();
          }
          $form_state->set('vpc_id', $vpc_id);
          $method_name = 'createSecurityGroup';
          break;

        case 'create_aws_cloud_carrier_gateway':
          /** @var \Drupal\aws_cloud\Entity\Vpc\CarrierGateway $entity */
          $vpc_id = $request->get('vpc_id', '');
          $form_state->set('vpc_id', $vpc_id);
          $entity->setName($request->get('name', ''));
          $entity->setVpcId($vpc_id);
          $method_name = 'createCarrierGateway';
          break;

        case 'create_aws_cloud_transit_gateway':
          /** @var \Drupal\aws_cloud\Entity\Vpc\TransitGatewayInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setDescription($request->get('description', ''));
          $entity->setAmazonSideAsn($request->get('amazon_side_asn', ''));
          // Use filter_var() to convert from string to boolean.
          $entity->setDnsSupport(filter_var($request->get('dns_support'), FILTER_VALIDATE_BOOLEAN));
          $entity->setVpnEcmpSupport(filter_var($request->get('vpn_ecmp_support', FALSE), FILTER_VALIDATE_BOOLEAN));
          $entity->setDefaultRouteTableAssociation(filter_var($request->get('default_route_table_association', FALSE), FILTER_VALIDATE_BOOLEAN));
          $entity->setDefaultRouteTablePropagation(filter_var($request->get('default_route_table_propagation', FALSE), FILTER_VALIDATE_BOOLEAN));
          $entity->setMulticastSupport(filter_var($request->get('multicast_support', FALSE), FILTER_VALIDATE_BOOLEAN));
          $entity->setAutoAcceptSharedAttachments(filter_var($request->get('auto_accept_shared_attachments', FALSE), FILTER_VALIDATE_BOOLEAN));
          $method_name = 'createTransitGateway';
          break;

        case 'edit_aws_cloud_transit_gateway':
          /** @var \Drupal\aws_cloud\Entity\Vpc\TransitGatewayInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setDescription($request->get('description', ''));
          $entity->setAmazonSideAsn($request->get('amazon_side_asn', ''));
          // Use filter_var() to convert from string to boolean.
          $entity->setDnsSupport(filter_var($request->get('dns_support'), FILTER_VALIDATE_BOOLEAN));
          $entity->setVpnEcmpSupport(filter_var($request->get('vpn_ecmp_support', FALSE), FILTER_VALIDATE_BOOLEAN));
          $entity->setDefaultRouteTableAssociation(filter_var($request->get('default_route_table_association', FALSE), FILTER_VALIDATE_BOOLEAN));
          $entity->setDefaultRouteTablePropagation(filter_var($request->get('default_route_table_propagation', FALSE), FILTER_VALIDATE_BOOLEAN));
          $entity->setMulticastSupport(filter_var($request->get('multicast_support', FALSE), FILTER_VALIDATE_BOOLEAN));
          $entity->setAutoAcceptSharedAttachments(filter_var($request->get('auto_accept_shared_attachments', FALSE), FILTER_VALIDATE_BOOLEAN));
          $entity->setAssociationDefaultRouteTableId($request->get('association_default_route_table_id', ''));
          $entity->setPropagationDefaultRouteTableId($request->get('propagation_default_route_table_id', ''));
          $entity->set('tags', json_decode($request->get('tags', '[]'), TRUE));
          $method_name = 'editTransitGateway';
          break;

        case 'create_aws_cloud_vpc':
          /** @var \Drupal\aws_cloud\Entity\Vpc\VpcInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setCidrBlock($request->get('cidr_block', ''));
          $entity->setInstanceTenancy($request->get('instance_tenancy', 'default'));
          $entity->setFlowLog(filter_var($request->get('flow_log', FALSE), FILTER_VALIDATE_BOOLEAN));
          $form_state->setValue('amazon_provided_ipv6_cidr_block', (int) $request->get('amazon_provided_ipv6_cidr_block', 0));
          $method_name = 'createVpc';
          break;

        case 'edit_aws_cloud_vpc':
          /** @var \Drupal\aws_cloud\Entity\Vpc\VpcInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setVpcId($request->get('vpc_id', ''));
          $entity->setFlowLog(filter_var($request->get('flow_log', FALSE), FILTER_VALIDATE_BOOLEAN));
          $entity->set('tags', json_decode($request->get('tags', '[]'), TRUE));
          $entity->setCidrBlocks(json_decode($request->get('cidr_blocks', '[]'), TRUE));
          $ipv6_cidr_blocks = json_decode($request->get('ipv6_cidr_blocks', '[]'));
          // Unset any empty cidr objects.  A cidr object is considered
          // empty if it does not have a cidr value.
          foreach ($ipv6_cidr_blocks as $key => $ipv6) {
            if (empty($ipv6->cidr)) {
              unset($ipv6_cidr_blocks[$key]);
            }
          }
          $entity->setIpv6CidrBlocks($ipv6_cidr_blocks);
          $method_name = 'editVpc';
          break;

        case 'create_aws_cloud_subnet':
          /** @var \Drupal\aws_cloud\Entity\Vpc\SubnetInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setVpcId($request->get('vpc_id', ''));
          $entity->setAvailabilityZone($request->get('availability_zone', ''));
          $entity->setCidrBlock($request->get('cidr_block', ''));
          $method_name = 'createSubnet';
          break;

        case 'edit_aws_cloud_subnet':
          /** @var \Drupal\aws_cloud\Entity\Vpc\SubnetInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setVpcId($request->get('vpc_id', ''));
          $entity->setAvailabilityZone($request->get('availability_zone', ''));
          $entity->setCidrBlock($request->get('cidr_block', ''));
          $entity->setState($request->get('state', ''));
          $entity->setAccountId($request->get('account_id', ''));
          $entity->setTags(json_decode($request->get('tags', '[]'), TRUE));
          $method_name = 'editSubnet';
          break;

        case 'create_aws_cloud_internet_gateway':
          /** @var \Drupal\aws_cloud\Entity\Vpc\InternetGatewayInterface $entity */
          $entity->setName($request->get('name', ''));
          $method_name = 'createInternetGateway';
          break;

        case 'create_aws_cloud_network_interface':
          /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterfaceInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setDescription($request->get('description', ''));
          $method_name = 'createNetworkInterface';
          break;

        case 'create_aws_cloud_vpc_peering_connection':
          /** @var \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection $entity */
          $entity->setName($request->get('name', ''));
          $entity->setRequesterVpcId($request->get('requester_vpc_id', ''));
          $form_state->setValue('requester_vpc_id', $request->get('requester_vpc_id', ''));

          // The `accepter_vpc_id` can be set in either `accepter_vpc_id` or
          // `accepter_vpc_id_text`. `accepter_vpc_id_text` is used when
          // trying to set up peering connection between vpcs in different
          // regions.
          $accepter_vpc_id = !empty($request->get('accepter_vpc_id', '')) ?
            $request->get('accepter_vpc_id', '') :
            $request->get('accepter_vpc_id_text', '');

          $entity->setAccepterVpcId($accepter_vpc_id);
          $entity->setAccepterAccountId($request->get('accepter_account_id', ''));
          $entity->setAccepterRegion($request->get('accepter_region', ''));

          $form_state->setValue('accepter_vpc_id', $accepter_vpc_id);
          $form_state->setValue('accepter_account_id', $request->get('accepter_account_id', ''));
          $form_state->setValue('accepter_region', $request->get('accepter_region', ''));

          $method_name = 'createVpcPeeringConnection';
          break;

        case 'edit_aws_cloud_vpc_peering_connection':
          /** @var \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection $entity */
          $entity->setName($request->get('name', ''));
          $entity->setVpcPeeringConnectionId($request->get('vpc_peering_connection_id', ''));
          $entity->setStatusCode($request->get('status_code', ''));
          $entity->setStatusMessage($request->get('status_message', ''));
          $entity->setRequesterVpcId($request->get('requester_vpc_id', ''));
          $entity->setRequesterCidrBlock($request->get('requester_cidr_block', ''));
          $entity->setRequesterAccountId($request->get('requester_account_id', ''));
          $entity->setRequesterRegion($request->get('requester_region', ''));
          $entity->setAccepterVpcId($request->get('accepter_vpc_id', ''));
          $entity->setAccepterCidrBlock($request->get('accepter_cidr_block', ''));
          $entity->setAccepterAccountId($request->get('accepter_account_id', ''));
          $entity->setAccepterRegion($request->get('accepter_region', ''));
          $entity->setTags(json_decode($request->get('tags', '[]'), TRUE));
          $method_name = 'editVpcPeeringConnection';
          break;

        case 'create_aws_cloud_snapshot':
          /** @var \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->set('volume_id', $request->get('volume_id', ''));
          $entity->set('description', $request->get('description', ''));
          $method_name = 'createSnapshot';
          break;

        case 'create_aws_cloud_volume':
          /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setSnapshotId($request->get('snapshot_id', ''));
          $entity->setSize($request->get('size', ''));
          $entity->setVolumeType($request->get('volume_type', ''));
          $entity->setIops($request->get('iop', ''));
          $entity->set('availability_zone', $request->get('availability_zone', ''));
          $entity->set('kms_key_id', $request->get('kms_key_id', ''));
          $entity->set('encrypted', $request->get('encrypted', ''));
          $method_name = 'createVolume';
          break;

        case 'delete_aws_cloud_carrier_gateway':
          $method_name = 'deleteCarrierGateway';
          break;

        case 'delete_aws_cloud_elastic_ip':
          $method_name = 'deleteElasticIp';
          break;

        case 'delete_aws_cloud_image':
          $method_name = 'deleteImage';
          break;

        case 'delete_aws_cloud_instance':
          $method_name = 'deleteInstance';
          break;

        case 'delete_aws_cloud_internet_gateway':
          $method_name = 'deleteInternetGateway';
          break;

        case 'delete_aws_cloud_key_pair':
          $method_name = 'deleteKeyPair';
          break;

        case 'delete_aws_cloud_network_interface':
          $method_name = 'deleteNetworkInterface';
          break;

        case 'delete_aws_cloud_snapshot':
          $method_name = 'deleteSnapshot';
          break;

        case 'delete_aws_cloud_subnet':
          $method_name = 'deleteSubnet';
          break;

        case 'delete_aws_cloud_transit_gateway':
          $method_name = 'deleteTransitGateway';
          break;

        case 'delete_aws_cloud_volume':
          $method_name = 'deleteVolume';
          break;

        case 'delete_aws_cloud_vpc':
          $method_name = 'deleteVpc';
          break;

        case 'delete_aws_cloud_vpc_peering_connection':
          $method_name = 'deleteVpcPeeringConnection';
          break;

        case 'disassociate_aws_cloud_elastic_ip':
          $method_name = 'disassociateElasticIp';
          break;

        case 'delete_aws_cloud_security_group':
          $method_name = 'deleteSecurityGroup';
          break;

        case 'edit_aws_cloud_elastic_ip':
          /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
          $entity->setName($request->get('name', ''));
          $method_name = 'editElasticIp';
          break;

        case 'edit_aws_cloud_image':
          /** @var \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setDescription($request->get('description', ''));
          $entity->setVisibility(!empty($request->get('visibility', '')) ? 'public' : 'private');
          $entity->setLaunchPermissionAccountIds(json_decode($request->get('launch_permission_account_ids', '[]'), TRUE));

          $method_name = 'editImage';
          break;

        case 'edit_aws_cloud_instance':
          /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setIamRole($request->get('iam_role', ''));
          $form_state->set('security_groups', json_decode($request->get('security_groups', '[]'), TRUE));
          $form_state->set('tags', json_decode($request->get('tags', '[]'), TRUE));
          $form_state->set('termination_timestamp', $request->get('termination_timestamp', NULL));
          $entity->setUserData($request->get('user_data', NULL));
          $entity->setTerminationProtection(json_decode($request->get('termination_protection', '"false"'), TRUE));
          $form_state->set('user_data_base64_encoded', json_decode($request->get('user_data_base64_encoded', '"false"'), TRUE));
          $entity->setSchedule($request->get('schedule', ''));
          $form_state->set('add_new_elastic_ip', $request->get('add_new_elastic_ip', ''));
          $form_state->set('current_allocation_id', $request->get('current_allocation_id', ''));
          $form_state->set('current_association_id', $request->get('current_association_id', ''));

          $method_name = 'editInstance';
          break;

        case 'edit_aws_cloud_key_pair':
          $method_name = 'editKeyPair';
          break;

        case 'edit_aws_cloud_network_interface':
          /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterfaceInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setDescription($request->get('description', ''));
          $method_name = 'editNetworkInterface';
          break;

        case 'edit_aws_cloud_security_group':
          /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
          $entity->setGroupName($request->get('group_name', ''));
          $entity->set('ip_permission', json_decode($request->get('ip_permission', '[]'), TRUE));
          $entity->set('outbound_permission', json_decode($request->get('outbound_permission', '[]'), TRUE));
          $method_name = 'editSecurityGroup';
          break;

        case 'edit_aws_cloud_snapshot':
          /** @var \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setEncrypted(empty(filter_var($request->get('encrypted_value', FALSE), FILTER_VALIDATE_BOOLEAN)) ? 'Not Encrypted' : 'Encrypted');
          $method_name = 'editSnapshot';
          break;

        case 'edit_aws_cloud_volume':
          /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity */
          $entity->setName($request->get('name', ''));
          $entity->setSize($request->get('size', ''));
          $entity->setVolumeType($request->get('volume_type', ''));
          $entity->setIops($request->get('iop', ''));
          $method_name = 'editVolume';
          break;

        case 'import_aws_cloud_key_pair':
          /** @var \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity */
          $entity->set('key_pair_name', $request->get('key_pair_name', ''));
          /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
          $file = $request->files->get('key_pair_public_key');
          if (empty($file)) {
            return new JsonResponse([
              'result' => 'NG',
              'reason' => 'Internal Server Error',
            ], 500);
          }
          $form_state->setValue('key_pair_public_key', $file->getRealPath());
          $method_name = 'importKeyPair';
          break;

        case 'reboot_aws_cloud_instance':
          $method_name = 'rebootInstance';
          break;

        case 'revoke_aws_cloud_security_group':
          $parameters = [
            'type' => $request->get('type', ''),
            'position' => $request->get('position', ''),
          ];
          $method_name = 'revokeSecurityGroup';
          break;

        case 'start_aws_cloud_instance':
          $method_name = 'startInstance';
          break;

        case 'stop_aws_cloud_instance':
          $method_name = 'stopInstance';
          break;

        case 'attach_aws_cloud_volume':
          $form_state->setValue('device_name', $request->get('device_name', ''));
          $form_state->setValue('instance_id', $request->get('instance_id', ''));
          $method_name = 'attachVolume';
          break;

        case 'attach_aws_cloud_internet_gateway':
          $form_state->setValue('vpc_id', $request->get('vpc_id', ''));
          $method_name = 'attachInternetGateway';
          break;

        case 'detach_aws_cloud_volume':
          $method_name = 'detachVolume';
          break;

        case 'detach_aws_cloud_internet_gateway':
          $method_name = 'detachInternetGateway';
          break;

        case 'edit_aws_cloud_carrier_gateway':
          /** @var \Drupal\aws_cloud\Entity\Vpc\CarrierGateway $entity */
          $vpc_id = $request->get('vpc_id', '');
          $form_state->set('vpc_id', $vpc_id);
          $entity->setName($request->get('name', ''));
          $entity->setCarrierGatewayId($request->get('carrier_gateway_id', ''));
          $entity->setState($request->get('state', ''));
          $entity->setVpcId($vpc_id);
          $entity->set('tags', json_decode($request->get('tags', '[]'), TRUE));
          $method_name = 'editCarrierGateway';
          break;

        case 'edit_aws_cloud_internet_gateway':
          /** @var \Drupal\aws_cloud\Entity\Vpc\InternetGateway $entity */
          $vpc_id = $request->get('vpc_id', '');
          $form_state->set('vpc_id', $vpc_id);
          $entity->setName($request->get('name', ''));
          $entity->setInternetGatewayId($request->get('internet_gateway_id', ''));
          $entity->setState($request->get('state', ''));
          $entity->setVpcId($vpc_id);
          $entity->set('tags', json_decode($request->get('tags', '[]'), TRUE));
          $method_name = 'editInternetGateway';
          break;

        case 'create_aws_cloud_cloud_launch_template':
          /** @var \Drupal\cloud\Entity\CloudLaunchTemplate $entity */
          $entity->set('name', $request->get('name', ''));
          $entity->set('field_description', $request->get('description', ''));
          $entity->set('field_instance_type', $request->get('field_instance_type', ''));
          $entity->set('field_min_count', (int) $request->get('field_min_count', 0));
          $entity->set('field_max_count', (int) $request->get('field_max_count', 0));
          $entity->set('field_test_only', json_decode($request->get('field_test_only', '"false"'), FALSE));
          $entity->set('field_image_id', $request->get('field_image_id', ''));
          $entity->set('field_kernel_id', $request->get('field_kernel_id', ''));
          $entity->set('field_ram', $request->get('field_ram', ''));
          $entity->set('field_availability_zone', $request->get('field_availability_zone', ''));
          $entity->set('field_vpc', $request->get('field_vpc', ''));
          $entity->set('field_subnet', $request->get('field_subnet', ''));
          $entity->set('field_security_group', json_decode($request->get('security_groups', '[]'), TRUE));
          $entity->set('field_ssh_key', $request->get('ssh_key', ''));
          $entity->set('field_network', $request->get('network_interface_id', ''));
          $entity->set('field_tags', json_decode($request->get('field_tags', '[]'), TRUE));
          $entity->set('field_workflow_status', $request->get('field_workflow_status', ''));
          $entity->set('field_instance_shutdown_behavior', $request->get('field_instance_shutdown_behavior', ''));
          $form_state->set('termination_protection', json_decode($request->get('termination_protection', '"false"'), TRUE));
          $entity->set('field_monitoring', json_decode($request->get('field_monitoring', '"false"'), TRUE));
          $form_state->set('schedule', $request->get('field_schedule', ''));
          $entity->set('field_user_data', $request->get('field_user_data', ''));
          $entity->set('revision_log_message', $request->get('revision_log_message', ''));

          $method_name = 'createCloudLaunchTemplate';
          break;

        case 'copy_aws_cloud_cloud_launch_template':
          /** @var \Drupal\cloud\Entity\CloudLaunchTemplate $entity */
          $entity->set('name', $request->get('name', ''));
          $entity->set('description', $request->get('field_description', ''));
          $entity->set('instance_type', $request->get('field_instance_type', ''));
          $entity->set('iam_role', $request->get('field_iam_role', ''));
          $entity->set('min_count', $request->get('field_min_count', ''));
          $entity->set('max_count', $request->get('field_max_count', ''));
          $entity->set('test_only', json_decode($request->get('field_test_only', '"false"'), FALSE));
          $entity->set('image_id', $request->get('field_image_id', ''));
          $entity->set('kernel_id', $request->get('field_kernel_id', ''));
          $entity->set('ram_id', $request->get('field_ram', ''));
          $entity->set('availability_zone', $request->get('field_availability_zone', ''));
          $entity->set('vpc', $request->get('field_vpc', ''));
          $entity->set('subnet', $request->get('field_subnet', ''));
          $entity->set('security_groups', $request->get(json_decode('security_groups', '[]'), []));
          $entity->set('ssh_key', $request->get('ssh_key', ''));
          $entity->set('network_interface_id', $request->get('network_interface_id', ''));
          $entity->set('tags', json_decode($request->get('tags', '[]'), TRUE));
          $entity->set('revision_log_message', $request->get('revision_log_message', ''));
          $method_name = 'copyCloudLaunchTemplate';
          break;

        case 'edit_cloud_launch_template':
          /** @var \Drupal\cloud\Entity\CloudLaunchTemplate $entity */
          $entity->set('field_description', $request->get('field_description', ''));
          $entity->set('field_instance_type', $request->get('field_instance_type', ''));
          $entity->set('field_iam_role', $request->get('field_iam_role', ''));
          $entity->set('field_min_count', $request->get('field_min_count', ''));
          $entity->set('field_max_count', $request->get('field_max_count', ''));
          $entity->set('field_test_only', json_decode($request->get('field_test_only', '"false"'), FALSE));
          $entity->set('field_image_id', $request->get('field_image_id', ''));
          $entity->set('field_kernel_id', $request->get('field_kernel_id', ''));
          $entity->set('field_ram', $request->get('field_ram', ''));
          $entity->set('field_availability_zone', $request->get('field_availability_zone', ''));
          $entity->set('field_vpc', $request->get('field_vpc', ''));
          $entity->set('field_subnet', $request->get('field_subnet', ''));
          $entity->set('field_security_group', json_decode($request->get('security_groups', '[]'), TRUE));
          $entity->set('field_ssh_key', $request->get('ssh_key', ''));
          $entity->set('field_network', $request->get('network_interface_id', ''));
          $entity->set('field_tags', json_decode($request->get('field_tags', '[]'), TRUE));
          $entity->set('field_workflow_status', $request->get('field_workflow_status', ''));
          $entity->set('field_instance_shutdown_behavior', $request->get('field_instance_shutdown_behavior', ''));
          $entity->set('field_termination_protection', json_decode($request->get('field_termination_protection', '"false"'), FALSE));
          $entity->set('field_monitoring', json_decode($request->get('field_monitoring', '"false"'), FALSE));
          $entity->set('field_schedule', $request->get('field_schedule', ''));
          $entity->set('field_user_data', $request->get('field_user_data', ''));
          $entity->set('revision_log_message', $request->get('revision_log_message', ''));
          $method_name = 'editCloudLaunchTemplate';
          break;

        case 'review_aws_cloud_cloud_launch_template':
          $method_name = 'reviewCloudLaunchTemplate';
          break;
      }
    }
    catch (\JsonException $e) {
      $this->handleException($e);
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
      ], 500);
    }

    /** @var /Drupal\Core\Entity\ContentEntityBase $entity */
    $violations = $entity->validate();

    // Remove practically harmless violations.
    if ($violations->count() > 0 && $entity_type_id === 'cloud_launch_template' && ($command === 'edit' || $command === 'copy')) {
      foreach ($violations as $key => $violation) {
        if ($violation->getMessageTemplate() === 'This entity (%type: %id) cannot be referenced.') {
          $violations->remove($key);
        }
      }
    }

    if ($violations->count() > 0) {
      $violationList = [];
      foreach ($violations as $violation) {
        $fieldName = $violation->getParameters()['@field_name'];
        $violationList[$fieldName] = $violation->getMessage()->render();
      }
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The value entered in the form is invalid.',
        'violationList' => $violationList,
      ], 400);
    }

    // Execute the process.
    $result = NULL;
    $this->ec2Service->setCloudContext($cloud_context);
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    if (method_exists($this->awsCloudOperationsService, $method_name)) {
      $form = [];
      if (!empty($parameters)) {
        switch ($command . '_' . $entity_type_id) {
          case 'revoke_aws_cloud_security_group':
            /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
            $result = $this->awsCloudOperationsService->revokeSecurityGroup(
              $entity,
              $form,
              $form_state,
              $parameters['type'],
              $parameters['position']
            );
            break;
        }
      }
      else {
        $result = $this->awsCloudOperationsService->$method_name(
          $entity,
          $form,
          $form_state
        );
      }
    }
    $this->messenger()->deleteAll();

    return !empty($result)
      ? new JsonResponse([
        'result' => 'OK',
        'id' => $entity->id(),
      ], 200)
      : new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
      ], 500);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceTypes(string $cloud_context): JsonResponse {
    $instance_type_dict = aws_cloud_get_instance_types($cloud_context);
    $output = [];
    foreach (array_keys($instance_type_dict) ?: [] as $key) {
      $output[] = [
        // Note: The dictionary type is expressed as Key-Value,
        // and the <option> tag in HTML is expressed by the value attribute
        // and children (labels).
        'value' => $key,
        // Use $key instead of $value because $value in return value of
        // array type of aws_cloud_get_instance_types() is difficult to
        // use as a label.
        'label' => $key,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getIamRoleOptions(string $cloud_context): JsonResponse {
    $iam_role_dict = aws_cloud_get_iam_roles($cloud_context);
    $output = [];
    foreach ($iam_role_dict ?: [] as $key => $value) {
      $output[] = [
        'value' => $key,
        'label' => $value,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroupsOptions(string $entity_id): JsonResponse {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entityTypeManager
      ->getStorage('aws_cloud_instance')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The instance has already been deleted.',
      ], 404);
    }

    $groups = $this->entityTypeManager->getStorage('aws_cloud_security_group')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'vpc_id' => $entity->getVpcId(),
      ]);

    $options = [];
    array_walk($groups, static function ($group) use (&$options) {
      $options[$group->getGroupName()] = $group->getGroupName();
    });

    natcasesort($options);

    $output = [];
    foreach ($options ?: [] as $key => $value) {
      $output[] = [
        'value' => $key,
        'label' => $value,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getScheduleOptions(): JsonResponse {
    $schedule_dict = aws_cloud_get_schedule();
    $output = [];
    foreach ($schedule_dict ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableElasticIps(string $cloud_context): JsonResponse {
    $elastic_ips = [];
    $results = $this->entityTypeManager
      ->getStorage('aws_cloud_elastic_ip')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $cloud_context)
      ->notExists('association_id')
      ->execute();

    foreach ($results ?: [] as $result) {
      /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIp $elastic_ip */
      $elastic_ip = ElasticIp::load($result);
      $elastic_ips[$elastic_ip->getAllocationId()] = $elastic_ip->getPublicIp();
    }

    $output = [];
    foreach ($elastic_ips ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailablePrivateIps(string $entity_id): JsonResponse {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entityTypeManager
      ->getStorage('aws_cloud_instance')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The instance has already been deleted.',
      ], 404);
    }

    $private_ips = [];
    $results = $this->entityTypeManager
      ->getStorage('aws_cloud_network_interface')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('instance_id', $entity->getInstanceId())
      ->condition('cloud_context', $entity->getCloudContext())
      ->notExists('association_id')
      ->execute();
    foreach ($results ?: [] as $result) {
      $network_interface = NetworkInterface::load($result);
      if (empty($network_interface)) {
        continue;
      }
      $private_ips[$network_interface->getNetworkInterfaceId()] = $network_interface->getPrimaryPrivateIp();
    }

    $output = [];
    foreach ($private_ips ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getVpcOptionData(string $cloud_context): JsonResponse {

    $vpcs = $this->getVpcOptions($cloud_context);

    $output = [];
    foreach ($vpcs ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkBorderGroupOptions(string $cloud_context): JsonResponse {
    $availability_zones = $this->entityTypeManager
      ->getStorage("aws_cloud_availability_zone")
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    // loadByProperties() must return some entities for Availability Zone;
    // however if the entities are empty, we try to get Availability Zones since
    // the entities are filled by aws_cloud_update_resources_queue.
    // The cost of describeAvailabilityZones() is 400-450 milliseconds.
    if (empty($availability_zones)) {
      // This is a trait however we assume $this->ec2Service is available.
      $this->ec2Service->updateAvailabilityZonesWithoutBatch();
      $availability_zones = $this->entityTypeManager
        ->getStorage("aws_cloud_availability_zone")
        ->loadByProperties([
          'cloud_context' => $cloud_context,
        ]);
    }

    $group_zones = [];
    foreach ($availability_zones ?: [] as $zone) {
      /** @var \Drupal\aws_cloud\Entity\Ec2\AvailabilityZoneInterface $zone */
      if (empty($zone->getNetworkBorderGroup())) {
        continue;
      }

      $group = $zone->getNetworkBorderGroup();
      if (empty($group_zones[$group])) {
        $group_zones[$group] = [];
      }
      $group_zones[$group][] = $zone->getZoneName();
    }

    $output = [];
    foreach ($group_zones as $group => $zones) {
      /** @var array $zones */
      $output[] = [
        'value' => $group,
        'label' => sprintf('%s (%s)', $group, implode(', ', $zones)),
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZones(string $cloud_context): JsonResponse {
    $zones = $this->getAvailabilityZoneOptions($cloud_context, 'aws_cloud', TRUE);
    $output = [];
    foreach ($zones ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }
    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubnetOptions($cloud_context): JsonResponse {
    /** @var \Drupal\aws_cloud\Entity\Vpc\SubnetInterface[] $subnets */
    $subnets = $this->entityTypeManager
      ->getStorage("aws_cloud_subnet")
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($subnets ?: [] as $subnet) {
      $options[$subnet->getSubnetId()] = sprintf('%s (%s | %s)',
        $subnet->getName(),
        $subnet->getSubnetId(),
        $subnet->getCidrBlock()
      );
    }

    natcasesort($options);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }
    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccountId(string $cloud_context): JsonResponse {
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    return new JsonResponse($cloud_config->field_account_id->value, Response::HTTP_OK);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultRegion(string $cloud_context): JsonResponse {
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    return new JsonResponse($cloud_config->field_region->value, Response::HTTP_OK);
  }

  /**
   * {@inheritdoc}
   */
  public function getRegions(string $cloud_context): JsonResponse {
    $this->ec2Service->setCloudContext($cloud_context);
    $regions = $this->ec2Service->getRegions();
    $output = [];
    foreach ($regions as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }
    return new JsonResponse($output, Response::HTTP_OK);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteTables(string $cloud_context, string $entity_id): JsonResponse {
    $entity = TransitGateway::load($entity_id);

    if (empty($entity)) {
      return new JsonResponse('', Response::HTTP_OK);
    }

    $tables = $this->entityTypeManager
      ->getStorage('aws_cloud_transit_gateway_route')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'transit_gateway_id' => $entity->getTransitGatewayId(),
      ]);

    $output = [];
    foreach ($tables ?: [] as $table) {
      $output[] = [
        'value' => $table->getTransitGatewayRouteTableId(),
        'label' => $table->getTransitGatewayRouteTableId(),
      ];
    }

    return new JsonResponse($output, Response::HTTP_OK);
  }

  /**
   * {@inheritdoc}
   */
  public function getAssociationDefaultRouteTableId(string $cloud_context, string $entity_id): JsonResponse {
    $entity = TransitGateway::load($entity_id);
    return new JsonResponse(empty($entity) ? '' : $entity->getAssociationDefaultRouteTableId(), Response::HTTP_OK);
  }

  /**
   * {@inheritdoc}
   */
  public function getPropagationDefaultRouteTableId(string $cloud_context, string $entity_id): JsonResponse {
    $entity = TransitGateway::load($entity_id);
    return new JsonResponse(empty($entity) ? '' : $entity->getPropagationDefaultRouteTableId(), Response::HTTP_OK);
  }

  /**
   * {@inheritdoc}
   */
  public function getUnassociatedInstanceOptions(string $entity_id): JsonResponse {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
    $entity = $this->entityTypeManager
      ->getStorage('aws_cloud_elastic_ip')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The Elastic IP has already been deleted.',
      ], 404);
    }

    // Get module name, network border group.
    $module_name = $this->getModuleName($entity);
    $network_border_group = $entity->getNetworkBorderGroup();

    $options = [];
    $account = $this->currentUser();

    $query = $this->entityTypeManager
      ->getStorage("{$module_name}_instance")
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $entity->getCloudContext());

    // Filter by network border group.
    if (!empty($network_border_group)) {
      $subnets = $this->entityTypeManager
        ->getStorage("{$module_name}_subnet")
        ->loadByProperties([
          'cloud_context' => $entity->getCloudContext(),
          'network_border_group' => $network_border_group,
        ]);
      $subnet_ids = array_map(static function ($subnet) {
        return $subnet->getSubnetId();
      }, $subnets);
      if (!empty($subnet_ids)) {
        $query->condition('subnet_id', $subnet_ids, 'IN');
      }
    }

    // Get cloud service provider name.
    // Use a static trait method through CloudService.
    $cloud_name = CloudService::convertUnderscoreToWhitespace($module_name);

    if (!$account->hasPermission("view any {$cloud_name} instance")) {
      $query->condition('uid', $account->id());
    }

    $results = $query->execute();

    foreach ($results ?: [] as $result) {

      $instance_list = $this->entityTypeManager
        ->getStorage("{$module_name}_instance")
        ->loadByProperties([
          'id' => $result,
          'cloud_context' => $entity->getCloudContext(),
        ]);

      /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
      $instance = count($instance_list) === 1
        ? array_shift($instance_list)
        : $instance_list;

      $private_ips = explode(', ', $instance->getPrivateIps());

      foreach ($private_ips ?: [] as $private_ip) {

        $elastic_ips = $this->entityTypeManager
          ->getStorage($entity->getEntityTypeId())
          ->loadbyProperties([
            'private_ip_address' => $private_ip,
            'cloud_context' => $entity->getCloudContext(),
          ]);

        if (count($elastic_ips) === 0) {
          $options[] = [
            'value' => $instance->id(),
            'label' => "{$instance->getName()} ({$instance->getInstanceId()}) - {$instance->getInstanceState()}",
          ];
        }
      }
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getUnassociatedNetworkInterfaceOptions(string $entity_id): JsonResponse {
    /** @var \Drupal\openstack\Entity\OpenStackFloatingIp $entity */
    $entity = $this->entityTypeManager
      ->getStorage('aws_cloud_elastic_ip')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The Elastic IP has already been deleted.',
      ], 404);
    }

    $options = [];

    // Get module name, network border group.
    $module_name = $this->getModuleName($entity);
    $network_border_group = $entity->getNetworkBorderGroup();

    $query = $this->entityTypeManager
      ->getStorage("{$module_name}_network_interface")
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $entity->getCloudContext());

    $group = $query
      ->orConditionGroup()
      ->notExists('association_id')
      ->notExists('secondary_association_id');

    $query->condition($group);

    // Filter by network border group.
    if (!empty($network_border_group)) {
      $subnets = $this->entityTypeManager
        ->getStorage("{$module_name}_subnet")
        ->loadByProperties([
          'cloud_context' => $entity->getCloudContext(),
          'network_border_group' => $network_border_group,
        ]);
      $subnet_ids = array_map(static function ($subnet) {
        return $subnet->getSubnetId();
      }, $subnets);
      if (!empty($subnet_ids)) {
        $query->condition('subnet_id', $subnet_ids, 'IN');
      }
    }

    $results = $query->execute();

    foreach ($results ?: [] as $result) {
      $interfaces = $this->entityTypeManager
        ->getStorage("{$module_name}_network_interface")
        ->loadByProperties([
          'id' => $result,
          'cloud_context' => $entity->getCloudContext(),
        ]);

      /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterface $interface */
      $interface = count($interfaces) === 1
        ? array_shift($interfaces)
        : $interfaces;

      if ($interface->getCloudContext() === $entity->getCloudContext()) {
        $options[] = [
          'value' => $interface->id(),
          'label' => "{$interface->getName()} - {$interface->getNetworkInterfaceId()}",
        ];
      }
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableNetworkInterfaceOptions(string $cloud_context): JsonResponse {
    $options = [];

    $module_name = 'aws_cloud';

    $query = $this->entityTypeManager
      ->getStorage("{$module_name}_network_interface")
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $cloud_context);

    $query->condition('status', 'available');

    $results = $query->execute();

    foreach ($results ?: [] as $result) {
      $interfaces = $this->entityTypeManager
        ->getStorage("{$module_name}_network_interface")
        ->loadByProperties([
          'id' => $result,
          'cloud_context' => $cloud_context,
        ]);

      /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterface $interface */
      $interface = count($interfaces) === 1
        ? array_shift($interfaces)
        : $interfaces;

      if ($interface->getCloudContext() === $cloud_context) {
        $options[] = [
          'value' => $interface->id(),
          'label' => "{$interface->getName()}",
        ];
      }
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstancePrivateIps(string $cloud_context, string $entity_id, string $instance_id): JsonResponse {
    if (empty($instance_id)) {
      return new JsonResponse([]);
    }
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
    $entity = $this->entityTypeManager
      ->getStorage('aws_cloud_elastic_ip')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The Elastic IP has already been deleted.',
      ], 404);
    }

    $options = [];
    $ips = $this->awsCloudOperationsService->getPrivateIps($entity, $instance_id, $cloud_context);
    foreach ($ips ?: [] as $value => $label) {
      $options[] = [
        'value' => $value,
        'label' => $label,
      ];
    }
    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkInterfacePrivateIps(string $entity_id, string $network_interface_id): JsonResponse {
    if (empty($network_interface_id)) {
      return new JsonResponse([]);
    }
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
    $entity = $this->entityTypeManager
      ->getStorage('aws_cloud_elastic_ip')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The Elastic IP has already been deleted.',
      ], 404);
    }

    $ips = $this->awsCloudOperationsService->getNetworkPrivateIps($entity, $network_interface_id);

    $options = [];
    foreach ($ips ?: [] as $value => $label) {
      $options[] = [
        'value' => $value,
        'label' => $label,
      ];
    }
    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getVolumeOptions($cloud_context): JsonResponse {
    $options = [];
    $params = [
      'cloud_context' => $cloud_context,
    ];

    if (!$this->currentUser->hasPermission("view any aws cloud volume")) {
      $params['uid'] = $this->currentUser->id();
    }

    $volumes = $this->entityTypeManager
      ->getStorage("aws_cloud_volume")
      ->loadByProperties($params);

    foreach ($volumes ?: [] as $volume) {
      $options[] = [
        'value' => $volume->getVolumeId(),
        'label' => $volume->getName() !== $volume->getVolumeId()
          ? "{$volume->getName()} ({$volume->getVolumeId()})"
          : $volume->getVolumeId(),
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getSnapshotOptions($cloud_context): JsonResponse {
    $options = [];
    $params = [
      'cloud_context' => $cloud_context,
    ];

    if (!$this->currentUser->hasPermission('view any aws cloud snapshot')) {
      $params['uid'] = $this->currentUser->id();
    }

    $snapshots = $this->entityTypeManager
      ->getStorage('aws_cloud_snapshot')
      ->loadByProperties($params);

    foreach ($snapshots ?: [] as $snapshot) {
      $options[] = [
        'value' => $snapshot->getSnapshotId(),
        'label' => $snapshot->getName() !== $snapshot->getSnapshotId()
          ? "{$snapshot->getName()} ({$snapshot->getSnapshotId()})"
          : $snapshot->getSnapshotId(),
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getVolumeTypes($cloud_context): JsonResponse {
    $dummy_options = [
      'gp2' => $this->t('General Purpose SSD (gp2)'),
      'gp3' => $this->t('General Purpose SSD (gp3)'),
      'io1' => $this->t('Provisioned IOPS SSD (io1)'),
      'io2' => $this->t('Provisioned IOPS SSD (io2)'),
      'sc1' => $this->t('Cold HDD (sc1)'),
      'st1' => $this->t('Throughput Optimized HDD (st1)'),
      'standard' => $this->t('Magnetic (standard)'),
    ];

    $options = [];
    foreach ($dummy_options ?: [] as $key => $option) {
      $options[] = [
        'value' => $key,
        'label' => $option,
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceOptions(string $cloud_context, string $entity_id): JsonResponse {
    /** @var \Drupal\openstack\Entity\OpenStackVolume $entity */
    $entity = $this->entityTypeManager
      ->getStorage('aws_cloud_volume')
      ->load($entity_id);

    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The volume has already been deleted.',
      ], 404);
    }

    $account = $this->currentUser();
    $properties = [
      'availability_zone' => $entity->getAvailabilityZone(),
      'cloud_context' => $cloud_context,
    ];

    if (!$account->hasPermission("view any aws cloud instance")) {
      $properties['uid'] = $account->id();
    }

    $results = $this->entityTypeManager
      ->getStorage('aws_cloud_instance')
      ->loadByProperties($properties);

    $options = [];
    foreach ($results ?: [] as $result) {
      $options[] = [
        'value' => $result->getInstanceId(),
        'label' => $this->t('@name - @instance_id', [
          '@name' => $result->getName(),
          '@instance_id' => $result->getInstanceId(),
        ]),
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getVpcs(string $cloud_context): JsonResponse {
    $vpcs = $this->getVpcOptions($cloud_context);
    natcasesort($vpcs);

    $gateways = $this->entityTypeManager
      ->getStorage('aws_cloud_internet_gateway')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($gateways as $gateway) {
      /** @var \Drupal\aws_cloud\Entity\Vpc\InternetGateway $gateway */
      if (empty($gateway->getVpcId())) {
        continue;
      }
      unset($vpcs[$gateway->getVpcId()]);
    }

    foreach ($vpcs ?: [] as $vpc_id => $name) {
      $options[] = [
        'value' => $vpc_id,
        'label' => $name,
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateNetworkOptionsAsJson($cloud_context): JsonResponse {
    /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterfaceInterface[] $networks */
    $networks = $this->entityTypeManager
      ->getStorage('aws_cloud_network')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($networks as $network) {
      $options[$network->id()] = $network->getName();
    }

    natcasesort($options);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateKeyPairOptionsAsJson($cloud_context): JsonResponse {
    /** @var \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface[] $keyPairs */
    $keyPairs = $this->entityTypeManager
      ->getStorage('aws_cloud_key_pair')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($keyPairs as $keyPair) {
      $options[$keyPair->id()] = $keyPair->getName();
    }

    natcasesort($options);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }
    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateSecurityGroupOptionsAsJson($cloud_context, $subnet_id = ''): JsonResponse {
    $vpc_id = NULL;
    if (!empty($subnet_id)) {
      /** @var \Drupal\aws_cloud\Entity\Vpc\SubnetInterface[] $subnets */
      $subnets = $this->entityTypeManager
        ->getStorage("aws_cloud_subnet")
        ->loadByProperties([
          'cloud_context' => $cloud_context,
          'subnet_id' => $subnet_id,
        ]);

      if (!empty($subnets)) {
        $vpc_id = array_shift($subnets)->getVpcId();
      }
    }

    $storage = $this->entityTypeManager
      ->getStorage("aws_cloud_security_group");

    $query = $storage
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $cloud_context);

    if ($vpc_id !== NULL) {
      $query = $query->condition('vpc_id', $vpc_id);
    }

    $entity_ids = $query->execute();

    $options = [];
    $security_groups = $storage->loadMultiple($entity_ids);
    foreach ($security_groups ?: [] as $security_group) {
      $options[$security_group->id()] = $security_group->getGroupName();
    }

    natcasesort($options);

    $output = array_map(static function ($value, $label) {
      return [
        'value' => $value,
        'label' => $label,
      ];
    }, array_keys($options ?: []), array_values($options ?: []));
    return new JsonResponse($output, 200);
  }

}
