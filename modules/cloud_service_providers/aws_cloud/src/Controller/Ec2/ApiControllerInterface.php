<?php

namespace Drupal\aws_cloud\Controller\Ec2;

use Drupal\aws_cloud\Entity\Ec2\InstanceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * {@inheritdoc}
 */
interface ApiControllerInterface {

  /**
   * Update all instances in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateInstanceList(string $cloud_context): RedirectResponse;

  /**
   * Update all images in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateImageList(string $cloud_context): RedirectResponse;

  /**
   * Update all security groups in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateSecurityGroupList(string $cloud_context): RedirectResponse;

  /**
   * Update all network interfaces in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateNetworkInterfaceList(string $cloud_context): RedirectResponse;

  /**
   * Update all Elastic IPs in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateElasticIpList(string $cloud_context): RedirectResponse;

  /**
   * Update all key pairs in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateKeyPairList(string $cloud_context): RedirectResponse;

  /**
   * Update all volumes in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateVolumeList(string $cloud_context): RedirectResponse;

  /**
   * Update all snapshots in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateSnapshotList(string $cloud_context): RedirectResponse;

  /**
   * Update all entities in a given region.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateAll(): RedirectResponse;

  /**
   * Search images.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A JSON response of images.
   */
  public function searchImages(string $cloud_context): Response;

  /**
   * Get instance metrics.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $aws_cloud_instance
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of an instance's metrics.
   */
  public function getInstanceMetrics(string $cloud_context, InstanceInterface $aws_cloud_instance): JsonResponse;

  /**
   * Get the count of AWS Cloud entities.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getEntityCount(string $cloud_context, string $entity_type_id): JsonResponse;

  /**
   * Get console output from AWS Cloud instance.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_id
   *   The entity ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getConsoleOutput(string $cloud_context, string $entity_id): JsonResponse;

  /**
   * Refresh status of AWS Cloud entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $entity_id
   *   The entity ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function refreshEntity(string $cloud_context, string $entity_type_id, string $entity_id): JsonResponse;

  /**
   * Operate OpenStack entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $entity_id
   *   The entity ID.
   * @param string $command
   *   Operation command.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function operateEntity(Request $request, string $cloud_context, string $entity_type_id, string $entity_id, string $command): JsonResponse;

  /**
   * Get instance type Options.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getInstanceTypes(string $cloud_context): JsonResponse;

  /**
   * Get IAM role Options.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getIamRoleOptions(string $cloud_context): JsonResponse;

  /**
   * Get Security groups options.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getSecurityGroupsOptions(string $entity_id): JsonResponse;

  /**
   * Get Schedule Options.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getScheduleOptions(): JsonResponse;

  /**
   * Get options for the select element network border group.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getNetworkBorderGroupOptions(string $cloud_context): JsonResponse;

  /**
   * Get Availability Zones.
   *
   * @param string $cloud_context
   *   The cloud context to query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getAvailabilityZones(string $cloud_context): JsonResponse;

  /**
   * Get select options of subnet.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getSubnetOptions($cloud_context): JsonResponse;

  /**
   * Get default account id.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getAccountId(string $cloud_context): JsonResponse;

  /**
   * Get all available regions.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getRegions(string $cloud_context): JsonResponse;

  /**
   * Get region based on cloud context.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getDefaultRegion(string $cloud_context): JsonResponse;

  /**
   * Helper function to get volume options.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getVolumeOptions($cloud_context): JsonResponse;

  /**
   * Helper function to get snapshot options.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSnapshotOptions($cloud_context): JsonResponse;

  /**
   * Helper function to get volume types.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getVolumeTypes($cloud_context): JsonResponse;

  /**
   * Helper function that loads all instances.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   * @param string $entity_id
   *   The entity id of the volume.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getInstanceOptions(string $cloud_context, string $entity_id): JsonResponse;

  /**
   * Helper function that loads route tables for transit gateways.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   * @param string $entity_id
   *   The entity id of the transit gateway.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getRouteTables(string $cloud_context, string $entity_id): JsonResponse;

  /**
   * Helper function that loads association default route tables ID.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   * @param string $entity_id
   *   The entity id of the transit gateway.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAssociationDefaultRouteTableId(string $cloud_context, string $entity_id): JsonResponse;

  /**
   * Helper function that loads propagation default route tables ID.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   * @param string $entity_id
   *   The entity id of the transit gateway.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPropagationDefaultRouteTableId(string $cloud_context, string $entity_id): JsonResponse;

  /**
   * Get vpcs based on cloud context.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getVpcs(string $cloud_context): JsonResponse;

  /**
   * Get select options of network.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getTemplateNetworkOptionsAsJson($cloud_context): JsonResponse;

  /**
   * Get select options of key pair.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getTemplateKeyPairOptionsAsJson($cloud_context): JsonResponse;

  /**
   * Get select options of security group.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $subnet_id
   *   The subnet ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getTemplateSecurityGroupOptionsAsJson($cloud_context, $subnet_id = ''): JsonResponse;

}
