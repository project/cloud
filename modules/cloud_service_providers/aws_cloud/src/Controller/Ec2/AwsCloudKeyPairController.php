<?php

namespace Drupal\aws_cloud\Controller\Ec2;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\Messenger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller responsible for AWS KeyPair.
 */
class AwsCloudKeyPairController extends ControllerBase {

  /**
   * ApiController constructor.
   *
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger Object.
   */
  public function __construct(Messenger $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return AwsCloudKeyPairController
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * Download Key.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param object $key_pair
   *   AWS Cloud KeyPair.
   * @param string $entity_type
   *   The entity type, such as cloud_launch_template.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\RedirectResponse
   *   A binary file response object or redirect if key file does not exist.
   */
  public function downloadKey($cloud_context, $key_pair, $entity_type = 'aws_cloud') {
    $key_pair = $this->entityTypeManager()->getStorage("{$entity_type}_key_pair")->load($key_pair);
    $key_material = $key_pair->getKeyMaterial();
    if (!empty($key_material)) {
      $key_pair->setKeyMaterial(NULL);
      $key_pair->save();
      return new Response($key_material, 200, [
        'Content-Type' => 'application/x-pem-file',
        'Content-Disposition' => sprintf('attachment; filename="%s.pem"', $key_pair->getKeyPairName()),
      ]);
    }
    else {
      // Just redirect to key pair listing page.
      return $this->redirect("view.{$entity_type}_key_pair.list", [
        'cloud_context' => $cloud_context,
      ]);
    }
  }

}
