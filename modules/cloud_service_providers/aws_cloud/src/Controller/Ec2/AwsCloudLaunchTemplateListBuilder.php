<?php

namespace Drupal\aws_cloud\Controller\Ec2;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\cloud\Controller\CloudLaunchTemplateListBuilder;
use Drupal\cloud\Service\Util\EntityLinkWithShortNameHtmlGenerator;

/**
 * Provides a list controller for CloudLaunchTemplate entity.
 *
 * @ingroup cloud_launch_template
 */
class AwsCloudLaunchTemplateListBuilder extends CloudLaunchTemplateListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $parent_header = parent::buildHeader();
    $header = [
      // The header gives the table the information it needs in order to make
      // the query calls for ordering. TableSort uses the field information
      // to know what database column to sort by.
      $parent_header[0],
      [
        'data' => $this->t('AMI name'),
        'specifier' => 'field_image_id',
        'field' => 'field_image_id',
      ],
      [
        'data' => $this->t('Instance type'),
        'specifier' => 'field_instance_type',
        'field' => 'field_instance_type',
      ],
      [
        'data' => $this->t('Security group'),
        'specifier' => 'field_security_group',
        'field' => 'field_security_group',
      ],
      [
        'data' => $this->t('Key pair'),
        'specifier' => 'field_ssh_key',
        'field' => 'field_ssh_key',
      ],
      [
        'data' => $this->t('VPC'),
        'specifier' => 'field_vpc',
        'field' => 'field_vpc',
      ],
      [
        'data' => $this->t('Max count'),
        'specifier' => 'field_max_count',
        'field' => 'field_max_count',
      ],
      [
        'data' => $this->t('Workflow status'),
        'specifier' => 'field_workflow_status',
        'field' => 'field_workflow_status',
      ],
    ];
    $header['operations'] = $parent_header['operations'];
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $parent_row = parent::buildRow($entity);

    $row[] = $parent_row['name'];

    // AMI image.
    // VPC.
    if ($entity->get('field_vpc')->value !== NULL) {
      $row[] = [
        'data' => $this->entityLinkRenderer->renderViewElement(
          $entity->get('field_image_id')->value,
          'aws_cloud_image',
          'image_id',
          [],
          '',
          EntityLinkWithShortNameHtmlGenerator::class
        ),
      ];
    }
    else {
      $row[] = '';
    }

    // Instance type.
    $instance_type = $entity->get('field_instance_type')->value;
    $row[] = [
      'data' => [
        '#type' => 'link',
        '#url' => Url::fromRoute(
          'aws_cloud.instance_type_prices',
          ['cloud_context' => $entity->getCloudContext()],
          ['fragment' => $instance_type]
        ),
        '#title' => $instance_type,
      ],
    ];

    // Security groups.
    $htmls = [];
    foreach ($entity->get('field_security_group') ?: [] as $group) {
      if ($group->entity !== NULL) {
        $group_id = $group->entity->getGroupId();
        $element = $this->entityLinkRenderer->renderViewElement(
          $group_id,
          'aws_cloud_security_group',
          'group_id',
          [],
          $group->entity->getName()
        );

        $htmls[] = $element['#markup'];
      }
      else {
        $htmls[] = '';
      }
    }
    $row[] = [
      'data' => ['#markup' => implode(', ', $htmls)],
    ];

    // SSH key.
    if ($entity->get('field_ssh_key')->entity !== NULL) {
      $row[] = [
        'data' => $this->entityLinkRenderer->renderViewElement(
          $entity->get('field_ssh_key')->entity->getKeyPairName(),
          'aws_cloud_key_pair',
          'key_pair_name'
        ),
      ];
    }
    else {
      $row[] = '';
    }

    // VPC.
    if ($entity->get('field_vpc')->value !== NULL) {
      $row[] = [
        'data' => $this->entityLinkRenderer->renderViewElement(
          $entity->get('field_vpc')->value,
          'aws_cloud_vpc',
          'vpc_id',
          [],
          '',
          EntityLinkWithShortNameHtmlGenerator::class
        ),
      ];
    }
    else {
      $row[] = '';
    }

    $row[] = $entity->get('field_max_count')->value;
    $row[] = $entity->get('field_workflow_status')->value;
    $row[] = $parent_row['operations'];

    return $row;
  }

}
