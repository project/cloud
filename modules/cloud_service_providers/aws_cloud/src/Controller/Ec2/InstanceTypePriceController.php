<?php

namespace Drupal\aws_cloud\Controller\Ec2;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\aws_cloud\Service\Pricing\InstanceTypePriceTableRenderer;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller responsible to show price list.
 */
class InstanceTypePriceController extends ControllerBase implements InstanceTypePriceControllerInterface {

  use CloudContentEntityTrait;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The route builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * Current route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRoute;

  /**
   * Current route.
   *
   * @var \Drupal\aws_cloud\Service\Pricing\InstanceTypePriceTableRenderer
   */
  protected $priceTableRenderer;

  /**
   * InstanceTypePriceController constructor.
   *
   * @param \Drupal\aws_cloud\Service\Pricing\InstanceTypePriceTableRenderer $price_table_renderer
   *   AWS Pricing service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route builder.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route
   *   The current route.
   */
  public function __construct(InstanceTypePriceTableRenderer $price_table_renderer, CloudConfigPluginManagerInterface $cloud_config_plugin_manager, RouteBuilderInterface $route_builder, RouteMatchInterface $current_route) {
    $this->priceTableRenderer = $price_table_renderer;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->routeBuilder = $route_builder;
    $this->currentRoute = $current_route;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The instance of ContainerInterface.
   *
   * @return InstanceTypePriceController
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.instance_type_price_table_renderer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('router.builder'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function show($cloud_context): array {
    $build = [];

    $build['table'] = $this->priceTableRenderer->render($cloud_context);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function updateAll(): RedirectResponse {
    $entities = $this->cloudConfigPluginManager->loadConfigEntities('aws_cloud');
    $this->messenger()->deleteAll();
    foreach ($entities ?: [] as $entity) {
      // Update any instance types if needed.
      aws_cloud_update_instance_types($entity);
      $this->messenger()->addStatus($this->t('Updated instance type prices for @name', [
        '@name' => $entity->getName(),
      ]));
    }
    $this->routeBuilder->rebuild();

    // Default to Front page if no redirect is passed.
    $redirect = '<front>';
    $params = [];

    $destination = Url::fromUserInput($this->getRedirectDestination()->get());
    if ($destination->isRouted() && $destination->getRouteName() !== $this->currentRoute->getRouteName()) {
      $redirect = $destination->getRouteName();
      try {
        $params = $destination->getRouteParameters();
      }
      catch (\Exception $e) {
        $this->handleException($e);
      }
    }
    return $this->redirect($redirect, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function update($cloud_context): RedirectResponse {
    // Load the cloud config.
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

    // Update instance types.
    if (!empty(aws_cloud_update_instance_types($cloud_config, TRUE))) {
      // Rebuild the route.
      $this->routeBuilder->rebuild();
    }

    // Redirect to the price list.
    return $this->redirect('aws_cloud.instance_type_prices', [
      'cloud_context' => $cloud_context,
    ]);
  }

}
