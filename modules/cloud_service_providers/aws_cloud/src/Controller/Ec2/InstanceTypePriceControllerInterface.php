<?php

namespace Drupal\aws_cloud\Controller\Ec2;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides an interface showing price list.
 */
interface InstanceTypePriceControllerInterface {

  /**
   * Show instance type prices list.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return array
   *   Returns the build array.
   */
  public function show($cloud_context): array;

  /**
   * Update instance type prices list.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function update($cloud_context): RedirectResponse;

  /**
   * Update all instance type price lists.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateAll(): RedirectResponse;

}
