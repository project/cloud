<?php

namespace Drupal\aws_cloud\Controller\Vpc;

use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Render\RendererInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Controller\CloudApiControllerBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller responsible for "update" URLs.
 *
 * This class is mainly responsible for
 * updating the aws entities from URLs.
 */
class ApiController extends CloudApiControllerBase implements ApiControllerInterface {

  /**
   * The Ec2Service.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  private $ec2Service;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Cloud service interface.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * ApiController constructor.
   *
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   Object for interfacing with AWS API.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger Object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   Cloud service.
   */
  public function __construct(
    Ec2ServiceInterface $ec2_service,
    Messenger $messenger,
    RequestStack $request_stack,
    RendererInterface $renderer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    CloudServiceInterface $cloud_service,
  ) {
    $this->ec2Service = $ec2_service;
    $this->messenger = $messenger;
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->cloudService = $cloud_service;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return ApiController
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.ec2'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('cloud')
    );
  }

  /**
   * Update message after refresh resources.
   *
   * @param string $resource_type
   *   Entity type name.
   * @param bool $updated
   *   Updated statuses.
   */
  protected function updateMessages($resource_type, bool $updated): void {
    $labels = $this->getDisplayLabels($resource_type);
    if ($updated) {
      $this->messenger->addStatus($this->t('Updated @resource_name.', ['@resource_name' => $labels['plural']]));
    }
    else {
      $this->messenger->addError($this->t('Unable to update @resource_name.', ['@resource_name' => $labels['plural']]));
    }
    $this->cloudService->invalidateCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function updateVpcList($cloud_context): RedirectResponse {

    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateVpcs();

    $this->updateMessages('aws_cloud_vpc', $updated);

    return $this->redirect('view.aws_cloud_vpc.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAllVpcList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_vpc');
  }

  /**
   * {@inheritdoc}
   */
  public function updateVpcPeeringConnectionList($cloud_context): RedirectResponse {

    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateVpcPeeringConnections();

    $this->updateMessages('aws_cloud_vpc_peering_connection', $updated);

    return $this->redirect('view.aws_cloud_vpc_peering_connection.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAllVpcPeeringConnectionList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_vpc_peering_connection');
  }

  /**
   * {@inheritdoc}
   */
  public function updateSubnetList($cloud_context): RedirectResponse {

    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateSubnets();

    $this->updateMessages('aws_cloud_subnet', $updated);

    return $this->redirect('view.aws_cloud_subnet.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAllSubnetList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_subnet');
  }

  /**
   * {@inheritdoc}
   */
  public function updateInternetGatewayList($cloud_context): RedirectResponse {

    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateInternetGateways();

    $this->updateMessages('aws_cloud_internet_gateway', $updated);

    return $this->redirect('view.aws_cloud_internet_gateway.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAllInternetGatewayList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_internet_gateway');
  }

  /**
   * {@inheritdoc}
   */
  public function updateCarrierGatewayList($cloud_context): RedirectResponse {

    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateCarrierGateways();

    $this->updateMessages('aws_cloud_carrier_gateway', $updated);

    return $this->redirect('view.aws_cloud_carrier_gateway.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAllCarrierGatewayList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_carrier_gateway');
  }

  /**
   * {@inheritdoc}
   */
  public function updateTransitGatewayList($cloud_context): RedirectResponse {

    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateTransitGateways();

    $this->updateMessages('aws_cloud_transit_gateway', $updated);

    return $this->redirect('view.aws_cloud_transit_gateway.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAllTransitGatewayList(): RedirectResponse {
    return $this->ec2Service->updateAllEntityList('aws_cloud_transit_gateway');
  }

}
