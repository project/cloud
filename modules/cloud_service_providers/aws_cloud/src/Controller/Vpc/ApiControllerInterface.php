<?php

namespace Drupal\aws_cloud\Controller\Vpc;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * {@inheritdoc}
 */
interface ApiControllerInterface {

  /**
   * Update all VPCs in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateVpcList($cloud_context): RedirectResponse;

  /**
   * Update all VPCs of all cloud region.
   */
  public function updateAllVpcList(): RedirectResponse;

  /**
   * Update all VPC peering connections in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateVpcPeeringConnectionList($cloud_context): RedirectResponse;

  /**
   * Update all VPC peering connections of all cloud region.
   */
  public function updateAllVpcPeeringConnectionList(): RedirectResponse;

  /**
   * Update all subnets in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateSubnetList($cloud_context): RedirectResponse;

  /**
   * Update all subnets of all cloud region.
   */
  public function updateAllSubnetList(): RedirectResponse;

  /**
   * Update all internet gateways in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateInternetGatewayList($cloud_context): RedirectResponse;

  /**
   * Update all internet gateways of all cloud region.
   */
  public function updateAllInternetGatewayList(): RedirectResponse;

  /**
   * Update all carrier gateways in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateCarrierGatewayList($cloud_context): RedirectResponse;

  /**
   * Update all carrier gateways of all cloud region.
   */
  public function updateAllCarrierGatewayList(): RedirectResponse;

  /**
   * Update all transit gateways in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateTransitGatewayList($cloud_context): RedirectResponse;

  /**
   * Update all transit gateways of all cloud region.
   */
  public function updateAllTransitGatewayList(): RedirectResponse;

}
