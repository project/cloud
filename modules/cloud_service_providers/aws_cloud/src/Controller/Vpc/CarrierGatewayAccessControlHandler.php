<?php

namespace Drupal\aws_cloud\Controller\Vpc;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Traits\AccessCheckTrait;

/**
 * Access controller for the carrier gateway entity.
 *
 * @see \Drupal\aws_cloud\Entity\Vpc\CarrierGateway.
 */
class CarrierGatewayAccessControlHandler extends EntityAccessControlHandler {

  use AccessCheckTrait;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {

    switch ($operation) {
      case 'view':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'view own aws cloud carrier gateway',
          'view any aws cloud carrier gateway',
          TRUE
        );

      case 'update':
      case 'edit':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'edit own aws cloud carrier gateway',
          'edit any aws cloud carrier gateway'
        );

      case 'delete':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'delete own aws cloud carrier gateway',
          'delete any aws cloud carrier gateway'
        );

    }
    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
