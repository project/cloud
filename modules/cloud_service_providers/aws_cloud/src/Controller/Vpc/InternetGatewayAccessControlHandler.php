<?php

namespace Drupal\aws_cloud\Controller\Vpc;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Traits\AccessCheckTrait;

/**
 * Access controller for the internet gateway entity.
 *
 * @see \Drupal\aws_cloud\Entity\Vpc\InternetGateway.
 */
class InternetGatewayAccessControlHandler extends EntityAccessControlHandler {

  use AccessCheckTrait;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {

    switch ($operation) {
      case 'view':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'view own aws cloud internet gateway',
          'view any aws cloud internet gateway',
          TRUE
        );

      case 'update':
      case 'edit':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'edit own aws cloud internet gateway',
          'edit any aws cloud internet gateway'
        );

      case 'delete':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'delete own aws cloud internet gateway',
          'delete any aws cloud internet gateway'
        );

      case 'attach':
        if ($entity->getVpcId() === NULL) {
          return $this->allowedIfCanAccessCloudConfigWithOwner(
            $entity,
            $account,
            'edit own aws cloud internet gateway',
            'edit any aws cloud internet gateway'
          );
        }
        break;

      case 'detach':
        if ($entity->getVpcId() !== NULL) {
          return $this->allowedIfCanAccessCloudConfigWithOwner(
            $entity,
            $account,
            'edit own aws cloud internet gateway',
            'edit any aws cloud internet gateway'
          );
        }
        break;
    }
    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
