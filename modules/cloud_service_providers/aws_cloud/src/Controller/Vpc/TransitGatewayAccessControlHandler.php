<?php

namespace Drupal\aws_cloud\Controller\Vpc;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Traits\AccessCheckTrait;

/**
 * Access controller for the transit gateway entity.
 *
 * @see \Drupal\aws_cloud\Entity\Vpc\TransitGateway.
 */
class TransitGatewayAccessControlHandler extends EntityAccessControlHandler {

  use AccessCheckTrait;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {

    switch ($operation) {
      case 'view':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'view own aws cloud transit gateway',
          'view any aws cloud transit gateway',
          TRUE
        );

      case 'update':
      case 'edit':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'edit own aws cloud transit gateway',
          'edit any aws cloud transit gateway'
        );

      case 'delete':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'delete own aws cloud transit gateway',
          'delete any aws cloud transit gateway'
        );

    }
    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
