<?php

namespace Drupal\aws_cloud\Controller\Vpc;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Traits\AccessCheckTrait;
use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * Access controller for the transit gateway route table entity.
 *
 * @see \Drupal\aws_cloud\Entity\Vpc\Entity\TransitGatewayRoute.
 */
class TransitGatewayRouteAccessControlHandler extends EntityAccessControlHandler {

  use AccessCheckTrait;
  use CloudContentEntityTrait;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {

    // Get cloud service provider name.
    $cloud_name = $this->getModuleNameWithWhitespace($entity);

    switch ($operation) {
      case 'view':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          "view own {$cloud_name} transit gateway route",
          "view any {$cloud_name} transit gateway route",
          TRUE
        );

    }
    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
