<?php

namespace Drupal\aws_cloud\Entity\CloudFormation;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the stack entity.
 *
 * @ingroup aws_cloud
 *
 * @ContentEntityType(
 *   id = "aws_cloud_stack",
 *   id_plural = "aws_cloud_stacks",
 *   label = @Translation("Stack"),
 *   label_collection = @Translation("Stacks"),
 *   label_singular = @Translation("Stack"),
 *   label_plural = @Translation("Stacks"),
 *   handlers = {
 *     "view_builder" = "Drupal\aws_cloud\Entity\CloudFormation\StackViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\aws_cloud\Entity\CloudFormation\StackViewsData",
 *     "form" = {
 *       "delete"                  = "Drupal\aws_cloud\Form\CloudFormation\StackDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\aws_cloud\Form\CloudFormation\StackDeleteMultipleForm",
 *     },
 *     "access"       = "Drupal\aws_cloud\Controller\CloudFormation\StackAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "aws_cloud_stack",
 *   admin_permission = "administer aws cloud availability zone",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "stack_name",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *     "canonical"            = "/clouds/aws_cloud/{cloud_context}/stack/{aws_cloud_stack}",
 *     "delete-form"          = "/clouds/aws_cloud/{cloud_context}/stack/{aws_cloud_stack}/delete",
 *     "collection"           = "/clouds/aws_cloud/{cloud_context}/stack",
 *     "delete-multiple-form" = "/clouds/aws_cloud/{cloud_context}/stack/delete_multiple",
 *   },
 *   field_ui_base_route = "aws_cloud_stack.settings"
 * )
 */
class Stack extends CloudContentEntityBase implements StackInterface {

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): StackInterface {
    return $this->set('description', $description);
  }

  /**
   * {@inheritdoc}
   */
  public function getStackName(): ?string {
    return $this->get('stack_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackName($stack_name = ''): StackInterface {
    return $this->set('stack_name', $stack_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getStackId(): ?string {
    return $this->get('stack_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackId($stack_id = ''): StackInterface {
    return $this->set('stack_id', $stack_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getStackStatus(): ?string {
    return $this->get('stack_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackStatus($stack_status = ''): StackInterface {
    return $this->set('stack_status', $stack_status);
  }

  /**
   * {@inheritdoc}
   */
  public function getStackStatusReason(): ?string {
    return $this->get('stack_status_reason')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackStatusReason($stack_status_reason = ''): StackInterface {
    return $this->set('stack_status_reason', $stack_status_reason);
  }

  /**
   * {@inheritdoc}
   */
  public function getDisableRollback(): ?bool {
    return $this->get('disable_rollback')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDisableRollback($disable_rollback = FALSE): StackInterface {
    return $this->set('disable_rollback', $disable_rollback);
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters(): array {
    return $this->get('parameters')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setParameters(array $parameters = []): StackInterface {
    return $this->set('parameters', $parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function getOutputs(): array {
    return $this->get('outputs')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setOutputs(array $outputs = []): StackInterface {
    return $this->set('outputs', $outputs);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): StackInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): StackInterface {
    return $this->set('account_id', $account_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the stack entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the stack entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('A user-defined description associated with the stack.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stack_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stack ID'))
      ->setDescription(t('Unique identifier of the stack.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE);

    $fields['stack_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stack name'))
      ->setDescription(t('The name associated with the stack.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['stack_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stack Status'))
      ->setDescription(t('Current status of the stack.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stack_status_reason'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stack Status Reason'))
      ->setDescription(t('Success/failure message associated with the stack status.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['disable_rollback'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Disable rollback'))
      ->setDescription(t('Boolean to enable or disable rollback on stack creation failures.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['parameters'] = BaseFieldDefinition::create('key_value')
      ->setLabel(t('Parameters'))
      ->setDescription(t('Parameters.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('long', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'weight' => -5,
      ]);

    $fields['outputs'] = BaseFieldDefinition::create('key_value')
      ->setLabel(t('Outputs'))
      ->setDescription(t('Outputs.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('long', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'weight' => -5,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the AMI was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the stack entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
