<?php

namespace Drupal\aws_cloud\Entity\CloudFormation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Stack entity.
 *
 * @ingroup aws_cloud
 */
interface StackInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): StackInterface;

  /**
   * {@inheritdoc}
   */
  public function getStackName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStackName($stack_name = ''): StackInterface;

  /**
   * {@inheritdoc}
   */
  public function getStackId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStackId($stack_id = ''): StackInterface;

  /**
   * {@inheritdoc}
   */
  public function getStackStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStackStatus($stack_status = ''): StackInterface;

  /**
   * {@inheritdoc}
   */
  public function getStackStatusReason(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStackStatusReason($stack_status_reason = ''): StackInterface;

  /**
   * {@inheritdoc}
   */
  public function getDisableRollback(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function setDisableRollback($disable_rollback = FALSE): StackInterface;

  /**
   * {@inheritdoc}
   */
  public function getParameters(): array;

  /**
   * {@inheritdoc}
   */
  public function setParameters(array $parameters = []);

  /**
   * {@inheritdoc}
   */
  public function getOutputs(): array;

  /**
   * {@inheritdoc}
   */
  public function setOutputs(array $outputs = []): StackInterface;

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): StackInterface;

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): StackInterface;

}
