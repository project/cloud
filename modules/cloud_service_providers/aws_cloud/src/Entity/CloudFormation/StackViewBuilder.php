<?php

namespace Drupal\aws_cloud\Entity\CloudFormation;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the Availability Zone view builders.
 */
class StackViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'stack',
        'title' => $this->t('Stack'),
        'open' => TRUE,
        'fields' => [
          'stack_name',
          'stack_id',
          'description',
          'stack_status',
          'stack_status_reason',
          'disable_rollback',
          'parameters',
          'outputs',
          'created',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
