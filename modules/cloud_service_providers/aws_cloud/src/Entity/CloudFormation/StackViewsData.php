<?php

namespace Drupal\aws_cloud\Entity\CloudFormation;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewsData;

/**
 * Provides the views data for the stack entity type.
 */
class StackViewsData extends AwsCloudViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['aws_cloud_stack']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('AWS Cloud stack'),
      'help'  => $this->t('The AWS Cloud stack entity ID.'),
    ];

    $data['aws_cloud_stack']['stack_bulk_form'] = [
      'title' => $this->t('Stack operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple stacks.'),
      'field' => [
        'id' => 'stack_bulk_form',
      ],
    ];

    $table_name = $this->storage->getEntityTypeId();
    $fields = $this->getFieldStorageDefinitions();

    // The following is a list of fields to turn from text search to
    // select list.  This list can be expanded through hook_views_data_alter().
    $selectable = [
      'stack_id',
      'stack_status',
    ];

    $data['aws_cloud_stack']['table']['base']['access query tag'] = 'aws_cloud_stack_views_access';
    $this->addDropdownSelector($data, $table_name, $fields, $selectable);

    return $data;
  }

}
