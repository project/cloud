<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\EntityInterface;
use Drupal\cloud\Service\Util\EntityLinkQuery;

/**
 * Query generator for attached instance links on volume pages.
 */
class AttachedInstanceLinkQuery extends EntityLinkQuery {

  /**
   * {@inheritdoc}
   */
  public function query(string $target_type, string $cloud_context, string $field_name, string $item, ?EntityInterface $entity = NULL): array {
    if (empty($entity) || !method_exists($entity, 'getAttachmentInformation')) {
      return [];
    }

    // To retrieve an AttachedInstance,
    // use VolumeInterface::getAttachmentInformation() to obtain the Attached
    // instance ID and search for the instance using it as the key.
    return $this->entityTypeManager
      ->getStorage($target_type)
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $cloud_context)
      ->condition($field_name, $entity->getAttachmentInformation())
      ->execute();
  }

}
