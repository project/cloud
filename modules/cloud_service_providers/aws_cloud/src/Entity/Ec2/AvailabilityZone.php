<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the AvailabilityZone entity.
 *
 * @ingroup aws_cloud
 *
 * @ContentEntityType(
 *   id = "aws_cloud_availability_zone",
 *   id_plural = "aws_cloud_availability_zones",
 *   label = @Translation("Availability Zone"),
 *   label_collection = @Translation("Availability Zones"),
 *   label_singular = @Translation("Availability Zone"),
 *   label_plural = @Translation("Availability Zones"),
 *   handlers = {
 *     "view_builder" = "Drupal\aws_cloud\Entity\Ec2\AvailabilityZoneViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\aws_cloud\Entity\Ec2\AvailabilityZoneViewsData",
 *     "form" = {
 *     },
 *     "access"       = "Drupal\aws_cloud\Controller\Ec2\AvailabilityZoneAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "aws_cloud_availability_zone",
 *   admin_permission = "administer aws cloud availability zone",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "zone_name",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *   },
 *   field_ui_base_route = "aws_cloud_availability_zone.settings"
 * )
 */
class AvailabilityZone extends CloudContentEntityBase implements AvailabilityZoneInterface {

  /**
   * {@inheritdoc}
   */
  public function getGroupName(): ?string {
    return $this->get('group_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGroupName($group_name = ''): AvailabilityZoneInterface {
    return $this->set('group_name', $group_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkBorderGroup(): ?string {
    return $this->get('network_border_group')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNetworkBorderGroup($network_border_group = ''): AvailabilityZoneInterface {
    return $this->set('network_border_group', $network_border_group);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptInStatus(): ?string {
    return $this->get('opt_in_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOptInStatus($opt_in_status = ''): AvailabilityZoneInterface {
    return $this->set('opt_in_status', $opt_in_status);
  }

  /**
   * {@inheritdoc}
   */
  public function getParentZoneId(): ?string {
    return $this->get('parent_zone_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentZoneId($parent_zone_id = ''): AvailabilityZoneInterface {
    return $this->set('parent_zone_id', $parent_zone_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getParentZoneName(): ?string {
    return $this->get('parent_zone_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentZoneName($parent_zone_name = ''): AvailabilityZoneInterface {
    return $this->set('parent_zone_name', $parent_zone_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getRegionName(): ?string {
    return $this->get('region_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRegionName($region_name = ''): AvailabilityZoneInterface {
    return $this->set('region_name', $region_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getZoneId(): ?string {
    return $this->get('zone_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setZoneId($zone_id = ''): AvailabilityZoneInterface {
    return $this->set('zone_id', $zone_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getZoneName(): ?string {
    return $this->get('zone_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setZoneName($zone_name = ''): AvailabilityZoneInterface {
    return $this->set('zone_name', $zone_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getZoneState(): ?string {
    return $this->get('zone_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setZoneState($zone_state = ''): AvailabilityZoneInterface {
    return $this->set('zone_state', $zone_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getZoneType(): ?string {
    return $this->get('zone_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setZoneType($zone_type = ''): AvailabilityZoneInterface {
    return $this->set('zone_type', $zone_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): AvailabilityZoneInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): AvailabilityZoneInterface {
    return $this->set('account_id', $account_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Availability Zone entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Availability Zone entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['group_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Group name'))
      ->setDescription(t('The group name of the Availability Zone entity.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE);

    $fields['network_border_group'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Network Border Group'))
      ->setDescription(t("The name of the network border group."))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['opt_in_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Opt In Status'))
      ->setDescription(t('For Availability Zones, this parameter always has the value of opt-in-not-required. For Local Zones and Wavelength Zones, this parameter is the opt-in status. The possible values are opted-in, and not-opted-in.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['parent_zone_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ParentZone ID'))
      ->setDescription(t('The ID of the zone that handles some of the Local Zone or Wavelength Zone control plane operations, such as API calls.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['parent_zone_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent zone name'))
      ->setDescription(t('The name of the zone that handles some of the Local Zone or Wavelength Zone control plane operations, such as API calls.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'aws_cloud_instance',
          'field_name' => 'instance_id',
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['region_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Region name'))
      ->setDescription(t('The name of the region.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['zone_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Zone ID'))
      ->setDescription(t('The ID of the Availability Zone, Local Zone, or Wavelength Zone.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['zone_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Zone name'))
      ->setDescription(t('The name of the Availability Zone, Local Zone, or Wavelength Zone.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['zone_state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Zone State'))
      ->setDescription(t('The state of the Availability Zone, Local Zone, or Wavelength Zone.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['platform'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Platform'))
      ->setDescription(t('Specifies the operating system (e.g, Windows), if applicable.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['zone_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Zone type'))
      ->setDescription(t('The type of zone. The valid values are availability-zone, local-zone, and wavelength-zone.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the AMI was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Availability Zone entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
