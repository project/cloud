<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an AvailabilityZone entity.
 *
 * @ingroup aws_cloud
 */
interface AvailabilityZoneInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getGroupName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setGroupName($group_name = ''): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function getNetworkBorderGroup(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNetworkBorderGroup($network_border_group = ''): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function getOptInStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setOptInStatus($opt_in_status = ''): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function getParentZoneId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setParentZoneId($parent_zone_id = ''): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function getParentZoneName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setParentZoneName($parent_zone_name = ''): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function getRegionName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setRegionName($region_name = ''): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function getZoneId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setZoneId($zone_id = ''): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function getZoneName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setZoneName($zone_name = ''): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function getZoneState(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setZoneState($zone_state = ''): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function getZoneType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setZoneType($zone_type = ''): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): AvailabilityZoneInterface;

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): AvailabilityZoneInterface;

}
