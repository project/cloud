<?php

namespace Drupal\aws_cloud\Entity\Ec2;

/**
 * Provides the Availability Zone view builders.
 */
class AvailabilityZoneViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'availability zone',
        'title' => $this->t('Availability Zone'),
        'open' => TRUE,
        'fields' => [
          'group_name',
          'zone_id',
          'zone_name',
          'created',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
