<?php

namespace Drupal\aws_cloud\Entity\Ec2;

/**
 * Provides the views data for the AvailabilityZone entity type.
 */
class AvailabilityZoneViewsData extends AwsCloudViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    return $data;
  }

}
