<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\EntityInterface;
use Drupal\cloud\Entity\CloudViewBuilder;

/**
 * Provides the EC2 base entity view builders.
 */
abstract class AwsCloudViewBuilder extends CloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL): array {
    $build = parent::view($entity, $view_mode, $langcode);
    $build['#attached']['library'][] = 'aws_cloud/aws_cloud_view_builder';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return array_merge(parent::trustedCallbacks(), ['reorderLaunchTemplate']);
  }

  /**
   * Reorder fields of AWS Cloud's cloud launch template.
   *
   * @param array $build
   *   Build array.
   *
   * @return array
   *   Build array reordered.
   */
  public static function reorderLaunchTemplate(array $build): array {
    $build['name']['#label_display'] = 'inline';
    $build['instance']['name'] = $build['name'];
    unset($build['name']);
    return $build;
  }

}
