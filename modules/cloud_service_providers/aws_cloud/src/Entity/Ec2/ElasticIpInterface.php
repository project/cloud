<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an ElasticIp entity.
 *
 * @ingroup aws_cloud
 */
interface ElasticIpInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name);

  /**
   * {@inheritdoc}
   */
  public function getPublicIp(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setPublicIp($public_ip = ''): ElasticIpInterface;

  /**
   * {@inheritdoc}
   */
  public function getElasticIpType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setElasticIpType($elastic_ip_type = ''): ElasticIpInterface;

  /**
   * {@inheritdoc}
   */
  public function setAllocationId($allocation_id = ''): ElasticIpInterface;

  /**
   * {@inheritdoc}
   */
  public function setAssociationId($association_id = ''): ElasticIpInterface;

  /**
   * {@inheritdoc}
   */
  public function getInstanceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setInstanceId($instance_id = ''): ElasticIpInterface;

  /**
   * {@inheritdoc}
   */
  public function getDomain(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDomain($domain): ElasticIpInterface;

  /**
   * {@inheritdoc}
   */
  public function getScope(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getNetworkInterfaceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getPrivateIpAddress(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getNetworkInterfaceOwner(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAllocationId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAssociationId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getNetworkBorderGroup(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNetworkBorderGroup($network_border_group = ''): ElasticIpInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): ElasticIpInterface;

}
