<?php

namespace Drupal\aws_cloud\Entity\Ec2;

/**
 * Provides the views data for the CloudScripting entity type.
 */
class ElasticIpViewsData extends AwsCloudViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();
    $table_name = $this->storage->getEntityTypeId();
    $fields = $this->getFieldStorageDefinitions();

    $data[$table_name]['elastic_ip_bulk_form'] = [
      'title' => $this->t('Elastic IP operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple Elastic IPs.'),
      'field' => [
        'id' => 'elastic_ip_bulk_form',
      ],
    ];

    // The following is a list of fields to turn from text search to
    // select list.  This list can be expanded through hook_views_data_alter().
    $selectable = [
      'domain',
      'scope',
      'network_interface_id',
      'allocation_id',
      'association_id',
    ];

    $data['aws_cloud_elastic_ip']['table']['base']['access query tag'] = 'aws_cloud_elastic_ip_views_access';
    $this->addDropdownSelector($data, $table_name, $fields, $selectable);

    return $data;
  }

}
