<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an image entity.
 *
 * @ingroup aws_cloud
 */
interface ImageInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description): ImageInterface;

  /**
   * {@inheritdoc}
   */
  public function getImageId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setImageId($image_id = ''): ImageInterface;

  /**
   * {@inheritdoc}
   */
  public function getInstanceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getArchitecture(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getVirtualizationType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getRootDeviceName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getRamdiskId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getProductCode(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAmiName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getSource(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getStateReason(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getPlatform(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getImageType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getRootDeviceType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getKernelId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getBlockDeviceMappings(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function getAccountId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getVisibility(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function getLaunchPermissionAccountIds(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): ImageInterface;

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): ImageInterface;

  /**
   * {@inheritdoc}
   */
  public function setName($name): ImageInterface;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): ImageInterface;

  /**
   * {@inheritdoc}
   */
  public function setVisibility($visibility): ImageInterface;

  /**
   * {@inheritdoc}
   */
  public function setBlockDeviceMappings(array $block_device_mappings): ImageInterface;

  /**
   * {@inheritdoc}
   */
  public function setLaunchPermissionAccountIds(array $launch_permission_account_ids): ImageInterface;

}
