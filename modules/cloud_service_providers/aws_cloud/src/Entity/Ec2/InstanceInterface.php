<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an instance entity.
 *
 * @ingroup aws_cloud
 */
interface InstanceInterface extends ContentEntityInterface, EntityOwnerInterface {

  public const TAG_LAUNCH_SOFTWARE = 'instance_launch_software';

  public const TAG_LAUNCH_ORIGIN = 'instance_launch_origin';

  public const TAG_LAUNCHED_BY = 'instance_launched_by';

  public const TAG_TERMINATION_TIMESTAMP = 'instance_termination_timestamp';

  public const TAG_BASTION = 'instance_bastion';

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getInstanceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getInstanceType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZone(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getInstanceState(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getPublicDns(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setPublicDns($dns): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getPublicIp(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getElasticIp(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getPrivateDns(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setPrivateDns($private_dns): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getPrivateIps(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setPrivateIps($private_ips): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getPrivateSecondaryIp(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setPrivateSecondaryIp($private_ip): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getKeyPairName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setKeyPairName($key_pair_name = ''): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function isMonitoring(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function setMonitoring($is_monitoring): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getMonitoring(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setLaunchTime($launch_time): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getLaunchTime(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroups(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setSecurityGroups($securityGroups): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getVpcId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getSubnetId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getNetworkInterfaces(): array;

  /**
   * {@inheritdoc}
   */
  public function getSourceDestCheck(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getEbsOptimized(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function getRootDeviceType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getRootDevice(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getBlockDevices(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setBlockDevices($block_devices): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getScheduledEvents(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getImageId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getPlatform(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getIamRole(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setIamRole($iam_role): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getTerminationProtection(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function setTerminationProtection($termination_protection): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getLifecycle(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAlarmStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getKernelId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getRamdiskId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getPlacementGroup(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getVirtualization(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getReservation(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAccountId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAmiLaunchIndex(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getTenancy(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getHostId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAffinity(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getStateTransitionReason(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getLoginUsername(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getUserData(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setUserData($user_data): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getMinCount(): ?int;

  /**
   * {@inheritdoc}
   */
  public function getMaxCount(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = '' /* Y/m/d H:i:s */): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function setInstanceId($instance_id = ''): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function setInstanceState($state = ''): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function setElasticIp($elastic_ip = ''): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function setPublicIp($public_ip = ''): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function setName($name): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getCost(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setCost($cost): InstanceInterface;

  /**
   * Load an instance entity by the aws instance_id.
   *
   * @param string $instance_id
   *   The ID of instance.
   *
   * @return \Drupal\aws_cloud\Entity\Ec2\Instance
   *   The instance entity.
   */
  public static function loadByInstanceId($instance_id): InstanceInterface;

  /**
   * Get the termination timestamp.
   */
  public function getTerminationTimestamp(): FieldItemListInterface;

  /**
   * Set the termination timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of termination.
   *
   * @return \Drupal\aws_cloud\Entity\Ec2\Instance
   *   The instance entity.
   */
  public function setTerminationTimestamp($timestamp): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function setLowUtilization($low_utilization): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getLowUtilization(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function setSchedule($schedule): InstanceInterface;

  /**
   * {@inheritdoc}
   */
  public function getSchedule(): ?string;

  /**
   * Get the tags.
   */
  public function getTags(): array;

  /**
   * Set the tags.
   *
   * @param array $tags
   *   The tags.
   *
   * @return \Drupal\aws_cloud\Entity\Ec2\Instance
   *   The instance entity.
   */
  public function setTags(array $tags): InstanceInterface;

  /**
   * Set network interfaces.
   *
   * @param array $interfaces
   *   An array of Interfaces.
   */
  public function setNetworkInterfaces(array $interfaces): InstanceInterface;

  /**
   * Calculate the days an instance has been running.
   *
   * @return int
   *   The number of days an instance has been running.
   */
  public function daysRunning(): int;

}
