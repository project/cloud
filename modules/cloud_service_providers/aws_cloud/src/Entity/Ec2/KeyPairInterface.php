<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a KeyPair entity.
 *
 * @ingroup aws_cloud
 */
interface KeyPairInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getKeyPairName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getKeyPairId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setKeyPairId($key_pair_id = ''): KeyPairInterface;

  /**
   * {@inheritdoc}
   */
  public function getKeyFingerprint(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setKeyFingerprint($key_fingerprint = ''): KeyPairInterface;

  /**
   * {@inheritdoc}
   */
  public function getKeyMaterial(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setKeyMaterial($key_material = ''): KeyPairInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): KeyPairInterface;

  /**
   * {@inheritdoc}
   */
  public function getKeyFileLocation();

  /**
   * {@inheritdoc}
   */
  public function getKeyFileName(): ?string;

}
