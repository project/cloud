<?php

namespace Drupal\aws_cloud\Entity\Ec2;

/**
 * Provides the key pair view builders.
 */
class KeyPairViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'key_pair',
        'title' => $this->t('Key pair'),
        'open' => TRUE,
        'fields' => [
          'key_pair_name',
          'key_pair_id',
          'key_fingerprint',
          'key_material',
          'created',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
