<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a NetworkInterface entity.
 *
 * @ingroup aws_cloud
 */
interface NetworkInterfaceInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name);

  /**
   * {@inheritdoc}
   */
  public function getNetworkInterfaceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNetworkInterfaceId($network_interface): NetworkInterfaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getVpcId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setVpcId($vpc_id = ''): NetworkInterfaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getMacAddress(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroups(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status = ''): NetworkInterfaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getPrivateDns(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getPrimaryPrivateIp(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getPrimary(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getSecondaryPrivateIps(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAttachmentId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAttachmentOwner(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAttachmentStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAccountId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAssociationId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getSubnetId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZone(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description): NetworkInterfaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getPublicIps(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getSourceDestCheck(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getInstanceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getDeviceIndex(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getDeleteOnTermination(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAllocationId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed();

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time);

  /**
   * {@inheritdoc}
   */
  public function setPrimaryPrivateIp($private_ip);

  /**
   * {@inheritdoc}
   */
  public function setSecondaryPrivateIp($secondary_ip);

  /**
   * {@inheritdoc}
   */
  public function setSecondaryAssociationId($association_id);

  /**
   * {@inheritdoc}
   */
  public function getSecondaryAssociationId();

  /**
   * {@inheritdoc}
   */
  public function setAssociationId($association_id);

  /**
   * {@inheritdoc}
   */
  public function setPublicIps($public_ips);

  /**
   * {@inheritdoc}
   */
  public function setSecurityGroups($security_groups);

}
