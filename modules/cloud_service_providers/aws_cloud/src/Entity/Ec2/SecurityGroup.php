<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;

/**
 * Defines the security group entity.
 *
 * @ingroup aws_cloud
 *
 * @ContentEntityType(
 *   id = "aws_cloud_security_group",
 *   id_plural = "aws_cloud_security_groups",
 *   label = @Translation("Security group"),
 *   label_collection = @Translation("Security groups"),
 *   label_singular = @Translation("Security group"),
 *   label_plural = @Translation("Security groups"),
 *   handlers = {
 *     "view_builder" = "Drupal\aws_cloud\Entity\Ec2\SecurityGroupViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\aws_cloud\Entity\Ec2\SecurityGroupViewsData",
 *     "form" = {
 *       "default"                 = "Drupal\aws_cloud\Form\Ec2\SecurityGroupEditForm",
 *       "add"                     = "Drupal\aws_cloud\Form\Ec2\SecurityGroupCreateForm",
 *       "edit"                    = "Drupal\aws_cloud\Form\Ec2\SecurityGroupEditForm",
 *       "delete"                  = "Drupal\aws_cloud\Form\Ec2\SecurityGroupDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\aws_cloud\Form\Ec2\SecurityGroupDeleteMultipleForm",
 *       "revoke"                  = "Drupal\aws_cloud\Form\Ec2\SecurityGroupRevokeForm",
 *       "copy"                    = "Drupal\aws_cloud\Form\Ec2\SecurityGroupCopyForm",
 *     },
 *     "access"       = "Drupal\aws_cloud\Controller\Ec2\SecurityGroupAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "aws_cloud_security_group",
 *   admin_permission = "administer aws cloud security group",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *     "canonical"            = "/clouds/aws_cloud/{cloud_context}/security_group/{aws_cloud_security_group}",
 *     "edit-form"            = "/clouds/aws_cloud/{cloud_context}/security_group/{aws_cloud_security_group}/edit",
 *     "delete-form"          = "/clouds/aws_cloud/{cloud_context}/security_group/{aws_cloud_security_group}/delete",
 *     "collection"           = "/clouds/aws_cloud/{cloud_context}/security_group",
 *     "delete-multiple-form" = "/clouds/aws_cloud/{cloud_context}/security_group/delete_multiple",
 *     "revoke-form"          = "/clouds/aws_cloud/{cloud_context}/security_group/{aws_cloud_security_group}/revoke",
 *     "copy-form"            = "/clouds/aws_cloud/{cloud_context}/security_group/{aws_cloud_security_group}/copy",
 *   },
 *   field_ui_base_route = "aws_cloud_security_group.settings"
 * )
 */
class SecurityGroup extends CloudContentEntityBase implements SecurityGroupInterface {

  use AwsCloudFormTrait;

  /**
   * {@inheritdoc}
   */
  public function getGroupId(): ?string {
    return $this->get('group_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGroupId($group_id = ''): SecurityGroupInterface {
    return $this->set('group_id', $group_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupName(): ?string {
    return $this->get('group_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGroupName($group_name = ''): SecurityGroupInterface {
    return $this->set('group_name', $group_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): SecurityGroupInterface {
    return $this->set('description', $description);
  }

  /**
   * {@inheritdoc}
   */
  public function getVpcId(): ?string {
    return $this->get('vpc_id')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultVpc(): bool {
    return $this->get('default_vpc')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): ?string {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getIpPermission(): FieldItemListInterface {
    return $this->get('ip_permission');
  }

  /**
   * {@inheritdoc}
   */
  public function getOutboundPermission(): FieldItemListInterface {
    return $this->get('outbound_permission');
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultVpc($default): SecurityGroupInterface {
    return $this->set('default_vpc', $default);
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): SecurityGroupInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $weight = -50;

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the SecurityGroup entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the SecurityGroup entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the security group entity.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['group_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Security group name'))
      ->setDescription(t('Enter the name of your security group.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ])
      // Re-enabled form field; the field can take advantage of validation.
      ->setDisplayOptions(
        'form', [
          'type' => 'string_textfield',
        ]
      )
      ->setRequired(TRUE)
      ->addConstraint('SecurityGroupName')
      ->setReadOnly(TRUE);

    $fields['group_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of your security group.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ])
      ->setReadOnly(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Description of security group.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => self::$awsCloudSecurityGroupDescriptionMaxLength,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ]);

    $fields['vpc_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('VPC ID'))
      ->setDescription(t('The ID of the virtual private cloud (VPC) the security group belongs to, if applicable. A VPC is an isolated portion of the AWS Cloud.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'aws_cloud_vpc',
          'field_name' => 'vpc_id',
          'html_generator_class' => EntityLinkWithNameHtmlGenerator::class,
        ],
        'weight' => $weight++,
      ])
      ->setReadOnly(TRUE);

    $fields['default_vpc'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Default VPC'))
      ->setDescription(t('Whether the VPC is a default VPC.'))
      ->setDefaultValue(FALSE)
      ->setReadOnly(TRUE);

    // Inbound permissions.
    $fields['ip_permission'] = BaseFieldDefinition::create('ip_permission')
      ->setLabel(t('Inbound rules'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('Ingress rules.'))
      ->setDisplayOptions('view', [
        'type' => 'ip_permission_formatter',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'ip_permission_item',
        'weight' => $weight,
      ]);

    // Outbound permissions.
    $fields['outbound_permission'] = BaseFieldDefinition::create('ip_permission')
      ->setLabel(t('Outbound rules'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('Egress rules.  Egress is only available for VPC security groups.'))
      ->setDisplayOptions('view', [
        'type' => 'ip_permission_formatter',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'ip_permission_item',
        'weight' => $weight,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => $weight++,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the SecurityGroup entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => $weight++,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => $weight,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
