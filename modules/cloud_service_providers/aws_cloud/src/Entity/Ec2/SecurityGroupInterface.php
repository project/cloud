<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a SecurityGroup entity.
 *
 * @ingroup aws_cloud
 */
interface SecurityGroupInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name);

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): SecurityGroupInterface;

  /**
   * {@inheritdoc}
   */
  public function getGroupId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setGroupId($group_id = ''): SecurityGroupInterface;

  /**
   * {@inheritdoc}
   */
  public function getGroupName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setGroupName($group_name = ''): SecurityGroupInterface;

  /**
   * {@inheritdoc}
   */
  public function getVpcId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getIpPermission(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function getOutboundPermission(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function isDefaultVpc(): bool;

  /**
   * {@inheritdoc}
   */
  public function setDefaultVpc($default): SecurityGroupInterface;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): SecurityGroupInterface;

}
