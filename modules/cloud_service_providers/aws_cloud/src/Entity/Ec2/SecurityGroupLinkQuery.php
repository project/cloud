<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\EntityInterface;
use Drupal\cloud\Service\Util\EntityLinkQuery;

/**
 * Query generator for security group link on instance page.
 */
class SecurityGroupLinkQuery extends EntityLinkQuery {

  /**
   * {@inheritdoc}
   */
  public function query(string $target_type, string $cloud_context, string $field_name, string $item, ?EntityInterface $entity = NULL): array {
    if (empty($entity) || !method_exists($entity, 'getVpcId')) {
      return [];
    }
    // Security groups names can be duplicated in different VPCs.  To make sure
    // the correct security group is returned, pass the entity's vpc_id into
    // the query.
    return $this->entityTypeManager
      ->getStorage($target_type)
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $cloud_context)
      ->condition('vpc_id', $entity->getVpcId())
      ->condition($field_name, $item)
      ->execute();
  }

}
