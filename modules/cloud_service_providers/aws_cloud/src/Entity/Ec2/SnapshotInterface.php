<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a snapshot entity.
 *
 * @ingroup aws_cloud
 */
interface SnapshotInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name);

  /**
   * {@inheritdoc}
   */
  public function getSnapshotId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setSnapshotId($snapshot_id = ''): SnapshotInterface;

  /**
   * {@inheritdoc}
   */
  public function getSize(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setSize($size): SnapshotInterface;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description): SnapshotInterface;

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status = 'unknown'): SnapshotInterface;

  /**
   * {@inheritdoc}
   */
  public function getStarted(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setStarted($started = 0): SnapshotInterface;

  /**
   * {@inheritdoc}
   */
  public function getVolumeId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAccountId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getOwnerAliases(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getEncrypted(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setEncrypted($encrypted = FALSE): SnapshotInterface;

  /**
   * {@inheritdoc}
   */
  public function getKmsKeyId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getStateMessage(): ?string;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): SnapshotInterface;

  /**
   * {@inheritdoc}
   */
  public function daysRunning(): int;

}
