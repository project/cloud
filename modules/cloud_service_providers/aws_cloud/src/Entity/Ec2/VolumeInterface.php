<?php

namespace Drupal\aws_cloud\Entity\Ec2;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a volume entity.
 *
 * @ingroup aws_cloud
 */
interface VolumeInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name);

  /**
   * {@inheritdoc}
   */
  public function getVolumeId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setVolumeId($volume_id = ''): VolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getSize(): int;

  /**
   * {@inheritdoc}
   */
  public function setSize($size): VolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getState(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setState($state = ''): VolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getVolumeStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAttachmentInformation(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAttachmentDeviceName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getVolumeType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setVolumeType($volume_type): VolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getProductCodes(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getIops();

  /**
   * {@inheritdoc}
   */
  public function setIops($iops);

  /**
   * {@inheritdoc}
   */
  public function getAlarmStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getSnapshotId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setSnapshotId(): VolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getSnapshotName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setSnapshotName($snapshot_name): VolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZone(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getEncrypted();

  /**
   * {@inheritdoc}
   */
  public function getKmsKeyId();

  /**
   * {@inheritdoc}
   */
  public function getKmsKeyAliases(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getKmsKeyArn(): ?string;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = 0): VolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): VolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function setAttachmentInformation($attachment_information): VolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function setAttachmentDeviceName($attachment_device_name): VolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function isVolumeUnused(): bool;

  /**
   * {@inheritdoc}
   */
  public function daysRunning(): int;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description): void;

}
