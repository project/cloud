<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a carrier gateway entity.
 *
 * @ingroup aws_cloud
 */
interface CarrierGatewayInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getCarrierGatewayId(): string;

  /**
   * {@inheritdoc}
   */
  public function setCarrierGatewayId($carrier_gateway_id = ''): CarrierGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getVpcId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setVpcId($vpc_id = ''): CarrierGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getAccountId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): CarrierGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getState(): string;

  /**
   * {@inheritdoc}
   */
  public function setState($state): CarrierGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getTags(): array;

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): CarrierGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): CarrierGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = 0): CarrierGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

}
