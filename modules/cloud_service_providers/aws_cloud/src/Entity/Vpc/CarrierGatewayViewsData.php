<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewsData;

/**
 * Provides the views data for the carrier gateway entity type.
 */
class CarrierGatewayViewsData extends AwsCloudViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['aws_cloud_carrier_gateway']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('AWS Cloud carrier gateway'),
      'help'  => $this->t('The AWS Cloud carrier gateway entity ID.'),
    ];

    $data['aws_cloud_carrier_gateway']['carrier_gateway_bulk_form'] = [
      'title' => $this->t('Carrier gateway operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple carrier gateways.'),
      'field' => [
        'id' => 'carrier_gateway_bulk_form',
      ],
    ];

    $table_name = $this->storage->getEntityTypeId();
    $fields = $this->getFieldStorageDefinitions();

    // The following is a list of fields to turn from text search to
    // select list.  This list can be expanded through hook_views_data_alter().
    $selectable = [
      'carrier_gateway_id',
      'vpc_id',
      'state',
      'account_id',
    ];

    $data['aws_cloud_carrier_gateway']['table']['base']['access query tag'] = 'aws_cloud_carrier_gateway_views_access';
    $this->addDropdownSelector($data, $table_name, $fields, $selectable);

    return $data;
  }

}
