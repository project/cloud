<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\aws_cloud\Plugin\Field\Util\AwsCloudReservedKeyChecker;
use Drupal\aws_cloud\Plugin\Field\Util\AwsCloudValueConverter;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;

/**
 * Defines the internet gateway entity.
 *
 * @ingroup aws_cloud
 *
 * @ContentEntityType(
 *   id = "aws_cloud_internet_gateway",
 *   id_plural = "aws_cloud_internet_gateways",
 *   label = @Translation("Internet gateway"),
 *   label_collection = @Translation("Internet gateways"),
 *   label_singular = @Translation("Internet gateway"),
 *   label_plural = @Translation("Internet gateways"),
 *   handlers = {
 *     "view_builder" = "Drupal\aws_cloud\Entity\Vpc\InternetGatewayViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\aws_cloud\Entity\Vpc\InternetGatewayViewsData",
 *     "form" = {
 *       "default"                 = "Drupal\aws_cloud\Form\Vpc\InternetGatewayEditForm",
 *       "add"                     = "Drupal\aws_cloud\Form\Vpc\InternetGatewayCreateForm",
 *       "edit"                    = "Drupal\aws_cloud\Form\Vpc\InternetGatewayEditForm",
 *       "delete"                  = "Drupal\aws_cloud\Form\Vpc\InternetGatewayDeleteForm",
 *       "attach"                  = "Drupal\aws_cloud\Form\Vpc\InternetGatewayAttachForm",
 *       "detach"                  = "Drupal\aws_cloud\Form\Vpc\InternetGatewayDetachForm",
 *       "delete-multiple-confirm" = "Drupal\aws_cloud\Form\Vpc\InternetGatewayDeleteMultipleForm",
 *     },
 *     "access"       = "Drupal\aws_cloud\Controller\Vpc\InternetGatewayAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "aws_cloud_internet_gateway",
 *   admin_permission = "administer aws cloud vpc",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id"  ,
 *     "label" = "name",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *     "canonical"            = "/clouds/aws_cloud/{cloud_context}/internet_gateway/{aws_cloud_internet_gateway}",
 *     "edit-form"            = "/clouds/aws_cloud/{cloud_context}/internet_gateway/{aws_cloud_internet_gateway}/edit",
 *     "delete-form"          = "/clouds/aws_cloud/{cloud_context}/internet_gateway/{aws_cloud_internet_gateway}/delete",
 *     "collection"           = "/clouds/aws_cloud/{cloud_context}/internet_gateway",
 *     "attach-form"          = "/clouds/aws_cloud/{cloud_context}/internet_gateway/{aws_cloud_internet_gateway}/attach",
 *     "detach-form"          = "/clouds/aws_cloud/{cloud_context}/internet_gateway/{aws_cloud_internet_gateway}/detach",
 *     "delete-multiple-form" = "/clouds/aws_cloud/{cloud_context}/internet_gateway/delete_multiple",
 *   },
 *   field_ui_base_route = "aws_cloud_internet_gateway.settings"
 * )
 */
class InternetGateway extends CloudContentEntityBase implements InternetGatewayInterface {

  /**
   * {@inheritdoc}
   */
  public function getInternetGatewayId(): ?string {
    return $this->get('internet_gateway_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInternetGatewayId($internet_gateway_id = ''): InternetGatewayInterface {
    return $this->set('internet_gateway_id', $internet_gateway_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getVpcId(): ?string {
    return $this->get('vpc_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVpcId($vpc_id = ''): InternetGatewayInterface {
    return $this->set('vpc_id', $vpc_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccountId(): ?string {
    return $this->get('account_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): InternetGatewayInterface {
    return $this->set('account_id', $account_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getState(): string {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state): InternetGatewayInterface {
    return $this->set('state', $state);
  }

  /**
   * {@inheritdoc}
   */
  public function getTags(): array {
    return $this->get('tags')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): InternetGatewayInterface {
    return $this->set('tags', $tags);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): InternetGatewayInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = 0): InternetGatewayInterface {
    return $this->set('created', $created);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the InternetGateway entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the InternetGateway entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the InternetGateway entity.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Description of internet gateway.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['internet_gateway_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Internet gateway ID'))
      ->setDescription(t('The internet gateway ID.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['vpc_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('VPC ID'))
      ->setDescription(t('The attached VPC ID.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'aws_cloud_vpc',
          'field_name' => 'vpc_id',
          'html_generator_class' => EntityLinkWithNameHtmlGenerator::class,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('State'))
      ->setDescription(t('The current state of the internet gateway.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['account_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('AWS account ID'))
      ->setDescription(t('The AWS account ID of the internet gateway owner, without dashes.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('Date/time the Amazon internet gateway was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the internet gateway entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tags'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('Tags.'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'weight' => -5,
        'settings' => [
          'value_converter_class' => AwsCloudValueConverter::class,
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'key_value_item',
        'settings' => [
          'reserved_key_checker_class' => AwsCloudReservedKeyChecker::class,
          'value_converter_class' => AwsCloudValueConverter::class,
        ],
      ])
      ->addConstraint('tags_data');

    return $fields;
  }

}
