<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an internet gateway entity.
 *
 * @ingroup aws_cloud
 */
interface InternetGatewayInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name);

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getInternetGatewayId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setInternetGatewayId($internet_gateway_id = ''): InternetGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getVpcId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setVpcId($vpc_id = ''): InternetGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getAccountId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): InternetGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getState(): string;

  /**
   * {@inheritdoc}
   */
  public function setState($state): InternetGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getTags(): array;

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): InternetGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): InternetGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = 0): InternetGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

}
