<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\aws_cloud\Plugin\Field\Util\AwsCloudReservedKeyChecker;
use Drupal\aws_cloud\Plugin\Field\Util\AwsCloudValueConverter;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;

/**
 * Defines the VPC entity.
 *
 * @ingroup aws_cloud
 *
 * @ContentEntityType(
 *   id = "aws_cloud_subnet",
 *   id_plural = "aws_cloud_subnets",
 *   label = @Translation("Subnet"),
 *   label_collection = @Translation("Subnets"),
 *   label_singular = @Translation("Subnet"),
 *   label_plural = @Translation("Subnets"),
 *   handlers = {
 *     "view_builder" = "Drupal\aws_cloud\Entity\Vpc\SubnetViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\aws_cloud\Entity\Vpc\SubnetViewsData",
 *     "form" = {
 *       "default"                 = "Drupal\aws_cloud\Form\Vpc\SubnetEditForm",
 *       "add"                     = "Drupal\aws_cloud\Form\Vpc\SubnetCreateForm",
 *       "edit"                    = "Drupal\aws_cloud\Form\Vpc\SubnetEditForm",
 *       "delete"                  = "Drupal\aws_cloud\Form\Vpc\SubnetDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\aws_cloud\Form\Vpc\SubnetDeleteMultipleForm",
 *     },
 *     "access"       = "Drupal\aws_cloud\Controller\Vpc\SubnetAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "aws_cloud_subnet",
 *   admin_permission = "administer aws cloud subnet",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id"  ,
 *     "label" = "name",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *     "canonical"            = "/clouds/aws_cloud/{cloud_context}/subnet/{aws_cloud_subnet}",
 *     "edit-form"            = "/clouds/aws_cloud/{cloud_context}/subnet/{aws_cloud_subnet}/edit",
 *     "delete-form"          = "/clouds/aws_cloud/{cloud_context}/subnet/{aws_cloud_subnet}/delete",
 *     "collection"           = "/clouds/aws_cloud/{cloud_context}/subnet",
 *     "delete-multiple-form" = "/clouds/aws_cloud/{cloud_context}/subnet/delete_multiple",
 *   },
 *   field_ui_base_route = "aws_cloud_subnet.settings"
 * )
 */
class Subnet extends CloudContentEntityBase implements SubnetInterface {

  /**
   * {@inheritdoc}
   */
  public function getSubnetId(): ?string {
    return $this->get('subnet_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubnetId($subnet_id = ''): SubnetInterface {
    return $this->set('subnet_id', $subnet_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getVpcId(): ?string {
    return $this->get('vpc_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVpcId($vpc_id = ''): SubnetInterface {
    return $this->set('vpc_id', $vpc_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getCidrBlock(): string {
    return $this->get('cidr_block')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCidrBlock($cidr_block): SubnetInterface {
    return $this->set('cidr_block', $cidr_block);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccountId(): ?string {
    return $this->get('account_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): SubnetInterface {
    return $this->set('account_id', $account_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getState(): string {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state): SubnetInterface {
    return $this->set('state', $state);
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZone(): string {
    return $this->get('availability_zone')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAvailabilityZone($availability_zone): SubnetInterface {
    return $this->set('availability_zone', $availability_zone);
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZoneId(): string {
    return $this->get('availability_zone_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAvailabilityZoneId($availability_zone_id): SubnetInterface {
    return $this->set('availability_zone_id', $availability_zone_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptInStatus(): string {
    return $this->get('opt_in_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOptInStatus($opt_in_status = ''): SubnetInterface {
    return $this->set('opt_in_status', $opt_in_status);
  }

  /**
   * {@inheritdoc}
   */
  public function getRegionName(): string {
    return $this->get('region_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRegionName($region_name = ''): SubnetInterface {
    return $this->set('region_name', $region_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getZoneName(): string {
    return $this->get('zone_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setZoneName($zone_name = ''): SubnetInterface {
    return $this->set('zone_name', $zone_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getZoneId(): string {
    return $this->get('zone_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setZoneId($zone_id = ''): SubnetInterface {
    return $this->set('zone_id', $zone_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupName(): string {
    return $this->get('group_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGroupName($group_name = ''): SubnetInterface {
    return $this->set('group_name', $group_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkBorderGroup(): string {
    return $this->get('network_border_group')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNetworkBorderGroup($network_border_group = ''): SubnetInterface {
    return $this->set('network_border_group', $network_border_group);
  }

  /**
   * {@inheritdoc}
   */
  public function getZoneType(): string {
    return $this->get('zone_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setZoneType($zone_type = ''): SubnetInterface {
    return $this->set('zone_type', $zone_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getParentZoneName(): string {
    return $this->get('parent_zone_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentZoneName($parent_zone_name = ''): SubnetInterface {
    return $this->set('parent_zone_name', $parent_zone_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getParentZoneId(): string {
    return $this->get('parent_zone_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentZoneId($parent_zone_id = ''): SubnetInterface {
    return $this->set('parent_zone_id', $parent_zone_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getTags(): array {
    return $this->get('tags')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): SubnetInterface {
    return $this->set('tags', $tags);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): SubnetInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = 0): SubnetInterface {
    return $this->set('created', $created);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Ec2ServiceVpc entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Ec2ServiceVpc entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Ec2ServiceVpc entity.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['cidr_block'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('CIDR Block'))
      ->setDescription(t('Information about the IPv4 CIDR blocks associated with the VPC.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['subnet_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subnet ID'))
      ->setDescription(t('The Subnet ID.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['vpc_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('VPC'))
      ->setDescription(t('The VPC name or ID.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'aws_cloud_vpc',
          'field_name' => 'vpc_id',
          'html_generator_class' => EntityLinkWithNameHtmlGenerator::class,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('State'))
      ->setDescription(t('The current state of the VPC.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['account_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('AWS account ID'))
      ->setDescription(t('The AWS account ID of the VPC owner, without dashes.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['availability_zone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Availability Zone'))
      ->setDescription(t('The Availability Zone for the subnet.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'availability_zone_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['availability_zone_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Availability Zone ID'))
      ->setDescription(t('The Availability Zone ID for the subnet.'))
      ->setReadOnly(TRUE);

    $fields['opt_in_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Opt-In Status'))
      ->setDescription(t('For Availability Zones, this parameter always has the value of opt-in-not-required. For Local Zones and Wavelength Zones, this parameter is the opt-in status. The possible values are opted-in, and not-opted-in.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['region_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Region name'))
      ->setDescription(t('The name of the region.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['zone_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Zone name'))
      ->setDescription(t('The name of the Availability Zone, Local Zone, or Wavelength Zone.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['zone_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Zone ID'))
      ->setDescription(t('The ID of the Availability Zone, Local Zone, or Wavelength Zone.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['group_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Group name'))
      ->setDescription(t('For Availability Zones, this parameter has the same value as the region name. For Local Zones, the name of the associated group, for example us-west-2-lax-1. For Wavelength Zones, the name of the associated group, for example us-east-1-wl1-bos-wlz-1.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['network_border_group'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Network border group'))
      ->setDescription(t('A unique set of Availability Zones.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['zone_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Zone type'))
      ->setDescription(t('The type of zone. The valid values are availability-zone, local-zone, and wavelength-zone.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['parent_zone_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent Zone name'))
      ->setDescription(t('The name of the zone that handles some of the Local Zone or Wavelength Zone control plane operations, such as API calls.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['parent_zone_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Parent Zone ID'))
      ->setDescription(t('The ID of the zone that handles some of the Local Zone or Wavelength Zone control plane operations, such as API calls.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('Date/time the Amazon VPC was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Ec2ServiceSubnet entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tags'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('Tags.'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'weight' => -5,
        'settings' => [
          'value_converter_class' => AwsCloudValueConverter::class,
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'key_value_item',
        'settings' => [
          'reserved_key_checker_class' => AwsCloudReservedKeyChecker::class,
          'value_converter_class' => AwsCloudValueConverter::class,
        ],
      ])
      ->addConstraint('tags_data');

    return $fields;
  }

}
