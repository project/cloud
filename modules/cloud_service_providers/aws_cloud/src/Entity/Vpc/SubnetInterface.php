<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a subnet entity.
 *
 * @ingroup aws_cloud
 */
interface SubnetInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name);

  /**
   * Get the subnet ID.
   */
  public function getSubnetId(): ?string;

  /**
   * Set subnet ID.
   *
   * @param string $subnet_id
   *   The subnet ID.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\Subnet
   *   The Subnet Entity.
   */
  public function setSubnetId($subnet_id = ''): SubnetInterface;

  /**
   * Get the VPC ID.
   */
  public function getVpcId(): ?string;

  /**
   * Set VPC ID.
   *
   * @param string $vpc_id
   *   The VPC ID.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\Subnet
   *   The Subnet entity.
   */
  public function setVpcId($vpc_id = ''): SubnetInterface;

  /**
   * Get the CIDR block.
   */
  public function getCidrBlock(): string;

  /**
   * Set CIDR block.
   *
   * @param string $cidr_block
   *   The CIDR block.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\Subnet
   *   The Subnet entity.
   */
  public function setCidrBlock($cidr_block);

  /**
   * Get the tags.
   */
  public function getTags(): array;

  /**
   * Set the tags.
   *
   * @param array $tags
   *   The tags.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\Subnet
   *   The Subnet entity.
   */
  public function setTags(array $tags): SubnetInterface;

  /**
   * Get the account ID.
   */
  public function getAccountId(): ?string;

  /**
   * Set account ID.
   *
   * @param string $account_id
   *   The account ID.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\Subnet
   *   The Subnet entity.
   */
  public function setAccountId($account_id): SubnetInterface;

  /**
   * Get the state.
   */
  public function getState(): string;

  /**
   * Set state.
   *
   * @param string $state
   *   The state.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\Subnet
   *   The Subnet entity.
   */
  public function setState($state): SubnetInterface;

  /**
   * Get the Availability Zone.
   */
  public function getAvailabilityZone(): string;

  /**
   * Set the Availability Zone.
   *
   * @param string $availability_zone
   *   The Availability Zone.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\Subnet
   *   The Subnet entity.
   */
  public function setAvailabilityZone($availability_zone): SubnetInterface;

  /**
   * Get the Availability Zone ID.
   */
  public function getAvailabilityZoneId(): string;

  /**
   * Set the Availability Zone ID.
   *
   * @param string $availability_zone_id
   *   The Availability Zone ID.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\Subnet
   *   The Subnet entity.
   */
  public function setAvailabilityZoneId($availability_zone_id): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getOptInStatus(): string;

  /**
   * {@inheritdoc}
   */
  public function setOptInStatus($opt_in_status = ''): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getRegionName(): string;

  /**
   * {@inheritdoc}
   */
  public function setRegionName($region_name = ''): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getZoneName(): string;

  /**
   * {@inheritdoc}
   */
  public function setZoneName($zone_name = ''): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getZoneId(): string;

  /**
   * {@inheritdoc}
   */
  public function setZoneId($zone_id = ''): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getGroupName(): string;

  /**
   * {@inheritdoc}
   */
  public function setGroupName($group_name = ''): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getNetworkBorderGroup(): string;

  /**
   * {@inheritdoc}
   */
  public function setNetworkBorderGroup($network_border_group = ''): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getZoneType(): string;

  /**
   * {@inheritdoc}
   */
  public function setZoneType($zone_type = ''): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getParentZoneName(): string;

  /**
   * {@inheritdoc}
   */
  public function setParentZoneName($parent_zone_name = ''): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getParentZoneId(): string;

  /**
   * {@inheritdoc}
   */
  public function setParentZoneId($parent_zone_id = ''): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = 0): SubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): SubnetInterface;

}
