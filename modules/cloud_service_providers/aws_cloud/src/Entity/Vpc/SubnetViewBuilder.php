<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the subnet view builders.
 */
class SubnetViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'subnet',
        'title' => $this->t('Subnet'),
        'open' => TRUE,
        'fields' => [
          'cidr_block',
          'subnet_id',
          'vpc_id',
          'state',
          'availability_zone',
          'availability_zone_id',
          'opt_in_status',
          'region_name',
          'zone_name',
          'zone_id',
          'group_name',
          'network_border_group',
          'zone_type',
          'parent_zone_name',
          'parent_zone_id',
          'account_id',
          'created',
        ],
      ],
      [
        'name' => 'fieldset_tags',
        'title' => $this->t('Tags'),
        'open' => TRUE,
        'fields' => [
          'tags',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
