<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\aws_cloud\Plugin\Field\Util\AwsCloudReservedKeyChecker;
use Drupal\aws_cloud\Plugin\Field\Util\AwsCloudValueConverter;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the transit gateway entity.
 *
 * @ingroup aws_cloud
 *
 * @ContentEntityType(
 *   id = "aws_cloud_transit_gateway",
 *   id_plural = "aws_cloud_transit_gateways",
 *   label = @Translation("Transit gateway"),
 *   label_collection = @Translation("Transit gateways"),
 *   label_singular = @Translation("Transit gateway"),
 *   label_plural = @Translation("Transit gateways"),
 *   handlers = {
 *     "view_builder" = "Drupal\aws_cloud\Entity\Vpc\TransitGatewayViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\aws_cloud\Entity\Vpc\TransitGatewayViewsData",
 *     "form" = {
 *       "default"                 = "Drupal\aws_cloud\Form\Vpc\TransitGatewayEditForm",
 *       "add"                     = "Drupal\aws_cloud\Form\Vpc\TransitGatewayCreateForm",
 *       "edit"                    = "Drupal\aws_cloud\Form\Vpc\TransitGatewayEditForm",
 *       "delete"                  = "Drupal\aws_cloud\Form\Vpc\TransitGatewayDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\aws_cloud\Form\Vpc\TransitGatewayDeleteMultipleForm",
 *     },
 *     "access"       = "Drupal\aws_cloud\Controller\Vpc\TransitGatewayAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "aws_cloud_transit_gateway",
 *   admin_permission = "administer aws cloud vpc",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id"  ,
 *     "label" = "name",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *     "canonical"            = "/clouds/aws_cloud/{cloud_context}/transit_gateway/{aws_cloud_transit_gateway}",
 *     "edit-form"            = "/clouds/aws_cloud/{cloud_context}/transit_gateway/{aws_cloud_transit_gateway}/edit",
 *     "delete-form"          = "/clouds/aws_cloud/{cloud_context}/transit_gateway/{aws_cloud_transit_gateway}/delete",
 *     "collection"           = "/clouds/aws_cloud/{cloud_context}/transit_gateway",
 *     "delete-multiple-form" = "/clouds/aws_cloud/{cloud_context}/transit_gateway/delete_multiple",
 *   },
 *   field_ui_base_route = "aws_cloud_transit_gateway.settings"
 * )
 */
class TransitGateway extends CloudContentEntityBase implements TransitGatewayInterface {

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): TransitGatewayInterface {
    return $this->set('description', $description);
  }

  /**
   * {@inheritdoc}
   */
  public function getTransitGatewayId(): ?string {
    return $this->get('transit_gateway_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTransitGatewayId($transit_gateway_id = ''): TransitGatewayInterface {
    return $this->set('transit_gateway_id', $transit_gateway_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccountId(): ?string {
    return $this->get('account_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): TransitGatewayInterface {
    return $this->set('account_id', $account_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getState(): string {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state): TransitGatewayInterface {
    return $this->set('state', $state);
  }

  /**
   * {@inheritdoc}
   */
  public function getAmazonSideAsn(): ?string {
    return $this->get('amazon_side_asn')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAmazonSideAsn($amazon_side_asn): TransitGatewayInterface {
    return $this->set('amazon_side_asn', $amazon_side_asn);
  }

  /**
   * {@inheritdoc}
   */
  public function getAssociationDefaultRouteTableId(): ?string {
    return $this->get('association_default_route_table_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAssociationDefaultRouteTableId($association_default_route_table_id): TransitGatewayInterface {
    return $this->set('association_default_route_table_id', $association_default_route_table_id);
  }

  /**
   * {@inheritdoc}
   */
  public function isAutoAcceptSharedAttachments(): bool {
    return $this->get('auto_accept_shared_attachments')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAutoAcceptSharedAttachments($auto_accept_shared_attachments): TransitGatewayInterface {
    return $this->set('auto_accept_shared_attachments', $auto_accept_shared_attachments);
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultRouteTableAssociation(): bool {
    return $this->get('default_route_table_association')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultRouteTableAssociation($default_route_table_association): TransitGatewayInterface {
    return $this->set('default_route_table_association', $default_route_table_association);
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultRouteTablePropagation(): bool {
    return $this->get('default_route_table_propagation')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultRouteTablePropagation($default_route_table_propagation): TransitGatewayInterface {
    return $this->set('default_route_table_propagation', $default_route_table_propagation);
  }

  /**
   * {@inheritdoc}
   */
  public function isDnsSupport(): bool {
    return $this->get('dns_support')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDnsSupport($dns_support): TransitGatewayInterface {
    return $this->set('dns_support', $dns_support);
  }

  /**
   * {@inheritdoc}
   */
  public function isMulticastSupport(): bool {
    return $this->get('multicast_support')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMulticastSupport($multicast_support): TransitGatewayInterface {
    return $this->set('multicast_support', $multicast_support);
  }

  /**
   * {@inheritdoc}
   */
  public function getPropagationDefaultRouteTableId(): ?string {
    return $this->get('propagation_default_route_table_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPropagationDefaultRouteTableId($propagation_default_route_table_id): TransitGatewayInterface {
    return $this->set('propagation_default_route_table_id', $propagation_default_route_table_id);
  }

  /**
   * {@inheritdoc}
   */
  public function isVpnEcmpSupport(): bool {
    return $this->get('vpn_ecmp_support')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVpnEcmpSupport($vpn_ecmp_support): TransitGatewayInterface {
    return $this->set('vpn_ecmp_support', $vpn_ecmp_support);
  }

  /**
   * {@inheritdoc}
   */
  public function getTags(): array {
    return $this->get('tags')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): TransitGatewayInterface {
    return $this->set('tags', $tags);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): TransitGatewayInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = 0): TransitGatewayInterface {
    return $this->set('created', $created);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the TransitGateway entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the TransitGateway entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the TransitGateway entity.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Description of transit gateway.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['transit_gateway_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Transit gateway ID'))
      ->setDescription(t('The transit gateway ID.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('State'))
      ->setDescription(t('The current state of the transit gateway.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['account_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('AWS account ID'))
      ->setDescription(t('The AWS account ID of the transit gateway owner, without dashes.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['amazon_side_asn'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Amazon Side ASN'))
      ->setDescription(t('A private Autonomous System Number (ASN) for the Amazon side of a BGP session. The range is 64512 to 65534 for 16-bit ASNs and 4200000000 to 4294967294 for 32-bit ASNs.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['association_default_route_table_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Association Default Route Table ID'))
      ->setDescription(t('The ID of the default association route table.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['auto_accept_shared_attachments'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Auto Accept Shared Attachments'))
      ->setDescription(t('Indicates whether attachment requests are automatically accepted.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('enable'),
        'off_label' => t('disable'),
      ])
      ->setReadOnly(TRUE);

    $fields['default_route_table_association'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Default Route Table Association'))
      ->setDescription(t('Indicates whether resource attachments are automatically associated with the default association route table.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('enable'),
        'off_label' => t('disable'),
      ])
      ->setReadOnly(TRUE);

    $fields['default_route_table_propagation'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Default Route Table Propagation'))
      ->setDescription(t('Indicates whether resource attachments automatically propagate routes to the default propagation route table.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('enable'),
        'off_label' => t('disable'),
      ])
      ->setReadOnly(TRUE);

    $fields['dns_support'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('DNS Support'))
      ->setDescription(t('Indicates whether DNS support is enabled.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('enable'),
        'off_label' => t('disable'),
      ])
      ->setReadOnly(TRUE);

    $fields['multicast_support'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Multicast Support'))
      ->setDescription(t('Indicates whether multicast is enabled on the transit gateway.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('enable'),
        'off_label' => t('disable'),
      ])
      ->setReadOnly(TRUE);

    $fields['propagation_default_route_table_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Propagation Default Route Table ID'))
      ->setDescription(t('The ID of the default propagation route table.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['vpn_ecmp_support'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('VPN ECMP Support'))
      ->setDescription(t('Indicates whether Equal Cost Multipath Protocol support is enabled.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('enable'),
        'off_label' => t('disable'),
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('Date/time the Amazon transit gateway was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the transit gateway entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tags'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('Tags.'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'weight' => -5,
        'settings' => [
          'value_converter_class' => AwsCloudValueConverter::class,
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'key_value_item',
        'settings' => [
          'reserved_key_checker_class' => AwsCloudReservedKeyChecker::class,
          'value_converter_class' => AwsCloudValueConverter::class,
        ],
      ])
      ->addConstraint('tags_data');

    return $fields;
  }

}
