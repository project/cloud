<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a transit gateway entity.
 *
 * @ingroup aws_cloud
 */
interface TransitGatewayInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name);

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getTransitGatewayId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setTransitGatewayId($transit_gateway_id = ''): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getAccountId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setAccountId($account_id): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getState(): string;

  /**
   * {@inheritdoc}
   */
  public function setState($state): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getAmazonSideAsn(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setAmazonSideAsn($amazon_side_asn): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getAssociationDefaultRouteTableId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setAssociationDefaultRouteTableId($association_default_route_table_id): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function isAutoAcceptSharedAttachments(): bool;

  /**
   * {@inheritdoc}
   */
  public function setAutoAcceptSharedAttachments($auto_accept_shared_attachments): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function isDefaultRouteTableAssociation(): bool;

  /**
   * {@inheritdoc}
   */
  public function setDefaultRouteTableAssociation($default_route_table_association): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function isDefaultRouteTablePropagation(): bool;

  /**
   * {@inheritdoc}
   */
  public function setDefaultRouteTablePropagation($default_route_table_propagation): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function isDnsSupport(): bool;

  /**
   * {@inheritdoc}
   */
  public function setDnsSupport($dns_support): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function isMulticastSupport(): bool;

  /**
   * {@inheritdoc}
   */
  public function setMulticastSupport($multicast_support): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getPropagationDefaultRouteTableId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setPropagationDefaultRouteTableId($propagation_default_route_table_id): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function isVpnEcmpSupport(): bool;

  /**
   * {@inheritdoc}
   */
  public function setVpnEcmpSupport($vpn_ecmp_support): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getTags(): array;

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = 0): TransitGatewayInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

}
