<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the TransitGatewayRoute entity.
 *
 * Because the entity_type_id should be less than 32 characters,
 * I use aws_cloud_transit_gateway_route instead of
 * aws_cloud_transit_gateway_route_table.
 *
 * @ingroup aws_cloud
 *
 * @ContentEntityType(
 *   id = "aws_cloud_transit_gateway_route",
 *   id_plural = "aws_cloud_transit_gateway_routes",
 *   label = @Translation("Transit gateway route"),
 *   label_collection = @Translation("Transit gateway routes"),
 *   label_singular = @Translation("Transit gateway route"),
 *   label_plural = @Translation("Transit gateway routes"),
 *   handlers = {
 *     "view_builder" = "Drupal\aws_cloud\Entity\Vpc\TransitGatewayRouteViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\aws_cloud\Entity\Vpc\TransitGatewayRouteViewsData",
 *     "form" = {
 *     },
 *     "access"       = "Drupal\aws_cloud\Controller\Vpc\TransitGatewayRouteAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "aws_cloud_transit_gateway_route",
 *   admin_permission = "administer aws cloud transit gateway route",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "transit_gateway_route_table_id",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *   },
 *   field_ui_base_route = "aws_cloud_transit_gateway_route.settings"
 * )
 */
class TransitGatewayRoute extends CloudContentEntityBase implements TransitGatewayRouteInterface {

  /**
   * {@inheritdoc}
   */
  public function getTransitGatewayRouteTableId(): ?string {
    return $this->get('transit_gateway_route_table_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTransitGatewayRouteTableId($transit_gateway_route_table_id = ''): TransitGatewayRouteInterface {
    return $this->set('transit_gateway_route_table_id', $transit_gateway_route_table_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getTransitGatewayId(): ?string {
    return $this->get('transit_gateway_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTransitGatewayId($transit_gateway_id = ''): TransitGatewayRouteInterface {
    return $this->set('transit_gateway_id', $transit_gateway_id);
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultAssociationRouteTable(): bool {
    return $this->get('default_association_route_table')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultAssociationRouteTable($default_association_route_table): TransitGatewayRouteInterface {
    return $this->set('default_association_route_table', $default_association_route_table);
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultPropagationRouteTable(): bool {
    return $this->get('default_propagation_route_table')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultPropagationRouteTable($default_propagation_route_table): TransitGatewayRouteInterface {
    return $this->set('default_propagation_route_table', $default_propagation_route_table);
  }

  /**
   * {@inheritdoc}
   */
  public function getState(): ?string {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state = ''): TransitGatewayRouteInterface {
    return $this->set('state', $state);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): TransitGatewayRouteInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the transit gateway route table entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the transit gateway route table entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['transit_gateway_route_table_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Transit gateway route table ID'))
      ->setDescription(t('The transit gateway route table ID of the transit gateway route table entity.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE);

    $fields['transit_gateway_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Transit gateway ID'))
      ->setDescription(t("The ID of the transit gateway."))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['default_association_route_table'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Default Association Route Table'))
      ->setDescription(t('Indicates whether this is the default association route table for the transit gateway.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('yes'),
        'off_label' => t('no'),
      ])
      ->setReadOnly(TRUE);

    $fields['default_propagation_route_table'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Default Propagation Route Table'))
      ->setDescription(t('Indicates whether this is the default propagation route table for the transit gateway.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('yes'),
        'off_label' => t('no'),
      ])
      ->setReadOnly(TRUE);

    $fields['state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('State'))
      ->setDescription(t('The state of the transit gateway route table.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the AMI was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the transit gateway route table entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
