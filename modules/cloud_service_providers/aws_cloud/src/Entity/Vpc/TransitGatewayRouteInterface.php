<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an TransitGatewayRouteTable entity.
 *
 * @ingroup aws_cloud
 */
interface TransitGatewayRouteInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getTransitGatewayRouteTableId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setTransitGatewayRouteTableId($transit_gateway_route_table_id = ''): TransitGatewayRouteInterface;

  /**
   * {@inheritdoc}
   */
  public function getTransitGatewayId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setTransitGatewayId($transit_gateway_id = ''): TransitGatewayRouteInterface;

  /**
   * {@inheritdoc}
   */
  public function isDefaultAssociationRouteTable(): bool;

  /**
   * {@inheritdoc}
   */
  public function setDefaultAssociationRouteTable($default_association_route_table): TransitGatewayRouteInterface;

  /**
   * {@inheritdoc}
   */
  public function isDefaultPropagationRouteTable(): bool;

  /**
   * {@inheritdoc}
   */
  public function setDefaultPropagationRouteTable($default_propagation_route_table): TransitGatewayRouteInterface;

  /**
   * {@inheritdoc}
   */
  public function getState(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setState($state = ''): TransitGatewayRouteInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): TransitGatewayRouteInterface;

}
