<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the transit gateway route table view builders.
 */
class TransitGatewayRouteViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'transit_gateway_route_table',
        'title' => $this->t('Transit gateway'),
        'open' => TRUE,
        'fields' => [
          'transit_gateway_route_table_id',
          'transit_gateway_id',
          'state',
          'default_association_route_table',
          'default_propagation_route_table',
          'created',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
