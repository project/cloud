<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the transit gateway view builders.
 */
class TransitGatewayViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'transit_gateway',
        'title' => $this->t('Transit gateway'),
        'open' => TRUE,
        'fields' => [
          'transit_gateway_id',
          'state',
          'account_id',
          'amazon_side_asn',
          'dns_support',
          'vpn_ecmp_support',
          'auto_accept_shared_attachments',
          'multicast_support',
          'default_route_table_association',
          'association_default_route_table_id',
          'default_route_table_propagation',
          'propagation_default_route_table_id',
          'created',
        ],
      ],
      [
        'name' => 'fieldset_tags',
        'title' => $this->t('Tags'),
        'open' => TRUE,
        'fields' => [
          'tags',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
