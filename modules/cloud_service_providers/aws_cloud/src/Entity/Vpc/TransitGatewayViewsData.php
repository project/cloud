<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewsData;

/**
 * Provides the views data for the transit gateway entity type.
 */
class TransitGatewayViewsData extends AwsCloudViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['aws_cloud_transit_gateway']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('AWS Cloud transit gateway'),
      'help'  => $this->t('The AWS Cloud transit gateway entity ID.'),
    ];

    $data['aws_cloud_transit_gateway']['transit_gateway_bulk_form'] = [
      'title' => $this->t('Transit gateway operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple transit gateways.'),
      'field' => [
        'id' => 'transit_gateway_bulk_form',
      ],
    ];

    $table_name = $this->storage->getEntityTypeId();
    $fields = $this->getFieldStorageDefinitions();

    // The following is a list of fields to turn from text search to
    // select list.  This list can be expanded through hook_views_data_alter().
    $selectable = [
      'transit_gateway_id',
      'state',
      'account_id',
    ];

    $data['aws_cloud_transit_gateway']['table']['base']['access query tag'] = 'aws_cloud_transit_gateway_views_access';
    $this->addDropdownSelector($data, $table_name, $fields, $selectable);

    return $data;
  }

}
