<?php

namespace Drupal\aws_cloud\Entity\Vpc;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a VPC peering connection entity.
 *
 * @ingroup aws_cloud
 */
interface VpcPeeringConnectionInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * Get the VPC peering connection ID.
   */
  public function getVpcPeeringConnectionId(): ?string;

  /**
   * Set the VPC peering connection ID.
   *
   * @param string $vpc_peering_connection_id
   *   The VPC peering connection ID.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setVpcPeeringConnectionId($vpc_peering_connection_id): VpcPeeringConnectionInterface;

  /**
   * Get the Requester Account ID.
   */
  public function getRequesterAccountId(): ?string;

  /**
   * Set Requester Account ID.
   *
   * @param string $requester_account_id
   *   The Requester Account ID.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setRequesterAccountId($requester_account_id): VpcPeeringConnectionInterface;

  /**
   * Get the Requester VPC ID.
   */
  public function getRequesterVpcId(): ?string;

  /**
   * Set Requester VPC ID.
   *
   * @param string $requester_vpc_id
   *   The Requester VPC ID.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setRequesterVpcId($requester_vpc_id): VpcPeeringConnectionInterface;

  /**
   * Get the Requester CIDR block.
   */
  public function getRequesterCidrBlock(): ?string;

  /**
   * Set Requester CIDR block.
   *
   * @param string $requester_cidr_block
   *   The Requester CIDR Block.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setRequesterCidrBlock($requester_cidr_block): VpcPeeringConnectionInterface;

  /**
   * Get the requester region.
   */
  public function getRequesterRegion(): string;

  /**
   * Set requester region.
   *
   * @param string $requester_region
   *   The requester region.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setRequesterRegion($requester_region): VpcPeeringConnectionInterface;

  /**
   * Get the accepter account ID.
   */
  public function getAccepterAccountId(): ?string;

  /**
   * Set accepter account ID.
   *
   * @param string $accepter_account_id
   *   The accepter account ID.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setAccepterAccountId($accepter_account_id): VpcPeeringConnectionInterface;

  /**
   * Get the accepter VPC ID.
   */
  public function getAccepterVpcId(): ?string;

  /**
   * Set accepter VPC ID.
   *
   * @param string $accepter_vpc_id
   *   The accepter VPC ID.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setAccepterVpcId($accepter_vpc_id): VpcPeeringConnectionInterface;

  /**
   * Get the accepter CIDR block.
   */
  public function getAccepterCidrBlock(): ?string;

  /**
   * Set accepter CIDR block.
   *
   * @param string $accepter_cidr_block
   *   The accepter CIDR Block.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setAccepterCidrBlock($accepter_cidr_block): VpcPeeringConnectionInterface;

  /**
   * Get the accepter region.
   */
  public function getAccepterRegion(): string;

  /**
   * Set accepter region.
   *
   * @param string $accepter_region
   *   The accepter region.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setAccepterRegion($accepter_region): VpcPeeringConnectionInterface;

  /**
   * Get the status code.
   */
  public function getStatusCode(): string;

  /**
   * Set status code.
   *
   * @param string $status_code
   *   The status code.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setStatusCode($status_code): VpcPeeringConnectionInterface;

  /**
   * Get the status message.
   */
  public function getStatusMessage(): ?string;

  /**
   * Set status message.
   *
   * @param string $status_message
   *   The status message.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setStatusMessage($status_message): VpcPeeringConnectionInterface;

  /**
   * Get the expiration time.
   */
  public function getExpirationTime(): string;

  /**
   * Set expiration time.
   *
   * @param int $expiration_time
   *   The expiration time.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection
   *   The VPC peering connection entity.
   */
  public function setExpirationTime($expiration_time): VpcPeeringConnectionInterface;

  /**
   * Get the tags.
   */
  public function getTags(): array;

  /**
   * Set the tags.
   *
   * @param array $tags
   *   The tags.
   *
   * @return \Drupal\aws_cloud\Entity\Vpc\Vpc
   *   The VPC entity.
   */
  public function setTags(array $tags): VpcPeeringConnectionInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = 0): VpcPeeringConnectionInterface;

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): VpcPeeringConnectionInterface;

}
