<?php

namespace Drupal\aws_cloud\EventSubscriber;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceException;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Event\CloudEntityEvent;
use Drupal\cloud\Event\CloudEntityEventType;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to handle updating duplicate AWS Cloud service providers.
 */
class AwsEntityUpdateSubscriber implements EventSubscriberInterface {

  use CloudContentEntityTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The Ec2Service object.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  private $ec2Service;

  /**
   * AwsEntityUpdateSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud config plugin manager.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The EC2 Service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CloudConfigPluginManagerInterface $cloud_config_plugin_manager, Ec2ServiceInterface $ec2_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->ec2Service = $ec2_service;
  }

  /**
   * Delete AWS launch template.
   *
   * @param \Drupal\cloud\Event\CloudEntityEvent $event
   *   The event object.
   *
   * @return bool
   *   TRUE if successful, otherwise return FALSE.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   Throws Ec2ServiceException if an error occurs.
   */
  public function deleteLaunchTemplate(CloudEntityEvent $event): bool {

    $entity = $event->getEntity();

    if (empty($entity)
      || $entity->getEntityTypeId() !== 'cloud_launch_template'
      || $entity->bundle() !== 'aws_cloud') {
      return FALSE;
    }

    try {

      $params = [
        'LaunchTemplateName' => $entity->getName(),
      ];
      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $this->ec2Service->deleteLaunchTemplate($params);
    }
    catch (\Exception $e) {
      throw new Ec2ServiceException('Failed to delete AWS launch template.');
    }

    return TRUE;
  }

  /**
   * Update any duplicate cloud service providers.
   *
   * @param \Drupal\cloud\Event\CloudEntityEvent $event
   *   The event object.
   */
  public function updateDuplicateProvider(CloudEntityEvent $event): void {
    $entity = $event->getEntity();
    $duplicate_regions = $this->getDuplicateCloudServiceProviders($event->getEntity());

    // Update the different regions entities.
    foreach ($duplicate_regions ?: [] as $region) {
      if ($region->isRemote()) {
        continue;
      }

      $this->ec2Service->setCloudContext($region->getCloudContext());
      // If any entity needs custom update calls, use the case statement
      // to break away from the default behavior.
      switch ($entity->getEntityTypeId()) {
        case 'aws_cloud_instance':
        case 'aws_cloud_elastic_ip':
          // Both instance and Elastic IPs need these update calls.
          $this->ec2Service->updateInstances();
          $this->ec2Service->updateElasticIps();
          $this->ec2Service->updateNetworkInterfaces();
          break;

        case 'aws_cloud_image':
          $this->ec2Service->updateImages([
            'Owners' => [
              $region->get('field_account_id')->value,
            ],
          ], TRUE);
          break;

        case 'cloud_launch_template':
          if ($entity->bundle() === 'aws_cloud') {
            $this->ec2Service->updateCloudLaunchTemplates();
          }
          break;

        default:
          // Default update methods can be programmatically derived.
          $plural_entity_name = $this->getShortEntityTypeNamePluralCamel($entity);
          $func = "update{$plural_entity_name}";
          if (method_exists($this->ec2Service, $func)) {
            $this->ec2Service->$func();
          }
          else {
            $this->logger('AwsEntityUpdateSubscriber')
              ->error($this->t('@func not found', [
                '@func' => $func,
              ]));
          }
          break;
      }
    }

  }

  /**
   * Update any duplicate instances when a Launch template is launched.
   *
   * @param \Drupal\cloud\Event\CloudEntityEvent $event
   *   The event object.
   */
  public function updateInstances(CloudEntityEvent $event): void {
    $entity = $event->getEntity();
    $duplicate_regions = $this->getDuplicateCloudServiceProviders($event->getEntity());

    if ($entity->getEntityTypeId() === 'cloud_launch_template' && $entity->bundle() === 'aws_cloud') {
      // Update the different regions entities.
      foreach ($duplicate_regions ?: [] as $region) {
        $this->ec2Service->setCloudContext($region->getCloudContext());
        $this->ec2Service->updateInstances();
      }
    }
  }

  /**
   * Find duplicate regions.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Cloud service provider entity.
   *
   * @return array
   *   Array of duplicate regions.
   */
  private function getDuplicateCloudServiceProviders(EntityInterface $entity): array {
    $duplicate_regions = [];
    try {
      $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
      $config_entity = $this->cloudConfigPluginManager->loadConfigEntity();
      // Only look up duplicate regions for AWS.
      if ($config_entity->bundle() === 'aws_cloud') {
        $dup_ids = $this->entityTypeManager->getStorage('cloud_config')
          ->getQuery()
          ->accessCheck(TRUE)
          ->condition('type', 'aws_cloud')
          ->condition('field_account_id', $config_entity->get('field_account_id')->value)
          ->condition('field_region', $config_entity->get('field_region')->value)
          ->condition('id', [$config_entity->id()], 'NOT IN')
          ->execute();

        $duplicate_regions = $this->entityTypeManager
          ->getStorage('cloud_config')
          ->loadMultiple($dup_ids);
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $duplicate_regions;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      CloudEntityEventType::FORM_SUBMIT => 'updateDuplicateProvider',
      CloudEntityEventType::FORM_SAVE => 'updateDuplicateProvider',
      CloudEntityEventType::FORM_MULTI_SUBMIT => 'updateDuplicateProvider',
      // When a template is launched. Update instances.
      CloudEntityEventType::TEMPLATE_LAUNCH => 'updateInstances',
      // When an AWS launch template is deleted.
      CloudEntityEventType::LAUNCH_TEMPLATE_DELETE => 'deleteLaunchTemplate',
    ];
  }

}
