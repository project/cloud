<?php

namespace Drupal\aws_cloud\Form\CloudFormation;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class AwsDeleteUpdateEntityForm - Base Delete class.
 *
 * This class inject entity update into build form.
 *
 * @package Drupal\aws_cloud\Form\CloudFormation
 */
class AwsCloudFormationDeleteUpdateEntityForm extends AwsCloudFormationDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;
    return !$this->updateEntityBuildForm($entity)
      ? $this->redirectUrl
      : $form;
  }

}
