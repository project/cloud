<?php

namespace Drupal\aws_cloud\Form\CloudFormation;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting a stack entity.
 *
 * @ingroup aws_cloud
 */
class StackDeleteForm extends AwsCloudFormationDeleteUpdateEntityForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $entity = $this->entity;
    $this->cloudFormationService->setCloudContext($entity->getCloudContext());

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->cloudFormationService->deleteStack([
      'StackName' => $entity->getStackName(),
    ]);

    // If $result is NULL, some error should have occurred
    // when calling the AWS API.
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return;
    }

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));
      return;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
  }

}
