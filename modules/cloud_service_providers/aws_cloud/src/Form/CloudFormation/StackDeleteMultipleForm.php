<?php

namespace Drupal\aws_cloud\Form\CloudFormation;

use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Provides an entities deletion confirmation form.
 */
class StackDeleteMultipleForm extends AwsCloudCloudFormationDeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  protected function processCloudResource(CloudContentEntityBase $entity): bool {

    $this->cloudFormationService->setCloudContext($entity->getCloudContext());

    return $this->cloudFormationService->deleteStack(
      ['StackName' => $entity->getStackName()]
      ) !== NULL;
  }

}
