<?php

namespace Drupal\aws_cloud\Form\Config;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\gapps\Service\GoogleSpreadsheetService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AWS Cloud admin notification settings.
 */
class AwsCloudAdminNotificationSettings extends ConfigFormBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The Google spreadsheet service.
   *
   * @var \Drupal\gapps\Service\GoogleSpreadsheetService
   */
  protected $googleSpreadsheetService;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a AwsCloudAdminNotificationSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\gapps\Service\GoogleSpreadsheetService $google_spreadsheet_service
   *   The Google spreadsheet service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    ModuleHandlerInterface $module_handler,
    GoogleSpreadsheetService $google_spreadsheet_service,
    TypedConfigManagerInterface $typed_config_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->fileSystem = $file_system;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->moduleHandler = $module_handler;
    $this->googleSpreadsheetService = $google_spreadsheet_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('module_handler'),
      $container->get('gapps.google_spreadsheet'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'aws_cloud_admin_notification_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['aws_cloud.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('aws_cloud.settings');

    $form['instance'] = [
      '#type' => 'details',
      '#title' => $this->t('Instance'),
      '#open' => TRUE,
    ];

    $form['instance']['notification_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Notification Settings'),
      '#open' => TRUE,
    ];

    $frequency_options = [
      86400 => $this->t('Once a day'),
      604800 => $this->t('Once every 7 days'),
      2592000 => $this->t('Once every 1 months'),
      5184000 => $this->t('Once every 2 months'),
      7776000 => $this->t('Once every 3 months'),
      15552000 => $this->t('Once every 6 months'),
      31104000 => $this->t('Once a year'),
    ];

    $form['instance']['notification_settings']['aws_cloud_notification_frequency'] = [
      '#type' => 'select',
      '#options' => $frequency_options,
      '#title' => $this->t('Notification frequency'),
      '#description' => $this->t('Instance owners will be notified once per option selected.'),
      '#default_value' => $config->get('aws_cloud_notification_frequency'),
    ];

    $form['instance']['notification_settings']['aws_cloud_instance_notification_fields'] = [
      '#type' => 'fieldgroup',
    ];

    $form['instance']['notification_settings']['aws_cloud_instance_notification_fields']['aws_cloud_instance_notification_title'] = [
      '#type' => 'item',
      '#title' => $this->t('Notification time'),
    ];

    $form['instance']['notification_settings']['aws_cloud_instance_notification_fields']['aws_cloud_instance_notification_hour'] = [
      '#type' => 'select',
      '#prefix' => '<div class="container-inline">',
      '#options' => $this->getDigits(24),
      '#default_value' => $config->get('aws_cloud_instance_notification_hour'),
    ];

    $form['instance']['notification_settings']['aws_cloud_instance_notification_fields']['aws_cloud_instance_notification_minutes'] = [
      '#prefix' => ': ',
      '#type' => 'select',
      '#options' => $this->getDigits(60),
      '#default_value' => $config->get('aws_cloud_instance_notification_minutes'),
      '#suffix' => '</div>' . $this->t('Time to send the instance usage email.'),
    ];

    $form['instance']['long_running_instance_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Long running EC2 instance settings'),
      '#open' => TRUE,
    ];

    $form['instance']['long_running_instance_settings']['aws_cloud_long_running_instance_notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable instance notification'),
      '#description' => $this->t('When enabled, instance owners or admins will be notified if their instance has been running for too long.'),
      '#default_value' => $config->get('aws_cloud_long_running_instance_notification'),
    ];

    $form['instance']['long_running_instance_settings']['aws_cloud_long_running_instance_notify_owner'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify owner'),
      '#description' => $this->t('When selected, instance owners will be notified.'),
      '#default_value' => $config->get('aws_cloud_long_running_instance_notify_owner'),
    ];

    $form['instance']['long_running_instance_settings']['aws_cloud_long_running_instance_notification_criteria'] = [
      '#type' => 'select',
      '#options' => [
        1 => $this->t('1 day'),
        30 => $this->t('30 days'),
        60 => $this->t('60 days'),
        90 => $this->t('90 days'),
        180 => $this->t('180 days'),
        365 => $this->t('One year'),
      ],
      '#title' => $this->t('Notification criteria'),
      '#description' => $this->t('Notify instance owners after an instance has been running for this period of time.'),
      '#default_value' => $config->get('aws_cloud_long_running_instance_notification_criteria'),
    ];

    $form['instance']['long_running_instance_settings']['aws_cloud_long_running_instance_notification_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email addresses'),
      '#description' => $this->t('Email addresses to be notified.  The email addresses can be comma separated.'),
      '#default_value' => $config->get('aws_cloud_long_running_instance_notification_emails'),
    ];

    $form['instance']['long_running_instance_settings']['aws_cloud_long_running_instance_notification_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject.'),
      '#default_value' => $config->get('aws_cloud_long_running_instance_notification_subject'),
    ];

    $form['instance']['long_running_instance_settings']['aws_cloud_long_running_instance_notification_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#default_value' => $config->get('aws_cloud_long_running_instance_notification_msg'),
      '#description' => $this->t('Available tokens are: [aws_cloud_instance:instances], [site:url].  The [aws_cloud_instance:instances] variable can be configured in the instance information below.'),
    ];

    $form['instance']['long_running_instance_settings']['aws_cloud_long_running_instance_notification_instance_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Instance information'),
      '#default_value' => $config->get('aws_cloud_long_running_instance_notification_instance_info'),
      '#description' => $this->t('More than one instance can appear in the email message. Available tokens are: [aws_cloud_instance:name], [aws_cloud_instance:id], [aws_cloud_instance:launch_time], [aws_cloud_instance:instance_state], [aws_cloud_instance:availability_zone], [aws_cloud_instance:private_ip], [aws_cloud_instance:public_up], [aws_cloud_instance:elastic_ip], [aws_cloud_instance:instance_link], [aws_cloud_instance:instance_edit_link].'),
    ];

    $form['instance']['low_utilization_instance_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Low utilization EC2 instance settings'),
      '#open' => TRUE,
    ];

    $form['instance']['low_utilization_instance_settings']['aws_cloud_low_utilization_instance_notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable instance notification'),
      '#description' => $this->t('When enabled, instance owners or admins will be notified if their instance has been running for in low utilization status for several days.'),
      '#default_value' => $config->get('aws_cloud_low_utilization_instance_notification'),
    ];

    $form['instance']['low_utilization_instance_settings']['aws_cloud_low_utilization_instance_notify_owner'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify owner'),
      '#description' => $this->t('When selected, instance owners will be notified.'),
      '#default_value' => $config->get('aws_cloud_low_utilization_instance_notify_owner'),
    ];

    $form['instance']['low_utilization_instance_settings']['aws_cloud_low_utilization_instance_cpu_utilization_threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('CPU Utilization Threshold'),
      '#description' => $this->t('The CPU utilization threshold of low utilization instance (The input range is 1-100%; e.g. 10% or less as the default value).'),
      '#size' => 3,
      '#min' => 1,
      '#max' => 100,
      '#field_suffix' => '%',
      '#default_value' => $config->get('aws_cloud_low_utilization_instance_cpu_utilization_threshold'),
    ];

    $form['instance']['low_utilization_instance_settings']['aws_cloud_low_utilization_instance_network_io_threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('Network I/O Threshold'),
      '#description' => $this->t('The network I/O threshold of low utilization instance (The input range is 1-1024MB; e.g. 5MB or less as the default value).'),
      '#size' => 4,
      '#min' => 1,
      '#max' => 1024,
      '#field_suffix' => 'MB',
      '#default_value' => $config->get('aws_cloud_low_utilization_instance_network_io_threshold'),
    ];

    $period_options = [];
    // The period is from 4 to 14 days.
    for ($i = 4; $i <= 14; $i++) {
      $period_options[$i] = $i;
    }
    $form['instance']['low_utilization_instance_settings']['aws_cloud_low_utilization_instance_period'] = [
      '#type' => 'select',
      '#title' => $this->t('Period'),
      '#description' => $this->t('The period of low utilization instance.'),
      '#options' => $period_options,
      '#field_suffix' => 'Days',
      '#default_value' => $config->get('aws_cloud_low_utilization_instance_period'),
    ];

    $form['instance']['low_utilization_instance_settings']['aws_cloud_low_utilization_instance_notification_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email addresses'),
      '#description' => $this->t('Email addresses to be notified.  The email addresses can be comma separated.'),
      '#default_value' => $config->get('aws_cloud_low_utilization_instance_notification_emails'),
    ];

    $form['instance']['low_utilization_instance_settings']['aws_cloud_low_utilization_instance_notification_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject.'),
      '#default_value' => $config->get('aws_cloud_low_utilization_instance_notification_subject'),
    ];

    $form['instance']['low_utilization_instance_settings']['aws_cloud_low_utilization_instance_notification_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#default_value' => $config->get('aws_cloud_low_utilization_instance_notification_msg'),
      '#description' => $this->t('Available tokens are: [aws_cloud_instance:instances], [site:url].  The [aws_cloud_instance:instances] variable can be configured in the instance information below.'),
    ];

    $form['instance']['low_utilization_instance_settings']['aws_cloud_low_utilization_instance_notification_instance_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Instance information'),
      '#default_value' => $config->get('aws_cloud_low_utilization_instance_notification_instance_info'),
      '#description' => $this->t('More than one instance can appear in the email message. Available tokens are: [aws_cloud_instance:name], [aws_cloud_instance:id], [aws_cloud_instance:launch_time], [aws_cloud_instance:instance_state], [aws_cloud_instance:availability_zone], [aws_cloud_instance:private_ip], [aws_cloud_instance:public_up], [aws_cloud_instance:elastic_ip], [aws_cloud_instance:instance_link], [aws_cloud_instance:instance_edit_link].'),
    ];

    $form['volume'] = [
      '#type' => 'details',
      '#title' => $this->t('Volume'),
      '#open' => TRUE,
    ];

    $form['volume']['notification_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Notification Settings'),
      '#open' => TRUE,
    ];

    $form['volume']['notification_settings']['aws_cloud_volume_notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable volume notification'),
      '#description' => $this->t('When enabled, an email will be sent if volumes are unused.  Additionally, the created date field will be marked in red on the volume listing page and Volume detail page.'),
      '#default_value' => $config->get('aws_cloud_volume_notification'),
    ];

    $form['volume']['notification_settings']['aws_cloud_volume_notify_owner'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify owner'),
      '#description' => $this->t('When selected, volume owners will be notified.'),
      '#default_value' => $config->get('aws_cloud_volume_notify_owner'),
    ];

    $form['volume']['notification_settings']['aws_cloud_volume_notification_frequency'] = [
      '#type' => 'select',
      '#options' => $frequency_options,
      '#title' => $this->t('Notification frequency'),
      '#description' => $this->t('Volume notification will be sent once per option selected.'),
      '#default_value' => $config->get('aws_cloud_volume_notification_frequency'),
    ];

    $form['volume']['notification_settings']['aws_cloud_unused_volume_criteria'] = [
      '#type' => 'select',
      '#title' => $this->t('Unused volume criteria'),
      '#description' => $this->t('A volume is considered unused if it has been created and available for the specified number of days.'),
      '#options' => [
        30 => $this->t('30 days'),
        60 => $this->t('60 days'),
        90 => $this->t('90 days'),
        180 => $this->t('180 days'),
        365 => $this->t('One year'),
      ],
      '#default_value' => $config->get('aws_cloud_unused_volume_criteria'),
    ];

    $form['volume']['notification_settings']['aws_cloud_volume_notification_fields'] = [
      '#type' => 'fieldgroup',
    ];

    $form['volume']['notification_settings']['aws_cloud_volume_notification_fields']['aws_cloud_volume_notification_title'] = [
      '#type' => 'item',
      '#title' => $this->t('Notification time'),
    ];

    $form['volume']['notification_settings']['aws_cloud_volume_notification_fields']['aws_cloud_volume_notification_hour'] = [
      '#type' => 'select',
      '#prefix' => '<div class="container-inline">',
      '#options' => $this->getDigits(24),
      '#default_value' => $config->get('aws_cloud_volume_notification_hour'),
    ];

    $form['volume']['notification_settings']['aws_cloud_volume_notification_fields']['aws_cloud_volume_notification_minutes'] = [
      '#prefix' => ': ',
      '#type' => 'select',
      '#options' => $this->getDigits(60),
      '#default_value' => $config->get('aws_cloud_volume_notification_minutes'),
      '#suffix' => '</div>' . $this->t('Time to send the volume usage email.'),
    ];

    $form['volume']['email_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Email Settings'),
      '#open' => TRUE,
    ];

    $form['volume']['email_settings']['aws_cloud_volume_notification_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email addresses'),
      '#description' => $this->t('Email addresses to be notified.  The email addresses can be comma separated.'),
      '#default_value' => $config->get('aws_cloud_volume_notification_emails'),
    ];

    $form['volume']['email_settings']['aws_cloud_volume_notification_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject.'),
      '#default_value' => $config->get('aws_cloud_volume_notification_subject'),
    ];

    $form['volume']['email_settings']['aws_cloud_volume_notification_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#default_value' => $config->get('aws_cloud_volume_notification_msg'),
      '#description' => $this->t('Available tokens are: [aws_cloud_volume:volumes], [site:url].  The [aws_cloud_volume:volumes] variable can be configured in the volume information below.'),
    ];

    $form['volume']['email_settings']['aws_cloud_volume_notification_volume_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Volume information'),
      '#default_value' => $config->get('aws_cloud_volume_notification_volume_info'),
      '#description' => $this->t('More than one volume can appear in the email message.  Available tokens are: [aws_cloud_volume:name], [aws_cloud_volume:volume_link], [aws_cloud_volume:created], [aws_cloud_volume:volume_edit_link].'),
    ];

    $form['snapshot'] = [
      '#type' => 'details',
      '#title' => $this->t('Snapshot'),
      '#open' => TRUE,
    ];

    $form['snapshot']['notification_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Notification Settings'),
      '#open' => TRUE,
    ];

    $form['snapshot']['notification_settings']['aws_cloud_snapshot_notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable snapshot notification'),
      '#description' => $this->t('When enabled, an email will be sent if snapshot are unused.'),
      '#default_value' => $config->get('aws_cloud_snapshot_notification'),
    ];

    $form['snapshot']['notification_settings']['aws_cloud_snapshot_notify_owner'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify owner'),
      '#description' => $this->t('When selected, snapshot owners will be notified.'),
      '#default_value' => $config->get('aws_cloud_snapshot_notify_owner'),
    ];

    $form['snapshot']['notification_settings']['aws_cloud_snapshot_notification_frequency'] = [
      '#type' => 'select',
      '#options' => $frequency_options,

      '#title' => $this->t('Notification frequency'),
      '#description' => $this->t('Snapshot notification will be sent once per option selected.'),
      '#default_value' => $config->get('aws_cloud_snapshot_notification_frequency'),
    ];

    $form['snapshot']['notification_settings']['aws_cloud_stale_snapshot_criteria'] = [
      '#type' => 'select',
      '#title' => $this->t('Stale snapshot criteria'),
      '#description' => $this->t('A snapshot is considered stale if it has been created and available for the specified number of days.'),
      '#options' => [
        30 => $this->t('30 days'),
        60 => $this->t('60 days'),
        90 => $this->t('90 days'),
        180 => $this->t('180 days'),
        365 => $this->t('One year'),
      ],
      '#default_value' => $config->get('aws_cloud_stale_snapshot_criteria'),
    ];

    $form['snapshot']['notification_settings']['aws_cloud_snapshot_notification_fields'] = [
      '#type' => 'fieldgroup',
    ];

    $form['snapshot']['notification_settings']['aws_cloud_snapshot_notification_fields']['aws_cloud_snapshot_notification_title'] = [
      '#type' => 'item',
      '#title' => $this->t('Notification time'),
    ];

    $form['snapshot']['notification_settings']['aws_cloud_snapshot_notification_fields']['aws_cloud_snapshot_notification_hour'] = [
      '#type' => 'select',
      '#prefix' => '<div class="container-inline">',
      '#options' => $this->getDigits(24),
      '#default_value' => $config->get('aws_cloud_snapshot_notification_hour'),
    ];

    $form['snapshot']['notification_settings']['aws_cloud_snapshot_notification_fields']['aws_cloud_snapshot_notification_minutes'] = [
      '#prefix' => ': ',
      '#type' => 'select',
      '#options' => $this->getDigits(60),
      '#default_value' => $config->get('aws_cloud_snapshot_notification_minutes'),
      '#suffix' => '</div>' . $this->t('Time to send the snapshot usage email.'),
    ];

    $form['snapshot']['email_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Email Settings'),
      '#open' => TRUE,
    ];

    $form['snapshot']['email_settings']['aws_cloud_snapshot_notification_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email addresses'),
      '#description' => $this->t('Email addresses to be notified.  The email addresses can be comma separated.'),
      '#default_value' => $config->get('aws_cloud_snapshot_notification_emails'),
    ];

    $form['snapshot']['email_settings']['aws_cloud_snapshot_notification_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject.'),
      '#default_value' => $config->get('aws_cloud_snapshot_notification_subject'),
    ];

    $form['snapshot']['email_settings']['aws_cloud_snapshot_notification_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#default_value' => $config->get('aws_cloud_snapshot_notification_msg'),
      '#description' => $this->t('Available tokens are: [aws_cloud_snapshot:snapshots], [site:url].  The [aws_cloud_snapshot:snapshots] text is configured in the snapshot information field below.'),
    ];

    $form['snapshot']['email_settings']['aws_cloud_snapshot_notification_snapshot_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Snapshot information'),
      '#default_value' => $config->get('aws_cloud_snapshot_notification_snapshot_info'),
      '#description' => $this->t('More than one snapshot can appear in the email message. Available tokens are: [aws_cloud_snapshot:name], [aws_cloud_snapshot:snapshot_link], [aws_cloud_snapshot:created], [aws_cloud_snapshot:snapshot_edit_link].'),
    ];

    $form['elastic_ip'] = [
      '#type' => 'details',
      '#title' => $this->t('Elastic IP'),
      '#open' => TRUE,
    ];

    $form['elastic_ip']['notification_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Notification Settings'),
      '#open' => TRUE,
    ];

    $form['elastic_ip']['notification_settings']['aws_cloud_elastic_ip_notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Elastic IP notification'),
      '#description' => $this->t('When enabled, an email will be sent if Elastic IPs are unused.'),
      '#default_value' => $config->get('aws_cloud_elastic_ip_notification'),
    ];

    $form['elastic_ip']['notification_settings']['aws_cloud_elastic_ip_notify_owner'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify owner'),
      '#description' => $this->t('When selected, Elastic IP owners will be notified.'),
      '#default_value' => $config->get('aws_cloud_elastic_ip_notify_owner'),
    ];

    $form['elastic_ip']['notification_settings']['aws_cloud_elastic_ip_notification_frequency'] = [
      '#type' => 'select',
      '#options' => $frequency_options,

      '#title' => $this->t('Notification frequency'),
      '#description' => $this->t('Elastic IP notification will be sent once per option selected.'),
      '#default_value' => $config->get('aws_cloud_elastic_ip_notification_frequency'),
    ];

    $form['elastic_ip']['notification_settings']['aws_cloud_elastic_ip_notification_fields'] = [
      '#type' => 'fieldgroup',
    ];

    $form['elastic_ip']['notification_settings']['aws_cloud_elastic_ip_notification_fields']['aws_cloud_elastic_ip_notification_title'] = [
      '#type' => 'item',
      '#title' => $this->t('Notification time'),
    ];

    $form['elastic_ip']['notification_settings']['aws_cloud_elastic_ip_notification_fields']['aws_cloud_elastic_ip_notification_hour'] = [
      '#type' => 'select',
      '#prefix' => '<div class="container-inline">',
      '#options' => $this->getDigits(24),
      '#default_value' => $config->get('aws_cloud_elastic_ip_notification_hour'),
    ];

    $form['elastic_ip']['notification_settings']['aws_cloud_elastic_ip_notification_fields']['aws_cloud_elastic_ip_notification_minutes'] = [
      '#prefix' => ': ',
      '#type' => 'select',
      '#options' => $this->getDigits(60),
      '#default_value' => $config->get('aws_cloud_elastic_ip_notification_minutes'),
      '#suffix' => '</div>' . $this->t('Time to send the Elastic IP usage email.'),
    ];

    $form['elastic_ip']['email_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Email Settings'),
      '#open' => TRUE,
    ];

    $form['elastic_ip']['email_settings']['aws_cloud_elastic_ip_notification_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email addresses'),
      '#description' => $this->t('Email addresses to be notified.  The email addresses can be comma separated.'),
      '#default_value' => $config->get('aws_cloud_elastic_ip_notification_emails'),
    ];

    $form['elastic_ip']['email_settings']['aws_cloud_elastic_ip_notification_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject.'),
      '#default_value' => $config->get('aws_cloud_elastic_ip_notification_subject'),
    ];

    $form['elastic_ip']['email_settings']['aws_cloud_elastic_ip_notification_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#default_value' => $config->get('aws_cloud_elastic_ip_notification_msg'),
      '#description' => $this->t('Available tokens are: [aws_cloud_elastic_ip:elastic_ips], [site:url].  The [aws_cloud_elastic_ip:elastic_ips] text is configured in the snapshot information field below.'),
    ];

    $form['elastic_ip']['email_settings']['aws_cloud_elastic_ip_notification_elastic_ip_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Elastic IP information'),
      '#default_value' => $config->get('aws_cloud_elastic_ip_notification_elastic_ip_info'),
      '#description' => $this->t('More than one Elastic IP can appear in the email message. Available tokens are: [aws_cloud_elastic_ip:name], [aws_cloud_elastic_ip:elastic_ip_link], [aws_cloud_elastic_ip:created], [aws_cloud_elastic_ip:elastic_ip_edit_link].'),
    ];

    $form['launch_template'] = [
      '#type' => 'details',
      '#title' => $this->t('Launch template'),
      '#open' => TRUE,
    ];

    $form['launch_template']['email_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Email Settings'),
      '#open' => TRUE,
    ];

    $form['launch_template']['email_settings']['aws_cloud_launch_template_notification_request_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email addresses'),
      '#description' => $this->t('Email addresses to be notified for an approval request. The email addresses can be comma separated.'),
      '#default_value' => $config->get('aws_cloud_launch_template_notification_request_emails'),
    ];

    $form['launch_template']['email_settings']['notification_request'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Notification of request'),
    ];

    $form['launch_template']['email_settings']['notification_request']['aws_cloud_launch_template_notification_request_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject of the launch template request.'),
      '#default_value' => $config->get('aws_cloud_launch_template_notification_request_subject'),
    ];

    $form['launch_template']['email_settings']['notification_request']['aws_cloud_launch_template_notification_request_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#description' => $this->t('Available tokens are: [aws_cloud_launch_template_request_email:launch_templates_request], [aws_cloud_launch_template_request_email:site_url].  The [aws_cloud_launch_template_request_email:launch_templates_request] variable can be configured in the launch template request information below.'),
      '#default_value' => $config->get('aws_cloud_launch_template_notification_request_msg'),
    ];

    $form['launch_template']['email_settings']['notification_request']['aws_cloud_launch_template_notification_launch_template_request_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Launch template request information'),
      '#default_value' => $config->get('aws_cloud_launch_template_notification_launch_template_request_info'),
      '#description' => $this->t('Available tokens are: [aws_cloud_launch_template_request:name], [aws_cloud_launch_template_request:launch_template_link], [aws_cloud_launch_template_request:launch_template_link_edit], [aws_cloud_launch_template_request:launch_template_button_approve], [aws_cloud_launch_template_request:changed].'),
    ];

    $form['launch_template']['email_settings']['notification_approved'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Notification of approval'),
    ];

    $form['launch_template']['email_settings']['notification_approved']['aws_cloud_launch_template_notification_approved_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject of the launch template approval.'),
      '#default_value' => $config->get('aws_cloud_launch_template_notification_approved_subject'),
    ];

    $form['launch_template']['email_settings']['notification_approved']['aws_cloud_launch_template_notification_approved_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#description' => $this->t('Available tokens are: [aws_cloud_launch_template_email:launch_templates], [site:url].  The [aws_cloud_launch_template_email:launch_templates] variable can be configured in the launch template information below.'),
      '#default_value' => $config->get('aws_cloud_launch_template_notification_approved_msg'),
    ];

    $form['launch_template']['email_settings']['notification_restore'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Notification of approved'),
    ];

    $form['launch_template']['email_settings']['notification_restore']['aws_cloud_launch_template_notification_restore_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject of the launch template restore to draft.'),
      '#default_value' => $config->get('aws_cloud_launch_template_notification_restore_subject'),
    ];

    $form['launch_template']['email_settings']['notification_restore']['aws_cloud_launch_template_notification_restore_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#description' => $this->t('Available tokens are: [aws_cloud_launch_template_email:launch_templates], [site:url].  The [aws_cloud_launch_template_email:launch_templates] variable can be configured in the launch template information below.'),
      '#default_value' => $config->get('aws_cloud_launch_template_notification_restore_msg'),
    ];

    $form['launch_template']['email_settings']['aws_cloud_launch_template_notification_launch_template_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Launch template information'),
      '#default_value' => $config->get('aws_cloud_launch_template_notification_launch_template_info'),
      '#description' => $this->t('Available tokens are: [aws_cloud_launch_template:name], [aws_cloud_launch_template:launch_template_link], [aws_cloud_launch_template:launch_template_link_edit], [aws_cloud_launch_template:changed].'),
    ];
    $form['#attached']['library'][] = 'aws_cloud/aws_cloud_view_builder';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('aws_cloud.settings');
    $old_config = clone $config;
    $form_state->cleanValues();

    $instance_time = '';
    $volume_time = '';
    $snapshot_time = '';
    foreach ($form_state->getValues() ?: [] as $key => $value) {
      if ($key === 'aws_cloud_instance_notification_hour') {
        $instance_time .= Html::escape($value);
      }
      if ($key === 'aws_cloud_instance_notification_minutes') {
        $instance_time .= ':' . Html::escape($value);
      }

      if ($key === 'aws_cloud_volume_notification_hour') {
        $volume_time .= Html::escape($value);
      }
      if ($key === 'aws_cloud_volume_notification_minutes') {
        $volume_time .= ':' . Html::escape($value);
      }

      if ($key === 'aws_cloud_snapshot_notification_hour') {
        $snapshot_time .= Html::escape($value);
      }
      if ($key === 'aws_cloud_snapshot_notification_minutes') {
        $snapshot_time .= ':' . Html::escape($value);
      }

      $config->set($key, Html::escape($value));
    }

    if (!empty($instance_time)) {
      // Add seconds into the instance time.
      $config->set('aws_cloud_instance_notification_time', $instance_time . ':00');
    }

    if (!empty($volume_time)) {
      // Add seconds into the volume time.
      $config->set('aws_cloud_volume_notification_time', $volume_time . ':00');
    }

    if (!empty($snapshot_time)) {
      // Add seconds into the snapshot time.
      $config->set('aws_cloud_snapshot_notification_time', $snapshot_time . ':00');
    }

    $config->save();

    // Update spreadsheets.
    $this->updateSpreadsheets($old_config, $config);

    parent::submitForm($form, $form_state);

    if ($this->shouldCacheBeCleaned($old_config, $config)) {
      drupal_flush_all_caches();
    }
  }

  /**
   * Update spreadsheets.
   *
   * @param \Drupal\Core\Config\Config $old_config
   *   The old config object.
   * @param \Drupal\Core\Config\Config $config
   *   The config object.
   */
  private function updateSpreadsheets(Config $old_config, Config $config): void {
    $old_spreadsheet_value = $old_config->get('aws_cloud_instance_type_prices_spreadsheet');
    $spreadsheet_value = $config->get('aws_cloud_instance_type_prices_spreadsheet');

    // If the value does not change, skip.
    if ($old_spreadsheet_value === $spreadsheet_value) {
      return;
    }

    // If the module gapps is not installed, skip.
    if (!$this->moduleHandler->moduleExists('gapps')) {
      return;
    }

    // If the credential is invalid, skip.
    if (!gapps_is_google_credential_valid()) {
      return;
    }

    // We do not use Dependency Injection for gapps.google_spreadsheet here
    // since it is provided by a different module (gapps module).
    $cloud_configs = $this->cloudConfigPluginManager->loadConfigEntities('aws_cloud');
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $old_url = $cloud_config->get('field_spreadsheet_pricing_url')->value;
      if (!empty($old_url)) {
        $this->googleSpreadsheetService->delete($old_url);
        $cloud_config->set('field_spreadsheet_pricing_url', '');
        $cloud_config->save();
      }
    }
  }

  /**
   * Helper function to generate values in the time drop down.
   *
   * @param int $max
   *   The maximum numbers to generate.
   *
   * @return array
   *   Array of time values.
   */
  private function getDigits($max): array {
    $digits = [];
    for ($i = 0; $i < $max; $i++) {
      $digits[sprintf('%02d', $i)] = sprintf('%02d', $i);
    }
    return $digits;
  }

  /**
   * Judge whether cache should be cleaned or not.
   *
   * @param \Drupal\Core\Config\Config $old_config
   *   The old config object.
   * @param \Drupal\Core\Config\Config $config
   *   The config object.
   *
   * @return bool
   *   Whether cache should be cleaned or not.
   */
  private function shouldCacheBeCleaned(Config $old_config, Config $config): bool {
    $items = [
      'aws_cloud_instance_type_prices',
      'aws_cloud_instance_type_prices_spreadsheet',
      'aws_cloud_instance_type_cost',
      'aws_cloud_instance_type_cost_list',
      'aws_cloud_instance_list_cost_column',
    ];

    foreach ($items ?: [] as $item) {
      if ($old_config->get($item) !== $config->get($item)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
