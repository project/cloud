<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class AwsDeleteUpdateEntityForm - Base Delete class.
 *
 * This class inject entity update into build form.
 *
 * @package Drupal\aws_cloud\Form\Ec2
 */
class AwsDeleteUpdateEntityForm extends AwsDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;
    if (!$this->updateEntityBuildForm($entity)) {
      return $this->redirectUrl;
    }
    return $form;
  }

}
