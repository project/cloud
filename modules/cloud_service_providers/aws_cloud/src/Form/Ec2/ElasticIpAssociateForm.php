<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Associate Elastic IP address.
 */
class ElasticIpAssociateForm extends AwsDeleteForm {

  use CloudContentEntityTrait;

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * ElasticIpAssociateForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud EC2 service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    EntityLinkRendererInterface $entity_link_renderer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $entity_link_renderer,
      $cloud_config_plugin_manager
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
    $this->currentUser = $current_user;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return OpenStackFloatingIpAssociateForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('entity.link_renderer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIp $entity */
    $entity = $this->entity;
    return $this->t('Select the instance OR network interface to which you want to associate this @label address (@ip_address).', [
      '@ip_address' => $entity->getPublicIp(),
      '@label' => $entity->getEntityType()->getSingularLabel(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Associate');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = '') {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
    $entity = $this->entity;
    if (!$this->updateEntityBuildForm($entity)) {
      return $this->redirectUrl;
    }
    $form = parent::buildForm($form, $form_state);

    // Get module name.
    $module_name = $this->getModuleName($entity);

    if ($entity->getAssociationId() !== NULL) {
      $form['error'] = [
        '#markup' => '<div>'
        . $this->t('@label is already associated.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ])
        . '</div>',
      ];
      unset($form['actions']['submit']);
    }
    else {
      $form['resource_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Resource type'),
        '#options' => [
          'instance' => $this->t('Instance'),
          'network_interface' => $this->t('Network interface'),
        ],
        '#description' => $this->t('Choose the type of resource to which to associate the @label address.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]),
        '#default_value' => 'instance',
      ];

      $form['instance_ip_container'] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'instance-ip-container',
        ],
      ];

      $form['instance_ip_container']['instance_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Instance'),
        '#options' => $this->getUnassociatedInstances($module_name, $entity->getNetworkBorderGroup()),
        '#ajax' => [
          'callback' => '::getPrivateIpsAjaxCallback',
          'event' => 'change',
          'wrapper' => 'instance-ip-container',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Retrieving...'),
          ],
        ],
        '#states' => [
          'visible' => [
            'select[name="resource_type"]' => ['value' => 'instance'],
          ],
        ],
      ];

      $form['instance_ip_container']['instance_private_ip'] = [
        '#type' => 'select',
        '#title' => $this->t('Private IP'),
        '#description' => $this->t('The private IP address to which to associate the @label address. Only private IP addresses that do not already have an @label associated with them are available.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]),
        '#options' => [
          '-1' => $this->t('Select a private IP.'),
        ],
        '#states' => [
          'visible' => [
            'select[name="resource_type"]' => ['value' => 'instance'],
          ],
        ],
      ];

      $form['network_interface_ip_container'] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'network-interface-ip-container',
        ],
      ];

      $form['network_interface_ip_container']['network_interface_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Network interface'),
        '#options' => $this->getUnassociatedNetworkInterfaces($module_name, $entity->getNetworkBorderGroup()),
        '#ajax' => [
          'callback' => '::getNetworkIpsAjaxCallback',
          'event' => 'change',
          'wrapper' => 'network-interface-ip-container',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Retrieving...'),
          ],
        ],
        '#states' => [
          'visible' => [
            'select[name="resource_type"]' => ['value' => 'network_interface'],
          ],
        ],
      ];
      $form['network_interface_ip_container']['network_private_ip'] = [
        '#type' => 'select',
        '#title' => $this->t('Private IP'),
        '#description' => $this->t('The private IP address to which to associate the @label address. Only private IP addresses that do not already have an @label associated with them are available.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]),
        '#options' => [
          '-1' => $this->t('Select a private IP.'),
        ],
        '#states' => [
          'visible' => [
            'select[name="resource_type"]' => ['value' => 'network_interface'],
          ],
        ],
      ];

      // Ajax support: Look at the instance value, and rebuild the private_ip
      // options.
      $instance = $form_state->getValue('instance_id');
      if (isset($instance)) {
        if ($instance !== '-1') {
          $ips = $this->getPrivateIps($instance, $entity->getCloudContext());
          $form['instance_ip_container']['instance_private_ip']['#options'] = $ips;
        }
        else {
          $form['instance_ip_container']['instance_private_ip']['#options'] = [
            '-1' => $this->t('Select a private IP.'),
          ];
        }
      }

      // Ajax support: Look at network interface value and rebuild the private
      // IP portion of the form.
      $network_interface = $form_state->getValue('network_interface_id');
      if (isset($network_interface)) {
        if ($network_interface !== '-1') {
          $ips = $this->getNetworkPrivateIps($network_interface);
          $form['network_interface_ip_container']['network_private_ip']['#options'] = $ips;
        }
        else {
          $form['network_interface_ip_container']['network_private_ip']['#options'] = [
            '-1' => $this->t('Select a private IP.'),
          ];
        }
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('resource_type') === 'instance') {
      if ($form_state->getValue('instance_id') === -1) {
        // Error out.
        $form_state->setErrorByName('instance_id', $this->t('Instance ID is empty.'));
      }
      if ($form_state->getValue('instance_private_ip') === -1) {
        // Error out.
        $form_state->setErrorByName('instance_private_ip', $this->t('Private IP is empty.'));
      }
    }
    else {
      if ($form_state->getValue('network_interface_id') === -1) {
        $form_state->setErrorByName('network_interface_id', $this->t('Network interface is empty.'));
      }
      if ($form_state->getValue('network_private_ip') === -1) {
        $form_state->setErrorByName('network_private_ip', $this->t('Private IP is empty.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->associateElasticIp($this->entity, $form, $form_state);
  }

  /**
   * Ajax callback when the instance dropdown changes.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface element.
   *
   * @return array
   *   Form element for instance_ip_container.
   */
  public function getPrivateIpsAjaxCallback(array $form, FormStateInterface $form_state): array {
    return $form['instance_ip_container'];
  }

  /**
   * Ajax callback when the network interface dropdown changes.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface element.
   *
   * @return array
   *   Form element for network_interface_ip_container.
   */
  public function getNetworkIpsAjaxCallback(array $form, FormStateInterface $form_state): array {
    return $form['network_interface_ip_container'];
  }

  /**
   * Helper function that loads all the private IPs for an instance.
   *
   * @param int $instance_id
   *   The instance ID.
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return array
   *   An array of IP addresses.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getPrivateIps($instance_id, $cloud_context): array {
    $entity = $this->entity;

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $instances = $this->entityTypeManager
      ->getStorage("{$module_name}_instance")
      ->loadByProperties([
        'id' => $instance_id,
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $instance */
    $instance = count($instances) === 1
      ? array_shift($instances)
      : $instances;

    $ips = !empty($instance) ? explode(', ', $instance->getPrivateIps()) : [];

    $private_ips = [];
    foreach ($ips ?: [] as $ip) {

      // Check if the IP is in the Elastic IP table.
      $result = $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->loadByProperties([
          'private_ip_address' => $ip,
          'cloud_context' => $cloud_context,
        ]);

      if (count($result) === 0) {
        $private_ips[$ip] = $ip;
      }
    }

    return $private_ips;
  }

  /**
   * Helper function to load primary and secondary private IPs.
   *
   * @param int $network_interface_id
   *   The network interface ID.
   *
   * @return array
   *   An array of IP addresses.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getNetworkPrivateIps($network_interface_id): array {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
    $entity = $this->entity;

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $network_interfaces = $this->entityTypeManager
      ->getStorage("{$module_name}_network_interface")
      ->loadByProperties([
        'id' => $network_interface_id,
        'cloud_context' => $entity->getCloudContext(),
      ]);

    /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterface $network_interface */
    $network_interface = count($network_interfaces) === 1
      ? array_shift($network_interfaces)
      : $network_interfaces;

    $association_id = $network_interface->getAssociationId();
    $secondary_association_id = $network_interface->getSecondaryAssociationId();

    $ips = [];
    if (empty($association_id)) {
      $ips[$network_interface->getPrimaryPrivateIp()] = $network_interface->getPrimaryPrivateIp();
    }

    if (empty($secondary_association_id) && !empty($network_interface->getSecondaryPrivateIps())) {
      $ips[$network_interface->getSecondaryPrivateIps()] = $network_interface->getSecondaryPrivateIps();
    }

    return $ips;
  }

  /**
   * Query the database for instances that do not have Elastic IPs.
   *
   * @param string $module_name
   *   Module name.
   * @param string $network_border_group
   *   The network border group.
   *
   * @return array
   *   An array of instances formatted for a dropdown.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getUnassociatedInstances($module_name, $network_border_group): array {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;
    $instances['-1'] = $this->t('Select an instance.');
    $account = $this->currentUser;

    $query = $this->entityTypeManager
      ->getStorage("{$module_name}_instance")
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $entity->getCloudContext());

    // Filter by network border group.
    if (!empty($network_border_group)) {
      $subnets = $this->entityTypeManager
        ->getStorage("{$module_name}_subnet")
        ->loadByProperties([
          'cloud_context' => $entity->getCloudContext(),
          'network_border_group' => $network_border_group,
        ]);
      $subnet_ids = array_map(static function ($subnet) {
        return $subnet->getSubnetId();
      }, $subnets);
      if (!empty($subnet_ids)) {
        $query->condition('subnet_id', $subnet_ids, 'IN');
      }
    }

    // Get cloud service provider name.
    // Use a static trait method through CloudService.
    $cloud_name = CloudService::convertUnderscoreToWhitespace($module_name);

    if (!$account->hasPermission("view any {$cloud_name} instance")) {
      $query->condition('uid', $account->id());
    }

    $results = $query->execute();

    foreach ($results ?: [] as $result) {

      $instance_list = $this->entityTypeManager
        ->getStorage("{$module_name}_instance")
        ->loadByProperties([
          'id' => $result,
          'cloud_context' => $entity->getCloudContext(),
        ]);

      /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
      $instance = count($instance_list) === 1
        ? array_shift($instance_list)
        : $instance_list;

      $private_ips = explode(', ', $instance->getPrivateIps());

      foreach ($private_ips ?: [] as $private_ip) {

        $elastic_ips = $this->entityTypeManager
          ->getStorage($entity->getEntityTypeId())
          ->loadbyProperties([
            'private_ip_address' => $private_ip,
            'cloud_context' => $entity->getCloudContext(),
          ]);

        if (count($elastic_ips) === 0) {
          $instances[$instance->id()] = $this->t('%name (%instance_id) - %status', [
            '%name' => $instance->getName(),
            '%instance_id' => $instance->getInstanceId(),
            '%status' => $instance->getInstanceState(),
          ]);
        }
      }
    }

    return $instances;
  }

  /**
   * Query the database for unassociated network interfaces IPs.
   *
   * @param string $module_name
   *   Module name.
   * @param string $network_border_group
   *   The network border group.
   *
   * @return array
   *   An array of instances formatted for a dropdown.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getUnassociatedNetworkInterfaces($module_name, $network_border_group): array {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
    $entity = $this->entity;
    $interfaces['-1'] = $this->t('Select a network interface.');

    $query = $this->entityTypeManager
      ->getStorage("{$module_name}_network_interface")
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $entity->getCloudContext());

    $group = $query
      ->orConditionGroup()
      ->notExists('association_id')
      ->notExists('secondary_association_id');

    $query->condition($group);

    // Filter by network border group.
    if (!empty($network_border_group)) {
      $subnets = $this->entityTypeManager
        ->getStorage("{$module_name}_subnet")
        ->loadByProperties([
          'cloud_context' => $entity->getCloudContext(),
          'network_border_group' => $network_border_group,
        ]);
      $subnet_ids = array_map(static function ($subnet) {
        return $subnet->getSubnetId();
      }, $subnets);
      if (!empty($subnet_ids)) {
        $query->condition('subnet_id', $subnet_ids, 'IN');
      }
    }

    $results = $query->execute();

    foreach ($results ?: [] as $result) {
      $interface_list = $this->entityTypeManager
        ->getStorage("{$module_name}_network_interface")
        ->loadByProperties([
          'id' => $result,
          'cloud_context' => $entity->getCloudContext(),
        ]);

      /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterface $interface */
      $interface = count($interface_list) === 1
        ? array_shift($interface_list)
        : $interface_list;

      if ($interface->getCloudContext() === $entity->getCloudContext()) {
        $interfaces[$interface->id()] = $this->t('%name - %interface_id', [
          '%name' => $interface->getName(),
          '%interface_id' => $interface->getNetworkInterfaceId(),
        ]);
      }
    }

    return $interfaces;
  }

}
