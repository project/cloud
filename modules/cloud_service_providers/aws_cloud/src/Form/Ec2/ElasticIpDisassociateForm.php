<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Disassociate Elastic IP address form.
 */
class ElasticIpDisassociateForm extends AwsDeleteForm {

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * ElasticIpAssociateForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud EC2 service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    EntityLinkRendererInterface $entity_link_renderer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $entity_link_renderer,
      $cloud_config_plugin_manager
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return OpenStackFloatingIpAssociateForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('entity.link_renderer'),
      $container->get('plugin.manager.cloud_config_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIp $entity */
    $entity = $this->entity;

    if ($this->ec2Service->isInstanceCloudOrchestrator($entity->getInstanceId()) === TRUE) {
      $this->messenger()->addWarning($this->t('Attempting to disassociate @ip_address from Cloud Orchestrator. Cloud Orchestrator will not be accessible if the IP address is disassociated.', [
        '@ip_address' => $entity->getPublicIp(),
      ]));
    }

    return $this->t('Are you sure you want to disassociate this @label address (@ip_address)', [
      '@label' => $entity->getEntityType()->getSingularLabel(),
      '@ip_address' => $entity->getPublicIp(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
    $entity = $this->entity;
    $instance_id = $entity->getInstanceId();
    $network_interface_id = $entity->getNetworkInterfaceId();

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $msg = $this->t('<h2>@label Information:</h2>', [
      '@label' => $entity->getEntityType()->getSingularLabel(),
    ]);
    $msg .= '<ul>';

    if (empty($instance_id) && empty($network_interface_id)) {
      $msg .= $this->t('<li>No information available for instance ID and network ID.</li>');
    }

    if (!empty($instance_id)) {
      $instance = $this->awsCloudOperationsService->getInstanceById($instance_id, $module_name, $entity->getCloudContext());
      $instance_link = $this->entityLinkRenderer->renderViewElement(
        $instance_id,
        "{$module_name}_instance",
        'instance_id',
        [],
        $instance->getName() !== $instance->getInstanceId() ? $this->t('@instance_name (@instance_id)', [
          '@instance_name' => $instance->getName(),
          '@instance_id' => $instance_id,
        ]) : $instance_id
      );

      $msg .= $this->t('<li>Instance ID: @instance_id</li>',
        [
          '@instance_id' => Markup::create($instance_link['#markup']),
        ]
      );
    }

    if (!empty($network_interface_id)) {
      $network_interface = $this->awsCloudOperationsService->getNetworkInterfaceById($network_interface_id, $module_name, $entity->getCloudContext());

      $network_interface_link = $this->entityLinkRenderer->renderViewElement(
        $network_interface_id,
        "{$module_name}_network_interface",
        'network_interface_id',
        [],
        $network_interface->getName() !== $network_interface->getNetworkInterfaceId() ? $this->t('@network_interface_name (@network_interface_id)', [
          '@network_interface_name' => $network_interface->getName(),
          '@network_interface_id' => $network_interface_id,
        ]) : $network_interface_id
      );

      $msg .= $this->t('<li>Network ID: @network_id</li>',
        [
          '@network_id' => Markup::create($network_interface_link['#markup']),
        ]
      );
    }

    $msg .= $this->t('</ul>');

    return $msg;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Disassociate Address');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->updateEntityBuildForm($this->entity)) {
      return $this->redirectUrl;
    }
    $form = parent::buildForm($form, $form_state);
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
    $entity = $this->entity;
    if ($entity->getAssociationId() === NULL) {
      $form['error'] = [
        '#markup' => '<div>' . $this->t('@label is already disassociated', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]) . '</div>',
      ];
      unset($form['description']);
      unset($form['actions']['submit']);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->disassociateElasticIp($this->entity, $form, $form_state);
  }

}
