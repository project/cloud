<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Entity\Ec2\ElasticIp;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Provides an entities disassociate confirmation form.
 */
class ElasticIpDisassociateMultipleForm extends AwsCloudProcessMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    $this->checkCloudOrchestratorIp();
    return $this->formatPlural(count($this->selection),
      'Are you sure you want to disassociate this @item?',
      'Are you sure you want to disassociate these @items?', [
        '@item' => $this->entityType->getSingularLabel(),
        '@items' => $this->entityType->getPluralLabel(),
      ]
    );
  }

  /**
   * Check if Cloud Orchestrator IP is getting disassociated.
   *
   * Notify the user with a warning message if Cloud Orchestrator's IP
   * is one of the selected IPs.
   */
  private function checkCloudOrchestratorIp(): void {
    foreach ($this->selection as $id => $selection) {
      $elastic_ip = ElasticIp::load($id);
      if ($this->ec2Service->isInstanceCloudOrchestrator($elastic_ip->getInstanceId()) === TRUE) {
        $this->messenger()->addWarning($this->t('Attempting to disassociate @ip_address from Cloud Orchestrator. Cloud Orchestrator will not be accessible if the IP address is disassociated.', [
          '@ip_address' => $elastic_ip->getPublicIp(),
        ]));
        break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {

    return $this->t('Disassociate');
  }

  /**
   * {@inheritdoc}
   */
  protected function processCloudResource(CloudContentEntityBase $entity): bool {

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->disassociateAddress([
      'AssociationId' => $entity->getAssociationId(),
    ]) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function processEntity(CloudContentEntityBase $entity): void {}

  /**
   * Process an entity and related AWS resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   *
   * @return bool
   *   Succeeded or failed.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function process(CloudContentEntityBase $entity): bool {

    if (!empty($entity) && ($result = $this->processCloudResource($entity))) {

      $this->processEntity($entity);
      $message = empty($result['SendToWorker']) ? 'disassociated' : 'disassociated remotely';
      $this->processOperationStatus($entity, $message);

      return TRUE;
    }

    $this->processOperationErrorStatus($entity, 'disassociated');

    return FALSE;
  }

  /**
   * Returns the message to show the user after an item was processed.
   *
   * @param int $count
   *   Count of processed translations.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The item processed message.
   */
  protected function getProcessedMessage($count): PluralTranslatableMarkup {

    $this->ec2Service->updateElasticIps();
    $this->ec2Service->updateInstances();
    $this->ec2Service->updateNetworkInterfaces();

    return $this->formatPlural($count, 'Disassociated @count Elastic IP.', 'Disassociated @count Elastic IPs.');
  }

}
