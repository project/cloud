<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the ElasticIp entity edit forms.
 *
 * @ingroup aws_cloud
 */
class ElasticIpEditForm extends AwsCloudContentForm {

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * ElasticIpEditForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManagerInterface $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    CloudServiceInterface $cloud_service,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = '') {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
    $entity = $this->entity;
    if (!$this->updateEntityBuildForm($entity)) {
      return $this->redirectUrl;
    }

    $weight = -50;

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $form['ip_address'] = [
      '#type' => 'details',
      '#title' => $this->t('@title', ['@title' => $entity->getEntityType()->getSingularLabel()]),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['ip_address']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#size' => 60,
      '#default_value' => $entity->label(),
    ];

    $form['ip_address']['elastic_ip_type'] = [
      '#type' => 'item',
      '#title' => $this->getItemTitle($this->t('Type')),
      '#markup' => $entity->getElasticIpType(),
    ];

    $form['ip_address']['public_ip'] = [
      '#type' => 'item',
      '#title' => $this->getItemTitle($this->t('@title', ['@title' => $entity->getEntityType()->getSingularLabel()])),
      '#markup' => $entity->getPublicIp(),
    ];

    $form['ip_address']['private_ip_address'] = [
      '#type' => 'item',
      '#title' => $this->getItemTitle($this->t('Private IP address')),
      '#markup' => $entity->getPrivateIpAddress(),
    ];

    $form['ip_address']['created'] = [
      '#type' => 'item',
      '#title' => $this->getItemTitle($this->t('Created')),
      '#markup' => $this->dateFormatter->format($entity->created(), 'short'),
    ];

    $form['assign'] = [
      '#type' => 'details',
      '#title' => $this->t('Assign'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['assign']['instance_id'] = $this->entityLinkRenderer->renderFormElements(
      $entity->getInstanceId(),
      "{$module_name}_instance",
      'instance_id',
      ['#title' => $this->getItemTitle($this->t('Instance ID'))],
      '',
      EntityLinkWithNameHtmlGenerator::class
    );

    if ($module_name === 'aws_cloud') {
      $form['assign']['network_interface_id'] = $this->entityLinkRenderer->renderFormElements(
        $entity->getNetworkInterfaceId(),
        "{$module_name}_network_interface",
        'network_interface_id',
        ['#title' => $this->getItemTitle($this->t('Network interface ID'))],
        '',
        EntityLinkWithNameHtmlGenerator::class
      );
    }

    $form['assign']['allocation_id'] = [
      '#type' => 'item',
      '#title' => $this->getItemTitle($this->t('Allocation ID')),
      '#markup' => $entity->getAllocationId(),
    ];

    $form['assign']['association_id'] = [
      '#type' => 'item',
      '#title' => $this->getItemTitle($this->t('Association ID')),
      '#markup' => $entity->getAssociationId(),
    ];

    $form['assign']['domain'] = [
      '#type' => 'item',
      '#title' => $this->getItemTitle($this->t('Domain (Standard | VPC)')),
      '#markup' => $entity->getDomain(),
    ];

    $form['assign']['network_interface_owner'] = [
      '#type' => 'item',
      '#title' => $this->getItemTitle($this->t('Network interface owner')),
      '#markup' => $entity->getNetworkInterfaceOwner(),
    ];

    $form['assign']['network_border_group'] = [
      '#type' => 'item',
      '#title' => $this->getItemTitle($this->t('Network border group')),
      '#markup' => $entity->getNetworkBorderGroup(),
    ];

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);
    $association_id = $entity->getAssociationId();
    if (isset($association_id)) {
      // Unset the delete button because the IP is allocated.
      unset($form['actions']['delete']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->editElasticIp($this->entity, $form, $form_state);
  }

}
