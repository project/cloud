<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the CloudScripting entity edit forms.
 *
 * @ingroup aws_cloud
 */
class ImageEditForm extends AwsCloudContentForm {

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * ImageEditForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManagerInterface $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    CloudServiceInterface $cloud_service,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = '') {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity */
    $entity = $this->entity;
    if (!$this->updateEntityBuildForm($entity)) {
      return $this->redirectUrl;
    }

    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

    $weight = -50;

    $form['image'] = [
      '#type' => 'details',
      '#title' => $this->t('Image'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['image']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->getName(),
      '#required'      => TRUE,
    ];

    // Make disabled when OwnerId is not same as Account ID of Cloud Config.
    $form['image']['description'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Description'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->getDescription(),
      '#required'      => TRUE,
      '#disabled' => $cloud_config->get('field_account_id')->value !== $entity->getAccountId(),
    ];

    $form['image']['ami_name'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('AMI name')),
      '#markup'        => $entity->getAmiName(),
    ];

    $form['image']['image_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Image ID')),
      '#markup'        => $entity->getImageId(),
    ];

    $form['image']['account_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Owner')),
      '#markup'        => $entity->getAccountId(),
    ];

    $form['image']['source'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Source')),
      '#markup'        => $entity->getSource(),
    ];

    $form['image']['status'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Status')),
      '#markup'        => $entity->getStatus(),
    ];

    $form['image']['state_reason'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('State Reason')),
      '#markup'        => $entity->getStateReason(),
    ];

    $form['image']['created'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Created')),
      '#markup'        => $this->dateFormatter->format($entity->created(), 'short'),
    ];

    $form['launch_permission'] = [
      '#type' => 'details',
      '#title' => $this->getItemTitle($this->t('Launch Permission')),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['launch_permission']['visibility_title'] = [
      '#type'          => 'markup',
      '#markup'        => '<b>Visibility: </b>',
      '#prefix'        => '<div class="container-inline">',
    ];

    // Make disabled when OwnerId is not same as Account ID of Cloud Config.
    $form['launch_permission']['visibility'] = [
      '#type' => 'radios',
      '#default_value' => $entity->getVisibility() ? '1' : '0',
      '#options' => [
        '0' => $this->t('Private'),
        '1' => $this->t('Public'),
      ],
      '#suffix' => '</div>',
      '#disabled' => $cloud_config->get('field_account_id')->value !== $entity->getAccountId(),
    ];

    $form['launch_permission']['launch_permission_account_ids_container'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          'input[name="visibility"]' => ['value' => 0],
        ],
      ],
    ];

    $form['launch_permission']['launch_permission_account_ids_container']['launch_permission_account_ids'] = $form['launch_permission_account_ids'];
    unset($form['launch_permission_account_ids']);

    $form['type'] = [
      '#type' => 'details',
      '#title' => $this->getItemTitle($this->t('Type')),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['type']['platform'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Platform')),
      '#markup'        => $entity->getPlatform(),
    ];

    $form['type']['architecture'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Architecture')),
      '#markup'        => $entity->getArchitecture(),
    ];

    $form['type']['virtualization_type'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Virtualization type')),
      '#markup'        => $entity->getVirtualizationType(),
    ];

    $form['type']['product_code'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Product Code')),
      '#markup'        => $entity->getProductCode(),
    ];

    $form['type']['image_type'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Image type')),
      '#markup'        => $entity->getImageType(),
    ];

    $form['device'] = [
      '#type' => 'details',
      '#title' => $this->getItemTitle($this->t('Device')),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['device']['root_device_name'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Root device name')),
      '#markup'        => $entity->getRootDeviceName(),
    ];

    $form['device']['root_device_type'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Root device type')),
      '#markup'        => $entity->getRootDeviceType(),
    ];

    $form['device']['kernel_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Kernel ID')),
      '#markup'        => $entity->getKernelId(),
    ];

    $form['device']['ramdisk_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Ramdisk ID')),
      '#markup'        => $entity->getRamdiskId(),
    ];

    // Just display the device block mappings in the form.  There is no
    // edit functionality.
    $viewBuilder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
    $output = $viewBuilder->viewField($entity->getBlockDeviceMappings(), 'full');
    $form['device']['device_block_mappings'] = [
      '#type' => 'item',
      '#not_field' => TRUE,
      '#markup' => $this->renderer->render($output),
    ];

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    // Customize others fieldset.
    $old_others = $form['others'];
    unset($form['others']['langcode']);
    unset($form['others']['uid']);

    $form['others']['langcode'] = $old_others['langcode'];
    $form['others']['uid'] = $old_others['uid'];

    $form['actions'] = $this->actions($form, $form_state);
    $form['actions']['#weight'] = $weight++;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->editImage($this->entity, $form, $form_state);
  }

}
