<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Entity\Ec2\ElasticIp;
use Drupal\aws_cloud\Entity\Ec2\NetworkInterface;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Associate an Elastic IP form.
 *
 * This form is instance specific and accessed from the instance operations.
 */
class InstanceAssociateElasticIpForm extends AwsDeleteForm {

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * InstanceAssociateElasticIpForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud EC2 service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    EntityLinkRendererInterface $entity_link_renderer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $entity_link_renderer,
      $cloud_config_plugin_manager
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('entity.link_renderer'),
      $container->get('plugin.manager.cloud_config_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $instance = $this->entity;
    return $this->t('Select Elastic IP to which you want to associate with this instance @instance_name (@instance_id)', [
      '@instance_name' => $instance->getName(),
      '@instance_id' => $instance->getInstanceId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Associate');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $form['allocation_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Elastic IP'),
      '#options' => $this->getAvailableElasticIps(),
    ];

    $form['network_interface_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Private IP'),
      '#options' => $this->getAvailablePrivateIps(),
      '#description' => $this->t('The private IP address to which to associate the Elastic IP address. Only private IP addresses that do not already have an Elastic IP associated with them are available.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->associateElasticIpForInstance($this->entity, $form, $form_state);
  }

  /**
   * Helper function that gets the available private IP addresses.
   *
   * Used as the #options array in a select field.
   *
   * @return array
   *   An array of private IP addresses.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getAvailablePrivateIps(): array {
    $private_ips = [];
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;
    $results = $this->entityTypeManager
      ->getStorage('aws_cloud_network_interface')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('instance_id', $entity->getInstanceId())
      ->condition('cloud_context', $entity->getCloudContext())
      ->notExists('association_id')
      ->execute();
    foreach ($results ?: [] as $result) {
      $network_interface = NetworkInterface::load($result);
      if (empty($network_interface)) {
        continue;
      }
      $private_ips[$network_interface->getNetworkInterfaceId()] = $network_interface->getPrimaryPrivateIp();
    }
    return $private_ips;
  }

  /**
   * Helper function that gets the available Elastic IP addresses.
   *
   * Used as the #options array in a select field.
   *
   * @return array
   *   An array of Elastic IP addresses.
   */
  private function getAvailableElasticIps(): array {
    $elastic_ips = [];
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;
    $results = $this->entityTypeManager
      ->getStorage('aws_cloud_elastic_ip')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $entity->getCloudContext())
      ->notExists('association_id')
      ->execute();

    foreach ($results ?: [] as $result) {
      /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIp $elastic_ip */
      $elastic_ip = ElasticIp::load($result);
      $elastic_ips[$elastic_ip->getAllocationId()] = $elastic_ip->getPublicIp();
    }
    return $elastic_ips;
  }

}
