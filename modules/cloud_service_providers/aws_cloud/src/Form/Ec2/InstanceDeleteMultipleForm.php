<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Entity\Ec2\Instance;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Provides an entities deletion confirmation form.
 */
class InstanceDeleteMultipleForm extends AwsCloudDeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    $this->checkCloudOrchestratorIp();
    return parent::getQuestion();
  }

  /**
   * Check if Cloud Orchestrator is being terminated.
   *
   * Notify the user with a warning message if Cloud Orchestrator's IP
   * is one of the selected IPs.
   */
  private function checkCloudOrchestratorIp(): void {
    foreach ($this->selection as $id => $selection) {
      $instance = Instance::load($id);
      if (!empty($instance) && $this->ec2Service->isInstanceCloudOrchestrator($instance->getInstanceId()) === TRUE) {
        $this->messenger()->addWarning($this->t('Attempting to delete Cloud Orchestrator instance.  Cloud Orchestrator will stop responding and all data will not be recovered.'));
        break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function processCloudResource(CloudContentEntityBase $entity): bool {

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->terminateInstances(
      ['InstanceIds' => [$entity->getInstanceId()]]
    ) !== NULL;
  }

}
