<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\aws_cloud\Entity\Ec2\ElasticIp;
use Drupal\aws_cloud\Entity\Ec2\PublicIpEntityLinkHtmlGenerator;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the CloudScripting entity edit forms.
 *
 * @ingroup aws_cloud
 */
class InstanceEditForm extends AwsCloudContentForm {

  public const SECURITY_GROUP_DELIMITER = ', ';

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * InstanceEditForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManagerInterface $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    CloudServiceInterface $cloud_service,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = '') {
    $form = parent::buildForm($form, $form_state);
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;

    $weight = -50;

    if (!$this->updateEntityBuildForm($entity)) {
      return $this->redirectUrl;
    }

    $form['instance'] = [
      '#type' => 'details',
      '#title' => $this->t('Instance'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['instance']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->label(),
      '#required'      => TRUE,
    ];

    $form['instance']['instance_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Instance ID')),
      '#markup'        => $entity->getInstanceId(),
    ];

    $form['instance']['instance_state'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Instance State')),
      '#markup'        => $entity->getInstanceState(),
    ];

    $form['instance']['instance_type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Instance type'),
      '#wrapped_label' => TRUE,
      '#default_value' => $entity->getInstanceType(),
      '#required'      => FALSE,
      '#options'       => $entity->getEntityTypeId() === 'aws_cloud_instance'
        ? $this->getInstanceTypeOptions() : [],
    ];

    $form['instance']['cost'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Cost')),
      '#markup'        => '$' . $entity->getCost(),
    ];

    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    if ($entity->getEntityTypeId() === 'aws_cloud_instance' && !$cloud_config->isRemote()) {
      $form['instance']['iam_role'] = [
        '#type'          => 'select',
        '#title'         => $this->t('IAM role'),
        '#wrapped_label' => TRUE,
        '#default_value' => $entity->getIamRole(),
        '#required'      => FALSE,
        '#options'       => $this->getIamRoleOptions(),
        '#empty_value'   => '',
        '#empty_option'  => $this->t('No Role'),
      ];
    }

    if ($entity->getInstanceState() !== 'stopped') {
      $form['instance']['instance_type'] += [
        '#attributes'  => ['readonly' => 'readonly'],
        '#disabled'    => TRUE,
      ];
    }

    $form['instance']['image_id'] = $this->entityLinkRenderer->renderFormElements(
        $entity->getImageId(),
        'aws_cloud_image',
        'image_id',
        ['#title' => $this->getItemTitle($this->t('AMI image'))],
        '',
        EntityLinkWithNameHtmlGenerator::class
    );

    $form['instance']['kernel_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Kernel image')),
      '#markup'        => $entity->getKernelId(),
    ];

    $form['instance']['ramdisk_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Ramdisk image')),
      '#markup'        => $entity->getRamdiskId(),
    ];

    $form['instance']['virtualization'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Virtualization')),
      '#markup'        => $entity->getVirtualization(),
    ];

    $form['instance']['reservation'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Reservation')),
      '#markup'        => $entity->getReservation(),
    ];

    $form['instance']['account_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('AWS account ID')),
      '#markup'        => $entity->getAccountId(),
    ];

    $form['instance']['launch_time'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Launch Time')),
      '#markup'        => $this->dateFormatter->format($entity->getLaunchTime(), 'short'),
    ];

    $form['instance']['created'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Created')),
      '#markup'        => $this->dateFormatter->format($entity->created(), 'short'),
    ];

    $form['network'] = [
      '#type' => 'details',
      '#title' => $this->t('Network'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $public_ip = $entity->getPublicIp();
    $current_elastic_ip = isset($public_ip) ? $this->getElasticIpByIpLookup($public_ip) : FALSE;

    // Public IP is not an Elastic IP.  It is an AWS assigned IP address.
    // Just display it.
    if (isset($public_ip) && $current_elastic_ip === FALSE) {
      $form['network']['public_ip'] = $this->entityLinkRenderer->renderFormElements(
        $entity->getPublicIp(),
        'aws_cloud_elastic_ip',
        'public_ip',
        ['#title' => $this->getItemTitle($this->t('Public IP'))]
      );
    }
    else {
      if ($entity->getInstanceState() === 'stopped') {
        // If no elastic_ip assigned and no available IPs, prompt user to create
        // one.
        if (count($this->getAvailableElasticIpCount()) === 0 && $current_elastic_ip === FALSE) {
          $link = Link::createFromRoute($this->t('Create a new Elastic IP'), 'view.aws_cloud_elastic_ip.list', [
            'cloud_context' => $entity->getCloudContext(),
          ])->toString();
          $form['network']['elastic_ip_link'] = [
            '#type'          => 'item',
            '#title'         => $this->getItemTitle($this->t('Elastic IP')),
            '#markup'        => $link,
            '#not_field'     => TRUE,
          ];
        }
        elseif (count($this->getNetworkInterfaceCount()) > 1) {
          // If instance has more than one network Interface, link will go to
          // Elastic IP list page.
          $link = Link::createFromRoute($this->t('Associate Elastic IP'), 'view.aws_cloud_elastic_ip.list', [
            'cloud_context' => $entity->getCloudContext(),
          ])->toString();

          $form['network']['elastic_ip_link'] = [
            '#type'          => 'item',
            '#title'         => $this->getItemTitle($this->t('Elastic IP')),
            '#markup'        => $link,
          ];
        }
        else {
          $available_elastic_ips = $this->getAvailableElasticIps();
          if ($current_elastic_ip !== FALSE) {
            unset($available_elastic_ips[-1]);
            $available_elastic_ips[$current_elastic_ip->getAllocationId()] = $current_elastic_ip->getPublicIp();
          }

          $form['network']['add_new_elastic_ip'] = [
            '#type' => 'select',
            '#title' => $this->getItemTitle($this->t('Elastic IP')),
            '#options' => $available_elastic_ips,
            '#default_value' => $current_elastic_ip !== FALSE ? $current_elastic_ip->getAllocationId() : '',
          ];

          // Store the current allocation ID, so we can use it to compare after
          // the form is submitted.
          if ($current_elastic_ip !== FALSE) {
            $form['network']['current_allocation_id'] = [
              '#type' => 'value',
              '#value' => $current_elastic_ip->getAllocationId(),
            ];
            $form['network']['current_association_id'] = [
              '#type' => 'value',
              '#value' => $current_elastic_ip->getAssociationId(),
            ];
          }
        }
      }
      else {
        if ($current_elastic_ip !== FALSE) {
          $form['network']['public_ip'] = $this->entityLinkRenderer->renderFormElements(
            $entity->getPublicIp(),
            'aws_cloud_elastic_ip',
            'public_ip',
            [
              '#title' => $this->getItemTitle($this->t('Elastic IP')),
            ],
            '',
            PublicIpEntityLinkHtmlGenerator::class
          );
        }
      }
    }

    $form['network']['private_ips'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Private IPs')),
      '#markup'        => $entity->getPrivateIps(),
    ];

    $form['network']['public_dns'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Public DNS')),
      '#markup'        => $entity->getPublicDns(),
    ];

    $form['network']['security_groups'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Security groups'),
      '#default_value' => explode(self::SECURITY_GROUP_DELIMITER,
                                  $entity->getSecurityGroups()),
      '#required'      => TRUE,
      '#multiple'      => TRUE,
      '#options'       => $this->getSecurityGroupsOptions(),
    ];

    $form['network']['key_pair_name'] = $this->entityLinkRenderer->renderFormElements(
      $entity->getKeyPairName(),
      'aws_cloud_key_pair',
      'key_pair_name',
      ['#title' => $this->getItemTitle($this->t('Key pair name'))]
    );

    $form['network']['vpc_id'] = $this->entityLinkRenderer->renderFormElements(
        $entity->getVpcId(),
        'aws_cloud_vpc',
        'vpc_id',
        ['#title' => $this->getItemTitle($this->t('VPC ID'))],
        '',
        EntityLinkWithNameHtmlGenerator::class
    );

    $form['network']['subnet_id'] = $this->entityLinkRenderer->renderFormElements(
        $entity->getSubnetId(),
        'aws_cloud_subnet',
        'subnet_id',
        ['#title' => $this->getItemTitle($this->t('Subnet ID'))],
        '',
        EntityLinkWithNameHtmlGenerator::class
    );

    $form['network']['availability_zone'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Availability Zone')),
      '#markup'        => $entity->getAvailabilityZone(),
      '#weight'        => 1,
    ];

    $interfaces = [];
    foreach ($entity->getNetworkInterfaces() ?: [] as $interface) {
      $render_element = $this->entityLinkRenderer->renderViewElement(
        $interface['value'],
        'aws_cloud_network_interface',
        'network_interface_id'
      );
      $interfaces[] = $render_element['#markup'];
    }

    if (count($interfaces)) {
      $form['network']['network_interfaces'] = [
        '#type' => 'item',
        '#title' => $this->getItemTitle($this->t('Network interfaces')),
        '#markup' => implode(', ', $interfaces),
      ];
    }

    $form['storage'] = [
      '#type'          => 'details',
      '#title'         => $this->t('Storage'),
      '#open'          => TRUE,
      '#weight'        => $weight++,
    ];

    $form['storage']['root_device_type'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Root device type')),
      '#markup'        => $entity->getRootDeviceType(),
    ];

    $form['storage']['root_device'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Root device')),
      '#markup'        => $entity->getRootDevice(),
    ];

    $form['storage']['ebs_optimized'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('EBS Optimized')),
      '#markup'        => $entity->getEbsOptimized() === 0 ? 'Off' : 'On',
    ];

    // Render volume ID elements.
    $blockDevices = [];
    $block_devices = explode(', ', $entity->getBlockDevices() ?? '');
    foreach ($block_devices ?: [] as $block_device) {
      $render_element = $this->entityLinkRenderer->renderFormElements(
        $block_device,
        'aws_cloud_volume',
        'volume_id',
        ['#title' => $this->getItemTitle($this->t('Volume'))],
        '',
        EntityLinkWithNameHtmlGenerator::class
      );
      $blockDevices[] = $render_element['#markup'];
    }
    $form['storage']['block_devices'] = [
      '#type' => 'item',
      '#title' => $this->getItemTitle($this->t('Volume')),
      '#markup' => implode(', ', $blockDevices),
    ];

    $form['fieldset_tags'] = [
      '#type'          => 'details',
      '#title'         => $this->t('Tags'),
      '#open'          => TRUE,
      '#weight'        => $weight++,
    ];

    $form['fieldset_tags'][] = $form['tags'];
    unset($form['tags']);

    $form['options'] = [
      '#type'          => 'details',
      '#title'         => $this->t('Options'),
      '#open'          => TRUE,
      '#weight'        => $weight++,
    ];

    $form['options']['termination_protection'] = [
      '#title'         => $this->t('Termination protection'),
      '#type'          => 'checkbox',
      '#description'   => $this->t('Enable the termination protection. If enabled, this instance cannot be terminated using the console, API, or CLI until termination protection is disabled.'),
      '#default_value' => $entity->getTerminationProtection(),
    ];

    $form['options']['is_monitoring'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Monitoring Enabled')),
      '#markup'        => $entity->isMonitoring() ? $this->t('Enabled') : $this->t('Disabled'),
    ];

    $form['options']['ami_launch_index'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('AMI Launch Index')),
      '#markup'        => $entity->getAmiLaunchIndex(),
    ];

    $form['options']['tenancy'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Tenancy')),
      '#markup'        => $entity->getTenancy(),
    ];

    // Set a message for termination timestamp.
    $date_format = DateFormat::load('html_date')->getPattern();
    $time_format = DateFormat::load('html_time')->getPattern();

    $form['termination_timestamp']['widget'][0]['value']['#description'] =
      $this->t('Format: Date: %date_format, Time: %time_format. Leave blank for no automatic termination.', [
        '%date_format' => $date_format,
        '%time_format' => $time_format,
      ]);

    $form['termination_timestamp']['#weight'] = $weight++;
    $form['options']['termination_timestamp'] = $form['termination_timestamp'];
    unset($form['termination_timestamp']);

    $schedule = $entity->getSchedule();
    $form['options']['schedule'] = [
      '#title' => $this->t('Schedule'),
      '#type' => 'select',
      '#default_value' => isset($schedule) ? $entity->getSchedule() : '',
      '#options'       => aws_cloud_get_schedule(),
      '#description'   => $this->t('Configure start and stop schedule. This helps reduce server hosting costs.'),
    ];

    $form['options']['login_username'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Login Username')),
      '#markup'        => $entity->getLoginUsername() ?: 'ec2-user',
    ];

    $description = '';
    $disabled = FALSE;
    if ($entity->getInstanceState() !== 'stopped') {
      $url = Url::fromRoute(
        'entity.aws_cloud_instance.stop_form',
        [
          'cloud_context' => $entity->getCloudContext(),
          'aws_cloud_instance' => $entity->id(),
        ]
      )->toString();
      $description = $this->t("To edit your instance's user data you first need to <strong><em><a href='@stop_instance_link'>stop</a></em></strong> your instance.",
        ['@stop_instance_link' => $url]
      );

      $disabled = TRUE;
    }

    // Check if the text is binary.
    $is_user_data_binary = FALSE;
    $user_data_decoded = base64_decode($entity->getUserData() ?? '');
    if (preg_match('/[^\x20-\x7e]/', $user_data_decoded)) {
      $is_user_data_binary = TRUE;
    }

    $form['options']['user_data'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('User data'),
      '#description'   => $description,
      '#maxlength'     => 1024 * 16,
      '#cols'          => 60,
      '#rows'          => 10,
      '#default_value' => $is_user_data_binary ? $entity->getUserData() : $user_data_decoded,
      '#required'      => FALSE,
      '#weight'        => $weight++,
      '#disabled'      => $disabled,
    ];

    $form['options']['user_data_base64_encoded'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('User data is base64 encoded'),
      '#description'   => $this->t('If the user data is binary, it will be shown as base64 encoded.'),
      '#default_value' => $is_user_data_binary,
      '#weight'        => $weight++,
    ];

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);
    $form['actions']['#weight'] = $weight++;

    // Hide delete button if termination_protection is selected.
    if ($entity->getTerminationProtection() === 1) {
      $form['actions']['delete']['#access'] = FALSE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    if ($form_state->getValue('user_data_base64_encoded')) {
      if (!$this->validateBase64EncodedString($form_state->getValue('user_data'))) {
        $form_state->setErrorByName(
          'user_data',
          $this->t("The user data isn't a valid base64 encoded string.")
        );
      }
    }

    $termination_timestamp = $form_state->getValue('termination_timestamp')[0]['value'] ?? '';
    $termination_protection = $form_state->getValue('termination_protection');
    if (!empty($termination_timestamp) && $termination_protection === 1) {
      $form_state->setErrorByName(
        'termination_timestamp',
        $this->t('"@name1" should be left blank if "@name2" is selected. Please leave "@name1" blank or unselect "@name2".', [
          '@name1' => $this->t('Termination date'),
          '@name2' => $this->t('Termination protection'),
        ])
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->editInstance($this->entity, $form, $form_state);
  }

  /**
   * Get instance type Options.
   *
   * @return array
   *   Array of instance type options.
   */
  private function getInstanceTypeOptions(): array {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;

    // This function gets the instance types from an EC2 endpoint.
    $instance_types = aws_cloud_get_instance_types($entity->getCloudContext());
    return array_combine(array_keys($instance_types), array_keys($instance_types));
  }

  /**
   * Get IAM role Options.
   *
   * @return array
   *   Array of IAM role Options.
   */
  private function getIamRoleOptions(): array {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;

    return aws_cloud_get_iam_roles($entity->getCloudContext());
  }

  /**
   * Get security groups options.
   *
   * @return array
   *   Array of security groups options.
   */
  protected function getSecurityGroupsOptions(): array {
    $options = [];
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;
    $groups = $this->entityTypeManager->getStorage('aws_cloud_security_group')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'vpc_id' => $entity->getVpcId(),
      ]);

    array_walk($groups, static function ($group) use (&$options) {
      $options[$group->getGroupName()] = $group->getGroupName();
    });

    natcasesort($options);
    return $options;
  }

  /**
   * Helper function to build Elastic IP dropdown.
   */
  private function getAvailableElasticIps(): array {
    $ips[-1] = $this->t('Select an Elastic IP.');

    $available_ips = $this->getAvailableElasticIpCount();

    foreach ($available_ips ?: [] as $ip) {
      $elastic_ip = ElasticIp::load($ip);
      if (empty($elastic_ip)) {
        continue;
      }
      $ips[$elastic_ip->getAllocationId()] = $this->t('@name (@ip)', [
        '@name' => $elastic_ip->getName(),
        '@ip' => $elastic_ip->getPublicIp(),
      ]);
    }
    return $ips;
  }

  /**
   * Helper function to query db for available Elastic IPs.
   */
  private function getAvailableElasticIpCount(): array {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;

    return $this->entityTypeManager->getStorage('aws_cloud_elastic_ip')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $entity->getCloudContext())
      ->notExists('association_id')
      ->execute();
  }

  /**
   * Helper to look up an Elastic IP row given the Elastic IP address.
   *
   * @param string $ip
   *   IP address used to look up the row.
   *
   * @return bool|mixed
   *   FALSE if no row found or the ElasticIp entity.
   */
  private function getElasticIpByIpLookup($ip) {
    $elastic_ip = FALSE;
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;
    $result = $this->entityTypeManager->getStorage('aws_cloud_elastic_ip')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'public_ip' => $ip,
      ]);
    if (count($result) === 1) {
      $elastic_ip = array_shift($result);
    }
    return $elastic_ip;
  }

  /**
   * Helper function to get network interfaces for an instance.
   *
   * @return array
   *   Array of network interfaces.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getNetworkInterfaceCount(): array {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;

    return $this->entityTypeManager->getStorage('aws_cloud_network_interface')
      ->loadByProperties([
        'instance_id' => $entity->getInstanceId(),
        'cloud_context' => $entity->getCloudContext(),
      ]);
  }

  /**
   * Validate if the string is correctly base64 encoded.
   *
   * @param string|null $base64_encoded_str
   *   Base64 encoded string.
   *
   * @return bool
   *   Correctly base64 encoded or not.
   */
  private function validateBase64EncodedString(?string $base64_encoded_str): bool {
    $base64_encoded_str = trim($base64_encoded_str ?: '');
    if (empty($base64_encoded_str)) {
      return TRUE;
    }

    $decoded_str = base64_decode($base64_encoded_str);
    if ($decoded_str === FALSE) {
      return FALSE;
    }

    return $base64_encoded_str === base64_encode($decoded_str);
  }

}
