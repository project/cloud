<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\aws_cloud\Entity\Ec2\InstanceInterface;
use Drupal\aws_cloud\Traits\AwsInstanceMonitorTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for monitor of an instance entity.
 *
 * @ingroup aws_cloud
 */
class InstanceMonitorForm extends FormBase {

  use AwsInstanceMonitorTrait;

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * InstanceMonitorForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'aws_cloud_instance_monitor_form';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = '', ?InstanceInterface $aws_cloud_instance = NULL): array {

    $form['chart_options'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'chart-options',
      ],
    ];
    $default_chart_type = $form_state->getValue('chart_type', 1);
    $form['chart_options']['chart_type'] = [
      '#type' => 'radios',
      '#default_value' => $default_chart_type,
      '#options' => [
        1 => $this->t('Horizon chart'),
        0 => $this->t('Line chart'),
      ],
      '#prefix' => Markup::create('<label>' . $this->t('Chart type') . '</label>'),
      '#ajax' => [
        'callback' => '::switchChart',
        'wrapper'  => 'aws-cloud-instance-monitor-charts',
      ],
    ];

    $this->buildChart($form, $default_chart_type);

    $form['#attached']['library'] = [
      'aws_cloud/aws_cloud_instance_monitor',
      'aws_cloud/aws_cloud_instance_monitor_horizon_chart',
    ];

    $form['#attached']['drupalSettings']['aws_cloud_monitor_refresh_interval']
      = $this->configFactory
        ->get('aws_cloud.settings')
        ->get('aws_cloud_monitor_refresh_interval');

    if ($aws_cloud_instance) {
      $metrics_string = 'internal:/clouds/aws_cloud/' . $aws_cloud_instance->get('cloud_context')->value . '/instance/' . $aws_cloud_instance->id() . '/metrics';
      $metrics_url = Url::fromUri($metrics_string)->toString();
      $form['#attached']['drupalSettings']['aws_cloud_monitor_metrics_url'] = $metrics_url ?: 'metrics';
    }

    return $form;
  }

  /**
   * Ajax callback for chart type.
   *
   * @param array &$form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   Response.
   */
  public function switchChart(array &$form, FormStateInterface $form_state): array {
    $chart_type = $form_state->getValue('chart_type', 1);

    $this->buildChart($form, $chart_type);

    return $form['aws_instance_monitor_charts'];
  }

}
