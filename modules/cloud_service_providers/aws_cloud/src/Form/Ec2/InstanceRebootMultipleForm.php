<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Entity\Ec2\Instance;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Provides an entities reboot confirmation form.
 */
class InstanceRebootMultipleForm extends AwsCloudProcessMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    $this->checkCloudOrchestratorIp();
    return $this->formatPlural(count($this->selection),
      'Are you sure you want to reboot this @item?',
      'Are you sure you want to reboot these @items?', [
        '@item' => $this->entityType->getSingularLabel(),
        '@items' => $this->entityType->getPluralLabel(),
      ]
    );
  }

  /**
   * Check if Cloud Orchestrator is being rebooted.
   *
   * Notify the user with a warning message if Cloud Orchestrator's IP
   * is one of the selected IPs.
   */
  private function checkCloudOrchestratorIp(): void {
    foreach ($this->selection as $id => $selection) {
      $instance = Instance::load($id);
      if (!empty($instance) && $this->ec2Service->isInstanceCloudOrchestrator($instance->getInstanceId()) === TRUE) {
        $this->messenger()->addWarning($this->t('Attempting to reboot Cloud Orchestrator instance.  Cloud Orchestrator will stop responding until the instance is rebooted.'));
        break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {

    return $this->t('Reboot');
  }

  /**
   * {@inheritdoc}
   */
  protected function processCloudResource(CloudContentEntityBase $entity): bool {

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->rebootInstances([
      'InstanceIds' => [$entity->getInstanceId()],
    ]) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function processEntity(CloudContentEntityBase $entity): void {}

  /**
   * Process an entity and related AWS resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   *
   * @return bool
   *   Succeeded or failed.
   */
  protected function process(CloudContentEntityBase $entity): bool {
    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    if ($cloud_config->isRemote()) {
      $this->processCloudResource($entity);
      $this->processOperationStatus($entity, 'rebooted remotely');
      return TRUE;
    }

    try {
      if (!empty($entity) && $this->processCloudResource($entity)) {

        $this->processEntity($entity);
        $this->processOperationStatus($entity, 'rebooted');

        return TRUE;
      }

      $this->processOperationErrorStatus($entity, 'rebooted');

      return FALSE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    // Exception return type handling.
    // Return unable to process.
    return FALSE;
  }

  /**
   * Returns the message to show the user after an item was processed.
   *
   * @param int $count
   *   Count of processed translations.
   *
   * @return \Drupal\Core\StringTranslation\PluralTranslatableMarkup
   *   The item processed message.
   */
  protected function getProcessedMessage($count): PluralTranslatableMarkup {
    $this->ec2Service->updateInstances();
    return $this->formatPlural($count, 'Rebooted @count item.', 'Rebooted @count items.');
  }

}
