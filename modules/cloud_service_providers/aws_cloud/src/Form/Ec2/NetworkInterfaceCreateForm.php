<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the NetworkInterface entity create form.
 *
 * @ingroup aws_cloud
 */
class NetworkInterfaceCreateForm extends AwsCloudContentForm {

  use CloudContentEntityTrait;

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * NetworkInterfaceCreateForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManagerInterface $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    CloudServiceInterface $cloud_service,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state);

    $this->ec2Service->setCloudContext($cloud_context);

    /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterface $entity */
    $entity = $this->entity;

    $weight = -50;

    $form['network_interface'] = [
      '#type' => 'details',
      '#title' => $entity->getEntityType()->getSingularLabel(),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['network_interface']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->label(),
      '#required'      => TRUE,
    ];

    $form['network_interface']['description'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Description'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->getDescription(),
      '#required'      => FALSE,
    ];

    $module_name = $this->getModuleName($entity);
    if ($module_name === 'aws_cloud') {
      $options = $this->getSubnetOptions($cloud_context, $entity);
    }
    else {
      $method_name = "{$module_name}_get_subnet_options_by_vpc_id";
      $options = $method_name(NULL, $entity);
    }
    $form['network_interface']['subnet_id'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Subnet'),
      '#options'       => $options,
      '#ajax' => [
        'callback' => '::subnetAjaxCallback',
        'wrapper'  => 'security-groups-wrapper',
      ],
      '#required'      => TRUE,
    ];

    $form['network_interface']['security_groups_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'security-groups-wrapper',
      ],
    ];

    $form['network_interface']['security_groups_wrapper']['security_groups'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Security groups'),
      '#size'          => 5,
      '#multiple'      => TRUE,
      '#options'       => $this->getSecurityGroupOptions($cloud_context),
      '#required'      => TRUE,
    ];

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    return $form;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->createNetworkInterface($this->entity, $form, $form_state);
  }

  /**
   * Ajax callback for select form item subnet.
   *
   * @param array &$form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   Response.
   */
  public function subnetAjaxCallback(array &$form, FormStateInterface $form_state): array {
    $cloud_context = $this->routeMatch->getParameter('cloud_context');

    if ($subnet_id = $form_state->getValue('subnet_id')) {
      $form['network_interface']['security_groups_wrapper']['security_groups']['#options']
        = $this->getSecurityGroupOptions($cloud_context, $subnet_id);
    }
    else {
      $form['network_interface']['security_groups_wrapper']['security_groups']['#options'] = [];
    }

    return $form['network_interface']['security_groups_wrapper'];
  }

  /**
   * Get select options of security groups.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $subnet_id
   *   The subnet ID.
   *
   * @return array
   *   The options of security group.
   */
  private function getSecurityGroupOptions($cloud_context, $subnet_id = NULL): array {
    $entity = $this->entity;

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $vpc_id = NULL;
    if (!empty($subnet_id)) {
      /** @var \Drupal\aws_cloud\Entity\Vpc\SubnetInterface[] $subnets */
      $subnets = $this->entityTypeManager
        ->getStorage("{$module_name}_subnet")
        ->loadByProperties([
          'cloud_context' => $cloud_context,
          'subnet_id' => $subnet_id,
        ]);

      if (!empty($subnets)) {
        $vpc_id = array_shift($subnets)->getVpcId();
      }
    }

    $storage = $this->entityTypeManager
      ->getStorage("{$module_name}_security_group");

    $query = $storage
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $cloud_context);

    if ($vpc_id !== NULL) {
      $query = $query->condition('vpc_id', $vpc_id);
    }

    $entity_ids = $query->execute();

    $options = [];
    $security_groups = $storage->loadMultiple($entity_ids);
    foreach ($security_groups ?: [] as $security_group) {
      $options[$security_group->getGroupId()] = $security_group->getGroupName();
    }

    natcasesort($options);
    return $options;
  }

  /**
   * Get select options of subnet.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity instance of EntityInterface.
   *
   * @return array
   *   The options of subnet.
   */
  private function getSubnetOptions($cloud_context, EntityInterface $entity): array {
    // Get module name.
    $module_name = $this->getModuleName($entity);

    /** @var \Drupal\aws_cloud\Entity\Vpc\SubnetInterface[] $subnets */
    $subnets = $this->entityTypeManager
      ->getStorage("{$module_name}_subnet")
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($subnets ?: [] as $subnet) {
      $options[$subnet->getSubnetId()] = sprintf('%s (%s | %s)',
        $subnet->getName(),
        $subnet->getSubnetId(),
        $subnet->getCidrBlock()
      );
    }

    natcasesort($options);
    return $options;
  }

}
