<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Entity\Ec2\PublicIpEntityLinkHtmlGenerator;
use Drupal\aws_cloud\Entity\Ec2\SecurityGroupLinkQuery;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the CloudScripting entity edit forms.
 *
 * @ingroup aws_cloud
 */
class NetworkInterfaceEditForm extends AwsCloudContentForm {

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * NetworkInterfaceEditForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManagerInterface $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    CloudServiceInterface $cloud_service,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );
    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = '') {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterface $entity */
    $entity = $this->entity;
    if (!$this->updateEntityBuildForm($entity)) {
      return $this->redirectUrl;
    }

    $weight = -50;

    $form['network_interface'] = [
      '#type' => 'details',
      '#title' => $this->t('Network interface'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['network_interface']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->label(),
      '#required'      => TRUE,
    ];

    $form['network_interface']['description'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Description'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->getDescription(),
      '#required'      => FALSE,
    ];

    $form['network_interface']['network_interface_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Network interface ID')),
      '#markup'        => $entity->getNetworkInterfaceId(),
    ];

    $form['network_interface']['instance_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Instance ID')),
      '#markup'        => $entity->getInstanceId(),
    ];

    $form['network_interface']['allocation_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Allocation ID')),
      '#markup'        => $entity->getAllocationId(),
    ];

    $form['network_interface']['mac_address'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Mac Address')),
      '#markup'        => $entity->getMacAddress(),
    ];

    $form['network_interface']['device_index'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Device Index')),
      '#markup'        => $entity->getDeviceIndex(),
    ];

    $form['network_interface']['status'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Status')),
      '#markup'        => $entity->getStatus(),
    ];

    $form['network_interface']['delete_on_termination'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Delete on Termination')),
      '#markup'        => $entity->getDeleteOnTermination(),
    ];

    $form['network_interface']['created'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Created')),
      '#markup'        => $this->dateFormatter->format($entity->created(), 'short'),
    ];

    $form['network'] = [
      '#type' => 'details',
      '#title' => $this->t('Network'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['network']['security_groups'] = $this->entityLinkRenderer->renderFormElements(
      $entity->getSecurityGroups(),
      'aws_cloud_security_group',
      'group_name',
      ['#title' => $this->getItemTitle($this->t('Security group'))],
      '',
      '',
      SecurityGroupLinkQuery::class,
      $entity
    );

    $form['network']['vpc_id'] = $this->entityLinkRenderer->renderFormElements(
      $entity->getVpcId(),
      'aws_cloud_vpc',
      'vpc_id',
      ['#title' => $this->getItemTitle($this->t('VPC ID'))],
      '',
      EntityLinkWithNameHtmlGenerator::class
    );

    $form['network']['cidr_block'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('CIDR Block')),
      '#markup'        => $entity->getCidrBlock(),
    ];

    $form['network']['subnet_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Subnet ID')),
      '#markup'        => $entity->getSubnetId(),
    ];

    $form['network']['public_ips'] = $this->entityLinkRenderer->renderFormElements(
      $entity->getPublicIps(),
      'aws_cloud_elastic_ip',
      'public_ip',
      ['#title' => $this->getItemTitle($this->t('Public IPs'))],
      '',
      PublicIpEntityLinkHtmlGenerator::class
    );

    $form['network']['primary_private_ip'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Primary private IP')),
      '#markup'        => $entity->getPrimaryPrivateIp(),
    ];

    $form['network']['secondary_private_ips'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Secondary private IPs')),
      '#markup'        => $entity->getSecondaryPrivateIps(),
    ];

    $form['network']['private_dns'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Private DNS')),
      '#markup'        => $entity->getPrivateDns(),
    ];

    $form['attachment'] = [
      '#type' => 'details',
      '#title' => $this->t('Attachment'),
      '#open' => FALSE,
      '#weight' => $weight++,
    ];

    $form['attachment']['attachment_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Attachment ID')),
      '#markup'        => $entity->getAttachmentId(),
    ];

    $form['attachment']['attachment_owner'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Attachment Owner')),
      '#markup'        => $entity->getAttachmentOwner(),
    ];

    $form['attachment']['attachment_status'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Attachment Status')),
      '#markup'        => $entity->getAttachmentStatus(),
    ];

    $form['owner'] = [
      '#type' => 'details',
      '#title' => $this->t('Owner'),
      '#open' => FALSE,
      '#weight' => $weight++,
    ];

    $form['owner']['account_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('AWS account ID')),
      '#markup'        => $entity->getAccountId(),
    ];

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->editNetworkInterface($this->entity, $form, $form_state);
  }

}
