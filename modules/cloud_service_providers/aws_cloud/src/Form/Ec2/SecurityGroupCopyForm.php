<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;

/**
 * Form controller for the CloudScripting entity copy forms.
 *
 * @ingroup aws_cloud
 */
class SecurityGroupCopyForm extends SecurityGroupEditForm {

  use AwsCloudFormTrait;

  const REF_GROUP_ID = 'createSecurityGroup#GroupId';

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $this->ec2Service->setCloudContext($cloud_context);

    $form = parent::buildForm($form, $form_state, $cloud_context);
    $weight = -50;

    /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
    $entity = $this->entity;

    $form['group_name']['#access'] = TRUE;
    $form['security_group']['group_name'] = $form['group_name'];
    $form['security_group']['group_name']['#weight'] = $weight++;
    $form['security_group']['group_name']['widget'][0]['value']['#default_value'] = $this->t(
      'Copy of @name',
      [
        '@name' => $entity->getGroupName(),
      ]);
    unset($form['security_group']['name'], $form['security_group']['group_id'], $form['group_name']);

    $form['security_group']['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#size' => self::$awsCloudSecurityGroupDescriptionMaxLength,
      '#maxlength' => self::$awsCloudSecurityGroupDescriptionMaxLength,
      '#pattern' => $this->getPatternOfSecurityGroupDescription(),
      '#default_value' => $entity->getDescription(),
      '#required' => TRUE,
      '#weight' => $weight++,
    ];

    $vpcs = $this->getVpcOptions($cloud_context);
    if (count($vpcs) === 0) {
      $this->messenger->addWarning($this->t('You do not have any VPCs. You need a VPC in order to create a security group. You can <a href=":create_vpc_link">create a VPC</a>.', [
        ':create_vpc_link' => Url::fromRoute(
          'entity.aws_cloud_vpc.add_form', [
            'cloud_context' => $cloud_context,
          ])->toString(),
      ]));
    }
    natcasesort($vpcs);
    $form['security_group']['vpc_id'] = [
      '#type' => 'select',
      '#title' => $this->t('VPC CIDR (ID)'),
      '#options' => $vpcs,
      '#default_value' => $entity->getVpcId(),
      '#required' => TRUE,
      '#weight' => $weight++,
    ];

    foreach ($form['rules'] ?: [] as &$ip_permission) {
      if (is_array($ip_permission) && isset($ip_permission['widget']) && !empty($ip_permission['widget'])) {
        foreach ($ip_permission['widget'] ?: [] as &$widget) {
          if (is_array($widget)) {
            if (isset($widget['group_id']) && $widget['group_id']['#default_value'] === $entity->getGroupId()) {
              $widget['group_id']['#description'] = $this->t('* Update with new Group ID.');
              $widget['group_id']['#attributes']['readonly'] = TRUE;
            }
          }
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
    /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
    $entity = $this->entity->createDuplicate();
    $this->entity = $entity;
    $this->trimTextfields($form, $form_state);

    // Load to the VPC to determine whether to permissions_to_revoke Cidr and/or
    // Ip6Cidr.
    $vpc = $this->getVpc($form_state->getValue('vpc_id'), $entity->getCloudContext());
    if (empty($vpc)) {
      // Cannot load VPC, set an error message and return.
      $form_state->setError($form, $this->t('Unable to load VPC @vpc.', [
        '@vpc' => $form_state->getValue('vpc_id'),
      ]));
      return;
    }

    $result = $this->ec2Service->createSecurityGroup([
      'GroupName' => $form_state->getValue('group_name')[0]['value'],
      'VpcId'       => $form_state->getValue('vpc_id'),
      'Description' => $form_state->getValue('description'),
    ]);

    $permissions_to_revoke = [];
    if (!empty($vpc->getCidrBlocks())) {
      $permissions_to_revoke[] = [
        'FromPort' => '0',
        'ToPort' => '65535',
        'IpProtocol' => '-1',
        'IpRanges' => [
          ['CidrIp' => '0.0.0.0/0'],
        ],
      ];
    }
    if (!empty($vpc->getIpv6CidrBlocks())) {
      $permissions_to_revoke[] = [
        'FromPort' => '0',
        'ToPort' => '65535',
        'IpProtocol' => '-1',
        'Ipv6Ranges' => [
          ['CidrIpv6' => '::/0'],
        ],
      ];
    }

    // Revoke the default outbound permissions.
    $this->ec2Service->revokeSecurityGroupEgress([
      'GroupId' => $result['GroupId'],
      'IpPermissions' => $permissions_to_revoke,
    ]);

    if (!empty($result['SendToWorker'])) {
      $result['GroupId'] = self::REF_GROUP_ID;
    }

    if (isset($result['GroupId'])
      && $entity->setGroupId($result['GroupId'])
      && $entity->set('name', $form_state->getValue('group_name')[0]['value'])
      && $entity->set('vpc_id', $form_state->getValue('vpc_id'))
      && $entity->set('description', $form_state->getValue('description'))) {

      return;
    }

    $form_state->setError($form, $this->t('Unable to update security group.'));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    // Create the new security group.
    $this->trimTextfields($form, $form_state);
    /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
    $entity = $this->entity;

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    if ($this->ec2Service->isWorkerResource()) {
      $this->changeSelfGroupId();

      $permissions = $this->awsCloudOperationsService->formatIpPermissions($entity->getIpPermission());
      $permissions['GroupId'] = self::REF_GROUP_ID;
      $this->ec2Service->authorizeSecurityGroupIngress($permissions, [], ['GroupId' => self::REF_GROUP_ID]);

      $permissions = $this->awsCloudOperationsService->formatIpPermissions($entity->getOutboundPermission());
      $permissions['GroupId'] = self::REF_GROUP_ID;
      $this->ec2Service->authorizeSecurityGroupEgress($permissions, [], ['GroupId' => self::REF_GROUP_ID]);

      // Use the custom message since security group uses 'group_name' for
      // its own label.  So do not change the following code.
      $this->messenger->addStatus($this->t('The @type %label has been created remotely.', [
        '@type' => $entity->getEntityType()->getSingularLabel(),
        '%label' => $entity->getGroupName(),
      ]));

      $form_state->setRedirect('view.aws_cloud_security_group.list', ['cloud_context' => $entity->getCloudContext()]);
      return;
    }

    // Set the created time right before save.
    if ($entity->set('created', time()) && $entity->save()) {

      $this->updateNameAndCreatedByTags($entity, $entity->getGroupId());

      $existing_group = $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->load($entity->id());

      $this->changeSelfGroupId();

      // Update Ingress and Egress permissions.
      $this->awsCloudOperationsService->updateIngressEgressPermissions($this->entity, $entity, $existing_group, FALSE, FALSE);

      // Have the system refresh the security group.
      $this->ec2Service->updateSecurityGroups([
        'GroupIds' => [$entity->getGroupId()],
      ], FALSE);

      if (count($this->messenger->messagesByType('error')) === 0) {
        // Check API calls, see if the permissions updates were
        // successful or not.
        /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
        $this->awsCloudOperationsService->validateAuthorize($entity);
      }

      if (count($this->messenger->messagesByType('status')) === 1) {

        $this->messenger->deleteAll();

        // Use the custom message since security group uses 'group_name' for
        // its own label.  So do not change the following code.
        $this->messenger->addStatus($this->t('The @type %label has been created.', [
          '@type' => $entity->getEntityType()->getSingularLabel(),
          '%label' => $entity->toLink($entity->getGroupName())->toString(),
        ]));
        $this->logOperationMessage($entity, 'created');
        $this->dispatchSaveEvent($entity);
        $form_state->setRedirectUrl($entity->toUrl('canonical'));
      }
    }
    else {
      $this->messenger->addError($this->t('Unable to update security group.'));
      $this->logOperationErrorMessage($entity, 'updated');
    }

    if (count($this->messenger->messagesByType('error')) > 0) {
      if ($entity->id()) {
        $form_state->setRedirect('entity.aws_cloud_security_group.canonical', [
          'cloud_context' => $entity->getCloudContext(),
          'aws_cloud_security_group' => $entity->id(),
        ]);
      }
      else {
        $form_state->setRedirect('view.aws_cloud_security_group.list', ['cloud_context' => $entity->getCloudContext()]);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    if (isset($actions['delete'])) {
      unset($actions['delete']);
    }
    if (isset($actions['submit'])) {
      $actions['submit']['#value'] = $this->t('Copy');
    }
    /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
    $entity = $this->entity;
    $url = $entity->toUrl('canonical');
    $url->setRouteParameter('cloud_context', $entity->getCloudContext());
    $actions['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $url,
      '#attributes' => ['class' => ['button']],
    ];
    return $actions;
  }

  /**
   * Change Group ID for inbound and outbound permissions.
   */
  private function changeSelfGroupId(): void {
    $funcs = ['getOutboundPermission', 'getIpPermission'];
    /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
    $entity = $this->entity;
    foreach ($funcs ?: [] as $func) {
      $permissions = $this->entity->$func();
      foreach ($permissions as $permission) {
        if (!empty($permission->getGroupId()) && $entity->getGroupId() !== $permission->getGroupId()) {
          $permission->setGroupId($entity->getGroupId());
        }
      }
    }
  }

}
