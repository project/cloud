<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Volume attach form.
 */
class VolumeAttachForm extends AwsDeleteForm {

  use CloudContentEntityTrait;

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * VolumeAttachForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud EC2 service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    EntityLinkRendererInterface $entity_link_renderer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $entity_link_renderer,
      $cloud_config_plugin_manager
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('entity.link_renderer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    $entity = $this->entity;

    return $this->t('Are you sure you want to attach volume: %name?', [
      '%name' => $entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Attach');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): TranslatableMarkup {
    $entity = $this->entity;
    return $this->t('<h2>Volume Information:</h2><ul><li>Volume id: %id</li><li>Volume name: %name</li></ul>', [
      '%id' => $entity->getVolumeId(),
      '%name' => $entity->getName(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = '') {
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;
    if (!$this->updateEntityBuildForm($entity)) {
      return $this->redirectUrl;
    }

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $instances = [];
    $results = $this->getInstances($this->entity->getAvailabilityZone(), $module_name, $cloud_context);

    foreach ($results ?: [] as $result) {
      /** @var \Drupal\aws_cloud\Entity\Ec2\Instance $result */
      $instances[$result->getInstanceId()] = $this->t('%name - %instance_id', [
        '%name' => $result->getName(),
        '%instance_id' => $result->getInstanceId(),
      ]);
    }
    if (count($results) > 0) {
      $form['device_name'] = [
        '#title' => $this->t('Device name'),
        '#type' => 'textfield',
        '#description' => $this->t('The device name (for example, /dev/sdh or xvdh).'),
        '#required' => TRUE,
      ];

      $form['instance_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Instance ID'),
        '#options' => $instances,
      ];
    }
    else {
      $form['message'] = [
        '#markup' => '<h1>' . $this->t('No instances available in the Availability Zone%zone.  Volume cannot be attached.',
          [
            '%zone' => $module_name === 'aws_cloud'
              ? ": {$this->entity->getAvailabilityZone()}"
              : '',
          ]
        )
        . '</h1>',
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);

    $entity = $this->entity;

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $results = $this->getInstances($this->entity->getAvailabilityZone(), $module_name, $entity->getCloudContext());
    if (count($results) === 0) {
      unset($actions['submit']);
    }
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->attachVolume($this->entity, $form, $form_state);
  }

  /**
   * Query DB for aws_cloud_instances that are in the same zone as the volume.
   *
   * This method respects instance visibility.
   *
   * @param string $zone
   *   The Availability Zone String.
   * @param string $module_name
   *   Module name.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The instance entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getInstances($zone, $module_name, $cloud_context = ''): array {

    $account = $this->currentUser;
    $properties = [
      'cloud_context' => $cloud_context,
    ];

    if ($module_name === 'aws_cloud') {
      $properties['availability_zone'] = $zone;
    }

    // Get cloud service provider name.
    // Use a static trait method through CloudService.
    $cloud_name = CloudService::convertUnderscoreToWhitespace($module_name);

    if (!$account->hasPermission("view any {$cloud_name} instance")) {
      $properties['uid'] = $account->id();
    }

    return $this->entityTypeManager
      ->getStorage("{$module_name}_instance")
      ->loadByProperties($properties);
  }

}
