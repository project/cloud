<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the volume entity create form.
 *
 * @ingroup aws_cloud
 */
class VolumeCreateForm extends AwsCloudContentForm {

  use AwsCloudFormTrait;
  use CloudContentEntityTrait;

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * VolumeCreateForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManagerInterface $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    CloudServiceInterface $cloud_service,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {

    $this->ec2Service->setCloudContext($cloud_context);

    /** @var \Drupal\aws_cloud\Entity\Ec2\Volume $entity */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    // Get module name.
    $module_name = $this->getModuleName($entity);

    // Use the value of parameter snapshot_id as the default value.
    $snapshot_id = $this->getRequest()->query->get('snapshot_id');
    $snapshot = NULL;
    if (!empty($snapshot_id)) {
      $snapshots = $this->entityTypeManager
        ->getStorage("{$module_name}_snapshot")
        ->loadByProperties([
          'cloud_context' => $cloud_context,
          'snapshot_id' => $snapshot_id,
        ]);

      if (!empty($snapshots)) {
        $snapshot = reset($snapshots);
      }
    }

    $weight = -50;

    $form['volume'] = [
      '#type' => 'details',
      '#title' => $this->t('@title', ['@title' => $entity->getEntityType()->getSingularLabel()]),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['volume']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->label(),
      '#weight'        => -5,
      '#required'      => TRUE,
    ];

    $form['volume']['snapshot_id'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Snapshot ID'),
      '#options'       => $this->getSnapshotOptions($cloud_context, $module_name),
      '#default_value' => $snapshot_id,
      '#weight'        => -5,
      '#required'      => FALSE,
      '#empty_value'   => '',
    ];

    $form['volume']['size'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Size (GiB)'),
      '#size'          => 60,
      '#default_value' => $snapshot ? $snapshot->getSize() : '',
      '#required'      => TRUE,
    ];

    $form['volume']['volume_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Volume type'),
      '#options' => [
        'gp2' => $this->t('General Purpose SSD (gp2)'),
        'gp3' => $this->t('General Purpose SSD (gp3)'),
        'io1' => $this->t('Provisioned IOPS SSD (io1)'),
        'io2' => $this->t('Provisioned IOPS SSD (io2)'),
        'sc1' => $this->t('Cold HDD (sc1)'),
        'st1' => $this->t('Throughput Optimized HDD (st1)'),
        'standard' => $this->t('Magnetic (standard)'),
      ],
    ];

    $form['volume']['iops'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('IOPS'),
      '#size'          => 60,
      '#default_value' => $entity->getIops(),
      '#required'      => FALSE,
    ];

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $availability_zone_options = $this->getAvailabilityZoneOptions($cloud_context, $module_name);

    $form['volume']['availability_zone'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Availability Zone'),
      '#options'       => $availability_zone_options,
      // Pick up the first Availability Zone in the array.
      '#default_value' => array_shift($availability_zone_options),
      '#required'      => TRUE,
    ];

    $form['volume']['kms_key_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('KMS key ID'),
      '#size'          => 60,
      '#default_value' => $entity->getKmsKeyId(),
      '#required'      => FALSE,
    ];

    $form['volume']['encrypted'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Encrypted'),
      '#size'          => 60,
      '#default_value' => $entity->getEncrypted(),
      '#required'      => FALSE,

    ];

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    // Add validation for volume types.  for io1 type,
    // iops must be set.  For other volume types, iops cannot
    // be set.
    if ($form_state->getValue('volume_type') === 'io1') {
      // Check if there is an iops value.
      if (empty($form_state->getValue('iops'))) {
        $form_state->setErrorByName('iops', $this->t('Specify an iops value.  The value must be a minimum of 100.'));
      }

      // Check if iops is an integer.
      if (!is_numeric($form_state->getValue('iops'))) {
        $form_state->setErrorByName('iops', $this->t('IOPS must be an integer.'));
      }
      // Check if iops is greater than 100.
      $iops = (int) $form_state->getValue('iops');
      if ($iops < 100) {
        $form_state->setErrorByName('iops', $this->t('IOPS must be a minimum of 100.'));
      }
    }
    else {
      if (!empty($form_state->getValue('iops'))) {
        $form_state->setErrorByName('iops', $this->t('IOPS cannot be set unless volume type is "Provisioned IOPS SSD".'));
      }
    }
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->createVolume($this->entity, $form, $form_state);
  }

  /**
   * Helper function to get snapshot options.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   * @param string $module_name
   *   Module name.
   *
   * @return array
   *   The snapshot options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getSnapshotOptions($cloud_context, $module_name): array {
    $options = [];
    $params = [
      'cloud_context' => $cloud_context,
    ];

    // Get cloud service provider name.
    // Use a static trait method through CloudService.
    $cloud_name = CloudService::convertUnderscoreToWhitespace($module_name);
    if (!$this->currentUser->hasPermission("view any {$cloud_name} snapshot")) {
      $params['uid'] = $this->currentUser->id();
    }

    $snapshots = $this->entityTypeManager
      ->getStorage("{$module_name}_snapshot")
      ->loadByProperties($params);
    foreach ($snapshots ?: [] as $snapshot) {
      if ($snapshot->getName() !== $snapshot->getSnapshotId()) {
        $options[$snapshot->getSnapshotId()] = "{$snapshot->getName()} ({$snapshot->getSnapshotId()})";
      }
      else {
        $options[$snapshot->getSnapshotId()] = $snapshot->getSnapshotId();
      }
    }

    natcasesort($options);
    return $options;
  }

}
