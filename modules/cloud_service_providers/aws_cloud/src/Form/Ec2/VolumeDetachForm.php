<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Volume detach form.
 */
class VolumeDetachForm extends AwsDeleteUpdateEntityForm {

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * VolumeDetachForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud EC2 service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    EntityLinkRendererInterface $entity_link_renderer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $entity_link_renderer,
      $cloud_config_plugin_manager
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('entity.link_renderer'),
      $container->get('plugin.manager.cloud_config_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    $entity = $this->entity;

    return $this->t('Are you sure you want to detach volume: %name?', [
      '%name' => $entity->getName(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Detach');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    $entity = $this->entity;

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $instance = $this->getInstanceName($entity->getAttachmentInformation(), $module_name, $entity->getCloudContext());

    $msg = '<h2>Volume Information:</h2>';

    $volume_id = $entity->getVolumeId();
    $name = $entity->getName();
    $attachment_info = $entity->getAttachmentInformation();
    $attachment_device_name = $entity->getAttachmentDeviceName();

    $msg .= $this->t(
      '<ul><li>Volume ID: %id</li><li>Volume name: %name</li><li>Attached to instance: %instance with ID: %instance_id Device name: %device_name</li></ul>',
      [
        '%id' => $volume_id,
        '%name' => $name,
        '%instance_id' => $attachment_info,
        '%instance' => $instance,
        '%device_name' => $attachment_device_name,
      ]
    );

    $msg .= 'Make sure to unmount any file systems on the device within your operating system before detaching the volume. Failure to do so can result in the volume becoming stuck in the busy state while detaching. If this happens, detachment can be delayed indefinitely until you unmount the volume, force detachment, reboot the instance, or all three. If an EBS volume is the root device of an instance, it can\'t be detached while the instance is running. To detach the root volume, stop the instance first.';
    return $msg;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->detachVolume($this->entity, $form, $form_state);
  }

  /**
   * Helper method to get instance name from database.
   *
   * @param string $instance_id
   *   The instance ID.
   * @param string $module_name
   *   Module name.
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return string
   *   The instance name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getInstanceName($instance_id, $module_name, $cloud_context): string {
    $name = '';
    $instances = $this->entityTypeManager
      ->getStorage("{$module_name}_instance")
      ->loadByProperties([
        'instance_id' => $instance_id,
        'cloud_context' => $cloud_context,
      ]);

    if (count($instances)) {
      /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
      $instance = array_shift($instances);
      $name = $instance->getName();
    }
    return $name;
  }

}
