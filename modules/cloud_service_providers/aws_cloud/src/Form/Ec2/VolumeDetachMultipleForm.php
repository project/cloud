<?php

namespace Drupal\aws_cloud\Form\Ec2;

use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Provides an entities detach confirmation form.
 */
class VolumeDetachMultipleForm extends AwsCloudProcessMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {

    return $this->formatPlural(count($this->selection),
      'Are you sure you want to detach this @item?',
      'Are you sure you want to detach these @items?', [
        '@item' => $this->entityType->getSingularLabel(),
        '@items' => $this->entityType->getPluralLabel(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {

    return $this->t('Detach');
  }

  /**
   * {@inheritdoc}
   */
  protected function processCloudResource(CloudContentEntityBase $entity): bool {

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->detachVolume([
      'VolumeId' => $entity->getVolumeId(),
    ]) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function processEntity(CloudContentEntityBase $entity): void {}

  /**
   * Process an entity and related AWS resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   *
   * @return bool
   *   Succeeded or failed.
   */
  protected function process(CloudContentEntityBase $entity): bool {

    try {

      if (!empty($entity) && $this->processCloudResource($entity)) {

        $this->processEntity($entity);
        $this->processOperationStatus($entity, 'detached');

        return TRUE;
      }

      $this->processOperationErrorStatus($entity, 'detached');

      return FALSE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    // Exception return type handling.
    // Return unable to process.
    return FALSE;
  }

  /**
   * Returns the message to show the user after an item was processed.
   *
   * @param int $count
   *   Count of processed translations.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The item processed message.
   */
  protected function getProcessedMessage($count): PluralTranslatableMarkup {

    $this->ec2Service->updateVolumes();
    $this->ec2Service->updateInstances();

    return $this->formatPlural($count, 'Detached @count volume.', 'Detached @count volumes.');
  }

}
