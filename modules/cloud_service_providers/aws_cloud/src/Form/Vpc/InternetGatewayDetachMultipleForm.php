<?php

namespace Drupal\aws_cloud\Form\Vpc;

use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Form\Ec2\AwsCloudProcessMultipleForm;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Provides an entities detach confirmation form.
 */
class InternetGatewayDetachMultipleForm extends AwsCloudProcessMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->formatPlural(count($this->selection),
      'Are you sure you want to detach this @item?',
      'Are you sure you want to detach these @items?', [
        '@item' => $this->entityType->getSingularLabel(),
        '@items' => $this->entityType->getPluralLabel(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {

    return $this->t('Detach');
  }

  /**
   * {@inheritdoc}
   */
  protected function processCloudResource(CloudContentEntityBase $entity): bool {

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->detachInternetGateway([
      'InternetGatewayId' => $entity->getInternetGatewayId(),
      'VpcId' => $entity->getVpcId(),
    ]) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function processEntity(CloudContentEntityBase $entity): void {}

  /**
   * Process an entity and related AWS resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   *
   * @return bool
   *   Succeeded or failed.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function process(CloudContentEntityBase $entity): bool {

    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

    if ($cloud_config->isRemote()) {
      $this->processCloudResource($entity);
      $this->processOperationStatus($entity, 'detached remotely');
      return TRUE;
    }

    if (!empty($entity) && $this->processCloudResource($entity)) {

      $this->processEntity($entity);
      $this->processOperationStatus($entity, 'detached');

      return TRUE;
    }

    $this->processOperationErrorStatus($entity, 'detached');

    return FALSE;
  }

  /**
   * Returns the message to show the user after an item was processed.
   *
   * @param int $count
   *   Count of processed translations.
   *
   * @return \Drupal\Core\StringTranslation\PluralTranslatableMarkup
   *   The item processed message.
   */
  protected function getProcessedMessage($count): PluralTranslatableMarkup {
    $this->ec2Service->updateInternetGateways();
    return $this->formatPlural($count, 'Detached @count internet gateway.', 'Detached @count internet gateways.');
  }

}
