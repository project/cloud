<?php

namespace Drupal\aws_cloud\Form\Vpc;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Form\Ec2\AwsCloudContentForm;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the transit gateway entity create form.
 *
 * @ingroup aws_cloud
 */
class TransitGatewayCreateForm extends AwsCloudContentForm {

  use CloudContentEntityTrait;

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * TransitGatewayCreateForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManagerInterface $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    CloudServiceInterface $cloud_service,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state);
    $this->ec2Service->setCloudContext($cloud_context);

    $weight = -50;

    $form['transit_gateway'] = [
      '#type' => 'details',
      '#title' => $this->t('Transit gateway'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['transit_gateway']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#required'      => TRUE,
    ];

    $form['transit_gateway']['description'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Description'),
      '#maxlength'     => 255,
      '#size'          => 60,
    ];

    $form['transit_gateway']['amazon_side_asn'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Amazon side ASN'),
      '#description'   => $this->t('Enter the Autonomous System Number (ASN) for the AWS side of a Border Gateway Protocol (BGP) session. You can use the default ASN, or you can specify a private ASN in the 64512-65534 or 4200000000-4294967294 ranges.'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#attributes'    => ['placeholder' => '64512-65534 or 4200000000-4294967294'],
    ];

    $form['transit_gateway']['dns_support'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('DNS support'),
      '#description'   => $this->t('Enable Domain Name System resolution for VPCs attached to this transit gateway.'),
      '#default_value' => TRUE,
    ];

    $form['transit_gateway']['vpn_ecmp_support'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('VPN ECMP support'),
      '#description'   => $this->t('Equal-cost multi-path routing for VPN Connections that are attached to this transit gateway.'),
      '#default_value' => TRUE,
    ];

    $form['transit_gateway']['default_route_table_association'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Default route table association'),
      '#description'   => $this->t("Automatically associate transit gateway attachments with this transit gateway's default route table."),
      '#default_value' => TRUE,
    ];

    $form['transit_gateway']['default_route_table_propagation'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Default route table propagation'),
      '#description'   => $this->t("Automatically propagate transit gateway attachments with this transit gateway's default route table."),
      '#default_value' => TRUE,
    ];

    $form['transit_gateway']['multicast_support'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Multicast support'),
      '#description'   => $this->t('Enable the ability to create multicast domains in this transit gateway.'),
    ];

    $form['transit_gateway']['auto_accept_shared_attachments'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Auto accept shared attachments'),
      '#description'   => $this->t('Automatically accept cross account attachments that are attached to this transit gateway.'),
    ];

    unset($form['tags']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $amazon_side_asn = $form_state->getValue('amazon_side_asn');
    if (empty($amazon_side_asn)) {
      return;
    }

    $message = $this->t('Specify a private ASN in the 64512-65534 or 4200000000-4294967294 ranges.');
    if (!ctype_digit($amazon_side_asn)) {
      $form_state->setErrorByName('amazon_side_asn', $message);
      return;
    }

    $amazon_side_asn = intval($amazon_side_asn);
    if (!($amazon_side_asn >= 64512 && $amazon_side_asn <= 65534) && !($amazon_side_asn >= 4200000000 && $amazon_side_asn <= 4294967294)) {
      $form_state->setErrorByName('amazon_side_asn', $message);
    }
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->createTransitGateway($this->entity, $form, $form_state);
  }

}
