<?php

namespace Drupal\aws_cloud\Form\Vpc;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Form\Ec2\AwsCloudContentForm;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the transit gateway entity edit forms.
 *
 * @ingroup aws_cloud
 */
class TransitGatewayEditForm extends AwsCloudContentForm {

  use CloudContentEntityTrait;

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * CarrierGatewayEditForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManagerInterface $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    CloudServiceInterface $cloud_service,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = '') {
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;
    if (!$this->updateEntityBuildForm($entity)) {
      return $this->redirectUrl;
    }
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $weight = -50;

    $form['transit_gateway'] = [
      '#type' => 'details',
      '#title' => $this->t('Transit gateway'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['transit_gateway']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->label(),
      '#required'      => TRUE,
    ];

    $form['transit_gateway']['description'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Description'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->getDescription(),
    ];

    $form['transit_gateway']['transit_gateway_id'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Transit gateway ID')),
      '#markup'        => $entity->getTransitGatewayId(),
    ];

    $form['transit_gateway']['state'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('State')),
      '#markup'        => $entity->getState(),
    ];

    $form['transit_gateway']['amazon_side_asn'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Amazon side ASN')),
      '#markup'        => $entity->getAmazonSideAsn(),
    ];

    $form['transit_gateway']['multicast_support'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Multicast support')),
      '#markup'        => $entity->isMulticastSupport() ? 'enable' : 'disable',
    ];

    $form['transit_gateway']['created'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Created')),
      '#markup'        => $this->dateFormatter->format($entity->created(), 'short'),
    ];

    $form['transit_gateway']['dns_support'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('DNS support'),
      '#description'   => $this->t('Enable Domain Name System resolution for VPCs attached to this transit gateway.'),
      '#default_value' => $entity->isDnsSupport(),
    ];

    $form['transit_gateway']['vpn_ecmp_support'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('VPN ECMP support'),
      '#description'   => $this->t('Equal-cost multi-path routing for VPN Connections that are attached to this transit gateway.'),
      '#default_value' => $entity->isVpnEcmpSupport(),
    ];

    $form['transit_gateway']['default_route_table_association'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Default route table association'),
      '#description'   => $this->t("Automatically associate transit gateway attachments with this transit gateway's default route table."),
      '#default_value' => $entity->isDefaultRouteTableAssociation(),
    ];

    $options = $this->getTransitGatewayRouteTables();
    $form['transit_gateway']['association_default_route_table_id'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Association Default Route Table ID'),
      '#options'       => $options,
      '#default_value' => $entity->getAssociationDefaultRouteTableId(),
      '#states' => [
        'visible' => [
          ':input[name="default_route_table_association"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['transit_gateway']['default_route_table_propagation'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Default route table propagation'),
      '#description'   => $this->t("Automatically propagate transit gateway attachments with this transit gateway's default route table."),
      '#default_value' => $entity->isDefaultRouteTablePropagation(),
    ];

    $form['transit_gateway']['propagation_default_route_table_id'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Propagation Default Route Table ID'),
      '#options'       => $options,
      '#default_value' => $entity->getPropagationDefaultRouteTableId(),
      '#states' => [
        'visible' => [
          ':input[name="default_route_table_propagation"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['transit_gateway']['auto_accept_shared_attachments'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Auto accept shared attachments'),
      '#description'   => $this->t('Automatically accept cross account attachments that are attached to this transit gateway.'),
      '#default_value' => $entity->isAutoAcceptSharedAttachments(),
    ];

    $form['fieldset_tags'] = [
      '#type'          => 'details',
      '#title'         => $this->t('Tags'),
      '#open'          => TRUE,
      '#weight'        => $weight++,
    ];

    $form['fieldset_tags'][] = $form['tags'];
    unset($form['tags']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);
    $form['actions']['#weight'] = $weight++;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->editTransitGateway($this->entity, $form, $form_state);
  }

  /**
   * Get options of transit gateway route table.
   *
   * @return array
   *   The options of transit gateway route table.
   */
  private function getTransitGatewayRouteTables(): array {
    $entity = $this->entity;
    $tables = $this->entityTypeManager
      ->getStorage('aws_cloud_transit_gateway_route')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'transit_gateway_id' => $entity->getTransitGatewayId(),
      ]);

    $options = [];
    foreach ($tables ?: [] as $table) {
      $options[$table->getTransitGatewayRouteTableId()] = $table->getTransitGatewayRouteTableId();
    }

    return $options;
  }

}
