<?php

namespace Drupal\aws_cloud\Form\Vpc;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Form\Ec2\AwsDeleteUpdateEntityForm;

/**
 * Accept an AWS VPC peering connection.
 */
class VpcPeeringConnectionAcceptForm extends AwsDeleteUpdateEntityForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    $entity = $this->entity;

    return $this->t('Are you sure you want to accept VPC peering connection: %name?', [
      '%name' => $entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Accept');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If acceptVpcPeeringConnection has some error.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $entity = $this->entity;

    $this->ec2Service->setCloudContext($this->entity->getCloudContext());
    $result = $this->ec2Service->acceptVpcPeeringConnection([
      'VpcPeeringConnectionId' => $this->entity->getVpcPeeringConnectionId(),
    ]);

    $form_state->setRedirect("view.{$this->entity->getEntityTypeId()}.list", ['cloud_context' => $this->entity->getCloudContext()]);
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'accepted');
      return;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'accepted remotely');
      return;
    }

    // Update the VPC.
    $this->ec2Service->updateVpcPeeringConnections([
      'VpcPeeringConnectionIds' => [$this->entity->getVpcPeeringConnectionId()],
    ], FALSE);

    $this->processOperationStatus($entity, 'accepted');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
  }

}
