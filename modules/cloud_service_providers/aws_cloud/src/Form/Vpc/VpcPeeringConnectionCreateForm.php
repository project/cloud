<?php

namespace Drupal\aws_cloud\Form\Vpc;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Form\Ec2\AwsCloudContentForm;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the VPC peering connection entity create form.
 *
 * @ingroup aws_cloud
 */
class VpcPeeringConnectionCreateForm extends AwsCloudContentForm {

  use CloudContentEntityTrait;

  use AwsCloudFormTrait;

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  protected $awsCloudOperationsService;

  /**
   * VPC constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManagerInterface $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    CloudServiceInterface $cloud_service,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->awsCloudOperationsService = $aws_cloud_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    /** @var \Drupal\aws_cloud\Entity\Ec2\Snapshot $entity */
    $form = parent::buildForm($form, $form_state);
    $this->ec2Service->setCloudContext($cloud_context);

    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

    $weight = -50;

    $form['vpc_peering_connection'] = [
      '#type' => 'details',
      '#title' => $this->t('VPC peering connection'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['vpc_peering_connection']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#required'      => TRUE,
    ];

    $vpcs = $this->getVpcOptions($cloud_context);
    natcasesort($vpcs);
    $form['vpc_peering_connection']['requester_vpc_id'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Requester VPC ID'),
      '#description'   => $this->t('Select the Requester VPC ID.'),
      '#options'       => $vpcs,
      '#required'      => TRUE,
    ];

    $form['vpc_peering_connection']['accepter_account_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Accepter AWS account ID'),
      '#description'   => $this->t('The accepter AWS account ID.'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $cloud_config->field_account_id->value,
      '#required'      => FALSE,
      '#ajax' => [
        'event'    => 'change',
        'callback' => '::accepterVpcIdSelectAjaxCallback',
        'wrapper'  => 'accepter-vpc-id-wrapper',
      ],
    ];

    $form['vpc_peering_connection']['accepter_region'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Accepter region'),
      '#description'   => $this->t('Select the accepter region.'),
      '#options'       => $this->ec2Service->getRegions(),
      '#default_value' => $cloud_config->field_region->value,
      '#required'      => FALSE,
      '#ajax' => [
        'callback' => '::accepterVpcIdSelectAjaxCallback',
        'wrapper'  => 'accepter-vpc-id-wrapper',
      ],
    ];

    $form['vpc_peering_connection']['accepter_vpc_id_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'accepter-vpc-id-wrapper',
      ],
    ];

    $accepter_region = $cloud_config->field_region->value;
    if (!empty($form_state->getValue('accepter_region'))) {
      $accepter_region = $form_state->getValue('accepter_region');
    }
    $accepter_account_id = $cloud_config->field_account_id->value;
    if (!empty($form_state->getValue('accepter_account_id'))) {
      $accepter_account_id = $form_state->getValue('accepter_account_id');
    }
    if ($cloud_config->field_region->value === $accepter_region
      && $cloud_config->field_account_id->value === $accepter_account_id) {
      $form['vpc_peering_connection']['accepter_vpc_id_wrapper']['accepter_vpc_id'] = [
        '#type'          => 'select',
        '#title'         => $this->t('Accepter VPC ID'),
        '#description'   => $this->t('The accepter VPC ID.'),
        '#options'       => $vpcs,
        '#name'          => 'accepter_vpc_id',
        '#required'      => TRUE,
      ];
    }
    else {
      $form['vpc_peering_connection']['accepter_vpc_id_wrapper']['accepter_vpc_id'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('Accepter VPC ID'),
        '#description'   => $this->t('The accepter VPC ID.'),
        '#maxlength'     => 255,
        '#size'          => 60,
        '#name'          => 'accepter_vpc_id',
        '#required'      => TRUE,
      ];
    }

    unset($form['tags']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->awsCloudOperationsService->createVpcPeeringConnection($this->entity, $form, $form_state);
  }

  /**
   * AJAX callback for select form accepter VPC ID.
   *
   * @param array &$form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   Response.
   */
  public function accepterVpcIdSelectAjaxCallback(array &$form, FormStateInterface $form_state): array {
    return $form['vpc_peering_connection']['accepter_vpc_id_wrapper'];
  }

}
