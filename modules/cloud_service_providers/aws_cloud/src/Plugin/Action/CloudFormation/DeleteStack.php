<?php

namespace Drupal\aws_cloud\Plugin\Action\CloudFormation;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a stack form.
 *
 * @Action(
 *   id = "entity:delete_action:aws_cloud_stack",
 *   label = @Translation("Delete stack"),
 *   type = "aws_cloud_stack"
 * )
 */
class DeleteStack extends DeleteAction {

}
