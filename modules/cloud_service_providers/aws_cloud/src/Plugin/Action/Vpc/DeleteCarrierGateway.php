<?php

namespace Drupal\aws_cloud\Plugin\Action\Vpc;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a carrier gateway form.
 *
 * @Action(
 *   id = "entity:delete_action:aws_cloud_carrier_gateway",
 *   label = @Translation("Delete carrier gateway"),
 *   type = "aws_cloud_carrier_gateway"
 * )
 */
class DeleteCarrierGateway extends DeleteAction {

}
