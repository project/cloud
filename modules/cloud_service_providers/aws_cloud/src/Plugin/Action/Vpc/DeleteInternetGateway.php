<?php

namespace Drupal\aws_cloud\Plugin\Action\Vpc;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to an internet gateway form.
 *
 * @Action(
 *   id = "entity:delete_action:aws_cloud_internet_gateway",
 *   label = @Translation("Delete internet gateway"),
 *   type = "aws_cloud_internet_gateway"
 * )
 */
class DeleteInternetGateway extends DeleteAction {

}
