<?php

namespace Drupal\aws_cloud\Plugin\Action\Vpc;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a transit gateway form.
 *
 * @Action(
 *   id = "entity:delete_action:aws_cloud_transit_gateway",
 *   label = @Translation("Delete transit gateway"),
 *   type = "aws_cloud_transit_gateway"
 * )
 */
class DeleteTransitGateway extends DeleteAction {

}
