<?php

namespace Drupal\aws_cloud\Plugin\Action\Vpc;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a VPC peering connection form.
 *
 * @Action(
 *   id = "entity:delete_action:aws_cloud_vpc_peering_connection",
 *   label = @Translation("Delete VPC peering connection"),
 *   type = "aws_cloud_vpc_peering_connection"
 * )
 */
class DeleteVpcPeeringConnection extends DeleteAction {

}
