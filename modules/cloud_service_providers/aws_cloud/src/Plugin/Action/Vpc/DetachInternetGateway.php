<?php

namespace Drupal\aws_cloud\Plugin\Action\Vpc;

use Drupal\cloud\Plugin\Action\OperateAction;

/**
 * Redirects to an internet gateway detach form.
 *
 * @Action(
 *   id = "aws_cloud_internet_gateway_detach_action",
 *   label = @Translation("Detach internet gateway"),
 *   type = "aws_cloud_internet_gateway",
 *   confirm_form_route_name
 *     = "entity.aws_cloud_internet_gateway.detach_multiple_form"
 * )
 */
class DetachInternetGateway extends OperateAction {

  /**
   * {@inheritdoc}
   */
  protected function getOperation(): string {
    return 'detach';
  }

}
