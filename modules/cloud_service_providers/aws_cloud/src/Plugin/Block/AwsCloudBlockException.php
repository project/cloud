<?php

namespace Drupal\aws_cloud\Plugin\Block;

/**
 * Provides AwsCloudBlockException class.
 */
class AwsCloudBlockException extends \Exception {

}
