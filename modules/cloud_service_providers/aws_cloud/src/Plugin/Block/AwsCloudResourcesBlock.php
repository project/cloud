<?php

namespace Drupal\aws_cloud\Plugin\Block;

use Drupal\Core\Link;
use Drupal\cloud\Plugin\Block\CloudBaseResourcesBlock;

/**
 * Provides a block displaying system resources.
 *
 * @Block(
 *   id = "aws_cloud_resources_block",
 *   admin_label = @Translation("AWS resources"),
 *   category = @Translation("AWS Cloud")
 * )
 */
class AwsCloudResourcesBlock extends CloudBaseResourcesBlock {

  /**
   * {@inheritdoc}
   */
  protected function getEntityBundleType(): string {
    return 'aws_cloud';
  }

  /**
   * Build a resource HTML table.
   *
   * @return array
   *   Table render array.
   */
  protected function buildResourceTable(): array {
    $resources = [
      'aws_cloud_instance' => [
        'view any aws cloud instance',
        [],
      ],
      'aws_cloud_image' => [
        'view any aws cloud image',
        [],
      ],
      'aws_cloud_security_group' => [
        'view any aws cloud security group',
        [],
      ],
      'aws_cloud_elastic_ip' => [
        'view any aws cloud elastic ip',
        [],
      ],
      'aws_cloud_network_interface' => [
        'view any aws cloud network interface',
        [],
      ],
      'aws_cloud_key_pair' => [
        'view any aws cloud key pair',
        [],
      ],
      'aws_cloud_volume' => [
        'view any aws cloud volume',
        [],
      ],
      'aws_cloud_snapshot' => [
        'view any aws cloud snapshot',
        [],
      ],
      'aws_cloud_vpc' => [
        'view any aws cloud vpc',
        [],
      ],
      'aws_cloud_subnet' => [
        'view any aws cloud subnet',
        [],
      ],
      'aws_cloud_vpc_peering_connection' => [
        'view any aws cloud vpc peering connection',
        [],
      ],
      'aws_cloud_internet_gateway' => [
        'view any aws cloud internet gateway',
        [],
      ],
      'aws_cloud_carrier_gateway' => [
        'view any aws cloud carrier gateway',
        [],
      ],
      'aws_cloud_transit_gateway' => [
        'view any aws cloud transit gateway',
        [],
      ],
      'instance_type_prices' => [
        'Instance type prices',
      ],
    ];

    $rows = $this->buildResourceTableRows($resources);

    return [
      '#type' => 'table',
      '#rows' => $rows,
    ];
  }

  /**
   * Build a resource table row.
   *
   * @param array $resources
   *   Entity array to build the row.
   *
   * @return array
   *   Fully built rows.
   */
  protected function buildResourceTableRows(array $resources): array {
    $rows = [];
    $data = [];
    $i = 0;
    $index = 0;

    foreach ($resources ?: [] as $key => $values) {
      $data[] = $key === 'instance_type_prices'
        ? $this->getInstanceTypePricingLink($key, $values[0])
        : $this->getResourceLink($key, $values[0], $values[1]);

      // Skip if $data does not have any value.
      if (empty($data[count($data) - 1])) {
        continue;
      }

      $rows[$index] = [
        'no_striping' => TRUE,
        'data' => $data,
      ];
      if ($i++ % 2) {
        $index++;
        $data = [];
      }
    }
    return $rows;
  }

  /**
   * Generate AWS instance type pricing link.
   *
   * @param string $resource_type
   *   The resource type.
   * @param string $label
   *   The link label.
   *
   * @return array
   *   The AWS resource link.
   */
  private function getInstanceTypePricingLink($resource_type, $label): array {

    // Return immediately if $cloud_context is empty.
    $cloud_context = $this->configuration['cloud_context'];
    if (empty($cloud_context)) {
      return [];
    }
    $instance = count(aws_cloud_get_instance_types($cloud_context));
    $pricing_label = "$instance $label";
    $link = Link::createFromRoute($pricing_label, "aws_cloud.{$resource_type}",
      ['cloud_context' => $cloud_context]
    );

    return $link;
  }

}
