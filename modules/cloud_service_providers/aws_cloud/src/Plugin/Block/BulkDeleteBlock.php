<?php

namespace Drupal\aws_cloud\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\system\ActionConfigEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract base class for blocks that needs a bulk delete form.
 */
abstract class BulkDeleteBlock extends BlockBase implements ContainerFactoryPluginInterface, FormInterface {

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The action storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $actionStorage;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The bulk operations.
   *
   * @var \Drupal\system\Entity\Action[]
   */
  protected $actions;

  /**
   * The entities being listed.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $entities = [];

  /**
   * Entity key for bulk form.
   *
   * @var string
   */
  protected $entitiesKey = 'entities';

  /**
   * Entity type ID.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * Cloud context variable for multiple form building.
   *
   * @var string
   */
  protected $cloudContext;

  /**
   * Creates a ResourcesBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The action storage.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    FormBuilderInterface $form_builder,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    try {
      $this->actionStorage = $this->entityTypeManager->getStorage('action');
    }
    catch (\Exception $e) {
      // NOTE: $this->messenger() is correct.
      // cf. MessengerTrait::messenger() MessengerInterface.
      $this->messenger()->addError("An error occurred: {$e->getMessage()}");
    }
    $this->formBuilder = $form_builder;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('plugin.manager.cloud_config_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'cloud_context' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['cloud_context'] = [
      '#type' => 'select',
      '#title' => $this->t('Cloud service provider'),
      '#description' => $this->t('Select cloud service provider.'),
      '#options' => $this->cloudConfigPluginManager
        ->getCloudConfigs($this->t('All AWS Cloud regions'), 'aws_cloud'),
      '#default_value' => $this->configuration['cloud_context'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['cloud_context']
      = $form_state->getValue('cloud_context');
  }

  /**
   * Set the entity type ID.
   *
   * @param string $entity_type_id
   *   Entity type id to set.
   */
  protected function setEntityTypeId($entity_type_id): void {
    $this->entityTypeId = $entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    // Before the form is built. Set the cloud_context variable.
    // That way, the form building code has access to which region
    // the form is being built for.
    if (!empty($this->configuration['cloud_context'])) {
      $this->cloudContext = $this->configuration['cloud_context'];
      return $this->formBuilder->getForm($this);
    }
    else {
      // Create one multi-select form per region.
      $form = [];
      $regions = $this->cloudConfigPluginManager->loadConfigEntities('aws_cloud');
      foreach ($regions ?: [] as $region) {
        $this->cloudContext = $region->getCloudContext();
        $form[] = $this->formBuilder->getForm($this);
      }
      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    return $this->buildBulkForm();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entities = [];
    $selected = array_filter($form_state->getValue($this->entitiesKey));
    $action = $this->actions[$form_state->getValue('action')];
    $count = 0;
    // Determine the region to submit the form for.
    $cloud_context = $form_state->getValue('cloud_context');

    // Do not continue with submit if there is no cloud_context.
    if (empty($cloud_context)) {
      return;
    }

    foreach ($selected ?: [] as $id) {
      $entity = $this->entities[$cloud_context][$id];
      // Skip execution if the user did not have access.
      if (!$action->getPlugin()->access($entity)) {
        // NOTE: $this->messenger() is correct.
        // cf. MessengerTrait::messenger() MessengerInterface.
        $this->messenger()->addError($this->t('No access to execute %action on the @entity_type_label %entity_label.', [
          '%action' => $action->label(),
          '@entity_type_label' => $entity->getEntityType()->getSingularLabel(),
          '%entity_label' => $entity->label(),
        ]));
        continue;
      }

      $count++;
      $entities[$id] = $entity;
    }

    // Do not perform any action unless there are some elements affected.
    // @see https://www.drupal.org/project/drupal/issues/3018148
    if (!$count) {
      return;
    }

    $action->execute($entities);

    $operation_definition = $action->getPluginDefinition();
    if (!empty($operation_definition['confirm_form_route_name'])) {
      $form_state->setRedirect(
        $operation_definition['confirm_form_route_name'],
        ['cloud_context' => $cloud_context],
        []
      );
    }
    else {
      // NOTE: $this->messenger() is correct.
      // cf. MessengerTrait::messenger() MessengerInterface.
      $this->messenger()->addWarning($this->formatPlural($count, '%action was not applied to @count item.', '%action was applied to @count items.', [
        '%action' => $action->label(),
      ]));
    }
  }

  /**
   * Set the actions that can be performed.
   *
   * @param array $params
   *   The params to used to query the Action Storage entities.
   *
   * @throws \Drupal\aws_cloud\Plugin\Block\AwsCloudBlockException
   */
  protected function setDeleteActions(array $params): void {
    if (empty($this->entityTypeId)) {
      // NOTE: According to Drupal coding standards,
      // exceptions should not be translated.
      throw new AwsCloudBlockException('No entity type id found.');
    }
    $entity_type_id = $this->entityTypeId;
    $this->actions = array_filter(
      $this->actionStorage
        ->loadByProperties($params),
      static function (ActionConfigEntityInterface $action) use ($entity_type_id) {
        return $action->getType() === $entity_type_id;
      }
    );
  }

  /**
   * Get the delete action.
   *
   * @return array
   *   Array of delete actions.
   */
  protected function getDeleteActions(): array {
    $action_options = [];
    foreach ($this->actions ?: [] as $id => $action) {
      $action_options[$id] = $action->label();
    }
    return $action_options;
  }

  /**
   * Build the table header.
   *
   * @return array
   *   Table header.
   */
  protected function buildTableHeader(): array {
    $header = [];
    $header['header'] = [
      '#type' => 'container',
      '#weight' => -100,
      '#attributes' => [
        'class' => [
          'actions',
        ],
      ],
    ];

    $header['header']['action'] = [
      'action' => [
        '#type' => 'select',
        '#title' => $this->t('Action'),
        '#options' => $this->getDeleteActions(),
      ],
    ];
    $header['header']['action-button'] = $this->buildActions();

    $header[$this->entitiesKey] = [
      '#type' => 'table',
      '#header' => [
        ['data' => $this->t('Name')],
      ],
      '#tableselect' => TRUE,
      '#attached' => [
        'library' => ['core/drupal.tableselect'],
      ],
    ];

    return $header;
  }

  /**
   * Helper method to build the actions array.
   *
   * @return array
   *   Actions array.
   */
  protected function buildActions(): array {
    return [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Apply to selected items'),
        '#button_type' => 'primary',
      ],
    ];
  }

  /**
   * Helper method to build a table row.
   *
   * @param string $id
   *   ID for select box column.
   * @param string $row_text
   *   Text to display for name column.
   *
   * @return array
   *   An array of rows.
   */
  protected function buildTableRow($id, $row_text): array {
    $row = [];
    $row['select'] = [
      '#id' => 'edit-entities-' . $id,
      '#type' => 'checkbox',
      '#title' => $this->t('Update this item'),
      '#title_display' => 'invisible',
      '#return_value' => $id,
      '#wrapper_attributes' => [
        'class' => ['table-select'],
      ],
      '#attributes' => [
        'data-drupal-selector' => 'edit-entities',
      ],
      '#parents' => [
        'entities',
        $id,
      ],
    ];
    $row['name']['data'] = $row_text;
    return $row;
  }

  /**
   * Build the opening fieldset form element.
   *
   * @param string $title
   *   Title to use for the fieldset.
   *
   * @return array
   *   Form element array.
   */
  public function buildFieldSet($title): array {
    return [
      '#type' => 'details',
      '#title' => $title,
      '#open' => TRUE,
      '#attributes' => [
        'class' => [
          'bulk-delete-form',
        ],
      ],
    ];
  }

  /**
   * Helper method to get the region name given the cloud_context.
   *
   * @return string
   *   Region name or NULL.
   */
  protected function getRegionName(): ?string {
    $this->cloudConfigPluginManager->setCloudContext($this->cloudContext);
    $region = $this->cloudConfigPluginManager->loadConfigEntity();
    return !empty($region) ? $region->getName() : NULL;
  }

  /**
   * All extending classes will implement buildBulkForm().
   *
   * @return array
   *   Form array containing bulk form.
   */
  abstract protected function buildBulkForm(): array;

}
