<?php

namespace Drupal\aws_cloud\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\aws_cloud\Traits\AwsInstanceMonitorTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block displaying an instance monitor for the AWS namespace.
 *
 * @Block(
 *   id = "aws_cloud_instance_monitor_block",
 *   admin_label = @Translation("AWS instance monitor"),
 *   category = @Translation("AWS Cloud")
 * )
 */
class InstanceMonitorBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use AwsInstanceMonitorTrait;
  use StringTranslationTrait;

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The page cache kill switch service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Creates an InstanceMonitorBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match service.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   The page cache kill switch service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $configFactory,
    RouteMatchInterface $routeMatch,
    KillSwitch $kill_switch,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->routeMatch = $routeMatch;
    $this->killSwitch = $kill_switch;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_route_match'),
      $container->get('page_cache_kill_switch'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return (parent::defaultConfiguration() ?: []) + [
      'display_on_instance_info_only' => 1,
      'chart_type' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    // Provide an option in admin form to display only on instance info page.
    $form['display_on_instance_info_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this block on AWS instance details page only'),
      '#default_value' => $this->configuration['display_on_instance_info_only'] ?? 1,
    ];

    $form['chart_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Chart type'),
      '#default_value' => $this->configuration['chart_type'] ?: 0,
      '#options' => [
        1 => $this->t('Horizon chart'),
        0 => $this->t('Line chart'),
      ],
    ];

    // Provide a NOTE:
    $form['block_description'] = [
      '#markup' => '<div class="alert alert-info">' . $this->t('NOTE: This block will not be displayed on the instance monitor page.') . '</div>',
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $this->configuration['display_on_instance_info_only']
      = $form_state->getValue('display_on_instance_info_only');
    $this->configuration['chart_type']
      = $form_state->getValue('chart_type');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    // We only need to check for the instance,
    // since we are only providing the instance monitor.
    $cloud_instance = $this->routeMatch->getParameter('aws_cloud_instance');

    // If display_on_instance_info_only is checked, check route name.
    $route_name = $this->routeMatch->getRouteName();
    if ($route_name !== 'entity.aws_cloud_instance.canonical'
        && $this->configuration['display_on_instance_info_only']) {
      return [];
    }
    // Hide on instance monitor page.
    if ($route_name === 'entity.aws_cloud_instance.monitor_form') {
      return [];
    }

    // If there is no context and no instance, we return a message.
    $block_message = '';
    if (!$cloud_instance) {
      $block_message = $this->t('There was no AWS instance identified from the context.');
    }

    $build = [];

    $chart_type = $this->configuration['chart_type'];
    $this->buildChart($build, $chart_type);

    array_walk($build, static function (&$chart, $name) use ($chart_type, $block_message) {
      empty($chart_type)
        ? $chart["{$name}_chart"]['#markup'] = $block_message
        : $chart['#markup'] = $block_message;
    });

    $build['#attached']['drupalSettings']['aws_cloud_monitor_refresh_interval'] = $this->configFactory
      ->get('aws_cloud.settings')
      ->get('aws_cloud_monitor_refresh_interval');

    // Provide the metrics_url.
    if ($cloud_instance) {
      $metrics_string = 'internal:/clouds/aws_cloud/' . $cloud_instance->get('cloud_context')->value . '/instance/' . $cloud_instance->id() . '/metrics';
      $metrics_url = Url::fromUri($metrics_string)->toString();
      $build['#attached']['drupalSettings']['aws_cloud_monitor_metrics_url'] = $metrics_url ?: 'metrics';
      // We are only attaching the library if we have identified an instance.
      // This way we are not getting 404 in the console from javascript.
      $build['#attached']['library'][] = empty($this->configuration['chart_type'])
        ? 'aws_cloud/aws_cloud_instance_monitor'
        : 'aws_cloud/aws_cloud_instance_monitor_horizon_chart';
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    $this->killSwitch->trigger();
    // BigPipe/#cache/max-age is breaking my block javascript
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2016-07-17/bigpipecachemax-age-is-breaking-my
    // "a slight delay of a second or two before the charts are built.
    // That seems to work, though it is a junky solution.".
    return $this->moduleHandler->moduleExists('big_pipe') ? 1 : 0;
  }

}
