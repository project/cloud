<?php

namespace Drupal\aws_cloud\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a block displaying long-running instances.
 *
 * @Block(
 *   id = "aws_cloud_long_running_instances_block",
 *   admin_label = @Translation("Long running instances"),
 *   category = @Translation("AWS Cloud")
 * )
 */
class LongRunningInstancesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The page cache kill switch service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Creates a LongRunningInstancesBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   The page cache kill switch service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    RequestStack $request_stack,
    KillSwitch $kill_switch,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->request = $request_stack->getCurrentRequest();
    $this->killSwitch = $kill_switch;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('page_cache_kill_switch'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return $this->buildInstanceList();
  }

  /**
   * Build a list of long-running instances.
   *
   * @return array
   *   Array of instance URLs.
   */
  private function buildInstanceList(): array {
    $build = [];
    $build['instances'] = [
      '#type' => 'details',
      '#title' => $this->t('Long running instances'),
      '#open' => TRUE,
      // Attach DataTables.
      '#attached' => [
        'library' => [
          'cloud/datatables',
        ],
      ],
    ];
    // Add the datatable class.
    $build['instances']['#attributes']['class'][] = 'simple-datatable';

    $instances = $this->getLongRunningInstances();
    if (count($instances)) {
      $rows = [];
      foreach ($instances ?: [] as $instance) {
        $days_running = $instance->daysRunning();
        $days_format_plural = $this->formatPlural($days_running, '1 day', '@count days');

        $rows[] = [
          'name' => $instance->toLink($instance->getName()),
          'days' => [
            'data' => $days_format_plural,
            'data-content' => $days_running,
          ],
        ];
      }

      $unused_days = $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_long_running_instance_notification_criteria');
      $build['instances'][] = [
        '#markup' => $this->t('The following instances have been running for more than %num days', ['%num' => $unused_days]),
      ];

      $headers = [
        ['data' => $this->t('Name')],
        ['data' => $this->t('Days')],
      ];

      $build['instances'][] = [
        '#theme' => 'table',
        '#header' => $headers,
        '#rows' => $rows,
      ];
    }
    else {
      $build['instances'][] = [
        '#markup' => $this->t('Great job! You have no long-running instances.'),
      ];
    }
    return $build;
  }

  /**
   * Get a list of long-running instances.
   *
   * @return array
   *   Array of long-running instances.
   */
  private function getLongRunningInstances(): array {
    $instances = aws_cloud_get_long_running_instances();
    if (!$this->currentUser->hasPermission('view any aws cloud instance')) {
      foreach ($instances ?: [] as $key => $instance) {
        if ($instance->getOwnerId() !== $this->currentUser->id()) {
          unset($instances[$key]);
        }
      }
    }
    return $instances;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    $this->killSwitch->trigger();
    // BigPipe/#cache/max-age is breaking my block javascript
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2016-07-17/bigpipecachemax-age-is-breaking-my
    // "a slight delay of a second or two before the charts are built.
    // That seems to work, though it is a junky solution.".
    return $this->moduleHandler->moduleExists('big_pipe') ? 1 : 0;
  }

}
