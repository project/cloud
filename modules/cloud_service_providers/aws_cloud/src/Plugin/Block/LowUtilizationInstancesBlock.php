<?php

namespace Drupal\aws_cloud\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\State\StateInterface;
use Drupal\aws_cloud\Service\CloudWatch\LowUtilizationInstanceChecker;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block displaying low utilization instances.
 *
 * @Block(
 *   id = "aws_cloud_low_utilization_instances_block",
 *   admin_label = @Translation("Low utilization instances"),
 *   category = @Translation("AWS Cloud")
 * )
 */
class LowUtilizationInstancesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use CloudContentEntityTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Low utilization instance checker.
   *
   * @var \Drupal\aws_cloud\Service\CloudWatch\LowUtilizationInstanceChecker
   */
  protected $lowUtilizationInstanceChecker;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Cache object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheDefault;

  /**
   * State object.
   *
   * @var \Drupal\aws_cloud\Plugin\Block\StateInterface
   */
  protected $state;

  /**
   * Cache key.
   *
   * @var string
   */
  private $cacheKey = 'low_utilization_block_cache_interval';

  /**
   * State key.
   *
   * @var string
   */
  private $stateKey = 'low_utilization_block_last_refresh';

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Creates a LongRunningInstancesBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\aws_cloud\Service\CloudWatch\LowUtilizationInstanceChecker $low_utilization_instance_checker
   *   The low utilization instance checker.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_default
   *   The Cache object.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state object.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    LowUtilizationInstanceChecker $low_utilization_instance_checker,
    ConfigFactoryInterface $config_factory,
    CacheBackendInterface $cache_default,
    StateInterface $state,
    DateFormatterInterface $date_formatter,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->lowUtilizationInstanceChecker = $low_utilization_instance_checker;
    $this->configFactory = $config_factory;
    $this->cacheDefault = $cache_default;
    $this->state = $state;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('aws_cloud.low_utilization_instances_checker'),
      $container->get('config.factory'),
      $container->get('cache.default'),
      $container->get('state'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $config = parent::defaultConfiguration();
    // Default the cache to 1 hour.
    $config['low_utilization_block_cache_interval'] = 3600;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $form['low_utilization_block_cache_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Low utilization cache setting'),
      '#options' => [
        -1 => $this->t('No cache'),
        3600 => $this->t('1 hour'),
        7200 => $this->t('2 hours'),
        10800 => $this->t('3 hours'),
        21600 => $this->t('6 hours'),
        86400 => $this->t('1 day'),
      ],
      '#description' => $this->t('Amount of time to cache low utilization results before fetching them again.
      Select No cache to always get results from AWS.  No cache is not recommended and can slow down page rendering.'),
      '#default_value' => $this->configuration['low_utilization_block_cache_interval'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $this->configuration['low_utilization_block_cache_interval']
      = $form_state->getValue('low_utilization_block_cache_interval');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return $this->buildInstanceList();
  }

  /**
   * Build a list of low utilization instances.
   *
   * @return array
   *   Array of instance URLs.
   */
  private function buildInstanceList(): array {
    $build = [];
    $build['instances'] = [
      '#type' => 'details',
      '#title' => $this->t('Low utilization instances'),
      '#open' => TRUE,
    ];

    $instances = $this->getLowUtilizationInstances();
    if (empty($instances)) {
      $build['instances'][] = [
        '#markup' => $this->t('Great job! You have no low utilization instances.'),
      ];
    }
    else {

      // NOTE: It is more appropriate to use a regular `function` rather than
      // `function static` since the functionality depends on the object's
      // context such as `$this->t()`.
      $urls = array_map(function ($instance) {
        return $instance->toLink($this->t('@instance', [
          '@instance' => $instance->getName(),
        ]));
      }, $instances);

      $cpu_threshold = $this->configFactory
        ->get('aws_cloud.settings')
        ->get('aws_cloud_low_utilization_instance_cpu_utilization_threshold');

      $network_threshold = $this->configFactory
        ->get('aws_cloud.settings')
        ->get('aws_cloud_low_utilization_instance_network_io_threshold');

      $period = $this->configFactory
        ->get('aws_cloud.settings')
        ->get('aws_cloud_low_utilization_instance_period');

      $build['instances'][] = [
        '#markup' => $this->t('The following instances are low utilization instances, whose daily CPU utilization was @cpu_threshold% or less and network I/O was @network_threshold MB or less in last @period days',
          [
            '@cpu_threshold' => $cpu_threshold,
            '@network_threshold' => $network_threshold,
            '@period' => $period,
          ]
        ),
      ];
      $build['instances'][] = [
        '#theme' => 'item_list',
        '#items' => $urls,
      ];
    }

    // Show last time data was updated if caching is turned on.
    if ((int) $this->configuration['low_utilization_block_cache_interval'] !== -1) {
      $last_refresh = $this->buildLastRefreshString();
      if (!empty($last_refresh)) {
        $build['instances'][]['#markup'] = $this->buildLastRefreshString();
      }
    }
    return $build;
  }

  /**
   * Helper method to build the last refresh string.
   *
   * @return string
   *   Refresh string.
   */
  private function buildLastRefreshString(): string {
    $last_refresh = $this->state->get($this->stateKey);
    $refresh_string = '';
    if (!empty($last_refresh)) {
      $refresh_string = '<div>' . $this->t('Last refresh: @refresh', [
        '@refresh' => $this->dateFormatter->format($last_refresh),
      ]) . '</div>';
    }
    return $refresh_string;
  }

  /**
   * Get a list of low utilization instances.
   *
   * @return array
   *   Array of low utilization instances.
   */
  private function getLowUtilizationInstances(): array {
    $low_instances = [];
    $low_utilization_block_cache_interval = (int) $this->configuration['low_utilization_block_cache_interval'];
    $cache = $this->cacheDefault->get($this->cacheKey);

    try {
      // Use cached values unless caching is turned off.
      if ($cache !== FALSE && $low_utilization_block_cache_interval !== -1) {
        $low_instances = $cache->data;
      }
      else {
        $low_instances = $this->entityTypeManager
          ->getStorage('aws_cloud_instance')
          ->loadByProperties([
            'instance_state' => 'running',
            'low_utilization' => FALSE,
          ]);

        // Store the low_instances in cache.
        // Store the last refresh timestamp in the state table.
        if ($low_utilization_block_cache_interval !== -1) {
          $this->cacheDefault->set($this->cacheKey, $low_instances, time() + $low_utilization_block_cache_interval);
          $this->state->set($this->stateKey, time());
        }
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $low_instances;
  }

}
