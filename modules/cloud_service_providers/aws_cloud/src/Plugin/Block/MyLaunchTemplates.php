<?php

namespace Drupal\aws_cloud\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\TableSort;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a block displaying an owner's launch templates.
 *
 * @Block(
 *   id = "aws_cloud_my_launch_templates_block",
 *   admin_label = @Translation("My Launch Templates"),
 *   category = @Translation("AWS Cloud")
 * )
 */
class MyLaunchTemplates extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Cloud Service.
   *
   * @var \Drupal\aws_cloud\Plugin\Block\CloudService
   */
  protected $cloudService;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Creates a Launch Template Block instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\cloud\Service\CloudService $cloud_service
   *   Cloud Service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    RequestStack $request_stack,
    CloudService $cloud_service,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->request = $request_stack->getCurrentRequest();
    $this->cloudService = $cloud_service;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('cloud'),
      $container->get('plugin.manager.cloud_config_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    // Do not show block for anonymous users.
    if ($this->currentUser->id() === 0) {
      return [];
    }
    return $this->buildTemplateList();
  }

  /**
   * Build a list of launch templates.
   *
   * @return array
   *   Array of instance URLs.
   */
  private function buildTemplateList(): array {
    $build['templates'] = [
      '#type' => 'details',
      '#title' => $this->t('My Launch Templates'),
      '#open' => TRUE,
    ];
    $headers = [
      ['data' => $this->t('Name'), 'field' => 'name'],
    ];
    $rows = [];
    $empty = '';

    $templates = $this->getMyLaunchTemplates();
    if (!empty($templates)) {
      // 1. Try to list up my templates.
      foreach ($templates ?: [] as $template) {
        $rows[] = [
          'name' => $template->toLink($template->getName()),
        ];
      }
    }
    else {
      // 2. If my templates are not found, try to list up available regions for
      // launch templates.
      $entities = $this->cloudConfigPluginManager->loadConfigEntities('aws_cloud');
      if (!empty($entities)) {
        // Change the title.
        $build['templates'][] = [
          '#markup' => $this->t('Create a Launch Template in the following @region.', [
            '@region' => $this->formatPlural(count($entities), 'region', 'regions'),
          ]),
        ];

        foreach ($entities ?: [] as $entity) {
          /** @var \Drupal\cloud\Entity\CloudConfig $entity */
          if ($this->currentUser->hasPermission('view ' . $entity->getCloudContext()) || $this->currentUser->hasPermission('view all cloud service providers')) {
            $rows[] = [
              'name' => Link::createFromRoute($entity->getName(), 'entity.cloud_launch_template.add_form', [
                'cloud_server_template_type' => 'aws_cloud',
                'cloud_context' => $entity->getCloudContext(),
              ]),
            ];
          }
        }
      }
      else {
        // 3. If my templates and available launch templates are not found,
        // display an error message.
        $empty = $this->t('There are no regions to create a Launch Template.  Contact the administrator.');
      }
    }

    // Use table sort to get the sort asc/desc.
    $sort = TableSort::getSort($headers, $this->request);
    // Use table sort to get the order by.
    $order = TableSort::getOrder($headers, $this->request)['sql'];

    // Sort data.
    usort($rows, static function ($a, $b) use ($sort, $order) {
      $result = $a[$order]->getText() < $b[$order]->getText() ? -1 : 1;

      if ($sort === 'desc') {
        $result *= -1;
      }

      return $result;
    });

    $build['templates'][] = [
      '#theme' => 'table',
      '#header' => $headers,
      '#empty' => $empty,
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * Get launch templates for the current user.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of entity objects indexed by their ids.
   */
  private function getMyLaunchTemplates(): array {
    $templates = [];
    try {
      $templates = $this->entityTypeManager->getStorage('cloud_launch_template')
        ->loadByProperties([
          'type' => 'aws_cloud',
          'uid' => $this->currentUser->id(),
        ]);
    }
    catch (\Exception $e) {
      $this->cloudService->handleException($e);
    }
    return $templates;
  }

}
