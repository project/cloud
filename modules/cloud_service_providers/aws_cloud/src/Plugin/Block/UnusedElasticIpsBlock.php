<?php

namespace Drupal\aws_cloud\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block displaying unused Elastic IPs.
 *
 * @Block(
 *   id = "aws_cloud_unused_elastic_ips_block",
 *   admin_label = @Translation("Unused Elastic IPs"),
 *   category = @Translation("AWS Cloud")
 * )
 */
class UnusedElasticIpsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $build['unused_elastic_ips'] = [
      '#type' => 'details',
      '#title' => $this->t('Unused Elastic IPs'),
      '#open' => TRUE,
    ];

    $unused_elastic_ips = aws_cloud_get_unused_elastic_ips();
    if (empty($unused_elastic_ips)) {
      $build['unused_elastic_ips'][] = [
        '#markup' => $this->t('You have no unused Elastic IPs.'),
      ];
    }
    else {
      $urls = array_map(static function ($elastic_ip) {
        if ($elastic_ip->getName() === $elastic_ip->getPublicIp()) {
          return $elastic_ip->toLink($elastic_ip->getPublicIp());
        }
        else {
          // Add name.
          $link = $elastic_ip->toLink($elastic_ip->getName())->toString();
          return ['#markup' => "$link ({$elastic_ip->getPublicIp()})"];
        }
      }, $unused_elastic_ips);

      $build['unused_elastic_ips'][] = [
        '#markup' => $this->t('The following Elastic IPs are unused.'),
      ];

      asort($urls);
      $build['unused_elastic_ips'][] = [
        '#theme' => 'item_list',
        '#items' => $urls,
      ];
    }

    return $build;
  }

}
