<?php

namespace Drupal\aws_cloud\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the 'availability_zone_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "availability_zone_formatter",
 *   label = @Translation("Availability Zone formatter"),
 *   field_types = {
 *     "string",
 *   }
 * )
 */
class AvailabilityZoneFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $entity = $items->getEntity();
    foreach ($items ?: [] as $delta => $item) {
      if (!$item->isEmpty()) {
        $elements[$delta] = [
          '#markup' => sprintf('%s (%s)', $item->value, $entity->getAvailabilityZoneId()),
        ];
      }
    }

    return $elements;
  }

}
