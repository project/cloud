<?php

namespace Drupal\aws_cloud\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the 'user_data_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "user_data_formatter",
 *   label = @Translation("User data formatter"),
 *   field_types = {
 *     "string_long",
 *   }
 * )
 */
class UserDataFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    foreach ($items ?: [] as $delta => $item) {

      $user_data_decoded = base64_decode($item->value ?? '');
      $is_user_data_binary = FALSE;

      // Check if the text is binary.
      if (preg_match('/[^\x20-\x7e]/', $user_data_decoded)) {
        $is_user_data_binary = TRUE;
      }

      $elements[$delta] = [
        '#markup' => sprintf('<div class="user-data-base64">%s</div><div class="user-data">%s</div>',
          $is_user_data_binary ? 'Base64 Encoded' : '',
          $is_user_data_binary ? $item->value : $user_data_decoded),
      ];
    }

    return $elements;
  }

}
