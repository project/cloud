<?php

namespace Drupal\aws_cloud\Plugin\Field\Util;

use Drupal\cloud\Plugin\Field\Util\ReservedKeyCheckerInterface;

/**
 * Aws cloud reserved key checker interface for key_value field type.
 */
class AwsCloudReservedKeyChecker implements ReservedKeyCheckerInterface {

  private const RESERVED_KEYS = [
    'Name',
  ];

  /**
   * {@inheritdoc}
   */
  public function isReservedWord($key): bool {
    if (empty($key)) {
      return FALSE;
    }

    if (in_array($key, self::RESERVED_KEYS)) {
      return TRUE;
    }

    // Reserve for special tags.
    if (strpos($key, 'aws:') === 0) {
      return TRUE;
    }

    // Reserve for aws_cloud_* tags.
    if (preg_match('/^aws_cloud_[a-z_]+$/', $key)) {
      return TRUE;
    }

    // Reserve for cloud_server_template_* tags.
    if (preg_match('/^cloud_server_template_[a-z_]+$/', $key)) {
      return TRUE;
    }

    // Reserve for launch_template_* tags.
    if (preg_match('/^launch_template_[a-z_]+$/', $key)) {
      return TRUE;
    }

    // Reserve for cloud_server_template_*__context__uuid tags.
    if (preg_match('/^cloud_server_template_+[a-z_]+__+[a-z0-9_]+__+[a-z0-9-]+$/', $key)) {
      return TRUE;
    }

    // Reserve for aws_cloud_*__context__uuid tags.
    if (preg_match('/^drupal-aws_cloud.+[a-z_]+.+[a-z0-9_]+.+[a-z0-9-]+$/', $key)) {
      return TRUE;
    }

    // Reserve for openstack_*__uuid__context tags.
    if (preg_match('/^drupal-openstack.+[a-z_]+.+[a-z0-9_]+.+[a-z0-9-]+$/', $key)) {
      return TRUE;
    }
    return FALSE;
  }

}
