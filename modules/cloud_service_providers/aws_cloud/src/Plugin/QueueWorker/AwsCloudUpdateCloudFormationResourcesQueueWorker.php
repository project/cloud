<?php

namespace Drupal\aws_cloud\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceInterface;
use Drupal\cloud\Plugin\QueueWorker\CloudBaseUpdateResourcesQueueWorker;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes for AWs Cloud Update Resources Queue.
 *
 * @QueueWorker(
 *   id = "aws_cloud_update_cloud_formation_resources_queue",
 *   title = @Translation("AWS Cloud update cloud formation resources queue"),
 *   deriver = "Drupal\cloud\Plugin\Derivative\CloudUpdateQueueDerivative",
 * )
 */
class AwsCloudUpdateCloudFormationResourcesQueueWorker extends CloudBaseUpdateResourcesQueueWorker implements ContainerFactoryPluginInterface {

  /**
   * The Cloud Formation service.
   *
   * @var \Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceInterface
   */
  private $cloudFormationService;

  /**
   * Constructs a new AwsCloudUpdateCloudFormationResourcesQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceInterface $cloud_formation_service
   *   The Cloud formation service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    CloudFormationServiceInterface $cloud_formation_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->cloudFormationService = $cloud_formation_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('aws_cloud.cloud_formation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $cloud_context = $data['cloud_context'];
    $aws_cloud_method_name = $data['aws_cloud_method_name'];
    $params = $data['params'] ?? [];

    try {
      $this->setCloudContext($this->cloudFormationService, $cloud_context, $aws_cloud_method_name);
      $this->checkServiceMethod($this->cloudFormationService, $aws_cloud_method_name);
      $this->cloudFormationService->$aws_cloud_method_name($params);
    }
    catch (\Exception $e) {
      // Catch all exceptions to bump it out of the queue.
      $this->handleException($e);
    }
  }

}
