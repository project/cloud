<?php

namespace Drupal\aws_cloud\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\aws_cloud\Access\AwsCloudAccessInterface;
use Drupal\cloud\Plugin\QueueWorker\CloudBaseUpdateResourcesQueueWorker;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes for Aws Cloud Update Resources Queue.
 *
 * @QueueWorker(
 *   id = "aws_cloud_update_iam_permissions_queue",
 *   title = @Translation("AWS Cloud update resources queue"),
 *   deriver = "Drupal\cloud\Plugin\Derivative\CloudUpdateQueueDerivative",
 * )
 */
class AwsCloudUpdateIamPermissionsQueueWorker extends CloudBaseUpdateResourcesQueueWorker implements ContainerFactoryPluginInterface {

  /**
   * The Aws cloud access service.
   *
   * @var \Drupal\aws_cloud\Access\AwsCloudAccessInterface
   */
  private $awsCloudAccess;

  /**
   * Constructs a new AwsCloudUpdateIamPermissionsQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\aws_cloud\Access\AwsCloudAccessInterface $aws_cloud_access
   *   The EC2 service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    AwsCloudAccessInterface $aws_cloud_access,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->awsCloudAccess = $aws_cloud_access;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('aws_cloud.access')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $aws_cloud_method_name = $data['aws_cloud_method_name'];

    try {
      $this->awsCloudAccess->$aws_cloud_method_name($data['cloud_context']);
    }
    catch (\Exception $e) {
      // Catch all exceptions to bump it out of the queue.
      $this->handleException($e);
    }
  }

}
