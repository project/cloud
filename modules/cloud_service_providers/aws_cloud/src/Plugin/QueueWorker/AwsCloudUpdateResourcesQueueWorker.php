<?php

namespace Drupal\aws_cloud\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\QueueWorker\CloudBaseUpdateResourcesQueueWorker;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes for AWs Cloud Update Resources Queue.
 *
 * @QueueWorker(
 *   id = "aws_cloud_update_resources_queue",
 *   title = @Translation("AWS Cloud update resources queue"),
 *   deriver = "Drupal\cloud\Plugin\Derivative\CloudUpdateQueueDerivative",
 * )
 */
class AwsCloudUpdateResourcesQueueWorker extends CloudBaseUpdateResourcesQueueWorker implements ContainerFactoryPluginInterface {

  /**
   * The EC2 service.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  private $ec2Service;

  /**
   * Constructs a new LocaleTranslation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The EC2 service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    Ec2ServiceInterface $ec2_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->ec2Service = $ec2_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('aws_cloud.ec2')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $cloud_context = $data['cloud_context'];
    $aws_cloud_method_name = $data['aws_cloud_method_name'];
    $params = $data['params'] ?? [];

    try {
      $this->setCloudContext($this->ec2Service, $cloud_context, $aws_cloud_method_name);
      $this->checkServiceMethod($this->ec2Service, $aws_cloud_method_name);
      $this->ec2Service->$aws_cloud_method_name($params);
    }
    catch (\Exception $e) {
      // Catch all exceptions to bump it out of the queue.
      $this->handleException($e);
    }
  }

}
