<?php

namespace Drupal\aws_cloud\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Perform AWS specific validations.
 */
class AWSConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new constraint validator.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager instance.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint): void {
    // Only perform validations for aws_cloud bundles.
    if ($entity->bundle() === 'aws_cloud') {
      $this->validateName($entity, $constraint);
      $this->validateInstanceType($entity, $constraint);
    }
  }

  /**
   * Validate entity name for AWS launch template.
   *
   * @param mixed $entity
   *   Entity for validation.
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   Constraint object.
   */
  private function validateName(mixed $entity, Constraint $constraint): void {
    $name = $entity->getName();
    if (empty($name)) {
      return;
    }
    // Validate name's characters.
    if (!$this->validateNameChars($name)) {
      $this->context
        ->buildViolation($constraint->nameInvalidChars, ['%name' => $name])
        ->atPath('name')
        ->addViolation();
    }

    // Validate name is not used.
    if (!$this->validateNameUnused($entity)) {
      $this->context
        ->buildViolation($constraint->nameUsed, ['%name' => $name])
        ->atPath('name')
        ->addViolation();
    }
  }

  /**
   * Validate instance_type for AWS launch template.
   *
   * @param mixed $entity
   *   Entity for validation.
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   Constraint object.
   */
  private function validateInstanceType(mixed $entity, Constraint $constraint): void {
    $instance_type = $entity->field_instance_type->value;
    $field_network = $entity->field_network->entity;
    $image = $entity->field_image_id->entity;
    $shutdown = $entity->field_instance_shutdown_behavior->value;
    // Make sure a network is specified when launching a t2.* instance.
    if (strpos($instance_type, 't2.') !== FALSE && !isset($field_network)) {
      $this->context->buildViolation($constraint->noNetwork, ['%instance_type' => $instance_type])->atPath('field_network')->addViolation();
    }
    // If the image is an instance-store, it cannot use stop for shutdown.
    if ($image !== NULL && $image->root_device_type->value === 'instance-store' && isset($shutdown) && $shutdown === 'stop') {
      $this->context->buildViolation($constraint->shutdownBehavior)->atPath('field_instance_shutdown_behavior')->addViolation();
    }
  }

  /**
   * Validate whether characters of the name are valid or not.
   *
   * A launch template name must be between 3 and 125 characters,
   * and may contain letters, numbers, and the following characters:
   * - ( ) . / _.
   *
   * @param string $name
   *   The name.
   *
   * @return bool
   *   Whether characters are valid or not.
   */
  private function validateNameChars(string $name): bool {
    if (strlen($name) < 3 || strlen($name) > 125) {
      return FALSE;
    }

    return preg_match('/^[a-zA-Z0-9\-().\/_]+$/', $name) === 1;
  }

  /**
   * Validate whether the name of the entity is unused or not.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The cloud launch template entity.
   *
   * @return bool
   *   Whether the name of the entity is unused or not.
   */
  private function validateNameUnused(CloudLaunchTemplateInterface $entity): bool {
    $name = $entity->getName();
    $storage = $this->entityTypeManager->getStorage('cloud_launch_template');

    if ($entity->isNew()) {
      $entity_ids = $storage
        ->getQuery()
        ->accessCheck(TRUE)
        ->condition('type', 'aws_cloud')
        ->condition('cloud_context', $entity->getCloudContext())
        ->condition('name', $name)
        ->execute();

      return empty($entity_ids);
    }

    return TRUE;
  }

}
