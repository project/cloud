<?php

namespace Drupal\aws_cloud\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Security group entity validation.
 *
 * @Constraint(
 *   id = "SecurityGroupName",
 *   label = @Translation("Security group", context = "Validation"),
 * )
 */
class SecurityGroupNameConstraint extends Constraint {

  /**
   * A message: 'Cannot create group with security group name "default".'.
   *
   * @var string
   */
  public $defaultName = 'Cannot create group with security group name "default".';

  /**
   * A message: "The group name already exists".
   *
   * @var string
   */
  public $duplicateGroupName = 'The security group name "@name" already exists.';

}
