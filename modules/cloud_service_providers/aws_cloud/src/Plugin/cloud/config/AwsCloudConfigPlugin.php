<?php

namespace Drupal\aws_cloud\Plugin\cloud\config;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Access\AwsCloudAccessInterface;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * AWS Cloud's cloud service provider (CloudConfig) plugin.
 */
class AwsCloudConfigPlugin extends CloudPluginBase implements CloudConfigPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * File system object.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * AWS Cloud access controller.
   *
   * @var Drupal\aws_cloud\Access\AwsCloudAccessInterface
   */
  protected $awsCloudAccess;

  /**
   * AwsCloudConfigPlugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity type manager.
   * @param \Drupal\Core\File\FileSystem $fileSystem
   *   The FileSystem object.
   * @param \Drupal\aws_cloud\Access\AwsCloudAccessInterface $aws_cloud_access
   *   The AwsCloudAccess object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    FileSystem $fileSystem,
    AwsCloudAccessInterface $aws_cloud_access,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->awsCloudAccess = $aws_cloud_access;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('aws_cloud.access')
    );
  }

  /**
   * Load all entities for a given entity type and bundle.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of Entity Interface.
   */
  public function loadConfigEntities(): array {
    return $this->entityTypeManager
      ->getStorage($this->pluginDefinition['entity_type'])
      ->loadByProperties([
        'type' => [$this->pluginDefinition['entity_bundle']],
      ]);
  }

  /**
   * Load an array of credentials.
   *
   * @param string $cloud_context
   *   Cloud Context string.
   *
   * @return array
   *   Array of credentials.
   */
  public function loadCredentials($cloud_context): array {
    /** @var \Drupal\cloud\Entity\CloudConfig $entity */
    $entity = $this->loadConfigEntity($cloud_context);
    $credentials = [];

    if (empty($entity)) {
      return $credentials;
    }

    // Check if using an instance profile or credentials.
    $credentials['use_instance_profile'] = $entity->get('field_use_instance_profile')->value ?? FALSE;
    $credentials['use_assume_role'] = $entity->get('field_use_assume_role')->value ?? FALSE;
    $credentials['use_switch_role'] = $entity->get('field_use_switch_role')->value ?? FALSE;
    $credentials['role_arn'] = '';
    $credentials['switch_role_arn'] = '';

    // Setup assume role configurations.
    if (!empty($credentials['use_assume_role'])) {
      $credentials['role_arn'] = sprintf('arn:aws:iam::%s:role/%s',
        trim($entity->get('field_account_id')->value ?: ''),
        trim($entity->get('field_iam_role')->value) ?: '');

      // Setup switch role configurations.
      if (!empty($credentials['use_switch_role'])) {
        $credentials['switch_role_arn'] = sprintf('arn:aws:iam::%s:role/%s',
          trim($entity->get('field_switch_role_account_id')->value ?: ''),
          trim($entity->get('field_switch_role_iam_role')->value ?: ''));
      }
    }

    $credentials['ini_file'] = $this->fileSystem->realpath(aws_cloud_ini_file_path($entity->get('cloud_context')->value));
    $credentials['region'] = $entity->get('field_region')->value;
    $credentials['version'] = $entity->get('field_api_version')->value;
    $credentials['endpoint'] = $entity->get('field_api_endpoint_uri')->value;

    return $credentials;
  }

  /**
   * Load a cloud service provider (CloudConfig) entity.
   *
   * @param string $cloud_context
   *   Cloud Context string.
   *
   * @return null|mixed
   *   Entity or NULL if there is no entity.
   */
  public function loadConfigEntity(string $cloud_context): ?CloudConfigInterface {
    $entity = $this->entityTypeManager
      ->getStorage($this->pluginDefinition['entity_type'])
      ->loadByProperties([
        'cloud_context' => [$cloud_context],
      ]);

    if (count(!empty($entity) ? $entity : []) > 0) {
      return array_shift($entity);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceCollectionTemplateName(): string {
    return 'view.aws_cloud_instance.list';
  }

  /**
   * {@inheritdoc}
   */
  public function getPricingPageRoute(): string {
    return 'aws_cloud.instance_type_prices';
  }

  /**
   * {@inheritdoc}
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface {
    return $this->awsCloudAccess->access($cloud_context, $account, $route);
  }

}
