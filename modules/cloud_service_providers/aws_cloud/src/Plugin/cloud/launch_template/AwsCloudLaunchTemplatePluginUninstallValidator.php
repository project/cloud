<?php

namespace Drupal\aws_cloud\Plugin\cloud\launch_template;

use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginUninstallValidator;

/**
 * Validates module uninstall readiness based on existing content entities.
 */
class AwsCloudLaunchTemplatePluginUninstallValidator extends CloudLaunchTemplatePluginUninstallValidator {

}
