<?php

namespace Drupal\aws_cloud\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a carrier gateway operations bulk form element.
 *
 * @ViewsField("carrier_gateway_bulk_form")
 */
class CarrierGatewayBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No carrier gateway selected.');
  }

}
