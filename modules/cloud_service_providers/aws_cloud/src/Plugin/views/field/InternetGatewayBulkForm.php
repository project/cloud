<?php

namespace Drupal\aws_cloud\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines an internet gateway operations bulk form element.
 *
 * @ViewsField("internet_gateway_bulk_form")
 */
class InternetGatewayBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No internet gateway selected.');
  }

}
