<?php

namespace Drupal\aws_cloud\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a VPC peering connection operations bulk form element.
 *
 * @ViewsField("vpc_peering_connection_bulk_form")
 */
class VpcPeeringConnectionBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No VPC peering connection selected.');
  }

}
