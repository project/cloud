<?php

namespace Drupal\aws_cloud\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class AwsCloudRouteSubscriber extends RouteSubscriberBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new AwsCloudRouteSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if ($route = $collection->get('aws_cloud.instance_type_prices')) {
      $config = $this->configFactory->get('aws_cloud.settings');

      // If the configuration is false, the page cannot be accessed.
      if ($config->get('aws_cloud_instance_type_prices') === FALSE) {
        $route->setRequirement('_access', 'FALSE');
      }
    }
  }

}
