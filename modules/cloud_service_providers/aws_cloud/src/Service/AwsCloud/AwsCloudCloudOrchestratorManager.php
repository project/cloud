<?php

namespace Drupal\aws_cloud\Service\AwsCloud;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceInterface;
use Drupal\cloud\Service\CloudOrchestratorManagerInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * AWS Cloud's cloud orchestrator manager.
 */
class AwsCloudCloudOrchestratorManager implements CloudOrchestratorManagerInterface {

  use CloudContentEntityTrait;

  /**
   * The CloudFormation Service.
   *
   * @var \Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceInterface
   */
  protected $cloudFormationService;

  /**
   * Entity field manager interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The AwsCloudCloudOrchestratorManager constructor.
   *
   * @param \Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceInterface $cloud_formation_service
   *   The CloudFormation Service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    CloudFormationServiceInterface $cloud_formation_service,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
  ) {

    $this->cloudFormationService = $cloud_formation_service;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;

    $this->messenger();
  }

  /**
   * {@inheritdoc}
   */
  public function deploy($cloud_context, $manifest, $parameters): array {
    $this->cloudFormationService->setCloudContext($cloud_context);

    $stack_name = $parameters['StackName'];
    unset($parameters['StackName']);

    $cloud_formation_params = [];
    foreach ($parameters as $key => $value) {
      $cloud_formation_params[] = [
        'ParameterKey' => $key,
        'ParameterValue' => $value,
      ];
    }

    $result = $this->cloudFormationService->createStack([
      'StackName' => $stack_name,
      'Capabilities' => [
        'CAPABILITY_NAMED_IAM',
        'CAPABILITY_AUTO_EXPAND',
      ],
      'TemplateBody' => $manifest,
      'Parameters' => $cloud_formation_params,
    ]);

    if (empty($result)) {
      throw new \Exception('Failed to create stack.');
    }

    // Update stack entities.
    $this->cloudFormationService->updateStacksWithoutBatch(['StackName' => $stack_name], FALSE);

    $stacks = $this->entityTypeManager
      ->getStorage('aws_cloud_stack')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
        'stack_name' => $stack_name,
      ]);
    if (empty($stacks)) {
      return [];
    }

    $this->processOperationStatus(reset($stacks), 'created');

    return [
      [
        'entity_type_id' => 'aws_cloud_stack',
        'entity_id' => array_key_first($stacks),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function undeploy(array $entities): void {
    foreach ($entities as $entity) {
      $this->cloudFormationService->setCloudContext($entity->getCloudContext());

      // Delete cloud resource.
      $this->cloudFormationService->deleteStack([
        'StackName' => $entity->getStackName(),
      ]);

      $entity->delete();

      $this->processOperationStatus($entity, 'deleted');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoint(array $entities): string {
    foreach ($entities as $entity) {
      if ($entity->getEntityTypeId() !== 'aws_cloud_stack') {
        continue;
      }

      $outputs = $entity->getOutputs();
      foreach ($outputs ?: [] as $output) {
        if ($output['item_key'] === 'DrupalUrl') {
          return $output['item_value'];
        }
      }
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function validateParameters(array $form, FormStateInterface $form_state, array $parameters): void {
  }

}
