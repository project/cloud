<?php

namespace Drupal\aws_cloud\Service\AwsCloud;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityDeleteFormTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface;
use Drupal\aws_cloud\Entity\Ec2\ImageInterface;
use Drupal\aws_cloud\Entity\Ec2\InstanceInterface;
use Drupal\aws_cloud\Entity\Ec2\KeyPairInterface;
use Drupal\aws_cloud\Entity\Ec2\NetworkInterface;
use Drupal\aws_cloud\Entity\Ec2\NetworkInterfaceInterface;
use Drupal\aws_cloud\Entity\Ec2\SecurityGroup;
use Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface;
use Drupal\aws_cloud\Entity\Ec2\SnapshotInterface;
use Drupal\aws_cloud\Entity\Ec2\VolumeInterface;
use Drupal\aws_cloud\Entity\Vpc\CarrierGatewayInterface;
use Drupal\aws_cloud\Entity\Vpc\InternetGatewayInterface;
use Drupal\aws_cloud\Entity\Vpc\SubnetInterface;
use Drupal\aws_cloud\Entity\Vpc\TransitGatewayInterface;
use Drupal\aws_cloud\Entity\Vpc\VpcInterface;
use Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnectionInterface;
use Drupal\aws_cloud\Form\Ec2\InstanceEditForm;
use Drupal\aws_cloud\Plugin\Field\FieldType\IpPermission;
use Drupal\aws_cloud\Service\AwsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Traits\AwsCloudEntityCheckTrait;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Event\CloudEntityEvent;
use Drupal\cloud\Event\CloudEntityEventType;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\openstack\Service\Rest\OpenStackService as OpenStackRestService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interacts with AWS Cloud using AWS Cloud Service API.
 */
class AwsCloudOperationsService implements AwsCloudOperationsServiceInterface {

  use AwsCloudEntityCheckTrait;
  use CloudContentEntityTrait;
  use EntityDeleteFormTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;


  /**
   * The cloud launch template (CloudLaunchTemplate) storage.
   *
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  protected $cloudLaunchTemplateStorage;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface|\Drupal\cloud\Service\CloudResourceTagInterface
   */
  private $cloudService;

  /**
   * The AWS Cloud EC2 service.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  private $ec2Service;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  private $entity;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  private $entityLinkRenderer;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * AwsCloudOperationsService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud EC2 service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    CloudServiceInterface $cloud_service,
    Ec2ServiceInterface $ec2_service,
    EntityLinkRendererInterface $entity_link_renderer,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    ModuleHandlerInterface $module_handler,
    TimeInterface $time,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cloudLaunchTemplateStorage = $entity_type_manager->getStorage('cloud_launch_template');
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->cloudService = $cloud_service;
    $this->ec2Service = $ec2_service;
    $this->entityLinkRenderer = $entity_link_renderer;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;
    $this->time = $time;

    $this->messenger = $this->messenger();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('cloud'),
      $container->get('aws_cloud.ec2'),
      $container->get('entity.link_renderer'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('module_handler'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setEc2Service(mixed $ec2_service): void {
    $this->ec2Service = $ec2_service;
  }

  /**
   * Gets the entity of this form.
   *
   * Provided by \Drupal\Core\Entity\EntityForm.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function copyFormItemValues(FieldableEntityInterface $entity, array $form): void {
    /** @var \Drupal\Core\Entity\FieldableEntityInterface */
    $original_entity = $this->entityTypeManager
      ->getStorage($entity->getEntityTypeId())
      ->load($entity->id());

    $item_field_names = [];
    foreach ($form ?: [] as $name => $item) {
      if (!is_array($item)) {
        continue;
      }

      if (isset($item['#type'])
        && $item['#type'] === 'item'
        && (!isset($item['#not_field']) || $item['#not_field'] === FALSE)
      ) {
        $item_field_names[] = $name;
      }

      if (isset($item['#type']) && $item['#type'] === 'details') {
        foreach ($item ?: [] as $sub_item_name => $sub_item) {
          if (is_array($sub_item)
            && isset($sub_item['#type'])
            && $sub_item['#type'] === 'item'
            && (!isset($sub_item['#not_field']) || $sub_item['#not_field'] === FALSE)
          ) {
            $item_field_names[] = $sub_item_name;
          }
        }
      }
    }

    foreach ($item_field_names ?: [] as $item_field_name) {
      // Support multi-valued item fields.
      $values = !empty($original_entity) ? $original_entity->get($item_field_name)->getValue() : [];
      if (!empty($values) && count($values) > 1) {
        $item_field_values = array_map(static function ($value) {
          return $value['value'];
        }, $values);
        $entity->set($item_field_name, $item_field_values);
      }

      $item_field_value = !empty($original_entity) && !empty($original_entity->get($item_field_name))
        ? $original_entity->get($item_field_name)->value
        : [];
      if (!empty($item_field_value)) {
        $entity->set($item_field_name, $item_field_value);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function trimTextfields(FieldableEntityInterface $entity, array $form, FormStateInterface $form_state): void {
    $field_names = [];
    foreach ($form ?: [] as $name => $item) {
      if (!is_array($item)) {
        continue;
      }

      if (isset($item['#type'])
        && $item['#type'] === 'textfield'
      ) {
        $field_names[] = $name;
      }

      if (isset($item['#type']) && $item['#type'] === 'details') {
        foreach ($item ?: [] as $sub_item_name => $sub_item) {
          if (is_array($sub_item)
            && isset($sub_item['#type'])
            && $sub_item['#type'] === 'textfield'
          ) {
            $field_names[] = $sub_item_name;
          }
        }
      }
    }

    foreach ($field_names ?: [] as $field_name) {
      $value = $form_state->getValue($field_name);
      if ($value === NULL) {
        continue;
      }

      $form_state->setValue($field_name, trim($value ?: ''));
      $entity->set($field_name, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function saveAwsCloudContent(FieldableEntityInterface $entity, array $form, FormStateInterface $form_state): void {
    // Copy values of the form elements whose type are item to entity.
    // If not, the properties corresponding to the form elements
    // will be saved as NULL.
    $this->copyFormItemValues($entity, $form);

    $this->trimTextfields($entity, $form, $form_state);

    $this->saveCloudContent($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function setTagsInAws(EntityInterface $entity, $resource_id, array $tag_map, $entity_type = ''): void {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity */
    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $tags = [];
    foreach ($tag_map ?: [] as $key => $value) {
      // Do not try to set tags that start with `aws:`.  Those
      // are reserved tags that are non-editable.
      // See the following for more information:
      // https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Tags.html
      if (strpos($key, 'aws:') === 0) {
        continue;
      }
      $tags[] = [
        'Key' => $key,
        'Value' => $value,
      ];
    }

    if (!method_exists($this->ec2Service, 'createTags')) {
      return;
    }

    // Create Tags with different parameters for AWS and OpenStack.
    if (preg_match('[^aws_cloud]', $entity->getEntityTypeId()) === 1) {
      $this->ec2Service->createTags([
        'Resources' => [$resource_id],
        'Tags' => $tags,
      ]);
    }
    else {
      $this->ec2Service->createTags([
        'Resources' => [$resource_id],
        'Tags' => $tags,
        'EntityType' => $entity_type,
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateNameAndCreatedByTags(EntityInterface $entity, $aws_cloud_resource_id): void {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity */
    $cloud_context = $entity->getCloudContext();
    if ($this->isCloudConfigRemote($entity)) {
      // Get original cloud context.
      if (($pos = strrpos($cloud_context, '_')) !== FALSE) {
        $cloud_context = substr($cloud_context, 0, $pos);
      }
    }

    $uid = !empty($entity->getOwner())
      ? $entity->getOwner()->id()
      : 0;
    /** @var \Drupal\openstack\Service\Ec2\OpenStackService $ec2_service */
    $ec2_service = $this->ec2Service;
    $uid_key_name = str_contains($entity->bundle(), 'aws_cloud')
      ? $this->cloudService->getTagKeyCreatedByUid('aws_cloud', $cloud_context)
      : $ec2_service->getTagKeyCreatedByUid(
        'openstack',
        $cloud_context,
        $entity->getOwner()->id()
      );
    $this->setTagsInAws($entity, $aws_cloud_resource_id, [
      $uid_key_name => $uid,
      'Name' => $entity->getName(),
    ],
    $entity->getEntityTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public function validateAuthorize(SecurityGroupInterface $group): void {
    /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $updated_group */
    $updated_group = $this->entityTypeManager
      ->getStorage($group->getEntityTypeId())
      ->load($group->id());

    if ($group->getIpPermission()->count() !== $updated_group->getIpPermission()->count()) {
      $this->messenger->addError(
        $this->t('Error updating inbound permissions for security group @name', [
          '@name' => $group->label(),
        ])
      );
    }

    if (!empty($group->getVpcId())) {
      if ($group->getOutboundPermission()->count() !== $updated_group->getOutboundPermission()->count()) {
        $this->messenger->addError(
          $this->t('Error updating outbound permissions for security group @name', [
            '@name' => $group->label(),
          ])
        );
      }
    }

    if (empty($this->messenger->messagesByType('error'))) {
      // No errors, success.
      $this->processOperationStatus($group, $this->ec2Service->isWorkerResource() ? 'updated remotely' : 'updated');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formatIpPermissions(FieldItemList $permissions): array {
    $ip_permissions = [];
    foreach ($permissions ?: [] as $permission) {
      if (empty($permission->getSource())) {
        continue;
      }
      $ip_permissions['IpPermissions'][] = $this->formatIpPermissionForAuthorize($permission);
    }
    return $ip_permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function updateIngressEgressPermissions(SecurityGroupInterface $original_entity, SecurityGroup $entity, SecurityGroup $existing_group, bool $revokeIngress = TRUE, bool $revokeEgress = TRUE): void {
    $inbound_result = $this->updatePermissions(
      $original_entity,
      $entity->getGroupId(),
      $existing_group->getIpPermission(),
      'IpPermissions',
      'getIpPermission',
      'revokeSecurityGroupIngress',
      'authorizeSecurityGroupIngress',
      $revokeIngress
    );

    // Update the outbound permissions.  This only applies to
    // VPC security groups.
    $outbound_result = TRUE;
    if (!empty($entity->getVpcId())) {
      $outbound_result = $this->updatePermissions(
        $original_entity,
        $entity->getGroupId(),
        $existing_group->getOutboundPermission(),
        'IpPermissions',
        'getOutboundPermission',
        'revokeSecurityGroupEgress',
        'authorizeSecurityGroupEgress',
        $revokeEgress
      );
    }

    if (!$inbound_result && !$outbound_result) {
      $this->messenger->addError($this->t('Inbound and outbound rules could not be saved because of the error from AWS.'));
    }
    elseif (!$inbound_result) {
      $this->messenger->addError($this->t('Inbound rules could not be saved because of the error from AWS.'));
    }
    elseif (!$outbound_result) {
      $this->messenger->addError($this->t('Outbound rules could not be saved because of the error from AWS.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPermission(string $type, int $position, SecurityGroupInterface $entity) {
    $permission = FALSE;
    switch ($type) {
      case 'outbound_permission':
        $permission = $entity->getOutboundPermission()->get($position);
        break;

      case 'ip_permission':
        $permission = $entity->getIpPermission()->get($position);
        break;

      default:
        break;
    }
    return $permission;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceById($instance_id, $module_name, $cloud_context): ?InstanceInterface {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface[] $instances */
    $instances = $this->entityTypeManager
      ->getStorage("{$module_name}_instance")
      ->loadByProperties([
        'instance_id' => $instance_id,
        'cloud_context' => $cloud_context,
      ]);
    return array_shift($instances);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkInterfaceById($network_interface_id, $module_name, $cloud_context): NetworkInterface {
    /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterface[] $network_interfaces */
    $network_interfaces = $this->entityTypeManager
      ->getStorage("{$module_name}_network_interface")
      ->loadByProperties([
        'network_interface_id' => $network_interface_id,
        'cloud_context' => $cloud_context,
      ]);
    return array_shift($network_interfaces);
  }

  /**
   * {@inheritdoc}
   */
  public function getPrivateIps(ElasticIpInterface $entity, $instance_id, $cloud_context): array {
    // Get module name.
    $module_name = $this->getModuleName($entity);

    $instances = $this->entityTypeManager
      ->getStorage("{$module_name}_instance")
      ->loadByProperties([
        'id' => $instance_id,
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $instance */
    $instance = count($instances) === 1
      ? array_shift($instances)
      : $instances;

    $ips = !empty($instance) ? explode(', ', $instance->getPrivateIps()) : [];

    $private_ips = [];
    foreach ($ips ?: [] as $ip) {

      // Check if the IP is in the Elastic IP table.
      $result = $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->loadByProperties([
          'private_ip_address' => $ip,
          'cloud_context' => $cloud_context,
        ]);

      if (count($result) === 0) {
        $private_ips[$ip] = $ip;
      }
    }

    return $private_ips;
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkPrivateIps(ElasticIpInterface $entity, $network_interface_id): array {
    // Get module name.
    $module_name = $this->getModuleName($entity);

    $network_interfaces = $this->entityTypeManager
      ->getStorage("{$module_name}_network_interface")
      ->loadByProperties([
        'id' => $network_interface_id,
        'cloud_context' => $entity->getCloudContext(),
      ]);

    /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterface $network_interface */
    $network_interface = count($network_interfaces) === 1
      ? array_shift($network_interfaces)
      : $network_interfaces;

    $association_id = $network_interface->getAssociationId();
    $secondary_association_id = $network_interface->getSecondaryAssociationId();

    $ips = [];
    if (empty($association_id)) {
      $ips[$network_interface->getPrimaryPrivateIp()] = $network_interface->getPrimaryPrivateIp();
    }

    if (empty($secondary_association_id) && !empty($network_interface->getSecondaryPrivateIps())) {
      $ips[$network_interface->getSecondaryPrivateIps()] = $network_interface->getSecondaryPrivateIps();
    }

    return $ips;
  }

  /**
   * {@inheritdoc}
   */
  public function associateElasticIp(ElasticIpInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->ec2Service->setCloudContext($entity->getCloudContext());

      // Get module name.
      $module_name = $this->getModuleName($entity);

      $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.canonical", [
        'cloud_context' => $entity->getCloudContext(),
        "{$entity->getEntityTypeId()}" => $entity->id(),
      ]);

      // If an IP address is attaching to instance.
      if ($form_state->getValue('resource_type') === 'instance') {

        $instance_entity_id = $form_state->getValue('instance_id');
        $private_ip = $form_state->getValue('instance_private_ip');

        if ($instance_entity_id !== -1) {

          $instances = $this->entityTypeManager
            ->getStorage("{$module_name}_instance")
            ->loadByProperties([
              'id' => $instance_entity_id,
              'cloud_context' => $entity->getCloudContext(),
            ]);

          if (empty($instances)) {
            $this->messenger->addError(
              $this->t('Unable to load the instance %instance_entity_id.', [
                '%instance_entity_id' => $instance_entity_id,
              ]
            ));

            $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.canonical", [
              'cloud_context' => $entity->getCloudContext(),
              "{$entity->getEntityTypeId()}" => $entity->id(),
            ]);
            return FALSE;
          }

          /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $instance */
          $instance = array_shift($instances);
          $instance_id = $instance->getInstanceId();

          $instance_link = $this->entityLinkRenderer->renderViewElement(
            $instance_id,
            "{$module_name}_instance",
            'instance_id',
            [],
            $instance->getName() !== $instance->getInstanceId()
              ? $this->t('@instance_name (@instance_id)', [
                '@instance_name' => $instance->getName(),
                '@instance_id' => $instance_id,
              ])
              : $instance_id
          );

          // Refresh network interfaces.
          $this->ec2Service->updateNetworkInterfaces();

          $network_interface = $this->getNetworkInterfaceByPrivateIp($private_ip, $module_name, $entity->getCloudContext());
          if (!empty($network_interface)) {
            $result = $this->ec2Service->associateAddress([
              'AllocationId' => $entity->getAllocationId(),
              'NetworkInterfaceId' => $network_interface->getNetworkInterfaceId(),
              'PrivateIpAddress' => $private_ip,
            ]);

            if ($result === NULL) {
              $this->messenger->addError($this->t('Unable to associate @label.', [
                '@label' => $entity->getEntityType()->getSingularLabel(),
              ]));
              return FALSE;
            }

            if (!empty($result['SendToWorker'])) {
              $message = $this->t("@label @ip_address associated with @private_ip for instance: @instance_id remotely", [
                '@ip_address' => $entity->getPublicIp(),
                '@private_ip' => $private_ip,
                '@instance_id' => Markup::create($instance_link['#markup']),
                '@label' => $entity->getEntityType()->getSingularLabel(),
              ]);
              $this->messenger->addStatus($message);
              return TRUE;
            }

            $message = $this->t("@label @ip_address associated with @private_ip for instance: @instance_id", [
              '@ip_address' => $entity->getPublicIp(),
              '@private_ip' => $private_ip,
              '@instance_id' => Markup::create($instance_link['#markup']),
              '@label' => $entity->getEntityType()->getSingularLabel(),
            ]);

            $this->updateElasticIpEntity($message);
            $this->clearCacheValues($entity->getCacheTags());
            $this->dispatchSubmitEvent($entity);
            return TRUE;
          }
          else {
            $this->messenger->addError($this->t('Unable to load network interface by private IP.'));
            return FALSE;
          }
        }
        else {
          $this->messenger->addError($this->t('Unable to load instance ID. No association performed.'));
          return FALSE;
        }
      }

      // If an IP address is attaching to network_interface.
      $network_interface_id = $form_state->getValue('network_interface_id');
      $network_private_ip = $form_state->getValue('network_private_ip');

      if ($network_interface_id !== -1) {

        $network_interfaces = $this->entityTypeManager
          ->getStorage("{$module_name}_network_interface")
          ->loadByProperties(
            [
              'id' => $network_interface_id,
              'cloud_context' => $entity->getCloudContext(),
            ]
          );

        /** @var \Drupal\aws_cloud\Entity\Ec2\NetworkInterfaceInterface $network_interface */
        $network_interface = count($network_interfaces) === 1
          ? array_shift($network_interfaces)
          : $network_interfaces;

        $result = $this->ec2Service->associateAddress([
          'AllocationId' => $entity->getAllocationId(),
          'NetworkInterfaceId' => $network_interface->getNetworkInterfaceId(),
          'PrivateIpAddress' => $network_private_ip,
        ]);

        if ($result === NULL) {
          $this->messenger->addError($this->t("Unable to associate @label.", [
            '@label' => $entity->getEntityType()->getSingularLabel(),
          ]));
          return FALSE;
        }

        if (!empty($result['SendToWorker'])) {
          $message = $this->t('@label @ip_address associated with @private_ip for network interface: @network_interface_id remotely', [
            '@ip_address' => $entity->getPublicIp(),
            '@network_interface_id' => $network_interface->getNetworkInterfaceId(),
            '@private_ip' => $network_private_ip,
            '@label' => $entity->getEntityType()->getSingularLabel(),
          ]);
          $this->messenger->addStatus($message);
          return TRUE;
        }

        $message = $this->t('@label @ip_address associated with @private_ip for network interface: @network_interface_id', [
          '@ip_address' => $entity->getPublicIp(),
          '@network_interface_id' => $network_interface->getNetworkInterfaceId(),
          '@private_ip' => $network_private_ip,
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]);

        $this->updateElasticIpEntity($message);
        $this->clearCacheValues($entity->getCacheTags());
        $this->dispatchSubmitEvent($entity);
        return TRUE;
      }
      else {
        $this->messenger->addError($this->t('Unable to load instance ID. No association performed.'));
        return FALSE;
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'associated');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function associateElasticIpForInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $allocation_id = $form_state->getValue('allocation_id');
      $network_interface_id = $form_state->getValue('network_interface_id');

      $result = $this->ec2Service->associateAddress([
        'AllocationId' => $allocation_id,
        'NetworkInterfaceId' => $network_interface_id,
      ]);

      if ($result !== NULL) {
        $this->ec2Service->updateElasticIps();
        $this->ec2Service->updateInstances();
        $this->ec2Service->updateNetworkInterfaces();

        $elastic_ip = $this->getElasticIp($allocation_id, $entity->getCloudContext());
        if ($elastic_ip !== FALSE) {
          $this->messenger->addStatus($this->t('Elastic IP @ip_address associated with @private_ip for instance: @instance', [
            '@ip_address' => $elastic_ip->getPublicIp(),
            '@instance' => $entity->getName(),
            '@private_ip' => $elastic_ip->getPrivateIpAddress(),
          ]));
          $this->clearCacheValues($entity->getCacheTags());
        }
        $form_state->setRedirect('entity.aws_cloud_instance.canonical', [
          'cloud_context' => $entity->getCloudContext(),
          'aws_cloud_instance' => $entity->id(),
        ]);
        return TRUE;
      }
      else {
        $this->messenger->addError($this->t('Unable to associate Elastic IP.'));
        $form_state->setRedirect('entity.aws_cloud_instance.canonical', [
          'cloud_context' => $entity->getCloudContext(),
          'aws_cloud_instance' => $entity->id(),
        ]);
        return FALSE;
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'associated');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOpenStackProjectIdByCloudContext(string $cloud_context): string {
    $project_id = '';
    $cloud_configs = $this->cloudConfigPluginManager->loadConfigEntities('openstack');
    foreach ($cloud_configs as $cloud_config) {
      if ($cloud_context === $cloud_config->getCloudContext()) {
        $project_id = $cloud_config->get('field_project_id')->getValue()[0]['value'];
      }
    }
    return $project_id;
  }

  /**
   * {@inheritdoc}
   */
  public function createImage(ImageInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->trimTextfields($entity, $form, $form_state);

      $result = $this->ec2Service->createImage([
        'InstanceId'  => $entity->getInstanceId(),
        'Name'        => $entity->getName(),
        'Description' => $entity->getDescription(),
        'Visibility'  => $entity->getVisibility(),
      ]);

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return TRUE;
      }

      $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
      $account_id = $this->cloudConfigPluginManager->loadConfigEntity()->get('field_account_id')->value;

      if (!empty($entity) && !empty($result['ImageId'])
        && ($entity->setName($form_state->getValue('name')))
        && ($entity->set('ami_name', $this->ec2Service instanceof OpenStackRestService ? '' : $form_state->getValue('name')))
        && ($entity->setImageId($result['ImageId']))
        && ($entity->set('account_id', $account_id))
        && ($entity->save())) {

        $this->updateNameAndCreatedByTags($entity, $entity->getImageId());

        $this->processOperationStatus($entity, 'created');

        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        $this->dispatchSaveEvent($entity);

        return TRUE;
      }
      else {
        $this->processOperationErrorStatus($entity, 'created');

        return FALSE;
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createImageFromInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      if (!$this->updateEntitySubmitForm($form_state, $entity)) {
        return FALSE;
      }

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $result = $this->ec2Service->createImage([
        'InstanceId' => $entity->getInstanceId(),
        'Name' => $form_state->getValue('image_name'),
        'NoReboot' => $form_state->getValue('no_reboot') === 0 ? FALSE : TRUE,
      ]);

      if (empty($entity) || empty($result['ImageId'])) {
        // Give an error.
        $this->processOperationErrorStatus($entity, 'created');
        return FALSE;
      }

      // Call image update on this particular image.
      $this->ec2Service->updateImages([
        'ImageIds' => [
          $result['ImageId'],
        ],
      ]);

      $this->messenger->addStatus($this->t('The @type %label (%image_id) has been created.', [
        '@type' => $entity->getEntityType()->getSingularLabel(),
        '%label' => $entity->toLink($entity->label())->toString(),
        '%image_id' => $result['ImageId'],
      ]));
      $this->clearCacheValues($entity->getCacheTags());
      $image = $this->loadNewImage($entity->getCloudContext(), $result['ImageId']);
      if (!empty($image)) {
        $this->dispatchSubmitEvent($image);
      }
      $form_state->setRedirect('view.aws_cloud_image.list', ['cloud_context' => $entity->getCloudContext()]);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createCarrierGateway(CarrierGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->trimTextfields($entity, $form, $form_state);
      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $form_state->setRedirect(
        'view.aws_cloud_carrier_gateway.list',
        ['cloud_context' => $entity->getCloudContext()]
      );

      $result = $this->ec2Service->createCarrierGateway([
        'VpcId' => $entity->getVpcId(),
      ]);

      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'created');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $entity->setCarrierGatewayId($result['CarrierGateway']['CarrierGatewayId']);
      $entity->save();
      $this->updateNameAndCreatedByTags($entity, $entity->getCarrierGatewayId());

      // Update the carrier gateway.
      $this->ec2Service->updateCarrierGateways([
        'CarrierGatewayIds' => [$entity->getCarrierGatewayId()],
      ], FALSE);

      $this->processOperationStatus($entity, 'created');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createElasticIp(ElasticIpInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->trimTextfields($entity, $form, $form_state);

    $params = [
      'Domain' => $entity->getDomain(),
    ];

    if (!empty($entity->getNetworkBorderGroup())) {
      $params['NetworkBorderGroup'] = $entity->getNetworkBorderGroup();
    }

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->allocateAddress($params);
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $public_ip = $result['PublicIp'] ?? $result['CarrierIp'];
    $elastic_ip_type = !empty($result['PublicIp']) ? 'Public IP' : 'Carrier IP';
    $entity->setPublicIp($public_ip);
    $entity->setAllocationId($result['AllocationId']);
    $entity->setDomain($result['Domain']);
    $entity->setElasticIpType($elastic_ip_type);
    $entity->save();

    if (!empty($result['NetworkBorderGroup'])) {
      $entity->setNetworkBorderGroup($result['NetworkBorderGroup']);
      $entity->save();
    }

    // Update the entity name.
    $this->updateNameAndCreatedByTags($entity, $entity->getAllocationId());

    $this->processOperationStatus($entity, 'created');
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createInternetGateway(InternetGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->trimTextfields($entity, $form, $form_state);
      $this->ec2Service->setCloudContext($entity->getCloudContext());

      $form_state->setRedirect(
        'view.aws_cloud_internet_gateway.list',
        ['cloud_context' => $entity->getCloudContext()]
      );

      $result = $this->ec2Service->createInternetGateway([]);
      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'created');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $entity->setInternetGatewayId($result['InternetGateway']['InternetGatewayId']);
      $entity->save();

      $this->updateNameAndCreatedByTags($entity, $entity->getInternetGatewayId());

      // Update the internet gateway.
      $this->ec2Service->updateInternetGateways([
        'InternetGatewayIds' => [$entity->getInternetGatewayId()],
      ], FALSE);

      $this->processOperationStatus($entity, 'created');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createNetworkInterface(NetworkInterfaceInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->trimTextfields($entity, $form, $form_state);

    $security_group_ids = !empty($form_state->getValue('security_groups'))
      ? array_values($form_state->getValue('security_groups')) : [];

    $security_groups = !empty($security_group_ids)
      ? implode(',', $security_group_ids) : '';
    $entity->setSecurityGroups($security_groups);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->createNetworkInterface([
      'Name' => $entity->getName(),
      'Description' => $entity->getDescription(),
      'SubnetId' => $entity->getSubnetId(),
      'Groups' => $security_group_ids,
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $entity->setNetworkInterfaceId($result['NetworkInterface']['NetworkInterfaceId']);
    $entity->save();

    $this->updateNameAndCreatedByTags($entity, $entity->getNetworkInterfaceId());

    // Update the VPC.
    $this->ec2Service->updateNetworkInterfaces([
      'NetworkInterfaceId' => $entity->getNetworkInterfaceId(),
    ]);

    $this->processOperationStatus($entity, 'created');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createTransitGateway(TransitGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->trimTextfields($entity, $form, $form_state);
      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $params = [];
      if (!empty($entity->getDescription())) {
        $params['Description'] = $entity->getDescription();
      }

      if (!empty($entity->getAmazonSideAsn())) {
        $params['Options']['AmazonSideAsn'] = $entity->getAmazonSideAsn();
      }
      $params['Options']['AutoAcceptSharedAttachments'] = $entity->isAutoAcceptSharedAttachments() === TRUE ? 'enable' : 'disable';
      $params['Options']['DefaultRouteTableAssociation'] = $entity->isDefaultRouteTableAssociation() === TRUE ? 'enable' : 'disable';
      $params['Options']['DefaultRouteTablePropagation'] = $entity->isDefaultRouteTablePropagation() === TRUE ? 'enable' : 'disable';
      $params['Options']['DnsSupport'] = $entity->isDnsSupport() === TRUE ? 'enable' : 'disable';
      $params['Options']['MulticastSupport'] = $entity->isMulticastSupport() === TRUE ? 'enable' : 'disable';
      $params['Options']['VpnEcmpSupport'] = $entity->isVpnEcmpSupport() === TRUE ? 'enable' : 'disable';
      $result = $this->ec2Service->createTransitGateway($params);

      $form_state->setRedirect(
        'view.aws_cloud_transit_gateway.list',
        ['cloud_context' => $entity->getCloudContext()]
      );

      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'created');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $entity->setTransitGatewayId($result['TransitGateway']['TransitGatewayId']);
      $entity->save();

      $this->updateNameAndCreatedByTags($entity, $entity->getTransitGatewayId());

      // Update the transit gateway.
      $this->ec2Service->updateTransitGateways([
        'TransitGatewayIds' => [$entity->getTransitGatewayId()],
      ], FALSE);

      $this->processOperationStatus($entity, 'created');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createVpc(VpcInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->trimTextfields($entity, $form, $form_state);
      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $form_state->setRedirect('view.aws_cloud_vpc.list', ['cloud_context' => $entity->getCloudContext()]);

      $result = $this->ec2Service->createVpc([
        'AmazonProvidedIpv6CidrBlock' => !empty($form_state->getValue('amazon_provided_ipv6_cidr_block')),
        'CidrBlock' => $entity->getCidrBlock(),
        'InstanceTenancy' => $entity->getInstanceTenancy(),
      ]);

      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'created');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $entity->setVpcId($result['Vpc']['VpcId']);
      $entity->save();

      $this->updateNameAndCreatedByTags($entity, $entity->getVpcId());

      // Create flow log.
      if ($entity->getFlowLog()) {
        aws_cloud_create_flow_log($entity->getCloudContext(), $entity->getVpcId());
      }

      // Update the VPC.
      $this->ec2Service->updateVpcs([
        'VpcIds' => [$entity->getVpcId()],
      ], FALSE);

      $this->processOperationStatus($entity, 'created');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editVpc(VpcInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      // Call copyFormItemValues() to ensure the form array is intact.
      $this->copyFormItemValues($entity, $form);

      $this->trimTextfields($entity, $form, $form_state);

      $this->entityTypeManager
        ->getStorage('aws_cloud_vpc')
        ->resetCache([$entity->id()]);

      /** @var \Drupal\aws_cloud\Entity\Vpc\VpcInterface $old_vpc */
      $old_vpc = $this->entityTypeManager
        ->getStorage('aws_cloud_vpc')
        ->load($entity->id());

      $form_state->setRedirect('view.aws_cloud_vpc.list', [
        'cloud_context' => $entity->getCloudContext(),
      ]);

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      if (!empty($old_vpc)) {
        // Delete old tags.
        $this->ec2Service->deleteTags([
          'Resources' => [$entity->getVpcId()],
        ]);

        // Update tags.
        $tag_map = [];
        foreach ($entity->getTags() ?: [] as $tag) {
          // If the tag name contains the prefix
          // which is reserved for AWS, it will not be saved.
          if (!str_contains($tag['item_key'], AwsServiceInterface::AWS_RESOURCE_TAG_PREFIX)) {
            $tag_map[$tag['item_key']] = $tag['item_value'];
          }
          else {
            $this->messenger->addWarning($this->t('The tag @tag_name is preserved as it is because it contains the prefix <em>aws:</em>, which is reserved by AWS.', [
              '@tag_name' => $tag['item_key'],
            ]));
          }
        }
        $this->setTagsInAws($entity, $entity->getVpcId(), $tag_map);

        $this->updateNameAndCreatedByTags($entity, $entity->getVpcId());

        // Update IPv4 CIDRs.
        $original_cidrs = [];
        $original_cidr_association_id_map = [];
        foreach ($old_vpc->getCidrBlocks() ?: [] as $cidr_block) {
          if ($cidr_block) {
            $original_cidrs[] = $cidr_block['cidr'];
            $original_cidr_association_id_map[$cidr_block['cidr']] = $cidr_block['association_id'];
          }
        }

        $new_cidrs = [];
        foreach ($entity->getCidrBlocks() ?: [] as $cidr_block) {
          $new_cidrs[] = $cidr_block['cidr'];
        }

        $cidrs_to_create = array_diff($new_cidrs, $original_cidrs);
        $cidrs_to_delete = array_diff($original_cidrs, $new_cidrs);

        foreach ($cidrs_to_create ?: [] as $cidr) {
          $this->ec2Service->associateVpcCidrBlock([
            'VpcId' => $entity->getVpcId(),
            'CidrBlock' => $cidr,
          ]);
        }

        foreach ($cidrs_to_delete ?: [] as $cidr) {
          $this->ec2Service->disassociateVpcCidrBlock([
            'AssociationId' => $original_cidr_association_id_map[$cidr],
          ]);
        }

        // Update IPv6 CIDRs.
        $original_cidrs = [];
        $original_association_id = NULL;
        foreach ($old_vpc->getIpv6CidrBlocks() ?: [] as $cidr_block) {
          if ($cidr_block) {
            $original_cidrs[] = $cidr_block['cidr'];
            $original_association_id = $cidr_block['association_id'];
          }
        }

        if (empty($original_cidrs)) {
          if (!empty($entity->getIpv6CidrBlocks())) {
            $this->ec2Service->associateVpcCidrBlock([
              'VpcId' => $entity->getVpcId(),
              'AmazonProvidedIpv6CidrBlock' => TRUE,
            ]);
          }
        }
        else {
          if (empty($entity->getIpv6CidrBlocks())) {
            $this->ec2Service->disassociateVpcCidrBlock([
              'AssociationId' => $original_association_id,
            ]);
          }
        }

        // Update the flow log.
        if ($entity->getFlowLog()) {
          // Create flow log.
          aws_cloud_create_flow_log($entity->getCloudContext(), $entity->getVpcId());
        }
        else {
          // Delete flow log.
          aws_cloud_delete_flow_log($entity->getCloudContext(), $entity->getVpcId());
        }

        if ($this->isCloudConfigRemote($entity)) {
          $this->processOperationStatus($entity, 'updated remotely');
          return TRUE;
        }

        $entity->save();

        // Update the VPC.
        $this->ec2Service->updateVpcs([
          'VpcId' => $entity->getVpcId(),
        ], FALSE);

        $this->processOperationStatus($entity, 'updated');
        $this->clearCacheValues($entity->getCacheTags());
        $this->dispatchSaveEvent($entity);
      }
      else {
        $this->messenger->addError($this->t('Unable to update @label.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]));
      }
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createVpcPeeringConnection(VpcPeeringConnectionInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->trimTextfields($entity, $form, $form_state);
      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $params = [];
      $params['VpcId'] = $form_state->getValue('requester_vpc_id');
      $params['PeerVpcId'] = $form_state->getValue('accepter_vpc_id');

      if (!empty($form_state->getValue('accepter_account_id'))) {
        $params['PeerOwnId'] = $form_state->getValue('accepter_account_id');
      }

      if (!empty($form_state->getValue('accepter_region'))) {
        $params['PeerRegion'] = $form_state->getValue('accepter_region');
      }

      $storage = $this->entityTypeManager->getStorage('aws_cloud_vpc_peering_connection');
      /** @var \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnectionInterface[] $entities */
      $entities = $storage->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'requester_vpc_id' => $params['VpcId'],
        'accepter_vpc_id' => $params['PeerVpcId'],
      ]);

      $form_state->setRedirect('view.aws_cloud_vpc_peering_connection.list', ['cloud_context' => $entity->getCloudContext()]);

      if (!empty($entities)) {
        $message = $this->t('The @type %vpc_peering_connection_id already exists.', [
          '@type' => $entity->getEntityType()->getSingularLabel(),
          '%vpc_peering_connection_id' => array_shift($entities)->getVpcPeeringConnectionId(),
        ]);
        $this->messenger->addError($message);
        $this->processOperationErrorStatus($entity, 'created');
        return TRUE;
      }

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $result = $this->ec2Service->createVpcPeeringConnection($params);
      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'created');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $vpc_peering_connection_id = $result['VpcPeeringConnection']['VpcPeeringConnectionId'];

      // Check if the VPC peering connection exists.
      $entities = $storage->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'vpc_peering_connection_id' => $vpc_peering_connection_id,
      ]);

      if (!empty($entities)) {
        $message = $this->t('The @type %vpc_peering_connection_id already exists.', [
          '@type' => $entity->getEntityType()->getSingularLabel(),
          '%vpc_peering_connection_id' => $vpc_peering_connection_id,
        ]);
        $this->messenger->addError($message);

        $this->processOperationErrorStatus($entity, 'created');
        return FALSE;
      }

      $entity->setVpcPeeringConnectionId($result['VpcPeeringConnection']['VpcPeeringConnectionId']);
      $entity->save();

      $this->updateNameAndCreatedByTags($entity, $entity->getVpcPeeringConnectionId());

      // Update the VPC peering connection.
      $this->ec2Service->updateVpcPeeringConnections([
        'VpcPeeringConnectionIds' => [$entity->getVpcPeeringConnectionId()],
      ], FALSE);

      /** @var \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnectionInterface[] $entities */
      $entities = $storage->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'vpc_peering_connection_id' => $vpc_peering_connection_id,
      ]);

      if (count($entities) > 0) {
        $entity = array_shift($entities);
      }

      if ($entity->getStatusCode() === 'failed') {
        $this->processOperationErrorStatus($entity, 'created');
        $message = $this->t("The VPC peering connection @name has failed status. During this state, it cannot be accepted, rejected, or deleted. The failed VPC peering connection remains visible to the requester for 2 hours.", [
          '@name' => $entity->label(),
        ]);
        $this->messenger->addError($message);
        return FALSE;
      }

      $this->processOperationStatus($entity, 'created');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editVpcPeeringConnection(VpcPeeringConnectionInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      // Call copyFormItemValues() to ensure the form array is intact.
      $this->copyFormItemValues($entity, $form);

      $this->trimTextfields($entity, $form, $form_state);

      $old_vpc_peering_connection = $this->entityTypeManager
        ->getStorage('aws_cloud_vpc_peering_connection')
        ->load($entity->id());

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      if (!empty($old_vpc_peering_connection)) {

        // Delete old tags.
        $this->ec2Service->deleteTags([
          'Resources' => [$entity->getVpcPeeringConnectionId()],
        ]);

        // Update tags.
        $tag_map = [];
        foreach ($entity->getTags() as $tag) {
          $tag_map[$tag['item_key']] = $tag['item_value'];
        }
        $this->setTagsInAws($entity, $entity->getVpcPeeringConnectionId(), $tag_map);

        $this->updateNameAndCreatedByTags($entity, $entity->getVpcPeeringConnectionId());

        if ($this->isCloudConfigRemote($entity)) {
          $this->processOperationStatus($entity, 'updated remotely');
          return TRUE;
        }

        $entity->save();

        // Update the VPC peering connection.
        $this->ec2Service->updateVpcPeeringConnections([
          'VpcPeeringConnectionId' => $entity->getVpcPeeringConnectionId(),
        ], FALSE);

        $this->processOperationStatus($entity, 'updated');
        $this->clearCacheValues($entity->getCacheTags());
        $this->dispatchSaveEvent($entity);
      }
      else {
        $this->messenger->addError($this->t('Unable to update @label.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]));
      }

      $form_state->setRedirect('view.aws_cloud_vpc_peering_connection.list', [
        'cloud_context' => $entity->getCloudContext(),
      ]);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createSubnet(SubnetInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->trimTextfields($entity, $form, $form_state);
      $this->ec2Service->setCloudContext($entity->getCloudContext());

      $params = [
        'VpcId' => $entity->getVpcId(),
        'CidrBlock' => $entity->getCidrBlock(),
      ];
      if (!empty($entity->getAvailabilityZone())) {
        $params['AvailabilityZone'] = $entity->getAvailabilityZone();
      }
      $result = $this->ec2Service->createSubnet($params);

      $form_state->setRedirect('view.aws_cloud_subnet.list', ['cloud_context' => $entity->getCloudContext()]);

      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'created');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $entity->setSubnetId($result['Subnet']['SubnetId']);
      $entity->save();
      $this->updateNameAndCreatedByTags($entity, $entity->getSubnetId());

      // Update the subnet.
      $this->ec2Service->updateSubnets([
        'SubnetIds' => [$entity->getSubnetId()],
      ], FALSE);

      $this->processOperationStatus($entity, 'created');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editSubnet(SubnetInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      // Call copyFormItemValues() to ensure the form array is intact.
      $this->copyFormItemValues($entity, $form);

      $this->trimTextfields($entity, $form, $form_state);

      $form_state->setRedirect('view.aws_cloud_subnet.list', [
        'cloud_context' => $entity->getCloudContext(),
      ]);

      $this->ec2Service->setCloudContext($entity->getCloudContext());

      // Delete old tags.
      $this->ec2Service->deleteTags([
        'Resources' => [$entity->getSubnetId()],
      ]);

      // Update tags.
      $tag_map = [];
      foreach ($entity->getTags() ?: [] as $tag) {
        $tag_map[$tag['item_key']] = $tag['item_value'];
      }
      $this->setTagsInAws($entity, $entity->getSubnetId(), $tag_map);

      $this->updateNameAndCreatedByTags($entity, $entity->getSubnetId());

      if ($this->isCloudConfigRemote($entity)) {
        $this->processOperationStatus($entity, 'updated remotely');
        return TRUE;
      }

      $entity->save();

      // Update the VPC.
      $this->ec2Service->updateSubnets([
        'SubnetId' => $entity->getSubnetId(),
      ], FALSE);

      $this->processOperationStatus($entity, 'updated');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createKeyPair(KeyPairInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->trimTextfields($entity, $form, $form_state);

      $result = $this->ec2Service->createKeyPair([
        'KeyName' => $entity->getKeyPairName(),
        'TagSpecifications' => [
          [
            'ResourceType' => 'key-pair',
            'Tags' => [
              [
                'Key' => $this->cloudService->getTagKeyCreatedByUid(
                  'aws_cloud',
                  $entity->getCloudContext()),
                'Value' => !empty($entity->getOwner()) ? $entity->getOwner()->id() : 0,
              ],
            ],
          ],
        ],
      ]);

      if (empty($result)) {
        // Use the custom message since key pair uses 'key_pair_name'
        // for its own label.  So do not change the following code.
        $this->messenger->addError($this->t('The @type @label could not be created.', [
          '@type' => $entity->getEntityType()->getSingularLabel(),
          '@label' => $entity->getKeyPairName(),
        ]));
        $this->logOperationErrorMessage($entity, 'created');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return TRUE;
      }

      $entity->setKeyFingerprint($result['KeyFingerprint']);
      $entity->setKeyPairId($result['KeyPairId']);
      $entity->setKeyMaterial($result['KeyMaterial']);
      $entity->save();
      $this->processOperationStatus($entity, 'created');
      $this->logOperationMessage($entity, 'created');
      $this->clearCacheValues($entity->getCacheTags());

      $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.canonical", [
        'cloud_context' => $entity->getCloudContext(),
        $entity->getEntityTypeId() => $entity->id(),
      ]);
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createSecurityGroup(SecurityGroupInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->trimTextfields($entity, $form, $form_state);

      $result = $this->ec2Service->createSecurityGroup(
        [
          'GroupName'   => $entity->getGroupName(),
          'Description' => $entity->getDescription(),
        ]
        + ($entity->getEntityTypeId() === 'aws_cloud_security_group'
          ? ['VpcId' => $entity->getVpcId()]
          : []
        )
      );

      if (!empty($result['SendToWorker'])) {
        $entity->setName($entity->getGroupName());
        $this->processOperationStatus($entity, 'created remotely');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return TRUE;
      }

      if (isset($result['GroupId'])
      && ($entity->setGroupId($result['GroupId']))
      && ($entity->set('name', $entity->getGroupName()))
      && ($entity->save())) {
        $this->updateNameAndCreatedByTags($entity, $entity->getGroupId());

        // Have the system refresh the security group.
        $this->ec2Service->updateSecurityGroups([
          'GroupIds' => [$entity->getGroupId()],
        ], FALSE);

        $this->processOperationStatus($entity, 'created');
        $this->messenger->addStatus('Set up the IP permissions.');

        $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.canonical", [
          'cloud_context' => $entity->getCloudContext(),
          $entity->getEntityTypeId() => $entity->id(),
        ]);
        $this->clearCacheValues($entity->getCacheTags());
        $this->dispatchSaveEvent($entity);
      }
      else {
        // Use the custom message since security group uses 'group_name' for its
        // own label.  So do not change the following code.
        $this->messenger->addError($this->t('The @type @label could not be created.', [
          '@type'  => $entity->getEntityType()->getSingularLabel(),
          '@label' => $entity->getGroupName(),
        ]));
        $this->logOperationErrorMessage($entity, 'created');
        return FALSE;
      }

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createSnapshot(SnapshotInterface $entity, array $form, FormStateInterface $form_state): bool {
    $this->trimTextfields($entity, $form, $form_state);

    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $result = [];
    if (!empty($entity->getVolumeId())) {
      $result = $this->ec2Service->createSnapshot([
        'Name'    => $entity->getName(),
        'VolumeId'    => $entity->getVolumeId(),
        'Description' => $entity->getDescription(),
      ]);
    }

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    if (empty($result)) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $start_time = is_object($result['StartTime'])
      ? strtotime($result['StartTime']->__toString())
      : strtotime($result['StartTime']);

    if (empty($entity->getName())) {
      $entity->setName($result['SnapshotId']);
    }
    $entity->setSnapshotId($result['SnapshotId']);
    $entity->setStatus($result['State']);
    $entity->setStarted($start_time);
    $entity->setEncrypted($result['Encrypted'] === FALSE ? 'Not Encrypted' : 'Encrypted');
    $entity->save();

    $this->updateNameAndCreatedByTags($entity, $entity->getSnapshotId());
    $this->processOperationStatus($entity, 'created');
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createVolume(VolumeInterface $entity, array $form, FormStateInterface $form_state): bool {

    $this->trimTextfields($entity, $form, $form_state);

    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $params = [
      'Name'             => $entity->getName(),
      'SnapshotId'       => $entity->getSnapshotId(),
      'Size'             => $entity->getSize(),
      'AvailabilityZone' => $entity->getAvailabilityZone(),
      'VolumeType'       => $entity->getVolumeType(),
      'Encrypted'        => $entity->getEncrypted() ? TRUE : FALSE,
    ];

    if ($entity->getVolumeType() === 'io1') {
      $params['Iops'] = (int) $entity->getIops();
    }

    if (!empty($entity->getKmsKeyId())) {
      $params['KmsKeyId'] = $entity->getKmsKeyId();
    }

    if (!empty($entity->getSnapshotId())) {
      $params['SnapshotId'] = $entity->getSnapshotId();
    }

    // Support description in OpenStack.
    if ($this->ec2Service instanceof OpenStackRestService && !empty($entity->getDescription())) {
      $params['Description'] = $entity->getDescription();
    }

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->createVolume($params);
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    if (!($entity->setVolumeId($result['VolumeId'])
      && $entity->setCreated(strtotime($result['CreateTime']))
      && $entity->setState($result['State'])
      && $entity->setSnapshotName($this->getSnapshotName($entity->getCloudContext(), $entity->getSnapshotId(), $this->getModuleName($entity)))
      && $entity->setVolumeType($result['VolumeType'])
      && $entity->save())) {

      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    $this->updateNameAndCreatedByTags($entity, $entity->getVolumeId());

    $this->processOperationStatus($entity, 'created');
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCarrierGateway(CarrierGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->deleteCarrierGateway([
      'CarrierGatewayId' => $entity->getCarrierGatewayId(),
    ]);

    // If $result is NULL, some error should have occurred
    // when calling the AWS API.
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteElasticIp(ElasticIpInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service->setCloudContext($entity->getCloudContext());

      $allocation_id = $entity->getAllocationId();
      $public_ip = $entity->getPublicIp();
      $params = [];
      if ($entity->getDomain() === 'standard' && !empty($public_ip)) {
        $params['PublicIp'] = $public_ip;
      }
      elseif ($entity->getDomain() === 'vpc' && !empty($allocation_id)) {
        $params['AllocationId'] = $allocation_id;
        $params['NetworkBorderGroup'] = $entity->getNetworkBorderGroup();
      }

      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

      $result = $this->ec2Service->releaseAddress($params);

      // If $result is NULL, some error should have occurred
      // when calling the AWS API.
      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'deleted');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
          '@type'  => $entity->getEntityType()->getSingularLabel(),
          '@label' => $entity->getName(),
        ]));
        return TRUE;
      }

      // Update instances after the Elastic IP is deleted.
      if ($entity->getEntityTypeId() === 'aws_cloud_elastic_ip') {
        $this->ec2Service->updateInstances();
      }

      $entity->delete();

      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSubmitEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteElasticIpCloudResource(ElasticIpInterface $entity): bool {
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $allocation_id = $entity->getAllocationId();
    $public_ip = $entity->getPublicIp();
    $params = [];

    if ($entity->getDomain() === 'standard' && !empty($public_ip)) {
      $params['PublicIp'] = $public_ip;
    }
    elseif ($entity->getDomain() === 'vpc' && !empty($allocation_id)) {
      $params['AllocationId'] = $allocation_id;
      $params['NetworkBorderGroup'] = $entity->getNetworkBorderGroup();
    }

    return $this->ec2Service->releaseAddress($params) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteImage(ImageInterface $entity, array $form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    if ($entity->getEntityTypeId() === 'aws_cloud_image'
        && !$this->updateEntitySubmitForm($form_state, $entity)) {
      return FALSE;
    }

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
    $account_id = $this->cloudConfigPluginManager->loadConfigEntity()->get('field_account_id')->value;

    // If the image is not owned by the aws user,
    // the calling for deregisterImage will be skipped for AWS Cloud,
    // and it will be called for OpenStack.
    if (($entity->getEntityTypeId() === 'aws_cloud_image'
    && ($entity->getAccountId() !== $account_id))
    || ($result = $this->ec2Service->deregisterImage([
      'ImageId' => $entity->getImageId(),
    ])) !== NULL) {

      if (!empty($result['SendToWorker'])) {
        $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
          '@type'  => $entity->getEntityType()->getSingularLabel(),
          '@label' => $entity->getName(),
        ]));

        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return TRUE;
      }

      $entity->delete();

      // Do not change the following message since we cannot use
      // $entity->label(), which represents 'ami_name'.
      $this->messenger->addStatus($this->t('The @type @label has been deleted.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));
      $this->logDeletionMessage();
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSubmitEvent($entity);
    }
    else {

      // Do not change the following message since we cannot use
      // $entity->label(), which represents 'ami_name'.
      $this->messenger->addError($this->t('The @type @label could not be deleted.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));

      $this->logger($entity->getEntityType()->getProvider())->error($this->t('@type: @label could not be deleted.', [
        '@type' => $entity->getEntityType()->getLabel(),
        '@label' => $entity->label(),
      ]), [
        'link' => $entity->toLink($this->t('View'))->toString(),
      ]);
    }

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteImageCloudResource(ImageInterface $entity): bool {
    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());

    $account_id = $this->cloudConfigPluginManager->loadConfigEntity()->get('field_account_id')->value;

    // If the image is not owned by the aws user, the calling for
    // deregisterImage will be skipped for AWS Cloud, but it will be called for
    // OpenStack.
    if ($entity->getEntityTypeId() === 'aws_cloud_image' && $entity->getAccountId() !== $account_id) {

      return TRUE;
    }

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    return $this->ec2Service->deregisterImage(
      ['ImageId' => $entity->getImageId()]
    ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      if (!$this->updateEntitySubmitForm($form_state, $entity)) {
        return FALSE;
      }
      $this->ec2Service->setCloudContext($entity->getCloudContext());

      $result = $this->ec2Service->terminateInstances([
        'InstanceIds' => [$entity->getInstanceId()],
      ]);

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'deleted remotely');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return TRUE;
      }

      if (!isset($result['TerminatingInstances'][0]['InstanceId'])) {
        $this->processOperationErrorStatus($entity, 'deleted');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return FALSE;
      }

      $entity->delete();

      $this->entity = $entity;
      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSubmitEvent($entity);
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteInstancesImpl(InstanceInterface $entity): bool {
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->terminateInstances(
      ['InstanceIds' => [$entity->getInstanceId()]]
    ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteInternetGateway(InternetGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->deleteInternetGateway([
      'InternetGatewayId' => $entity->getInternetGatewayId(),
    ]);

    // If $result is NULL, some error should have occurred
    // when calling the AWS API.
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteKeyPair(KeyPairInterface $entity, array $form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->deleteKeyPair([
      'KeyName' => $entity->getKeyPairName(),
    ]);

    // If $result is NULL, some error should have occurred
    // when calling the AWS API.
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNetworkInterface(NetworkInterfaceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;

      $this->ec2Service->setCloudContext($entity->getCloudContext());

      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

      $result = $this->ec2Service->deleteNetworkInterface([
        'NetworkInterfaceId' => $entity->getNetworkInterfaceId(),
      ]);

      // If $result is NULL, some error should have occurred
      // when calling the AWS API.
      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'deleted');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
          '@type'  => $entity->getEntityType()->getSingularLabel(),
          '@label' => $entity->getName(),
        ]));
        return TRUE;
      }

      $entity->delete();

      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSubmitEvent($entity);

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSnapshot(SnapshotInterface $entity, array $form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->deleteSnapshot([
      'SnapshotId' => $entity->getSnapshotId(),
    ]);

    // If $result is NULL, some error should have occurred
    // when calling the AWS API.
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSnapshotCloudResource(SnapshotInterface $entity): bool {
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deleteSnapshot(
      ['SnapshotId' => $entity->getSnapshotId()]
    ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSubnet(SubnetInterface $entity, array $form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->deleteSubnet([
      'SubnetId' => $entity->getSubnetId(),
    ]);

    // If $result is NULL, some error should have occurred
    // when calling the AWS API.
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTransitGateway(TransitGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->deleteTransitGateway([
      'TransitGatewayId' => $entity->getTransitGatewayId(),
    ]);

    // If $result is NULL, some error should have occurred
    // when calling the AWS API.
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVolume(VolumeInterface $entity, array $form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->deleteVolume([
      'VolumeId' => $entity->getVolumeId(),
    ]);

    // If $result is NULL, some error should have occurred
    // when calling the AWS API.
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVolumeCloudResource(VolumeInterface $entity): bool {
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return !empty($this->ec2Service->deleteVolume([
      'VolumeId' => $entity->getVolumeId(),
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVpc(VpcInterface $entity, array $form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    // Delete flow log.
    aws_cloud_delete_flow_log($entity->getCloudContext(), $entity->getVpcId());

    $form_state->setRedirect('view.aws_cloud_vpc.list', ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->deleteVpc([
      'VpcId' => $entity->getVpcId(),
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVpcPeeringConnection(VpcPeeringConnectionInterface $entity, array $form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->deleteVpcPeeringConnection([
      'VpcPeeringConnectionId' => $entity->getVpcPeeringConnectionId(),
    ]);

    // If $result is NULL, some error should have occurred
    // when calling the AWS API.
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '@label' => $entity->getName(),
      ]));
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function disassociateElasticIp(ElasticIpInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service->setCloudContext($entity->getCloudContext());

      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

      $result = $this->ec2Service->disassociateAddress([
        'AssociationId' => $entity->getAssociationId(),
      ]);

      if ($result === NULL) {
        $this->messenger->addError($this->t('Unable to disassociate @label.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]));
        return FALSE;
      }

      // Get module name.
      $module_name = $this->getModuleName($entity);

      $instance_id = $entity->getInstanceId();
      $network_interface_id = $entity->getNetworkInterfaceId();
      if (empty($instance_id) || empty($network_interface_id)) {
        return FALSE;
      }

      $instance = $this->getInstanceById($instance_id, $module_name, $entity->getCloudContext());
      $instance_link = $this->entityLinkRenderer->renderViewElement(
        $instance_id,
        "{$module_name}_instance",
        'instance_id',
        [],
        !empty($instance->getName()) !== $instance->getInstanceId()
          ? $this->t('@instance_name (@instance_id)', [
            '@instance_name' => $instance->getName(),
            '@instance_id' => $instance_id,
          ])
          : $instance_id
      );

      $network_interface = $this->getNetworkInterfaceById($network_interface_id, $module_name, $entity->getCloudContext());
      $network_interface_link = $this->entityLinkRenderer->renderViewElement(
        $network_interface_id,
        "{$module_name}_network_interface",
        'network_interface_id',
        [],
        $network_interface->getName() !== $network_interface->getNetworkInterfaceId()
          ? $this->t('@network_interface_name (@network_interface_id)', [
            '@network_interface_name' => $network_interface->getName(),
            '@network_interface_id' => $network_interface_id,
          ])
          : $network_interface_id
      );

      if (!empty($result['SendToWorker'])) {
        $this->messenger->addStatus($this->t('@label disassociated from remotely: <ul><li>
          Instance: @instance_id </li> <li>Network: @network_id</li></ul>', [
            '@label' => $entity->getEntityType()->getSingularLabel(),
            '@instance_id' => Markup::create($instance_link['#markup']),
            '@network_id' => Markup::create($network_interface_link['#markup']),
          ]));
        return TRUE;
      }

      $this->messenger->addStatus($this->t('@label disassociated from: <ul><li>
        Instance: @instance_id </li> <li>Network: @network_id</li></ul>', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
          '@instance_id' => Markup::create($instance_link['#markup']),
          '@network_id' => Markup::create($network_interface_link['#markup']),
        ]));
      $this->ec2Service->updateElasticIps();
      $this->ec2Service->updateInstances();
      $this->ec2Service->updateNetworkInterfaces();

      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSubmitEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSecurityGroup(SecurityGroupInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service->setCloudContext($entity->getCloudContext());

      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

      $result = $this->ec2Service->deleteSecurityGroup([
        'GroupId'   => $entity->getGroupId(),
      ]);

      // If $result is NULL, some error should have occurred
      // when calling the AWS API.
      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'deleted');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
          '@type'  => $entity->getEntityType()->getSingularLabel(),
          '@label' => $entity->getName(),
        ]));
        return TRUE;
      }

      $entity->delete();

      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSubmitEvent($entity);

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSecurityGroupCloudResource(SecurityGroupInterface $entity): bool {
    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deleteSecurityGroup(
      ['GroupId' => $entity->getGroupId()]
      ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function editElasticIp(ElasticIpInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->saveAwsCloudContent($entity, $form, $form_state);

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $this->setEc2Service($this->ec2Service);

      // Delete old tags.
      $this->ec2Service->deleteTags([
        'Resources' => [$entity->getAllocationId()],
      ]);

      // Update the name.
      $this->updateNameAndCreatedByTags($entity, $entity->getAllocationId());

      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editImage(ImageInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      /** @var \Drupal\aws_cloud\Entity\Ec2\ImageInterface $original_entity */
      $original_entity = $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->load($entity->id());

      $this->saveAwsCloudContent($entity, $form, $form_state);

      // Delete old tags.
      $this->ec2Service->deleteTags([
        'Resources' => [$entity->getImageId()],
      ]);

      $this->updateNameAndCreatedByTags($entity, $entity->getImageId());

      $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

      // Call 'modifyImageAttribute'
      // only when OwnerId is same as Account ID of Cloud Config.
      // It's to avoid AuthFailure error from AWS API.
      if ($cloud_config->get('field_account_id')->value !== $entity->getAccountId()) {
        return TRUE;
      }

      $form_values = $form_state->getValues();
      $visibility = $form_values['visibility'] ?? 0;

      $permission = [
        [
          'Group' => 'all',
          'UserId' => $entity->getAccountId(),
        ],
      ];

      // 1 => Public, 0 => Private.
      $launch_permission =
        $visibility
          ? ['Add' => $permission]
          : ['Remove' => $permission];

      // Update image.
      $this->ec2Service->setCloudContext($entity->getCloudContext());

      if (!method_exists($this->ec2Service, 'modifyImageAttribute')) {
        return TRUE;
      }

      // Update image description.
      $this->ec2Service->modifyImageAttribute([
        'ImageId' => $entity->getImageId(),
        'Description' => ['Value' => $entity->getDescription()],
      ]);

      // These fields (Description , LaunchPermission , or ProductCode) cannot
      // be specified at the same time.
      // See also:
      // https://docs.aws.amazon.com/cli/latest/reference/ec2/modify-image-attribute.html
      // Update image launch permission.
      $this->ec2Service->modifyImageAttribute([
        'ImageId' => $entity->getImageId(),
        'LaunchPermission' => $launch_permission,
      ]);

      $new_account_ids = [];
      foreach ($entity->getLaunchPermissionAccountIds() ?: [] as $account_id) {
        if (empty($account_id->value)) {
          continue;
        }
        $new_account_ids[] = $account_id->value;
      }

      $old_account_ids = [];
      foreach ($original_entity->getLaunchPermissionAccountIds() ?: [] as $account_id) {
        if (empty($account_id->value)) {
          continue;
        }
        $old_account_ids[] = $account_id->value;
      }

      $account_ids_to_remove = array_diff($old_account_ids, $new_account_ids);
      $account_ids_to_add = array_diff($new_account_ids, $old_account_ids);

      if (!empty($account_ids_to_remove)) {
        $this->ec2Service->modifyImageAttribute([
          'ImageId' => $entity->getImageId(),
          'LaunchPermission' => [
            'Remove' => array_map(static function ($item) {
              return ['UserId' => $item];
            }, $account_ids_to_remove),
          ],
        ]);
      }

      if (!empty($account_ids_to_add)) {
        $this->ec2Service->modifyImageAttribute([
          'ImageId' => $entity->getImageId(),
          'LaunchPermission' => [
            'Add' => array_map(static function ($item) {
              return ['UserId' => $item];
            }, $account_ids_to_add),
          ],
        ]);
      }

      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $termination_timestamp = !empty($form_state->getValue('termination_timestamp')[0]['value'])
        ? $form_state->getValue('termination_timestamp')[0]['value']->getTimeStamp()
        : NULL;

      if ($termination_timestamp === NULL) {
        // Unset the termination timestamp.
        $entity->set('termination_timestamp', NULL);
      }

      $security_groups = array_values($form_state->getValue('security_groups'));
      if ($security_groups === NULL) {
        $security_groups = [];
      }
      $entity->setSecurityGroups(implode(InstanceEditForm::SECURITY_GROUP_DELIMITER, $security_groups));

      $this->saveAwsCloudContent($entity, $form, $form_state);

      if (!$form_state->getValue('user_data_base64_encoded')) {
        $entity->setUserData(base64_encode($entity->getUserData() ?? ''));
        $entity->save();
      }

      $user_data_default_value = $form['options']['user_data_base64_encoded']['#default_value']
        ? $form['options']['user_data']['#default_value']
        : base64_encode($form['options']['user_data']['#default_value'] ?? '');

      $config = $this->configFactory->get('aws_cloud.settings');

      $this->updateTagsField(
        $entity,
        $form_state->getValue('tags'),
        $termination_timestamp,
        $config->get('aws_cloud_scheduler') === TRUE
          ? $entity->getSchedule()
          : NULL
      );

      $this->ec2Service->setCloudContext($entity->getCloudContext());

      $this->updateAwsTags($entity);

      // Update the instance type.
      $this->updateInstanceType($entity, NULL, $form['instance']['instance_type']['#default_value']);

      // Update security group.
      $security_groups = array_values($form_state->getValue('security_groups'));
      if ($security_groups === NULL) {
        $security_groups = [];
      }
      $this->updateSecurityGroup(
        $entity,
        InstanceEditForm::SECURITY_GROUP_DELIMITER,
        $form['network']['security_groups']['#default_value'],
        $security_groups
      );

      // Update terminate protection.
      $this->updateTerminateProtection($entity, NULL, $form['options']['termination_protection']['#default_value']);

      // Update IAM role.
      $this->updateIamRole($entity);

      // Update user data.
      $this->updateUserData($entity, NULL, $user_data_default_value);

      // If Elastic IP is specified and instance is stopped, attach it.
      $this->attachElasticIp(
        $entity,
        $form_state->getValue('add_new_elastic_ip'),
        $form_state->getValue('current_allocation_id'),
        $form_state->getValue('current_association_id')
      );

      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editKeyPair(KeyPairInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      // Call copyFormItemValues() to ensure the form array is intact.
      $this->copyFormItemValues($entity, $form);

      $this->trimTextfields($entity, $form, $form_state);

      $bundle = str_contains($entity->bundle(), 'aws_cloud')
        ? 'aws_cloud' : 'openstack';

      $uid_key_name = $this->cloudService->getTagKeyCreatedByUid(
        $bundle,
        $entity->getCloudContext());

      $this->ec2Service->setCloudContext($entity->getCloudContext());

      if ($entity->save()) {
        // Delete old tags.
        $this->ec2Service->deleteTags([
          'Resources' => [$entity->getKeyPairId()],
        ]);

        $this->setTagsInAws($entity, $entity->getKeyPairName(), [
          $uid_key_name => !empty($entity->getOwner())
            ? $entity->getOwner()->id() : 0,
        ], $entity->getEntityTypeId());
      }

      $this->processOperationStatus($entity, 'updated');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editNetworkInterface(NetworkInterfaceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->ec2Service->setCloudContext($entity->getCloudContext());

      $this->saveAwsCloudContent($entity, $form, $form_state);

      $params = [
        'NetworkInterfaceId' => $entity->getNetworkInterfaceId(),
        'Description' => ['Value' => $entity->getDescription()],
      ];

      if (method_exists($this->ec2Service, 'modifyNetworkInterfaceAttribute')) {
        $this->ec2Service->modifyNetworkInterfaceAttribute($params);
      }

      // Delete old tags.
      $this->ec2Service->deleteTags([
        'Resources' => [$entity->getNetworkInterfaceId()],
      ]);

      $this->updateNameAndCreatedByTags($entity, $entity->getNetworkInterfaceId());

      // Update OpenStack REST NetworkInterfaces.
      if ($this->ec2Service instanceof OpenStackRestService) {
        /** @var \Drupal\openstack\Service\Rest\OpenStackService|\Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service */
        $ec2_service = $this->ec2Service;
        // Delete old tags.
        $ec2_service->deleteTags(
          [
            'NetworkInterfaceId' => $entity->getNetworkInterfaceId(),
            'EntityType'         => $entity->getEntityTypeId(),
          ]);

        $ec2_service->updateNetworkInterfaces([
          'NetworkInterfaceId' => $entity->getNetworkInterfaceId(),
          'Name' => $entity->getName(),
          'Description' => $entity->getDescription(),
        ]);
      }

      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editSecurityGroup(SecurityGroupInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      // Call copyFormItemValues() to ensure the form array is intact.
      $this->copyFormItemValues($entity, $form);

      $this->trimTextfields($entity, $form, $form_state);

      $this->ec2Service->setCloudContext($entity->getCloudContext());

      /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroup */
      $existing_group = $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->load($entity->id());

      if ($entity->save()) {
        // Delete old tags.
        $this->ec2Service->deleteTags([
          'Resources' => [$entity->getGroupId()],
        ]);

        $this->updateNameAndCreatedByTags($entity, $entity->getGroupId());

        // Update the inbound/outbound permissions.
        $this->updateIngressEgressPermissions($entity, $entity, $existing_group);

        // Have the system refresh the security group.
        $this->ec2Service->updateSecurityGroups([
          'GroupIds' => [$entity->getGroupId()],
        ], FALSE);

        if (empty($this->messenger->messagesByType('error'))) {
          // Check API calls, see if the permissions updates were
          // successful or not.
          $this->validateAuthorize($entity);
          $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.canonical", [
            'cloud_context' => $entity->getCloudContext(),
            $entity->getEntityTypeId() => $entity->id(),
          ]);
        }
        $this->clearCacheValues($entity->getCacheTags());
        $this->dispatchSaveEvent($entity);
        return TRUE;
      }
      else {
        $this->processOperationErrorStatus($this->entity, 'updated');
        return FALSE;
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editSnapshot(SnapshotInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->copyFormItemValues($entity, $form);

      $this->trimTextfields($entity, $form, $form_state);

      $this->saveCloudContent($entity);

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $this->setEc2Service($this->ec2Service);

      $projectId = $this->ec2Service instanceof OpenStackRestService
        ? $this->getOpenStackProjectIdByCloudContext($entity->getCloudContext()) : '';

      // Delete old tags.
      $params = $this->ec2Service instanceof OpenStackRestService
        ? [
          'EntityType' => $entity->getEntityTypeId(),
          'SnapshotId' => $entity->getSnapshotId(),
          'UidKeyName' => $this->ec2Service->getTagKeyCreatedByUid(
            'openstack',
            $entity->getCloudContext()
          ),
          'ProjectId' => $projectId,
        ]
        : [
          'Resources' => [$entity->getSnapshotId()],
        ];
      $this->ec2Service->deleteTags($params);

      // Update the name.
      $this->updateNameAndCreatedByTags($entity, $entity->getSnapshotId());

      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editVolume(VolumeInterface $entity, array $form, FormStateInterface $form_state): bool {
    $this->copyFormItemValues($entity, $form);

    $this->trimTextfields($entity, $form, $form_state);

    $this->entity = $entity;

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $projectId = $this->ec2Service instanceof OpenStackRestService
      ? $this->getOpenStackProjectIdByCloudContext($entity->getCloudContext()) : '';

    /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface|null */
    $old_entity = $this->entityTypeManager
      ->getStorage('aws_cloud_volume')
      ->load($entity->id());

    if ($entity->save()) {
      // Delete old tags.
      $params = $this->ec2Service instanceof OpenStackRestService
        ? [
          'EntityType' => $entity->getEntityTypeId(),
          'VolumeId' => $entity->getVolumeId(),
          'UidKeyName' => $this->ec2Service->getTagKeyCreatedByUid(
            'openstack',
            $entity->getCloudContext()
          ),
          'ProjectId' => $projectId,
        ]
        : [
          'Resources' => [$entity->getSnapshotId()],
        ];
      $this->ec2Service->deleteTags($params);

      // Update name.
      $this->updateNameAndCreatedByTags($entity, $entity->getVolumeId());

      // Update volume type.
      if ($entity->getEntityTypeId() === 'aws_cloud_volume') {
        if (!empty($entity)
          && !empty($old_entity)
          && $this->isVolumeChanged($entity, $old_entity)) {
          $params = [
            'VolumeId' => $entity->getVolumeId(),
            'VolumeType' => $entity->getVolumeType(),
            'Size' => $entity->getSize(),
          ];

          // Only if the type is io1, the iops can be modified.
          if ($entity->getVolumeType === 'io1' && $entity->getIops()) {
            $params['Iops'] = $entity->getIops();
          }

          $this->ec2Service->modifyVolume($params);
        }
      }

      $this->processOperationStatus($entity, 'updated');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);
    }
    else {

      $this->processOperationErrorStatus($entity, 'updated');
    }

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list",
      [
        'cloud_context' => $entity->getCloudContext(),
      ]);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function editCarrierGateway(CarrierGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      // Call copyFormItemValues() to ensure the form array is intact.
      $this->copyFormItemValues($entity, $form);

      $this->trimTextfields($entity, $form, $form_state);

      $old_carrier_gateway = $this->entityTypeManager
        ->getStorage('aws_cloud_carrier_gateway')
        ->load($entity->id());

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      if (!empty($old_carrier_gateway)) {

        // Delete old tags.
        $this->ec2Service->deleteTags([
          'Resources' => [$entity->getCarrierGatewayId()],
        ]);

        // Update tags.
        $tag_map = [];
        foreach ($entity->getTags() ?: [] as $tag) {
          $tag_map[$tag['item_key']] = $tag['item_value'];
        }
        $this->setTagsInAws($entity, $entity->getCarrierGatewayId(), $tag_map);

        $this->updateNameAndCreatedByTags($entity, $entity->getCarrierGatewayId());

        if ($this->isCloudConfigRemote($entity)) {
          $this->processOperationStatus($entity, 'updated remotely');
          return TRUE;
        }

        $entity->save();

        // Update the carrier gateway.
        $this->ec2Service->updateCarrierGateways([
          'CarrierGatewayId' => $entity->getCarrierGatewayId(),
        ], FALSE);

        $this->processOperationStatus($entity, 'updated');
        $this->clearCacheValues($entity->getCacheTags());
        $this->dispatchSaveEvent($entity);
      }
      else {
        $this->messenger->addError($this->t('Unable to update @label.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]));
      }

      $form_state->setRedirect('view.aws_cloud_carrier_gateway.list', [
        'cloud_context' => $entity->getCloudContext(),
      ]);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editInternetGateway(InternetGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      // Call copyFormItemValues() to ensure the form array is intact.
      $this->copyFormItemValues($entity, $form);

      $this->trimTextfields($entity, $form, $form_state);

      $old_internet_gateway = $this->entityTypeManager
        ->getStorage('aws_cloud_internet_gateway')
        ->load($entity->id());

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      if (!empty($old_internet_gateway)) {

        // Delete old tags.
        $this->ec2Service->deleteTags([
          'Resources' => [$entity->getInternetGatewayId()],
        ]);

        // Update tags.
        $tag_map = [];
        foreach ($entity->getTags() ?: [] as $tag) {
          $tag_map[$tag['item_key']] = $tag['item_value'];
        }
        $this->setTagsInAws($entity, $entity->getInternetGatewayId(), $tag_map);

        $this->updateNameAndCreatedByTags($entity, $entity->getInternetGatewayId());

        if ($this->isCloudConfigRemote($entity)) {
          $this->processOperationStatus($entity, 'updated remotely');
          return TRUE;
        }

        $entity->save();

        // Update the internet gateway.
        $this->ec2Service->updateInternetGateways([
          'InternetGatewayId' => $entity->getInternetGatewayId(),
        ], FALSE);

        $this->processOperationStatus($entity, 'updated');
        $this->clearCacheValues($entity->getCacheTags());
        $this->dispatchSaveEvent($entity);

        return TRUE;
      }
      else {
        $this->messenger->addError($this->t('Unable to update @label.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]));
      }

      $form_state->setRedirect('view.aws_cloud_internet_gateway.list', [
        'cloud_context' => $entity->getCloudContext(),
      ]);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editTransitGateway(TransitGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      // Call copyFormItemValues() to ensure the form array is intact.
      $this->copyFormItemValues($entity, $form);

      $this->trimTextfields($entity, $form, $form_state);

      $old_transit_gateway = $this->entityTypeManager
        ->getStorage('aws_cloud_transit_gateway')
        ->load($entity->id());

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      if (!empty($old_transit_gateway) && $entity->save()) {

        // Modify transit gateway.
        $params = [
          'Description' => $entity->getDescription(),
          'TransitGatewayId' => $entity->getTransitGatewayId(),
          'Options' => [
            'AutoAcceptSharedAttachments' => $entity->isAutoAcceptSharedAttachments() ? 'enable' : 'disable',
            'DefaultRouteTableAssociation' => $entity->isDefaultRouteTableAssociation() ? 'enable' : 'disable',
            'DefaultRouteTablePropagation' => $entity->isDefaultRouteTablePropagation() ? 'enable' : 'disable',
            'DnsSupport' => $entity->isDnsSupport() ? 'enable' : 'disable',
            'VpnEcmpSupport' => $entity->isVpnEcmpSupport() ? 'enable' : 'disable',
          ],
        ];
        if ($entity->isDefaultRouteTableAssociation()) {
          $params['Options']['AssociationDefaultRouteTableId'] = $entity->getAssociationDefaultRouteTableId();
        }
        if ($entity->isDefaultRouteTablePropagation()) {
          $params['Options']['PropagationDefaultRouteTableId'] = $entity->getPropagationDefaultRouteTableId();
        }

        $this->ec2Service->modifyTransitGateway($params);

        // Delete old tags.
        $this->ec2Service->deleteTags([
          'Resources' => [$entity->getTransitGatewayId()],
        ]);

        // Update tags.
        $tag_map = [];
        foreach ($entity->getTags() ?: [] as $tag) {
          $tag_map[$tag['item_key']] = $tag['item_value'];
        }
        $this->setTagsInAws($entity, $entity->getTransitGatewayId(), $tag_map);

        $this->updateNameAndCreatedByTags($entity, $entity->getTransitGatewayId());

        // Update the transit gateway.
        $this->ec2Service->updateTransitGateways([
          'TransitGatewayId' => $entity->getTransitGatewayId(),
        ], FALSE);

        $this->processOperationStatus($entity, 'updated');
        $this->clearCacheValues($entity->getCacheTags());
        $this->dispatchSaveEvent($entity);
      }
      else {
        $this->messenger->addError($this->t('Unable to update @label.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]));
      }

      $form_state->setRedirect('view.aws_cloud_transit_gateway.list', [
        'cloud_context' => $entity->getCloudContext(),
      ]);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConsoleOutput(InstanceInterface $entity): string {
    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->getConsoleOutput(['InstanceId' => $entity->getInstanceId()]);

    $output = !empty($result) && !empty($result['Output'])
      ? base64_decode($result['Output'])
      : '';

    return !empty($output) ? $output : '';
  }

  /**
   * {@inheritdoc}
   */
  public function importKeyPair(KeyPairInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      if (!($path = $form_state->getValue('key_pair_public_key'))) {
        return FALSE;
      }

      $handle = fopen($path, 'r');
      $key_material = fread($handle, filesize($path));
      fclose($handle);

      $result = $this->ec2Service->importKeyPair([
        'KeyName' => $entity->getKeyPairName(),
        'PublicKeyMaterial' => $key_material,
        'TagSpecifications' => [
          [
            'ResourceType' => 'key-pair',
            'Tags' => [
              [
                'Key' => $this->cloudService->getTagKeyCreatedByUid(
                  'aws_cloud',
                  $entity->getCloudContext()),
                'Value' => !empty($entity->getOwner()) ? $entity->getOwner()->id() : 0,
              ],
            ],
          ],
        ],
      ]);

      if (empty($result)) {
        $this->processOperationErrorStatus($entity, 'imported');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'imported remotely');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return TRUE;
      }

      // Following AWS specification and not storing key material.
      if ((!empty($entity)
      && $entity->setKeyFingerprint($result['KeyFingerprint'])
      && $entity->setKeyPairId($result['KeyPairId'])
      && $entity->save())) {

        // Use the custom message since key pair uses 'key_pair_name' for its
        // own label.  So do not change the following code.
        $this->messenger->addStatus($this->t('The @type %label has been imported.', [
          '@type' => $entity->getEntityType()->getSingularLabel(),
          '%label' => $entity->toLink($entity->getKeyPairName())->toString(),
        ]));
        $this->logOperationMessage($entity, 'imported');

        $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.canonical", [
          'cloud_context' => $entity->getCloudContext(),
          $entity->getEntityTypeId() => $entity->id(),
        ]);

        $this->clearCacheValues($entity->getCacheTags());
        $this->dispatchSaveEvent($entity);
        return TRUE;
      }

      $this->processOperationErrorStatus($entity, 'imported');

      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
      return FALSE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'imported');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function launchInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      // Set parameters to entity.
      $params = [
        'image_id'          => $form['image_id']['#value'],
        'max_count'         => $form['max_count']['#value'],
        'min_count'         => $form['min_count']['#value'],
        'key_pair_name'     => $form['key_pair_name']['#value'],
        'is_monitoring'     => $form['is_monitoring']['#value'] ? TRUE : FALSE,
        'availability_zone' => $form['availability_zone']['#value'],
        'security_groups'   => $form['security_groups']['#value'],
        'instance_type'     => $form['instance_type']['#value'],
        'kernel_id'         => $form['kernel_id']['#value'],
        'ramdisk_id'        => $form['ramdisk_id']['#value'],
        'user_data'         => $form['user_data']['#value'],
      ];

      foreach ($params ?: [] as $key => $value) {
        $entity->set($key, $value);
      }

      // Get parameters for launching an instance.
      $key_name       = preg_replace('/ \([^)]*\)$/', '', $entity->getKeyPairName());
      $security_group = preg_replace('/ \([^)]*\)$/', '', $entity->getSecurityGroups());

      $params = [
        // The following parameters are required.
        'ImageId'        => $entity->getImageId(),
        'MaxCount'       => $entity->getMaxCount(),
        'MinCount'       => $entity->getMinCount(),
        'InstanceType'   => $entity->getInstanceType(),
        'Monitoring'     => ['Enabled' => $entity->isMonitoring() ? TRUE : FALSE],
        'KeyName'        => $key_name,
        'Placement'      => ['AvailabilityZone' => $entity->getAvailabilityZone()],
        'SecurityGroups' => [$security_group],
      ];

      // The following parameters are optional.
      $params['KernelId'] ?: $entity->getKernelId();
      $params['RamdiskId'] ?: $entity->getRamdiskId();
      $params['UserData'] ?: $entity->getUserData();

      // Launch an instance.
      $result = $this->ec2Service->runInstances($params, []);

      if (!isset($result['Instances'][0]['InstanceId'])) {
        $this->processOperationErrorStatus($entity, 'launched');
        return FALSE;
      }

      if (($entity->setInstanceId($result['Instances'][0]['InstanceId']))
        && ($entity->setPublicIp($result['Instances'][0]['PublicIpAddress']))
        && ($entity->setKeyPairName($result['Instances'][0]['KeyName']))
        && ($entity->setInstanceState($result['Instances'][0]['State']['Name']))
        && ($entity->setCreated($result['Instances'][0]['LaunchTime']))
        && ($entity->save())) {

        $instance_ids = array_map(static function ($result_instance) {
          return $result_instance['InstanceId'];
        }, $result['Instances']);

        $this->messenger->addStatus($this->t('The @type "@label (@instance_id)" request has been initiated. This may take some time. Use Refresh to update the status.', [
          '@type'        => $entity->getEntityType()->getSingularLabel(),
          '@label'       => $entity->label(),
          '@instance_id' => implode(', ', $instance_ids),
        ]));

        $form_state->setRedirectUrl(
          $entity
            ->toUrl('collection')
            ->setRouteParameter('cloud_context', $entity->getCloudContext())
        );
      }
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'launched');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function rebootInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      if (!$this->updateEntitySubmitForm($form_state, $entity)) {
        return FALSE;
      }

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $result = $this->ec2Service->rebootInstances([
        'InstanceIds' => [
          $entity->getInstanceId(),
        ],
      ]);

      if (empty($result)) {
        $this->processOperationErrorStatus($entity, 'rebooted');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'rebooted remotely');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return TRUE;
      }

      $this->processOperationStatus($entity, 'rebooted');
      $this->clearCacheValues($entity->getCacheTags());
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'rebooted');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function rebootInstanceCloudResource(InstanceInterface $entity): bool {
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->rebootInstances([
      'InstanceIds' => [$entity->getInstanceId()],
    ]) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function revokeSecurityGroup(SecurityGroupInterface $entity, array $form, FormStateInterface $form_state, string $type, string $position): bool {
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $permission = $this->getPermission($type, $position, $entity);
    if (!$permission) {
      return FALSE;
    }

    $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.canonical", [
      'cloud_context' => $entity->getCloudContext(),
      $entity->getEntityTypeId() => $entity->id(),
    ]);

    $perm_array = [
      'GroupId' => $entity->getGroupId(),
    ];

    if ($type === 'outbound_permission') {
      $perm_array['IpPermissions'][] = $this->formatIpPermissionForRevoke($permission);
      $result = $this->ec2Service->revokeSecurityGroupEgress($perm_array);
    }
    else {
      $perm_array['IpPermissions'][] = $this->formatIpPermissionForRevoke($permission);
      $result = $this->ec2Service->revokeSecurityGroupIngress($perm_array);
    }

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('Permission revoked remotely'));
      return TRUE;
    }

    // Have the system refresh the security group.
    $this->ec2Service->updateSecurityGroups([
      'GroupIds' => [$entity->getGroupId()],
    ], FALSE);

    $this->validateRevoke($type, $entity)
      ? $this->messenger->addStatus($this->t('Permission revoked'))
      : $this->messenger->addError($this->t('Permission not revoked'));

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function startInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      if (!$this->updateEntitySubmitForm($form_state, $entity)) {
        return FALSE;
      }

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $result = $this->ec2Service->startInstances([
        'InstanceIds' => [
          $entity->getInstanceId(),
        ],
      ]);

      if (empty($result)) {
        $this->processOperationErrorStatus($entity, 'started');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'started remotely');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return TRUE;
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'started');
      return FALSE;
    }

    try {
      $current_state = $result['StartingInstances'][0]['CurrentState']['Name'];
      /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
      $instance = $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->load($entity->id());
      $instance->setInstanceState($current_state);
      $instance->save();

      $this->processOperationStatus($instance, 'started');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSubmitEvent($entity);
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'started');
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function startInstanceCloudResource(InstanceInterface $entity): bool {
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->startInstances([
      'InstanceIds' => [$entity->getInstanceId()],
    ]) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function stopInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      if (!$this->updateEntitySubmitForm($form_state, $entity)) {
        return FALSE;
      }

      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $result = $this->ec2Service->stopInstances([
        'InstanceIds' => [
          $entity->getInstanceId(),
        ],
      ]);

      if (empty($result)) {
        $this->processOperationErrorStatus($entity, 'stopped');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'stopped remotely');
        $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
        return TRUE;
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'stopped');
      return FALSE;
    }

    try {
      $current_state = $result['StoppingInstances'][0]['CurrentState']['Name'];
      /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
      $instance = $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->load($entity->id());
      $instance->setInstanceState($current_state);
      $instance->save();

      $this->processOperationStatus($instance, 'stopped');
      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSubmitEvent($entity);
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'stopped');
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function stopInstanceCloudResource(InstanceInterface $entity): bool {
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->stopInstances([
      'InstanceIds' => [$entity->getInstanceId()],
    ]) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function attachVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\aws_cloud\Entity\Ec2\Volume $entity */
    $this->entity = $entity;

    $instance_id = $form_state->getValue('instance_id');
    $volume_id = $entity->getVolumeId();
    $device_name = $form_state->getValue('device_name');

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->attachVolume([
      'InstanceId' => $instance_id,
      'VolumeId' => $volume_id,
      'Device' => $device_name,
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'attached');
      return FALSE;
    }

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The volume %volume is attaching to %instance on %device_name remotely.', [
        '%volume' => $volume_id,
        '%instance' => $instance_id,
        '%device_name' => $device_name,
      ]));
      return TRUE;
    }

    // Set the instance_id in the volume entity and save.
    $entity->setAttachmentInformation($instance_id);
    $entity->setAttachmentDeviceName($device_name);
    $entity->setState($result['State'] ?? 'in-use');
    $entity->save();
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    $this->messenger->addStatus($this->t('The volume %volume is attaching to %instance on %device_name.', [
      '%volume' => $volume_id,
      '%instance' => $instance_id,
      '%device_name' => $device_name,
    ]));
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function attachInternetGateway(InternetGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\aws_cloud\Entity\Vpc\InternetGateway $entity */
    $this->entity = $entity;

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $vpc_id = $form_state->getValue('vpc_id');
    $internet_gateway_id = $entity->getInternetGatewayId();

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->attachInternetGateway([
      'InternetGatewayId' => $internet_gateway_id,
      'VpcId' => $vpc_id,
    ]);

    if ($result === NULL) {
      return FALSE;
    }

    $internet_gateway_link = $this->entityLinkRenderer->renderViewElement(
      $internet_gateway_id,
      "{$module_name}_internet_gateway",
      'internet_gateway_id',
      [],
      '',
      EntityLinkWithNameHtmlGenerator::class
    );

    $vpc_link = $this->entityLinkRenderer->renderViewElement(
      $vpc_id,
      "{$module_name}_vpc",
      'vpc_id',
      [],
      '',
      EntityLinkWithNameHtmlGenerator::class
    );

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The internet gateway %internet_gateway will be attached to VPC %vpc remotely.', [
        '%internet_gateway' => Markup::create($internet_gateway_link['#markup']),
        '%vpc' => Markup::create($vpc_link['#markup']),
      ]));
      return TRUE;
    }

    $this->messenger->addStatus($this->t('The internet gateway %internet_gateway is attached to VPC %vpc.', [
      '%internet_gateway' => Markup::create($internet_gateway_link['#markup']),
      '%vpc' => Markup::create($vpc_link['#markup']),
    ]));

    // Update the internet gateway.
    $this->ec2Service->updateInternetGateways([
      'InternetGatewayIds' => [$internet_gateway_id],
    ], FALSE);

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function detachVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\aws_cloud\Entity\Ec2\Volume $entity */
    $this->entity = $entity;
    $volume_id = $entity->getVolumeId();
    $instance_id = $entity->getAttachmentInformation();
    $device_name = $entity->getAttachmentDeviceName();

    $module_name = $this->getModuleName($entity);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $params = [
      'VolumeId' => $volume_id,
    ];
    if ($module_name !== 'aws_cloud') {
      $params['InstanceId'] = $instance_id;
    }

    $result = $this->ec2Service->detachVolume($params);
    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'detached');
      return FALSE;
    }

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The volume %volume is detaching from %instance on %device_name remotely', [
        '%volume' => $volume_id,
        '%instance' => $instance_id,
        '%device_name' => $device_name,
      ]));
      return TRUE;
    }

    // Set the instance_id in the volume entity and save.
    $entity->setAttachmentInformation('');
    $entity->setAttachmentDeviceName('');
    $entity->setState($result['State'] ?? 'available');
    $entity->save();

    $this->messenger->addStatus($this->t('The volume %volume is detaching from %instance on %device_name', [
      '%volume' => $volume_id,
      '%instance' => $instance_id,
      '%device_name' => $device_name,
    ]));
    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function detachVolumeCloudResource(VolumeInterface $entity): bool {
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->detachVolume([
      'VolumeId' => $entity->getVolumeId(),
    ]) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function detachInternetGateway(InternetGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\aws_cloud\Entity\Vpc\InternetGateway $entity */
    $this->entity = $entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $vpc_id = $entity->getVpcId();

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $result = $this->ec2Service->detachInternetGateway([
      'InternetGatewayId' => $entity->getInternetGatewayId(),
      'VpcId' => $entity->getVpcId(),
    ]);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    if ($result === NULL) {
      $this->messenger->addError($this->t('Unable to detach internet gateway.'));
      return FALSE;
    }

    $internet_gateway_link = $this->entityLinkRenderer->renderViewElement(
      $entity->getInternetGatewayId(),
      "{$module_name}_internet_gateway",
      'internet_gateway_id',
      [],
      '',
      EntityLinkWithNameHtmlGenerator::class
    );

    $vpc_link = $this->entityLinkRenderer->renderViewElement(
      $vpc_id,
      "{$module_name}_vpc",
      'vpc_id',
      [],
      '',
      EntityLinkWithNameHtmlGenerator::class
    );

    if (!empty($result['SendToWorker'])) {
      $this->messenger->addStatus($this->t('The internet gateway %internet_gateway will be detached from VPC %vpc remotely', [
        '%internet_gateway' => Markup::create($internet_gateway_link['#markup']),
        '%vpc' => Markup::create($vpc_link['#markup']),
      ]));

      return TRUE;
    }

    $this->messenger->addStatus($this->t('The internet gateway %internet_gateway detached from VPC %vpc', [
      '%internet_gateway' => Markup::create($internet_gateway_link['#markup']),
      '%vpc' => Markup::create($vpc_link['#markup']),
    ]));

    // Update the internet gateway.
    $this->ec2Service->updateInternetGateways([
      'InternetGatewayIds' => [$entity->getInternetGatewayId()],
    ], FALSE);

    $this->clearCacheValues($entity->getCacheTags());
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      // Save as a new revision if requested to do so.
      $entity->setNewRevision(FALSE);

      $status = $this->saveCloudContent($entity);
      $this->messenger->deleteAll();

      $status_map = [
        SAVED_NEW => 'created',
        SAVED_UPDATED => 'updated',
      ];

      $this->processOperationStatus($entity, $status_map[$status] ?? $status_map[SAVED_UPDATED]);

      // Clear block and menu cache.
      $this->clearCacheValues($entity->getCacheTags());
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function copyCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->alterCloudLaunchTemplateCopyForm($entity, $form, $form_state);
      return $this->submitCloudLaunchTemplateCopyForm($entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'copied');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->alterCloudLaunchTemplateEditForm($entity, $form, $form_state);
      return $this->submitCloudLaunchTemplateEditForm($entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function reviewCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $form_state->cleanValues();
      // Update the changed timestamp of the entity.
      if ($entity instanceof EntityChangedInterface) {
        $entity->setChangedTime($this->time->getRequestTime());
      }

      // Launch the instance here.
      $redirect_route = [
        'route_name' => 'entity.cloud_launch_template.canonical',
        'params' => [
          'cloud_launch_template' => $entity->id(),
          'cloud_context' => $entity->getCloudContext(),
        ],
      ];
      // Let other modules alter the redirect after a cloud launch template has
      // been launched.
      $this->moduleHandler->invokeAll('cloud_launch_template_post_launch_redirect_alter', [
        &$redirect_route,
        $entity,
      ]);
      $form_state->setRedirectUrl(new Url($redirect_route['route_name'], $redirect_route['params']));

      // Clear block and menu cache.
      $this->clearCacheValues($entity->getCacheTags());
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'changed');
      return FALSE;
    }
  }

  /**
   * Save a cloud content.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Interface for entities having fields.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function saveCloudContent(FieldableEntityInterface $entity): int {
    if (!($entity instanceof CloudConfigInterface)) {
      /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $original_entity */
      $original_entity = $entity;
      $this->cloudConfigPluginManager->setCloudContext($original_entity->getCloudContext());
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    }
    else {
      $cloud_config = CloudConfig::create([
        'cloud_context' => $entity->getCloudContext(),
      ]);
    }

    if (!$entity->isNew() && !($entity instanceof CloudConfigInterface) && $cloud_config->isRemote()) {
      $status = SAVED_UPDATED;
      $passive_operation = 'updated remotely';
      $this->processOperationStatus($entity, $passive_operation);
      return $status;
    }

    $status = $entity->save();

    // Add an updated status message and the log.
    $status_map = [
      SAVED_NEW => 'created',
      SAVED_DELETED => 'deleted',
      SAVED_UPDATED => 'updated',
    ];

    $passive_operation = $status_map[$status] ?? $status_map[SAVED_UPDATED];

    if (!($entity instanceof CloudConfigInterface) && $cloud_config->isRemote()) {
      $passive_operation .= ' remotely';
    }

    $this->processOperationStatus($entity, $passive_operation);

    // Clear block and menu cache.
    $this->clearCacheValues($entity->getCacheTags());

    return $status;
  }

  /**
   * Helper function to update field tags.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The AWS Cloud instance entity.
   * @param array $form_tags
   *   Form tags.
   * @param int|null $termination_timestamp
   *   Termination timestamp by UNIX seconds.
   * @param string|null $schedule
   *   Schedule.
   */
  private function updateTagsField(
    InstanceInterface $entity,
    array $form_tags,
    ?int $termination_timestamp,
    ?string $schedule,
  ): void {
    usort($form_tags, static function ($a, $b) {
      return (int) $a['_weight'] - (int) $b['_weight'];
    });

    $module_name = $this->getModuleName($entity);
    $uid_key_name = $this->cloudService->getTagKeyCreatedByUid(
      'aws_cloud',
      $entity->getCloudContext()
    );

    $fixed_tags = [];
    $fixed_tags['Name'] = $entity->getName();
    $fixed_tags[$uid_key_name] = is_null($entity->getOwner())
      ? 0
      : $entity->getOwner()->id();

    $fixed_tags[$module_name . '_' . InstanceInterface::TAG_TERMINATION_TIMESTAMP] = is_null($termination_timestamp)
      ? ''
      : $termination_timestamp;

    $config = $this->configFactory->get('aws_cloud.settings');
    if ($config->get('aws_cloud_scheduler') === TRUE) {
      $fixed_tags['aws_cloud_scheduler_tag'] = $schedule;
    }

    $tags = [];
    foreach ($form_tags ?: [] as $form_tag) {
      $item_key = $form_tag['item_key'];
      if ($item_key === '') {
        continue;
      }

      // Skip special tags.
      if (strpos($item_key, 'aws:') === 0) {
        continue;
      }

      $item_value = $form_tag['item_value'];
      if (isset($fixed_tags[$item_key])) {
        $item_value = $fixed_tags[$item_key];
        unset($fixed_tags[$item_key]);
      }

      $tags[] = ['item_key' => $item_key, 'item_value' => $item_value];
    }

    foreach ($fixed_tags ?: [] as $item_key => $item_value) {
      $tags[] = ['item_key' => $item_key, 'item_value' => $item_value];
    }

    $entity->setTags($tags);
    $entity->save();
  }

  /**
   * Helper function to update AWS tags.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The AWS Cloud instance entity.
   */
  private function updateAwsTags(InstanceInterface $entity): void {
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $params = [
      'Resources' => [$entity->getInstanceId()],
    ];

    // Delete old tags.
    $this->ec2Service->deleteTags($params);

    foreach ($entity->getTags() ?: [] as $tag) {
      $params['Tags'][] = [
        'Key' => $tag['item_key'],
        'Value' => $tag['item_value'],
      ];
    }

    // Create Tags with different parameters for AWS and OpenStack.
    if (preg_match('[^aws_cloud]', $entity->getEntityTypeId()) === 1) {
      $this->ec2Service->createTags($params);
    }
    else {
      $this->ec2Service->createTags([
        'Resources' => [$entity->getInstanceId()],
        'Tags' => [
          ['Key' => 'Name', 'Value' => $entity->getName()],
        ],
      ]);
    }
  }

  /**
   * Update the instance type.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The AWS Cloud instance entity.
   * @param string|null $instance_type
   *   Instance type.
   * @param string|null $default_instance_type
   *   Default value of instance type.
   */
  private function updateInstanceType(
    InstanceInterface $entity,
    ?string $instance_type,
    ?string $default_instance_type,
  ): void {
    // If the state of the instance is not stopped, do not update it.
    if ($entity->getInstanceState() !== 'stopped') {
      return;
    }

    // When executed from Form, $instance_type is NULL.
    // When executed from REST API, $instance_type is not NULL.
    // If it is run from Form and the instance type of $entity
    // is equal to the default instance type, the instance type
    // is not changed and no update is performed.
    if ($instance_type === NULL
      && $entity->getInstanceType() === $default_instance_type) {
      return;
    }

    $this->ec2Service->modifyInstanceAttribute([
      'InstanceId' => $entity->getInstanceId(),
      'InstanceType' => ['Value' => $instance_type ?? $entity->getInstanceType()],
    ]);
  }

  /**
   * Get security group IDs by names.
   *
   * @param array $group_names
   *   Array of group names.
   * @param string $vpc_id
   *   VPC id used for the query.
   *
   * @return array
   *   Array of group names.
   */
  private function getSecurityGroupIdsByNames(array $group_names, string $vpc_id): array {
    // Querying for group name is done using filters.
    // @see https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-ec2-2016-11-15.html#describesecuritygroups
    $response = $this->ec2Service->describeSecurityGroups([
      'Filters' => [
        [
          'Name' => 'group-name',
          'Values' => $group_names,
        ],
        [
          // Specify VPC ID because the group names can duplicate across
          // different VPC networks. For example the 'default' group name
          // exists across different groups.
          'Name' => 'vpc-id',
          'Values' => [
            $vpc_id,
          ],
        ],
      ],
    ]);

    $group_ids = [];
    foreach ($response['SecurityGroups'] ?: [] as $security_group) {
      $group_ids[] = $security_group['GroupId'];
    }

    return $group_ids;
  }

  /**
   * Update security group.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The AWS Cloud instance entity.
   * @param string $security_group_delimiter
   *   Delimiter string for security groups.
   * @param array $default_security_groups
   *   Default value of security groups.
   * @param array $security_groups
   *   Security groups.
   */
  private function updateSecurityGroup(
    InstanceInterface $entity,
    string $security_group_delimiter,
    array $default_security_groups,
    array $security_groups,
  ): void {
    if ($entity->getSecurityGroups() !== implode($security_group_delimiter, $default_security_groups)) {

      $this->ec2Service->modifyInstanceAttribute([
        'InstanceId' => $entity->getInstanceId(),
        'Groups' => $this->getSecurityGroupIdsByNames($security_groups, $entity->getVpcId()),
      ]);
    }
  }

  /**
   * Update terminate protection.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The AWS Cloud instance entity.
   * @param bool|null $termination_protection
   *   Termination protection flag.
   * @param bool|null $default_termination_protection
   *   Default value of termination protection flag.
   */
  private function updateTerminateProtection(
    InstanceInterface $entity,
    ?bool $termination_protection,
    ?bool $default_termination_protection,
  ): void {
    if ($termination_protection === NULL
      && $entity->getTerminationProtection() === $default_termination_protection) {
      return;
    }

    $this->ec2Service->modifyInstanceAttribute([
      'InstanceId' => $entity->getInstanceId(),
      'DisableApiTermination' => [
        'Value' => $termination_protection ?? $entity->getTerminationProtection(),
      ],
    ]);
  }

  /**
   * Update IAM role.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The AWS Cloud instance entity.
   */
  private function updateIamRole(InstanceInterface $entity): void {
    if ($entity->getEntityTypeId() === 'aws_cloud_instance') {
      // Update IAM role.
      $associations_result = $this->ec2Service->describeIamInstanceProfileAssociations([
        'Filters' => [
          [
            'Name' => 'instance-id',
            'Values' => [$entity->getInstanceId()],
          ],
        ],
      ]);

      $associations = $associations_result['IamInstanceProfileAssociations'];
      if (empty($associations)) {
        if ($entity->getIamRole() !== NULL) {
          // Associate.
          $this->ec2Service->associateIamInstanceProfile([
            'InstanceId' => $entity->getInstanceId(),
            'IamInstanceProfile' => [
              'Arn' => $entity->getIamRole(),
            ],
          ]);
        }
      }
      else {
        if ($entity->getIamRole() === NULL) {
          // Disassociate.
          $this->ec2Service->disassociateIamInstanceProfile([
            'AssociationId' => $associations[0]['AssociationId'],
          ]);
        }
        elseif ($entity->getIamRole() !== $associations[0]['IamInstanceProfile']['Arn']) {
          // Disassociate.
          $this->ec2Service->disassociateIamInstanceProfile([
            'AssociationId' => $associations[0]['AssociationId'],
          ]);

          // Associate.
          $this->ec2Service->associateIamInstanceProfile([
            'InstanceId' => $entity->getInstanceId(),
            'IamInstanceProfile' => [
              'Arn' => $entity->getIamRole(),
            ],
          ]);
        }
      }
    }
  }

  /**
   * Update user data.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The AWS Cloud instance entity.
   * @param string|null $user_data
   *   User data.
   * @param string|null $default_user_data
   *   Default value of user data.
   */
  private function updateUserData(InstanceInterface $entity, ?string $user_data, ?string $default_user_data): void {
    if ($entity->getInstanceState() !== 'stopped') {
      return;
    }

    if ($user_data === NULL
      && $entity->getUserData() === $default_user_data) {
      return;
    }

    $user_data_impl = $user_data ?? base64_decode($entity->getUserData() ?? '');
    $this->ec2Service->modifyInstanceAttribute([
      'InstanceId' => $entity->getInstanceId(),
      'UserData' => ['Value' => $user_data_impl],
    ]);

    $url = Url::fromRoute(
      'entity.aws_cloud_instance.start_form', [
        'cloud_context' => $entity->getCloudContext(),
        'aws_cloud_instance' => $entity->id(),
      ]
    )->toString();

    $this->messenger->addStatus($this->t("The user data of @type %label has been updated. <strong><em><a href='@start_instance_link'>Start</a></em></strong> the @type to reflect the user data.", [
      '@type' => $entity->getEntityType()->getSingularLabel(),
      '%label' => $entity->toLink($entity->label())->toString(),
      '@start_instance_link' => $url,
    ]));
    $this->logOperationMessage($entity, 'updated');
  }

  /**
   * If Elastic IP is specified and instance is stopped, attach it.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The AWS Cloud instance entity.
   * @param string|null $new_elastic_ip
   *   New Elastic IP.
   * @param string|null $current_allocation_id
   *   Current Allocation ID.
   * @param string|null $current_association_id
   *   Current Association ID.
   */
  private function attachElasticIp(
    InstanceInterface $entity,
    ?string $new_elastic_ip,
    ?string $current_allocation_id,
    ?string $current_association_id,
  ): void {
    if ($entity->getInstanceState() === 'stopped') {
      if (isset($new_elastic_ip) && (int) $new_elastic_ip !== -1) {
        $update_entities = FALSE;

        if (isset($current_allocation_id) && isset($current_association_id)) {
          // Reassign the Elastic IP address to the instance.
          // (the Elastic IP address = ['AssociationId', 'InstanceId'])
          if ($current_allocation_id !== $new_elastic_ip) {
            $this->ec2Service->disassociateAddress([
              'AssociationId' => $current_association_id,
              'InstanceId' => $entity->getInstanceId(),
            ]);

            $this->ec2Service->associateAddress([
              'AllocationId' => $new_elastic_ip,
              'InstanceId' => $entity->getInstanceId(),
            ]);
            $update_entities = TRUE;
          }
        }
        else {
          // Instance does not have Elastic IP. Assign a new one.
          $this->ec2Service->associateAddress([
            'AllocationId' => $new_elastic_ip,
            'InstanceId' => $entity->getInstanceId(),
          ]);
          $update_entities = TRUE;
        }
        if ($update_entities === TRUE) {
          // Update the following entities if the Elastic IPs have changed.
          $this->ec2Service->updateElasticIps();
          $this->ec2Service->updateInstances();
          $this->ec2Service->updateNetworkInterfaces();
        }
      }
    }
  }

  /**
   * Helper function that loads an aws_cloud_elastic_ip entity.
   *
   * @param string $allocation_id
   *   The allocation ID to look up.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface|false
   *   The loaded aws_cloud_elastic_ip entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getElasticIp(string $allocation_id, string $cloud_context): ElasticIpInterface|FALSE {
    $elastic_ip = FALSE;
    $results = $this->entityTypeManager
      ->getStorage('aws_cloud_elastic_ip')
      ->loadByProperties([
        'allocation_id' => $allocation_id,
        'cloud_context' => $cloud_context,
      ]);
    if (count($results) === 1) {
      $elastic_ip = array_shift($results);
    }
    return $elastic_ip;
  }

  /**
   * Helper method to load image entity.
   *
   * @param string $cloud_context
   *   Cloud context.
   * @param string $image_id
   *   Image id to load.
   *
   * @return null|\Drupal\aws_cloud\entity\ec2\ImageInterface
   *   NULL if not found else image entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function loadNewImage(string $cloud_context, string $image_id): ?ImageInterface {
    $entities = $this->entityTypeManager
      ->getStorage('aws_cloud_image')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
        'image_id' => $image_id,
      ]);

    return !empty($entities)
      ? array_shift($entities)
      : NULL;
  }

  /**
   * Check if current cloud config is remote or not.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Interface for entities having fields.
   *
   * @return bool
   *   True if the current cloud config is remote.
   */
  private function isCloudConfigRemote(EntityInterface $entity): bool {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity */
    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    return $cloud_config->isRemote();
  }

  /**
   * Format the IpPermission object.
   *
   * Format the IpPermission object for use with
   * the AuthorizeSecurityGroup [Ingress and Egress] Amazon EC2 API call.
   *
   * @param \Drupal\aws_cloud\Plugin\Field\FieldType\IpPermission $ip_permission
   *   The IP permission object.
   *
   * @return array
   *   The permission.
   */
  private function formatIpPermissionForAuthorize(IpPermission $ip_permission): array {
    $permission = [
      'FromPort' => (int) $ip_permission->getFromPort(),
      'ToPort' => (int) $ip_permission->getToPort(),
      'IpProtocol' => $ip_permission->getIpProtocol(),
    ];
    if ($ip_permission->getSource() === 'ip4') {
      $item = [
        'CidrIp' => $ip_permission->getCidrIp(),
      ];
      if (!empty($ip_permission->getDescription())) {
        $item['Description'] = $ip_permission->getDescription();
      }
      $permission['IpRanges'][] = $item;
    }
    elseif ($ip_permission->getSource() === 'ip6') {
      $item = [
        'CidrIpv6' => $ip_permission->getCidrIpv6(),
      ];
      if (!empty($ip_permission->getDescription())) {
        $item['Description'] = $ip_permission->getDescription();
      }
      $permission['Ipv6Ranges'][] = $item;
    }
    elseif ($ip_permission->getSource() === 'prefix') {
      $item = [
        'PrefixListId' => $ip_permission->getPrefixListId(),
      ];
      if (!empty($ip_permission->getDescription())) {
        $item['Description'] = $ip_permission->getDescription();
      }
      $permission['PrefixListIds'][] = $item;
    }
    else {
      $group = [];
      // Use GroupID if non-default VPC or EC2-Classic.
      // For other permissions, use group name.
      if (!empty($ip_permission->getGroupId())) {
        $group['GroupId'] = $ip_permission->getGroupId();
      }
      else {
        $group['GroupName'] = $ip_permission->getGroupName();
      }

      $group['UserId'] = $ip_permission->getUserId();
      $group['PeeringStatus'] = $ip_permission->getPeeringStatus();
      $group['VpcId'] = $ip_permission->getVpcId();
      $group['VpcPeeringConnectionId'] = $ip_permission->getPeeringConnectionId();
      if (!empty($ip_permission->getDescription())) {
        $group['Description'] = $ip_permission->getDescription();
      }

      $permission['UserIdGroupPairs'][] = $group;
    }
    return $permission;
  }

  /**
   * Format IP Permissions.
   *
   * @param \Drupal\Core\Field\FieldItemList $ip_permissions
   *   The security group.
   *
   * @return array
   *   Formatted IP Permissions.
   */
  private function formatIpPermissionsForRevoke(FieldItemList $ip_permissions): array {
    $permissions_formatted = [];
    foreach ($ip_permissions ?: [] as $ip_permission) {
      if (empty($ip_permission->getSource())) {
        continue;
      }
      $permission_formatted = $this->formatIpPermissionForAuthorize($ip_permission);
      $permissions_formatted[] = $permission_formatted;
    }

    return $permissions_formatted;
  }

  /**
   * Revoke a set of permissions.
   *
   * @param string $method
   *   String value of revokeSecurityGroupEgress or revokeSecurityGroupIngress.
   * @param array $revoke_permissions
   *   Array of permissions to revoke.
   * @param string $group_id
   *   Security group id to revoke.
   */
  private function revokeIpPermissions($method, array $revoke_permissions, $group_id): void {
    if (!empty($revoke_permissions)) {
      // Delete the existing egress permissions.
      $this->ec2Service->$method([
        'GroupId' => $group_id,
        'IpPermissions' => $revoke_permissions,
      ]);
    }
  }

  /**
   * Helper method to format a permissions array for resubmission.
   *
   * This is used in case of any error during the permissions update process.
   *
   * @param \Drupal\Core\Field\FieldItemList $permissions
   *   IP Permissions.
   * @param string $group_id
   *   Group ID.
   *
   * @return array
   *   Formatted permissions array.
   */
  private function formatIpPermissionsForAdd(FieldItemList $permissions, $group_id): array {
    $existing_permissions['GroupId'] = $group_id;
    foreach ($permissions ?: [] as $permission) {
      if (empty($permission->getSource())) {
        continue;
      }
      $existing_permissions['IpPermissions'][] = $this->formatIpPermissionForAuthorize($permission);
    }
    return $existing_permissions;
  }

  /**
   * Main method for updating permissions.
   *
   * Supports Ingress and Egress permissions.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The AWS Cloud instance entity.
   * @param string $group_id
   *   Security group ID.
   * @param \Drupal\Core\Field\FieldItemList $existing_permissions
   *   Existing permissions.
   * @param string $permission_name
   *   The permission field name.
   * @param string $entity_func
   *   The entity function name to retrieve permissions.  Options are
   *   getIpPermission or getOutboundPermission.
   * @param string $revoke_func
   *   The API function name to revoke permissions.  Options are
   *   revokeSecurityGroupIngress or revokeSecurityGroupEgress.
   * @param string $authorize_func
   *   The API function name to authorize permissions.  Options are
   *   authorizeSecurityGroupIngress or authorizeSecurityGroupEgress.
   * @param bool $revoke
   *   Indicates skipping revoke operation.
   *
   * @return bool
   *   Indicates success or failure
   */
  private function updatePermissions(
    SecurityGroupInterface $entity,
    string $group_id,
    FieldItemList $existing_permissions,
    string $permission_name,
    string $entity_func,
    string $revoke_func,
    string $authorize_func,
    bool $revoke = TRUE,
  ) : bool {
    // Clear out the existing permissions.
    if (!empty($existing_permissions) && $revoke === TRUE) {
      $revoke_permissions = $this->formatIpPermissionsForRevoke($existing_permissions);
      $this->revokeIpPermissions($revoke_func, $revoke_permissions, $group_id);
    }

    // Format the user entered permissions into AWS accepted array.
    $permissions = $this->formatIpPermissions($entity->$entity_func());
    if (!empty($permissions[$permission_name])) {
      // Setup permissions array for AuthorizeSecurityGroupIngress.
      $permissions['GroupId'] = $group_id;
      $result = $this->ec2Service->$authorize_func($permissions);

      // $result === NULL means an exception was thrown.  Attempt to add
      // the old set of permissions so the user's configurations are not lost.
      if ($result === NULL || count($this->messenger->messagesByType('error')) >= 1) {
        if (!empty($existing_permissions)) {
          $existing_permissions = $this->formatIpPermissionsForAdd(
            $existing_permissions,
            $entity->getGroupId()
          );
          $result = $this->ec2Service->$authorize_func($existing_permissions);
          if ($result === NULL) {
            $this->messenger->addError($this->t('Cannot re-add existing @name rules', [
              '@name' => $permission_name,
            ]));
          }
        }
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Format the IP permission array for use with revoke security group.
   *
   * @param \Drupal\aws_cloud\Plugin\Field\FieldType\IpPermission $ip_permission
   *   IP Permission object.
   *
   * @return array
   *   Permissions array.
   */
  private function formatIpPermissionForRevoke(IpPermission $ip_permission): array {
    $permission = [
      'FromPort' => (int) $ip_permission->getFromPort(),
      'ToPort' => (int) $ip_permission->getToPort(),
      'IpProtocol' => $ip_permission->getIpProtocol(),
    ];
    if ($ip_permission->getSource() === 'ip4') {
      $permission['IpRanges'][]['CidrIp'] = $ip_permission->getCidrIp();
    }
    else {
      if ($ip_permission->getSource() === 'ip6') {
        $permission['Ipv6Ranges'][]['CidrIpv6'] = $ip_permission->getCidrIpv6();
      }
      else {
        // Use GroupID if non-default VPC or EC2-Classic.
        // For other permissions, use group name.
        /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
        $entity = $this->entity;
        $vpc_id = $entity->getVpcId();
        if ($entity->isDefaultVpc() === TRUE || !isset($vpc_id)) {
          $group['GroupName'] = $ip_permission->getGroupName();
        }
        else {
          $group['GroupId'] = $ip_permission->getGroupId();
        }
        $group['UserId'] = $ip_permission->getUserId();
        $group['PeeringStatus'] = $ip_permission->getPeeringStatus();
        $group['VpcId'] = $ip_permission->getVpcId();
        $group['VpcPeeringConnectionId'] = $ip_permission->getPeeringConnectionId();

        $permission['UserIdGroupPairs'][] = $group;
      }
    }
    return $permission;
  }

  /**
   * Verify that the revoke was successful.
   *
   * Since EC2 does not return any error codes in the revoke API calls, the
   * only way to verify is to count the permissions array from the current
   * entity, and the entity that is newly updated from the updateSecurityGroups
   * API call.
   *
   * @param string $type
   *   The permission type.
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $group
   *   The security group.
   *
   * @return bool
   *   True or false.
   */
  private function validateRevoke($type, SecurityGroupInterface $group): bool {
    $verified = FALSE;
    /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroup $updated_group */
    $updated_group = $this->entityTypeManager
      ->getStorage($group->getEntityTypeId())
      ->load($group->id());
    if ($type === 'outbound_permission') {
      if ($group->getIpPermission()->count() === $updated_group->getIpPermission()->count()) {
        $verified = TRUE;
      }
    }
    else {
      if ($group->getOutboundPermission()->count() === $updated_group->getOutboundPermission()->count()) {
        $verified = TRUE;
      }
    }
    return $verified;
  }

  /**
   * Get a network interface given a private_ip address.
   *
   * @param string $private_ip
   *   The private IP used to look up the network interface.
   * @param string $module_name
   *   Module name.
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return \Drupal\aws_cloud\Entity\Ec2\NetworkInterfaceInterface|null
   *   False or network interface object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getNetworkInterfaceByPrivateIp($private_ip, $module_name, $cloud_context) {
    $results = $this->entityTypeManager
      ->getStorage("{$module_name}_network_interface")
      ->loadByProperties([
        'primary_private_ip' => $private_ip,
        'cloud_context' => $cloud_context,
      ]);

    return count($results) === 1
      ? array_shift($results)
      : NULL;
  }

  /**
   * Helper function to update the current aws_cloud_elastic_ip entity.
   *
   * @param string $message
   *   Message to display to use.
   */
  private function updateElasticIpEntity($message): void {
    $this->messenger->addStatus($message);

    // Update the following entities from EC2.
    $this->ec2Service->updateElasticIps();
    $this->ec2Service->updateInstances();
    $this->ec2Service->updateNetworkInterfaces();
  }

  /**
   * Get a snapshot name.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $snapshot_id
   *   The snapshot ID.
   * @param string $module_name
   *   Module name.
   *
   * @return string
   *   The snapshot name.
   */
  private function getSnapshotName($cloud_context, $snapshot_id, $module_name): string {
    $snapshot_name = '';
    if (empty($snapshot_id)) {
      return $snapshot_name;
    }

    /** @var \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface[] $snapshots */
    $snapshots = $this->entityTypeManager
      ->getStorage("{$module_name}_snapshot")
      ->loadByProperties([
        'cloud_context' => $cloud_context,
        'snapshot_id' => $snapshot_id,
      ]);

    if (empty($snapshots)) {
      return $snapshot_name;
    }

    return array_shift($snapshots)->getName();
  }

  /**
   * Check Whether the volume changed or not.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $new_volume
   *   The new volume.
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $old_volume
   *   The old volume.
   *
   * @return bool
   *   Whether the volume changed or not.
   */
  private function isVolumeChanged(VolumeInterface $new_volume, VolumeInterface $old_volume): bool {
    return $old_volume->getVolumeType() !== $new_volume->getVolumeType()
      || $old_volume->getSize() !== $new_volume->getSize()
      || $old_volume->getIops() !== $new_volume->getIops();
  }

  /**
   * Common alter function for edit and add forms.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  private function alterCloudLaunchTemplateCommonForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state) {
    $this->cloudService->reorderForm($form, aws_cloud_launch_template_field_orders());

    $form['instance']['field_iam_role']['widget']['#options']['_none'] = $this->t('No Role');

    $form['instance']['field_instance_type']['widget']['#ajax'] = [
      'callback' => 'aws_cloud_ajax_callback_get_image_ids',
    ];

    $form['ami']['field_image_id']['widget']['#ajax'] = [
      'callback' => 'aws_cloud_ajax_callback_get_instance_types',
    ];

    $form['network']['field_vpc']['widget']['#ajax'] = [
      'callback' => 'aws_cloud_ajax_callback_get_fields',
    ];

    $vpc_id = '_none';
    if (!empty($form['network']['field_vpc']['widget']['#default_value'])) {
      $vpc_id = $form['network']['field_vpc']['widget']['#default_value'][0];
    }

    // If validation happened, we should get vpc_id from user input.
    if ($entity->hasField('field_vpc')) {
      $vpc_id = $entity->get('field_vpc')->value;
    }

    /** @var \Drupal\Core\Entity\EntityFormInterface $form_object */
    $subnet_options = aws_cloud_get_subnet_options_by_vpc_id($vpc_id, $entity);
    $form['#attached']['library'][] = 'aws_cloud/aws_cloud_form';
    $form['#attached']['drupalSettings']['aws_cloud']['field_subnet_default_values']
      = array_keys($subnet_options);

    $security_group_options = aws_cloud_get_security_group_options_by_vpc_id($vpc_id, $entity);
    $security_group_default_values = [];
    foreach ($security_group_options ?: [] as $id => $security_group_option) {
      $security_group_default_values[] = (string) $id;
    }

    $form['#attached']['drupalSettings']['aws_cloud']['field_security_group_default_values']
      = $security_group_default_values;

    $config = $this->configFactory->get('aws_cloud.settings');
    $form['#attached']['drupalSettings']['aws_cloud']['aws_cloud_instance_type_cost']
      = $config->get('aws_cloud_instance_type_cost');

    // Hide labels of field_tags.
    $form['tags']['field_tags']['widget']['#title'] = NULL;
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  private function alterCloudLaunchTemplateCopyForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state) {
    $this->alterCloudLaunchTemplateCommonForm($entity, $form, $form_state);

    // Change name for copy.
    $name = $form['instance']['name']['widget'][0]['value']['#default_value'];
    $form['instance']['name']['widget'][0]['value']['#default_value'] = $this->t('copy_of_@name',
      [
        '@name' => $name,
      ]);

    // Hide new revision checkbox.
    $form['new_revision']['#access'] = FALSE;

    // Clear the revision log message.
    $form['others']['revision_log_message']['widget'][0]['value']['#default_value'] = NULL;

    // Change value of the submit button.
    $form['actions']['submit']['#value'] = $this->t('Copy');

    // Delete the delete button.
    $form['actions']['delete']['#access'] = FALSE;

    // Overwrite function ::save.
    $form['actions']['submit']['#submit'][1] = 'aws_cloud_form_cloud_launch_template_aws_cloud_copy_form_submit';
  }

  /**
   * Submit function for form cloud_launch_template_aws_cloud_copy_form.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  private function submitCloudLaunchTemplateCopyForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $server_template = $entity->createDuplicate();
    /** @var \Drupal\Core\Entity\EntityFormInterface $form_object */
    $form_object = $form_state->getFormObject();
    $form_object->setEntity($server_template);

    return $this->submitCloudLaunchTemplateAddForm($entity, $form, $form_state);
  }

  /**
   * Submit function for form cloud_launch_template_aws_cloud_copy_form.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  private function submitCloudLaunchTemplateAddForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $ec2_service = $this->ec2Service;
    $cloud_context = $entity->getCloudContext();
    $ec2_service->setCloudContext($cloud_context);

    $params = [];
    $params['LaunchTemplateName'] = $entity->getName();
    $params['VersionDescription'] = $entity->getRevisionLogMessage();
    $params['LaunchTemplateData'] = aws_cloud_get_launch_template_data($entity);

    $result = $ec2_service->createLaunchTemplate($params);
    $success = isset($result['LaunchTemplate']);
    if ($success) {
      aws_cloud_launch_template_update_field_tags($entity, $params['LaunchTemplateData']);
      $entity->set('field_version', $result['LaunchTemplate']['LatestVersionNumber']);
    }

    if ($success && $entity->save()
    ) {

      aws_cloud_dispatch_event(new CloudEntityEvent($entity, CloudEntityEventType::FORM_SUBMIT));

      $this->cloudService->processOperationStatus($entity, 'created');

      $form_state->setRedirect(
        'entity.cloud_launch_template.canonical',
        [
          'cloud_context' => $cloud_context,
          'cloud_launch_template' => $entity->id(),
        ]
      );
      return TRUE;
    }

    $this->cloudService->processOperationErrorStatus($entity, 'created');
    return FALSE;
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  private function alterCloudLaunchTemplateEditForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state) {
    $this->alterCloudLaunchTemplateCommonForm($entity, $form, $form_state);

    // Hide new revision checkbox.
    $form['new_revision']['#access'] = FALSE;

    // Disable name field.
    $form['instance']['name']['#disabled'] = TRUE;

    // Overwrite function ::save.
    $form['actions']['submit']['#submit'][1] = 'aws_cloud_form_cloud_launch_template_aws_cloud_edit_form_submit';
  }

  /**
   * Submit function for form cloud_launch_template_aws_cloud_edit_form.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  private function submitCloudLaunchTemplateEditForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {

    /** @var \Drupal\cloud\Entity\CloudLaunchTemplateInterface $revision */
    $revision = $this->cloudLaunchTemplateStorage->loadRevision($entity->getRevisionId());

    if (!empty($entity)
    && !empty($entity->get('field_workflow_status'))
    && !empty($revision)
    && !empty($revision->get('field_workflow_status'))
    && $revision->get('field_workflow_status')->value === CloudLaunchTemplateInterface::APPROVED
    && aws_cloud_launch_template_edit_is_changed($entity, $revision)) {
      $entity->get('field_workflow_status')->setValue(CloudLaunchTemplateInterface::DRAFT);
      $entity->validate();
    }

    if (!empty($entity)
    && !empty($entity->get('field_workflow_status'))
    && !empty($revision)
    && !empty($revision->get('field_workflow_status'))) {
      $this->cloudService->notifyLaunchTemplateWorkflowStatusChanged('aws_cloud', $entity, $revision->get('field_workflow_status')->value);
    }

    $ec2_service = $this->ec2Service;
    $cloud_context = $entity->getCloudContext();
    $ec2_service->setCloudContext($cloud_context);

    // Always create a new revision.
    $entity->setRevisionUserId($this->currentUser->id());
    $entity->setNewRevision(TRUE);

    $params = [];
    $params['LaunchTemplateName'] = $entity->getName();
    $params['VersionDescription'] = $entity->getRevisionLogMessage();
    $params['LaunchTemplateData'] = aws_cloud_get_launch_template_data($entity);
    $result = $ec2_service->createLaunchTemplateVersion($params);

    $success = isset($result['LaunchTemplateVersion']);
    if ($success) {
      aws_cloud_launch_template_update_field_tags($entity, $params['LaunchTemplateData']);

      $version = $result['LaunchTemplateVersion']['VersionNumber'];
      $entity->set('field_version', $version);

      // Set default version.
      $params = [];
      $params['LaunchTemplateName'] = $entity->getName();
      $params['DefaultVersion'] = $version;
      // Set the revision creation time using `CreateTime` from results.
      $entity->setRevisionCreationTime(strtotime($result['LaunchTemplateVersion']['CreateTime']));

      $ec2_service->modifyLaunchTemplate($params);
    }

    if ($success && $entity->save()) {
      aws_cloud_dispatch_event(new CloudEntityEvent($entity, CloudEntityEventType::FORM_SUBMIT));
      $this->cloudService->processOperationStatus($entity, 'updated');

      $form_state->setRedirect(
        'entity.cloud_launch_template.canonical',
        [
          'cloud_context' => $cloud_context,
          'cloud_launch_template' => $entity->id(),
        ]
      );
      return TRUE;
    }

    $this->cloudService->processOperationErrorStatus($entity, 'updated');
    return FALSE;
  }

}
