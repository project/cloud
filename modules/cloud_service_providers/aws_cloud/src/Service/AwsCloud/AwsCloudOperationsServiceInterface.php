<?php

namespace Drupal\aws_cloud\Service\AwsCloud;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface;
use Drupal\aws_cloud\Entity\Ec2\ImageInterface;
use Drupal\aws_cloud\Entity\Ec2\InstanceInterface;
use Drupal\aws_cloud\Entity\Ec2\KeyPairInterface;
use Drupal\aws_cloud\Entity\Ec2\NetworkInterface;
use Drupal\aws_cloud\Entity\Ec2\NetworkInterfaceInterface;
use Drupal\aws_cloud\Entity\Ec2\SecurityGroup;
use Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface;
use Drupal\aws_cloud\Entity\Ec2\SnapshotInterface;
use Drupal\aws_cloud\Entity\Ec2\VolumeInterface;
use Drupal\aws_cloud\Entity\Vpc\CarrierGatewayInterface;
use Drupal\aws_cloud\Entity\Vpc\InternetGatewayInterface;
use Drupal\aws_cloud\Entity\Vpc\SubnetInterface;
use Drupal\aws_cloud\Entity\Vpc\TransitGatewayInterface;
use Drupal\aws_cloud\Entity\Vpc\VpcInterface;
use Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnectionInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;

/**
 * Provides AwsCloudOperationsService interface.
 */
interface AwsCloudOperationsServiceInterface {

  /**
   * Set the EC2 Service.
   *
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface|\Drupal\openstack\Service\OpenStackServiceInterface $ec2_service
   *   The EC2 Service.
   */
  public function setEc2Service(mixed $ec2_service): void;

  /**
   * Copy values from #type=item elements to its original element type.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Interface for entities having fields.
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  public function copyFormItemValues(FieldableEntityInterface $entity, array $form): void;

  /**
   * Trim white spaces in the values of textfields.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Interface for entities having fields.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function trimTextfields(FieldableEntityInterface $entity, array $form, FormStateInterface $form_state): void;

  /**
   * Save an AWS Cloud content.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Interface for entities having fields.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function saveAwsCloudContent(FieldableEntityInterface $entity, array $form, FormStateInterface $form_state): void;

  /**
   * Set the tags for the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Interface for entities having fields.
   * @param string $resource_id
   *   The resource ID.  For example, instance_id, volume_id.
   * @param array $tag_map
   *   The map of tags.
   * @param string $entity_type
   *   The type of entity.
   */
  public function setTagsInAws(EntityInterface $entity, string $resource_id, array $tag_map, string $entity_type = ''): void;

  /**
   * Update name and created_by tags.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Interface for entities having fields.
   * @param string $aws_cloud_resource_id
   *   The AWS Cloud resource ID.
   */
  public function updateNameAndCreatedByTags(EntityInterface $entity, string $aws_cloud_resource_id): void;

  /**
   * Verify the authorization call was successful.
   *
   * Since EC2 does not return any error codes from any of the authorize
   * API calls, the only way to verify is to count the permissions array
   * from the current entity,  and the entity that is newly updated from
   * the updateSecurityGroups API call.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $group
   *   The security group.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function validateAuthorize(SecurityGroupInterface $group): void;

  /**
   * Take the ip_permissions field type and format it for AWS API call.
   *
   * @param \Drupal\Core\Field\FieldItemList $permissions
   *   Array of IpPermission fields.
   *
   * @return array
   *   Array of formatted permissions for AWS.
   */
  public function formatIpPermissions(FieldItemList $permissions): array;

  /**
   * Helper method to update both ingress and egress permissions.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $original_entity
   *   The original security group entity.
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroup $entity
   *   Security group entity.
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroup $existing_group
   *   The existing security group entity.
   * @param bool $revokeIngress
   *   Flag indicating whether to skip revoking ingress permissions.
   * @param bool $revokeEgress
   *   Flag indicating whether to skip revoking egress permissions.
   */
  public function updateIngressEgressPermissions(SecurityGroupInterface $original_entity, SecurityGroup $entity, SecurityGroup $existing_group, bool $revokeIngress = TRUE, bool $revokeEgress = TRUE): void;

  /**
   * Get an individual permission from outbound or inbound permission.
   *
   * @param string $type
   *   Outbound_permission or ip_permission.
   * @param int $position
   *   Position in the multivalued list of permissions.
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   Security group entity.
   *
   * @return \Drupal\aws_cloud\Plugin\Field\FieldType\IpPermission|false
   *   FALSE or permission object.
   */
  public function getPermission(string $type, int $position, SecurityGroupInterface $entity);

  /**
   * Helper method to load instance by ID.
   *
   * @param string $instance_id
   *   Instance ID to load.
   * @param string $module_name
   *   Module name.
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return \Drupal\aws_cloud\Entity\Ec2\Instance
   *   The instance entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getInstanceById($instance_id, $module_name, $cloud_context): ?InstanceInterface;

  /**
   * Helper method to load network interface by ID.
   *
   * @param string $network_interface_id
   *   Network interface ID to load.
   * @param string $module_name
   *   Module name.
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return \Drupal\aws_cloud\Entity\Ec2\NetworkInterface
   *   The NetworkInterface entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNetworkInterfaceById($network_interface_id, $module_name, $cloud_context): NetworkInterface;

  /**
   * Helper function that loads all the private IPs for an instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The OpenStack floating IP entity.
   * @param int $instance_id
   *   The instance ID.
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return array
   *   An array of IP addresses.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPrivateIps(ElasticIpInterface $entity, $instance_id, $cloud_context): array;

  /**
   * Helper function to load primary and secondary private IPs.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The OpenStack floating IP entity.
   * @param int $network_interface_id
   *   The network interface ID.
   *
   * @return array
   *   An array of IP addresses.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNetworkPrivateIps(ElasticIpInterface $entity, $network_interface_id): array;

  /**
   * Associate an Elastic IP.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The Elastic IP entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function associateElasticIp(ElasticIpInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Associate an Elastic IP for AWS Cloud instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function associateElasticIpForInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Get OpenStack's Provider ID by Cloud context.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return string
   *   Provider ID of OpenStack associated with cloud context
   */
  public function getOpenStackProjectIdByCloudContext(string $cloud_context): string;

  /**
   * Create an image.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity
   *   The image entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createImage(ImageInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Create image from AWS Cloud instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createImageFromInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Create carrier gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\CarrierGatewayInterface $entity
   *   CarrierGateway entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createCarrierGateway(CarrierGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create an Elastic IP.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The Elastic IP entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createElasticIp(ElasticIpInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create internet gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\InternetGatewayInterface $entity
   *   InternetGateway entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createInternetGateway(InternetGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create a NetworkInterface.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\NetworkInterfaceInterface $entity
   *   NetworkInterface entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createNetworkInterface(NetworkInterfaceInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create transit gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\TransitGatewayInterface $entity
   *   TransitGateway entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createTransitGateway(TransitGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create Vpc.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\VpcInterface $entity
   *   VPC entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createVpc(VpcInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit Vpc.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\VpcInterface $entity
   *   VPC entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function editVpc(VpcInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create Vpc peering connection.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnectionInterface $entity
   *   Vpc peering connection entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createVpcPeeringConnection(VpcPeeringConnectionInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit Vpc peering connection.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnectionInterface $entity
   *   Vpc peering connection entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function editVpcPeeringConnection(VpcPeeringConnectionInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create subnet.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\SubnetInterface $entity
   *   Subnet entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createSubnet(SubnetInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit subnet.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\SubnetInterface $entity
   *   Subnet entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function editSubnet(SubnetInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create a KeyPair.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity
   *   The key pair entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createKeyPair(KeyPairInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create a SecurityGroup.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The SecurityGroup entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createSecurityGroup(SecurityGroupInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Create a snapshot.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity
   *   The SecurityGroup entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createSnapshot(SnapshotInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Create a Volume.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The SecurityGroup entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function createVolume(VolumeInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Delete a carrier gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\CarrierGatewayInterface $entity
   *   CarrierGateway entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteCarrierGateway(CarrierGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an Elastic IP.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The Elastic IP entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteElasticIp(ElasticIpInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete Elastic IP.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The Elastic IP entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteElasticIpCloudResource(ElasticIpInterface $entity): bool;

  /**
   * Delete an image.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity
   *   The image entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteImage(ImageInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete images.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity
   *   The image entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteImageCloudResource(ImageInterface $entity): bool;

  /**
   * Delete an instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack instances.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteInstancesImpl(InstanceInterface $entity): bool;

  /**
   * Delete an internet gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\InternetGatewayInterface $entity
   *   InternetGateway entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteInternetGateway(InternetGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete a KeyPair.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity
   *   The key pair entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteKeyPair(KeyPairInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Delete a NetworkInterface.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\NetworkInterfaceInterface $entity
   *   NetworkInterface entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteNetworkInterface(NetworkInterfaceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Delete a snapshot.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity
   *   The snapshot entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteSnapshot(SnapshotInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete snapshots.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity
   *   The snapshot entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteSnapshotCloudResource(SnapshotInterface $entity): bool;

  /**
   * Delete a Subnet.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\SubnetInterface $entity
   *   The SecurityGroup entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteSubnet(SubnetInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Delete a transit gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\TransitGatewayInterface $entity
   *   TransitGateway entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteTransitGateway(TransitGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete a Vpc.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\VpcInterface $entity
   *   The SecurityGroup entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteVpc(VpcInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Delete a Vpc.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnectionInterface $entity
   *   The SecurityGroup entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteVpcPeeringConnection(VpcPeeringConnectionInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Delete a Volume.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The Volume entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function deleteVolume(VolumeInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete images.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The Volume entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteVolumeCloudResource(VolumeInterface $entity): bool;

  /**
   * Disassociate an Elastic IP.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The Elastic IP entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function disassociateElasticIp(ElasticIpInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Delete a SecurityGroup.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The security group entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteSecurityGroup(SecurityGroupInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete security group.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The security group entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteSecurityGroupCloudResource(SecurityGroupInterface $entity): bool;

  /**
   * Edit an Elastic IP.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The Elastic IP entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editElasticIp(ElasticIpInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Edit an image.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity
   *   The image entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function editImage(ImageInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Edit an instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function editInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Save a Key Pair.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity
   *   The key pair entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function editKeyPair(KeyPairInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Save a network interface.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\NetworkInterfaceInterface $entity
   *   The network interface entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editNetworkInterface(NetworkInterfaceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Edit a security group.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The security group entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function editSecurityGroup(SecurityGroupInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Edit a snapshot.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity
   *   The SecurityGroup entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function editSnapshot(SnapshotInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Edit a Volume.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The SecurityGroup entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function editVolume(VolumeInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Edit carrier gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\CarrierGatewayInterface $entity
   *   CarrierGateway entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function editCarrierGateway(CarrierGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit internet gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\InternetGatewayInterface $entity
   *   CarrierGateway entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function editInternetGateway(InternetGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit transit gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\TransitGatewayInterface $entity
   *   Transit gateway entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Boolean status.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function editTransitGateway(TransitGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Get console output from AWS Cloud instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The AWS Cloud instance entity.
   *
   * @return string
   *   Console output.
   */
  public function getConsoleOutput(InstanceInterface $entity): string;

  /**
   * Import a KeyPair.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity
   *   The key pair entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function importKeyPair(KeyPairInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Launch an instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function launchInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Reboot an instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function rebootInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to reboot instances.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function rebootInstanceCloudResource(InstanceInterface $entity): bool;

  /**
   * Revoke a Rest SecurityGroup.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The SecurityGroup entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $type
   *   Outbound_permission or ip_permission.
   * @param string $position
   *   Position in the multivalued list of permissions.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revokeSecurityGroup(SecurityGroupInterface $entity, array $form, FormStateInterface $form_state, string $type, string $position): bool;

  /**
   * Start an instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function startInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to start instances.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function startInstanceCloudResource(InstanceInterface $entity): bool;

  /**
   * Stop an instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function stopInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to stop instances.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function stopInstanceCloudResource(InstanceInterface $entity): bool;

  /**
   * Attach a Volume.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function attachVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Attach an Internet gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\InternetGatewayInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function attachInternetGateway(InternetGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Detach a volume.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function detachVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to detach an OpenStack volumes.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The OpenStack volume entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function detachVolumeCloudResource(VolumeInterface $entity): bool;

  /**
   * Detach an internet gateway.
   *
   * @param \Drupal\aws_cloud\Entity\Vpc\InternetGatewayInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function detachInternetGateway(InternetGatewayInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create CloudLaunchTemplate.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Copy CloudLaunchTemplate.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function copyCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit CloudLaunchTemplate.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Review CloudLaunchTemplate.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function reviewCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool;

}
