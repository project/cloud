<?php

namespace Drupal\aws_cloud\Service;

/**
 * Aws Service interface.
 */
interface AwsServiceInterface {

  /**
   * Resource tag prefix for AWS.
   *
   * @var string
   *
   * @see https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-resource-tags.html
   */
  public const AWS_RESOURCE_TAG_PREFIX = 'aws:';

}
