<?php

namespace Drupal\aws_cloud\Service\CloudFormation;

use Drupal\aws_cloud\Entity\CloudFormation\Stack;
use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * Entity update methods for Batch API processing.
 */
class CloudFormationBatchOperations {

  use CloudContentEntityTrait;

  /**
   * The finish callback function.
   *
   * Deletes stale entities from the database.
   *
   * @param string $entity_type
   *   The entity type.
   * @param array $stale
   *   The stale entities to delete.
   * @param bool $clear
   *   TRUE to clear entities, FALSE keep them.
   */
  public static function finished($entity_type, array $stale, $clear = TRUE): void {
    $entity_type_manager = \Drupal::entityTypeManager();
    if (count($stale) && $clear === TRUE) {
      $entity_type_manager->getStorage($entity_type)->delete($stale);
    }
  }

  /**
   * Update or create a Stack entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $stack
   *   The stack array.
   */
  public static function updateStack(string $cloud_context, array $stack): void {
    /** @var \Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceInterface */
    $cloudFormationService = \Drupal::service('aws_cloud.cloud_formation');
    $cloudFormationService->setCloudContext($cloud_context);

    $stack_name = $stack['StackName'] ?? NULL;

    $timestamp = time();
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'aws_cloud_stack', 'stack_name', $stack_name);
    $uid = $cloudFormationService->getUidTagValue($stack, 'aws_cloud_stack');

    if (!empty($entity_id)) {
      $entity = Stack::load($entity_id);
      if (!empty($entity)) {
        $entity->setRefreshed($timestamp);
        $entity->setOwnerById($uid > 0 ? $uid : 0);
      }
    }
    else {
      $entity = Stack::create([
        'cloud_context' => $cloud_context,
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
    }

    $entity->setStackName($stack['StackName'] ?? NULL);
    $entity->setStackId($stack['StackId'] ?? NULL);
    $entity->setDescription($stack['Description'] ?? NULL);
    $entity->setStackStatus($stack['StackStatus'] ?? NULL);
    $entity->setStackStatusReason($stack['StackStatusReason'] ?? NULL);
    $entity->setDisableRollback($stack['DisableRollback'] ?? FALSE);
    $entity->setParameters(array_map(fn($item) => [
      'item_key' => $item['ParameterKey'] ?? '',
      'item_value' => $item['ParameterValue'] ?? '',
    ], $stack['Parameters'] ?? []));

    $entity->setOutputs(array_map(fn($item) => [
      'item_key' => $item['OutputKey'] ?? '',
      'item_value' => $item['OutputValue'] ?? '',
    ], $stack['Outputs'] ?? []));

    $entity->save();
  }

}
