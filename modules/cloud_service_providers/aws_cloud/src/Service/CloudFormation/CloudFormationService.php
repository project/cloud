<?php

namespace Drupal\aws_cloud\Service\CloudFormation;

use Aws\CloudFormation\CloudFormationClient;
use Aws\CloudFormation\Exception\CloudFormationException;
use Aws\Credentials\AssumeRoleCredentialProvider;
use Aws\Credentials\CredentialProvider;
use Aws\MockHandler;
use Aws\Result;
use Aws\ResultInterface;
use Aws\Sts\StsClient;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceBase;
use Drupal\cloud\Service\CloudServiceInterface;

/**
 * Interacts with the AWS Cloud CloudFormation API.
 */
class CloudFormationService extends CloudServiceBase implements CloudFormationServiceInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * TRUE or FALSE whether to be in test mode.
   *
   * @var bool
   */
  private $testMode;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The lock interface.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new CloudFormationService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock interface.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    LockBackendInterface $lock,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    CloudServiceInterface $cloud_service,
    ModuleHandlerInterface $module_handler,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    // Set up the entity type manager for querying entities.
    $this->entityTypeManager = $entity_type_manager;

    // Set up the configuration factory.
    $this->configFactory = $config_factory;

    $this->lock = $lock;

    // Set up the testMode flag.
    $this->testMode = (bool) $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_test_mode');

    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;

    $this->cloudService = $cloud_service;

    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context): void {
    $this->cloudContext = $cloud_context;
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function createStack(array $params = []): ?ResultInterface {
    $results = $this->execute('CreateStack', $params);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function describeStacks(array $params = []): ?ResultInterface {
    $results = $this->execute('DescribeStacks', $params);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteStack(array $params = []): ?ResultInterface {
    $results = $this->execute('DeleteStack', $params);
    return $results;
  }

  /**
   * Execute the API of AWS Cloud CloudFormation service.
   *
   * @param string $operation
   *   The operation to perform.
   * @param array $params
   *   An array of parameters.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceException
   *   If the $cloud_formation_client (CloudFormationClient) is NULL.
   */
  private function execute(string $operation, array $params = []): ?ResultInterface {
    $results = NULL;

    $cloud_formation_client = $this->getCloudFormationClient();
    if ($cloud_formation_client === NULL) {
      throw new CloudFormationServiceException('No CloudFormation Client found. Cannot perform API operations');
    }

    try {
      // Let other modules alter the parameters
      // before they are sent through the API.
      $this->moduleHandler->invokeAll('aws_cloud_pre_execute_alter', [
        &$params,
        $operation,
        $this->cloudContext,
      ]);

      $command = $cloud_formation_client->getCommand($operation, $params);
      $results = $cloud_formation_client->execute($command);

      // Let other modules alter the results before the module processes it.
      $this->moduleHandler->invokeAll('aws_cloud_post_execute_alter', [
        &$results,
        $operation,
        $this->cloudContext,
      ]);
    }
    catch (CloudFormationException $e) {
      // IAM permission validation needs AwsException to be thrown as it is
      // determined by the AwsErrorCode of the exception, 'DryRunOperation'.
      if (!empty($params['DryRun'])) {
        throw $e;
      }

      $this->messenger->addError($this->t('Error: The operation "@operation" could not be performed.', [
        '@operation' => $operation,
      ]));

      $this->messenger->addError($this->t('Error Info: @error_info', [
        '@error_info' => $e->getAwsErrorCode(),
      ]));

      $this->messenger->addError($this->t('Error from: @error_type-side', [
        '@error_type' => $e->getAwsErrorType(),
      ]));

      $this->messenger->addError($this->t('Status Code: @status_code', [
        '@status_code' => $e->getStatusCode(),
      ]));

      $this->messenger->addError($this->t('Message: @msg', ['@msg' => $e->getAwsErrorMessage()]));
    }
    catch (\Exception $e) {
      $this->handleException($e);

      throw $e;
    }
    return $results;
  }

  /**
   * Load and return a CloudFormationClient.
   */
  private function getCloudFormationClient(): ?CloudFormationClient {
    // Use the plugin manager to load the aws credentials.
    $credentials = $this->cloudConfigPluginManager->loadCredentials();

    try {
      $cloud_formation_params = [
        'region' => $credentials['region'],
        'version' => $credentials['version'],
        'http' => [
          'connect_timeout' => $this->connectTimeout,
        ],
      ];
      $provider = FALSE;

      // Load credentials if needed.
      if (empty($credentials['use_instance_profile'])) {
        $provider = CredentialProvider::ini('default', $credentials['ini_file']);
        $provider = CredentialProvider::memoize($provider);
      }

      if (!empty($credentials['use_assume_role'])) {
        // Assume role.
        $sts_params = [
          'region' => $credentials['region'],
          'version' => $credentials['version'],
        ];
        if ($provider !== FALSE) {
          $sts_params['credentials'] = $provider;
        }
        $assumeRoleCredentials = new AssumeRoleCredentialProvider([
          'client' => new StsClient($sts_params),
          'assume_role_params' => [
            'RoleArn' => $credentials['role_arn'],
            'RoleSessionName' => 'cloud_formation_client_assume_role',
          ],
        ]);

        // Memoize takes care of re-authenticating when the tokens expire.
        $assumeRoleCredentials = CredentialProvider::memoize($assumeRoleCredentials);
        $cloud_formation_params = [
          'region' => $credentials['region'],
          'version' => $credentials['version'],
          'credentials' => $assumeRoleCredentials,
        ];
        // If switch role is enabled, execute one more assume role.
        if (!empty($credentials['use_switch_role'])) {
          $switch_sts_params = [
            'region' => $credentials['region'],
            'version' => $credentials['version'],
            'credentials' => $assumeRoleCredentials,
          ];
          $switchRoleCredentials = new AssumeRoleCredentialProvider([
            'client' => new StsClient($switch_sts_params),
            'assume_role_params' => [
              'RoleArn' => $credentials['switch_role_arn'],
              'RoleSessionName' => 'cloud_formation_client_switch_role',
            ],
          ]);
          $switchRoleCredentials = CredentialProvider::memoize($switchRoleCredentials);
          $cloud_formation_params['credentials'] = $switchRoleCredentials;
        }
      }
      elseif ($provider !== FALSE) {
        $cloud_formation_params['credentials'] = $provider;
      }

      $cloud_formation_client = new CloudFormationClient($cloud_formation_params);
    }
    catch (\Exception $e) {
      $cloud_formation_client = NULL;
      $this->logger('cloud_formation_service')->error($e->getMessage());
    }
    if ($this->testMode) {
      $this->addMockHandler($cloud_formation_client);
    }
    return $cloud_formation_client;
  }

  /**
   * Add a mock handler of aws sdk for testing.
   *
   * The mock data of aws response is saved
   * in configuration "aws_cloud_mock_data".
   *
   * @param \Aws\CloudFormation\CloudFormationClient $cloud_formation_client
   *   The CloudFormation client.
   *
   * @see https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_handlers-and-middleware.html
   */
  private function addMockHandler(CloudFormationClient $cloud_formation_client): void {
    $mock_data = $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_mock_data');
    if (empty($this->testMode) || empty($mock_data)) {
      return;
    }

    $mock_data = json_decode($mock_data, TRUE);
    $result = static function ($command, $request) use ($mock_data) {

      $command_name = $command->getName();
      $response_data = $mock_data[$command_name] ?? [];

      // ErrorCode field is proprietary defined to mock AwsErrorCode.
      // Checking the value of DryRun parameter as some test cases expect a
      // different result based on the DryRun parameter.
      if (!empty($response_data['ErrorCode'])
        && $command->hasParam('DryRun')
        && ($command->toArray())['DryRun']) {
        return new CloudFormationException('CloudFormationException by CloudFormationService::addMockHandler()',
          $command, ['code' => $response_data['ErrorCode']]);
      }

      return new Result($response_data);
    };

    // Set a mock handler with the number of mocked results in the queue.
    // The mock queue count should be the number of API calls and is similar
    // to the prepared mocked results. TO be safe, an additional queue is set
    // for an async call or a repeated call.
    $results_on_queue = array_pad([], count($mock_data) + 1, $result);
    $cloud_formation_client
      ->getHandlerList()
      ->setHandler(new MockHandler($results_on_queue));
  }

  /**
   * Call the API to update Stack entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateStackEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = ''): bool {
    $result = $this->describeStacks($params);

    if (empty($result)) {
      return FALSE;
    }

    $all_stacks = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
    $stale = [];
    // Make it easier to look up the stack by setting up
    // the array with the stack_id.
    foreach ($all_stacks ?: [] as $stack) {
      $stale[$stack->getStackId()] = $stack;
    }
    /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
    $batch_builder = $this->initBatch('Stack Update');

    foreach ($result['Stacks'] ?: [] as $stack) {
      if (!empty($stale[$stack['StackId']])) {
        unset($stale[$stack['StackId']]);
      }

      $batch_builder->addOperation([
        CloudFormationBatchOperations::class,
        'updateStack',
      ], [$this->cloudContext, $stack]);
    }

    $batch_builder->addOperation([
      CloudFormationBatchOperations::class,
      'finished',
    ], [$entity_type, $stale, $clear]);
    $this->runBatch($batch_builder);

    return TRUE;
  }

  /**
   * Update the stacks.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateStacks(array $params = [], $clear = TRUE): bool {
    $entity_type = 'aws_cloud_stack';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateStackEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateStacksWithoutBatch(array $params = [], $clear = TRUE, $to_update = TRUE): bool {
    $updated = FALSE;
    $entity_type = 'aws_cloud_stack';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = empty($params['StackName']) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'stack_name' => $params['StackName'],
    ];
    $all_stacks = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up
    // the array with the stack name.
    foreach ($all_stacks ?: [] as $stack) {
      $stale[$stack->getStackName()] = $stack;
    }

    $result = $this->describeStacks($params);

    if (!empty($result)) {
      foreach ($result['Stacks'] ?: [] as $stack) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stack['StackName'])
          && !empty($stale[$stack['StackName']])) {
          unset($stale[$stack['StackName']]);
        }
        if ($to_update) {
          CloudFormationBatchOperations::updateStack($this->cloudContext, $stack);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = empty($params['StackName']) ? TRUE : !empty($result['Stacks']);
    }
    elseif (!empty($params['StackName']) && $clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all Stacks of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllStacks(array $params = [], $clear = TRUE): bool {
    $entity_type = 'aws_cloud_stack';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateStackEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Initialize a new batch builder.
   *
   * @param string $batch_name
   *   The batch name.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   The initialized batch object.
   */
  protected function initBatch($batch_name): BatchBuilder {
    return (new BatchBuilder())
      ->setTitle($batch_name);
  }

  /**
   * Run the batch job to process entities.
   *
   * @param \Drupal\Core\Batch\BatchBuilder $batch_builder
   *   The batch builder object.
   */
  protected function runBatch(BatchBuilder $batch_builder): void {
    try {
      // Log the start time.
      $start = $this->getTimestamp();
      $batch_array = $batch_builder->toArray();
      batch_set($batch_array);
      // Reset the progressive so batch works w/o a web head.
      $batch = &batch_get();
      $batch['progressive'] = FALSE;

      // Check if using Drush to process the queue.
      // using drush_backend_batch_process() will fix the "Route not found"
      // error.
      PHP_SAPI === 'cli' && function_exists('drush_backend_batch_process')
        ? drush_backend_batch_process()
        : batch_process();

      // Log the end time.
      $end = $this->getTimestamp();
      $this->logger('cloud_formation_service')->info($this->t('@updater - @cloud_context: Batch operation took @time seconds.', [
        '@cloud_context' => $this->cloudContext,
        '@updater' => $batch_array['title'],
        '@time' => $end - $start,
      ]));
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    finally {
      // Reset the batch otherwise this operation hangs when using Drush.
      // https://www.drupal.org/project/drupal/issues/3166042
      $batch = [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getUidTagValue(array $tags_array, string $entity_type_id): int {
    $bundle = $this->entityTypeManager->getDefinition($entity_type_id)->getProvider();
    $key = $this->cloudService->getTagKeyCreatedByUid($bundle, $this->cloudContext);

    $uid = 0;
    if (empty($tags_array['Tags'])) {
      return $uid;
    }

    foreach ($tags_array['Tags'] ?: [] as $tag) {
      if (!empty($tag['Key']) && $tag['Key'] === $key) {
        $uid = $tag['Value'];
        break;
      }
    }

    return $uid;
  }

  /**
   * {@inheritdoc}
   */
  public function createResourceQueueItems(CloudConfigInterface $cloud_config): void {
    $queue_limit = $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_queue_limit');
    $method_names = [
      'updateStacks',
    ];
    self::updateResourceQueue($cloud_config, $method_names, $queue_limit, self::getCloudFormationQueueName($cloud_config));
  }

}
