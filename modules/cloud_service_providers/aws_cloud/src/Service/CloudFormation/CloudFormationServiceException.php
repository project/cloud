<?php

namespace Drupal\aws_cloud\Service\CloudFormation;

/**
 * AWS Cloud CloudFormation service exception.
 */
class CloudFormationServiceException extends \Exception {

}
