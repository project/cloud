<?php

namespace Drupal\aws_cloud\Service\CloudFormation;

use Aws\ResultInterface;

/**
 * Interacts with the AWS Cloud CloudFormation API.
 */
interface CloudFormationServiceInterface {

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function setCloudContext($cloud_context): void;

  /**
   * Calls the Amazon CloudFormation API endpoint CreateStack.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createStack(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon CloudFormation API endpoint DescribeStacks.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of Stacks or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceException
   *   If the $params is empty or
   *   $cloud_formation_client (CloudFormationClient) is NULL.
   */
  public function describeStacks(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteStack.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\CloudFormation\CloudFormationServiceException
   *   If the $params is empty or
   *   $cloud_formation_client (CloudFormationClient) is NULL.
   */
  public function deleteStack(array $params = []): ?ResultInterface;

  /**
   * Update the stacks.
   *
   * Delete old Stack entities,
   * query the API for updated entities and
   * store them as Stack entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale Stacks.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateStacks(array $params = [], $clear = TRUE): bool;

  /**
   * Update the stacks without batch.
   *
   * Delete old Stacks entities,
   * query the API for updated entities and
   * store them as Stacks entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale Stacks.
   * @param bool $to_update
   *   TRUE to update stale Stacks.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateStacksWithoutBatch(array $params = [], $clear = TRUE, $to_update = TRUE): bool;

}
