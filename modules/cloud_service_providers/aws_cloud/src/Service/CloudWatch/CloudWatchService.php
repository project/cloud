<?php

namespace Drupal\aws_cloud\Service\CloudWatch;

use Aws\CloudWatch\CloudWatchClient;
use Aws\CloudWatch\Exception\CloudWatchException;
use Aws\Credentials\AssumeRoleCredentialProvider;
use Aws\Credentials\CredentialProvider;
use Aws\MockHandler;
use Aws\Result;
use Aws\ResultInterface;
use Aws\Sts\StsClient;
use Drupal\Component\Utility\Random;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceBase;

/**
 * CloudWatchService service interacts with the AWS Cloud CloudWatch API.
 */
class CloudWatchService extends CloudServiceBase implements CloudWatchServiceInterface {

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * TRUE or FALSE whether to be in test mode.
   *
   * @var bool
   */
  private $testMode;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new CloudWatchService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    ModuleHandlerInterface $module_handler,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    // Set up the configuration factory.
    $this->configFactory = $config_factory;

    // Set up the testMode flag.
    $this->testMode = (bool) $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_test_mode');

    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;

    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context): void {
    $this->cloudContext = $cloud_context;
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function getMetricData(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $random = new Random();
      $params += [
        'StartTime' => strtotime('-1 days'),
        'EndTime' => time(),
        'MetricDataQueries' => [
          [
            'Id' => 'cpu',
            'MetricStat' => [
              'Metric' => [
                'Namespace'  => 'AWS/EC2',
                'MetricName' => 'CPUUtilization',
                'Dimensions' => [
                  [
                    'Name' => 'InstanceId',
                    'Value' => "i-{$random->name(32, TRUE)}",
                  ],
                ],
              ],
              // 1day.
              'Period' => 24 * 60 * 60,
              'Stat' => 'Average',
              'Unit' => 'Seconds',
            ],
            'ReturnData' => FALSE,
          ],
        ],
      ];
    }
    return $this->execute('GetMetricData', $params, $credentials);
  }

  /**
   * Execute the API of AWS Cloud CloudWatch service.
   *
   * @param string $operation
   *   The operation to perform.
   * @param array $params
   *   An array of parameters.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\CloudWatch\CloudWatchServiceException
   *   If the $cloud_watch_client (CloudWatchClient) is NULL.
   */
  private function execute($operation, array $params = [], array $credentials = []): ?ResultInterface {
    $results = NULL;

    $cloud_watch_client = $this->getCloudWatchClient($credentials);
    if ($cloud_watch_client === NULL) {
      throw new CloudWatchServiceException('No CloudWatch Client found. Cannot perform API operations');
    }

    try {
      // Let other modules alter the parameters
      // before they are sent through the API.
      $this->moduleHandler->invokeAll('aws_cloud_pre_execute_alter', [
        &$params,
        $operation,
        $this->cloudContext,
      ]);

      $command = $cloud_watch_client->getCommand($operation, $params);
      $results = $cloud_watch_client->execute($command);

      // Let other modules alter the results before the module processes it.
      $this->moduleHandler->invokeAll('aws_cloud_post_execute_alter', [
        &$results,
        $operation,
        $this->cloudContext,
      ]);
    }
    catch (CloudWatchException $e) {
      // IAM permission validation needs AwsException to be thrown as it is
      // determined by the AwsErrorCode of the exception, 'AccessDenied'.
      // Note: CloudWatchService API itself does not support DryRun param, but
      // our code passes it as a marker.
      if (!empty($params['DryRun'])) {
        throw $e;
      }

      $this->messenger->addError($this->t('Error: The operation "@operation" could not be performed.', [
        '@operation' => $operation,
      ]));

      $this->messenger->addError($this->t('Error Info: @error_info', [
        '@error_info' => $e->getAwsErrorCode(),
      ]));

      $this->messenger->addError($this->t('Error from: @error_type-side', [
        '@error_type' => $e->getAwsErrorType(),
      ]));

      $this->messenger->addError($this->t('Status Code: @status_code', [
        '@status_code' => $e->getStatusCode(),
      ]));

      $this->messenger->addError($this->t('Message: @msg', ['@msg' => $e->getAwsErrorMessage()]));

    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $results;
  }

  /**
   * Load and return an CloudWatchClient.
   *
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\CloudWatch\CloudWatchClient
   *   CloudWatchClient or NULL.
   */
  public function getCloudWatchClient(array $credentials = []): CloudWatchClient {
    // Use the plugin manager to load the aws credentials.
    if (empty($credentials)) {
      if (empty($this->cloudContext)) {
        $credentials = [
          'use_instance_profile' => TRUE,
          'version' => 'latest',
        ];
      }
      else {
        $credentials = $this->cloudConfigPluginManager->loadCredentials();
      }
    }

    $credentials['endpoint'] = "https://monitoring." . $credentials['region'] . "amazonaws.com";

    try {
      $cloud_watch_params = [
        'region' => $credentials['region'],
        'version' => $credentials['version'],
      ];
      $provider = FALSE;

      // Load credentials if needed.
      if (empty($credentials['use_instance_profile']) && !empty($credentials['ini_file'])) {
        $provider = CredentialProvider::ini('default', $credentials['ini_file']);
        $provider = CredentialProvider::memoize($provider);
      }

      if (!empty($credentials['use_assume_role'])) {
        // Assume role.
        $sts_params = [
          'region' => $credentials['region'],
          'version' => $credentials['version'],
        ];
        if ($provider !== FALSE) {
          $sts_params['credentials'] = $provider;
        }
        $assumeRoleCredentials = new AssumeRoleCredentialProvider([
          'client' => new StsClient($sts_params),
          'assume_role_params' => [
            'RoleArn' => $credentials['role_arn'],
            'RoleSessionName' => 'cloud_watch_client_assume_role',
          ],
        ]);

        // Memoize takes care of re-authenticating when the tokens expire.
        $assumeRoleCredentials = CredentialProvider::memoize($assumeRoleCredentials);
        $cloud_watch_params = [
          'region' => $credentials['region'],
          'version' => $credentials['version'],
          'credentials' => $assumeRoleCredentials,
        ];
        // If switch role is enabled, execute one more assume role.
        if (!empty($credentials['use_switch_role'])) {
          $switch_sts_params = [
            'region' => $credentials['region'],
            'version' => $credentials['version'],
            'credentials' => $assumeRoleCredentials,
          ];
          $switchRoleCredentials = new AssumeRoleCredentialProvider([
            'client' => new StsClient($switch_sts_params),
            'assume_role_params' => [
              'RoleArn' => $credentials['switch_role_arn'],
              'RoleSessionName' => 'cloud_watch_client_switch_role',
            ],
          ]);
          $switchRoleCredentials = CredentialProvider::memoize($switchRoleCredentials);
          $cloud_watch_params['credentials'] = $switchRoleCredentials;
        }
      }
      elseif ($provider !== FALSE) {
        $cloud_watch_params['credentials'] = $provider;
      }

      $cloud_watch_client = new CloudWatchClient($cloud_watch_params);
    }
    catch (\Exception $e) {
      $cloud_watch_client = NULL;
      $this->logger('cloud_watch_service')->error($e->getMessage());
    }
    if ($this->testMode) {
      $this->addMockHandler($cloud_watch_client);
    }
    return $cloud_watch_client;
  }

  /**
   * Add a mock handler of aws sdk for testing.
   *
   * The mock data of aws response is saved
   * in configuration "aws_cloud_mock_data".
   *
   * @param \Aws\CloudWatch\CloudWatchClient $cloud_watch_client
   *   The CloudWatch client.
   *
   * @see https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_handlers-and-middleware.html
   */
  private function addMockHandler(CloudWatchClient $cloud_watch_client): void {
    $mock_data = $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_mock_data');
    if (empty($this->testMode) || empty($mock_data)) {
      return;
    }

    $mock_data = json_decode($mock_data, TRUE);
    $result = static function ($command, $request) use ($mock_data) {

      $command_name = $command->getName();
      $response_data = $mock_data[$command_name] ?? [];

      // ErrorCode field is proprietary defined to mock AwsErrorCode.
      // Note: Amazon CloudWatch does not support the DryRun parameter.
      if (!empty($response_data['ErrorCode'])) {
        return new CloudWatchException('CloudWatchException by CloudWatchService::addMockHandler()',
          $command, ['code' => $response_data['ErrorCode']]);
      }

      return new Result($response_data);
    };

    // Set a mock handler with the number of mocked results in the queue.
    // The mock queue count should be the number of API calls and is similar
    // to the prepared mocked results. TO be safe, an additional queue is set
    // for an async call or a repeated call.
    $results_on_queue = array_pad([], count($mock_data) + 1, $result);
    $cloud_watch_client
      ->getHandlerList()
      ->setHandler(new MockHandler($results_on_queue));
  }

}
