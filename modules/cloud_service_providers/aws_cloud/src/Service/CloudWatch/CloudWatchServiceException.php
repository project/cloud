<?php

namespace Drupal\aws_cloud\Service\CloudWatch;

/**
 * AWS Cloud CloudWatch service exception.
 */
class CloudWatchServiceException extends \Exception {

}
