<?php

namespace Drupal\aws_cloud\Service\CloudWatch;

use Aws\ResultInterface;

/**
 * Provides CloudWatch service interface.
 */
interface CloudWatchServiceInterface {

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function setCloudContext($cloud_context): void;

  /**
   * Calls the Amazon CloudWatch API endpoint getMetricData.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result or NULL if there is an error.
   */
  public function getMetricData(array $params = [], array $credentials = []): ?ResultInterface;

}
