<?php

namespace Drupal\aws_cloud\Service\Ec2;

use Aws\Api\DateTimeResult;
use Aws\Credentials\AssumeRoleCredentialProvider;
use Aws\Credentials\CredentialProvider;
use Aws\Ec2\Ec2Client;
use Aws\Ec2\Exception\Ec2Exception;
use Aws\Endpoint\PartitionEndpointProvider;
use Aws\MockHandler;
use Aws\Result;
use Aws\ResultInterface;
use Aws\Sts\StsClient;
use Drupal\Component\Utility\Random;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\aws_cloud\Entity\Ec2\Instance;
use Drupal\aws_cloud\Entity\Ec2\KeyPair;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudCacheServiceInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\CloudServiceBase;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Traits\AccessCheckTrait;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Ec2Service interacts with the Amazon EC2 API.
 */
class Ec2Service extends CloudServiceBase implements Ec2ServiceInterface {

  use AccessCheckTrait;
  use CloudContentEntityTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Field type plugin manager.
   *
   * @var \Drupal\core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Entity field manager interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * TRUE or FALSE whether to be in test mode.
   *
   * @var bool
   */
  private $testMode;

  /**
   * The lock interface.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Guzzle Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The Cloud cache service.
   *
   * @var \Drupal\cloud\Service\CloudCacheServiceInterface
   */
  protected $cloudCacheService;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Random instance.
   *
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;

  /**
   * Constructs a new Ec2Service object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\core\Field\FieldTypePluginManagerInterface $field_type_plugin_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock interface.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\cloud\Service\CloudCacheServiceInterface $cloud_cache_service
   *   The Cloud cache service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    FieldTypePluginManagerInterface $field_type_plugin_manager,
    EntityFieldManagerInterface $entity_field_manager,
    LockBackendInterface $lock,
    QueueFactory $queue_factory,
    ClientInterface $http_client,
    CloudServiceInterface $cloud_service,
    CloudCacheServiceInterface $cloud_cache_service,
    ModuleHandlerInterface $module_handler,
    RequestStack $request_stack,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    // Set up the entity type manager for querying entities.
    $this->entityTypeManager = $entity_type_manager;

    // Set up the configuration factory.
    $this->configFactory = $config_factory;

    // Set up the testMode flag.
    $this->testMode = (bool) $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_test_mode');

    $this->currentUser = $current_user;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->fieldTypePluginManager = $field_type_plugin_manager;

    $this->entityFieldManager = $entity_field_manager;
    $this->lock = $lock;
    $this->queueFactory = $queue_factory;
    $this->httpClient = $http_client;
    $this->cloudService = $cloud_service;
    $this->cloudCacheService = $cloud_cache_service;
    $this->moduleHandler = $module_handler;
    $this->requestStack = $request_stack;

    $this->random = new Random();
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context) {
    $this->cloudContext = $cloud_context;
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
  }

  /**
   * Load and return an Ec2Client.
   *
   * @param array $credentials
   *   The array of credentials.
   *
   * @return \Aws\Ec2\Ec2Client|null
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  protected function getEc2Client(array $credentials = []): ?Ec2Client {

    if (empty($credentials)) {
      $credentials = $this->cloudConfigPluginManager->loadCredentials();
    }

    try {
      $ec2_params = [
        'region' => $credentials['region'],
        'version' => $credentials['version'],
        'http' => [
          'connect_timeout' => $this->connectTimeout,
        ],
      ];
      $provider = FALSE;

      // Load credentials if needed.
      if (empty($credentials['use_instance_profile'])) {
        if (!empty($credentials['env'])) {
          $this->setCredentialsToEnv(
            $credentials['env']['access_key'],
            $credentials['env']['secret_key']
          );
          $provider = CredentialProvider::env();
        }
        else {
          $provider = CredentialProvider::ini('default', $credentials['ini_file']);
        }
        $provider = CredentialProvider::memoize($provider);
      }

      if (!empty($credentials['use_assume_role'])) {
        $sts_params = [
          'region' => $credentials['region'],
          'version' => $credentials['version'],
        ];
        if ($provider !== FALSE) {
          $sts_params['credentials'] = $provider;
        }
        $assumeRoleCredentials = new AssumeRoleCredentialProvider([
          'client' => new StsClient($sts_params),
          'assume_role_params' => [
            'RoleArn' => $credentials['role_arn'],
            'RoleSessionName' => 'ec2_client_assume_role',
          ],
        ]);
        // Memoize takes care of re-authenticating when the tokens expire.
        $assumeRoleCredentials = CredentialProvider::memoize($assumeRoleCredentials);
        $ec2_params['credentials'] = $assumeRoleCredentials;

        // If switch role is enabled, execute one more assume role.
        if (!empty($credentials['use_switch_role'])) {
          $switch_sts_params = [
            'region' => $credentials['region'],
            'version' => $credentials['version'],
            'credentials' => $assumeRoleCredentials,
          ];
          $switchRoleCredentials = new AssumeRoleCredentialProvider([
            'client' => new StsClient($switch_sts_params),
            'assume_role_params' => [
              'RoleArn' => $credentials['switch_role_arn'],
              'RoleSessionName' => 'ec2_client_switch_role',
            ],
          ]);
          $switchRoleCredentials = CredentialProvider::memoize($switchRoleCredentials);
          $ec2_params['credentials'] = $switchRoleCredentials;
        }
      }
      elseif ($provider !== FALSE) {
        $ec2_params['credentials'] = $provider;
      }

      $ec2_client = new Ec2Client($ec2_params);
    }
    catch (\Exception $e) {
      $ec2_client = NULL;
      $this->logger('aws_cloud')->error($e->getMessage());
    }
    if ($this->testMode) {
      $this->addMockHandler($ec2_client);
    }
    return $ec2_client;
  }

  /**
   * Set credentials to ENV.
   *
   * @param string $access_key
   *   The access key.
   * @param string $secret_key
   *   The secret key.
   */
  protected function setCredentialsToEnv($access_key, $secret_key) {
    putenv(CredentialProvider::ENV_KEY . "=$access_key");
    putenv(CredentialProvider::ENV_SECRET . "=$secret_key");
  }

  /**
   * Add a mock handler of AWS SDK for testing.
   *
   * The mock data of AWS response is saved
   * in configuration "aws_cloud_mock_data".
   *
   * @param \Aws\Ec2\Ec2Client $ec2_client
   *   The ec2 client.
   *
   * @see https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_handlers-and-middleware.html
   */
  private function addMockHandler(Ec2Client $ec2_client): void {
    $mock_data = $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_mock_data');
    if (empty($this->testMode) || empty($mock_data)) {
      return;
    }

    $mock_data = json_decode($mock_data, TRUE);
    $result = static function ($command, $request) use ($mock_data) {

      $command_name = $command->getName();
      $response_data = $mock_data[$command_name] ?? [];

      // ErrorCode field is proprietary defined to mock AwsErrorCode.
      // Checking the value of DryRun parameter as some test cases expect a
      // different result based on the DryRun parameter.
      if (!empty($response_data['ErrorCode'])
        && $command->hasParam('DryRun')
        && ($command->toArray())['DryRun']) {
        return new Ec2ServiceException('AwsException on ' . $command_name . ' by Ec2Service::addMockHandler()',
          $command_name, $response_data['ErrorCode']);
      }

      // Support EC2-Classic.
      // See also: https://www.drupal.org/i/3032292.
      if ($command_name === 'DescribeAccountAttributes'
        && empty($response_data)) {
        $response_data = [
          'AccountAttributes' => [
            [
              'AttributeName' => 'supported-platforms',
              'AttributeValues' => [
                [
                  'AttributeValue' => 'VPC',
                ],
              ],
            ],
          ],
        ];
      }
      // Because launch time is special,
      // we need to convert it from string to DateTimeResult.
      if ($command_name === 'DescribeInstances'
        && !empty($response_data)) {
        // NOTE: We won't use foreach ($items ?: [] as $item) here since
        // $response_data['Reservations'] is used as a reference.
        foreach ($response_data['Reservations'] as &$reservation) {
          foreach ($reservation['Instances'] as &$instance) {
            // Initialize $instance['LaunchTime'].
            $instance['LaunchTime'] = !empty($instance['LaunchTime'])
              ? new DateTimeResult($instance['LaunchTime'])
              : new DateTimeResult();
          }
        }
      }
      return new Result($response_data);
    };

    // Set a mock handler with the number of mocked results in the queue.
    // The mock queue count should be the number of API calls and is similar
    // to the prepared mocked results. TO be safe, an additional queue is set
    // for an async call or a repeated call.
    $results_on_queue = array_pad([], count($mock_data) + 1, $result);
    $ec2_client
      ->getHandlerList()
      ->setHandler(new MockHandler($results_on_queue));
  }

  /**
   * Execute the API of Amazon EC2 Service.
   *
   * @param string $operation
   *   The operation to perform.
   * @param array $params
   *   An array of parameters.
   * @param array $credentials
   *   The array of credentials.
   * @param string $update_entities_method
   *   The method name of updating entities.
   * @param array $update_entities_method_params
   *   The method parameters of updating entities.
   * @param array $refs
   *   The array of references.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  private function execute(string $operation, array $params = [], array $credentials = [], string $update_entities_method = '', array $update_entities_method_params = [], array $refs = []): ?ResultInterface {
    if ($this->isWorkerResource()) {
      $this->saveOperation($operation, [$params], $update_entities_method, $update_entities_method_params, $refs);
      return new Result(['SendToWorker' => TRUE]);
    }

    $results = NULL;
    $error_code = NULL;
    $this->cloudCacheService->setErrorCodes(self::PERMISSION_ERROR_CODES);
    $ec2_client = $this->getEc2Client($credentials);
    if (empty($ec2_client)) {
      throw new Ec2ServiceException('No EC2 Client found. Cannot perform API operations');
    }

    try {
      // Let other modules alter the parameters
      // before they are sent through the API.
      $this->moduleHandler->invokeAll('aws_cloud_pre_execute_alter', [
        &$params,
        $operation,
        $this->cloudContext,
      ]);

      // Converts NULL in the array to an empty string value ('').
      !is_array($params) ?: array_walk_recursive($params, static function (&$value, $key) {
        $value = $value ?? '';
      });
      $command = $ec2_client->getCommand($operation, $params);
      $results = $ec2_client->execute($command);

      // Let other modules alter the results before the module processes it.
      $this->moduleHandler->invokeAll('aws_cloud_post_execute_alter', [
        &$results,
        $operation,
        $this->cloudContext,
      ]);
    }
    catch (Ec2Exception $e) {

      $error_code = $e->getAwsErrorCode();

      // IAM permission validation needs AwsException to be thrown as it is
      // determined by the AwsErrorCode of the exception, 'DryRunOperation',
      // 'UnauthorizedOperation' or others.
      if (!empty($params['DryRun'])) {
        $this->cloudCacheService->setCachedOperationAccess($this->cloudContext, $operation, $error_code);
        throw $e;
      }

      // Handle an option field "Validate IAM permissions". If checked, validate
      // the AWS account ID has all the necessary IAM permissions. By default,
      // the option is checked in the AWS Cloud service provider creation form,
      // which means TRUE.
      $check_iam_permissions = TRUE;

      try {
        $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

        // Err on the side of caution, default to TRUE to show the messages.
        // When a user *unchecks* the option "Validate IAM permission" for e.g.
        // air-gaped environment to avoid the Internet access to validate the
        // IAM permission, it does not show any error messages (FALSE).
        $check_iam_permissions = !empty($cloud_config)
          && $cloud_config->hasField('field_check_iam_permissions')
          && !empty($cloud_config->get('field_check_iam_permissions')->value)
          ?: FALSE;
      }
      catch (\Exception $ex) {
        // If an error occurs, display the error message.
        $check_iam_permissions = TRUE;
        $this->logger('aws_cloud')->error($ex->getMessage());
      }

      // If check permissions is FALSE, no need to display these error messages.
      if (!empty($check_iam_permissions)) {

        $messages = [
          $this->t('Error Info: %error_info', [
            '%error_info' => $e->getAwsErrorCode() ?? 'N/A',
          ]),
          $this->t('Error from: %error_type-side', [
            '%error_type' => $e->getAwsErrorType() ?? 'N/A',
          ]),
          $this->t('Status Code: %status_code', [
            '%status_code' => $e->getStatusCode() ?? 'N/A',
          ]),
          $this->t('Message: %msg', [
            '%msg' => $e->getAwsErrorMessage() ?? 'N/A',
          ]),
        ];

        $output = '';
        foreach ($messages ?: [] as $message) {
          $output .= "<li>{$message}</li>";
        }
        $this->messenger->addError($this->t('The operation %operation could not being performed:<ul>@output</ul>', [
          '%operation' => $operation,
          '@output' => Markup::Create($output),
        ]));
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->logger('aws_cloud')->error($e->getMessage());
    }
    $this->cloudCacheService->setCachedOperationAccess($this->cloudContext, $operation, $error_code);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function associateAddress(array $params = []): ?ResultInterface {
    return $this->execute('AssociateAddress', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function allocateAddress(array $params = []): ?ResultInterface {
    return $this->execute('AllocateAddress', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function associateIamInstanceProfile(array $params = []): ?ResultInterface {
    return $this->execute('AssociateIamInstanceProfile', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function describeAccountAttributes(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeAccountAttributes', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function disassociateIamInstanceProfile(array $params = []): ?ResultInterface {
    return $this->execute('DisassociateIamInstanceProfile', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function replaceIamInstanceProfileAssociation(array $params = []): ?ResultInterface {
    return $this->execute('ReplaceIamInstanceProfileAssociation', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function describeIamInstanceProfileAssociations(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeIamInstanceProfileAssociations', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function authorizeSecurityGroupIngress(array $params = [], array $credentials = [], array $refs = []): ?ResultInterface {
    return $this->execute('AuthorizeSecurityGroupIngress', $params, $credentials, '', [], $refs);
  }

  /**
   * {@inheritdoc}
   */
  public function authorizeSecurityGroupEgress(array $params = [], array $credentials = [], array $refs = []): ?ResultInterface {
    return $this->execute('AuthorizeSecurityGroupEgress', $params, $credentials, '', [], $refs);
  }

  /**
   * {@inheritdoc}
   */
  public function createImage(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += [
        'InstanceId' => "i-{$this->random->name(32, TRUE)}",
        'Name' => $this->random->name(32, TRUE),
      ];
    }
    return $this->execute('CreateImage', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function modifyImageAttribute(array $params = []): ?ResultInterface {
    return $this->execute('ModifyImageAttribute', $params, [], 'updateImages',
      ['ImageIds' => [$params['ImageId']]]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function modifyTransitGateway(array $params = []): ?ResultInterface {
    return $this->execute('ModifyTransitGateway', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function createKeyPair(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += ['KeyName' => $this->random->name(32, TRUE)];
    }
    return $this->execute('CreateKeyPair', $params, $credentials, 'createKeyPairEntity');
  }

  /**
   * {@inheritdoc}
   */
  public function createKeyPairEntity(array $params): void {
    $result = $params['result'];
    $uid = $this->getUidTagValue($result->toArray(), 'aws_cloud_key_pair');
    $timestamp = time();

    $entity = KeyPair::create([
      'cloud_context' => $this->cloudContext,
      'key_pair_name' => $result['KeyName'],
      'key_pair_id' => $result['KeyPairId'],
      'key_fingerprint' => $result['KeyFingerprint'],
      'key_material' => $result['KeyMaterial'],
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $uid,
    ]);
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createNetworkInterface(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += ['SubnetId' => "subnet-{$this->random->name(32, TRUE)}"];
    }
    return $this->execute('CreateNetworkInterface', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function modifyNetworkInterfaceAttribute(array $params = []): ?ResultInterface {
    return $this->execute('ModifyNetworkInterfaceAttribute', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function createTags(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun']) && count($params) === 1) {
      $result = $this->describeSecurityGroups(
        [
          'DryRun' => FALSE,
          'GroupNames' => ['default'],
        ],
        $credentials
      );
      if (empty($result['SecurityGroups'][0]['GroupId'])) {
        throw new Ec2ServiceException('Failed to test createTags() on getting a required parameter, a Resource ID, by calling describeSecurityGroups().',
          'describeSecurityGroups', 'UnauthorizedOperation');
      }
      $group_id_prepared_by_aws = $result['SecurityGroups'][0]['GroupId'];
      // Set required parameters if missing.
      $params += [
        'Resources' => [$group_id_prepared_by_aws],
        'Tags' => [
          [
            'Key' => 'Name',
            'Value' => $this->random->name(32, TRUE),
          ],
        ],
      ];
    }
    return $this->execute('CreateTags', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTags(array $params = []): ?ResultInterface {
    return $this->execute('DeleteTags', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function createVolume(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun']) && count($params) === 1) {
      $availability_zones = $this->getAvailabilityZones();
      if (empty($availability_zones)) {
        throw new Ec2ServiceException('Failed to test createVolume() on getting a required parameter, AvailabilityZone, by calling getAvailabilityZones().',
          'createVolume', 'UnauthorizedOperation');
      }
      // Set required parameters if missing.
      $params += [
        'AvailabilityZone' => array_shift($availability_zones),
        'Size' => 1024,
      ];
    }
    return $this->execute('CreateVolume', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function modifyVolume(array $params = []): ?ResultInterface {
    return $this->execute('ModifyVolume', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function createSnapshot(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun']) && count($params) === 1) {
      $volumes = $this->describeVolumes([], $credentials);
      if (empty($volumes['Volumes'][0]['VolumeId'])) {
        throw new Ec2ServiceException('Failed to test createSnapshot() on getting a required parameter, VolumeId, by calling describeVolumes().',
          'createSnapshot', 'UnauthorizedOperation');
      }
      $params['VolumeId'] = $volumes['Volumes'][0]['VolumeId'];
    }
    return $this->execute('CreateSnapshot', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function createVpc(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += ['CidrBlock' => '10.0.0.0/8'];
    }
    return $this->execute('CreateVpc', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function createInternetGateway(array $params = []): ?ResultInterface {
    return $this->execute('CreateInternetGateway', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function createCarrierGateway(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += ['VpcId' => $this->random->name(32, TRUE)];
    }
    return $this->execute('CreateCarrierGateway', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function createTransitGateway(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('CreateTransitGateway', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function createFlowLogs(array $params = []): ?ResultInterface {
    return $this->execute('CreateFlowLogs', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function createVpcPeeringConnection(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += ['VpcId' => $this->random->name(32, TRUE)];
    }
    return $this->execute('CreateVpcPeeringConnection', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function acceptVpcPeeringConnection(array $params = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += ['VpcPeeringConnectionId' => $this->random->name(32, TRUE)];
    }
    return $this->execute('AcceptVpcPeeringConnection', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function describeVpcPeeringConnections(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeVpcPeeringConnections', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeFlowLogs(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeFlowLogs', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function createSecurityGroup(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += [
        'Description' => $this->random->name(32, TRUE),
        'GroupName' => $this->random->name(32, TRUE),
      ];
    }
    return $this->execute('CreateSecurityGroup', $params, $credentials, 'updateSecurityGroups', []);
  }

  /**
   * {@inheritdoc}
   */
  public function deregisterImage(array $params = []): ?ResultInterface {
    return $this->execute('DeregisterImage', $params, [], 'updateImages',
      ['ImageIds' => [$params['ImageId']]]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function describeInstances(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeInstances', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeInstanceAttribute(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += [
        'Attribute' => 'userData',
        'InstanceId' => "i-{$this->random->name(32, TRUE)}",
      ];
    }
    return $this->execute('DescribeInstanceAttribute', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeInstanceTypes(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeInstanceTypes', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeImages(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeImages', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeImageAttribute(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += [
        'Attribute' => 'launchPermission',
        'ImageId' => "ami-{$this->random->name(32, TRUE)}",
      ];
    }
    return $this->execute('DescribeImageAttribute', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeSecurityGroups(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeSecurityGroups', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeNetworkInterfaces(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeNetworkInterfaces', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeAddresses(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeAddresses', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeSnapshots(array $params = [], array $credentials = []): ?ResultInterface {
    if (empty($credentials)) {
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
      $account_id = $cloud_config->get('field_account_id')->value;
      if ($cloud_config->hasField('field_use_assume_role')) {
        $use_assume_role = $cloud_config->get('field_use_assume_role')->value ?? FALSE;
      }
      if ($cloud_config->hasField('field_use_switch_role')) {
        $use_switch_role = $cloud_config->get('field_use_switch_role')->value ?? FALSE;
      }
      if (!empty($use_assume_role) && !empty($use_switch_role)) {
        $account_id = trim($cloud_config->get('field_switch_role_account_id')->value ?: '');
      }
      $params['RestorableByUserIds'] = [$account_id];
    }
    return $this->execute('DescribeSnapshots', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeKeyPairs(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeKeyPairs', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeVolumes(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeVolumes', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeAvailabilityZones(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeAvailabilityZones', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeVpcs(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeVpcs', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeInternetGateways(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeInternetGateways', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeCarrierGateways(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeCarrierGateways', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeTransitGateways(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeTransitGateways', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeTransitGatewayRouteTables(array $params = []): ?ResultInterface {
    return $this->execute('DescribeTransitGatewayRouteTables', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function associateVpcCidrBlock(array $params = []): ?ResultInterface {
    return $this->execute('AssociateVpcCidrBlock', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function disassociateVpcCidrBlock(array $params = []): ?ResultInterface {
    return $this->execute('DisassociateVpcCidrBlock', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function describeSubnets(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeSubnets', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function createSubnet(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += [
        'CidrBlock' => '10.0.0.0/8',
        'VpcId' => $this->random->name(32, TRUE),
      ];
    }
    return $this->execute('CreateSubnet', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function getRegions(): array {
    $regions = PartitionEndpointProvider::defaultProvider()
      ->getPartition($region = '', 'ec2')['regions'];

    return array_map(static function ($region) {
      return $region['description'];
    }, $regions ?: []);
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpointUrls(): array {
    // The $endpoints will be an array like ['us-east-1' => [], 'us-east-2' =>
    // [], ...].
    $endpoints = PartitionEndpointProvider::defaultProvider()
      ->getPartition('', 'ec2')['services']['ec2']['endpoints'];

    $urls = [];
    foreach ($endpoints ?: [] as $endpoint => $item) {
      $url = "https://ec2.$endpoint.amazonaws.com";
      $urls[$endpoint] = $url;
    }

    return $urls;
  }

  /**
   * {@inheritdoc}
   */
  public function importKeyPair(array $params = []): ?ResultInterface {
    return $this->execute('ImportKeyPair', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function terminateInstances(array $params = []): ?ResultInterface {
    return $this->execute('TerminateInstances', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSecurityGroup(array $params = []): ?ResultInterface {
    return $this->execute('DeleteSecurityGroup', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNetworkInterface(array $params = []): ?ResultInterface {
    return $this->execute('DeleteNetworkInterface', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function disassociateAddress(array $params = []): ?ResultInterface {
    return $this->execute('DisassociateAddress', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function releaseAddress(array $params = []): ?ResultInterface {
    return $this->execute('ReleaseAddress', $params, [], 'updateElasticIps', []);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteKeyPair(array $params = []): ?ResultInterface {
    return $this->execute('DeleteKeyPair', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVolume(array $params = []): ?ResultInterface {
    return $this->execute('DeleteVolume', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function attachVolume(array $params = []): ?ResultInterface {
    return $this->execute('AttachVolume', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function detachVolume(array $params = []): ?ResultInterface {
    return $this->execute('DetachVolume', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSnapshot(array $params = []): ?ResultInterface {
    return $this->execute('DeleteSnapshot', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVpc(array $params = []): ?ResultInterface {
    return $this->execute('DeleteVpc', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteInternetGateway(array $params = []): ?ResultInterface {
    return $this->execute('DeleteInternetGateway', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function attachInternetGateway(array $params = []): ?ResultInterface {
    return $this->execute('AttachInternetGateway', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function detachInternetGateway(array $params = []): ?ResultInterface {
    return $this->execute('DetachInternetGateway', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCarrierGateway(array $params = []): ?ResultInterface {
    return $this->execute('DeleteCarrierGateway', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTransitGateway(array $params = []): ?ResultInterface {
    return $this->execute('DeleteTransitGateway', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVpcPeeringConnection(array $params = []): ?ResultInterface {
    return $this->execute('DeleteVpcPeeringConnection', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteFlowLogs(array $params = []): ?ResultInterface {
    return $this->execute('DeleteFlowLogs', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSubnet(array $params = []): ?ResultInterface {
    return $this->execute('DeleteSubnet', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function revokeSecurityGroupIngress(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('RevokeSecurityGroupIngress', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function revokeSecurityGroupEgress(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('RevokeSecurityGroupEgress', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function runInstances(array $params = [], array $tags = [], string $bundle = 'aws_cloud') {
    $uid_key_name = $this->cloudService->getTagKeyCreatedByUid(
      $bundle,
      $this->cloudContext
    );

    // Add meta tags to identify where the instance was launched from.
    $params['TagSpecifications'] = [
      [
        'ResourceType' => 'instance',
        'Tags' => [
          [
            'Key' => 'aws_cloud_' . Instance::TAG_LAUNCH_ORIGIN,
            'Value' => $this->requestStack->getCurrentRequest()->getHost(),
          ],
          [
            'Key' => 'aws_cloud_' . Instance::TAG_LAUNCH_SOFTWARE,
            'Value' => 'Drupal 8 Cloud Orchestrator',
          ],
          [
            'Key' => 'aws_cloud_' . Instance::TAG_LAUNCHED_BY,
            'Value' => $this->currentUser->getAccountName(),
          ],
          [
            'Key' => $uid_key_name,
            'Value' => $this->currentUser->id(),
          ],
        ],
      ],
    ];

    // If there are tags, add them to the Tags array.
    foreach ($tags ?: [] as $tag) {
      $params['TagSpecifications'][0]['Tags'][] = $tag;
    }

    return $this->execute('RunInstances', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function stopInstances(array $params = []): ?ResultInterface {
    return $this->execute('StopInstances', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function startInstances(array $params = []): ?ResultInterface {
    return $this->execute('StartInstances', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function modifyInstanceAttribute(array $params = []): ?ResultInterface {
    return $this->execute('ModifyInstanceAttribute', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function rebootInstances(array $params = []): ?ResultInterface {
    return $this->execute('RebootInstances', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function createLaunchTemplate(array $params = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += [
        'LaunchTemplateName' => $this->random->name(32, TRUE),
        'LaunchTemplateData' => [],
      ];
    }
    return $this->execute('CreateLaunchTemplate', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function modifyLaunchTemplate(array $params = []): ?ResultInterface {
    return $this->execute('ModifyLaunchTemplate', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteLaunchTemplate(array $params = []): ?ResultInterface {
    return $this->execute('DeleteLaunchTemplate', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function describeLaunchTemplates(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeLaunchTemplates', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function createLaunchTemplateVersion(array $params = []): ?ResultInterface {
    return $this->execute('CreateLaunchTemplateVersion', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteLaunchTemplateVersions(array $params = []): ?ResultInterface {
    return $this->execute('DeleteLaunchTemplateVersions', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function describeLaunchTemplateVersions(array $params = [], array $credentials = []): ?ResultInterface {
    return $this->execute('DescribeLaunchTemplateVersions', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function updateSingleEntity($entity, $is_submit = FALSE, $update = FALSE): bool {
    $entity_type = $entity->getEntityTypeId();
    $updated = TRUE;
    if ($this->isWorkerResource()) {
      return $updated;
    }

    switch ((string) $entity_type) {
      case 'aws_cloud_instance':
        $updated = $this->updateInstancesWithoutBatch(['InstanceIds' => [$entity->getInstanceId()]], TRUE, $update);
        break;

      case 'aws_cloud_image':
        $updated = $this->updateImagesWithoutBatch(
          ['ImageIds' => [$entity->getImageId()]],
          TRUE,
          !empty($update) ? TRUE : !$is_submit
        );
        break;

      case 'aws_cloud_security_group':
        $updated = $this->updateSecurityGroupEntities(['GroupIds' => [$entity->getGroupId()]], TRUE, $entity_type, $this->cloudContext, $update);
        break;

      case 'aws_cloud_elastic_ip':
        $updated = $this->updateElasticIpEntities($entity_type, $this->cloudContext, ['PublicIps' => [$entity->getPublicIp()]], FALSE, $update);
        break;

      case 'aws_cloud_key_pair':
        $updated = $this->updateKeyPairEntities($entity_type, $this->cloudContext, ['KeyNames' => [$entity->getKeyPairName()]], $update);
        break;

      case 'aws_cloud_volume':
        $updated = $this->updateVolumeEntities($entity_type, $this->cloudContext, ['VolumeIds' => [$entity->getVolumeId()]], $update);
        break;

      case 'aws_cloud_snapshot':
        $updated = $this->updateSnapshotEntities($entity_type, $this->cloudContext, ['SnapshotIds' => [$entity->getSnapshotId()]], $update);
        break;

      case 'aws_cloud_network_interface':
        $updated = $this->updateNetworkInterfaceEntities(['NetworkInterfaceIds' => [$entity->getNetworkInterfaceId()]], TRUE, $entity_type, $this->cloudContext, $update);
        break;

      case 'aws_cloud_vpc':
        $updated = $this->updateVpcsWithoutBatch(['VpcIds' => [$entity->getVpcId()]], TRUE, $update);
        break;

      case 'aws_cloud_subnet':
        $updated = $this->updateSubnetsWithoutBatch(['SubnetIds' => [$entity->getSubnetId()]], TRUE, $update);
        break;

      case 'aws_cloud_vpc_peering_connection':
        $updated = $this->updateVpcPeeringConnectionsWithoutBatch(['VpcPeeringConnectionIds' => [$entity->getVpcPeeringConnectionId()]], TRUE, $update);
        break;

      case 'aws_cloud_internet_gateway':
        $updated = $this->updateInternetGatewaysWithoutBatch(['InternetGatewayIds' => [$entity->getInternetGatewayId()]], TRUE, $update);
        break;

      case 'aws_cloud_carrier_gateway':
        $updated = $this->updateCarrierGatewaysWithoutBatch(['CarrierGatewayIds' => [$entity->getCarrierGatewayId()]], TRUE, $update);
        break;

      case 'aws_cloud_transit_gateway':
        $updated = $this->updateTransitGatewaysWithoutBatch(['TransitGatewayIds' => [$entity->getTransitGatewayId()]], TRUE, $update);
        break;

      case 'aws_cloud_transit_gateway_route':
        $updated = $this->updateTransitGatewayRoutesWithoutBatch(['TransitGatewayRouteTableIds' => [$entity->getTransitGatewayRouteTableId()]], TRUE, $update);
        break;

      case 'aws_cloud_availability_zone':
        $updated = $this->updateAvailabilityZonesWithoutBatch(['ZoneIds' => [$entity->getZoneId()]], TRUE, $update);
        break;

    }
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as instance entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateInstanceEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '') {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    // Call the API and get all instances.
    $result = $this->describeInstances($params);

    if (!empty($result)) {

      $all_instances = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the instances by setting up
      // the array with the instance_id.
      foreach ($all_instances ?: [] as $instance) {
        $stale[$instance->getInstanceId()] = $instance;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Instance Update');

      // Loop through the reservations and store each one as an instance entity.
      foreach ($result['Reservations'] ?: [] as $reservation) {

        foreach ($reservation['Instances'] ?: [] as $instance) {
          // Keep track of instances that do not exist anymore
          // delete them after saving the rest of the instances.
          if (!empty($stale[$instance['InstanceId']])) {
            unset($stale[$instance['InstanceId']]);
          }
          // Store the Reservation OwnerId in instance so batch
          // callback has access.
          $instance['reservation_ownerid'] = $reservation['OwnerId'];
          $instance['reservation_id'] = $reservation['ReservationId'];

          $batch_builder->addOperation([
            Ec2BatchOperations::class,
            'updateInstance',
          ], [$cloud_context, $instance]);
        }
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Update the EC2Instances.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateInstances(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_instance';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateInstanceEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateInstancesWithoutBatch(array $params = [], $clear = FALSE, $to_update = TRUE) {
    $updated = FALSE;
    $entity_type = 'aws_cloud_instance';
    $lock_name = $this->getLockKey($entity_type);
    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    // Load all entities by cloud_context.
    $properties = empty($params['InstanceIds'][0])
    ? ['cloud_context' => $this->cloudContext]
    : [
      'cloud_context' => $this->cloudContext,
      'instance_id' => $params['InstanceIds'][0],
    ];

    $instance_entities = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadByProperties($properties);

    $result = $this->describeInstances($params);
    $stale = [];
    // Make it easier to look up the images by setting up
    // the array with the image_id.
    foreach ($instance_entities ?: [] as $instance) {
      $stale[$instance->getInstanceId()] = $instance;
    }

    if (!empty($result)) {
      foreach ($result['Reservations'] ?: [] as $reservation) {
        foreach ($reservation['Instances'] ?: [] as $instance) {
          if (!empty($instance['State']['Name']) && $instance['State']['Name'] === 'terminated') {
            continue;
          }
          $updated = TRUE;
          // Keep track of images that do not exist anymore
          // delete them after saving the rest of the images.
          if (!empty($stale[$instance['InstanceId']])) {
            unset($stale[$instance['InstanceId']]);
          }

          // Store the Reservation OwnerId in instance so batch
          // callback has access.
          $instance['reservation_ownerid'] = $reservation['OwnerId'];
          $instance['reservation_id'] = $reservation['ReservationId'];
          if ($to_update) {
            Ec2BatchOperations::updateInstance($this->cloudContext, $instance);
          }
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }
    }
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all Instances of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllInstances(array $params = [], $clear = FALSE) {
    $entity_type = 'aws_cloud_instance';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateInstanceEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as image entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateImageEntities(array $params = [], $clear = FALSE, $entity_type = '', $cloud_context = '') {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    // Load all entities by cloud_context and owner id if passed.
    $query_params = [
      'cloud_context' => $this->cloudContext,
    ];
    if (!empty($params['Owners'])) {
      $query_params['account_id'] = $params['Owners'];
    }
    $image_entities = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadByProperties($query_params);
    $result = $this->describeImages($params);

    if (!empty($result)) {

      $this->cloudConfigPluginManager->setCloudContext($cloud_context);
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

      $stale = [];
      // Make it easier to look up the images by setting up
      // the array with the image_id.
      foreach ($image_entities ?: [] as $image) {
        $stale[$image->getImageId()] = $image;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Image Update');

      foreach ($result['Images'] ?: [] as $image) {
        // Keep track of images that do not exist anymore
        // delete them after saving the rest of the images.
        if (!empty($stale[$image['ImageId']])) {
          unset($stale[$image['ImageId']]);
        }

        $batch_builder->addOperation([
          Ec2BatchOperations::class,
          'updateImage',
        ], [$cloud_context, $image]);
      }

      if (!array_key_exists('ImageIds', $params)) {
        // If  OwnerId of image is not same as account ID of Cloud config,
        // it might be the public image.
        // Then try to call 'describeImages' with ImageId.
        foreach ($stale ?: [] as $entity) {
          if ($cloud_config->get('field_account_id')->value === $entity->getAccountId()) {
            continue;
          }
          $images = $this->describeImages([
            'ImageIds' => [$entity->getImageId()],
          ]);
          if (!empty($images) && !empty($images['Images'])) {
            $image = reset($images['Images']);
            $batch_builder->addOperation([
              Ec2BatchOperations::class,
              'updateImage',
            ], [$this->cloudContext, $image]);

            unset($stale[$image['ImageId']]);
            $result['Images'][] = $image;
          }
        }
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateImages(array $params = [], $clear = FALSE, $save_operation = FALSE) {
    if ($save_operation && $this->isWorkerResource()) {
      $this->saveOperation('updateImages', [$params, $clear]);
      return TRUE;
    }

    $entity_type = 'aws_cloud_image';
    $lock_name = $this->getLockKey($entity_type);
    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateImageEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateImagesWithoutBatch(array $params = [], $clear = FALSE, $to_update = TRUE): bool {
    $updated = FALSE;
    $entity_type = 'aws_cloud_image';
    $lock_name = $this->getLockKey($entity_type);
    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    // Load all entities by cloud_context.
    $properties = empty($params['ImageIds'][0])
    ? ['cloud_context' => $this->cloudContext]
    : [
      'cloud_context' => $this->cloudContext,
      'image_id' => $params['ImageIds'][0],
    ];
    $image_entities = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadByProperties($properties);
    $result = $this->describeImages($params);

    $stale = [];
    // Make it easier to look up the images by setting up
    // the array with the image_id.
    foreach ($image_entities ?: [] as $image) {
      $stale[$image->getImageId()] = $image;
    }

    if (!empty($result)) {
      foreach ($result['Images'] ?: [] as $image) {
        $updated = TRUE;
        // Keep track of images that do not exist anymore
        // delete them after saving the rest of the images.
        if (!empty($stale[$image['ImageId']])) {
          unset($stale[$image['ImageId']]);
        }
        if ($to_update) {
          Ec2BatchOperations::updateImage($this->cloudContext, $image);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all Images of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllImages(array $params = [], $clear = FALSE): ?bool {
    $entity_type = 'aws_cloud_image';
    $lock_name = $this->getLockKey($entity_type);
    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateImageEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as SecurityGroup entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update security group.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSecurityGroupEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    $updated = FALSE;
    $result = $this->describeSecurityGroups($params);

    $properties = empty($params['GroupIds'][0]) ? ['cloud_context' => $cloud_context] : [
      'cloud_context' => $cloud_context,
      'group_id' => $params['GroupIds'][0],
    ];
    $all_groups = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the groups by setting up
    // the array with the group_id.
    foreach ($all_groups ?: [] as $group) {
      $stale[$group->getGroupId()] = $group;
    }

    /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
    $batch_builder = $this->initBatch('Security group update');

    if (!empty($result)) {
      foreach ($result['SecurityGroups'] ?: [] as $security_group) {

        // Keep track of instances that do not exist anymore
        // delete them after saving the rest of the instances.
        if (!empty($stale[$security_group['GroupId']])) {
          unset($stale[$security_group['GroupId']]);
        }
        if ($update) {
          $batch_builder->addOperation([
            Ec2BatchOperations::class,
            'updateSecurityGroup',
          ], [$cloud_context, $security_group]);
        }
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = empty($params['GroupIds'][0]) ? TRUE : !empty($result['SecurityGroups']);
    }
    elseif (!empty($params['GroupIds'][0])) {
      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
    }

    return $updated;
  }

  /**
   * Update the EC2SecurityGroups.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSecurityGroups(array $params = [], $clear = TRUE): bool {
    $entity_type = 'aws_cloud_security_group';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSecurityGroupEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all SecurityGroups of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllSecurityGroups(array $params = [], $clear = TRUE): bool {
    $entity_type = 'aws_cloud_security_group';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSecurityGroupEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateCloudLaunchTemplates(array $params = [], $clear = TRUE): bool {
    $updated = FALSE;
    $entity_type = 'cloud_launch_template';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    // Load all entities by cloud_context.
    $entities = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadByProperties([
        'type' => 'aws_cloud',
        'cloud_context' => $this->cloudContext,
      ]);

    $result = $this->describeLaunchTemplates($params);

    if (!empty($result)) {

      $stale = [];
      foreach ($entities ?: [] as $entity) {
        $stale[$entity->getName()] = $entity;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Launch Template Update');

      foreach ($result['LaunchTemplates'] ?: [] as $template) {
        if (!empty($stale[$template['LaunchTemplateName']])) {
          unset($stale[$template['LaunchTemplateName']]);
        }
        $batch_builder->addOperation([
          Ec2BatchOperations::class,
          'updateCloudLaunchTemplate',
        ], [$this->cloudContext, $template]);
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateCloudLaunchTemplatesWithoutBatch(array $params = [], $clear = TRUE): bool {
    $updated = FALSE;
    $entity_type = 'cloud_launch_template';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    // Load all entities by cloud_context.
    $entities = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadByProperties([
        'type' => 'aws_cloud',
        'cloud_context' => $this->cloudContext,
      ]);

    $result = $this->describeLaunchTemplates($params);

    if (!empty($result)) {

      $stale = [];
      foreach ($entities ?: [] as $entity) {
        $stale[$entity->getName()] = $entity;
      }

      foreach ($result['LaunchTemplates'] ?: [] as $template) {
        if (!empty($stale[$template['LaunchTemplateName']])) {
          unset($stale[$template['LaunchTemplateName']]);
        }

        Ec2BatchOperations::updateCloudLaunchTemplate($this->cloudContext, $template);
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = TRUE;
    }
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function setupIpPermissions(&$security_group, $field, array $ec2_permissions): void {
    // Permissions are always overwritten with the latest from
    // EC2.  The reason is that there is no way to guarantee a 1
    // to 1 mapping from the $security_group['IpPermissions'] array.
    // There is no IP permission ID coming back from EC2.
    // Clear out all items before re-adding them.
    $i = $security_group->$field->count() - 1;
    while ($i >= 0) {
      if ($security_group->$field->get($i)) {
        $security_group->$field->removeItem($i);
      }
      $i--;
    }

    // Setup all permission objects.
    $count = 0;
    foreach ($ec2_permissions ?: [] as $permissions) {
      $permission_objects = $this->setupIpPermissionObject($permissions);
      // Loop through the permission objects and add them to the
      // security group ip_permission field.
      foreach ($permission_objects ?: [] as $permission) {
        $security_group->$field->set($count, $permission);
        $count++;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setupIpPermissionObject(array $ec2_permission) {
    $ip_permissions = [];

    // Get the field definition for an IpPermission object.
    $definition = $this->entityFieldManager->getBaseFieldDefinitions('aws_cloud_security_group');

    /* Set up the more global attributes.
     * NOTE: DO NOT change from isset() to !empty() here since
     * !empty($ec2_permission['FromPort']) is considered empty
     * in PHP when $ec2_permission['FromPort'] is a zero (0) value.
     */
    $from_port = isset($ec2_permission['FromPort']) ? (string) ($ec2_permission['FromPort']) : NULL;
    $to_port = isset($ec2_permission['ToPort']) ? (string) ($ec2_permission['ToPort']) : NULL;
    $ip_protocol = $ec2_permission['IpProtocol'];

    // To keep things consistent, if ip_protocol is -1,
    // set from_port and to_port as 0-65535.
    if ($ip_protocol === '-1') {
      $from_port = '0';
      $to_port = '65535';
    }

    if (!empty($ec2_permission['IpRanges'])) {
      // Create a IPv4 permission object.
      foreach ($ec2_permission['IpRanges'] ?: [] as $ip_range) {
        $ip_range_permission = $this->fieldTypePluginManager->createInstance('ip_permission', [
          'field_definition' => $definition['ip_permission'],
          'parent' => NULL,
          'name' => NULL,
        ]);
        // Source is an internal identifier.  Does not come from EC2.
        $ip_range_permission->source = 'ip4';
        $ip_range_permission->cidr_ip = $ip_range['CidrIp'];
        $ip_range_permission->description = $ip_range['Description'] ?? NULL;

        $ip_range_permission->from_port = $from_port;
        $ip_range_permission->to_port = $to_port;
        $ip_range_permission->ip_protocol = $ip_protocol;

        $ip_permissions[] = $ip_range_permission;
      }
    }

    if (!empty($ec2_permission['Ipv6Ranges'])) {
      // Create IPv6 permissions object.
      foreach ($ec2_permission['Ipv6Ranges'] ?: [] as $ip_range) {
        $ip_v6_permission = $this->fieldTypePluginManager->createInstance('ip_permission', [
          'field_definition' => $definition['ip_permission'],
          'parent' => NULL,
          'name' => NULL,
        ]);
        // Source is an internal identifier.  Does not come from EC2.
        $ip_v6_permission->source = 'ip6';
        $ip_v6_permission->cidr_ip_v6 = $ip_range['CidrIpv6'];
        $ip_v6_permission->description = $ip_range['Description'] ?? NULL;
        $ip_v6_permission->from_port = $from_port;
        $ip_v6_permission->to_port = $to_port;
        $ip_v6_permission->ip_protocol = $ip_protocol;
        $ip_permissions[] = $ip_v6_permission;
      }
    }

    if (!empty($ec2_permission['UserIdGroupPairs'])) {
      // Create Group permissions object.
      foreach ($ec2_permission['UserIdGroupPairs'] ?: [] as $group) {
        $group_permission = $this->fieldTypePluginManager->createInstance('ip_permission', [
          'field_definition' => $definition['ip_permission'],
          'parent' => NULL,
          'name' => NULL,
        ]);
        // Source is an internal identifier.  Does not come from EC2.
        $group_permission->source = 'group';
        $group_permission->group_id = $group['GroupId'] ?? NULL;
        $group_permission->group_name = $group['GroupName'] ?? NULL;
        $group_permission->user_id = $group['UserId'] ?? NULL;
        $group_permission->peering_status = $group['PeeringStatus'] ?? NULL;
        $group_permission->vpc_id = $group['VpcId'] ?? NULL;
        $group_permission->peering_connection_id = $group['VpcPeeringConnectionId'] ?? NULL;
        $group_permission->description = $group['Description'] ?? NULL;
        $group_permission->from_port = $from_port;
        $group_permission->to_port = $to_port;
        $group_permission->ip_protocol = $ip_protocol;
        $ip_permissions[] = $group_permission;
      }
    }

    if (!empty($ec2_permission['PrefixListIds'])) {
      foreach ($ec2_permission['PrefixListIds'] ?: [] as $prefix) {
        $prefix_permission = $this->fieldTypePluginManager->createInstance('ip_permission', [
          'field_definition' => $definition['ip_permission'],
          'parent' => NULL,
          'name' => NULL,
        ]);
        // Source is an internal identifier.  Does not come from EC2.
        $prefix_permission->source = 'prefix';
        $prefix_permission->prefix_list_id = $prefix['PrefixListId'];
        $prefix_permission->description = $prefix['Description'] ?? NULL;
        $prefix_permission->from_port = $from_port;
        $prefix_permission->to_port = $to_port;
        $prefix_permission->ip_protocol = $ip_protocol;
        $ip_permissions[] = $prefix_permission;
      }
    }

    return $ip_permissions;
  }

  /**
   * Call API for updated entities and store them as NetworkInterface entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateNetworkInterfaceEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE) {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $result = $this->describeNetworkInterfaces($params);

    $properties = empty($params['NetworkInterfaceIds'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'network_interface_id' => $params['NetworkInterfaceIds'][0],
    ];
    $all_interfaces = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the groups by setting up
    // the array with the group_id.
    foreach ($all_interfaces ?: [] as $interface) {
      $stale[$interface->getNetworkInterfaceId()] = $interface;
    }

    /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
    $batch_builder = $this->initBatch('Network interface update');

    if (!empty($result)) {
      foreach ($result['NetworkInterfaces'] ?: [] as $network_interface) {
        // Keep track of network interfaces that do not exist anymore
        // delete them after saving the rest of the network interfaces.
        if (!empty($stale[$network_interface['NetworkInterfaceId']])) {
          unset($stale[$network_interface['NetworkInterfaceId']]);
        }
        if ($update) {
          $batch_builder->addOperation([
            Ec2BatchOperations::class,
            'updateNetworkInterface',
          ], [$cloud_context, $network_interface]);
        }
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = empty($params['NetworkInterfaceIds'][0]) ? TRUE : !empty($result['NetworkInterfaces']);
    }
    elseif (!empty($params['NetworkInterfaceIds'][0])) {
      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
    }
    return $updated;
  }

  /**
   * Update the EC2NetworkInterfaces.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateNetworkInterfaces(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_network_interface';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateNetworkInterfaceEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all NetworkInterfaces of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllNetworkInterfaces(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_network_interface';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateNetworkInterfaceEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as Elastic Ip entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   IP address param.
   * @param bool $clear
   *   TRUE to delete stale entities.
   * @param bool $update
   *   TRUE to update EI info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateElasticIpEntities($entity_type = '', $cloud_context = '', array $params = [], $clear = TRUE, $update = TRUE) {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $result = $this->describeAddresses($params);
    $properties = empty($params['PublicIps'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'public_ip' => $params['PublicIps'][0],
    ];
    $all_ips = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the groups by setting up
    // the array with the group_id.
    foreach ($all_ips ?: [] as $ip) {
      if (empty($stale[$ip->getPublicIp()])) {
        $stale[$ip->getPublicIp()] = $ip;
      }
      else {
        $ip->delete();
      }
    }

    /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
    $batch_builder = $this->initBatch('ElasticIp Update');

    if (!empty($result)) {
      foreach ($result['Addresses'] ?: [] as $elastic_ip) {
        $public_ip = $elastic_ip['PublicIp'] ?? $elastic_ip['CarrierIp'];

        // Keep track of IPs that do not exist anymore
        // delete them after saving the rest of the IPs.
        if (!empty($stale[$public_ip])) {
          unset($stale[$public_ip]);
        }
        if ($update) {
          $batch_builder->addOperation([
            Ec2BatchOperations::class,
            'updateElasticIp',
          ], [$cloud_context, $elastic_ip]);
        }
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = empty($params['PublicIps'][0]) ? TRUE : !empty($result['Addresses']);
    }
    elseif (!empty($params['PublicIps'][0])) {
      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
    }
    return $updated;
  }

  /**
   * Update the EC2ElasticIps.
   *
   * @param array $params
   *   Parameters.
   * @param bool $clear
   *   TRUE to delete stale entities.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateElasticIps(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_elastic_ip';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateElasticIpEntities($entity_type, $this->cloudContext, $params, $clear);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all Elastic Ips of all cloud region.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllElasticIps() {
    $entity_type = 'aws_cloud_elastic_ip';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateElasticIpEntities($entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as KeyPair entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   Key name info.
   * @param bool $update
   *   TRUE to update Key info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateKeyPairEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE) {
    $updated = FALSE;
    $result = $this->describeKeyPairs($params);
    // Load all entities by cloud_context.
    $properties = empty($params['KeyNames'][0])
      ? ['cloud_context' => $cloud_context]
      : [
        'cloud_context' => $cloud_context,
        'key_pair_name' => $params['KeyNames'][0],
      ];
    $all_keys = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the groups by setting up
    // the array with the group_id.
    foreach ($all_keys ?: [] as $key) {
      $stale[$key->getKeyPairName()] = $key;
    }
    /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
    $batch_builder = $this->initBatch('Keypair Update');

    if (!empty($result)) {
      foreach ($result['KeyPairs'] ?: [] as $key_pair) {
        // Keep track of key pair that do not exist anymore
        // delete them after saving the rest of the key pair.
        if (!empty($stale[$key_pair['KeyName']])) {
          unset($stale[$key_pair['KeyName']]);
        }
        if ($update) {
          $batch_builder->addOperation([
            Ec2BatchOperations::class,
            'updateKeyPair',
          ], [$cloud_context, $key_pair]);
        }
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = empty($params['KeyNames'][0]) ? TRUE : !empty($result['KeyPairs']);
    }
    elseif (!empty($params['KeyNames'][0])) {
      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
    }
    return $updated;
  }

  /**
   * Update the EC2KeyPairs.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateKeyPairs() {
    $entity_type = 'aws_cloud_key_pair';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateKeyPairEntities($entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all KeyPairs of all cloud region.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllKeyPairs() {
    $entity_type = 'aws_cloud_key_pair';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateKeyPairEntities($entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as Volume entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   The volume info.
   * @param bool $update
   *   TRUE to update volume.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateVolumeEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE) {
    $updated = FALSE;
    $result = $this->describeVolumes($params);
    $properties = empty($params['VolumeIds'][0])
      ? ['cloud_context' => $cloud_context]
      : [
        'cloud_context' => $cloud_context,
        'volume_id' => $params['VolumeIds'][0],
      ];
    $all_volumes = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the groups by setting up
    // the array with the group_id.
    foreach ($all_volumes ?: [] as $volume) {

      if (empty($volume)) {
        continue;
      }
      $stale[$volume->getVolumeId()] = $volume;
    }

    /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
    $batch_builder = $this->initBatch('Volume update');

    if (!empty($result)) {

      $snapshot_id_name_map = $this->getSnapshotIdNameMap($result['Volumes'] ?: []);
      foreach ($result['Volumes'] ?: [] as $volume) {

        if (empty($volume)) {
          continue;
        }

        // Keep track of network interfaces that do not exist anymore
        // delete them after saving the rest of the network interfaces.
        if (!empty($stale[$volume['VolumeId']])) {
          unset($stale[$volume['VolumeId']]);
        }
        if ($update) {
          $batch_builder->addOperation([
            Ec2BatchOperations::class,
            'updateVolume',
          ], [$cloud_context, $volume, $snapshot_id_name_map]);
        }
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = empty($params['VolumeIds'][0]) ? TRUE : !empty($result['Volumes']);
    }
    elseif (!empty($params['VolumeIds'][0])) {
      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
    }
    return $updated;
  }

  /**
   * Update the EC2Volumes.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateVolumes() {
    $entity_type = 'aws_cloud_volume';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateVolumeEntities($entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all Volumes of all cloud region.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllVolumes() {
    $entity_type = 'aws_cloud_volume';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateVolumeEntities($entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as snapshot entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   The snapshot IDs.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSnapshotEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE) {
    $updated = FALSE;
    $result = $this->describeSnapshots($params);

    $properties = empty($params['SnapshotIds'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'snapshot_id' => $params['SnapshotIds'][0],
    ];
    $all_snapshots = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up
    // the array with the snapshot_id.
    foreach ($all_snapshots ?: [] as $snapshot) {
      $stale[$snapshot->getSnapshotId()] = $snapshot;
    }
    /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
    $batch_builder = $this->initBatch('Snapshot Update');

    if (!empty($result)) {
      foreach ($result['Snapshots'] ?: [] as $snapshot) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$snapshot['SnapshotId']])) {
          unset($stale[$snapshot['SnapshotId']]);
        }
        if ($update) {
          $batch_builder->addOperation([
            Ec2BatchOperations::class,
            'updateSnapshot',
          ], [$cloud_context, $snapshot]);
        }
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = empty($params['SnapshotIds'][0]) ? TRUE : !empty($result['Snapshots']);
    }
    elseif (!empty($params['SnapshotIds'][0])) {
      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
    }

    return $updated;
  }

  /**
   * Update the EC2Snapshots.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSnapshots() {
    $entity_type = 'aws_cloud_snapshot';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSnapshotEntities($entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all snapshots of all cloud region.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllSnapshots() {
    $entity_type = 'aws_cloud_snapshot';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSnapshotEntities($entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as Vpc entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateVpcEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '') {
    $updated = FALSE;
    $result = $this->describeVpcs($params);

    if (!empty($result)) {

      $all_vpcs = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the snapshot by setting up
      // the array with the vpc_id.
      foreach ($all_vpcs ?: [] as $vpc) {
        $stale[$vpc->getVpcId()] = $vpc;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('VPC Update');

      foreach ($result['Vpcs'] ?: [] as $vpc) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$vpc['VpcId']])) {
          unset($stale[$vpc['VpcId']]);
        }

        $batch_builder->addOperation([
          Ec2BatchOperations::class,
          'updateVpc',
        ], [$this->cloudContext, $vpc]);
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the EC2Vpcs.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateVpcs(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_vpc';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateVpcEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateVpcsWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE) {
    $updated = FALSE;
    $entity_type = 'aws_cloud_vpc';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = empty($params['VpcIds'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'vpc_id' => $params['VpcIds'][0],
    ];
    $all_vpcs = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up
    // the array with the vpc_id.
    foreach ($all_vpcs ?: [] as $vpc) {
      $stale[$vpc->getVpcId()] = $vpc;
    }

    $result = $this->describeVpcs($params);

    if (!empty($result)) {
      foreach ($result['Vpcs'] ?: [] as $vpc) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$vpc['VpcId']])) {
          unset($stale[$vpc['VpcId']]);
        }
        if ($update) {
          Ec2BatchOperations::updateVpc($this->cloudContext, $vpc);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = empty($params['VpcIds'][0]) ? TRUE : !empty($result['Vpcs']);
    }
    elseif (!empty($params['VpcIds'][0]) && $clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all Vpcs of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllVpcs(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_vpc';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateVpcEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities.
   *
   * Store them as VpcPeeringConnection entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateVpcPeeringConnectionEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '') {
    $updated = FALSE;
    $result = $this->describeVpcPeeringConnections($params);
    if (!empty($result)) {

      $all_vpc_peering_connections = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the snapshot by setting up
      // the array with the vpc_id.
      foreach ($all_vpc_peering_connections as $vpc_peering_connection) {
        /** @var \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnectionInterface $vpc_peering_connection */
        $stale[$vpc_peering_connection->getVpcPeeringConnectionId()] = $vpc_peering_connection;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('VPC peering connection update');

      foreach ($result['VpcPeeringConnections'] ?: [] as $vpc_peering_connection) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$vpc_peering_connection['VpcPeeringConnectionId']])) {
          unset($stale[$vpc_peering_connection['VpcPeeringConnectionId']]);
        }

        $batch_builder->addOperation([
          Ec2BatchOperations::class,
          'updateVpcPeeringConnection',
        ], [$cloud_context, $vpc_peering_connection]);
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the EC2VpcPeeringConnections.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateVpcPeeringConnections(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_vpc_peering_connection';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateVpcPeeringConnectionEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all VpcPeeringConnections of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllVpcPeeringConnections(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_vpc_peering_connection';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateVpcPeeringConnectionEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateVpcPeeringConnectionsWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE) {
    $updated = FALSE;
    $entity_type = 'aws_cloud_vpc_peering_connection';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $result = $this->describeVpcPeeringConnections($params);

    $properties = empty($params['VpcPeeringConnectionIds'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'vpc_peering_connection_id' => $params['VpcPeeringConnectionIds'][0],
    ];
    $all_vpc_peering_connections = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up
    // the array with the vpc_id.
    foreach ($all_vpc_peering_connections as $vpc_peering_connection) {
      /** @var \Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnectionInterface $vpc_peering_connection */
      $stale[$vpc_peering_connection->getVpcPeeringConnectionId()] = $vpc_peering_connection;
    }

    if (!empty($result)) {

      foreach ($result['VpcPeeringConnections'] ?: [] as $vpc_peering_connection) {
        if ($vpc_peering_connection['Status']['Code'] !== 'failed'
          && $vpc_peering_connection['Status']['Code'] !== 'deleted') {
          $updated = TRUE;
        }
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$vpc_peering_connection['VpcPeeringConnectionId']])) {
          if (!empty($params['VpcPeeringConnectionIds'][0])
            && $vpc_peering_connection['Status']['Code'] === 'deleted') {
            $this->messageUser($this->t("The @label %name has %status status. The VPC peering connection remains visible to the party that deleted it for 2 hours, and visible to the other party for 2 days. If the VPC peering connection was created within the same AWS account, the deleted request remains visible for 2 hours.", [
              '@label' => $stale[$vpc_peering_connection['VpcPeeringConnectionId']]->getEntityType()->getSingularLabel(),
              '%name' => $stale[$vpc_peering_connection['VpcPeeringConnectionId']]->getName(),
              '%status' => $vpc_peering_connection['Status']['Code'],
            ]), 'error');
          }
          elseif (!empty($params['VpcPeeringConnectionIds'][0])
            && $vpc_peering_connection['Status']['Code'] === 'failed') {
            $this->messageUser($this->t("The @label %name has %status status. During this state, it cannot be accepted, rejected, or deleted. The failed VPC peering connection remains visible to the requester for 2 hours.", [
              '@label' => $stale[$vpc_peering_connection['VpcPeeringConnectionId']]->getEntityType()->getSingularLabel(),
              '%name' => $stale[$vpc_peering_connection['VpcPeeringConnectionId']]->getName(),
              '%status' => $vpc_peering_connection['Status']['Code'],
            ]), 'error');
          }
          unset($stale[$vpc_peering_connection['VpcPeeringConnectionId']]);
        }

        if ($update) {
          Ec2BatchOperations::updateVpcPeeringConnection($this->cloudContext, $vpc_peering_connection);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }
      $updated = empty($params['VpcPeeringConnectionIds'][0]) ? TRUE : $updated;
    }
    elseif (!empty($params['VpcPeeringConnectionIds'][0]) && $clear) {
      $this->messageUser($this->t('The @label %name has already been deleted.', [
        '@label' => end($stale)->getEntityType()->getSingularLabel(),
        '%name' => end($stale)->getName(),
      ]), 'error');
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    $this->cloudService->invalidateCacheTags();
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as Subnet entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSubnetEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '') {
    $updated = FALSE;
    $result = $this->describeSubnets($params);

    if (!empty($result)) {

      $all_subnets = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the subnet by setting up
      // the array with the subnet_id.
      foreach ($all_subnets ?: [] as $subnet) {
        $stale[$subnet->getSubnetId()] = $subnet;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Subnet Update');

      foreach ($result['Subnets'] ?: [] as $subnet) {

        // Keep track of subnet that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$subnet['SubnetId']])) {
          unset($stale[$subnet['SubnetId']]);
        }

        $batch_builder->addOperation([
          Ec2BatchOperations::class,
          'updateSubnet',
        ], [$cloud_context, $subnet]);
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the EC2Subnets.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSubnets(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_subnet';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSubnetEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateSubnetsWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE) {
    $updated = FALSE;
    $entity_type = 'aws_cloud_subnet';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = empty($params['SubnetIds'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'subnet_id' => $params['SubnetIds'][0],
    ];
    $all_subnets = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the subnet by setting up
    // the array with the subnet_id.
    foreach ($all_subnets ?: [] as $subnet) {
      $stale[$subnet->getSubnetId()] = $subnet;
    }

    $result = $this->describeSubnets($params);

    if (!empty($result)) {
      foreach ($result['Subnets'] ?: [] as $subnet) {
        // Keep track of subnet that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$subnet['SubnetId']])) {
          unset($stale[$subnet['SubnetId']]);
        }
        if ($update) {
          Ec2BatchOperations::updateSubnet($this->cloudContext, $subnet);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = empty($params['SubnetIds'][0]) ? TRUE : !empty($result['Subnets']);
    }
    elseif (!empty($params['SubnetIds'][0]) && $clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all Subnets of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllSubnets(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_subnet';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSubnetEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API to update internet gateway entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateInternetGatewayEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '') {
    $updated = FALSE;
    $result = $this->describeInternetGateways($params);

    if (!empty($result)) {
      $all_internet_gateways = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the snapshot by setting up
      // the array with the internet_gateway_id.
      foreach ($all_internet_gateways ?: [] as $internet_gateway) {
        $stale[$internet_gateway->getInternetGatewayId()] = $internet_gateway;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Internet gateway update');

      foreach ($result['InternetGateways'] ?: [] as $internet_gateway) {
        if (!empty($stale[$internet_gateway['InternetGatewayId']])) {
          unset($stale[$internet_gateway['InternetGatewayId']]);
        }

        $batch_builder->addOperation([
          Ec2BatchOperations::class,
          'updateInternetGateway',
        ], [$this->cloudContext, $internet_gateway]);
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the internet gateways.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateInternetGateways(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_internet_gateway';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateInternetGatewayEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateInternetGatewaysWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE) {
    $updated = FALSE;
    $entity_type = 'aws_cloud_internet_gateway';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = empty($params['InternetGatewayIds'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'internet_gateway_id' => $params['InternetGatewayIds'][0],
    ];
    $all_internet_gateways = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up
    // the array with the internet_gateway_id.
    foreach ($all_internet_gateways ?: [] as $internet_gateway) {
      $stale[$internet_gateway->getInternetGatewayId()] = $internet_gateway;
    }

    $result = $this->describeInternetGateways($params);

    if (!empty($result)) {
      foreach ($result['InternetGateways'] ?: [] as $internet_gateway) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$internet_gateway['InternetGatewayId']])) {
          unset($stale[$internet_gateway['InternetGatewayId']]);
        }
        if ($update) {
          Ec2BatchOperations::updateInternetGateway($this->cloudContext, $internet_gateway);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = empty($params['InternetGatewayIds'][0]) ? TRUE : !empty($result['InternetGateways']);
    }
    elseif (!empty($params['InternetGatewayIds'][0]) && $clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all internet gateways of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllInternetGateways(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_internet_gateway';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateInternetGatewayEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API to update carrier gateway entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateCarrierGatewayEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '') {
    $updated = FALSE;
    $result = $this->describeCarrierGateways($params);

    if (!empty($result)) {
      $all_carrier_gateways = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the snapshot by setting up
      // the array with the carrier_gateway_id.
      foreach ($all_carrier_gateways ?: [] as $carrier_gateway) {
        $stale[$carrier_gateway->getCarrierGatewayId()] = $carrier_gateway;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Carrier gateway update');

      foreach ($result['CarrierGateways'] ?: [] as $carrier_gateway) {
        if (!empty($stale[$carrier_gateway['CarrierGatewayId']])) {
          unset($stale[$carrier_gateway['CarrierGatewayId']]);
        }

        $batch_builder->addOperation([
          Ec2BatchOperations::class,
          'updateCarrierGateway',
        ], [$this->cloudContext, $carrier_gateway]);
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the carrier gateways.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateCarrierGateways(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_carrier_gateway';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateCarrierGatewayEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateCarrierGatewaysWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE) {
    $updated = FALSE;
    $entity_type = 'aws_cloud_carrier_gateway';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = empty($params['CarrierGatewayIds'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'carrier_gateway_id' => $params['CarrierGatewayIds'][0],
    ];
    $all_carrier_gateways = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up
    // the array with the carrier_gateway_id.
    foreach ($all_carrier_gateways ?: [] as $carrier_gateway) {
      $stale[$carrier_gateway->getCarrierGatewayId()] = $carrier_gateway;
    }

    $result = $this->describeCarrierGateways($params);

    if (!empty($result)) {
      foreach ($result['CarrierGateways'] ?: [] as $carrier_gateway) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$carrier_gateway['CarrierGatewayId']])) {
          unset($stale[$carrier_gateway['CarrierGatewayId']]);
        }
        if ($update) {
          Ec2BatchOperations::updateCarrierGateway($this->cloudContext, $carrier_gateway);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = empty($params['CarrierGatewayIds'][0]) ? TRUE : !empty($result['CarrierGateways']);
    }
    elseif (!empty($params['CarrierGatewayIds'][0]) && $clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all carrier gateways of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllCarrierGateways(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_carrier_gateway';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateCarrierGatewayEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API to update transit gateway entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateTransitGatewayEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '') {
    $updated = FALSE;
    $result = $this->describeTransitGateways($params);

    if (!empty($result)) {
      $all_transit_gateways = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the snapshot by setting up
      // the array with the transit_gateway_id.
      foreach ($all_transit_gateways ?: [] as $transit_gateway) {
        $stale[$transit_gateway->getTransitGatewayId()] = $transit_gateway;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Transit gateway update');

      foreach ($result['TransitGateways'] ?: [] as $transit_gateway) {
        if (!empty($stale[$transit_gateway['TransitGatewayId']])) {
          unset($stale[$transit_gateway['TransitGatewayId']]);
        }

        $batch_builder->addOperation([
          Ec2BatchOperations::class,
          'updateTransitGateway',
        ], [$this->cloudContext, $transit_gateway]);
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the transit gateways.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateTransitGateways(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_transit_gateway';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateTransitGatewayEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateTransitGatewaysWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE) {
    $updated = FALSE;
    $entity_type = 'aws_cloud_transit_gateway';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = empty($params['TransitGatewayIds'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'transit_gateway_id' => $params['TransitGatewayIds'][0],
    ];
    $all_transit_gateways = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up
    // the array with the transit_gateway_id.
    foreach ($all_transit_gateways ?: [] as $transit_gateway) {
      $stale[$transit_gateway->getTransitGatewayId()] = $transit_gateway;
    }

    $result = $this->describeTransitGateways($params);

    if (!empty($result)) {
      foreach ($result['TransitGateways'] ?: [] as $transit_gateway) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$transit_gateway['TransitGatewayId']])) {
          unset($stale[$transit_gateway['TransitGatewayId']]);
        }
        if ($update) {
          Ec2BatchOperations::updateTransitGateway($this->cloudContext, $transit_gateway);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = empty($params['TransitGatewayIds'][0]) ? TRUE : !empty($result['TransitGateways']);
    }
    elseif (!empty($params['TransitGatewayIds'][0]) && $clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all transit gateways of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllTransitGateways(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_transit_gateway';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateTransitGatewayEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API to update Availability Zone entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAvailabilityZoneEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '') {
    $updated = FALSE;
    $result = $this->describeAvailabilityZones($params);

    if (!empty($result)) {
      $all_availability_zones = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the snapshot by setting up
      // the array with the transit_gateway_id.
      foreach ($all_availability_zones ?: [] as $availability_zone) {
        $stale[$availability_zone->getZoneId()] = $availability_zone;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Availability Zone Update');

      foreach ($result['AvailabilityZones'] ?: [] as $availability_zone) {
        if (!empty($stale[$availability_zone['ZoneId']])) {
          unset($stale[$availability_zone['ZoneId']]);
        }

        $batch_builder->addOperation([
          Ec2BatchOperations::class,
          'updateAvailabilityZone',
        ], [$this->cloudContext, $availability_zone]);
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the Availability Zones.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAvailabilityZones(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_availability_zone';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateAvailabilityZoneEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateAvailabilityZonesWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE) {
    $updated = FALSE;
    $entity_type = 'aws_cloud_availability_zone';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = empty($params['ZoneIds'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'zone_id' => $params['ZoneIds'][0],
    ];
    $all_availability_zones = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up
    // the array with the zone_id.
    foreach ($all_availability_zones ?: [] as $availability_zone) {
      $stale[$availability_zone->getZoneId()] = $availability_zone;
    }

    $result = $this->describeAvailabilityZones($params);

    if (!empty($result)) {
      foreach ($result['AvailabilityZones'] ?: [] as $availability_zone) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($availability_zone['ZoneId'])
          && !empty($stale[$availability_zone['ZoneId']])) {
          unset($stale[$availability_zone['ZoneId']]);
        }
        if ($update) {
          Ec2BatchOperations::updateAvailabilityZone($this->cloudContext, $availability_zone);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = empty($params['ZoneIds'][0]) ? TRUE : !empty($result['AvailabilityZones']);
    }
    elseif (!empty($params['ZoneIds'][0]) && $clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all Availability Zones of all cloud region.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllAvailabilityZones(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_availability_zone';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateAvailabilityZoneEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API to update transit gateway route table entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateTransitGatewayRouteEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '') {
    $updated = FALSE;
    $result = $this->describeTransitGatewayRouteTables($params);

    if (!empty($result)) {
      $all_transit_gateway_route_tables = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the snapshot by setting up
      // the array with the transit_gateway_id.
      foreach ($all_transit_gateway_route_tables ?: [] as $transit_gateway_route_table) {
        $stale[$transit_gateway_route_table->getTransitGatewayRouteTableId()] = $transit_gateway_route_table;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Transit gateway route table update');

      foreach ($result['TransitGatewayRouteTables'] ?: [] as $transit_gateway_route_table) {
        if (!empty($stale[$transit_gateway_route_table['TransitGatewayRouteTableId']])) {
          unset($stale[$transit_gateway_route_table['TransitGatewayRouteTableId']]);
        }

        $batch_builder->addOperation([
          Ec2BatchOperations::class,
          'updateTransitGatewayRoute',
        ], [$this->cloudContext, $transit_gateway_route_table]);
      }

      $batch_builder->addOperation([
        Ec2BatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the transit gateway route.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale transit gateway route tables.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateTransitGatewayRoutes(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_transit_gateway_route';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateTransitGatewayRouteEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateTransitGatewayRoutesWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE) {
    $updated = FALSE;
    $entity_type = 'aws_cloud_transit_gateway_route';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = empty($params['TransitGatewayRouteTableIds'][0]) ? ['cloud_context' => $this->cloudContext] : [
      'cloud_context' => $this->cloudContext,
      'transit_gateway_route_table_id' => $params['TransitGatewayRouteTableIds'][0],
    ];
    $all_transit_gateway_route_tables = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up
    // the array with the transit_gateway_route_table_id.
    foreach ($all_transit_gateway_route_tables ?: [] as $transit_gateway_route_table) {
      $stale[$transit_gateway_route_table->getTransitGatewayRouteTableId()] = $transit_gateway_route_table;
    }

    $result = $this->describeTransitGatewayRouteTables($params);

    if (!empty($result)) {
      foreach ($result['TransitGatewayRouteTables'] ?: [] as $transit_gateway_route_table) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$transit_gateway_route_table['TransitGatewayRouteTableId']])) {
          unset($stale[$transit_gateway_route_table['TransitGatewayRouteTableId']]);
        }
        if ($update) {
          Ec2BatchOperations::updateTransitGatewayRoute($this->cloudContext, $transit_gateway_route_table);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = empty($params['TransitGatewayRouteTableIds'][0]) ? TRUE : !empty($result['TransitGatewayRouteTables']);
    }
    elseif (!empty($params['TransitGatewayRouteTableIds'][0]) && $clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update all transit gateway route tables.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale transit gateway route tables.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateAllTransitGatewayRoutes(array $params = [], $clear = TRUE) {
    $entity_type = 'aws_cloud_transit_gateway_route';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateTransitGatewayRouteEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function getVpcs() {
    $vpcs = [];
    $result = $this->describeVpcs();
    if (!empty($result)) {
      foreach ($result['Vpcs'] ?: [] as $vpc) {
        if (empty($vpc['VpcId'])) {
          continue;
        }

        $name = '';
        foreach ($vpc['Tags'] ?? [] as $tag) {
          if ($tag['Key'] === 'Name') {
            $name = $tag['Value'];
            break;
          }
        }
        $vpcs[$vpc['VpcId']] = (!empty($name) ? $name : $vpc['VpcId']) . " ({$vpc['VpcId']} | {$vpc['CidrBlock']})";
      }
    }
    return $vpcs;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZones($with_id = FALSE, array $credentials = []): array {
    $zones = [];
    $result = $this->describeAvailabilityZones([], $credentials);
    if (!empty($result)) {
      foreach ($result['AvailabilityZones'] ?: [] as $zone_info) {
        if ($with_id) {
          $name = sprintf('%s (%s)', $zone_info['ZoneName'], $zone_info['ZoneId']);
        }
        else {
          $name = $zone_info['ZoneName'];
        }
        $zones[$zone_info['ZoneName']] = $name;
      }
    }
    return $zones;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedPlatforms(array $credentials = []): array {
    $platforms = [];
    $result = $this->describeAccountAttributes([
      'AttributeNames' => [
        'supported-platforms',
      ],
    ], $credentials);
    if (!empty($result)) {
      foreach ($result['AccountAttributes'] ?: [] as $attribute) {
        if ($attribute['AttributeName'] === 'supported-platforms') {
          foreach ($attribute['AttributeValues'] ?: [] as $value) {
            $platforms[] = $value['AttributeValue'];
          }
        }
      }
    }
    return $platforms;
  }

  /**
   * {@inheritdoc}
   */
  public function updateServerGroupsWithoutBatch(array $params = [], bool $clear = TRUE, bool $update = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsoleOutput(array $params = [], array $credentials = []): ?ResultInterface {
    if (!empty($params['DryRun'])) {
      // Set required parameters if missing.
      $params += ['InstanceId' => "i-{$this->random->name(32, TRUE)}"];
    }
    return $this->execute('GetConsoleOutput', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function getSnapshotIdNameMap(array $volumes = []) {

    $snapshot_ids = array_filter(array_column($volumes, 'SnapshotId'));
    if (empty($snapshot_ids)) {
      return [];
    }

    $map = [];
    foreach ($snapshot_ids ?: [] as $snapshot_id) {
      $map[$snapshot_id] = '';
    }

    $result = $this->describeSnapshots();

    if (!empty($result)) {

      foreach ($result['Snapshots'] ?: [] as $snapshot) {
        $snapshot_id = $snapshot['SnapshotId'];
        if (!array_key_exists($snapshot_id, $map)) {
          continue;
        }

        $map[$snapshot_id] = $this->getTagName($snapshot, '');
      }
    }

    return $map;
  }

  /**
   * {@inheritdoc}
   */
  public function getTagName(array $aws_obj, $default_value) {
    $name = $default_value;
    if (empty($aws_obj['Tags'])) {
      return $name;
    }

    foreach ($aws_obj['Tags'] ?: [] as $tag) {
      if (!empty($tag['Key']) && $tag['Key'] === 'Name' && !empty($tag['Value'])) {
        $name = $tag['Value'];
        break;
      }
    }
    return $name;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrivateIps(array $network_interfaces) {
    $ip_string = FALSE;
    $private_ips = [];
    foreach ($network_interfaces ?: [] as $interface) {
      $private_ips[] = $interface['PrivateIpAddress'];
    }
    if (count($private_ips)) {
      $ip_string = implode(', ', $private_ips);
    }
    return $ip_string;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateInstanceCost(array $instance, array $instance_types): ?float {
    $cost = NULL;
    if ($instance['State']['Name'] === 'stopped') {
      return NULL;
    }

    $instance_type = $instance['InstanceType'];
    if (!empty($instance_types[$instance_type])) {
      $parts = explode(':', $instance_types[$instance_type]);
      $hourly_rate = $parts[4];
      $launch_time = strtotime($instance['LaunchTime']->__toString());
      $cost = round((time() - $launch_time) / 3600 * floatval($hourly_rate), 2);
    }
    return $cost;
  }

  /**
   * {@inheritdoc}
   */
  public function getUidTagValue(array $tags_array, string $entity_type_id): int {
    $bundle = $this->entityTypeManager->getDefinition($entity_type_id)->getProvider();
    $key = $this->cloudService->getTagKeyCreatedByUid($bundle, $this->cloudContext);

    $uid = 0;
    if (!empty($tags_array['Tags'])) {
      foreach ($tags_array['Tags'] ?: [] as $tag) {
        if (!empty($tag['Key']) && $tag['Key'] === $key) {
          $uid = $tag['Value'];
          break;
        }
      }
    }
    return $uid;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceUid($instance_id): int {
    $uid = 0;
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface[] $instances */
    $instances = $this->entityTypeManager
      ->getStorage('aws_cloud_instance')
      ->loadByProperties([
        'instance_id' => $instance_id,
        'cloud_context' => $this->cloudContext,
      ]);

    if (count($instances) > 0) {
      $instance = array_shift($instances);
      $uid = $instance->getOwnerId();
    }
    return $uid;
  }

  /**
   * {@inheritdoc}
   */
  public function clearPluginCache(): void {
    $this->cloudConfigPluginManager->clearCachedDefinitions();
  }

  /**
   * Initialize a new batch builder.
   *
   * @param string $batch_name
   *   The batch name.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   The initialized batch object.
   */
  protected function initBatch($batch_name): BatchBuilder {
    return (new BatchBuilder())
      ->setTitle($batch_name);
  }

  /**
   * Run the batch job to process entities.
   *
   * @param \Drupal\Core\Batch\BatchBuilder $batch_builder
   *   The batch builder object.
   */
  protected function runBatch(BatchBuilder $batch_builder): void {
    try {
      // Log the start time.
      $start = $this->getTimestamp();
      $batch_array = $batch_builder->toArray();
      batch_set($batch_array);
      // Reset the progressive so batch works w/o a web head.
      $batch = &batch_get();
      $batch['progressive'] = FALSE;
      // Check if using Drush to process the queue.
      // using drush_backend_batch_process() will fix the "Route not found"
      // error.
      if (PHP_SAPI === 'cli' && function_exists('drush_backend_batch_process')) {
        drush_backend_batch_process();
      }
      else {
        batch_process();
      }
      // Log the end time.
      $end = $this->getTimestamp();
      $this->logger('ec2_service')->info($this->t('@updater - @cloud_context: Batch operation took @time seconds.', [
        '@cloud_context' => $this->cloudContext,
        '@updater' => $batch_array['title'],
        '@time' => $end - $start,
      ]));
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    finally {
      // Reset the batch otherwise this operation hangs when using Drush.
      // https://www.drupal.org/project/drupal/issues/3166042
      $batch = [];
    }
  }

  /**
   * Ping the metadata server for security credentials URL.
   *
   * @return bool
   *   TRUE if either 169.254.169.254 (EC2) or 169.254.170.2 (ECS) is
   *   accessible.
   */
  public function pingMetadataSecurityServer(): bool {

    $pinged = FALSE;

    // Instance profile (IAM instance role).
    $metadata_urls[] = 'http://169.254.169.254/latest/meta-data/iam/security-credentials/';

    // IAM roles for ECS Tasks.
    $metadata_urls[] = array_key_exists('AWS_CONTAINER_CREDENTIALS_RELATIVE_URI', $_ENV)
      ? "http://169.254.170.2{$_ENV['AWS_CONTAINER_CREDENTIALS_RELATIVE_URI']}"
      : NULL;

    foreach ($metadata_urls ?: [] as $metadata_url) {
      try {
        $this->httpClient->request('GET', $metadata_url, $this->getGuzzleOptions());
        $pinged = TRUE;
        break;
      }
      catch (\Exception $e) {
        $this->logger('aws_cloud')->info($e->getMessage());
        $pinged = FALSE;
      }
    }

    return $pinged;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceIdFromMetadata(): string {
    $metadata_url = 'http://169.254.169.254/latest/meta-data/instance-id';
    try {
      $response = $this->httpClient->request('GET', $metadata_url, $this->getGuzzleOptions());
      $instance_id = $response->getBody()->getContents();
    }
    catch (\Exception $e) {
      $this->logger('aws_cloud')->info($e->getMessage());
      $instance_id = '';
    }
    return $instance_id;
  }

  /**
   * {@inheritdoc}
   */
  public function isInstanceCloudOrchestrator($instance_id): bool {
    $is_cloud_orchestrator = FALSE;
    if ($this->pingMetadataSecurityServer() && $instance_id === $this->getInstanceIdFromMetadata()) {
      $is_cloud_orchestrator = TRUE;
    }
    return $is_cloud_orchestrator;
  }

  /**
   * Return an array of default guzzle options.
   *
   * @return array
   *   Default options for Guzzle.
   */
  private function getGuzzleOptions(): array {
    return [
      'connect_timeout' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function terminateExpiredInstances() {
    // Terminate instances past the timestamp.
    $instances = aws_cloud_get_expired_instances($this->cloudContext);
    if (!empty($instances)) {
      $this->logger('aws_cloud')->info($this->t('Terminating the following instances %instance', [
        '%instance' => implode(', ', $instances['InstanceIds']),
      ]));
      $this->terminateInstances($instances);
    }
  }

  /**
   * Get image IDs of pending images.
   *
   * @return array
   *   Images IDs of pending images.
   */
  private function getPendingImageIds() {
    $image_ids = [];
    $entity_storage = $this->entityTypeManager->getStorage('aws_cloud_image');
    $entity_ids = $entity_storage
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', 'pending')
      ->condition('cloud_context', $this->cloudContext)
      ->execute();
    $entities = $entity_storage->loadMultiple($entity_ids);
    foreach ($entities ?: [] as $entity) {
      $image_ids[] = $entity->getImageId();
    }
    return $image_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function updatePendingImages() {
    $image_ids = $this->getPendingImageIds();
    if (count($image_ids)) {
      $this->updateImages([
        'ImageIds' => $image_ids,
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createResourceQueueItems(CloudConfigInterface $cloud_config): void {
    $queue_limit = $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_queue_limit');
    $method_names = [
      'terminateExpiredInstances',
      'updatePendingImages',
      'updateAllAvailabilityZones',
      'updateInstances',
      'updateSecurityGroups',
      'updateKeyPairs',
      'updateElasticIps',
      'updateNetworkInterfaces',
      'updateSnapshots',
      'updateVolumes',
      'updateVpcs',
      'updateVpcPeeringConnections',
      'updateSubnets',
      'updateInternetGateways',
      'updateTransitGateways',
      'updateCarrierGateways',
      'updateTransitGatewayRoutes',
      'updateCloudLaunchTemplates',
    ];
    self::updateResourceQueue($cloud_config, $method_names, $queue_limit);
  }

  /**
   * Helper method to add messages for the end user.
   *
   * @param string $message
   *   The message.
   * @param string $type
   *   The message type: error or message.
   */
  private function messageUser($message, $type = 'message') {
    switch ($type) {
      case 'error':
        $this->messenger->addError($message);
        break;

      case 'message':
        $this->messenger->addStatus($message);
        break;

      default:
        break;
    }
  }

  /**
   * Get account ID.
   *
   * @param array $cloud_config_entities
   *   Cloud config entities array.
   *
   * @return string
   *   The Account ID.
   */
  public function getAccountId(array $cloud_config_entities) {
    $account_id = '';
    if (!empty($cloud_config_entities)) {
      $cloud_config = reset($cloud_config_entities);
      $account_id = $cloud_config->get('field_account_id')->value;
      // Use the switch role account_id if switching is enabled.
      $use_assume_role = $cloud_config->get('field_use_assume_role')->value ?? FALSE;
      $use_switch_role = $cloud_config->get('field_use_switch_role')->value ?? FALSE;
      if (!empty($use_assume_role) && !empty($use_switch_role)) {
        $account_id = trim($cloud_config->get('field_switch_role_account_id')->valu ?: '');
      }
    }

    return $account_id;
  }

  /**
   * Update entity message.
   *
   * @param string $entity_type
   *   The entity type.
   * @param object $cloud_config
   *   The cloud config object.
   * @param bool $updated
   *   TRUE if entity is updated.
   */
  private function updateEntityMessage($entity_type, $cloud_config, $updated) {
    $labels = $this->getDisplayLabels($entity_type);
    $resource_link = $cloud_config->getResourceLink($labels['plural'], $entity_type);
    $cloud_config_link = $cloud_config->getCloudConfigLink();

    $success_message = $this->t('Updated %resources of %cloud_config cloud service provider.', [
      '%resources' => $resource_link ?? $labels['plural'],
      '%cloud_config' => $cloud_config_link ?? $cloud_config->getName(),
    ]);

    $error_message = $this->t('Unable to update %resources of %cloud_config cloud service provider.', [
      '%resources' => $resource_link ?? $labels['plural'],
      '%cloud_config' => $cloud_config_link ?? $cloud_config->getName(),
    ]);

    $updated === TRUE
    ? $this->messageUser($success_message)
    : $this->messageUser($error_message, 'error');
  }

  /**
   * Helper method to update all entity list.
   *
   * @param string $entity_type_name
   *   The entity type name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function updateAllEntityList($entity_type_name) {
    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'aws_cloud',
      ]);

    $labels = $this->getDisplayLabels($entity_type_name);
    $update_method_name = 'updateAll' . self::getCamelCaseWithoutWhitespace($labels['plural']);

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Only update cloud service providers user has edit access to.
      // Use a static trait method through CloudService.
      $entity_name_with_space = CloudService::convertUnderscoreToWhitespace($entity_type_name);
      $access = $this->allowedIfCanAccessCloudConfigWithOwner(
        $cloud_config,
        $this->currentUser,
        "edit own {$entity_name_with_space}",
        "edit any {$entity_name_with_space}"
      );
      if ($access->isNeutral()) {
        continue;
      }
      try {
        switch ($entity_type_name) {
          case 'aws_cloud_image':
            $cloud_config_entities = $this->entityTypeManager->getStorage('cloud_config')->loadByProperties([
              'cloud_context' => [$cloud_context],
            ]);

            $cloud_config = !empty($cloud_config_entities) ? reset($cloud_config_entities) : [];
            $account_id = $this->getAccountId($cloud_config_entities);

            if (!empty($account_id)) {
              $this->setCloudContext($cloud_context);
              $updated = $this->updateAllImages([
                'Owners' => [
                  $account_id,
                ],
              ], TRUE);

              $this->updateEntityMessage('aws_cloud_image', $cloud_config, $updated);
            }
            else {
              $message = $this->t('AWS User ID is not specified.');
              $this->currentUser->hasPermission('edit cloud service providers')
                ? $message = Link::createFromRoute($message, 'entity.cloud_config.edit_form', ['cloud_config' => $cloud_config->id()])->toString()
                : '';
              $this->messageUser($message, 'error');
            }
            break;

          case 'aws_cloud_elastic_ip':
            $this->setCloudContext($cloud_context);
            $labels = $this->getDisplayLabels('aws_cloud_elastic_ip');
            $resource_link = $cloud_config->getResourceLink($labels['plural'], 'aws_cloud_elastic_ip');
            $cloud_config_link = $cloud_config->getCloudConfigLink();
            $updated = $this->$update_method_name();

            if ($updated === TRUE) {
              // Also update network interfaces.
              $network_interface_label = $this->getDisplayLabels('aws_cloud_network_interface');
              $network_interface_link = Link::fromTextAndUrl(
                $network_interface_label['plural'],
                Url::fromRoute(
                  "entity.aws_cloud_network_interface.collection",
                  ['cloud_context' => $cloud_config->getCloudContext()]
                )
              )->toString();

              if ($updated === TRUE) {
                // Also update network interfaces.
                $updated = $this->updateAllNetworkInterfaces();
                $success_message = $this->t('Updated %resources and %other_resource of %cloud_config cloud service provider.', [
                  '%resources' => $resource_link ?? $labels['plural'],
                  '%other_resource' => $network_interface_link,
                  '%cloud_config' => $cloud_config_link ?? $cloud_config->getName(),
                ]);

                $error_message = $this->t('Unable to update %other_resource while updating %resources of %cloud_config cloud service provider.', [
                  '%resources' => $resource_link ?? $labels['plural'],
                  '%other_resource' => $network_interface_link,
                  '%cloud_config' => $cloud_config_link ?? $cloud_config->getName(),
                ]);
              }
            }
            else {
              $error_message = $this->t('Unable to update %resources of %cloud_config cloud service provider.', [
                '%resources' => $resource_link ?? $labels['plural'],
                '%cloud_config' => $cloud_config_link ?? $cloud_config->getName(),
              ]);
            }
            $updated === TRUE
              ? $this->messageUser($success_message ?? '')
              : $this->messageUser($error_message, 'error');
            break;

          default:
            $this->setCloudContext($cloud_context);
            $updated = $this->$update_method_name();
            $this->updateEntityMessage($entity_type_name, $cloud_config, $updated);
        }
      }
      catch (\Exception $e) {
        $this->messenger->addError($this->t('Unable to update @resources of @cloud_config cloud service provider.', [
          '@resources' => $labels['plural'],
          '@cloud_config' => $cloud_config->getName(),
        ]));
      }
    }

    $this->cloudService->invalidateCacheTags();

    $redirect_url = Url::fromRoute("view.$entity_type_name.all");

    $redirect_response = new RedirectResponse($redirect_url->toString());
    $redirect_response->send();

    return $redirect_response;
  }

  /**
   * {@inheritdoc}
   */
  public function isWorkerResource(): bool {
    if (empty($this->cloudContext)) {
      return FALSE;
    }
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    return $cloud_config->isRemote();
  }

  /**
   * Save operation to queue.
   *
   * @param string $operation
   *   The operation to perform.
   * @param array $params
   *   An array of parameters.
   * @param string $update_entities_method
   *   The method name of updating entities.
   * @param array $update_entities_method_params
   *   The method parameters of updating entities.
   * @param array $refs
   *   The array of references.
   */
  private function saveOperation(string $operation, array $params, string $update_entities_method = '', ?array $update_entities_method_params = NULL, array $refs = []): void {
    if (empty($this->cloudContext)) {
      return;
    }

    // Skip describe operations.
    if (strpos($operation, 'Describe') === 0) {
      return;
    }

    // Skip dry run operations.
    if (!empty($params[0]['DryRun'])) {
      return;
    }

    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    if (!$cloud_config->isRemote()) {
      return;
    }

    if (empty($update_entities_method)) {
      $update_entities_method = preg_replace('/^[[:lower:]]+/', 'update', lcfirst($operation));
    }
    if ($update_entities_method_params === NULL) {
      $update_entities_method_params = [$params[0], FALSE];
    }
    else {
      $update_entities_method_params = [$update_entities_method_params, FALSE];
    }

    $queue = $this->queueFactory->get('operation_queue_' . $cloud_config->get('cloud_cluster_name')->value . $cloud_config->get('cloud_cluster_worker_name')->value);
    $queue->createItem([
      'cloud_context' => $this->cloudContext,
      'operation' => $operation,
      'params' => $params,
      'service_name' => 'aws_cloud.ec2',
      'update_entities_method' => $update_entities_method,
      'update_entities_method_params' => $update_entities_method_params,
      'refs' => $refs,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function addArchitecturesToInstanceTypes(array &$instance_types = []): void {
    $param = [
      'InstanceTypes' => [],
    ];

    $types = array_keys($instance_types);

    // Temporary keep error messages.
    $errors = $this->messenger->deleteByType('error');

    foreach ($types ?: [] as $idx => $type) {
      $param['InstanceTypes'][] = $type;

      // The maximum result count of DescribeInstanceTypes is 100.
      // When the count of InstanceTypes is 100 or $type is the last item,
      // execute describeInstanceTypes.
      if (count($param['InstanceTypes']) < 100 && $idx < (count($types) - 1)) {
        continue;
      }

      // Execute DescribeInstanceTypes.
      $desc_instance_types = $this->describeInstanceTypes($param);

      // Delete and confirm error messages.
      $messages = $this->messenger->deleteByType('error');
      if (!empty($messages)) {
        $exclude_types = [];
        foreach ($messages ?: [] as $message) {
          // If the message includes 'InvalidInstanceType',
          // there are some invalid instance types in the parameter.
          if (strpos($message, 'InvalidInstanceType') === FALSE) {
            continue;
          }

          $dom = new \DOMDocument();
          $dom->loadHTML($message);
          $items = [];
          foreach ($dom->getElementsByTagName('li') ?: [] as $node) {
            $items[] = $node->textContent;
          }

          foreach ($items ?: [] as $item) {
            if (strpos($item, 'Message:') === FALSE) {
              continue;
            }

            $matches = [];
            if (preg_match('/\[.*?\]/', $item, $matches)) {
              // Get the invalid instance types.
              $exclude_types = explode(', ', trim($matches[0], '[]'));
            }
          }
          break;
        }

        if (empty($exclude_types)) {
          $param['InstanceTypes'] = [];
          continue;
        }

        // Exclude the invalid instance types.
        // Then execute DescribeInstanceTypes again.
        $param['InstanceTypes'] = array_diff($param['InstanceTypes'], $exclude_types);
        $desc_instance_types = $this->describeInstanceTypes($param);
      }

      if (empty($desc_instance_types['InstanceTypes'])) {
        $param['InstanceTypes'] = [];
        continue;
      }

      foreach ($desc_instance_types['InstanceTypes'] ?: [] as $instance_type) {
        if (empty($instance_types[$instance_type['InstanceType']])) {
          continue;
        }

        if (empty($instance_type['ProcessorInfo']['SupportedArchitectures'])) {
          continue;
        }

        // Add the architecture to instance type.
        $instance_types[$instance_type['InstanceType']] .= ':';
        $instance_types[$instance_type['InstanceType']] .= implode(self::INSTANCE_TYPE_ARCHITECTURE_SEPARATOR, $instance_type['ProcessorInfo']['SupportedArchitectures']);
      }

      $param['InstanceTypes'] = [];
    }

    // Add temporary keeping error messages.
    foreach ($errors ?: [] as $error) {
      $this->messenger->addError($error);
    }

  }

}
