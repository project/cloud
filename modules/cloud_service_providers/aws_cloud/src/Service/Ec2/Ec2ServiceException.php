<?php

namespace Drupal\aws_cloud\Service\Ec2;

use Aws\Command;
use Aws\Ec2\Exception\Ec2Exception;

/**
 * AWS Cloud EC2 service exception.
 */
class Ec2ServiceException extends Ec2Exception {

  /**
   * Ec2ServiceException constructor.
   *
   * @param string $message
   *   Exception message.
   * @param string $command_name
   *   Aws command name.
   * @param string|null $error_code
   *   Aws error code to be set in the context.
   * @param array $context
   *   Exception context.
   * @param \Exception|null $previous
   *   The previous exception.
   */
  public function __construct(string $message, $command_name = 'Unknown', ?string $error_code = NULL, array $context = [], ?\Exception $previous = NULL) {

    $command = new Command(ucfirst($command_name));
    if (!empty($error_code)) {
      $context['code'] = $error_code;
    }
    parent::__construct($message, $command, $context, $previous);
  }

}
