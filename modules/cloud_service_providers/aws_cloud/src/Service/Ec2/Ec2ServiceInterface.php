<?php

namespace Drupal\aws_cloud\Service\Ec2;

use Aws\ResultInterface;
use Drupal\cloud\Entity\CloudConfigInterface;

/**
 * The Interface for Ec2Service.
 */
interface Ec2ServiceInterface {

  /**
   * AWS error code indicating permission granted.
   *
   * @FIXME share with S3 Service.
   *
   * @var string
   */
  public const DRY_RUN_OPERATION = 'DryRunOperation';

  /**
   * No IAM validation check option.
   *
   * @var string
   */
  public const IAM_VALIDATION_NO_CHECK = 'no_check';

  /**
   * IAM validation for one region.
   *
   * @var string
   */
  public const IAM_VALIDATION_ONE_REGION = 'one_region';

  /**
   * IAM validation for all region.
   *
   * @var string
   */
  public const IAM_VALIDATION_ALL_REGIONS = 'all_regions';

  /**
   * AWS error codes for permission failure.
   *
   * @FIXME Move to AWS common service level to share with other services,
   * i.e., S3 Service.
   *
   * @var string[]
   * @see https://docs.aws.amazon.com/AWSEC2/latest/APIReference/errors-overview.html
   */
  public const PERMISSION_ERROR_CODES = [
    'AccessDenied',
    'AssumeRoleDenied',
    'AuthFailure',
    'Blocked',
    'OptInRequired',
    'PendingVerification',
    'UnauthorizedOperation',
    'ValidationError',
  ];

  /**
   * Mapping from Drupal permission to AWS API.
   *
   * @var string[]
   */
  public const PERMISSION_TO_AWS_API = [
    'add aws cloud elastic ip' => 'AllocateAddress',
    'add aws cloud carrier gateway' => 'CreateCarrierGateway',
    'add aws cloud image' => 'CreateImage',
    'add aws cloud internet gateway' => 'CreateInternetGateway',
    'add aws cloud key pair' => 'CreateKeyPair',
    'add aws cloud network interface' => 'CreateNetworkInterface',
    'add aws cloud snapshot' => 'CreateSnapshot',
    'add aws cloud security group' => ['CreateSecurityGroup', 'CreateVpc'],
    'add aws cloud subnet' => 'CreateSubnet',
    'add aws cloud transit gateway' => 'CreateTransitGateway',
    'add aws cloud vpc' => 'CreateVpc',
    'add aws cloud vpc peering connection' => 'CreateVpcPeeringConnection',
    'add aws cloud volume' => 'CreateVolume',
    // @See https://docs.aws.amazon.com/autoscaling/ec2/userguide/ec2-auto-scaling-launch-template-permissions.html#policy-example-create-launch-template
    'add cloud server templates' => [
      'CreateLaunchTemplate',
      'CreateTags',
      'DescribeAvailabilityZones',
      'DescribeImages',
      'DescribeKeyPairs',
      'DescribeNetworkInterfaces',
      'DescribeSecurityGroups',
      'DescribeSubnets',
      'DescribeVpcs',
    ],
    'list cloud server template' => 'DescribeLaunchTemplates',
  ];

  /**
   * IAM validation for all region.
   *
   * @var string
   */
  public const INSTANCE_TYPE_ARCHITECTURE_SEPARATOR = ', ';

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function setCloudContext($cloud_context);

  /**
   * Calls the Amazon EC2 API endpoint AssociateAddress.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function associateAddress(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint AuthorizeSecurityGroupIngress.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   * @param array $refs
   *   Array of references.
   *
   * @return \Aws\ResultInterface
   *   Result or NULL if there is an error.
   */
  public function authorizeSecurityGroupIngress(array $params = [], array $credentials = [], array $refs = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint AuthorizeSecurityGroupEgress.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   * @param array $refs
   *   Array of references.
   *
   * @return \Aws\ResultInterface
   *   Result or NULL if there is an error.
   */
  public function authorizeSecurityGroupEgress(array $params = [], array $credentials = [], array $refs = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint AllocateAddress.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of ElasticIps or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function allocateAddress(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint AssociateIamInstanceProfile.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   An IamInstanceProfileAssociation or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function associateIamInstanceProfile(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DisassociateIamInstanceProfile.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   An IamInstanceProfileAssociation or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function disassociateIamInstanceProfile(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint ReplaceIamInstanceProfileAssociation.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   An IamInstanceProfileAssociation or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function replaceIamInstanceProfileAssociation(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeIamInstanceProfileAssociations.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of IamInstanceProfileAssociation or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeIamInstanceProfileAssociations(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint CreateImage.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of image or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createImage(array $params = []): ?ResultInterface;

  /**
   * Modifies the specified attribute of an image.
   *
   * @param array $params
   *   Parameters array to send to API.
   */
  public function modifyImageAttribute(array $params = []): ?ResultInterface;

  /**
   * Modifies transit gateway.
   *
   * @param array $params
   *   Parameters array to send to API.
   */
  public function modifyTransitGateway(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create key pair.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createKeyPair(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Create key pair entity.
   *
   * @param array $params
   *   Parameters to create key pair entity.
   */
  public function createKeyPairEntity(array $params): void;

  /**
   * Calls the Amazon EC2 API endpoint Create network interface.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createNetworkInterface(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Modifies the specified attribute of a network interface.
   *
   * @param array $params
   *   Parameters array to send to API.
   */
  public function modifyNetworkInterfaceAttribute(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create Volume.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createVolume(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Modify Volume.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of the volume or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function modifyVolume(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create snapshot.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createSnapshot(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create Vpc.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createVpc(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create internet gateway.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of internet gateway or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createInternetGateway(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create carrier gateway.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createCarrierGateway(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create transit gateway.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createTransitGateway(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create Flow logs.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of FlowLog or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createFlowLogs(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create VPC peering connection.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createVpcPeeringConnection(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Accept VPC peering connection.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of VPC or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function acceptVpcPeeringConnection(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Describe VPC peering connections.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of VPC or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeVpcPeeringConnections(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Describe flow logs.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of FlowLog or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeFlowLogs(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create security group.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createSecurityGroup(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Create Tags.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createTags(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Delete Tags.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteTags(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeregisterImage.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deregisterImage(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeInstances.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of instances or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeInstances(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeInstanceAttribute.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of instances or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeInstanceAttribute(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeInstanceTypes.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of instances or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeInstanceTypes(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeImages.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of images or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeImages(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeImageAttribute.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of images or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeImageAttribute(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeSecurityGroups.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of SecurityGroups or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeSecurityGroups(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeNetworkInterfaces.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of NetworkInterfaceList or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeNetworkInterfaces(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeAccountAttributes.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The credentials array.
   *
   * @return \Aws\ResultInterface
   *   Result or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeAccountAttributes(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeAddresses.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of Addresses or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeAddresses(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeSnapshots.
   *
   * Only snapshots restorable by the user are returned.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of snapshots or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeSnapshots(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeKeyPairs.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of KeyPairs or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeKeyPairs(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeVolumes.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of Volumes or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeVolumes(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeAvailabilityZones.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of AvailabilityZones or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeAvailabilityZones(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeVpcs.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of VPCs or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeVpcs(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeInternetGateways.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of VPCs or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeInternetGateways(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeCarrierGateways.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of VPCs or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeCarrierGateways(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeTransitGateways.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of VPCs or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeTransitGateways(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeTransitGatewayRouteTables.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of VPCs or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeTransitGatewayRouteTables(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint AssociateVpcCidrBlock.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of VPCs or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function associateVpcCidrBlock(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DisassociateVpcCidrBlock.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of VPCs or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function disassociateVpcCidrBlock(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DescribeSubnets.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of Subnets or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function describeSubnets(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint CreateSubnet.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function createSubnet(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Get regions.
   *
   * @return array
   *   Array of regions.
   */
  public function getRegions(): array;

  /**
   * Get endpoint URLs.
   *
   * @return array
   *   Array of region endpoint URLs.
   */
  public function getEndpointUrls(): array;

  /**
   * Calls the Amazon EC2 API endpoint ImportKeyPair.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of KeyPair or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function importKeyPair(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint RebootInstances.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   This call does not return anything.
   */
  public function rebootInstances(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint TerminateInstances.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of instance or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function terminateInstances(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteSecurityGroup.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteSecurityGroup(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteNetworkInterface.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteNetworkInterface(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint ReleaseAddress.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function releaseAddress(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteKeyPair.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteKeyPair(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteVolume.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteVolume(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteSnapshot.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteSnapshot(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteVpc.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteVpc(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteInternetGateway.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteInternetGateway(array $params = []): ?ResultInterface;

  /**
   * Attaches an internet gateway.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Response array or NULL if there is an error.
   */
  public function attachInternetGateway(array $params = []): ?ResultInterface;

  /**
   * Detaches an internet gateway.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Response array or NULL if there is an error.
   */
  public function detachInternetGateway(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteCarrierGateway.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteCarrierGateway(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteTransitGateway.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteTransitGateway(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteVpcPeeringConnection.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteVpcPeeringConnection(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint Delete Flow logs.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of FlowLog or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteFlowLogs(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DeleteSubnet.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function deleteSubnet(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint DisassociateAddress.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function disassociateAddress(array $params = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API endpoint RunInstances.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $tags
   *   Optional tags to be sent during the runInstance call.
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   Array of instances or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function runInstances(array $params = [], array $tags = [], string $bundle = 'aws_cloud');

  /**
   * Calls the Amazon EC2 API RevokeSecurityGroupIngress.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result or NULL if there is an error.
   */
  public function revokeSecurityGroupIngress(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Calls the Amazon EC2 API RevokeSecurityGroupEgress.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result or NULL if there is an error.
   */
  public function revokeSecurityGroupEgress(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Update the EC2Instances.
   *
   * Delete old instance entities, query the API for updated entities and store
   * them as instance entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateInstances(array $params = [], $clear = TRUE);

  /**
   * Update the EC2Instances without batch.
   *
   * Delete old instance entities, query the API for updated entities and store
   * them as instance entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param bool $to_update
   *   TRUE to update stale instances.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateInstancesWithoutBatch(array $params = [], $clear = TRUE, $to_update = TRUE);

  /**
   * Update the EC2Images.
   *
   * Delete old images entities, query the API
   * for updated entities and store them as images entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to delete images entities before importing.
   * @param bool $save_operation
   *   TRUE to save operation and not to execute.
   *
   * @return bool|int
   *   FALSE if nothing is updated.  Number of images imported returned as
   *   integer if successful.
   */
  public function updateImages(array $params = [], $clear = FALSE, $save_operation = TRUE);

  /**
   * Update the EC2Images not using batch API.
   *
   * Delete old images entities, query the API
   * for updated entities and store them as images entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to delete images entities before importing.
   * @param bool $to_update
   *   TRUE to update stale images.
   *
   * @return bool|int
   *   FALSE if nothing is updated.  Number of images imported returned as
   *   integer if successful.
   */
  public function updateImagesWithoutBatch(array $params = [], $clear = FALSE, $to_update = TRUE): bool;

  /**
   * Update the EC2 security groups.
   *
   * Delete old security groups entities, query the API for updated entities and
   * store them as security groups entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSecurityGroups(array $params = [], $clear = TRUE): bool;

  /**
   * Update the EC2 network interfaces.
   *
   * Delete old network interfaces entities, query the API for updated entities
   * and store them as network interfaces entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale VPCs.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateNetworkInterfaces(array $params = [], $clear = TRUE);

  /**
   * Update the EC2Elastic IPs.
   *
   * Delete old network interfaces entities, query the API for updated entities
   * and store them as network interfaces entities.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateElasticIps();

  /**
   * Update the EC2 key pairs.
   *
   * Delete old key pairs entities,
   * query the API for updated entities and store them as key pairs entities.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateKeyPairs();

  /**
   * Update the EC2Volumes.
   *
   * Delete old Volumes entities,
   * query the API for updated entities and store them as Volumes entities.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateVolumes();

  /**
   * Update the EC2snapshots.
   *
   * Delete old snapshots entities,
   * query the API for updated entities and store them as snapshots entities.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSnapshots();

  /**
   * Update the EC2VPCs.
   *
   * Delete old VPCs entities,
   * query the API for updated entities and store them as VPCs entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale VPCs.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateVpcs(array $params = [], $clear = TRUE);

  /**
   * Update the EC2VPCs without batch.
   *
   * Delete old VPCs entities,
   * query the API for updated entities and store them as VPCs entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale VPCs.
   * @param bool $update
   *   TRUE to update stale VPCs.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateVpcsWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE);

  /**
   * Update the internet gateways.
   *
   * Delete old internet gateways entities,
   * query the API for updated entities and
   * store them as internet gateways entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale internet gateways.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateInternetGateways(array $params = [], $clear = TRUE);

  /**
   * Update the carrier gateways.
   *
   * Delete old carrier gateways entities,
   * query the API for updated entities and
   * store them as carrier gateways entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale carrier gateways.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateCarrierGateways(array $params = [], $clear = TRUE);

  /**
   * Update the internet gateways without batch.
   *
   * Delete old internet gateways entities,
   * query the API for updated entities and
   * store them as internet gateways entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale internet gateways.
   * @param bool $update
   *   TRUE to update stale internet gateways.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateInternetGatewaysWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE);

  /**
   * Update the carrier gateways without batch.
   *
   * Delete old carrier gateways entities,
   * query the API for updated entities and
   * store them as carrier gateways entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale carrier gateways.
   * @param bool $update
   *   TRUE to update stale carrier gateways.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateCarrierGatewaysWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE);

  /**
   * Update the transit gateways.
   *
   * Delete old transit gateways entities,
   * query the API for updated entities and
   * store them as transit gateways entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale transit gateways.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateTransitGateways(array $params = [], $clear = TRUE);

  /**
   * Update the transit gateways without batch.
   *
   * Delete old transit gateways entities,
   * query the API for updated entities and
   * store them as transit gateways entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale transit gateways.
   * @param bool $update
   *   TRUE to update stale transit gateways.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateTransitGatewaysWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE);

  /**
   * Update the Availability Zones.
   *
   * Delete old Availability Zone entities,
   * query the API for updated entities and
   * store them as Availability Zone entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale Availability Zones.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateAvailabilityZones(array $params = [], $clear = TRUE);

  /**
   * Update the Availability Zones without batch.
   *
   * Delete old Availability Zones entities,
   * query the API for updated entities and
   * store them as Availability Zones entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale Availability Zones.
   * @param bool $update
   *   TRUE to update stale Availability Zones.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateAvailabilityZonesWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE);

  /**
   * Update the transit gateway route tables.
   *
   * Delete old transit gateway route entities,
   * query the API for updated entities and
   * store them as transit gateway route entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale transit gateway route tables.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateTransitGatewayRoutes(array $params = [], $clear = TRUE);

  /**
   * Update the transit gateway route tables without batch.
   *
   * Delete old transit gateway route tables entities,
   * query the API for updated entities and
   * store them as transit gateway route tables entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale transit gateway route tables.
   * @param bool $update
   *   TRUE to update stale transit gateway route tables.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateTransitGatewayRoutesWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE);

  /**
   * Update the VPC peering connections.
   *
   * Delete old VPC peering connections entities,
   * query the API for updated entities and store them as
   * VPC peering connections entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale VPC peering connections.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateVpcPeeringConnections(array $params = [], $clear = TRUE);

  /**
   * Update the VPC peering connections without batch.
   *
   * Delete old VPC peering connections entities,
   * query the API for updated entities and store them as
   * VPC peering connections entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale VPC peering connections.
   * @param bool $update
   *   TRUE to update stale VPC peering connections.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateVpcPeeringConnectionsWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE);

  /**
   * Update the EC2subnets.
   *
   * Delete old subnets entities,
   * query the API for updated entities and store them as subnets entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale subnets.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSubnets(array $params = [], $clear = TRUE);

  /**
   * Update the EC2subnets.
   *
   * Delete old subnets entities,
   * query the API for updated entities and store them as subnets entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale subnets.
   * @param bool $update
   *   TRUE to update stale subnets.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSubnetsWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE);

  /**
   * Update the Server groups without batch.
   *
   * Delete old Server groups entities,
   * query the API for updated entities and
   * store them as Server groups entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale Server groups.
   * @param bool $update
   *   TRUE to update stale Server groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateServerGroupsWithoutBatch(array $params = [], bool $clear = TRUE, bool $update = TRUE): bool;

  /**
   * Update cloud launch templates.
   *
   * Delete old cloud launch template entities,
   * query the API for updated entities and store them as
   * cloud launch template entities.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateCloudLaunchTemplates(): bool;

  /**
   * Update cloud launch templates.
   *
   * Delete old cloud launch template entities,
   * query the API for updated entities and store them as
   * cloud launch template entities.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateCloudLaunchTemplatesWithoutBatch(): bool;

  /**
   * Method gets all the Availability Zones in a particular cloud context.
   *
   * @param bool $with_id
   *   Show Availability Zone ID.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of Availability Zones.
   */
  public function getAvailabilityZones($with_id = FALSE, array $credentials = []): array;

  /**
   * Method gets all the VPCs in a particular cloud context.
   *
   * @return array
   *   Array of VPCs.
   */
  public function getVpcs();

  /**
   * Stops ec2 instance given an instance array.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of instances or NULL if there is an error.
   */
  public function stopInstances(array $params = []): ?ResultInterface;

  /**
   * Start ec2 instance given an instance array.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of instances or NULL if there is an error.
   */
  public function startInstances(array $params = []): ?ResultInterface;

  /**
   * Modifies the specified attribute of an instance.
   *
   * @param array $params
   *   Parameters array to send to API.
   */
  public function modifyInstanceAttribute(array $params = []): ?ResultInterface;

  /**
   * Attaches an EBS volume.
   *
   * Attaches an EBS Volume to a running or stopped
   * instance and exposes it to the instance with the
   * specified device name.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of VolumeAttachment or NULL if there is an error.
   */
  public function attachVolume(array $params = []): ?ResultInterface;

  /**
   * Detaches an EBS volume.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of VolumeAttachment or NULL if there is an error.
   */
  public function detachVolume(array $params = []): ?ResultInterface;

  /**
   * Create a launch template.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of LaunchTemplate or NULL if there is an error.
   */
  public function createLaunchTemplate(array $params = []): ?ResultInterface;

  /**
   * Delete a launch template.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of LaunchTemplate or NULL if there is an error.
   */
  public function deleteLaunchTemplate(array $params = []): ?ResultInterface;

  /**
   * Modify a launch template.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of LaunchTemplate or NULL if there is an error.
   */
  public function modifyLaunchTemplate(array $params = []): ?ResultInterface;

  /**
   * Describe launch templates.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of LaunchTemplate or NULL if there is an error.
   */
  public function describeLaunchTemplates(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Create a launch template version.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of LaunchTemplate or NULL if there is an error.
   */
  public function createLaunchTemplateVersion(array $params = []): ?ResultInterface;

  /**
   * Delete launch template versions.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   Array of LaunchTemplate or NULL if there is an error.
   */
  public function deleteLaunchTemplateVersions(array $params = []): ?ResultInterface;

  /**
   * Describe launch template versions.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   Array of LaunchTemplate or NULL if there is an error.
   */
  public function describeLaunchTemplateVersions(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Retrieves the supported platforms supported by a particular ec2 account.
   *
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   An array of supported accounts.
   */
  public function getSupportedPlatforms(array $credentials = []): array;

  /**
   * Calls the Amazon EC2 API endpoint GetConsoleOutput.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param array $credentials
   *   Array of credentials.
   *
   * @return \Aws\ResultInterface
   *   Result or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  public function getConsoleOutput(array $params = [], array $credentials = []): ?ResultInterface;

  /**
   * Helper method to get the name of AWS object.
   *
   * @param array $aws_obj
   *   Array of AWS object.
   * @param string $default_value
   *   Default value of tag name.
   *
   * @return string
   *   Tag name.
   */
  public function getTagName(array $aws_obj, $default_value);

  /**
   * Helper method to get the map of snapshot ID and name.
   *
   * @param array $volumes
   *   Array of volumes.
   *
   * @return array
   *   Map of snapshots.
   */
  public function getSnapshotIdNameMap(array $volumes);

  /**
   * Helper function to parse drupal uid value out of the tags array.
   *
   * @param array $tags_array
   *   The tags array.
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return int
   *   Drupal uid.
   */
  public function getUidTagValue(array $tags_array, string $entity_type_id);

  /**
   * Helper function to get an instance's uid.
   *
   * @param string $instance_id
   *   The instance_id to load.
   *
   * @return int
   *   The uid of the instance.
   */
  public function getInstanceUid($instance_id): int;

  /**
   * Helper function to loop the network interfaces.
   *
   * Also creates a comma delimited string of private IPs. Function returns
   * false if no private IPs found.
   *
   * @param array $network_interfaces
   *   Array of network interfaces from the EC2 DescribeInstance API.
   *
   * @return string|false
   *   Imploded string or FALSE if no private IPs found.
   */
  public function getPrivateIps(array $network_interfaces);

  /**
   * Set up the ip_permission field given the inbound security group array.
   *
   * The array comes from DescribeSecurityGroup Amazon EC2 API call.
   *
   * @param array $ec2_permission
   *   An array object of EC2permission.
   *
   * @return array
   *   An array of \Drupal\Core\Field\FieldItemInterface.
   */
  public function setupIpPermissionObject(array $ec2_permission);

  /**
   * Setup IP Permissions.
   *
   * @param object $security_group
   *   The security group entity.
   * @param string $field
   *   Field to used for lookup.
   * @param array $ec2_permissions
   *   Permissions array from Ec2.
   */
  public function setupIpPermissions(&$security_group, $field, array $ec2_permissions): void;

  /**
   * Calculate the cost of an instance.
   *
   * @param array $instance
   *   The instance.
   * @param array $instance_types
   *   All instance types.
   *
   * @return float
   *   Cost of the instance.
   */
  public function calculateInstanceCost(array $instance, array $instance_types): ?float;

  /**
   * Clear plugin cache.
   */
  public function clearPluginCache(): void;

  /**
   * Ping the metadata server for security credentials URL.
   *
   * @return bool
   *   TRUE if either 169.254.169.254 (EC2) or 169.254.170.2 (ECS) is
   *   accessible.
   */
  public function pingMetadataSecurityServer(): bool;

  /**
   * Get instance ID from metadata server.
   *
   * @return string
   *   Instance ID string.
   */
  public function getInstanceIdFromMetadata(): string;

  /**
   * Check if a particular instance is running Cloud Orchestrator.
   *
   * @param string $instance_id
   *   The instance to check.
   *
   * @return bool
   *   True or false.
   */
  public function isInstanceCloudOrchestrator($instance_id): bool;

  /**
   * Terminate expired instances.
   */
  public function terminateExpiredInstances();

  /**
   * Update pending images.
   */
  public function updatePendingImages();

  /**
   * Create queue items for update resources queue.
   *
   * @param \Drupal\cloud\Entity\CloudConfigInterface $cloud_config
   *   The Cloud Config entity.
   */
  public function createResourceQueueItems(CloudConfigInterface $cloud_config): void;

  /**
   * Helper method to update all entity list.
   *
   * @param string $entity_type_name
   *   The entity type name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function updateAllEntityList($entity_type_name);

  /**
   * Process single entity update.
   *
   * @param object $entity
   *   Resource entity.
   * @param bool $is_submit
   *   If checking in submit form, do not update.
   * @param bool $update
   *   TRUE to update data.
   *
   * @return bool
   *   If entity exists, return TRUE, otherwise return FALSE.
   *
   * @throws Ec2ServiceException
   *   If param is empty.
   */
  public function updateSingleEntity($entity, $is_submit = FALSE, $update = FALSE): bool;

  /**
   * Check whether the cloud config is the resource of a worker.
   *
   * @return bool
   *   Whether the cloud config is from a worker or not.
   */
  public function isWorkerResource(): bool;

  /**
   * Add Architectures to instance types.
   *
   * @param array $instance_types
   *   The instance type list.
   */
  public function addArchitecturesToInstanceTypes(array &$instance_types = []) : void;

}
