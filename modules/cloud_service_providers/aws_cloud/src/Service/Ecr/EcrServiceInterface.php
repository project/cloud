<?php

namespace Drupal\aws_cloud\Service\Ecr;

use Aws\ResultInterface;

/**
 * Provides EcrService interface.
 */
interface EcrServiceInterface {

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function setCloudContext($cloud_context): void;

  /**
   * Get authorization token.
   *
   * @param array $params
   *   Parameter array.
   *
   * @return \Aws\ResultInterface
   *   Authorization token array.
   */
  public function getAuthorizationToken(array $params): ?ResultInterface;

  /**
   * Creates an image repository.
   *
   * @param string $name
   *   The repository to create, including the namespace.
   *
   * @return \Aws\ResultInterface
   *   Results.
   */
  public function createRepository($name): ?ResultInterface;

  /**
   * Describe ECR repositories.
   *
   * @param array $params
   *   Parameter array.
   *
   * @return \Aws\ResultInterface
   *   Repository array.
   */
  public function describeRepositories(array $params): ?ResultInterface;

  /**
   * Build the ECR endpoint using cloud_config parameters.
   *
   * @return string
   *   The ECR endpoint.
   */
  public function getEcrEndpoint(): string;

  /**
   * Check whether repository exists.
   *
   * @param string $name
   *   Repository name to check.
   *
   * @return bool
   *   True or false depending on if the repository exists.
   */
  public function doesRepositoryExists($name): bool;

  /**
   * Describe images.
   *
   * @param array $params
   *   Parameter array.
   *
   * @return \Aws\ResultInterface
   *   Repository array.
   */
  public function describeImages(array $params): ?ResultInterface;

  /**
   * Check if an image exists.
   *
   * @param string $name
   *   Repository name.
   * @param string $tag
   *   Repository tag.
   *
   * @return bool
   *   True if image exists else false.
   */
  public function doesImageExist($name, $tag): bool;

}
