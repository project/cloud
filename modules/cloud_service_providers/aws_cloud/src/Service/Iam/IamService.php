<?php

namespace Drupal\aws_cloud\Service\Iam;

use Aws\Credentials\AssumeRoleCredentialProvider;
use Aws\Credentials\CredentialProvider;
use Aws\Iam\Exception\IamException;
use Aws\Iam\IamClient;
use Aws\MockHandler;
use Aws\Result;
use Aws\ResultInterface;
use Aws\Sts\StsClient;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudCacheServiceInterface;
use Drupal\cloud\Service\CloudServiceBase;

/**
 * IamService service interacts with the AWS Cloud IAM API.
 */
class IamService extends CloudServiceBase implements IamServiceInterface {

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * TRUE or FALSE whether to be in test mode.
   *
   * @var bool
   */
  private $testMode;

  /**
   * The Cloud cache service.
   *
   * @var \Drupal\cloud\Service\CloudCacheServiceInterface
   */
  protected $cloudCacheService;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new IamService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\CloudCacheServiceInterface $cloud_cache_service
   *   The Cloud cache service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    CloudCacheServiceInterface $cloud_cache_service,
    ModuleHandlerInterface $module_handler,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    // Set up the configuration factory.
    $this->configFactory = $config_factory;

    // Set up the testMode flag.
    $this->testMode = (bool) $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_test_mode');

    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;

    $this->cloudCacheService = $cloud_cache_service;

    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context): void {
    $this->cloudContext = $cloud_context;
    if (!empty($cloud_context)) {
      $this->cloudConfigPluginManager->setCloudContext($cloud_context);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function listInstanceProfiles(array $params = [], array $credentials = [], $show_errors = TRUE): ?ResultInterface {
    return $this->execute('ListInstanceProfiles', $params, $credentials, $show_errors);
  }

  /**
   * Execute the API of AWS Cloud IAM service.
   *
   * @param string $operation
   *   The operation to perform.
   * @param array $params
   *   An array of parameters.
   * @param array $credentials
   *   The array of credentials.
   * @param bool $show_errors
   *   Show IAM errors.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Iam\IamServiceException
   *   If the $iam_client (IamClient) is NULL.
   */
  private function execute($operation, array $params = [], array $credentials = [], $show_errors = TRUE): ?ResultInterface {
    $results = NULL;
    $error_code = NULL;
    $this->cloudCacheService->setErrorCodes(self::PERMISSION_ERROR_CODES);

    $iam_client = $this->getIamClient($credentials);
    if ($iam_client === NULL) {
      throw new IamServiceException('No Iam Client found. Cannot perform API operations');
    }

    try {
      // Let other modules alter the parameters
      // before they are sent through the API.
      $this->moduleHandler->invokeAll('aws_cloud_pre_execute_alter', [
        &$params,
        $operation,
        $this->cloudContext,
      ]);

      $command = $iam_client->getCommand($operation, $params);
      $results = $iam_client->execute($command);

      // Let other modules alter the results before the module processes it.
      $this->moduleHandler->invokeAll('aws_cloud_post_execute_alter', [
        &$results,
        $operation,
        $this->cloudContext,
      ]);
    }
    catch (IamException $e) {

      $error_code = $e->getAwsErrorCode();

      // IAM permission validation needs AwsException to be thrown as it is
      // determined by the AwsErrorCode of the exception, 'AccessDenied'.
      // Note: IamService API itself does not support DryRun param, but our code
      // passes it as a marker.
      if (!empty($params['DryRun'])) {
        $this->cloudCacheService->setCachedOperationAccess($this->cloudContext, $operation, $error_code);
        throw $e;
      }

      if ($show_errors === TRUE) {
        $this->messenger->addError($this->t('Error: The operation "@operation" could not be performed.', [
          '@operation' => $operation,
        ]));

        $this->messenger->addError($this->t('Error Info: @error_info', [
          '@error_info' => $e->getAwsErrorCode(),
        ]));

        $this->messenger->addError($this->t('Error from: @error_type-side', [
          '@error_type' => $e->getAwsErrorType(),
        ]));

        $this->messenger->addError($this->t('Status Code: @status_code', [
          '@status_code' => $e->getStatusCode(),
        ]));

        $this->messenger->addError($this->t('Message: @msg', ['@msg' => $e->getAwsErrorMessage()]));
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    $this->cloudCacheService->setCachedOperationAccess($this->cloudContext, $operation, $error_code);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function getIamClient(array $credentials = []): ?IamClient {
    // Use the plugin manager to load the aws credentials.
    if (empty($credentials)) {
      if (empty($this->cloudContext)) {
        $credentials = [
          'use_instance_profile' => TRUE,
          'version' => 'latest',
        ];
      }
      else {
        $credentials = $this->cloudConfigPluginManager->loadCredentials();
      }
    }

    $credentials['endpoint'] = 'https://iam.amazonaws.com';
    $credentials['region'] = 'us-east-1';

    try {
      $iam_params = [
        'region' => $credentials['region'],
        'version' => $credentials['version'],
        'http' => [
          'connect_timeout' => $this->connectTimeout,
        ],
      ];
      $provider = FALSE;

      // Load credentials if needed.
      if (empty($credentials['use_instance_profile'])) {
        if (!empty($credentials['env'])) {
          $this->setCredentialsToEnv(
            $credentials['env']['access_key'],
            $credentials['env']['secret_key']
          );
          $provider = CredentialProvider::env();
        }
        else {
          $provider = CredentialProvider::ini('default', $credentials['ini_file']);
        }
        $provider = CredentialProvider::memoize($provider);
      }

      if (!empty($credentials['use_assume_role'])) {
        // Assume role.
        $sts_params = [
          'region' => $credentials['region'],
          'version' => $credentials['version'],
        ];
        if ($provider !== FALSE) {
          $sts_params['credentials'] = $provider;
        }
        $assumeRoleCredentials = new AssumeRoleCredentialProvider([
          'client' => new StsClient($sts_params),
          'assume_role_params' => [
            'RoleArn' => $credentials['role_arn'],
            'RoleSessionName' => 'iam_client_assume_role',
          ],
        ]);

        // Memoize takes care of re-authenticating when the tokens expire.
        $assumeRoleCredentials = CredentialProvider::memoize($assumeRoleCredentials);
        $iam_params = [
          'region' => $credentials['region'],
          'version' => $credentials['version'],
          'credentials' => $assumeRoleCredentials,
        ];
        // If switch role is enabled, execute one more assume role.
        if (!empty($credentials['use_switch_role'])) {
          $switch_sts_params = [
            'region' => $credentials['region'],
            'version' => $credentials['version'],
            'credentials' => $assumeRoleCredentials,
          ];
          $switchRoleCredentials = new AssumeRoleCredentialProvider([
            'client' => new StsClient($switch_sts_params),
            'assume_role_params' => [
              'RoleArn' => $credentials['switch_role_arn'],
              'RoleSessionName' => 'iam_client_switch_role',
            ],
          ]);
          $switchRoleCredentials = CredentialProvider::memoize($switchRoleCredentials);
          $iam_params['credentials'] = $switchRoleCredentials;
        }
      }
      elseif ($provider !== FALSE) {
        $iam_params['credentials'] = $provider;
      }

      $iam_client = new IamClient($iam_params);
    }
    catch (\Exception $e) {
      $iam_client = NULL;
      $this->logger('iam_service')->error($e->getMessage());
    }
    if ($this->testMode) {
      $this->addMockHandler($iam_client);
    }
    return $iam_client;
  }

  /**
   * Add a mock handler of aws sdk for testing.
   *
   * The mock data of aws response is saved
   * in configuration "aws_cloud_mock_data".
   *
   * @param \Aws\Iam\IamClient $iam_client
   *   The IAM client.
   *
   * @see https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_handlers-and-middleware.html
   */
  private function addMockHandler(IamClient $iam_client): void {
    $mock_data = $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_mock_data');
    if (empty($this->testMode) || empty($mock_data)) {
      return;
    }

    $mock_data = json_decode($mock_data, TRUE);
    $result = static function ($command, $request) use ($mock_data) {

      $command_name = $command->getName();
      $response_data = $mock_data[$command_name] ?? [];

      // ErrorCode field is proprietary defined to mock AwsErrorCode.
      // Note: AWS IAM does not support the DryRun parameter.
      if (!empty($response_data['ErrorCode'])) {
        return new IamException('IamException by IamService::addMockHandler()',
          $command, ['code' => $response_data['ErrorCode']]);
      }

      return new Result($response_data);
    };

    // Set a mock handler with the number of mocked results in the queue.
    // The mock queue count should be the number of API calls and is similar
    // to the prepared mocked results. TO be safe, an additional queue is set
    // for an async call or a repeated call.
    $results_on_queue = array_pad([], count($mock_data) + 1, $result);
    $iam_client
      ->getHandlerList()
      ->setHandler(new MockHandler($results_on_queue));
  }

  /**
   * Set credentials to ENV.
   *
   * @param string $access_key
   *   The access key.
   * @param string $secret_key
   *   The secret key.
   */
  protected function setCredentialsToEnv($access_key, $secret_key): void {
    putenv(CredentialProvider::ENV_KEY . "=$access_key");
    putenv(CredentialProvider::ENV_SECRET . "=$secret_key");
  }

}
