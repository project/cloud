<?php

namespace Drupal\aws_cloud\Service\Iam;

/**
 * AWS Cloud EC2 service exception.
 */
class IamServiceException extends \Exception {

}
