<?php

namespace Drupal\aws_cloud\Service\Iam;

use Aws\Iam\IamClient;
use Aws\ResultInterface;

/**
 * IamService service interacts with the AWS Cloud IAM API.
 */
interface IamServiceInterface {

  /**
   * AWS error codes for permission failure.
   *
   * @FIXME Move to AWS common service level to share with other services,
   * i.e., S3 Service.
   *
   * @var string[]
   */
  public const PERMISSION_ERROR_CODES = [
    'AccessDenied',
    'IncompleteSignature',
    'InvalidAuthenticationCode',
    'InvalidCertificate',
    'InvalidPublicKey',
    'KeyPairMismatch',
    'MalformedCertificate',
  ];

  /**
   * Mapping from Drupal permission to AWS API.
   *
   * @var string[]
   */
  public const PERMISSION_TO_AWS_API = [
    'add cloud server templates' => 'ListInstanceProfiles',
  ];

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function setCloudContext($cloud_context): void;

  /**
   * Load and return an IamClient.
   *
   * @param array $credentials
   *   The array of credentials.
   *
   * @return \Aws\Iam\IamClient|null
   *   If the $params is empty or $iam_client (Iam2Client) is NULL.
   */
  public function getIamClient(array $credentials = []): ?IamClient;

  /**
   * Get instance profiles.
   *
   * @param array $params
   *   Array of parameters.
   * @param array $credentials
   *   The array of credentials.
   * @param bool $show_errors
   *   Show IAM errors.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   */
  public function listInstanceProfiles(array $params = [], array $credentials = [], $show_errors = TRUE): ?ResultInterface;

}
