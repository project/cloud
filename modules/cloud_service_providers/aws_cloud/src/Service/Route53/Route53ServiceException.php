<?php

namespace Drupal\aws_cloud\Service\Route53;

/**
 * AWS Cloud Route53 service exception.
 */
class Route53ServiceException extends \Exception {

}
