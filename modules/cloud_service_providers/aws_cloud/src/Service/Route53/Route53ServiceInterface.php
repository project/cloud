<?php

namespace Drupal\aws_cloud\Service\Route53;

use Aws\ResultInterface;

/**
 * Route53 service interface to interact with AWS Cloud Route53 API.
 */
interface Route53ServiceInterface {

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function setCloudContext(string $cloud_context): void;

  /**
   * Calls Amazon Route53 API and list resource records.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return \Aws\ResultInterface|null
   *   An array of results or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Route53\Route53ServiceException
   *   Exception if there is an error.
   */
  public function listResourceRecordSets(array $params = []): ?ResultInterface;

  /**
   * Calls Amazon Route53 API and list hosted zones.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return \Aws\ResultInterface|null
   *   An array of results or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Route53\Route53ServiceException
   *   Exception if there is an error.
   */
  public function listHostedZonesByName(array $params = []): ?ResultInterface;

}
