<?php

namespace Drupal\aws_cloud\Service\S3;

use Aws\Credentials\AssumeRoleCredentialProvider;
use Aws\Credentials\CredentialProvider;
use Aws\MockHandler;
use Aws\Result;
use Aws\ResultInterface;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Aws\S3\S3ClientInterface;
use Aws\Sts\StsClient;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceBase;

/**
 * S3Service service interacts with the AWS Cloud S3 API.
 */
class S3Service extends CloudServiceBase implements S3ServiceInterface {

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * TRUE or FALSE whether to be in test mode.
   *
   * @var bool
   */
  private $testMode;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new S3Service object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    ModuleHandlerInterface $module_handler,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    // Set up the configuration factory.
    $this->configFactory = $config_factory;

    // Set up the testMode flag.
    $this->testMode = (bool) $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_test_mode');

    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;

    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context): void {
    $this->cloudContext = $cloud_context;
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function getObject(array $params = []): ?ResultInterface {
    $results = $this->execute('GetObject', $params);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function putObject(array $params = []): ?ResultInterface {
    $results = $this->execute('PutObject', $params);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function listObjects(array $params = []): ?ResultInterface {
    $results = $this->execute('ListObjects', $params);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMatchingObjects(array $params = []): ?ResultInterface {
    $results = $this->execute('DeleteMatchingObjects', $params);
    return $results;
  }

  /**
   * Execute the API of AWS Cloud S3 service.
   *
   * @param string $operation
   *   The operation to perform.
   * @param array $params
   *   An array of parameters.
   *
   * @return \Aws\ResultInterface
   *   Result object or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\S3\S3ServiceException
   *   If the $s3_client (S3Client) is NULL.
   */
  private function execute(string $operation, array $params = []): ?ResultInterface {
    $results = NULL;

    $s3_client = $this->getS3Client();
    if ($s3_client === NULL) {
      throw new S3ServiceException('No S3 Client found. Cannot perform API operations');
    }

    try {
      // Let other modules alter the parameters
      // before they are sent through the API.
      $this->moduleHandler->invokeAll('aws_cloud_pre_execute_alter', [
        &$params,
        $operation,
        $this->cloudContext,
      ]);

      if ($operation === 'DeleteMatchingObjects') {
        $s3_client->deleteMatchingObjects($params['Bucket'], $params['Prefix']);
      }
      else {
        $command = $s3_client->getCommand($operation, $params);
        $results = $s3_client->execute($command);
      }

      // Let other modules alter the results before the module processes it.
      $this->moduleHandler->invokeAll('aws_cloud_post_execute_alter', [
        &$results,
        $operation,
        $this->cloudContext,
      ]);
    }
    catch (S3Exception $e) {
      // IAM permission validation needs AwsException to be thrown as it is
      // determined by the AwsErrorCode of the exception, 'DryRunOperation'.
      if (!empty($params['DryRun'])) {
        throw $e;
      }

      $this->messenger->addError($this->t('Error: The operation "@operation" could not be performed.', [
        '@operation' => $operation,
      ]));

      $this->messenger->addError($this->t('Error Info: @error_info', [
        '@error_info' => $e->getAwsErrorCode(),
      ]));

      $this->messenger->addError($this->t('Error from: @error_type-side', [
        '@error_type' => $e->getAwsErrorType(),
      ]));

      $this->messenger->addError($this->t('Status Code: @status_code', [
        '@status_code' => $e->getStatusCode(),
      ]));

      $this->messenger->addError($this->t('Message: @msg', ['@msg' => $e->getAwsErrorMessage()]));

    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $results;
  }

  /**
   * Load and return an S3Client.
   */
  private function getS3Client(): ?S3ClientInterface {
    // Use the plugin manager to load the aws credentials.
    $credentials = $this->cloudConfigPluginManager->loadCredentials();

    try {
      $s3_params = [
        'region' => $credentials['region'],
        'version' => $credentials['version'],
        'http' => [
          'connect_timeout' => $this->connectTimeout,
        ],
      ];
      $provider = FALSE;

      // Load credentials if needed.
      if (empty($credentials['use_instance_profile'])) {
        $provider = CredentialProvider::ini('default', $credentials['ini_file']);
        $provider = CredentialProvider::memoize($provider);
      }

      if (!empty($credentials['use_assume_role'])) {
        // Assume role.
        $sts_params = [
          'region' => $credentials['region'],
          'version' => $credentials['version'],
        ];
        if ($provider !== FALSE) {
          $sts_params['credentials'] = $provider;
        }
        $assumeRoleCredentials = new AssumeRoleCredentialProvider([
          'client' => new StsClient($sts_params),
          'assume_role_params' => [
            'RoleArn' => $credentials['role_arn'],
            'RoleSessionName' => 's3_client_assume_role',
          ],
        ]);

        // Memoize takes care of re-authenticating when the tokens expire.
        $assumeRoleCredentials = CredentialProvider::memoize($assumeRoleCredentials);
        $s3_params = [
          'region' => $credentials['region'],
          'version' => $credentials['version'],
          'credentials' => $assumeRoleCredentials,
        ];
        // If switch role is enabled, execute one more assume role.
        if (!empty($credentials['use_switch_role'])) {
          $switch_sts_params = [
            'region' => $credentials['region'],
            'version' => $credentials['version'],
            'credentials' => $assumeRoleCredentials,
          ];
          $switchRoleCredentials = new AssumeRoleCredentialProvider([
            'client' => new StsClient($switch_sts_params),
            'assume_role_params' => [
              'RoleArn' => $credentials['switch_role_arn'],
              'RoleSessionName' => 's3_client_switch_role',
            ],
          ]);
          $switchRoleCredentials = CredentialProvider::memoize($switchRoleCredentials);
          $s3_params['credentials'] = $switchRoleCredentials;
        }
      }
      elseif ($provider !== FALSE) {
        $s3_params['credentials'] = $provider;
      }

      $s3_client = new S3Client($s3_params);
    }
    catch (\Exception $e) {
      $s3_client = NULL;
      $this->logger('s3_service')->error($e->getMessage());
    }
    if ($this->testMode) {
      $this->addMockHandler($s3_client);
    }
    return $s3_client;
  }

  /**
   * Add a mock handler of aws sdk for testing.
   *
   * The mock data of aws response is saved
   * in configuration "aws_cloud_mock_data".
   *
   * @param \Aws\S3\S3Client $s3_client
   *   The S3 client.
   *
   * @see https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_handlers-and-middleware.html
   */
  private function addMockHandler(S3Client $s3_client): void {
    $mock_data = $this->configFactory->get('aws_cloud.settings')->get('aws_cloud_mock_data');
    if (empty($this->testMode) || empty($mock_data)) {
      return;
    }

    $mock_data = json_decode($mock_data, TRUE);
    $result = static function ($command, $request) use ($mock_data) {

      $command_name = $command->getName();
      $response_data = $mock_data[$command_name] ?? [];

      // ErrorCode field is proprietary defined to mock AwsErrorCode.
      // Checking the value of DryRun parameter as some test cases expect a
      // different result based on the DryRun parameter.
      if (!empty($response_data['ErrorCode'])
        && $command->hasParam('DryRun')
        && ($command->toArray())['DryRun']) {
        return new S3Exception('S3Exception by S3Service::addMockHandler()',
          $command, ['code' => $response_data['ErrorCode']]);
      }

      return new Result($response_data);
    };

    // Set a mock handler with the number of mocked results in the queue.
    // The mock queue count should be the number of API calls and is similar
    // to the prepared mocked results. TO be safe, an additional queue is set
    // for an async call or a repeated call.
    $results_on_queue = array_pad([], count($mock_data) + 1, $result);
    $s3_client
      ->getHandlerList()
      ->setHandler(new MockHandler($results_on_queue));
  }

}
