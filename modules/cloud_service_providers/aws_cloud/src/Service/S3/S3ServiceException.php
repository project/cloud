<?php

namespace Drupal\aws_cloud\Service\S3;

/**
 * AWS Cloud S3 service exception.
 */
class S3ServiceException extends \Exception {

}
