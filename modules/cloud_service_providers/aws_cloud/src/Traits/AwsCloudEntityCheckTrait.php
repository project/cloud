<?php

namespace Drupal\aws_cloud\Traits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * The trait checking permission.
 */
trait AwsCloudEntityCheckTrait {
  /**
   * Redirect render object.
   *
   * @var \Symfony\Component\HttpFoundation\RedirectResponse
   */
  protected $redirectUrl;

  /**
   * Helper to check if entity exists in submitForm().
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state handles redirect.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Process non-property entity.
   *
   * @return bool
   *   If entity does not exit, return FALSE and redirect to list page.
   */
  protected function updateEntitySubmitForm(FormStateInterface $form_state, EntityInterface $entity): bool {
    $ec2Service = \Drupal::service('aws_cloud.ec2');
    $ec2Service->setCloudContext($entity->getCloudContext());

    if ($ec2Service->updateSingleEntity($entity, TRUE) === FALSE) {
      $this->showEntityMessage($entity);
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", [
        'cloud_context' => $entity->getCloudContext(),
      ]);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Helper to check if entity exists in buildForm().
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Process non-property entity.
   *
   * @return bool
   *   TRUE if entity was updated, FALSE otherwise.
   */
  protected function updateEntityBuildForm(EntityInterface $entity): bool {
    $ec2Service = \Drupal::service('aws_cloud.ec2');
    $ec2Service->setCloudContext($entity->getCloudContext());

    if ($ec2Service->updateSingleEntity($entity) === FALSE) {
      if ($entity->getEntityTypeId() !== 'aws_cloud_vpc_peering_connection') {
        $this->showEntityMessage($entity);
      }
      $url = Url::fromRoute(
        "entity.{$entity->getEntityTypeId()}.collection",
        ['cloud_context' => $entity->getCloudContext()]
      );
      $this->setRedirectUrl($url->setAbsolute(TRUE)->toString());
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Set redirect render.
   *
   * @param string $url
   *   Redirect URL.
   */
  protected function setRedirectUrl($url): void {
    $this->redirectUrl = new RedirectResponse($url);
  }

  /**
   * Show appropriate message if Ec2Service::updateSingleEntity is FALSE.
   *
   * If the entity is deleted, show an error message.  Otherwise, show a
   * warning that the entity is being updated.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to check.
   */
  private function showEntityMessage(EntityInterface $entity): void {
    $entity_type_manager = \Drupal::entityTypeManager();
    $deleted_entity = $entity_type_manager->getStorage($entity->getEntityTypeId())
      ->load($entity->id());
    if (empty($deleted_entity)) {
      \Drupal::messenger()->addError($this->t('The @label %name has already been deleted.', [
        '@label' => $entity->getEntityType()->getSingularLabel(),
        '%name' => $entity->getName(),
      ]));
    }
    else {
      \Drupal::messenger()->addWarning($this->t('The @label %name is being updated. Try clicking %edit again shortly.', [
        '@label' => $entity->getEntityType()->getSingularLabel(),
        '%name' => $entity->toLink($entity->getName(), 'edit-form')->toString(),
        '%edit' => $entity->toLink($this->t('Edit'), 'edit-form')->toString(),
      ]));
    }
  }

}
