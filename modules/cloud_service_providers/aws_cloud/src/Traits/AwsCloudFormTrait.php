<?php

namespace Drupal\aws_cloud\Traits;

/**
 * The trait for AWS Cloud form.
 */
trait AwsCloudFormTrait {

  /**
   * Max length of the description of AWS Cloud security group (rule).
   *
   * @var int
   */
  public static $awsCloudSecurityGroupDescriptionMaxLength = 255;

  /**
   * Special characters in the description of AWS Cloud security group (rule).
   *
   * @var string
   */
  public static $awsCloudSecurityGroupDescriptionSpecialCharacters = '. _-:/()#,@[]+=&;{}!$*';

  /**
   * Get the pattern of a security group (rule) description.
   *
   * @return string
   *   A regexp pattern excluding ^ and $.
   */
  protected function getPatternOfSecurityGroupDescription(): string {
    return '[A-Za-z0-9' . preg_quote(self::$awsCloudSecurityGroupDescriptionSpecialCharacters, NULL) . ']*';
  }

  /**
   * Get VPC options.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return array
   *   The VPC options.
   */
  protected function getVpcOptions(string $cloud_context): array {
    $options = [];
    $vpcs = $this->entityTypeManager
      ->getStorage('aws_cloud_vpc')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    foreach ($vpcs ?: [] as $vpc) {
      $name = NULL;
      foreach ($vpc->getTags() ?: [] as $tag) {
        if ($tag['item_key'] === 'Name') {
          $name = $tag['item_value'];
          break;
        }
      }

      $options[$vpc->getVpcId()] = ($name ?: $vpc->getVpcId()) . " ({$vpc->getVpcId()} | {$vpc->getCidrBlock()})";
    }

    natcasesort($options);
    return $options;
  }

  /**
   * Load a VPC entity using the VPC ID.
   *
   * @param string $vpc_id
   *   VPC ID to load.
   * @param string $cloud_context
   *   The cloud context to load.
   *
   * @return false|\Drupal\aws_cloud\Entity\Vpc\Vpc
   *   FALSE or the VPC entity.
   */
  protected function getVpc(string $vpc_id, string $cloud_context) {
    $vpc = FALSE;
    try {
      $vpcs = $this->entityTypeManager
        ->getStorage('aws_cloud_vpc')
        ->loadByProperties([
          'vpc_id' => $vpc_id,
          'cloud_context' => $cloud_context,
        ]);
      if (!empty($vpcs) && count($vpcs) === 1) {
        $vpc = array_shift($vpcs);
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $vpc;
  }

  /**
   * Get Availability Zone options.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $module_name
   *   The module name.
   * @param bool $with_id
   *   Whether show ID or not.
   * @param string $component_name
   *   The openstack component name.
   * @param string $zone_resource
   *   The openstack zone resource.
   *
   * @return array
   *   The Availability Zone options.
   */
  protected function getAvailabilityZoneOptions(string $cloud_context, string $module_name, $with_id = FALSE, string $component_name = '', string $zone_resource = ''): array {
    $options = [];

    $properties = ['cloud_context' => $cloud_context];
    if ($module_name === 'openstack') {
      /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
      $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);

      // When trying to get the OpenStack Network Availability Zone,
      // check in advance whether extension is enabled, just like Horizon.
      // If disabled, you will not get an Availability Zone.
      if ($component_name === 'network' && !empty($zone_resource) && !$openstack_service->isNetworkExtensionSupported($zone_resource . '_availability_zone')) {
        return [];
      }

      // For OpenStack, if component_name and zone_resource are specified,
      // only fetch Availability Zones that match those conditions.
      if (!empty($component_name)) {
        $properties['component_name'] = $component_name;
      }
      if (!empty($zone_resource)) {
        $properties['zone_resource'] = $zone_resource;
      }
    }

    $availability_zones = $this->entityTypeManager
      ->getStorage("{$module_name}_availability_zone")
      ->loadByProperties($properties);

    // loadByProperties() must return some entities for Availability Zone;
    // however if the entities are empty, we try to get Availability Zones since
    // the entities are filled by aws_cloud_update_resources_queue.
    // The cost of describeAvailabilityZones() is 400-450 milliseconds.
    if (empty($availability_zones)) {
      // This is a trait however we assume $this->ec2Service is available.
      $this->ec2Service->updateAvailabilityZonesWithoutBatch();
      $availability_zones = $this->entityTypeManager
        ->getStorage("{$module_name}_availability_zone")
        ->loadByProperties($properties);
    }

    foreach ($availability_zones ?: [] as $availability_zone) {
      $name = $with_id
        ? sprintf('%s (%s)', $availability_zone->getZoneName(), $availability_zone->getZoneId())
        : $availability_zone->getZoneName();

      $options[$availability_zone->getZoneName()] = $name;
    }

    natcasesort($options);
    return $options;
  }

}
