<?php

namespace Drupal\aws_cloud\Traits;

/**
 * The trait for AWS instance monitor.
 */
trait AwsInstanceMonitorTrait {

  /**
   * Build chart.
   *
   * @param array $build
   *   Form array.
   * @param int $chart_type
   *   The chart type.
   */
  protected function buildChart(array &$build, $chart_type = 0): void {

    $charts = empty($chart_type)
      ? [
        'cpu' => $this->t('CPU Utilization (Percent)'),
        'network' => $this->t('Network Traffic (Megabytes)'),
        'disk' => $this->t('Disk (Bytes)'),
        'disk_operation' => $this->t('Disk (Operations)'),
      ]
      : [
        'cpu' => $this->t('CPU'),
        'network_in' => $this->t('Network In'),
        'network_out' => $this->t('Network Out'),
        'disk_read' => $this->t('Disk Read'),
        'disk_write' => $this->t('Disk Write'),
        'disk_read_operation' => $this->t('Disk Operation Read'),
        'disk_write_operation' => $this->t('Disk Operation Write'),
      ];

    $build['aws_instance_monitor_charts'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'aws-cloud-instance-monitor-charts',
      ],
    ];

    $parent = &$build['aws_instance_monitor_charts'];
    if (!empty($chart_type)) {
      $build['aws_instance_monitor_charts']['monitor'] = [
        '#type' => 'details',
        '#title' => $this->t('Monitor'),
        '#open' => TRUE,
      ];
      $parent = &$build['aws_instance_monitor_charts']['monitor'];
    }

    array_walk($charts, static function ($title, $chart) use ($chart_type, &$parent) {
      $name = empty($chart_type) ? $chart : "{$chart}_horizon_chart";
      $parent[$name] = empty($chart_type)
        ? [
          '#type' => 'details',
          '#title' => $title,
          '#open' => TRUE,
          "{$chart}_chart" => [
            '#type' => 'container',
            '#attributes' => [
              'id' => 'edit-' . str_replace('_', '-', $chart) . '-chart',
            ],
          ],
        ]
        : [
          '#type' => 'container',
          '#attributes' => [
            'id' => 'edit-' . str_replace('_', '-', $chart) . '-horizon-chart',
            'chart-name' => $title,
          ],
        ];
    });

    $build['aws_instance_monitor_charts']['#attached']['drupalSettings']['aws_cloud_monitor_chart_type'] = $chart_type;
  }

}
