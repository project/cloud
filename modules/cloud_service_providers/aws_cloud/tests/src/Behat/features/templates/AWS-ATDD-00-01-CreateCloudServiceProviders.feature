@minimal @launch_instance
Feature: Create a cloud service provider for AWS as "Cloud administrator"

  @setup
  @api @javascript
  Scenario: Add an AWS Cloud service provider using an instance profile
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config/add/aws_cloud"
    And I enter "{{ cloud_service_provider_name_entered }}" for "Name"
    And I enter "{{ account_id }}" for "Account ID"
    And I select "{{ region_name }}" from "Regions"
    And I check the box "Use instance profile"
    And I press "Save"
    And I wait for AJAX to finish
    Then I should be on "/clouds"
    And I should see the success message "Creating cloud service provider was performed successfully."
    And I should see neither error nor warning messages
    And I should see "{{ cloud_service_provider_name }}"

  @ignore @ci_job
  @api @javascript
  Scenario: Add an AWS Cloud service provider using assume role
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config/add/aws_cloud"
    And I enter "{{ cloud_service_provider_name_entered }}" for "Name"
    And I enter "{{ account_id }}" for "Account ID"
    And I select "{{ region_name }}" from "Regions"
    And I check the box "Use instance profile"
    And I check the box "Use assume role"
    And I enter "{{ iam_role }}" for "IAM role for assume role"
    And I press "Save"
    And I wait for AJAX to finish
    Then I should be on "/clouds"
    And I should see the success message "Creating cloud service provider was performed successfully"
    And I should see the warning message "Pricing data unavailable. Click to refresh all pricing data."
    And I click "Click to refresh all pricing data." in the warning_message
    And I should see the success message "Updated instance type prices"
    # Temporarily comment out the following assertion for no error message
    # as Drupal core may display a 'No active batch' error message.
    # And I should see no error message
    And I should see "{{ cloud_service_provider_name }}"

  @ignore
  @api @javascript
  Scenario: Add an AWS Cloud service provider using switch role
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config/add/aws_cloud"
    And I enter "{{ cloud_service_provider_name_entered }}" for "Name"
    And I enter "{{ account_id }}" for "Account ID"
    And I select "{{ region_name }}" from "Regions"
    And I check the box "Use instance profile"
    And I check the box "Use assume role"
    And I enter "{{ iam_role }}" for "IAM role"
    And I check the box "Use switch role"
    And I enter "{{ account_id_switch_to }}" for "Switch role account ID"
    And I enter "{{ iam_switch_role }}" for "IAM role for switch role"
    And I press "Save"
    And I wait for AJAX to finish
    Then I should be on "/clouds"
    And I should see the success message "Creating cloud service provider was performed successfully"
    And I should see the warning message "Pricing data unavailable. Click to refresh all pricing data."
    And I click "Click to refresh all pricing data." in the warning_message
    And I should see the success message "Updated instance type prices"
    And I should see no error message
    And I should see "{{ cloud_service_provider_name }}"
