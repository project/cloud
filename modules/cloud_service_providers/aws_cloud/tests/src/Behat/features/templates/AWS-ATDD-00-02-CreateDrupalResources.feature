@minimal @ci_job @launch_instance @setup
Feature: Create a Drupal role for AWS as "Administrator"

  @api
  Scenario: An authenticated user cannot see the created cloud service provider
    Given I am logged in as a user with the "Authenticated user" role
    When I visit "/clouds"
    And I should not see "{{ cloud_service_provider_name }}"
    And I go to "/clouds/aws_cloud/{{ cloud_context }}/instance"
    Then I should get a 403 HTTP response

  @api
  Scenario: Create a role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/roles/add"
    And I enter "{{ role_name }}" for "Role name"
    And I enter "{{ role_name_machine }}" for "Machine-readable name"
    And I press "Save"
    Then I should be on "/admin/people/roles"
    And I should see "{{ role_name }}"
    And I visit "/admin/people/permissions/{{ role_name_machine }}"
    # Allow to see the cloud service provider
    And I check the box "Access {{ cloud_service_provider_name }}"
    # Allow to launch an instance.
    And I check the box "Launch approved cloud launch template"
    And I press "Save permissions"
    And I should see the success message "The changes have been saved."
    And I should see neither error nor warning messages

  @api
  Scenario: Create a user
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/create"
    And I enter "{{ user_name }}" for "Username"
    And I enter "{{ user_password }}" for "Password"
    And I enter "{{ user_password }}" for "Confirm password"
    And I check the box "{{ role_name }}"
    And I press "Create new account"
    Then I should see the success message "Created a new user account"
    And I should see neither error nor warning messages
    And I visit "/admin/people"
    And I should see "{{ user_name }}"
