@ci_job
Feature: Read and update the created AWS Cloud service provider

  @api
  Scenario: Update and read the updated Cloud service provider
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config"
    And I should see the link "{{ cloud_service_provider_name }}" in the table
    And I should see "Edit" in the "{{ cloud_service_provider_name }}" row in the table
    And I wait {{ wait_dropdown }} milliseconds
    And I click "Edit" in the "{{ cloud_service_provider_name }}" row
    Then the url should match "/edit"
    And I enter "{{ cloud_service_provider_name_updated }}" for "Name"
    And I enter "{{ cloud_service_provider_description }}" for "Description"
    # Temporarily setting Authored by until the issue with null owner is fixed.
    And I fill in "Authored by" with "{{ cloud_service_provider_author }}"
    And I press "Save"
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ cloud_service_provider_name_updated }}" in the "success_message"
    Then the url should match "admin/structure/cloud_config/"
    And I should see the heading "{{ cloud_service_provider_name_updated }}"
    And I should see "{{ cloud_service_provider_name_updated }}" in the "details"
    And I should see "{{ cloud_service_provider_description }}"

  @api
  Scenario: Revert the update and read the reverted Cloud service provider
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config"
    And I should see the link "{{ cloud_service_provider_name_updated }}" in the table
    And I should see "Edit" in the "{{ cloud_service_provider_name_updated }}" row in the table
    And I wait {{ wait_dropdown }} milliseconds
    And I click "Edit" in the "{{ cloud_service_provider_name_updated }}" row
    Then the url should match "/edit"
    And I enter "{{ cloud_service_provider_name }}" for "Name"
    And I fill in "Description" with ""
    And I press "Save"
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ cloud_service_provider_name }}" in the "success_message"
    Then the url should match "admin/structure/cloud_config/"
    And I should see the heading "{{ cloud_service_provider_name }}"
    And I should see "{{ cloud_service_provider_name }}" in the details
    And I should not see "{{ cloud_service_provider_description }}"
