@ci_job
Feature: Confirm no AWS resources are created under the created cloud service
  provider

  @api
  Scenario: No link to the instance
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I should see the heading "AWS Cloud instances"
    And I click "Refresh"
    Then I should not see "pending"
    And I should see neither error nor warning messages

  @api
  Scenario: No link to the instance name on the template list
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the heading "Launch template list"
    And I click "Refresh"
    Then I should not see the link "{{ instance_name }}"
    And I should see neither error nor warning messages

  @api
  Scenario Outline: No link to the "<resource_name>" on the "<resource_type> list view"
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/<resource_type>"
    And I should see the heading "AWS Cloud <resource_type_name>"
    And I click "Refresh"
    Then I should not see the link "<resource_name>"
    And I should see neither error nor warning messages

    Examples:
      | resource_type          | resource_type_name      | resource_name                             |
      | instance               | instances               | {{ instance_name}}                        |
      | image                  | images                  | {{ image_name }}                          |
      | security_group         | security groups         | {{ security_group_name }}                 |
      | elastic_ip             | Elastic IPs             | {{ elastic_ip_name }}                     |
      | key_pair               | key pairs               | {{ ssh_key_name }}                        |
      | volume                 | volumes                 | {{ volume_name }}                         |
      | snapshot               | snapshots               | {{ snapshot_name_operate }}               |
      | network_interface      | network interfaces      | {{ network_interface_name_operate }}      |
      | subnet                 | subnets                 | {{ subnet_name }}                         |
      | vpc                    | VPCs                    | {{ vpc_name }}                            |
      | vpc_peering_connection | VPC peering connections | {{ vpc_peering_connection_name_operate }} |
      | internet_gateway       | internet gateways       | {{ internet_gateway_name }}               |
      | carrier_gateway        | carrier gateways        | {{ carrier_gateway_name_operate }}        |
      | transit_gateway        | transit gateways        | {{ transit_gateway_name_operate }}        |

  @api
  Scenario Outline: Access is denied for authenticated users
    Given I am logged in as a user with the "Authenticated user" role
    When I go to "/clouds/aws_cloud/{{ cloud_context }}/<resource_type>"
    Then I should get a 403 HTTP response
    And I should see the text "Access denied"

    Examples:
      | resource_type          |
      | instance               |
      | image                  |
      | security_group         |
      | elastic_ip             |
      | key_pair               |
      | volume                 |
      | snapshot               |
      | network_interface      |
      | subnet                 |
      | vpc                    |
      | vpc_peering_connection |
      | internet_gateway       |
      | carrier_gateway        |
      | transit_gateway        |
