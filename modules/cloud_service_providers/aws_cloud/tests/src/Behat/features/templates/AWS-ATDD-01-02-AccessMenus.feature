@ci_job
Feature: View the created resources for AWS Cloud as "Cloud administrator"

  @api
  Scenario Outline: View the menu of AWS Cloud service providers
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "AWS" in the "nav_bar"
    When I go to "/clouds"
    Then I should get a 200 HTTP response
    And I should see the heading "Cloud service providers"
    Then I should see "Location map"
    And I should see the button "Apply"
    # DO NOT change <status> to "<status>" since <status> is not an :arg1.
    And I should <status> "No items." in the table
    And I should see neither error nor warning messages

    Examples:
      | role                | status  |
      | Authenticated user  | see     |
      | Cloud administrator | not see |

  @api
  Scenario Outline: View the list of AWS Cloud resources
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "AWS" in the "nav_bar"
    When I go to "/clouds/aws_cloud/instance"
    Then I should get a 200 HTTP response
    And I should see the heading "All AWS Cloud instances"
    And I should see the button "Apply"
    And I should see the link "Instances"

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |
