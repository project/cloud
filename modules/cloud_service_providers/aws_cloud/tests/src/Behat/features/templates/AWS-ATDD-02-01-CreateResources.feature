@minimal @ci_job @launch_instance
Feature: Create resources for AWS as "Authenticated User"

  @api
  Scenario: Log into the site
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds"
    Then I should see the heading "Cloud service providers"
    And I should see the link "All" in the "nav_bar"
    And I should see the link "AWS" in the "nav_bar"

  @api
  Scenario: Create an SSH key pair
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/key_pair/add"
    And I should see the heading "Add AWS Cloud key pair"
    And I enter "{{ ssh_key_name }}" for "Key pair name"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/key_pair"
    And I should see the success message "has been created"
    And I should see no error message
    And I should see the link "{{ ssh_key_name }}"

  @api
  Scenario: Add a VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc/add"
    And I should see the heading "Add AWS Cloud VPC"
    And I enter "{{ vpc_name }}" for "Name"
    And I enter "{{ ipv4_cidr }}" for "IPv4 CIDR block"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ vpc_name }}" in the table

  @api @javascript
  Scenario: Add a subnet
    Given I am logged in as user "{{ user_name }}"
    # The following step requires @javascript.
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/subnet/add"
    And I should see the heading "Add AWS Cloud subnet"
    And I enter "{{ subnet_name }}" for "Name"
    And I select "{{ vpc_name }}" from "VPC CIDR (ID)"
    And I select "{{ availability_zone }}" from "Availability Zone"
    And I enter "{{ ipv4_cidr }}" for "IPv4 CIDR block"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/subnet"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ subnet_name }}" in the table

  @api @javascript
  Scenario: Add a security group
    Given I am logged in as user "{{ user_name }}"
    # The following step requires @javascript.
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/security_group/add"
    And I should see the heading "Add AWS Cloud security group"
    And I should see neither error nor warning messages
    And I enter "{{ security_group_name }}" for "Security group name"
    And I enter "{{ description }}" for "Description"
    And I select "{{ vpc_name }}" from "VPC CIDR (ID)"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ security_group_name }}" in the success_message
    And I should see "Outbound rules"
