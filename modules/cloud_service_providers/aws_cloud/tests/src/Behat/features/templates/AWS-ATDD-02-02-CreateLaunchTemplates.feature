@minimal @ci_job @launch_instance
Feature: Create an EC2 launch template as "Authenticated User"

  @api @javascript
  Scenario: Import an AMI image
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/images/import"
    And I should see the heading "Import images"
    And I enter "{{ image_id }}" for "Image IDs"
    # This requires @javascript for image search.
    And I press "Import"
    And I wait for AJAX to finish
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I should see the success message "Imported 1 images"
    And I should see neither error nor warning messages
    And I should see the link "{{ image_name }}" in the table

  @api @javascript
  Scenario: Create a Cloud launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}/aws_cloud/add"
    # @todo Add AWS Cloud launch template.
    And I should see the heading "Add AWS Cloud"
    And I enter "{{ instance_name }}" for "Name"
    And I select the option "{{ instance_type }}" in the TomSelect element "{{ instance_type_tom_select_dom_id }}"
    And I wait for AJAX to finish
    And I select the option "{{ image_name }}" in the TomSelect element "{{ image_id_tom_select_dom_id }}"
    And I wait for AJAX to finish
    And I select "{{ availability_zone }}" from "Availability Zone"
    And I select "{{ vpc_name }}" from "VPC"
    And I wait for AJAX to finish
    And I select "{{ subnet_name }}" from "Subnet"
    And I select "{{ security_group_name }}" from "Security groups"
    And I select "{{ ssh_key_name }}" from "SSH key"
    And I select "{{ status }}" from "Status"
    And I press "Save"
    And I wait for AJAX to finish
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}/"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the heading "{{ instance_name }}"
    And I should see "{{ instance_type }}"
    And I should see "{{ security_group_name }}"
    And I should see "{{ ssh_key_name }}"
    And I should see "{{ vpc_name }}"

  @api
  Scenario: Cannot launch a Cloud launch template without approval
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}/"
    And I should see the link "{{ instance_name }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ instance_name }}"
    Then I should not see "Launch" in the "nav_tabs"
    And I should not see "Approve"
