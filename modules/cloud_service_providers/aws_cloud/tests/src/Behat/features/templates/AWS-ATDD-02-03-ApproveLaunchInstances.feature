@minimal @ci_job @launch_instance
Feature: Approve of launching an EC2 instance as "Cloud administrator"

  @api
  Scenario: Approve of launching an EC2 instance
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "{{ instance_name }}"
    And I should see the heading "{{ instance_name }}"
    And I should see "Launch" in the "nav_tabs"
    And I click "Approve"
    And the url should match "/approve"
    And I press "Approve"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}/"
    And I should see the success message "has been approved"
    And I should see neither error nor warning messages
