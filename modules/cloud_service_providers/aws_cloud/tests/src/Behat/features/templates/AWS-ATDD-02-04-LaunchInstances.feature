@minimal @ci_job @launch_instance
Feature: Launch an EC2 instance as "Authenticated User"

  @api
  Scenario: Launch an EC2 instance
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "{{ instance_name }}"
    And I should see the heading "{{ instance_name }}"
    And I click "Launch"
    And the url should match "/launch"
    And I check the box "Automatically terminate instance"
    And I press "Launch"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I should see the success message "has been launched"
    And I should see the text "pending" or "running" in the "{{ instance_name }}" row
    And I should see "{{ instance_type }}" in the "{{ instance_name }}" row
    And I should see "{{ availability_zone }}" in the "{{ instance_name }}" row
    And I should see neither error nor warning messages

  @api
  Scenario: View the EC2 instance
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I click "{{ instance_name }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/instance/"
    And I should see the link "Info"
    And I should see the link "Monitor"
    And I should see the link "Console output"
