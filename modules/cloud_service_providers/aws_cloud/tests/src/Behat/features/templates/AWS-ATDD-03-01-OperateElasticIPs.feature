@ci_job
Feature: Allocate and release an Elastic IP as "Authenticated User"

  @api
  Scenario: Add an internet gateway
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway/add"
    And I should see the heading "Add AWS Cloud internet gateway"
    And I enter "{{ internet_gateway_name }}" for "Name"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway"
    And I should see the link "{{ internet_gateway_name }}"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages

  @api
  Scenario: Attach an internet gateway
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway"
    And I should see the heading "AWS Cloud internet gateways"
    And I click "Refresh"
    And I should see the link "{{ internet_gateway_name }}"
    And I should see the button "Attach"
    And I wait {{ wait }} milliseconds
    And I click "{{ internet_gateway_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway/"
    And I click "Attach"
    And the url should match "/attach"
    And I select "{{ vpc_name }}" from "VPC CIDR (ID)"
    And I press "Attach"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway"
    And I should see the success message "is attached to VPC"
    And I should see neither error nor warning messages
    # List view.
    And I should see "Detach" in the "{{ internet_gateway_name }}" row
    And I should see "VPC" in the "Name" row
    And I should not see "VPC ID" in the "Name" row

  @api
  Scenario: View the internet gateway
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway"
    And I click "Refresh"
    And I should see the heading "AWS Cloud internet gateways"
    And I should see "{{ internet_gateway_name }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ internet_gateway_name }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway/"
    And I should see "{{ internet_gateway_name }}" in the "page_header"
    And I should see "{{ internet_gateway_name }}" in the "Tag name"
    And I should see "{{ vpc_name }}" in the "VPC ID"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api
  Scenario: Allocate an Elastic IP
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip/add"
    And I should see the heading "Add AWS Cloud Elastic IP"
    And I enter "{{ elastic_ip_name }}" for "Name"
    And I select "{{ elastic_ip_domain }}" from "Domain (Standard | VPC)"
    And I select "{{ region_code }}" from "Network border group"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Associate the Elastic IP
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip"
    And I click "Refresh"
    And I should see the link "{{ elastic_ip_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ elastic_ip_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip/"
    And I click "Associate Elastic IP" in the "actions"
    And the url should match "/associate"
    And I select "{{ resource_type }}" from "Resource type"
    And I select "{{ instance_name }}" from "Instance"
    And I wait {{ wait }} milliseconds
    And I press "Associate"
    # Without @javascript, an error occurs: Unable to load network interface by private IP.
    Then I should see the success message "associated with"
    And I should see neither error nor warning messages
    And I should see "Disassociate Elastic IP" in the "actions"

  @api
  Scenario: View the Elastic IP
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip"
    And I click "Refresh"
    And I should see the heading "AWS Cloud Elastic IPs"
    And I should see "{{ elastic_ip_name }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ elastic_ip_name }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip/"
    And I should see "{{ elastic_ip_name }}" in the "page_header"
    And I should see "{{ instance_name }}" in the "{{ resource_type }} ID"
    And I should see "{{ elastic_ip_domain }}" in the "Domain"
    And I should see "{{ region_code }}" in the "Network border group"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @javascript @api
  Scenario: Disassociate the Elastic IP
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip"
    And I click "Refresh"
    And I should see the link "{{ elastic_ip_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ elastic_ip_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip/"
    And I click "Disassociate Elastic IP" in the "actions"
    And the url should match "/disassociate"
    And I press "Disassociate Address"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip"
    And I should see the success message "Elastic IP disassociated from"
    And I should see neither error nor warning messages

  @api
  Scenario: Release the Elastic IP
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip"
    And I click "Refresh"
    And I should see the link "{{ elastic_ip_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ elastic_ip_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/elastic_ip"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    # No table exists.
    And I should not see the link "{{ elastic_ip_name }}"

  @api @javascript
  Scenario: Detach an internet gateway
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway"
    And I should see the link "{{ internet_gateway_name }}"
    And I should see the button "Detach"
    And I wait {{ wait }} milliseconds
    And I click "{{ internet_gateway_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway/"
    And I click "Detach"
    And the url should match "/detach"
    And I should see "Internet gateway information"
    And I press "Detach"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway"
    And I should see the success message "detached from VPC"
    And I should see neither error nor warning messages
    # The following step requires @javascript.
    And I should see the button "Attach"

  @api
  Scenario: Delete an internet gateway
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway"
    And I should see the heading "AWS Cloud internet gateways"
    And I click "Refresh"
    And I should see the link "{{ internet_gateway_name }}"
    And I should see the button "Attach"
    And I wait {{ wait }} milliseconds
    And I click "{{ internet_gateway_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway/"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/internet_gateway"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
