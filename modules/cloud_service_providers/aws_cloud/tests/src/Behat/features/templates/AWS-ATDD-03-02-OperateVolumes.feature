@ci_job
Feature: Create and delete a volume as "Authenticated User"

  @api
  Scenario: Create a volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/volume/add"
    And I should see the heading "Add AWS Cloud volume"
    And I enter "{{ volume_name }}" for "Name"
    And I enter "{{ volume_size }}" for "Size (GiB)"
    And I select "{{ availability_zone }}" from "Availability Zone"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "creating" in the "{{ volume_name }}" row

  @api
  Scenario: Attach the volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I should see the heading "AWS Cloud volumes"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ volume_name }}"
    And I should see "available" in the "{{ volume_name }}" row
    And I click "{{ volume_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/volume/"
    And I click "Attach"
    And the url should match "/attach"
    And I enter "{{ device_name }}" for "Device name"
    And I select "{{ instance_name }}" from "Instance ID"
    And I press "Attach"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I should see the success message "is attaching to"
    And I should see neither error nor warning messages
    And I should see "attaching" in the "{{ volume_name }}" row

  @api
  Scenario: View the volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I click "Refresh"
    And I should see the heading "AWS Cloud volumes"
    And I should see "{{ volume_name }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ volume_name }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/volume/"
    And I should see "{{ volume_name }}" in the "page_header"
    And I should see "{{ volume_size }}" in the "Size"
    And I should see "{{ availability_zone }}" in the "Availability Zone"
    And I should see "{{ instance_name }}" in the "Instance ID"
    # And I should see "{{ device_name }} in the "Device name"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api
  Scenario: Detach the volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I should see the heading "AWS Cloud volumes"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ volume_name }}"
    And I should see "in-use" in the "{{ volume_name }}" row
    And I click "{{ volume_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/volume/"
    And I click "Detach"
    And the url should match "/detach"
    And I press "Detach"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I should see the success message "is detaching from"
    And I should see neither error nor warning messages
    And I should see "detaching" in the "{{ volume_name }}" row

  @api
  Scenario: Delete the volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ volume_name }}"
    And I should see "available" in the "{{ volume_name }}" row
    And I click "{{ volume_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/volume/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    # volume_name exists in the listing view.
    And I should not see the link "{{ volume_name }}"
