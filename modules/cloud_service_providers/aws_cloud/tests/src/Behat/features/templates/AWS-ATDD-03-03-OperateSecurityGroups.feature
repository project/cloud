@ci_job
Feature: Copying/editing a security group as "Authenticated User"

  @api @javascript
  Scenario: Add a security group
    Given I am logged in as user "{{ user_name }}"
    # Add a VPC.
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc/add"
    And I enter "{{ vpc_name_1 }}" for "Name"
    And I enter "{{ ipv4_cidr }}" for "IPv4 CIDR block"
    And I press "Save"
    And I wait {{ wait }} milliseconds
    And I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ vpc_name_1 }}" in the table
    # Add a security group to the VPC
    # The following step requires @javascript.
    And I visit "/clouds/aws_cloud/{{ cloud_context }}/security_group/add"
    And I enter "{{ security_group_name_to_copy }}" for "Security group name"
    And I enter "{{ description }}" for "Description"
    And I select "{{ vpc_name_1 }}" from "VPC CIDR (ID)"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ security_group_name_to_copy }}" in the success_message
    And I should not see "Inbound rules"
    And I should see "Outbound rules"
    And I should see "All Traffic" in the "Outbound rules"
    And I should see "0.0.0.0/0" in the "Outbound rules"
    And I visit "/clouds/aws_cloud/{{ cloud_context }}/security_group"
    # List view.
    And I should see "{{ security_group_name_to_copy }}" in the "{{ vpc_name_1 }}" row
    And I should see "VPC" in the "Name" row
    And I should not see "VPC ID" in the "Name" row

  @api
  Scenario: Copy the security group
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/security_group"
    And I click "Refresh"
    And I should see the link "{{ security_group_name_to_copy }}"
    And I should see "{{ security_group_name_to_copy }}" in the "{{ vpc_name_1 }}" row
    And I wait {{ wait }} milliseconds
    And I click "{{ security_group_name_to_copy }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    And I click "Copy" in the "actions"
    And the url should match "/copy"
    And I enter "{{ security_group_name_copied }}" for "Security group name"
    And I enter "{{ description }}" for "Description"
    And I select "{{ vpc_name }}" from "VPC CIDR (ID)"
    And I press "Copy"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ security_group_name_copied }}" in the success_message
    And I should see "Outbound rules"

  @api
  Scenario: View the copied security group
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/security_group"
    And I click "Refresh"
    Then I should see the link "{{ security_group_name_copied }}"
    And I wait {{ wait }} milliseconds
    And I should see "{{ vpc_name }}" in the "{{ security_group_name_copied }}" row
    And I wait {{ wait }} milliseconds
    And I click "{{ security_group_name_copied }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    And I should see neither error nor warning messages
    And I should see "{{ security_group_name_copied }}"
    And I should see "{{ description }}"
    And I should see "{{ vpc_name }}" in the "VPC ID"
    And I should not see "01/01/1970 - 00:00"
    And I should not see "12/31/1969 - 23:59"
    And I should not see "Inbound rules"
    And I should see "Outbound rules"
    And I should see "All Traffic" in the "Outbound rules"
    And I should see "0.0.0.0/0" in the "Outbound rules"

  @api
  Scenario: Attach the copied security group to an instance
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I click "Refresh"
    And I should see the link "{{ instance_name }}"
    And I wait {{wait}} milliseconds
    And I click "{{ instance_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/instance/"
    And I should not see the link "{{ security_group_name_copied }}"
    And I should see the link "{{ security_group_name }}"
    And I click "Edit" in the "actions"
    And the url should match "/edit"
    And I additionally select "{{ security_group_name_copied }}" from "Security groups"
    And I press "Save"
    Then the url should match "/edit"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ instance_name }}" in the success_message
    And I click "{{ instance_name }}" in the success_message
    And I should see the link "{{ security_group_name_copied }}"
    And I should see the link "{{ security_group_name }}"

  @api
  Scenario: Detach the copied security group from the instance
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I click "Refresh"
    And I should see the link "{{ instance_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ instance_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/instance/"
    And I should see the link "{{ security_group_name_copied }}"
    And I should see the link "{{ security_group_name }}"
    And I click "Edit" in the "actions"
    And the url should match "/edit"
    And I select "{{ security_group_name }}" from "Security groups"
    And I press "Save"
    Then the url should match "/edit"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ instance_name }}" in the success_message
    And I click "{{ instance_name }}" in the success_message
    And I should not see the link "{{ security_group_name_copied }}"
    And I should see the link "{{ security_group_name }}"

  @api
  Scenario: Add rules to the security group
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/security_group"
    And I click "Refresh"
    And I should see the link "{{ security_group_name_copied }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ security_group_name_copied }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    And I click "Edit" in the "actions"
    And the url should match "/edit"
    # Add a rule to Inbound rules.
    And I fill in "From port" with "{{ port }}" in the row "0" in the "Inbound rules"
    And I fill in "To port" with "{{ port }}" in the row "0" in the "Inbound rules"
    And I select "{{ security_group_source }}" from "Source" in the row "0" in the "Inbound rules"
    And I fill in "CIDR IP" with "{{ ipv4_cidr }}" in the row "0" in the "Inbound rules"
    And I fill in "Description" with "{{ description }}" in the row "0" in the "Inbound rules"
    # Add a rule to Outbound rules.
    And I fill in "From port" with "{{ port }}" in the row "1" in the "Outbound rules"
    And I fill in "To port" with "{{ port }}" in the row "1" in the "Outbound rules"
    And I select "{{ security_group_source }}" from "Source" in the row "1" in the "Outbound rules"
    And I fill in "CIDR IP" with "{{ ipv4_cidr }}" in the row "1" in the "Outbound rules"
    And I fill in "Description" with "{{ description }}" in the row "1" in the "Outbound rules"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ security_group_name_copied }}" in the success_message
    And I click "{{ security_group_name_copied }}"
    And I should see "{{ port }}" in the "{{ description }}" row in the "Inbound rules"
    And I should see "{{ port }}" in the "{{ description }}" row in the "Inbound rules"
    And I should see "{{ ipv4_cidr }}" in the "{{ description }}" row in the "Inbound rules"
    And I should see "{{ port }}" in the "{{ description }}" row in the "Outbound rules"
    And I should see "{{ port }}" in the "{{ description }}" row in the "Outbound rules"
    And I should see "{{ ipv4_cidr }}" in the "{{ description }}" row in the "Outbound rules"

  @api @javascript
  Scenario: Remove a rule from the security group
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/security_group"
    And I click "Refresh"
    And I should see the link "{{ security_group_name_copied }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ security_group_name_copied }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    # Confirm default rule in Outbound rules.
    And I should see "0.0.0.0/0" in the "Outbound rules"
    And I click "Edit" in the "actions"
    And the url should match "/edit"
    And I follow "Remove rule" in a row with "0.0.0.0/0" set in "CIDR IP" in the "Outbound rules"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ security_group_name_copied }}" in the success_message
    And I click "{{ security_group_name_copied }}"
    # Without @javascript, the following step fails.
    And I should not see "0.0.0.0/0" in the "Outbound rules"

  @api
  Scenario: Delete two security groups
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/security_group"
    And I click "Refresh"
    And I should see the link "{{ security_group_name_to_copy }}"
    And I wait {{ wait }} milliseconds
    And I check the box in the "{{ security_group_name_to_copy }}" row
    And I should see the link "{{ security_group_name_copied }}"
    And I wait {{ wait }} milliseconds
    And I check the box in the "{{ security_group_name_copied }}" row
    And I select "Delete security group(s)" from "Action"
    And I press "Apply to selected items"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/delete_multiple"
    And I should see the text "{{ security_group_name_to_copy }}"
    And I should see the text "{{ security_group_name_copied }}"
    And I press "Delete"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group"
    And I should see the success message "has been deleted"
    And I should see the success message "Deleted 2 items"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ security_group_name_to_copy }}" in the table
    And I should not see the link "{{ security_group_name_copied }}" in the table

  @api
  Scenario: Delete the VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I click "Refresh"
    And I should see the link "{{ vpc_name_1 }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ vpc_name_1 }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc/"
    And I wait {{ wait }} milliseconds
    And I click "Delete" in the "actions"
    And I wait {{ wait }} milliseconds
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should not see the link "{{ vpc_name_1 }}" in the table
