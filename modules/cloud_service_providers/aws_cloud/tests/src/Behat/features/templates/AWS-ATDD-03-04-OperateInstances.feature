@ci_job
Feature: Launch an EC2 instance with various instance types as "Authenticated User"
  along with approval by "Cloud administrator"

  @api @javascript
  Scenario Outline: Create a Cloud launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}/aws_cloud/add"
    And I should see the heading "Add AWS Cloud"
    And I enter "<instance_name>" for "Name"
    And I select the option "<instance_type>" in the TomSelect element "{{ instance_type_tom_select_dom_id }}"
    And I wait for AJAX to finish
    And I select the option "{{ image_name }}" in the TomSelect element "{{ image_id_tom_select_dom_id }}"
    And I wait for AJAX to finish
    And I select "{{ availability_zone }}" from "Availability Zone"
    And I select "{{ vpc_name }}" from "VPC"
    And I wait for AJAX to finish
    And I select "{{ subnet_name }}" from "Subnet"
    And I select "{{ security_group_name }}" from "Security groups"
    And I select "{{ ssh_key_name }}" from "SSH key"
    And I select "{{ status }}" from "Status"
    And I press "Save"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}/"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the heading "<instance_name>"
    And I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the link "<instance_name>" in the table
    And I should see "<instance_type>" in the "<instance_name>" row
    And I should see "{{ security_group_name }}" in the "{{ instance_name }}" row
    And I should see "{{ ssh_key_name }}" in the "<instance_name>" row
    And I should see "{{ vpc_name }}" in the "{{ instance_name }}" row

    Examples:
    | instance_name          | instance_type         |
    | {{ instance_name_1 }}  | {{ instance_type_1 }} |
    | {{ instance_name_2 }}  | {{ instance_type_2 }} |

  @api
  Scenario Outline: Approve of launching an EC2 instance
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "<instance_name>"
    And I should see the heading "<instance_name>"
    And I click "Approve"
    And the url should match "/approve"
    And I press "Approve"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}/"
    And I should see "has been approved"
    And I should see neither error nor warning messages

    Examples:
      | instance_name         |
      | {{ instance_name_1 }} |
      | {{ instance_name_2 }} |

  @api
  Scenario Outline: Launch an EC2 instance
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "<instance_name>"
    And I click "Launch"
    And the url should match "/launch"
    And I check the box "Automatically terminate instance"
    And I press "Launch"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I should see the success message "has been launched"
    And I should see neither error nor warning messages
    And I should see the text "pending" or "running" in the "<instance_name>" row
    And I should see "<instance_type>" in the "<instance_name>" row
    And I should see "{{ availability_zone }}" in the "<instance_name>" row

    Examples:
      | instance_name         | instance_type         |
      | {{ instance_name_1 }} | {{ instance_type_1 }} |
      | {{ instance_name_2 }} | {{ instance_type_2 }} |

  @api
  Scenario Outline: View the EC2 instance
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I click "<instance_name>"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/instance/"
    And I should see neither error nor warning messages
    And I should see the link "Info"
    And I should see the link "Monitor"
    And I should see the link "Console output"

    Examples:
      | instance_name         |
      | {{ instance_name_1 }} |
      | {{ instance_name_2 }} |

  @api
  Scenario Outline: Terminate an EC2 instance
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I click "Refresh"
    And I should see the link "{{ instance_name }}"
    And I wait {{ wait }} milliseconds
    And I click "<instance_name>"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/instance/"
    And I click "Delete" in the "actions"
    And the url should match "/terminate"
    And I press "Delete | Terminate"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should see "<instance_name>" in the table
    And I should see the text "shutting-down" or "terminated" in the "<instance_name>" row

    Examples:
      | instance_name         |
      | {{ instance_name_1 }} |
      | {{ instance_name_2 }} |

  @api
  Scenario Outline: Delete a Cloud launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Refresh"
    And I should see the link "<instance_name>"
    And I wait {{ wait }} milliseconds
    And I click "<instance_name>"
    And the url should match "/clouds/design/server_template/{{ cloud_context }}/"
    And I click "Delete" in the "nav_tabs"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "<instance_name>" in the table

    Examples:
      | instance_name         |
      | {{ instance_name_1 }} |
      | {{ instance_name_2 }} |
