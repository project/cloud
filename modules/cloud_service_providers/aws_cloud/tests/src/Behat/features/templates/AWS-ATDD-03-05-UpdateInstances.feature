@ci_job
Feature: Edit an instance as "Authenticated User"

  @api #@javascript
  Scenario: Attach a default security group to an instance
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I click "Refresh"
    And I should see the link "{{ instance_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ instance_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/instance/"
    And I should not see the link "default"
    And I should see the link "{{ security_group_name }}"
    And I click "Edit" in the "actions"
    And the url should match "/edit"
    And I additionally select "default" from "Security groups"
    And I press "Save"
    #And I wait for AJAX to finish
    Then the url should match "/edit"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ instance_name }}" in the success_message
    And I click "{{ instance_name }}" in the success_message
    And I should see the link "default"
    And I should see the link "{{ security_group_name }}"

  @api #@javascript
  Scenario: Detach the default security group from the instance
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I click "Refresh"
    And I should see the link "{{ instance_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ instance_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/instance/"
    And I should see the link "default"
    And I should see the link "{{ security_group_name }}"
    And I click "Edit" in the "actions"
    And the url should match "/edit"
    And I select "{{ security_group_name }}" from "Security groups"
    And I press "Save"
    #And I wait for AJAX to finish
    Then the url should match "/edit"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ instance_name }}" in the success_message
    And I click "{{ instance_name }}" in the success_message
    And I should not see the link "default"
    And I should see the link "{{ security_group_name }}"
