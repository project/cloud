@ci_job
Feature: Create and view a key pair as "Authenticated User"

  @api
  Scenario: Create an SSH key pair
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/key_pair/add"
    And I should see the heading "Add AWS Cloud key pair"
    And I enter "{{ ssh_key_name_operate }}" for "Key pair name"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/key_pair"
    And I should see the success message "has been created"
    And I should see no error message
    And I should see the link "{{ ssh_key_name_operate }}"

  @api
  Scenario: View an SSH key pair
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/key_pair"
    And I click "Refresh"
    And I should see the heading "AWS Cloud key pairs"
    And I should see the link "{{ ssh_key_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ ssh_key_name_operate }}" in the table
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/key_pair/"
    And I should see "{{ ssh_key_name_operate }}" in the "page_header"
    And I should see "{{ ssh_key_name_operate }}" in the "details"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api
  Scenario: Delete the SSH key pair
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/key_pair"
    And I click "Refresh"
    And I should see the link "{{ ssh_key_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ ssh_key_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/key_pair/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/key_pair"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    # No table exists.
    And I should not see the link "{{ ssh_key_name_operate }}"
