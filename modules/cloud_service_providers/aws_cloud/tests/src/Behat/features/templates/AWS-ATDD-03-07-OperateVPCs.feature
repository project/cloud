@ci_job
Feature: Create and view a VPC as "Authenticated User"

  @api
  Scenario: Create a VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc/add"
    And I should see the heading "Add AWS Cloud VPC"
    And I enter "{{ vpc_name_operate }}" for "Name"
    And I enter "{{ ipv4_cidr }}" for "IPv4 CIDR block"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ vpc_name_operate }}" in the table

  @api
  Scenario: View the VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I click "Refresh"
    And I should see the heading "AWS Cloud VPCs"
    And I should see the link "{{ vpc_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ vpc_name_operate }}" in the table
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see "{{ vpc_name_operate }}" in the "page_header"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api
  Scenario: Update the VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I click "Refresh"
    And I should see the heading "AWS Cloud VPCs"
    And I should see the link "{{ vpc_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ vpc_name_operate }}" in the table
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I click "Edit"
    And the url should match "/edit"
    And I should see "Edit {{ vpc_name_operate }}" in the "page_header"
    And I enter "{{ vpc_name_operate_updated }}" for "Name"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ vpc_name_operate_updated }}" in the table
    And I should not see the link "{{ vpc_name_operate }}" in the table

  @api @javascript
  Scenario: Delete the VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I click "Refresh"
    And I should see the link "{{ vpc_name_operate_updated }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ vpc_name_operate_updated }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc/"
    And I should see "{{ vpc_name_operate_updated }}" in the "page_header"
    And I press "Others"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages
    And I wait {{ wait }} milliseconds
    And I click "Delete" in the "actions"
    And I wait {{ wait }} milliseconds
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    # No table exists.
    # Without @javascript, the following step fails.
    And I should not see the link "{{ vpc_name_operate_updated }}"
