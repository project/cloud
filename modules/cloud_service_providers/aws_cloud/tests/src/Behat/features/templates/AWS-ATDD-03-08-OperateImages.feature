@ci_job
Feature: Create and view an image as "Authenticated User"

  @api @javascript
  Scenario: Import an AMI image
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/images/import"
    And I should see the heading "Import images"
    And I enter "{{ image_id_operate }}" for "Image IDs"
    # This requires @javascript for image search.
    And I press "Import"
    And I wait for AJAX to finish
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I should see the success message "Imported 1 images"
    And I should see neither error nor warning messages
    And I should see the link "{{ image_name_operate }}" in the table

  @api
  Scenario: View the image
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I click "Refresh"
    And I should see the heading "AWS Cloud images"
    And I should see "{{ image_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ image_name_operate }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I should see "{{ image_name_operate }}" in the "page_header"
    And I should see "{{ image_name_operate }}" in the "details"
    And I should see "{{ importer_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api
  Scenario: Update the image
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I click "Refresh"
    And I should see the heading "AWS Cloud images"
    And I should see "{{ image_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ image_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I click "Edit"
    And the url should match "/edit"
    And I should see "Edit {{ image_name_operate }}" in the "page_header"
    And I enter "{{ image_name_operate_updated }}" for "Name"
    And I press "Save"
    Then the url should match "/edit"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Make sure a duplicate image will not be created
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/images/import"
    And I should see the heading "Import images"
    And I enter "{{ image_id_operate }}" for "Image IDs"
    And I press "Import"
    And I wait for AJAX to finish
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I should see the success message "Imported 1 images"
    And I should see neither error nor warning messages
    And I should see the link "{{ image_name_operate_updated }}" in the table
    And I should not see the link "{{ image_name_operate }}" in the table

  @api
  Scenario: Delete an AMI image
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I click "Refresh"
    And I should see the link "{{ image_name_operate_updated }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ image_name_operate_updated }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/image/"
    And I should see "{{ image_name_operate_updated }}" in the "details"
    And I should see "{{ importer_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages
    And I click "Delete" in the "actions"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/image/"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    # No table exists.
    And I should not see the link "{{ image_name_operate_updated }}"
