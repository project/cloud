@ci_job
Feature: Create and view a subnet as "Authenticated User"

  @api
  Scenario: Create a VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc/add"
    And I should see the heading "Add AWS Cloud VPC"
    And I enter "{{ vpc_name_operate }}" for "Name"
    And I enter "{{ ipv4_cidr }}" for "IPv4 CIDR block"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ vpc_name_operate }}" in the table

  @api
  Scenario: Create a subnet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/subnet/add"
    And I should see the heading "Add AWS Cloud subnet"
    And I enter "{{ subnet_name_operate }}" for "Name"
    And I select "{{ vpc_name_operate }}" from "VPC CIDR (ID)"
    And I select "{{ availability_zone }}" from "Availability Zone"
    And I enter "{{ ipv4_cidr }}" for "IPv4 CIDR block"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/subnet"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "available" in the "{{ subnet_name_operate }}" row

  @api
  Scenario: View the subnet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/subnet"
    And I click "Refresh"
    And I should see the heading "AWS Cloud subnets"
    And I should see "{{ subnet_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ subnet_name_operate }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/subnet/"
    And I should see "{{ subnet_name_operate }}" in the "page_header"
    And I should see "{{ ipv4_cidr }}" in the "CIDR Block"
    And I should see "{{ vpc_name_operate }}" in the "VPC"
    And I should see "{{ availability_zone }}" in the "Zone name"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api
  Scenario: Update the subnet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/subnet"
    And I click "Refresh"
    And I should see the heading "AWS Cloud subnets"
    And I should see "{{ subnet_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ subnet_name_operate }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/subnet/"
    And I click "Edit" in the "actions"
    And the url should match "/edit"
    And I enter "{{ subnet_name_operate_updated }}" for "Name"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/subnet"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages

  @api
  Scenario: Delete the subnet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/subnet"
    And I click "Refresh"
    And I should see the link "{{ subnet_name_operate_updated }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ subnet_name_operate_updated }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/subnet/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/subnet"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    # No table exists.
    And I should not see the link "{{ subnet_name_operate_updated }}"

  @api
  Scenario: Delete the VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I click "Refresh"
    And I should see the link "{{ vpc_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ vpc_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc/"
    And I wait {{ wait }} milliseconds
    And I click "Delete" in the "actions"
    And I wait {{ wait }} milliseconds
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should not see the link "{{ vpc_name_operate }}"
