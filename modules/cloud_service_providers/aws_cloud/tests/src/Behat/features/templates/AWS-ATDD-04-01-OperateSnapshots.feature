@ci_job
Feature: Create and view a snapshot as "Authenticated User"

  @api
  Scenario: Create a volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/volume/add"
    And I should see the heading "Add AWS Cloud volume"
    And I enter "{{ volume_name_operate }}" for "Name"
    And I enter "{{ volume_size }}" for "Size (GiB)"
    And I select "{{ availability_zone }}" from "Availability Zone"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "creating" in the "{{ volume_name_operate }}" row

  @api
  Scenario: Refresh IAM permissions
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/config/system/cron"
    Then I should see the heading "Cron"
    And I click "Run cron"
    And I run drush cr

  @api
  Scenario: Create a snapshot
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/snapshot/add"
    And I should see the heading "Add AWS Cloud snapshot"
    And I enter "{{ snapshot_name_operate }}" for "Name"
    And I select "{{ volume_name_operate }}" from "Volume ID"
    And I enter "{{ description }}" for "Description"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ snapshot_name_operate }}" in the table
    And I should see "pending" in the "{{ snapshot_name_operate }}" row

  @api
  Scenario: Refresh the list with the "Cloud administrator" role
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I should see the link "{{ snapshot_name_operate }}" in the table
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ snapshot_name_operate }}"

  @api
  Scenario: View the snapshot
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I click "Refresh"
    And I should see the heading "AWS Cloud snapshots"
    And I should see "{{ snapshot_name_operate }}" in the table
    And I should see "completed" in the "{{ snapshot_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I click "{{ snapshot_name_operate }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/snapshot/"
    And I should see "{{ snapshot_name_operate }}" in the "page_header"
    # Temporarily commented out until the view includes the snapshot name along with the volume name.
    # And I should see "{{ snapshot_name_operate }}" in the "details"
    # And I should see "{{ volume_name_operate }}" in the "details"
    And I should see "{{ description }}" in the "Description"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api
  Scenario: Update the snapshot
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I click "Refresh"
    And I should see the heading "AWS Cloud snapshots"
    And I should see "{{ snapshot_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ snapshot_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I click "Edit"
    And the url should match "/edit"
    And I should see "Edit {{ snapshot_name_operate }}" in the "page_header"
    And I enter "{{ snapshot_name_operate_updated }}" for "Name"
    # As the AWS specification, the Description is not editable.
    # And I enter "{{ description_updated }}" for "Description"
    And I press "Save"
    Then the url should match "/edit"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages

  @api
  Scenario: Delete the snapshot
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ snapshot_name_operate_updated }}"
    And I click "{{ snapshot_name_operate_updated }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/snapshot/"
    And I click "Delete" in the "actions"
    And I wait {{ wait }} milliseconds
    And the url should match "/delete"
    And I wait {{ wait }} milliseconds
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    # No table exists.
    And I should not see the link "{{ snapshot_name_operate_updated }}"

  @api
  Scenario: Create a snapshot with the "Cloud administrator" role
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/snapshot/add"
    And I should see the heading "Add AWS Cloud snapshot"
    And I enter "{{ snapshot_name_operate }}" for "Name"
    And I select "{{ volume_name_operate }}" from "Volume ID"
    And I enter "{{ description }}" for "Description"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ snapshot_name_operate }}" in the table
    And I should see "pending" in the "{{ snapshot_name_operate }}" row
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ snapshot_name_operate }}"

  @api
  Scenario: Refresh the list
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I should not see the link "{{ snapshot_name_operate }}" in the table
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should not see the link "{{ snapshot_name_operate }}"

  @api
  Scenario: Delete the snapshot
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ snapshot_name_operate }}"
    And I click "{{ snapshot_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/snapshot/"
    And I wait {{ wait }} milliseconds
    And I click "Delete" in the "actions"
    And I wait {{ wait }} milliseconds
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/snapshot"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    # No table exists.
    And I should not see the link "{{ snapshot_name_operate }}"

  @api
  Scenario: Delete the volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ volume_name_operate }}"
    And I should see "available" in the "{{ volume_name_operate }}" row
    And I click "{{ volume_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/volume/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/volume"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    # volume_name exists in the listing view.
    And I should not see the link "{{ volume_name_operate }}"
