@ci_job
Feature: Create and view a network interface as "Authenticated User"

  @api
  Scenario: Add a VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc/add"
    And I should see the heading "Add AWS Cloud VPC"
    And I enter "{{ vpc_name_operate }}" for "Name"
    And I enter "{{ ipv4_cidr }}" for "IPv4 CIDR block"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ vpc_name_operate }}" in the table

  @api @javascript
  Scenario: Add a subnet
    Given I am logged in as user "{{ user_name }}"
    # The following step requires @javascript.
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/subnet/add"
    And I should see the heading "Add AWS Cloud subnet"
    And I enter "{{ subnet_name_operate }}" for "Name"
    And I select "{{ vpc_name_operate }}" from "VPC CIDR (ID)"
    And I select "{{ availability_zone }}" from "Availability Zone"
    And I enter "{{ ipv4_cidr }}" for "IPv4 CIDR block"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/subnet"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ subnet_name_operate }}" in the table

  @api @javascript
  Scenario: Add a security group
    Given I am logged in as user "{{ user_name }}"
    # The following step requires @javascript.
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/security_group/add"
    And I should see the heading "Add AWS Cloud security group"
    And I should see neither error nor warning messages
    And I enter "{{ security_group_name_operate }}" for "Security group name"
    And I enter "{{ description }}" for "Description"
    And I select "{{ vpc_name_operate }}" from "VPC CIDR (ID)"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ security_group_name_operate }}" in the success_message
    And I should see "Outbound rules"

  @api
  Scenario: Create a network interface
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/network_interface/add"
    And I should see the heading "Add AWS Cloud network interface"
    And I enter "{{ network_interface_name_operate }}" for "Name"
    And I enter "{{ description }}" for "Description"
    And I select "{{ subnet_name_operate }}" from "Subnet"
    And I select "{{ security_group_name_operate }}" from "Security groups"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/network_interface"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_interface_name_operate }}" in the table

  @api
  Scenario: View the network interface
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/network_interface"
    And I click "Refresh"
    And I should see the heading "AWS Cloud network interfaces"
    And I should see the link "{{ network_interface_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ network_interface_name_operate }}" in the table
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/network_interface/"
    And I should see "{{ network_interface_name_operate }}" in the "page_header"
    And I should see "{{ description }}" in the "Description"
    And I should see "{{ vpc_name_operate }}" in the "VPC ID"
    # Temporarily commented out until the view includes the name of the resources.
    # And I should see "{{ security_group_name_operate }}" in the "Security groups"
    # And I should see "{{ subnet_name_operate }}" in the "Subnet ID"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api
  Scenario: Update the network interface
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/network_interface"
    And I click "Refresh"
    And I should see the heading "AWS Cloud network interfaces"
    And I should see the link "{{ network_interface_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ network_interface_name_operate }}" in the table
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/network_interface/"
    And I click "Edit"
    And the url should match "/edit"
    And I should see "Edit {{ network_interface_name_operate }}" in the "page_header"
    And I enter "{{ network_interface_name_operate_updated }}" for "Name"
    And I enter "{{ description_updated }}" for "Description"
    And I press "Save"
    Then the url should match "/edit"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages

  @api
  Scenario: Delete the network interface
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/network_interface"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ network_interface_name_operate_updated }}"
    And I click "{{ network_interface_name_operate_updated }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/network_interface/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/network_interface"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    # volume_name exists in the listing view.
    And I should not see the link "{{ network_interface_name_operate_updated }}"

  @api
  Scenario: Delete the security group
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/security_group"
    And I click "Refresh"
    And I should see the link "{{ security_group_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ security_group_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/security_group/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/security_group"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ security_group_name_operate }}"

  @api
  Scenario: Delete the subnet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/subnet"
    And I click "Refresh"
    And I should see the link "{{ subnet_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ subnet_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/subnet/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/subnet"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    # No table exists.
    And I should not see the link "{{ subnet_name_operate }}"

  @api @javascript
  Scenario: Delete the VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I click "Refresh"
    And I should see the link "{{ vpc_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ vpc_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc/"
    And I wait {{ wait }} milliseconds
    And I click "Delete" in the "actions"
    And I wait {{ wait }} milliseconds
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    # No table exists.
    # Without @javascript, the following step fails.
    And I should not see the link "{{ vpc_name_operate }}"
