@ci_job
Feature: Create and view a VPC peering connection as "Authenticated User"

  @api
  Scenario: Add a requester VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc/add"
    And I should see the heading "Add AWS Cloud VPC"
    And I enter "{{ vpc_requester_name }}" for "Name"
    And I enter "{{ ipv4_cidr_requester }}" for "IPv4 CIDR block"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ vpc_requester_name }}" in the table

  @api
  Scenario: Add an acceptor VPC
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc/add"
    And I should see the heading "Add AWS Cloud VPC"
    And I enter "{{ vpc_accepter_name }}" for "Name"
    # needs an VPC with a different cidr rance from the first VPC
    And I enter "{{ ipv4_cidr_accepter }}" for "IPv4 CIDR block"
    And I press "Save"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ vpc_accepter_name }}" in the table

  @api
  Scenario: Create a VPC peering connection
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection/add"
    And I should see the heading "Add AWS Cloud VPC peering connection"
    And I enter "{{ vpc_peering_connection_name_operate }}" for "Name"
    And I select "{{ vpc_requester_name }}" from "Requester VPC ID"
    And I select "{{ vpc_accepter_name }}" from "Accepter VPC ID"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    # List view.
    And I should see "pending-acceptance" in the "{{ vpc_peering_connection_name_operate }}" row
    And I should see "VPC" in the "Name" row
    And I should not see "VPC ID" in the "Name" row

  @api
  Scenario: Accept the VPC peering connection
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ vpc_peering_connection_name_operate }}"
    And I should see "pending-acceptance" in the "{{ vpc_peering_connection_name_operate }}" row
    And I click "{{ vpc_peering_connection_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection/"
    And I click "Accept" in the "actions"
    And the url should match "/accept"
    And I press "Accept"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection"
    And I should see the success message "has been accepted"
    And I should see neither error nor warning messages
    And I should see "active" in the "{{ vpc_peering_connection_name_operate }}" row

  @api
  Scenario: View the VPC peering connection
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection"
    And I click "Refresh"
    And I should see the heading "AWS Cloud VPC peering connections"
    And I should see "{{ vpc_peering_connection_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ vpc_peering_connection_name_operate }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection/"
    And I should see "{{ vpc_peering_connection_name_operate }}" in the "page_header"
    And I should see "{{ vpc_requester_name }}" in the "Requester VPC ID"
    And I should see "{{ ipv4_cidr_requester }}" in the "Requester CIDR Block"
    And I should see "{{ vpc_accepter_name }}" in the "Accepter VPC ID"
    And I should see "{{ ipv4_cidr_accepter }}" in the "Accepter CIDR Block"
    And I should see "{{ vpc_peering_connection_name_operate }} in the "Tag name"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api
  Scenario: Update the VPC peering connection
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection"
    And I click "Refresh"
    And I should see the heading "AWS Cloud VPC peering connections"
    And I should see "{{ vpc_peering_connection_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ vpc_peering_connection_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection"
    And I click "Edit"
    And the url should match "/edit"
    And I should see "Edit {{ vpc_peering_connection_name_operate }}" in the "page_header"
    And I enter "{{ vpc_peering_connection_name_operate_updated }}" for "Name"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ vpc_peering_connection_name_operate_updated }}" in the table


  @api
  Scenario: Delete the VPC peering connection
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ vpc_peering_connection_name_operate_updated }}"
    And I should see "active" in the "{{ vpc_peering_connection_name_operate_updated }}" row
    And I click "{{ vpc_peering_connection_name_operate_updated }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc_peering_connection"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should see "deleted" in the "{{ vpc_peering_connection_name_operate_updated }}" row

  @api
  Scenario: Delete the VPCs
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ vpc_requester_name }}"
    And I check the box in the "{{ vpc_requester_name }}" row
    And I should see the link "{{ vpc_accepter_name }}"
    And I check the box in the "{{ vpc_accepter_name }}" row
    And I select "Delete VPC(s)" from "Action"
    And I press "Apply to selected items"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/vpc/delete_multiple"
    And I should see the text "{{ vpc_requester_name }}"
    And I should see the text "{{ vpc_accepter_name }}"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/vpc"
    And I should see the success message "has been deleted"
    And I should see the success message "Deleted 2 items"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should not see the link "{{ vpc_requester_name }}"
    And I should not see the link "{{ vpc_accepter_name }}"
