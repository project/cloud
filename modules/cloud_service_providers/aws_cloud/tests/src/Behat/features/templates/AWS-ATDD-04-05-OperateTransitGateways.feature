@ci_job
Feature: Create and view a transit gateway as "Authenticated User"

  @api
  Scenario: Create a transit gateway
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/transit_gateway/add"
    And I should see the heading "Add AWS Cloud transit gateway"
    And I enter "{{ transit_gateway_name_operate }}" for "Name"
    And I enter "{{ description }}" for "Description"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/transit_gateway"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "pending" in the "{{ transit_gateway_name_operate }}" row
    And I should see the link "{{ transit_gateway_name_operate }}"

  @api
  Scenario: View the transit gateway
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/transit_gateway"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the heading "AWS Cloud transit gateways"
    And I should see "{{ transit_gateway_name_operate }}" in the table
    And I click "{{ transit_gateway_name_operate }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/transit_gateway/"
    And I should see "{{ transit_gateway_name_operate }}" in the "page_header"
    # Temporarily commented out until the view includes the description of the transit gateway.
    # And I should see "{{ description }}" in the "Description"
    And I should see "{{ transit_gateway_name_operate }} in the "Tag name"
    And I should see "{{ user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api
  Scenario: Update the transit gateway
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/transit_gateway"
    # It takes a while for the new transit gateway to be an available state
    # To update or delete the transit gateway, the state should be available, not pending
    And I wait {{ wait_transit_gateway }} milliseconds
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the heading "AWS Cloud transit gateways"
    And I should see "{{ transit_gateway_name_operate }}" in the table
    And I should see "available" in the "{{ transit_gateway_name_operate }}" row
    And I click "{{ transit_gateway_name_operate }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/transit_gateway/"
    And I click "Edit"
    And the url should match "/edit"
    And I should see "Edit {{ transit_gateway_name_operate }}" in the "page_header"
    And I enter "{{ transit_gateway_name_operate_updated }}" for "Name"
    And I enter "{{ description_updated }}" for "Description"
    And I press "Save"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/transit_gateway"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ transit_gateway_name_operate_updated }}" in the table

  @api
  Scenario: Delete the transit gateway
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/transit_gateway"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ transit_gateway_name_operate_updated }}"
    And I click "{{ transit_gateway_name_operate_updated }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/transit_gateway/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/transit_gateway"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should see "deleting" in the "{{ transit_gateway_name_operate_updated }}" row
