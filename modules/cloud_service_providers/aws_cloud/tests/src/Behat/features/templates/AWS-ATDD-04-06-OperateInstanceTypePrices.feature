@ci_job
Feature: View and refresh an instance type prices as "Authenticated User"

  @api
  Scenario: View and refresh a list of instance type prices
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/instance_type_price"
    And I should see "{{ instance_type }}" in the table
    And I click "Refresh"
    Then I should see the success message "Updated instance type prices"
    And I should see "{{ instance_type }}" in the table
    And I should see neither error nor warning messages
