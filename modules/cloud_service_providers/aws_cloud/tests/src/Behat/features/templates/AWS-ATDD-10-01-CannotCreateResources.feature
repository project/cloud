@ci_job
Feature: Make sure "Authenticated user" cannot add resources

  @api @javascript
  Scenario: Cannot add an instance
    Given I am logged in as a user with the "Authenticated user" role
    When I visit "/clouds/aws_cloud/instance"
    And I should see the heading "All AWS Cloud instances"
    Then I should not see "Add | Launch AWS Cloud instance"

  @api @javascript
  Scenario: Cannot add a launch template
    Given I am logged in as a user with the "Authenticated user" role
    When I go to "/clouds/design/server_template/{{ cloud_context }}"
    Then I should get a 403 HTTP response
    When I go to "clouds/design/server_template/{{ cloud_context }}/aws_cloud/add"
    Then I should get a 403 HTTP response

  @api @javascript
  Scenario Outline: Cannot add a resource
    Given I am logged in as a user with the "Authenticated user" role
    When I visit "/clouds/aws_cloud/<resource_type>"
    And I should see the heading "All AWS Cloud <resource_type_name>"
    Then I should not see "Add AWS Cloud <resource_type_name>"
    And I go to "/clouds/aws_cloud/{{ cloud_context }}/<resource_type>/add"
    And I should get a 403 HTTP response

    Examples:
      | resource_type          | resource_type_name      |
      | image                  | images                  |
      | security_group         | security groups         |
      | elastic_ip             | Elastic IPs             |
      | key_pair               | key pairs               |
      | volume                 | volumes                 |
      | snapshot               | snapshots               |
      | network_interface      | network interfaces      |
      | vpc                    | VPCs                    |
      | subnet                 | subnets                 |
      | vpc_peering_connection | VPC peering connections |
      | internet_gateway       | internet gateways       |
      | carrier_gateway        | carrier gateways        |
      | transit_gateway        | transit gateways        |
