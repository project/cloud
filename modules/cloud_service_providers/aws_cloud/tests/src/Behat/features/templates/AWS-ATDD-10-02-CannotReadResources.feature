@ci_job
Feature: Make sure "Authenticated user" cannot see resources or Refresh button
  as the user by default has no access to any cloud service providers

  @api @javascript
  Scenario: Cannot see the created cloud service provider
    Given I am logged in as a user with the "Authenticated user" role
    When I visit "/clouds"
    Then I should not see "{{ cloud_service_provider_name }}"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario Outline: Cannot see the created "<resource_type>"
    Given I am logged in as a user with the "Authenticated user" role
    When I visit "/clouds/aws_cloud/<resource_type>"
    Then I should not see the link "Refresh"
    And I should not see "<resource_name>"
    And I should see neither error nor warning messages

    Examples:
      | resource_type          | resource_name                             |
      | instance               | {{ instance_name }}                       |
      | image                  | {{ image_name }}                          |
      | security_group         | {{ security_group_name }}                 |
      | elastic_ip             | {{ elastic_ip_name }}                     |
      | key_pair               | {{ ssh_key_name }}                        |
      | volume                 | {{ volume_name }}                         |
      | snapshot               | {{ snapshot_name_operate }}               |
      | network_interface      | {{ network_interface_name_operate }}      |
      | subnet                 | {{ subnet_name }}                         |
      | vpc                    | {{ vpc_name }}                            |
      | vpc_peering_connection | {{ vpc_peering_connection_name_operate }} |
      | internet_gateway       | {{ internet_gateway_name }}               |
      | carrier_gateway        | {{ carrier_gateway_name_operate }}        |
      | transit_gateway        | {{ transit_gateway_name_operate }}        |
