@minimal @ci_job
Feature: Terminate an instance as "Authenticated user"

  @api
  Scenario: Terminate an EC2 instance
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I click "Refresh"
    And I should see the link "{{ instance_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ instance_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/instance/"
    And I click "Delete" in the "actions"
    And the url should match "/terminate"
    And I press "Delete | Terminate"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/instance"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should see "{{ instance_name }}" in the table
    And I should see the text "shutting-down" or "terminated" in the "{{ instance_name }}" row

  @api
  Scenario: Delete a Cloud launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Refresh"
    And I should see the link "{{ instance_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ instance_name }}"
    And the url should match "/clouds/design/server_template/{{ cloud_context }}/"
    And I click "Delete" in the "nav_tabs"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ instance_name }}"
