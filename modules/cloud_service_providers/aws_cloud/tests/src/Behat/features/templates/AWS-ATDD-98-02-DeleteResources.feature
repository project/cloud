@minimal @ci_job
Feature: Delete the created resources as "Authenticated user"

  @api @javascript
  Scenario Outline: Delete the "<resource_type>"
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/<resource_type>"
    And I click "Refresh"
    And I should see the link "<resource_name>"
    And I wait {{ wait }} milliseconds
    And I click "<resource_name>"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/<resource_type>/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/<resource_type>"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    # No table exists.
    # Without @javascript, the following step fails.
    And I should not see the link "<resource_name>"

    Examples:
      | resource_type  | resource_name             |
      | security_group | {{ security_group_name }} |
      | subnet         | {{ subnet_name }}         |
      | vpc            | {{ vpc_name }}            |
      | key_pair       | {{ ssh_key_name }}        |

  @api
  Scenario: Cannot delete an AMI image
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I click "Refresh"
    And I should see the link "{{ image_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ image_name }}"
    Then the url should match "/clouds/aws_cloud/{{ cloud_context }}/image/"
    And I should not see the "Delete" button
