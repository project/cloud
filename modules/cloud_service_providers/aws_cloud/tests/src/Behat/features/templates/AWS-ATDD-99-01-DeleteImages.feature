@minimal @ci_job
Feature: Delete the created resources as "Cloud administrator"

  @api
  Scenario: Delete an AMI image
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I click "Refresh"
    And I should see the link "{{ image_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ image_name }}"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/image/"
    And I click "Delete" in the "actions"
    And the url should match "/clouds/aws_cloud/{{ cloud_context }}/image/"
    And I press "Delete"
    Then I should be on "/clouds/aws_cloud/{{ cloud_context }}/image"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    # No table exists.
    And I should not see the link "{{ image_name }}"
