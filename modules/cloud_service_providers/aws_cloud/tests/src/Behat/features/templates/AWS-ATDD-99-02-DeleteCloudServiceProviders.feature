@minimal @cleanup
Feature: Delete the created resources as "Cloud administrator"

  @ci_job @FIXME
#  Temporarily comment out until the Drupal core+sqlite issue is fixed.
#  See also: https://www.drupal.org/i/1120020
#  This intermittent issue doesn't occur in the env. using mariaDB.
  @api
  Scenario: Delete a created AWS Cloud service provider
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config"
    And I check the box in the "{{ cloud_service_provider_name }}" row
    And I select "Delete cloud service provider(s)" from "Action"
    And I press "Apply to selected items"
    And I press "Delete"
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "Deleted 1 item."
    And I should see neither error nor warning messages
    And I should not see the link "{{ cloud_service_provider_name }}"
