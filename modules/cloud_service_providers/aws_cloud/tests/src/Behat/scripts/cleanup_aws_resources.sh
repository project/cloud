#!/bin/bash
#
# Clean up AWS resources.
#

# List the plural resource names in the order of fewer dependencies.
readonly ALL=('instances' 'launch_templates' 'security_groups' 'elastic_ips' 'internet_gateways' 'subnets' 'vpcs' 'key_pairs' 'snapshots' 'volumes')
declare -a awscli_args

# Per resource name(= an element of array ALL), define describe/delete commands and arguments if any other than default.
# Default values: [describe_cmd]: describe-${resource_name}
#                 [filters_name]: Name
#                 [delete_cmd]: delete-${resource_name}
declare -A instances=(
  [scenario]='Scenario: Terminate an EC2 instance'
  [describe_query]='Reservations[*].Instances[*].InstanceId'
  [delete_cmd]='terminate-instances'
  [delete_arg]='--instance-ids'
)

declare -A launch_templates=(
  [scenario]='Scenario: Delete a Cloud launch template'
  [describe_cmd]='describe-launch-templates'
  [describe_query]='LaunchTemplates[*].LaunchTemplateId'
  # --filters Name=launch-template-name,Values=string.
  [filters_tag_name]='launch-template-name'
  [delete_cmd]='delete-launch-template'
  [delete_arg]='--launch-template-id'
)

declare -A security_groups=(
  [scenario]='Scenario: Delete a security group'
  [describe_cmd]='describe-security-groups'
  [describe_query]='SecurityGroups[*].GroupId'
  [delete_cmd]='delete-security-group'
  [delete_arg]='--group-id'
)

declare -A elastic_ips=(
  [scenario_detach]='Scenario: Disassociate the Elastic IPs'
  [describe_query_to_detach]='Addresses[*].AssociationId'
  [detach_cmd]='disassociate-address'
  [detach_arg]='--association-id'
  [filters_arg_addition_to_detach]='Name=association-id,Values=*'
  [scenario]='Scenario: Release an Elastic IP'
  [describe_cmd]='describe-addresses'
  [describe_query]='Addresses[*].AllocationId'
  [delete_cmd]='release-address'
  [delete_arg]='--allocation-id'

)

declare -A internet_gateways=(
  [scenario_detach]='Scenario: Detach the internet gateways'
  [describe_cmd]='describe-internet-gateways'
  [describe_query_attached_vpc]='InternetGateways[*].[Attachments[*].VpcId]'
  [describe_query_to_detach]='InternetGateways[*].[InternetGatewayId,Attachments[*].VpcId]'
  [detach_cmd]='detach-internet-gateway'
  [detach_arg]='--vpc-id'
  # After detach, we can delete an internet gateway.
  [scenario]='Scenario: Delete an internet gateway'
  [describe_query_after_detach]='InternetGateways[*].InternetGatewayId'
  [delete_cmd]='delete-internet-gateway'
  [delete_arg]='--internet-gateway-id'
)

declare -A subnets=(
  [scenario]='Scenario: Delete a subnet'
  [describe_query]='Subnets[*].SubnetId'
  [delete_cmd]='delete-subnet'
  [delete_arg]='--subnet-id'
)

declare -A vpcs=(
  [scenario]='Scenario: Delete a VPC'
  [describe_query]='Vpcs[*].VpcId'
  [delete_cmd]='delete-vpc'
  [delete_arg]='--vpc-id'
)

declare -A key_pairs=(
  [scenario]='Scenario: Delete the SSH key pairs'
  [describe_cmd]='describe-key-pairs'
  [describe_query]='KeyPairs[*].KeyName'
  # --filters Name=key-name,Values=string.
  [filters_tag_name]='key-name'
  [delete_cmd]='delete-key-pair'
  [delete_arg]='--key-name'
)

declare -A snapshots=(
  [scenario]='Scenario: Delete snapshots'
  [describe_query]='Snapshots[*].SnapshotId'
  [delete_cmd]='delete-snapshot'
  [delete_arg]='--snapshot-id'
)

declare -A volumes=(
  [scenario_detach]='Scenario: Detach the Volumes'
  [detach_cmd]='detach-volume'
  [detach_arg]='--volume-id'
  [filters_arg_addition_to_detach]='Name=status,Values=in-use'
  # After detach, we can delete the volume.
  [scenario]='Scenario: Delete the volume'
  [describe_query]='Volumes[*].VolumeId'
  [delete_cmd]='delete-volume'
  [delete_arg]='--volume-id'
)

function usage() {
  cat <<EOF
Usage: $0 -n name_pattern [OPTION]

Example: $0 -n BDD_*_random -r "${AWS_REGION:-ap-northeast-1}"

OPTION:

  --dry-run | --no-dry-run (boolean)
    dry-run option set to AWS delete commands. [default: --dry-run]

  -h, --help:               Show this usage.
  -n, --name:               Name pattern of AWS resources to clean up. Required.
  -p, --profile:            AWS profile to pass AWS CLI.
  -r, --region:             AWS region code.
  -t, --target:             Target resources to clean up.
                            [default: ${ALL[@]}]
EOF
  exit 1
}

function main() {
  while [[ "$1" != "" ]]; do
    case "$1" in
    --dry-run | --no-dry-run)
      dry_run_delete="$1"
      shift
      ;;
    -n | --name)
      shift
      val_reg_exp="$1"
      shift
      ;;
    -p | --profile)
      shift
      awscli_args+=("--profile $1")
      shift
      ;;
    -r | --region)
      shift
      awscli_args+=("--region $1")
      shift
      ;;
    -t | --target)
      shift
      while [[ "$1" != "" && ! $1 =~ '-' ]]; do
        targets+=("$1")
        shift
      done
      ;;
    *) usage ;;
    esac
  done
  if [[ -z "${val_reg_exp}" ]]; then
    echo "Missing name pattern."
    usage
  fi
}

count=0
function echo_count() {
  echo
  echo "($((++count))/${TOTAL}) $1"
}

function run_cmd() {
  local cmd="$@"
  info "Executing ${cmd}"
  if ! result=$(${cmd}); then
    err 'Failed.'
    return 1
  fi
  echo "${result}"
}

function info() {
  echo "  $*" >&2
}

function err() {
  echo "  [Error] $*" >&2
}

function setup() {
  if ! (command -v aws &>/dev/null); then
    err 'Command not found: aws'
    exit 1
  fi
  if [[ ! -v targets[@] ]]; then
    targets=("${ALL[@]}")
  fi
  # Add one for internet gateway detach.
  readonly TOTAL=$((3 + ${#targets[@]}))
}

function print_env() {
  echo "aws version:    $(aws --version)"
  echo "Dry-run option to delete: ${dry_run_delete:=--dry-run}"
  cat <<EOF
AWSCLI args:               ${awscli_args[@]}
Name pattern of resources: ${val_reg_exp}
Resource types:            ${targets[@]}
EOF
}


function describe() {
  local resource_name="$1"
  declare -n ref="${resource_name}"
  local describe_cmd="${ref[describe_cmd]:=describe-${resource_name}}"
  local filters_arg="Name=${ref[filters_tag_name]:-tag:Name},Values=${val_reg_exp}"
  local filters_arg_addition="${ref[filters_arg_addition]}"
  local query_arg="${ref[describe_query]}"

  local cmd="aws ec2 ${awscli_args[@]} ${describe_cmd} --filters ${filters_arg} ${filters_arg_addition} --query ${query_arg} --no-paginate --output text"
  local results
  results=$(run_cmd "${cmd}")

  if [[ "${results}" == 'None' ]] || [[ "${results}" == 'null' ]]; then
    err "Something went wrong. The returned value was ${results}."
    exit 1
  fi
  echo "${results}"
}

function delete() {
  local resource_name="$1"
  shift
  local resource_ids="$@"
  declare -n ref="${resource_name}"
  local del_cmd="${ref[delete_cmd]:=delete-${resource_name}}"
  local del_arg="${dry_run_delete} ${ref[delete_arg]}"

  case "${resource_name}" in
  instances)
    # shellcheck disable=SC2145
    run_cmd "aws ec2 ${awscli_args[@]} ${del_cmd} ${del_arg} ${resource_ids}"
    ;;
  *)
    for resource_id in ${resource_ids}; do
      # shellcheck disable=SC2145
      run_cmd "aws ec2 ${awscli_args[@]} ${del_cmd} ${del_arg} ${resource_id}"
    done
    ;;
  esac
}

function describe_and_detach_vpc() {
  local resource_name="$1"
  declare -n ref="${resource_name}"
  # 1. describe attached vpc.
  ref[describe_query]="${ref[describe_query_attached_vpc]}"
  local resource_ids=$(describe "${target}")
  if [[ -z "${resource_ids}" ]]; then
    info 'Nothing to detach.'
    # Reset the query value for after_detach.
    ref[describe_query]="${ref[describe_query_after_detach]}"
    return
  fi
  # 2. describe attached vpc and internet gateways.
  ref[describe_query]="${ref[describe_query_to_detach]}"
  resource_ids=$(describe "${target}")
  info '* Found the following resource(s) to detach:'
  info "${resource_ids}"
  # 3. detach internet gateway.
  local detach_cmd="${ref[detach_cmd]}"
  local detach_arg="${dry_run_delete} ${ref[detach_arg]}"
  local detach_igw_arg="${ref[delete_arg]}"
  for resource_id in ${resource_ids}; do
    if [[ "${resource_id}" =~ 'igw' ]]; then
      local igw_id="${resource_id}"
    else
      # shellcheck disable=SC2145
      run_cmd "aws ec2 ${awscli_args[@]} ${detach_cmd} ${detach_arg} ${resource_id} ${detach_igw_arg} $igw_id"
    fi
  done
  # 4. Reset the query value for after_detach.
  ref[describe_query]="${ref[describe_query_after_detach]}"
}

function describe_and_detach() {
  local resource_name="$1"
  declare -n ref="${resource_name}"
  # 1. save filters_arg_addition for the filter after detach
  ref[fiters_arg_addition_after_detach]="${ref[filters_arg_addition]}"
  ref[describe_query_after_detach]="${ref[describe_query]}"
  # 2. describe resources to detach.
  ref[filters_arg]="${ref[filters_arg_to_detach]}"
  ref[filters_arg_addition]="${ref[filters_arg_addition_to_detach]:-${ref[filters_arg_addition]}}"
  ref[describe_query]="${ref[describe_query_to_detach]:-${ref[describe_query]}}"
  local resource_ids=$(describe "${target}")
  if [[ -z "${resource_ids}" ]]; then
    info '* Nothing to detach.'
    # Reset the filters_arg_additions for after_detach.
    ref[filters_arg_addition]="${ref[fiters_arg_addition_after_detach]}"
    ref[describe_query]="${ref[describe_query_after_detach]}"
    return
  fi
  info '* Found the following resource(s) to detach:'
  info "${resource_ids}"
  # 3. detach volumes.
  local detach_cmd="${ref[detach_cmd]}"
  local detach_arg="${dry_run_delete} ${ref[detach_arg]}"
  for resource_id in ${resource_ids}; do
      # shellcheck disable=SC2145
      run_cmd "aws ec2 ${awscli_args[@]} ${detach_cmd} ${detach_arg} ${resource_id}"
  done
  # 4. Reset the query value for after_detach.
  ref[filters_arg_addition]="${ref[fiters_arg_addition_after_detach]}"
  ref[describe_query]="${ref[describe_query_after_detach]}"
}

## main
main "$@"
setup
echo '** Cleanup AWS resources'
print_env

for target in "${targets[@]}"; do
  declare -n ref="${target}"
  if [[ "${target}" == 'internet_gateways' ]]; then
    echo_count "${ref[scenario_detach]}"
    describe_and_detach_vpc "${target}"
  elif [[ "${target}" == 'volumes' || "${target}" == 'elastic_ips' ]]; then
    echo_count "${ref[scenario_detach]}"
    describe_and_detach "${target}"
  fi

  echo_count "${ref[scenario]}"
  results_to_delete=$(describe "${target}")
  if [[ -z "${results_to_delete}" ]]; then
    info '* Nothing to delete.'
    continue
  fi
  info '* Found the following resource(s) to delete:'
  info "${results_to_delete}"
  if ! delete "${target}" "${results_to_delete}"; then
    continue
  fi

  results_after_deletion=$(describe "${target}")
  if [[ -n "${results_after_deletion}" ]]; then
    err "Failed to delete or taking time to delete ${results_to_delete}."
    continue
  fi
  info '* Deleted the resources.'
done
