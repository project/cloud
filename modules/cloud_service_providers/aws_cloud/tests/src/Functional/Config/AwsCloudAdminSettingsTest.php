<?php

namespace Drupal\Tests\aws_cloud\Functional\Config;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\Tests\aws_cloud\Traits\AwsCloudTestFormDataTrait;

/**
 * Test Case class for AWS Cloud admin setting forms.
 *
 * @group AWS Cloud
 */
class AwsCloudAdminSettingsTest extends AwsCloudTestBase {

  use AwsCloudTestFormDataTrait;

  public const AWS_CLOUD_ADMIN_SETTINGS_REPEAT_COUNT = 2;

  /**
   * Get permissions of login user.
   *
   * @return array
   *   permissions of login user.
   */
  protected function getPermissions(): array {
    return [
      'administer aws_cloud',
      'administer site configuration',
    ];
  }

  /**
   * Test for AwsCloudAdminSettings.
   *
   * @param string $setting_type
   *   The setting type id.
   * @param array $edit
   *   The array of input data.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function runAwsCloudAdminSettings($setting_type, array $edit): void {
    $this->drupalGet("/admin/config/services/cloud/aws_cloud/{$setting_type}");
    $this->assertNoErrorMessage();

    $this->submitForm(
      $edit,
      $this->t('Save')->render()
    );
    $this->assertNoErrorMessage();
  }

  /**
   * Test AWS Cloud admin setting forms on locations.
   */
  public function testAwsCloudAdminLocationsSettings(): void {
    $edit = $this->createAwsCloudLocationsFormData(self::AWS_CLOUD_ADMIN_SETTINGS_REPEAT_COUNT);

    for ($i = 0; $i < self::AWS_CLOUD_ADMIN_SETTINGS_REPEAT_COUNT; $i++) {
      $edit[$i] = array_intersect_key($edit[$i], array_flip(
        (array) array_rand($edit[$i], random_int(1, count($edit[$i])))
      ));
      $this->runAwsCloudAdminSettings('location', $edit[$i]);
    }
  }

  /**
   * Test AWS Cloud admin setting forms on notifications.
   */
  public function testAwsCloudAdminNotificationsSettings(): void {
    $edit = $this->createAwsCloudNotificationsFormData(self::AWS_CLOUD_ADMIN_SETTINGS_REPEAT_COUNT);

    for ($i = 0; $i < self::AWS_CLOUD_ADMIN_SETTINGS_REPEAT_COUNT; $i++) {
      $edit[$i] = array_intersect_key($edit[$i], array_flip(
        (array) array_rand($edit[$i], random_int(1, count($edit[$i])))
      ));
      $this->runAwsCloudAdminSettings('notification', $edit[$i]);
    }
  }

  /**
   * Test AWS Cloud admin setting forms on settings.
   */
  public function testAwsCloudAdminSettings(): void {
    $edit = $this->createAwsCloudSettingsFormData(self::AWS_CLOUD_ADMIN_SETTINGS_REPEAT_COUNT);

    for ($i = 0; $i < self::AWS_CLOUD_ADMIN_SETTINGS_REPEAT_COUNT; $i++) {
      $edit[$i] = array_intersect_key($edit[$i], array_flip(
        (array) array_rand($edit[$i], random_int(1, count($edit[$i])))
      ));
      $this->runAwsCloudAdminSettings('settings', $edit[$i]);
    }
  }

}
