<?php

namespace Drupal\Tests\aws_cloud\Functional\Ec2;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\aws_cloud\Entity\Ec2\KeyPair;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;

/**
 * Tests AWS Cloud key pair.
 *
 * @group AWS Cloud
 */
class KeyPairTest extends AwsCloudTestBase {

  public const AWS_CLOUD_KEY_PAIR_REPEAT_COUNT = 2;
  public const AWS_CLOUD_KEY_PAIR_ADD_BUTTON = 'Add AWS Cloud key pair';
  public const AWS_CLOUD_KEY_PAIR_IMPORT_BUTTON = 'Import AWS Cloud key pair';

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list aws cloud key pair',
      'add aws cloud key pair',
      'view any aws cloud key pair',
      'edit any aws cloud key pair',
      'delete any aws cloud key pair',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    $key_fingerprint_parts = [];
    for ($i = 0; $i < 20; $i++) {
      $key_fingerprint_parts[] = sprintf('%02x', random_int(0, 255));
    }

    $key_material = '---- BEGIN RSA PRIVATE KEY ----'
      . $this->random->name(871, TRUE)
      . '-----END RSA PRIVATE KEY-----';
    return [
      'key_name' => $this->random->name(15, TRUE),
      'key_pair_id' => 'key-' . $this->getRandomId(),
      'key_fingerprint' => implode(':', $key_fingerprint_parts),
      'key_material' => $key_material,
      'error_code' => Ec2ServiceInterface::DRY_RUN_OPERATION,
    ];
  }

  /**
   * Tests CRUD for key pair information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testKeyPair(): void {
    $cloud_context = $this->cloudContext;
    $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
      'aws_cloud',
      $cloud_context
    );

    // List key pair for Amazon EC2.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains(self::AWS_CLOUD_KEY_PAIR_ADD_BUTTON);
    $this->assertSession()->pageTextContains(self::AWS_CLOUD_KEY_PAIR_IMPORT_BUTTON);

    // Add a new key pair.
    $add = $this->createKeyPairTestFormData(self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT);
    $this->reloadMockData();
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->addKeyPairMockData($add[$i]['key_pair_name'], $tag_created_uid, $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Key pair', '%label' => $add[$i]['key_pair_name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $i + 1; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['key_pair_name']);
      }
    }

    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all key_pair listing exists.
      $this->drupalGet('/clouds/aws_cloud/key_pair');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['key_pair_name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair/$num");
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete key pair.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Key pair', '@label' => $add[$i]['key_pair_name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
      }
    }
  }

  /**
   * Tests deleting key pairs with bulk operation.
   *
   * @throws \Exception
   */
  public function testKeyPairBulk(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      // Create key pairs.
      $key_pairs = $this->createKeyPairsRandomTestFormData();
      $index = 1;
      $entities = [];
      foreach ($key_pairs ?: [] as $key_pair) {
        $entities[] = $this->createKeyPairTestEntity(KeyPair::class, $index++, $key_pair['Name'], $key_pair['KeyFingerprint'], $cloud_context);
      }

      $this->runTestEntityBulk('key_pair', $entities);
    }
  }

  /**
   * Test updating key pair list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateKeyPairList(): void {

    $cloud_context = $this->cloudContext;
    $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
      'aws_cloud',
      $cloud_context
    );

    // Add a new key pair.
    $add = $this->createKeyPairTestFormData(self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT);
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->addKeyPairMockData($add[$i]['key_pair_name'], $tag_created_uid, $cloud_context, $this->webUser->id());
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated key pairs.'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/key_pair/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/key_pair/$num/delete");
      $this->assertSession()->linkExists($this->t('List AWS Cloud key pairs'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List AWS Cloud key pairs'));
      $this->assertNoErrorMessage();

      // Confirm the edit view.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair/$num/edit");
      $this->assertSession()->linkNotExists($this->t('Edit'));
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/key_pair/$num/delete");
    }

    // Edit key pair information.
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {

      // Change key pair name in mock data.
      $add[$i]['key_pair_name'] = $this->random->name(15, TRUE);
      $this->updateKeyPairMockData($i, $add[$i]['key_pair_name']);

    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated key pairs.'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Delete key pair in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->deleteFirstKeyPairMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated key pairs.'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }

  }

  /**
   * Test updating all key pair list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllKeyPairList(): void {
    $cloud_configs = [];

    // List KeyPair for Amazon EC2.
    $this->drupalGet('/clouds/aws_cloud/key_pair');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
        'aws_cloud',
        $cloud_context
      );
      // Add a new key pair.
      $add = $this->createKeyPairTestFormData(self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT);
      for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
        $this->addKeyPairMockData($add[$i]['key_pair_name'], $tag_created_uid, $cloud_context, $this->webUser->id());
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/key_pair');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Key pairs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/key_pair/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/{$cloud_context}/key_pair/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/{$cloud_context}/key_pair/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List AWS Cloud key pairs'));
        // Click 'List AWS Cloud key pairs'.
        $this->clickLink($this->t('List AWS Cloud key pairs'));
        $this->assertNoErrorMessage();

        // Confirm the edit view.
        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/key_pair/{$num}/edit");
        $this->assertSession()->linkNotExists($this->t('Edit'));
      }
    }

    // Edit key pair information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {

        // Change key pair name in mock data.
        $add[$i]['key_pair_name'] = $this->random->name(15, TRUE);
        $this->updateKeyPairMockData($i, $add[$i]['key_pair_name']);

      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/key_pair');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'key pairs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Delete key pair in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->deleteFirstKeyPairMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/key_pair');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Key pairs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }
  }

  /**
   * Tests hiding buttons based on access control.
   */
  public function testKeyPairAccessControl(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::AWS_CLOUD_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->deleteIamPermission($cloud_context, 'CreateKeyPair');
      // List key pairs for Amazon EC2.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains(self::AWS_CLOUD_KEY_PAIR_ADD_BUTTON);
      $this->assertSession()->pageTextNotContains(self::AWS_CLOUD_KEY_PAIR_IMPORT_BUTTON);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair/add");
      $this->assertAccessDenied();

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/key_pair/import");
      $this->assertAccessDenied();
    }
  }

}
