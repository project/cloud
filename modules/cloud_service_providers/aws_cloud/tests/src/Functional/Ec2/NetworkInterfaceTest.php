<?php

namespace Drupal\Tests\aws_cloud\Functional\Ec2;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\aws_cloud\Entity\Ec2\NetworkInterface;
use Drupal\aws_cloud\Entity\Ec2\SecurityGroup;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;

/**
 * Tests AWS Cloud network interface.
 *
 * @group AWS Cloud
 */
class NetworkInterfaceTest extends AwsCloudTestBase {

  public const AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT = 2;
  public const AWS_CLOUD_NETWORK_INTERFACE_ADD_BUTTON = 'Add AWS Cloud network interface';

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list aws cloud network interface',
      'add aws cloud network interface',
      'view any aws cloud network interface',
      'edit any aws cloud network interface',
      'delete any aws cloud network interface',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'network_interface_id' => 'eni-' . $this->getRandomId(),
      'vpc_id' => 'vpc-' . $this->getRandomId(),
      'error_code' => Ec2ServiceInterface::DRY_RUN_OPERATION,
    ];
  }

  /**
   * Tests CRUD for network interface information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testNetworkInterface(): void {
    $cloud_context = $this->cloudContext;
    $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
      'aws_cloud',
      $cloud_context
    );

    // List network interface for Amazon EC2.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains(self::AWS_CLOUD_NETWORK_INTERFACE_ADD_BUTTON);

    // Create security groups.
    $security_groups = $this->createSecurityGroupRandomTestFormData();
    $index = 0;
    foreach ($security_groups ?: [] as $security_group) {
      $this->createSecurityGroupTestEntity(SecurityGroup::class, $index++, $security_group['GroupId'], $security_group['Name'], '', $cloud_context);
    }

    // Create subnets.
    $subnets = $this->createRandomSubnets();
    $index = 0;
    foreach ($subnets ?: [] as $subnet) {
      $this->createSubnetTestEntity($index++, $subnet['SubnetId'], $subnet['Tags'][0]['Value'], $cloud_context);
    }

    // Add a new network interface.
    $add = $this->createNetworkInterfaceTestFormData(self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT);
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {

      $network_interface_id = $this->addNetworkInterfaceMockData($add[$i], $tag_created_uid, $cloud_context, $this->webUser->id());
      $this->updateCreateNetworkInterfaceMockData($network_interface_id);

      unset($add[$i]['primary_private_ip']);

      $subnet_index = array_rand($subnets);
      $add[$i]['subnet_id'] = $subnets[$subnet_index]['SubnetId'];
      $security_group_index = array_rand($security_groups);
      $add[$i]['security_groups[]'] = $security_groups[$security_group_index]['GroupId'];

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains(
        $this->t('Network interface @name', [
          '@name' => $add[$i]['name'],
        ])
      );
      $this->assertSession()->pageTextContains(
        $add[$i]['name']
      );

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $i + 1; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all network_interface listing exists.
      $this->drupalGet('/clouds/aws_cloud/network_interface');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface/$num");
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Network interface information.
    $edit = $this->createNetworkInterfaceTestFormData(self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++, $num++) {

      unset(
        $edit[$i]['subnet_id'],
        $edit[$i]['security_groups[]'],
        $edit[$i]['primary_private_ip']
      );

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Network interface', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure the description.
      $this->assertSession()->fieldValueEquals('description', $edit[$i]['description']);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }
    }

    // Delete network interface.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['name']);

      $t_args = ['@type' => 'Network interface', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests deleting network interfaces with bulk operation.
   *
   * @throws \Exception
   */
  public function testNetworkInterfaceBulk(): void {

    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      // Create network interface.
      $network_interfaces = $this->createNetworkInterfacesRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($network_interfaces ?: [] as $network_interface) {
        $entities[] = $this->createNetworkInterfaceTestEntity(NetworkInterface::class, $index++, $network_interface['NetworkInterfaceId'], $network_interface['Name']);
      }

      $this->runTestEntityBulk('network_interface', $entities);
    }
  }

  /**
   * Test updating network interface.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateNetworkInterfaceList(): void {
    $cloud_context = $this->cloudContext;
    $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
      'aws_cloud',
      $cloud_context
    );

    // Add a new Network interface.
    $add = $this->createNetworkInterfaceTestFormData(self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT);
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addNetworkInterfaceMockData($add[$i], $tag_created_uid, $cloud_context, $this->webUser->id());
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated network interfaces.'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/network_interface/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/network_interface/$num/delete");
      $this->assertSession()->linkExists($this->t('List AWS Cloud network interfaces'));
      // Click 'Refresh'.
      $this->clickLink($this->t('List AWS Cloud network interfaces'));
      $this->assertNoErrorMessage();

      // Confirm the edit view.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface/$num/edit");
      $this->assertSession()->linkNotExists($this->t('Edit'));
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/network_interface/$num/delete");
      $this->assertSession()->linkNotExists('Edit');
    }

    // Edit network interface information.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++, $num++) {

      // Change network interface name in mock data.
      $add[$i]['name'] = 'eni-' . $this->getRandomId();
      $this->updateNetworkInterfaceMockData($num - 1, $add[$i]['name']);

    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated network interfaces.'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Update network interface tags.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {

      // Update tags.
      $add[$i]['tags_name'] = $this->getRandomId();
      $this->updateTagsInMockData($i, 'NetworkInterfaces', 'Name', $add[$i]['tags_name'], FALSE, 'TagSet');
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['tags_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($add[$i]['tags_name']);
    }

    // Update network interface tags for empty.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'NetworkInterfaces', 'Name', '', FALSE, 'TagSet');
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($add[$i]['tags_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['tags_name']);
      $this->assertSession()->linkExists($add[$i]['name']);
    }

    // Delete network interface tags.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'NetworkInterfaces', 'Name', '', TRUE, 'TagSet');
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['tags_name']);
      $this->assertSession()->linkExists($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['tags_name']);
      $this->assertSession()->linkExists($add[$i]['name']);
    }

    // Delete network interface in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->deleteFirstNetworkInterfaceMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated network interfaces.'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Test updating all network interface.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllNetworkInterfaceList(): void {
    $cloud_configs = [];

    // List network interface for Amazon EC2.
    $this->drupalGet('/clouds/aws_cloud/network_interface');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      // Add a new network interface.
      $add = $this->createNetworkInterfaceTestFormData(self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT);
      $cloud_context = $cloud_config->getCloudContext();
      $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
        'aws_cloud',
        $cloud_context
      );
      for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
        $this->reloadMockData();
        $this->addNetworkInterfaceMockData($add[$i], $tag_created_uid, $cloud_context, $this->webUser->id());
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/network_interface');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Network interfaces',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/network_interface/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/{$cloud_context}/network_interface/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/{$cloud_context}/network_interface/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List AWS Cloud network interfaces'));
        // Click 'List AWS Cloud network interfaces'.
        $this->clickLink($this->t('List AWS Cloud network interfaces'));
        $this->assertNoErrorMessage();

        // Confirm the edit view.
        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/network_interface/{$num}/edit");
        $this->assertSession()->linkNotExists($this->t('Edit'));
      }
    }

    // Edit network interface information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0, $num = 1; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++, $num++) {

        // Change network interface name in mock data.
        $add[$i]['name'] = 'eni-' . $this->getRandomId();
        $this->updateNetworkInterfaceMockData($num - 1, $add[$i]['name']);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/network_interface');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Network interfaces',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete Network interface in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->deleteFirstNetworkInterfaceMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/network_interface');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Network interfaces',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Tests hiding buttons based on access control.
   */
  public function testNetworkInterfaceAccessControl(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::AWS_CLOUD_NETWORK_INTERFACE_REPEAT_COUNT; $i++) {
      $this->deleteIamPermission($cloud_context, 'CreateNetworkInterface');
      // List network interface for Amazon EC2.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains(self::AWS_CLOUD_NETWORK_INTERFACE_ADD_BUTTON);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/network_interface/add");
      $this->assertAccessDenied();
    }
  }

}
