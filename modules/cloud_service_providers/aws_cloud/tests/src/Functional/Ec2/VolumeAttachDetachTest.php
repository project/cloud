<?php

namespace Drupal\Tests\aws_cloud\Functional\Ec2;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\aws_cloud\Entity\Ec2\Instance;
use Drupal\aws_cloud\Entity\Ec2\Volume;

/**
 * Tests AWS Cloud volume for attach and detach operations.
 *
 * @group AWS Cloud
 */
class VolumeAttachDetachTest extends AwsCloudTestBase {

  /**
   * Number of times to repeat the test.
   */
  public const MAX_TEST_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'list aws cloud volume',
      'add aws cloud volume',
      'view any aws cloud volume',
      'edit any aws cloud volume',
      'delete any aws cloud volume',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'create_time' => date('c'),
    ];
  }

  /**
   * Test volume attach.
   */
  public function testVolumeAttachDetach(): void {
    try {
      $this->repeatTestVolumeAttachDetach(self::MAX_TEST_REPEAT_COUNT);
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Repeating test volume attach detach.
   *
   * @param int $max_test_repeat_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  private function repeatTestVolumeAttachDetach($max_test_repeat_count = 1): void {

    $regions = ['us-west-1', 'us-west-2'];
    $add = $this->createVolumeTestFormData($max_test_repeat_count + 1);
    $uid_key_name = $this->cloudService->getTagKeyCreatedByUid(
      'aws_cloud',
      $this->cloudContext);
    for ($i = 1; $i <= $max_test_repeat_count; $i++) {
      // Setup for testing.
      $device_name = $this->random->name(8, TRUE);

      // Set up a test instance.
      $instance = $this->createInstanceTestEntity(Instance::class, $i, $regions);
      $instance_id = $instance->getInstanceId();

      // Set up a test volume.
      $volume = $this->createVolumeTestEntity(
        Volume::class,
        $i,
        'vol-' . $this->getRandomId(),
        "volume-name #$i - {$this->random->name(32, TRUE)}",
        $this->cloudContext,
        $this->loggedInUser->id()
      );
      $volume_id = $volume->getVolumeId();
      $add[$i]['VolumeId'] = $volume_id;
      $add[$i]['name'] = $volume->getName();
      $this->addVolumeMockData($add[$i], $uid_key_name, $this->cloudContext, $this->webUser->id());

      $attach_data = [
        'device_name' => $device_name,
        'instance_id' => $instance_id,
      ];

      // Test attach.
      $this->updateAttachDetachVolumeMockData('AttachVolume', $device_name, $volume_id, $instance_id, 'attaching');
      $this->drupalGet("/clouds/aws_cloud/$this->cloudContext/volume/$i/attach");
      $this->submitForm(
        $attach_data,
        $this->t('Attach')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('The volume @volume is attaching to @instance on @device_name', [
        '@instance' => $instance_id,
        '@volume' => $volume_id,
        '@device_name' => $device_name,
      ]));

      // Test detach.
      $volume->setState('in-use');
      $volume->setAttachmentInformation($instance_id);
      $volume->setAttachmentDeviceName($device_name);
      $volume->save();
      $this->updateAttachDetachVolumeMockData('DetachVolume', $device_name, $volume_id, $instance_id, 'detaching');
      $this->drupalGet("/clouds/aws_cloud/$this->cloudContext/volume/$i/detach");
      $this->submitForm(
        [],
        $this->t('Detach')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('The volume @volume is detaching from @instance on @device_name', [
        '@instance' => $instance_id,
        '@volume' => $volume_id,
        '@device_name' => $device_name,
      ]));
    }

  }

  /**
   * Tests detaching Volumes with bulk operation.
   *
   * @throws \Exception
   */
  public function testVolumeBulkDetach(): void {

    $cloud_context = $this->cloudContext;
    $total_count = 0;
    $total_volumes = [];
    $regions = ['us-west-1', 'us-west-2'];
    $uid_key_name = $this->cloudService->getTagKeyCreatedByUid(
      'aws_cloud',
      $cloud_context
    );

    // NOTE: $num needs to be incremented outside $j loop.
    for ($i = 0, $num = 1; $i < self::MAX_TEST_REPEAT_COUNT; $i++) {

      $volumes_count = random_int(1, self::MAX_TEST_REPEAT_COUNT);
      // Create Volumes.
      $volumes = $this->createVolumeTestFormData($volumes_count);

      for ($j = 0; $j < $volumes_count; $j++, $num++) {
        // Setup for testing.
        $device_name = $this->random->name(8, TRUE);

        // Set up a test instance.
        $instance = $this->createInstanceTestEntity(Instance::class, $j, $regions);
        $instance_id = $instance->getInstanceId();
        $this->addInstanceMockData(InstanceTest::class, $instance->getName(), $instance->getKeyPairName(), $regions, 'running', '', $cloud_context);

        // Set up a test volume.
        $volume = $this->createVolumeTestEntity(
          Volume::class,
          $j,
          $volumes[$j]['name'],
          $volumes[$j]['name'],
          $this->cloudContext,
          $this->loggedInUser->id()
        );
        $volume_id = $volume->getVolumeId();
        $this->addVolumeMockData($volumes[$j], $uid_key_name, $cloud_context, $this->webUser->id());

        $attach_data = [
          'device_name' => $device_name,
          'instance_id' => $instance_id,
        ];
        $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");

        // Test attach.
        $this->updateAttachDetachVolumeMockData('AttachVolume', $device_name, $volume_id, $instance_id, 'attaching');
        $this->drupalGet("/clouds/aws_cloud/$this->cloudContext/volume/$num/attach");
        $this->submitForm(
          $attach_data,
          $this->t('Attach')->render()
        );
        $this->assertNoErrorMessage();

        $volume->setState('in-use');
        $volume->setAttachmentInformation($instance_id);
        $volume->setAttachmentDeviceName($device_name);
        $volume->save();
        $volumes[$j]['instance_id'] = $instance_id;

      }

      $total_count += $volumes_count;
      foreach ($volumes ?: [] as $volume) {
        $total_volumes[] = $volume;
      }

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");

      $data = [];
      $data['action'] = 'aws_cloud_volume_detach_action';

      $checkboxes = $this->cssSelect('input[type=checkbox]');
      foreach ($checkboxes ?: [] as $checkbox) {
        if ($checkbox->getAttribute('name') === NULL) {
          continue;
        }

        $data[$checkbox->getAttribute('name')] = $checkbox->getAttribute('value');
      }

      // Confirm.
      $this->submitForm(
        $data,
        $this->t('Apply to selected items')->render()
      );
      $this->assertNoErrorMessage();

      $message = 'Are you sure you want to detach these Volumes?';

      if ($total_count === 1) {
        $message = 'Are you sure you want to detach this Volume?';
      }
      $this->assertSession()->pageTextContains($message);

      foreach ($total_volumes ?: [] as $volume) {
        $this->assertSession()->pageTextContains($volume['name']);
      }

      // Disassociate.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/detach_multiple");
      $this->submitForm(
        [],
        $this->t('Detach')->render()
      );
      $this->assertNoErrorMessage();

      if ($total_count === 1) {
        $this->assertSession()->pageTextContains("Detached $volumes_count volume.");
      }
      else {
        $this->assertSession()->pageTextContains("Detached $total_count volumes.");
      }

      for ($j = 0; $j < $total_count; $j++) {
        $volume = $total_volumes[$j];
        $name = $volume['name'];
        $t_args = ['@type' => 'Volume', '%label' => $name];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been detached.', $t_args)));
        $this->updateVolumeMockData($j, $volume['name'], '');
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      $this->assertSession()->pageTextContains($this->t('Updated Volumes.'));

      // Make sure if disassociated from an instance.
      foreach ($total_volumes ?: [] as $volume) {
        $this->assertSession()->pageTextNotContains($volume['instance_id']);
      }
    }
  }

}
