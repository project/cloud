<?php

namespace Drupal\Tests\aws_cloud\Functional\Ec2;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\Tests\cloud\Functional\Utils;
use Drupal\aws_cloud\Entity\Ec2\AvailabilityZone;
use Drupal\aws_cloud\Entity\Ec2\Instance;
use Drupal\aws_cloud\Entity\Ec2\Snapshot;
use Drupal\aws_cloud\Entity\Ec2\Volume;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;

/**
 * Tests AWS Cloud volume.
 *
 * @group AWS Cloud
 */
class VolumeTest extends AwsCloudTestBase {

  public const AWS_CLOUD_VOLUME_REPEAT_COUNT = 2;
  public const AWS_CLOUD_VOLUME_ADD_BUTTON = 'Add AWS Cloud volume';

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list aws cloud volume',
      'add aws cloud volume',
      'view any aws cloud volume',
      'edit any aws cloud volume',
      'delete any aws cloud volume',

      'add aws cloud snapshot',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'volume_id' => 'vol-' . $this->getRandomId(),
      'create_time' => date('c'),
      'uid' => !empty($this->webUser) ? $this->webUser->id() : 0,
      'cloud_context' => $this->cloudContext,
      'error_code' => Ec2ServiceInterface::DRY_RUN_OPERATION,
    ];
  }

  /**
   * Tests CRUD for the volume information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testVolume(): void {
    $cloud_context = $this->cloudContext;

    // List the volumes for Amazon EC2.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains(self::AWS_CLOUD_VOLUME_ADD_BUTTON);

    // Add a new volume.
    $delete_count = 0;
    $add = $this->createVolumeTestFormData(self::AWS_CLOUD_VOLUME_REPEAT_COUNT);

    // Create Availability Zones.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->createAvailabilityZoneTestEntity(AvailabilityZone::class, $i, $cloud_context, '', $add[$i]['availability_zone']);
    }

    $this->reloadMockData();
    $this->deleteFirstVolumeMockData();
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++, $num++) {
      $state = $this->createRandomState();
      $volume_id = 'vol-' . $this->getRandomId();
      $snapshot_name = 'snapshot-name' . $this->random->name(10, TRUE);
      $this->updateCreateVolumeMockData($state, $volume_id);
      $this->createSnapshotTestEntity(Snapshot::class, $i, $add[$i]['snapshot_id'], $snapshot_name, $cloud_context);
      $this->updateDescribeSnapshotsMockData([
        [
          'id' => $add[$i]['snapshot_id'],
          'name' => $snapshot_name,
        ],
      ]);
      if ($state !== 'in-use') {
        $delete_count++;
      }

      // Make sure checkbox Encrypted is checked.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/add");
      $this->assertSession()->checkboxChecked('encrypted');

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
      $this->assertSession()->pageTextContains(
        $this->t('The volume @name', ['@name' => $add[$i]['name']])
      );

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$i]['name']);
      }

      // Assert delete link count.
      if ($delete_count > 0) {
        $this->assertSession()->linkExists($this->t('Delete'), $delete_count - 1);
      }

      // Make sure view.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/$num");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($volume_id);
      $this->assertSession()->pageTextContains($add[$i]['snapshot_id']);
      $this->assertSession()->pageTextContains($snapshot_name);
      // Make sure uid.
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
      $add[$i]['VolumeId'] = $volume_id;
      $uid_key_name = $this->cloudService->getTagKeyCreatedByUid(
        'aws_cloud',
        $cloud_context
      );
      $this->addVolumeMockData($add[$i], $uid_key_name, $cloud_context, $this->webUser->id());
    }

    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all volume listing exists.
      $this->drupalGet('/clouds/aws_cloud/volume');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/$num");
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a volume information.
    $edit = $this->createVolumeTestFormData(self::AWS_CLOUD_VOLUME_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++, $num++) {

      unset(
        $edit[$i]['snapshot_id'],
        $edit[$i]['size'],
        $edit[$i]['availability_zone'],
        $edit[$i]['iops'],
        $edit[$i]['encrypted'],
        $edit[$i]['volume_type']
      );

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Volume', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Delete Volume.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['name']);

      $t_args = ['@type' => 'Volume', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Test updating volume list.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateVolumeList(): void {

    $cloud_context = $this->cloudContext;
    $uid_key_name = $this->cloudService->getTagKeyCreatedByUid(
      'aws_cloud',
      $cloud_context
    );

    // Add a new Volume.
    $add = $this->createVolumeTestFormData(self::AWS_CLOUD_VOLUME_REPEAT_COUNT);
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->addVolumeMockData($add[$i], $uid_key_name, $cloud_context, $this->webUser->id());
      $snapshot_name = 'snapshot-name' . $this->random->name(10, TRUE);
      $this->createSnapshotTestEntity(Snapshot::class, $i, $add[$i]['snapshot_id'], $snapshot_name, $cloud_context);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Volumes.'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure uid.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/volume/$num/edit");
      $this->assertSession()->linkExists($this->t('Attach'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/volume/$num/attach");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/volume/$num/delete");
      $this->assertSession()->linkExists($this->t('List AWS Cloud volumes'));
      // Click 'Refresh'.
      $this->clickLink($this->t('List AWS Cloud volumes'));
      $this->assertNoErrorMessage();

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/$num/edit");
      $this->assertSession()->linkNotExists('Edit');
      $this->assertSession()->linkExists($this->t('Attach'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/volume/$num/attach");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/volume/$num/delete");
    }

    $regions = ['us-west-1', 'us-west-2'];
    // Edit the volume information.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++, $num++) {

      // Set up a test instance.
      $instance = $this->createInstanceTestEntity(Instance::class, $i, $regions);
      $instance_id = $instance->getInstanceId();

      // Change the volume name in mock data.
      $add[$i]['name'] = "volume-name #$num - {$this->random->name(32, TRUE)}";

      $this->updateVolumeMockData($i, $add[$i]['name'], $instance_id);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Volumes.'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {

      $num = $i + self::AWS_CLOUD_VOLUME_REPEAT_COUNT + 1;

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/volume/$num/edit");
      $this->assertSession()->linkExists($this->t('Detach'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/volume/$num/detach");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/$cloud_context/volume/$num/delete");
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Update tags.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++, $num++) {

      // Update tags.
      $add[$i]['tags_name'] = $this->getRandomId();
      $this->updateTagsInMockData($i, 'Volumes', 'Name', $add[$i]['tags_name'], FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['tags_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($add[$i]['tags_name']);
    }

    // Update tags for empty.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'Volumes', 'Name', '', FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($add[$i]['tags_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['tags_name']);
      $this->assertSession()->linkExists($add[$i]['name']);
    }

    // Delete name tags.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'Volumes', 'Name', '', TRUE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['tags_name']);
      $this->assertSession()->linkExists($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['tags_name']);
      $this->assertSession()->linkExists($add[$i]['name']);
    }

    // Delete a volume in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->deleteFirstVolumeMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Volumes.'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test updating all volume list.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllVolumeList(): void {
    $cloud_configs = [];

    // List the volumes for Amazon EC2.
    $this->drupalGet('/clouds/aws_cloud/volume');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    $this->cloudContext = $this->random->name(8);
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $uid_key_name = $this->cloudService->getTagKeyCreatedByUid('aws_cloud', $cloud_context);
      // Add a new Volume.
      $add = $this->createVolumeTestFormData(self::AWS_CLOUD_VOLUME_REPEAT_COUNT);
      for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
        $this->addVolumeMockData($add[$i], $uid_key_name, $cloud_context, $this->webUser->id());
        $snapshot_name = "snapshot-name{$this->random->name(10, TRUE)}";
        $this->createSnapshotTestEntity(Snapshot::class, $i, $add[$i]['snapshot_id'], $snapshot_name, $cloud_context);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/volume');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Volumes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++, $num++) {

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/volume/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/{$cloud_context}/volume/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Attach'));
        $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/{$cloud_context}/volume/{$num}/attach");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/{$cloud_context}/volume/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List AWS Cloud volumes'));
        // Click 'Refresh'.
        $this->clickLink($this->t('List AWS Cloud volumes'));
        $this->assertNoErrorMessage();

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/volume/{$num}/edit");
        $this->assertSession()->linkNotExists('Edit');
        $this->assertSession()->linkExists($this->t('Attach'));
        $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/{$cloud_context}/volume/{$num}/attach");
      }
    }

    $regions = ['us-west-1', 'us-west-2'];
    // Edit the volume information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++, $num++) {

        // Set up a test instance.
        $instance = $this->createInstanceTestEntity(Instance::class, $i, $regions);
        $instance_id = $instance->getInstanceId();

        // Change the volume name in mock data.
        $add[$i]['name'] = "volume-name #{$num} - {$this->random->name(32, TRUE)}";

        $this->updateVolumeMockData($i, $add[$i]['name'], $instance_id);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/volume');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Volumes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete a volume in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->deleteFirstVolumeMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/volume');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Volumes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test the operation of creating snapshot.
   */
  public function testCreateSnapshotOperation(): void {
    $this->repeatTestCreateSnapshotOperation(
      self::AWS_CLOUD_VOLUME_REPEAT_COUNT
    );
  }

  /**
   * Repeat testing the operation of creating snapshot.
   *
   * @param int $max_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  private function repeatTestCreateSnapshotOperation($max_count): void {
    $cloud_context = $this->cloudContext;
    $uid_key_name = $this->cloudService->getTagKeyCreatedByUid(
      'aws_cloud',
      $cloud_context
    );

    $add = $this->createVolumeTestFormData(self::AWS_CLOUD_VOLUME_REPEAT_COUNT);

    // Create Availability Zones.
    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->createAvailabilityZoneTestEntity(AvailabilityZone::class, $i, $cloud_context, '', $add[$i]['availability_zone']);
    }

    for ($i = 0; $i < $max_count; $i++) {
      $this->reloadMockData();

      $snapshot_name = 'snapshot-name' . $this->random->name(10, TRUE);
      $this->updateDescribeSnapshotsMockData([
        [
          'id' => $add[$i]['snapshot_id'],
          'name' => $snapshot_name,
        ],
      ]);
      $this->createSnapshotTestEntity(Snapshot::class, $i, $add[$i]['snapshot_id'], $snapshot_name, $cloud_context);

      // Add a volume to DescribeVolumes.
      $volume_id = $this->latestTemplateVars['volume_id'];
      $add[$i]['VolumeId'] = $volume_id;
      $this->addVolumeMockData($add[$i], $uid_key_name, $cloud_context, $this->webUser->id());
      unset($add[$i]['VolumeId']);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('Create snapshot'));

      // Click "Create snapshot" link.
      $this->clickLink($this->t('Create snapshot'), $i);

      // Make sure creating page.
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('Add AWS Cloud snapshot'));

      // Make sure the default value of field volume_id.
      $this->assertSession()->fieldValueEquals('volume_id', $volume_id);
    }
  }

  /**
   * Tests deleting volumes with bulk operation.
   *
   * @throws \Exception
   */
  public function testVolumeBulk(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0, $num = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      // Create volumes.
      $volumes = $this->createVolumesRandomTestFormData();
      $entities = [];
      foreach ($volumes ?: [] as $volume) {
        $entities[] = $this->createVolumeTestEntity(
          Volume::class,
          $num++,
          $volume['VolumeId'],
          $volume['Name'],
          $cloud_context,
          Utils::getRandomUid()
        );
      }

      $this->runTestEntityBulk('volume', $entities);
    }
  }

  /**
   * Tests hiding buttons based on access control.
   */
  public function testVolumeAccessControl(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::AWS_CLOUD_VOLUME_REPEAT_COUNT; $i++) {
      $this->deleteIamPermission($cloud_context, 'CreateVolume');
      if (random_int(0, 1) === 1) {
        $this->deleteIamPermission($cloud_context, 'DescribeAvailabilityZones');
      }
      // List the volumes for Amazon EC2.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains(self::AWS_CLOUD_VOLUME_ADD_BUTTON);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/volume/add");
      $this->assertAccessDenied();
    }
  }

}
