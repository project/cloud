<?php

namespace Drupal\Tests\aws_cloud\Functional\Vpc;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;

/**
 * Tests AWS Cloud carrier gateway.
 *
 * @group AWS Cloud
 */
class CarrierGatewayTest extends AwsCloudTestBase {

  public const AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT = 2;
  public const AWS_CLOUD_CARRIER_GATEWAY_ADD_BUTTON = 'Add AWS Cloud carrier gateway';

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list aws cloud carrier gateway',
      'add aws cloud carrier gateway',
      'view any aws cloud carrier gateway',
      'edit any aws cloud carrier gateway',
      'delete any aws cloud carrier gateway',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'carrier_gateway_id' => 'igw-' . $this->getRandomId(),
      'error_code' => Ec2ServiceInterface::DRY_RUN_OPERATION,
    ];
  }

  /**
   * Tests CRUD for carrier gateway information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testCarrierGateway(): void {
    $cloud_context = $this->cloudContext;

    // List carrier gateway for Amazon EC2.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway");
    $this->assertNoErrorMessage();

    $vpc_ids = $this->updateVpcsMockData(self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT);

    // Create VPC.
    $vpc_data = $this->createVpcTestFormData(self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT);
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $vpc = $this->createVpcTestEntity($i, $vpc_ids[$i], $vpc_data[$i]['name'], $cloud_context);
      $vpc->setCidrBlock($vpc_data[$i]['cidr_block']);
      $vpc->save();
    }

    // Add a new carrier gateway.
    $add = $this->createCarrierGatewayTestFormData(self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT, $vpc_ids);
    $this->reloadMockData();
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $carrier_gateway_id = 'igw-' . $this->getRandomId();
      $this->addCarrierGatewayMockData($add[$i], $carrier_gateway_id, $cloud_context, $this->webUser->id());
      $this->updateCreateCarrierGatewayMockData($carrier_gateway_id, $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Carrier gateway', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);

      $add[$i]['carrier_gateway_id'] = $carrier_gateway_id;
    }

    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all carrier gateway listing exists.
      $this->drupalGet('/clouds/aws_cloud/carrier_gateway');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a carrier gateway.
    $edit = $this->createCarrierGatewayTestFormData(self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT, $vpc_ids);
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++, $num++) {
      unset($edit[$i]['vpc_id']);

      $this->modifyCarrierGatewayMockData($i, $edit[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Carrier gateway', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Update tags for empty.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'CarrierGateways', 'Name', '', FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway");
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['carrier_gateway_id']);
    }

    // Delete name tags.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'CarrierGateways', 'Name', '', TRUE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway");
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['carrier_gateway_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['carrier_gateway_id']);
    }

    // Update tags.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {

      $this->updateTagsInMockData($i, 'CarrierGateways', 'Name', $edit[$i]['name'], FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway");
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['carrier_gateway_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Delete carrier gateway.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway/$num/delete");
      $this->assertNoErrorMessage();

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Carrier gateway', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($edit[$i]['name']);
    }
  }

  /**
   * Tests CRUD for carrier gateway information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllCarrierGateway(): void {
    $cloud_configs = [];

    // List carrier gateway for Amazon EC2.
    $this->drupalGet('/clouds/aws_cloud/carrier_gateway');
    $this->assertNoErrorMessage();

    $vpc_ids = $this->updateVpcsMockData(self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT);

    // Create Cloud Config.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();

      // Create VPC.
      $vpc_data = $this->createVpcTestFormData(self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT);
      for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
        $vpc = $this->createVpcTestEntity($i, $vpc_ids[$i], $vpc_data[$i]['name'], $cloud_context);
        $vpc->setCidrBlock($vpc_data[$i]['cidr_block']);
        $vpc->save();
      }

      // Add a new carrier gateway.
      $add = $this->createCarrierGatewayTestFormData(self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT, $vpc_ids);
      for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $carrier_gateway_id = $this->latestTemplateVars['carrier_gateway_id'];
        $this->addCarrierGatewayMockData($add[$i], $carrier_gateway_id, $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/carrier_gateway/add");
        $this->submitForm(
          $add[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = ['@type' => 'Carrier gateway', '%label' => $add[$i]['name']];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
        $add[$i]['carrier_gateway_id'] = $carrier_gateway_id;
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/carrier_gateway');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Carrier gateways',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Edit a carrier gateway.
    $edit = $this->createCarrierGatewayTestFormData(self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT, $vpc_ids);
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++, $num++) {
        unset($edit[$i]['vpc_id']);

        $this->modifyCarrierGatewayMockData($i, $edit[$i], $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/carrier_gateway/{$num}/edit");
        $this->submitForm(
          $edit[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = [
          '@type' => 'Carrier gateway',
          '%label' => $edit[$i]['name'],
        ];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/carrier_gateway');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Carrier gateways',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Delete carrier gateway in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->deleteCarrierGatewayMockData($i);
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/carrier_gateway');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Carrier gateways',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($edit[$i]['name']);
    }
  }

  /**
   * Tests deleting carrier gateways with bulk operation.
   *
   * @throws \Exception
   */
  public function testCarrierGatewayBulk(): void {
    $cloud_context = $this->cloudContext;
    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      // Create carrier gateways.
      $carrier_gateways = $this->createCarrierGatewaysRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($carrier_gateways ?: [] as $carrier_gateway) {
        $entities[] = $this->createCarrierGatewayTestEntity($index++, $carrier_gateway['CarrierGatewayId'], $carrier_gateway['Name'], $cloud_context);
      }
      $this->runTestEntityBulk('carrier_gateway', $entities);
    }
  }

  /**
   * Tests hiding buttons based on access control.
   */
  public function testCarrierGatewayAccessControl(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::AWS_CLOUD_CARRIER_GATEWAY_REPEAT_COUNT; $i++) {
      $this->deleteIamPermission($cloud_context, 'CreateCarrierGateway');
      // List carrier gateway for Amazon EC2.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains(self::AWS_CLOUD_CARRIER_GATEWAY_ADD_BUTTON);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/carrier_gateway/add");
      $this->assertAccessDenied();
    }
  }

}
