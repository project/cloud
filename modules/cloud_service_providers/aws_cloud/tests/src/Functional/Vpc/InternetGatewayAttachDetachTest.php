<?php

namespace Drupal\Tests\aws_cloud\Functional\Vpc;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\Tests\cloud\Functional\Utils;

/**
 * Tests AWS Cloud internet gateway for attach and detach operations.
 *
 * @group AWS Cloud
 */
class InternetGatewayAttachDetachTest extends AwsCloudTestBase {

  /**
   * Number of times to repeat the test.
   */
  public const MAX_TEST_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'list aws cloud internet gateway',
      'add aws cloud internet gateway',
      'view any aws cloud internet gateway',
      'edit any aws cloud internet gateway',
      'delete any aws cloud internet gateway',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'create_time' => date('c'),
    ];
  }

  /**
   * Test internet gateway attach.
   */
  public function testInternetGatewayAttachDetach(): void {
    try {
      $this->repeatTestInternetGatewayAttachDetach(self::MAX_TEST_REPEAT_COUNT);
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Repeating test internet gateway attach detach.
   *
   * @param int $max_test_repeat_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  private function repeatTestInternetGatewayAttachDetach($max_test_repeat_count = 1): void {

    $add = $this->createInternetGatewayTestFormData($max_test_repeat_count + 1);

    for ($i = 1; $i <= $max_test_repeat_count; $i++) {

      // Set up a test vpc.
      $vpc_id = $this->getRandomId();
      $vpc = $this->createVpcTestEntity($i, $vpc_id, $vpc_id);
      $vpc->setCidrBlock(Utils::getRandomCidr());
      $vpc->save();

      $internet_gateway_id = 'igw-' . $this->getRandomId();
      // Set up a test internet gateway.
      $internet_gateway = $this->createInternetGatewayTestEntity(
        $i,
        $internet_gateway_id,
        $internet_gateway_id,
        $this->cloudContext
      );
      $internet_gateway->setVpcId(NULL);
      $internet_gateway->setState('detached');
      $internet_gateway->save();
      $internet_gateway_id = $internet_gateway->getInternetGatewayId();
      $add[$i]['InternetGatewayId'] = $internet_gateway_id;
      $add[$i]['name'] = $internet_gateway->getName();
      $this->addInternetGatewayMockData($add[$i], $internet_gateway_id, $this->cloudContext, $this->webUser->id());

      $attach_data = [
        'vpc_id' => $vpc_id,
      ];

      // Confirm 'Attache/Detach' link.
      $this->drupalGet("/clouds/aws_cloud/$this->cloudContext/internet_gateway/$i");
      $this->assertSession()->linkExists($this->t('Attach'));
      $this->assertSession()->linkNotExists($this->t('Detach'));

      // Test attach.
      $this->updateAttachDetachInternetGatewayMockData('AttachInternetGateway', $internet_gateway_id, $vpc_id, 'attaching');
      $this->drupalGet("/clouds/aws_cloud/$this->cloudContext/internet_gateway/$i/attach");
      $this->submitForm(
        $attach_data,
        $this->t('Attach')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('The internet gateway @internet_gateway is attached to VPC @vpc', [
        '@vpc' => $vpc_id,
        '@internet_gateway' => $internet_gateway_id,
      ]));

      // Test detach.
      $internet_gateway->setState('available');
      $internet_gateway->setVpcId($vpc_id);
      $internet_gateway->save();

      // Confirm 'Attache/Detach' link.
      $this->drupalGet("/clouds/aws_cloud/$this->cloudContext/internet_gateway/$i");
      $this->assertSession()->linkNotExists($this->t('Attach'));
      $this->assertSession()->linkExists($this->t('Detach'));

      $this->updateAttachDetachInternetGatewayMockData('DetachInternetGateway', $internet_gateway_id, $vpc_id, 'detaching');
      $this->drupalGet("/clouds/aws_cloud/$this->cloudContext/internet_gateway/$i/detach");
      $this->submitForm(
        [],
        $this->t('Detach')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('The internet gateway @internet_gateway detached from VPC @vpc', [
        '@vpc' => $vpc_id,
        '@internet_gateway' => $internet_gateway_id,
      ]));
    }

  }

  /**
   * Tests detaching internet gateways with bulk operation.
   *
   * @throws \Exception
   */
  public function testInternetGatewayBulkDetach(): void {

    $cloud_context = $this->cloudContext;
    $total_count = 0;
    $total_internet_gateways = [];

    // NOTE: $num needs to be incremented outside $j loop.
    for ($i = 0, $num = 1; $i < self::MAX_TEST_REPEAT_COUNT; $i++) {

      $internet_gateways_count = random_int(1, self::MAX_TEST_REPEAT_COUNT);
      // Create internet gateways.
      $internet_gateways = $this->createInternetGatewayTestFormData($internet_gateways_count);

      for ($j = 0; $j < $internet_gateways_count; $j++, $num++) {

        // Set up a test vpc.
        $vpc = $this->createVpcTestEntity($i);
        $vpc->setCidrBlock(Utils::getRandomCidr());
        $vpc->save();
        $vpc_id = $vpc->getVpcId();

        // Set up a test internet_gateway.
        $internet_gateway = $this->createInternetGatewayTestEntity(
          $j,
          $internet_gateways[$j]['name'],
          $internet_gateways[$j]['name'],
          $this->cloudContext
        );
        $internet_gateway->setVpcId(NULL);
        $internet_gateway->setState('detached');
        $internet_gateway->save();
        $internet_gateway_id = $internet_gateway->getInternetGatewayId();
        $this->addInternetGatewayMockData($internet_gateways[$j], $internet_gateway_id, $cloud_context, $this->webUser->id());

        $attach_data = [
          'vpc_id' => $vpc_id,
        ];
        $this->drupalGet("/clouds/aws_cloud/$cloud_context/internet_gateway");

        // Test attach.
        $this->updateAttachDetachInternetGatewayMockData('AttachInternetGateway', $internet_gateway_id, $vpc_id, 'attaching');
        $this->drupalGet("/clouds/aws_cloud/$this->cloudContext/internet_gateway/$num/attach");
        $this->submitForm(
          $attach_data,
          $this->t('Attach')->render()
        );
        $this->assertNoErrorMessage();

        $internet_gateway->setState('available');
        $internet_gateway->setVpcId($vpc_id);
        $internet_gateway->save();
        $internet_gateways[$j]['vpc_id'] = $vpc_id;

      }

      $total_count += $internet_gateways_count;
      foreach ($internet_gateways ?: [] as $internet_gateway) {
        $total_internet_gateways[] = $internet_gateway;
      }

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/internet_gateway");

      $data = [];
      $data['action'] = 'aws_cloud_internet_gateway_detach_action';

      $checkboxes = $this->cssSelect('input[type=checkbox]');
      foreach ($checkboxes ?: [] as $checkbox) {
        if ($checkbox->getAttribute('name') === NULL) {
          continue;
        }

        $data[$checkbox->getAttribute('name')] = $checkbox->getAttribute('value');
      }

      // Confirm.
      $this->submitForm(
        $data,
        $this->t('Apply to selected items')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($total_count === 1
        ? $this->t('Are you sure you want to detach this internet gateway?')->render()
        : $this->t('Are you sure you want to detach these internet gateways?')->render()
      );

      foreach ($total_internet_gateways ?: [] as $internet_gateway) {
        $this->assertSession()->pageTextContains($internet_gateway['name']);
      }

      // Disassociate.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/internet_gateway/detach_multiple");
      $this->submitForm(
        [],
        $this->t('Detach')->render()
      );
      $this->assertNoErrorMessage();

      // Only string literals should be passed to t() where possible, so we do
      // not use $this->t() here.
      $total_count === 1
        ? $this->assertSession()->pageTextContains("Detached $internet_gateways_count internet gateway.")
        : $this->assertSession()->pageTextContains("Detached $total_count internet gateways.");

      for ($j = 0; $j < $total_count; $j++) {
        $internet_gateway = $total_internet_gateways[$j];
        $name = $internet_gateway['name'];
        $t_args = ['@type' => 'Internet gateway', '%label' => $name];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been detached.', $t_args)));
        $this->updateInternetGatewayMockData($j, $internet_gateway['name'], '');
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      $this->assertSession()->pageTextContains($this->t('Updated internet gateways.'));

      // Make sure if disassociated from an instance.
      foreach ($total_internet_gateways ?: [] as $internet_gateway) {
        $this->assertSession()->pageTextNotContains($internet_gateway['vpc_id']);
      }
    }
  }

}
