<?php

namespace Drupal\Tests\aws_cloud\Functional\Vpc;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;

/**
 * Tests AWS Cloud subnet.
 *
 * @group AWS Cloud
 */
class SubnetTest extends AwsCloudTestBase {

  public const AWS_CLOUD_SUBNET_REPEAT_COUNT = 2;
  public const AWS_CLOUD_SUBNET_ADD_BUTTON = 'Add AWS Cloud subnet';

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list aws cloud subnet',
      'add aws cloud subnet',
      'view any aws cloud subnet',
      'edit any aws cloud subnet',
      'delete any aws cloud subnet',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'subnet_id' => 'subnet-' . $this->getRandomId(),
      'error_code' => Ec2ServiceInterface::DRY_RUN_OPERATION,
    ];
  }

  /**
   * Tests CRUD for Subnet information.
   *
   * @throws \Exception
   */
  public function testSubnet(): void {
    $cloud_context = $this->cloudContext;

    // List Subnet for Amazon EC2.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains(self::AWS_CLOUD_SUBNET_ADD_BUTTON);

    $vpc_ids = $this->updateVpcsMockData(self::AWS_CLOUD_SUBNET_REPEAT_COUNT);

    // Create VPC.
    $vpc_data = $this->createVpcTestFormData(self::AWS_CLOUD_SUBNET_REPEAT_COUNT);
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $vpc = $this->createVpcTestEntity($i, $vpc_ids[$i], $vpc_data[$i]['name'], $cloud_context);
      $vpc->setCidrBlock($vpc_data[$i]['cidr_block']);
      $vpc->save();
    }

    // Add a new Subnet.
    $add = $this->createSubnetTestFormData(self::AWS_CLOUD_SUBNET_REPEAT_COUNT, $vpc_ids);
    $this->reloadMockData();
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {

      $subnet_id = 'subnet-' . $this->getRandomId();
      $this->addSubnetMockData($add[$i], $subnet_id, $cloud_context, $this->webUser->id());
      $this->updateCreateSubnetMockData($subnet_id);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Subnet', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);

      $add[$i]['subnet_id'] = $subnet_id;
    }

    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all subnet listing exists.
      $this->drupalGet('/clouds/aws_cloud/subnet');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Subnet.
    $edit = $this->createSubnetTestFormData(self::AWS_CLOUD_SUBNET_REPEAT_COUNT, $vpc_ids);
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++, $num++) {

      unset(
        $edit[$i]['vpc_id'],
        $edit[$i]['cidr_block']
      );

      $this->modifySubnetMockData($i, $edit[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Subnet', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['name']);
      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Update tags for empty.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'Subnets', 'Name', '', FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet");
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['subnet_id']);
    }

    // Delete name tags.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'Subnets', 'Name', '', TRUE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet");
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['subnet_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['subnet_id']);
    }

    // Update tags.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {

      $this->updateTagsInMockData($i, 'Subnets', 'Name', $edit[$i]['name'], FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet");
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['subnet_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Delete Subnet.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet/$num/delete");
      $this->assertNoErrorMessage();

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Subnet', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($edit[$i]['name']);
    }
  }

  /**
   * Test updating all subnet list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllSubnetList(): void {
    $cloud_configs = [];

    // List subnet for Amazon EC2.
    $this->drupalGet('/clouds/aws_cloud/subnet');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    $vpc_ids = $this->updateVpcsMockData(self::AWS_CLOUD_SUBNET_REPEAT_COUNT);

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();

      // Create VPC.
      $vpc_data = $this->createVpcTestFormData(self::AWS_CLOUD_SUBNET_REPEAT_COUNT);
      for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
        $vpc = $this->createVpcTestEntity($i, $vpc_ids[$i], $vpc_data[$i]['name'], $cloud_context);
        $vpc->setCidrBlock($vpc_data[$i]['cidr_block']);
        $vpc->save();
      }

      // Add a new Subnet.
      $add = $this->createSubnetTestFormData(self::AWS_CLOUD_SUBNET_REPEAT_COUNT, $vpc_ids);
      for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $subnet_id = $this->latestTemplateVars['subnet_id'];
        $this->addSubnetMockData($add[$i], $subnet_id, $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/subnet/add");
        $this->submitForm(
          $add[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = ['@type' => 'Subnet', '%label' => $add[$i]['name']];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

        $add[$i]['subnet_id'] = $subnet_id;
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/subnet');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Subnets',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Edit a Subnet.
    $edit = $this->createSubnetTestFormData(self::AWS_CLOUD_SUBNET_REPEAT_COUNT, $vpc_ids);
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++, $num++) {

        unset(
          $edit[$i]['vpc_id'],
          $edit[$i]['cidr_block']
        );

        $this->modifySubnetMockData($i, $edit[$i], $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/subnet/{$num}/edit");
        $this->submitForm(
          $edit[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = ['@type' => 'Subnet', '%label' => $edit[$i]['name']];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/subnet');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Subnets',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Delete Subnet in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->deleteSubnetMockData($i);
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/subnet');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Subnets',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($edit[$i]['name']);
    }
  }

  /**
   * Tests deleting subnets with bulk operation.
   *
   * @throws \Exception
   */
  public function testSubnetBulk(): void {
    $cloud_context = $this->cloudContext;
    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      // Create Subnets.
      $subnets = $this->createSubnetsRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($subnets ?: [] as $subnet) {
        $entities[] = $this->createSubnetTestEntity($index++, $subnet['SubnetId'], $subnet['Name'], $cloud_context);
      }
      $this->runTestEntityBulk('subnet', $entities);
    }
  }

  /**
   * Tests hiding buttons based on access control.
   */
  public function testSubnetAccessControl(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::AWS_CLOUD_SUBNET_REPEAT_COUNT; $i++) {
      $this->deleteIamPermission($cloud_context, 'CreateSubnet');
      // List Subnet for Amazon EC2.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains(self::AWS_CLOUD_SUBNET_ADD_BUTTON);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/subnet/add");
      $this->assertAccessDenied();
    }
  }

}
