<?php

namespace Drupal\Tests\aws_cloud\Functional\Vpc;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;

/**
 * Tests AWS Cloud transit gateway.
 *
 * @group AWS Cloud
 */
class TransitGatewayTest extends AwsCloudTestBase {

  public const AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT = 2;
  public const AWS_CLOUD_TRANSIT_GATEWAY_ADD_BUTTON = 'Add AWS Cloud transit gateway';

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list aws cloud transit gateway',
      'add aws cloud transit gateway',
      'view any aws cloud transit gateway',
      'edit any aws cloud transit gateway',
      'delete any aws cloud transit gateway',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'transit_gateway_id' => 'igw-' . $this->getRandomId(),
      'transit_gateway_route_table_id' => 'tgw-rtb-' . $this->getRandomId(),
      'error_code' => Ec2ServiceInterface::DRY_RUN_OPERATION,
    ];
  }

  /**
   * Tests CRUD for transit gateway information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testTransitGateway(): void {
    $cloud_context = $this->cloudContext;

    // List transit gateway for Amazon EC2.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains(self::AWS_CLOUD_TRANSIT_GATEWAY_ADD_BUTTON);

    $vpc_ids = $this->updateVpcsMockData(self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT);

    // Add a new transit gateway.
    $add = $this->createTransitGatewayTestFormData(self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT, $vpc_ids);
    $this->reloadMockData();
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $transit_gateway_id = 'igw-' . $this->getRandomId();
      $this->addTransitGatewayMockData($add[$i], $transit_gateway_id, $cloud_context, $this->webUser->id());
      $this->updateCreateTransitGatewayMockData($transit_gateway_id);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Transit gateway', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);

      $add[$i]['transit_gateway_id'] = $transit_gateway_id;
    }

    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all transit gateway listing exists.
      $this->drupalGet('/clouds/aws_cloud/transit_gateway');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a transit gateway.
    $edit = $this->createTransitGatewayTestFormData(self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT, $vpc_ids);
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++, $num++) {
      unset($edit[$i]['vpc_id']);

      $this->modifyTransitGatewayMockData($i, $edit[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Transit gateway', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Update tags for empty.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'TransitGateways', 'Name', '', FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway");
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['transit_gateway_id']);
    }

    // Delete name tags.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'TransitGateways', 'Name', '', TRUE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway");
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['transit_gateway_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['transit_gateway_id']);
    }

    // Update tags.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {

      $this->updateTagsInMockData($i, 'TransitGateways', 'Name', $edit[$i]['name'], FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway");
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['transit_gateway_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Delete transit gateway.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway/$num/delete");
      $this->assertNoErrorMessage();

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Transit gateway', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($edit[$i]['name']);
    }
  }

  /**
   * Tests CRUD for transit gateway information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllTransitGateway(): void {
    $cloud_configs = [];

    // List transit gateway for Amazon EC2.
    $this->drupalGet('/clouds/aws_cloud/transit_gateway');
    $this->assertNoErrorMessage();

    $vpc_ids = $this->updateVpcsMockData(self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT);

    // Create Cloud Config.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new transit gateway.
      $add = $this->createTransitGatewayTestFormData(self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT, $vpc_ids);
      for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $transit_gateway_id = $this->latestTemplateVars['transit_gateway_id'];
        $this->addTransitGatewayMockData($add[$i], $transit_gateway_id, $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/transit_gateway/add");
        $this->submitForm(
          $add[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = ['@type' => 'Transit gateway', '%label' => $add[$i]['name']];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
        $add[$i]['transit_gateway_id'] = $transit_gateway_id;
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/transit_gateway');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Transit gateways',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Edit a transit gateway.
    $edit = $this->createTransitGatewayTestFormData(self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT, $vpc_ids);
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++, $num++) {
        unset($edit[$i]['vpc_id']);

        $this->modifyTransitGatewayMockData($i, $edit[$i], $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/transit_gateway/{$num}/edit");
        $this->submitForm(
          $edit[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = [
          '@type' => 'Transit gateway',
          '%label' => $edit[$i]['name'],
        ];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/transit_gateway');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Transit gateways',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Delete transit gateway in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->deleteTransitGatewayMockData($i);
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/transit_gateway');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Transit gateways',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($edit[$i]['name']);
    }
  }

  /**
   * Tests deleting transit gateways with bulk operation.
   *
   * @throws \Exception
   */
  public function testTransitGatewayBulk(): void {
    $cloud_context = $this->cloudContext;
    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      // Create transit gateways.
      $transit_gateways = $this->createTransitGatewaysRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($transit_gateways ?: [] as $transit_gateway) {
        $entities[] = $this->createTransitGatewayTestEntity($index++, $transit_gateway['TransitGatewayId'], $transit_gateway['Name'], $cloud_context);
      }
      $this->runTestEntityBulk('transit_gateway', $entities);
    }
  }

  /**
   * Tests hiding buttons based on access control.
   */
  public function testTransitGatewayAccessControl(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::AWS_CLOUD_TRANSIT_GATEWAY_REPEAT_COUNT; $i++) {
      $this->deleteIamPermission($cloud_context, 'CreateTransitGateway');
      // List transit gateway for Amazon EC2.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains(self::AWS_CLOUD_TRANSIT_GATEWAY_ADD_BUTTON);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/transit_gateway/add");
      $this->assertAccessDenied();
    }
  }

}
