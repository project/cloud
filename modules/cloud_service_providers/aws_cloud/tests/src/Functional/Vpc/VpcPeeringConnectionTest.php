<?php

namespace Drupal\Tests\aws_cloud\Functional\Vpc;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;

/**
 * Tests AWS Cloud VPC peering connection.
 *
 * @group AWS Cloud
 */
class VpcPeeringConnectionTest extends AwsCloudTestBase {

  public const AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT = 2;
  public const AWS_CLOUD_VPC_PEERING_CONNECTION_ADD_BUTTON = 'Add AWS Cloud VPC peering connection';

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list aws cloud vpc peering connection',
      'add aws cloud vpc peering connection',
      'view any aws cloud vpc peering connection',
      'edit any aws cloud vpc peering connection',
      'delete any aws cloud vpc peering connection',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'vpc_peering_connection_id' => 'pcx' . $this->getRandomId(),
      'error_code' => Ec2ServiceInterface::DRY_RUN_OPERATION,
    ];
  }

  /**
   * Tests CRUD for VPC peering connection information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  public function testVpcPeeringConnection(): void {
    $cloud_context = $this->cloudContext;

    // List VPC peering connection for Amazon EC2.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains(self::AWS_CLOUD_VPC_PEERING_CONNECTION_ADD_BUTTON);

    $vpc_ids = $this->updateVpcsMockData(self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT);

    // Create VPC.
    $vpc_data = $this->createVpcTestFormData(self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT);
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $vpc = $this->createVpcTestEntity($i, $vpc_ids[$i], $vpc_data[$i]['name'], $cloud_context);
      $vpc->setCidrBlock($vpc_data[$i]['cidr_block']);
      $vpc->save();
    }

    // Add a new VPC peering connection.
    $add = $this->createVpcPeeringConnectionTestFormData(self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT, $vpc_ids);
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $vpc_peering_connection_id = $this->latestTemplateVars['vpc_peering_connection_id'];
      $this->addVpcPeeringConnectionMockData($add[$i], $vpc_peering_connection_id, $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'VPC peering connection',
        '%label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);

      $add[$i]['vpc_peering_connection_id'] = $vpc_peering_connection_id;

      // Accept a VPC peering connection.
      $num = $i + 1;
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection/$num/accept");
      $this->submitForm(
        [],
        $this->t('Accept')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'VPC peering connection',
        '%label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been accepted.', $t_args)));
    }

    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all vpc_peering_connection listing exists.
      $this->drupalGet('/clouds/aws_cloud/vpc_peering_connection');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a VPC peering connection.
    $edit = $this->createVpcPeeringConnectionTestFormData(self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT, $vpc_ids);
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++, $num++) {

      unset(
        $edit[$i]['requester_vpc_id'],
        $edit[$i]['accepter_vpc_id']
      );

      $this->modifyVpcPeeringConnectionMockData($i, $edit[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'VPC peering connection',
        '%label' => $edit[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Update tags for empty.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'VpcPeeringConnections', 'Name', '', FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection");
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['vpc_peering_connection_id']);
    }

    // Delete name tags.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'VpcPeeringConnections', 'Name', '', TRUE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection");
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['vpc_peering_connection_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['vpc_peering_connection_id']);
    }

    // Update tags.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {

      $this->updateTagsInMockData($i, 'VpcPeeringConnections', 'Name', $edit[$i]['name'], FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection");
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['vpc_peering_connection_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Delete VPC peering connection.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection/$num/delete");
      $this->assertNoErrorMessage();

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'VPC peering connection',
        '@label' => $edit[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($edit[$i]['name']);
    }
  }

  /**
   * Test updating all VpcPeeringConnection list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllVpcPeeringConnection(): void {
    $cloud_configs = [];

    // List VPC peering connection for Amazon EC2.
    $this->drupalGet('/clouds/aws_cloud/vpc_peering_connection');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    $vpc_ids = $this->updateVpcsMockData(self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT);

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();

      // Create VPC.
      $vpc_data = $this->createVpcTestFormData(self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT);
      for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
        $vpc = $this->createVpcTestEntity($i, $vpc_ids[$i], $vpc_data[$i]['name'], $cloud_context);
        $vpc->setCidrBlock($vpc_data[$i]['cidr_block']);
        $vpc->save();
      }

      // Add a new VPC peering connection.
      $add = $this->createVpcPeeringConnectionTestFormData(self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT, $vpc_ids);
      for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $vpc_peering_connection_id = $this->latestTemplateVars['vpc_peering_connection_id'];
        $this->addVpcPeeringConnectionMockData($add[$i], $vpc_peering_connection_id, $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/vpc_peering_connection/add");
        $this->submitForm(
          $add[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = [
          '@type' => 'VPC peering connection',
          '%label' => $add[$i]['name'],
        ];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

        $add[$i]['vpc_peering_connection_id'] = $vpc_peering_connection_id;
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/vpc_peering_connection');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'VPC peering connections',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Edit a VPC peering connection.
    $edit = $this->createVpcPeeringConnectionTestFormData(self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT, $vpc_ids);
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++, $num++) {

        unset(
          $edit[$i]['requester_vpc_id'],
          $edit[$i]['accepter_vpc_id']
        );

        $this->modifyVpcPeeringConnectionMockData($i, $edit[$i], $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/vpc_peering_connection/{$num}/edit");
        $this->submitForm(
          $edit[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = [
          '@type' => 'VPC peering connection',
          '%label' => $edit[$i]['name'],
        ];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/vpc_peering_connection');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'VPC peering connections',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Delete VPC peering connection in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->deleteVpcPeeringConnectionMockData($i);
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/vpc_peering_connection');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'VPC peering connections',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($edit[$i]['name']);
    }
  }

  /**
   * Tests deleting VPC peering connections with bulk operation.
   *
   * @throws \Exception
   */
  public function testVpcPeeringConnectionBulk(): void {
    $cloud_context = $this->cloudContext;
    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      // Create VPC peering connections.
      $vpc_peering_connections = $this->createVpcPeeringConnectionsRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($vpc_peering_connections as $vpc_peering_connection) {
        $entities[] = $this->createVpcPeeringConnectionTestEntity(
          $index++,
          $vpc_peering_connection['VpcPeeringConnectionId'],
          $vpc_peering_connection['Name'],
          $cloud_context
        );
      }
      $this->runTestEntityBulk('vpc_peering_connection', $entities);
    }
  }

  /**
   * Tests hiding buttons based on access control.
   */
  public function testVpcPeeringConnectionAccessControl(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::AWS_CLOUD_VPC_PEERING_CONNECTION_REPEAT_COUNT; $i++) {
      $this->deleteIamPermission($cloud_context, 'CreateVpcPeeringConnection');
      // List VPC peering connection for Amazon EC2.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains(self::AWS_CLOUD_VPC_PEERING_CONNECTION_ADD_BUTTON);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc_peering_connection/add");
      $this->assertAccessDenied();
    }
  }

}
