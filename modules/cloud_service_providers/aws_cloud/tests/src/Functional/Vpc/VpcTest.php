<?php

namespace Drupal\Tests\aws_cloud\Functional\Vpc;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;

/**
 * Tests AWS Cloud VPC.
 *
 * @group AWS Cloud
 */
class VpcTest extends AwsCloudTestBase {

  public const AWS_CLOUD_VPC_REPEAT_COUNT = 2;
  public const AWS_CLOUD_VPC_ADD_BUTTON = 'Add AWS Cloud VPC';

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list aws cloud vpc',
      'add aws cloud vpc',
      'view any aws cloud vpc',
      'edit any aws cloud vpc',
      'delete any aws cloud vpc',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'vpc_id' => 'vpc-' . $this->getRandomId(),
      'error_code' => Ec2ServiceInterface::DRY_RUN_OPERATION,
    ];
  }

  /**
   * Tests CRUD for VPC information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testVpc(): void {
    $cloud_context = $this->cloudContext;

    // List VPC for Amazon EC2.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains(self::AWS_CLOUD_VPC_ADD_BUTTON);

    // Add a new Vpc.
    $add = $this->createVpcTestFormData(self::AWS_CLOUD_VPC_REPEAT_COUNT);
    $this->reloadMockData();
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $vpc_id = 'vpc-' . $this->getRandomId();
      $this->addVpcMockData($add[$i], $vpc_id, $cloud_context, $this->webUser->id());
      $this->updateCreateVpcMockData($vpc_id);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'VPC', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);

      $add[$i]['vpc_id'] = $vpc_id;
    }

    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all VPC listing exists.
      $this->drupalGet('/clouds/aws_cloud/vpc');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Vpc.
    $edit = $this->createVpcTestFormData(self::AWS_CLOUD_VPC_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++, $num++) {

      unset($edit[$i]['cidr_block']);

      $this->modifyVpcMockData($i, $edit[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'VPC', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure uid.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Update tags for empty.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'Vpcs', 'Name', '', FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc");
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['vpc_id']);
    }

    // Delete name tags.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateTagsInMockData($i, 'Vpcs', 'Name', '', TRUE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc");
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['vpc_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['vpc_id']);
    }

    // Update tags.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {

      $this->updateTagsInMockData($i, 'Vpcs', 'Name', $edit[$i]['name'], FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc");
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($edit[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['vpc_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($edit[$i]['name']);
    }

    // Delete Vpc.
    for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc/$num/delete");
      $this->assertNoErrorMessage();

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'VPC', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($edit[$i]['name']);
    }
  }

  /**
   * Tests CRUD for VPC information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllVpc(): void {
    $cloud_configs = [];

    // List VPC for Amazon EC2.
    $this->drupalGet('/clouds/aws_cloud/vpc');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Vpc.
      $add = $this->createVpcTestFormData(self::AWS_CLOUD_VPC_REPEAT_COUNT);
      for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $vpc_id = $this->latestTemplateVars['vpc_id'];
        $this->addVpcMockData($add[$i], $vpc_id, $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/vpc/add");
        $this->submitForm(
          $add[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = ['@type' => 'VPC', '%label' => $add[$i]['name']];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
        $add[$i]['vpc_id'] = $vpc_id;
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/vpc');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'VPCs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Edit a Vpc.
    $edit = $this->createVpcTestFormData(self::AWS_CLOUD_VPC_REPEAT_COUNT);
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++, $num++) {

        unset($edit[$i]['cidr_block']);

        $this->modifyVpcMockData($i, $edit[$i], $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/vpc/{$num}/edit");
        $this->submitForm(
          $edit[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = ['@type' => 'VPC', '%label' => $edit[$i]['name']];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/vpc');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'VPCs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Delete Vpc in mock data.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->deleteVpcMockData($i);
    }

    // Make sure listing.
    $this->drupalGet('/clouds/aws_cloud/vpc');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'VPCs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($edit[$i]['name']);
    }
  }

  /**
   * Tests deleting VPCs with bulk operation.
   *
   * @throws \Exception
   */
  public function testVpcBulk(): void {
    $cloud_context = $this->cloudContext;
    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      // Create VPCs.
      $vpcs = $this->createVpcsRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($vpcs ?: [] as $vpc) {
        $entities[] = $this->createVpcTestEntity($index++, $vpc['VpcId'], $vpc['Name'], $cloud_context);
      }
      $this->runTestEntityBulk('vpc', $entities);
    }
  }

  /**
   * Tests hiding buttons based on access control.
   */
  public function testVpcAccessControl(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::AWS_CLOUD_VPC_REPEAT_COUNT; $i++) {
      $this->deleteIamPermission($cloud_context, 'CreateVpc');
      // List VPC for Amazon EC2.
      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains(self::AWS_CLOUD_VPC_ADD_BUTTON);

      $this->drupalGet("/clouds/aws_cloud/$cloud_context/vpc/add");
      $this->assertAccessDenied();
    }
  }

}
