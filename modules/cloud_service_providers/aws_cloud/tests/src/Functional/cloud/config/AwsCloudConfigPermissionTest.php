<?php

namespace Drupal\Tests\aws_cloud\Functional\cloud\config;

use Drupal\Tests\aws_cloud\Traits\AwsCloudTestFormDataTrait;
use Drupal\Tests\aws_cloud\Traits\AwsCloudTestMockTrait;
use Drupal\Tests\cloud\Functional\cloud\config\CloudConfigTestBase;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Tests cloud service provider (CloudConfig).
 *
 * @group Cloud
 */
class AwsCloudConfigPermissionTest extends CloudConfigTestBase {

  use AwsCloudTestFormDataTrait;
  use AwsCloudTestMockTrait;

  /**
   * AWS_CLOUD_CONFIG_REPEAT_COUNT.
   *
   * @var int
   */
  public const AWS_CLOUD_CONFIG_REPEAT_COUNT = 2;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'cloud',
    'aws_cloud',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return array_merge([
      'administer cloud service providers',
      'add cloud service providers',
      'edit cloud service providers',
      'edit own cloud service providers',
      'delete cloud service providers',
      'delete own cloud service providers',
      'view published cloud service providers',
      'view own published cloud service providers',
      'access dashboard',
      'view cloud service provider admin list',
      'list cloud server template',
      'administer aws_cloud',
      'view aws cloud instance type prices',
      'list aws cloud instance',
    ], $this->getOptionalPermissions());
  }

  /**
   * Get optional permissions. In case of lacking, resulting in warning.
   *
   * @return string[]
   *   Optional permissions.
   */
  private function getOptionalPermissions(): array {
    return [
      'add aws cloud key pair',
      'add aws cloud network interface',
      'add aws cloud security group',
      'add aws cloud service provider',
      'add aws cloud subnet',
      'add aws cloud vpc',
      'delete own aws cloud security group',
      'edit own aws cloud instance',
      'edit own aws cloud security group',
      'list aws cloud carrier gateway',
      'list aws cloud elastic ip',
      'list aws cloud image',
      'list aws cloud internet gateway',
      'list aws cloud key pair',
      'list aws cloud network interface',
      'list aws cloud security group',
      'list aws cloud snapshot',
      'list aws cloud subnet',
      'list aws cloud transit gateway',
      'list aws cloud volume',
      'list aws cloud vpc peering connection',
      'list aws cloud vpc',
      'view own aws cloud instance',
    ];
  }

  /**
   * Get permissions to validate.
   *
   * @FIXEME no duplicate definition.
   *
   * @return array
   *   An associative array of permissions.
   */
  private function getPermissionsToValidate(): array {
    return [
      'mandatory' => [
        // EC2 APIs.
        // Excluding 'DescribeInstances' as this check is included in
        // aws_cloud_form_cloud_config_aws_cloud_describe_instances_validate().
        'DescribeAddresses' => 'list aws cloud elastic ip',
        'DescribeAvailabilityZones' => 'list aws cloud instance',
        'DescribeCarrierGateways' => 'list aws cloud carrier gateway',
        'DescribeFlowLogs' => 'list aws cloud vpc',
        'DescribeIamInstanceProfileAssociations' => 'list aws cloud instance',
        'DescribeImageAttribute' => 'list aws cloud image',
        'DescribeImages' => 'list aws cloud image',
        'DescribeInstanceAttribute' => 'list aws cloud instance',
        'DescribeInternetGateways' => 'list aws cloud internet gateway',
        'DescribeKeyPairs' => 'list aws cloud key pair',
        'DescribeNetworkInterfaces' => 'list aws cloud network interface',
        'DescribeSecurityGroups' => 'list aws cloud security group',
        'DescribeSnapshots' => 'list aws cloud snapshot',
        'DescribeSubnets' => 'list aws cloud subnet',
        'DescribeTransitGateways' => 'list aws cloud transit gateway',
        'DescribeVolumes' => 'list aws cloud volume',
        'DescribeVpcPeeringConnections' => 'list aws cloud vpc peering connection',
        'DescribeVpcs' => 'list aws cloud vpc',
        'DescribeLaunchTemplates' => 'list cloud server template',
        'DescribeLaunchTemplateVersions' => 'list cloud server template',
        // IAM APIs.
        'ListInstanceProfiles' => 'add aws cloud service provider',
      ],
      'optional' => [
        // EC2 APIs.
        'AuthorizeSecurityGroupEgress' => 'edit own aws cloud security group',
        'AuthorizeSecurityGroupIngress' => 'edit own aws cloud security group',
        'CreateKeyPair' => 'add aws cloud key pair',
        'CreateNetworkInterface' => 'add aws cloud network interface',
        'CreateSecurityGroup' => 'add aws cloud security group',
        'CreateSubnet' => 'add aws cloud subnet',
        'CreateTags' => 'edit own aws cloud instance',
        'CreateVpc' => 'add aws cloud vpc',
        'GetConsoleOutput' => 'view own aws cloud instance',
        'RevokeSecurityGroupEgress' => 'delete own aws cloud security group',
        'RevokeSecurityGroupIngress' => 'delete own aws cloud security group',
        // Cloud Watch APIs.
        'GetMetricData' => 'view own aws cloud instance',
      ],
    ];
  }

  /**
   * Get permissions that those validation depends on other permission.
   *
   * @return array
   *   An associative array.
   */
  private function getPermissionsDependingOn(): array {
    return [
      'DescribeSecurityGroups' => [
        'AuthorizeSecurityGroupEgress',
        'AuthorizeSecurityGroupIngress',
        'CreateTags',
        'RevokeSecurityGroupEgress',
        'RevokeSecurityGroupIngress',
      ],
    ];
  }

  /**
   * Flatten permissions.
   *
   * @return array
   *   Permissions.
   */
  private function flattenPermissions(): array {
    return array_keys(array_merge([], ...array_values($this->getPermissionsToValidate())));
  }

  /**
   * Create cloud context.
   *
   * @param string $bundle
   *   The CloudConfig bundle type ('aws_cloud').
   *
   * @return \Drupal\cloud\Entity\CloudConfig
   *   The cloud service provider (CloudConfig) entity.
   */
  protected function createCloudContext($bundle = __CLASS__): CloudContentEntityBase {
    return parent::createCloudContext($this->getModuleName($bundle));
  }

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function setUp(): void {

    parent::setUp();

    $this->init(__CLASS__, $this);
    $this->initMockInstanceTypes();

    // Delete the existing $this->cloudContext since we test a CloudConfig
    // entities multiple deletion for themselves.
    if (!empty($this->cloudConfig)) {
      $this->cloudConfig->delete();
    }

    $this->setUpAddOperation();
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'group_id'    => 'sg-' . $this->getRandomId(),
      'error_code'  => Ec2ServiceInterface::DRY_RUN_OPERATION,
    ];
  }

  /**
   * Set up before testing Create operation.
   *
   * @throws \Exception
   */
  private function setUpAddOperation(): void {

    // List AWS Cloud service providers for Amazon EC2.
    $this->drupalGet('/admin/structure/cloud_config');
    $this->assertNoErrorMessage();

    // Add random VPC information (Server-side).
    $vpcs = $this->createVpcsRandomTestFormData();
    $subnets = $this->createSubnetsRandomTestFormData();
    $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);

    // Add random image information (Server-side).
    $images = $this->createImagesRandomTestFormData();
    $this->updateImagesMockData($images);
  }

  /**
   * Set up detailed data before save operation.
   *
   * @param int $repeat_index
   *   Test repeat index.
   * @param array $add
   *   Cloud config data to add.
   * @param array $selected_region_codes
   *   Selected region codes to add.
   *
   * @throws \Exception
   */
  private function setUpDetailsBeforeSave(int $repeat_index, array &$add, array $selected_region_codes): void {

    if (empty($add[$repeat_index])) {
      return;
    }

    foreach ($selected_region_codes ?: [] as $selected_region_code) {
      // Set randomly selected regions.
      $add[$repeat_index]['regions[]'][] = $selected_region_code;
    }

    // These variables below are filled in and sent from the form to add an
    // AWS Cloud service provider on a browser.  Therefore, we do not use the
    // auto-generated test values by createCloudConfigTestFormData.
    // field_region is used for each AWS Cloud service provider edit form.
    unset(
      $add[$repeat_index]['field_region']
    );

    // Set default security group for IAM permission check.
    $this->addSecurityGroupMockData('default', $this->getRandomSecurityGroupDescription(), $this->getRandomId());
  }

  /**
   * Assert warning and error messages based on unset IAM permissions.
   *
   * @param string $unset_mandatory_perm
   *   A mandatory IAM permission to unset.
   * @param string $unset_optional_perm
   *   An optional IAM permission to unset.
   */
  private function assertWarningAndErrorsBasedOnIamPermissions(string $unset_mandatory_perm, string $unset_optional_perm): void {

    // Asserting an error message regardless of randomly selected permissions.
    $t_args = [
      '@to_fix' => 'credentials',
      '@unauthorized_apis' => $unset_mandatory_perm,
    ];
    $this->assertSession()
      ->pageTextContains(strip_tags($this->t('Add the following IAM permission(s) to your @to_fix: @unauthorized_apis', $t_args)));

    if (!empty($this->getPermissionsDependingOn()[$unset_mandatory_perm])
      && in_array($unset_optional_perm, $this->getPermissionsDependingOn()[$unset_mandatory_perm], TRUE)) {
      // If the removed optional permission depends on the removed mandatory
      // permission, a warning message is to be suppressed. Only error
      // message appears.
      $this->assertErrorMessage();
      return;
    }

    $t_args = [
      '@to_fix' => 'credentials',
      '@unauthorized_apis' => $unset_optional_perm,
    ];
    $this->assertSession()
      ->pageTextContains(strip_tags($this->t('You might want to add the following IAM permission(s) to your @to_fix: @unauthorized_apis', $t_args)));
    $this->assertWarningAndErrorMessage();

  }

  /**
   * Tests warning messages while adding cloud service provider (CloudConfig).
   *
   * Tests for adding cloud service provider (CloudConfig) with warnings lacking
   * of Drupal permission.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testAddCloudServiceProviderWithWarnings(): void {

    // Add a new CloudConfig information.
    $add = $this->createCloudConfigTestFormData(self::AWS_CLOUD_CONFIG_REPEAT_COUNT);

    // Get all regions.
    $regions = \Drupal::service('aws_cloud.ec2')->getRegions();

    // Pick up the regions using CloudConfigInterface::MAX_REGION_COUNT.
    $selected_region_codes = (array) array_rand($regions, CloudConfigInterface::MAX_REGION_COUNT);

    for ($i = 0; $i < self::AWS_CLOUD_CONFIG_REPEAT_COUNT; $i++) {

      if (empty($add[$i])) {
        continue;
      }

      // Pick up the regions by up to count($regions).
      // Get the diff for remaining regions.
      $regions = array_diff($regions, $selected_region_codes);
      $selected_region_codes = (array) array_rand($regions, CloudConfigInterface::MAX_REGION_COUNT);
      $this->setUpDetailsBeforeSave($i, $add, $selected_region_codes);

      // Expect warning by removing an optional permissions.
      $perms = $this->getPermissions();
      $count_mandatory_perms = count($perms) - count($this->getOptionalPermissions());
      $unset_rand_key = array_rand($this->getOptionalPermissions()) + $count_mandatory_perms;
      unset($perms[$unset_rand_key]);

      $web_user = $this->drupalCreateUser($perms);
      $this->drupalLogin($web_user);

      // Set default security group for IAM permission check.
      $this->addSecurityGroupMockData('default', $this->getRandomSecurityGroupDescription(), $this->getRandomId());

      $this->drupalGet('/admin/structure/cloud_config/add');
      // Expect no error on loading add form.
      $this->assertNoErrorMessage();

      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertWarningAndErrorMessage();
      $this->assertGoogleAppErrorOnly();

      // Assert warning for the unset permission.
      $t_args = ['@unset_perm' => $this->getPermissions()[$unset_rand_key]];
      $this->assertSession()->pageTextContains(strip_tags($this->t('You might want to enable the following permission(s): @unset_perm', $t_args)));
    }
  }

  /**
   * Tests Create operation for cloud service provider expecting errors.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testAddCloudServiceProviderWithErrors(): void {

    // Add a new Config information.
    $add = $this->createCloudConfigTestFormData(self::AWS_CLOUD_CONFIG_REPEAT_COUNT);
    // Get all regions in assoc_array: name => description.
    $regions = \Drupal::service('aws_cloud.ec2')->getRegions();

    for ($i = 0; $i < self::AWS_CLOUD_CONFIG_REPEAT_COUNT; $i++) {
      // Pick up the regions using CloudConfigInterface::MAX_REGION_COUNT.
      $selected_region_codes = (array) array_rand($regions, CloudConfigInterface::MAX_REGION_COUNT);
      $this->setUpDetailsBeforeSave($i, $add, $selected_region_codes);

      // Enable/disable IAM permission check.
      $permission_options = [
        0 => Ec2ServiceInterface::IAM_VALIDATION_NO_CHECK,
        1 => Ec2ServiceInterface::IAM_VALIDATION_ONE_REGION,
        2 => Ec2ServiceInterface::IAM_VALIDATION_ALL_REGIONS,
      ];

      $is_check_iam_permissions_enabled = $permission_options[array_rand($permission_options)];
      $add[$i]['iam_validation_options'] = $is_check_iam_permissions_enabled;
      // Set all required IAM permissions.
      $this->updateErrorCodeMockData($this->flattenPermissions(), Ec2ServiceInterface::DRY_RUN_OPERATION);
      // For error-expected testing, unset randomly-picked IAM permissions.
      $unset_mandatory_perm = array_rand($this->getPermissionsToValidate()['mandatory']);
      $unset_optional_perm = array_rand($this->getPermissionsToValidate()['optional']);

      $errors_and_warnings = array_rand([TRUE, FALSE]);
      $this->updateAwsCloudConfigPermissionMockData([
        $unset_mandatory_perm,
        $unset_optional_perm,
      ], $errors_and_warnings);

      // Make sure no error message on loading an add form.
      $this->drupalGet('/admin/structure/cloud_config/add');
      $this->assertNoErrorMessage();

      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );

      if ($is_check_iam_permissions_enabled === Ec2ServiceInterface::IAM_VALIDATION_NO_CHECK) {
        $this->assertGoogleAppErrorOnly();
        continue;
      }
      !empty($errors_and_warnings)
        ? $this->assertWarningAndErrorsBasedOnIamPermissions($unset_mandatory_perm, $unset_optional_perm)
        : $this->assertErrorMessage();
      // Assert no additional errors.
      $this->assertSession()->pageTextNotContains($this->t('An error occurred'));
      $this->assertSession()->pageTextNotContains($this->t('An illegal choice has been detected. Contact the site administrator.'));
    }
  }

  /**
   * Tests Update operation for cloud service provider expecting errors.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testEditCloudServiceProviderWithErrors(): void {

    // Add a new Config information only once regardless of
    // self::AWS_CLOUD_CONFIG_REPEAT_COUNT.
    $add = $this->createCloudConfigTestFormData(1);

    // Get all regions in assoc_array: name => description.
    $regions = \Drupal::service('aws_cloud.ec2')->getRegions();

    // The length of the cloud service provider name limited up to
    // CloudConfigInterface::MAX_NAME_LENGTH characters due to the
    // consideration of a suffix by adding a UUID to deploy multiple Cloud
    // Orchestrator instances.  Therefore, we shorten the AWS region name
    // from e.g. `US East (N. Virgina)` to `(N.Virginia)` to keep the
    // location name instead of using both region and location name.
    $region_names = preg_replace('/^.*(\(.*\))/', '$1', $regions);

    // Pick up one region to be edited later.
    $selected_region_codes = (array) array_rand($regions, 1);

    $repeat_index = 0;
    $this->setUpDetailsBeforeSave($repeat_index, $add, $selected_region_codes);

    // To avoid DryRunOperation AwdException, empty the error code.
    $this->updateErrorCodeMockData($this->flattenPermissions(), '');

    // Make sure no error message on showing an add form.
    $this->drupalGet('/admin/structure/cloud_config/add');
    $this->assertNoErrorMessage();

    $this->submitForm(
      $add[$repeat_index],
      $this->t('Save')->render()
    );

    $label_prefix = $add[$repeat_index]['name[0][value]'];
    $selected_region_code = $selected_region_codes[$repeat_index];
    $labels[$repeat_index][] = substr(
      "{$label_prefix} {$region_names[$selected_region_code]}",
      0, CloudConfigInterface::MAX_NAME_LENGTH - 1
    );
    // We need to grant the 'view <CLOUD_CONTEXT>' permission.
    // Note: grantPermissions() can be called after "Save", and will be in
    // effect later on the refreshed list.
    $this->grantPermissions(
      Role::load(RoleInterface::AUTHENTICATED_ID), [
        'view ' . aws_cloud_form_cloud_config_aws_cloud_add_form_create_cloud_context(
          $label_prefix,
          $selected_region_code
        ),
      ]
    );

    // Expect no error on add test and proceed with edit testing.
    $this->assertErrorMessage();
    $this->assertGoogleAppErrorOnly();
    $this->assertSession()->pageTextContains($this->t('Status message'));
    $this->assertSession()
      ->pageTextContains($this->t('Creating cloud service provider was performed successfully.'));

    // Make sure listing for '/admin/config/services/cloud/aws_cloud'.
    $this->drupalGet('/admin/config/services/cloud/aws_cloud');
    $this->assertNoErrorMessage();
    $this->assertSession()->linkExists($labels[$repeat_index][0]);

    // Edit Config starts here.
    $edit = $this->createCloudConfigTestFormData(self::AWS_CLOUD_CONFIG_REPEAT_COUNT);

    // This is CloudConfig test case, so we do not require default
    // $this->cloudContext, which has been already deleted in this setUp().
    // The entity number of $this->cloudContext was '1'.  Therefore, the entity
    // number starts from '2', not '1', here.
    for ($i = 0, $num = 2; $i < self::AWS_CLOUD_CONFIG_REPEAT_COUNT; $i++) {
      // Enable/disable IAM permission check.
      $is_check_iam_permissions_enabled = array_rand([0, 1]);
      $edit[$i]['field_check_iam_permissions[value]'] = $is_check_iam_permissions_enabled;

      // Set all required IAM permissions.
      $this->updateErrorCodeMockData($this->flattenPermissions(), Ec2ServiceInterface::DRY_RUN_OPERATION);
      // For error-expected testing, unset randomly-picked IAM permissions.
      $unset_mandatory_perm = array_rand($this->getPermissionsToValidate()['mandatory']);
      $unset_optional_perm = array_rand($this->getPermissionsToValidate()['optional']);

      $errors_and_warnings = array_rand([TRUE, FALSE]);
      $this->updateAwsCloudConfigPermissionMockData([
        $unset_mandatory_perm,
        $unset_optional_perm,
      ], $errors_and_warnings);

      // Make sure no error message on showing an edit form.
      $this->drupalGet("/admin/structure/cloud_config/{$num}/edit");
      $this->assertNoErrorMessage();

      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );

      if ($is_check_iam_permissions_enabled === 0) {
        $this->assertGoogleAppErrorOnly();
        continue;
      }
      !empty($errors_and_warnings)
        ? $this->assertWarningAndErrorsBasedOnIamPermissions($unset_mandatory_perm, $unset_optional_perm)
        : $this->assertErrorMessage();
      // Assert no additional errors.
      $this->assertSession()->pageTextNotContains($this->t('An error occurred'));
      $this->assertSession()->pageTextNotContains($this->t('An illegal choice has been detected. Contact the site administrator.'));
    }
  }

}
