<?php

namespace Drupal\Tests\aws_cloud\Functional\cloud\config;

use Drupal\Tests\aws_cloud\Traits\AwsCloudTestFormDataTrait;
use Drupal\Tests\aws_cloud\Traits\AwsCloudTestMockTrait;
use Drupal\Tests\cloud\Functional\cloud\config\CloudConfigTestBase;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Tests cloud service provider (CloudConfig).
 *
 * @group Cloud
 */
class AwsCloudConfigTest extends CloudConfigTestBase {

  use AwsCloudTestFormDataTrait;
  use AwsCloudTestMockTrait;

  /**
   * AWS_CLOUD_CONFIG_REPEAT_COUNT.
   *
   * @var int
   */
  public const AWS_CLOUD_CONFIG_REPEAT_COUNT = 5;

  /**
   * AWS_CLOUD_CONFIG_MENU_REPEAT_COUNT.
   *
   * @var int
   */
  public const AWS_CLOUD_CONFIG_MENU_REPEAT_COUNT = 5;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'cloud',
    'aws_cloud',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return array_merge([
      'administer cloud service providers',
      'add cloud service providers',
      'edit cloud service providers',
      'edit own cloud service providers',
      'delete cloud service providers',
      'delete own cloud service providers',
      'view published cloud service providers',
      'view own published cloud service providers',
      'access dashboard',
      'view cloud service provider admin list',
      'list cloud server template',
      'administer aws_cloud',
      'view aws cloud instance type prices',
      'list aws cloud instance',
    ], $this->getOptionalPermissions());
  }

  /**
   * Get optional permissions. In case of lacking, resulting in warning.
   *
   * @return string[]
   *   Optional permissions.
   */
  private function getOptionalPermissions(): array {
    return [
      'add aws cloud key pair',
      'add aws cloud network interface',
      'add aws cloud security group',
      'add aws cloud service provider',
      'add aws cloud subnet',
      'add aws cloud vpc',
      'delete own aws cloud security group',
      'edit own aws cloud instance',
      'edit own aws cloud security group',
      'list aws cloud carrier gateway',
      'list aws cloud elastic ip',
      'list aws cloud image',
      'list aws cloud internet gateway',
      'list aws cloud key pair',
      'list aws cloud network interface',
      'list aws cloud security group',
      'list aws cloud snapshot',
      'list aws cloud subnet',
      'list aws cloud transit gateway',
      'list aws cloud volume',
      'list aws cloud vpc peering connection',
      'list aws cloud vpc',
      'view own aws cloud instance',
    ];
  }

  /**
   * Create cloud context.
   *
   * @param string $bundle
   *   The CloudConfig bundle type ('aws_cloud').
   *
   * @return \Drupal\cloud\Entity\CloudConfig
   *   The cloud service provider (CloudConfig) entity.
   */
  protected function createCloudContext($bundle = __CLASS__): CloudContentEntityBase {
    return parent::createCloudContext($this->getModuleName($bundle));
  }

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {

    parent::setUp();

    $this->init(__CLASS__, $this);
    $this->initMockInstanceTypes();

    // Delete the existing $this->cloudContext since we test a CloudConfig
    // entities multiple deletion for themselves.
    if (!empty($this->cloudConfig)) {
      $this->cloudConfig->delete();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'group_id'  => 'sg-' . $this->getRandomId(),
    ];
  }

  /**
   * Tests CRUD for cloud service provider (CloudConfig) information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testCloudConfig(): void {

    // -----------------------------------.
    // 1. Add AWS Cloud service providers .
    // -----------------------------------.
    // List AWS Cloud service providers for Amazon EC2.
    $this->drupalGet('/admin/structure/cloud_config');
    $this->assertNoErrorMessage();

    // Add a new CloudConfig information.
    $add = $this->createCloudConfigTestFormData(self::AWS_CLOUD_CONFIG_REPEAT_COUNT);

    // Add random VPC information (Server-side).
    $vpcs = $this->createVpcsRandomTestFormData();
    $subnets = $this->createSubnetsRandomTestFormData();
    $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);

    // Add random image information (Server-side).
    $images = $this->createImagesRandomTestFormData();
    $this->updateImagesMockData($images);

    // Get all regions.
    $regions = \Drupal::service('aws_cloud.ec2')->getRegions();

    // Pick up the regions using CloudConfigInterface::MAX_REGION_COUNT.
    $remaining_regions = array_intersect_key($regions, array_flip(
      array_rand($regions, CloudConfigInterface::MAX_REGION_COUNT)
    ));

    for ($selected_region_codes = [], $labels = [], $instance_types = [],
      $i = 0; $i < self::AWS_CLOUD_CONFIG_REPEAT_COUNT; $i++) {

      if (empty($add[$i])) {
        continue;
      }

      $label_prefix = $add[$i]['name[0][value]'];
      $add[$i]['regions[]'] = [];

      // Select regions randomly based on the available regions by getting diff
      // with the previous selected regions.  The number of regions to select is
      // different in each random test case.
      $remaining_regions = array_diff_key($remaining_regions, array_flip(
        !empty($selected_region_codes[$i - 1])
          ? $selected_region_codes[$i - 1] : []
      ));

      // Pick up the regions by up to:
      // count($regions) - self::AWS_CLOUD_CONFIG_REPEAT_COUNT + $i + 1.
      $selected_region_codes[$i] = (array) array_rand(
        $remaining_regions,
        random_int(1, count($remaining_regions) - self::AWS_CLOUD_CONFIG_REPEAT_COUNT + $i + 1)
      );

      // The length of the cloud service provider name limited up to
      // CloudConfigInterface::MAX_NAME_LENGTH characters due to the
      // consideration of a suffix by adding a UUID to deploy multiple Cloud
      // Orchestrator instances.  Therefore, we shorten the AWS region name
      // from e.g. `US East (N. Virgina)` to `(N.Virginia)` to keep the
      // location name instead of using both region and location name.
      $region_names = preg_replace('/^.*(\(.*\))/', '$1', $regions);

      foreach ($selected_region_codes[$i] ?: [] as $selected_region_code) {

        $labels[$i][] = substr(
          "{$label_prefix} {$region_names[$selected_region_code]}",
          0, CloudConfigInterface::MAX_NAME_LENGTH - 1
        );

        // Set a selected region code.
        $add[$i]['regions[]'][] = $selected_region_code;

        // Get AWS Cloud instance type prices.
        $instance_types[$i][] = aws_cloud_get_instance_types_by_region($regions[$selected_region_code]);

        // Set cache ker for AWS Cloud instance type prices.
        $cache_key = _aws_cloud_get_instance_type_cache_key_by_region($regions[$selected_region_code]);

        // Delete cache of AWS Cloud instance type prices.
        \Drupal::cache()->delete($cache_key);
      }

      // These variables below are filled in and sent from the form to add an
      // AWS Cloud service provider on a browser.  Therefore, we do not use the
      // auto-generated test values by createCloudConfigTestFormData.
      // field_region is used for each AWS Cloud service provider edit form.
      unset(
        $add[$i]['field_region']
      );

      // Set 'field_get_price_list' value.
      $add[$i]['field_get_price_list[value]'] = array_rand([0, 1]);

      // Set default security group for IAM permission check.
      $this->addSecurityGroupMockData('default', $this->getRandomSecurityGroupDescription(), $this->getRandomId());

      $this->drupalGet('/admin/structure/cloud_config/add');
      $this->assertNoErrorMessage();

      $this->drupalGet('/admin/structure/cloud_config/add');
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );

      foreach ($selected_region_codes[$i] ?: [] as $selected_region_code) {
        // We need to grant the 'view <CLOUD_CONTEXT>' permission.
        // Note: grantPermissions() can be called after "Save", and will be in
        // effect later on the refreshed list.
        $this->grantPermissions(
          Role::load(RoleInterface::AUTHENTICATED_ID), [
            'view ' . aws_cloud_form_cloud_config_aws_cloud_add_form_create_cloud_context(
              $label_prefix,
              $selected_region_code
            ),
          ]
        );
      }

      // NOTE: We do not use assertNoErrorMessage(), since it displays
      // "The module Google Applications is invalid. Enable the module.".
      // Thus, asserting any other error messages not found.
      $this->assertErrorMessage();
      $this->assertGoogleAppErrorOnly();

      $this->assertSession()->pageTextContains($this->t('Status message'));
      $this->assertSession()
        ->pageTextContains($this->t('Creating cloud service provider was performed successfully.'));

      // Make sure listing for '/admin/structure/cloud_config'.
      $this->drupalGet('/admin/structure/cloud_config');
      $this->assertNoErrorMessage();

      foreach ($labels[$i] ?: [] as $label) {
        $this->assertSession()->linkExists($label);
      }

      // Make sure listing for '/clouds'.
      $this->drupalGet('/clouds');
      $this->assertNoErrorMessage();

      foreach ($labels[$i] ?: [] as $label) {
        $this->assertSession()->linkExists($label);
      }

      // Make sure listing for '/admin/config/services/cloud/aws_cloud'.
      $this->drupalGet('/admin/config/services/cloud/aws_cloud');
      $this->assertNoErrorMessage();

      foreach ($labels[$i] ?: [] as $label) {
        $this->assertSession()->linkExists($label);
      }

      // Make sure listing of AWS Cloud instance type prices.
      $cloud_context = aws_cloud_form_cloud_config_aws_cloud_add_form_create_cloud_context(
        $label_prefix,
        $selected_region_code ?? ''
      );
      $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/instance_type_price");

      $this->assertNoErrorMessage();
      $this->assertSession()
        ->pageTextContains($this->t('AWS Cloud instance type prices'));
      foreach ($instance_types[$i] ?: [] as $instance_type) {
        foreach ($instance_type ?: [] as $instance_type_data) {
          $parts = explode(':', $instance_type_data);

          $add[$i]['field_get_price_list[value]'] === 1
            ? $this->assertSession()->pageTextContains($parts[0])
            : $this->assertSession()->pageTextNotContains($parts[0]);
        }
      }

      // Try to add an AWS Cloud service provider w/ the same input data.
      $this->drupalGet('/admin/structure/cloud_config/add');
      $this->assertNoErrorMessage();

      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );

      // Make sure if the error is reported or not.
      $this->assertErrorMessage();
      $this->assertSession()
        ->pageTextContains($this->t('The AWS Cloud service provider(s) already exists as the same ID: @regions. Specify the different AWS Cloud service provider name since the AWS Cloud service provider ID must be unique.', [
          '@regions' => implode(', ', array_map(static function ($selected_region_code) use ($label_prefix) {
            return aws_cloud_form_cloud_config_aws_cloud_add_form_create_cloud_context(
              $label_prefix,
              $selected_region_code
            );
          }, $selected_region_codes[$i])),
        ]));
    }

    // ------------------------------------.
    // 2. Edit AWS Cloud service providers .
    // ------------------------------------.
    // Get the CloudConfig form data to edit.
    $edit = $this->createCloudConfigTestFormData(self::AWS_CLOUD_CONFIG_REPEAT_COUNT);

    // This is CloudConfig test case, so we do not require default
    // $this->cloudContext, which has been already deleted in this setUp().
    // The entity number of $this->cloudContext was '1'.  Therefore, the entity
    // number starts from '2', not '1', here.
    for ($num = 2, $labels = [], $instance_types = [],
      $i = 0; $i < self::AWS_CLOUD_CONFIG_REPEAT_COUNT; $i++) {

      if (empty($edit[$i])
        || empty($selected_region_codes[$i])) {
        continue;
      }

      $label_prefix = $edit[$i]['name[0][value]'];

      // Edit AWS Cloud service provider of entity selected region one by one.
      foreach ($selected_region_codes[$i] ?: [] as $selected_region_code) {
        // The length of the cloud service provider name limited up to
        // CloudConfigInterface::MAX_NAME_LENGTH characters due to the
        // consideration of a suffix by adding a UUID to deploy multiple Cloud
        // Orchestrator instances.  Therefore, we shorten the AWS region name
        // from e.g. `US East (N. Virgina)` to `(N.Virginia)` to keep the
        // location name instead of using both region and location name.
        $region_name = preg_replace('/^.*(\(.*\))/', '$1', $regions[$selected_region_code]);
        $labels[$i][$selected_region_code] = substr(
          "{$label_prefix} {$region_name}",
          0,
        CloudConfigInterface::MAX_NAME_LENGTH - 1
        );
        $edit[$i]['name[0][value]'] = $labels[$i][$selected_region_code];

        // Set a selected region code.
        $edit[$i]['field_region'] = $selected_region_code;

        // Set 'field_get_price_list' value.
        $edit[$i]['field_get_price_list[value]'] = array_rand([0, 1]);

        // Get AWS Cloud instance type prices.
        $instance_types[$i][] = aws_cloud_get_instance_types_by_region($edit[$i]['field_get_price_list[value]']);

        // Set cache ker for AWS Cloud instance type prices.
        $cache_key = _aws_cloud_get_instance_type_cache_key_by_region($edit[$i]['field_get_price_list[value]']);

        // Delete cache of AWS Cloud instance type Prices.
        \Drupal::cache()->delete($cache_key);

        $this->drupalGet("/admin/structure/cloud_config/{$num}/edit");
        $this->submitForm(
          $edit[$i],
          $this->t('Save')->render()
        );

        // Increment $num as is the CloudConfig entity ID in the path .
        $num++;

        // NOTE: Not refactored due to the error message,
        // An error occurred: Cannot read credentials from /var/www/html/web/
        // sites/simpletest/85990801/private/aws_cloud/.aws/cloud_config_xxx.ini.
        $this->assertErrorMessage();
        $this->assertSession()
          ->pageTextNotContains($this->t('Warning message'));

        $this->assertSession()
          ->pageTextContains($labels[$i][$selected_region_code]);

        // Make sure listing for '/admin/structure/cloud_config'.
        $this->drupalGet('/admin/structure/cloud_config');
        $this->assertNoErrorMessage();

        foreach ($labels[$i] ?: [] as $region_code => $label) {
          $this->assertSession()->linkExists($label);
        }

        // Make sure listing for '/clouds'.
        $this->drupalGet('/clouds');
        $this->assertNoErrorMessage();

        foreach ($labels[$i] ?: [] as $region_code => $label) {
          $this->assertSession()->linkExists($label);
        }

        // Make sure listing for '/admin/config/services/cloud/aws_cloud'.
        $this->drupalGet('/admin/config/services/cloud/aws_cloud');
        $this->assertNoErrorMessage();

        foreach ($labels[$i] ?: [] as $region_code => $label) {
          $this->assertSession()->linkExists($label);
        }

        // Make sure listing of AWS Cloud instance type prices.
        foreach ($selected_region_codes[$i] ?: [] as $region_code) {
          $cloud_context = aws_cloud_form_cloud_config_aws_cloud_add_form_create_cloud_context(
            $add[$i]['name[0][value]'],
            $region_code
          );

          $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/instance_type_price");
          $this->assertNoErrorMessage();
          $this->assertSession()->pageTextContains($this->t('AWS Cloud instance type prices'));
          foreach ($instance_types[$i] ?: [] as $instance_type) {
            foreach ($instance_type ?: [] as $instance_type_data) {
              $parts = explode(':', $instance_type_data);

              $edit[$i]['field_get_price_list[value]'] === 1
                ? $this->assertSession()->pageTextContains($parts[0])
                : $this->assertSession()->pageTextNotContains($parts[0]);
            }
          }
        }
      }
    }

    // --------------------------------------.
    // 3. Delete AWS Cloud service providers .
    // --------------------------------------.
    // Ditto. This is CloudConfig test case, so we do not require default
    // $this->cloudContext, which has been already deleted in this setUp().
    // The entity number of $this->cloudContext was '1'.  Therefore, the entity
    // number starts from '2', not '1', here.
    for ($i = 0, $num = 2; $i < self::AWS_CLOUD_CONFIG_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/admin/structure/cloud_config/{$num}/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests deleting cloud service provider (CloudConfig) with bulk operation.
   *
   * @throws \Exception
   */
  public function testCloudConfigBulk(): void {
    $this->runTestEntityBulk('aws_cloud');
  }

  /**
   * Tests Redirect for cloud service provider (CloudConfig) information.
   */
  public function testCloudConfigRedirect(): void {
    try {
      $this->repeatTestCloudConfigRedirect(self::AWS_CLOUD_CONFIG_REPEAT_COUNT);
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Tests AWS menu based on cloud service provider.
   *
   * @throws \Exception
   */
  public function testAwsMenu(): void {

    $this->drupalPlaceBlock('system_menu_block:main', [
      'region' => 'header',
      'theme' => 'claro',
    ]);

    // List AWS Cloud service providers for Amazon EC2.
    $this->drupalGet('/admin/structure/cloud_config');
    $this->assertNoErrorMessage();

    // Add a new form data for the CloudConfig entity.
    $add = $this->createCloudConfigTestFormData(self::AWS_CLOUD_CONFIG_MENU_REPEAT_COUNT);

    // Add random VPC information (Server-side).
    $vpcs = $this->createVpcsRandomTestFormData();
    $subnets = $this->createSubnetsRandomTestFormData();
    $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);

    // Add random image information (Server-side).
    $images = $this->createImagesRandomTestFormData();
    $this->updateImagesMockData($images);

    // Get all regions.
    $regions = \Drupal::service('aws_cloud.ec2')->getRegions();

    // Pick up the regions using CloudConfigInterface::MAX_REGION_COUNT.
    $remaining_regions = array_intersect_key($regions, array_flip(
      array_rand($regions, CloudConfigInterface::MAX_REGION_COUNT)
    ));

    for ($labels = [], $selected_region_codes = [],
      $i = 0; $i < self::AWS_CLOUD_CONFIG_REPEAT_COUNT; $i++) {

      if (empty($add[$i])) {
        continue;
      }

      $label_prefix = $add[$i]['name[0][value]'];
      $add[$i]['regions[]'] = [];

      // Select regions randomly based on the available regions by getting diff
      // with the previous selected regions.  The number of regions to select is
      // different in each random test case.
      $remaining_regions = array_diff_key($remaining_regions, array_flip(
        !empty($selected_region_codes[$i - 1])
          ? $selected_region_codes[$i - 1] : []
      ));

      // Pick up the regions by up to:
      // count($regions) - self::AWS_CLOUD_CONFIG_REPEAT_COUNT + $i + 1.
      $selected_region_codes[] = (array) array_rand(
        $remaining_regions,
        random_int(1, count($remaining_regions) - self::AWS_CLOUD_CONFIG_REPEAT_COUNT + $i + 1)
      );

      foreach ($selected_region_codes[$i] ?: [] as $selected_region_code) {
        // The length of the cloud service provider name limited up to
        // CloudConfigInterface::MAX_NAME_LENGTH characters due to the
        // consideration of a suffix by adding a UUID to deploy multiple Cloud
        // Orchestrator instances.  Therefore, we shorten the AWS region name
        // from e.g. `US East (N. Virgina)` to `(N.Virginia)` to keep the
        // location name instead of using both region and location name.
        $region_name = preg_replace('/^.*(\(.*\))/', '$1', $regions[$selected_region_code]);
        $labels[$i][$selected_region_code] = substr(
          "{$label_prefix} {$region_name}",
          0, CloudConfigInterface::MAX_NAME_LENGTH - 1
        );

        // Set a selected region code.
        $add[$i]['regions[]'][] = $selected_region_code;
      }

      // These variables below are filled in and sent from the form to add an
      // AWS Cloud service provider on a browser.  Therefore, we do not use the
      // auto-generated test values by createCloudConfigTestFormData.
      // field_region is used for each AWS Cloud service provider edit form.
      unset(
        $add[$i]['field_region']
      );

      // Set 'field_get_price_list' value.
      $add[$i]['field_get_price_list[value]'] = array_rand([0, 1]);

      // Set default security group for IAM permission check.
      $this->addSecurityGroupMockData('default', $this->getRandomSecurityGroupDescription(), $this->getRandomId());

      $this->drupalGet('/admin/structure/cloud_config/add');
      $this->assertNoErrorMessage();

      $this->drupalGet('/admin/structure/cloud_config/add');
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );

      foreach ($selected_region_codes[$i] ?: [] as $selected_region_code) {
        // We need to grant the 'view <CLOUD_CONTEXT>' permission.
        // Note: grantPermissions() can be called after "Save", and will be in
        // effect later on the refreshed list.
        $this->grantPermissions(
          Role::load(RoleInterface::AUTHENTICATED_ID), [
            'view ' . aws_cloud_form_cloud_config_aws_cloud_add_form_create_cloud_context(
              $label_prefix,
              $selected_region_code
            ),
          ]
        );
      }

      // NOTE: We do not use assertNoErrorMessage(), since it displays
      // "The module Google Applications is invalid. Enable the module.".
      // Thus, asserting any other error messages not found.
      $this->assertErrorMessage();
      $this->assertGoogleAppErrorOnly();

      $this->assertSession()->pageTextContains($this->t('Status message'));
      $this->assertSession()
        ->pageTextContains($this->t('Creating cloud service provider was performed successfully.'));

      // Make sure listing for '/admin/structure/cloud_config'.
      $this->drupalGet('/admin/structure/cloud_config');
      $this->assertNoErrorMessage();

      foreach ($labels[$i] ?: [] as $selected_region_code => $label) {
        $this->assertSession()->linkExists($label);
      }

      // Make sure listing for '/clouds'.
      $this->drupalGet('/clouds');
      $this->assertNoErrorMessage();

      foreach ($labels[$i] ?: [] as $selected_region_code => $label) {
        // Verify AWS parent menu exist or not.
        $this->assertSession()->linkExists($this->t('AWS'));

        $cloud_context = aws_cloud_form_cloud_config_aws_cloud_add_form_create_cloud_context(
          $add[$i]['name[0][value]'],
          $selected_region_code
        );
        // Check that menu is existing or not.
        $this->assertSession()->linkByHrefExists("/clouds/aws_cloud/{$cloud_context}/instance");

        $this->clickLink($label);
        $this->assertNoErrorMessage();
      }
    }

    // Delete CloudConfig entities.
    // Ditto. This is CloudConfig test case, so we do not require default
    // $this->cloudContext, which has been already deleted in this setUp().
    // The entity number of $this->cloudContext was '1'.  Therefore, the entity
    // number starts from '2', not '1', here.
    for ($i = 0, $num = 2; $i < self::AWS_CLOUD_CONFIG_MENU_REPEAT_COUNT; $i++) {
      foreach ($selected_region_codes[$i] ?: [] as $selected_region_code) {
        $this->drupalGet("/admin/structure/cloud_config/{$num}/delete");
        $this->submitForm(
          [],
          $this->t('Delete')->render()
        );

        // Increment $num as the CloudConfig entity in the path.
        $num++;
      }

      // Verify AWS Cloud parent menu.
      $i === self::AWS_CLOUD_CONFIG_MENU_REPEAT_COUNT - 1
        ? $this->assertSession()->linkNotExistsExact('AWS')
        : $this->assertSession()->linkExistsExact('AWS');
    }

    // Verify AWS dropdown menu does not exist.
    for ($i = 0; $i < self::AWS_CLOUD_CONFIG_MENU_REPEAT_COUNT; $i++) {
      foreach ($selected_region_codes[$i] ?: [] as $selected_region_code) {
        $this->assertSession()->linkNotExistsExact($labels[$i][$selected_region_code]);
      }
    }

    // Verify first level menu AWS is removed or not.
    $this->assertSession()->linkNotExistsExact($this->t('AWS'));
    $this->assertSession()->linkExistsExact($this->t('Cloud service providers'));
    $this->assertSession()->linkExistsExact($this->t('Add cloud service provider'));
  }

}
