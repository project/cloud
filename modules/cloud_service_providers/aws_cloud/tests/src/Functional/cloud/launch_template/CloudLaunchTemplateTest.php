<?php

namespace Drupal\Tests\aws_cloud\Functional\cloud\launch_template;

use Drupal\Tests\aws_cloud\Functional\AwsCloudTestBase;
use Drupal\Tests\cloud\Traits\CloudLaunchTemplateTrait;
use Drupal\aws_cloud\Entity\Ec2\Image;
use Drupal\aws_cloud\Entity\Ec2\KeyPair;
use Drupal\aws_cloud\Entity\Ec2\SecurityGroup;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;

/**
 * Tests cloud launch templates (CloudLaunchTemplate).
 *
 * @group Cloud
 */
class CloudLaunchTemplateTest extends AwsCloudTestBase {

  use CloudLaunchTemplateTrait;

  public const CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT = 2;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'cloud',
    'aws_cloud',
  ];

  /**
   * The generic login user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser;

  /**
   * A user with administrative permissions.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'add cloud server templates',
      'list cloud server template',
      'view any published cloud server templates',
      'edit any cloud server templates',
      'delete any cloud server templates',
      'access cloud server template revisions',
      'revert all cloud server template revisions',
      'delete all cloud server template revisions',

      'add aws cloud image',
      'list aws cloud image',
      'view any aws cloud image',
      'edit any aws cloud image',
      'delete any aws cloud image',
      'list aws cloud key pair',
      'view any aws cloud key pair',
      'list aws cloud security group',
      'view any aws cloud security group',
      'list aws cloud instance',
      'view own aws cloud instance',
      'launch approved cloud server template',
    ];
  }

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {

    parent::setUp();

    $this->webUser = $this->createLoginUser();
    $perms = [
      'approve launch aws cloud instance',
      'launch cloud server template',
    ];
    $this->adminUser = $this->createLoginUser($perms);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'security_group_id' => 'sg-' . $this->getRandomId(),
      'security_group_name' => 'default',
      'error_code' => Ec2ServiceInterface::DRY_RUN_OPERATION,
      'create_time' => date('c'),
    ];
  }

  /**
   * Create cloud context.
   *
   * @param string $bundle
   *   The cloud service provide bundle type ('aws_cloud').
   *
   * @return \Drupal\cloud\Entity\CloudConfig
   *   The cloud service provider (CloudConfig) entity.
   *
   * @throws \Exception
   */
  protected function createCloudContext($bundle = 'aws_cloud'): CloudContentEntityBase {
    $cloud_context = parent::createCloudContext($bundle);

    // Get AWS Cloud instance type prices.
    aws_cloud_get_instance_types($this->cloudContext);

    return $cloud_context;
  }

  /**
   * Tests CRUD for server_template information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testCloudLaunchTemplate(): void {

    $cloud_context = $this->cloudContext;

    $image = $this->createImageTestEntity(Image::class, 0, $image_id = '', $cloud_context);
    $image_id = $image->getImageId();

    // Make sure if the image entity is created or not.
    $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($image->getName());
    $this->assertSession()->pageTextContains($image->getImageId());

    $this->createSecurityGroupTestEntity(SecurityGroup::class, 0, '', '', '', $this->cloudContext);
    $this->createKeyPairTestEntity(KeyPair::class, 0, '', '', $this->cloudContext);

    // List cloud launch template for AWS.
    $this->drupalGet("/clouds/design/server_template/$cloud_context");
    $this->assertNoErrorMessage();

    $vpcs = $this->createVpcsRandomTestFormData();
    $subnets = $this->createSubnetsRandomTestFormData();
    $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);

    // IAM roles.
    $iam_roles = $this->createIamRolesRandomTestFormData();
    $this->updateIamRolesMockData($iam_roles);

    // Add a new server_template information.
    $add = $this->createLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {

      $add[$i]['field_image_id'] = $image_id;

      $vpc_index = array_rand($vpcs);
      $add[$i]['field_vpc'] = $vpcs[$vpc_index]['VpcId'];
      $vpc_name = $this->getNameFromArray($vpcs, $vpc_index, 'VpcId');

      $subnet_index = array_rand($subnets);
      $add[$i]['field_subnet'] = $subnets[$subnet_index]['SubnetId'];
      $subnet_name = $this->getNameFromArray($subnets, $subnet_index, 'SubnetId');

      $iam_role_index = array_rand($iam_roles);
      $add[$i]['field_iam_role'] = $iam_roles[$iam_role_index]['Arn'];
      $iam_role_name = $iam_roles[$iam_role_index]['InstanceProfileName'];

      $this->drupalGet("/clouds/design/server_template/$cloud_context/aws_cloud/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      $this->assertSession()->pageTextContains($add[$i]['name[0][value]']);
      $this->assertSession()->pageTextContains($vpc_name);
      $this->assertSession()->pageTextContains($subnet_name);
      $this->assertSession()->pageTextContains($iam_role_name);
      $this->assertSession()->pageTextContains($add[$i]['field_tags[0][item_key]']);
      $this->assertSession()->pageTextContains($add[$i]['field_tags[0][item_value]']);
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        )
      );

      // Make sure listing.
      $this->drupalGet("/clouds/design/server_template/$cloud_context");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $i + 1; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name[0][value]']);
      }
    }

    // Edit case.
    $edit = $this->createLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++, $num++) {

      // Name cannot be changed.
      $edit[$i]['name[0][value]'] = $add[$i]['name[0][value]'];

      $edit[$i]['field_image_id'] = $image_id;

      $vpc_index = array_rand($vpcs);
      $edit[$i]['field_vpc'] = $vpcs[$vpc_index]['VpcId'];
      $vpc_name = $this->getNameFromArray($vpcs, $vpc_index, 'VpcId');

      $subnet_index = array_rand($subnets);
      $edit[$i]['field_subnet'] = $subnets[$subnet_index]['SubnetId'];
      $subnet_name = $this->getNameFromArray($subnets, $subnet_index, 'SubnetId');

      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $edit[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->assertSession()->pageTextContains($edit[$i]['name[0][value]']);
      $this->assertSession()->pageTextContains($vpc_name);
      $this->assertSession()->pageTextContains($subnet_name);

      // Make sure listing.
      $this->drupalGet("/clouds/design/server_template/$cloud_context");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$j]['name[0][value]']);
      }
    }

    // Delete server_template Items.
    for ($i = 0, $num = 1; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/design/server_template/$cloud_context");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextNotContains($edit[$j]['name[0][value]']);
      }
    }

    // Click 'Refresh'.
    // @todo Need tests for the entities from the mock objects.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated launch templates.'));
    $this->assertNoErrorMessage();
  }

  /**
   * Tests CRUD for server_template revision information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testCloudLaunchTemplateRevision(): void {
    $cloud_context = $this->cloudContext;

    $image = $this->createImageTestEntity(Image::class, 0, $image_id = '', $cloud_context);
    $image_id = $image->getImageId();

    // Make sure if the image entity is created or not.
    $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($image->getName());
    $this->assertSession()->pageTextContains($image->getImageId());

    $this->createSecurityGroupTestEntity(SecurityGroup::class, 0, '', '', '', $cloud_context);
    $this->createKeyPairTestEntity(KeyPair::class, 0, '', '', $cloud_context);

    $vpcs = $this->createVpcsRandomTestFormData();
    $subnets = $this->createSubnetsRandomTestFormData();
    $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);

    // IAM roles.
    $iam_roles = $this->createIamRolesRandomTestFormData();
    $this->updateIamRolesMockData($iam_roles);

    // Create a cloud launch template.
    $add = $this->createLaunchTemplateTestFormData();

    $add[0]['field_image_id'] = $image_id;

    $vpc_index = array_rand($vpcs);
    $add[0]['field_vpc'] = $vpcs[$vpc_index]['VpcId'];

    $subnet_index = array_rand($subnets);
    $add[0]['field_subnet'] = $subnets[$subnet_index]['SubnetId'];

    $this->drupalGet("/clouds/design/server_template/$cloud_context/aws_cloud/add");
    $this->submitForm(
      $add[0],
      $this->t('Save')->render()
    );
    $this->assertNoErrorMessage();

    $t_args = [
      '@type' => 'launch template',
      '%label' => $add[0]['name[0][value]'],
    ];
    $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

    // Make sure listing revisions.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/revisions");
    $this->assertNoErrorMessage();

    // Create a new revision.
    $edit = $add[0];
    $old_description = $edit['field_description[0][value]'];
    $revision_desc = $this->random->name(32, TRUE);
    $edit['field_description[0][value]'] = $revision_desc;
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/edit");
    $this->submitForm(
      $edit,
      $this->t('Save')->render()
    );
    $this->assertNoErrorMessage();

    $t_args = [
      '@type' => 'launch template',
      '%label' => $edit['name[0][value]'],
    ];
    $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

    // Make sure listing revisions.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/revisions");
    $this->assertNoErrorMessage();
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/1/revisions/1/view");
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/1/revisions/1/revert");
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/1/revisions/1/delete");

    // View the revision.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/revisions/1/view");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($old_description);

    // Revert the revision.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/revisions/1/revert");
    $this->submitForm(
      [],
      $this->t('Revert')->render()
    );
    $this->assertNoErrorMessage();
    // Check the string: 'The launch template %title has been reverted to
    // the revision from %revision-date.'.
    $this->assertSession()->pageTextContains($this->t('The launch template @name has been reverted to the revision from', [
      '@name' => $edit['name[0][value]'],
    ]));
    // The new revision is created.
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/1/revisions/2/view");

    // Delete the revision.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/revisions/1/delete");
    $this->submitForm(
      [],
      $this->t('Delete')->render()
    );
    $this->assertNoErrorMessage();

    // Check the part of the string: 'Revision from %revision-date of launch
    // template @label has been deleted.'.
    $t_args = [
      '@type' => 'launch template',
      '@label' => $edit['name[0][value]'],
    ];
    $this->assertSession()->pageTextContains(strip_tags($this->t('of @type @label has been deleted.', $t_args)));

    // The revision is deleted.
    $this->assertSession()->linkByHrefNotExists("server_template/$cloud_context/1/revisions/1/view");

    // Test copy function.
    $this->drupalGet("/clouds/design/server_template/{$cloud_context}");
    $this->clickLink($add[0]['name[0][value]']);
    $this->clickLink('Copy');
    $copy_url = $this->getUrl();
    $this->drupalGet($copy_url);
    $this->submitForm(
      [],
      $this->t('Copy')->render()
    );
    $this->assertNoErrorMessage();

    $t_args = [
      '@type' => 'launch template',
      '%label' => "copy_of_{$add[0]['name[0][value]']}",
    ];
    $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
    $this->assertSession()->pageTextContains("copy_of_{$add[0]['name[0][value]']}");
  }

  /**
   * Tests CRUD for server_template information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  public function testCloudLaunchTemplateCopy(): void {
    $cloud_context = $this->cloudContext;

    $image = $this->createImageTestEntity(Image::class, 0, $image_id = '', $cloud_context);
    $image_id = $image->getImageId();

    // Make sure if the image entity is created or not.
    $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($image->getName());
    $this->assertSession()->pageTextContains($image->getImageId());

    $this->createSecurityGroupTestEntity(SecurityGroup::class, 0, '', '', '', $cloud_context);
    $this->createKeyPairTestEntity(KeyPair::class, 0, '', '', $cloud_context);

    $vpcs = $this->createVpcsRandomTestFormData();
    $subnets = $this->createSubnetsRandomTestFormData();
    $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);

    // IAM roles.
    $iam_roles = $this->createIamRolesRandomTestFormData();
    $this->updateIamRolesMockData($iam_roles);

    // Add a new server_template information.
    $add = $this->createLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    $copy = $this->createLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);

    $num = 1;
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {

      $add[$i]['field_image_id'] = $image_id;

      $vpc_index = array_rand($vpcs);
      $add[$i]['field_vpc'] = $vpcs[$vpc_index]['VpcId'];

      $subnet_index = array_rand($subnets);
      $add[$i]['field_subnet'] = $subnets[$subnet_index]['SubnetId'];

      $iam_role_index = array_rand($iam_roles);
      $add[$i]['field_iam_role'] = $iam_roles[$iam_role_index]['Arn'];

      $this->drupalGet("/clouds/design/server_template/$cloud_context/aws_cloud/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Access copy page.
      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/copy");
      $this->assertSession()->pageTextContains('Copy launch template');
      $this->assertSession()->fieldValueEquals('name[0][value]', "copy_of_{$add[$i]['name[0][value]']}");

      // Submit copy.
      $copy[$i]['field_image_id'] = $image_id;
      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/copy");
      $this->submitForm(
        $copy[$i],
        $this->t('Copy')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $copy[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
      $this->assertSession()->pageTextNotContains($add[$i]['name[0][value]']);

      // Access edit page.
      $num++;
      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/edit");

      foreach ($copy[$i] ?: [] as $key => $value) {
        if (strpos($key, 'field_tags') === FALSE) {
          $this->assertSession()->fieldValueEquals($key, $value);
        }
      }

      $num++;

    }
  }

  /**
   * Tests launch of instances from server_template by admin, before approval.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function runLaunchTemplateLaunchBeforeApprovalTest(): void {
    $cloud_context = $this->cloudContext;
    $module_name = 'aws_cloud';

    $image = $this->createImageTestEntity(Image::class, 0, $image_id = '', $cloud_context);
    $image_id = $image->getImageId();

    // Make sure if the image entity is created or not.
    $this->drupalLogin($this->webUser);
    $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($image->getName());
    $this->assertSession()->pageTextContains($image->getImageId());

    $this->createSecurityGroupTestEntity(SecurityGroup::class, 0, '', '', '', $cloud_context);
    $this->createKeyPairTestEntity(KeyPair::class, 0, '', '', $cloud_context);

    $vpcs = $this->createVpcsRandomTestFormData();
    $subnets = $this->createSubnetsRandomTestFormData();
    $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);
    $vpc_index = array_rand($vpcs);
    $subnet_index = array_rand($subnets);

    // IAM roles.
    $iam_roles = $this->createIamRolesRandomTestFormData();
    $iam_role_index = array_rand($iam_roles);
    $this->updateIamRolesMockData($iam_roles);

    // Create test data.
    $form_data = $this->createLaunchTemplateTestFormData();
    $form_data[0]['field_image_id'] = $image_id;
    $form_data[0]['field_vpc'] = $vpcs[$vpc_index]['VpcId'];
    $form_data[0]['field_subnet'] = $subnets[$subnet_index]['SubnetId'];
    $form_data[0]['field_iam_role'] = $iam_roles[$iam_role_index]['Arn'];
    $form_data[0]['field_workflow_status'] = CloudLaunchTemplateInterface::DRAFT;
    $edit = $form_data[0];
    $list_url = "/clouds/design/server_template/$cloud_context";
    $view_url = "/clouds/design/server_template/$cloud_context/1";

    // 1. Login as admin and create launch template.
    $this->drupalLogin($this->adminUser);
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_ADD, $module_name);
    // Validate the status is Draft.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT, TRUE);

    // 2. The admin user launches an instance from the launch template.
    $this->drupalGet("$view_url/launch");
    $this->submitForm(
      [],
      $this->t('Launch')->render()
    );
    $this->assertNoErrorMessage();

    // After launching the instance.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT, TRUE);

    // 3. Delete the template.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/delete");
    $this->submitForm(
      [],
      $this->t('Delete')->render()
    );
    $this->assertNoErrorMessage();
  }

  /**
   * Tests CRUD for server_template information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testCloudLaunchTemplateWorkflowUpdate(): void {
    $cloud_context = $this->cloudContext;
    $module_name = 'aws_cloud';

    $image = $this->createImageTestEntity(Image::class, 0, $image_id = '', $cloud_context);
    $image_id = $image->getImageId();

    // Make sure if the image entity is created or not.
    $this->drupalLogin($this->webUser);
    $this->drupalGet("/clouds/aws_cloud/{$cloud_context}/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($image->getName());
    $this->assertSession()->pageTextContains($image->getImageId());

    $this->createSecurityGroupTestEntity(SecurityGroup::class, 0, '', '', '', $cloud_context);
    $this->createKeyPairTestEntity(KeyPair::class, 0, '', '', $cloud_context);

    $vpcs = $this->createVpcsRandomTestFormData();
    $subnets = $this->createSubnetsRandomTestFormData();
    $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);
    $vpc_index = array_rand($vpcs);
    $subnet_index = array_rand($subnets);

    // IAM roles.
    $iam_roles = $this->createIamRolesRandomTestFormData();
    $iam_role_index = array_rand($iam_roles);
    $this->updateIamRolesMockData($iam_roles);

    // Create test data.
    $form_data = $this->createLaunchTemplateTestFormData();
    $form_data[0]['field_image_id'] = $image_id;
    $form_data[0]['field_vpc'] = $vpcs[$vpc_index]['VpcId'];
    $form_data[0]['field_subnet'] = $subnets[$subnet_index]['SubnetId'];
    $form_data[0]['field_iam_role'] = $iam_roles[$iam_role_index]['Arn'];
    $form_data[0]['field_workflow_status'] = CloudLaunchTemplateInterface::DRAFT;
    $edit = $form_data[0];
    $list_url = "/clouds/design/server_template/$cloud_context";
    $view_url = "/clouds/design/server_template/$cloud_context/1";

    // 1. A generic user creates a launch template.
    $this->drupalLogin($this->webUser);
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_ADD, $module_name);
    // Validate the status is Draft.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT);

    // 2. The generic user changes the launch template status from 'Draft' to
    // 'Review'.
    $this->drupalLogin($this->webUser);
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::REVIEW;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name);

    // Validate the status is Review.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::REVIEW);

    // 3. The generic user changes the launch template status from 'Review' to
    // 'Draft'.
    $this->drupalLogin($this->webUser);
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::DRAFT;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name);
    // Validate the status is still Draft.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT);

    // 4. The generic user changes the launch template status from 'Draft' to
    // 'Review'.
    $this->drupalLogin($this->webUser);
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::REVIEW;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name);

    // Validate the status is still REVIEW.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::REVIEW);

    // Make sure that the generic user cannot approve.
    $this->drupalGet($view_url);
    $this->assertSession()->linkNotExistsExact($this->t('Approve'));
    $this->assertSession()->linkByHrefNotExists("server_template/$cloud_context/1/approve");

    // 5. The IT admin changes the Launch Template status from 'Review' to
    // 'Approved'.
    $this->drupalLogin($this->adminUser);

    // Make sure that the admin user can approve and launch.
    $this->drupalGet($view_url);
    $this->assertSession()->linkExistsExact($this->t('Approve'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/1/approve");
    $this->assertSession()->linkExistsExact($this->t('Launch'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/1/launch");
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::APPROVED;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name);

    // Validate the status is still 'Approved'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::APPROVED, TRUE);

    // Validate the 'Launch' tab is shown in the generic user's launch template.
    $this->drupalLogin($this->webUser);
    $this->drupalGet($view_url);
    $this->assertSession()->linkExistsExact($this->t('Launch'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/1/launch");

    // 6. The generic user saves the launch template as it is w/o any change.
    $this->drupalLogin($this->webUser);
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name);

    // Validate the status is still 'Approved'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::APPROVED);

    // 7. The generic user saves the launch template w/ some changes.
    $this->drupalLogin($this->webUser);
    unset($vpcs[$vpc_index]);
    $vpc_index = array_rand($vpcs);
    $edit['field_vpc'] = $vpcs[$vpc_index]['VpcId'];
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name);
    // Validate the status is still DRAFT.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT);

    // 8.The generic user changes the launch template status from 'Draft' to
    // 'Review'.
    $this->drupalLogin($this->webUser);
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::REVIEW;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name);

    // Validate the status is still 'Review'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::REVIEW);

    // Make sure that Generic user cannot approve or launch.
    $this->drupalGet($view_url);
    $this->assertSession()->linkNotExistsExact($this->t('Approve'));
    $this->assertSession()->linkByHrefNotExists("server_template/$cloud_context/1/approve");
    $this->assertSession()->linkNotExistsExact($this->t('Launch'));
    $this->assertSession()->linkByHrefNotExists("server_template/$cloud_context/1/launch");

    // 9. The IT admin changes the launch template status from 'Review' to
    // 'Approved'.
    $this->drupalLogin($this->adminUser);

    // Make sure that the Admin user can Approve.
    $this->drupalGet($view_url);
    $this->assertSession()->linkExistsExact($this->t('Approve'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/1/approve");
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::APPROVED;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name);

    // Validate the status is still 'Approved'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::APPROVED, TRUE);

    // Validate the 'Launch' tab is shown in the generic user's launch template.
    $this->drupalLogin($this->webUser);
    $this->drupalGet($view_url);
    $this->assertSession()->linkExistsExact($this->t('Launch'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/1/launch");

    // 10. The generic user launches an instance from the launch template.
    $this->drupalLogin($this->webUser);
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/launch");
    $this->submitForm(
      [],
      $this->t('Launch')->render()
    );
    $this->assertNoErrorMessage();

    // Validate the status is 'Draft' (List and canonical view) after launching
    // the instance.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT);
  }

}
