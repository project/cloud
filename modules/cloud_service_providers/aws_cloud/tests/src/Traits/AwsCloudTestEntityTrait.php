<?php

namespace Drupal\Tests\aws_cloud\Traits;

use Drupal\Tests\cloud\Functional\Utils;
use Drupal\aws_cloud\Entity\Ec2\ElasticIp;
use Drupal\aws_cloud\Entity\Ec2\Image;
use Drupal\aws_cloud\Entity\Vpc\CarrierGateway;
use Drupal\aws_cloud\Entity\Vpc\InternetGateway;
use Drupal\aws_cloud\Entity\Vpc\Subnet;
use Drupal\aws_cloud\Entity\Vpc\TransitGateway;
use Drupal\aws_cloud\Entity\Vpc\Vpc;
use Drupal\aws_cloud\Entity\Vpc\VpcPeeringConnection;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Service\Iam\IamServiceInterface;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Entity\CloudLaunchTemplate;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;

/**
 * The trait creating test entity for aws cloud testing.
 */
trait AwsCloudTestEntityTrait {

  /**
   * Create an AWS Cloud Config test entity.
   *
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return object
   *   The Cloud Config entity.
   *
   * @throws \Exception
   */
  protected function createCloudConfigTestEntity($cloud_context) {
    $random = $this->random;
    $num = random_int(1, 2);
    $this->initIamPermissions($cloud_context);
    return $this->createTestEntity(CloudConfig::class, [
      'type'                    => 'aws_cloud',
      'cloud_context'           => $cloud_context,
      'label'                   => "Amazon EC2 US West ($num) - {$random->name(8, TRUE)}",
      'field_description'       => "{$this->cloudRegion}: " . date('Y/m/d H:i:s - D M j G:i:s T Y') . $random->string(64, TRUE),
      'field_region'            => "us-west-$num",
      'field_account_id'        => $random->name(16, TRUE),
      'field_access_key'        => $random->name(20, TRUE),
      'field_secret_key'        => $random->name(40, TRUE),
      'field_api_endpoint_uri'  => "https://ec2.us-west-{$num}.amazonaws.com",
      'field_api_version'       => 'latest',
      'field_image_upload_url'  => "https://ec2.us-west-{$num}.amazonaws.com",
      'field_x_509_certificate' => $random->string(255, TRUE),
      'field_automatically_assign_vpc' => '0',
      'field_get_price_list' => '0',
    ]);
  }

  /**
   * Create an AWS Cloud instance test entity.
   *
   * @param string $class
   *   The instance class.
   * @param int $num
   *   The index.
   * @param array $regions
   *   The regions.
   * @param string $public_ip
   *   The public IP.
   * @param string $instance_name
   *   The instance name.
   * @param string $instance_id
   *   The instance ID.
   * @param string $instance_state
   *   The instance state.
   *
   * @return object
   *   The instance entity.
   *
   * @throws \Exception
   */
  protected function createInstanceTestEntity($class, $num = 0, array $regions = [], $public_ip = NULL, $instance_name = '', $instance_id = '', $instance_state = 'running') {
    if (!isset($public_ip)) {
      $public_ip = Utils::getRandomPublicIp();
    }
    $private_ip = Utils::getRandomPrivateIp();
    $region = $regions[array_rand($regions)];

    return $this->createTestEntity($class, [
      'cloud_context' => $this->cloudContext,
      'name' => $instance_name ?: sprintf('instance-entity #%d - %s - %s', $num + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'image_id' => 'ami-' . $this->getRandomId(),
      'key_pair_name' => "key_pair-{$this->random->name(8, TRUE)}",
      'is_monitoring' => 0,
      'availability_zone' => "us-west-$num",
      'security_groups' => "security_group-{$this->random->name(8, TRUE)}",
      'instance_type' => "t$num.small",
      'kernel_id' => 'aki-' . $this->getRandomId(),
      'ramdisk_id' => 'ari-' . $this->getRandomId(),
      'user_data' => "User data #$num: {$this->random->string(64, TRUE)}",
      'account_id' => random_int(100000000000, 999999999999),
      'reservation_id' => 'r-' . $this->getRandomId(),
      'group_name' => $this->random->name(8, TRUE),
      'host_id' => $this->random->name(8, TRUE),
      'affinity' => $this->random->name(8, TRUE),
      'launch_time' => date('c'),
      'security_group_id' => "sg-{$this->getRandomId()}",
      'security_group_name' => $this->random->name(10, TRUE),
      'public_dns_name' => Utils::getPublicDns($region, $public_ip),
      'public_ip_address' => $public_ip,
      'private_dns_name' => Utils::getPrivateDns($region, $private_ip),
      'private_ip_address' => $private_ip,
      'vpc_id' => 'vpc-' . $this->getRandomId(),
      'subnet_id' => 'subnet-' . $this->getRandomId(),
      'reason' => $this->random->string(16, TRUE),
      'instance_id' => $instance_id ?: "i-{$this->getRandomId()}",
      'instance_state' => $instance_state,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an AWS Cloud VPC test entity.
   *
   * @param int $index
   *   The VPC index.
   * @param string $vpc_id
   *   The VPC ID.
   * @param string $vpc_name
   *   The VPC name.
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The VPC entity.
   */
  protected function createVpcTestEntity($index = 0, $vpc_id = '', $vpc_name = '', $cloud_context = ''): CloudContentEntityBase {

    return $this->createTestEntity(Vpc::class, [
      'cloud_context' => $cloud_context ?: $this->cloudContext,
      'vpc_id' => $vpc_id ?: 'vpc-' . $this->getRandomId(),
      'name' => $vpc_name ?: sprintf('vpc-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
    ]);
  }

  /**
   * Create an AWS Cloud VPC peering connection test entity.
   *
   * @param int $index
   *   The VPC peering connection index.
   * @param string $vpc_peering_connection_id
   *   The VPC peering connection ID.
   * @param string $vpc_peering_connection_ame
   *   The VPC peering connection name.
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The VPC entity.
   */
  protected function createVpcPeeringConnectionTestEntity($index = 0, $vpc_peering_connection_id = '', $vpc_peering_connection_ame = '', $cloud_context = ''): CloudContentEntityBase {
    return $this->createTestEntity(VpcPeeringConnection::class, [
      'cloud_context' => $cloud_context ?: $this->cloudContext,
      'vpc_peering_connection_id' => $vpc_peering_connection_id ?: 'pcx-' . $this->getRandomId(),
      'name' => $vpc_peering_connection_ame ?: sprintf('pcx-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
    ]);
  }

  /**
   * Create an AWS Cloud subnet test entity.
   *
   * @param int $index
   *   The Subnet index.
   * @param string $subnet_id
   *   The Subnet ID.
   * @param string $subnet_name
   *   The Subnet name.
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The Subnet entity.
   */
  protected function createSubnetTestEntity($index = 0, $subnet_id = '', $subnet_name = '', $cloud_context = ''): CloudContentEntityBase {

    return $this->createTestEntity(Subnet::class, [
      'cloud_context' => $cloud_context ?: $this->cloudContext,
      'subnet_id' => $subnet_id ?: 'subnet-' . $this->getRandomId(),
      'name' => $subnet_name ?: sprintf('subnet-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'cidr_block'   => Utils::getRandomCidr(),
    ]);
  }

  /**
   * Create an AWS Cloud internet gateway test entity.
   *
   * @param int $index
   *   The internet gateway index.
   * @param string $internet_gateway_id
   *   The internet gateway ID.
   * @param string $internet_gateway_name
   *   The internet gateway name.
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The internet gateway entity.
   */
  protected function createInternetGatewayTestEntity($index = 0, $internet_gateway_id = '', $internet_gateway_name = '', $cloud_context = ''): CloudContentEntityBase {

    return $this->createTestEntity(InternetGateway::class, [
      'cloud_context' => $cloud_context ?: $this->cloudContext,
      'internet_gateway_id' => $internet_gateway_id ?: 'igw-' . $this->getRandomId(),
      'state' => 'attached',
      'vpc_id' => 'vpc-' . $this->getRandomId(),
      'name' => $internet_gateway_name ?: sprintf('internet-gateway-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
    ]);
  }

  /**
   * Create an AWS Cloud carrier gateway test entity.
   *
   * @param int $index
   *   The carrier gateway index.
   * @param string $carrier_gateway_id
   *   The carrier gateway ID.
   * @param string $carrier_gateway_name
   *   The carrier gateway name.
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The carrier gateway entity.
   */
  protected function createCarrierGatewayTestEntity($index = 0, $carrier_gateway_id = '', $carrier_gateway_name = '', $cloud_context = ''): CloudContentEntityBase {

    return $this->createTestEntity(CarrierGateway::class, [
      'cloud_context' => $cloud_context ?: $this->cloudContext,
      'carrier_gateway_id' => $carrier_gateway_id ?: 'igw-' . $this->getRandomId(),
      'state' => 'attached',
      'vpc_id' => 'vpc-' . $this->getRandomId(),
      'name' => $carrier_gateway_name ?: sprintf('carrier-gateway-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
    ]);
  }

  /**
   * Create an AWS Cloud transit gateway test entity.
   *
   * @param int $index
   *   The transit gateway index.
   * @param string $transit_gateway_id
   *   The transit gateway ID.
   * @param string $transit_gateway_name
   *   The transit gateway name.
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The transit gateway entity.
   */
  protected function createTransitGatewayTestEntity($index = 0, $transit_gateway_id = '', $transit_gateway_name = '', $cloud_context = ''): CloudContentEntityBase {

    return $this->createTestEntity(TransitGateway::class, [
      'cloud_context' => $cloud_context ?: $this->cloudContext,
      'transit_gateway_id' => $transit_gateway_id ?: 'igw-' . $this->getRandomId(),
      'state' => 'available',
      'name' => $transit_gateway_name ?: sprintf('transit-gateway-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
    ]);
  }

  /**
   * Create an AWS Cloud network interface test entity.
   *
   * @param string $class
   *   The network interface class.
   * @param int $index
   *   The Index.
   * @param string $network_interface_id
   *   The network interface ID.
   * @param string $network_interface_name
   *   The network interface name.
   * @param string $instance_id
   *   The instance ID.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The network interface entity.
   *
   * @throws \Exception
   */
  protected function createNetworkInterfaceTestEntity($class, $index = 0, $network_interface_id = '', $network_interface_name = '', $instance_id = ''): CloudContentEntityBase {
    $timestamp = time();
    $private_ip = Utils::getRandomPrivateIp();
    $secondary_private_ip = Utils::getRandomPrivateIp();

    return $this->createTestEntity($class, [
      'cloud_context' => $this->cloudContext,
      'name' => $network_interface_name ?: sprintf('eni-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'network_interface_id' => $network_interface_id ?: 'eni-' . $this->getRandomId(),
      'vpc_id' => "vpc-{$this->random->name(8, TRUE)}",
      'mac_address' => NULL,
      'security_groups' => "security_group-{$this->random->name(8, TRUE)}",
      'status' => 'in-use',
      'private_dns' => NULL,
      'primary_private_ip' => $private_ip,
      'secondary_private_ips' => [$secondary_private_ip],
      'attachment_id' => 'attachment-' . $this->getRandomId(),
      'attachment_owner' => NULL,
      'attachment_status' => NULL,
      'owner_id' => random_int(100000000000, 999999999999),
      'association_id' => NULL,
      'secondary_association_id' => NULL,
      'subnet_id' => NULL,
      'description' => NULL,
      'public_ips' => NULL,
      'source_dest_check' => NULL,
      'instance_id' => $instance_id ?: 'i-' . $this->getRandomId(),
      'device_index' => NULL,
      'delete_on_termination' => NULL,
      'allocation_id' => NULL,
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
    ]);
  }

  /**
   * Create an AWS Cloud Elastic IP test entity.
   *
   * @param int $index
   *   The Elastic IP index.
   * @param string $elastic_ip_name
   *   The Elastic IP name.
   * @param string $public_ip
   *   The public IP address.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The Elastic IP entity.
   *
   * @throws \Exception
   */
  protected function createElasticIpTestEntity($index = 0, $elastic_ip_name = '', $public_ip = '', $cloud_context = ''): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity(ElasticIp::class, [
      'cloud_context' => $cloud_context ?: $this->cloudContext,
      'name' => $elastic_ip_name ?: sprintf('eip-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'public_ip' => $public_ip ?: Utils::getRandomPublicIp(),
      'instance_id' => NULL,
      'network_interface_id' => NULL,
      'private_ip_address' => NULL,
      'network_interface_owner' => NULL,
      'allocation_id' => NULL,
      'association_id' => NULL,
      'elastic_ip_type' => 'Public IP',
      'network_border_group' => 'us-west-2',
      'domain' => 'standard',
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
    ]);
  }

  /**
   * Create an AWS Cloud snapshot test entity.
   *
   * @param string $class
   *   The snapshot class.
   * @param int $index
   *   The index.
   * @param string $snapshot_id
   *   The snapshot ID.
   * @param string $snapshot_name
   *   The snapshot name.
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The snapshot entity.
   */
  protected function createSnapshotTestEntity($class, $index = 0, $snapshot_id = '', $snapshot_name = '', $cloud_context = ''): CloudContentEntityBase {
    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'snapshot_id' => $snapshot_id,
      'name' => $snapshot_name ?: sprintf('snapshot-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'created' => time(),
    ]);
  }

  /**
   * Create an AWS Cloud volume test entity.
   *
   * @param string $class
   *   The volume class.
   * @param int $num
   *   The volume number.
   * @param string $volume_id
   *   The volume ID.
   * @param string $volume_name
   *   The volume name.
   * @param string $cloud_context
   *   The Cloud context.
   * @param int $uid
   *   The User ID.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The volume entity.
   */
  protected function createVolumeTestEntity($class, $num, $volume_id, $volume_name, $cloud_context, $uid): CloudContentEntityBase {
    return $this->createTestEntity($class, [
      'cloud_context'     => $cloud_context,
      'volume_id'         => $volume_id,
      'name'              => $volume_name ?: sprintf('volume-entity #%d - %s - %s', $num + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'uid' => $uid,
      'size'              => $num * 10,
      'availability_zone' => "us-west-$num",
      'iops'              => $num * 1000,
      'encrypted'         => $num % 2,
      'volume_type'       => 'io1',
      'volume_status'     => 'Available',
      'state' => 'available',
    ]);
  }

  /**
   * Create an AWS Cloud security group test entity.
   *
   * @param string $class
   *   The security group class.
   * @param int $index
   *   The security group index.
   * @param string $group_id
   *   The security group.
   * @param string $group_name
   *   The security group name.
   * @param string $vpc_id
   *   The VPC ID.
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The security group entity.
   */
  protected function createSecurityGroupTestEntity($class, $index = 0, $group_id = '', $group_name = '', $vpc_id = '', $cloud_context = ''): CloudContentEntityBase {

    $group_name = $group_name ?: sprintf('sg-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(8, TRUE));

    return $this->createTestEntity($class, [
      'name'          => $group_name,
      'group_name'    => $group_name,
      'group_id'      => $group_id ?: 'sg-' . $this->getRandomId(),
      'vpc_id'        => $vpc_id,
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Create an AWS Cloud image test entity.
   *
   * @param string $class
   *   The image class.
   * @param int $index
   *   The image index.
   * @param string $image_id
   *   The image ID.
   * @param string $cloud_context
   *   Tne Cloud context.
   *
   * @return \Drupal\aws_cloud\Entity\Ec2\Image
   *   The image entity.
   */
  protected function createImageTestEntity($class, $index = 0, $image_id = '', $cloud_context = ''): CloudContentEntityBase {
    $name = sprintf('image-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE));
    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'image_id' => $image_id ?: "ami-{$this->getRandomId()}",
      'name' => $name,
      'ami_name' => $name,
      'root_device_type' => 'ebs',
      'created' => time(),
    ]);
  }

  /**
   * Create an AWS Cloud key pair test entity.
   *
   * @param string $class
   *   The key pair class.
   * @param int $index
   *   The key pair index.
   * @param string $key_pair_name
   *   The key pair name.
   * @param string $key_fingerprint
   *   The Fingerprint.
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The key pair entity.
   */
  protected function createKeyPairTestEntity($class, $index = 0, $key_pair_name = '', $key_fingerprint = '', $cloud_context = ''): CloudContentEntityBase {
    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'key_pair_name' => $key_pair_name ?: sprintf('key_pair-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'key_fingerprint' => $key_fingerprint,
      'created' => time(),
    ]);
  }

  /**
   * Create an AWS Cloud Availability Zones entity.
   *
   * @param string $class
   *   The key pair class.
   * @param int $index
   *   The index.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $network_border_group
   *   The network border group.
   * @param string $zone_name
   *   The zone name.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The array of Availability Zone entity.
   */
  protected function createAvailabilityZoneTestEntity($class, $index, $cloud_context = '', $network_border_group = '', $zone_name = ''): CloudContentEntityBase {
    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'group_name' => sprintf('group-#%d-%s-%s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(4, TRUE)),
      'network_border_group' => $network_border_group,
      'zone_id' => 'zone-id-' . $this->random->name(8, TRUE),
      'zone_name' => empty($zone_name) ? 'zone-name-' . $this->random->name(8, TRUE) : $zone_name,
      'created' => time(),
    ]);
  }

  /**
   * Create cloud launch template.
   *
   * @param array $iam_roles
   *   The IAM roles.
   * @param \Drupal\aws_cloud\Entity\Ec2\Image $image
   *   The image entity.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The CloudLaunchTemplate entity.
   */
  protected function createLaunchTemplateTestEntity(
    array $iam_roles,
    Image $image,
    $cloud_context,
  ): CloudContentEntityBase {
    // Create template.
    $template = $this->createTestEntity(CloudLaunchTemplate::class, [
      'cloud_context' => $cloud_context,
      'type' => 'aws_cloud',
      'name' => 'test_template1',
    ]);

    $template->field_test_only->value = '1';
    $template->field_max_count->value = 1;
    $template->field_min_count->value = 1;
    $template->field_monitoring->value = '0';
    $template->field_instance_type->value = 't3.micro';
    $template->field_image_id->value = $image->getImageId()
      ?: "ami-{$this->getRandomId()}";

    // Set the IAM role which can be NULL or something.
    $iam_role_index = array_rand($iam_roles);
    if ($iam_role_index === 0) {
      $template->field_iam_role = NULL;
    }
    else {

      // @todo Is this correct? Make sure if field_iam_role->value is correct or not
      $template->field_iam_role = str_replace(
        'role/',
        'instance-profile/',
        $iam_roles[$iam_role_index]['Arn']
      );
    }

    $template->field_workflow_status->value = CloudLaunchTemplateInterface::APPROVED;

    $template->save();
    return $template;
  }

  /**
   * Insert all IAM permissions into DB.
   *
   * @param string $cloud_context
   *   The cloud context to initialize.
   */
  protected function initIamPermissions(string $cloud_context): void {
    $access = \Drupal::service('aws_cloud.access');
    $access->deletePermissions($cloud_context);
    $operations = array_merge_recursive(Ec2ServiceInterface::PERMISSION_TO_AWS_API, IamServiceInterface::PERMISSION_TO_AWS_API);
    foreach ($operations as $perm => $operation) {
      if (is_array($operation)) {
        foreach ($operation as $op) {
          $access->insertPermission($cloud_context, $op, $perm);
        }
        continue;
      }
      $access->insertPermission($cloud_context, $operation, $perm);
    }
  }

  /**
   * Delete Iam permission and clear operation cache.
   *
   * @param string $cloud_context
   *   The cloud context to clear.
   * @param string $command
   *   The Iam permission to clear.
   */
  protected function deleteIamPermission(string $cloud_context, string $command): void {
    $access = \Drupal::service('aws_cloud.access');
    $access->deletePermissions($cloud_context, $command);
    \Drupal::service('cloud.cache')->clearOperationAccessCache($cloud_context);
  }

}
