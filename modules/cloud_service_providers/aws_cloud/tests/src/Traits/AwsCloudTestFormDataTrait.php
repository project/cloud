<?php

namespace Drupal\Tests\aws_cloud\Traits;

use Drupal\Component\Utility\Random;
use Drupal\Core\Datetime\DateHelper;
use Drupal\Tests\cloud\Functional\Utils;
use Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\Field\FieldType\KeyValue;

/**
 * The trait creating form data for aws cloud testing.
 */
trait AwsCloudTestFormDataTrait {

  use AwsCloudFormTrait;

  /**
   * AWS_CLOUD_CONFIG_REPEAT_COUNT.
   *
   * @var int
   */
  public static $awsCloudConfigRepeatCount = 1;

  /**
   * AWS_CLOUD_SECURITY_GROUP_REPEAT_COUNT.
   *
   * @var int
   */
  public static $awsCloudSecurityGroupRepeatCount = 2;

  /**
   * AWS_CLOUD_SECURITY_GROUP_RULES_REPEAT_COUNT.
   *
   * @var int
   */
  public static $awsCloudSecurityGroupRulesRepeatCount = 10;

  /**
   * AWS_CLOUD_SECURITY_GROUP_RULES_INBOUND.
   *
   * @var int
   */
  public static $awsCloudSecurityGroupRulesInbound = 0;

  /**
   * AWS_CLOUD_SECURITY_GROUP_RULES_OUTBOUND.
   *
   * @var int
   */
  public static $awsCloudSecurityGroupRulesOutbound = 1;

  /**
   * AWS_CLOUD_SECURITY_GROUP_RULES_MIX.
   *
   * @var int
   */
  public static $awsCloudSecurityGroupRulesMix = 2;

  /**
   * Create test data for cloud service provider (CloudConfig).
   *
   * @param int $max_count
   *   The max repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createCloudConfigTestFormData(int $max_count = 1): array {
    $this->random = new Random();

    // Input Fields.
    $data = [];
    for ($i = 0, $num = 1; $i < $max_count; $i++, $num++) {

      $data[] = [
        'name[0][value]'              => sprintf('#%d-%s', $num, $this->random->name(3, TRUE)),
        'field_description[0][value]' => "description #$num: " . date('Y/m/d H:i:s - D M j G:i:s T Y') . $this->random->string(64, TRUE),
        'field_api_version[0][value]' => 'latest',
        'field_region'                => "us-west-" . (1 + $num % 2),
        'field_secret_key[0][value]'  => $this->random->name(20, TRUE),
        'field_access_key[0][value]'  => $this->random->name(40, TRUE),
        'field_account_id[0][value]'  => $this->random->name(16, TRUE),
        'field_get_price_list[value]' => 0,
      ];
    }

    return $data;
  }

  /**
   * Create Elastic IP test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return string[][]
   *   Elastic IP array.
   */
  protected function createElasticIpTestFormData($repeat_count): array {
    $data = [];
    // 3 times.
    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      // Input Fields.
      $data[$i] = [
        'name'   => sprintf('eip-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
        'domain' => 'standard',
        'network_border_group' => 'us-west-2',
      ];
    }
    return $data;
  }

  /**
   * Create network interface test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param bool $attachment
   *   Whether attachment is needed.
   *
   * @return array
   *   Array of network interface test data.
   *
   * @throws \Exception
   */
  protected function createNetworkInterfaceTestFormData($repeat_count, $attachment = FALSE): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[$i] = [
        'name' => $this->random->name(32, TRUE),
        'description' => "Description #$num - {$this->random->name(64, TRUE)}",
        'subnet_id' => "subnet_id-{$this->getRandomId()}",
        'security_groups[]' => "sg-{$this->getRandomId()}",
        'primary_private_ip' => Utils::getRandomPrivateIp(),
      ];

      if ($attachment) {
        $data[$i]['attachment_id'] = "attachment-{$this->getRandomId()}";
      }
    }
    return $data;
  }

  /**
   * Create image test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return string[][]
   *   test data array.
   */
  protected function createImageTestFormData($repeat_count): array {
    $data = [];
    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[$i] = [
        'name'        => "Image #$num - " . date('Y/m/d - ') . $this->random->name(8, TRUE),
        'instance_id' => "i-{$this->getRandomId()}",
        'description' => "description-{$this->random->name(64)}",
      ];
    }
    return $data;
  }

  /**
   * Create instance test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createInstanceTestFormData($repeat_count = 1): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      $instance_name = sprintf('instance-entity #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(8, TRUE));

      // Input Fields.
      $data[$i] = [
        'name'                => $instance_name,
        'image_id'            => "ami-{$this->getRandomId()}",
        'min_count'           => $num,
        'max_count'           => $num * 2,
        'key_pair_name'       => "key_pair-{$num}-{$this->random->name(8, TRUE)}",
        'is_monitoring'       => 0,
        'availability_zone'   => "us-west-{$num}",
        'security_groups[]'   => "security_group-$num-{$this->random->name(8, TRUE)}",
        'instance_type'       => "t{$num}.micro",
        'kernel_id'           => "aki-{$this->getRandomId()}",
        'ramdisk_id'          => "ari-{$this->getRandomId()}",
        'user_data'           => "User data #{$num}: {$this->random->name(64, TRUE)}",
        'tags[0][item_key]'   => 'Name',
        'tags[0][item_value]' => $instance_name,
      ];
    }

    return $data;
  }

  /**
   * Create KeyPair test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   An array of data.
   */
  protected function createKeyPairTestFormData($repeat_count): array {
    $data = [];

    for ($i = 0; $i < $repeat_count; $i++) {
      // Input Fields.
      $data[$i] = [
        'key_pair_name' => $this->random->name(15, TRUE),
      ];
    }

    return $data;
  }

  /**
   * Create security group test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param bool $is_edit
   *   Whether edit mode or not.
   *
   * @return array
   *   Security group test data.
   */
  protected function createSecurityGroupTestFormData($repeat_count = 1, $is_edit = FALSE): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[$i] = [
        'description' => $this->getRandomSecurityGroupDescription(1, self::$awsCloudSecurityGroupDescriptionMaxLength, 'description-#$num - '),
      ];

      if ($is_edit) {
        $data[$i]['name'] = "group-name-#$num - {$this->random->name(15, TRUE)}";
      }
      else {
        $data[$i]['group_name[0][value]'] = "group-name-#$num - {$this->random->name(15, TRUE)}";
      }
    }
    return $data;
  }

  /**
   * Create snapshot test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return string[][]
   *   test data array.
   */
  protected function createSnapshotTestFormData($repeat_count): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[$i] = [
        'name'        => "Name #$num - {$this->random->name(32, TRUE)}",
        'volume_id'   => "vol-{$this->getRandomId()}",
        'description' => "Description #$num - {$this->random->name(64, TRUE)}",
      ];
    }
    return $data;
  }

  /**
   * Create volume test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param bool $is_openstack
   *   Whether openstack or not.
   *
   * @return string[][]|number[][]
   *   test data array.
   */
  protected function createVolumeTestFormData($repeat_count, $is_openstack = FALSE): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[$i] = [
        'name'              => "volume-name #$num - {$this->random->name(32, TRUE)}",
        'snapshot_id'       => "snap-{$this->getRandomId()}",
        'size'              => $num * 10,
      ];

      if ($is_openstack) {
        $data[$i]['availability_zone'] = 'RegionOne';
        $data[$i]['volume_type'] = 'lvmdriver-1';
      }
      else {
        $data[$i]['availability_zone'] = "us-west-{$num}";
        $data[$i]['iops'] = $num * 1000;
        $data[$i]['encrypted'] = $num % 2;
        $data[$i]['volume_type'] = 'io1';
      }
    }
    return $data;
  }

  /**
   * Create VPC test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   *
   * @throws \Exception
   */
  protected function createVpcTestFormData($repeat_count = 1): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[] = [
        'name'        => "VPC #$num - {$this->random->name(32, TRUE)}",
        'cidr_block'   => Utils::getRandomCidr(),
      ];
    }
    return $data;
  }

  /**
   * Create VPC peering connection test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param array $vpc_ids
   *   VPC IDs.
   *
   * @return array
   *   Test data.
   */
  protected function createVpcPeeringConnectionTestFormData($repeat_count, array $vpc_ids): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[] = [
        'name'              => "VPC peering connection #$num - {$this->random->name(32, TRUE)}",
        'requester_vpc_id'  => $vpc_ids[$i],
        'accepter_vpc_id'   => $vpc_ids[$i],
      ];
    }
    return $data;
  }

  /**
   * Create subnet test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param array $vpc_ids
   *   VPC IDs.
   *
   * @return array
   *   Test data.
   *
   * @throws \Exception
   */
  protected function createSubnetTestFormData($repeat_count, array $vpc_ids): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[] = [
        'vpc_id'      => $vpc_ids[$i],
        'name'        => "Name #$num - {$this->random->name(32, TRUE)}",
        'cidr_block'  => Utils::getRandomCidr(),
      ];
    }
    return $data;
  }

  /**
   * Create internet gateway test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   *
   * @throws \Exception
   */
  protected function createInternetGatewayTestFormData($repeat_count = 1): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[] = [
        'name' => "Internet gateway #$num - {$this->random->name(32, TRUE)}",
      ];
    }
    return $data;
  }

  /**
   * Create carrier gateway test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param array $vpc_ids
   *   VPC IDs.
   *
   * @return array
   *   Test data.
   *
   * @throws \Exception
   */
  protected function createCarrierGatewayTestFormData($repeat_count, array $vpc_ids): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[] = [
        'vpc_id'      => $vpc_ids[$i],
        'name' => "Carrier gateway #$num - {$this->random->name(32, TRUE)}",
      ];
    }
    return $data;
  }

  /**
   * Create transit gateway test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param array $vpc_ids
   *   VPC IDs.
   *
   * @return array
   *   Test data.
   *
   * @throws \Exception
   */
  protected function createTransitGatewayTestFormData($repeat_count, array $vpc_ids): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      // Input Fields.
      $data[] = [
        'name' => "Transit gateway #$num - {$this->random->name(32, TRUE)}",
      ];
    }
    return $data;
  }

  /**
   * Create cloud launch template test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createLaunchTemplateTestFormData($repeat_count = 1): array {
    $data = [];
    $random = $this->random;

    for ($i = 0, $num = 1, $instance_family = 3; $i < $repeat_count; $i++, $num++, $instance_family++) {

      // Input Fields.
      $data[] = [
        'cloud_context[0][value]' => $this->cloudContext,
        'name[0][value]' => "LaunchTemplate-{$random->name(16, TRUE)}",
        'field_description[0][value]' => "#$num: " . date('Y/m/d H:i:s - D M j G:i:s T Y')
        . ' - SimpleTest Launch Template Description - '
        . $random->name(32, TRUE),
        'field_test_only[value]' => '1',
        'field_instance_type' => "c{$instance_family}.xlarge",
        'field_availability_zone' => 'us-west-1',
        'field_monitoring[value]' => '1',
        'field_image_id' => "ami-{$this->getRandomId()}",
        'field_min_count[0][value]' => 1,
        'field_max_count[0][value]' => 1,
        'field_kernel_id[0][value]' => "aki-{$this->getRandomId()}",
        'field_ram[0][value]' => "ari-{$this->getRandomId()}",
        'field_security_group' => 1,
        'field_ssh_key' => 1,
        'field_tags[0][item_key]' => "key_{$random->name(8, TRUE)}",
        'field_tags[0][item_value]' => "value_{$random->name(8, TRUE)}",
        'field_workflow_status' => CloudLaunchTemplateInterface::APPROVED,
      ];
    }
    return $data;
  }

  /**
   * Create random data for snapshots.
   *
   * @return array
   *   Random snapshots.
   *
   * @throws \Exception
   */
  protected function createSnapshotsRandomTestFormData(): array {
    $snapshots = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $snapshots[] = [
        'SnapshotId' => "snap-{$this->getRandomId()}",
        'Name' => sprintf('snapshot-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $snapshots;
  }

  /**
   * Create random volumes data.
   *
   * @return array
   *   Random volumes data.
   *
   * @throws \Exception
   */
  protected function createVolumesRandomTestFormData(): array {
    $volumes = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $volumes[] = [
        'VolumeId' => "vol-{$this->getRandomId()}",
        'Name' => sprintf('volume-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $volumes;
  }

  /**
   * Create random images data.
   *
   * @return array
   *   Random images data.
   *
   * @throws \Exception
   */
  protected function createImagesRandomTestFormData(): array {
    $images = [];
    $architecture = ['x86_64', 'arm64'];
    $image_type = ['machine', 'kernel', 'ramdisk'];
    $state = ['available', 'pending', 'failed'];
    $hypervisor = ['ovm', 'xen'];
    $public = [0, 1];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $images[] = [
        'ImageId' => "ami-{$this->getRandomId()}",
        'Name' => sprintf('image-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
        'BlockDeviceMappings' => [],
        'OwnerId' => $this->random->name(8, TRUE),
        'Architecture' => $architecture[array_rand($architecture)],
        'ImageType' => $image_type[array_rand($image_type)],
        'State' => $state[array_rand($state)],
        'Hypervisor' => $hypervisor[array_rand($hypervisor)],
        'Public' => $public[array_rand($public)],
        'ProductCodes' => [
          ['ProductCode' => $this->random->name(8, TRUE)],
          ['ProductCode' => $this->random->name(8, TRUE)],
        ],
      ];
    }

    return $images;
  }

  /**
   * Create random instances data.
   *
   * @return array
   *   Random instances data.
   *
   * @throws \Exception
   */
  protected function createInstancesRandomTestFormData(): array {
    $instances = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $instances[] = [
        'InstanceId' => "i-{$this->getRandomId()}",
        'Name' => sprintf('instance-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $instances;
  }

  /**
   * Create random VPCs data.
   *
   * @return array
   *   Random VPCs data.
   *
   * @throws \Exception
   */
  protected function createVpcsRandomTestFormData(): array {
    $vpcs = [];
    $count = random_int(2, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $vpcs[] = [
        'VpcId' => "vpc-{$this->getRandomId()}",
        'Name' => sprintf('vpc-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
        'CidrBlock' => Utils::getRandomCidr(),
        'DhcpOptionsId' => 'dopt-' . $this->getRandomId(),
        'InstanceTenancy' => 'default',
        'IsDefault' => FALSE,
        'State' => 'available',
        'Tags' => [
          [
            'Key' => 'Name',
            'Value' => sprintf('vpc-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
          ],
        ],
      ];
    }

    return $vpcs;
  }

  /**
   * Create random VPC peering connections data.
   *
   * @return array
   *   Random VPCs data.
   *
   * @throws \Exception
   */
  protected function createVpcPeeringConnectionsRandomTestFormData(): array {
    $vpc_peering_connections = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $vpc_peering_connections[] = [
        'VpcPeeringConnectionId' => "pcx-{$this->getRandomId()}",
        'Name' => sprintf('pcx-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
        'Tags' => [
          [
            'Key' => 'Name',
            'Value' => sprintf('pcx-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
          ],
        ],
      ];
    }

    return $vpc_peering_connections;
  }

  /**
   * Create random internet gateways data.
   *
   * @return array
   *   Random internet gateways data.
   *
   * @throws \Exception
   */
  protected function createInternetGatewaysRandomTestFormData(): array {
    $internet_gateways = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $internet_gateways[] = [
        'InternetGatewayId' => "igw-{$this->getRandomId()}",
        'Name' => sprintf('internet-gateway-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
        'VpcId' => "vpc-{$this->getRandomId()}",
        'Tags' => [
          [
            'Key' => 'Name',
            'Value' => sprintf('internet-gateway-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
          ],
        ],
      ];
    }

    return $internet_gateways;
  }

  /**
   * Create random internet gateways data.
   *
   * @return array
   *   Random internet gateways data.
   *
   * @throws \Exception
   */
  protected function createCarrierGatewaysRandomTestFormData(): array {
    $carrier_gateways = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $carrier_gateways[] = [
        'CarrierGatewayId' => "igw-{$this->getRandomId()}",
        'Name' => sprintf('carrier-gateway-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
        'VpcId' => "vpc-{$this->getRandomId()}",
        'Tags' => [
          [
            'Key' => 'Name',
            'Value' => sprintf('carrier-gateway-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
          ],
        ],
      ];
    }

    return $carrier_gateways;
  }

  /**
   * Create random internet gateways data.
   *
   * @return array
   *   Random internet gateways data.
   *
   * @throws \Exception
   */
  protected function createTransitGatewaysRandomTestFormData(): array {
    $transit_gateways = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $transit_gateways[] = [
        'TransitGatewayId' => "igw-{$this->getRandomId()}",
        'Name' => sprintf('transit-gateway-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
        'VpcId' => "vpc-{$this->getRandomId()}",
        'Tags' => [
          [
            'Key' => 'Name',
            'Value' => sprintf('transit-gateway-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
          ],
        ],
      ];
    }

    return $transit_gateways;
  }

  /**
   * Create random network interfaces data.
   *
   * @return array
   *   Random network interfaces data.
   *
   * @throws \Exception
   */
  protected function createNetworkInterfacesRandomTestFormData(): array {
    $network_interfaces = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $network_interfaces[] = [
        'NetworkInterfaceId' => "eni-{$this->getRandomId()}",
        'Name' => sprintf('eni-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $network_interfaces;
  }

  /**
   * Create random security groups data.
   *
   * @return array
   *   Random security groups data.
   *
   * @throws \Exception
   */
  protected function createSecurityGroupRandomTestFormData(): array {
    $security_groups = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $security_groups[] = [
        'GroupId' => "sg-{$this->getRandomId()}",
        'GroupName' => sprintf('sg-random-data #%d-%s-%s', $i, date('Y/m/d-H:i:s'), $this->random->name(8, TRUE)),
        'Name' => sprintf('sg-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $security_groups;
  }

  /**
   * Create random Elastic IPs data.
   *
   * @return array
   *   Random Elastic IPs.
   *
   * @throws \Exception
   */
  protected function createElasticIpRandomTestFormData(): array {
    $elastic_ips = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $elastic_ips[] = [
        'PublicIp' => Utils::getRandomPublicIp(),
        'Name' => sprintf('eip-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $elastic_ips;
  }

  /**
   * Create random key pairs data.
   *
   * @return array
   *   Random key pairs data.
   *
   * @throws \Exception
   */
  protected function createKeyPairsRandomTestFormData(): array {

    $key_pairs = [];
    $count = random_int(1, 10);

    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {

      $key_fingerprint_parts = [];
      for ($part = 0; $part < 20; $part++) {
        $key_fingerprint_parts[] = sprintf('%02x', random_int(0, 255));
      }

      $key_pairs[] = [
        'KeyFingerprint' => implode(':', $key_fingerprint_parts),
        'Name' => sprintf('key_pair-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $key_pairs;
  }

  /**
   * Create random IAM roles.
   *
   * @return array
   *   Random IAM roles.
   *
   * @throws \Exception
   */
  protected function createIamRolesRandomTestFormData(): array {
    $random = $this->random;
    $iam_roles = [];
    $count = random_int(1, 10);
    for ($i = 0; $i < $count; $i++) {
      $arn_num = sprintf('%012s', random_int(1, 999999999999));
      $arn_name = $random->name(16, TRUE);
      $name = $random->name(10, TRUE);
      $iam_roles[] = [
        'InstanceProfileName' => $name,
        'Arn' => "arn:aws:iam::$arn_num:instance-profile/$arn_name",
        'Roles' => [
          ['RoleName' => $name],
        ],
      ];

    }

    return $iam_roles;
  }

  /**
   * Create random subnets.
   *
   * @return array
   *   Random subnets array.
   *
   * @throws \Exception
   */
  protected function createSubnetsRandomTestFormData(): array {
    $subnets = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $subnets[] = [
        'SubnetId' => "subnet-{$this->getRandomId()}",
        'Name' => sprintf(
          'subnet-random-data #%d - %s - %s',
          $num,
          date('Y/m/d H:i:s'),
          $this->random->name(32, TRUE)
        ),
        'CidrBlock' => Utils::getRandomCidr(),
        'State' => 'available',
        'AvailabilityZone' => 'zone1',
        'AvailabilityZoneId' => 'zone_id_1',
        'Tags' => [
          [
            'Key' => 'Name',
            'Value' => sprintf(
              'subnet-random-data #%d - %s - %s',
              $num,
              date('Y/m/d H:i:s'),
              $this->random->name(32, TRUE)
            ),
          ],
        ],
      ];
    }

    return $subnets;
  }

  /**
   * Create rules.
   *
   * @param int $rules_type
   *   The type of rules. Inbound | Outbound | Mixed.
   * @param string $edit_url
   *   The URL of security group edit form.
   * @param string $self_group_id
   *   The security group ID for rules.
   * @param int $security_group_rules_repeat_count
   *   The security group rules repeat count.
   *
   * @return array
   *   The rules created.
   *
   * @throws \Exception
   */
  protected function createRulesTestFormData($rules_type, $edit_url, $self_group_id = NULL, $security_group_rules_repeat_count = 1): array {
    $rules = [];
    $count = random_int(1, $security_group_rules_repeat_count);
    for ($i = 0; $i < $count; $i++) {
      $permissions = [
        [
          'source' => 'ip4',
          'cidr_ip' => Utils::getRandomCidr(),
          'from_port' => Utils::getRandomFromPort(),
          'to_port' => Utils::getRandomToPort(),
          'description' => $this->getRandomSecurityGroupDescription(),
        ],
        [
          'source' => 'ip4',
          'cidr_ip' => Utils::getRandomCidr(),
          'from_port' => Utils::getRandomFromPort(0, 0),
          'to_port' => Utils::getRandomToPort(),
          'description' => $this->getRandomSecurityGroupDescription(),
        ],
        [
          'source' => 'ip6',
          'cidr_ip_v6' => Utils::getRandomCidrV6(),
          'from_port' => Utils::getRandomFromPort(),
          'to_port' => Utils::getRandomToPort(),
          'description' => $this->getRandomSecurityGroupDescription(),
        ],
      ];

      if ($rules_type === self::$awsCloudSecurityGroupRulesInbound
        || $rules_type === self::$awsCloudSecurityGroupRulesOutbound) {
        $type = $rules_type;
      }
      else {
        $types = [
          self::$awsCloudSecurityGroupRulesInbound,
          self::$awsCloudSecurityGroupRulesOutbound,
        ];
        $type = $types[array_rand($types)];
      }
      $rules[] = $permissions[array_rand($permissions)] + ['type' => $type];
    }

    // Post to form.
    $params = [];
    $inbound_index = 0;
    $outbound_index = 0;
    $rules_added = [];
    foreach ($rules ?: [] as $rule) {
      if ($rule['type'] === self::$awsCloudSecurityGroupRulesInbound) {
        $index = $inbound_index++;
        $prefix = 'ip_permission';
      }
      else {
        $index = $outbound_index++;
        $prefix = 'outbound_permission';
      }

      foreach ($rule ?: [] as $key => $value) {
        if ($key === 'type') {
          continue;
        }
        $params["{$prefix}[{$index}][{$key}]"] = $value;
      }

      $rules_added[] = $rule;
      $this->updateRulesMockData($rules_added, self::$awsCloudSecurityGroupRulesOutbound);

      $this->drupalGet($edit_url);
      $this->submitForm(
        $params,
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($rule['from_port']);
    }

    // Confirm the values of edit form.
    $this->confirmRulesFormData($rules, $edit_url);

    return $rules;
  }

  /**
   * Revoke rules.
   *
   * @param array $rules
   *   The rules to be revoked.
   * @param string $view_url
   *   The URL of security group detailed view.
   */
  protected function revokeRulesTestFormData(array $rules, $view_url): void {
    $this->drupalGet($view_url);
    $count = count($rules);
    $inbound_rules = array_filter($rules, static function ($a) {
      return $a['type'] === self::$awsCloudSecurityGroupRulesInbound;
    });
    $inbound_rules_count = count($inbound_rules);
    for ($i = 0; $i < $count; $i++) {
      $rule = array_shift($rules);

      $index = 0;
      if ($rule['type'] === self::$awsCloudSecurityGroupRulesOutbound) {
        $index += $inbound_rules_count;
      }
      else {
        $inbound_rules_count--;
      }

      $this->clickLink(t('Revoke'), $index);
      $this->assertSession()->pageTextContains($rule['from_port']);

      $this->updateRulesMockData($rules, self::$awsCloudSecurityGroupRulesOutbound);
      $this->submitForm(
        [],
        $this->t('Revoke')->render()
      );
      $this->assertSession()->pageTextContains('Permission revoked');
    }
  }

  /**
   * Confirm rule values of security group edit form.
   *
   * @param array $rules
   *   The array of rules.
   * @param string $edit_url
   *   The url of edit form.
   */
  protected function confirmRulesFormData(array $rules, $edit_url): void {
    $this->drupalGet($edit_url);

    $inbound_index = 0;
    $outbound_index = 0;
    foreach ($rules ?: [] as $rule) {
      if ($rule['type'] === self::$awsCloudSecurityGroupRulesInbound) {
        $index = $inbound_index++;
        $prefix = 'ip_permission';
      }
      else {
        $index = $outbound_index++;
        $prefix = 'outbound_permission';
      }
      foreach ($rule ?: [] as $key => $value) {
        if ($key === 'type') {
          continue;
        }
        $name = "{$prefix}[{$index}][{$key}]";
        $this->assertSession()->fieldValueEquals($name, $value);
      }
    }

    // Confirm the last raw is empty.
    $name = "ip_permission[{$inbound_index}][from_port]";
    $this->assertSession()->fieldValueEquals($name, '');
    $name = "outbound_permission[{$outbound_index}][from_port]";
    $this->assertSession()->fieldValueEquals($name, '');
  }

  /**
   * Create random subnets.
   *
   * @return array
   *   Random subnets array.
   *
   * @throws \Exception
   */
  protected function createRandomSubnets(): array {

    $subnets = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $subnets[] = [
        'SubnetId' => 'subnet-' . $this->getRandomId(),
        'CidrBlock' => long2ip(mt_rand()) . '/255',
        'Tags' => [
          [
            'Key' => 'Name',
            'Value' => sprintf('subnet-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
          ],
        ],
      ];
    }

    return $subnets;
  }

  /**
   * Create update snapshot test cases.
   *
   * @return string[][]
   *   test cases array.
   */
  protected function createUpdateSnapshotTestCases(): array {
    $test_cases = [];
    $random = $this->random;

    // Only ID.
    $test_cases[] = ['id' => 'snap-' . $this->getRandomId()];

    // Name and ID.
    $test_cases[] = [
      'name' => 'Snapshot name ' . $random->name(32, TRUE),
      'id' => 'snap-' . $this->getRandomId(),
    ];

    return $test_cases;
  }

  /**
   * Get random rule.
   *
   * @param array $rule
   *   The array of rule.
   * @param bool $include_group
   *   Boolean to include group rule.
   *
   * @throws \Exception
   */
  protected function getRandomRule(array &$rule, $include_group = TRUE): void {
    $sources = ['ip4', 'ip6'];
    // Group can cause testing issues because a valid group_id is required
    // for AWS tests.  Added a boolean flag to disable group permissions from
    // being included in the rules.
    if ($include_group === TRUE) {
      $sources[] = 'group';
    }
    $rule = ['type' => $rule['type']];
    $source = $sources[array_rand($sources)];
    $rule['source'] = $source;
    $rule['from_port'] = Utils::getRandomFromPort();
    $rule['to_port'] = Utils::getRandomToPort();
    if ($source === 'ip4') {
      $rule['cidr_ip'] = Utils::getRandomCidr();
    }
    elseif ($source === 'ip6') {
      $rule['cidr_ip_v6'] = Utils::getRandomCidrV6();
    }
    else {
      $rule['user_id'] = $this->random->name(8, TRUE);
      $rule['group_id'] = "sg-{$this->getRandomId()}";
      $rule['vpc_id'] = "vpc-{$this->getRandomId()}";
      $rule['peering_connection_id'] = "pcx-{$this->getRandomId()}";
      $rule['peering_status'] = 'active';
    }
    $rule['description'] = $this->getRandomSecurityGroupDescription();

  }

  /**
   * Create random state.
   *
   * @return string
   *   random state.
   */
  protected function createRandomState(): string {
    $states = ['creating', 'in-use'];
    return $states[array_rand($states)];
  }

  /**
   * Get name from array.
   *
   * @param array $array
   *   The VPCs or subnets array.
   * @param int $index
   *   The index of array.
   * @param string $id_key_name
   *   The key name of index.
   *
   * @return string
   *   value of array.
   */
  protected function getNameFromArray(array $array, $index, $id_key_name): string {
    // Get ID.
    return $array[$index][$id_key_name];
  }

  /**
   * Get Random domain.
   *
   * @return string
   *   a random domain.
   */
  protected function getRandomDomain(): string {
    $domains = ['standard', 'vpc'];
    return $domains[array_rand($domains)];
  }

  /**
   * Get invalid characters for the security group (rule) description.
   *
   * @return string
   *   Invalid special characters.
   */
  private function getInvalidCharactersForSecurityGroupDescription(): string {

    // The code ranges of ASCII special characters included in a random string
    // is generated later.
    // A random name consists of ASCII characters of code range(65, 90),
    // range(97, 122), and range(48, 57).
    // A random string consists of ASCII characters of codes 32 to 126, which
    // include invalid special chars.
    $special_char_codes = array_merge(range(32, 47), range(58, 64),
      range(91, 96), range(123, 126));

    // Get invalid chars as the complement of the defined valid special chars.
    $invalid_chars = NULL;
    foreach ($special_char_codes as $c) {
      if (strpos(self::$awsCloudSecurityGroupDescriptionSpecialCharacters, chr($c)) === FALSE) {
        $invalid_chars .= chr($c);
      }
    }
    return $invalid_chars;
  }

  /**
   * Validator to check if the description string has any invalid characters.
   *
   * @param string $description
   *   Description string randomly generated.
   *
   * @return bool
   *   True if Description string has any invalid characters.
   */
  public function isSecurityGroupDescriptionWithInvalidCharacters(string $description): bool {
    $valid_pattern = '/^' . addcslashes($this->getPatternOfSecurityGroupDescription() ?? '', '/') . '$/';
    return preg_match($valid_pattern, $description) !== 1;
  }

  /**
   * Get a valid random description with constrains.
   *
   * Constraints: Up to 255 characters in length. Allowed characters are a-z,
   * A-Z, 0-9, spaces, and ._-:/()#,@[]+=&;{}!$* .
   *
   * @param int $min_length
   *   (optional) Min length of a description to generate.
   * @param int $max_length
   *   (optional) Max length of a description to generate.
   * @param string $prefix
   *   (optional) Prefix string.
   *
   * @return string
   *   A random description.
   *
   * @throws \Exception
   */
  protected function getRandomSecurityGroupDescription(int $min_length = 1, int $max_length = 255, string $prefix = ''): string {

    $length = random_int($min_length, $max_length - strlen($prefix));

    $invalid_chars = $this->getInvalidCharactersForSecurityGroupDescription();
    // Replace all invalid chars with valid ones.
    $pattern = '/[' . preg_quote($invalid_chars, NULL) . ']/';
    return preg_replace(
      $pattern,
      $this->random->name(1),
      $prefix . $this->random->string($length)
    );

  }

  /**
   * Get random description of a security group (rule) with invalid characters.
   *
   * @param int $min_length
   *   (optional) Min length of a description to generate.
   * @param int $max_length
   *   (optional) Max length of a description to generate.
   * @param string $prefix
   *   (optional) Prefix string.
   *
   * @return string
   *   A random description.
   *
   * @throws \Exception
   */
  protected function getRandomSecurityGroupDescriptionWithInvalidCharacters(int $min_length = 1, int $max_length = 255, string $prefix = ''): string {
    $length = random_int($min_length, $max_length - strlen($prefix));
    return $prefix . $this->random->string($length, FALSE, [
      $this,
      'isSecurityGroupDescriptionWithInvalidCharacters',
    ]);
  }

  /**
   * Extract array item.
   *
   * @param array $array
   *   Array to be extracted.
   * @param int $index
   *   Index.
   *
   * @return array
   *   Extracted array.
   */
  protected function extractArrayItem(array $array, $index): array {
    $extracted = [];
    foreach ($array ?: [] as $item) {
      $extracted[] = $item[$index];
    }

    return $extracted;
  }

  /**
   * Create field security groups test cases.
   *
   * @param string $vpc_id
   *   The ID of vpc.
   *
   * @return array
   *   The test cases of security group.
   */
  protected function createFieldSecurityGroupsTestCases($vpc_id): array {

    $mock_security_group_and_ids = [];
    for ($i = 0; $i < self::MAX_SECURITY_GROUPS_COUNT; $i++) {
      // Mock object of SecurityGroup.
      $mock_security_group = $this->createMock(SecurityGroupInterface::class);
      $group_id = 'sg-' . $this->getRandomId();
      $mock_security_group
        ->method('getGroupId')
        ->willReturn($group_id);

      $mock_security_group
        ->method('getVpcId')
        ->willReturn($vpc_id);

      $mock_security_group_and_ids[] = [
        (object) ['entity' => $mock_security_group],
        $group_id,
      ];
    }

    $mock_security_group_and_ids_exclude_first_last = array_slice(
      $mock_security_group_and_ids,
      1,
      count($mock_security_group_and_ids) - 2
    );

    $field_security_groups_test_cases = [];

    // Test cases.
    //
    // Test when security groups has the only one item, the first item only.
    // Security group A (* SELECTED).
    // ...
    // Security group M.
    // ...
    // Security group N.
    $field_security_groups_test_cases[] = [reset($mock_security_group_and_ids)];

    // Test when security groups has more than two items.
    if (self::MAX_SECURITY_GROUPS_COUNT > 1) {

      // The last item only.
      // Security group A.
      // ...
      // Security group M.
      // ...
      // Security group N (* SELECTED).
      $field_security_groups_test_cases[] = [end($mock_security_group_and_ids)];

      // The first item and the last one only.
      // Security group A (* SELECTED).
      // ...
      // Security group M.
      // ...
      // Security group N (* SELECTED).
      $field_security_groups_test_cases[] = [
        reset($mock_security_group_and_ids),
        end($mock_security_group_and_ids),
      ];

      if (self::MAX_SECURITY_GROUPS_COUNT > 2) {

        // Randomly picking up the only one item.
        // One random item.
        // Security group A.
        // ...
        // Security group M (* SELECTED).
        // ...
        // Security group N.
        $rand_index = array_rand($mock_security_group_and_ids_exclude_first_last);
        $field_security_groups_test_cases[] = [
          // Pickup one index randomly (array index: 1 to the (last index -2))
          $mock_security_group_and_ids_exclude_first_last[$rand_index],
        ];

        // The first item and one random one.
        // Security group A (* SELECTED).
        // ...
        // Security group M (* SELECTED).
        // ...
        // Security group N.
        $rand_index = array_rand($mock_security_group_and_ids_exclude_first_last);
        $field_security_groups_test_cases[] = [
          reset($mock_security_group_and_ids),
          // Pickup one index randomly (array index: 1 to the (last index -2))
          $mock_security_group_and_ids_exclude_first_last[$rand_index],
        ];

        // One random item and the last one.
        // Security group A.
        // ...
        // Security group M (* SELECTED).
        // ...
        // Security group N (* SELECTED).
        $rand_index = array_rand($mock_security_group_and_ids_exclude_first_last);
        $field_security_groups_test_cases[] = [
          // Pickup one index randomly (array index: 1 to the (last index -2))
          $mock_security_group_and_ids_exclude_first_last[$rand_index],
          end($mock_security_group_and_ids),
        ];

        if (self::MAX_SECURITY_GROUPS_COUNT > 3) {

          // Two random items.
          // Security group A.
          // ...
          // Security group M1 (* SELECTED).
          // ...
          // Security group M2 (* SELECTED).
          // ...
          // Security group N.
          $array_indices = array_rand($mock_security_group_and_ids_exclude_first_last, 2);
          $field_security_groups_test_cases[] = [
            $mock_security_group_and_ids_exclude_first_last[$array_indices[0]],
            $mock_security_group_and_ids_exclude_first_last[$array_indices[1]],
          ];
        }
      }
    }

    return $field_security_groups_test_cases;
  }

  /**
   * Create random tags.
   *
   * @return array
   *   Tag mock objects.
   *
   * @throws \Exception
   */
  protected function createRandomTags(): array {
    $mock_tags = [];
    $count = random_int(0, 5);
    for ($i = 0; $i < $count; $i++) {
      $mock_tag = $this->createMock(KeyValue::class);

      $mock_tag
        ->method('getItemKey')
        ->willReturn('key_' . $this->random->name(8, TRUE));

      $mock_tag
        ->method('getItemValue')
        ->willReturn('value_' . $this->random->name(8, TRUE));

      $mock_tags[] = $mock_tag;
    }

    return $mock_tags;
  }

  /**
   * Convert mock tag objects to AWS tags.
   *
   * @param array $mock_tags
   *   Mock tag objects.
   *
   * @return array
   *   AWS tags.
   */
  protected function convertToTags(array $mock_tags): array {
    $tags = [];
    foreach ($mock_tags ?: [] as $mock_tag) {
      $tags[] = [
        'Key' => $mock_tag->getItemKey(),
        'Value' => $mock_tag->getItemValue(),
      ];
    }

    return $tags;
  }

  /**
   * Get the AWS Cloud service providers as an array.
   *
   * @return array
   *   An array of AWS Cloud service providers.
   */
  protected function getCloudServiceProviderOptions(): array {
    $entities = \Drupal::service('plugin.manager.cloud_config_plugin')->loadConfigEntities('aws_cloud');

    $cloud_configs = [];
    foreach ($entities ?: [] as $entity) {
      $cloud_configs[] = $entity->getCloudContext();
    }

    return $cloud_configs;
  }

  /**
   * Get all other criteria options on AWS Cloud notifications.
   *
   * @return array
   *   Array of criteria.
   */
  protected function getCriteriaOptions(): array {
    return [30, 60, 90, 180, 365];
  }

  /**
   * Get the frequency options on AWS Cloud notifications.
   *
   * @return array
   *   Array of frequency.
   */
  protected function getFrequencyOptions(): array {
    return [86400, 604800, 2592000, 5184000, 7776000, 15552000, 31104000];
  }

  /**
   * Get the items per page on Pager options, Views, AWS Cloud settings.
   *
   * @return array
   *   Array of item per page.
   */
  protected function getItemsPerPageOptions(): array {
    return [10, 15, 20, 25, 50, 100];
  }

  /**
   * Get the notification criteria options on AWS Cloud notifications.
   *
   * @return array
   *   Array of notification criteria.
   */
  protected function getNotificationCriteriaOptions(): array {
    return [1, 30, 60, 90, 180, 365];
  }

  /**
   * Get all period options on AWS Cloud notifications.
   *
   * @return array
   *   Array of period.
   */
  protected function getPeriodOptions(): array {
    return ['4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14'];
  }

  /**
   * Create an array of random input data for Locations.
   *
   * @return array
   *   The array including random input data.
   */
  public function createAwsCloudLocationsFormData($repeat_count = 1): array {
    $regions = \Drupal::service('aws_cloud.ec2')->getRegions();

    $edit = [];
    for ($i = 0; $i < $repeat_count; $i++) {
      foreach (array_keys($regions) ?: [] as $region) {
        $latitude = random_int(-90, 90);
        $longitude = random_int(-180, 180);
        $edit[] = [
          "aws_cloud_region_locations[$region][city]" => $this->random->name(128, TRUE),
          "aws_cloud_region_locations[$region][latitude]" => $latitude,
          "aws_cloud_region_locations[$region][longitude]" => $longitude,
        ];
      }
    }

    return $edit;
  }

  /**
   * Create an array of random input data for Notifications.
   *
   * @return array
   *   The array including random input data.
   */
  public function createAwsCloudNotificationsFormData($repeat_count = 1): array {
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('aws_cloud.settings');

    $period_options = $this->getPeriodOptions();
    $criteria_options = $this->getCriteriaOptions();
    $frequency_options = $this->getFrequencyOptions();
    $notification_criteria_options = $this->getNotificationCriteriaOptions();

    $hour_options = DateHelper::hours();
    $minute_options = DateHelper::minutes();
    unset($hour_options['']);
    unset($minute_options['']);

    $edit = [];
    for ($i = 0; $i < $repeat_count; $i++) {
      $edit[] = [
        'aws_cloud_notification_frequency' => $frequency_options[array_rand($frequency_options)],
        'aws_cloud_instance_notification_hour' => (string) $hour_options[array_rand($hour_options)],
        'aws_cloud_instance_notification_minutes' => (string) $minute_options[array_rand($minute_options)],
        'aws_cloud_long_running_instance_notification' => !$config->get('aws_cloud_long_running_instance_notification'),
        'aws_cloud_long_running_instance_notify_owner' => !$config->get('aws_cloud_long_running_instance_notify_owner'),
        'aws_cloud_long_running_instance_notification_criteria' => $notification_criteria_options[array_rand($notification_criteria_options)],
        'aws_cloud_long_running_instance_notification_emails' => $this->random->name(10, TRUE),
        'aws_cloud_long_running_instance_notification_subject' => $this->random->name(10, TRUE),
        'aws_cloud_long_running_instance_notification_msg' => $this->random->name(10, TRUE),
        'aws_cloud_long_running_instance_notification_instance_info' => $this->random->name(10, TRUE),
        'aws_cloud_low_utilization_instance_notification' => !$config->get('aws_cloud_low_utilization_instance_notification'),
        'aws_cloud_low_utilization_instance_notify_owner' => !$config->get('aws_cloud_low_utilization_instance_notify_owner'),
        'aws_cloud_low_utilization_instance_cpu_utilization_threshold' => random_int(1, 100),
        'aws_cloud_low_utilization_instance_network_io_threshold' => random_int(1, 1024),
        'aws_cloud_low_utilization_instance_period' => $period_options[array_rand($period_options)],
        'aws_cloud_low_utilization_instance_notification_emails' => $this->random->name(10, TRUE),
        'aws_cloud_low_utilization_instance_notification_subject' => $this->random->name(10, TRUE),
        'aws_cloud_low_utilization_instance_notification_msg' => $this->random->name(10, TRUE),
        'aws_cloud_low_utilization_instance_notification_instance_info' => $this->random->name(10, TRUE),
        'aws_cloud_volume_notification' => !$config->get('aws_cloud_volume_notification'),
        'aws_cloud_volume_notify_owner' => !$config->get('aws_cloud_volume_notify_owner'),
        'aws_cloud_volume_notification_frequency' => $frequency_options[array_rand($frequency_options)],
        'aws_cloud_unused_volume_criteria' => $criteria_options[array_rand($criteria_options)],
        'aws_cloud_volume_notification_hour' => (string) $hour_options[array_rand($hour_options)],
        'aws_cloud_volume_notification_minutes' => (string) $minute_options[array_rand($minute_options)],
        'aws_cloud_volume_notification_emails' => $this->random->name(10, TRUE),
        'aws_cloud_volume_notification_subject' => $this->random->name(10, TRUE),
        'aws_cloud_volume_notification_msg' => $this->random->name(10, TRUE),
        'aws_cloud_volume_notification_volume_info' => $this->random->name(10, TRUE),
        'aws_cloud_snapshot_notification' => !$config->get('aws_cloud_snapshot_notification'),
        'aws_cloud_snapshot_notify_owner' => !$config->get('aws_cloud_snapshot_notify_owner'),
        'aws_cloud_snapshot_notification_frequency' => $frequency_options[array_rand($frequency_options)],
        'aws_cloud_stale_snapshot_criteria' => $criteria_options[array_rand($criteria_options)],
        'aws_cloud_snapshot_notification_hour' => (string) $hour_options[array_rand($hour_options)],
        'aws_cloud_snapshot_notification_minutes' => (string) $minute_options[array_rand($minute_options)],
        'aws_cloud_snapshot_notification_emails' => $this->random->name(10, TRUE),
        'aws_cloud_snapshot_notification_subject' => $this->random->name(10, TRUE),
        'aws_cloud_snapshot_notification_msg' => $this->random->name(10, TRUE),
        'aws_cloud_snapshot_notification_snapshot_info' => $this->random->name(10, TRUE),
        'aws_cloud_elastic_ip_notification' => !$config->get('aws_cloud_elastic_ip_notification'),
        'aws_cloud_elastic_ip_notify_owner' => !$config->get('aws_cloud_elastic_ip_notify_owner'),
        'aws_cloud_elastic_ip_notification_frequency' => $frequency_options[array_rand($frequency_options)],
        'aws_cloud_elastic_ip_notification_hour' => (string) $hour_options[array_rand($hour_options)],
        'aws_cloud_elastic_ip_notification_minutes' => (string) $minute_options[array_rand($minute_options)],
        'aws_cloud_elastic_ip_notification_emails' => $this->random->name(10, TRUE),
        'aws_cloud_elastic_ip_notification_subject' => $this->random->name(10, TRUE),
        'aws_cloud_elastic_ip_notification_msg' => $this->random->name(10, TRUE),
        'aws_cloud_elastic_ip_notification_elastic_ip_info' => $this->random->name(10, TRUE),
        'aws_cloud_launch_template_notification_request_emails' => $this->random->name(10, TRUE),
        'aws_cloud_launch_template_notification_request_subject' => $this->random->name(10, TRUE),
        'aws_cloud_launch_template_notification_request_msg' => $this->random->name(10, TRUE),
        'aws_cloud_launch_template_notification_launch_template_request_info' => $this->random->name(10, TRUE),
        'aws_cloud_launch_template_notification_approved_subject' => $this->random->name(10, TRUE),
        'aws_cloud_launch_template_notification_approved_msg' => $this->random->name(10, TRUE),
        'aws_cloud_launch_template_notification_restore_subject' => $this->random->name(10, TRUE),
        'aws_cloud_launch_template_notification_restore_msg' => $this->random->name(10, TRUE),
        'aws_cloud_launch_template_notification_launch_template_info' => $this->random->name(10, TRUE),
      ];
    }

    return $edit;
  }

  /**
   * Create an array of random input data for Settings.
   *
   * @return array
   *   The array including random input data.
   */
  public function createAwsCloudSettingsFormData($repeat_count = 1): array {
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('aws_cloud.settings');

    $items_per_page = $this->getItemsPerPageOptions();
    $cloud_service_providers = $this->getCloudServiceProviderOptions();

    $edit = [];
    for ($i = 0; $i < $repeat_count; $i++) {
      $edit[] = [
        'aws_cloud_test_mode' => !$config->get('aws_cloud_test_mode'),
        'aws_cloud_view_refresh_interval' => random_int(1, 9999),
        'aws_cloud_view_expose_items_per_page' => !$config->get('aws_cloud_view_expose_items_per_page'),
        'aws_cloud_view_items_per_page' => $items_per_page[array_rand($items_per_page)],
        'aws_cloud_instance_terminate' => !$config->get('aws_cloud_instance_terminate'),
        'aws_cloud_scheduler_tag' => $this->random->name(10, TRUE),
        'aws_cloud_scheduler_periods' => $this->random->name(10, TRUE),
        'aws_cloud_instance_type_prices' => !$config->get('aws_cloud_instance_type_prices'),
        'aws_cloud_instance_type_prices_spreadsheet' => !$config->get('aws_cloud_instance_type_prices_spreadsheet'),
        'aws_cloud_instance_type_cost' => !$config->get('aws_cloud_instance_type_cost'),
        'aws_cloud_instance_type_cost_list' => !$config->get('aws_cloud_instance_type_cost_list'),
        'aws_cloud_instance_list_cost_column' => !$config->get('aws_cloud_instance_list_cost_column'),
        'aws_cloud_ec2_pricing_endpoint' => $this->random->name(10, TRUE),
        'aws_cloud_service_provider' => $cloud_service_providers[array_rand($cloud_service_providers)],
        'aws_cloud_price_rate_ec2' => random_int(1, 100),
        'aws_cloud_monitor_refresh_interval' => random_int(1, 9999),
        'aws_cloud_update_resources_queue_cron_time' => random_int(1, 9999),
        'aws_cloud_queue_limit' => random_int(1, 50),
      ];
    }

    return $edit;
  }

}
