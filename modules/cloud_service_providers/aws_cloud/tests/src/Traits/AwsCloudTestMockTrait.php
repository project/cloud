<?php

namespace Drupal\Tests\aws_cloud\Traits;

use Drupal\Component\Serialization\Yaml;
use Drupal\Tests\aws_cloud\Functional\Ec2\ImageTest;
use Drupal\Tests\cloud\Functional\Utils;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\gapps\Service\GoogleSpreadsheetService;

/**
 * The trait creating mock data for aws cloud testing.
 */
trait AwsCloudTestMockTrait {

  /**
   * Initialize mock instance types.
   *
   * The mock instance types will be saved to configuration
   * aws_cloud_mock_instance_types.
   *
   * @throws \JsonException
   */
  protected function initMockInstanceTypes(): void {

    // The variable $mock_instance_types should be fine as a dummy table.
    // For testing, we use:
    // "c{$num}.xlarge" in createLaunchTemplateTestFormData,
    // "t{$num}.micro"  in createInstanceTestFormData and
    // "t{$num}.small"  in createInstanceTestEntity.
    $mock_instance_types = [
      'c3.xlarge' => 'c3.xlarge:2:10:4:0.085:876:1629',
      'c4.xlarge' => 'c4.xlarge:4:20:8:0.17:1751:3529',
      'c5.xlarge' => 'c5.xlarge:8:39:16:0.34:3503:6518',
      't1.micro'  => 't1.micro:2:1:1:0.0104:53:103',
      't2.micro'  => 't2.micro:2:2:2:0.0208:107:206',
      't3.micro'  => 't3.micro:2:4:4:0.0416:213:412',
      't1.small'  => 't1.small:1:3:3.75:0.067:453:687',
      't2.small'  => 't2.small:2:6.5:7.5:0.133:713:1373',
      't3.small'  => 't3.small:4:13:15:0.266:1428:2746',
    ];

    $config = \Drupal::configFactory()->getEditable('aws_cloud.settings');

    try {
      $config->set('aws_cloud_mock_instance_types', json_encode($mock_instance_types))
        ->save();
    }
    catch (\JsonException $e) {
      throw $e;
    }
  }

  /**
   * Mock the GoogleSpreadsheetService.
   *
   * The mock-up will return a spreadsheet URL.
   */
  private function initMockGoogleSpreadsheetService(): void {
    $mock_spreadsheet_service = $this->createMock(GoogleSpreadsheetService::class);

    $spreadsheet_id = $this->random->name(44, TRUE);
    $spreadsheet_url = "https://docs.google.com/spreadsheets/d/{$spreadsheet_id}/edit";
    $mock_spreadsheet_service->expects($this->any())
      ->method('createOrUpdate')
      ->willReturn($spreadsheet_url);

    // Provide a mock service container, for the services our module uses.
    $container = \Drupal::getContainer();
    $container->set('gapps.google_spreadsheet', $mock_spreadsheet_service);
  }

  /**
   * Add Elastic IP mock data.
   *
   * @param string $name
   *   The Elastic IP name.
   * @param string $public_ip
   *   The public IP.
   * @param string $domain
   *   The domain.
   *
   * @throws \Exception
   */
  protected function addElasticIpMockData(
    string $name = '',
    string $public_ip = '',
    string $domain = 'standard',
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $elastic_ip = [
      'AllocationId' => $name,
      'PublicIp' => $public_ip,
      'PrivateIpAddress' => Utils::getRandomPrivateIp(),
      'Domain' => $domain,
      'NetworkBorderGroup' => 'us-west-2',
    ];

    $mock_data['DescribeAddresses']['Addresses'][] = $elastic_ip;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add instance mock data.
   *
   * @param string $test_class
   *   The InstanceTest class.  It can be InstanceTest::class or
   *   OpenStackInstanceTest::class.
   * @param string $name
   *   Instance name.
   * @param string $key_pair_name
   *   Key pair name.
   * @param array $regions
   *   The regions.
   * @param string $state
   *   Instance state.
   * @param string $schedule_value
   *   Schedule value.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $vpc_id
   *   The VPC ID.
   *
   * @return string
   *   The instance ID.
   *
   * @throws \Exception
   */
  protected function addInstanceMockData(
    string $test_class,
    string $name = '',
    string $key_pair_name = '',
    array $regions = [],
    string $state = 'running',
    string $schedule_value = '',
    string $cloud_context = '',
    string $vpc_id = '',
  ): string {
    // Prepare the mock data for an instance.
    $public_ip = Utils::getRandomPublicIp();
    $private_ip = Utils::getRandomPrivateIp();

    $region = $regions[array_rand($regions)];

    $uid_tag_key = \Drupal::service('cloud')->getTagKeyCreatedByUid(
      'aws_cloud',
      $cloud_context
    );

    $vars = [
      'name' => $name,
      'key_name' => $key_pair_name,
      'account_id' => random_int(100000000000, 999999999999),
      'instance_id' => "i-{$this->getRandomId()}",
      'reservation_id' => "r-{$this->getRandomId()}",
      'group_name' => $this->random->name(8, TRUE),
      'host_id' => $this->random->name(8, TRUE),
      'affinity' => $this->random->name(8, TRUE),
      'launch_time' => date('c'),
      'security_group_id' => 'sg-' . $this->getRandomId(),
      'security_group_name' => $this->random->name(10, TRUE),
      'public_dns_name' => Utils::getPublicDns($region, $public_ip),
      'public_ip_address' => $public_ip,
      'private_dns_name' => Utils::getPrivateDns($region, $private_ip),
      'private_ip_address' => $private_ip,
      'subnet_id' => "subnet-{$this->getRandomId()}",
      'image_id' => "ami-{$this->getRandomId()}",
      'reason' => $this->random->string(16, TRUE),
      'state' => $state,
      'uid' => $this->loggedInUser->id(),
      'uid_tag_key' => $uid_tag_key,
    ];

    $vars['vpc_id'] = empty($vpc_id) ? "vpc-{$this->getRandomId()}" : $vpc_id;

    $vars = array_merge($this->getMockDataTemplateVars(), $vars);

    $instance_mock_data_content = $this->getMockDataFileContent($test_class, $vars, '_instance');

    $instance_mock_data = Yaml::decode($instance_mock_data_content);
    // OwnerId and ReservationId need to be set.
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeInstances']['Reservations'][0]['OwnerId'] = $this->random->name(8, TRUE);
    $mock_data['DescribeInstances']['Reservations'][0]['ReservationId'] = $this->random->name(8, TRUE);

    // Add Schedule information if available.
    if (!empty($schedule_value)) {
      $instance_mock_data['Tags'][] = [
        'Key' => 'Schedule',
        'Value' => $schedule_value,
      ];
    }

    $mock_data['DescribeInstances']['Reservations'][0]['Instances'][] = $instance_mock_data;
    $this->updateMockDataToConfig($mock_data);
    return $vars['instance_id'];
  }

  /**
   * Add network interface mock data.
   *
   * @param array $data
   *   Array of network interface data.
   * @param string $tag_created_uid
   *   The tag created by uid in mock data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   * @param bool $ec2_api
   *   Whether EC2 API or not.
   *
   * @return string
   *   The Network interface ID.
   */
  protected function addNetworkInterfaceMockData(
    array &$data,
    string $tag_created_uid,
    string $cloud_context,
    int $uid,
    bool $ec2_api = TRUE,
  ): string {

    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->latestTemplateVars;

    $network_interface = [
      'NetworkInterfaceId' => $vars['network_interface_id'],
      'VpcId' => $vars['vpc_id'],
      'Description' => $data['description'],
      'SubnetId' => $data['subnet_id'],
      'MacAddress' => NULL,
      'Status' => 'in-use',
      'PrivateDnsName' => NULL,
      'OwnerId' => $this->random->name(8, TRUE),
      'SourceDestCheck' => NULL,
      'PrivateIpAddress' => $data['primary_private_ip'],
      'Attachment' => [
        'AttachmentId' => $data['attachment_id'] ?? NULL,
        'InstanceOwnerId' => NULL,
        'Status' => NULL,
        'InstanceId' => NULL,
        'DeviceIndex' => '',
        'DeleteOnTermination' => NULL,
      ],
      'Association' => [
        'AllocationId' => '',
      ],
      'PrivateIpAddresses' => [[
        'Primary' => TRUE,
        'PrivateIpAddress' => $data['primary_private_ip'],
      ],
      ],
      'Groups' => [['GroupName' => $data['security_groups[]']]],
    ];

    $network_interface['Tags'] = !empty($ec2_api)
      ? [
          [
            'Key' => $tag_created_uid,
            'Value' => $uid,
          ],
      ]
      : [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ];

    $data['name'] = $network_interface['NetworkInterfaceId'];
    $mock_data['DescribeNetworkInterfaces']['NetworkInterfaces'][] = $network_interface;
    $this->updateMockDataToConfig($mock_data);

    return $vars['network_interface_id'];
  }

  /**
   * Delete first Elastic IP in mock data.
   */
  protected function deleteFirstElasticIpMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $addresses = $mock_data['DescribeAddresses']['Addresses'];
    array_shift($addresses);
    $mock_data['DescribeAddresses']['Addresses'] = $addresses;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete all Elastic IP in mock data.
   */
  protected function deleteAllElasticIpInMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeAddresses']['Addresses'] = [];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update domain in mock data.
   *
   * @param string $domain
   *   A domain.
   */
  protected function updateDomainMockData(string $domain): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['AllocateAddress']['Domain'] = $domain;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Elastic IP mock data.
   *
   * @param int $elastic_ip_index
   *   The index of Elastic IP.
   * @param string $name
   *   The Elastic IP name.
   * @param string $association_id
   *   The association ID.
   * @param string $instance_id
   *   The instance ID.
   */
  protected function updateElasticIpMockData(
    int $elastic_ip_index = 0,
    string $name = '',
    string $association_id = '',
    string $instance_id = '',
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeAddresses']['Addresses'][$elastic_ip_index]['AllocationId'] = $name;
    $mock_data['DescribeAddresses']['Addresses'][$elastic_ip_index]['AssociationId'] = $association_id;
    $mock_data['DescribeAddresses']['Addresses'][$elastic_ip_index]['InstanceId'] = $instance_id;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update instance in mock data.
   *
   * @param string $test_class
   *   The InstanceTest class.  It can be InstanceTest::class or
   *   OpenStackInstanceTest::class.
   * @param int $index
   *   Instance index.
   * @param string $name
   *   Instance name.
   * @param array $regions
   *   The regions.
   * @param string $state
   *   Instance state.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @throws \Exception
   */
  protected function updateInstanceMockData(
    string $test_class,
    int $index = 0,
    string $name = '',
    array $regions = [],
    string $state = 'stopped',
    string $cloud_context = '',
  ): void {
    $mock_data = $this->getMockDataFromConfig();

    if (empty($name)) {
      // Prepare the mock data for an instance.
      $public_ip = Utils::getRandomPublicIp();
      $private_ip = Utils::getRandomPrivateIp();

      $region = $regions[array_rand($regions)];

      $uid_tag_key = \Drupal::service('cloud')->getTagKeyCreatedByUid(
        'aws_cloud',
        $cloud_context
      );

      // These key-value pairs are replaced into the placeholder such as {{..}}
      // in <ResourceName>Test.hml.
      $vars = [
        // 12 digits.
        'account_id' => random_int(100000000000, 999999999999),
        'reservation_id' => 'r-' . $this->getRandomId(),
        'group_name' => $this->random->name(8, TRUE),
        'host_id' => $this->random->name(8, TRUE),
        'affinity' => $this->random->name(8, TRUE),
        'launch_time' => date('c'),
        'security_group_id' => 'sg-' . $this->getRandomId(),
        'security_group_name' => $this->random->name(10, TRUE),
        'public_dns_name' => Utils::getPublicDns($region, $public_ip),
        'public_ip_address' => $public_ip,
        'private_dns_name' => Utils::getPrivateDns($region, $private_ip),
        'private_ip_address' => $private_ip,
        'vpc_id' => 'vpc-' . $this->getRandomId(),
        'subnet_id' => 'subnet-' . $this->getRandomId(),
        'image_id' => 'ami-' . $this->getRandomId(),
        'reason' => $this->random->string(16, TRUE),
        'instance_id' => 'i-' . $this->getRandomId(),
        'uid' => $this->loggedInUser->id(),
        'uid_tag_key' => $uid_tag_key,
      ];

      // Create a mock data for an instance.
      $instance_mock_data_content = $this->getMockDataFileContent($test_class, $vars, '_instance');
      $instance_mock_data = Yaml::decode($instance_mock_data_content);

      // OwnerId and ReservationId need to be set.
      $mock_data['DescribeInstances']['Reservations'][0]['OwnerId'] = $this->random->name(8, TRUE);
      $mock_data['DescribeInstances']['Reservations'][0]['ReservationId'] = $this->random->name(8, TRUE);
      $mock_data['DescribeInstances']['Reservations'][0]['Instances'][$index] = $instance_mock_data;
    }

    $mock_data['DescribeInstances']['Reservations'][0]['Instances'][$index]['Tags'][0]['Value'] = $name;
    $mock_data['DescribeInstances']['Reservations'][0]['Instances'][$index]['State']['Name'] = $state;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add image mock data.
   *
   * @param string $name
   *   The image's name.
   * @param string $description
   *   The description.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   * @param string $tag_created_uid
   *   The tag created by uid in mock data.
   * @param bool $ec2_api
   *   Whether EC2 API or not.
   *
   * @return array
   *   The image items of DescribeImages mock data.
   */
  protected function addImageMockData(
    string $name,
    string $description,
    string $cloud_context,
    int $uid,
    string $tag_created_uid,
    bool $ec2_api = TRUE,
  ): array {
    $cloud_config_plugin = \Drupal::service('plugin.manager.cloud_config_plugin');
    $cloud_config_plugin->setCloudContext($cloud_context);
    $cloud_config = $cloud_config_plugin->loadConfigEntity();
    $account_id = $cloud_config->get('field_account_id')->value;

    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $mock_data['CreateImage'] = [
      'ImageId' => $vars['image_id'],
    ];
    $this->updateMockDataToConfig($mock_data);

    $image = [
      'ImageId' => $vars['image_id'],
      'OwnerId' => $account_id,
      'Architecture' => $vars['architecture'],
      'Description' => $description,
      'VirtualizationType' => 'hvm',
      'RootDeviceType' => 'ebs',
      'RootDeviceName' => '/dev/sda1',
      'Name' => $name,
      'KernelId' => $vars['kernel_id'],
      'RamdiskId' => $vars['ramdisk_id'],
      'ImageType' => $vars['image_type'],
      'ProductCodes' => [
        'ProductCode' => [$vars['product_code1'], $vars['product_code2']],
      ],
      'ImageLocation' => $vars['image_location'],
      'StateReason' => ['Message' => $vars['state_reason_message']],
      'Platform' => $vars['platform'],
      'Public' => $vars['public'],
      'State' => $vars['state'],
      'CreationDate' => $vars['creation_date'],
      'BlockDeviceMappings' => [['DeviceName' => NULL]],
      'ImageVisibility' => 'public',
      'Hypervisor' => $vars['hypervisor'],
      'Tags' => !empty($ec2_api)
        ? [
          [
            'Key' => $tag_created_uid,
            'Value' => $uid,
          ],
        ]
        : [
          \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
        ],
    ];

    $mock_data['DescribeImages']['Images'][] = $image;
    $this->updateMockDataToConfig($mock_data);
    return $image;
  }

  /**
   * Update image mock data.
   *
   * @param array $images
   *   The image data.
   */
  protected function updateImagesMockData(array $images): void {
    $mock_data = $this->getMockDataFromConfig();
    // Clear all existing images.
    $mock_data['DescribeImages']['Images'] = [];
    foreach ($images ?: [] as $image) {
      $items = [];
      foreach ($image ?: [] as $key => $value) {
        $items[$key] = $value;
      }
      $mock_data['DescribeImages']['Images'][] = $items;
    }
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update image description in mock data.
   *
   * @param int $index
   *   The image's index.
   * @param string $description
   *   The image description.
   */
  protected function updateImageDescriptionMockData(int $index, string $description): void {
    $mock_data = $this->getMockDataFromConfig();
    // Clear all existing images.
    $mock_data['DescribeImages']['Images'][$index]['Description'] = $description;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first image in mock data.
   */
  protected function deleteFirstImageMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $images = $mock_data['DescribeImages']['Images'];
    array_shift($images);
    $mock_data['DescribeImages']['Images'] = $images;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add KeyPair mock data.
   *
   * @param string $name
   *   The key pair name.
   * @param string $tag_created_uid
   *   The tag created by uid in mock data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   * @param bool $ec2_api
   *   Whether EC2 API or not.
   */
  protected function addKeyPairMockData(
    string $name,
    string $tag_created_uid,
    string $cloud_context,
    int $uid,
    bool $ec2_api = TRUE,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();
    $key_pair = [
      'KeyName' => $name,
      'KeyFingerprint' => $vars['key_fingerprint'],
      'KeyPairId' => 'key-' . $this->getRandomId(),
    ];
    $key_pair['Tags'] = !empty($ec2_api)
      ? [
          [
            'Key' => $tag_created_uid,
            'Value' => $uid,
          ],
      ]
      : [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ];
    $mock_data['DescribeKeyPairs']['KeyPairs'][] = $key_pair;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first key pair in mock data.
   */
  protected function deleteFirstKeyPairMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $key_pairs = $mock_data['DescribeKeyPairs']['KeyPairs'];
    array_shift($key_pairs);
    $mock_data['DescribeKeyPairs']['KeyPairs'] = $key_pairs;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update key pair mock data.
   *
   * @param int $key_pair_index
   *   The index of key pair.
   * @param string $name
   *   The key pair name.
   */
  protected function updateKeyPairMockData(int $key_pair_index, string $name): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeKeyPairs']['KeyPairs'][$key_pair_index]['KeyName'] = $name;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first network interface in mock data.
   */
  protected function deleteFirstNetworkInterfaceMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $network_interfaces = $mock_data['DescribeNetworkInterfaces']['NetworkInterfaces'];
    array_shift($network_interfaces);
    $mock_data['DescribeNetworkInterfaces']['NetworkInterfaces'] = $network_interfaces;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update network interface mock data.
   *
   * @param int $network_interface_index
   *   The index of network interface.
   * @param string $name
   *   The network Interface name.
   */
  protected function updateNetworkInterfaceMockData(int $network_interface_index, string $name): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeNetworkInterfaces']['NetworkInterfaces'][$network_interface_index]['NetworkInterfaceId'] = $name;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update create network interface in mock data.
   *
   * @param string $network_interface_id
   *   Network interface ID.
   */
  protected function updateCreateNetworkInterfaceMockData(string $network_interface_id): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['CreateNetworkInterface']['NetworkInterfaceId'] = $network_interface_id;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add security group mock data.
   *
   * @param string $name
   *   The security group name.
   * @param string $description
   *   The description.
   * @param string $vpc_id
   *   The VPC ID.
   * @param string $tag_created_uid
   *   The tag created by uid in mock data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   * @param bool $ec2_api
   *   Whether EC2 API or not.
   *
   * @return string
   *   The security group ID.
   */
  protected function addSecurityGroupMockData(
    string $name,
    string $description,
    ?string $vpc_id = NULL,
    string $tag_created_uid = '',
    string $cloud_context = '',
    int $uid = 0,
    bool $ec2_api = TRUE,
  ): string {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();
    $security_group = [
      'GroupId' => $vars['group_id'],
      'GroupName' => $name,
      'Description' => $description,
      'VpcId' => $vpc_id ?? $vars['vpc_id'],
      'OwnerId' => $this->random->name(8, TRUE),
    ];
    $security_group['Tags'] = !empty($ec2_api)
      ? [
          [
            'Key' => $tag_created_uid,
            'Value' => $uid,
          ],
      ]
      : [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ];
    $mock_data['DescribeSecurityGroups']['SecurityGroups'][] = $security_group;
    $this->updateMockDataToConfig($mock_data);
    return $vars['group_id'];
  }

  /**
   * Update the CreateSecurityGroup mock data.
   */
  protected function updateCreateSecurityGroupMockData() {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();
    $mock_data['CreateSecurityGroup']['GroupId'] = $vars['group_id'];
    $mock_data['CreateSecurityGroup']['ErrorCode'] = $vars['error_code'];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first security group in mock data.
   */
  protected function deleteFirstSecurityGroupMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $security_groups = $mock_data['DescribeSecurityGroups']['SecurityGroups'];
    array_shift($security_groups);
    $mock_data['DescribeSecurityGroups']['SecurityGroups'] = $security_groups;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add a snapshot mock data.
   *
   * @param string $name
   *   The snapshot's name.
   * @param string $volume_id
   *   The volume ID.
   * @param string $description
   *   The description.
   * @param string $tag_created_uid
   *   The tag created by uid in mock data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   * @param bool $ec2_api
   *   Whether EC2 API or not.
   *
   * @return string
   *   An array of possible key and value options.
   */
  protected function addSnapshotMockData(
    string &$name,
    string $volume_id,
    string $description,
    string $tag_created_uid,
    string $cloud_context,
    int $uid,
    bool $ec2_api = TRUE,
  ): string {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $snapshot = [
      'SnapshotId' => $vars['snapshot_id'],
      'VolumeSize' => 10,
      'Description' => $description,
      'State' => 'completed',
      'VolumeId' => $volume_id,
      'Progress' => 100,
      'Encrypted' => FALSE,
      'KmsKeyId' => NULL,
      'OwnerId' => $this->random->name(8, TRUE),
      'OwnerAlias' => NULL,
      'StateMessage' => NULL,
      'StartTime' => $vars['start_time'],
    ];

    $snapshot['Tags'] = !empty($ec2_api)
      ? [
          [
            'Key' => $tag_created_uid,
            'Value' => $uid,
          ],
      ]
      : [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ];

    $name = $snapshot['SnapshotId'];

    $mock_data['DescribeSnapshots']['Snapshots'][] = $snapshot;
    $this->updateMockDataToConfig($mock_data);

    return $vars['snapshot_id'];
  }

  /**
   * Delete the first snapshot in mock data.
   */
  protected function deleteFirstSnapshotMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $snapshots = $mock_data['DescribeSnapshots']['Snapshots'];
    array_shift($snapshots);
    $mock_data['DescribeSnapshots']['Snapshots'] = $snapshots;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update the snapshot mock data.
   *
   * @param int $snapshot_index
   *   The index of snapshot.
   * @param string $name
   *   The snapshot's name.
   */
  protected function updateSnapshotMockData(int $snapshot_index, string $name): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeSnapshots']['Snapshots'][$snapshot_index]['Tags'][0]
      = ['Key' => 'Name', 'Value' => $name];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update create snapshot in mock data.
   *
   * @param string $snapshot_id
   *   The snapshot ID.
   */
  protected function updateCreateSnapshotMockData(string $snapshot_id): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['CreateSnapshot']['SnapshotId'] = $snapshot_id;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add Volume mock data.
   *
   * @param array $data
   *   Array of volume data.
   * @param string $tag_created_uid
   *   The tag created by uid in mock data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   * @param bool $ec2_api
   *   Whether EC2 API or not.
   *
   * @throws \Exception
   */
  protected function addVolumeMockData(
    array $data,
    string $tag_created_uid,
    string $cloud_context,
    int $uid,
    bool $ec2_api = TRUE,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $volume = [
      'VolumeId' => $data['VolumeId'] ?? $data['name'],
      'Attachments' => [
        ['InstanceId' => NULL],
        ['Device' => ''],
      ],
      'State' => 'available',
      'SnapshotId' => $data['snapshot_id'],
      'Size' => $data['size'],
      'VirtualizationType' => NULL,
      'VolumeType' => $data['volume_type'],
      'Iops' => $data['iops'] ?? '',
      'AvailabilityZone' => $data['availability_zone'],
      'Encrypted' => $data['encrypted'] ?? '',
      'KmsKeyId' => NULL,
      'CreateTime' => $vars['create_time'],
    ];

    $volume['Tags'] = !empty($ec2_api)
      ? [
          [
            'Key' => $tag_created_uid,
            'Value' => $uid,
          ],
      ]
      : [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ];

    $mock_data['DescribeVolumes']['Volumes'][] = $volume;
    $this->updateMockDataToConfig($mock_data);

    $snapshot_name = 'snapshot-name' . $this->random->name(10, TRUE);
    $this->updateDescribeSnapshotsMockData([
      ['id' => $data['snapshot_id'], 'name' => $snapshot_name],
    ]);
  }

  /**
   * Delete first Volume in mock data.
   */
  protected function deleteFirstVolumeMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $addresses = $mock_data['DescribeVolumes']['Volumes'];
    array_shift($addresses);
    $mock_data['DescribeVolumes']['Volumes'] = $addresses;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update a volume mock data.
   *
   * @param int $volume_index
   *   The index of Volume.
   * @param string $name
   *   The volume's name.
   * @param string $instance_id
   *   The instance ID.
   */
  protected function updateVolumeMockData(int $volume_index, string $name, string $instance_id): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeVolumes']['Volumes'][$volume_index]['VolumeId'] = $name;
    $mock_data['DescribeVolumes']['Volumes'][$volume_index]['InstanceId']
      = $instance_id;
    $mock_data['DescribeVolumes']['Volumes'][$volume_index]['State'] = 'in-use';
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update the volume state and instance.
   *
   * @param string $api
   *   The API data.
   * @param string $device
   *   The device.
   * @param string $volume_id
   *   The volume_id.
   * @param string $instance_id
   *   The instance_id.
   * @param string $state
   *   The state.
   */
  protected function updateAttachDetachVolumeMockData(
    string $api,
    string $device,
    string $volume_id,
    string $instance_id,
    string $state,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data[$api] = [
      'Device' => $device,
      'InstanceId' => $instance_id,
      'State' => $state,
      'VolumeId' => $volume_id,
    ];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first instance in mock data.
   */
  protected function deleteFirstInstanceMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $instances = $mock_data['DescribeInstances']['Reservations'][0]['Instances'];
    array_shift($instances);
    $mock_data['DescribeInstances']['Reservations'][0]['Instances'] = $instances;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update security group in mock data.
   *
   * @param string $security_group_name1
   *   Security group name1.
   * @param string $security_group_name2
   *   Security group name2.
   */
  protected function updateSecurityGroupsMockData(
    string $security_group_name1,
    string $security_group_name2,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeInstances']['Reservations'][0]['Instances'][0]['SecurityGroups']
      = [
        ['GroupName' => $security_group_name1],
        ['GroupName' => $security_group_name2],
      ];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add VPC to mock data.
   *
   * @param array $vpc_data
   *   VPC data.
   * @param string $vpc_id
   *   VPC ID.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function addVpcMockData(array $vpc_data, $vpc_id, string $cloud_context, int $uid): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeVpcs']['Vpcs'][] = [
      'VpcId' => $vpc_id,
      'State' => 'available',
      'CidrBlock' => $vpc_data['cidr_block'],
      'CidrBlockAssociationSet' => [
        [
          'CidrBlock' => $vpc_data['cidr_block'],
          'AssociationId' => 'vpc-cidr-assoc' . $this->getRandomId(),
          'CidrBlockState' => [
            'State' => 'associated',
          ],
        ],
      ],
      'Ipv6CidrBlockAssociationSet' => [],
      'DhcpOptionsId' => 'dopt-' . $this->getRandomId(),
      'InstanceTenancy' => 'default',
      'IsDefault' => FALSE,
      'Tags' => [
        [
          'Key' => 'Name',
          'Value' => $vpc_data['name'],
        ],
        [
          'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
            'aws_cloud',
            $cloud_context
          ),
          'Value' => $uid,
        ],
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add internet gateway to mock data.
   *
   * @param array $internet_gateway_data
   *   Internet gateway data.
   * @param string $internet_gateway_id
   *   Internet gateway ID.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function addInternetGatewayMockData(
    array $internet_gateway_data,
    string $internet_gateway_id,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeInternetGateways']['InternetGateways'][] = [
      'InternetGatewayId' => $internet_gateway_id,
      'Attachments' => [
        [
          'State' => 'attached',
          'VpcId' => 'vpc-' . $this->getRandomId(),
        ],
      ],
      'Tags' => [
        [
          'Key' => 'Name',
          'Value' => $internet_gateway_data['name'],
        ],
        [
          'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
            'aws_cloud',
            $cloud_context
          ),
          'Value' => $uid,
        ],
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add carrier gateway to mock data.
   *
   * @param array $carrier_gateway_data
   *   Carrier gateway data.
   * @param string $carrier_gateway_id
   *   Carrier gateway ID.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function addCarrierGatewayMockData(
    array $carrier_gateway_data,
    string $carrier_gateway_id,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeCarrierGateways']['CarrierGateways'][] = [
      'CarrierGatewayId' => $carrier_gateway_id,
      'State' => 'available',
      'VpcId' => 'vpc-' . $this->getRandomId(),
      'Tags' => [
        [
          'Key' => 'Name',
          'Value' => $carrier_gateway_data['name'],
        ],
        [
          'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
            'aws_cloud',
            $cloud_context),
          'Value' => $uid,
        ],
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add transit gateway to mock data.
   *
   * @param array $transit_gateway_data
   *   Transit gateway data.
   * @param string $transit_gateway_id
   *   Transit gateway ID.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function addTransitGatewayMockData(
    array $transit_gateway_data,
    string $transit_gateway_id,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();

    $values = ['enable', 'disable'];
    $mock_data['DescribeTransitGateways']['TransitGateways'][] = [
      'TransitGatewayId' => $transit_gateway_id,
      'State' => 'available',
      'Options' => [
        'AutoAcceptSharedAttachments' => $values[array_rand($values)],
        'DefaultRouteTableAssociation' => $values[array_rand($values)],
        'DefaultRouteTablePropagation' => $values[array_rand($values)],
        'DnsSupport' => $values[array_rand($values)],
        'VpnEcmpSupport' => $values[array_rand($values)],
        'MulticastSupport' => $values[array_rand($values)],
        'AssociationDefaultRouteTableId' => '',
        'PropagationDefaultRouteTableId' => '',
      ],
      'Tags' => [
        [
          'Key' => 'Name',
          'Value' => $transit_gateway_data['name'],
        ],
        [
          'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
            'aws_cloud',
            $cloud_context
          ),
          'Value' => $uid,
        ],
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add VPC peering connection to mock data.
   *
   * @param array $vpc_peering_connection_data
   *   VPC peering connection data.
   * @param string $vpc_peering_connection_id
   *   VPC peering connection ID.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   *
   * @throws \Exception
   */
  protected function addVpcPeeringConnectionMockData(
    array $vpc_peering_connection_data,
    string $vpc_peering_connection_id,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeVpcPeeringConnections']['VpcPeeringConnections'][] = [
      'VpcPeeringConnectionId' => $vpc_peering_connection_id,
      'RequesterVpcInfo' => [
        'OwnerId' => (string) random_int(100000000000, 999999999999),
        'VpcId' => 'vpc-' . $this->getRandomId(),
        'CidrBlock' => Utils::getRandomCidr(),
        'Region' => 'ap-northeast-1',
      ],
      'AccepterVpcInfo' => [
        'OwnerId' => (string) random_int(100000000000, 999999999999),
        'VpcId' => 'vpc-' . $this->getRandomId(),
        'CidrBlock' => Utils::getRandomCidr(),
        'Region' => 'ap-northeast-1',
      ],
      'Status' => [
        'Code' => 'pending-acceptance',
        'Message' => 'Pending Acceptance',
      ],
      'Tags' => [
        [
          'Key' => 'Name',
          'Value' => $vpc_peering_connection_data['name'],
        ],
        [
          'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
            'aws_cloud',
            $cloud_context
          ),
          'Value' => $uid,
        ],
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add subnet to mock data.
   *
   * @param array $subnet_data
   *   Subnet data.
   * @param string $subnet_id
   *   Subnet ID.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function addSubnetMockData(
    array $subnet_data,
    string $subnet_id,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeSubnets']['Subnets'][] = [
      'VpcId' => $subnet_data['vpc_id'],
      'SubnetId' => $subnet_id,
      'State' => 'available',
      'CidrBlock' => $subnet_data['cidr_block'],
      'AvailabilityZone' => 'zone1',
      'AvailabilityZoneId' => 'zone_id_1',
      'Tags' => [
        [
          'Key' => 'Name',
          'Value' => $subnet_data['name'],
        ],
        [
          'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
            'aws_cloud',
            $cloud_context
          ),
          'Value' => $uid,
        ],
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the VPC of mock data.
   *
   * @param int $index
   *   The index of VPC.
   */
  protected function deleteVpcMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeVpcs']['Vpcs'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the internet gateway of mock data.
   *
   * @param int $index
   *   The index of internet gateway.
   */
  protected function deleteInternetGatewayMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeInternetGateways']['InternetGateways'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the carrier gateway of mock data.
   *
   * @param int $index
   *   The index of carrier gateway.
   */
  protected function deleteCarrierGatewayMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeCarrierGateways']['CarrierGateways'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the transit gateway of mock data.
   *
   * @param int $index
   *   The index of transit gateway.
   */
  protected function deleteTransitGatewayMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeTransitGateways']['TransitGateways'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the VPC peering connection of mock data.
   *
   * @param int $index
   *   The index of VPC.
   */
  protected function deleteVpcPeeringConnectionMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeVpcPeeringConnections']['VpcPeeringConnections'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the subnet of mock data.
   *
   * @param int $index
   *   The index of subnet.
   */
  protected function deleteSubnetMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeSubnets']['Subnets'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update create subnet in mock data.
   *
   * @param string $subnet_id
   *   Subnet ID.
   */
  protected function updateCreateSubnetMockData(string $subnet_id): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['CreateSubnet']['Subnet']['SubnetId'] = $subnet_id;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update internet gateway mock data.
   *
   * @param int $internet_gateway_index
   *   The index of internet gateway.
   * @param string $name
   *   The internet gateway name.
   * @param string $vpc_id
   *   The vpc ID.
   */
  protected function updateInternetGatewayMockData(
    int $internet_gateway_index,
    string $name,
    string $vpc_id,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeInternetGateways']['InternetGateways'][$internet_gateway_index]['InternetGatewayId'] = $name;
    $mock_data['DescribeInternetGateways']['InternetGateways'][$internet_gateway_index]['Attachments']['State'] = empty($vpc_id)
      ? 'detached'
      : 'available';
    $mock_data['DescribeInternetGateways']['InternetGateways'][$internet_gateway_index]['Attachments']['VpcId'] = $vpc_id;

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update the internet gateway state.
   *
   * @param string $api
   *   The API data.
   * @param string $internet_gateway_id
   *   The internet_gateway_id.
   * @param string $vpc_id
   *   The vpc_id.
   * @param string $state
   *   The state.
   */
  protected function updateAttachDetachInternetGatewayMockData(
    string $api,
    string $internet_gateway_id,
    string $vpc_id,
    string $state,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data[$api] = [
      'VpcId' => $vpc_id,
      'State' => $state,
      'InternetGatewayId' => $internet_gateway_id,
    ];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Modify the VPC of mock data.
   *
   * @param int $index
   *   The index of VPC.
   * @param array $vpc_data
   *   VPC data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function modifyVpcMockData(
    int $index,
    array $vpc_data,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeVpcs']['Vpcs'][$index]['Tags'] = [
      [
        'Key' => 'Name',
        'Value' => $vpc_data['name'],
      ],
      [
        'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        ),
        'Value' => $uid,
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update create VPC in mock data.
   *
   * @param string $vpc_id
   *   Vpc ID.
   */
  protected function updateCreateVpcMockData(string $vpc_id): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['CreateVpc']['Vpc']['VpcId'] = $vpc_id;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Modify the internet gateway of mock data.
   *
   * @param int $index
   *   The index of internet gateway.
   * @param array $internet_gateway_data
   *   Internet gateway data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function modifyInternetGatewayMockData(
    int $index,
    array $internet_gateway_data,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeInternetGateways']['InternetGateways'][$index]['Tags'] = [
      [
        'Key' => 'Name',
        'Value' => $internet_gateway_data['name'],
      ],
      [
        'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        ),
        'Value' => $uid,
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update create internet gateway in mock data.
   *
   * @param string $internet_gateway_id
   *   Internet gateway ID.
   */
  protected function updateCreateInternetGatewayMockData(string $internet_gateway_id): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['CreateInternetGateway']['InternetGateway']['InternetGatewayId'] = $internet_gateway_id;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Modify the carrier gateway of mock data.
   *
   * @param int $index
   *   The index of carrier gateway.
   * @param array $carrier_gateway_data
   *   Carrier gateway data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function modifyCarrierGatewayMockData(
    int $index,
    array $carrier_gateway_data,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeCarrierGateways']['CarrierGateways'][$index]['Tags'] = [
      [
        'Key' => 'Name',
        'Value' => $carrier_gateway_data['name'],
      ],
      [
        'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        ),
        'Value' => $uid,
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update create carrier gateway in mock data.
   *
   * @param string $carrier_gateway_id
   *   Carrier gateway ID.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function updateCreateCarrierGatewayMockData(
    string $carrier_gateway_id,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['CreateCarrierGateway']['CarrierGateway']['CarrierGatewayId'] = $carrier_gateway_id;
    $mock_data['CreateCarrierGateway']['CarrierGateway']['Tags'] = [
      [
        'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        ),
        'Value' => $uid,
      ],
    ];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Modify the transit gateway of mock data.
   *
   * @param int $index
   *   The index of transit gateway.
   * @param array $carrier_gateway_data
   *   Transit gateway data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function modifyTransitGatewayMockData(
    int $index,
    array $carrier_gateway_data,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeTransitGateways']['TransitGateways'][$index]['Tags'] = [
      [
        'Key' => 'Name',
        'Value' => $carrier_gateway_data['name'],
      ],
      [
        'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        ),
        'Value' => $uid,
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update create transit gateway in mock data.
   *
   * @param string $carrier_gateway_id
   *   Transit gateway ID.
   */
  protected function updateCreateTransitGatewayMockData(string $carrier_gateway_id): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['CreateTransitGateway']['TransitGateway']['TransitGatewayId'] = $carrier_gateway_id;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Modify the VPC of mock data.
   *
   * @param int $index
   *   The index of VPC peering connection.
   * @param array $vpc_peering_connection_data
   *   VPC peering connection data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function modifyVpcPeeringConnectionMockData(
    int $index,
    array $vpc_peering_connection_data,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeVpcPeeringConnections']['VpcPeeringConnections'][$index]['Tags'] = [
      [
        'Key' => 'Name',
        'Value' => $vpc_peering_connection_data['name'],
      ],
      [
        'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        ),
        'Value' => $uid,
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Modify the subnet of mock data.
   *
   * @param int $index
   *   The index of subnet.
   * @param array $subnet_data
   *   Subnet data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The user ID.
   */
  protected function modifySubnetMockData(
    int $index,
    array $subnet_data,
    string $cloud_context,
    int $uid,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeSubnets']['Subnets'][$index]['Tags'] = [
      [
        'Key' => 'Name',
        'Value' => $subnet_data['name'],
      ],
      [
        'Key' => \Drupal::service('cloud')->getTagKeyCreatedByUid(
          'aws_cloud',
          $cloud_context
        ),
        'Value' => $uid,
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describe snapshot in mock data.
   *
   * @param array $test_cases
   *   Test cases array.
   *
   * @throws \Exception
   */
  protected function updateDescribeSnapshotsMockData(array $test_cases): void {
    $random = $this->random;

    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeSnapshots'] = ['Snapshots' => []];
    foreach ($test_cases ?: [] as $test_case) {
      $snapshot = [
        'SnapshotId' => $test_case['id'],
        'VolumeSize' => 10,
        'Description' => $random->string(32, TRUE),
        'State' => 'completed',
        'VolumeId' => 'vol-' . $this->getRandomId(),
        'Progress' => '100%',
        'Encrypted' => TRUE,
        'KmsKeyId' => 'arn:aws:kms:us-east-1:123456789012:key/6876fb1b-example',
        'OwnerId' => (string) random_int(100000000000, 999999999999),
        'OwnerAlias' => 'amazon',
        'StateMessage' => $random->string(32, TRUE),
        'StartTime' => date('c'),
      ];

      if (isset($test_case['name'])) {
        $snapshot['Tags'] = [
          ['Key' => 'Name', 'Value' => $test_case['name']],
        ];
      }

      $mock_data['DescribeSnapshots']['Snapshots'][] = $snapshot;
    }

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update IAM roles to mock data.
   *
   * @param array $iam_roles
   *   The IAM roles.
   */
  protected function updateIamRolesMockData(array $iam_roles): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['ListInstanceProfiles']['InstanceProfiles'] = $iam_roles;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describe security groups in mock data.
   *
   * @param array $security_groups
   *   Security groups array.
   */
  protected function updateDescribeSecurityGroupsMockData(array $security_groups): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeSecurityGroups']['SecurityGroups'] = $security_groups;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update instance type in mock data.
   *
   * @param string $instance_type
   *   Instance type.
   */
  protected function updateInstanceTypeMockData(string $instance_type): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeInstances']['Reservations'][0]['Instances'][0]['InstanceType']
      = $instance_type;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update image creation in mock data.
   *
   * @param string $image_id
   *   Image ID.
   * @param string $image_name
   *   Image name.
   * @param string $image_state
   *   Image state.
   */
  protected function updateImageCreationMockData(
    string $image_id,
    string $image_name,
    string $image_state,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();
    $vars['image_id'] = $image_id;
    $vars['name'] = $image_name;
    $vars['state'] = $image_state;

    // Unset DescribeImages so that the state can be updated.
    unset(
      $mock_data['DescribeImages'],
      $mock_data['CreateImage']
    );

    $image_mock_data_content = $this->getMockDataFileContent(ImageTest::class, $vars);
    $image_mock_data = Yaml::decode($image_mock_data_content);

    $this->updateMockDataToConfig(array_merge($image_mock_data, $mock_data));
  }

  /**
   * Update schedule tag in mock data.
   *
   * @param string $schedule_value
   *   Schedule value.
   */
  protected function updateScheduleTagMockData(string $schedule_value): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeInstances']['Reservations'][0]['Instances'][0]['Tags'][0]['Name'] = 'Schedule';
    $mock_data['DescribeInstances']['Reservations'][0]['Instances'][0]['Tags'][0]['Value'] = $schedule_value;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update create volume in mock data.
   *
   * @param string $state
   *   Volume state.
   * @param string $volume_id
   *   Volume ID.
   */
  protected function updateCreateVolumeMockData(string $state, string $volume_id): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['CreateVolume']['State'] = $state;
    $mock_data['CreateVolume']['VolumeId'] = $volume_id;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update mock data related to security group rules.
   *
   * @param array $rules
   *   The security group rules.
   * @param int $rule_type
   *   The security group rule type.
   */
  protected function updateRulesMockData(array $rules, int $rule_type): void {
    $mock_data = $this->getMockDataFromConfig();

    $security_group = &$mock_data['DescribeSecurityGroups']['SecurityGroups'][0];
    $security_group['IpPermissions'] = [];
    $security_group['IpPermissionsEgress'] = [];
    foreach ($rules ?: [] as $rule) {
      $permission_name = 'IpPermissions';
      if ($rule['type'] === $rule_type) {
        $permission_name = 'IpPermissionsEgress';
      }

      $permission = [
        'IpProtocol' => 'tcp',
        'FromPort' => $rule['from_port'],
        'ToPort' => $rule['to_port'],
      ];

      if ($rule['source'] === 'ip4') {
        $permission['IpRanges'] = [
          [
            'CidrIp' => $rule['cidr_ip'],
            'Description' => $rule['description'],
          ],
        ];
      }
      elseif ($rule['source'] === 'ip6') {
        $permission['Ipv6Ranges'] = [
          [
            'CidrIpv6' => $rule['cidr_ip_v6'],
            'Description' => $rule['description'],
          ],
        ];
      }
      elseif ($rule['source'] === 'group') {
        $permission['UserIdGroupPairs'] = [
          [
            'UserId' => $rule['user_id'],
            'GroupId' => $rule['group_id'],
            'VpcId' => $rule['vpc_id'],
            'VpcPeeringConnectionId' => $rule['peering_connection_id'],
            'PeeringStatus' => $rule['peering_status'],
            'Description' => $rule['description'],
          ],
        ];
      }

      $security_group[$permission_name][] = $permission;
    }

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add describe snapshots in mock data.
   *
   * @param string $snapshot_id
   *   The snapshot ID.
   */
  protected function addDescribeSnapshotsMockData(string $snapshot_id): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeSnapshots'] = [
      'Snapshots' => [
        [
          'SnapshotId' => $snapshot_id,
        ],
      ],
    ];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describe volumes in mock data.
   *
   * @param array $volumes
   *   Volumes array.
   */
  protected function updateDescribeVolumesMockData(array $volumes): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeVolumes']['Volumes'] = $volumes;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update VPCs and subnets to mock data.
   *
   * @param array $vpcs
   *   The VPCs array.
   * @param array $subnets
   *   The subnets array.
   */
  protected function updateVpcsAndSubnetsMockData(array $vpcs, array $subnets): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeVpcs']['Vpcs'] = $vpcs;
    $mock_data['DescribeSubnets']['Subnets'] = $subnets;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update DescribeVpcs in mock data.
   *
   * @param int $count
   *   The count of VPCs.
   *
   * @return array
   *   The IDs of VPCs.
   *
   * @throws \Exception
   */
  protected function updateVpcsMockData(int $count): array {
    $mock_data = $this->getMockDataFromConfig();
    $vpcs = [];
    $vpc_ids = [];
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $vpc_id = 'vpc-' . $this->getRandomId();
      $vpcs[] = [
        'VpcId' => $vpc_id,
        'Name' => sprintf('vpc-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
        'CidrBlock' => Utils::getRandomCidr(),
      ];

      $vpc_ids[] = $vpc_id;
    }

    $mock_data['DescribeVpcs']['Vpcs'] = $vpcs;
    $this->updateMockDataToConfig($mock_data);

    return $vpc_ids;
  }

  /**
   * Update tags in mock data.
   *
   * @param int $index
   *   The index to update tags.
   * @param string $entity_name
   *   The entity name in mock data.
   * @param string $key
   *   The tag key.
   * @param string $value
   *   The tag's value.
   * @param bool $delete_flg
   *   Whether delete or not.
   * @param string $tag_name
   *   The tag's name.
   */
  protected function updateTagsInMockData(
    int $index,
    string $entity_name,
    string $key,
    string $value,
    bool $delete_flg = FALSE,
    string $tag_name = 'Tags',
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $describe_name = 'Describe' . $entity_name;
    $data = &$mock_data[$describe_name][$entity_name][$index];

    if ($delete_flg) {
      if (isset($data[$tag_name])) {
        foreach ($data[$tag_name] ?: [] as $idx => $tag) {
          if ($tag['Key'] === $key) {
            unset($data[$tag_name][$idx]);
          }
        }
      }
    }
    else {
      if (!isset($data[$tag_name])) {
        $data[$tag_name] = [];
      }
      else {
        foreach ($data[$tag_name] ?: [] as $idx => $tag) {
          if ($tag['Key'] === $key) {
            unset($data[$tag_name][$idx]);
          }
        }
      }
      $data[$tag_name][] = ['Key' => $key, 'Value' => $value];
    }

    $this->updateMockDataToConfig($mock_data);

  }

  /**
   * Update tags of the interface in mock data.
   *
   * @param int $index
   *   The interface's index.
   * @param string $key
   *   The tag key.
   * @param string $value
   *   The tag's value.
   * @param bool $delete_flg
   *   Whether delete or not.
   */
  protected function updateInstanceTagsInMockData(
    int $index,
    string $key,
    string $value,
    bool $delete_flg = FALSE,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $data = &$mock_data['DescribeInstances']['Reservations'][0]['Instances'][$index];

    if ($delete_flg) {
      if (isset($data['Tags'])) {
        foreach ($data['Tags'] ?: [] as $idx => $tag) {
          if ($tag['Key'] === $key) {
            unset($data['Tags'][$idx]);
          }
        }
      }
    }
    else {
      if (!isset($data['Tags'])) {
        $data['Tags'] = [];
      }
      else {
        foreach ($data['Tags'] ?: [] as $idx => $tag) {
          if ($tag['Key'] === $key) {
            unset($data['Tags'][$idx]);
          }
        }
      }
      $data['Tags'][] = ['Key' => $key, 'Value' => $value];
    }

    $this->updateMockDataToConfig($mock_data);

  }

  /**
   * Update VPCs and subnets to mock data.
   *
   * @param array $subnets
   *   The subnets array.
   */
  protected function updateSubnetsToMockData(array $subnets): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeSubnets']['Subnets'] = $subnets;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Aws Cloud Config Permission mock data with a random error.
   *
   * @param array $commands
   *   Array of commands to update mocked result.
   * @param bool $conditional_errors
   *   TRUE to set a random conditional message: an error for mandatory
   *   permission and warning for optional one. FALSE to set a random error
   *   in both cases.
   */
  protected function updateAwsCloudConfigPermissionMockData(array $commands = [], bool $conditional_errors = FALSE): void {

    $mock_data = $this->getMockDataFromConfig();
    $error_array = $conditional_errors ? [
      'UnauthorizedOperation',
      'AccessDenied',
    ] : [
      'AssumeRoleDenied',
      'AuthFailure',
    ];

    foreach ($commands as $command) {
      if (!empty($mock_data[$command])) {
        $mock_data[$command]['ErrorCode'] = $error_array[array_rand($error_array)];
      }
    }
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Aws error code mock data.
   *
   * @param array $commands
   *   Array of commands to update mocked result.
   * @param string|null $error_code
   *   An error code to set. If not specified, a random permission error is set.
   *   If an empty string is set, it can suppress AwsException in mockHandler.
   */
  protected function updateErrorCodeMockData(array $commands = [], ?string $error_code = NULL): void {

    $mock_data = $this->getMockDataFromConfig();
    foreach ($commands as $command) {
      if (!empty($mock_data[$command])) {
        // This $error_code is used for AwsException error code.
        // If $error_code is NULL, a random permission error code is set.
        // If $error_code is an empty string, set an empty string NOT to throw
        // AwsException in mockHandler.
        $mock_data[$command]['ErrorCode'] = $error_code
          ?? Ec2ServiceInterface::PERMISSION_ERROR_CODES[array_rand(
            Ec2ServiceInterface::PERMISSION_ERROR_CODES
          )];
      }
    }
    $this->updateMockDataToConfig($mock_data);
  }

}
