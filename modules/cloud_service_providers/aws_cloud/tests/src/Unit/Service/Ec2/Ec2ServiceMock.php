<?php

namespace Drupal\Tests\aws_cloud\Unit\Service\Ec2;

use Aws\ResultInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2Service;

/**
 * Mock class for Ec2Service.
 */
class Ec2ServiceMock extends Ec2Service {

  /**
   * {@inheritdoc}
   */
  public function describeAvailabilityZones(array $params = [], array $credentials = []): ResultInterface {
    return $this->execute('DescribeAvailabilityZones', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeInstances(array $params = [], array $credentials = []): ResultInterface {
    return $this->execute('DescribeInstances', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeImages(array $params = [], $credentials = []): ResultInterface {
    return $this->execute('DescribeImages', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  public function describeAccountAttributes(array $params = [], $credentials = []): ResultInterface {
    return $this->execute('DescribeAccountAttributes', $params, $credentials);
  }

  /**
   * {@inheritdoc}
   */
  private function execute($operation, array $params = [], array $credentials = []): ResultInterface {

    $ec2_client = $this->getEc2Client($credentials);
    $command = $ec2_client->getCommand($operation, $params);
    return $ec2_client->execute($command);
  }

}
