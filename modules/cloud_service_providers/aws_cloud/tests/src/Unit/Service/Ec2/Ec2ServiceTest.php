<?php

namespace Drupal\Tests\aws_cloud\Unit\Service\Ec2;

use Aws\Api\DateTimeResult;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\aws_cloud\Traits\AwsCloudTestMockTrait;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudCacheServiceInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Tests AWS Cloud service.
 *
 * @group AWS Cloud
 */
class Ec2ServiceTest extends UnitTestCase {

  use AwsCloudTestMockTrait;

  /**
   * Ec2ServiceMock object.
   *
   * @var \Drupal\Tests\aws_cloud\Unit\Service\Ec2\Ec2ServiceMock
   */
  private Ec2ServiceMock $service;

  /**
   * Creating random data utility.
   *
   * @var \Drupal\Component\Utility\Random
   */
  private $random;

  /**
   * {@inheritdoc}
   */
  protected function getCredentials(): array {
    return [
      'region' => 'us-west-2',
      'version' => 'latest',
      'env' => [
        'access_key' => $this->random->name(20, TRUE),
        'secret_key' => $this->random->name(40, TRUE),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEc2ServiceMock($response_data): Ec2ServiceMock {
    // Create messenger, logger.factory and string_translation container.
    $container = new ContainerBuilder();

    // Messenger.
    $mock_messenger = $this->createMock(Messenger::class);

    // Queue Factory.
    $mock_queue_factory = $this->createMock(QueueFactory::class);

    // Logger.
    $mock_logger = $this->createMock(LoggerChannelInterface::class);
    $mock_logger_factory = $this->createMock(LoggerChannelFactoryInterface::class);
    $mock_logger_factory
      ->method('get')
      ->willReturn($mock_logger);

    // Set containers.
    $container->set('messenger', $mock_messenger);
    $container->set('logger.factory', $mock_logger_factory);
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    $ec2_service = new Ec2ServiceMock(
      $this->createMock(EntityTypeManagerInterface::class),
      $this->getConfigFactoryStub([
        'aws_cloud.settings' => [
          'aws_cloud_test_mode' => TRUE,
          // $response_data must be an array object.
          'aws_cloud_mock_data' => json_encode($response_data),
        ],
      ]),
      $this->createMock(AccountInterface::class),
      $this->createMock(CloudConfigPluginManagerInterface::class),
      $this->createMock(FieldTypePluginManagerInterface::class),
      $this->createMock(EntityFieldManagerInterface::class),
      $this->createMock(LockBackendInterface::class),
      $mock_queue_factory,
      $this->createMock(Client::class),
      $this->createMock(CloudServiceInterface::class),
      $this->createMock(CloudCacheServiceInterface::class),
      $this->createMock(ModuleHandlerInterface::class),
      new RequestStack()
    );
    $container->set('aws_cloud.ec2', $ec2_service);

    return $ec2_service;
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->random = $this->getRandomGenerator();
  }

  /**
   * Testing get Availability Zones.
   *
   * @throws \JsonException
   */
  public function testGetAvailabilityZones(): void {
    self::assertSame(1, 1);
    $zones = [
      $this->random->name(8, TRUE),
      $this->random->name(8, TRUE),
      $this->random->name(8, TRUE),
    ];

    $response_zones = [
      'DescribeAvailabilityZones' => [
        'AvailabilityZones' => array_map(static function ($zone) {
          return ['ZoneName' => $zone];
        }, $zones),
      ],
    ];

    $ec2_service = $this->getEc2ServiceMock($response_zones);

    $expected_result = array_combine($zones, $zones);
    $actual_result = $ec2_service->getAvailabilityZones(FALSE, $this->getCredentials());
    self::assertSame($expected_result, $actual_result);
  }

  /**
   * Test addMockHandler() by calling DescribeAccountAttributes.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   Throws Ec2ServiceException.
   */
  public function testAddMockHandlerByDescribeAccountAttributes(): void {

    // 1. Test Ec2Service::describeAccountAttributes();
    $response_data = [
      [
        // 1-1. DescribeAccountAttributes w/o mock data.
        'DescribeAccountAttributes' => [],
      ], [
        // 1-2. No mock data.
        // No command such as DescribeAccountAttributes and no response data.
        [],
      ], [
        // 1-3. DescribeInstances w/ mock data. (Different command name)
        'DescribeInstances' => [
          'Reservations' => [
            [
              'Instances' => [],
            ],
          ],
        ],
      ], [
        // 1-4. DescribeImages w/o mock data.  (Different command name)
        'DescribeImages' => [],
      ],
    ];

    foreach ($response_data as $response) {
      $ec2_service = $this->getEc2ServiceMock($response);
      $result = $ec2_service->describeAccountAttributes([], $this->getCredentials());

      $expected_result = [
        [
          'AttributeName' => 'supported-platforms',
          'AttributeValues' => [
            [
              'AttributeValue' => 'VPC',
            ],
          ],
        ],
      ];
      $actual_result = $result['AccountAttributes'];

      // Expect exact match for the input and output as
      // 'AttributeValue' => 'VPC'.
      self::assertSame($expected_result, $actual_result);
    }

    $response_data = [
      [
        // 1-5. DescribeAccountAttributes w/ mock data (VPC).
        'DescribeAccountAttributes' => [
          'AccountAttributes' => [
            [
              'AttributeName' => 'supported-platforms',
              'AttributeValues' => [
                [
                  'AttributeValue' => 'VPC',
                ],
              ],
            ],
          ],
        ],
      ], [
        // 1-6. DescribeAccountAttributes w/ mock data (EC2).
        'DescribeAccountAttributes' => [
          'AccountAttributes' => [
            [
              'AttributeName' => 'supported-platforms',
              'AttributeValues' => [
                [
                  'AttributeValue' => 'EC2',
                ],
              ],
            ],
          ],
        ],
      ],
    ];

    foreach ($response_data as $response) {
      $ec2_service = $this->getEc2ServiceMock($response);
      $result = $ec2_service->describeAccountAttributes([], $this->getCredentials());

      $expected_result = $response['DescribeAccountAttributes']['AccountAttributes'];
      $actual_result = $result['AccountAttributes'];

      // Verify input and output array for exact match.
      self::assertSame($expected_result, $actual_result);
    }

    // 2. Test Ec2Service::getSupportedPlatforms();
    $response_data = [
      [
        // 2-1. DescribeAccountAttributes w/ mock data.
        'DescribeAccountAttributes' => [],
      ],
      [
        // 2-2. DescribeAccountAttributes w/o mock data.
        [],
      ], [
        // 2-3. DescribeInstances w/ mock data.
        'DescribeInstances' => [
          'Reservations' => [
            [
              'Instances' => [],
            ],
          ],
        ],
      ], [
        // 2-4. DescribeImages w/o mock data.
        'DescribeImages' => [],
        // 2-5. DescribeAccountAttributes w/ mock data (VPC).
        'DescribeAccountAttributes' => [
          'AccountAttributes' => [
            [
              'AttributeName' => 'supported-platforms',
              'AttributeValues' => [
                [
                  'AttributeValue' => 'VPC',
                ],
              ],
            ],
          ],
        ],
      ],
    ];

    foreach ($response_data as $response) {
      $ec2_service = $this->getEc2ServiceMock($response);
      $result = $ec2_service->getSupportedPlatforms($this->getCredentials());

      // Verify Platform string which should be 'VPC'.
      $expected_result = 'VPC';
      $actual_result = $result[0];
      self::assertSame($expected_result, $actual_result);
    }

    $response_data = [
      // 2-6. DescribeAccountAttributes w/ mock data (EC2).
      'DescribeAccountAttributes' => [
        'AccountAttributes' => [
          [
            'AttributeName' => 'supported-platforms',
            'AttributeValues' => [
              [
                'AttributeValue' => 'EC2',
              ],
            ],
          ],
        ],
      ],
    ];

    $ec2_service = $this->getEc2ServiceMock($response_data);
    $result = $ec2_service->getSupportedPlatforms($this->getCredentials());

    // Verify Platform string which should be 'EC2'.
    $expected_result = 'EC2';
    $actual_result = $result[0];
    self::assertSame($expected_result, $actual_result);
  }

  /**
   * Test addMockHandler() by calling DescribeInstances.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   Throws Ec2ServiceException.
   */
  public function testAddMockHandlerByDescribeInstances(): void {

    // 1. DescribeInstances w/ mock data
    $date_time = new DateTimeResult();
    $response_data = [
      [
        // 1-1. DescribeInstances w/ mock data (InstanceId only).
        'DescribeInstances' => [
          'Reservations' => [
            [
              'Instances' => [
                [
                  'InstanceId' => "i-{$this->random->name(20, TRUE)}",
                ],
              ],
            ],
          ],
        ],
      ], [
        // 1-2. DescribeInstances w/ mock data (LaunchTime).
        'DescribeInstances' => [
          'Reservations' => [
            [
              'Instances' => [
                [
                  'InstanceId' => "i-{$this->random->name(20, TRUE)}",
                  'LaunchTime' => $date_time,
                ],
              ],
            ],
          ],
        ],
      ], [
        // 1-3. DescribeInstances w/ mock data (No LaunchTime).
        'DescribeInstances' => [
          'Reservations' => [
            [
              'Instances' => [
                [
                  'InstanceId' => "i-{$this->random->name(20, TRUE)}",
                  'LaunchTime' => '',
                ],
              ],
            ],
          ],
        ],
      ],
    ];

    foreach ($response_data as $response) {
      $ec2_service = $this->getEc2ServiceMock($response);
      $result = $ec2_service->describeInstances([], $this->getCredentials());

      $response['DescribeInstances']['Reservations'][0]['Instances'][0]['LaunchTime']
        = !empty($response['DescribeInstances']['Reservations'][0]['Instances'][0])
          && !empty($response['DescribeInstances']['Reservations'][0]['Instances'][0]['LaunchTime'])
          ? new DateTimeResult($date_time)
          : new DateTimeResult();

      // Verify LaunchTime.
      $expected_result = $response['DescribeInstances']['Reservations'][0]['Instances'][0]['LaunchTime']->jsonSerialize();
      $actual_result = $result['Reservations'][0]['Instances'][0]['LaunchTime']->jsonSerialize();
      self::assertSame($expected_result, $actual_result);
    }

    // 2. DescribeInstances w/o mock data
    // 2-1. No mock data at all.
    $response_data = [];
    $ec2_service = $this->getEc2ServiceMock($response_data);

    $expected_result = [
      'effectiveUri' => 'https://ec2.us-west-2.amazonaws.com',
      'statusCode' => 200,
      'transferStats' => [
        'http' => [
          [],
        ],
      ],
    ];

    $actual_result = $ec2_service->describeInstances([], $this->getCredentials());
    self::assertSame($expected_result, $actual_result['@metadata']);

    // 2-2. No "Instances".
    $response_data = [
      'DescribeInstances' => [
        'Reservations' => [
          [
            'Instances' => [],
          ],
        ],
      ],
    ];

    $ec2_service = $this->getEc2ServiceMock($response_data);
    $result = $ec2_service->describeInstances([], $this->getCredentials());

    $expected_result = $response_data['DescribeInstances']['Reservations'];
    $actual_result = $result['Reservations'];
    self::assertSame($expected_result, $actual_result);
  }

}
