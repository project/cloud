INTRODUCTION
============

- Cloud Cluster module enables to manage multiple Cloud Orchestrators.
  This is a subsidiary module of Cloud module, therefore the module does NOT
  work without Cloud module.

- See also [Cloud](https://drupal.org/project/cloud/) module.

REQUIREMENTS
============

- Drupal `10.x` or higher (The latest version of Drupal `10.x`)
- Cloud `8.x` or higher

INSTALLATION
============

- `composer require drupal/cloud`
