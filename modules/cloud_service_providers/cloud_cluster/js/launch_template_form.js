(function () {
  'use strict';

  function initOptions() {
    if (!document.querySelector('input[name=field_deployment_template]:checked')) {
      document.querySelector('input[name=field_deployment_template]').checked = true;
    }
  }

  let field_deployment_template_default_values = drupalSettings.cloud_cluster.field_deployment_template_default_values;
  document.querySelectorAll('input[name=field_deployment_template]').forEach(function (input) {
    if (!field_deployment_template_default_values.includes(input.value)) {
      input.closest('.form-type-radio,.js-form-type-radio').remove();
    }
  });

  document.querySelector('.field--name-field-deployment-template').style.display = 'block';

  initOptions();

})();
