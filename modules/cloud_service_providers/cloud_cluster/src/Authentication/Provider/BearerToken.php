<?php

namespace Drupal\cloud_cluster\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud_cluster\Traits\CloudClusterTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Bearer token based authentication provider.
 */
class BearerToken implements AuthenticationProviderInterface {

  use CloudClusterTrait;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Constructs a new bearer token authentication provider.
   *
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   */
  public function __construct(CloudConfigPluginManagerInterface $cloud_config_plugin_manager) {
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request): bool {
    $request_token = $this->getRequestToken($request);
    return !empty($request_token);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request): ?AccountInterface {
    $request_token = $this->getRequestToken($request);
    if (empty($request_token)) {
      return NULL;
    }

    $cloud_config = $this->getCloudConfigByRequestToken($this->cloudConfigPluginManager, $request_token);
    if (empty($cloud_config)) {
      return NULL;
    }

    return $cloud_config->field_access_user->entity;
  }

}
