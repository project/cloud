<?php

namespace Drupal\cloud_cluster\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Locale\CountryManagerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Url;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller responsible for Cloud Orchestrator Worker Location.
 */
class CloudClusterWorkerLocationController extends ControllerBase {

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloud;

  /**
   * CloudConfigLocationController constructor.
   *
   * @param \Drupal\Core\Locale\CountryManagerInterface $country_manager
   *   Country Manager.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   Route Provider.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud
   *   The cloud service.
   */
  public function __construct(
    CountryManagerInterface $country_manager,
    RouteProviderInterface $route_provider,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    CloudServiceInterface $cloud,
  ) {
    $this->countryManager = $country_manager;
    $this->routeProvider = $route_provider;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->cloud = $cloud;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The instance of ContainerInterface.
   *
   * @return CloudConfigLocationController
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('country_manager'),
      $container->get('router.route_provider'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('cloud')
    );
  }

  /**
   * Get Cloud Orchestrator Worker location.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a Cloud Orchestrator Worker location.
   */
  public function getCloudClusterWorkerLocation($cloud_context) {
    $locations = [];
    $country_allowed_values = CountryManager::getStandardList();
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

    $workers = $this->entityTypeManager()
      ->getStorage('cloud_cluster_worker')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);
    foreach ($workers ?: [] as $worker) {
      if (empty($worker)) {
        continue;
      }

      if (empty($worker->getLocationCountry())
        || empty($worker->getLocationCity())
        || empty($worker->getLocationLatitude())
        || empty($worker->getLocationLongitude())) {
        continue;
      }

      $region_id = $worker->getLocationLatitude() . '|' . $worker->getLocationLongitude();
      if (empty($locations[$region_id])) {
        $locations[$region_id] = [
          'Type' => 'cloud_cluster',
          'Country' => $country_allowed_values[$worker->getLocationCountry()],
          'City' => $worker->getLocationCity(),
          'Latitude' => $worker->getLocationLatitude(),
          'Longitude' => $worker->getLocationLongitude(),
        ];
      }

      $location = &$locations[$region_id];
      $image_url = '';
      try {
        // Add icon url.
        $file_target = $cloud_config->getIconFid();
        $fileStorage = $this->entityTypeManager->getStorage('file');
        $image = $fileStorage->load($file_target);
        $imageStyleStorage = $this->entityTypeManager->getStorage('image_style');
        $image_url = $imageStyleStorage->load('icon_32x32')
          ->buildUrl($image->uri->value);
      }
      catch (\Exception $e) {
        $this->cloud->handleException($e);
      }

      $location['Items'][] = [
        'Name' => $worker->getName(),
        'Url' => Url::fromRoute('entity.cloud_cluster_worker.canonical', [
          'cloud_context' => $cloud_config->getCloudContext(),
          'cloud_cluster_worker' => $worker->id(),
        ])->toString(),
        'Image' => $image_url,
      ];
    }

    $response = array_values($locations);
    return new JsonResponse(array_values($response));
  }

}
