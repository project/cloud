<?php

namespace Drupal\cloud_cluster\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;

/**
 * Provides a form element for entity selection box.
 *
 * @FormElement("entity_select")
 */
class EntitySelect extends Select {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#process' => [
        [static::class, 'processEntitySelect'],
        [Select::class, 'processAjaxForm'],
      ],
    ] + parent::getInfo();
  }

  /**
   * Processes a select list form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @see _form_validate()
   */
  public static function processEntitySelect(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    // Load entities.
    $entities = \Drupal::entityTypeManager()
      ->getStorage($element['#entity_type'])
      ->loadByProperties([
        'cloud_context' => $element['#cloud_context'],
      ]);

    $options = [];
    foreach ($entities as $entity) {
      $options[$entity->get($element['#entity_key'])->value] = $entity->label();
    }

    $element['#options'] = $options;
    return Select::processSelect($element, $form_state, $complete_form);
  }

}
