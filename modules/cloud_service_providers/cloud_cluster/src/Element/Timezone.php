<?php

namespace Drupal\cloud_cluster\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;

/**
 * Provides a form element for timezone selection box.
 *
 * @FormElement("timezone")
 */
class Timezone extends Select {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#process' => [
        [static::class, 'processTimezone'],
        [Select::class, 'processAjaxForm'],
      ],
    ] + parent::getInfo();
  }

  /**
   * Processes a select list form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @see _form_validate()
   */
  public static function processTimezone(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    $zones = \DateTimeZone::listIdentifiers();
    $element['#options'] = array_combine($zones, $zones);
    return Select::processSelect($element, $form_state, $complete_form);
  }

}
