<?php

namespace Drupal\cloud_cluster\Entity\CloudClusterSite;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud\Entity\CloudLaunchTemplate;
use Drupal\cloud_cluster\Entity\CloudClusterEntityBase;

/**
 * Defines the Cloud Cluster Site entity.
 *
 * @ingroup cloud_cluster
 *
 * @ContentEntityType(
 *   id = "cloud_cluster_site",
 *   id_plural = "cloud_cluster_sites",
 *   label = @Translation("Cloud Orchestrator Site"),
 *   label_collection = @Translation("Cloud Orchestrator Sites"),
 *   label_singular = @Translation("Cloud Orchestrator Site"),
 *   label_plural = @Translation("Cloud Orchestrator Sites"),
 *   handlers = {
 *     "view_builder" = "Drupal\cloud_cluster\Entity\CloudClusterSite\CloudClusterSiteViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\cloud_cluster\Entity\CloudClusterSite\CloudClusterSiteViewsData",
 *     "access"       = "Drupal\cloud_cluster\Controller\CloudClusterSiteAccessControlHandler",
 *     "form" = {
 *       "delete"     = "Drupal\cloud_cluster\Form\CloudClusterSiteDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cloud_cluster_site",
 *   admin_permission = "administer cloud cluster site",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical"  = "/clouds/cloud_cluster/{cloud_context}/site/{cloud_cluster_site}",
 *     "collection" = "/clouds/cloud_cluster/{cloud_context}/site",
 *     "delete-form" = "/clouds/cloud_cluster/{cloud_context}/site/{cloud_cluster_site}/delete",
 *   },
 *   field_ui_base_route = "cloud_cluster_site.settings",
 *   constraints = {
 *     "CloudClusterSiteConstraint" = {}
 *   }
 * )
 */
class CloudClusterSite extends CloudClusterEntityBase implements CloudClusterSiteInterface {

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): CloudClusterSiteInterface {
    return $this->set('status', $status);
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl(): ?string {
    return $this->get('url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUrl($url): CloudClusterSiteInterface {
    return $this->set('url', $url);
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetCloudContext(): ?string {
    return $this->get('target_cloud_context')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetCloudContext($target_cloud_context): CloudClusterSiteInterface {
    return $this->set('target_cloud_context', $target_cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function getCloudLaunchTemplate(): CloudLaunchTemplate {
    return $this->get('cloud_launch_template')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudLaunchTemplate($cloud_launch_template): CloudClusterSiteInterface {
    return $this->set('cloud_launch_template', $cloud_launch_template);
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters(): array {
    return $this->get('parameters')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setParameters(array $parameters): CloudClusterSiteInterface {
    return $this->set('parameters', $parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function getCloudResources(): array {
    return $this->get('cloud_resources')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudResources(array $cloud_resources): CloudClusterSiteInterface {
    return $this->set('cloud_resources', $cloud_resources);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = CloudClusterEntityBase::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of site.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('URL'))
      ->setDescription(t('The URL of site.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => -5,
      ]);

    $fields['target_cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Target cloud service provider ID'))
      ->setDescription(t('A unique ID for the target cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'cloud_id_formatter',
        'weight' => -5,
      ]);

    $fields['cloud_launch_template'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Launch template'))
      ->setDescription(t('The launch template of site.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'cloud_launch_template')
      ->setSetting('handler', 'default:cloud_launch_template')
      ->setSetting('handler_settings', [
        'auto_create' => FALSE,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => -5,
      ]);

    $fields['parameters'] = BaseFieldDefinition::create('key_value')
      ->setLabel(t('Parameters'))
      ->setDescription(t('Parameters used to deploy the site.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'weight' => -5,
      ]);

    $fields['cloud_resources'] = BaseFieldDefinition::create('key_value')
      ->setLabel(t('Cloud resources'))
      ->setDescription(t('Cloud resources linked to the site.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'type' => 'cloud_resource_key_value_formatter',
        'weight' => -5,
      ]);

    return $fields;
  }

}
