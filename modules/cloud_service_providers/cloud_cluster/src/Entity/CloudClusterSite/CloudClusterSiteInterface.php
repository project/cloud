<?php

namespace Drupal\cloud_cluster\Entity\CloudClusterSite;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\cloud\Entity\CloudLaunchTemplate;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Cloud Cluster Site entity.
 *
 * @ingroup cloud_cluster
 */
interface CloudClusterSiteInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): CloudClusterSiteInterface;

  /**
   * {@inheritdoc}
   */
  public function getUrl(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setUrl($url): CloudClusterSiteInterface;

  /**
   * {@inheritdoc}
   */
  public function getTargetCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setTargetCloudContext($target_cloud_context): CloudClusterSiteInterface;

  /**
   * {@inheritdoc}
   */
  public function getCloudLaunchTemplate(): CloudLaunchTemplate;

  /**
   * {@inheritdoc}
   */
  public function setCloudLaunchTemplate($cloud_launch_template): CloudClusterSiteInterface;

  /**
   * {@inheritdoc}
   */
  public function getParameters(): array;

  /**
   * {@inheritdoc}
   */
  public function setParameters(array $parameters): CloudClusterSiteInterface;

  /**
   * {@inheritdoc}
   */
  public function getCloudResources();

  /**
   * {@inheritdoc}
   */
  public function setCloudResources(array $cloud_resources): CloudClusterSiteInterface;

}
