<?php

namespace Drupal\cloud_cluster\Entity\CloudClusterSite;

use Drupal\cloud\Entity\CloudViewBuilder;

/**
 * Provides the cloud cluster site view builders.
 */
class CloudClusterSiteViewBuilder extends CloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'cloud_cluster_site',
        'title' => $this->t('Cloud Orchestrator Site'),
        'open' => TRUE,
        'fields' => [
          'name',
          'status',
          'url',
          'target_cloud_context',
          'cloud_launch_template',
          'created',
          'parameters',
          'cloud_resources',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
