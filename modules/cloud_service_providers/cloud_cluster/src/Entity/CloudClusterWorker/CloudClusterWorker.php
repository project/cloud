<?php

namespace Drupal\cloud_cluster\Entity\CloudClusterWorker;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud_cluster\Entity\CloudClusterEntityBase;

/**
 * Defines the Cloud Cluster Worker entity.
 *
 * @ingroup cloud_cluster
 *
 * @ContentEntityType(
 *   id = "cloud_cluster_worker",
 *   id_plural = "cloud_cluster_workers",
 *   label = @Translation("Cloud Orchestrator Worker"),
 *   label_collection = @Translation("Cloud Orchestrator Workers"),
 *   label_singular = @Translation("Cloud Orchestrator Worker"),
 *   label_plural = @Translation("Cloud Orchestrator Workers"),
 *   handlers = {
 *     "view_builder" = "Drupal\cloud_cluster\Entity\CloudClusterWorker\CloudClusterWorkerViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\cloud_cluster\Entity\CloudClusterWorker\CloudClusterWorkerViewsData",
 *     "access"       = "Drupal\cloud_cluster\Controller\CloudClusterWorkerAccessControlHandler",
 *     "form" = {
 *       "delete"     = "Drupal\cloud_cluster\Form\CloudClusterWorkerDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cloud_cluster_worker",
 *   admin_permission = "administer cloud cluster worker",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical"  = "/clouds/cloud_cluster/{cloud_context}/worker/{cloud_cluster_worker}",
 *     "collection" = "/clouds/cloud_cluster/{cloud_context}/worker",
 *     "delete-form" = "/clouds/cloud_cluster/{cloud_context}/worker/{cloud_cluster_worker}/delete",
 *   },
 *   field_ui_base_route = "cloud_cluster_worker.settings",
 *   constraints = {
 *     "CloudClusterWorkerConstraint" = {}
 *   }
 * )
 */
class CloudClusterWorker extends CloudClusterEntityBase implements CloudClusterWorkerInterface {

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): CloudClusterWorkerInterface {
    return $this->set('status', $status);
  }

  /**
   * {@inheritdoc}
   */
  public function getLocationCountry(): ?string {
    return $this->get('location_country')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocationCountry($location_country): CloudClusterWorkerInterface {
    return $this->set('location_country', $location_country);
  }

  /**
   * {@inheritdoc}
   */
  public function getLocationCity(): ?string {
    return $this->get('location_city')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocationCity($location_city): CloudClusterWorkerInterface {
    return $this->set('location_city', $location_city);
  }

  /**
   * {@inheritdoc}
   */
  public function getLocationLatitude():?float {
    return $this->get('location_latitude')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocationLatitude($location_latitude): CloudClusterWorkerInterface {
    return $this->set('location_latitude', $location_latitude);
  }

  /**
   * {@inheritdoc}
   */
  public function getLocationLongitude(): ?float {
    return $this->get('location_longitude')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocationLongitude($location_longitude): CloudClusterWorkerInterface {
    return $this->set('location_longitude', $location_longitude);
  }

  /**
   * {@inheritdoc}
   */
  public function getTags(): array {
    return $this->get('tags')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): CloudClusterWorkerInterface {
    return $this->set('tags', $tags);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = CloudClusterEntityBase::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of worker.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['location_country'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Location Country'))
      ->setDescription(t('The country of location.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['location_city'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Location City'))
      ->setDescription(t('The city of location.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['location_latitude'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Location Latitude'))
      ->setDescription(t('The latitude of location.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['location_longitude'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Location Longitude'))
      ->setDescription(t('The longitude of location.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['tags'] = BaseFieldDefinition::create('entity_reference')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Tags'))
      ->setDescription(t('The tags of worker.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['cloud_cluster_tags' => 'cloud_cluster_tags'],
        'auto_create' => TRUE,
        'auto_create_bundle' => 'cloud_cluster_tags',
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => -5,
      ]);

    return $fields;
  }

}
