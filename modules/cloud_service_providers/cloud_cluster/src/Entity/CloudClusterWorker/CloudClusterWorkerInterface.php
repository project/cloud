<?php

namespace Drupal\cloud_cluster\Entity\CloudClusterWorker;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Cloud Cluster Worker entity.
 *
 * @ingroup cloud_cluster
 */
interface CloudClusterWorkerInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): CloudClusterWorkerInterface;

  /**
   * {@inheritdoc}
   */
  public function getLocationCountry(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setLocationCountry($location_country): CloudClusterWorkerInterface;

  /**
   * {@inheritdoc}
   */
  public function getLocationCity(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setLocationCity($location_city): CloudClusterWorkerInterface;

  /**
   * {@inheritdoc}
   */
  public function getLocationLatitude(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setLocationLatitude($location_latitude): CloudClusterWorkerInterface;

  /**
   * {@inheritdoc}
   */
  public function getLocationLongitude(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setLocationLongitude($location_longitude): CloudClusterWorkerInterface;

  /**
   * {@inheritdoc}
   */
  public function getTags(): array;

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): CloudClusterWorkerInterface;

}
