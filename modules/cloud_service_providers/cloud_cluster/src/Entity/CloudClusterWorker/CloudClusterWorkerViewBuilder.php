<?php

namespace Drupal\cloud_cluster\Entity\CloudClusterWorker;

use Drupal\cloud\Entity\CloudViewBuilder;

/**
 * Provides the cloud cluster worker view builders.
 */
class CloudClusterWorkerViewBuilder extends CloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'cloud_cluster_worker',
        'title' => $this->t('Cloud Orchestrator Worker'),
        'open' => TRUE,
        'fields' => [
          'name',
          'status',
          'tags',
          'created',
        ],
      ],
      [
        'name' => 'location',
        'title' => $this->t('Location'),
        'open' => TRUE,
        'fields' => [
          'location_country',
          'location_city',
          'location_latitude',
          'location_longitude',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
