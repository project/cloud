<?php

namespace Drupal\cloud_cluster\Entity\CloudClusterWorker;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the Cloud Cluster Worker entity type.
 */
class CloudClusterWorkerViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    $base_table = $this->entityType->getBaseTable() ?: $this->entityType->id();
    $data[$base_table]['cloud_config']['relationship'] = [
      'id' => 'standard',
      'title' => $this->t('Cloud Config'),
      'label' => $this->t('Cloud Config'),
      'group' => 'Kubernetes Pod',
      'help' => $this->t('Reference to cloud config'),
      'base' => 'cloud_config_field_data',
      'base field' => 'cloud_context',
      'relationship field' => 'cloud_context',
    ];

    return $data;
  }

}
