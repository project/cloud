<?php

namespace Drupal\cloud_cluster\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Form\CloudContentDeleteForm;

/**
 * Class CloudClusterSiteDeleteForm - Base Delete class.
 *
 * @package Drupal\cloud_cluster\Form
 */
class CloudClusterSiteDeleteForm extends CloudContentDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entity = $this->entity;
    $form_state->setRedirect("view.cloud_cluster_site.list", ['cloud_context' => $entity->getCloudContext()]);

    try {
      $resource_entities = [];
      foreach ($entity->getCloudResources() ?: [] as $resource) {
        $resource_entity = $this->entityTypeManager
          ->getStorage($resource['item_key'])
          ->load($resource['item_value']);
        if (empty($resource_entity)) {
          continue;
        }

        $resource_entities[] = $resource_entity;
      }

      $deployment_type = $entity->getCloudLaunchTemplate()->field_deployment_type->value;
      $service_name = "$deployment_type.cloud_orchestrator_manager";
      // @phpstan-ignore-next-line
      if (!\Drupal::hasService($service_name)) {
        $message = $this->t('The cloud orchestrator manager for %type could not be found.', [
          '%type' => $deployment_type,
        ]);
        $this->messenger->addError($message);
        return;
      }

      // @phpstan-ignore-next-line
      \Drupal::service($service_name)->undeploy($resource_entities);

      $entity->delete();

      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
    }
    catch (\Exception $e) {
      $this->processOperationErrorStatus($entity, 'deleted');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $message = '';
    foreach ($this->entity->getCloudResources() ?: [] as $resource) {
      $entity = $this->entityTypeManager
        ->getStorage($resource['item_key'])
        ->load($resource['item_value']);
      if (empty($entity)) {
        continue;
      }

      $type = $entity->getEntityType()->getSingularLabel();
      $label = $entity->id()
        ? $entity->toLink($entity->label())->toString()
        : $entity->label();

      $message .= "<li>$type: $label</li>";
    }

    if (!empty($message)) {
      $message = "<ul>$message</ul>";
    }

    return $this->t('This action cannot be undone.')
      . '<br>'
      . $this->t('The following resources will be deleted too.')
      . $message;

  }

}
