<?php

namespace Drupal\cloud_cluster\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Form\CloudContentDeleteForm;

/**
 * Class CloudClusterWorkerDeleteForm - Base Delete class.
 *
 * @package Drupal\cloud_cluster\Form
 */
class CloudClusterWorkerDeleteForm extends CloudContentDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entity = $this->entity;

    try {
      $entity->delete();

      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
    }
    catch (\Exception $e) {
      $this->processOperationErrorStatus($entity, 'deleted');
    }

    $form_state->setRedirect("view.cloud_cluster_worker.list", ['cloud_context' => $entity->getCloudContext()]);
  }

}
