<?php

namespace Drupal\cloud_cluster\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides plugin definitions for custom local menu.
 *
 * @see \Drupal\cloud_cluster\Plugin\Derivative\CloudClusterMenuLinks
 */
class CloudClusterMenuLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs new CloudClusterMenuLinks.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    ModuleHandlerInterface $module_handler,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    // Get all Cloud Cluster entities.
    $entities = $this->cloudConfigPluginManager->loadConfigEntities('cloud_cluster');
    $links = [];
    $weight = 100;
    $entity_types = [
      'cloud_cluster_worker',
      'cloud_cluster_site',
    ];

    if (!empty($entities)) {

      // Add dropdown menu for Cloud Orchestrator (Cloud Cluster).
      $title = 'Cloud Orchestrator';
      $id = 'cloud_cluster.service_providers.menu';
      $links[$id] = $base_plugin_definition;
      $links[$id]['title'] = $this->t('@title', ['@title' => $title]);
      $links[$id]['route_name'] = 'view.cloud_config.list';
      $links[$id]['menu_name'] = 'main';
      $links[$id]['parent'] = 'cloud.service_providers.menu';
      $links[$id]['weight'] = $weight++;
      $links[$id]['expanded'] = TRUE;
    }

    foreach ($entities ?: [] as $entity) {
      if (empty($entity)) {
        continue;
      }

      if ($entity->hasField('cloud_cluster_name') && !empty($entity->get('cloud_cluster_name')->value)) {
        continue;
      }

      $cloud_context = $entity->getCloudContext();
      $entity_id = $entity->id();
      $entity_label = $entity->label();
      $base_id = "$entity_id.local_tasks.$cloud_context";

      // Add menu items for cloud_cluster cluster.
      $menu_data = [];
      $menu_data[$base_id] = [
        'title' => $this->t('@entity_label', ['@entity_label' => $entity_label]),
        'route_name' => 'view.cloud_cluster_worker.list',
        'weight' => $weight++,
      ];

      $this->addMenuItems(
        $links,
        $base_plugin_definition,
        $cloud_context,
        $entity_types,
        $menu_data
      );
    }

    // Add dropdown menus for cloud design from cloud_context.
    $weight = 100;
    foreach ($entities ?: [] as $entity) {
      // Add dropdown menus for cloud budget for each k8s cloud_context.
      $id = "server.{$entity->id()}.design.local_tasks.{$entity->getCloudContext()}";
      $links[$id] = $base_plugin_definition;
      $links[$id]['title'] = $entity->label();
      $links[$id]['parent'] = 'cloud.menu.design_links:cloud_server.template';
      $links[$id]['route_name'] = 'entity.cloud_launch_template.collection';
      $links[$id]['route_parameters'] = ['cloud_context' => $entity->getCloudContext()];
      $links[$id]['weight'] = $weight++;
    }

    return $links;
  }

  /**
   * Add menu items to the links.
   *
   * @param array &$links
   *   Links.
   * @param array $base_plugin_definition
   *   An array of the base_plugin_definition.
   * @param string $cloud_context
   *   The cloud context.
   * @param array $entity_types
   *   The entity types.
   * @param array $menu_data
   *   The data of menu.
   */
  private function addMenuItems(
    array &$links,
    array $base_plugin_definition,
    $cloud_context,
    array $entity_types,
    array $menu_data,
  ): void {

    foreach ($menu_data ?: [] as $id => $link_data) {
      $links[$id] = $base_plugin_definition;
      $links[$id]['base_route'] = 'cloud_cluster.service_providers.menu';
      $links[$id]['parent'] = 'cloud_cluster.menu.cloud_context:cloud_cluster.service_providers.menu';
      $links[$id]['expanded'] = TRUE;
      $links[$id]['route_parameters'] = [
        'cloud_context' => $cloud_context,
      ];

      $links[$id] = $link_data + $links[$id];

      // Get extra route parameters.
      $extra_route_parameters = [];
      if (!empty($link_data['route_parameters']['cloud_cluster_namespace'])) {
        $namespace = $this->entityTypeManager
          ->getStorage('cloud_cluster_namespace')
          ->load($link_data['route_parameters']['cloud_cluster_namespace']);
        $extra_route_parameters['namespace'] = !empty($namespace) ?: $namespace->getName();
      }

      // Add child items.
      $this->addChildItems(
        $links,
        $entity_types,
        $base_plugin_definition,
        $cloud_context,
        $id,
        $extra_route_parameters
      );
    }
  }

  /**
   * Add child items to the links.
   *
   * @param array &$links
   *   The links.
   * @param array $entity_types
   *   The entity types.
   * @param array $base_plugin_definition
   *   An array of the base_plugin_definition.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $parent_link_id
   *   The ID of the parent link.
   * @param array $extra_route_parameters
   *   The extra route parameters.
   */
  private function addChildItems(
    array &$links,
    array $entity_types,
    array $base_plugin_definition,
    $cloud_context,
    $parent_link_id,
    array $extra_route_parameters = [],
  ): void {

    $weight = 0;
    foreach ($entity_types ?: [] as $entity_type) {
      $entity_definition = $this->entityTypeManager->getDefinition($entity_type);
      if ($entity_definition === NULL) {
        continue;
      }

      $label = (string) $entity_definition->getCollectionLabel();
      $title = preg_replace('/Cloud Orchestrator (.*)/', '${1}', $label);
      $id = "$parent_link_id.{$entity_definition->id()}";

      $links[$id] = $base_plugin_definition;
      $links[$id]['title'] = $this->t('@title', ['@title' => $title]);
      $links[$id]['route_name'] = "view.{$entity_definition->id()}.list";
      $links[$id]['menu_name'] = 'main';
      $links[$id]['parent'] = "cloud_cluster.menu.cloud_context:$parent_link_id";
      $links[$id]['route_parameters'] = ['cloud_context' => $cloud_context] + $extra_route_parameters;
      $links[$id]['weight'] = $weight++;
    }
  }

}
