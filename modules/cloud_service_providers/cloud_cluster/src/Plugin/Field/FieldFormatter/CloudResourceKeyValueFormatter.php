<?php

namespace Drupal\cloud_cluster\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'cloud_resource_key_value_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "cloud_resource_key_value_formatter",
 *   label = @Translation("Key value formatter"),
 *   field_types = {
 *     "key_value"
 *   }
 * )
 */
class CloudResourceKeyValueFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a KeyValueItem instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    EntityTypeManagerInterface $entity_type_manager,
  ) {

    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $rows = [];

    foreach ($items ?: [] as $item) {
      /** @var \Drupal\cloud\Plugin\Field\FieldType\KeyValue $item */
      if ($item->isEmpty()) {
        continue;
      }

      $entity = $this->entityTypeManager
        ->getStorage($item->item_key)
        ->load($item->item_value);

      if (empty($entity)) {
        continue;
      }

      $label = $entity->label();
      if ($entity->hasLinkTemplate('canonical')) {
        $label = $entity->id()
          ? $entity->toLink($entity->label())->toString()
          : $entity->label();
      }

      $rows[] = [
        $entity->getEntityType()->getSingularLabel(),
        $label,
      ];
    }

    if (count($rows)) {
      $elements[0] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('Key'),
          $this->t('Value'),
        ],
        '#rows' => $rows,
      ];
    }

    return $elements;
  }

}
