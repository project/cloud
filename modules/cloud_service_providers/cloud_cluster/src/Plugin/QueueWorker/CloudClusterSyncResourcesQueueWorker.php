<?php

namespace Drupal\cloud_cluster\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\cloud\Entity\CloudConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Processes for Cloud Cluster Sync Resources Queue.
 *
 * @QueueWorker(
 *   id = "cloud_cluster_sync_resources_queue",
 *   title = @Translation("Cloud Cluster Sync Resources Queue"),
 *   cron = {"time" = 60}
 * )
 */
class CloudClusterSyncResourcesQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The serializer object.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * The entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new LocaleTranslation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory instance.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   The serializer object.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    LoggerChannelFactoryInterface $logger_factory,
    Serializer $serializer,
    EntityTypeManager $entity_type_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger_factory->get('cloud_cluster');
    $this->serializer = $serializer;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('serializer'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $cloud_context = $data['cloud_context'];
    $entity_type = $data['entity_type'];
    $resource_jsons = $data['resource_jsons'];
    $cloud_cluster_worker_name = $data['cloud_cluster_worker_name'] ?? '';
    $cloud_cluster_name = $data['cloud_cluster_name'] ?? '';
    $clear = $data['clear'] ?? TRUE;

    if (!cloud_cluster_is_entity_type_supported($entity_type)) {
      return;
    }

    $definition = $this->entityTypeManager->getDefinition($entity_type);
    $label_key = $definition->getKey('label');
    $entities = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadByProperties(['cloud_context' => $cloud_context]);

    $entity_map = [];
    foreach ($entities ?: [] as $entity) {
      if (empty($entity->get('cloud_cluster_name')->value)
        || empty($entity->get('cloud_cluster_worker_name')->value)) {
        continue;
      }

      $label = $entity->label();
      if (strpos($entity_type, 'k8s_') === 0 && method_exists($entity, 'getNamespace')) {
        $label = $entity->getNamespace() . ':' . $label;
      }
      $key = sprintf(
        '%s:%s:%s',
        $entity->get('cloud_cluster_name')->value,
        $entity->get('cloud_cluster_worker_name')->value,
        $entity_type === 'cloud_config' ? $entity->getCloudContext() : $label);

      $entity_map[$key] = $entity;
    }

    $class = $definition->getClass();

    // Create or update.
    foreach ($resource_jsons ?: [] as $resource_json) {
      $resource_array = json_decode($resource_json, TRUE);

      unset($resource_array['id']);
      unset($resource_array['uuid']);
      unset($resource_array['uid']);

      if ($entity_type === 'cloud_config' || $entity_type === 'cloud_launch_template') {
        unset($resource_array['vid']);
        unset($resource_array['revision_user']);
      }

      $label = !empty($label_key)
        ? $resource_array[$label_key][0]['value']
        : $resource_array['name'][0]['value'];

      if (strpos($entity_type, 'k8s_') === 0
        && !empty($resource_array['namespace'])
        && !empty($resource_array['namespace'][0])
        && !empty($resource_array['namespace'][0]['value'])) {
        $label = $resource_array['namespace'][0]['value'] . ':' . $label;
      }

      $key = sprintf(
        '%s:%s:%s',
        $cloud_cluster_name,
        $cloud_cluster_worker_name,
        $entity_type === 'cloud_config' ? $cloud_context : $label);

      $update = FALSE;
      if (!empty($entity_map[$key])) {
        $update = TRUE;
        $resource_array['id'][0]['value'] = $entity_map[$key]->id();
        $resource_array['uuid'][0]['value'] = $entity_map[$key]->uuid();
        unset($entity_map[$key]);
      }

      $entity = $this->serializer->deserialize(json_encode($resource_array), $class, 'json');
      if ($update) {
        $entity->enforceIsNew(FALSE);
      }

      $entity->setCloudContext($cloud_context);

      // Skip entities from cloud cluster.
      if (!empty($entity->get('cloud_cluster_name')->value)
        || !empty($entity->get('cloud_cluster_worker_name')->value)) {
        continue;
      }

      $entity->set('cloud_cluster_name', $cloud_cluster_name);
      $entity->set('cloud_cluster_worker_name', $cloud_cluster_worker_name);
      if ($entity_type === 'cloud_config') {
        $name = sprintf('%s-%s-%s', substr($cloud_cluster_name, 0, 4), substr($cloud_cluster_worker_name, 0, 8), $entity->getName());
        $name = substr($name, -CloudConfigInterface::MAX_NAME_LENGTH);
        $entity->setName($name);
      }

      $entity->save();
    }

    // Delete.
    if (!$clear) {
      return;
    }
    foreach ($entity_map ?: [] as $key => $entity) {
      $this->logger->info($this->t('Deleted @entity_type @key.', [
        '@entity_type' => $entity_type,
        '@key' => $key,
      ]));
      $entity->delete();
    }
  }

}
