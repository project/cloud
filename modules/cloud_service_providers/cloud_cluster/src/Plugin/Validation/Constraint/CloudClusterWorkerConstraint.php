<?php

namespace Drupal\cloud_cluster\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\CompositeConstraintBase;

/**
 * Cloud cluster worker validation.
 *
 * @Constraint(
 *   id = "CloudClusterWorkerConstraint",
 *   label = @Translation("Cloud Cluster Worker", context = "Validation"),
 * )
 */
class CloudClusterWorkerConstraint extends CompositeConstraintBase {

  /**
   * The error message for the duplicated name.
   *
   * @var array
   */
  public $duplicatedName = 'The name is duplicated.';

  /**
   * {@inheritdoc}
   */
  public function coversFields() {
    return ['name'];
  }

}
