<?php

namespace Drupal\cloud_cluster\Plugin\cloud\launch_template;

use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud_cluster\Entity\CloudClusterSite\CloudClusterSite;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Cloud cluster cloud launch template plugin.
 */
class CloudClusterCloudLaunchTemplatePlugin extends CloudPluginBase implements CloudLaunchTemplatePluginInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Current login user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  protected $entityLinkRenderer;

  /**
   * File system object.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * Twig.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twig;

  /**
   * CloudClusterCloudLaunchTemplatePlugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The uuid service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current login user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\Core\Template\TwigEnvironment $twig
   *   The Twig handler.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    UuidInterface $uuid_service,
    AccountProxyInterface $current_user,
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityLinkRendererInterface $entity_link_renderer,
    FileSystemInterface $file_system,
    CloudServiceInterface $cloud_service,
    TwigEnvironment $twig,
    ExtensionPathResolver $extension_path_resolver,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->uuidService = $uuid_service;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityLinkRenderer = $entity_link_renderer;
    $this->fileSystem = $file_system;
    $this->cloudService = $cloud_service;
    $this->twig = $twig;
    $this->extensionPathResolver = $extension_path_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity.link_renderer'),
      $container->get('file_system'),
      $container->get('cloud'),
      $container->get('twig'),
      $container->get('extension.path.resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityBundleName(): string {
    return $this->pluginDefinition['entity_bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function launch(CloudLaunchTemplateInterface $cloud_launch_template, ?FormStateInterface $form_state = NULL): array {
    $route = [
      'route_name' => 'entity.cloud_launch_template.canonical',
      'params' => [
        'cloud_launch_template' => $cloud_launch_template->id(),
        'cloud_context' => $cloud_launch_template->getCloudContext(),
      ],
    ];

    $deployment_type = $cloud_launch_template->field_deployment_type->value;

    $deployment_definition_path = realpath(
      sprintf(
        '%s/templates/%s/deploy.yml',
        $this->extensionPathResolver->getPath('module', 'cloud_cluster'),
        $deployment_type
      )
    );

    $deployment_template = $cloud_launch_template->field_deployment_template->value;

    $deployment_definition = Yaml::decode(file_get_contents($deployment_definition_path));
    $template_definition = $deployment_definition['templates'][$deployment_template];
    $template_location = $template_definition['location'];

    // If the location is a relative path, the definition folder path
    // should be added.
    $template_content = file_get_contents($template_location)
      ?: file_get_contents(dirname($deployment_definition_path) . '/' . $template_location);

    $parameters = [];
    foreach ($form_state->getValue('parameters') ?: [] as $group) {
      foreach ($group ?: [] as $name => $value) {
        $parameters[$name] = $value;
      }
    }

    $yaml = $this->twig->renderInline($template_content, $parameters);

    $service_name = "$deployment_type.cloud_orchestrator_manager";
    // @phpstan-ignore-next-line
    if (!\Drupal::hasService($service_name)) {
      $message = $this->t('The cloud orchestrator manager for %type could not be found.', [
        '%type' => $deployment_type,
      ]);
      $this->messenger->addError($message);
      return $route;
    }

    try {
      // @phpstan-ignore-next-line
      $cloud_resources = \Drupal::service($service_name)->deploy($form_state->getValue('target_provider'), $yaml, $parameters);
      $cloud_launch_template->get('field_workflow_status')->setValue(CloudLaunchTemplateInterface::DRAFT);
      $cloud_launch_template->validate();
      $cloud_launch_template->save();

      $parameter_definitions = $deployment_definition['parameters'];

      // Create cloud cluster site object.
      $timestamp = time();
      $entity = CloudClusterSite::create([
        'name' => $form_state->getValue('site_name'),
        'cloud_context' => $cloud_launch_template->getCloudContext(),
        'status' => 'Deploying',
        'target_cloud_context' => $form_state->getValue('target_provider'),
        'cloud_launch_template' => $cloud_launch_template->id(),
        'parameters' => array_map(fn($key, $value) => [
          'item_key' => $key,
          'item_value' => $parameter_definitions[$key]['type'] === 'password' ? '******' : $value,
        ], array_keys($parameters ?: []), array_values($parameters ?: [])),
        'cloud_resources' => array_map(fn($item) => [
          'item_key' => $item['entity_type_id'],
          'item_value' => $item['entity_id'],
        ], $cloud_resources ?: []),
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $this->currentUser->id(),
      ]);
      $entity->save();

      $this->messenger->addMessage($this->t('Succeeded to deploy cloud orchestrator @label.', [
        '@label' => $entity->toLink($entity->label())->toString(),
      ]));
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      $this->messenger->addError($this->t('Failed to deploy cloud orchestrator.'));
    }

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function buildListHeader(): array {
    return [
      [
        'data' => $this->t('Deployment type'),
        'specifier' => 'field_deployment_type',
        'field' => 'field_deployment_type',
      ], [
        'data' => $this->t('Deployment template'),
        'specifier' => 'field_deployment_template',
        'field' => 'field_deployment_template',
      ], [
        'data' => $this->t('Workflow status'),
        'specifier' => 'field_workflow_status',
        'field' => 'field_workflow_status',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRow(CloudLaunchTemplateInterface $entity): array {
    $row['field_deployment_type'] = [
      'data' => $this->renderField($entity, 'field_deployment_type'),
    ];
    $row['field_deployment_template'] = [
      'data' => $this->renderField($entity, 'field_deployment_template'),
    ];
    $row['field_workflow_status'] = [
      'data' => $this->renderField($entity, 'field_workflow_status'),
    ];
    return $row;
  }

  /**
   * Build the launch form for Cloud Cluster.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state if launch is called from a form.
   */
  public function buildLaunchForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function validateGit(
    CloudLaunchTemplateInterface $entity,
    array &$files_arr,
    string &$tmp_dir_name,
    bool $delete_tmp_dir = FALSE,
  ): string {
    return '';
  }

  /**
   * Render a launch template entity field.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The launch template entity.
   * @param string $field_name
   *   The field to render.
   * @param string $view
   *   The view to render.
   *
   * @return array
   *   A fully loaded render array for that field or empty array.
   */
  private function renderField(CloudLaunchTemplateInterface $entity, $field_name, $view = 'default'): array {
    $field = [];
    if ($entity->hasField($field_name)) {
      $field = $entity->get($field_name)->view($view);
      // Hide the label.
      $field['#label_display'] = 'hidden';
    }
    return $field;
  }

}
