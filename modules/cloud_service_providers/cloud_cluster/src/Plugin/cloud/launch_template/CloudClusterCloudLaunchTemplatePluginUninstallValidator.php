<?php

namespace Drupal\cloud_cluster\Plugin\cloud\launch_template;

use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginUninstallValidator;

/**
 * Validates module uninstall readiness based on existing content entities.
 */
class CloudClusterCloudLaunchTemplatePluginUninstallValidator extends CloudLaunchTemplatePluginUninstallValidator {

}
