<?php

namespace Drupal\cloud_cluster\Plugin\rest\resource;

use Drupal\Core\Queue\QueueFactory;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud_cluster\Traits\CloudClusterTrait;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Represents sync request from workers as resource.
 *
 * @RestResource(
 *   id = "operation_resource",
 *   label = @Translation("Operation Resource"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "create" = "/cloud_cluster/operation"
 *   }
 * )
 */
class CloudClusterOperationResource extends ResourceBase {

  use CloudClusterTrait;

  /**
   * The queue factory object.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  private $queueFactory;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    QueueFactory $queue_factory,
    RequestStack $request_stack,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->queueFactory = $queue_factory;
    $this->requestStack = $request_stack;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('queue'),
      $container->get('request_stack'),
      $container->get('plugin.manager.cloud_config_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function post($data): ModifiedResourceResponse {
    $request = $this->requestStack->getCurrentRequest();
    $request_token = $this->getRequestToken($request);
    $cloud_config = $this->getCloudConfigByRequestToken($this->cloudConfigPluginManager, $request_token);
    $cloud_context = $cloud_config->getCloudContext();

    $queue = $this->queueFactory->get('operation_queue_' . $cloud_context . $data['cloud_cluster_worker_name']);
    if (empty($queue)) {
      return new ModifiedResourceResponse([], 200);
    }

    $operations = [];
    while ($item = $queue->claimItem()) {
      $operations[] = $item->data;
      $queue->deleteItem($item);
    }

    return new ModifiedResourceResponse($operations, 200);
  }

}
