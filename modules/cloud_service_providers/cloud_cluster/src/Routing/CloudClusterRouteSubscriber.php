<?php

namespace Drupal\cloud_cluster\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Add dynamic routes.
 */
class CloudClusterRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    $collection_to_add = new RouteCollection();
    foreach ($collection ?: [] as $name => $route) {
      if (strpos($name, 'view.cloud_config.') !== 0) {
        continue;
      }

      $new_route = clone $route;
      $new_route->setPath('/clouds/cloud_cluster/{cloud_context}/worker/{cloud_cluster_worker}' . $new_route->getPath());

      $options = $new_route->getOptions();
      $options['parameters']['cloud_cluster_worker'] = [
        'type' => 'entity:cloud_cluster_worker',
        'converter' => 'paramconverter.entity',
      ];
      $new_route->setOptions($options);

      $collection_to_add->add('cloud_cluster.' . $name, $new_route);
    }
    $collection->addCollection($collection_to_add);
  }

}
