<?php

namespace Drupal\cloud_cluster\Service;

/**
 * Provides K8sService interface.
 */
interface CloudClusterServiceInterface {

  public const CLOUD_CLUSTER_SUPPORTED_ENTITY_TYPES = [
    'aws_cloud_*',
    'k8s_*',
    'vmware_*',
    'openstack_*',
    'cloud_config',
    'cloud_launch_template',
  ];

}
