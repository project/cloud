<?php

namespace Drupal\cloud_cluster\Traits;

use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginException;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The trait for Cloud Cluster.
 */
trait CloudClusterTrait {

  /**
   * Get the token from request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return string|null
   *   The token.
   */
  protected function getRequestToken(Request $request): ?string {
    $authorization = trim($request->headers->get('Authorization') ?: '');
    if (empty($authorization)) {
      return NULL;
    }

    if (strpos($authorization, 'Bearer ') !== 0) {
      return NULL;
    }

    $token = trim(substr($authorization ?: '', strlen('Bearer ')));
    $result = base64_decode($token);
    if ($result === FALSE) {
      return NULL;
    }

    return $result;
  }

  /**
   * Get the cloud config object by request token.
   *
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud config plugin manager.
   * @param string $request_token
   *   The request token.
   *
   * @return \Drupal\cloud\Entity\CloudConfig|null
   *   The cloud config object.
   */
  protected function getCloudConfigByRequestToken(
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    string $request_token,
  ): ?CloudConfig {

    try {
      $cloud_configs = $cloud_config_plugin_manager
        ->loadConfigEntities('cloud_cluster');
    }
    catch (CloudConfigPluginException $e) {
      $cloud_configs = [];
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      if (empty($cloud_config)) {
        continue;
      }

      if ($request_token === $cloud_config->field_cloud_cluster_api_token->value) {
        return $cloud_config;
      }
    }

    return NULL;
  }

}
