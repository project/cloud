parameters:
  StackName:
    title: Stack name
    default_value: ''
    type: textfield
    required: true
  StackPrefix:
    title: Stack prefix
    description: A prefix to append to resource names/IDs. For example, ${StackPrefix}-IAM-Role,
      ${StackPrefix}-Drupal-RDS for RDS DB Identifier. Must be between 1 and 20 characters
      and only contain alphanumeric characters and hyphens.
    default_value: ''
    pattern: ^[a-zA-Z0-9\\-]+$
    maxlength: 20
    type: textfield
    required: true
  DrupalUserName:
    title: Drupal administrator username
    default_value: cloud_admin
    type: textfield
    required: true
  DrupalPassword:
    title: Drupal administrator password
    description: >-
      The Drupal admin account password. Must be between 6 and 32 characters
      and only contain alphanumeric characters and these special characters ` ~ ! # $ % ^ & * ( ) _ + , . \ -
    default_value: cloud_admin
    pattern: ^[\w`~!#$%^&*()_+,.\\-]+$
    maxlength: 32
    type: password
    required: true
  DrupalEmail:
    title: Drupal administrator email address
    default_value: cloud_admin@example.com
    type: email
    required: true
  DrupalTimezone:
    title: Drupal default time zone
    default_value: America/Los_Angeles
    type: timezone
    required: true
  MySQLUserName:
    title: MySQL administrator username
    default_value: mysql_admin
    type: textfield
    required: true
  MySQLPassword:
    title: MySQL administrator password
    description: >-
      Password for the RDS Username.  Must be between 6 and 32 characters
      and only contain alphanumeric characters and these special characters ` ~ ! # $ % ^ & * ( ) _ + , . \ -
    default_value: mysql_admin_password
    maxlength: 32
    pattern: ^[\w`~!#$%^&*()_+,.\\-]+$
    type: password
    required: true
  DatabaseName:
    title: MySQL database name
    description: The name of the database. Must be between 4 and 32 characters and
      only contain alphanumeric characters and underscores.
    pattern: ^[a-zA-Z0-9_]+$
    maxlength: 32
    minlength: 4
    default_value: cloud_orchestrator
    type: textfield
    required: true
  KeyName:
    title: EC2 key name
    default_value: ''
    type: entity_select
    entity_type: aws_cloud_key_pair
    entity_key: key_pair_name
    cloud_context: ''
    required: true
  VPC:
    title: VPC
    default_value: ''
    type: entity_select
    entity_type: aws_cloud_vpc
    entity_key: vpc_id
    cloud_context: ''
    required: true
  PrivateSubnet1:
    title: Private subnet1
    default_value: ''
    type: entity_select
    entity_type: aws_cloud_subnet
    entity_key: subnet_id
    cloud_context: ''
    required: true
  PrivateSubnet2:
    title: Private subnet2
    default_value: ''
    type: entity_select
    entity_type: aws_cloud_subnet
    entity_key: subnet_id
    cloud_context: ''
    required: true
  PublicSubnet1:
    title: Public subnet1
    default_value: ''
    type: entity_select
    entity_type: aws_cloud_subnet
    entity_key: subnet_id
    cloud_context: ''
    required: true
  Subnet:
    title: Subnet
    default_value: ''
    type: entity_select
    entity_type: aws_cloud_subnet
    entity_key: subnet_id
    cloud_context: ''
    required: true

templates:
  cloud_orchestrator_full:
    label: Cloud Orchestrator Full
    location: https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_full.yaml
    parameter_groups:
      - name: stack
        title: Stack Configuration
        parameter_refs:
          - StackName
          - StackPrefix
      - name: drupal
        title: Drupal configuration
        parameter_refs:
          - DrupalUserName
          - DrupalPassword
          - DrupalEmail
          - DrupalTimezone

      - name: database
        title: Database configuration
        parameter_refs:
          - MySQLUserName
          - MySQLPassword
          - DatabaseName

      - name: aws_ec2
        title: Amazon EC2 Configuration
        parameter_refs:
          - KeyName

  cloud_orchestrator_full_manual_vpc:
    label: Cloud Orchestrator Full Manual VPC
    location: https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_full_manual_vpc.yaml
    parameter_groups:
      - name: stack
        title: Stack Configuration
        parameter_refs:
          - StackName
          - StackPrefix
      - name: drupal
        title: Drupal configuration
        parameter_refs:
          - DrupalUserName
          - DrupalPassword
          - DrupalEmail
          - DrupalTimezone

      - name: database
        title: Database configuration
        parameter_refs:
          - MySQLUserName
          - MySQLPassword
          - DatabaseName

      - name: aws_ec2
        title: Amazon EC2 Configuration
        parameter_refs:
          - KeyName
          - VPC
          - PrivateSubnet1
          - PrivateSubnet2
          - PublicSubnet1

  cloud_orchestrator_single:
    label: Cloud Orchestrator Single
    location: https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_single.yaml
    parameter_groups:
      - name: stack
        title: Stack Configuration
        parameter_refs:
          - StackName
          - StackPrefix
      - name: drupal
        title: Drupal configuration
        parameter_refs:
          - DrupalUserName
          - DrupalPassword
          - DrupalEmail
          - DrupalTimezone

      - name: database
        title: Database configuration
        parameter_refs:
          - MySQLUserName
          - MySQLPassword
          - DatabaseName

      - name: aws_ec2
        title: Amazon EC2 Configuration
        parameter_refs:
          - KeyName

  cloud_orchestrator_single_manual_vpc:
    label: Cloud Orchestrator Single Manual VPC
    location: https://cloud-orchestrator.s3.amazonaws.com/cfn/cloud_orchestrator_single_manual_vpc.yaml
    parameter_groups:
      - name: stack
        title: Stack Configuration
        parameter_refs:
          - StackName
          - StackPrefix
      - name: drupal
        title: Drupal configuration
        parameter_refs:
          - DrupalUserName
          - DrupalPassword
          - DrupalEmail
          - DrupalTimezone

      - name: database
        title: Database configuration
        parameter_refs:
          - MySQLUserName
          - MySQLPassword
          - DatabaseName

      - name: aws_ec2
        title: Amazon EC2 Configuration
        parameter_refs:
          - KeyName
          - VPC
          - Subnet
