INTRODUCTION
============

- Docker is a subsidiary module of Cloud module, especially intended to support
  Amazon ECR integration with Kubernetes (K8s) module.
- This module only supplies the API integration for Docker. It does nothing by
  itself. Enable K8s and K8s to S3 to utilize its functionality.

- See also [Cloud](https://drupal.org/project/cloud/) module.

REQUIREMENTS
============

- Drupal `10.x` or higher (The latest version of Drupal `10.x`)
- Cloud `8.x` or higher

INSTALLATION
============

- `composer require drupal/cloud`
