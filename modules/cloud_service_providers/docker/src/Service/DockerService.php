<?php

namespace Drupal\docker\Service;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Utility\Error;
use Drupal\cloud\Service\CloudServiceBase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * Provides Docker service.
 */
class DockerService extends CloudServiceBase implements DockerServiceInterface {

  /**
   * The docker api version to use.
   *
   * @var string
   */
  protected $apiVersion = '';

  /**
   * The return format of API requests.
   *
   * @var string
   */
  protected $format = 'json';

  /**
   * The docker unix socket.
   *
   * @var string
   */
  protected $unixSocket;

  /**
   * Flag whether to use the unix socket in the API requests.
   *
   * @var bool
   */
  protected $useSocket = TRUE;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new DockerService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle Http client.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
  ) {

    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    // Set the unix socket.
    $this->unixSocket = $config_factory->get('docker.settings')->get('docker_unix_socket');
    $this->apiVersion = $config_factory->get('docker.settings')->get('docker_api_version');
  }

  /**
   * Helper method to do a Guzzle HTTP POST.
   *
   * @param string $endpoint
   *   The endpoint to access.
   * @param array $params
   *   Parameters to pass to the POST request.
   *
   * @return string
   *   String interpretation of the response body or FALSE
   */
  protected function doPost($endpoint, array $params = []): string {
    $output = '';
    try {
      $headers = array_merge_recursive($this->getDefaultParams(), $params);
      $response = $this->httpClient->request(
        'POST',
        $endpoint,
        $headers
      );
      $output = $response->getBody()->getContents();
    }
    catch (ClientException $error) {
      $response = $error->getResponse();
      $response_info = $response->getBody()->getContents();
      $message = new FormattableMarkup(
        'Docker api error. Error details are ?: [] as follows:<pre>@response</pre>',
        [
          '@response' => print_r(json_decode($response_info), TRUE),
        ]
      );
      $this->logError('Remote API Connection', $error, $message);
    }
    catch (\Exception $error) {
      $this->logError('Remote API Connection', $error, $this->t('An unknown error
      occurred while trying to connect to the remote API. This is not a Guzzle
      error, nor an error in the remote API, rather a generic local error
      occurred. The reported error was @error',
        ['@error' => $error->getMessage()]
      ));
    }
    return $output;
  }

  /**
   * Helper method to do a Guzzle HTTP GET.
   *
   * @param string $endpoint
   *   The endpoint to access.
   * @param array $params
   *   Parameters to pass to the POST request.
   *
   * @return string
   *   String interpretation of the response body or FALSE
   *
   * @throws \Drupal\docker\Service\DockerServiceException
   */
  protected function doGet($endpoint, array $params = []): string {
    $output = '';
    try {
      $response = $this->httpClient->request(
        'GET',
        $endpoint,
        array_merge($this->getDefaultParams(), $params)
      );

      $output = $response->getBody()->getContents();
    }
    catch (ClientException $error) {
      $response = $error->getResponse();
      if ($response !== NULL) {
        $response_info = $response->getBody()->getContents();
        $response_info = new FormattableMarkup(
          'Docker api error. Error details are as follows:<pre>@response</pre>',
          [
            '@response' => print_r(json_decode($response_info), TRUE),
          ]
        );
      }
      else {
        $response_info = $error->getMessage();
      }
      $message = new FormattableMarkup(
        'Docker api error. Error details are as follows:<pre>@response</pre>',
        [
          '@response' => $response_info,
        ]
      );

      $this->logError('Remote API Connection', $error, $message);
    }
    catch (\Exception $error) {
      $this->logError('Remote API Connection', $error, $this->t('An unknown error
      occurred while trying to connect to the remote API. This is not a Guzzle
      error, nor an error in the remote API, rather a generic local error
      occurred. The reported error was @error',
        ['@error' => $error->getMessage()]
      ));
    }
    return $output;
  }

  /**
   * Set the unix socket path.
   *
   * @param string $unix_socket
   *   The unix socket string.
   */
  public function setUnixSocket($unix_socket): void {
    $this->unixSocket = $unix_socket;
  }

  /**
   * Set api version.
   *
   * @param string $api_version
   *   The api version.
   */
  public function setApiVersion($api_version): void {
    $this->apiVersion = $api_version;
  }

  /**
   * List images in docker.
   *
   * @return array
   *   Array of images.
   */
  public function listImages(): array {
    $output = [];
    $endpoint = $this->buildEndpoint("images/$this->format");
    $results = $this->doGet($endpoint);
    if (!empty($results)) {
      $output = json_decode($results);
    }
    return $output;
  }

  /**
   * Push an image.
   *
   * @param string $name
   *   Image name to push.
   * @param array $auth_array
   *   Authentication array according to
   *   https://docs.docker.com/engine/api/v1.39/#section/Authentication.
   *
   * @return string
   *   Response body.
   */
  public function pushImage($name, array $auth_array = []): string {
    $params = [];
    $endpoint = $this->buildEndpoint("images/$name/push");
    if (!empty($auth_array)) {
      $auth_json = json_encode($auth_array, JSON_THROW_ON_ERROR);
      $params['headers']['X-Registry-Auth'] = base64_encode($auth_json ?: '');
    }
    return $this->doPost($endpoint, $params);
  }

  /**
   * Inspect an image.
   *
   * @param string $name
   *   Name of image to inspect.
   *
   * @return array|bool
   *   Array containing image information.
   */
  public function inspectImage($name): string {
    $endpoint = $this->buildEndpoint("images/$name/$this->format");
    return $this->doGet($endpoint);
  }

  /**
   * Pull image into Docker.
   *
   * @param string $from_image
   *   Can be a name or Uri.
   *
   * @return bool
   *   Response body.
   *
   * @throws \Drupal\docker\Service\DockerServiceException
   */
  public function pullImage($from_image): bool {
    $endpoint = $this->buildEndpoint("images/create?fromImage=$from_image");
    $this->doPost($endpoint, []);
    $success = FALSE;
    // Verify image was pulled.
    try {
      $this->inspectImage($from_image);
      $success = TRUE;
    }
    catch (DockerServiceException $e) {
      $this->logError('Docker pull image', $e, $this->t('Unable to find pulled image'), FALSE);
    }
    return $success;
  }

  /**
   * Tag an image.
   *
   * @param string $name
   *   The image to tag.
   * @param string $repo
   *   Repository to tag in.
   * @param string $tag
   *   Name of new tag.
   */
  public function tagImage($name, $repo, $tag): void {
    $endpoint = $this->buildEndpoint("images/$name/tag?repo=$repo&tag=$tag");
    $this->doPost($endpoint, []);
  }

  /**
   * {@inheritdoc}
   */
  public function setFormat($format): void {
    $this->format = $format;
  }

  /**
   * Set a boolean on whether to use the local docker unix socket.
   *
   * @param bool $use_socket
   *   TRUE | FALSE.
   */
  public function setUseSocket($use_socket): void {
    $this->useSocket = $use_socket;
  }

  /**
   * Parse and extract information from an image string.
   *
   * @param string $image
   *   The image string to parse.
   *
   * @return array
   *   An array with image information.
   */
  public function parseImage($image): array {
    $pattern = "/^(?:([^\/]+)\/)?(?:([^\/]+)\/)?([^@:\/]+)(?:[@:](.+))?$/";
    $match = preg_match($pattern, $image, $matches);

    if ($match === FALSE) {
      return [];
    }

    $registry = $matches[1];
    $namespace = $matches[2];
    $repository = $matches[3];
    $tag = $matches[4] ?? 'latest';

    // If there is no hostname, the registry variable is the namespace.
    if (empty($namespace) && !empty($registry) && !preg_match('/[:.]/', $registry)) {
      $namespace = $registry;
      $registry = '';
    }

    $info = [
      'namespace' => $namespace,
      'registry' => $registry,
      'repository' => $repository,
      'tag' => $tag,
    ];

    // Derive the namespace/repository and also the full name.
    $registry = !empty($registry) ? $registry . '/' : '';
    $namespace = !empty($namespace) ? $namespace . '/' : '';
    $tag = ':' . $tag;

    $info['full_repository'] = $namespace . $repository;
    $info['name'] = $registry . $namespace . $repository . $tag;

    return $info;
  }

  /**
   * Check if docker is available.
   *
   * @param string $unix_socket
   *   Docker unix socket to check.
   * @param string $api_version
   *   Api version to check.
   *
   * @return bool
   *   TRUE if docker is up.
   *
   * @throws \Drupal\docker\Service\DockerServiceException
   */
  public function isDockerUp($unix_socket = '', $api_version = ''): bool {
    $is_available = FALSE;
    try {
      if (!empty($unix_socket)) {
        $this->setUnixSocket($unix_socket);
      }
      if (!empty($api_version)) {
        $this->setApiVersion($api_version);
      }
      $this->listImages();
      $is_available = TRUE;
    }
    catch (DockerServiceException $e) {
      // Set an error if docker is unreachable.
      $this->logError('Remote API Connection', $e, $this->t('Docker unreachable.  @error', ['@error' => $e->getMessage()]), FALSE);
    }
    return $is_available;
  }

  /**
   * Log errors to watchdog and throw an exception.
   *
   * @param string $type
   *   The error type.
   * @param \Exception $exception
   *   The exception object.
   * @param string $message
   *   The message to log.
   * @param bool $throw
   *   TRUE to throw exception.
   *
   * @throws \Drupal\docker\Service\DockerServiceException
   */
  private function logError($type, \Exception $exception, $message, $throw = TRUE): void {
    // See also: https://www.drupal.org/node/2932520.
    Error::logException($this->logger($type), $exception, $message);
    if ($throw === TRUE) {
      throw new DockerServiceException($exception);
    }
  }

  /**
   * Build a local api endpoint.
   *
   * @param string $operation
   *   Docker operation.
   * @param bool $https
   *   Build string using http or https.
   *
   * @return string
   *   Docker endpoint.
   */
  private function buildEndpoint($operation, $https = FALSE): string {
    $endpoint = $https === TRUE ? 'https' : 'http';
    $endpoint .= '://localhost';
    if (!empty($this->apiVersion)) {
      $endpoint .= "/v{$this->apiVersion}";
    }
    $endpoint .= "/$operation";
    return $endpoint;
  }

  /**
   * Setup any default parameters for the Guzzle request.
   *
   * @return array
   *   Array of parameters.
   */
  private function getDefaultParams(): array {
    $params = [
      'timeout' => 0,
      'headers' => [
        'Content-Type' => 'application/json',
      ],
    ];
    if ($this->useSocket === TRUE) {
      $params['curl'] = [
        CURLOPT_UNIX_SOCKET_PATH => $this->unixSocket,
      ];
    }
    return $params;
  }

}
