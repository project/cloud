(function () {
  'use strict';

  const cloud_context_namespaces = drupalSettings.k8s.cloud_context_namespaces;
  const updateNamespaceOptions = function (cloud_context) {
    const namespaceOptions = document.querySelectorAll('#edit-namespace option');

    namespaceOptions.forEach(function (option) {
      const namespace = option.value;

      // If the cloud context is any.
      if (!cloud_context) {
        option.style.display = '';
        return;
      }

      // If the namespace is any.
      if (!namespace) {
        option.style.display = '';
        return;
      }

      // If the cloud context does not have any namespace,
      // or the namespace does not below to the cloud context.
      if (!cloud_context_namespaces[cloud_context]
        || !cloud_context_namespaces[cloud_context][namespace]) {

        option.style.display = 'none';
        option.selected = false;
      } else {
        option.style.display = '';
      }
    });
  };

  const cloud_context_element = document.querySelector('#edit-cloud-context');
  updateNamespaceOptions(cloud_context_element.value);
  cloud_context_element.addEventListener('change', function () {
    updateNamespaceOptions(this.value);
  });
})();
