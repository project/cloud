(function () {
  'use strict';

  for (const apply_button of document.querySelectorAll('.simple-datatable .form-inline .button[name="apply"]')) {
    const form_inline = apply_button.closest('.form-inline');
    const block_id = form_inline.closest('.simple-datatable').id;
    if (!block_id) {
      continue;
    }

    apply_button.addEventListener('click', function () {
      const url = new URL(location.href);
      form_inline.querySelectorAll('select').forEach(function (select) {
        url.searchParams.set(block_id + '[' + select.name + ']', select.value);
      });
      location.href = url.href;
    });
  }
})();
