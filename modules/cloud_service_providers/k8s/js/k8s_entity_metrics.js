(function () {
  'use strict';

  const k8s_entity_metrics = document.getElementById('k8s_entity_metrics');
  if (!k8s_entity_metrics) {
    return;
  }

  if (!drupalSettings.k8s || !drupalSettings.k8s.metrics_enabled) {
    if (k8s_entity_metrics.parentElement && k8s_entity_metrics.parentElement.parentElement) {
      k8s_entity_metrics.parentElement.parentElement.style.display = 'none';
    }
    return;
  }

  const color = Chart.helpers.color;
  const formatMemory = function (value) {
    let mb = value / 1024 / 1024;
    mb = Math.round(mb);

    let gb = mb / 1024;
    gb = Math.round(gb * 100) / 100;

    if (gb >= 1) {
      return gb + ' Gi';
    } else {
      return mb + ' Mi';
    }
  };

  const updateCharts = function () {
    fetch(window.location.pathname + '/metrics')
        .then(response => response.json())
        .then(json => {
          const cpu_data = [];
          const memory_data = [];
          json.forEach(item => {
            cpu_data.push({
              t: item.timestamp * 1000,
              y: item.cpu
            });

            memory_data.push({
              t: item.timestamp * 1000,
              y: item.memory
            });
          });

          cpu_chart.data.datasets[0].data = cpu_data;
          cpu_chart.update();

          memory_chart.data.datasets[0].data = memory_data;
          memory_chart.update();
        });
  };

  // Build CPU chart.
  const cpu_div = document.createElement('div');
  cpu_div.className = 'col-sm-6';
  cpu_div.innerHTML = '<canvas id="cpu_chart"></canvas>';
  k8s_entity_metrics.appendChild(cpu_div);

  const cpu_ctx = cpu_div.querySelector('canvas').getContext('2d');
  const cpu_cfg = {
    type: 'bar',
    data: {
      datasets: [{
        label: 'CPU usage',
        backgroundColor: color('green').alpha(0.5).rgbString(),
        borderColor: 'green',
        data: [],
        type: 'line',
        pointRadius: 0,
        fill: false,
        lineTension: 0,
        borderWidth: 2
      }]
    },
    options: {
      scales: {
        xAxes: [{
          type: 'time',
          distribution: 'linear',
          bounds: 'ticks',
          ticks: {
            source: 'ticks',
            autoSkip: false
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'CPU (Cores)'
          }
        }]
      },
      tooltips: {
        intersect: false,
        mode: 'index',
        callbacks: {
          label: function (tooltipItem, myData) {
            let label = myData.datasets[tooltipItem.datasetIndex].label || '';
            if (label) {
              label += ': ';
            }
            label += parseFloat(tooltipItem.value).toFixed(2);
            return label;
          }
        }
      }
    }
  };

  const cpu_chart = new Chart(cpu_ctx, cpu_cfg);

  // Build Memory chart.
  const memory_div = document.createElement('div');
  memory_div.className = 'col-sm-6';
  memory_div.innerHTML = '<canvas id="memory_chart"></canvas>';
  k8s_entity_metrics.appendChild(memory_div);

  const memory_ctx = memory_div.querySelector('canvas').getContext('2d');
  const memory_cfg = {
    type: 'bar',
    data: {
      datasets: [{
        label: 'Memory usage',
        backgroundColor: color('blue').alpha(0.5).rgbString(),
        borderColor: 'blue',
        data: [],
        type: 'line',
        pointRadius: 0,
        fill: false,
        lineTension: 0,
        borderWidth: 2
      }]
    },
    options: {
      scales: {
        xAxes: [{
          type: 'time',
          distribution: 'linear',
          bounds: 'ticks',
          ticks: {
            source: 'ticks',
            autoSkip: false
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Memory (Bytes)'
          },
          ticks: {
            callback: function (value, index, values) {
              return formatMemory(value);
            }
          }
        }]
      },
      tooltips: {
        intersect: false,
        mode: 'index',
        callbacks: {
          label: function (tooltipItem, myData) {
            let label = myData.datasets[tooltipItem.datasetIndex].label || '';
            if (label) {
              label += ': ';
            }
            label += formatMemory(tooltipItem.value);
            return label;
          }
        }
      }
    }
  };

  const memory_chart = new Chart(memory_ctx, memory_cfg);

  updateCharts();

  const interval = drupalSettings.k8s.k8s_js_refresh_interval || 10;
  setInterval(function () {
    updateCharts();
  }, interval * 1000);
})();
