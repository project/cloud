(function () {
  'use strict';

  const active_tab_observer = new MutationObserver((mutations) => {
    mutations.forEach((mutation) => {
      // changed vertical tab.
      updateSourcetypeOptions(mutation.target.value);
    });
  });
  const tab_configuration_git_observer = new MutationObserver((mutations) => {
    mutations.forEach((mutation) => {
      const className = mutation.target.className;
      if (className.includes('vertical-tabs-pane') && className.includes('active')) {
        // active git tab.
        updateSourcetypeOptions('git');
      }
    });
  });
  const tab_configuration_yml_observer = new MutationObserver((mutations) => {
    mutations.forEach((mutation) => {
      const className = mutation.target.className;
      if (className.includes('vertical-tabs-pane') && className.includes('active')) {
        // active yaml tab.
        updateSourcetypeOptions('yml');
      }
    });
  });
  active_tab_observer.observe(
    document.querySelector('.vertical-tabs__active-tab'),
    {
      attributes: true,
      attributeFilter: ['value']
    }
  );
  tab_configuration_git_observer.observe(
    document.querySelector('#edit-configuration-git'),
    {
      attributes: true,
      attributeFilter: ['class']
    }
  );
  tab_configuration_yml_observer.observe(
    document.querySelector('#edit-configuration-yml'),
    {
      attributes: true,
      attributeFilter: ['class']
    }
  );

  const updateSourcetypeOptions = function (active_tab) {
    if (!active_tab) {
      return;
    }
    const field_source_type = document.querySelector("select[name='field_source_type']").value;
    const old_git_source_type = field_source_type === 'yml'
      ? document.getElementById("old_git_source_type").value
      : field_source_type;
    document.getElementById("old_git_source_type").value = old_git_source_type;
    document.querySelectorAll('#edit-field-source-type option').forEach(function (option) {
      const source_type = option.value;
      if (option.selected) {
        option.selected = false;
      }
      // Hide _none.
      // And hide yml when the git tab is active.
      if (source_type === '_none'
        || active_tab.includes('git') && source_type === 'yml') {
        option.style.display = 'none';
        return true;
      }
    });

    // When the yml tab is active, select yml.
    // And when the git tab is active, select the value of old_git_source_type.
    const source_type = active_tab.includes('yml') ? 'yml' : old_git_source_type;
    document.querySelectorAll('#edit-field-source-type option').forEach(function (option) {
      const item = option.value;

      if (item === source_type) {
        option.selected = true;
        return false;
      }
    });
  };

  document.addEventListener('DOMContentLoaded', function () {
    const source_type = document.querySelector('#edit-field-source-type');
    if (source_type) {
      source_type.addEventListener('change', function () {
        // Disable the submit button as ajax is executed
        // when changing the source type.
        document.querySelector('#edit-submit').disabled = true;
      });
    }
  });

  // Drupal's behavior for handling K8s launch template source type.
  Drupal.behaviors.k8sLaunchTemplateSourceType = {
    attach: function (context, settings) {
      const ajax_settings = {
        url: document.URL,
      };

      const ajax = new Drupal.Ajax(null, null, ajax_settings);
      ajax.success = function (response, status, xmlhttprequest) {
        // Enable submit button if ajax succeeds.
        document.getElementById('edit-submit').disabled = false;
      };
      ajax.execute();
    },
  };

})();