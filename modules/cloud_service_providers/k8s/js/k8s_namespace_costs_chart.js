(function () {
  'use strict';

  if (!drupalSettings.k8s || !drupalSettings.k8s.k8s_namespace_costs_chart_json_url) {
    return;
  }

  const newElement = (tag, attrs, ...nodes) => {
    const element = document.createElement(tag);
    if (!(element instanceof Element)) { return; }
    for (const [name, value] of Object.entries(attrs ? attrs : {})) {
      element.setAttribute(name, value);
    }
    for (const node of nodes) {
      if (node instanceof Array) {
        const newElem = newElement(...node);
        newElem && element.append(newElem);
        continue;
      }

      if (node instanceof Node || (typeof node === 'string' && node)) {
        element.append(node);
      }
    }
    return element;
  };

  const costTypeSelect = newElement('select', { id: 'cost_type' });
  const chartPeriodSelect = newElement('select', { id: 'chart_period' });
  const chartDiv = document.getElementById('k8s_namespace_costs_chart');
  chartDiv.append(newElement(
    'div', { id: 'chart_options', style: 'font-size: x-small; text-align: right;' },
    ['span', {},
      ['label', { 'for': costTypeSelect.id }, Drupal.t('Cost type')],
      costTypeSelect],
    ['span', {},
      ['label', { 'for': chartPeriodSelect.id, style: 'margin-left: 10px;' }, Drupal.t('Chart period')],
      chartPeriodSelect]));

  if (drupalSettings.k8s.k8s_namespace_costs_cost_types_json_url) {
    fetch(drupalSettings.k8s.k8s_namespace_costs_cost_types_json_url)
      .then((response) => response.json())
      .then((costTypes) => {
        for (const [value, text] of Object.entries(costTypes)) {
          const newElem = newElement('option', { value, style: 'font-size: x-small;' }, text);
          newElem && costTypeSelect.append(newElem);
        }
        costTypeSelect.value = drupalSettings.k8s.k8s_namespace_costs_chart_ec2_cost_type;
      });
  }

  if (drupalSettings.k8s.k8s_namespace_costs_chart_periods_json_url) {
    fetch(drupalSettings.k8s.k8s_namespace_costs_chart_periods_json_url)
      .then((response) => response.json())
      .then((chartPeriods) => {
        for (const [value, text] of Object.entries(chartPeriods)) {
          const newElem = newElement('option', { value, style: 'font-size: x-small;' }, text);
          newElem && chartPeriodSelect.append(newElem);
        }
        chartPeriodSelect.value = drupalSettings.k8s.k8s_namespace_costs_chart_period || 1;
      });
  }

  const initChart = (costChart) => {
    // Using chart data sent in Page build instead of JSON request upon
    // page load.
    const chartInitialData = Array.from(drupalSettings.k8s.k8s_namespace_costs_chart_initial_data);

    costChart.data.datasets = [];
    chartInitialData.forEach(({ namespace, costs }, i) => {
      const color = d3.interpolateRainbow((i + 0.5) / chartInitialData.length);
      const rgb = d3.rgb(color);
      rgb.opacity = 0.5;
      const costData = costs.map(({ timestamp, cost }) => ({ t: timestamp * 1000, y: cost }));
      costChart.data.datasets.push({
        label: namespace,
        backgroundColor: rgb.brighter(1),
        borderColor: color,
        data: costData,
        type: 'line',
        pointRadius: 0,
        fill: false,
        lineTension: 0,
        borderWidth: 2
      });
    });
    costChart.update();
  };

  const updateChart = (costChart) => {
    const base_url = drupalSettings.k8s.k8s_namespace_costs_chart_json_url;
    const params = new URLSearchParams({ cost_type: costTypeSelect.value, period: chartPeriodSelect.value });
    const update_json_url = `${base_url}?${params.toString()}`;
    fetch(update_json_url)
      .then((response) => response.json())
      .then((chartData) => {
        chartData.forEach(({ costs }, i) => {
          const costData = costs.map(({ timestamp, cost }) => ({ t: timestamp * 1000, y: cost }));
          costChart.data.datasets[i].data = costData;
        });
        costChart.update();
      });
  };

  // Build CPU chart.
  chartDiv.append(newElement('div', { class: 'col-sm-9', style: 'float: none; margin: 0 auto;' }, ['canvas', { id: 'cost_chart' }]));
  const cost_ctx = document.getElementById('cost_chart').getContext('2d');
  const cost_cfg = {
    type: 'bar',
    options: {
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            unit: 'day',
            displayFormats: {
              hour: 'MMM D hA'
            }
          },
          distribution: 'linear',
          bounds: 'ticks',
          ticks: {
            source: 'ticks',
            autoSkip: false
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Cost ($)'
          }
        }]
      },
      tooltips: {
        intersect: false,
        mode: 'nearest',
        callbacks: {
          label(tooltipItem, myData) {
            const label = myData.datasets[tooltipItem.datasetIndex].label || '';
            const precision = costTypeSelect.value === 'on_demand_hourly' ? 4 : 2;
            return `${label}${label ? ': ' : ''}$${parseFloat(tooltipItem.value).toFixed(precision)}`;
          }
        }
      }
    }
  };
  {
    const costChart = new Chart(cost_ctx, cost_cfg);
    initChart(costChart);

    const interval = drupalSettings.k8s.k8s_js_refresh_interval || 10;
    setInterval(() => updateChart(costChart), interval * 1000);
    costTypeSelect.addEventListener('change', () => updateChart(costChart));
    chartPeriodSelect.addEventListener('change', () => updateChart(costChart));
  }
})();
