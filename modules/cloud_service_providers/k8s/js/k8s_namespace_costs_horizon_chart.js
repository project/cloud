(function () {
  'use strict';

  if (!drupalSettings.k8s || !drupalSettings.k8s.k8s_namespace_costs_chart_json_url) {
    return;
  }

  const COSTS_DATA_URL = drupalSettings.k8s.k8s_namespace_costs_chart_json_url;
  const COST_TYPE_OPTIONS_URL = drupalSettings.k8s.k8s_namespace_costs_cost_types_json_url;
  const CHART_PERIOD_OPTIONS_URL = drupalSettings.k8s.k8s_namespace_costs_chart_periods_json_url;

  const DEFAULT_COST_TYPE = drupalSettings.k8s.k8s_namespace_costs_chart_ec2_cost_type;
  const DEFAULT_CHART_PERIOD = drupalSettings.k8s.k8s_namespace_costs_chart_period || 1;

  // Refresh interval (sec).
  const REFRESH_INTERVAL = drupalSettings.k8s.k8s_js_refresh_interval || 10;

  const INITIAL_COSTS_DATA = drupalSettings.k8s.k8s_namespace_costs_chart_initial_data;

  const newElement = (tag, attrs, ...nodes) => {
    const element = document.createElement(tag);
    if (!(element instanceof Element)) { return; }
    for (const [name, value] of Object.entries(attrs ? attrs : {})) {
      element.setAttribute(name, value);
    }
    for (const node of nodes) {
      if (node instanceof Array) {
        const newElement = newElement(...node);
        newElement && element.append(newElement);
        continue;
      }

      if (node instanceof Node || (typeof node === 'string' && node)) {
        element.append(node);
      }
    }
    return element;
  };

  const setDefaultSelectValue = (selectElement, ...values) => {
    const options = [...selectElement.options];
    const value = values.find((val) => options.some((opt) => opt.value === val));
    if (value) {
      selectElement.value = value;
    }
  };

  const addOptions = (selectElement, optionsJsonURL) => {
    if (!optionsJsonURL) { return; }
    fetch(optionsJsonURL)
      .then((response) => response.json())
      .then((json) => {
        for (const [value, text] of Object.entries(json)) {
          const newOption = newElement('option', { value, style: 'font-size: x-small;' }, text);
          newOption && selectElement.append(newOption);
        }
      });
  };

  const costsJsonURL = (costType, period) => {
    const params = new URLSearchParams({ cost_type: costType, period });
    return `${COSTS_DATA_URL}?${params.toString()}`;
  };

  const costTypeSelect = newElement('select', { id: 'cost_type' });
  costTypeSelect.addEventListener('change', () => updateChart());
  setDefaultSelectValue(costTypeSelect, DEFAULT_COST_TYPE);
  addOptions(costTypeSelect, COST_TYPE_OPTIONS_URL);

  const chartPeriodSelect = newElement('select', { id: 'chart_period' });
  chartPeriodSelect.addEventListener('change', () => updateChart());
  setDefaultSelectValue(chartPeriodSelect, DEFAULT_CHART_PERIOD, 1);
  addOptions(chartPeriodSelect, CHART_PERIOD_OPTIONS_URL);

  const chartDiv = document.getElementById('k8s_namespace_costs_chart');
  chartDiv.append(newElement(
    'div', { id: 'chart_options', style: 'font-size: x-small; text-align: right;' },
    ['span', {},
      ['label', { 'for': costTypeSelect.id }, Drupal.t('Cost type')],
      costTypeSelect],
    ['span', {},
      ['label', { 'for': chartPeriodSelect.id, style: 'margin-left: 10px;' }, Drupal.t('Chart period')],
      chartPeriodSelect]));

  const drawChart = (json, init = false) => {
    const chartData = [];
    json.forEach(({ namespace, costs }) => {
      const values = costs.map(({ timestamp, cost }) => [new Date(timestamp * 1000), cost]);
      const selector = `#k8s-namespace-costs-row--${namespace}`;
      chartData.push({ id: namespace, values, selector });
      if (init) {
        chartDiv.append(newElement('div', { id: selector.slice(1) }));
      }
    });
    Drupal.Cloud.drawChart(chartData, true);
  };

  // Using chart data sent in Page build instead of JSON request upon
  // page load.
  const initChart = () => drawChart(INITIAL_COSTS_DATA, true);

  const updateChart = () => fetch(costsJsonURL(costTypeSelect.value, chartPeriodSelect.value))
    .then((response) => response.json())
    .then((json) => drawChart(json));

  initChart();
  setInterval(updateChart, REFRESH_INTERVAL * 1000);

})();
