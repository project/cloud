(function (drupalSettings) {
  'use strict';

  if (!drupalSettings.k8s || !drupalSettings.k8s.metrics_enabled) {
    const k8sNodeResources = document.getElementById('k8s_node_allocated_resources');
    if (k8sNodeResources) {
      k8sNodeResources.parentNode.parentNode.style.display = 'none';
    }
    return;
  }

  const FONT_FAMILY = 'Lucida Grande, -apple-system, BlinkMacSystemFont';
  const FONT_SIZE_RSC_ALLOCATION_RATIO = 19;
  const FONT_SIZE_RSC_USAGE = 15;
  const FONT_SIZE_TITLE = 19;
  const WIDTH = 250;
  const HEIGHT = 250;
  const TITLE_HEIGHT = 20;
  const RING_WIDTH = 50;
  const RADIUS = Math.min(WIDTH, HEIGHT) / 2;
  const FOREGROUND_RING_WIDTH = 30;

  // Create dummy <span> elements to measure the length (pixel) of a string.
  function createDummySpan(id, fontSize) {
    const span = document.createElement('span');
    span.id = id;
    span.style.fontSize = fontSize + 'px';
    span.style.visibility = 'hidden';
    span.style.position = 'absolute';
    span.style.whiteSpace = 'nowrap';
    document.body.appendChild(span);
  }

  createDummySpan('rsc_allocation_ratio_width_check', FONT_SIZE_RSC_ALLOCATION_RATIO);
  createDummySpan('rsc_usage_width_check', FONT_SIZE_RSC_USAGE);
  createDummySpan('title_width_check', FONT_SIZE_TITLE);

  const pie = d3.pie()
    .sort(null)
    .value(function (d) {
      return d.value;
    });

  const arc = d3.arc()
    .innerRadius(RADIUS - RING_WIDTH - FOREGROUND_RING_WIDTH)
    .outerRadius(RADIUS - FOREGROUND_RING_WIDTH);

  function createSvg(data) {
    const svgContainer = document.querySelector(data.selector);
    svgContainer.align = 'center';
    const svgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svgElement.setAttribute('class', data.class);
    svgElement.setAttribute('width', WIDTH);
    svgElement.setAttribute('height', HEIGHT + TITLE_HEIGHT);
    svgElement.style.display = 'inline';
    svgContainer.appendChild(svgElement);
    return d3.select(svgElement);
  }

  // Create a tooltip.
  const tooltip = d3.select('body').append('div')
    .style('opacity', 1)
    .attr('class', 'tooltip')
    .style('background-color', 'white')
    .style('border', 'solid')
    .style('border-width', '2px')
    .style('border-radius', '5px')
    .style('padding', '5px')
    .style('display', 'none');

  // Tooltip event functions.
  const mouseover = function (pie) {
    tooltip.style('display', 'block');

    d3.select(this)
      .style('stroke', pie.data.color)
      .style('stroke-width', 5)
      .style('opacity', 1);
  };

  const mousemove = function (pie) {
    tooltip.html('<strong>' + pie.value + ' ' + pie.data.suffix + '</strong>')
      .style('left', (d3.event.pageX + 15) + 'px')
      .style('top', (d3.event.pageY - 40) + 'px');
  };

  const mouseleave = function (pie) {
    tooltip.style('display', 'none');
    d3.select(this)
      .style('stroke', 'none')
      .style('opacity', 0.8);
  };

  const drawLegend = function () {
    const width = 600;
    const height = 50;
    const padding = 20;
    const xpadding = 90;
    const number = 20;
    const svg = d3.select('#k8s_node_allocated_resources').append('svg')
      .style('max-width', width)
      .attr("preserveAspectRatio", "xMidYMid")
      .attr('viewBox', 0 + ' ' + 0 + ' ' + width + ' ' + height);

    svg.append('text')
      .attr('x', 10)
      .attr('y', 25)
      .style('font-family', FONT_FAMILY)
      .style('font-size', 16)
      .style('font-weight', 900)
      .text('Legend');

    const color = d3.interpolateSpectral;
    const defs = svg.append('defs').append('linearGradient')
      .attr('id', 'legendGradient');

    for (let i = 0; i <= number; i++) {
      defs.append('stop')
        .attr('offset', (i * 100 / number) + '%')
        .attr('stop-color', color((number - i) / number));
    }

    svg.append('rect')
      .attr('x', xpadding)
      .attr('y', 0)
      .attr('width', width - 2 * xpadding)
      .attr('height', height - padding)
      .attr('fill', 'url(#legendGradient)');

    const xScale = d3.scaleLinear()
      .range([0, width - 2 * xpadding])
      .domain([0, 1.0]);

    const axis = svg.append('g')
      .attr('transform', 'translate(' + xpadding + ',' + (height - padding) + ')')
      .call(d3.axisBottom(xScale).tickFormat(d3.format('.0%')));
    axis.select('path').attr('opacity', 0.0);
  };

  function render(data) {
    const svg = d3.select('svg.' + data.class);

    svg.selectAll('.foreground')
      .data(pie(data.pieChart))
      .enter().append('path')
      .attr('class', 'foreground')
      .attr('transform', 'translate(' + WIDTH / 2 + ',' + HEIGHT / 2 + ')')
      .attr('fill', function (d, i) {
        return d.data.color;
      })
      .on('mouseover', mouseover)
      .on('mousemove', mousemove)
      .on('mouseleave', mouseleave)
      .attr('d', arc)
      .transition()
      .duration(1000)
      .attrTween('d', function (d) {
        const interpolate = d3.interpolate(
          // The start angle in each pie chart.
          {startAngle: 0, endAngle: 0},
          // The end angle of each pie chart.
          {startAngle: d.startAngle, endAngle: d.endAngle}
        );
        return function (t) {
          return arc(interpolate(t));
        };
      });

    const rscUsageWidthCheck = document.getElementById('rsc_usage_width_check');
    rscUsageWidthCheck.textContent = data.resourceUsage;
    const rsc_usage_width_check_px = rscUsageWidthCheck.offsetWidth;
    const rsc_usage_height_check_px = rscUsageWidthCheck.offsetHeight;

    const rscAllocationRatioWidthCheck = document.getElementById('rsc_allocation_ratio_width_check');
    rscAllocationRatioWidthCheck.textContent = data.resourceAllocationRatio;
    const rsc_allocation_ratio_width_check_px = rscAllocationRatioWidthCheck.offsetWidth;
    const rsc_allocation_ratio_height_check_px = rscAllocationRatioWidthCheck.offsetHeight;

    const titleWidthCheck = document.getElementById('title_width_check');
    titleWidthCheck.textContent = data.title;
    const title_width_check_px = titleWidthCheck.offsetWidth;
    const title_height_check_px = titleWidthCheck.offsetHeight;

    // Remove the <span/> element.
    rscUsageWidthCheck.textContent = '';
    rscAllocationRatioWidthCheck.textContent = '';

    svg.selectAll('.allocation_ratio')
        .remove();

    svg.append('text')
        .attr('class', 'allocation_ratio')
        .attr('x', WIDTH / 2 + rsc_allocation_ratio_width_check_px / -2)
        .attr('y', HEIGHT / 2 + rsc_allocation_ratio_height_check_px / 2)
        // Or, we can locate a text like this:
        // .attr('transform', 'translate(' + (width / 2 + width_check_px / -2) + ',' + (height / 2 + height_check_px / 2) + ')')
        .style('font-family', FONT_FAMILY)
        .style('font-size', FONT_SIZE_RSC_ALLOCATION_RATIO)
        .style('font-weight', 900)
        .style('fill', d3.rgb(data.pieChart[0].color).darker(1))
        .text(data.resourceAllocationRatio);

    svg.selectAll('.resource_usage')
        .remove();

    svg.append('text')
        .attr('class', 'resource_usage')
        .attr('x', WIDTH / 2 + rsc_usage_width_check_px / -2)
        .attr('y', HEIGHT - rsc_usage_height_check_px * 2.5)
        .style('font-family', FONT_FAMILY)
        .style('font-size', FONT_SIZE_RSC_USAGE)
        .style('fill', d3.schemeSet3[8] + data.pieChart[0].color)
        .text(data.resourceUsage);

    // Add title.
    svg.selectAll('.chart_title')
        .remove();

    svg.append('text')
        .attr('class', '.chart_title')
        .attr('x', WIDTH / 2 + title_width_check_px / -2)
        .attr('y', HEIGHT - title_height_check_px + TITLE_HEIGHT)
        .style('font-family', FONT_FAMILY)
        .style('font-size', FONT_SIZE_TITLE)
        .style('font-weight', 900)
        .style('fill', d3.rgb(data.pieChart[0].color).darker(1))
        .text(data.title);
  }

  const selectors = [{
    selector: '#k8s_node_allocated_resources',
    class: 'k8s_node_cpu_usage'
  }, {
    selector: '#k8s_node_allocated_resources',
    class: 'k8s_node_memory_usage'
  }, {
    selector: '#k8s_node_allocated_resources',
    class: 'k8s_node_pods_allocation'
  }];

  const svgs = selectors.map(createSvg);

  function updateGraph(nodes) {
    let cpu_capacity = 0;
    let cpu_request = 0;
    let memory_capacity = 0;
    let memory_request = 0;
    let pods_capacity = 0;
    let pods_allocation = 0;

    nodes.forEach(function (node) {
      cpu_capacity += parseFloat(node.cpuCapacity);
      cpu_request += parseFloat(node.cpuRequest);
      memory_capacity += parseFloat(node.memoryCapacity);
      memory_request += parseFloat(node.memoryRequest);
      pods_capacity += parseInt(node.podsCapacity);
      pods_allocation += parseInt(node.podsAllocation);
    });

    // CPU usage doughnut chart.
    const cpu_request_ratio = cpu_capacity > 0 ? cpu_request / cpu_capacity : 0;
    const cpu_request_rounded = cpu_request.toFixed(2);
    render({
      svg: svgs.pop(),
      selector: '#k8s_node_allocated_resources',
      class: 'k8s_node_cpu_usage',
      pieChart: [{
        color: d3.interpolateSpectral(1 - cpu_request_ratio),
        value: cpu_request_rounded,
        suffix: 'Cores'
      }, {
        color: d3.interpolateSpectral(1 - cpu_request_ratio - 0.15),
        value: (cpu_capacity - cpu_request_rounded).toFixed(2),
        suffix: 'Cores'
      }],
      resourceUsage: cpu_request_rounded + ' / ' +
        cpu_capacity + ' Cores',
      resourceAllocationRatio: (cpu_request_ratio * 100).toFixed(1) + '%',
      title: 'CPU core usage'
    });

    // Memory usage doughnut chart.
    const memory_request_ratio = memory_capacity > 0 ? memory_request / memory_capacity : 0;
    const memory_request_rounded = (memory_request / 1024 / 1024 / 1024).toFixed(2);
    const memory_capacity_rounded = (memory_capacity / 1024 / 1024 / 1024).toFixed(2);
    render({
      svg: svgs.pop(),
      selector: '#k8s_node_allocated_resources',
      class: 'k8s_node_memory_usage',
      pieChart: [{
        color: d3.interpolateSpectral(1 - memory_request_ratio),
        value: memory_request_rounded,
        suffix: 'GiB'
      }, {
        color: d3.interpolateSpectral(1 - memory_request_ratio - 0.15),
        value: (memory_capacity_rounded - memory_request_rounded).toFixed(2),
        suffix: 'GiB'
      }],
      resourceUsage:
        memory_request_rounded + ' / ' +
        memory_capacity_rounded + ' GiB',
      resourceAllocationRatio: (memory_request_ratio * 100).toFixed(1) + '%',
      title: 'Memory usage'
    });

    // Pods allocation doughnut chart.
    const pods_allocation_ratio = pods_capacity > 0 ? pods_allocation / pods_capacity : 0;
    render({
      svg: svgs.pop(),
      selector: '#k8s_node_allocated_resources',
      class: 'k8s_node_pods_allocation',
      pieChart: [{
        color: d3.interpolateSpectral(1 - pods_allocation_ratio),
        value: pods_allocation,
        suffix: 'Pods'
      }, {
        color: d3.interpolateSpectral(1 - pods_allocation_ratio - 0.15),
        value: pods_capacity - pods_allocation,
        suffix: 'Pods'
      }],
      resourceUsage: pods_allocation + ' / ' + pods_capacity + ' Pods',
      resourceAllocationRatio: (pods_allocation_ratio * 100).toFixed(1) + '%',
      title: 'Pods allocation'
    });
  }

  updateGraph(drupalSettings.k8s.initial_data);
  drawLegend();

  const updateNodeAllocatedResources = function () {
    fetch(drupalSettings.k8s.resource_url)
      .then(response => response.json())
      .then(nodes => updateGraph(nodes));
  };

  document.body.style.position = 'static';

  if (drupalSettings.k8s.use_event_listener == true) {
    document.addEventListener('updatedNodes', function (e) {
      updateGraph(e.detail);
    });
  } else {
    const interval = drupalSettings.k8s.k8s_js_refresh_interval || 10;
    setInterval(updateNodeAllocatedResources, interval * 1000);
  }
}(drupalSettings));
