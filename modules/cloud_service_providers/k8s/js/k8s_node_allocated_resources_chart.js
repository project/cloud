(function () {
  'use strict';

  if (!drupalSettings.k8s || !drupalSettings.k8s.k8s_node_allocated_resources_chart_json_url) {
    return;
  }

  const chart_period = drupalSettings.k8s.k8s_node_allocated_resources_chart_period || 1;

  const chartContainer = document.getElementById('k8s_node_allocated_resources_chart');
  chartContainer.innerHTML += '<div id="node_allocated_resources_chart_options">';
  const chartOptions = document.getElementById('node_allocated_resources_chart_options');
  chartOptions.style.fontSize = 'x-small';
  chartOptions.style.textAlign = 'right';
  chartOptions.innerHTML += '<span>';
  chartOptions.innerHTML += '<span>';

  if (drupalSettings.k8s.k8s_node_allocated_resources_chart_periods_json_url) {
    const chart_periods_json_url = drupalSettings.k8s.k8s_node_allocated_resources_chart_periods_json_url;
    fetch(chart_periods_json_url)
      .then(response => response.json())
      .then(json => {
        const chartOptions = document.getElementById('node_allocated_resources_chart_options');
        const lastSpan = chartOptions.querySelectorAll('span:last-child')[0];
        lastSpan.innerHTML += '<label for="node_allocated_resources_chart_period">' + Drupal.t('Chart period') + '</label>';
        lastSpan.querySelector('label').style.marginLeft = '10px';
        lastSpan.innerHTML += '<select id="node_allocated_resources_chart_period">';

        Object.entries(json).forEach(([index, value]) => {
          document.getElementById('node_allocated_resources_chart_period').innerHTML += '<option value="' + index + '">' + value + '</option>';
        });

        const chartPeriodSelect = document.getElementById('node_allocated_resources_chart_period');
        const options = chartPeriodSelect.options;
        for (let i = 0; i < options.length; i++) {
          options[i].style.fontSize = 'x-small';
        }

        if (node_allocated_resources_chart_period && chartPeriodSelect.querySelector('option[value="' + chart_period + '"]')) {
          chartPeriodSelect.value = chart_period;
        } else if (chartPeriodSelect.querySelector('option[value="1"]')) {
          chartPeriodSelect.value = 1;
        }

        chartPeriodSelect.addEventListener('change', function () {
          updateChart();
        });
      });
  }

  const createData = function (json) {
    const resource_data = [];
    resource_data['cpu'] = [];
    resource_data['memory'] = [];
    resource_data['pod'] = [];
    json.forEach(function (data) {
      const cpu_capacity = data.cpu.cpu_capacity;
      const cpu_usage = data.cpu.cpu_usage;
      const cpu_usage_ratio = cpu_capacity > 0 ? cpu_usage / cpu_capacity : 0;
      const cpu_usage_rounded = cpu_usage.toFixed(2);
      resource_data['cpu'].push({
        t: data.cpu.timestamp * 1000,
        y: (cpu_usage_ratio * 100).toFixed(1),
        usage: cpu_usage_rounded + ' / ' + cpu_capacity + ' Cores',
      });
      const memory_capacity = data.memory.memory_capacity;
      const memory_usage = data.memory.memory_usage;
      const memory_usage_rounded = (memory_usage / 1024 / 1024 / 1024).toFixed(2);
      const memory_capacity_rounded = (memory_capacity / 1024 / 1024 / 1024).toFixed(2);
      const memory_usage_ratio = memory_capacity > 0 ? memory_usage / memory_capacity : 0;
      resource_data['memory'].push({
        t: data.memory.timestamp * 1000,
        y: (memory_usage_ratio * 100).toFixed(1),
        usage: memory_usage_rounded + ' / ' + memory_capacity_rounded + ' GiB',
      });
      const pod_capacity = data.pod.pod_capacity;
      const pod_usage = data.pod.pod_usage;
      const pod_usage_ratio = pod_capacity > 0 ? pod_usage / pod_capacity : 0;
      resource_data['pod'].push({
        t: data.cpu.timestamp * 1000,
        y: (pod_usage_ratio * 100).toFixed(1),
        usage: pod_usage + ' / ' + pod_capacity + ' Pods',
      });
    });
    return resource_data;
  };

  const json_url = drupalSettings.k8s.k8s_node_allocated_resources_chart_json_url;

  const initChart = function () {
    const init_json_url = json_url + '?period=' + chart_period;
    fetch(init_json_url)
      .then(response => response.json())
      .then(json => {
        resource_chart.data.datasets = [];
        const data = createData(json);
        const color = Chart.helpers.color;
        resource_chart.data.datasets.push({
          label: 'CPU core usage',
          backgroundColor: color('green').alpha(0.5).rgbString(),
          borderColor: 'green',
          data: data['cpu'],
          type: 'line',
          pointRadius: 0,
          fill: false,
          lineTension: 0,
          borderWidth: 2
        });
        resource_chart.data.datasets.push({
          label: 'Memory usage',
          backgroundColor: color('blue').alpha(0.5).rgbString(),
          borderColor: 'blue',
          data: data['memory'],
          type: 'line',
          pointRadius: 0,
          fill: false,
          lineTension: 0,
          borderWidth: 2
        });
        resource_chart.data.datasets.push({
          label: 'Pods allocation',
          backgroundColor: color('red').alpha(0.5).rgbString(),
          borderColor: 'red',
          data: data['pod'],
          type: 'line',
          pointRadius: 0,
          fill: false,
          lineTension: 0,
          borderWidth: 2
        });
        resource_chart.update();
      });
  };

  const updateChart = function () {
    const update_json_url = json_url + '?period=' + document.getElementById('node_allocated_resources_chart_period').value;
    fetch(update_json_url)
      .then(response => response.json())
      .then(json => {
        const data = createData(json);
        resource_chart.data.datasets[0].data = data['cpu'];
        resource_chart.data.datasets[1].data = data['memory'];
        resource_chart.data.datasets[2].data = data['pod'];
        resource_chart.update();
      });
  };

  chartContainer.innerHTML += '<div class="col-sm-9" style="float: none; margin: 0 auto"><canvas id="resource_chart"></canvas></div>';
  const panel_title = document.querySelector('.panel-title');
  let font_color;
  if (panel_title) {
    font_color = panel_title.color;
  }
  const resource_ctx = document.getElementById('resource_chart').getContext('2d');
  const resource_cfg = {
    type: 'bar',
    options: {
      legend: {
        labels: {
          fontColor: font_color,
        }
      },
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            unit: 'day',
            displayFormats: {
              hour: 'MMM D hA'
            }
          },
          distribution: 'linear',
          bounds: 'ticks',
          ticks: {
            source: 'ticks',
            autoSkip: false,
            fontColor: font_color,
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Resource Usage (%)',
            fontColor: font_color,
          },
          ticks: {
            source: 'ticks',
            autoSkip: false,
            fontColor: font_color,
          }
        }]
      },
      tooltips: {
        intersect: false,
        mode: 'nearest',
        callbacks: {
          label: function (tooltipItem, myData) {
            let label = myData.datasets[tooltipItem.datasetIndex].label || '';
            if (label) {
              label += ': ';
            }
            const usage = myData.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].usage;
            label += usage + ' (';
            label += parseFloat(tooltipItem.value).toFixed(1);
            label += '%)';
            return label;
          }
        }
      }
    }
  };

  const resource_chart = new Chart(resource_ctx, resource_cfg);
  console.log(resource_chart);

  initChart();

  const interval = drupalSettings.k8s.k8s_js_refresh_interval || 10;
  setInterval(function () {
    updateChart();
  }, interval * 1000);

})();
