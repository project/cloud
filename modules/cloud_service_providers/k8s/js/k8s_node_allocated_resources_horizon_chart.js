/**
 * File splits functionality with k8s_node_allocated_resources_chart.js.
 * This file is used only when the chart_type within block config is checked.
 * Functionality includes displaying data under d3 chart.
 */
(function () {
  'use strict';

  // Check if settings exist.
  if (!drupalSettings.k8s || !drupalSettings.k8s.k8s_node_allocated_resources_chart_json_url) {
    return;
  }

  // Chart period.
  const chart_period = drupalSettings.k8s.k8s_node_allocated_resources_chart_period || 1;

  // Add Period Options.
  const chartContainer = document.getElementById('k8s_node_allocated_resources_chart');
  chartContainer.insertAdjacentHTML('beforeend', '<div id="node_allocated_resources_chart_options">');
  const chartOptions = document.getElementById('node_allocated_resources_chart_options');
  chartOptions.style.fontSize = 'x-small';
  chartOptions.style.textAlign = 'right';
  chartOptions.insertAdjacentHTML('beforeend', '<span>');
  chartOptions.insertAdjacentHTML('beforeend', '<span>');

  // Select logic for Period Options.
  if (drupalSettings.k8s.k8s_node_allocated_resources_chart_periods_json_url) {
    const chart_periods_json_url = drupalSettings.k8s.k8s_node_allocated_resources_chart_periods_json_url;
    fetch(chart_periods_json_url)
      .then(response => response.json())
      .then(json => {
        const lastSpan = chartOptions.querySelectorAll('span:last-child')[0];
        lastSpan.insertAdjacentHTML('beforeend', '<label for="node_allocated_resources_chart_period" style="margin-right: 5px;">' + Drupal.t('Chart period') + '</label>');
        lastSpan.querySelector('label').style.marginLeft = '10px';
        lastSpan.insertAdjacentHTML('beforeend', '<select id="node_allocated_resources_chart_period">');
        const selectElement = document.getElementById('node_allocated_resources_chart_period');
        Object.entries(json).forEach(([index, value]) => {
          selectElement.insertAdjacentHTML('beforeend', '<option value="' + index + '">' + value + '</option>');
        });
        selectElement.style.fontSize = 'x-small';

        if (node_allocated_resources_chart_period && selectElement.querySelector('option[value="' + chart_period + '"]')) {
          selectElement.value = chart_period;
        }
        else if (selectElement.querySelector('option[value="1"]')) {
          selectElement.value = 1;
        }
        selectElement.addEventListener('change', function () {
          updateChart();
        });
      });
  }

  // Define variables and constants.
  const json_url = drupalSettings.k8s.k8s_node_allocated_resources_chart_json_url;
  const json_init = json_url + '?period=' + chart_period;
  const div = document.getElementById('k8s_node_allocated_resources_chart');
  div.style.position = 'relative';

  // Append divs for each resource.
  div.insertAdjacentHTML('beforeend', '<div id="cpu" class="col-12"></div>');
  div.insertAdjacentHTML('beforeend', '<div id="memory" class="col-12"></div>');
  div.insertAdjacentHTML('beforeend', '<div id="pod" class="col-12"></div>');

  // Define divs for each resource.
  const divCpu = document.getElementById('cpu'),
    divMemory = document.getElementById('memory'),
    divPod = document.getElementById('pod');

  // Add labels.
  divCpu.insertAdjacentHTML('beforeend', '<label style="margin-left: 5px;"> CPU usage </label>');
  divMemory.insertAdjacentHTML('beforeend', '<label style="margin-left: 5px;"> Memory usage </label>');
  divPod.insertAdjacentHTML('beforeend', '<label style="margin-left: 5px;"> Pod usage </label>');

  // Format Data
  function formatData(data, item) {
    return data.map((d) => {
      return {
        time: d3.timeParse('%s')(d[item]['timestamp']),
        value: d[item][item + '_usage'] / d[item][item + '_capacity'] * 100,
        capacity: d[item][item + '_capacity'],
      };
    });
  }

  const bisectDate = d3.bisector(function (d) { return d.time; }).left;

  // Get Data and Draw Chart
  function initChart() {
    d3.json(json_init).then((rawData) => {
      const cpuData = formatData(rawData, 'cpu');
      const memoryData = formatData(rawData, 'memory');
      const podData = formatData(rawData, 'pod');
      drawChart(cpuData, divCpu, '#2ca02c');
      drawChart(memoryData, divMemory, '#2ca02c');
      drawChart(podData, divPod, '#2ca02c');
    });
  }

  //Read the Data and Build chart.
  function drawChart(data, selector, color) {
    // Set the dimensions and margins of the graph
    const element = d3.select(div).node();
    const margin = { top: 30, right: 20, bottom: 30, left: 20 },
      width = element.getBoundingClientRect().width - margin.left - margin.right,
      height = 100 - margin.top - margin.bottom;

    // Delete current chart.
    d3.select(selector).select('svg').remove();

    // Append the svg object to the body of the page
    const svg = d3
      .select(selector)
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    // Add X axis --> date format.
    const x = d3
      .scaleTime()
      .domain(
        d3.extent(data, function (d) {
          return d.time;
        })
      )
      .range([0, width]),
      xAxis = svg
        .append('g')
        .attr('transform', 'translate(0,' + height + ')')
        .call(d3.axisBottom(x)).selectAll('.domain, .tick line').remove();

    // Add Y axis --> percentage format.
    const y = d3
      .scaleLinear().nice()
      // Domain can be 0-100 since we're passing % as a value.
      .domain([
        0, 100
      ])
      .range([height, 0]),
      yAxis = d3.axisLeft(y);

    // Add area.
    const area = d3.area()
      // Creates X.
      .x(function (d) {
        return x(d.time);
      })
      // Creates cut in path.
      // White space.
      .y0(y(0))
      // Creates Y.
      .y1(function (d) {
        return y(d.value);
      });

    // Here we draw the chart.
    svg.append('path')
      .datum(data)
      .attr('fill', color)
      .attr('d', area);

    const focus = svg.append('g')
      .style('display', 'none');

    // Get label color.
    const labelColor = document.querySelector('label').style.color;

    // append the x line
    focus.append('line')
      .attr('class', 'x')
      .style('stroke', labelColor)
      .style('stroke-dasharray', '3,3')
      .style('opacity', .5)
      .attr('y1', 0)
      .attr('y2', height);

    // append the y line
    focus.append('line')
      .attr('class', 'y')
      .style('stroke', labelColor)
      .style('stroke-dasharray', '3,3')
      .style('opacity', .5)
      .attr('x1', width)
      .attr('x2', width);

    focus.append('circle')
      .attr('class', 'y')
      .style('fill', 'none')
      .style('stroke', labelColor)
      .attr('r', 2);

    // Add text.
    focus.append('text')
      .attr('class', 'y2')
      .attr('dx', 8)
      .attr('dy', '-.3em')
      .style('font-size', '10px')
      .style('fill', labelColor);

    svg.append('rect')
      .attr('width', width)
      .attr('height', height)
      .style('fill', 'none')
      .style('pointer-events', 'all')
      .on('mouseover', function () { focus.style('display', null); })
      .on('mouseout', function () { focus.style('display', 'none'); })
      .on('mousemove', mousemove);

    function mousemove() {
      const x0 = x.invert(d3.mouse(this)[0]),
        i = bisectDate(data, x0, 1);

      if (!data[i - 1] || !data[i]) {
        return;
      }

      const d0 = data[i - 1],
        d1 = data[i],
        d = x0 - d0.time > d1.time - x0 ? d1 : d0;

      focus.select('circle.y')
        .attr('transform',
          'translate(' + x(d.time) + ',' +
          y(d.value) + ')');

      focus.select('text.y2')
        .attr('transform',
          'translate(' + x(d.time) + ',' +
          y(d.value) + ')')
        .text(d.value.toFixed(2) + '%');

      focus.select('.x')
        .attr('transform',
          'translate(' + x(d.time) + ',' +
          y(d.value) + ')')
        .attr('y2', height - y(d.value));

      focus.select('.y')
        .attr('transform',
          'translate(' + width * -1 + ',' +
          y(d.value) + ')')
        .attr('x2', width + width);
    }

  }

  // Update function.
  const updateChart = function () {
    const chartPeriodElement = document.getElementById('node_allocated_resources_chart_period');
    if (!chartPeriodElement) {
      return;
    }
    const update_json_url = json_url + '?period=' + chartPeriodElement.value;
    fetch(update_json_url)
      .then(response => response.json())
      .then(rawData => {
        const cpuData = formatData(rawData, 'cpu');
        const memoryData = formatData(rawData, 'memory');
        const podData = formatData(rawData, 'pod');
        drawChart(cpuData, divCpu, '#2ca02c');
        drawChart(memoryData, divMemory, '#2ca02c');
        drawChart(podData, divPod, '#2ca02c');
      });
  };

  // Init chart.
  initChart();

  // Auto Update chart.
  const interval = drupalSettings.k8s.k8s_js_refresh_interval || 10;
  setInterval(function () {
    updateChart();
  }, interval * 1000);

  // Resize.
  window.addEventListener('resize', function () {
    updateChart();
  });

})();
