(function (drupalSettings) {

  'use strict';

  if (!drupalSettings.k8s || !drupalSettings.k8s.metrics_enabled) {
    document.querySelector('#k8s_node_heatmap').parentNode.parentNode.style.display = 'none';
    return;
  }

  const FONT_FAMILY = 'Lucida Grande, -apple-system, BlinkMacSystemFont';
  const FONT_SIZE_AXIS_LABEL = 13;
  const RECT_PX = 50;
  const MAX_WIDTH = 750;

  const updateGraph = function (pods_count, nodes) {

    let pods = [];
    let index = 0;
    let rows = 1;
    let node_name;
    let node_index = 1;

    const k8sNodeHeatmap = document.querySelector('#k8s_node_heatmap');
    const svgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svgElement.style.width = '100%';
    svgElement.style.maxWidth = MAX_WIDTH + 'px';
    k8sNodeHeatmap.appendChild(svgElement);

    const offset_width = svgElement.clientWidth;
    let pods_auto_count = Math.floor(offset_width / RECT_PX);

    if (pods_auto_count === 0) {
      pods_auto_count = 10;
    }

    if (!pods_count) {
      pods_count = pods_auto_count;
    }

    nodes.forEach(function (node) {
      if (node_name !== node.name) {
        node_name = node.name;
        node_index++;
      }

      for (let i = 0; i < node.podsCapacity; i++) {
        index++;
        if (index === parseInt(pods_count) + 1) {
          rows++;
          index = 1;
        }
        const pod_name = i < node.podsAllocation && node.pods[i]
          ? node.pods[i].name
          : '';

        let cpu_usage = i < node.podsAllocation && node.pods[i]
          ? node.pods[i].cpuUsage * 100 * 3 + 20
          : 0;

        cpu_usage = Math.round(cpu_usage * 100) / 100;

        const memory_usage = i < node.podsAllocation && node.pods[i]
          ? Math.floor(node.pods[i].memoryUsage / 1024 / 1024)
          : '0';

        pods.push({
          index: index,
          node_index: node_index,
          row: rows,
          nodeName: node.name,
          name: pod_name,
          cpuUsage: cpu_usage,
          memoryUsage: memory_usage
        });
      }
    });

    let max_pods_capacity = index;
    if (rows > 1) {
      max_pods_capacity = parseInt(pods_count);
    }

    // Set the dimensions and margins of the graph.
    const pods_allocation_scale
      = [...Array(max_pods_capacity).keys()].map(i => ++i);

    const margin = {top: 0, right: 15, bottom: 20, left: 15};
    const width = max_pods_capacity * RECT_PX;
    let height = rows * RECT_PX;
    height = height < RECT_PX
      ? RECT_PX - margin.top / 4 - margin.bottom / 4
      : height;

    // Clear the k8s_node_heatmap element and set styles
    k8sNodeHeatmap.innerHTML = '';
    k8sNodeHeatmap.style.position = 'relative';
    k8sNodeHeatmap.style.textAlign = 'center';

    const podsCountOptionDiv = document.createElement('div');
    podsCountOptionDiv.id = 'pods_count_option';
    podsCountOptionDiv.style.fontSize = 'x-small';
    podsCountOptionDiv.style.textAlign = 'right';
    k8sNodeHeatmap.appendChild(podsCountOptionDiv);

    const label = document.createElement('label');
    label.setAttribute('for', 'pods_count');
    label.textContent = 'Pods count of row';
    podsCountOptionDiv.appendChild(label);

    const select = document.createElement('select');
    select.id = 'pods_count';
    ['Auto', 10, 15, 20, 25, 50, 100].forEach(val => {
      const option = document.createElement('option');
      option.value = val === 'Auto' ? pods_auto_count : val;
      option.textContent = val;
      select.appendChild(option);
    });
    select.value = pods_count;
    podsCountOptionDiv.appendChild(select);

    select.addEventListener('change', function () {
      updateNodeHeatmap(this.value);
    });

    // Append the svg object to the body of the page.
    const svg = d3.select('#k8s_node_heatmap')
      .append('svg')
      .attr('class', 'node_heatmap')
      .attr("viewBox", "0 0 " + (width + margin.left + margin.right) + " " + (height + margin.top + margin.bottom))
      .attr("preserveAspectRatio", "xMidYMid")
      .style('max-width', MAX_WIDTH + 'px')
      .append('g')
      .style('font-family', FONT_FAMILY)
      .attr('transform',
        'translate(' + margin.left + ', ' + margin.top + ')');

    // Build X scales and axis:
    const x = d3.scaleBand()
      .range([0, width])
      .domain(pods_allocation_scale)
      .padding(0.1);

    svg.append('g')
      .style('font-size', FONT_SIZE_AXIS_LABEL)
      .style('font-family', FONT_FAMILY)
      .attr('transform', 'translate(0,' + height + ')')
      .call(d3.axisBottom(x).tickSize(0))
      .select('.domain').remove();

    let y_axis_scale
      = [...Array(rows).keys()].map(i => ++i);
    y_axis_scale = y_axis_scale.reverse();

    // Build Y scales and axis:
    const y = d3.scaleBand()
      .range([height, 0])
      .domain(y_axis_scale)
      .padding(0.1);

    // Build color scale.
    const my_color_green = d3.scaleSequential(d3.interpolateGreens).domain([1, 100]);
    const my_color_blue = d3.scaleSequential(d3.interpolateBlues).domain([1, 100]);

    // Create a tooltip.
    const tooltip = d3.select('#k8s_node_heatmap')
      .append('div')
      .style('opacity', 0)
      .attr('class', 'tooltip')
      .style('background-color', 'white')
      .style('border', 'solid')
      .style('border-width', '2px')
      .style('border-radius', '5px')
      .style('padding', '5px');

    // Three function that change the tooltip when user hover / move / leave a cell.
    const mouseover = function (pod) {
      tooltip.style('opacity', 1);
      d3.select(this)
        .style('stroke', '#cc6600')
        .style('opacity', 1);
    };

    const mousemove = function (pod) {
      const client_rect = this.getBoundingClientRect();
      const div = document.getElementById("k8s_node_heatmap");
      const div_rect = div.getBoundingClientRect();
      tooltip
        .html('<strong>Node: ' + pod.nodeName + '<br/>Pod: ' + pod.name + '</strong><br />CPU: ' + pod.cpuUsage + ' %<br />Memory: ' + pod.memoryUsage + ' MiB')
        .style('display', 'block')
        .style('left', (client_rect.left - div_rect.left - client_rect.width / 2) + 'px')
        .style('top', (client_rect.top - div_rect.top + client_rect.height) + 'px')
    };

    const mouseleave = function (pod) {
      tooltip.style('display', 'none');
      d3.select(this)
        .style('stroke', 'none')
        .style('opacity', 0.8);
    };

    // Add the squares.
    svg.selectAll('svg.node_heatmap')
      .exit()
      .remove()
      .data(pods, function (pod) {
        return pod.row + ':' + pod.index;
      })
      .enter()
      .append('rect')
      .attr('x', function (pod) {
        return x(pod.index);
      })
      .attr('y', function (pod) {
        return y(pod.row);
      })
      .attr('rx', 5)
      .attr('ry', 5)
      .attr('width', x.bandwidth())
      .attr('height', y.bandwidth())
      // 20: Adjust color.
      .style('fill', function (pod) {
        if (pod.node_index % 2 === 0) {
          return my_color_green(pod.cpuUsage + 20);
        } else {
          return my_color_blue(pod.cpuUsage + 20);
        }
      })
      .style('stroke-width', 4)
      .style('stroke', 'none')
      .style('opacity', 0.8)
      .on('mouseover', mouseover)
      .on('mousemove', mousemove)
      .on('mouseleave', mouseleave);
  }

  // Read the data.
  const updateNodeHeatmap = function (pods_count) {
    fetch(drupalSettings.k8s.resource_url)
      .then(response => response.json())
      .then(nodes => {
        updateGraph(pods_count, nodes);
      });
  };

  // Update the graph with the initial data set.
  updateGraph(null, drupalSettings.k8s.initial_data);

  if (drupalSettings.k8s.use_event_listener == true) {
    document.addEventListener('updatedNodes', function (e) {
      const pods_count = document.querySelector('#pods_count').value;
      updateGraph(pods_count, e.detail);
    });
  } else {
    const interval = drupalSettings.k8s.k8s_js_refresh_interval || 10;
    setInterval(function () {
      const pods_count = document.querySelector('#pods_count').value;
      updateNodeHeatmap(pods_count);
    }, interval * 1000);
  }
}(drupalSettings));
