(function () {
  'use strict';

  if (!drupalSettings.k8s || !drupalSettings.k8s.k8s_project_costs_chart_json_url) {
    return;
  }

  const newElement = (tag, attrs, ...nodes) => {
    const element = document.createElement(tag);
    if (!(element instanceof Element)) { return; }
    for (const [name, value] of Object.entries(attrs ? attrs : {})) {
      element.setAttribute(name, value);
    }
    for (const node of nodes) {
      if (node instanceof Array) {
        const newElem = newElement(...node);
        newElem && element.append(newElem);
        continue;
      }

      if (node instanceof Node || (typeof node === 'string' && node)) {
        element.append(node);
      }
    }
    return element;
  };

  const costTypeSelect = newElement('select', { id: 'project_cost_type' });
  const chartPeriodSelect = newElement('select', { id: 'project_chart_period' });
  const chartDiv = document.getElementById('k8s_project_costs_chart');
  chartDiv.append(newElement(
    'div', { id: 'project_chart_options', style: 'font-size: x-small; text-align: right;' },
    ['span', {},
      ['label', { 'for': costTypeSelect.id }, Drupal.t('Cost type')],
      costTypeSelect],
    ['span', {},
      ['label', { 'for': chartPeriodSelect.id, style: 'margin-left: 10px;' }, Drupal.t('Chart period')],
      chartPeriodSelect]));

  const defaultCostType = drupalSettings.k8s.k8s_project_costs_chart_ec2_cost_type;
  if (drupalSettings.k8s.k8s_project_costs_cost_types_json_url) {
    fetch(drupalSettings.k8s.k8s_project_costs_cost_types_json_url)
      .then((response) => response.json())
      .then((costTypes) => {
        for (const [value, text] of Object.entries(costTypes)) {
          const newElem = newElement('option', { value, style: 'font-size: x-small;' }, text);
          newElem && costTypeSelect.append(newElem);
        }
        costTypeSelect.value = defaultCostType;
      });
  }

  const defaultChartPeriod = drupalSettings.k8s.k8s_project_costs_chart_period || 1;
  if (drupalSettings.k8s.k8s_project_costs_chart_periods_json_url) {
    fetch(drupalSettings.k8s.k8s_project_costs_chart_periods_json_url)
      .then((response) => response.json())
      .then((chartPeriods) => {
        for (const [value, text] of Object.entries(chartPeriods)) {
          const newElem = newElement('option', { value, style: 'font-size: x-small;' }, text);
          newElem && chartPeriodSelect.append(newElem);
        }
        chartPeriodSelect.value = defaultChartPeriod;
      });
  }

  const apiURL = (cost_type, period) => {
    const base = drupalSettings.k8s.k8s_project_costs_chart_json_url;
    const params = new URLSearchParams({ cost_type, period });
    return `${base}?${params.toString()}`;
  };

  const initChart = (costChart) => {
    fetch(apiURL(defaultCostType, defaultChartPeriod))
      .then((response) => response.json())
      .then((chartData) => {
        costChart.data.datasets = [];
        chartData.forEach(({ project, costs }, i) => {
          const color = d3.interpolateRainbow((i + 0.5) / chartData.length);
          const rgb = d3.rgb(color);
          rgb.opacity = 0.5;
          const costData = costs.map(({ timestamp, cost }) => ({ t: timestamp * 1000, y: cost }));
          costChart.data.datasets.push({
            label: project,
            backgroundColor: rgb.brighter(1),
            borderColor: color,
            data: costData,
            type: 'line',
            pointRadius: 0,
            fill: false,
            lineTension: 0,
            borderWidth: 2
          });
        });
        costChart.update();
      });
  };

  const updateChart = (costChart) => {
    fetch(apiURL(costTypeSelect.value, chartPeriodSelect.value))
      .then((response) => response.json())
      .then((chartData) => {
        chartData.forEach(({ costs }, i) => {
          const costData = costs.map(({ timestamp, cost }) => ({ t: timestamp * 1000, y: cost }));
          costChart.data.datasets[i].data = costData;
        });
        costChart.update();
      });
  };

  // Build Cost chart.
  chartDiv.append(
    newElement('div', { class: 'col-sm-9', style: 'float: none; margin: 0 auto;' },
      ['canvas', { id: 'project_cost_chart' }]));
  const cost_ctx = document.getElementById('project_cost_chart').getContext('2d');
  const cost_cfg = {
    type: 'bar',
    options: {
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            unit: 'day',
            displayFormats: {
              hour: 'MMM D hA'
            }
          },
          distribution: 'linear',
          bounds: 'ticks',
          ticks: {
            source: 'ticks',
            autoSkip: false
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Cost ($)'
          }
        }]
      },
      tooltips: {
        intersect: false,
        mode: 'nearest',
        callbacks: {
          label(tooltipItem, myData) {
            const label = myData.datasets[tooltipItem.datasetIndex].label || '';
            const precision = costTypeSelect.value === 'on_demand_hourly' ? 4 : 2;
            return `${label}${label ? ': ' : ''}$${parseFloat(tooltipItem.value).toFixed(precision)}`;
          }
        }
      }
    }
  };
  {
    const costChart = new Chart(cost_ctx, cost_cfg);
    initChart(costChart);

    const interval = drupalSettings.k8s.k8s_js_refresh_interval || 10;
    setInterval(() => updateChart(costChart), interval * 1000);
    costTypeSelect.addEventListener('change', () => updateChart(costChart));
    chartPeriodSelect.addEventListener('change', () => updateChart(costChart));
  }
})();
