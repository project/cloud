(function (drupalSettings) {

  'use strict';

  const fetchAllocatedResources = function () {
    fetch(drupalSettings.k8s.resource_url)
      .then(response => response.json())
      .then(function (nodes) {
        const event = new CustomEvent('updatedNodes', {detail: nodes});
        document.dispatchEvent(event);
      }
    );
  };

  const interval = drupalSettings.k8s.k8s_js_refresh_interval || 10;
  setInterval(fetchAllocatedResources, interval * 1000);

}(drupalSettings));
