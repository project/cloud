<?php

/**
 * @file
 * Builds placeholder replacement tokens for k8s-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function k8s_token_info(): array {
  $types['k8s_launch_template'] = [
    'name' => t('K8s Launch Template'),
    'description' => t('Tokens related to individual K8s launch template.'),
    'needs-data' => 'k8s_launch_template',
  ];
  $launch_template['name'] = [
    'name' => t('List of launch template name'),
    'description' => t('Enter the name of the launch template entity.'),
  ];
  $launch_template['launch_template_link'] = [
    'name' => t('Launch Template Link'),
    'description' => t('An absolute link to launch template.'),
  ];
  $launch_template['launch_template_edit_link'] = [
    'name' => t('Launch Template Edit Link'),
    'description' => t('An absolute link to edit the launch template.'),
  ];
  $launch_template['changed'] = [
    'name' => t('Change date'),
    'description' => t('The launch template change date.'),
  ];
  $types['k8s_launch_template_email'] = [
    'name' => t('K8s Launch Template Email'),
    'description' => t('Tokens related to individual K8s launch template email.'),
    'needs-data' => 'k8s_launch_template_email',
  ];
  $launch_template_email['launch_templates'] = [
    'name' => t('List of launch templates'),
    'description' => t('List of launch templates to display to a user.'),
  ];
  $types['k8s_launch_template_request_email'] = [
    'name' => t('K8s Launch Template Request Email'),
    'description' => t('Tokens related to individual K8s launch template request email.'),
    'needs-data' => 'k8s_launch_template_request_email',
  ];
  $launch_template_request_email['launch_templates_request'] = [
    'name' => t('List of request launch templates'),
    'description' => t('List of request launch templates to display to a user.'),
  ];
  return [
    'types' => $types,
    'tokens' => [
      'k8s_launch_template' => $launch_template,
      'k8s_launch_template_email' => $launch_template_email,
      'k8s_launch_template_request_email' => $launch_template_request_email,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function k8s_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {

  $replacements = [];
  if ($type === 'k8s_launch_template' && !empty($data['k8s_launch_template'])) {
    $replacements = k8s_launch_template_tokens($tokens, $data);
  }
  elseif ($type === 'k8s_launch_template_request' && !empty($data['k8s_launch_template_request'])) {
    $replacements = k8s_launch_template_request_tokens($tokens, $data);
  }
  elseif ($type === 'k8s_launch_template_email') {
    $replacements = k8s_launch_template_email_tokens($tokens, $data);
  }
  elseif ($type === 'k8s_launch_template_request_email') {
    $replacements = k8s_launch_template_request_email_tokens($tokens, $data);
  }
  return $replacements;
}

/**
 * Setup launch template email token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function k8s_launch_template_email_tokens(array $tokens, array $data): array {
  $replacements = [];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'launch_templates':
        $replacements[$original] = $data['launch_templates'];
        break;
    }
  }
  return $replacements;
}

/**
 * Setup launch template request email token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function k8s_launch_template_request_email_tokens(array $tokens, array $data): array {
  $replacements = [];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'launch_templates_request':
        $replacements[$original] = $data['launch_templates_request'];
        break;

      case 'site_url':
        global $base_url;
        $replacements[$original] = t('<a href=":url">:url</a>', [
          ':url' => $base_url,
        ]);
    }
  }
  return $replacements;
}

/**
 * Setup launch template token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function k8s_launch_template_tokens(array $tokens, array $data): array {
  $replacements = [];
  /** @var Drupal\cloud\Entity\CloudLaunchTemplate $launch_template */
  $launch_template = $data['k8s_launch_template'];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'name':
        $replacements[$original] = $launch_template->getName();
        break;

      case 'launch_template_link':
        $replacements[$original] = $launch_template->toUrl('canonical', ['absolute' => TRUE])->toString();
        break;

      case 'launch_template_link_edit':
        $replacements[$original] = $launch_template->toUrl('edit-form', ['absolute' => TRUE])
          ->toString();
        break;

      case 'changed':
        $replacements[$original] = \Drupal::service('date.formatter')->format($launch_template->changed->value, 'custom', 'Y-m-d H:i:s O');
        break;
    }
  }
  return $replacements;
}

/**
 * Setup launch template token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function k8s_launch_template_request_tokens(array $tokens, array $data): array {
  $replacements = [];
  /** @var Drupal\cloud\Entity\CloudLaunchTemplate $launch_template */
  $launch_template = $data['k8s_launch_template_request'];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'name':
        $replacements[$original] = $launch_template->getName();
        break;

      case 'launch_template_link':
        $replacements[$original] = t('<a href=":url">:url</a>', [
          ':url' => $launch_template->toUrl('canonical', ['absolute' => TRUE])->toString(),
        ]);
        break;

      case 'launch_template_link_edit':
        $replacements[$original] = t('<a href=":url">:url</a>', [
          ':url' => $launch_template->toUrl('edit-form', ['absolute' => TRUE])->toString(),
        ]);
        break;

      case 'launch_template_button_approve':
        $replacements[$original] = $launch_template->toUrl('approve', ['absolute' => TRUE])->toString();
        break;

      case 'changed':
        $replacements[$original] = \Drupal::service('date.formatter')->format($launch_template->changed->value, 'custom', 'Y-m-d H:i:s O');
        break;
    }
  }
  return $replacements;
}
