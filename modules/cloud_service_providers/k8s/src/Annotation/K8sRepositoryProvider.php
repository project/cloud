<?php

namespace Drupal\k8s\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an K8sRepositoryProvider annotation object.
 *
 * @see \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderBase
 * @see \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderInterface
 * @see \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderManager
 * @see plugin_api
 *
 * @Annotation
 */
class K8sRepositoryProvider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the module providing the plugin.
   *
   * @var string
   */
  public $module;

}
