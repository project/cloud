<?php

namespace Drupal\k8s\Controller;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\cloud\Controller\CloudApiControllerBase;
use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Traits\CloudResourceTrait;
use Drupal\k8s\Entity\K8sNode;
use Drupal\k8s\Entity\K8sPod;
use Drupal\k8s\Form\K8sContentFormInterface;
use Drupal\k8s\Service\K8sOperationsService;
use Drupal\k8s\Service\K8sServiceInterface;
use Drupal\k8s\Traits\K8sBlockTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller responsible for "update" urls.
 *
 * This class is mainly responsible for
 * updating the K8s entities from urls.
 */
class ApiController extends CloudApiControllerBase implements ApiControllerInterface {

  use K8sBlockTrait;
  use CloudResourceTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The K8s service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  private $k8sService;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * The cloud store plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager
   */
  private $cloudStorePluginManager;

  /**
   * Cloud service interface.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  private $cloudService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The configuration data.
   *
   * @var array
   */
  private $configuration = ['cloud_context' => ''];

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * ApiController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   Object for interfacing with K8s API.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger Object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager $plugin_manager
   *   The cloud store plugin manager.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   Cloud service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    K8sServiceInterface $k8s_service,
    Messenger $messenger,
    RequestStack $request_stack,
    RendererInterface $renderer,
    Connection $database,
    CloudStorePluginManager $plugin_manager,
    CloudServiceInterface $cloud_service,
    AccountInterface $current_user,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->k8sService = $k8s_service;
    $this->messenger = $messenger;
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->database = $database;
    $this->cloudStorePluginManager = $plugin_manager;
    $this->cloudService = $cloud_service;
    $this->currentUser = $current_user;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return ApiController
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('k8s'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('database'),
      $container->get('plugin.manager.cloud_store_plugin'),
      $container->get('cloud'),
      $container->get('current_user'),
      $container->get('plugin.manager.cloud_config_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateAllResources($cloud_context): RedirectResponse {
    k8s_update_resources($cloud_context);

    return $this->redirect('view.k8s_node.list', [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateNodeList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('node', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateNamespaceList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('namespace', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updatePodList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('pod', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateNetworkPolicyList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('network_policy', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateDeploymentList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('deployment', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateReplicaSetList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('replica_set', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateServiceList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('service', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateCronJobList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('cron_job', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateJobList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('job', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateResourceQuotaList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('resource_quota', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateLimitRangeList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('limit_range', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateSecretList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('secret', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateConfigMapList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('config_map', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateRoleList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('role', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateClusterRoleList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('cluster_role', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolumeList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('persistent_volume', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateStorageClassList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('storage_class', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateStatefulSetsList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('stateful_set', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateIngressList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('ingress', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateDaemonSetList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('daemon_set', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateEndpointList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('endpoint', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateEventList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('event', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolumeClaimList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('persistent_volume_claim', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateClusterRoleBindingList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('cluster_role_binding', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateServiceAccountsList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('service_account', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateApiServiceList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('api_service', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateRoleBindingsList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('role_binding', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updatePriorityClassesList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('priority_class', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateHorizontalPodAutoscalersList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('horizontal_pod_autoscaler', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeMetrics($cloud_context, K8sNode $k8s_node): JsonResponse {
    $plugin = NULL;
    try {
      $plugin = $this->cloudStorePluginManager->loadPluginVariant('k8s_node_resource_store');
    }
    catch (PluginNotFoundException $e) {
      return new JsonResponse([]);
    }

    $from_time = time() - 7 * 24 * 60 * 60;
    $to_time = time();

    $node_name = $k8s_node->getName();
    /** @var \Drupal\k8s\Plugin\cloud\store\K8sCloudStorePlugin $plugin */
    $result = $plugin->extract('k8s_node_resource_store', $cloud_context, $node_name, $from_time, $to_time);

    $data = [];
    foreach ($result ?: [] as $key => $record) {
      if ($key !== 'total_cost') {
        $date = date('Y-m-d H:i', $record['created']);
        $resources = $record['resources'];
        $data[] = [
          'timestamp' => strtotime($date),
          'cpu' => (float) $resources['cpu'],
          'memory' => (float) ($resources['memory'] ?? 0),
        ];
      }
    }
    return new JsonResponse($data);
  }

  /**
   * {@inheritdoc}
   */
  public function getPodMetrics($cloud_context, K8sPod $k8s_pod): JsonResponse {
    $pod_name = $k8s_pod->getName();
    $pod_namespace = $k8s_pod->getNamespace();

    $plugin = NULL;
    try {
      $plugin = $this->cloudStorePluginManager->loadPluginVariant('k8s_pod_resource_store');
    }
    catch (PluginNotFoundException $e) {
      return new JsonResponse([]);
    }

    $from_time = time() - 7 * 24 * 60 * 60;
    $to_time = time();

    /** @var \Drupal\k8s\Plugin\cloud\store\K8sCloudStorePlugin $plugin */
    $result = $plugin->extract('k8s_pod_resource_store', $cloud_context, "$pod_namespace:$pod_name", $from_time, $to_time);

    $data = [];
    foreach ($result ?: [] as $key => $record) {
      if ($key !== 'total_cost') {
        $date = date('Y-m-d H:i', $record['created']);
        $resources = $record['resources'];
        $data[] = [
          'timestamp' => strtotime($date),
          'cpu' => (float) $resources['cpu'],
          'memory' => (float) ($resources['memory'] ?? 0),
        ];
      }
    }

    return new JsonResponse($data);
  }

  /**
   * Get all metrics information on all nodes in a cluster of $cloud_context.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   All metrics information on all nodes across a cluster specified by
   *   $cloud_context.
   */
  public function getNodeAllocatedResourcesList($cloud_context): JsonResponse {
    return $this->getNodeAllocatedResources($cloud_context);
  }

  /**
   * Get the metrics information on all node(s)
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The metrics information on node(s).
   */
  public function getAllNodeAllocatedResources(): JsonResponse {
    return new JsonResponse($this->loadAllNodeAllocatedResources());
  }

  /**
   * Get the metrics information on the node(s)
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   * @param \Drupal\k8s\Entity\K8sNode $k8s_node
   *   The K8s node entity to expect to get the metrics information.  If this
   *   parameter is omitted, it assumes that all nodes are specified.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The metrics information on node(s).
   */
  public function getNodeAllocatedResources($cloud_context, ?CloudProjectInterface $cloud_project = NULL, ?K8sNode $k8s_node = NULL): JsonResponse {
    return new JsonResponse($this->loadNodeAllocatedResources($cloud_context, $cloud_project, $k8s_node));
  }

  /**
   * Helper method to update entities.
   *
   * @param string $entity_type_name
   *   The entity type name.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   */
  private function updateEntityList($entity_type_name, $cloud_context): RedirectResponse {
    if (empty($cloud_context)) {
      return $this->updateAllEntityList($entity_type_name);
    }

    $update_method_name = 'update' . $this->getShortEntityTypeNamePluralCamelByTypeName('k8s_' . $entity_type_name);
    $this->k8sService->setCloudContext($cloud_context);
    $updated = $this->k8sService->$update_method_name();

    $labels = $this->getDisplayLabels('k8s_' . $entity_type_name);
    if ($updated !== FALSE) {
      $this->messageUser($this->t('Updated @name.', ['@name' => $labels['plural']]));
    }
    else {
      $this->messageUser($this->t('Unable to update @name.', ['@name' => $labels['plural']]), 'error');
    }

    // Update the cache.
    $this->cloudService->invalidateCacheTags();

    return $this->redirect("view.k8s_$entity_type_name.list", [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Helper method to update all entities.
   *
   * @param string $entity_type_name
   *   The entity type name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   */
  private function updateAllEntityList($entity_type_name): RedirectResponse {
    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'k8s',
      ]);
    $update_method_name = 'update' . $this->getShortEntityTypeNamePluralCamelByTypeName('k8s_' . $entity_type_name);

    $labels = $this->getDisplayLabels('k8s_' . $entity_type_name);
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $this->k8sService->setCloudContext($cloud_context);
      $updated = $this->k8sService->$update_method_name();

      $resource_link = Link::fromTextAndUrl(
        $labels['plural'],
        Url::fromRoute(
          "entity.k8s_{$entity_type_name}.collection",
          ['cloud_context' => $cloud_context]
        )
      )->toString();

      $cloud_config_link = $cloud_config->getCloudConfigLink();

      if ($updated !== FALSE) {
        $this->messageUser($this->t('Updated %resource_link of %cloud_config_link cloud service provider.', [
          '%resource_link' => $resource_link,
          '%cloud_config_link' => $cloud_config_link ?? $cloud_config->getName(),
        ]));
      }
      else {
        $this->messageUser($this->t('Unable to update %resource_link of %cloud_config_link cloud service provider.', [
          '%resource_link' => $resource_link,
          '%cloud_config_link' => $cloud_config_link ?? $cloud_config->getName(),
        ]), 'error'
        );
      }
    }

    // Update the cache.
    $this->cloudService->invalidateCacheTags();

    return $this->redirect("view.k8s_$entity_type_name.all");
  }

  /**
   * Helper method to add messages for the end user.
   *
   * @param string $message
   *   The message.
   * @param string $type
   *   The message type: error or message.
   */
  private function messageUser($message, $type = 'message'): void {
    switch ($type) {
      case 'error':
        $this->messenger->addError($message);
        break;

      case 'message':
        $this->messenger->addStatus($message);
        break;

      default:
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityCount(Request $request, string $cloud_context, string $entity_type_id): JsonResponse {
    $namespace = $request->get('namespace');

    $params = !empty($cloud_context)
      ? ['cloud_context' => $cloud_context] : [];
    if (!empty($namespace)) {
      $params['namespace'] = $namespace;
    }
    if (!empty($cloud_context)) {
      $this->configuration['cloud_context'] = $cloud_context;
    }
    $count = $this->getResourceCount(
      $entity_type_id,
      // Use a static trait method through CloudService.
      'list ' . CloudService::convertUnderscoreToWhitespace($entity_type_id),
      $params
    );

    return new JsonResponse(['count' => $count]);
  }

  /**
   * {@inheritdoc}
   */
  public function getPodLog(string $cloud_context, string $entity_id): JsonResponse {
    // Get entity data.
    $entity = $this->entityTypeManager
      ->getStorage('k8s_pod')
      ->load($entity_id);

    // Get values for function's parameter.
    $container_options = [];
    /** @var \Drupal\k8s\Entity\K8sPod $entity */
    foreach ($entity->getContainers() ?: [] as $container) {
      $container_obj = Yaml::decode($container['value']);
      $container_options[$container_obj['name']] = $container_obj['name'];
    }
    $params = [
      'metadata' => ['name' => $entity->getName()],
    ];
    $container_name = NULL;
    if (!empty($container_options)) {
      $container_name = array_keys($container_options)[0];
    }

    // Get logs of K8s pod.
    $this->k8sService->setCloudContext($cloud_context);
    $logs = $this->k8sService->getPodLogs($entity->getNamespace(), $params, $container_name);

    return new JsonResponse(['log' => $logs]);
  }

  /**
   * {@inheritdoc}
   */
  public function operateEntity(Request $request, string $cloud_context, string $entity_type_id, string $entity_id, string $command): JsonResponse {
    $entity = $command !== 'create'
      ? $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->load($entity_id)
      : $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->create([
          'cloud_context' => $cloud_context,
        ]);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The instance has already been deleted.',
      ], 404);
    }

    $method_name = '';
    $parameter = [];
    switch ($command) {
      case 'update':
        switch ($entity_type_id) {
          case 'k8s_namespace':
            $method_name = 'updateNamespace';
            $parameter = [
              'labels' => json_decode($request->get('labels', '[]'), TRUE),
              'annotations' => json_decode($request->get('annotations', '[]'), TRUE),
            ];
            break;

          case 'k8s_schedule':
            $method_name = 'updateSchedule';
            /** @var \Drupal\k8s\Entity\K8sScheduleInterface $entity */
            $parameter = [
              'startHour' => (int) ($request->get('startHour', (string) $entity->getStartHour())),
              'startMinute' => (int) ($request->get('startMinute', (string) $entity->getStartMinute())),
              'stopHour' => (int) ($request->get('stopHour', (string) $entity->getStopHour())),
              'stopMinute' => (int) ($request->get('stopMinute', (string) $entity->getStopMinute())),
            ];
            break;

          default:
            $method_name = 'updateEntity';
            /** @var \Drupal\k8s\Entity\K8sEntityInterface $entity */
            $parameter = [
              'detail' => $request->get('detail', $entity->getDetail()),
            ];
            break;
        }
        break;

      case 'create':
        switch ($entity_type_id) {
          case 'k8s_pod':
            $method_name = 'createPod';
            $parameter = [
              'namespace' => $request->get('namespace', 'default'),
              'detail' => $request->get('detail', ''),
              'enable_time_scheduler' => $request->get('enable_time_scheduler', 'true') === 'true',
              'time_scheduler_option' => $request->get('time_scheduler_option', K8sContentFormInterface::CLOUD_ORCHESTRATOR_SCHEDULER),
              'yaml_url' => $request->get('yaml_url', ''),
              'start_hour' => $request->get('start_hour', 6),
              'start_minute' => $request->get('start_minute', 0),
              'stop_hour' => $request->get('stop_hour', 18),
              'stop_minute' => $request->get('stop_minute', 0),
            ];
            /** @var \Drupal\k8s\Entity\K8sPodInterface|\Drupal\k8s\Entity\K8sEntityInterface $entity */
            $entity->setNamespace($parameter['namespace']);
            $entity->setDetail($parameter['detail']);
            $entity->setYamlUrl($parameter['yaml_url']);
            break;

          case 'k8s_namespace':
            $method_name = 'createNamespace';
            $parameter = [
              'name' => $request->get('name', 'default'),
              'labels' => json_decode($request->get('labels', '[]'), TRUE),
            ];
            /** @var \Drupal\k8s\Entity\K8sNamespaceInterface|\Drupal\k8s\Entity\K8sEntityInterface $entity */
            $entity->setName($parameter['name']);
            $entity->setLabels($parameter['labels']);
            $entity->setAnnotations(json_decode($request->get('annotations', '[]'), TRUE));
            break;

          case 'k8s_deployment':
            $method_name = 'createDeployment';
            $parameter = [
              'namespace' => $request->get('namespace', 'default'),
              'detail' => $request->get('detail', ''),
              'enable_time_scheduler' => $request->get('enable_time_scheduler', 'true') === 'true',
              'time_scheduler_option' => $request->get('time_scheduler_option', K8sContentFormInterface::CLOUD_ORCHESTRATOR_SCHEDULER),
              'yaml_url' => $request->get('yaml_url', ''),
              'start_hour' => $request->get('start_hour', 6),
              'start_minute' => $request->get('start_minute', 0),
              'stop_hour' => $request->get('stop_hour', 18),
              'stop_minute' => $request->get('stop_minute', 0),
            ];
            /** @var \Drupal\k8s\Entity\K8sDeploymentInterface|\Drupal\k8s\Entity\K8sEntityInterface $entity */
            $entity->setNamespace($parameter['namespace']);
            $entity->setDetail($parameter['detail']);
            $entity->setYamlUrl($parameter['yaml_url']);
            break;

          default:
            $method_name = 'createK8sEntity';
            $parameter = [
              'namespace' => $request->get('namespace', 'default'),
              'detail' => $request->get('detail', ''),
              'enable_time_scheduler' => $request->get('enable_time_scheduler', 'true') === 'true',
              'start_hour' => $request->get('start_hour', 6),
              'start_minute' => $request->get('start_minute', 0),
              'stop_hour' => $request->get('stop_hour', 18),
              'stop_minute' => $request->get('stop_minute', 0),
            ];
            /** @var \Drupal\k8s\Entity\K8sPodInterface|\Drupal\k8s\Entity\K8sEntityInterface $entity */
            $entity->setNamespace($parameter['namespace']);
            $entity->setDetail($parameter['detail']);
            break;
        }
        break;

      case 'scale':
        if ($entity_type_id === 'k8s_deployment') {
          $method_name = 'scaleDeployment';
          /** @var \Drupal\k8s\Entity\K8sDeploymentInterface|\Drupal\k8s\Entity\K8sEntityInterface $entity */
          $parameter = [
            'podNumber' => $request->get('podNumber', $entity->getReplicas()),
          ];
        }
        break;

      case 'delete':
        $method_name = $entity_type_id === 'k8s_namespace'
          ? 'deleteNamespace'
          : 'deleteK8sEntity';
        break;
    }

    /** @var /Drupal\Core\Entity\ContentEntityBase $entity */
    $violations = $entity->validate();
    if ($violations->count() > 0) {
      $violationList = [];
      foreach ($violations as $violation) {
        $fieldName = $violation->getParameters()['@field_name'];
        $violationList[$fieldName] = $violation->getMessage()->render();
      }
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The value entered in the form is invalid.',
        'violationList' => $violationList,
      ], 400);
    }

    $result = FALSE;
    $service = new K8sOperationsService(
      $this->k8sService,
      $this->cloudService,
      $this->cloudConfigPluginManager,
      $this->entityTypeManager
    );
    if (method_exists($service, $method_name)) {
      $result = $service->$method_name($entity, $parameter);
    }
    $this->messenger()->deleteAll();

    return !empty($result)
      ? new JsonResponse([
        'result' => 'OK',
        'id' => $entity->id(),
      ], 200)
      : new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
      ], 500);
  }

}
