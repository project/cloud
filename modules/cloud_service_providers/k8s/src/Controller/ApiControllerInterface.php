<?php

namespace Drupal\k8s\Controller;

use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\k8s\Entity\K8sNode;
use Drupal\k8s\Entity\K8sPod;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * {@inheritdoc}
 */
interface ApiControllerInterface {

  /**
   * Update all resources.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateAllResources($cloud_context);

  /**
   * Update all Nodes.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateNodeList($cloud_context);

  /**
   * Update all Namespaces.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateNamespaceList($cloud_context);

  /**
   * Update all Pods.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updatePodList($cloud_context);

  /**
   * Update all network policies.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateNetworkPolicyList($cloud_context);

  /**
   * Update all Deployments.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateDeploymentList($cloud_context);

  /**
   * Update all K8s ReplicaSets.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateReplicaSetList($cloud_context);

  /**
   * Update all services.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateServiceList($cloud_context);

  /**
   * Update all CronJobs.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateCronJobList($cloud_context);

  /**
   * Update all Jobs.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateJobList($cloud_context);

  /**
   * Update all resource quotas.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateResourceQuotaList($cloud_context);

  /**
   * Update all K8s LimitRanges.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateLimitRangeList($cloud_context);

  /**
   * Update all Secrets.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateSecretList($cloud_context);

  /**
   * Update all ConfigMaps.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateConfigMapList($cloud_context);

  /**
   * Update all Roles.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateRoleList($cloud_context);

  /**
   * Update all cluster roles.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateClusterRoleList($cloud_context);

  /**
   * Update all persistent volumes.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updatePersistentVolumeList($cloud_context);

  /**
   * Update all storage classes.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateStorageClassList($cloud_context);

  /**
   * Update all K8s StatefulSets.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateStatefulSetsList($cloud_context);

  /**
   * Update all Ingresses.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateIngressList($cloud_context);

  /**
   * Update all K8s DaemonSets.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateDaemonSetList($cloud_context);

  /**
   * Update all Endpoints.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateEndpointList($cloud_context);

  /**
   * Update all Events.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateEventList($cloud_context);

  /**
   * Update all persistent volume claims.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updatePersistentVolumeClaimList($cloud_context);

  /**
   * Update all cluster role bindings.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateClusterRoleBindingList($cloud_context);

  /**
   * Update all role bindings.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateRoleBindingsList($cloud_context);

  /**
   * Update all K8s ServiceAccounts.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateServiceAccountsList($cloud_context);

  /**
   * Get node metrics.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\k8s\Entity\K8sNode $node
   *   The K8s node entity.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getNodeMetrics($cloud_context, K8sNode $node): JsonResponse;

  /**
   * Get pod metrics.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\k8s\Entity\K8sPod $pod
   *   The node name.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getPodMetrics($cloud_context, K8sPod $pod): JsonResponse;

  /**
   * Get pods allocation data for a Pods allocation chart.
   *
   * @param string $cloud_context
   *   A cloud context string.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a Node heatmap incl. Node name, Pods capacity and
   *   Allocations.
   */
  public function getNodeAllocatedResourcesList($cloud_context);

  /**
   * Get pods allocation data for a Pods allocation chart.
   *
   * @param string $cloud_context
   *   A cloud context string.
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   * @param \Drupal\k8s\Entity\K8sNode $node
   *   The K8s node entity.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a Node heatmap incl. Node name, Pods capacity and
   *   Allocations.
   */
  public function getNodeAllocatedResources($cloud_context, ?CloudProjectInterface $cloud_project = NULL, ?K8sNode $node = NULL);

  /**
   * Update all API services.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateApiServiceList($cloud_context);

  /**
   * Update all priority classes.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updatePriorityClassesList($cloud_context);

  /**
   * Update all horizontal pod autoscalers.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateHorizontalPodAutoscalersList($cloud_context): RedirectResponse;

  /**
   * Get the count of K8s entities.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getEntityCount(Request $request, string $cloud_context, string $entity_type_id): JsonResponse;

  /**
   * Get log of K8s pod.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_id
   *   The entity ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getPodLog(string $cloud_context, string $entity_id): JsonResponse;

  /**
   * Operate K8s entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $entity_id
   *   The entity ID.
   * @param string $command
   *   Operation command.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function operateEntity(Request $request, string $cloud_context, string $entity_type_id, string $entity_id, string $command): JsonResponse;

}
