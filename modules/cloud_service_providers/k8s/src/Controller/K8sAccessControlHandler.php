<?php

namespace Drupal\k8s\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Traits\AccessCheckTrait;
use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * Access controller for the K8s entity.
 */
class K8sAccessControlHandler extends EntityAccessControlHandler {

  use AccessCheckTrait;
  use CloudContentEntityTrait;

  /**
   * Checks simple view access to an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to check access.
   * @param string $operation
   *   The operation access should be checked for.
   *   Usually one of "view", "view label", "update" or "delete".
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The user session for which to check access, or NULL to check
   *   access for the current user. Defaults to NULL.
   * @param string $entity_name
   *   The permission name.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result. Returns a boolean if $return_as_object is FALSE (this
   *   is the default) and otherwise an AccessResultInterface object.
   *   When a boolean is returned, the result of AccessInterface::isAllowed() is
   *   returned, i.e. TRUE means access is explicitly allowed, FALSE means
   *   access is either explicitly forbidden or "no opinion".
   */
  private function checkSimpleAccess(EntityInterface $entity, string $operation, AccountInterface $account, string $entity_name): AccessResultInterface {
    if ($operation === 'view') {
      return $this->allowedIfCanAccessCloudConfig(
        $entity,
        $account,
        "view {$entity_name}"
      );
    }
    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * Checks any/own access to an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to check access.
   * @param string $operation
   *   The operation access should be checked for.
   *   Usually one of "view", "view label", "update" or "delete".
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The user session for which to check access, or NULL to check
   *   access for the current user. Defaults to NULL.
   * @param string $entity_name
   *   The permission name.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result. Returns a boolean if $return_as_object is FALSE (this
   *   is the default) and otherwise an AccessResultInterface object.
   *   When a boolean is returned, the result of AccessInterface::isAllowed() is
   *   returned, i.e. TRUE means access is explicitly allowed, FALSE means
   *   access is either explicitly forbidden or "no opinion".
   */
  private function checkAnyOwnAccess(EntityInterface $entity, string $operation, AccountInterface $account, string $entity_name): AccessResultInterface {
    $namespace_permission = '';
    // Not all K8s entities have namespace field.
    if (method_exists($entity, 'getNamespace')) {
      $namespace = $entity->getNamespace();
      $namespace_permission = $account->hasPermission('view any k8s namespace entities')
        ? 'view any k8s namespace entities'
        : "view k8s namespace {$namespace}";
    }

    switch ($operation) {
      case 'view':
      case 'log':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          $this->getPermissionArray($namespace_permission, $entity_name, 'view own'),
          $this->getPermissionArray($namespace_permission, $entity_name, 'view any')
        );

      case 'update':
      case 'edit':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          $this->getPermissionArray($namespace_permission, $entity_name, 'edit own'),
          $this->getPermissionArray($namespace_permission, $entity_name, 'edit any')
        );

      case 'delete':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          $this->getPermissionArray($namespace_permission, $entity_name, 'delete own'),
          $this->getPermissionArray($namespace_permission, $entity_name, 'delete any')
        );
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    $entity_name = $this->getPermissionName($entity->getEntityTypeId());

    // Add any simple view access check entities here.
    $simple_check = [
      'k8s_node',
      'k8s_event',
    ];
    if (\in_array($entity->getEntityTypeId(), $simple_check) === TRUE) {
      return $this->checkSimpleAccess($entity, $operation, $account, $entity_name);
    }

    return $this->checkAnyOwnAccess($entity, $operation, $account, $entity_name);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    $entity_name = $this->getPermissionName($this->entityTypeId);
    return $this->allowedIfCanAccessCloudConfig(
      NULL,
      $account,
      "add {$entity_name}"
    );
  }

  /**
   * Helper function to get permission name.
   *
   * @param string $entity_type_id
   *   Entity type id string.
   *
   * @return string
   *   Permission name.
   */
  private function getPermissionName(string $entity_type_id): string {
    return CloudService::convertUnderscoreToWhitespace($entity_type_id);
  }

  /**
   * Helper that determines if namespace permission is added to the array.
   *
   * @param string $namespace_permission
   *   Namespace permission.  If empty, do not add to permission array.
   * @param string $entity_name
   *   Entity name.
   * @param string $permission_prefix
   *   Permission prefix.
   *
   * @return array
   *   Array of permissions.
   */
  private function getPermissionArray(string $namespace_permission, string $entity_name, string $permission_prefix): array {
    $permissions = [];
    if (!empty($namespace_permission)) {
      $permissions[] = $namespace_permission;
    }
    $permissions[] = "{$permission_prefix} {$entity_name}";
    return $permissions;
  }

}
