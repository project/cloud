<?php

namespace Drupal\k8s\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager;
use Drupal\k8s\Traits\K8sBlockTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Controller Base responsible for K8s Costs Block.
 */
class K8sCostsControllerBase extends ControllerBase {

  use K8sBlockTrait;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The cloud store plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager
   */
  protected $cloudStorePluginManager;

  /**
   * The cost store type service.
   *
   * @var string
   */
  protected $costStoreType = 'k8s_cost_store';

  /**
   * CloudConfigLocationController constructor.
   *
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   Route Provider.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager $plugin_manager
   *   The cloud store plugin manager.
   */
  public function __construct(RouteProviderInterface $route_provider, Request $request, Connection $database, ConfigFactoryInterface $config_factory, CloudStorePluginManager $plugin_manager) {
    $this->routeProvider = $route_provider;
    $this->request = $request;
    $this->database = $database;
    $this->configFactory = $config_factory;
    $this->cloudStorePluginManager = $plugin_manager;

    // Set the refresh flag.
    $this->refresh = $this->configFactory
      ->get('k8s.settings')
      ->get('k8s_update_pricing_data_cache');
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The instance of ContainerInterface.
   *
   * @return K8sNamespaceCostsChartController
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.route_provider'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('plugin.manager.cloud_store_plugin')
    );
  }

  /**
   * Checks user access for k8s namespace costs chart.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route): AccessResultInterface {
    global $base_url;
    // Get the referer url.
    $referer = $this->request->headers->get('referer');
    if (!empty($referer)) {
      // Get the alias or the referer.
      $alias = substr($referer, strlen($base_url));
      $url = Url::fromUri("internal:$alias");
      if ($url->access($account)) {
        return AccessResult::allowed();
      }
    }
    return AccessResult::forbidden();
  }

  /**
   * Get a list of Ec2 cost Types such as On-demand, RI one or three years.
   *
   * @return array
   *   The list response of Ec2 cost types.
   */
  public function getEc2CostTypes(): array {
    $cost_types = [
      'on_demand_hourly'  => $this->t('On-demand hourly'),
      'on_demand_daily'   => $this->t('On-demand daily'),
      'on_demand_monthly' => $this->t('On-demand monthly'),
      'on_demand_yearly'  => $this->t('On-demand yearly'),
      'ri_one_year'       => $this->t('RI 1 year'),
      'ri_three_year'     => $this->t('RI 3 years'),
    ];
    return $cost_types;
  }

  /**
   * Get JSON of Ec2 cost Types list such as On-demand, RI one or three years.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response of Ec2 cost types.
   */
  public function getEc2CostTypesJson(): JsonResponse {
    $cost_types = $this->getEc2CostTypes();
    return new JsonResponse($cost_types);
  }

  /**
   * Get a list of chart period.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   * @param string $name
   *   The name of the entity.
   *
   * @return array
   *   The list response of chart period.
   */
  public function getEc2ChartPeriod($cloud_context = NULL, ?CloudProjectInterface $cloud_project = NULL, $name = NULL): array {
    $cloud_contexts = [];

    if (!empty($cloud_project)) {
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }
    elseif (!empty($cloud_context)) {
      $cloud_contexts[] = $cloud_context;
    }

    $timestamp = time();
    try {
      $storage = $this->entityTypeManager()->getStorage('cloud_store');
      $query = $storage
        ->getQuery()
        ->accessCheck(TRUE);
      $query->condition('type', $this->costStoreType);
      $query->sort('created');
      $query->range(0, 1);
      if (!empty($cloud_contexts)) {
        $query->condition('cloud_context', $cloud_contexts, 'IN');
      }
      if (!empty($name)) {
        $query->condition('name', $name);
      }
      $ids = $query->execute();
      if (!empty($ids)) {
        $entity = $storage->load(reset($ids));
        $timestamp = $entity->get('created')->value;
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }

    $period_option = [
      1 => $this->t('One day'),
      7 => $this->t('One week'),
      14 => $this->t('Two weeks'),
      31 => $this->t('One month'),
      183 => $this->t('Half a year'),
      365 => $this->t('One year'),
    ];
    $keys = array_keys($period_option);
    $keys = array_reverse($keys);
    foreach ($keys ?: [] as $idx => $day) {
      if ($day > 7 && (time() - $day * 24 * 60 * 60 <= $timestamp) && (time() - $keys[$idx + 1] * 24 * 60 * 60 <= $timestamp)) {
        unset($period_option[$day]);
      }
    }
    return $period_option;
  }

  /**
   * Get JSON of chart period list.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   * @param string $name
   *   The name of the entity.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response of chart period.
   */
  public function getEc2ChartPeriodJson($cloud_context = NULL, ?CloudProjectInterface $cloud_project = NULL, $name = NULL): JsonResponse {
    $period_option = $this->getEc2ChartPeriod($cloud_context, $cloud_project, $name);
    return new JsonResponse($period_option);
  }

}
