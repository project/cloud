<?php

namespace Drupal\k8s\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\cloud\Controller\CloudContentListBuilder;

/**
 * Provides a listing of K8s Entity.
 */
class K8sListBuilder extends CloudContentListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity): array {
    $operations = parent::getOperations($entity);
    $account = $this->currentUser;

    if ($entity->getEntityTypeId() === 'k8s_deployment') {
      if ($account->hasPermission('edit any k8s deployment')
      || ($account->hasPermission('edit own k8s deployment')
      && !empty($entity->getOwner()) && $account->id() === $entity->getOwner()->id())) {
        $operations['scale'] = [
          'title' => $this->t('Scale'),
          'url' => $entity->toUrl('scale-form'),
          'weight' => -50,
        ];
      }
    }
    return $operations;
  }

}
