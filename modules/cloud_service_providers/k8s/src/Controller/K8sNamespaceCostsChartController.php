<?php

namespace Drupal\k8s\Controller;

use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\k8s\Entity\K8sNamespaceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller responsible for K8s namespace costs chart.
 */
class K8sNamespaceCostsChartController extends K8sCostsControllerBase {

  /**
   * The cost store type service.
   *
   * @var string
   */
  protected $costStoreType = 'k8s_namespace_resource_store';

  /**
   * Get cost for all namespaces.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a K8s namespace costs.
   */
  public function getAllK8sNamespaceCosts(): JsonResponse {
    $namespaces = $this->getEntitiesByType('k8s_namespace');
    return $this->calculateNamespaceCosts($namespaces);
  }

  /**
   * Get K8s namespace costs.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   * @param \Drupal\k8s\Entity\K8sNamespaceInterface $k8s_namespace
   *   K8s namespace entity.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a K8s namespace costs.
   */
  public function getK8sNamespaceCosts($cloud_context, ?CloudProjectInterface $cloud_project = NULL, ?K8sNamespaceInterface $k8s_namespace = NULL): JsonResponse {
    $entities = $this->loadK8sNamespaceEntities($cloud_context, $cloud_project, $k8s_namespace);
    $namespaces = $entities['namespaces'] ?? [];

    return !empty($cloud_project)
      ? $this->calculateNamespaceCosts($namespaces)
      : $this->calculateNamespaceCosts($namespaces, $cloud_context);
  }

  /**
   * Calculate namespace costs.
   *
   * @param array $namespaces
   *   Array of K8s namespace entities.
   * @param string $cloud_context
   *   Optional cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  private function calculateNamespaceCosts(array $namespaces, $cloud_context = NULL): JsonResponse {
    // Extract the parameters for generating the costs.
    $cost_type = $this->request->get('cost_type');
    if (empty($cost_type)) {
      $cost_type = 'ri_one_year';
    }
    $period = $this->request->get('period');

    if (empty($period)) {
      $period = 14;
    }

    $response = $this->generateNamespaceCosts($namespaces, $cloud_context, $cost_type, $period);
    return new JsonResponse($response);
  }

}
