<?php

namespace Drupal\k8s\Controller;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\k8s\Entity\K8sNodeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller responsible for K8s node allocated resources chart.
 */
class K8sNodeAllocatedResourcesChartController extends K8sCostsControllerBase {

  /**
   * The cost store type service.
   *
   * @var string
   */
  protected $costStoreType = 'k8s_node_resource_store';

  /**
   * Get allocated resources for all node.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a K8s node allocated resources.
   */
  public function getAllK8sNodeAllocatedResources(): JsonResponse {
    $node_ids = $this->entityTypeManager()
      ->getStorage('k8s_node')
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    $nodes = $this->entityTypeManager()
      ->getStorage('k8s_node')->loadMultiple($node_ids);

    return $this->calculateNodeAllocatedResources($nodes);
  }

  /**
   * Get K8s node allocated resources.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   * @param \Drupal\k8s\Entity\K8sNodeInterface $k8s_node
   *   K8s node entity.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a K8s node allocated resources.
   */
  public function getK8sNodeAllocatedResources($cloud_context, ?CloudProjectInterface $cloud_project = NULL, ?K8sNodeInterface $k8s_node = NULL): JsonResponse {
    $nodes = [];
    $cloud_contexts = [];

    if (!empty($cloud_project)) {
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }
    elseif (!empty($cloud_context)) {
      $cloud_contexts[] = $cloud_context;
    }

    // If cloud_context is passed, filter entities with it.
    if (!empty($cloud_contexts)) {
      $nodes = !empty($k8s_node)
        ? $this->loadEntities('k8s_node', [
          'id' => $k8s_node->id(),
        ], FALSE)
        : $this->loadEntities('k8s_node', [
          'cloud_context' => $cloud_contexts,
        ], FALSE);
    }

    return $this->calculateNodeAllocatedResources($nodes);
  }

  /**
   * Calculate node allocated resources.
   *
   * @param array $nodes
   *   Array of K8s node entities.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  private function calculateNodeAllocatedResources(array $nodes): JsonResponse {

    $period = $this->request->get('period');

    if (empty($period)) {
      $period = 14;
    }

    $node_metrics = $this->getAllNodesMetrics($nodes, $period);
    $response = [];

    foreach ($node_metrics ?: [] as $time => $node_data) {
      $data['cpu'] = [
        'timestamp' => $time,
        'cpu_usage' => $node_data['cpu'],
        'cpu_capacity' => $node_data['cpu_capacity'],
      ];
      $data['memory'] = [
        'timestamp' => $time,
        'memory_usage' => $node_data['memory'],
        'memory_capacity' => $node_data['memory_capacity'],
      ];
      $data['pod'] = [
        'timestamp' => $time,
        'pod_usage' => $node_data['pod_allocation'],
        'pod_capacity' => $node_data['pod_capacity'],
      ];
      $response[] = $data;
    }

    return new JsonResponse($response);
  }

  /**
   * Get all nodes' metrics.
   *
   * @param array $nodes
   *   Array of K8s node entities
   *   all metric messages.
   * @param int $period
   *   The period to get data.
   *
   * @return array
   *   The node metrics array.
   */
  private function getAllNodesMetrics(array $nodes, $period = 14): array {
    $plugin = NULL;
    try {
      $plugin = $this->cloudStorePluginManager->loadPluginVariant($this->costStoreType);
    }
    catch (PluginNotFoundException $e) {
      return [];
    }

    $from_time = time() - (int) $period * 24 * 60 * 60;
    $to_time = time();

    $node_metrics = [];
    foreach ($nodes ?: [] as $node) {
      $result = $plugin->extract($this->costStoreType, $node->getCloudContext(), $node->getName(), $from_time, $to_time);
      foreach ($result ?: [] as $key => $record) {
        if ($key !== 'total_cost') {
          $resources = $record['resources'];
          $date = date('Y-m-d H:i', $record['created']);
          $timestamp = strtotime($date);
          if (!isset($node_metrics[$timestamp])) {
            $node_metrics[$timestamp] = [
              'cpu' => (float) 0,
              'memory' => (float) 0,
              'pod_allocation' => 0,
              'cpu_capacity' => (float) 0,
              'memory_capacity' => (float) 0,
              'pod_capacity' => 0,
            ];
          }
          $node_metrics[$timestamp]['cpu'] += (float) $resources['cpu'];
          $node_metrics[$timestamp]['memory'] += (float) $resources['memory'];
          $node_metrics[$timestamp]['pod_allocation'] += $resources['pod_allocation'];
          $node_metrics[$timestamp]['cpu_capacity'] += (float) $resources['cpu_capacity'];
          $node_metrics[$timestamp]['memory_capacity'] += (float) $resources['memory_capacity'];
          $node_metrics[$timestamp]['pod_capacity'] += $resources['pod_capacity'];
        }
      }
    }

    return $node_metrics;
  }

}
