<?php

namespace Drupal\k8s\Controller;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\cloud\Entity\CloudProjectInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller responsible for K8s Project Costs Chart.
 */
class K8sProjectCostsChartController extends K8sCostsControllerBase {

  /**
   * The cost store type service.
   *
   * @var string
   */
  protected $costStoreType = 'k8s_namespace_resource_store';

  /**
   * Get cost for all projects.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a K8s Project Costs.
   */
  public function getAllK8sProjectCosts(): JsonResponse {
    $storage = $this->entityTypeManager()->getStorage('cloud_project');
    $cloud_projects = $storage->loadByProperties(['type' => 'k8s']);
    return $this->calculateProjectCosts($cloud_projects, TRUE);
  }

  /**
   * Get cost for all projects with clusters.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a K8s Project Costs.
   */
  public function getAllK8sProjectClusterCosts(): JsonResponse {
    $storage = $this->entityTypeManager()->getStorage('cloud_project');
    $cloud_projects = $storage->loadByProperties(['type' => 'k8s']);
    return $this->calculateProjectCosts($cloud_projects, TRUE, TRUE);
  }

  /**
   * Get K8s Project Costs.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a K8s Project Costs.
   */
  public function getK8sProjectCosts($cloud_context): JsonResponse {
    $storage = $this->entityTypeManager()->getStorage('cloud_project');
    $cloud_projects = $storage->loadByProperties([
      'cloud_context' => $cloud_context,
      'type' => 'k8s',
    ]);
    return $this->calculateProjectCosts($cloud_projects, TRUE, FALSE);
  }

  /**
   * Get K8s Project Costs with clusters.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response of a K8s Project Costs.
   */
  public function getK8sProjectClusterCosts($cloud_context, ?CloudProjectInterface $cloud_project = NULL): JsonResponse {
    $cloud_projects = [];

    if (!empty($cloud_project)) {
      $cloud_projects[] = $cloud_project;
    }
    elseif (!empty($cloud_context)) {
      $storage = $this->entityTypeManager()->getStorage('cloud_project');
      $cloud_projects = $storage->loadByProperties([
        'cloud_context' => $cloud_context,
        'type' => 'k8s',
      ]);
    }
    return !empty($cloud_project)
      ? $this->calculateProjectCosts($cloud_projects, FALSE, TRUE)
      : $this->calculateProjectCosts($cloud_projects, TRUE, TRUE);
  }

  /**
   * Calculate node allocated resources.
   *
   * @param array $cloud_projects
   *   Array of K8s cloud project entities.
   * @param bool $need_project
   *   Whether a project is required or not.
   * @param bool $need_cluster
   *   Whether clusters are required or not.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  private function calculateProjectCosts(array $cloud_projects, $need_project = TRUE, $need_cluster = FALSE): JsonResponse {
    $response = [];
    $project_costs = $this->getProjectCosts($cloud_projects, $need_project, $need_cluster);

    foreach ($project_costs ?: [] as $project => $costs) {
      $data['project'] = "$project";

      foreach ($costs as $time => $cost) {
        $data['costs'][] = [
          'timestamp' => $time,
          'cost' => $cost,
        ];
      }
      $response[] = $data;
    }
    return new JsonResponse($response);
  }

  /**
   * Get project costs from resource storage.
   *
   * @param array $cloud_projects
   *   Array of K8s cloud project entities.
   * @param bool $need_project
   *   Whether a project is required or not.
   * @param bool $need_cluster
   *   Whether clusters are required or not.
   *
   * @return array
   *   The pod metrics array.
   */
  private function getProjectCosts(array $cloud_projects, $need_project = TRUE, $need_cluster = FALSE): array {
    $plugin = NULL;
    try {
      $plugin = $this->cloudStorePluginManager->loadPluginVariant($this->costStoreType);
    }
    catch (PluginNotFoundException $e) {
      return [];
    }

    $cost_type = $this->request->get('cost_type');
    if (empty($cost_type)) {
      $cost_type = 'ri_one_year';
    }

    $period = $this->request->get('period');
    if (empty($period)) {
      $period = 14;
    }

    $from_time = time() - (int) $period * 24 * 60 * 60;
    $to_time = time();
    $project_costs = [];
    foreach ($cloud_projects ?: [] as $cloud_project) {
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $k8s_namespaces = $this->entityTypeManager()
          ->getStorage('k8s_namespace')->loadByProperties([
            'cloud_context' => $k8s_cluster['value'],
          ]
        );

        $project = '';
        if ($need_project) {
          $project = $cloud_project->getName();
        }
        if ($need_cluster) {
          $project .= $need_project ? ':' . $k8s_cluster['value'] : $k8s_cluster['value'];
        }

        foreach ($k8s_namespaces ?: [] as $k8s_namespace) {
          $result = $plugin->extract($this->costStoreType, $k8s_namespace->getCloudContext(), $k8s_namespace->getName(), $from_time, $to_time, $cost_type);

          if (count($result) > 1) {
            if (!isset($project_costs[$project])) {
              $project_costs[$project] = [];
            }
            foreach ($result ?: [] as $key => $record) {
              if ($key !== 'total_cost') {
                $date = date('Y-m-d H:i', $record['created']);
                $timestamp = strtotime($date);
                if (!isset($project_costs[$project][$timestamp])) {
                  $project_costs[$project][$timestamp] = $record['cost'];
                }
                else {
                  $project_costs[$project][$timestamp] += $record['cost'];
                }
              }
            }
          }
        }
      }
    }

    return $project_costs;
  }

  /**
   * {@inheritdoc}
   */
  public function getEc2ChartPeriod($cloud_context = NULL, ?CloudProjectInterface $cloud_project = NULL, $name = NULL): array {
    $period_option = [
      1 => $this->t('One day'),
      7 => $this->t('One week'),
      14 => $this->t('Two weeks'),
      31 => $this->t('One month'),
      183 => $this->t('Half a year'),
      365 => $this->t('One year'),
    ];

    $cloud_contexts = [];
    $k8s_namespaces = [];
    $timestamp = 0;

    if (!empty($cloud_project)) {
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }
    elseif (!empty($cloud_context)) {
      $cloud_contexts[] = $cloud_context;
    }
    foreach ($cloud_contexts ?: [] as $cluster) {
      $namespaces = $this->entityTypeManager()
        ->getStorage('k8s_namespace')->loadByProperties([
          'cloud_context' => $cluster,
        ]
      );
      if (!empty($namespaces)) {
        foreach ($namespaces ?: [] as $namespace) {
          $k8s_namespaces[] = $namespace->getName();
        }
      }
    }

    try {
      $storage = $this->entityTypeManager()->getStorage('cloud_store');
      $query = $storage
        ->getQuery()
        ->accessCheck(TRUE);
      $query->condition('type', 'k8s_namespace_resource_store');
      $query->sort('created');
      $query->range(0, 1);
      if (!empty($cloud_contexts)) {
        $query->condition('cloud_context', $cloud_contexts, 'IN');
      }
      if (!empty($k8s_namespaces)) {
        $query->condition('name', $k8s_namespaces, 'IN');
      }
      $ids = $query->execute();
      if (!empty($ids)) {
        $entity = $storage->load(reset($ids));
        $timestamp = $entity->get('created')->value;
      }
      else {
        $timestamp = time();
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }

    $keys = array_keys($period_option);
    $keys = array_reverse($keys);
    foreach ($keys ?: [] as $idx => $day) {
      if ($day > 7 && (time() - $day * 24 * 60 * 60 <= $timestamp) && (time() - $keys[$idx + 1] * 24 * 60 * 60 <= $timestamp)) {
        unset($period_option[$day]);
      }
    }
    return $period_option;
  }

}
