<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an API service entity.
 *
 * @ingroup k8s
 */
interface K8sApiServiceInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getGroupPriorityMinimum();

  /**
   * {@inheritdoc}
   */
  public function setGroupPriorityMinimum($group_priority_minimum): K8sApiServiceInterface;

  /**
   * {@inheritdoc}
   */
  public function getService(): string;

  /**
   * {@inheritdoc}
   */
  public function setService($service): K8sApiServiceInterface;

  /**
   * {@inheritdoc}
   */
  public function getVersionPriority(): string;

  /**
   * {@inheritdoc}
   */
  public function setVersionPriority($version_priority): K8sApiServiceInterface;

  /**
   * {@inheritdoc}
   */
  public function getConditions(): array;

  /**
   * {@inheritdoc}
   */
  public function setConditions($conditions): K8sApiServiceInterface;

  /**
   * {@inheritdoc}
   */
  public function getGroup(): string;

  /**
   * {@inheritdoc}
   */
  public function setGroup($group): K8sApiServiceInterface;

  /**
   * {@inheritdoc}
   */
  public function getInsecureSkipTlsVerify(): string;

  /**
   * {@inheritdoc}
   */
  public function setInsecureSkipTlsVerify($insecure_skip_tls_verify): K8sApiServiceInterface;

  /**
   * {@inheritdoc}
   */
  public function getVersion(): string;

  /**
   * {@inheritdoc}
   */
  public function setVersion($version): K8sApiServiceInterface;

}
