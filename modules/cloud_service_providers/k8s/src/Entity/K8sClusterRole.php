<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the cluster role entity.
 *
 * @ingroup k8s
 *
 * @ContentEntityType(
 *   id = "k8s_cluster_role",
 *   id_plural = "k8s_cluster_roles",
 *   label = @Translation("Cluster role"),
 *   label_collection = @Translation("Cluster roles"),
 *   label_singular = @Translation("Cluster role"),
 *   label_plural = @Translation("Cluster roles"),
 *   namespaceable = FALSE,
 *   handlers = {
 *     "view_builder" = "Drupal\k8s\Entity\K8sClusterRoleViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\k8s\Entity\K8sClusterRoleViewsData",
 *     "access"       = "Drupal\k8s\Controller\K8sAccessControlHandler",
 *     "form" = {
 *       "add"        = "Drupal\k8s\Form\K8sCreateForm",
 *       "edit"       = "Drupal\k8s\Form\K8sEditForm",
 *       "delete"     = "Drupal\k8s\Form\K8sDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\k8s\Form\K8sDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "k8s_cluster_role",
 *   admin_permission = "administer k8s cluster role",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *     "class" = "Drupal\k8s\Entity\K8sClusterRole",
 *   },
 *   links = {
 *     "canonical"            = "/clouds/k8s/{cloud_context}/cluster_role/{k8s_cluster_role}",
 *     "collection"           = "/clouds/k8s/{cloud_context}/cluster_role",
 *     "add-form"             = "/clouds/k8s/{cloud_context}/cluster_role/add",
 *     "edit-form"            = "/clouds/k8s/{cloud_context}/cluster_role/{k8s_cluster_role}/edit",
 *     "delete-form"          = "/clouds/k8s/{cloud_context}/cluster_role/{k8s_cluster_role}/delete",
 *     "delete-multiple-form" = "/clouds/k8s/{cloud_context}/cluster_role/delete_multiple",
 *   },
 *   field_ui_base_route = "k8s_cluster_role.settings"
 * )
 */
class K8sClusterRole extends K8sEntityBase implements K8sClusterRoleInterface {

  /**
   * {@inheritdoc}
   */
  public function getRules(): array {
    return $this->get('rules')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRules($rules): K8sClusterRoleInterface {
    return $this->set('rules', $rules);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = K8sEntityBase::baseFieldDefinitions($entity_type);

    $fields['rules'] = BaseFieldDefinition::create('rule')
      ->setLabel(t('Rules'))
      ->setDescription(t('Rules.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('max_length', 4096)
      ->setSetting('long', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'rule_formatter',
        'weight' => -5,
      ]);

    return $fields;
  }

}
