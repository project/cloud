<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a cluster role binding entity.
 *
 * @ingroup k8s
 */
interface K8sClusterRoleBindingInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getSubjects(): array;

  /**
   * {@inheritdoc}
   */
  public function setSubjects($subjects): K8sClusterRoleBindingInterface;

  /**
   * {@inheritdoc}
   */
  public function getRoleRef(): string;

  /**
   * {@inheritdoc}
   */
  public function setRoleRef($role_ref): K8sClusterRoleBindingInterface;

}
