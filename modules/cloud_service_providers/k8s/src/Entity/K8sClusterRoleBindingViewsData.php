<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the cluster role binding entity type.
 */
class K8sClusterRoleBindingViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_cluster_role_binding']['cluster_role_binding_bulk_form'] = [
      'title' => $this->t('Cluster role binding operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple cluster roles Bindings.'),
      'field' => [
        'id' => 'cluster_role_binding_bulk_form',
      ],
    ];

    return $data;
  }

}
