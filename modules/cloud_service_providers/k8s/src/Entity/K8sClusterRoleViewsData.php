<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the cluster role entity type.
 */
class K8sClusterRoleViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_cluster_role']['cluster_role_bulk_form'] = [
      'title' => $this->t('Cluster role operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple cluster roles.'),
      'field' => [
        'id' => 'cluster_role_bulk_form',
      ],
    ];

    return $data;
  }

}
