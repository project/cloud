<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a ConfigMap entity.
 *
 * @ingroup k8s
 */
interface K8sConfigMapInterface extends ContentEntityInterface, EntityOwnerInterface, K8sExportableEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sConfigMapInterface;

  /**
   * {@inheritdoc}
   */
  public function getData(): array;

  /**
   * {@inheritdoc}
   */
  public function setData($data): K8sConfigMapInterface;

}
