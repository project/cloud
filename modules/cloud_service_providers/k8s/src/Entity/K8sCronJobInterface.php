<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a CronJob entity.
 *
 * @ingroup k8s
 */
interface K8sCronJobInterface extends ContentEntityInterface, EntityOwnerInterface, K8sExportableEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sCronJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getSchedule(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setSchedule($schedule): K8sCronJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getActive(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setActive($active): K8sCronJobInterface;

  /**
   * {@inheritdoc}
   */
  public function isSuspend(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setSuspend($suspend): K8sCronJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getLastScheduleTime(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setLastScheduleTime($last_schedule_time): K8sCronJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getConcurrencyPolicy(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setConcurrencyPolicy($concurrency_policy): K8sCronJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getStartingDeadlineSeconds(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setStartingDeadlineSeconds($starting_deadline_seconds): K8sCronJobInterface;

}
