<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the CronJob entity type.
 */
class K8sCronJobViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_cron_job']['cron_job_bulk_form'] = [
      'title' => $this->t('CronJob operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple CronJobs.'),
      'field' => [
        'id' => 'cron_job_bulk_form',
      ],
    ];

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
