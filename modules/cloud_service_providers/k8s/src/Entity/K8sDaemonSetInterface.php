<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a DaemonSet entity.
 *
 * @ingroup k8s
 */
interface K8sDaemonSetInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sDaemonSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getCpuRequest(): float;

  /**
   * {@inheritdoc}
   */
  public function setCpuRequest($cpu_request): K8sDaemonSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getCpuLimit(): float;

  /**
   * {@inheritdoc}
   */
  public function setCpuLimit($cpu_limit): K8sDaemonSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getMemoryRequest(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setMemoryRequest($memory_request): K8sDaemonSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getMemoryLimit(): float;

  /**
   * {@inheritdoc}
   */
  public function setMemoryLimit($memory_limit): K8sDaemonSetInterface;

}
