<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Deployment entity.
 *
 * @ingroup k8s
 */
interface K8sDeploymentInterface extends ContentEntityInterface, EntityOwnerInterface, K8sExportableEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sDeploymentInterface;

  /**
   * {@inheritdoc}
   */
  public function getStrategy(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setStrategy($strategy): K8sDeploymentInterface;

  /**
   * {@inheritdoc}
   */
  public function getMinReadySeconds(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setMinReadySeconds($min_ready_seconds): K8sDeploymentInterface;

  /**
   * {@inheritdoc}
   */
  public function getRevisionHistoryLimit(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setRevisionHistoryLimit($revision_history_limit): K8sDeploymentInterface;

  /**
   * {@inheritdoc}
   */
  public function getAvailableReplicas(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setAvailableReplicas($available_replicas): K8sDeploymentInterface;

  /**
   * {@inheritdoc}
   */
  public function getCollisionCount(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setCollisionCount($collision_count): K8sDeploymentInterface;

  /**
   * {@inheritdoc}
   */
  public function getObservedGeneration(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setObservedGeneration($observed_generation): K8sDeploymentInterface;

  /**
   * {@inheritdoc}
   */
  public function getReadyReplicas(): int;

  /**
   * {@inheritdoc}
   */
  public function setReadyReplicas($ready_replicas): K8sDeploymentInterface;

  /**
   * {@inheritdoc}
   */
  public function getReplicas(): int;

  /**
   * {@inheritdoc}
   */
  public function setReplicas($replicas): K8sDeploymentInterface;

  /**
   * {@inheritdoc}
   */
  public function getUnavailableReplicas(): int;

  /**
   * {@inheritdoc}
   */
  public function setUnavailableReplicas($unavailable_replicas): K8sDeploymentInterface;

  /**
   * {@inheritdoc}
   */
  public function getUpdatedReplicas(): int;

  /**
   * {@inheritdoc}
   */
  public function setUpdatedReplicas($updated_replicas): K8sDeploymentInterface;

}
