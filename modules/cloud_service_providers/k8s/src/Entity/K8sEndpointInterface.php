<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an Endpoint entity.
 *
 * @ingroup k8s
 */
interface K8sEndpointInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sEndpointInterface;

  /**
   * {@inheritdoc}
   */
  public function getNodeName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNodeName($node_name): K8sEndpointInterface;

  /**
   * {@inheritdoc}
   */
  public function getAddresses(): array;

  /**
   * {@inheritdoc}
   */
  public function setAddresses(array $addresses): K8sEndpointInterface;

}
