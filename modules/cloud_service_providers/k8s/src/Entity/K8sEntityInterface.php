<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a K8s entity.
 *
 * @ingroup k8s
 */
interface K8sEntityInterface extends ContentEntityInterface, EntityOwnerInterface {

  public const ANNOTATION_CREATED_BY_UID = 'k8s_created_uid';

  public const ANNOTATION_LAUNCHED_APPLICATION_ID = 'k8s_launched_application_id';

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName();

  /**
   * {@inheritdoc}
   */
  public function setName($name): K8sEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getLabels(): array;

  /**
   * {@inheritdoc}
   */
  public function setLabels(array $labels): K8sEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getAnnotations(): array;

  /**
   * {@inheritdoc}
   */
  public function setAnnotations(array $annotations): K8sEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getDetail(): string;

  /**
   * {@inheritdoc}
   */
  public function setDetail($detail): K8sEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getYamlUrl(): string;

  /**
   * {@inheritdoc}
   */
  public function setYamlUrl($detail): K8sEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): K8sEntityInterface;

}
