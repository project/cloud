<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an Event entity.
 *
 * @ingroup k8s
 */
interface K8sEventInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getType();

  /**
   * {@inheritdoc}
   */
  public function setType($val): K8sEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getReason(): string;

  /**
   * {@inheritdoc}
   */
  public function setReason($val): K8sEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getObjectKind(): string;

  /**
   * {@inheritdoc}
   */
  public function setObjectKind($val): K8sEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getObjectName(): string;

  /**
   * {@inheritdoc}
   */
  public function setObjectName($val): K8sEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getMessage(): string;

  /**
   * {@inheritdoc}
   */
  public function setMessage($val): K8sEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getTimeStamp(): string;

  /**
   * {@inheritdoc}
   */
  public function setTimeStamp($val): K8sEventInterface;

}
