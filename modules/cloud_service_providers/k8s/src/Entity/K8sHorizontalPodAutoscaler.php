<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the horizontal pod autoscaler entity.
 *
 * @ingroup k8s
 *
 * @ContentEntityType(
 *   id = "k8s_horizontal_pod_autoscaler",
 *   id_plural = "k8s_horizontal_pod_autoscalers",
 *   label = @Translation("Horizontal pod autoscaler"),
 *   label_collection = @Translation("Horizontal pod autoscalers"),
 *   label_singular = @Translation("Horizontal pod autoscaler"),
 *   label_plural = @Translation("Horizontal pod autoscalers"),
 *   namespaceable = FALSE,
 *   handlers = {
 *     "view_builder" = "Drupal\k8s\Entity\K8sHorizontalPodAutoscalerViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\k8s\Entity\K8sHorizontalPodAutoscalerViewsData",
 *     "access"       = "Drupal\k8s\Controller\K8sAccessControlHandler",
 *     "form" = {
 *       "add"        = "Drupal\k8s\Form\K8sCreateForm",
 *       "edit"       = "Drupal\k8s\Form\K8sEditForm",
 *       "delete"     = "Drupal\k8s\Form\K8sDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\k8s\Form\K8sDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "k8s_horizontal_pod_autoscaler",
 *   admin_permission = "administer k8s horizontal pod autoscaler",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *     "class" = "Drupal\k8s\Entity\K8sHorizontalPodAutoscaler",
 *   },
 *   links = {
 *     "canonical"            = "/clouds/k8s/{cloud_context}/horizontal_pod_autoscaler/{k8s_horizontal_pod_autoscaler}",
 *     "collection"           = "/clouds/k8s/{cloud_context}/horizontal_pod_autoscaler",
 *     "add-form"             = "/clouds/k8s/{cloud_context}/horizontal_pod_autoscaler/add",
 *     "edit-form"            = "/clouds/k8s/{cloud_context}/horizontal_pod_autoscaler/{k8s_horizontal_pod_autoscaler}/edit",
 *     "delete-form"          = "/clouds/k8s/{cloud_context}/horizontal_pod_autoscaler/{k8s_horizontal_pod_autoscaler}/delete",
 *     "delete-multiple-form" = "/clouds/k8s/{cloud_context}/horizontal_pod_autoscaler/delete_multiple",
 *   },
 *   field_ui_base_route = "k8s_horizontal_pod_autoscaler.settings"
 * )
 */
class K8sHorizontalPodAutoscaler extends K8sEntityBase implements K8sHorizontalPodAutoscalerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string {
    return $this->get('namespace')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sHorizontalPodAutoscalerInterface {
    return $this->set('namespace', $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function getScaleTarget(): string {
    return $this->get('scale_target')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setScaleTarget($scale_target): K8sHorizontalPodAutoscalerInterface {
    return $this->set('scale_target', $scale_target);
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetCpuUtilization(): int {
    return $this->get('target_cpu_utilization')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetCpuUtilization($target_cpu_utilization): K8sHorizontalPodAutoscalerInterface {
    return $this->set('target_cpu_utilization', $target_cpu_utilization);
  }

  /**
   * {@inheritdoc}
   */
  public function getMinimumReplicas(): int {
    return $this->get('minimum_replicas')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMinimumReplicas($minimum_replicas): K8sHorizontalPodAutoscalerInterface {
    return $this->set('minimum_replicas', $minimum_replicas);
  }

  /**
   * {@inheritdoc}
   */
  public function getMaximumReplicas(): int {
    return $this->get('maximum_replicas')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMaximumReplicas($maximum_replicas): K8sHorizontalPodAutoscalerInterface {
    return $this->set('maximum_replicas', $maximum_replicas);
  }

  /**
   * {@inheritdoc}
   */
  public function getDeploymentPods(): int {
    return $this->get('deployment_pods')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeploymentPods($deployment_pods): K8sHorizontalPodAutoscalerInterface {
    return $this->set('deployment_pods', $deployment_pods);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceCpuOnPods(): int {
    return $this->get('resource_cpu_on_pods')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceCpuOnPods($resource_cpu_on_pods): K8sHorizontalPodAutoscalerInterface {
    return $this->set('resource_cpu_on_pods', $resource_cpu_on_pods);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = K8sEntityBase::baseFieldDefinitions($entity_type);

    $fields['namespace'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Namespace'))
      ->setDescription(t('The namespace of horizontal pod autoscaler.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['scale_target'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Scale Target'))
      ->setDescription(t('The target of scaling.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['target_cpu_utilization'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Target CPU Utilization (%)'))
      ->setDescription(t('The CPU utilization of the target.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'integer',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['minimum_replicas'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Minimum Replicas'))
      ->setDescription(t('The minimum number of the pods to scale.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'integer',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['maximum_replicas'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Maximum Replicas'))
      ->setDescription(t('The maximum number of the pods to scale.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'integer',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['deployment_pods'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Deployment Pods'))
      ->setDescription(t('The number of current pods'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'integer',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['resource_cpu_on_pods'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Resource CPU on Pods (%)'))
      ->setDescription(t('The CPU utilization on pods as a percentage of request.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'integer',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    return $fields;
  }

}
