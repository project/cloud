<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a horizontal pod autoscaler entity.
 *
 * @ingroup k8s
 */
interface K8sHorizontalPodAutoscalerInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sHorizontalPodAutoscalerInterface;

  /**
   * {@inheritdoc}
   */
  public function getScaleTarget(): string;

  /**
   * {@inheritdoc}
   */
  public function setScaleTarget($scale_target): K8sHorizontalPodAutoscalerInterface;

  /**
   * {@inheritdoc}
   */
  public function getTargetCpuUtilization(): int;

  /**
   * {@inheritdoc}
   */
  public function setTargetCpuUtilization($target_cpu_utilization_target): K8sHorizontalPodAutoscalerInterface;

  /**
   * {@inheritdoc}
   */
  public function getMinimumReplicas(): int;

  /**
   * {@inheritdoc}
   */
  public function setMinimumReplicas($minimum_replicas): K8sHorizontalPodAutoscalerInterface;

  /**
   * {@inheritdoc}
   */
  public function getMaximumReplicas(): int;

  /**
   * {@inheritdoc}
   */
  public function setMaximumReplicas($maximum_replicas): K8sHorizontalPodAutoscalerInterface;

  /**
   * {@inheritdoc}
   */
  public function getDeploymentPods(): int;

  /**
   * {@inheritdoc}
   */
  public function setDeploymentPods($deployment_pods): K8sHorizontalPodAutoscalerInterface;

  /**
   * {@inheritdoc}
   */
  public function getResourceCpuOnPods(): int;

  /**
   * {@inheritdoc}
   */
  public function setResourceCpuOnPods($resource_cpu_on_pods): K8sHorizontalPodAutoscalerInterface;

}
