<?php

namespace Drupal\k8s\Entity;

use Drupal\cloud\Entity\CloudViewBuilder;

/**
 * Provides the horizontal pod autoscaler view builders.
 */
class K8sHorizontalPodAutoscalerViewBuilder extends CloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'horizontal_pod_autoscaler',
        'title' => $this->t('Horizontal pod autoscaler'),
        'open' => TRUE,
        'fields' => [
          'name',
          'namespace',
          'scale_target',
          'target_cpu_utilization',
        ],
      ],
      [
        'name' => 'horizontal_pod_autoscaler_status',
        'title' => $this->t('Status'),
        'open' => TRUE,
        'fields' => [
          'minimum_replicas',
          'maximum_replicas',
          'deployment_pods',
          'resource_cpu_on_pods',
        ],
      ],
      [
        'name' => 'horizontal_pod_autoscaler_detail',
        'title' => $this->t('Detail'),
        'open' => FALSE,
        'fields' => [
          'detail',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
