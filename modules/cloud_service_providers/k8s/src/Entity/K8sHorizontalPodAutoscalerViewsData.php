<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the horizontal pod autoscaler entity type.
 */
class K8sHorizontalPodAutoscalerViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_horizontal_pod_autoscaler']['horizontal_pod_autoscaler_bulk_form'] = [
      'title' => $this->t('Horizontal pod autoscaler operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple horizontal pod autoscaler.'),
      'field' => [
        'id' => 'horizontal_pod_autoscaler_bulk_form',
      ],
    ];

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
