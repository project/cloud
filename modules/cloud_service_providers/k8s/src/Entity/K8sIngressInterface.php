<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an Ingress entity.
 *
 * @ingroup k8s
 */
interface K8sIngressInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sIngressInterface;

  /**
   * {@inheritdoc}
   */
  public function getBackend(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setBackend($backend): K8sIngressInterface;

  /**
   * {@inheritdoc}
   */
  public function getRules(): array;

  /**
   * {@inheritdoc}
   */
  public function setRules($rules): K8sIngressInterface;

  /**
   * {@inheritdoc}
   */
  public function getTls(): array;

  /**
   * {@inheritdoc}
   */
  public function setTls($tls): K8sIngressInterface;

  /**
   * {@inheritdoc}
   */
  public function getLoadBalancer(): array;

  /**
   * {@inheritdoc}
   */
  public function setLoadBalancer($load_balancer): K8sIngressInterface;

}
