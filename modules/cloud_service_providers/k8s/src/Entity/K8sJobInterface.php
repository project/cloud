<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Job entity.
 *
 * @ingroup k8s
 */
interface K8sJobInterface extends ContentEntityInterface, EntityOwnerInterface, K8sExportableEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getImage(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setImage($image): K8sJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getCompletions(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setCompletions($completions): K8sJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getParallelism(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setParallelism($parallelism): K8sJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getActive(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setActive($active): K8sJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getFailed(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setFailed($failed): K8sJobInterface;

  /**
   * {@inheritdoc}
   */
  public function getSucceeded(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setSucceeded($succeeded): K8sJobInterface;

}
