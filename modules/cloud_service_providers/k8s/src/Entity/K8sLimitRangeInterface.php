<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a LimitRange entity.
 *
 * @ingroup k8s
 */
interface K8sLimitRangeInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sLimitRangeInterface;

  /**
   * {@inheritdoc}
   */
  public function getLimits(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setLimits($limits): K8sLimitRangeInterface;

}
