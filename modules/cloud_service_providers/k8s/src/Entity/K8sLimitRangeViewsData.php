<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the LimitRange entity type.
 */
class K8sLimitRangeViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_limit_range']['limit_range_bulk_form'] = [
      'title' => $this->t('LimitRange operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple LimitRanges.'),
      'field' => [
        'id' => 'limit_range_bulk_form',
      ],
    ];

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
