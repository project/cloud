<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the Namespace entity type.
 */
class K8sNamespaceViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_namespace']['namespace_bulk_form'] = [
      'title' => $this->t('Namespace operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple Namespaces.'),
      'field' => [
        'id' => 'namespace_bulk_form',
      ],
    ];
    $data['k8s_namespace']['table']['base']['access query tag'] = 'k8s_entity_views_access';
    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
