<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a network policy entity.
 *
 * @ingroup k8s
 */
interface K8sNetworkPolicyInterface extends ContentEntityInterface, EntityOwnerInterface, K8sExportableEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sNetworkPolicyInterface;

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): K8sNetworkPolicyInterface;

  /**
   * {@inheritdoc}
   */
  public function getEgress(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setEgress($egress): K8sNetworkPolicyInterface;

  /**
   * {@inheritdoc}
   */
  public function getIngress(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setIngress($ingress): K8sNetworkPolicyInterface;

  /**
   * {@inheritdoc}
   */
  public function getPolicyTypes(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setPolicyTypes($policy_types): K8sNetworkPolicyInterface;

  /**
   * {@inheritdoc}
   */
  public function getPodSelector(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setPodSelector($pod_selector): K8sNetworkPolicyInterface;

}
