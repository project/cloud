<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the network policy entity type.
 */
class K8sNetworkPolicyViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_network_policy']['network_policy_bulk_form'] = [
      'title' => $this->t('Network policy operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple network policies.'),
      'field' => [
        'id' => 'network_policy_bulk_form',
      ],
    ];

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
