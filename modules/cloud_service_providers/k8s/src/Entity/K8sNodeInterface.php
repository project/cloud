<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Node entity.
 *
 * @ingroup k8s
 */
interface K8sNodeInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getStatus();

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getAddresses(): ?array;

  /**
   * {@inheritdoc}
   */
  public function setAddresses(array $addresses): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getPodCidr(): string;

  /**
   * {@inheritdoc}
   */
  public function setPodCidr($pod_cidr): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getProviderId(): string;

  /**
   * {@inheritdoc}
   */
  public function setProviderId($provider_id): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function isUnschedulable(): bool;

  /**
   * {@inheritdoc}
   */
  public function setUnschedulable($unschedulable): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getMachineId(): string;

  /**
   * {@inheritdoc}
   */
  public function setMachineId($machine_id): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getSystemUuid(): string;

  /**
   * {@inheritdoc}
   */
  public function setSystemUuid($system_uuid): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getBootId(): string;

  /**
   * {@inheritdoc}
   */
  public function setBootId($boot_id): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getKernelVersion(): string;

  /**
   * {@inheritdoc}
   */
  public function setKernelVersion($kernel_version): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getOsImage(): string;

  /**
   * {@inheritdoc}
   */
  public function setOsImage($os_image): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getContainerRuntimeVersion(): string;

  /**
   * {@inheritdoc}
   */
  public function setContainerRuntimeVersion($container_runtime_version): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getKubeletVersion(): string;

  /**
   * {@inheritdoc}
   */
  public function setKubeletVersion($kubelet_version): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getKubeProxyVersion(): string;

  /**
   * {@inheritdoc}
   */
  public function setKubeProxyVersion($kube_proxy_version): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getOperatingSystem(): string;

  /**
   * {@inheritdoc}
   */
  public function setOperatingSystem($operating_system): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getArchitecture(): string;

  /**
   * {@inheritdoc}
   */
  public function setArchitecture($architecture): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getCpuCapacity(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setCpuCapacity($cpu_capacity): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getCpuRequest(): float;

  /**
   * {@inheritdoc}
   */
  public function setCpuRequest($cpu_request): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getCpuLimit(): float;

  /**
   * {@inheritdoc}
   */
  public function setCpuLimit($cpu_limit): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getCpuUsage(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setCpuUsage($cpu_usage): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getMemoryCapacity(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setMemoryCapacity($memory_capacity): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getMemoryRequest(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setMemoryRequest($memory_request): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getMemoryLimit(): float;

  /**
   * {@inheritdoc}
   */
  public function setMemoryLimit($memory_limit): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getMemoryUsage(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setMemoryUsage($memory_usage): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getPodsCapacity(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setPodsCapacity($pods_capacity): K8sNodeInterface;

  /**
   * {@inheritdoc}
   */
  public function getPodsAllocation(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setPodsAllocation($pods_allocation): K8sNodeInterface;

}
