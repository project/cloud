<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the persistent volume entity.
 *
 * @ingroup k8s
 *
 * @ContentEntityType(
 *   id = "k8s_persistent_volume",
 *   id_plural = "k8s_persistent_volumes",
 *   label = @Translation("Persistent volume"),
 *   label_collection = @Translation("Persistent volumes"),
 *   namespaceable = FALSE,
 *   label_singular = @Translation("Persistent volume"),
 *   label_plural = @Translation("Persistent volumes"),
 *   handlers = {
 *     "view_builder" = "Drupal\k8s\Entity\K8sPersistentVolumeViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\k8s\Entity\K8sPersistentVolumeViewsData",
 *     "access"       = "Drupal\k8s\Controller\K8sAccessControlHandler",
 *     "form" = {
 *       "add"        = "Drupal\k8s\Form\K8sCreateForm",
 *       "edit"       = "Drupal\k8s\Form\K8sEditForm",
 *       "delete"     = "Drupal\k8s\Form\K8sDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\k8s\Form\K8sDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "k8s_persistent_volume",
 *   admin_permission = "administer k8s persistent volume",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *     "class" = "Drupal\k8s\Entity\K8sPersistentVolume",
 *   },
 *   links = {
 *     "canonical"            = "/clouds/k8s/{cloud_context}/persistent_volume/{k8s_persistent_volume}",
 *     "collection"           = "/clouds/k8s/{cloud_context}/persistent_volume",
 *     "add-form"             = "/clouds/k8s/{cloud_context}/persistent_volume/add",
 *     "edit-form"            = "/clouds/k8s/{cloud_context}/persistent_volume/{k8s_persistent_volume}/edit",
 *     "delete-form"          = "/clouds/k8s/{cloud_context}/persistent_volume/{k8s_persistent_volume}/delete",
 *     "delete-multiple-form" = "/clouds/k8s/{cloud_context}/persistent_volume/delete_multiple",
 *   },
 *   field_ui_base_route = "k8s_persistent_volume.settings"
 * )
 */
class K8sPersistentVolume extends K8sEntityBase implements K8sPersistentVolumeInterface {

  /**
   * {@inheritdoc}
   */
  public function getCapacity(): string {
    return $this->get('capacity')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCapacity($capacity): K8sPersistentVolumeInterface {
    return $this->set('capacity', $capacity);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessModes(): string {
    return $this->get('access_modes')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAccessModes($access_modes): K8sPersistentVolumeInterface {
    return $this->set('access_modes', $access_modes);
  }

  /**
   * {@inheritdoc}
   */
  public function getReclaimPolicy(): string {
    return $this->get('reclaim_policy')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setReclaimPolicy($reclaim_policy): K8sPersistentVolumeInterface {
    return $this->set('reclaim_policy', $reclaim_policy);
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageClassName(): string {
    return $this->get('storage_class_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStorageClassName($storage_class_name): K8sPersistentVolumeInterface {
    return $this->set('storage_class_name', $storage_class_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getPhase(): string {
    return $this->get('phase')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPhase($phase): K8sPersistentVolumeInterface {
    return $this->set('phase', $phase);
  }

  /**
   * {@inheritdoc}
   */
  public function getClaimRef(): string {
    return $this->get('claim_ref')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setClaimRef($claim_ref): K8sPersistentVolumeInterface {
    return $this->set('claim_ref', $claim_ref);
  }

  /**
   * {@inheritdoc}
   */
  public function getReason(): string {
    return $this->get('reason')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setReason($reason): K8sPersistentVolumeInterface {
    return $this->set('reason', $reason);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = K8sEntityBase::baseFieldDefinitions($entity_type);

    $fields['capacity'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Capacity'))
      ->setDescription(t('Capacity.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['access_modes'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Access Modes'))
      ->setDescription(t('Access Modes.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['reclaim_policy'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Reclaim Policy'))
      ->setDescription(t('Reclaim Policy.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['storage_class_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Storage class name'))
      ->setDescription(t('Storage class name.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['phase'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Phase'))
      ->setDescription(t('Phase indicates if a volume is available, bound to a claim, or released by a claim.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['claim_ref'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Claim'))
      ->setDescription(t('ClaimRef is part of a bi-directional binding between PersistentVolume and PersistentVolumeClaim.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['reason'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Reason'))
      ->setDescription(t('Reason is a brief CamelCase string that describes any failure and is meant for machine parsing and tidy display in the CLI.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    return $fields;
  }

}
