<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a persistent volume claim entity.
 *
 * @ingroup k8s
 */
interface K8sPersistentVolumeClaimInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($val): K8sPersistentVolumeClaimInterface;

  /**
   * {@inheritdoc}
   */
  public function getPhase(): string;

  /**
   * {@inheritdoc}
   */
  public function setPhase($val): K8sPersistentVolumeClaimInterface;

  /**
   * {@inheritdoc}
   */
  public function getVolumeName(): string;

  /**
   * {@inheritdoc}
   */
  public function setVolumeName($val): K8sPersistentVolumeClaimInterface;

  /**
   * {@inheritdoc}
   */
  public function getCapacity(): string;

  /**
   * {@inheritdoc}
   */
  public function setCapacity($val): K8sPersistentVolumeClaimInterface;

  /**
   * {@inheritdoc}
   */
  public function getRequest(): string;

  /**
   * {@inheritdoc}
   */
  public function setRequest($val): K8sPersistentVolumeClaimInterface;

  /**
   * {@inheritdoc}
   */
  public function getAccessMode(): string;

  /**
   * {@inheritdoc}
   */
  public function setAccessMode($val): K8sPersistentVolumeClaimInterface;

  /**
   * {@inheritdoc}
   */
  public function getStorageClass(): string;

  /**
   * {@inheritdoc}
   */
  public function setStorageClass($val): K8sPersistentVolumeClaimInterface;

}
