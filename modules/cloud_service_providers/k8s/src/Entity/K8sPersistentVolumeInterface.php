<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a persistent volume entity.
 *
 * @ingroup k8s
 */
interface K8sPersistentVolumeInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCapacity();

  /**
   * {@inheritdoc}
   */
  public function setCapacity($capacity): K8sPersistentVolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getAccessModes(): string;

  /**
   * {@inheritdoc}
   */
  public function setAccessModes($access_modes): K8sPersistentVolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getReclaimPolicy(): string;

  /**
   * {@inheritdoc}
   */
  public function setReclaimPolicy($reclaim_policy): K8sPersistentVolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getStorageClassName(): string;

  /**
   * {@inheritdoc}
   */
  public function setStorageClassName($storage_class_name): K8sPersistentVolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getPhase(): string;

  /**
   * {@inheritdoc}
   */
  public function setPhase($phase): K8sPersistentVolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getClaimRef(): string;

  /**
   * {@inheritdoc}
   */
  public function setClaimRef($claim_ref): K8sPersistentVolumeInterface;

  /**
   * {@inheritdoc}
   */
  public function getReason(): string;

  /**
   * {@inheritdoc}
   */
  public function setReason($reason): K8sPersistentVolumeInterface;

}
