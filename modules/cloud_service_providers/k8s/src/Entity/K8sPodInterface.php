<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Pod entity.
 *
 * @ingroup k8s
 */
interface K8sPodInterface extends ContentEntityInterface, EntityOwnerInterface, K8sExportableEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getQosClass(): string;

  /**
   * {@inheritdoc}
   */
  public function setQosClass($qos_class): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getNodeName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNodeName($node_name): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getPodIp(): string;

  /**
   * {@inheritdoc}
   */
  public function setPodIp($pod_ip): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getContainers(): array;

  /**
   * {@inheritdoc}
   */
  public function setContainers($containers): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getRestarts(): int;

  /**
   * {@inheritdoc}
   */
  public function setRestarts($restarts): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getCpuRequest(): float;

  /**
   * {@inheritdoc}
   */
  public function setCpuRequest($cpu_request): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getCpuLimit(): float;

  /**
   * {@inheritdoc}
   */
  public function setCpuLimit($cpu_limit): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getCpuUsage(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setCpuUsage($cpu_usage): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getMemoryRequest(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setMemoryRequest($memory_request): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getMemoryLimit(): float;

  /**
   * {@inheritdoc}
   */
  public function setMemoryLimit($memory_limit): K8sPodInterface;

  /**
   * {@inheritdoc}
   */
  public function getMemoryUsage(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setMemoryUsage($memory_usage): K8sPodInterface;

}
