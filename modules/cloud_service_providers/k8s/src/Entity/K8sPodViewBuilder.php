<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;
use Drupal\cloud\Entity\CloudViewBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Pod view builders.
 */
class K8sPodViewBuilder extends CloudViewBuilder {

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityViewBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Theme\Registry $theme_registry
   *   The theme registry.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityRepositoryInterface $entity_repository,
    LanguageManagerInterface $language_manager,
    ?Registry $theme_registry = NULL,
    ?EntityDisplayRepositoryInterface $entity_display_repository = NULL,
    ?EntityTypeManager $entity_type_manager = NULL,
  ) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('theme.registry'),
      $container->get('entity_display.repository'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'entity_metrics',
        'title' => $this->t('Metrics'),
        'open' => TRUE,
        'fields' => [],
      ],
      [
        'name' => 'pod',
        'title' => $this->t('Pod'),
        'open' => TRUE,
        'fields' => [
          'name',
          'namespace',
          'status',
          'qos_class',
          'node_name',
          'pod_ip',
          'created',
          'labels',
          'annotations',
        ],
      ],
      [
        'name' => 'metrics',
        'title' => $this->t('Metrics'),
        'open' => TRUE,
        'fields' => [
          'cpu_request',
          'cpu_limit',
          'cpu_usage',
          'memory_request',
          'memory_limit',
          'memory_usage',
        ],
      ],
      [
        'name' => 'pod_containers',
        'title' => $this->t('Containers'),
        'open' => FALSE,
        'fields' => [
          'containers',
        ],
      ],
      [
        'name' => 'pod_detail',
        'title' => $this->t('Detail'),
        'open' => FALSE,
        'fields' => [
          'detail',
          'creation_yaml',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL): array {
    $build = parent::view($entity, $view_mode, $langcode);
    $limit_ranges = $this->entityTypeManager
      ->getStorage('k8s_limit_range')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'namespace' => $entity->getNamespace(),
      ]);

    if (count($limit_ranges) === 1) {
      $limit_range = reset($limit_ranges);

      $label = $this->t('Limit Range');
      $value = $limit_range->toLink($limit_range->getName())->toString();

      // Use html similar to a real field.
      $markup = <<<EOS
      <div class="field field--label-inline">
        <div class="field--label">$label</div>
        <div class="field--item">$value</div>
      </div>
EOS;
      $build['pod']['limit_range'] = [
        '#markup' => $markup,
      ];
    }

    return $build;
  }

}
