<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a priority class entity.
 *
 * @ingroup k8s
 */
interface K8sPriorityClassInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(): int;

  /**
   * {@inheritdoc}
   */
  public function setValue($value): K8sPriorityClassInterface;

  /**
   * {@inheritdoc}
   */
  public function getGlobalDefault(): bool;

  /**
   * {@inheritdoc}
   */
  public function setGlobalDefault($global_default): K8sPriorityClassInterface;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description): K8sPriorityClassInterface;

}
