<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the priority class entity type.
 */
class K8sPriorityClassViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_priority_class']['priority_class_bulk_form'] = [
      'title' => $this->t('Priority class operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple priority classes.'),
      'field' => [
        'id' => 'priority_class_bulk_form',
      ],
    ];

    return $data;
  }

}
