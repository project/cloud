<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a ReplicaSet entity.
 *
 * @ingroup k8s
 */
interface K8sReplicaSetInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sReplicaSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getReplicas(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setReplicas($replicas): K8sReplicaSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getSelector(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setSelector($selector): K8sReplicaSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getTemplate(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setTemplate($template): K8sReplicaSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getConditions(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setConditions($conditions): K8sReplicaSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getAvailableReplicas(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setAvailableReplicas($available_replicas): K8sReplicaSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getFullyLabeledReplicas(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setFullyLabeledReplicas($fully_labeled_replicas): K8sReplicaSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getReadyReplicas(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setReadyReplicas($ready_replicas): K8sReplicaSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getObservedGeneration(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setObservedGeneration($observed_generation): K8sReplicaSetInterface;

}
