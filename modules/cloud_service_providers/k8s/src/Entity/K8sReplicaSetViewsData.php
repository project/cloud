<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the ReplicaSet entity type.
 */
class K8sReplicaSetViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_replica_set']['replica_set_bulk_form'] = [
      'title' => $this->t('ReplicaSet operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple ReplicaSets.'),
      'field' => [
        'id' => 'replica_set_bulk_form',
      ],
    ];

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
