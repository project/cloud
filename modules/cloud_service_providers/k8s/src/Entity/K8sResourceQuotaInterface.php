<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a resource quota entity.
 *
 * @ingroup k8s
 */
interface K8sResourceQuotaInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sResourceQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getStatusHard(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setStatusHard($status_hard): K8sResourceQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getStatusUsed(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setStatusUsed($status_used): K8sResourceQuotaInterface;

}
