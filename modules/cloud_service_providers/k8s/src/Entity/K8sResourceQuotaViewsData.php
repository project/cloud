<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the resource quota entity type.
 */
class K8sResourceQuotaViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_resource_quota']['resource_quota_bulk_form'] = [
      'title' => $this->t('Resource quota operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple resource quotas.'),
      'field' => [
        'id' => 'resource_quota_bulk_form',
      ],
    ];

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
