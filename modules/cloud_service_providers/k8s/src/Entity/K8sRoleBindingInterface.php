<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a role binding entity.
 *
 * @ingroup k8s
 */
interface K8sRoleBindingInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sRoleBindingInterface;

  /**
   * {@inheritdoc}
   */
  public function getRole(): string;

  /**
   * {@inheritdoc}
   */
  public function setRole($role_ref): K8sRoleBindingInterface;

  /**
   * {@inheritdoc}
   */
  public function getSubjects(): array;

  /**
   * {@inheritdoc}
   */
  public function setSubjects($subjects): K8sRoleBindingInterface;

}
