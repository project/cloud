<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the role binding entity type.
 */
class K8sRoleBindingViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_role_binding']['role_binding_bulk_form'] = [
      'title' => $this->t('Role binding operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple role bindings.'),
      'field' => [
        'id' => 'role_binding_bulk_form',
      ],
    ];

    return $data;
  }

}
