<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Role entity.
 *
 * @ingroup k8s
 */
interface K8sRoleInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sRoleInterface;

  /**
   * {@inheritdoc}
   */
  public function getRules(): array;

  /**
   * {@inheritdoc}
   */
  public function setRules($rules): K8sRoleInterface;

}
