<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\k8s\Form\K8sContentFormInterface;

/**
 * Defines the Schedule entity.
 *
 * @ingroup k8s
 *
 * @ContentEntityType(
 *   id = "k8s_schedule",
 *   id_plural = "k8s_schedules",
 *   label = @Translation("Schedule"),
 *   label_collection = @Translation("Schedules"),
 *   label_singular = @Translation("Schedule"),
 *   label_plural = @Translation("Schedules"),
 *   handlers = {
 *     "view_builder" = "Drupal\k8s\Entity\K8sScheduleViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\k8s\Entity\K8sScheduleViewsData",
 *     "access"       = "Drupal\k8s\Controller\K8sAccessControlHandler",
 *     "form" = {
 *       "edit"       = "Drupal\k8s\Form\K8sScheduleEditForm",
 *       "delete"     = "Drupal\k8s\Form\K8sScheduleDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\k8s\Form\K8sScheduleDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "k8s_schedule",
 *   admin_permission = "administer k8s schedule",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *     "class" = "Drupal\k8s\Entity\K8sSchedule",
 *   },
 *   links = {
 *     "canonical"            = "/clouds/k8s/{cloud_context}/schedule/{k8s_schedule}",
 *     "collection"           = "/clouds/k8s/{cloud_context}/schedule",
 *     "edit-form"            = "/clouds/k8s/{cloud_context}/schedule/{k8s_schedule}/edit",
 *     "delete-form"          = "/clouds/k8s/{cloud_context}/schedule/{k8s_schedule}/delete",
 *     "delete-multiple-form" = "/clouds/k8s/{cloud_context}/schedule/delete_multiple",
 *   },
 *   field_ui_base_route = "k8s_schedule.settings"
 * )
 */
class K8sSchedule extends CloudContentEntityBase implements K8sScheduleInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name): K8sScheduleInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function setCreated($created = 0): K8sScheduleInterface {
    return $this->set('created', $created);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): ?int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): K8sScheduleInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public function getKind(): ?string {
    return $this->get('kind')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setKind($kind): K8sScheduleInterface {
    return $this->set('kind', $kind);
  }

  /**
   * {@inheritdoc}
   */
  public function getNamespaceName(): ?string {
    return $this->get('namespace_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNamespaceName($namespace_name): K8sScheduleInterface {
    return $this->set('namespace_name', $namespace_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceName(): ?string {
    return $this->get('resource_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceName($resource_name): K8sScheduleInterface {
    return $this->set('resource_name', $resource_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getStartHour(): ?int {
    return $this->get('start_hour')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStartHour($start_hour): K8sScheduleInterface {
    return $this->set('start_hour', $start_hour);
  }

  /**
   * {@inheritdoc}
   */
  public function getStartMinute(): ?int {
    return $this->get('start_minute')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStartMinute($start_minute): K8sScheduleInterface {
    return $this->set('start_minute', $start_minute);
  }

  /**
   * {@inheritdoc}
   */
  public function getStartTime(): ?string {
    return $this->get('start_time')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStartTime($start_time): K8sScheduleInterface {
    return $this->set('start_time', $start_time);
  }

  /**
   * {@inheritdoc}
   */
  public function getStopHour(): ?int {
    return $this->get('stop_hour')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStopHour($stop_hour): K8sScheduleInterface {
    return $this->set('stop_hour', $stop_hour);
  }

  /**
   * {@inheritdoc}
   */
  public function getStopMinute(): ?int {
    return $this->get('stop_minute')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStopMinute($stop_minute): K8sScheduleInterface {
    return $this->set('stop_minute', $stop_minute);
  }

  /**
   * {@inheritdoc}
   */
  public function getStopTime(): ?string {
    return $this->get('stop_time')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStopTime($stop_time): K8sScheduleInterface {
    return $this->set('stop_time', $stop_time);
  }

  /**
   * {@inheritdoc}
   */
  public function getManifest(): ?string {
    return $this->get('manifest')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setManifest($manifest): K8sScheduleInterface {
    return $this->set('manifest', $manifest);
  }

  /**
   * {@inheritdoc}
   */
  public function getLaunchTemplateName(): ?string {
    return $this->get('launch_template_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLaunchTemplateName($launch_template_name): K8sScheduleInterface {
    return $this->set('launch_template_name', $launch_template_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getState(): ?string {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state): K8sScheduleInterface {
    return $this->set('state', $state);
  }

  /**
   * {@inheritdoc}
   */
  public function getScheduleType(): ?string {
    return $this->get('schedule_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setScheduleType($schedule_type): K8sScheduleInterface {
    return $this->set('schedule_type', $schedule_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getYamlUrl(): ?string {
    return $this->get('yaml_url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setYamlUrl($yaml_url): K8sScheduleInterface {
    return $this->set('yaml_url', $yaml_url);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The entity name.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['kind'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Kind'))
      ->setDescription(t('The type of resource.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['namespace_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Namespace name'))
      ->setDescription(t('The name of namespace.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'k8s_namespace',
          'field_name' => 'name',
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['resource_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Resource name'))
      ->setDescription(t('The name of resource.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type_list' => 'k8s_deployment, k8s_pod',
          'field_name_list' => 'name, name',
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['start_hour'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Start Hour'))
      ->setDescription(t('The start hour of schedule.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['start_minute'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Start Minute'))
      ->setDescription(t('The start minute of schedule.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stop_hour'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Stop Hour'))
      ->setDescription(t('The stop hour of schedule.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stop_minute'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Stop Minute'))
      ->setDescription(t('The stop minute of schedule.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['start_time'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Start'))
      ->setDescription(t('The start time of schedule.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stop_time'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stop'))
      ->setDescription(t('The stop time of schedule.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['manifest'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Manifest'))
      ->setDescription(t('The manifest of pod.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'pre_string_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['launch_template_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Launch template name'))
      ->setDescription(t('Enter the name of launch template.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'cloud_launch_template',
          'field_name' => 'name',
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('State'))
      ->setDescription(t('The state of resource.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['schedule_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Schedule type'))
      ->setDescription(t('Schedule type.'))
      ->setDefaultValue(K8sContentFormInterface::CLOUD_ORCHESTRATOR_SCHEDULER);

    $fields['yaml_url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('YAML URL'))
      ->setDescription(t('YAML URL.'));

    return $fields;
  }

}
