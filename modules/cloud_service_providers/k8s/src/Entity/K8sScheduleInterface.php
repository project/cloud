<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Schedule entity.
 *
 * @ingroup k8s
 */
interface K8sScheduleInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getKind(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setKind($kind): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getNamespaceName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespaceName($namespace_name): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getResourceName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setResourceName($resource_name): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getStartHour(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setStartHour($start_hour): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getStartMinute(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setStartMinute($start_minute): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getStartTime(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStartTime($start_time): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getStopHour(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setStopHour($stop_hour): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getStopMinute(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setStopMinute($stop_minute): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getStopTime(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStopTime($stop_time): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getManifest(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setManifest($manifest): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getLaunchTemplateName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setLaunchTemplateName($launch_template_name): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getState(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setState($state): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getScheduleType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setScheduleType($schedule_type): K8sScheduleInterface;

  /**
   * {@inheritdoc}
   */
  public function getYamlUrl(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setYamlUrl($yaml_url): K8sScheduleInterface;

}
