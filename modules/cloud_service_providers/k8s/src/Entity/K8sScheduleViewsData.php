<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the Schedule entity type.
 */
class K8sScheduleViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_schedule']['schedule_bulk_form'] = [
      'title' => $this->t('Schedule operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple Schedules.'),
      'field' => [
        'id' => 'schedule_bulk_form',
      ],
    ];

    // Additional information for Views integration, such as table joins, can be
    // put here.
    $data['k8s_schedule']['table']['base']['query metadata'] = ['base_table' => 'k8s_schedule'];

    return $data;
  }

}
