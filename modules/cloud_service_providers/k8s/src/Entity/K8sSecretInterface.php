<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Secret entity.
 *
 * @ingroup k8s
 */
interface K8sSecretInterface extends ContentEntityInterface, EntityOwnerInterface, K8sExportableEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sSecretInterface;

  /**
   * {@inheritdoc}
   */
  public function getSecretType(): string;

  /**
   * {@inheritdoc}
   */
  public function setSecretType($secret_type): K8sSecretInterface;

  /**
   * {@inheritdoc}
   */
  public function getData(): array;

  /**
   * {@inheritdoc}
   */
  public function setData($data): K8sSecretInterface;

}
