<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a ServiceAccount entity.
 *
 * @ingroup k8s
 */
interface K8sServiceAccountInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sServiceAccountInterface;

  /**
   * {@inheritdoc}
   */
  public function getSecrets(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setSecrets($secrets): K8sServiceAccountInterface;

  /**
   * {@inheritdoc}
   */
  public function getImagePullSecrets(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setImagePullSecrets($image_pull_secrets): K8sServiceAccountInterface;

}
