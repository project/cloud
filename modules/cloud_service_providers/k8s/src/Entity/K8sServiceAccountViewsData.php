<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the ServiceAccount entity type.
 */
class K8sServiceAccountViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_service_account']['service_account_bulk_form'] = [
      'title' => $this->t('ServiceAccount operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple ServiceAccounts.'),
      'field' => [
        'id' => 'service_account_bulk_form',
      ],
    ];

    return $data;
  }

}
