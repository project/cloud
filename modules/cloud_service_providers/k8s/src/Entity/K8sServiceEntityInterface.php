<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Service entity.
 *
 * @ingroup k8s
 */
interface K8sServiceEntityInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sServiceEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getSelector(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setSelector($selector): K8sServiceEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getSessionAffinity(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setSessionAffinity($session_affinity): K8sServiceEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getClusterIp(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setClusterIp($cluster_ip): K8sServiceEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getInternalEndpoints(): array;

  /**
   * {@inheritdoc}
   */
  public function setInternalEndpoints($internal_endpoints): K8sServiceEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getExternalEndpoints(): array;

  /**
   * {@inheritdoc}
   */
  public function setExternalEndpoints($external_endpoints): K8sServiceEntityInterface;

}
