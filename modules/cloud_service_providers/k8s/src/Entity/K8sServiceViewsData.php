<?php

namespace Drupal\k8s\Entity;

/**
 * Provides the views data for the Service entity type.
 */
class K8sServiceViewsData extends K8sViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['k8s_service']['service_bulk_form'] = [
      'title' => $this->t('Service operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple services.'),
      'field' => [
        'id' => 'service_bulk_form',
      ],
    ];

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
