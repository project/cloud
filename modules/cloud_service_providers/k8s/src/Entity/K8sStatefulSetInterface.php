<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a StatefulSet entity.
 *
 * @ingroup k8s
 */
interface K8sStatefulSetInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getNamespace(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNamespace($namespace): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getUpdateStrategy(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setUpdateStrategy($update_strategy): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getRevisionHistoryLimit(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setRevisionHistoryLimit($revision_history_limit): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getServiceName(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setServiceName($service_name): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getPodManagementPolicy(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setPodManagementPolicy($pod_management_policy): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getCollisionCount(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setCollisionCount($collision_count): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getObservedGeneration(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setObservedGeneration($observed_generation): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getReadyReplicas(): int;

  /**
   * {@inheritdoc}
   */
  public function setReadyReplicas($ready_replicas): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getReplicas(): int;

  /**
   * {@inheritdoc}
   */
  public function setReplicas($replicas): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getCurrentReplicas(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setCurrentReplicas($current_replicas): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getUpdatedReplicas(): FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setUpdatedReplicas($updated_replicas): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getCurrentRevision(): string;

  /**
   * {@inheritdoc}
   */
  public function setCurrentRevision($current_revision): K8sStatefulSetInterface;

  /**
   * {@inheritdoc}
   */
  public function getUpdateRevision(): string;

  /**
   * {@inheritdoc}
   */
  public function setUpdateRevision($update_revision): K8sStatefulSetInterface;

}
