<?php

namespace Drupal\k8s\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a storage class entity.
 *
 * @ingroup k8s
 */
interface K8sStorageClassInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getParameters(): array;

  /**
   * {@inheritdoc}
   */
  public function setParameters($parameters): K8sStorageClassInterface;

  /**
   * {@inheritdoc}
   */
  public function getProvisioner(): string;

  /**
   * {@inheritdoc}
   */
  public function setProvisioner($provisioner): K8sStorageClassInterface;

  /**
   * {@inheritdoc}
   */
  public function getReclaimPolicy(): string;

  /**
   * {@inheritdoc}
   */
  public function setReclaimPolicy($reclaim_policy): K8sStorageClassInterface;

  /**
   * {@inheritdoc}
   */
  public function getVolumeBindingMode(): string;

  /**
   * {@inheritdoc}
   */
  public function setVolumeBindingMode($volume_binding_mode): K8sStorageClassInterface;

}
