<?php

namespace Drupal\k8s\Form\Config;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Traits\CloudFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Kubernetes Admin Notifications.
 */
class K8sAdminNotifications extends ConfigFormBase {

  use CloudFormTrait;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * The cloud service provider plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a K8sAdminNotifications object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityTypeManagerInterface $entity_type_manager,
    TypedConfigManagerInterface $typed_config_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->fileSystem = $file_system;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity_type.manager'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'k8s_admin_notifications';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['k8s.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('k8s.settings');

    $form['launch_template'] = [
      '#type' => 'details',
      '#title' => $this->t('Launch template'),
      '#open' => TRUE,
    ];

    $form['launch_template']['email_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Email Settings'),
      '#open' => TRUE,
    ];

    $form['launch_template']['email_settings']['k8s_launch_template_notification_request_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email addresses'),
      '#description' => $this->t('Email addresses to be notified for an approval request. The email addresses can be comma separated.'),
      '#default_value' => $config->get('k8s_launch_template_notification_request_emails'),
    ];

    $form['launch_template']['email_settings']['notification_request'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Notification of request'),
    ];

    $form['launch_template']['email_settings']['notification_request']['k8s_launch_template_notification_request_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject of the launch template request.'),
      '#default_value' => $config->get('k8s_launch_template_notification_request_subject'),
    ];

    $form['launch_template']['email_settings']['notification_request']['k8s_launch_template_notification_request_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#description' => $this->t('Available tokens are: [k8s_launch_template_request_email:launch_templates_request], [k8s_launch_template_request_email:site_url].  The [k8s_launch_template_request_email:launch_templates_request] variable can be configured in the launch template request information below.'),
      '#default_value' => $config->get('k8s_launch_template_notification_request_msg'),
    ];

    $form['launch_template']['email_settings']['notification_request']['k8s_launch_template_notification_launch_template_request_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Launch template request information'),
      '#default_value' => $config->get('k8s_launch_template_notification_launch_template_request_info'),
      '#description' => $this->t('Available tokens are: [k8s_launch_template:name], [k8s_launch_template:launch_template_link], [k8s_launch_template:launch_template_link_edit], [k8s_launch_template:launch_template_button_approve], [k8s_launch_template:changed].'),
    ];

    $form['launch_template']['email_settings']['notification_approved'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Notification of approval'),
    ];

    $form['launch_template']['email_settings']['notification_approved']['k8s_launch_template_notification_approved_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject of the launch template approval.'),
      '#default_value' => $config->get('k8s_launch_template_notification_approved_subject'),
    ];

    $form['launch_template']['email_settings']['notification_approved']['k8s_launch_template_notification_approved_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#description' => $this->t('Available tokens are: [k8s_launch_template_email:launch_templates], [site:url].  The [k8s_launch_template_email:launch_templates] variable can be configured in the launch template information below.'),
      '#default_value' => $config->get('k8s_launch_template_notification_approved_msg'),
    ];

    $form['launch_template']['email_settings']['notification_restore'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Notification of approved'),
    ];

    $form['launch_template']['email_settings']['notification_restore']['k8s_launch_template_notification_restore_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject of the launch template restore to draft.'),
      '#default_value' => $config->get('k8s_launch_template_notification_restore_subject'),
    ];

    $form['launch_template']['email_settings']['notification_restore']['k8s_launch_template_notification_restore_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#description' => $this->t('Available tokens are: [k8s_launch_template_email:launch_templates], [site:url].  The [k8s_launch_template_email:launch_templates] variable can be configured in the launch template information below.'),
      '#default_value' => $config->get('k8s_launch_template_notification_restore_msg'),
    ];

    $form['launch_template']['email_settings']['k8s_launch_template_notification_launch_template_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Launch template information'),
      '#default_value' => $config->get('k8s_launch_template_notification_launch_template_info'),
      '#description' => $this->t('Available tokens are: [k8s_launch_template:name], [k8s_launch_template:launch_template_link], [k8s_launch_template:launch_template_link_edit], [k8s_launch_template:changed].'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('k8s.settings');
    $form_state->cleanValues();
    foreach ($form_state->getValues() ?: [] as $key => $value) {
      $config->set($key, Html::escape($value));
    }

    $config->save();

    $views_settings = [];
    $views_settings['k8s_view_items_per_page'] = (int) $form_state->getValue('k8s_view_items_per_page');
    $views_settings['k8s_view_expose_items_per_page'] = (boolean) $form_state->getValue('k8s_view_expose_items_per_page');
    $this->updateViewsPagerOptions('k8s', $views_settings);

    parent::submitForm($form, $form_state);
  }

}
