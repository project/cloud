<?php

namespace Drupal\k8s\Form\Config;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Traits\CloudFormTrait;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Kubernetes Admin Settings.
 */
class K8sAdminSettings extends ConfigFormBase {

  use CloudFormTrait;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * The cloud service provider plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a K8sAdminSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityTypeManagerInterface $entity_type_manager,
    TypedConfigManagerInterface $typed_config_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->fileSystem = $file_system;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity_type.manager'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'k8s_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['k8s.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('k8s.settings');

    $form['views'] = [
      '#type' => 'details',
      '#title' => $this->t('Views'),
      '#open' => TRUE,
      '#description' => $this->t("Note that selecting the default option will overwrite View's settings."),
    ];

    $form['views']['refresh_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Refresh options'),
      '#open' => TRUE,
    ];

    $form['views']['refresh_options']['k8s_js_refresh_interval'] = [
      '#type' => 'number',
      '#title' => 'View refresh interval',
      '#description' => $this->t('Refresh UI (Charts and etc) at periodical intervals.'),
      '#default_value' => $config->get('k8s_js_refresh_interval'),
      '#min' => 1,
      '#max' => 9999,
      '#field_suffix' => 'seconds',
    ];

    $form['views']['refresh_options']['k8s_update_pricing_data_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow pricing data to be updated during rendering of K8s blocks.'),
      '#description' => $this->t('This allows the pricing data to be updated if not found in cache. Turning this on could result in performance issues if there are many regions to update.'),
      '#default_value' => $config->get('k8s_update_pricing_data_cache'),
    ];

    $form['views']['pager_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Pager options'),
      '#open' => TRUE,
    ];

    $form['views']['pager_options']['k8s_view_expose_items_per_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow user to control the number of items displayed in views.'),
      '#default_value' => $config->get('k8s_view_expose_items_per_page'),
      '#description' => $this->t('When enabled, an "Items per page" dropdown listbox is shown.'),
    ];

    $form['views']['pager_options']['k8s_view_items_per_page'] = [
      '#type' => 'select',
      '#options' => cloud_get_views_items_options(),
      '#title' => $this->t('Items per page'),
      '#description' => $this->t('Number of items to display on each page in views.'),
      '#default_value' => $config->get('k8s_view_items_per_page'),
    ];

    $form['cron'] = [
      '#type' => 'details',
      '#title' => $this->t('Cron'),
      '#open' => TRUE,
    ];

    $form['cron']['k8s_update_resources_queue_cron_time'] = [
      '#type' => 'number',
      '#title' => 'Update resources every',
      '#description' => $this->t('Run cron to process queue to update resources.'),
      '#default_value' => $config->get('k8s_update_resources_queue_cron_time'),
      '#min' => 1,
      '#max' => 9999,
      '#field_suffix' => 'seconds',
    ];

    $form['cron']['k8s_update_cost_store_cron_time'] = [
      '#type' => 'number',
      '#title' => 'Update cost store every',
      '#description' => $this->t('Run cron to process queue to update cost stores.'),
      '#default_value' => $config->get('k8s_update_cost_store_cron_time'),
      '#min' => 1,
      '#max' => 9999,
      '#field_suffix' => 'seconds',
    ];

    $form['cron']['k8s_queue_limit'] = [
      '#type' => 'number',
      '#title' => 'Queue Limit',
      '#description' => $this->t('Number of items allowed in the update queue. New items are added when the queue count falls below this threshold.'),
      '#default_value' => $config->get('k8s_queue_limit'),
    ];

    $form['launch_templates'] = [
      '#type' => 'details',
      '#title' => $this->t('Launch Templates'),
      '#open' => TRUE,
    ];

    $extensions = str_replace(' ', ', ', $config->get('k8s_yaml_file_extensions'));
    $form['launch_templates']['k8s_yaml_file_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed YAML file extensions'),
      '#default_value' => $extensions,
      '#description' => $this->t('Separate extensions with a space or comma and do not include the leading dot.'),
      '#element_validate' => [[FileItem::class, 'validateExtensions']],
      '#weight' => 1,
      '#maxlength' => 256,
      '#required' => TRUE,
    ];

    $form['icon'] = [
      '#type' => 'details',
      '#title' => $this->t('Icon'),
      '#open' => TRUE,
    ];

    $form['icon']['k8s_cloud_config_icon'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('K8s cloud service provider icon'),
      '#default_value' => [
        'fids' => $config->get('k8s_cloud_config_icon'),
      ],
      '#description' => $this->t('Upload an image to represent K8s.'),
      '#upload_location' => 'public://images/cloud/icons',
      '#upload_validators' => [
        'file_validate_is_image' => [],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('k8s.settings');
    $form_state->cleanValues();
    foreach ($form_state->getValues() ?: [] as $key => $value) {

      if ($key === 'k8s_cloud_config_icon') {
        $fileStorage = $this->entityTypeManager->getStorage('file');
        $file = $fileStorage->load($value[0]);
        // Save the icon.
        if (!empty($file)) {
          $file->setPermanent();
          $file->save();
          $config->set('k8s_cloud_config_icon', $value[0]);
        }
        else {
          $config->set('k8s_cloud_config_icon', '');
        }
        continue;
      }

      $config->set($key, Html::escape($value));
    }

    $config->save();

    $views_settings = [];
    $views_settings['k8s_view_items_per_page'] = (int) $form_state->getValue('k8s_view_items_per_page');
    $views_settings['k8s_view_expose_items_per_page'] = (boolean) $form_state->getValue('k8s_view_expose_items_per_page');
    $this->updateViewsPagerOptions('k8s', $views_settings);

    parent::submitForm($form, $form_state);
  }

}
