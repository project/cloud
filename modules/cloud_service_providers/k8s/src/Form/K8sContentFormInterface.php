<?php

namespace Drupal\k8s\Form;

/**
 * Provides an interface defining a K8s content form.
 */
interface K8sContentFormInterface {

  public const MAX_RESOURCE_NAME_LENGTH = 63;

  public const CLOUD_ORCHESTRATOR_SCHEDULER = 'cloud_orchestrator_scheduler';

  public const CRONJOB_SCHEDULER = 'cronjob_scheduler';

  public const TIME_SCHEDULER_OPTIONS = [
    self::CLOUD_ORCHESTRATOR_SCHEDULER => 'Use Cloud Orchestrator',
    self::CRONJOB_SCHEDULER => 'Use CronJob',
  ];

}
