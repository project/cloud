<?php

namespace Drupal\k8s\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form controller for the entity create form.
 *
 * @ingroup k8s
 */
class K8sCreateForm extends K8sContentForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state);
    $this->k8sService->setCloudContext($cloud_context);

    $name_underscore = $this->getShortEntityTypeNameUnderscore($this->entity);
    $name_whitespace = $this->getShortEntityTypeNameWhitespace($this->entity);

    $weight = -50;

    $form[$name_underscore] = [
      '#type' => 'details',
      '#title' => $name_whitespace,
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    if (method_exists($this->entity, 'getNamespace')) {

      $options = [];
      try {
        $options = $this->getNamespaceOptions();
      }
      catch (\Exception $e) {
        $this->k8sService->handleError($e, $cloud_context, $this->entity);
      }

      // Redirect to list view if there isn't a namespace which can be used.
      if (empty($options)) {
        $this->messenger->addWarning(
          $this->t('You will not be able to create a K8s @type since you have no accessible K8s namespace.', [
            '@type' => $this->entity->getEntityType()->getSingularLabel(),
          ])
        );

        if ($this->currentUser->hasPermission('add k8s namespace')) {
          $add_k8s_namespace = Link::createFromRoute($this->t('create a namespace'), 'entity.k8s_namespace.add_form', [
            'cloud_context' => $cloud_context,
          ])->toString();

          $this->messenger->addWarning($this->t('Try to @add_k8s_namespace or contact your site administrator.', [
            '@add_k8s_namespace' => $add_k8s_namespace,
          ]));
        }

        $redirect_url = Url::fromRoute("view.k8s_{$this->getShortEntityTypeNameUnderScore($this->entity)}.list", [
          'cloud_context' => $cloud_context,
        ]);
        $redirect = new RedirectResponse($redirect_url->toString());
        $redirect->send();
        return $form;
      }

      $form[$name_underscore]['namespace'] = [
        '#type'          => 'select',
        '#title'         => $this->t('Namespace'),
        '#options'       => $options,
        '#default_value' => 'default',
        '#required'      => TRUE,
        '#weight' => $weight++,
      ];
    }

    $form[$name_underscore]['detail'] = $form['detail'];
    $form[$name_underscore]['detail']['#states'] = [
      'invisible' => [
        'input[name="time_scheduler_option"]' => [
          'value' => K8sContentFormInterface::CRONJOB_SCHEDULER,
        ],
      ],
    ];
    $form[$name_underscore]['detail']['#weight'] = $weight++;
    unset($form['detail']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\k8s\Service\K8sServiceException
   *    Thrown when unable to create entity.
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->trimTextfields($form, $form_state);

    $entity = $this->entity;
    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $namespace = method_exists($this->entity, 'getNamespace')
      ? $this->entity->getNamespace()
      : 'default';
    $detail = $this->entity->getDetail();
    $enable_time_scheduler = !empty($form_state->getValue('enable_time_scheduler'));
    $start_hour = $form_state->getValue('start_hour', 6);
    $start_minute = $form_state->getValue('start_minute', 0);
    $stop_hour = $form_state->getValue('stop_hour', 18);
    $stop_minute = $form_state->getValue('stop_minute', 0);

    $this->k8sOperationsService->createK8sEntity(
      $entity,
      $cloud_context,
      $namespace,
      $detail,
      $enable_time_scheduler,
      $start_hour,
      $start_minute,
      $stop_hour,
      $stop_minute
    );

    $form_state->setRedirect(
      "view.k8s_{$this->getShortEntityTypeNameUnderscore($entity)}.list",
      ['cloud_context' => $entity->getCloudContext()]
    );
  }

}
