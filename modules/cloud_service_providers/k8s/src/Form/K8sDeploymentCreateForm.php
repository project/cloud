<?php

namespace Drupal\k8s\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\k8s\Traits\K8sFormTrait;

/**
 * Form controller for the Deployment create form.
 *
 * @ingroup k8s
 */
class K8sDeploymentCreateForm extends K8sCreateForm {

  use K8sFormTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state, $cloud_context);

    $form['deployment']['yaml_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('YAML URL'),
      '#description' => $this->t(
        'This must be an external URL such as %url',
        ['%url' => 'https://example.com/examples/hello_world.yaml']
      ),
      '#states' => [
        'visible' => [
          'input[name="enable_time_scheduler"]' => ['checked' => TRUE],
          'input[name="time_scheduler_option"]' => [
            'value' => K8sContentFormInterface::CRONJOB_SCHEDULER,
          ],
        ],
      ],
    ];

    $form['time_scheduler'] = [
      '#type' => 'details',
      '#title' => $this->t('Time scheduler'),
      '#open' => TRUE,
      '#weight' => $form['deployment']['#weight'],
    ];

    $form['time_scheduler']['enable_time_scheduler'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable time scheduler'),
    ];

    $states = [
      'visible' => [
        ':input[name="enable_time_scheduler"]' => ['checked' => TRUE],
      ],
    ];

    $form['time_scheduler']['container'] = [
      '#type' => 'container',
      '#states' => $states,
    ];

    $form['time_scheduler']['container']['time_scheduler_option_title'] = [
      '#type' => 'item',
      '#title' => 'Scheduler Use Type',
    ];

    $form['time_scheduler']['container']['time_scheduler_option'] = [
      '#type' => 'radios',
      '#default_value' => K8sContentFormInterface::CLOUD_ORCHESTRATOR_SCHEDULER,
      '#options' => K8sContentFormInterface::TIME_SCHEDULER_OPTIONS,
      '#states' => $states,
    ];

    $form['time_scheduler']['container']['start_time_title'] = [
      '#type' => 'item',
      '#title' => 'Start-up Time',
    ];

    $hour_options = [];
    for ($i = 0; $i < 24; $i++) {
      $hour_options[$i] = sprintf('%02d', $i);
    }

    $minute_options = [];
    for ($i = 0; $i < 60; $i++) {
      $minute_options[$i] = sprintf('%02d', $i);
    }

    $form['time_scheduler']['container']['start_hour'] = [
      '#type' => 'select',
      '#options' => $hour_options,
      '#default_value' => 0,
      '#prefix' => '<div class= "container-inline">',
    ];

    $form['time_scheduler']['container']['start_minute'] = [
      '#type' => 'select',
      '#options' => $minute_options,
      '#default_value' => 0,
      '#prefix' => ': ',
      '#suffix' => '</div>',
    ];

    $form['time_scheduler']['container']['stop_time_title'] = [
      '#type' => 'item',
      '#title' => 'Stop Time',
    ];

    $form['time_scheduler']['container']['stop_hour'] = [
      '#type' => 'select',
      '#options' => $hour_options,
      '#default_value' => 6,
      '#prefix' => '<div class= "container-inline">',
    ];

    $form['time_scheduler']['container']['stop_minute'] = [
      '#type' => 'select',
      '#options' => $minute_options,
      '#default_value' => 0,
      '#prefix' => ': ',
      '#suffix' => '</div>',
    ];

    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    $cloud_config_link = Link::fromTextAndUrl(
      $cloud_config->getName(),
      Url::fromRoute(
        'entity.cloud_config.canonical',
        [
          'cloud_config' => $cloud_config->id(),
        ]
      )
    )->toString();

    $form['time_scheduler']['container']['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Specify start-up and stop time in %user_timezone. When you choose a <strong><em>Use CronJob</em></strong> option, the system automatically adjusts the time based on the timezone of K8s cluster <strong>%cloud_config_link</strong> for the CronJob manifest, so you will find the adjusted start-up and stop time in CronJob information.', [
        '%user_timezone' => date_default_timezone_get(),
        '%cloud_config_link' => $cloud_config_link,
      ]),
      '#states' => [
        'visible' => [
          'input[name="enable_time_scheduler"]' => ['checked' => TRUE],
          'input[name="time_scheduler_option"]' => [
            'value' => K8sContentFormInterface::CRONJOB_SCHEDULER,
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('time_scheduler_option') === K8sContentFormInterface::CRONJOB_SCHEDULER) {
      $yaml_url = $form_state->getValue('yaml_url');
      if (empty($yaml_url)) {
        $form_state->setErrorByName('yaml_url', $this->t('The yaml is empty.'));
        return;
      }
      $content = file_get_contents($yaml_url);
      if (
        empty($content)
        || empty($this->k8sService->decodeMultipleDocYaml($content))
      ) {
        $form_state->setErrorByName('yaml_url', $this->t('The yaml is invalid.'));
        return;
      }
    }
    else {
      $detail = $form['deployment']['detail']['widget'][0]['value']['#value'];
      if (empty($detail)) {
        $form_state->setErrorByName('detail', $this->t('The detail is empty.'));
        return;
      }
      elseif (empty($this->k8sService->decodeMultipleDocYaml($detail))) {
        $form_state->setErrorByName('detail', $this->t('The detail is invalid.'));
        return;
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\k8s\Service\K8sServiceException
   *    Thrown when unable to create entity.
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->trimTextfields($form, $form_state);

    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $namespace = method_exists($this->entity, 'getNamespace')
      ? $this->entity->getNamespace()
      : 'default';
    $detail = $this->entity->getDetail();
    $enable_time_scheduler = !empty($form_state->getValue('enable_time_scheduler'));
    $time_scheduler_option = $form_state->getValue('time_scheduler_option');
    $yaml_url = $form_state->getValue('yaml_url');
    $start_hour = $form_state->getValue('start_hour');
    $start_minute = $form_state->getValue('start_minute');
    $stop_hour = $form_state->getValue('stop_hour');
    $stop_minute = $form_state->getValue('stop_minute');

    $this->k8sOperationsService->createDeployment(
      $this->entity,
      $cloud_context,
      $namespace,
      $detail,
      $enable_time_scheduler,
      $time_scheduler_option,
      $yaml_url,
      $start_hour,
      $start_minute,
      $stop_hour,
      $stop_minute
    );

    $form_state->setRedirect(
      "view.k8s_{$this->getShortEntityTypeNameUnderscore($this->entity)}.list",
      ['cloud_context' => $this->entity->getCloudContext()]
    );
  }

}
