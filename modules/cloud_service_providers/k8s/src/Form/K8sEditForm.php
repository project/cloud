<?php

namespace Drupal\k8s\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the entity edit forms.
 *
 * @ingroup k8s
 */
class K8sEditForm extends K8sContentForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    $name_underscore = $this->getShortEntityTypeNameUnderscore($entity);
    $name_whitespace = $this->getShortEntityTypeNameWhitespace($entity);

    $weight = -50;
    $form[$name_underscore] = [
      '#type' => 'details',
      '#title' => $name_whitespace,
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form[$name_underscore]['name'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Name')),
      '#markup'        => $entity->getName(),
      '#weight'        => $weight++,
    ];

    if (method_exists($this->entity, 'getNamespace')) {
      $form[$name_underscore]['namespace'] = [
        '#type'          => 'item',
        '#title'         => $this->getItemTitle($this->t('Namespace')),
        '#markup'        => $entity->getNamespace(),
        '#weight'        => $weight++,
      ];
    }

    if (!empty($form['detail'])) {
      $form[$name_underscore]['detail'] = $form['detail'];
      unset($form['detail']);
    }

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);
    $form['actions']['#weight'] = $weight++;

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\k8s\Service\K8sServiceException
   *    Thrown when unable to update entity.
   */
  public function save(array $form, FormStateInterface $form_state): void {
    // Call copyFormItemValues() to ensure the form array is intact.
    $this->copyFormItemValues($form);

    $this->trimTextfields($form, $form_state);
    $entity = $this->entity;

    $this->k8sOperationsService->updateEntity($entity, ['detail' => $entity->getDetail()]);

    $name_underscore = $this->getShortEntityTypeNameUnderscore($entity);
    $form_state->setRedirect("view.k8s_{$name_underscore}.list", [
      'cloud_context' => $entity->getCloudContext(),
    ]);
  }

}
