<?php

namespace Drupal\k8s\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the Namespace entity create form.
 *
 * @ingroup k8s
 */
class K8sNamespaceCreateForm extends K8sContentForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state);
    $this->k8sService->setCloudContext($cloud_context);

    try {
      // Try to test if getNamespaces() is successful or not.
      $this->k8sService->getNamespaces();
    }
    // If getNamespaces() is not successful, redirect to the namespace list.
    catch (\Exception $e) {
      $this->k8sService->handleError($e, $cloud_context, $this->entity);
    }

    $weight = -50;

    $form['namespace'] = [
      '#type' => 'details',
      '#title' => $this->t('Namespace'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['namespace']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#description' => $this->t("A lowercase RFC 1123 label must consist of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character (e.g. 'my-name', or '123-abc')."),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#required'      => TRUE,
    ];

    $form['namespace']['labels'] = $form['labels'];
    $form['namespace']['annotations'] = $form['annotations'];

    unset($form['labels']);
    unset($form['annotations']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->trimTextfields($form, $form_state);

    $cloud_context = $this->routeMatch->getParameter('cloud_context');

    $entity = $this->entity;
    $entity->setCloudContext($cloud_context);
    $name = $entity->getName();
    $labels = $entity->getLabels();

    $status = $this->k8sOperationsService->createNamespace($entity, $cloud_context, $name, $labels);
    if (!empty($status)) {
      $form_state->setRedirect('view.k8s_namespace.list', ['cloud_context' => $entity->getCloudContext()]);
    }
  }

}
