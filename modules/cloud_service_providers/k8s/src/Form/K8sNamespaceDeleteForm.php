<?php

namespace Drupal\k8s\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a form for deleting a Namespace entity.
 *
 * @ingroup k8s
 */
class K8sNamespaceDeleteForm extends K8sDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    $entity = $this->entity;

    return $this->t('Are you sure you want to delete the "@name" namespace?<br>CAUTION: The role "@name" is also going to be deleted.', [
      '@name' => $entity->getName(),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\k8s\Service\K8sServiceException
   *    Thrown when unable to delete entity.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entity = $this->entity;

    $this->k8sOperationsService->deleteNamespace($entity);

    $form_state->setRedirect('view.k8s_namespace.list', ['cloud_context' => $entity->getCloudContext()]);
  }

}
