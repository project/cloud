<?php

namespace Drupal\k8s\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the Namespace entity edit forms.
 *
 * @ingroup k8s
 */
class K8sNamespaceEditForm extends K8sContentForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;
    $weight = -50;
    $form['namespace'] = [
      '#type' => 'details',
      '#title' => $this->t('Namespace'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['namespace']['name'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Name')),
      '#markup'        => $entity->getName(),
    ];

    $form['namespace']['labels'] = $form['labels'];
    $form['namespace']['annotations'] = $form['annotations'];

    unset($form['labels']);
    unset($form['annotations']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);
    $form['actions']['#weight'] = $weight++;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    // Call copyFormItemValues() to ensure the form array is intact.
    $this->copyFormItemValues($form);

    $this->trimTextfields($form, $form_state);

    $entity = $this->entity;

    $this->k8sOperationsService->updateNamespace($entity, [
      'labels' => [],
      'annotations' => [],
    ]);

    $form_state->setRedirect('view.k8s_namespace.list', [
      'cloud_context' => $entity->getCloudContext(),
    ]);
  }

}
