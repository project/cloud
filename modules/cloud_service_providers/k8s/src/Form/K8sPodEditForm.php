<?php

namespace Drupal\k8s\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the entity edit forms.
 *
 * @ingroup k8s
 */
class K8sPodEditForm extends K8sEditForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state, $cloud_context);

    $entity = $this->entity;
    $limit_ranges = $this->entityTypeManager
      ->getStorage('k8s_limit_range')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'namespace' => $entity->getNamespace(),
      ]);

    if (count($limit_ranges) === 1) {
      $limit_range = reset($limit_ranges);
      $form['pod']['limit_range'] = [
        '#type' => 'item',
        '#title' => $this->t('Limit Range')->render() . ': ',
        '#markup' => $limit_range->toLink($limit_range->getName())->toString(),
        '#weight' => $form['pod']['detail']['#weight'] - 1,
      ];
    }

    return $form;
  }

}
