<?php

namespace Drupal\k8s\Form;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Form\FormStateInterface;
use Drupal\k8s\Service\K8sServiceException;

/**
 * Form controller for the Pod entity log forms.
 *
 * @ingroup k8s
 */
class K8sPodLogForm extends K8sContentForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state);
    unset($form['detail']);
    unset($form['actions']);
    unset($form['uid']);

    $entity = $this->entity;

    try {
      $this->k8sService->setCloudContext($entity->getCloudContext());

      $container_options = [];
      foreach ($entity->getContainers() as $container) {
        $container_obj = Yaml::decode($container['value']);
        $container_options[$container_obj['name']] = $container_obj['name'];
      }
      if (count($container_options) > 1) {
        $form['container'] = [
          '#type' => 'select',
          '#title' => $this->t('Container'),
          '#options' => $container_options,
          '#ajax' => [
            'callback' => '::containerCallback',
            'wrapper'  => 'log-wrapper',
          ],
        ];
      }

      $form['log-wrapper'] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'log-wrapper',
        ],
      ];

      $params = [
        'metadata' => ['name' => $entity->getName()],
      ];
      $container_name = NULL;
      if (!empty($container_options)) {
        $container_name = array_keys($container_options)[0];
      }
      $logs = $this->k8sService->getPodLogs($entity->getNamespace(), $params, $container_name);
      $logs = htmlspecialchars($logs);
      $form['log-wrapper']['log'] = [
        '#type'          => 'item',
        '#markup'        => "<pre>$logs</pre>",
      ];
    }
    catch (K8sServiceException $e) {
      $this->messenger->addError($this->t('Unable to retrieve logs of @label.', [
        '@label' => $entity->getEntityType()->getSingularLabel(),
      ]));

      $this->logger('k8s')->error($this->t('Unable to retrieve logs of @label.', [
        '@label' => $entity->getEntityType()->getSingularLabel(),
      ]));
    }

    return $form;
  }

  /**
   * Ajax callback for select form item container.
   *
   * @param array &$form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   Response.
   */
  public function containerCallback(array &$form, FormStateInterface $form_state): array {
    $entity = $this->entity;
    $params = [
      'metadata' => ['name' => $entity->getName()],
    ];
    $logs = $this->k8sService->getPodLogs($entity->getNamespace(), $params, $form_state->getValue('container'));
    $logs = htmlspecialchars($logs);
    $form['log-wrapper']['log']['#markup'] = "<pre>$logs</pre>";
    return $form['log-wrapper'];
  }

}
