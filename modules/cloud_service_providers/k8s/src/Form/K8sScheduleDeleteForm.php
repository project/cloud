<?php

namespace Drupal\k8s\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a form for deleting a Schedule entity.
 *
 * @ingroup k8s
 */
class K8sScheduleDeleteForm extends K8sDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    $entity = $this->entity;

    return $this->t('Are you sure you want to delete the "@name" schedule?<br>CAUTION: The @kind "@resource_name" is also going to be deleted.', [
      '@name' => $entity->getName(),
      '@kind' => $entity->getKind(),
      '@resource_name' => $entity->getResourceName(),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\k8s\Service\K8sServiceException
   *    Thrown when unable to delete entity.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entity = $this->entity;

    // Delete an entity for a resource scheduled in K8sSchedule.
    $scheduled_entity = !empty($entity->getCloudContext()) && !empty($entity->getResourceName()) && $entity->getNamespaceName()
      ? $this->k8sService->getEntity(
        'k8s_' . self::getSnakeCase($entity->getKind()),
        $entity->getCloudContext(),
        $entity->getResourceName(),
        $entity->getNamespaceName()
      )
      : NULL;
    empty($scheduled_entity) ?: $this->k8sOperationsService->deleteK8sEntity($scheduled_entity);

    // Delete K8sSchedule entity.
    $this->k8sOperationsService->deleteK8sEntity($entity);

    $form_state->setRedirect('view.k8s_schedule.list', ['cloud_context' => $entity->getCloudContext()]);
  }

}
