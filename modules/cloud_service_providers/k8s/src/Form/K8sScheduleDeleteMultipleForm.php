<?php

namespace Drupal\k8s\Form;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Provides an entities deletion confirmation form.
 */
class K8sScheduleDeleteMultipleForm extends K8sDeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  protected function processCloudResource(CloudContentEntityBase $entity): bool {

    // Delete an entity for a resource scheduled in K8sSchedule.
    $scheduled_entity = !empty($entity->getCloudContext()) && !empty($entity->getResourceName()) && $entity->getNamespaceName()
      ? $this->k8sService->getEntity(
        'k8s_' . self::getSnakeCase($entity->getKind()),
        $entity->getCloudContext(),
        $entity->getResourceName(),
        $entity->getNamespaceName()
      )
      : NULL;
    if (!empty($scheduled_entity)) {
      $name_camel = $this->getShortEntityTypeNameCamel($scheduled_entity);
      $this->deleteCloudResource($scheduled_entity, "delete{$name_camel}");
    }

    return $this->deleteCloudResource($entity, 'deleteSchedule');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {

    return $this->formatPlural(count($this->selection),
      'Are you sure you want to delete this @item?<br>CAUTION: The scheduled resources is also going to be deleted.',
      'Are you sure you want to delete these @items?<br>CAUTION: The scheduled resources are also going to be deleted.', [
        '@item' => $this->entityType->getSingularLabel(),
        '@items' => $this->entityType->getPluralLabel(),
      ]
    );
  }

}
