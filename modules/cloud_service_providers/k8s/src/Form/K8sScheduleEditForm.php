<?php

namespace Drupal\k8s\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\k8s\Traits\K8sFormTrait;

/**
 * Form controller for the entity edit forms.
 *
 * @ingroup k8s
 */
class K8sScheduleEditForm extends K8sEditForm {

  use K8sFormTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state, $cloud_context);

    $entity = $this->entity;
    $form['schedule']['namespace_name'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Namespace name')),
      '#markup'        => $entity->getNamespaceName(),
    ];

    $form['schedule']['resource_name'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Resource name')),
      '#markup'        => $entity->getResourceName(),
    ];

    $form['schedule']['launch_template_name'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Launch template name')),
      '#markup'        => $entity->getLaunchTemplateName(),
    ];

    $hour_options = [];
    for ($i = 0; $i < 24; $i++) {
      $hour_options[$i] = sprintf('%02d', $i);
    }

    $minute_options = [];
    for ($i = 0; $i < 60; $i++) {
      $minute_options[$i] = sprintf('%02d', $i);
    }

    $form['schedule']['start_time_title'] = [
      '#type'          => 'item',
      '#title'         => 'Start-up Time',
    ];

    $form['schedule']['start_hour'] = [
      '#type'          => 'select',
      '#options'       => $hour_options,
      '#default_value' => $entity->getStartHour(),
      '#prefix'        => '<div class= "container-inline">',
    ];

    $form['schedule']['start_minute'] = [
      '#type'          => 'select',
      '#options'       => $minute_options,
      '#default_value' => $entity->getStartMinute(),
      '#prefix'        => ': ',
      '#suffix'        => '</div>',
    ];

    $form['schedule']['stop_time_title'] = [
      '#type'          => 'item',
      '#title'         => 'Stop Time',
    ];

    $form['schedule']['stop_hour'] = [
      '#type'          => 'select',
      '#options'       => $hour_options,
      '#default_value' => $entity->getStopHour(),
      '#prefix'        => '<div class= "container-inline">',
    ];

    $form['schedule']['stop_minute'] = [
      '#type'          => 'select',
      '#options'       => $minute_options,
      '#default_value' => $entity->getStopMinute(),
      '#prefix'        => ': ',
      '#suffix'        => '</div>',
    ];

    $form['schedule']['state'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('State')),
      '#markup'        => $entity->getState(),
    ];

    $form['schedule_detail'] = [
      '#type'          => 'details',
      '#title'         => $this->t('Detail'),
      '#open'          => TRUE,
      '#weight'        => $form['schedule']['#weight'],
    ];

    $form['schedule_detail']['manifest'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Manifest'),
      '#default_value' => $entity->getManifest(),
      '#rows'          => 20,
      '#disabled'      => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\k8s\Service\K8sServiceException
   *    Thrown when unable to update entity.
   */
  public function save(array $form, FormStateInterface $form_state): void {
    // Call copyFormItemValues() to ensure the form array is intact.
    $this->copyFormItemValues($form);

    $this->trimTextfields($form, $form_state);

    $entity = $this->entity;

    $this->k8sOperationsService->updateSchedule($entity, [
      'startHour' => $entity->getStartHour(),
      'startMinute' => $entity->getStartMinute(),
      'stopHour' => $entity->getStopHour(),
      'stopMinute' => $entity->getStopMinute(),
    ]);

    $form_state->setRedirect('view.k8s_schedule.list', [
      'cloud_context' => $entity->getCloudContext(),
    ]);
  }

}
