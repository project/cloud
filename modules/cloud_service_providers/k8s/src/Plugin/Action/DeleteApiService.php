<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to an API service form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_api_service",
 *   label = @Translation("Delete API service"),
 *   type = "k8s_api_service"
 * )
 */
class DeleteApiService extends DeleteAction {

}
