<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a cluster role binding form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_cluster_role_binding",
 *   label = @Translation("Delete cluster role binding"),
 *   type = "k8s_cluster_role_binding"
 * )
 */
class DeleteClusterRoleBinding extends DeleteAction {

}
