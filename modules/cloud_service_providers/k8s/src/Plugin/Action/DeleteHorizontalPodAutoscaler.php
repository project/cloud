<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a horizontal pod autoscaler form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_horizontal_pod_autoscaler",
 *   label = @Translation("Delete horizontal pod autoscaler"),
 *   type = "k8s_horizontal_pod_autoscaler"
 * )
 */
class DeleteHorizontalPodAutoscaler extends DeleteAction {

}
