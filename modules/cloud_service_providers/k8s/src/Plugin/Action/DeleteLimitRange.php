<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a LimitRange form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_limit_range",
 *   label = @Translation("Delete LimitRange"),
 *   type = "k8s_limit_range"
 * )
 */
class DeleteLimitRange extends DeleteAction {

}
