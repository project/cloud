<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a network policy form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_network_policy",
 *   label = @Translation("Delete network policy"),
 *   type = "k8s_network_policy"
 * )
 */
class DeleteNetworkPolicy extends DeleteAction {

}
