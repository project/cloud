<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a persistent volume claim form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_persistent_volume_claim",
 *   label = @Translation("Delete persistent volume claim"),
 *   type = "k8s_persistent_volume_claim"
 * )
 */
class DeletePersistentVolumeClaim extends DeleteAction {

}
