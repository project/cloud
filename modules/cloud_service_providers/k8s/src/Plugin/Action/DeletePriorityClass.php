<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a priority class form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_priority_class",
 *   label = @Translation("Delete priority class"),
 *   type = "k8s_priority_class"
 * )
 */
class DeletePriorityClass extends DeleteAction {

}
