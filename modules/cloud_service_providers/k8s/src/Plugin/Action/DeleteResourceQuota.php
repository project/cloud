<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a resource quota form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_resource_quota",
 *   label = @Translation("Delete resource quota"),
 *   type = "k8s_resource_quota"
 * )
 */
class DeleteResourceQuota extends DeleteAction {

}
