<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a role binding form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_role_binding",
 *   label = @Translation("Delete role binding"),
 *   type = "k8s_role_binding"
 * )
 */
class DeleteRoleBinding extends DeleteAction {

}
