<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a Schedule form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_schedule",
 *   label = @Translation("Delete Schedule"),
 *   type = "k8s_schedule"
 * )
 */
class DeleteSchedule extends DeleteAction {

}
