<?php

namespace Drupal\k8s\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a storage class form.
 *
 * @Action(
 *   id = "entity:delete_action:k8s_storage_class",
 *   label = @Translation("Delete storage class"),
 *   type = "k8s_storage_class"
 * )
 */
class DeleteStorageClass extends DeleteAction {

}
