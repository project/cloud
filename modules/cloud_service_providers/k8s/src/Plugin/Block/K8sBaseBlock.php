<?php

namespace Drupal\k8s\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginException;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\k8s\Service\CostFieldsRendererInterface;
use Drupal\k8s\Service\K8sServiceInterface;
use Drupal\k8s\Traits\K8sBlockTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Base class for K8s blocks.
 *
 * Extend this class to access common the services.
 */
abstract class K8sBaseBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use K8sBlockTrait;

  public const ONE_YEAR = 365.25;

  public const DEFAULT_COST_TYPE = 'on_demand_yearly';

  public const DEFAULT_ITEMS_PER_PAGE = 50;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The page cache kill switch service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The cost fields renderer.
   *
   * @var \Drupal\k8s\Service\CostFieldsRendererInterface
   */
  protected $costFieldsRenderer;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Class Resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The K8s service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The K8s service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  protected $k8sService;

  /**
   * Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Pager manager object.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * Admin context object.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * The cloud store plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager
   */
  protected $cloudStorePluginManager;

  /**
   * Constructs a new K8sNodeHeatmapBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   The page cache kill switch service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\k8s\Service\CostFieldsRendererInterface $cost_fields_renderer
   *   The cost fields renderer.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   The K8s service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context.
   * @param \Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager $plugin_manager
   *   The cloud store plugin manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RouteMatchInterface $route_match,
    KillSwitch $kill_switch,
    ModuleHandlerInterface $module_handler,
    CostFieldsRendererInterface $cost_fields_renderer,
    Messenger $messenger,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    ConfigFactoryInterface $config_factory,
    UrlGeneratorInterface $url_generator,
    ClassResolverInterface $class_resolver,
    AccountInterface $current_user,
    CloudServiceInterface $cloud_service,
    K8sServiceInterface $k8s_service,
    RequestStack $request_stack,
    PagerManagerInterface $pager_manager,
    AdminContext $admin_context,
    CloudStorePluginManager $plugin_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
    $this->killSwitch = $kill_switch;
    $this->moduleHandler = $module_handler;
    $this->costFieldsRenderer = $cost_fields_renderer;
    $this->messenger = $messenger;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->configFactory = $config_factory;
    $this->urlGenerator = $url_generator;
    $this->classResolver = $class_resolver;
    $this->currentUser = $current_user;
    $this->cloudService = $cloud_service;
    $this->k8sService = $k8s_service;
    $this->request = $request_stack->getCurrentRequest();
    $this->pagerManager = $pager_manager;
    $this->adminContext = $admin_context;
    $this->cloudStorePluginManager = $plugin_manager;

    // Set the refresh flag.
    $this->refresh = $this->configFactory
      ->get('k8s.settings')
      ->get('k8s_update_pricing_data_cache');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('page_cache_kill_switch'),
      $container->get('module_handler'),
      $container->get('k8s.cost_fields_renderer'),
      $container->get('messenger'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('config.factory'),
      $container->get('url_generator'),
      $container->get('class_resolver'),
      $container->get('current_user'),
      $container->get('cloud'),
      $container->get('k8s'),
      $container->get('request_stack'),
      $container->get('pager.manager'),
      $container->get('router.admin_context'),
      $container->get('plugin.manager.cloud_store_plugin')
    );
  }

  /**
   * Check if Metrics server is enabled.
   *
   * @param string $cloud_context
   *   Cloud context to check.  If empty, check all K8s nodes.
   * @param string $message
   *   The message to show if no metrics servers are found.
   *
   * @return bool
   *   TRUE|FALSE whether metrics is enabled.
   */
  protected function isMetricsServerEnabled($cloud_context, $message): bool {
    $metrics_enabled = FALSE;
    if (!empty($cloud_context)) {
      $metrics_enabled = k8s_is_metrics_enabled($cloud_context);
    }
    else {
      try {
        // If no cloud_context passed, check all k8s cloud service providers.
        // If there is one match, return TRUE.
        $nodes = $this->loadEntities('k8s_node');
        foreach ($nodes ?: [] as $node) {
          if (k8s_is_metrics_enabled($node->getCloudContext()) === TRUE) {
            $metrics_enabled = TRUE;
            break;
          }
        }
      }
      catch (\Exception $e) {
        $this->handleException($e);
      }
    }

    if ($metrics_enabled === FALSE) {
      $this->messenger->addWarning($this->t('@message Install %metrics_server_link', [
        '@message' => $message,
        '%metrics_server_link' => $this->k8sService->getMetricsServerLink($cloud_context),
      ]));
    }
    return $metrics_enabled;
  }

  /**
   * Set a message that the cloud_context needs to be refreshed.
   *
   * @param string $cloud_context
   *   The cloud context used to build the message.
   * @param string $message
   *   Optional message to display to users.
   */
  protected function setUpdateMessage($cloud_context, $message): void {
    try {
      $this->cloudConfigPluginManager->setCloudContext($cloud_context);
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
      $update_link = Link::fromTextAndUrl(
        $this->t('Update Data'),
        Url::fromRoute(
          'k8s.update_all_resources',
          ['cloud_context' => $cloud_context]
        )
      )->toString();
      $cloud_config_link = Link::fromTextAndUrl(
        $cloud_config->getName(),
        Url::fromRoute(
        'entity.cloud_config.edit_form', [
          'cloud_config' => $cloud_config->id(),
        ]
      )
      )->toString();
      $this->messenger->addWarning($this->t('@message Click @page_link or update @cloud_service_provider.', [
        '@message' => $message,
        '@cloud_service_provider' => $cloud_config_link,
        '@page_link' => $update_link,
      ]));
    }
    catch (CloudConfigPluginException $e) {
      $this->messenger->addError($this->t('Cloud service provider @cloud_context not available.  Add @cloud_context as a @link', [
        '@cloud_context' => $cloud_context,
        '@link' => Link::fromTextAndUrl(
          $this->t('Cloud service provider'),
          Url::fromRoute(
           'entity.cloud_config.add_form', ['cloud_config_type' => 'k8s']
          )
        )->toString(),
      ]));
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
  }

  /**
   * Check if there are k8s entities in the system.
   *
   * @return bool
   *   True|False if there are k8s entities in the system.
   */
  public function isK8sEmpty(): bool {
    $entities = $this->cloudConfigPluginManager->loadConfigEntities('k8s');
    return empty($entities) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account): AccessResultInterface {
    $url = Url::createFromRequest($this->request);
    if ($account->isAnonymous() || !$url->access($account)) {
      return AccessResult::forbidden();
    }

    $route = $this->routeMatch->getRouteObject();
    $is_admin = $this->adminContext->isAdminRoute($route);
    if (!$this->configuration['display_in_admin_pages'] && $is_admin) {
      return AccessResult::forbidden();
    }

    $cloud_project = $this->routeMatch->getParameter('cloud_project');
    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $cloud_config_entities = $this->entityTypeManager->getStorage('cloud_config')->loadByProperties(
      ['cloud_context' => [$cloud_context]]
    );
    if (!empty($cloud_config_entities)) {
      $cloud_config = reset($cloud_config_entities);
    }

    if ($this->isK8sEmpty() === TRUE
      || (!empty($cloud_config) && $cloud_config->bundle() !== 'k8s')
      || (!empty($cloud_project) && $cloud_project->bundle() !== 'k8s')) {
      return AccessResult::forbidden();
    }

    if (empty($cloud_project) && empty($cloud_context)) {
      $cloud_config_entities = $this->loadEntities('cloud_config', ['type' => 'k8s']);
      if (empty($cloud_config_entities)) {
        return AccessResult::forbidden();
      }
    }

    if ($this->isNodeAccessCheckNeeded()) {
      $access = $this->blockNodeAccess($account);
      if ($access->isForbidden()) {
        return $access;
      }

    }
    return parent::blockAccess($account);
  }

  /**
   * Access check for the node related block.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user session for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  protected function blockNodeAccess(AccountInterface $account): AccessResultInterface {

    $cloud_project = $this->routeMatch->getParameter('cloud_project');
    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $k8s_node = $this->routeMatch->getParameter('k8s_node');
    $nodes = [];
    $cloud_contexts = [];
    if (!empty($k8s_node)) {
      if (!$k8s_node->access('view')) {
        return AccessResult::forbidden();
      }
    }
    elseif (!empty($cloud_project)) {
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }
    elseif (!empty($cloud_context)) {
      $cloud_contexts[] = $cloud_context;
    }
    try {
      $nodes = !empty($cloud_contexts)
        ? $this->loadEntities('k8s_node', [
          'cloud_context' => $cloud_contexts,
        ])
        : $this->loadEntities('k8s_node');

      if (empty($nodes)) {
        return AccessResult::forbidden();
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $form['block_description'] = [
      '#markup' => '<div class="alert alert-info">' . $this->t('NOTE: This block is not displayed if a user does not have a permission to view the entities on this block.') . '</div>',
      '#weight' => 10,
    ];

    $form['display_in_admin_pages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this block in admin pages'),
      '#default_value' => $this->configuration['display_in_admin_pages'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {

    $this->configuration['display_in_admin_pages']
      = $form_state->getValue('display_in_admin_pages');

  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'display_in_admin_pages' => '0',
    ];
  }

  /**
   * Check if node access check is needed.
   *
   * @return bool
   *   True|False if node access check is needed.
   */
  public function isNodeAccessCheckNeeded(): bool {
    return FALSE;
  }

  /**
   * Get duration list.
   *
   * @return array
   *   The duration list.
   */
  protected function getDuration(): array {
    return [
      'none' => $this->t('- None -'),
      'daily' => $this->t('Daily'),
      'monthly' => $this->t('Monthly'),
      'yearly' => $this->t('Yearly'),
      '3years' => $this->t('3 years'),
    ];
  }

  /**
   * Get converted cost.
   *
   * @param float $cost
   *   The cost to convert.
   * @param string $cost_type
   *   The cost type.
   * @param string $duration
   *   The duration.
   *
   * @return float
   *   The converted cost.
   */
  protected function getConvertedCost($cost, $cost_type, $duration = 'none'): float {

    if ($duration === 'daily'
      && $cost_type !== 'on_demand_daily') {
      if ($cost_type === 'on_demand_hourly') {
        $cost *= 24;
      }
      elseif ($cost_type === 'on_demand_monthly') {
        $cost /= (self::ONE_YEAR / 12);
      }
      elseif ($cost_type === 'on_demand_yearly' || $cost_type === 'ri_one_year') {
        $cost /= self::ONE_YEAR;
      }
      elseif ($cost_type === 'ri_three_year') {
        $cost /= (self::ONE_YEAR * 3);
      }
    }
    if ($duration === 'monthly'
      && $cost_type !== 'on_demand_monthly') {
      if ($cost_type === 'on_demand_hourly') {
        $cost *= (24 * self::ONE_YEAR / 12);
      }
      elseif ($cost_type === 'on_demand_daily') {
        $cost *= self::ONE_YEAR / 12;
      }
      elseif ($cost_type === 'on_demand_yearly' || $cost_type === 'ri_one_year') {
        $cost /= 12;
      }
      elseif ($cost_type === 'ri_three_year') {
        $cost /= (12 * 3);
      }
    }
    if ($duration === 'yearly'
      && $cost_type !== 'on_demand_yearly'
      && $cost_type !== 'ri_one_year') {
      if ($cost_type === 'on_demand_hourly') {
        $cost *= (24 * self::ONE_YEAR);
      }
      elseif ($cost_type === 'on_demand_daily') {
        $cost *= self::ONE_YEAR;
      }
      elseif ($cost_type === 'on_demand_monthly') {
        $cost *= 12;
      }
      elseif ($cost_type === 'ri_three_year') {
        $cost /= 3;
      }
    }
    if ($duration === '3years'
      && $cost_type !== 'ri_three_year') {
      if ($cost_type === 'on_demand_hourly') {
        $cost *= (24 * self::ONE_YEAR * 3);
      }
      elseif ($cost_type === 'on_demand_daily') {
        $cost *= (self::ONE_YEAR * 3);
      }
      elseif ($cost_type === 'on_demand_monthly') {
        $cost *= (12 * 3);
      }
      elseif ($cost_type === 'on_demand_yearly' || $cost_type === 'ri_one_year') {
        $cost *= 3;
      }
    }
    return $cost;
  }

  /**
   * Get default cost type.
   *
   * @param array $cloud_contexts
   *   The cloud contexts.
   *
   * @return string
   *   The default cost type.
   */
  protected function getDefaultCostType(array $cloud_contexts): string {
    $default_cost_type = '';
    $cloud_configs = [];
    if (!empty($cloud_contexts)) {
      foreach ($cloud_contexts ?: [] as $cluster) {
        $this->cloudConfigPluginManager->setCloudContext($cluster);
        $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
        $cloud_configs[] = $cloud_config;
      }
    }
    else {
      $cloud_configs = $this->cloudConfigPluginManager->loadConfigEntities('k8s');
    }
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $value = $cloud_config->get('field_cost_type')->value;
      $default_cost_type = empty($value)
        || (!empty($default_cost_type) && ($value !== $default_cost_type))
        ? self::DEFAULT_COST_TYPE
        : $value;
    }

    return $default_cost_type;
  }

}
