<?php

namespace Drupal\k8s\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\k8s\Controller\K8sCostsControllerBase;

/**
 * Provides a block of the costs.
 *
 * @Block(
 *   id = "k8s_namespace_costs",
 *   admin_label = @Translation("K8s namespace costs"),
 *   category = @Translation("K8s")
 * )
 */
class K8sNamespaceCostsBlock extends K8sBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return (parent::defaultConfiguration() ?: []) + [
      'display_view_k8s_namespace_list_only' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $form['display_view_k8s_namespace_list_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this block in K8s namespace list page and K8s Project Content page only'),
      '#default_value' => $this->configuration['display_view_k8s_namespace_list_only'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);

    $this->configuration['display_view_k8s_namespace_list_only']
      = $form_state->getValue('display_view_k8s_namespace_list_only');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    // If aws_cloud module is not enabled; do nothing.
    if (!$this->moduleHandler->moduleExists('aws_cloud')) {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('This block requires the AWS Cloud
        module to be enabled.'),
      ];
    }

    // If display_view_k8s_namespace_list_only is checked, do nothing.
    $route_name = $this->routeMatch->getRouteName();
    if ($route_name !== 'view.k8s_namespace.list'
      && $route_name !== 'entity.cloud_project.canonical'
      && $this->configuration['display_view_k8s_namespace_list_only']) {
      return [];
    }

    $nodes = [];
    $namespaces = [];
    $cloud_contexts = [];

    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $cloud_project = $this->routeMatch->getParameter('cloud_project');

    if (!empty($cloud_project)) {
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }
    elseif (!empty($cloud_context)) {
      $cloud_contexts[] = $cloud_context;
    }

    // If cloud_context is passed, filter entities with it.
    // Else load all nodes and namespaces.
    $nodes = !empty($cloud_contexts)
      ? $this->loadEntities('k8s_node', [
        'cloud_context' => $cloud_contexts,
      ], FALSE)
      : $this->loadEntities('k8s_node', [], FALSE);

    $namespaces = !empty($cloud_contexts)
      ? $this->loadEntities('k8s_namespace', [
        'cloud_context' => $cloud_contexts,
      ])
      : $this->loadEntities('k8s_namespace');

    // If nodes or namespaces are empty, ask the user
    // to try updating the cloud_context to bring in
    // the latest data.
    if (!empty($cloud_contexts) && (empty($nodes) || empty($namespaces))) {
      $message = $this->t('Nodes and/or namespaces not found for K8s namespace cost block.');
      $this->setUpdateMessage($cloud_context, $message);
    }

    if (empty($nodes) || empty($namespaces)) {
      return [];
    }

    $build = [];

    $block_id = Html::getUniqueId($this->getPluginId());
    $params = $this->request->get($block_id, []);

    $controller = $this->classResolver->getInstanceFromDefinition(K8sCostsControllerBase::class);
    $cost_type_list = $controller->getEc2CostTypes();

    $default_cost_type = !empty($params['cost_type'])
      ? $params['cost_type']
      : $this->getDefaultCostType($cloud_contexts);

    $cost_type = array_key_exists($default_cost_type, $cost_type_list)
      ? $default_cost_type
      : self::DEFAULT_COST_TYPE;

    $total_costs = $this->k8sService->getTotalCosts($nodes, $cost_type, $this->refresh);

    // $cpu_capacity = $this->getCpuCapacity($nodes);
    $cpu_capacity = array_sum(array_map(static function ($node) {
      return $node->getCpuCapacity();
    }, $nodes));

    $memory_capacity = array_sum(array_map(static function ($node) {
      return $node->getMemoryCapacity();
    }, $nodes));

    $pod_capacity = array_sum(array_map(static function ($node) {
      return $node->getPodsCapacity();
    }, $nodes));

    // Get row data.
    $rows = [];
    $precision = $cost_type === 'on_demand_hourly' ? 4 : 2;
    foreach ($namespaces ?: [] as $namespace) {
      $pods = $this->loadEntities('k8s_pod', [
        'cloud_context' => $namespace->getCloudContext(),
        'namespace' => $namespace->getName(),
      ], FALSE);

      $row = [];
      if (!empty($cloud_project)) {
        $this->cloudConfigPluginManager->setCloudContext($namespace->getCloudContext());
        $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
        $route = $this->cloudConfigPluginManager->getInstanceCollectionTemplateName();

        $row['k8s_cluster'] = [
          'data' => [
            '#type' => 'link',
            '#url' => Url::fromRoute($route, ['cloud_context' => $namespace->getCloudContext()]),
            '#title' => $cloud_config->getName(),
          ],
        ];
      }

      $row['namespace'] = [
        'data' => [
          '#type' => 'link',
          '#title' => $namespace->getName(),
          '#url' => Url::fromRoute(
            'entity.k8s_namespace.canonical',
            [
              'cloud_context' => $namespace->getCloudContext(),
              'k8s_namespace' => $namespace->id(),
            ]
          ),
        ],
      ];

      $cpu_usage = array_sum(array_map(static function ($pod) {
        return $pod->getCpuUsage();
      }, $pods));

      $memory_usage = array_sum(array_map(static function ($pod) {
        return $pod->getMemoryUsage();
      }, $pods));

      $pod_usage = count($pods);

      $cpu_usage_ratio = $cpu_capacity > 0 ? $cpu_usage / $cpu_capacity : 0;
      $memory_capacity_ratio = $memory_capacity > 0 ? $memory_usage / $memory_capacity : 0;
      $pod_capacity_ratio = $pod_capacity > 0 ? $pod_usage / $pod_capacity : 0;
      $costs = $total_costs > 0 ? ($cpu_usage_ratio + $memory_capacity_ratio + $pod_capacity_ratio) / 3 * $total_costs : 0;

      $row['costs'] = [
        'data' => $this->k8sService->formatCosts($costs, $total_costs, $precision),
        'value' => $costs,
      ];
      $row['cpu_usage'] = [
        'data' => $this->k8sService->formatCpuUsage($cpu_usage, $cpu_capacity),
        'value' => $cpu_usage,
      ];
      $row['memory_usage'] = [
        'data' => $this->k8sService->formatMemoryUsage($memory_usage, $memory_capacity),
        'value' => $memory_usage,
      ];
      $row['pod_usage'] = [
        'data' => $this->k8sService->formatPodUsage($pod_usage, $pod_capacity),
        'value' => $pod_usage,
      ];

      $rows[] = ['data' => $row];
    }

    $dynamic_headers = [];
    $headers = [];

    if (!empty($cloud_project)) {
      $dynamic_headers['k8s_cluster'] = $this->t('K8s cluster');
    }

    $static_headers = [
      ['data' => $this->t('Namespace')],
      ['data' => $this->t('Total costs ($)')],
      ['data' => $this->t('CPU (Usage)')],
      ['data' => $this->t('Memory (Usage)')],
      ['data' => $this->t('Pods allocation')],
    ];

    $headers = array_merge($dynamic_headers, $static_headers);

    $items_per_page_options = cloud_get_views_items_options();
    $items_per_page_options['All'] = '- All -';

    $items_per_page
      = !empty($params['items_per_page'])
        && array_key_exists($params['items_per_page'], $items_per_page_options)
        ? $params['items_per_page'] : self::DEFAULT_ITEMS_PER_PAGE;

    $current_rows_chunk = $rows;
    if ($items_per_page !== 'All') {
      // Set pager's ID as 10.
      $current_page = $this->pagerManager
        ->createPager(count($rows), $items_per_page, 10)
        ->getCurrentPage();
      $rows_chunks = array_chunk($rows, $items_per_page);
      $current_rows_chunk = !empty($rows_chunks) ? $rows_chunks[$current_page] : [];
    }

    $table = [
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $current_rows_chunk,
      '#attributes' => [
        'block_id' => $block_id,
      ],
    ];

    $build['k8s_namespace_costs'] = [
      '#type' => 'details',
      '#title' => $this->t('Namespace costs') . " ($cost_type_list[$cost_type])",
      '#open' => TRUE,
      '#id' => $block_id,
      // Attach DataTables.
      '#attached' => [
        'library' => [
          'cloud/datatables',
        ],
      ],
    ];
    // Add the datatable class.
    $build['k8s_namespace_costs']['#attributes']['class'][] = 'simple-datatable';

    $build['k8s_namespace_costs']['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form--inline', 'form-inline', 'clearfix'],
      ],
    ];

    $build['k8s_namespace_costs']['container']['items_per_page'] = [
      '#type' => 'select',
      '#name' => 'items_per_page',
      '#title' => $this->t('Items per page'),
      '#value' => $items_per_page,
      '#options' => $items_per_page_options,
    ];

    $build['k8s_namespace_costs']['container']['cost_type'] = [
      '#type' => 'select',
      '#name' => 'cost_type',
      '#title' => $this->t('Cost type'),
      '#value' => $cost_type,
      '#options' => $cost_type_list,
    ];

    $build['k8s_namespace_costs']['container']['apply'] = [
      '#type' => 'button',
      '#name' => 'apply',
      '#value' => $this->t('Apply'),
    ];

    $build['k8s_namespace_costs']['table'] = $table;
    if ($items_per_page !== 'All') {
      $build['k8s_namespace_costs']['pager'] = [
        '#type' => 'pager',
        '#element' => 10,
        '#tags' => [
          $this->t('« First'),
          $this->t('‹‹'),
          '',
          $this->t('››'),
          $this->t('Last »'),
        ],
      ];
    }

    $build['#attached']['library'][] = 'k8s/k8s_cost_block';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): bool {
    $this->killSwitch->trigger();
    // BigPipe/#cache/max-age is breaking my block javascript
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2016-07-17/bigpipecachemax-age-is-breaking-my
    // "a slight delay of a second or two before the charts are built.
    // That seems to work, though it is a junky solution.".
    return $this->moduleHandler->moduleExists('big_pipe') ? 1 : 0;
  }

}
