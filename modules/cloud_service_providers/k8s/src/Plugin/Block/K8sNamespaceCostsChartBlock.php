<?php

namespace Drupal\k8s\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\k8s\Controller\K8sCostsControllerBase;
use Drupal\k8s\Controller\K8sNamespaceCostsChartController;

/**
 * Provides a block of the costs chart.
 *
 * @Block(
 *   id = "k8s_namespace_costs_chart",
 *   admin_label = @Translation("K8s namespace costs chart"),
 *   category = @Translation("K8s")
 * )
 */
class K8sNamespaceCostsChartBlock extends K8sBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return (parent::defaultConfiguration() ?: []) + [
      'display_view_k8s_namespace_list_only' => 1,
      'aws_cloud_chart_period' => 14,
      'chart_type' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $controller = $this->classResolver->getInstanceFromDefinition(K8sNamespaceCostsChartController::class);

    $form['display_view_k8s_namespace_list_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this block in K8s namespace list page and K8s Project Content page only'),
      '#default_value' => $this->configuration['display_view_k8s_namespace_list_only'],
    ];

    $form['aws_cloud_chart_period'] = [
      '#type' => 'select',
      '#title' => $this->t('AWS Cloud chart period'),
      '#options' => $controller->getEc2ChartPeriod(),
      '#default_value' => $this->configuration['aws_cloud_chart_period'],
    ];

    $form['chart_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Chart Type'),
      '#default_value' => $this->configuration['chart_type'] ?: 0,
      '#options' => [
        0 => $this->t('Line chart'),
        1 => $this->t('Horizon chart'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $this->configuration['display_view_k8s_namespace_list_only']
      = $form_state->getValue('display_view_k8s_namespace_list_only');
    $this->configuration['aws_cloud_chart_period']
      = $form_state->getValue('aws_cloud_chart_period');
    $this->configuration['chart_type']
      = $form_state->getValue('chart_type');
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account): AccessResultInterface {
    $access = parent::blockAccess($account);

    if ($access->isForbidden()) {
      return $access;
    }
    $cloud_project = $this->routeMatch->getParameter('cloud_project');
    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $k8s_namespace = $this->routeMatch->getParameter('k8s_namespace');
    $nodes = [];
    $namespaces = [];
    $cloud_contexts = [];
    if (!empty($k8s_namespace)) {
      if (!$k8s_namespace->access('view')) {
        return AccessResult::forbidden();
      }
    }
    elseif (!empty($cloud_project)) {
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }
    elseif (!empty($cloud_context)) {
      $cloud_contexts[] = $cloud_context;
    }
    try {
      $nodes = !empty($cloud_contexts)
        ? $this->loadEntities('k8s_node', [
          'cloud_context' => $cloud_contexts,
        ], FALSE)
        : $this->loadEntities('k8s_node', [], FALSE);

      $namespaces = !empty($cloud_contexts)
        ? $this->loadEntities('k8s_namespace', [
          'cloud_context' => $cloud_contexts,
        ])
        : $this->loadEntities('k8s_namespace');

      if (empty($nodes) || empty($namespaces)) {
        return AccessResult::forbidden();
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $access;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $route_name = $this->routeMatch->getRouteName();
    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $cloud_project = $this->routeMatch->getParameter('cloud_project');

    // If display_view_k8s_namespace_list_only is checked, do nothing.
    if ($route_name !== 'view.k8s_namespace.list'
      && $route_name !== 'entity.cloud_project.canonical'
      && $this->configuration['display_view_k8s_namespace_list_only']) {
      return [];
    }

    $k8s_namespace_costs_cost_types_json_url = Url::fromRoute(
      'entity.k8s_namespace.cost_types')->toString();

    $k8s_namespace_costs_chart_periods_json_url = Url::fromRoute(
      'entity.k8s_namespace.all_chart_periods')->toString();

    // Use a different chart_periods endpoint
    // if cloud_context or cloud_project is present.
    if (!empty($cloud_project)) {
      $k8s_namespace_costs_chart_periods_json_url = Url::fromRoute(
      'entity.k8s_namespace.cloud_project.chart_periods',
        [
          'cloud_context' => $cloud_context,
          'cloud_project' => $cloud_project->id(),
        ]
      )->toString();
    }
    elseif (!empty($cloud_context)) {
      if ($route_name === 'entity.k8s_namespace.canonical') {
        $k8s_namespace = $this->routeMatch->getParameter('k8s_namespace');
        $k8s_namespace_costs_chart_periods_json_url = Url::fromRoute(
        'entity.k8s_namespace.chart_periods',
        [
          'cloud_context' => $cloud_context,
          'name' => $k8s_namespace->getName(),
        ]
        )->toString();
      }
      else {
        $k8s_namespace_costs_chart_periods_json_url = Url::fromRoute(
          'entity.k8s_namespace.cloud_context.chart_periods',
          [
            'cloud_context' => $cloud_context,
          ]
        )->toString();
      }
    }

    $build = [];
    $fieldset_defs = [
      [
        'name' => 'k8s_namespace_costs',
        'title' => $this->t('Namespace costs chart'),
        'open' => TRUE,
        'fields' => [
          'k8s_namespace_costs_chart',
        ],
      ],
    ];

    if (!empty($cloud_project)) {
      $json_url = Url::fromRoute(
      'entity.k8s_namespace.cloud_project.costs',
        [
          'cloud_context' => $cloud_context,
          'cloud_project' => $cloud_project->id(),
        ]
      )->toString();
    }
    elseif (!empty($cloud_context)) {
      if ($route_name === 'entity.k8s_namespace.canonical') {
        $k8s_namespace = $this->routeMatch->getParameter('k8s_namespace');
        $json_url = Url::fromRoute(
          'entity.k8s_namespace.cost',
          [
            'cloud_context' => $cloud_context,
            'k8s_namespace' => $k8s_namespace->id(),
          ]
        )->toString();
      }
      else {
        $json_url = Url::fromRoute(
          'entity.k8s_namespace.costs',
          [
            'cloud_context' => $cloud_context,
          ]
        )->toString();
      }
    }
    else {
      $json_url = Url::fromRoute('entity.k8s_namespace.all_costs')->toString();
    }

    $controller = $this->classResolver->getInstanceFromDefinition(K8sCostsControllerBase::class);
    $cost_type_list = $controller->getEc2CostTypes();

    $cloud_contexts = [];
    if (!empty($cloud_project)) {
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }
    elseif (!empty($cloud_context)) {
      $cloud_contexts[] = $cloud_context;
    }

    $default_cost_type = $this->getDefaultCostType($cloud_contexts);

    $cost_type = array_key_exists($default_cost_type, $cost_type_list)
      ? $default_cost_type
      : self::DEFAULT_COST_TYPE;

    $build = [];
    $library = empty($this->configuration['chart_type'])
      ? 'k8s/k8s_namespace_costs_chart'
      : 'k8s/k8s_namespace_costs_horizon_chart';

    $build['k8s_namespace_costs_chart'] = [
      '#markup' => '<div id="k8s_namespace_costs_chart"></div>',
      '#attached' => [
        'library' => [$library] ,
        'drupalSettings' => [
          'k8s' => [
            'k8s_js_refresh_interval' => $this->configFactory->get('k8s.settings')
              ->get('k8s_js_refresh_interval'),
            'k8s_namespace_costs_chart_initial_data' => $this->buildInitialDataSet($cloud_project, $cloud_context, $route_name),
            'k8s_namespace_costs_chart_json_url' => $json_url,
            'k8s_namespace_costs_cost_types_json_url' => $k8s_namespace_costs_cost_types_json_url,
            'k8s_namespace_costs_chart_periods_json_url' => $k8s_namespace_costs_chart_periods_json_url,
            'k8s_namespace_costs_chart_ec2_cost_type' => $cost_type,
            'k8s_namespace_costs_chart_period' => $this->configuration['aws_cloud_chart_period'],
          ],
        ],
      ],
    ];

    $this->cloudService->reorderForm($build, $fieldset_defs);

    return $build;
  }

  /**
   * Helper function to build initial data set.
   *
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   * @param string $cloud_context
   *   Cloud context string.
   * @param string $route_name
   *   The route name.
   *
   * @return array
   *   Array of initial data.
   */
  private function buildInitialDataSet(?CloudProjectInterface $cloud_project = NULL, $cloud_context = '', $route_name = ''): array {
    $entities = [];

    if (!empty($cloud_project) && !empty($cloud_context)) {
      $entities = $this->loadK8sNamespaceEntities($cloud_context, $cloud_project);
    }
    elseif (!empty($cloud_context)) {
      if ($route_name === 'entity.k8s_namespace.canonical') {
        $k8s_namespace = $this->routeMatch->getParameter('k8s_namespace');
        $entities = $this->loadK8sNamespaceEntities($cloud_context, $cloud_project, $k8s_namespace);
      }
      else {
        $entities = $this->loadK8sNamespaceEntities($cloud_context);
      }
    }
    else {
      // This means the block is on a generic page.
      $entities['namespaces'] = $this->getEntitiesByType('k8s_namespace');
    }

    return $this->generateNamespaceCosts($entities['namespaces'] ?? [], !empty($cloud_project) ? NULL : $cloud_context, self::DEFAULT_COST_TYPE, $this->configuration['aws_cloud_chart_period']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): bool {
    $this->killSwitch->trigger();
    // BigPipe/#cache/max-age is breaking my block javascript
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2016-07-17/bigpipecachemax-age-is-breaking-my
    // "a slight delay of a second or two before the charts are built.
    // That seems to work, though it is a junky solution.".
    return $this->moduleHandler->moduleExists('big_pipe') ? 1 : 0;
  }

}
