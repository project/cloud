<?php

namespace Drupal\k8s\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\k8s\Entity\K8sNodeInterface;

/**
 * Provides a block of the allocated resources at a Node.
 *
 * @Block(
 *   id = "k8s_node_allocated_resources",
 *   admin_label = @Translation("K8s node allocated resources"),
 *   category = @Translation("K8s")
 * )
 */
class K8sNodeAllocatedResourcesBlock extends K8sBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return (parent::defaultConfiguration() ?: []) + [
      'display_view_k8s_node_list_only' => 1,
      'use_event_listener' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $form['display_view_k8s_node_list_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this block in K8s node detail/list page and K8s Project Content page only'),
      '#default_value' => $this->configuration['display_view_k8s_node_list_only'] ?? 1,
    ];
    $form['use_event_listener'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use a common JS resource URL.'),
      '#description' => $this->t('Check to use a common JS resource URL to update this block on dashboard.
      Useful if this block is placed on a page along with K8s node heatmap block.  Reduces duplicate AJAX calls to
      K8s resource URL.'),
      '#default_value' => $this->configuration['use_event_listener'] ?? TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $this->configuration['display_view_k8s_node_list_only']
      = $form_state->getValue('display_view_k8s_node_list_only');
    $this->configuration['use_event_listener']
      = $form_state->getValue('use_event_listener');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function build(): array {

    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    if (!empty($cloud_context) && !CloudConfig::checkCloudContext($cloud_context, 'k8s')) {
      return [];
    }

    // If display_view_k8s_node_list_only is checked, do nothing.
    $route_name = $this->routeMatch->getRouteName();
    if ($route_name !== 'view.k8s_node.list'
      && $route_name !== 'entity.k8s_node.canonical'
      && $route_name !== 'entity.cloud_project.canonical'
      && $this->configuration['display_view_k8s_node_list_only']) {
      return [];
    }

    $cloud_project = $this->routeMatch->getParameter('cloud_project');
    $k8s_node = $this->routeMatch->getParameter('k8s_node');

    $metrics_enabled = FALSE;
    if (!empty($cloud_project)) {
      // Confirm whether the metrics API can be used or not.
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $this->cloudConfigPluginManager->setCloudContext($k8s_cluster['value']);
        $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
        $message = count($k8s_clusters) > 1
          ? $this->t('Node allocated resources block requires K8s Metrics Server for %cloud_config.', [
            '%cloud_config' => $cloud_config->toLink()->toString(),
          ])
          : $this->t('Node allocated resources block requires K8s Metrics Server.');
        if ($this->isMetricsServerEnabled($k8s_cluster['value'] ?? '', $message)) {
          $metrics_enabled = TRUE;
        }
      }
    }
    else {
      // The following $cloud_context is reference to view.k8s_node.list, not
      // entity.k8s_node.collection in out case.  See also:
      // ...
      // $this->derivatives[$id]['route_name'] = 'view.k8s_node.list';
      //
      // _OR_
      //
      // K8sLocalTask::getDerivativeDefinitions($base_plugin_definition) {
      // ...
      // $this->derivatives[$id]['route_name'] = 'view.k8s_node.list';
      // ...
      // Confirm whether the metrics API can be used or not.
      // This method handles cases where there are no cloud_context passed.
      // This usually happens when the block is called from a dashboard page.
      $metrics_enabled = $this->isMetricsServerEnabled(
        $cloud_context ?? '',
        $this->t('Node allocated resources block requires K8s Metrics Server.')
      );
    }

    $fieldset_defs = [
      [
        'name' => 'k8s_allocated_resources',
        'title' => $this->t('Allocated Resources'),
        'open' => TRUE,
        'fields' => [
          'k8s_node_allocated_resources',
        ],
      ],
    ];

    $build = [];

    $build['k8s_node_allocated_resources'] = [
      '#markup' => '<div id="k8s_node_allocated_resources"></div>',
      '#attached' => [
        'library' => [
          'k8s/k8s_node_allocated_resources',
        ],
        'drupalSettings' => [
          'k8s' => [
            'k8s_js_refresh_interval' => $this->configFactory->get('k8s.settings')
              ->get('k8s_js_refresh_interval'),
            'metrics_enabled' => $metrics_enabled,
            'resource_url' => k8s_get_resource_url(),
            'initial_data' => $this->buildInitialDataSet($cloud_context, !empty($cloud_project) ? $cloud_project : NULL, !empty($k8s_node) ? $k8s_node : NULL),
            'use_event_listener' => FALSE,
          ],
        ],
      ],
    ];

    // Add refresh js that uses Event Trigger method of updating the block.
    if ($this->configuration['use_event_listener'] ?? TRUE) {
      $build['k8s_node_allocated_resources']['#attached']['library'][] = 'k8s/k8s_refresh_resource_trigger';
      $build['k8s_node_allocated_resources']['#attached']['drupalSettings']['k8s']['use_event_listener'] = TRUE;
    }

    $this->cloudService->reorderForm($build, $fieldset_defs);

    return $build;
  }

  /**
   * Build the initial data set for the allocated resource block.
   *
   * @param string $cloud_context
   *   The cloud context to use.
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   * @param \Drupal\k8s\Entity\K8sNodeInterface $k8s_node
   *   The k8s node.
   *
   * @return array
   *   Array of nodes.
   */
  private function buildInitialDataSet($cloud_context, ?CloudProjectInterface $cloud_project = NULL, ?K8sNodeInterface $k8s_node = NULL): array {
    if (empty($cloud_context)) {
      $response = $this->loadAllNodeAllocatedResources();
    }
    else {
      $response = $this->loadNodeAllocatedResources($cloud_context, $cloud_project, $k8s_node);
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function isNodeAccessCheckNeeded(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): bool {
    $this->killSwitch->trigger();
    // BigPipe/#cache/max-age is breaking my block javascript
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2016-07-17/bigpipecachemax-age-is-breaking-my
    // "a slight delay of a second or two before the charts are built.
    // That seems to work, though it is a junky solution.".
    return $this->moduleHandler->moduleExists('big_pipe') ? 1 : 0;
  }

}
