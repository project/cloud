<?php

namespace Drupal\k8s\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\k8s\Controller\K8sNodeAllocatedResourcesChartController;

/**
 * Provides a block of the allocated resources chart at Nodes.
 *
 * @Block(
 *   id = "k8s_node_allocated_resources_chart",
 *   admin_label = @Translation("K8s node allocated resources chart"),
 *   category = @Translation("K8s")
 * )
 */
class K8sNodeAllocatedResourcesChartBlock extends K8sBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return (parent::defaultConfiguration() ?: []) + [
      'display_view_k8s_node_list_only' => 1,
      'chart_type' => 0,
      'chart_period' => 14,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $controller = $this->classResolver->getInstanceFromDefinition(K8sNodeAllocatedResourcesChartController::class);

    $form['display_view_k8s_node_list_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this block in K8s node list page and K8s Project Content page only'),
      '#default_value' => $this->configuration['display_view_k8s_node_list_only'],
    ];

    $form['chart_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Chart Type'),
      '#default_value' => $this->configuration['chart_type'] ?: 0,
      '#options' => [
        0 => $this->t('Line Chart'),
        1 => $this->t('Horizon Area Chart'),
      ],
    ];

    $form['chart_period'] = [
      '#type' => 'select',
      '#title' => $this->t('Default chart period'),
      '#options' => $controller->getEc2ChartPeriod(),
      '#default_value' => $this->configuration['chart_period'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $this->configuration['display_view_k8s_node_list_only']
      = $form_state->getValue('display_view_k8s_node_list_only');
    $this->configuration['chart_period']
      = $form_state->getValue('chart_period');
    $this->configuration['chart_type']
      = $form_state->getValue('chart_type');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $route_name = $this->routeMatch->getRouteName();
    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $cloud_project = $this->routeMatch->getParameter('cloud_project');

    // If display_view_k8s_node_list_only is checked, do nothing.
    if ($route_name !== 'view.k8s_node.list'
      && $route_name !== 'entity.cloud_project.canonical'
      && $this->configuration['display_view_k8s_node_list_only']) {
      return [];
    }

    $k8s_allocated_resources_chart_periods_json_url = Url::fromRoute(
      'entity.k8s_node.all_chart_periods')->toString();

    // Use a different chart_periods endpoint
    // if cloud_context or cloud_project is present.
    if (!empty($cloud_project)) {
      $k8s_allocated_resources_chart_periods_json_url = Url::fromRoute(
      'entity.k8s_node.cloud_project.chart_periods',
        [
          'cloud_context' => $cloud_context,
          'cloud_project' => $cloud_project->id(),
        ]
      )->toString();
    }
    elseif (!empty($cloud_context)) {
      if ($route_name === 'entity.k8s_node.canonical') {
        $k8s_node = $this->routeMatch->getParameter('k8s_node');
        $k8s_allocated_resources_chart_periods_json_url = Url::fromRoute(
        'entity.k8s_node.chart_periods',
        [
          'cloud_context' => $cloud_context,
          'name' => $k8s_node->getName(),
        ]
        )->toString();
      }
      else {
        $k8s_allocated_resources_chart_periods_json_url = Url::fromRoute(
          'entity.k8s_node.cloud_context.chart_periods',
          [
            'cloud_context' => $cloud_context,
          ]
        )->toString();
      }
    }

    $build = [];
    $fieldset_defs = [
      [
        'name' => 'k8s_allocated_resources_chart',
        'title' => $this->t('Node allocated resources chart'),
        'open' => TRUE,
        'fields' => [
          'k8s_node_allocated_resources_chart',
        ],
      ],
    ];

    if (!empty($cloud_project)) {
      $json_url = Url::fromRoute(
      'entity.k8s_node.cloud_project.allocated_resources',
        [
          'cloud_context' => $cloud_context,
          'cloud_project' => $cloud_project->id(),
        ]
      )->toString();
    }
    elseif (!empty($cloud_context)) {
      if ($route_name === 'entity.k8s_node.canonical') {
        $k8s_node = $this->routeMatch->getParameter('k8s_node');
        $json_url = Url::fromRoute(
          'entity.k8s_node.allocated_resource',
          [
            'cloud_context' => $cloud_context,
            'k8s_node' => $k8s_node->id(),
          ]
        )->toString();
      }
      else {
        $json_url = Url::fromRoute(
          'entity.k8s_node.allocated_resources',
          [
            'cloud_context' => $cloud_context,
          ]
        )->toString();
      }
    }
    else {
      $json_url = Url::fromRoute('entity.k8s_node.all_allocated_resources')->toString();
    }

    $build = [];

    $build['k8s_node_allocated_resources_chart'] = [
      '#markup' => '<div id="k8s_node_allocated_resources_chart"></div>',
      '#attached' => [
        'library' => [
          $this->configuration['chart_type']
            ? 'k8s/k8s_node_allocated_resources_horizon_chart'
            : 'k8s/k8s_node_allocated_resources_chart',
        ],
        'drupalSettings' => [
          'k8s' => [
            'k8s_node_allocated_resources_chart_json_url' => $json_url,
            'k8s_node_allocated_resources_chart_periods_json_url' => $k8s_allocated_resources_chart_periods_json_url,
            'k8s_node_allocated_resources_chart_period' => $this->configuration['chart_period'],
          ],
        ],
      ],
    ];

    $this->cloudService->reorderForm($build, $fieldset_defs);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function isNodeAccessCheckNeeded(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): bool {
    $this->killSwitch->trigger();
    // BigPipe/#cache/max-age is breaking my block javascript
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2016-07-17/bigpipecachemax-age-is-breaking-my
    // "a slight delay of a second or two before the charts are built.
    // That seems to work, though it is a junky solution.".
    return $this->moduleHandler->moduleExists('big_pipe') ? 1 : 0;
  }

}
