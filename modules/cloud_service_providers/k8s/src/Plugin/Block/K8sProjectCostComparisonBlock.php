<?php

namespace Drupal\k8s\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\k8s\Controller\K8sCostsControllerBase;

/**
 * Provides a block of the cost comparison for cloud project.
 *
 * @Block(
 *   id = "k8s_project_cost_comparison",
 *   admin_label = @Translation("K8s Project Cost Comparison"),
 *   category = @Translation("K8s")
 * )
 */
class K8sProjectCostComparisonBlock extends K8sBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return (parent::defaultConfiguration() ?: []) + [
      'display_k8s_cloud_project_page_only' => 1,
      'display_cluster_in_project_pages_only' => 1,
      'duration' => 'none',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $form['display_k8s_cloud_project_page_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this block in K8s Project Content page only'),
      '#default_value' => $this->configuration['display_k8s_cloud_project_page_only'],
    ];

    $form['display_cluster_in_project_pages_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display K8s cluster information in K8s Project Content page only'),
      '#default_value' => $this->configuration['display_cluster_in_project_pages_only'],
    ];

    $form['duration'] = [
      '#type' => 'select',
      '#title' => $this->t('Duration'),
      '#options' => $this->getDuration(),
      '#default_value' => $this->configuration['duration'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);

    $this->configuration['display_k8s_cloud_project_page_only']
      = $form_state->getValue('display_k8s_cloud_project_page_only');
    $this->configuration['display_cluster_in_project_pages_only']
      = $form_state->getValue('display_cluster_in_project_pages_only');
    $this->configuration['duration']
      = $form_state->getValue('duration');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    // If aws_cloud module is not enabled; do nothing.
    if (!$this->moduleHandler->moduleExists('aws_cloud')) {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('This block requires the AWS Cloud
        module to be enabled.'),
      ];
    }

    $route_name = $this->routeMatch->getRouteName();
    $cloud_project = $this->routeMatch->getParameter('cloud_project');
    $cloud_context = $this->routeMatch->getParameter('cloud_context');

    // If display_view_k8s_namespace_list_only is checked, do nothing.
    if ($route_name !== 'entity.cloud_project.canonical'
      && $this->configuration['display_k8s_cloud_project_page_only']) {
      return [];
    }

    $nodes = [];
    $cloud_contexts = [];
    $cloud_projects = [];

    if (!empty($cloud_project)) {
      $cloud_projects[] = $cloud_project;
    }
    elseif (!empty($cloud_context)) {
      $cloud_projects = array_merge($nodes, $this->loadEntities('cloud_project', [
        'cloud_context' => $cloud_context,
      ]));
    }
    else {
      $cloud_projects = $this->loadEntities('cloud_project');
    }

    foreach ($cloud_projects ?: [] as $project) {
      $k8s_clusters = $project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }

    $nodes = $cloud_contexts
      ? $this->loadEntities('k8s_node', [
        'cloud_context' => $cloud_contexts,
      ], FALSE)
      : [];

    // If nodes are empty, ask the user
    // to try updating the cloud_context to bring in
    // the latest data.
    if (empty($nodes)) {
      $message = $this->t('Nodes not found for K8s Project Cost block.');
      $this->setUpdateMessage($cloud_context, $message);
      return [];
    }

    $build = [];
    $block_id = Html::getUniqueId($this->getPluginId());
    $params = $this->request->get($block_id, []);

    $display_cluster = $this->configuration['display_cluster_in_project_pages_only'];

    $controller = $this->classResolver->getInstanceFromDefinition(K8sCostsControllerBase::class);
    $cost_type_list = $controller->getEc2CostTypes();
    $cost_types = array_keys($cost_type_list);

    $duration_list = $this->getDuration();
    $duration = !empty($params['duration']) ? $params['duration'] : '';
    if (empty($duration) || !array_key_exists($duration, $duration_list)) {
      $duration = $this->configuration['duration'];
    }

    $total_costs = [];
    foreach ($cost_types ?: [] as $cost_type) {
      $cost = $this->k8sService->getTotalCosts($nodes, $cost_type, $this->refresh);
      $total_costs[$cost_type] = $this->getConvertedCost($cost, $cost_type, $duration);
    }

    $cpu_capacity = array_sum(array_map(static function ($node) {
      return $node->getCpuCapacity();
    }, $nodes));

    $memory_capacity = array_sum(array_map(static function ($node) {
      return $node->getMemoryCapacity();
    }, $nodes));

    $pod_capacity = array_sum(array_map(static function ($node) {
      return $node->getPodsCapacity();
    }, $nodes));

    // Get row data.
    $rows = [];
    // Add Total footer.
    $first_col_key = empty($cloud_project)
      ? 'k8s_cloud_project'
      : 'k8s_cluster';

    $footer = [
      'data' => [
        $first_col_key => [
          'data' => [
            '#type' => 'link',
            '#url' => Url::fromRoute('entity.cloud_project.collection', ['cloud_context' => $cloud_context]),
            '#title' => $this->t('Total'),
          ],
          'class' => ['total-label'],
        ],
      ],
      'class' => ['total-row'],
    ];

    $cost_total = [];
    foreach ($cloud_projects ?: [] as $project) {
      $k8s_clusters = $project->get('field_k8s_clusters')->getValue();
      $pods = [];
      foreach ($k8s_clusters ?: [] as $idx => $k8s_cluster) {
        $pods = array_merge($this->loadEntities('k8s_pod', [
          'cloud_context' => $k8s_cluster['value'],
        ], FALSE));

        $row = [];
        if (empty($cloud_project)) {
          if ($display_cluster && $idx < count($k8s_clusters) - 1) {
            continue;
          }
          $row['k8s_cloud_project'] = [
            'data' => [
              '#type' => 'link',
              '#url' => Url::fromRoute('entity.cloud_project.canonical', [
                'cloud_context' => $project->getCloudContext(),
                'cloud_project' => $project->id(),
              ]),
              '#title' => $project->getName(),
            ],
          ];
        }

        $this->cloudConfigPluginManager->setCloudContext($k8s_cluster['value']);
        $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
        $route = $this->cloudConfigPluginManager->getInstanceCollectionTemplateName();
        if (!empty($cloud_project) || !$display_cluster) {
          $row['k8s_cluster'] = [
            'data' => [
              '#type' => 'link',
              '#url' => Url::fromRoute($route, ['cloud_context' => $k8s_cluster['value']]),
              '#title' => $cloud_config->getName(),
            ],
          ];
        }

        $cpu_usage = array_sum(array_map(static function ($pod) {
          return $pod->getCpuUsage();
        }, $pods));

        $memory_usage = array_sum(array_map(static function ($pod) {
          return $pod->getMemoryUsage();
        }, $pods));

        $pod_usage = count($pods);

        foreach ($cost_types ?: [] as $cost_type) {
          $cpu_usage_ratio = $cpu_capacity > 0 ? $cpu_usage / $cpu_capacity : 0;
          $memory_capacity_ratio = $memory_capacity > 0 ? $memory_usage / $memory_capacity : 0;
          $pod_capacity_ratio = $pod_capacity > 0 ? $pod_usage / $pod_capacity : 0;
          $costs = $total_costs[$cost_type] > 0 ? ($cpu_usage_ratio + $memory_capacity_ratio + $pod_capacity_ratio) / 3 * $total_costs[$cost_type] : 0;
          $cost_total[$cost_type] = !empty($cost_total[$cost_type]) ? $cost_total[$cost_type] + $costs : $costs;
          $precision = $cost_type === 'on_demand_hourly' ? 4 : 2;
          $row[$cost_type] = [
            'data' => $this->k8sService->formatCosts($costs, $total_costs[$cost_type], $precision),
            'value' => $costs,
          ];
        }
        $rows[] = ['data' => $row];
      }
    }

    // Add the total values to the first row.
    if (empty($cloud_project) && !$display_cluster) {
      $footer['data']['k8s_cluster'] = '';
    }
    foreach ($cost_types ?: [] as $cost_type) {
      $precision = $cost_type === 'on_demand_hourly' ? 4 : 2;
      $footer['data'][$cost_type] = [
        'data' => $this->k8sService->formatCosts($cost_total[$cost_type], $total_costs[$cost_type], $precision),
        'class' => ['total-costs'],
      ];
    }

    $headers = [];
    if (empty($cloud_project)) {
      $headers['k8s_cloud_project'] = [
        'data' => $this->t('Project Content'),
      ];
    }
    if (!empty($cloud_project) || !$display_cluster) {
      $headers['k8s_cluster'] = [
        'data' => $this->t('K8s cluster'),
      ];
    }
    foreach ($cost_type_list ?: [] as $cost_type => $label) {
      $headers[$cost_type] = ['data' => "$label ($)"];
    }

    $items_per_page_options = cloud_get_views_items_options();
    $items_per_page_options['All'] = '- All -';

    $items_per_page
      = !empty($params['items_per_page'])
        && array_key_exists($params['items_per_page'], $items_per_page_options)
        ? $params['items_per_page'] : self::DEFAULT_ITEMS_PER_PAGE;

    $current_rows_chunk = $rows;
    if ($items_per_page !== 'All') {
      // Set pager's ID as 10.
      $current_page = $this->pagerManager
        ->createPager(count($rows), $items_per_page, 10)
        ->getCurrentPage();
      $rows_chunks = array_chunk($rows, $items_per_page);
      $current_rows_chunk = $rows_chunks[$current_page];
    }

    $table = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $current_rows_chunk,
      '#footer' => [
        $footer,
      ],
      '#attributes' => [
        'class' => [
          'show-footer',
        ],
      ],
    ];

    $duration_label = $duration_list[$duration];
    if ($duration === 'none') {
      $duration_label = '';
    }
    $build['k8s_project_cost_comparison'] = [
      '#type' => 'details',
      '#title' => $this->t('Project') . " $duration_label Cost List",
      '#open' => TRUE,
      '#id' => $block_id,
      // Attach DataTables.
      '#attached' => [
        'library' => [
          'cloud/datatables',
        ],
      ],
      '#attributes' => [
        'class' => [
          'simple-datatable',
        ],
      ],
    ];

    $build['k8s_project_cost_comparison']['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form--inline', 'form-inline', 'clearfix'],
      ],
    ];

    $build['k8s_project_cost_comparison']['container']['items_per_page'] = [
      '#type' => 'select',
      '#name' => 'items_per_page',
      '#title' => $this->t('Items per page'),
      '#value' => $items_per_page,
      '#options' => $items_per_page_options,
    ];

    $build['k8s_project_cost_comparison']['container']['duration'] = [
      '#type' => 'select',
      '#name' => 'duration',
      '#title' => $this->t('Duration'),
      '#value' => $duration,
      '#options' => $duration_list,
    ];

    $build['k8s_project_cost_comparison']['container']['apply'] = [
      '#type' => 'button',
      '#name' => 'apply',
      '#value' => $this->t('Apply'),
    ];

    $build['k8s_project_cost_comparison']['table'] = $table;
    if ($items_per_page !== 'All') {
      $build['k8s_project_cost_comparison']['pager'] = [
        '#type' => 'pager',
        '#element' => 10,
        '#tags' => [
          $this->t('« First'),
          $this->t('‹‹'),
          '',
          $this->t('››'),
          $this->t('Last »'),
        ],
      ];
    }

    $build['#attached']['library'][] = 'k8s/k8s_cost_block';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    $this->killSwitch->trigger();
    // BigPipe/#cache/max-age is breaking my block javascript
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2016-07-17/bigpipecachemax-age-is-breaking-my
    // "a slight delay of a second or two before the charts are built.
    // That seems to work, though it is a junky solution.".
    return $this->moduleHandler->moduleExists('big_pipe') ? 1 : 0;
  }

}
