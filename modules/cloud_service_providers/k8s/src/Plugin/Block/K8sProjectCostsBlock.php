<?php

namespace Drupal\k8s\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\k8s\Controller\K8sCostsControllerBase;

/**
 * Provides a block of the costs for cloud project.
 *
 * @Block(
 *   id = "k8s_project_costs",
 *   admin_label = @Translation("K8s Project Costs"),
 *   category = @Translation("K8s")
 * )
 */
class K8sProjectCostsBlock extends K8sBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return (parent::defaultConfiguration() ?: []) + [
      'display_k8s_cloud_project_page_only' => 1,
      'display_cluster_in_project_pages_only' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $form['display_k8s_cloud_project_page_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this block in K8s Project Content page only'),
      '#default_value' => $this->configuration['display_k8s_cloud_project_page_only'],
    ];

    $form['display_cluster_in_project_pages_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display K8s cluster information in K8s Project Content page only'),
      '#default_value' => $this->configuration['display_cluster_in_project_pages_only'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);

    $this->configuration['display_k8s_cloud_project_page_only']
      = $form_state->getValue('display_k8s_cloud_project_page_only');
    $this->configuration['display_cluster_in_project_pages_only']
      = $form_state->getValue('display_cluster_in_project_pages_only');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    // If aws_cloud module is not enabled; do nothing.
    if (!$this->moduleHandler->moduleExists('aws_cloud')) {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('This block requires the AWS Cloud
        module to be enabled.'),
      ];
    }

    $route_name = $this->routeMatch->getRouteName();
    $cloud_project = $this->routeMatch->getParameter('cloud_project');
    $cloud_context = $this->routeMatch->getParameter('cloud_context');

    // If display_view_k8s_namespace_list_only is checked, do nothing.
    if ($route_name !== 'entity.cloud_project.canonical'
      && $this->configuration['display_k8s_cloud_project_page_only']) {
      return [];
    }

    $nodes = [];
    $cloud_contexts = [];
    $cloud_projects = [];

    if (!empty($cloud_project)) {
      $cloud_projects[] = $cloud_project;
    }
    elseif (!empty($cloud_context)) {
      $cloud_projects = array_merge($nodes, $this->loadEntities('cloud_project', [
        'cloud_context' => $cloud_context,
      ]));
    }
    else {
      $cloud_projects = $this->loadEntities('cloud_project');
    }

    foreach ($cloud_projects ?: [] as $project) {
      $k8s_clusters = $project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }

    $nodes = $cloud_contexts
      ? $this->loadEntities('k8s_node', [
        'cloud_context' => $cloud_contexts,
      ], FALSE)
      : [];

    // If nodes are empty, ask the user
    // to try updating the cloud_context to bring in
    // the latest data.
    if (empty($nodes)) {
      $message = $this->t('Nodes not found for K8s Project Cost block.');
      $this->setUpdateMessage($cloud_context, $message);
      return [];
    }

    $build = [];

    $block_id = Html::getUniqueId($this->getPluginId());
    $params = $this->request->get($block_id, []);

    $controller = $this->classResolver->getInstanceFromDefinition(K8sCostsControllerBase::class);
    $cost_type_list = $controller->getEc2CostTypes();

    $default_cost_type = !empty($params['cost_type'])
      ? $params['cost_type']
      : $this->getDefaultCostType($cloud_contexts);

    $cost_type = array_key_exists($default_cost_type, $cost_type_list)
      ? $default_cost_type
      : self::DEFAULT_COST_TYPE;

    $total_costs = $this->k8sService->getTotalCosts($nodes, $cost_type, $this->refresh);
    $display_cluster = $this->configuration['display_cluster_in_project_pages_only'];
    $precision = $cost_type === 'on_demand_hourly' ? 4 : 2;

    $cpu_capacity = array_sum(array_map(static function ($node) {
      return $node->getCpuCapacity();
    }, $nodes));

    $memory_capacity = array_sum(array_map(static function ($node) {
      return $node->getMemoryCapacity();
    }, $nodes));

    $pod_capacity = array_sum(array_map(static function ($node) {
      return $node->getPodsCapacity();
    }, $nodes));

    // Get row data.
    $rows = [];
    $first_col_key = '';
    // Add Total row.
    if (empty($cloud_project)) {
      $first_col_key = 'k8s_cloud_project';
    }
    else {
      $first_col_key = 'k8s_cluster';
    }
    $footer = [
      'data' => [
        $first_col_key => [
          'data' => [
            '#type' => 'link',
            '#url' => Url::fromRoute('entity.cloud_project.collection', ['cloud_context' => $cloud_context]),
            '#title' => $this->t('Total'),
          ],
          'class' => ['total-label'],
        ],
      ],
      'class' => ['total-row'],
    ];

    $cpu_usage_total = 0;
    $memory_usage_total = 0;
    $pod_usage_total = 0;
    $cost_total = 0;
    foreach ($cloud_projects ?: [] as $project) {
      $k8s_clusters = $project->get('field_k8s_clusters')->getValue();
      $pods = [];
      foreach ($k8s_clusters ?: [] as $idx => $k8s_cluster) {
        $pods = array_merge($this->loadEntities('k8s_pod', [
          'cloud_context' => $k8s_cluster['value'],
        ], FALSE));

        $row = [];
        if (empty($cloud_project)) {
          if ($display_cluster && $idx < count($k8s_clusters) - 1) {
            continue;
          }
          $row['k8s_cloud_project'] = [
            'data' => [
              '#type' => 'link',
              '#url' => Url::fromRoute('entity.cloud_project.canonical', [
                'cloud_context' => $project->getCloudContext(),
                'cloud_project' => $project->id(),
              ]),
              '#title' => $project->getName(),
            ],
          ];
        }

        $this->cloudConfigPluginManager->setCloudContext($k8s_cluster['value']);
        $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
        $route = $this->cloudConfigPluginManager->getInstanceCollectionTemplateName();
        if (!empty($cloud_project) || !$display_cluster) {
          $row['k8s_cluster'] = [
            'data' => [
              '#type' => 'link',
              '#url' => Url::fromRoute($route, ['cloud_context' => $k8s_cluster['value']]),
              '#title' => $cloud_config->getName(),
            ],
          ];
        }

        $cpu_usage = array_sum(array_map(static function ($pod) {
          return $pod->getCpuUsage();
        }, $pods));
        $cpu_usage_total += $cpu_usage;

        $memory_usage = array_sum(array_map(static function ($pod) {
          return $pod->getMemoryUsage();
        }, $pods));
        $memory_usage_total += $memory_usage;

        $pod_usage = count($pods);
        $pod_usage_total += $pod_usage;

        $cpu_usage_ratio = $cpu_capacity > 0 ? $cpu_usage / $cpu_capacity : 0;
        $memory_usage_ratio = $memory_capacity > 0 ? $memory_usage / $memory_capacity : 0;
        $pod_usage_ratio = $pod_capacity > 0 ? $pod_usage / $pod_capacity : 0;
        $costs = $total_costs > 0 ? ($cpu_usage_ratio + $memory_usage_ratio + $pod_usage_ratio) / 3 * $total_costs : 0;
        $cost_total += $costs;

        $row['costs'] = [
          'data' => $this->k8sService->formatCosts($costs, $total_costs, $precision),
          'value' => $costs,
        ];
        $row['cpu_usage'] = [
          'data' => $this->k8sService->formatCpuUsage($cpu_usage, $cpu_capacity),
          'value' => $cpu_usage,
        ];
        $row['memory_usage'] = [
          'data' => $this->k8sService->formatMemoryUsage($memory_usage, $memory_capacity),
          'value' => $memory_usage,
        ];
        $row['pod_usage'] = [
          'data' => $this->k8sService->formatPodUsage($pod_usage, $pod_capacity),
          'value' => $pod_usage,
        ];

        $rows[] = ['data' => $row];
        $pods = [];
      }
    }

    // Add the total values to the first row.
    if (empty($cloud_project) && !$display_cluster) {
      $footer['data']['k8s_cluster'] = '';
    }
    $footer['data']['costs'] = [
      'data' => $this->k8sService->formatCosts($cost_total, $total_costs, $precision),
      'class' => ['total-costs'],
    ];
    $footer['data']['cpu_usage'] = [
      'data' => $this->k8sService->formatCpuUsage($cpu_usage_total, $cpu_capacity),
      'class' => ['total-cpu-usage'],
    ];
    $footer['data']['memory_usage'] = [
      'data' => $this->k8sService->formatMemoryUsage($memory_usage_total, $memory_capacity),
      'class' => ['total-memory-usage'],
    ];
    $footer['data']['pod_usage'] = [
      'data' => $this->k8sService->formatPodUsage($pod_usage_total, $pod_capacity),
      'class' => ['total-pod-usage'],
    ];

    $dynamic_headers = [];
    $headers = [];

    if (empty($cloud_project)) {
      $dynamic_headers[] = ['data' => $this->t('Project Content')];
    }

    if (!empty($cloud_project) || !$display_cluster) {
      $dynamic_headers[] = ['data' => $this->t('K8s cluster')];
    }

    $static_headers = [
      ['data' => $this->t('Total costs ($)')],
      ['data' => $this->t('CPU (Usage)')],
      ['data' => $this->t('Memory (Usage)')],
      ['data' => $this->t('Pods allocation')],
    ];

    $headers = array_merge($dynamic_headers, $static_headers);

    $items_per_page_options = cloud_get_views_items_options();
    $items_per_page_options['All'] = '- All -';

    $items_per_page
      = !empty($params['items_per_page'])
        && array_key_exists($params['items_per_page'], $items_per_page_options)
        ? $params['items_per_page'] : self::DEFAULT_ITEMS_PER_PAGE;

    $current_rows_chunk = $rows;
    if ($items_per_page !== 'All') {
      // Set pager's ID as 10.
      $current_page = $this->pagerManager
        ->createPager(count($rows), $items_per_page, 10)
        ->getCurrentPage();
      $rows_chunks = array_chunk($rows, $items_per_page);
      $current_rows_chunk = $rows_chunks[$current_page];
    }

    $table = [
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $current_rows_chunk,
      '#attributes' => [
        'block_id' => $block_id,
        'class' => [
          'show-footer',
        ],
      ],
      '#footer' => [
        $footer,
      ],
      // Attach DataTables.
      '#attached' => [
        'library' => [
          'cloud/datatables',
        ],
      ],
    ];

    $build['k8s_project_costs'] = [
      '#type' => 'details',
      '#title' => $this->t('Project Costs') . " ($cost_type_list[$cost_type])",
      '#open' => TRUE,
      '#id' => $block_id,
    ];

    // Add the datatable class.
    $build['k8s_project_costs']['#attributes']['class'][] = 'simple-datatable';

    $build['k8s_project_costs']['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form--inline', 'form-inline', 'clearfix'],
      ],
    ];

    $build['k8s_project_costs']['container']['items_per_page'] = [
      '#type' => 'select',
      '#name' => 'items_per_page',
      '#title' => $this->t('Items per page'),
      '#value' => $items_per_page,
      '#options' => $items_per_page_options,
    ];

    $build['k8s_project_costs']['container']['cost_type'] = [
      '#type' => 'select',
      '#name' => 'cost_type',
      '#title' => $this->t('Cost type'),
      '#value' => $cost_type,
      '#options' => $cost_type_list,
    ];

    $build['k8s_project_costs']['container']['apply'] = [
      '#type' => 'button',
      '#name' => 'apply',
      '#value' => $this->t('Apply'),
    ];

    $build['k8s_project_costs']['table'] = $table;
    if ($items_per_page !== 'All') {
      $build['k8s_project_costs']['pager'] = [
        '#type' => 'pager',
        '#element' => 10,
        '#tags' => [
          $this->t('« First'),
          $this->t('‹‹'),
          '',
          $this->t('››'),
          $this->t('Last »'),
        ],
      ];
    }

    $build['#attached']['library'][] = 'k8s/k8s_cost_block';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): bool {
    $this->killSwitch->trigger();
    // BigPipe/#cache/max-age is breaking my block javascript
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2016-07-17/bigpipecachemax-age-is-breaking-my
    // "a slight delay of a second or two before the charts are built.
    // That seems to work, though it is a junky solution.".
    return $this->moduleHandler->moduleExists('big_pipe') ? 1 : 0;
  }

}
