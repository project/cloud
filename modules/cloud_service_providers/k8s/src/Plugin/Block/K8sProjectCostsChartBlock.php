<?php

namespace Drupal\k8s\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\k8s\Controller\K8sCostsControllerBase;
use Drupal\k8s\Controller\K8sProjectCostsChartController;

/**
 * Provides a block of the costs chart.
 *
 * @Block(
 *   id = "k8s_project_costs_chart",
 *   admin_label = @Translation("K8s Project Costs Chart"),
 *   category = @Translation("K8s")
 * )
 */
class K8sProjectCostsChartBlock extends K8sBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return (parent::defaultConfiguration() ?: []) + [
      'display_k8s_project_pages_only' => 1,
      'display_cluster_in_project_pages_only' => 1,
      'chart_period' => 14,
      'chart_type' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $controller = $this->classResolver->getInstanceFromDefinition(K8sProjectCostsChartController::class);

    $form['display_k8s_project_pages_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this block in K8s Project list page and K8s Project Content page only'),
      '#default_value' => $this->configuration['display_k8s_project_pages_only'],
    ];

    $form['display_cluster_in_project_pages_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display K8s cluster information in K8s Project Content page only'),
      '#default_value' => $this->configuration['display_cluster_in_project_pages_only'],
    ];

    $form['chart_period'] = [
      '#type' => 'select',
      '#title' => $this->t('Default chart period'),
      '#options' => $controller->getEc2ChartPeriod(),
      '#default_value' => $this->configuration['chart_period'],
    ];

    $form['chart_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Chart Type'),
      '#default_value' => $this->configuration['chart_type'] ?: 0,
      '#options' => [
        0 => $this->t('Line chart'),
        1 => $this->t('Horizon chart'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $this->configuration['display_k8s_project_pages_only']
      = $form_state->getValue('display_k8s_project_pages_only');
    $this->configuration['display_cluster_in_project_pages_only']
      = $form_state->getValue('display_cluster_in_project_pages_only');
    $this->configuration['chart_period']
      = $form_state->getValue('chart_period');
    $this->configuration['chart_type']
      = $form_state->getValue('chart_type');
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account): AccessResultInterface {
    $access = parent::blockAccess($account);

    if ($access->isForbidden()) {
      return $access;
    }
    $cloud_project = $this->routeMatch->getParameter('cloud_project');
    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $cloud_projects = [];
    if (!empty($cloud_context)) {
      $cloud_projects = $this->loadEntities('cloud_project', [
        'cloud_context' => $cloud_context,
        'type' => 'k8s',
      ]);
    }
    elseif (empty($cloud_project)) {
      $cloud_projects = $this->loadEntities('cloud_project', [
        'type' => 'k8s',
      ]);
    }
    if (empty($cloud_projects)) {
      return AccessResult::forbidden();
    }
    return $access;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $route_name = $this->routeMatch->getRouteName();
    $cloud_project = $this->routeMatch->getParameter('cloud_project');
    $cloud_context = $this->routeMatch->getParameter('cloud_context');

    // If display_k8s_project_pages_only is checked, do nothing.
    if ($route_name !== 'entity.cloud_project.collection'
      && $route_name !== 'entity.cloud_project.canonical'
      && $this->configuration['display_k8s_project_pages_only']) {
      return [];
    }

    $k8s_project_costs_cost_types_json_url = Url::fromRoute(
      'entity.k8s_project.cost_types')->toString();

    $k8s_project_costs_chart_periods_json_url = Url::fromRoute(
      'entity.k8s_project.all_chart_periods')->toString();

    // Use a different chart_periods endpoint
    // if cloud_context or cloud_project is present.
    if (!empty($cloud_project)) {
      $k8s_project_costs_chart_periods_json_url = Url::fromRoute(
      'entity.k8s_project.cloud_project.chart_periods',
        [
          'cloud_context' => $cloud_context,
          'cloud_project' => $cloud_project->id(),
        ]
      )->toString();
    }
    elseif (!empty($cloud_context)) {
      $k8s_project_costs_chart_periods_json_url = Url::fromRoute(
        'entity.k8s_project.chart_periods',
        [
          'cloud_context' => $cloud_context,
        ]
      )->toString();
    }

    $build = [];
    $fieldset_defs = [
      [
        'name' => 'k8s_project_costs',
        'title' => $this->t('Project Costs Chart'),
        'open' => TRUE,
        'fields' => [
          'k8s_project_costs_chart',
        ],
      ],
    ];

    $display_cluster = $this->configuration['display_cluster_in_project_pages_only'];

    if (!empty($cloud_project)) {
      $json_url = Url::fromRoute(
        'entity.k8s_project.cloud_project.costs',
        [
          'cloud_context' => $cloud_context,
          'cloud_project' => $cloud_project->id(),
        ]
      )->toString();
    }
    elseif (!empty($cloud_context)) {
      if ($display_cluster) {
        $json_url = Url::fromRoute('entity.k8s_project.costs', ['cloud_context' => $cloud_context])->toString();
      }
      else {
        $json_url = Url::fromRoute('entity.k8s_project.cluster.costs', ['cloud_context' => $cloud_context])->toString();
      }
    }
    else {
      if ($display_cluster) {
        $json_url = Url::fromRoute('entity.k8s_project.all_costs')->toString();
      }
      else {
        $json_url = Url::fromRoute('entity.k8s_project.cluster.all_costs')->toString();
      }
    }

    $controller = $this->classResolver->getInstanceFromDefinition(K8sCostsControllerBase::class);
    $cost_type_list = $controller->getEc2CostTypes();

    $cloud_contexts = [];
    if (!empty($cloud_project)) {
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }
    elseif (!empty($cloud_context)) {
      $cloud_contexts[] = $cloud_context;
    }

    $default_cost_type = $this->getDefaultCostType($cloud_contexts);

    $cost_type = array_key_exists($default_cost_type, $cost_type_list)
      ? $default_cost_type
      : self::DEFAULT_COST_TYPE;

    $library = empty($this->configuration['chart_type'])
      ? 'k8s/k8s_project_costs_chart'
      : 'k8s/k8s_project_costs_horizon_chart';

    $build['k8s_project_costs_chart'] = [
      '#markup' => '<div id="k8s_project_costs_chart"></div>',
      '#attached' => [
        'library' => [$library],
        'drupalSettings' => [
          'k8s' => [
            'k8s_project_costs_chart_json_url' => $json_url,
            'k8s_project_costs_cost_types_json_url' => $k8s_project_costs_cost_types_json_url,
            'k8s_project_costs_chart_periods_json_url' => $k8s_project_costs_chart_periods_json_url,
            'k8s_project_costs_chart_ec2_cost_type' => $cost_type,
            'k8s_project_costs_chart_period' => $this->configuration['chart_period'],
          ],
        ],
      ],
    ];

    $this->cloudService->reorderForm($build, $fieldset_defs);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): bool {
    $this->killSwitch->trigger();
    // BigPipe/#cache/max-age is breaking my block javascript
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2016-07-17/bigpipecachemax-age-is-breaking-my
    // "a slight delay of a second or two before the charts are built.
    // That seems to work, though it is a junky solution.".
    return $this->moduleHandler->moduleExists('big_pipe') ? 1 : 0;
  }

}
