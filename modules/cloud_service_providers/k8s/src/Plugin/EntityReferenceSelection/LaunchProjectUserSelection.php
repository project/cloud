<?php

namespace Drupal\k8s\Plugin\EntityReferenceSelection;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\user\Plugin\EntityReferenceSelection\UserSelection;

/**
 * Control the selection of users launching project.
 *
 * @EntityReferenceSelection(
 *   id = "default:launch_project_user",
 *   label = @Translation("Launch Project User Selection"),
 *   entity_types = {"user"},
 *   group = "default",
 *   weight = 1
 * )
 */
class LaunchProjectUserSelection extends UserSelection {

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0): array {
    $options = parent::getReferenceableEntities($match, $match_operator, $limit);

    foreach ($options ?: [] as $key => $bundle) {
      foreach ($bundle ?: [] as $uid => $name) {
        $user = User::load($uid);
        if (!$user->hasPermission('launch cloud project')) {
          unset($options[$key][$uid]);
        }
      }
    }
    return $options;
  }

  /**
   * Validator for entity_autocomplete elements with launch project user.
   *
   * @param array $element
   *   The element being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function validateUser(array &$element, FormStateInterface $form_state): void {
    $uid = $form_state->getValue('launch_user');
    if (!is_numeric($uid)) {
      return;
    }
    $user = User::load($uid);
    if (!empty($user) && !$user->hasPermission('launch cloud project')) {
      $form_state->setError($element, t("The user <a href=':url'> %label</a> does not have permissions to launch projects.", [
        ':url' => $user->toUrl('canonical')->toString(),
        '%label' => $user->label(),
      ]));
    }
  }

}
