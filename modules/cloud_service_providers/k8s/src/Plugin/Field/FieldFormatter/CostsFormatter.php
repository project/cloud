<?php

namespace Drupal\k8s\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\k8s\Controller\K8sCostsControllerBase;
use Drupal\k8s\Service\K8sServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'costs_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "costs_formatter",
 *   label = @Translation("Costs formatter"),
 *   field_types = {
 *     "string_long",
 *   }
 * )
 */
class CostsFormatter extends K8sFormatterBase {

  /**
   * Class Resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   The k8s service.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    K8sServiceInterface $k8s_service,
    ClassResolverInterface $class_resolver,
  ) {

    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings, $label,
      $view_mode,
      $third_party_settings,
      $k8s_service);

    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $plugin_id,
        $plugin_definition,
        $configuration['field_definition'],
        $configuration['settings'],
        $configuration['label'],
        $configuration['view_mode'],
        $configuration['third_party_settings'],
        $container->get('k8s'),
        $container->get('class_resolver'));
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $summary[] = $this->t('Displays costs.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'list_style' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element['list_style'] = [
      '#title' => $this->t('List style'),
      '#type' => 'select',
      '#options' => [
        'table' => $this->t('Table'),
        'list' => $this->t('Simple List'),
        'label_list' => $this->t('Labeled List'),
      ],
      '#default_value' => $this->getSetting('list_style'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $list_style = $this->getSetting('list_style');

    $controller = $this->classResolver->getInstanceFromDefinition(K8sCostsControllerBase::class);
    $cost_types = $controller->getEc2CostTypes();

    foreach ($items ?: [] as $delta => $item) {
      if ($item->isEmpty()) {
        continue;
      }
      $costs = Yaml::decode(!empty($item->value) ? $item->value : '');

      if ($list_style === 'table') {
        $headers = [
          'cost_type' => $this->t('Cost type'),
          'cost' => $this->t('Cost ($)'),
        ];

        $rows = array_map(static function ($type) use ($costs, $cost_types) {
          $precision = $type === 'on_demand_hourly' ? 4 : 2;
          $costs_str = sprintf('%.' . $precision . 'f', $costs[$type]);

          return [
            !empty($cost_types[$type]) ? $cost_types[$type] : $type,
            $costs_str,
          ];
        }, array_keys((array) $costs ?: []));

        $elements[$delta] = [
          '#type' => 'table',
          '#header' => $headers,
          '#rows' => $rows,
        ];

        continue;
      }

      $items = array_map(static function ($type) use ($costs, $cost_types, $list_style) {
        $label = !empty($cost_types[$type]) ? $cost_types[$type] : $type;
        $precision = $type === 'on_demand_hourly' ? 4 : 2;
        $costs_str = sprintf('%.' . $precision . 'f', $costs[$type]);

        return $list_style === 'label_list'
          ? "<label>$label ($):</label> $costs_str"
          : "$label ($): $costs_str";
      }, array_keys((array) $costs ?: []));

      $elements[$delta] = [
        '#markup' => implode('<br/>', $items),
        '#allowed_tags' => ['label', 'br'],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    return $field_definition->getName() === 'field_costs';
  }

}
