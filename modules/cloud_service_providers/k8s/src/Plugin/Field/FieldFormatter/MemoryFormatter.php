<?php

namespace Drupal\k8s\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Service\CloudService;

/**
 * Plugin implementation of the 'memory_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "memory_formatter",
 *   label = @Translation("Memory formatter"),
 *   field_types = {
 *     "float",
 *   }
 * )
 */
class MemoryFormatter extends K8sFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $summary[] = $this->t('Displays the memory.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'show_percentage' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element['show_percentage'] = [
      '#title' => $this->t('Show percentage'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('show_percentage'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $show_percentage = $this->getSetting('show_percentage');
    $entity = $items->getEntity();
    foreach ($items ?: [] as $delta => $item) {
      if (!$item->isEmpty()) {
        $memory_str = CloudService::formatMemory($item->value);
        if ($show_percentage) {
          $capacity = $entity->getMemoryCapacity();
          $memory_str = $this->k8sService->formatMemoryUsage($item->value, $capacity);
        }
        $elements[$delta] = ['#markup' => $memory_str];
      }
    }

    return $elements;
  }

}
