<?php

namespace Drupal\k8s\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'pods_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "pods_formatter",
 *   label = @Translation("Pods formatter"),
 *   field_types = {
 *     "integer",
 *   }
 * )
 */
class PodsFormatter extends K8sFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $entity = $items->getEntity();
    foreach ($items ?: [] as $delta => $item) {
      if (!$item->isEmpty()) {
        $allocation = $item->value;
        $capacity = $entity->getPodsCapacity();
        $pod_usage = $this->k8sService->formatPodUsage((float) $allocation, $capacity);
        $elements[$delta] = ['#markup' => $pod_usage];
      }
    }

    return $elements;
  }

}
