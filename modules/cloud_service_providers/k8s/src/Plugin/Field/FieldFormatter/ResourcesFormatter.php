<?php

namespace Drupal\k8s\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\k8s\Service\K8sServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'resources_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "resources_formatter",
 *   label = @Translation("Resources formatter"),
 *   field_types = {
 *     "string_long",
 *   }
 * )
 */
class ResourcesFormatter extends K8sFormatterBase {

  /**
   * The cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloud;

  /**
   * Class Resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   The k8s service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud
   *   The cloud service.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    K8sServiceInterface $k8s_service,
    CloudServiceInterface $cloud,
    ClassResolverInterface $class_resolver,
  ) {

    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings, $label,
      $view_mode,
      $third_party_settings,
      $k8s_service);

    $this->cloud = $cloud;
    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $plugin_id,
        $plugin_definition,
        $configuration['field_definition'],
        $configuration['settings'],
        $configuration['label'],
        $configuration['view_mode'],
        $configuration['third_party_settings'],
        $container->get('k8s'),
        $container->get('cloud'),
        $container->get('class_resolver'));
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $summary[] = $this->t('Displays resources.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'list_style' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element['list_style'] = [
      '#title' => $this->t('List style'),
      '#type' => 'select',
      '#options' => [
        'table' => $this->t('Table'),
        'list' => $this->t('Simple List'),
        'label_list' => $this->t('Labeled List'),
      ],
      '#default_value' => $this->getSetting('list_style'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $list_style = $this->getSetting('list_style');
    $entity = $items->getEntity();
    foreach ($items ?: [] as $delta => $item) {
      if ($item->isEmpty()) {
        continue;
      }
      $resources = Yaml::decode(!empty($item->value) ? $item->value : '');

      if ($list_style === 'table') {
        $headers = $entity->bundle() === 'k8s_pod_resource_store'
          ? [
            'resource_type' => $this->t('Resource'),
            'usage' => $this->t('Usage'),
          ] : [
            'resource_type' => $this->t('Resource'),
            'usage' => $this->t('Usage'),
            'capacity' => $this->t('Capacity'),
          ];

        $rows = $entity->bundle() === 'k8s_pod_resource_store'
          ? [
            'cpu' => [$this->t('CPU')],
            'memory' => [$this->t('Memory')],
          ] : [
            'cpu' => [$this->t('CPU'), 0, 0],
            'memory' => [$this->t('Memory'), 0, 0],
            'pod' => ['Pods allocation', 0, 0],
          ];

        foreach ($resources ?: [] as $type => $resource) {
          $type_arr = explode('_', $type);

          $row = $rows[$type_arr[0]];
          $value = $resource;
          if ($type_arr[0] === 'cpu') {
            $value = sprintf('%.2f', $resource);
          }
          if ($type_arr[0] === 'memory') {
            $value = $this->cloud->formatMemory($resource);
          }
          if ($entity->bundle() === 'k8s_pod_resource_store') {
            $row[] = $value;
            $rows[$type_arr[0]] = $row;
            continue;
          }
          if (count($type_arr) === 1 || $type_arr[1] === 'allocation') {
            $row[1] = $value;
          }
          if (count($type_arr) === 2 && $type_arr[1] === 'capacity') {
            $row[2] = $value;
          }
          $rows[$type_arr[0]] = $row;
        }

        $elements[$delta] = [
          '#type' => 'table',
          '#header' => $headers,
          '#rows' => $rows,
        ];

        continue;
      }

      natcasesort($resources);

      $items = [];
      foreach ($resources ?: [] as $type => $resource) {
        $type_str = ucwords(str_replace('_', ' ', $type));
        $type_arr = explode(' ', $type_str);
        $value = $resource;

        if (count($type_arr) === 1) {
          $type_arr[1] = 'Usage';
        }
        if ($type_arr[0] === 'Cpu') {
          $type_arr[0] = 'CPU';
          $value = sprintf('%.2f', $resource);
        }
        if ($type_arr[0] === 'Memory') {
          $value = CloudService::formatMemory($resource);
        }

        if ($type_arr[0] === 'Pod') {
          $type_arr[0] = 'Pods';
        }

        $label = "{$type_arr[0]} ({$type_arr[1]})";
        $items[] = $list_style === 'label_list'
          ? "<label>$label:</label> $value"
          : "$label: $value";
      }

      $elements[$delta] = [
        '#markup' => implode('<br/>', $items),
        '#allowed_tags' => ['label', 'br'],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    return $field_definition->getName() === 'field_resources';
  }

}
