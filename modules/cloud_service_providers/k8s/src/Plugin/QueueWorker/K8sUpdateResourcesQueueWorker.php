<?php

namespace Drupal\k8s\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\cloud\Plugin\QueueWorker\CloudBaseUpdateResourcesQueueWorker;
use Drupal\k8s\Service\K8sServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes for K8s Update Resources Queue.
 *
 * @QueueWorker(
 *   id = "k8s_update_resources_queue",
 *   title = @Translation("K8s Update Resources Queue"),
 *   deriver = "Drupal\cloud\Plugin\Derivative\CloudUpdateQueueDerivative",
 * )
 */
class K8sUpdateResourcesQueueWorker extends CloudBaseUpdateResourcesQueueWorker implements ContainerFactoryPluginInterface {

  /**
   * The k8s service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  private $k8sService;

  /**
   * Constructs a new LocaleTranslation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s
   *   The k8s service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, K8sServiceInterface $k8s) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->k8sService = $k8s;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('k8s')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $cloud_context = $data['cloud_context'];
    $k8s_method_name = $data['k8s_method_name'];

    try {
      $this->setCloudContext($this->k8sService, $cloud_context, $k8s_method_name);
      $this->checkServiceMethod($this->k8sService, $k8s_method_name);

      if ($k8s_method_name === 'updateK8sNamespaceResourceStoreEntity') {
        $this->k8sService->$k8s_method_name($data['cloud_context'], $data['namespace']);

        return;
      }
      if ($k8s_method_name === 'deleteK8sCloudStore') {
        $this->k8sService->setCloudContext($data['cloud_context']);
        $this->k8sService->$k8s_method_name($data['metrics_duration']);

        return;
      }

      $this->k8sService->$k8s_method_name();
    }
    catch (\Exception $e) {
      // Catch all exceptions to bump it out of the queue.
      $this->handleException($e);
    }
  }

}
