<?php

namespace Drupal\k8s\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\k8s\Service\K8sServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates yaml data.
 */
class YamlArrayDataConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The K8s service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  protected $k8sService;

  /**
   * Constructs a new constraint validator.
   *
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   The K8s service.
   */
  public function __construct(K8sServiceInterface $k8s_service) {
    $this->k8sService = $k8s_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('k8s')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint): void {
    foreach ($items ?: [] as $item) {
      try {
        $yamls = $this->k8sService->decodeMultipleDocYaml($item->value);
        foreach ($yamls ?: [] as $yaml) {
          if (!is_array($yaml)) {
            $this->context->addViolation($constraint->invalidYamlArray);
            return;
          }
        }
      }
      catch (\Exception $e) {
        $this->context->addViolation($constraint->invalidYaml . $e->getMessage());
        break;
      }
    }
  }

}
