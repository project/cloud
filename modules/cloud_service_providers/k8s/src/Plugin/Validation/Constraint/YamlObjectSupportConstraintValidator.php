<?php

namespace Drupal\k8s\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\Validator\Constraint;

/**
 * Validates the "kind" element.
 */
class YamlObjectSupportConstraintValidator extends YamlArrayDataConstraintValidator implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint): void {
    $templates = $this->k8sService->supportedCloudLaunchTemplates();

    foreach ($items ?: [] as $item) {
      try {
        $yamls = $this->k8sService->decodeMultipleDocYaml($item->value);
        foreach ($yamls ?: [] as $yaml) {
          if (is_array($yaml)
            && array_key_exists('kind', $yaml)
            && !empty($yaml['kind'])) {
            $kind = $yaml['kind'];
            $object = array_search($kind, $templates);
            if ($object === FALSE) {
              $this->context->addViolation($constraint->unsupportedObjectType);
              return;
            }
          }
          else {
            $this->context->addViolation($constraint->noObjectFound);
            return;
          }
        }
      }
      catch (\Exception $e) {
        $this->context->addViolation($constraint->invalidYaml . $e->getMessage());
        break;
      }
    }
  }

}
