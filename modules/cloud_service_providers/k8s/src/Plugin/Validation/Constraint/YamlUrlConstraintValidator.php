<?php

namespace Drupal\k8s\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManagerInterface;
use Drupal\k8s\Service\K8sServiceInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates YAML URL fields.
 */
class YamlUrlConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The K8s service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  protected $k8sService;

  /**
   * The CloudLaunchTemplatePluginManager.
   *
   * @var \Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManagerInterface
   */
  protected $launchTemplatePluginManager;

  /**
   * Guzzle Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new constraint validator.
   *
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   The K8s service.
   * @param \Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManagerInterface $launch_template_plugin_manager
   *   The launch template plugin manager.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle Http client.
   */
  public function __construct(
    K8sServiceInterface $k8s_service,
    CloudLaunchTemplatePluginManagerInterface $launch_template_plugin_manager,
    ClientInterface $http_client,
  ) {
    $this->k8sService = $k8s_service;
    $this->launchTemplatePluginManager = $launch_template_plugin_manager;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('k8s'),
      $container->get('plugin.manager.cloud_launch_template_plugin'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint): void {

    // Only perform validations for K8s bundles.
    if ($entity->bundle() !== 'k8s') {
      return;
    }

    $yaml_url = $entity->get('field_yaml_url')->uri;
    $git_repository = $entity->get('field_git_repository')->uri ?? '';
    $detail = $entity->get('field_detail')->value;
    $source_type = $entity->get('field_source_type')->value;

    if (str_contains($source_type, 'git')) {
      if (empty($git_repository)) {
        $this->context
          ->buildViolation($constraint->requiredGitUrl)
          ->atPath('field_git_repository')
          ->addViolation();
        return;
      }

      if (strpos($git_repository, '.git') !== strlen($git_repository) - 4) {
        $this->context
          ->buildViolation($constraint->invalidGitUrl)
          ->atPath('field_git_repository')
          ->addViolation();
        return;
      }

      $url = substr($git_repository, 0, strrpos($git_repository, '/'));
      try {
        $this->httpClient->request('GET', $url);
      }
      catch (RequestException $e) {
        $this->context
          ->buildViolation($constraint->unreachableGitUrl)
          ->atPath('field_git_repository')
          ->addViolation();
        return;
      }
      $files_arr = [];
      $tmp_dir_name = '';
      $error_message = $this->launchTemplatePluginManager->validateGit($entity, $files_arr, $tmp_dir_name, TRUE);
      if (!empty($error_message)) {
        $this->context
          ->buildViolation($error_message)
          ->atPath('field_git_repository')
          ->addViolation();
      }
      return;
    }

    if (empty($yaml_url) && empty($detail)) {
      $this->context
        ->buildViolation($constraint->requiredYamlUrlOrDetail)
        ->atPath('field_yaml_url')
        ->addViolation();
      return;
    }

    if (!empty($yaml_url) && !empty($detail)) {
      $this->context
        ->buildViolation($constraint->prohibitYamlUrlAndDetail)
        ->atPath('field_yaml_url')
        ->addViolation();
      return;
    }

    if (empty($yaml_url)) {
      return;
    }

    $content = file_get_contents($yaml_url);
    if (empty($content)) {
      $this->context
        ->buildViolation($constraint->invalidYamlUrl)
        ->atPath('field_yaml_url')
        ->addViolation();
      return;
    }

    $templates = $this->k8sService->supportedCloudLaunchTemplates();
    try {
      $yamls = $this->k8sService->decodeMultipleDocYaml($content);
      foreach ($yamls ?: [] as $yaml) {
        if (!is_array($yaml)) {
          $this->context
            ->buildViolation($constraint->invalidYamlFormat)
            ->atPath('field_yaml_url')
            ->addViolation();
          return;
        }

        if (empty($yaml['kind'])) {
          $this->context
            ->buildViolation($constraint->noKindFound)
            ->atPath('field_yaml_url')
            ->addViolation();
          return;
        }

        $kind = $yaml['kind'];
        $object = array_search($kind, $templates);
        if (empty($object)) {
          $this->context
            ->buildViolation($constraint->unsupportedObjectType, ['%kind' => $kind])
            ->atPath('field_yaml_url')
            ->addViolation();
          return;
        }
      }
    }
    catch (\Exception $e) {
      $this->context
        ->buildViolation($constraint->invalidYaml . $e->getMessage())
        ->atPath('field_yaml_url')
        ->addViolation();
    }
  }

}
