<?php

namespace Drupal\k8s\Plugin\cloud\launch_template;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Utility\Html;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\Exception\NotRegularDirectoryException;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\k8s\Entity\K8sEntityInterface;
use Drupal\k8s\Entity\K8sNamespace;
use Drupal\k8s\Entity\K8sSchedule;
use Drupal\k8s\Form\K8sContentFormInterface;
use Drupal\k8s\Plugin\EntityReferenceSelection\LaunchTemplateUserSelection;
use Drupal\k8s\Service\K8sOperationsServiceInterface;
use Drupal\k8s\Service\K8sServiceException;
use Drupal\k8s\Service\K8sServiceInterface;
use Drupal\k8s\Traits\K8sFormTrait;
use GuzzleHttp\Promise\Promise;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * K8s cloud launch template plugin.
 */
class K8sCloudLaunchTemplatePlugin extends CloudPluginBase implements CloudLaunchTemplatePluginInterface, ContainerFactoryPluginInterface {

  use K8sFormTrait;

  public const CHECK_RETRY_COUNT = 5;

  public const CHECK_SLEEP_SECOND = 1;

  /**
   * The K8s service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  protected $k8sService;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Current login user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  protected $entityLinkRenderer;

  /**
   * File system object.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The K8s operation service.
   *
   * @var \Drupal\k8s\Service\K8sOperationsServiceInterface
   */
  protected $k8sOperationsService;

  /**
   * K8sCloudLaunchTemplatePlugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The uuid service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current login user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   The K8s service.
   * @param \Drupal\k8s\Service\K8sOperationsServiceInterface $k8s_operations_service
   *   The K8s operation service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    UuidInterface $uuid_service,
    AccountProxyInterface $current_user,
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityLinkRendererInterface $entity_link_renderer,
    FileSystemInterface $file_system,
    CloudServiceInterface $cloud_service,
    K8sServiceInterface $k8s_service,
    K8sOperationsServiceInterface $k8s_operations_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->uuidService = $uuid_service;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityLinkRenderer = $entity_link_renderer;
    $this->fileSystem = $file_system;
    $this->cloudService = $cloud_service;
    $this->k8sService = $k8s_service;
    $this->k8sOperationsService = $k8s_operations_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity.link_renderer'),
      $container->get('file_system'),
      $container->get('cloud'),
      $container->get('k8s'),
      $container->get('k8s.operations')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityBundleName(): string {
    return $this->pluginDefinition['entity_bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function launch(CloudLaunchTemplateInterface $cloud_launch_template, ?FormStateInterface $form_state = NULL): array {
    $k8s_obj = $cloud_launch_template->get('field_object')->value;
    $source_type = $cloud_launch_template->get('field_source_type')->value;
    $delete_resources = $form_state->getValue('field_delete_all_resources');
    $launch_uid = $form_state->getValue('launch_user');

    if (!empty($delete_resources)) {
      $resources = $cloud_launch_template->get('field_launch_resources')->getValue();
      if (!empty($resources)) {
        $entities = [];
        $schedules_arr = [];
        foreach ($resources ?: [] as $resource) {
          $type = $resource['item_key'];
          $id = $resource['item_value'];
          $entity = $this->entityTypeManager
            ->getStorage($type)
            ->load($id);
          if (empty($entity)) {
            continue;
          }

          $entities[] = $entity;

          // Collect schedule entities.
          if ($type === 'k8s_deployment' || $type === 'k8s_pod') {
            $schedule_entities = $this->entityTypeManager
              ->getStorage('k8s_schedule')
              ->loadByProperties([
                'cloud_context' => $cloud_launch_template->getCloudContext(),
                'launch_template_name' => $cloud_launch_template->getName(),
                'namespace_name' => $entity->getNamespace(),
                'resource_name' => $entity->getName(),
                'schedule_type' => K8sContentFormInterface::CLOUD_ORCHESTRATOR_SCHEDULER,
              ]);

            $schedules_arr[] = array_values($schedule_entities);
          }
        }

        // $schedules_arr is a two-level array,
        // so we can't simply use array_merge.
        $schedules = call_user_func_array('array_merge', $schedules_arr);

        if (!empty($entities)) {
          $this->k8sService->deleteResourcesWithEntities($entities, TRUE);
        }

        // Remove schedules.
        $this->k8sService->deleteSchedules($schedules);

        $cloud_launch_template->get('field_launch_resources')->setValue(NULL);
      }
    }

    $route = [
      'route_name' => 'entity.cloud_launch_template.canonical',
      'params' => [
        'cloud_launch_template' => $cloud_launch_template->id(),
        'cloud_context' => $cloud_launch_template->getCloudContext(),
      ],
    ];

    $this->k8sService->setCloudContext($cloud_launch_template->getCloudContext());

    if (str_contains($source_type, 'git')) {
      $this->launchByGitYamlFiles($cloud_launch_template, $form_state, $launch_uid);
      return $route;
    }
    $object_types = $this->k8sService->supportedCloudLaunchTemplates();
    if (empty($object_types[$k8s_obj])) {
      $this->messenger->addError($this->t('@object launch not supported.', ['@object' => $k8s_obj]));
      return $route;
    }

    $errors = $this->checkExistence(
      $cloud_launch_template->get('field_detail')->value,
      $cloud_launch_template->getCloudContext(),
      $cloud_launch_template->get('field_namespace')->value
    );

    if (!empty($errors)) {
      $resources = '';
      foreach ($errors as $error) {
        $resources .= "<li>{$error->render()}</li>";
      }

      $message = $this->formatPlural(count($errors),
        'The following resource still exists. Launch again after completely deleting it.<ul>@resources</ul>',
        'The following resources still exist. Launch again after completely deleting them.<ul>@resources</ul>', [
          '@resources' => Markup::create($resources),
        ]);

      $this->messenger->addError($message);

      return $route;
    }

    return $this->launchK8sResources($cloud_launch_template, $launch_uid);
  }

  /**
   * {@inheritdoc}
   */
  public function buildListHeader(): array {
    return [
      [
        'data' => $this->t('Namespace'),
        'specifier' => 'field_namespace',
        'field' => 'field_namespace',
      ], [
        'data' => $this->t('Object'),
        'specifier' => 'field_object',
        'field' => 'field_object',
      ], [
        'data' => $this->t('Enable time scheduler'),
        'specifier' => 'field_enable_time_scheduler',
        'field' => 'field_enable_time_scheduler',
      ], [
        'data' => $this->t('Workflow status'),
        'specifier' => 'field_workflow_status',
        'field' => 'field_workflow_status',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRow(CloudLaunchTemplateInterface $entity): array {
    $row['field_namespace'] = [
      'data' => $this->entityLinkRenderer->renderViewElement(
        $entity->get('field_namespace')->value,
        'k8s_namespace',
        'name',
        []
      ),
    ];
    $row['field_object'] = [
      'data' => $this->renderField($entity, 'field_object'),
    ];
    $row['field_enable_time_scheduler'] = [
      'data' => $this->renderField($entity, 'field_enable_time_scheduler'),
    ];
    $row['field_workflow_status'] = [
      'data' => $this->renderField($entity, 'field_workflow_status'),
    ];
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function validateGit(
    CloudLaunchTemplateInterface $entity,
    array &$files_arr,
    string &$tmp_dir_name,
    bool $delete_tmp_dir = FALSE,
  ): string {
    $git_url_value = $entity->get('field_git_repository')->getValue();
    $git_url = $git_url_value[0]['uri'];
    $git_branch_value = $entity->get('field_git_branch')->getValue();
    $git_branch = !empty($git_branch_value) ? $git_branch_value[0]['value'] : '';
    $git_paths = $entity->get('field_git_path')->getValue();
    $git_username = $entity->get('field_git_username')->value;
    $git_access_token = $entity->get('field_git_access_token')->value;

    $tmp_dir_name = $this->getTempDirectoryNameFromEntityId($entity);

    // Make the temporary working directory.
    $result = $this->fileSystem->mkdir($tmp_dir_name);

    if (!$result) {
      return $this->t('Unable to create the working directory.');
    }
    if ($git_username) {
      $account = rawurlencode($git_username);
      if ($git_access_token) {
        $account .= ":" . rawurlencode($git_access_token);
      }
      $git_url = str_replace("://", "://$account@", $git_url);
    }
    $output = [];
    $return_var = 0;
    exec(
      !empty($git_branch)
        ? "git clone -b $git_branch $git_url $tmp_dir_name"
        : "git clone $git_url $tmp_dir_name",
      $output,
      $return_var
    );

    if ($return_var !== 0) {
      if ($delete_tmp_dir) {
        $this->fileSystem->deleteRecursive($tmp_dir_name);
      }
      return $this->t('Unable to clone the git repository.');
    }
    $match = '/.*/';
    $files_arr = [];
    try {
      foreach ($git_paths ?: [] as $path) {
        $files_arr[] = $this->fileSystem->scanDirectory($tmp_dir_name . $path['value'], $match);
      }
    }
    catch (NotRegularDirectoryException $e) {
      if ($delete_tmp_dir) {
        $this->fileSystem->deleteRecursive($tmp_dir_name);
      }
      return $this->t('Git resource path might not be correct.');
    }

    if ($delete_tmp_dir) {
      $this->fileSystem->deleteRecursive($tmp_dir_name);
    }

    return '';
  }

  /**
   * Build the launch form for K8s.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state if launch is called from a form.
   */
  public function buildLaunchForm(array &$form, FormStateInterface $form_state): void {
    $form_object = $form_state->getFormObject();
    $entity = $form_object->getEntity();
    $source_type = $entity->get('field_source_type')->value;
    // Instantiate launch template parameters which will be altered by checks.
    if ($entity->hasField('field_launch_template_parameters')) {
      $items_launch_template_parameters = $entity->get('field_launch_template_parameters');
    }

    $form_display = $this->entityTypeManager
      ->getStorage('entity_form_display')
      ->load('cloud_launch_template.k8s.default');

    try {
      $form['#parents'] = [];
      $widget_cloud_context = $form_display->getRenderer('cloud_context');
      $items_cloud_context = $entity->get('cloud_context');
      $items_cloud_context->filterEmptyItems();
      $form['cloud_context'] = $widget_cloud_context->form($items_cloud_context, $form, $form_state);
      $form['cloud_context']['widget'][0]['value']['#title'] = $this->t('K8s cluster');
      $form['cloud_context']['widget'][0]['value']['#type'] = 'select';
      $option = $this->k8sService->clusterAllowedValues();
      $option[K8sServiceInterface::AUTOMATIC_SELECTION] = K8sServiceInterface::AUTOMATIC_SELECTION;
      $form['cloud_context']['widget'][0]['value']['#options'] = $option;
      $form['cloud_context']['widget'][0]['value']['#ajax'] = [
        'callback' => 'k8s_ajax_callback_get_fields',
      ];

      unset(
        $form['cloud_context']['widget'][0]['value']['#size'],
        $form['cloud_context']['widget'][0]['value']['#description']
      );

      $widget_namespace = $form_display->getRenderer('field_namespace');
      $items_namespace = $entity->get('field_namespace');
      $items_namespace->filterEmptyItems();
      $form['field_namespace'] = $widget_namespace->form($items_namespace, $form, $form_state);

      $resources = $entity->get('field_launch_resources')->getValue();
      if (!empty($resources)) {
        k8s_create_resources_message($form, $resources);
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }

    $fieldsets_def = [
      [
        'name' => 'k8s',
        'title' => $this->t('K8s'),
        'open' => TRUE,
        'fields' => [
          'cloud_context',
          'field_namespace',
        ],
      ],
    ];

    $git_available = $this->k8sService->isGitCommandAvailable();
    if (!$git_available && str_contains($source_type, 'git')) {
      $this->messenger->addError($this->t('A git command could not be found on the server.  Install the git command and try again.'));
      $this->handleError($form, $fieldsets_def);
      return;
    }
    if (str_contains($source_type, 'git')) {
      $files_arr = [];
      $tmp_dir_name = '';
      $error_message = $this->validateGit($entity, $files_arr, $tmp_dir_name);
      if (!empty($error_message)) {
        $this->messenger->addError($error_message);
        $this->handleError($form, $fieldsets_def);
        $this->fileSystem->deleteRecursive($tmp_dir_name);
        return;
      }

      // $files_arr is a two-level array, so we can't simply use array_merge.
      $files = call_user_func_array('array_merge', $files_arr);

      if (!empty($files)) {
        usort($files, static function ($file1, $file2) {
          $dir1 = dirname($file1->uri);
          $dir2 = dirname($file2->uri);

          if ($dir1 === $dir2) {
            return ($file1 > $file2) ? 1 : -1;
          }
          return ($dir1 > $dir2) ? 1 : -1;
        });

        $templates = $this->k8sService->supportedCloudLaunchTemplates();
        $extensions = $this->configFactory->get('k8s.settings')
          ->get('k8s_yaml_file_extensions');
        $regex = '/\.(' . preg_replace('/ +/', '|', preg_quote($extensions, NULL)) . ')$/i';

        for ($idx = 0; $idx < count($files); $idx++) {
          $file = $files[$idx];
          $message = NULL;
          $validated = FALSE;
          $file_contents = file_get_contents($file->uri);
          if (!$file_contents) {
            $message = $this->t('Unable to get contents from the file %file.', ['%file' => $file->filename]);
            continue;
          }
          if (!preg_match($regex, $file->filename)) {
            continue;
          }

          try {
            $yamls = $this->k8sService->decodeMultipleDocYaml(
              // We can render the Yaml even if we
              // do not have launch_template_parameters.
              // This way, we bypass errors thrown by decoding
              // and the below validation will show a more helpful error.
              $this->cloudService->renderTemplate($file_contents, $this->getLaunchTemplateParameters($entity))
            );

            // Validate YAML files.
            foreach ($yamls ?: [] as $yaml) {
              if (!is_array($yaml)) {
                $message = $this->t('The file %file is not YAML format.', ['%file' => $file->filename]);
                break;
              }

              // Check if there have been any placeholders,
              // replaced by NULL during rendering.
              $undefined_placeholders = $this->checkUndefinedPlaceholders($file_contents, $this->getLaunchTemplateParameters($entity));
              if (!empty($undefined_placeholders)) {
                $missing_parameters = '';
                foreach ($undefined_placeholders ?: [] as $missing_parameter) {
                  $missing_parameters .= "<li>{$missing_parameter}</li>";
                  // Check for FieldItemList to avoid call on NULL.
                  if (empty($items_launch_template_parameters)) {
                    continue;
                  }

                  // Add missing KVP's to the form.
                  $render_kvp = TRUE;
                  foreach ($items_launch_template_parameters ?: [] as $key_value) {
                    if ($key_value->getItemKey() === $missing_parameter) {
                      $render_kvp = FALSE;
                      break;
                    }
                  }
                  if ($render_kvp === TRUE) {
                    // Render kvp for current missing parameter.
                    $items_launch_template_parameters->appendItem([
                      'item_key' => $missing_parameter,
                      'item_value' => '',
                    ]);
                  }
                }
                $message = $this->t('The file <strong><a href=":url">%file</a></strong> contains undefined parameters:<ul>@missing_parameters</ul>',
                [
                  ':url' => $entity->toUrl('edit-form', ['absolute' => TRUE])->toString(),
                  '%file' => $file->filename,
                  '@missing_parameters' => Markup::create($missing_parameters),
                ]);
                break;
              }

              if (!isset($yaml['kind'])) {
                $message = $this->t("No 'Kind' element found in the file %file.", ['%file' => $file->filename]);
                break;
              }

              $kind = $yaml['kind'];
              $object = array_search($kind, $templates);
              if ($object === FALSE) {
                $message = $this->t("Unsupported 'Kind' element in the file %file.", ['%file' => $file->filename]);
                break;
              }

              $validated = TRUE;
            }
          }
          catch (InvalidDataTypeException $e) {
            $message = $this->t('The file %file fails to decode: @exception', [
              '%file' => $file->filename,
              '@exception' => $e->getMessage(),
            ]);
          }

          $tmp_dir_name = $tmp_dir_name ?: $this->getTempDirectoryNameFromEntityId($entity);
          $directory = str_replace($tmp_dir_name, '', dirname($file->uri));

          if (!isset($form['yaml_files'][$directory])) {
            $form['yaml_files'][$directory] = [
              '#type' => 'details',
              '#title' => $directory,
              '#open' => TRUE,
              'files' => [],
            ];
          }
          $form['yaml_files'][$directory]['files'][$idx] = [
            '#type' => 'details',
            '#title' => $file->filename,
            '#open' => !$validated,
          ];
          if (!$validated && !empty($message)) {
            $form['yaml_files'][$directory]['files'][$idx] += [
              'message' => [
                '#theme' => 'status_messages',
                '#message_list' => ['error' => [$message]],
              ],
              '#attributes' => [
                'class' => ['error', 'has-error'],
              ],
            ];
          }
          $form['yaml_files'][$directory]['files'][$idx]["launch_resource_$idx"] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Launch this resource'),
            '#default_value' => $validated,
          ];
          if (!$validated && !empty($message)) {
            $form['yaml_files'][$directory]['files'][$idx]["launch_resource_$idx"]['#attributes'] = ['class' => ['error']];
          }
          $form['yaml_files'][$directory]['files'][$idx] += [
            "yaml_file_name_$idx" => [
              '#type' => 'hidden',
              '#default_value' => $file->filename,
            ],
            "yaml_file_content_$idx" => [
              '#type' => 'textarea',
              '#default_value' => $file_contents,
              '#attributes' => ['readonly' => 'readonly'],
            ],
            "yaml_file_path_$idx" => [
              '#type' => 'hidden',
              '#default_value' => str_replace($tmp_dir_name . '/', '', $file->uri),
            ],
          ];
        }
      }

      // If we do not have any launch resources, we display a warning.
      if (!isset($form['yaml_files'])) {
        $this->messenger->addWarning($this->t('There were no launch resources found. Check <a href=":url">%label</a> for the Git repository or YAML URL settings and try again.', [
          ':url' => $entity->toUrl('edit-form', ['absolute' => TRUE])->toString(),
          '%label' => $entity->label(),
        ]));
        $this->handleError($form, $fieldsets_def);
        $this->fileSystem->deleteRecursive($tmp_dir_name);
        return;
      }

      // Check if entity has field,
      // Field might be missing if update has not been performed.
      if (!empty($items_launch_template_parameters)) {
        // Add editable KVP for launch_template_parameters.
        $widget_launch_template_parameters = $form_display->getRenderer('field_launch_template_parameters');

        // Re-render form with missing parameters.
        $form['field_launch_template_parameters'] = $widget_launch_template_parameters
          ->form($items_launch_template_parameters->filterEmptyItems(), $form, $form_state);
        $form['field_launch_template_parameters']['widget']['#title'] = NULL;

        // Add the fieldset definition.
        $fieldsets_def[] = [
          'name' => 'launch_template_parameters',
          'title' => $this->t('Launch template parameters'),
          'open' => TRUE,
          'fields' => [
            'field_launch_template_parameters',
          ],
        ];
      }

      $fieldsets_def[] = [
        'name' => 'launch_resources',
        'title' => $this->t('Launch Resources'),
        'open' => TRUE,
        'fields' => [
          'yaml_files',
        ],
      ];

      $this->fileSystem->deleteRecursive($tmp_dir_name);
    }
    elseif (str_contains($source_type, 'yml')) {
      $time_scheduler_option = $entity->get('field_time_scheduler_option')->value;
      $enable_time_scheduler = $entity->get('field_enable_time_scheduler')->value;
      $yaml_url = $entity->get('field_yaml_url')->uri;
      if (!empty($enable_time_scheduler)
        && $time_scheduler_option === K8sContentFormInterface::CRONJOB_SCHEDULER
        && empty($yaml_url)) {
        $this->messenger->addError($this->t('If you are using CronJob Scheduler, you need to enter the YAML URL.'));
        $this->handleError($form, $fieldsets_def);
        return;
      }
    }

    $resources = $entity->get('field_launch_resources')->getValue();
    if (!empty($resources)) {
      $form['field_delete_all_resources'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Delete following resources'),
        '#default_value' => FALSE,
      ];

      $fieldsets_def[] = [
        'name' => 'delete_resources',
        'title' => $this->t('Delete Resources'),
        'open' => TRUE,
        'fields' => [
          'field_delete_all_resources',
          'confirm_message',
        ],
      ];
    }

    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    $form['launch_user'] = [
      '#type'          => 'entity_autocomplete',
      '#target_type'   => 'user',
      '#title'         => $this->t('Launch as'),
      '#size'          => 60,
      '#default_value' => $user,
      '#weight'        => 0,
      '#required'      => TRUE,
      '#selection_handler' => 'default:launch_template_user',
      '#element_validate' => [
        [EntityAutocomplete::class, 'validateEntityAutocomplete'],
        [LaunchTemplateUserSelection::class, 'validateUser'],
      ],
    ];

    $fieldsets_def[] = [
      'name' => 'username',
      'title' => $this->t('Username'),
      'open' => TRUE,
      'fields' => [
        'launch_user',
      ],
    ];

    $this->cloudService->reorderForm($form, $fieldsets_def);
    $form['description']['#weight'] = 1 + (
      !empty($form['username'])
        ? $form['username']['#weight']
        : count($fieldsets_def)
    );
  }

  /**
   * Render a launch template entity field.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The launch template entity.
   * @param string $field_name
   *   The field to render.
   * @param string $view
   *   The view to render.
   *
   * @return array
   *   A fully loaded render array for that field or empty array.
   */
  private function renderField(CloudLaunchTemplateInterface $entity, $field_name, $view = 'default'): array {
    $field = [];
    if ($entity->hasField($field_name)) {
      $field = $entity->get($field_name)->view($view);
      // Hide the label.
      $field['#label_display'] = 'hidden';
    }
    return $field;
  }

  /**
   * Launch a K8s resource from a launch template.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template
   *   Cloud launch template interface.
   * @param int $launch_uid
   *   The launch user ID.
   * @param string $yaml_file_path
   *   The yaml file path.
   *
   * @return array
   *   The route to redirect after launch.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   *   Thrown when unable to launch deployment.
   */
  private function launchK8sResources(
    CloudLaunchTemplateInterface $cloud_launch_template,
    int $launch_uid,
    string $yaml_file_path = '',
  ): array {
    $object_types = $this->k8sService->supportedCloudLaunchTemplates();
    // @todo Support $this->cloudService->getTagKeyCreatedByUid() in Terraform module.
    $created_by_uid_name = $cloud_launch_template->bundle() === 'k8s'
      ? $this->cloudService->getTagKeyCreatedByUid('k8s', $cloud_launch_template->getCloudContext())
      : K8sEntityInterface::ANNOTATION_CREATED_BY_UID;

    // If we have twig values within this template, we replace.
    $rendered_file_contents
      = !empty($cloud_launch_template->get('field_detail'))
      && !empty($cloud_launch_template->get('field_detail')->value)
      ? $this->cloudService->renderTemplate(
        $cloud_launch_template->get('field_detail')->value,
        $this->getLaunchTemplateParameters($cloud_launch_template))
      : '';

    $enable_time_scheduler = $cloud_launch_template->get('field_enable_time_scheduler')->value;
    $time_scheduler_option = !empty($cloud_launch_template->hasField('field_time_scheduler_option'))
      && !empty($cloud_launch_template->get('field_time_scheduler_option'))
      ? $cloud_launch_template->get('field_time_scheduler_option')->value
      : K8sContentFormInterface::CLOUD_ORCHESTRATOR_SCHEDULER;
    $source_type = $cloud_launch_template->get('field_source_type')->value;

    $yamls = $this->k8sService->decodeMultipleDocYaml($rendered_file_contents);

    $entity_type_id = '';
    foreach ($yamls ?: [] as $yaml) {
      // Add owner uid to annotations.
      $yaml['metadata']['annotations'][$created_by_uid_name] = "$launch_uid";
      if ($yaml['kind'] === 'Deployment') {
        // Add owner uid to pod template.
        $yaml['spec']['template']['metadata']['annotations'][$created_by_uid_name] = "$launch_uid";
      }

      $launch_template_id = $cloud_launch_template->id();
      $yaml['metadata']['annotations'][K8sEntityInterface::ANNOTATION_LAUNCHED_APPLICATION_ID] = "$launch_template_id";
      $kind = $yaml['kind'];
      $object_type = array_search($kind, $object_types);
      $entity_type_id = "k8s_$object_type";

      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      // We cannot use empty() here since namespaceable can be
      // one of two values such as FALSE or NULL.
      $namespaceable = $entity_type->get('namespaceable') === NULL;

      // Get the name of method create<ResourceName>.
      $short_label = self::getCamelCaseWithWhitespace($object_type);
      $name_camel = self::getCamelCaseWithoutWhitespace($short_label);
      $create_method_name = "create{$name_camel}";

      // Get the name of method update<ResourceName>s.
      $name_plural_camel = $this->getShortEntityTypeNamePluralCamelByType($entity_type);
      $update_method_name = "update{$name_plural_camel}";

      try {
        if ($namespaceable) {
          $namespace = $cloud_launch_template->get('field_namespace')->value;
          if ($kind !== 'Pod' && $kind !== 'Deployment') {
            $this->k8sService->$create_method_name(
              $namespace,
              $yaml
            );
          }
          else {
            if (!empty($enable_time_scheduler)) {
              $timestamp = time();
              $resource_name = $yaml['metadata']['name'];
              $start_hour = $cloud_launch_template->get('field_startup_time_hour')->value;
              $start_minute = $cloud_launch_template->get('field_startup_time_minute')->value;
              $stop_hour = $cloud_launch_template->get('field_stop_time_hour')->value;
              $stop_minute = $cloud_launch_template->get('field_stop_time_minute')->value;
              if ($time_scheduler_option === K8sContentFormInterface::CLOUD_ORCHESTRATOR_SCHEDULER) {
                $schedule = K8sSchedule::create([
                  'cloud_context' => $cloud_launch_template->getCloudContext(),
                  'name' => $namespace . '_' . $resource_name,
                  'kind' => $kind,
                  'namespace_name' => $namespace,
                  'resource_name' => $resource_name,
                  'launch_template_name' => $cloud_launch_template->getName(),
                  'start_hour' => $start_hour,
                  'start_minute' => $start_minute,
                  'start_time' => $this->getTimeFormat($start_hour, $start_minute),
                  'stop_hour' => $stop_hour,
                  'stop_minute' => $stop_minute,
                  'stop_time' => $this->getTimeFormat($stop_hour, $stop_minute),
                  'manifest' => Yaml::encode($yaml),
                  'created' => $timestamp,
                  'changed' => $timestamp,
                  'refreshed' => $timestamp,
                ]);
                $schedule->save();

                // Check schedule.
                $current_hour_minute = date('Hi');
                $start_hour_minute = sprintf('%02d%02d', $start_hour, $start_minute);
                $stop_hour_minute = sprintf('%02d%02d', $stop_hour, $stop_minute);

                $is_in_period = (
                  ($start_hour_minute <= $stop_hour_minute)
                  && ($current_hour_minute >= $start_hour_minute && $current_hour_minute <= $stop_hour_minute)
                )
                || (
                  ($start_hour_minute > $stop_hour_minute)
                  && ($current_hour_minute >= $start_hour_minute || $current_hour_minute <= $stop_hour_minute)
                );

                if ($is_in_period) {
                  $this->k8sService->$create_method_name(
                    $namespace,
                    $yaml
                  );
                }
              }
              elseif ($time_scheduler_option === K8sContentFormInterface::CRONJOB_SCHEDULER) {
                $yaml_url = !empty($cloud_launch_template->hasField('field_yaml_url'))
                    && !empty($cloud_launch_template->get('field_yaml_url'))
                    ? $cloud_launch_template->get('field_yaml_url')->uri
                    : '';
                $cloud_context = $cloud_launch_template->getCloudContext();
                $schedule = K8sSchedule::create([
                  'cloud_context' => $cloud_context,
                  'name' => $namespace . '_' . $resource_name,
                  'kind' => $kind,
                  'namespace_name' => $namespace,
                  'resource_name' => $resource_name,
                  'launch_template_name' => $cloud_launch_template->getName(),
                  'start_hour' => $start_hour,
                  'start_minute' => $start_minute,
                  'start_time' => $this->getTimeFormat($start_hour, $start_minute),
                  'stop_hour' => $stop_hour,
                  'stop_minute' => $stop_minute,
                  'stop_time' => $this->getTimeFormat($stop_hour, $stop_minute),
                  'manifest' => Yaml::encode($yaml),
                  'created' => $timestamp,
                  'changed' => $timestamp,
                  'refreshed' => $timestamp,
                  'schedule_type' => K8sContentFormInterface::CRONJOB_SCHEDULER,
                  'yaml_url' => $yaml_url,
                ]);
                $schedule->save();

                if (str_contains($source_type, 'git') && !empty($yaml_file_path)) {
                  $this->k8sOperationsService->createGitCronJobFromYaml(
                      $cloud_launch_template,
                      $cloud_context,
                      $namespace,
                      $resource_name,
                      $kind,
                      $yaml_file_path,
                      $yaml,
                      $start_hour,
                      $start_minute,
                      $stop_hour,
                      $stop_minute
                    );
                }
                elseif (str_contains($source_type, 'yml')) {
                  $this->k8sOperationsService->createCronJobFromYaml(
                    $cloud_launch_template,
                    $cloud_context,
                    $namespace,
                    $yaml_url,
                    $start_hour,
                    $start_minute,
                    $stop_hour,
                    $stop_minute
                  );
                }
              }
            }
            else {
              $this->k8sService->$create_method_name(
                $namespace,
                $yaml
              );
            }
          }
        }
        else {
          $this->k8sService->$create_method_name($yaml);
        }

        $this->k8sService->$update_method_name([
          'metadata.name' => $yaml['metadata']['name'],
        ], FALSE);

        $params = [
          'cloud_context' => $cloud_launch_template->getCloudContext(),
          'name' => $yaml['metadata']['name'],
        ];

        $params += !empty($namespaceable) && !empty($namespace) ? ['namespace' => $namespace] : [];

        // Update creation_yaml field of entity.
        $entities = $this->entityTypeManager->getStorage($entity_type_id)->loadByProperties($params);
        if (!empty($entities)) {
          $entity = reset($entities);

          // Update creation_yaml.
          if (method_exists($entity, 'setCreationYaml')) {
            unset($yaml['metadata']['annotations'][K8sEntityInterface::ANNOTATION_LAUNCHED_APPLICATION_ID]);
            $entity->setCreationYaml(Yaml::encode($yaml));
            $entity->save();
          }

          // Update owner uid.
          $entity->setOwnerId($launch_uid);
          $entity->save();

          if (empty($enable_time_scheduler)
            || (!empty($enable_time_scheduler)
              && $time_scheduler_option === K8sContentFormInterface::CLOUD_ORCHESTRATOR_SCHEDULER)) {
            $this->messenger->addStatus($this->t('The @type <a href=":url">%label</a> has been launched.', [
              '@type' => $entity_type->getSingularLabel(),
              ':url' => $entity->toUrl('canonical')->toString(),
              '%label' => $yaml['metadata']['name'],
            ]));

            $this->logger('k8s')->info($this->t('@type: created %label.', [
              '@type' => $entity_type->getSingularLabel(),
              '%label' => $yaml['metadata']['name'],
            ]), [
              'link' => $entity->toLink($this->t('View'))->toString(),
            ]);
          }

          $cloud_launch_template->get('field_launch_resources')->appendItem([
            'item_key' => $entity->getEntityTypeId(),
            'item_value' => $entity->id(),
          ]);

          // Identify whether called by 'launchByGitYamlFiles'.
          // Parameters are 3 when called by the function.
          // And the cloud launch template shouldn't be saved.
          if (count(func_get_args()) === 2) {
            $cloud_launch_template->setValidationRequired(FALSE);
            $cloud_launch_template->save();
          }
        }
        else {
          if (empty($enable_time_scheduler)
            || (!empty($enable_time_scheduler)
              && $time_scheduler_option === K8sContentFormInterface::CLOUD_ORCHESTRATOR_SCHEDULER)) {
            $this->messenger->addStatus($this->t('The @type %label has been launched.', [
              '@type' => $entity_type->getSingularLabel(),
              '%label' => $yaml['metadata']['name'],
            ]));

            $this->logger('k8s')->info($this->t('@type: launched %label.', [
              '@type' => $entity_type->getSingularLabel(),
              '%label' => $yaml['metadata']['name'],
            ]));
          }
        }

      }
      catch (K8sServiceException
      | EntityStorageException
      | EntityMalformedException $e) {

        $this->processOperationErrorStatus($cloud_launch_template, 'launched');

        $route = [
          'route_name' => 'entity.cloud_launch_template.canonical',
          'params' => [
            'cloud_launch_template' => $cloud_launch_template->id(),
            'cloud_context' => $cloud_launch_template->getCloudContext(),
          ],
        ];

        return $route;
      }
    }

    if (count($yamls) === 1) {
      $route = [
        'route_name' => "view.$entity_type_id.list",
        'params' => ['cloud_context' => $cloud_launch_template->getCloudContext()],
      ];
    }
    else {
      $route = [
        'route_name' => 'entity.cloud_launch_template.canonical',
        'params' => [
          'cloud_launch_template' => $cloud_launch_template->id(),
          'cloud_context' => $cloud_launch_template->getCloudContext(),
        ],
      ];
    }

    return $route;
  }

  /**
   * Launch by YAML files in git repository.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template
   *   Cloud launch template interface.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state if launch is called from a form.
   * @param int $launch_uid
   *   The launch user ID.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  private function launchByGitYamlFiles(CloudLaunchTemplateInterface $cloud_launch_template, ?FormStateInterface $form_state = NULL, $launch_uid = 0): void {
    $values = $form_state->getValues();

    $launch_resources = [];
    $yaml_file_contents = [];
    $yaml_file_names = [];
    $yaml_file_paths = [];
    foreach ($values as $key => $value) {
      if (strpos($key, 'launch_resource') !== FALSE) {
        $launch_resources[] = $value;
      }
      if (strpos($key, 'yaml_file_content') !== FALSE) {
        $yaml_file_contents[] = $value;
      }
      if (strpos($key, 'yaml_file_name') !== FALSE) {
        $yaml_file_names[] = $value;
      }
      if (strpos($key, 'yaml_file_path') !== FALSE) {
        $yaml_file_paths[] = $value;
      }
    }

    if (empty($yaml_file_contents) || empty($launch_resources) || empty($yaml_file_names) || empty($yaml_file_paths)) {
      return;
    }

    $objects = [];
    $templates = $this->k8sService->supportedCloudLaunchTemplates();
    $errors_arr = [];
    foreach (array_map(
      NULL,
      $yaml_file_contents,
      $launch_resources,
      $yaml_file_names,
      $yaml_file_paths
      ) as [$file_contents, $launch_resource, $yaml_file_name, $yaml_file_path]) {
      if (empty($launch_resource)) {
        continue;
      }

      try {
        // Render YAML.
        // Even if there is nothing to render,
        // this will not affect the functionality.
        $rendered_yaml_contents = $this->cloudService->renderTemplate($file_contents, $this->getLaunchTemplateParameters($cloud_launch_template));
        $yamls = $this->k8sService->decodeMultipleDocYaml($rendered_yaml_contents);
        $validated = FALSE;
        $object = NULL;

        // Validate yamls.
        foreach ($yamls ?: [] as $yaml) {
          if (!is_array($yaml)) {
            $this->messenger->addError($this->t('The file %file is not YAML format.', ['%file' => $yaml_file_name]));
            break;
          }

          // Check if there have been any placeholders
          // replaced by NULL during rendering.
          $undefined_placeholders = $this->checkUndefinedPlaceholders($file_contents, $this->getLaunchTemplateParameters($cloud_launch_template));
          if (!empty($undefined_placeholders)) {
            $missing_parameters = '';
            foreach ($undefined_placeholders ?: [] as $missing_parameter) {
              $missing_parameters .= "<li>{$missing_parameter}</li>";
            }
            $this->messenger->addError($this->t('The file <strong><a href=":url">%file</a></strong> contains undefined parameters:<ul>@missing_parameters</ul>',
            [
              ':url' => $cloud_launch_template->toUrl('edit-form', ['absolute' => TRUE])->toString(),
              '%file' => $yaml_file_name,
              '@missing_parameters' => Markup::create($missing_parameters),
            ]));
            break;
          }

          if (!isset($yaml['kind'])) {
            $this->messenger->addError($this->t("No 'Kind' element found in the file %file.", ['%file' => $yaml_file_name]));
            break;
          }

          $kind = $yaml['kind'];
          $object = array_search($kind, $templates);
          if ($object === FALSE) {
            $this->messenger->addError($this->t("Unsupported 'Kind' element in the file %file.", ['%file' => $yaml_file_name]));
            break;
          }

          $validated = TRUE;
        }

        // Whenever we need to check something,
        // we should use the rendered $yaml string.
        $result = !empty($validated)
          ? $this->checkExistence(
            $rendered_yaml_contents,
            $cloud_launch_template->getCloudContext(),
            $cloud_launch_template->get('field_namespace')->value
          ) : [];

        if ($validated && !empty($result)) {
          $errors_arr[] = $result;
          continue;
        }

        if ($validated) {
          $cloud_launch_template->get('field_detail')->setValue($rendered_yaml_contents);
          $errors_before = count($this->messenger->messagesByType($this->messenger::TYPE_ERROR));
          // Put an extra 2nd param to identify called by this function.
          $this->launchK8sResources($cloud_launch_template, $launch_uid, $yaml_file_path);
          $errors_after = count($this->messenger->messagesByType($this->messenger::TYPE_ERROR));
          if ($errors_before === $errors_after) {
            if (count($yamls) === 1) {
              $objects[] = $object;
            }
            else {
              $objects[] = 'mixed';
            }
          }
        }
      }
      catch (\Exception $e) {
        $this->messenger->addError($this->t('Invalid Yaml format in the file %file: %message', [
          '%file' => $yaml_file_name,
          '%message' => $e->getMessage(),
        ]));
      }
    }

    // $errors_arr is a two-level array, so we can't simply use array_merge.
    $errors = call_user_func_array('array_merge', $errors_arr);

    if (!empty($errors)) {
      $resources = '';
      foreach ($errors ?: [] as $error) {
        $resources .= "<li>{$error->render()}</li>";
      }

      $message = $this->formatPlural(count($errors),
      'The following resource still exists. Launch again after completely deleting it.<ul>@resources</ul>',
      'The following resources still exist. Launch again after completely deleting them.<ul>@resources</ul>', [
        '@resources' => Markup::create($resources),
      ]);

      $this->messenger->addError($message);
    }

    $cloud_launch_template->get('field_object')->setValue($objects);
    $cloud_launch_template->get('field_detail')->setValue(NULL);
    $cloud_launch_template->setValidationRequired(FALSE);
    $cloud_launch_template->save();
  }

  /**
   * Handling error on launch form.
   *
   * @param array $form
   *   Form elements.
   * @param array $fieldsets_def
   *   Field set definitions.
   */
  private function handleError(array &$form, array $fieldsets_def): void {
    // Hide submit button and description message.
    $form['actions']['submit']['#access'] = FALSE;
    $form['description']['#access'] = FALSE;

    if (!empty($form['confirm_message'])) {
      unset($form['confirm_message']);
    }

    $this->cloudService->reorderForm($form, $fieldsets_def);
  }

  /**
   * Returns launch_template_parameters.
   *
   * @param \CloudLaunchTemplateInterface $cloud_launch_template
   *   The server template to retrieve launch parameters from.
   *
   * @return array
   *   The set of KVP's.
   */
  private function getLaunchTemplateParameters(CloudLaunchTemplateInterface $cloud_launch_template): array {
    if (!$cloud_launch_template->hasField('field_launch_template_parameters')) {
      return [];
    }
    $result = [];
    $parameters = $cloud_launch_template->get('field_launch_template_parameters')->getValue();
    foreach ($parameters ?: [] as $parameter) {
      if (!empty($parameter['item_key']) && !empty($parameter['item_value'])) {
        $result[$parameter['item_key']] = $parameter['item_value'];
      }
    }
    return $result;
  }

  /**
   * Check the existence of resources.
   *
   * @param string $yaml_string
   *   The rendered YAML string.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   *
   * @return array
   *   YAMLS with resources still existing.
   */
  private function checkExistence($yaml_string, $cloud_context, $namespace): array {

    $self = $this;

    $promise = new Promise(static function () use (&$promise, $self, $yaml_string, $namespace) {
      $object_types = $self->k8sService->supportedCloudLaunchTemplates();
      $yamls = $self->k8sService->decodeMultipleDocYaml($yaml_string);

      for ($count = 0; $count < $self::CHECK_RETRY_COUNT; $count++) {
        if (empty($yamls)) {
          break;
        }

        foreach ($yamls ?: [] as $idx => $yaml) {
          $name = $yaml['metadata']['name'];
          $kind = $yaml['kind'];
          $object_type = array_search($kind, $object_types);
          $entity_type_id = "k8s_$object_type";
          $entity_type = $self->entityTypeManager->getDefinition($entity_type_id);
          // We cannot use empty() here since namespaceable can be
          // one of two values such as FALSE or NULL.
          $namespaceable = $entity_type->get('namespaceable') === NULL;

          // Check existence.
          $result = $self->k8sService->hasResource($entity_type_id,
            !empty($namespaceable)
              ? [
                'metadata.namespace' => $namespace,
                'metadata.name' => $name,
              ] : [
                'metadata.name' => $name,
              ]);

          if (empty($result)) {
            unset($yamls[$idx]);
            continue;
          }

          $yamls[$idx]['entity_type_id'] = $entity_type_id;
        }

        sleep($self::CHECK_SLEEP_SECOND);
      }

      $promise->resolve($yamls);
    });

    $result = $promise->wait();

    $messages = [];

    foreach ($result ?: [] as $yaml) {
      $name = $yaml['metadata']['name'];
      $entity_type_id = $yaml['entity_type_id'];
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      // We cannot use empty() here since namespaceable can be
      // one of two values such as FALSE or NULL.
      $namespaceable = $entity_type->get('namespaceable') === NULL;

      $name_camel = $this->getShortEntityTypeNamePluralCamelByType($entity_type);
      $update_method_name = "update{$name_camel}";

      // Refresh resources.
      $this->k8sService->$update_method_name([
        'metadata.name' => $name,
      ], FALSE);

      $params = [
        'cloud_context' => $cloud_context,
        'name' => $name,
      ];

      $params += !empty($namespaceable) ? ['namespace' => $namespace] : [];

      $entities = $this->entityTypeManager->getStorage($entity_type_id)->loadByProperties($params);

      if (empty($entities)) {
        $messages[] = $this->t('@type: %name', [
          '@type' => $entity_type->getSingularLabel(),
          '%name' => $name,
        ]);
        continue;
      }

      $entity = reset($entities);

      if (!empty($namespaceable) && !($entity instanceof K8sNamespace)) {
        $namespaces = $this->entityTypeManager->getStorage('k8s_namespace')
          ->loadByProperties([
            'name' => $entity->getNamespace(),
            'cloud_context' => $entity->getCloudContext(),
          ]);
        if (!empty($namespaces)) {
          $namespace_entity = reset($namespaces);
          $messages[] = $this->t('@type: <a href=":url">%name</a> (Namespace: <a href=":namespace_url">%namespace</a>)', [
            '@type' => $entity->getEntityType()->getSingularLabel(),
            '%name' => $entity->getName(),
            ':url' => $entity->toUrl('canonical')->toString(),
            '%namespace' => $namespace_entity->getName(),
            ':namespace_url' => $namespace_entity->toUrl('canonical')->toString(),
          ]);
          continue;
        }
      }
      $messages[] = $this->t('@type: <a href=":url">%name</a>', [
        '@type' => $entity->getEntityType()->getSingularLabel(),
        '%name' => $entity->getName(),
        ':url' => $entity->toUrl('canonical')->toString(),
      ]);
    }

    return $messages;
  }

  /**
   * Helper function to check for undefined placeholders.
   *
   * @param string $yaml
   *   Unrendered yaml representation as string.
   * @param array $placeholders
   *   Array of KVP's to check against,
   *    can be obtained from field_launch_template_parameters.
   *
   * @return array
   *   Empty array if all placeholders have values
   *    or array of undefined placeholders to display.
   */
  private function checkUndefinedPlaceholders(string $yaml, array $placeholders): array {
    // Extract all twig placeholders from the $yaml string.
    $twig_pattern = '/\{{[^}]*\}}/';
    preg_match_all($twig_pattern, $yaml, $matches);
    $leftovers = [];
    // If there are placeholders found
    // check to see if a KVP has been provided.
    foreach (array_unique($matches[0]) ?: [] as $match) {
      // Check to see if a kvp has been provided for it.
      preg_match('~{{(.*?)}}~', $match, $token);
      // If not, return placeholder.
      if (empty($placeholders[str_replace(' ', '', $token[1])])) {
        $leftovers[] = str_replace(' ', '', $token[1]);
      }
    }

    return $leftovers;
  }

  /**
   * Get the temp directory name from the Entity ID.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The cloud launch template entity.
   *
   * @return string
   *   The temp directory name.
   */
  private function getTempDirectoryNameFromEntityId(CloudLaunchTemplateInterface $entity): string {
    $tmp_dir = $this->fileSystem->getTempDirectory();
    $name = Html::getId($entity->getName() . ' ' . $entity->id());
    return $this->fileSystem->getDestinationFilename($tmp_dir . "/$name", FileExists::Rename);
  }

  /**
   * {@inheritdoc}
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface {
    return AccessResult::allowed();
  }

}
