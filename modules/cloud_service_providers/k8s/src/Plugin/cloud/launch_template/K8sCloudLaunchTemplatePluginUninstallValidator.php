<?php

namespace Drupal\k8s\Plugin\cloud\launch_template;

use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginUninstallValidator;

/**
 * Validates module uninstall readiness based on existing content entities.
 */
class K8sCloudLaunchTemplatePluginUninstallValidator extends CloudLaunchTemplatePluginUninstallValidator {

}
