<?php

namespace Drupal\k8s\Plugin\cloud\project;

use Drupal\Component\Utility\Html;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Plugin\cloud\project\CloudProjectPluginInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\k8s\Entity\K8sEntityInterface;
use Drupal\k8s\Service\K8sServiceException;
use Drupal\k8s\Service\K8sServiceInterface;
use Drupal\user\Entity\Role;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * K8s cloud project plugin.
 */
class K8sCloudProjectPlugin extends CloudPluginBase implements CloudProjectPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The K8s service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  protected $k8sService;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Current login user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  protected $entityLinkRenderer;

  /**
   * File system object.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * K8sCloudProjectPlugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   The Kubernetes cluster.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The uuid service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current login user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    K8sServiceInterface $k8s_service,
    EntityTypeManagerInterface $entity_type_manager,
    UuidInterface $uuid_service,
    AccountProxyInterface $current_user,
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityLinkRendererInterface $entity_link_renderer,
    FileSystemInterface $file_system,
    CloudServiceInterface $cloud_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->k8sService = $k8s_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->uuidService = $uuid_service;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityLinkRenderer = $entity_link_renderer;
    $this->fileSystem = $file_system;
    $this->cloudService = $cloud_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('k8s'),
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity.link_renderer'),
      $container->get('file_system'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityBundleName(): string {
    return $this->pluginDefinition['entity_bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function launch(CloudProjectInterface $cloud_project, ?FormStateInterface $form_state = NULL): array {
    $project_name = $cloud_project->get('name')->value;
    $enable_resource_scheduler = $cloud_project->get('field_enable_resource_scheduler')->value;
    $k8s_clusters = $cloud_project->get('field_k8s_clusters');
    $launch_uid = $form_state->getValue('launch_user');
    // @todo Support $this->cloudService->getTagKeyCreatedByUid() in Terraform module.
    $created_by_uid_name = $cloud_project->bundle() === 'k8s'
      ? $this->cloudService->getTagKeyCreatedByUid('k8s', $cloud_project->getCloudContext())
      : K8sEntityInterface::ANNOTATION_CREATED_BY_UID;
    $message_warnings = [];

    $route = [
      'route_name' => 'entity.cloud_project.collection',
      'params' => [
        'cloud_context' => $cloud_project->getCloudContext(),
      ],
    ];

    $k8s_resource_list = [
      'k8s_namespace',
      'k8s_resource_quota',
    ];

    $k8s_cluster_all_list = $this->k8sService->clusterAllowedValues();
    $k8s_cluster_list = [];
    foreach ($k8s_clusters ?: [] as $cloud_context) {
      if (!empty($cloud_context->value)) {
        $k8s_cluster_list[$cloud_context->value] = $cloud_context->value;
      }
    }

    try {
      // Delete exist resource.
      if (!empty($k8s_cluster_all_list)) {
        $diff = array_diff(array_keys($k8s_cluster_all_list), array_keys($k8s_cluster_list));
        if (!empty(k8s_delete_specific_resource($diff, $k8s_resource_list, $project_name))) {
          $message_warnings[] = $this->t('The existing resources have been recreated since the names were the same.');
        }
      }

      // Create K8s namespace.
      $params = [];
      $params['metadata']['name'] = $project_name;
      $params['metadata']['annotations']['entity_type'] = $cloud_project->getEntityType()->getSingularLabel();
      // Add owner uid to annotations.
      $params['metadata']['annotations'][$created_by_uid_name] = $launch_uid;

      if (!empty($enable_resource_scheduler)) {

        // Create K8s resource quota.
        $params_resource_quota = [];
        $params_resource_quota['metadata']['name'] = $project_name;
        $params_resource_quota['metadata']['annotations'][$created_by_uid_name] = $launch_uid;

        if ($cloud_project->hasField('field_request_cpu')) {
          $request_cpu = Html::escape($cloud_project->get('field_request_cpu')->value) . 'm';
          $params_resource_quota['spec']['hard']['limits.cpu'] = $request_cpu;
        }
        if ($cloud_project->hasField('field_request_memory')) {
          $request_memory = Html::escape($cloud_project->get('field_request_memory')->value) . 'Mi';
          $params_resource_quota['spec']['hard']['limits.memory'] = $request_memory;
        }
        if ($cloud_project->hasField('field_pod_count')) {
          $pod_count = Html::escape($cloud_project->get('field_pod_count')->value);
          $params_resource_quota['spec']['hard']['pods'] = $pod_count;
        }
      }

      // NOTE: $k8s_cluster is unused variable.
      foreach ($k8s_cluster_list ?: [] as $cloud_context) {
        if (empty($cloud_context)) {
          continue;
        }
        $this->k8sService->setCloudContext($cloud_context);
        $this->k8sService->updateNamespaces();
        $this->k8sService->updateResourceQuotas();
        $this->k8sService->updateResourceWithEntity('k8s_namespace', $cloud_context, $project_name, $params);
        // Update Owner uid.
        $entities = $this->entityTypeManager->getStorage('k8s_namespace')->loadByProperties(
          [
            'cloud_context' => $cloud_project->getCloudContext(),
            'name' => $project_name,
          ]
        );
        if (!empty($entities)) {
          $entity = reset($entities);

          // Update owner uid.
          $entity->setOwnerId($launch_uid);
          $entity->save();
        }

        if (!empty($enable_resource_scheduler)) {
          $this->k8sService->updateResourceWithEntity('k8s_resource_quota', $cloud_context, $project_name, $params_resource_quota);
        }
        else {
          $resource_quotas = $this->entityTypeManager->getStorage('k8s_resource_quota')
            ->loadByProperties([
              'name' => $project_name,
              'cloud_context' => $cloud_context,
            ]);
          if (!empty($resource_quotas)) {
            $this->k8sService->deleteResourcesWithEntities($resource_quotas);
          }
        }
      }

      $roles = $this->entityTypeManager->getStorage('user_role')
        ->loadByProperties([
          'id' => $project_name,
        ]);

      if (!empty($launch_uid)) {
        $user = $this->entityTypeManager->getStorage('user')->load($launch_uid);
        $data = [
          'id' => $project_name,
          'label' => $project_name,
        ];

        if (empty($roles)) {
          $role = Role::create($data);
          $role->save();

          // Grant permissions.
          $this->grantPermissions($role, $k8s_cluster_list, $project_name);

          // Add role.
          $user->addRole($role->id());
          $user->save();

          // Add Message.
          $this->processOperationStatus($role, 'created');
        }
      }
      $message_all = $this->messenger->all();
      if (!empty($message_all)) {
        $messages = array_shift($message_all);
        $this->messenger->deleteAll();

        $output = '';
        foreach ($messages ?: [] as $message) {
          $output .= "<li>{$message}</li>";
        }
        $this->messenger->addStatus($this->t('The @type %label has been launched.<ul>@output</ul>', [
          '@type' => $cloud_project->getEntityType()->getSingularLabel(),
          '%label' => $cloud_project->toLink()->toString(),
          '@output' => Markup::Create($output),
        ]));
        $this->logOperationMessage($cloud_project, 'launched');
      }
      if (!empty($message_warnings)) {
        foreach ($message_warnings ?: [] as $message) {
          $this->messenger->addWarning($message);
        }
      }
      return $route;
    }
    catch (K8sServiceException
    | EntityStorageException
    | EntityMalformedException $e) {

      try {

        $this->processOperationErrorStatus($cloud_project, 'launched');

      }
      catch (EntityMalformedException $e) {
        $this->handleException($e);
      }
    }
    // Exception return type handling.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildListHeader(): array {
    return [
      [
        'data' => $this->t('K8s cluster'),
        'specifier' => 'field_k8s_clusters',
        'field' => 'field_k8s_clusters',
      ],
      [
        'data' => $this->t('Enable resource scheduler'),
        'specifier' => 'field_enable_resource_scheduler',
        'field' => 'field_enable_resource_scheduler',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRow(CloudProjectInterface $entity): array {

    $k8s_clusters = $entity->get('field_k8s_clusters')->getValue() ?? [];

    $cluster_link = '';
    foreach ($k8s_clusters ?: [] as $cluster_name) {
      $cloud_config_entity = $this->entityTypeManager
        ->getStorage('cloud_config')
        ->loadByProperties([
          'type' => ['k8s'],
          'cloud_context' => [$cluster_name['value']],
        ]);
      $cloud_config = array_shift($cloud_config_entity);
      $link = Link::createFromRoute(
          $cloud_config->getName(),
          'view.k8s_node.list',
          [
            'cloud_context' => $cloud_config->getCloudContext(),
          ]
        )->toString();
      $cluster_link .= $link . ", ";
    }

    $cluster_link = rtrim($cluster_link ?: '', ', ');

    $row['field_k8s_clusters'] = [
      'data' => ['#markup' => $cluster_link],
    ];
    $row['field_enable_resource_scheduler'] = [
      'data' => $this->renderField($entity, 'field_enable_resource_scheduler'),
    ];
    return $row;
  }

  /**
   * Render a project entity field.
   *
   * @param \Drupal\cloud\Entity\CloudProjectInterface $entity
   *   The project entity.
   * @param string $field_name
   *   The field to render.
   * @param string $view
   *   The view to render.
   *
   * @return array
   *   A fully loaded render array for that field or empty array.
   */
  private function renderField(CloudProjectInterface $entity, $field_name, $view = 'default'): array {
    $field = [];
    if ($entity->hasField($field_name)) {
      $field = $entity->get($field_name)->view($view);
      // Hide the label.
      $field['#label_display'] = 'hidden';
    }
    return $field;
  }

  /**
   * Grant permissions.
   *
   * @param \Drupal\user\Entity\Role $role
   *   The role entity.
   * @param array $k8s_clusters
   *   The k8s clusters.
   * @param string $namespace
   *   The namespace.
   */
  private function grantPermissions(Role $role, array $k8s_clusters, $namespace): void {
    $permissions = [];

    // Dashboard.
    $permissions[] = 'access dashboard';

    // K8s clusters.
    foreach ($k8s_clusters ?: [] as $k8s_cluster) {
      $permissions[] = "view $k8s_cluster";
    }

    // Cloud launch template.
    $permissions[] = 'list cloud server template';
    $permissions[] = 'add cloud server templates';
    $permissions[] = 'delete own cloud server templates';
    $permissions[] = 'edit own cloud server templates';
    $permissions[] = 'view own published cloud server templates';
    $permissions[] = 'access cloud server template revisions';
    $permissions[] = 'revert all cloud server template revisions';
    $permissions[] = 'delete all cloud server template revisions';
    $permissions[] = 'launch cloud server template';

    // Project.
    $permissions[] = 'list cloud project';
    $permissions[] = 'add cloud projects';
    $permissions[] = 'delete own cloud projects';
    $permissions[] = 'edit own cloud projects';
    $permissions[] = 'view own published cloud projects';
    $permissions[] = 'access cloud project revisions';
    $permissions[] = 'revert all cloud project revisions';
    $permissions[] = 'delete all cloud project revisions';
    $permissions[] = 'launch cloud project';

    // Access namespace.
    $permissions[] = "view k8s namespace $namespace";

    // Resources CRUD.
    $resources = [
      'api service',
      'cluster role',
      'cluster role binding',
      'config map',
      'cron job',
      'daemon set',
      'deployment',
      'endpoint',
      'horizontal pod autoscaler',
      'ingress',
      'job',
      'limit range',
      'namespace',
      'network policy',
      'pod',
      'replica set',
      'persistent volume',
      'persistent volume claim',
      'priority class',
      'resource quota',
      'role',
      'role binding',
      'schedule',
      'secret',
      'service',
      'service account',
      'stateful set',
      'storage class',
    ];

    foreach ($resources ?: [] as $resource) {
      $permissions[] = "add k8s $resource";
      $permissions[] = "list k8s $resource";
      $permissions[] = "view own k8s $resource";
      $permissions[] = "edit own k8s $resource";
      $permissions[] = "delete own k8s $resource";
    }

    foreach ($permissions ?: [] as $permission) {
      $role->grantPermission($permission);
    }

    $role->save();
  }

}
