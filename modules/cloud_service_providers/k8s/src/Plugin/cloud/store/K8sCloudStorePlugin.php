<?php

namespace Drupal\k8s\Plugin\cloud\store;

use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Entity\CloudStoreInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Plugin\cloud\store\CloudStorePluginInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\k8s\Service\K8sServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * K8s cloud store plugin.
 */
class K8sCloudStorePlugin extends CloudPluginBase implements CloudStorePluginInterface, ContainerFactoryPluginInterface {

  /**
   * Interval to update cost stores.
   *
   * @var int
   */
  private $updateInterval = 600;

  /**
   * The K8s service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  protected $k8sService;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Current login user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  protected $entityLinkRenderer;

  /**
   * File system object.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $queue;

  /**
   * K8sCloudStorePlugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   The K8s service.
   * @param \Drupal\Cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The uuid service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current login user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    K8sServiceInterface $k8s_service,
    CloudServiceInterface $cloud_service,
    EntityTypeManagerInterface $entity_type_manager,
    UuidInterface $uuid_service,
    AccountProxyInterface $current_user,
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityLinkRendererInterface $entity_link_renderer,
    FileSystemInterface $file_system,
    ClassResolverInterface $class_resolver,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->k8sService = $k8s_service;
    $this->cloudService = $cloud_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->uuidService = $uuid_service;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityLinkRenderer = $entity_link_renderer;
    $this->fileSystem = $file_system;
    $this->classResolver = $class_resolver;

    $this->updateInterval = $this->configFactory
      ->get('k8s.settings')
      ->get('k8s_update_cost_store_cron_time');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('k8s'),
      $container->get('cloud'),
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity.link_renderer'),
      $container->get('file_system'),
      $container->get('class_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityBundleName(): string {
    return $this->pluginDefinition['entity_bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildListHeader(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRow(CloudStoreInterface $entity): array {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @return iterable
   *   Array of EntityInterface.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadConfigEntities(): iterable {
    return $this->entityTypeManager->getStorage($this->pluginDefinition['entity_type'])
      ->loadByProperties(['type' => [$this->pluginDefinition['entity_bundle']]]);
  }

  /**
   * Load a cloud service provider (CloudConfig) entity.
   *
   * @param string $entity_bundle
   *   Bundle string.
   *
   * @return \Drupal\cloud\Entity\CloudConfigInterface
   *   Entity or NULL if there is no entity.
   */
  public function loadConfigEntity(string $entity_bundle): ?CloudConfigInterface {
    $entity = $this->entityTypeManager
      ->getStorage($this->pluginDefinition['entity_type'])
      ->loadByProperties(['entity_bundle' => [$entity_bundle]]);

    if (count(!empty($entity) ? $entity : []) > 0) {
      return array_shift($entity);
    }
    return NULL;
  }

  /**
   * Queuing items for updating resource store entities.
   */
  public function updateK8sResourceStoreEntity(): void {
    $entities = $this->cloudConfigPluginManager->loadConfigEntities('k8s');
    if (empty($entities)) {
      return;
    }

    // Get target entities to be updated.
    foreach ($entities ?: [] as $k8s_cluster) {
      $cloud_context = $k8s_cluster->getCloudContext();

      // To check whether k8s metrics server is installed or not.
      if (!k8s_is_metrics_enabled($cloud_context)) {
        $this->messenger->addWarning($this->t('@message Install %metrics_server_link into @cloud_context', [
          '@message' => $this->t('Node Resource Store requires K8s Metrics Server.'),
          '%metrics_server_link' => $this->k8sService->getMetricsServerLink($cloud_context),
          '@cloud_context' => $cloud_context,
        ]));
        continue;
      }

      $namespace_entities = $this->entityTypeManager
        ->getStorage('k8s_namespace')
        ->loadByProperties([
          'cloud_context' => $cloud_context,
        ]);
      if (empty($namespace_entities)) {
        continue;
      }

      // Set queue.
      $queue = $this->cloudService->createQueue($this->cloudService->getResourceQueueName($k8s_cluster));

      $this->k8sService->setCloudContext($cloud_context);
      foreach ($namespace_entities ?: [] as $namespace) {
        // If the interval time has elapsed since the last data was created,
        // skip to create new data.
        if (!$this->k8sService->checkCloudStoreLatestCreatedTime('k8s_namespace_resource_store', $namespace->getName())) {
          continue;
        }
        // Get resource metrics from each namespace.
        $queue->createItem([
          'k8s_method_name' => 'updateK8sNamespaceResourceStoreEntity',
          'cloud_context' => $cloud_context,
          'namespace' => $namespace->getName(),
        ]);
      }
    }
  }

  /**
   * Queuing items for deleting resource store entities.
   */
  public function deleteK8sResourceStoreEntity(): void {
    $k8s_clusters = $this->cloudConfigPluginManager->loadConfigEntities('k8s');
    if (empty($k8s_clusters)) {
      return;
    }

    foreach ($k8s_clusters ?: [] as $k8s_cluster) {
      $queue = $this->cloudService->createQueue($this->cloudService->getResourceQueueName($k8s_cluster));
      $cloud_context = $k8s_cluster->getCloudContext();
      $metrics_duration = !empty($k8s_cluster) && !empty($k8s_cluster->get('field_metrics_duration'))
        ? $k8s_cluster->get('field_metrics_duration')->value
        : 0;
      if ($metrics_duration < 1) {
        continue;
      }
      $queue->createItem([
        'k8s_method_name' => 'deleteK8sCloudStore',
        'cloud_context' => $cloud_context,
        'metrics_duration' => $metrics_duration,
      ]);
    }
  }

  /**
   * Extract data based on time range.
   *
   * @param string $cloud_store_type
   *   Cloud store type.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The name of the cost store.
   * @param int $from_time
   *   The unix timestamp of the starting time.
   * @param int $to_time
   *   The unix timestamp of the ending time.
   * @param string $cost_type
   *   The cost type.
   *
   * @return array
   *   The result including resources, cost and unix time.
   */
  public function extract($cloud_store_type, $cloud_context, $name, $from_time, $to_time, $cost_type = NULL): array {
    $storage = $this->entityTypeManager->getStorage('cloud_store');
    $query = $storage
      ->getQuery()
      ->accessCheck(TRUE);
    $query->condition('type', $cloud_store_type);
    if (!empty($cloud_context)) {
      $query->condition('cloud_context', $cloud_context);
    }
    if (!empty($name)) {
      $query->condition('name', $name);
    }

    $query->condition('type', $cloud_store_type);
    // Adding from/to time to query to avoid loading every row.
    $query->condition('created', $from_time, '>');
    $query->condition('created', $to_time, '<');
    $query->sort('created');
    $entity_ids = $query->execute();
    $entities = $storage->loadMultiple($entity_ids);

    $result = [];
    $total_cost = 0;
    foreach ($entities ?: [] as $entity) {
      $created_time = $entity->get('created')->value;
      if ($to_time < $created_time || $created_time <= $from_time) {
        continue;
      }
      $resources = Yaml::decode(!empty($entity->get('field_resources')->value) ? $entity->get('field_resources')->value : '');
      $result[] = [
        'resources' => $resources,
        'name' => $entity->getName(),
        'created' => $created_time,
      ];
      if ($entity->hasField('field_costs')) {
        $costs = Yaml::decode(!empty($entity->get('field_costs')->value) ? $entity->get('field_costs')->value : '');
        $cost = !empty($costs[$cost_type]) ? $costs[$cost_type] : 0;
        $result[count($result) - 1]['cost'] = $cost;
        $total_cost += $cost;
      }
    }
    $result['total_cost'] = $total_cost;
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function updateCostStoreEntity(): void {
    $entity_type_manager = $this->entityTypeManager;
    $k8s_service = $this->k8sService;
    // Get target entities to be updated.
    $project_entities = $this->entityTypeManager
      ->getStorage('cloud_project')
      ->loadByProperties([]);

    if (empty($project_entities)) {
      return;
    }

    foreach ($project_entities ?: [] as $project_entity) {
      $label = $project_entity->label();
      $k8s_clusters = $project_entity->get('field_k8s_clusters');

      // Check whether namespace exists or not.
      $namespace_entities = $this->entityTypeManager->getStorage('k8s_namespace')
        ->loadByProperties([
          'name' => $label,
        ]);
      if (empty($namespace_entities)) {
        continue;
      }

      // Get latest entity.
      $store_ids = $this->entityTypeManager->getStorage('cloud_store')
        ->getQuery()
        ->accessCheck(TRUE)
        ->condition('name', $label)
        ->condition('type', 'k8s_cost_store')
        ->condition('name', $label)
        ->sort('created', 'ASC')
        ->execute();

      $store_entities = $this->entityTypeManager->getStorage('cloud_store')->loadMultiple($store_ids);
      $store_entity = array_pop($store_entities);

      // To check time difference between current and latest update time
      // in order to decide whether to update or not.
      $latest_time = time() - (60 * 60 * 24);
      if (!empty($store_entity)) {
        $latest_time = $store_entity->get('created')->value;
        $time_diff = time() - $latest_time;

        // If previous update is within the specific range,
        // update process will be skipped.
        if ($time_diff < $this->updateInterval) {
          continue;
        }
      }

      $result[$label] = [];
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_context = $k8s_cluster->value;
        // Round the time to get k8s namespace resource stores created within
        // seconds defined as update cost store interval. That is, an entity of
        // k8s_cost_store is created by combining the data of
        // k8s_namespace_resource_store within the time defined by
        // k8s_update_cost_store_cron_time. Without this logic, one entity of
        // k8s_cost_store will be created for each k8s_namespace_resource_store.
        $current_time = floor(time() / $this->updateInterval) * $this->updateInterval;
        $ids = $this->entityTypeManager
          ->getStorage('cloud_store')
          ->getQuery()
          ->accessCheck(TRUE)
          ->condition('type', 'k8s_namespace_resource_store')
          ->condition('cloud_context', $cloud_context)
          ->condition('name', $label)
          ->condition('created', [$latest_time, $current_time], 'BETWEEN')
          ->execute();
        $resource_entities = $this->entityTypeManager->getStorage('cloud_store')
          ->loadMultiple($ids);

        // If no resource entity, it tries to catch the resource info from k8s.
        if (empty($resource_entities)) {
          $namespace_entities = $this->entityTypeManager
            ->getStorage('k8s_namespace')->loadByProperties([
              'cloud_context' => $cloud_context,
              'name' => $label,
            ]);
          foreach ($namespace_entities ?: [] as $namespace_entity) {
            $namespace = $namespace_entity->getName();
            $k8s_service->updateK8sNamespaceResourceStoreEntity($cloud_context, $namespace);
          }
          continue;
        }

        // Count cost in specific time range from all k8s clusters.
        foreach ($resource_entities ?: [] as $resource_entity) {
          $created_time = $resource_entity->get('created')->value;
          // Round the time to bundle resource entities created within
          // seconds defined as update cost store interval.
          // See also the above comment.
          $time_range = ceil($created_time / $this->updateInterval) * $this->updateInterval;
          if (!isset($result[$label][$time_range])) {
            $result[$label][$time_range] = [
              'total_cost' => [],
              'cost_type' => '',
              'count' => 0,
              'resources' => [
                'cpu' => 0,
                'memory' => 0,
                'pod_allocation' => 0,
              ],
            ];
          }

          $costs = Yaml::decode($resource_entity->get('field_costs')->value);
          foreach ($costs ?: [] as $cost_type => $cost) {
            if (empty([$label][$time_range]['total_cost'][$cost_type])) {
              $result[$label][$time_range]['total_cost'][$cost_type] = $cost;
              continue;
            }
            $result[$label][$time_range]['total_cost'][$cost_type] += $cost;
          }
          $result[$label][$time_range]['count'] += 1;
          $resources = Yaml::decode($resource_entity->get('field_resources')->value);
          $result[$label][$time_range]['resources']['cpu'] += $resources['cpu'];
          $result[$label][$time_range]['resources']['memory'] += $resources['memory'];
          $result[$label][$time_range]['resources']['pod_allocation'] += $resources['pod_allocation'];

          $result[$label][$time_range]['cloud_context'] = $cloud_context;
        }
      }

      // Update cost entity.
      foreach ($result[$label] ?: [] as $key => $value) {
        $cpu_usage_avg = 0;
        $memory_usage_avg = 0;
        $pod_usage_avg = 0;
        $avg_cost = !empty($value['total_cost'] && is_array($value['total_cost']))
          ? $value['total_cost'] : [];
        $count = $value['count'];

        array_walk($avg_cost, static function (&$cost, $cost_type) use ($count) {
          $cost = $cost > 0 && $count > 0
            ? (float) ($cost / $count) : 0;
        });

        if ($value['resources']['cpu'] > 0) {
          $cpu_usage_avg = (float) ($value['resources']['cpu'] / $value['count']);
        }
        if ($value['resources']['memory'] > 0) {
          $memory_usage_avg = (float) ($value['resources']['memory'] / $value['count']);
        }
        if ($value['resources']['pod_allocation'] > 0) {
          $pod_usage_avg = (float) ($value['resources']['pod_allocation'] / $value['count']);
        }
        $resources = [
          'cpu_usage_avg' => $cpu_usage_avg,
          'memory_usage_avg' => $memory_usage_avg,
          'pod_usage_avg' => $pod_usage_avg,
          'count' => $value['count'],
        ];

        $data = [
          'name' => $label,
          'cloud_context' => $value['cloud_context'],
          'type' => 'k8s_cost_store',
          'field_costs' => Yaml::encode($avg_cost),
          'field_resources' => Yaml::encode($resources),
          'created' => $key,
          'uid' => $this->k8sService->getDefaultCloudStoreUid($value['cloud_context']),
        ];
        // Update cost store entity.
        $entity = $entity_type_manager->getStorage('cloud_store')->create($data);
        $entity->save();
      }
    }
  }

}
