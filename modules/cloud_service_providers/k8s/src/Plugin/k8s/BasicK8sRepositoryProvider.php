<?php

namespace Drupal\k8s\Plugin\k8s;

use Drupal\k8s\Service\K8sClientExtension\Repositories\ApiServiceRepository;
use Drupal\k8s\Service\K8sClientExtension\Repositories\ClusterRoleBindingRepository;
use Drupal\k8s\Service\K8sClientExtension\Repositories\ClusterRoleRepository;
use Drupal\k8s\Service\K8sClientExtension\Repositories\K8sQuotaRepository;
use Drupal\k8s\Service\K8sClientExtension\Repositories\LimitRangeRepository;
use Drupal\k8s\Service\K8sClientExtension\Repositories\MetricsNodeRepository;
use Drupal\k8s\Service\K8sClientExtension\Repositories\MetricsPodRepository;
use Drupal\k8s\Service\K8sClientExtension\Repositories\PriorityClassRepository;
use Drupal\k8s\Service\K8sClientExtension\Repositories\StatefulSetRepository;
use Drupal\k8s\Service\K8sClientExtension\Repositories\StorageClassRepository;

/**
 * Defines the basic k8s repository provider.
 *
 * @K8sRepositoryProvider(
 *   id = "basic_k8s_repository_provider"
 * )
 */
class BasicK8sRepositoryProvider extends K8sRepositoryProviderBase {

  /**
   * {@inheritdoc}
   */
  public function getResourceMapping(): array {
    return [
      'apiServices' => ApiServiceRepository::class,
      'clusterRoles' => ClusterRoleRepository::class,
      'clusterRoleBindings' => ClusterRoleBindingRepository::class,
      'limitRanges' => LimitRangeRepository::class,
      'metricsNodes' => MetricsNodeRepository::class,
      'metricsPods' => MetricsPodRepository::class,
      'priorityClasses' => PriorityClassRepository::class,
      'quotas' => K8sQuotaRepository::class,
      'storageClasses' => StorageClassRepository::class,
      'statefulSets' => StatefulSetRepository::class,
    ];
  }

}
