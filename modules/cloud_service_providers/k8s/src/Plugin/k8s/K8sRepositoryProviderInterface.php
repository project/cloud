<?php

namespace Drupal\k8s\Plugin\k8s;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for K8sRepositoryProvider plugins.
 *
 * @see \Drupal\k8s\Annotation\K8sRepositoryProvider
 * @see \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderBase
 * @see \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderManager
 * @see plugin_api
 */
interface K8sRepositoryProviderInterface extends PluginInspectionInterface {

  /**
   * Get resource mapping.
   *
   * @return array
   *   The resource mapping.
   */
  public function getResourceMapping(): array;

}
