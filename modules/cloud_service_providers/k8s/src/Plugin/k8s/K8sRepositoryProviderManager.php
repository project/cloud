<?php

namespace Drupal\k8s\Plugin\k8s;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\k8s\Annotation\K8sRepositoryProvider;

/**
 * Provides a K8sRepositoryProvider manager.
 *
 * @see \Drupal\k8s\Annotation\K8sRepositoryProvider
 * @see \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderBase
 * @see \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderInterface
 * @see plugin_api
 */
class K8sRepositoryProviderManager extends DefaultPluginManager {

  /**
   * Constructs a K8sRepositoryProviderManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/k8s', $namespaces, $module_handler, K8sRepositoryProviderInterface::class, K8sRepositoryProvider::class);
    $this->alterInfo('k8s_repository_provider');
    $this->setCacheBackend($cache_backend, 'k8s:k8s_repository_provider');
  }

  /**
   * Get all resource mappings.
   *
   * @return array
   *   The results of all resource mappings.
   */
  public function getAllResourceMappings(): array {
    $mappings = [];
    foreach ($this->getDefinitions() ?: [] as $id => $definition) {
      $plugin = $this->createInstance($id);
      $mappings[] = $plugin->getResourceMapping();
    }
    return array_merge([], ...$mappings);
  }

}
