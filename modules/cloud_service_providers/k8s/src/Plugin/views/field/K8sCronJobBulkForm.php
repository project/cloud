<?php

namespace Drupal\k8s\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a CronJob operations bulk form element.
 *
 * @ViewsField("cron_job_bulk_form")
 */
class K8sCronJobBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No CronJob selected.');
  }

}
