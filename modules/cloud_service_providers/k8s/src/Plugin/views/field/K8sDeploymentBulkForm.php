<?php

namespace Drupal\k8s\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Deployment operations bulk form element.
 *
 * @ViewsField("deployment_bulk_form")
 */
class K8sDeploymentBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No Deployment selected.');
  }

}
