<?php

namespace Drupal\k8s\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a horizontal pod autoscaler operations bulk form element.
 *
 * @ViewsField("horizontal_pod_autoscaler_bulk_form")
 */
class K8sHorizontalPodAutoscalerBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No horizontal pod autoscaler selected.');
  }

}
