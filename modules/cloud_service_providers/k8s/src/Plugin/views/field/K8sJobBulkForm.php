<?php

namespace Drupal\k8s\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Job operations bulk form element.
 *
 * @ViewsField("job_bulk_form")
 */
class K8sJobBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No Job selected.');
  }

}
