<?php

namespace Drupal\k8s\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a persistent volume operations bulk form element.
 *
 * @ViewsField("persistent_volume_bulk_form")
 */
class K8sPersistentVolumeBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No persistent volume selected.');
  }

}
