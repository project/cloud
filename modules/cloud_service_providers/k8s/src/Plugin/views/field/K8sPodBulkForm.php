<?php

namespace Drupal\k8s\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Pod operations bulk form element.
 *
 * @ViewsField("pod_bulk_form")
 */
class K8sPodBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No Pod selected.');
  }

}
