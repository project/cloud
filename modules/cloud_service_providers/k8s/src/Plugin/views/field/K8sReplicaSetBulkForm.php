<?php

namespace Drupal\k8s\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a ReplicaSet operations bulk form element.
 *
 * @ViewsField("replica_set_bulk_form")
 */
class K8sReplicaSetBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No ReplicaSet selected.');
  }

}
