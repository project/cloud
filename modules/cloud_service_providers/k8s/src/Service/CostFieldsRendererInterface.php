<?php

namespace Drupal\k8s\Service;

/**
 * Provides CostFieldsRenderer interface.
 */
interface CostFieldsRendererInterface {

  /**
   * Render cost fields.
   *
   * @param string $region
   *   The region.
   * @param array $instance_types
   *   The instance types.
   * @param bool $refresh
   *   TRUE to refresh the data.
   *
   * @return array
   *   The build array of cost fields.
   */
  public function render(
    $region,
    array $instance_types,
    $refresh = TRUE,
  ): array;

}
