<?php

namespace Drupal\k8s\Service\K8sClientExtension\Collections;

use Drupal\k8s\Service\K8sClientExtension\Models\ApiService;
use Maclof\Kubernetes\Collections\Collection;

/**
 * K8s api services collection.
 */
class ApiServiceCollection extends Collection {

  /**
   * The constructor.
   *
   * @param array $items
   *   The items.
   */
  public function __construct(array $items) {
    parent::__construct($this->getApiServices($items));
  }

  /**
   * Get an array of api services.
   *
   * @param array $items
   *   The items.
   *
   * @return array
   *   The array of roles.
   */
  protected function getApiServices(array $items): array {
    foreach ($items ?: [] as &$item) {
      if ($item instanceof ApiService) {
        continue;
      }

      $item = new ApiService($item);
    }

    return $items;
  }

}
