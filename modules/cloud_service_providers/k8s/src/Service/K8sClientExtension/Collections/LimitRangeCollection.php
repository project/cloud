<?php

namespace Drupal\k8s\Service\K8sClientExtension\Collections;

use Drupal\k8s\Service\K8sClientExtension\Models\LimitRange;
use Maclof\Kubernetes\Collections\Collection;

/**
 * K8s LimitRanges collection.
 */
class LimitRangeCollection extends Collection {

  /**
   * The constructor.
   *
   * @param array $items
   *   The items.
   */
  public function __construct(array $items) {
    parent::__construct($this->getLimitRanges($items));
  }

  /**
   * Get an array of K8s LimitRanges.
   *
   * @param array $items
   *   The items.
   *
   * @return array
   *   The array of K8s LimitRanges.
   */
  protected function getLimitRanges(array $items): array {
    foreach ($items ?: [] as &$item) {
      if ($item instanceof LimitRange) {
        continue;
      }

      $item = new LimitRange($item);
    }

    return $items;
  }

}
