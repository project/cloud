<?php

namespace Drupal\k8s\Service\K8sClientExtension\Collections;

use Drupal\k8s\Service\K8sClientExtension\Models\MetricsNode;
use Maclof\Kubernetes\Collections\Collection;

/**
 * K8s metrics nodes collection.
 */
class MetricsNodeCollection extends Collection {

  /**
   * The constructor.
   *
   * @param array $items
   *   The items.
   */
  public function __construct(array $items) {
    parent::__construct($this->getMetricsNodes($items));
  }

  /**
   * Get an array of metrics nodes.
   *
   * @param array $items
   *   The items.
   *
   * @return array
   *   The array of metrics nodes.
   */
  protected function getMetricsNodes(array $items): array {
    foreach ($items ?: [] as &$item) {
      if ($item instanceof MetricsNode) {
        continue;
      }

      $item = new MetricsNode($item);
    }

    return $items;
  }

}
