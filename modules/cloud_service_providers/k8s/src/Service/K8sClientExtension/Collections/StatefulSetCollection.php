<?php

namespace Drupal\k8s\Service\K8sClientExtension\Collections;

use Drupal\k8s\Service\K8sClientExtension\Models\StatefulSet;
use Maclof\Kubernetes\Collections\Collection;

/**
 * K8s StatefulSets collection.
 */
class StatefulSetCollection extends Collection {

  /**
   * The constructor.
   *
   * @param array $items
   *   The items.
   */
  public function __construct(array $items) {
    parent::__construct($this->getStatefulSets($items));
  }

  /**
   * Get an array of K8s StatefulSets.
   *
   * @param array $items
   *   The items.
   *
   * @return array
   *   The array of K8s StatefulSets.
   */
  protected function getStatefulSets(array $items): array {
    foreach ($items ?: [] as &$item) {
      if ($item instanceof StatefulSet) {
        continue;
      }

      $item = new StatefulSet($item);
    }

    return $items;
  }

}
