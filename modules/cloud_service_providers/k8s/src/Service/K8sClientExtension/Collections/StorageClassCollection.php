<?php

namespace Drupal\k8s\Service\K8sClientExtension\Collections;

use Drupal\k8s\Service\K8sClientExtension\Models\StorageClass;
use Maclof\Kubernetes\Collections\Collection;

/**
 * K8s storage classes collection.
 */
class StorageClassCollection extends Collection {

  /**
   * The constructor.
   *
   * @param array $items
   *   The items.
   */
  public function __construct(array $items) {
    parent::__construct($this->getStorageClasses($items));
  }

  /**
   * Get an array of storage classes.
   *
   * @param array $items
   *   The items.
   *
   * @return array
   *   The array of storage classes.
   */
  protected function getStorageClasses(array $items): array {
    foreach ($items ?: [] as &$item) {
      if ($item instanceof StorageClass) {
        continue;
      }

      $item = new StorageClass($item);
    }

    return $items;
  }

}
