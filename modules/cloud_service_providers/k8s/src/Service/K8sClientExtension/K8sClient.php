<?php

namespace Drupal\k8s\Service\K8sClientExtension;

use Drupal\cloud\Service\CloudResourceTagInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\k8s\Service\K8sServiceException;
use Maclof\Kubernetes\Client;
use Maclof\Kubernetes\Exceptions\ApiServerException;
use Maclof\Kubernetes\Exceptions\BadRequestException;

/**
 * K8s client.
 */
class K8sClient extends Client {

  use CloudContentEntityTrait;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\k8s\Service\K8sServiceException
   *    Thrown when unable to sendRequest.
   */
  public function sendRequest(
    $method,
    $uri,
    $query = [],
    $body = NULL,
    $namespace = TRUE,
    $apiVersion = NULL,
    array $requestOptions = [],
  ) {

    // Normalize $body to either NULL or a non-empty string if appropriate.
    $body = empty($body) && !is_bool($body) && !is_numeric($body) ? NULL : $body;
    if (!is_null($body) && !is_string($body)) {
      throw new \InvalidArgumentException('A variable $body must be a string, array, or NULL');
    }

    try {
      // If the resource type is K8s role or cluster role, the apiGroups maybe
      // empty, then it cannot be removed.
      if (!empty($body)
      && in_array($method, ['PUT', 'POST'])
      && !in_array($uri, ['/roles', '/clusterroles'], TRUE)) {
        $array = $this->removeEmptyProperties(json_decode($body, TRUE));
        $body = json_encode($array, JSON_PRETTY_PRINT);
      }

      // Fallback for namespace availability.
      $namespace = !empty($this->namespace);

      // Handling different response statuses.
      $response = parent::sendRequest(
        $method,
        $uri,
        $query,
        $body,
        $namespace,
        $apiVersion,
        $requestOptions
      );

      if (empty($response['code'])) {
        return $response;
      }

      if ($response['code'] === 400) {
        throw new BadRequestException(json_encode($response), $response['code']);
      }

      throw new ApiServerException(
        !empty($response['message']) ? $response['message'] : '',
        $response['code']
      );
    }
    catch (BadRequestException $e) {
      $error_info = json_decode($e->getMessage(), TRUE);
      if (empty($error_info)) {
        // NOTE: According to Drupal coding standards,
        // exceptions should not be translated.
        throw new K8sServiceException('Unknown error occurred when calling K8s API.');
      }

      $this->messenger()->addError($this->t('An error occurred when calling K8s API: @method @uri', [
        '@method' => $method,
        '@uri' => $uri,
      ]));

      $this->messenger()->addError($this->t('Status Code: @status_code', [
        '@status_code' => $error_info['code'],
      ]));

      $this->messenger()->addError($this->t('Error reason: @error_reason', [
        '@error_reason' => $error_info['reason'],
      ]));

      $this->messenger()->addError($this->t('Message: @msg', [
        '@msg' => $error_info['message'],
      ]));

      // NOTE: According to Drupal coding standards,
      // exceptions should not be translated.
      throw new K8sServiceException('An error occurred when calling K8s API.');
    }
    catch (\Exception $e) {
      throw new K8sServiceException($e->getMessage());
    }
  }

  /**
   * Remove empty properties.
   *
   * @param array $haystack
   *   The array.
   *
   * @return array
   *   The array whose empty properties were removed.
   */
  private function removeEmptyProperties(array $haystack): array {
    foreach ($haystack ?: [] as $key => $value) {
      if (is_array($value)) {
        $haystack[$key] = $this->removeEmptyProperties($value);
      }
      // The uid annotation can be assigned 0 (anonymous user) during user
      // delete operation.  Do not unset a string that starts with `drupal-k8s.`
      // which is the tag used to store UIDs in K8s.
      if (!str_starts_with($key, CloudResourceTagInterface::PREFIX_UID_TAG_NAME['k8s']) && empty($haystack[$key])) {
        unset($haystack[$key]);
      }
    }

    return $haystack;
  }

}
