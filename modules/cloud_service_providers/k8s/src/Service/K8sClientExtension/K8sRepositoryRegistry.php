<?php

namespace Drupal\k8s\Service\K8sClientExtension;

use Drupal\k8s\Plugin\k8s\K8sRepositoryProviderManager;
use Maclof\Kubernetes\RepositoryRegistry;

/**
 * K8s repository registry.
 */
class K8sRepositoryRegistry extends RepositoryRegistry {

  /**
   * Constructs a new constraint validator.
   *
   * @param \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderManager $k8s_repository_provider_manager
   *   The K8s repository provider manager.
   */
  public function __construct(K8sRepositoryProviderManager $k8s_repository_provider_manager) {

    parent::__construct();

    $this->map = array_merge($this->map, $k8s_repository_provider_manager->getAllResourceMappings());
  }

}
