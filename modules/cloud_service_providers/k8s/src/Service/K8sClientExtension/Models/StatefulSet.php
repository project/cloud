<?php

namespace Drupal\k8s\Service\K8sClientExtension\Models;

use Maclof\Kubernetes\Models\Model;

/**
 * K8s StatefulSet model.
 */
class StatefulSet extends Model {

  /**
   * The api version.
   *
   * @var string
   */
  protected string $apiVersion = 'apps/v1';

}
