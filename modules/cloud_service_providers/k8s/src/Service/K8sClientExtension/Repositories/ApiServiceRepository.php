<?php

namespace Drupal\k8s\Service\K8sClientExtension\Repositories;

use Drupal\k8s\Service\K8sClientExtension\Collections\ApiServiceCollection;
use Maclof\Kubernetes\Repositories\Repository;

/**
 * K8s cluster api service repository.
 */
class ApiServiceRepository extends Repository {

  /**
   * The uri.
   *
   * @var string
   */
  protected string $uri = 'apiservices';

  /**
   * {@inheritdoc}
   */
  protected function createCollection(array $response): ApiServiceCollection {
    return new ApiServiceCollection($response['items']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiVersion(): string {
    return 'apiregistration.k8s.io/v1';
  }

}
