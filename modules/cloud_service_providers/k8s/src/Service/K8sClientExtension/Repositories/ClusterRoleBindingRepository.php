<?php

namespace Drupal\k8s\Service\K8sClientExtension\Repositories;

use Drupal\k8s\Service\K8sClientExtension\Collections\ClusterRoleBindingCollection;
use Maclof\Kubernetes\Repositories\Repository;

/**
 * K8s cluster role binding repository.
 */
class ClusterRoleBindingRepository extends Repository {

  /**
   * The uri.
   *
   * @var string
   */
  protected string $uri = 'clusterrolebindings';

  /**
   * {@inheritdoc}
   */
  protected function createCollection(array $response): ClusterRoleBindingCollection {
    return new ClusterRoleBindingCollection($response['items']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiVersion(): string {
    return 'rbac.authorization.k8s.io/v1';
  }

}
