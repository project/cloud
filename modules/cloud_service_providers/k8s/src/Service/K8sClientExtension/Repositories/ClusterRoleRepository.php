<?php

namespace Drupal\k8s\Service\K8sClientExtension\Repositories;

use Drupal\k8s\Service\K8sClientExtension\Collections\ClusterRoleCollection;
use Maclof\Kubernetes\Repositories\Repository;

/**
 * K8s cluster role repository.
 */
class ClusterRoleRepository extends Repository {

  /**
   * The uri.
   *
   * @var string
   */
  protected string $uri = 'clusterroles';

  /**
   * {@inheritdoc}
   */
  protected function createCollection(array $response): ClusterRoleCollection {
    return new ClusterRoleCollection($response['items']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiVersion(): string {
    return 'rbac.authorization.k8s.io/v1';
  }

}
