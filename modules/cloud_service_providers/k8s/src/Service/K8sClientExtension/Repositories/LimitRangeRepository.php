<?php

namespace Drupal\k8s\Service\K8sClientExtension\Repositories;

use Drupal\k8s\Service\K8sClientExtension\Collections\LimitRangeCollection;
use Maclof\Kubernetes\Repositories\Repository;

/**
 * K8s LimitRange repository.
 */
class LimitRangeRepository extends Repository {

  /**
   * The uri.
   *
   * @var string
   */
  protected string $uri = 'limitranges';

  /**
   * {@inheritdoc}
   */
  protected function createCollection(array $response): LimitRangeCollection {
    return new LimitRangeCollection($response['items']);
  }

}
