<?php

namespace Drupal\k8s\Service\K8sClientExtension\Repositories;

use Drupal\k8s\Service\K8sClientExtension\Collections\MetricsNodeCollection;
use Maclof\Kubernetes\Repositories\Repository;

/**
 * K8s node repository for metrics.
 */
class MetricsNodeRepository extends Repository {

  /**
   * The uri.
   *
   * @var string
   */
  protected string $uri = 'nodes';

  /**
   * Whether using a namespace or not.
   *
   * @var bool
   */
  protected bool $namespace = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function createCollection($response): MetricsNodeCollection {
    // K8s API server may return a string '404 page not found', not an array.
    return new MetricsNodeCollection(is_array($response)
      ? $response['items']
      : ['items' => []]
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiVersion(): string {
    return 'metrics.k8s.io/v1beta1';
  }

}
