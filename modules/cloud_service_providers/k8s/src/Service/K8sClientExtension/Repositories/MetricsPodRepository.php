<?php

namespace Drupal\k8s\Service\K8sClientExtension\Repositories;

use Drupal\k8s\Service\K8sClientExtension\Collections\MetricsPodCollection;
use Maclof\Kubernetes\Repositories\Repository;

/**
 * K8s pod repository for metrics.
 */
class MetricsPodRepository extends Repository {

  /**
   * The uri.
   *
   * @var string
   */
  protected string $uri = 'pods';

  /**
   * {@inheritdoc}
   */
  protected function createCollection($response): MetricsPodCollection {
    // K8s API server may return a string '404 page not found', not an array.
    return new MetricsPodCollection(is_array($response)
      ? $response['items']
      : ['items' => []]
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiVersion(): string {
    return 'metrics.k8s.io/v1beta1';
  }

}
