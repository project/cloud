<?php

namespace Drupal\k8s\Service\K8sClientExtension\Repositories;

use Drupal\k8s\Service\K8sClientExtension\Collections\PriorityClassCollection;
use Maclof\Kubernetes\Repositories\Repository;

/**
 * K8s cluster priority class repository.
 */
class PriorityClassRepository extends Repository {

  /**
   * The uri.
   *
   * @var string
   */
  protected string $uri = 'priorityclasses';

  /**
   * Whether using namespace or not.
   *
   * @var bool
   */
  protected bool $namespace = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function createCollection(array $response): PriorityClassCollection {
    return new PriorityClassCollection($response['items']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiVersion(): string {
    // @FIXME refactor to avoid duplicate apiVersion def.
    return 'scheduling.k8s.io/v1';
  }

}
