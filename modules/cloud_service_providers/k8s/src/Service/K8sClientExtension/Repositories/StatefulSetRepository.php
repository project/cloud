<?php

namespace Drupal\k8s\Service\K8sClientExtension\Repositories;

use Drupal\k8s\Service\K8sClientExtension\Collections\StatefulSetCollection;
use Maclof\Kubernetes\Repositories\Repository;

/**
 * K8s StatefulSet repository.
 */
class StatefulSetRepository extends Repository {

  /**
   * The uri.
   *
   * @var string
   */
  protected string $uri = 'statefulsets';

  /**
   * {@inheritdoc}
   */
  protected function createCollection(array $response): StatefulSetCollection {
    return new StatefulSetCollection($response['items']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiVersion(): string {
    return 'apps/v1';
  }

}
