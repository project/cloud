<?php

namespace Drupal\k8s\Service\K8sClientExtension\Repositories;

use Drupal\k8s\Service\K8sClientExtension\Collections\StorageClassCollection;
use Maclof\Kubernetes\Repositories\Repository;

/**
 * K8s storage class repository.
 */
class StorageClassRepository extends Repository {

  /**
   * The uri.
   *
   * @var string
   */
  protected string $uri = 'storageclasses';

  /**
   * {@inheritdoc}
   */
  protected function createCollection(array $response): StorageClassCollection {
    return new StorageClassCollection($response['items']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiVersion(): string {
    return 'storage.k8s.io/v1';
  }

}
