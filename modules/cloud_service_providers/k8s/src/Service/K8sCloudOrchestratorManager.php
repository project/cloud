<?php

namespace Drupal\k8s\Service;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Service\CloudOrchestratorManagerInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * K8s cloud orchestrator manager.
 */
class K8sCloudOrchestratorManager implements CloudOrchestratorManagerInterface {

  use CloudContentEntityTrait;

  /**
   * The K8s Service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  protected $k8sService;

  /**
   * Entity field manager interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The K8sCloudOrchestratorManager constructor.
   *
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   The K8s Service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    K8sServiceInterface $k8s_service,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
  ) {

    $this->k8sService = $k8s_service;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;

    $this->messenger();
  }

  /**
   * {@inheritdoc}
   */
  public function deploy($cloud_context, $manifest, $parameters): array {
    $cloud_resources = [];
    $this->k8sService->setCloudContext($cloud_context);
    $object_types = $this->k8sService->supportedCloudLaunchTemplates();

    $yamls = $this->k8sService->decodeMultipleDocYaml($manifest);
    foreach ($yamls ?: [] as $yaml) {
      $kind = $yaml['kind'];
      $object_type = array_search($kind, $object_types);

      // Get the name of method create<ResourceName>.
      $short_label = self::getCamelCaseWithWhitespace($object_type);
      $name_camel = self::getCamelCaseWithoutWhitespace($short_label);
      $create_method_name = "create{$name_camel}";

      $entity_type_id = "k8s_$object_type";
      $fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $entity_type_id);

      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      $name = $yaml['metadata']['name'];

      $has_namespace = !empty($fields['namespace']);
      $has_namespace
      ? $this->k8sService->$create_method_name(
        $yaml['metadata']['namespace'],
        $yaml
      )
      : $this->k8sService->$create_method_name(
        $yaml
      );

      // Get the name of method update<ResourceName>s.
      $name_plural_camel = $this->getShortEntityTypeNamePluralCamelByType($entity_type);
      $update_method_name = "update{$name_plural_camel}";

      $update_method_params = [
        'metadata.name' => $name,
      ];
      if ($has_namespace) {
        $update_method_params['metadata.namespace'] = $yaml['metadata']['namespace'];
      }
      $this->k8sService->$update_method_name($update_method_params, FALSE);

      $params = [
        'cloud_context' => $cloud_context,
        'name' => $name,
      ];
      if ($has_namespace) {
        $params['namespace'] = $yaml['metadata']['namespace'];
      }

      $entities = $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->loadByProperties($params);

      if (empty($entities)) {
        continue;
      }

      $entity = reset($entities);

      // Update creation_yaml.
      if (method_exists($entity, 'setCreationYaml')) {
        $entity->setCreationYaml(Yaml::encode($yaml));
        $entity->save();
      }

      $this->processOperationStatus($entity, 'created');

      $cloud_resources[] = [
        'entity_type_id' => $entity->getEntityTypeId(),
        'entity_id' => $entity->id(),
      ];
    }

    return $cloud_resources;
  }

  /**
   * {@inheritdoc}
   */
  public function undeploy(array $entities): void {
    foreach (array_reverse($entities) as $entity) {
      $this->k8sService->setCloudContext($entity->getCloudContext());
      $params = [
        'metadata' => [
          'name' => $entity->getName(),
        ],
      ];

      // Delete cloud resource.
      $name_camel = $this->getShortEntityTypeNameCamel($entity);
      $delete_method = "delete{$name_camel}";
      method_exists($entity, 'getNamespace')
        ? $this->k8sService->$delete_method($entity->getNamespace(), $params)
        : $this->k8sService->$delete_method($params);

      $entity->delete();

      $this->processOperationStatus($entity, 'deleted');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoint(array $entities): string {
    foreach ($entities as $entity) {
      if ($entity->getEntityTypeId() !== 'k8s_service' || $entity->getName() !== 'cloud-orchestrator') {
        continue;
      }

      $endpoints = $entity->getExternalEndpoints();
      if (empty($endpoints)) {
        continue;
      }

      return 'http://' . $endpoints[0]['value'];
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function validateParameters(array $form, FormStateInterface $form_state, array $parameters): void {
    // Validate namespace.
    $entities = $this->entityTypeManager
      ->getStorage('k8s_namespace')
      ->loadByProperties([
        'cloud_context' => $form_state->getValue('target_provider'),
        'name' => $parameters['namespace'],
      ]);

    if (!empty($entities)) {
      $form_state->setError($form['parameters']['k8s']['namespace'], $this->t(
        'The namespace %namespace already exists. Please input another namespace.',
        ['%namespace' => $parameters['namespace']]
      ));
    }
  }

}
