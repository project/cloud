<?php

namespace Drupal\k8s\Service;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDeleteFormTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceBase;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\k8s\Entity\K8sSchedule;
use Drupal\k8s\Form\K8sContentFormInterface;
use Drupal\k8s\Traits\K8sFormTrait;

/**
 * Entity update methods for REST API processing.
 */
class K8sOperationsService extends CloudServiceBase implements K8sOperationsServiceInterface {

  use EntityDeleteFormTrait;
  use K8sFormTrait;

  /**
   * The K8s service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  private $k8sService;

  /**
   * Cloud service interface.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  private $cloudService;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The K8sOperationsService constructor.
   *
   * @param \Drupal\k8s\Service\K8sServiceInterface $k8s_service
   *   The K8s service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   */
  public function __construct(
    K8sServiceInterface $k8s_service,
    CloudServiceInterface $cloud_service,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityTypeManagerInterface $entity_type_manager,
  ) {

    parent::__construct();

    $this->k8sService = $k8s_service;
    $this->cloudService = $cloud_service;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function updateNamespace(ContentEntityInterface $entity, array $params, bool $batch = TRUE): bool {
    $labels = $params['labels'] ?? [];
    $annotations = $params['annotations'] ?? [];
    empty($labels) ?: $entity->setLabels($labels);
    empty($annotations) ?: $entity->setAnnotations($annotations);

    $this->k8sService->setCloudContext($entity->getCloudContext());
    $parameters = [
      'metadata' => [
        'name' => $entity->getName(),
        'labels' => $entity->getLabels(),
        'annotations' => $entity->getAnnotations(),
      ],
    ];

    if (!empty($entity->getOwner())) {
      // Add owner uid to annotations.
      $parameters['metadata']['annotations'][$this->cloudService->getTagKeyCreatedByUid('k8s', $entity->getCloudContext())] = $entity->getOwner()->id();
    }

    try {
      $result = $this->k8sService->updateNamespace($parameters);

      $entity->save();

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'updated remotely');
        return TRUE;
      }

      $this->refreshEntity($entity, $batch);

      $this->processOperationStatus($entity, 'updated');
    }
    catch (
      K8sServiceException
      | EntityStorageException
      | EntityMalformedException $e
    ) {

      try {
        $this->processOperationErrorStatus($entity, 'updated');
      }
      catch (EntityMalformedException $e) {
        $this->handleException($e);
      }

      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateSchedule(ContentEntityInterface $entity, array $params): bool {
    $startHour = $params['startHour'];
    $startMinute = $params['startMinute'];
    $stopHour = $params['stopHour'];
    $stopMinute = $params['stopMinute'];
    $entity->set('start_time', $this->getTimeFormat($startHour, $startMinute));
    $entity->set('stop_time', $this->getTimeFormat($stopHour, $stopMinute));
    $entity->save();
    $this->processOperationStatus($entity, 'updated');

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateEntity(ContentEntityInterface $entity, array $params, bool $batch = TRUE): bool {
    $detail = $params['detail'];
    $name_underscore = $this->getShortEntityTypeNameUnderscore($entity);
    $name_camel = $this->getShortEntityTypeNameCamel($entity);

    $this->k8sService->setCloudContext($entity->getCloudContext());
    try {
      $method_name = "update{$name_camel}";

      $parameters = Yaml::decode($detail);

      if (!empty($entity->getOwner())) {
        // Add owner uid to annotations.
        $parameters['metadata']['annotations'][$this->cloudService->getTagKeyCreatedByUid('k8s', $entity->getCloudContext())] = $entity->getOwner()->id();

        if ($name_underscore === 'deployment') {
          // Add owner uid to pod template.
          $parameters['spec']['template']['metadata']['annotations'][$this->cloudService->getTagKeyCreatedByUid('k8s', $entity->getCloudContext())] = $entity->getOwner()->id();
        }
      }

      $result = method_exists($entity, 'getNamespace')
        ? $this->k8sService->$method_name(
          $entity->getNamespace(),
          $parameters
        )
        : $this->k8sService->$method_name($parameters);

      $entity->save();

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'updated remotely');
        return TRUE;
      }

      // Update the entity.
      $this->refreshEntity($entity, $batch);

      $this->processOperationStatus($entity, 'updated');
    }
    catch (
      K8sServiceException
      | EntityStorageException
      | EntityMalformedException $e
    ) {

      try {
        $this->processOperationErrorStatus($entity, 'updated');
      }
      catch (EntityMalformedException $e) {
        $this->handleException($e);
      }

      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function refreshEntity(EntityInterface $entity, bool $batch = TRUE): void {
    $this->k8sService->setCloudContext($entity->getCloudContext());
    if ($entity->getEntityTypeId() === 'k8s_namespace') {
      $method_name = ($batch === TRUE)
        ? 'updateNamespaces'
        : 'updateNamespacesWithoutBatch';

      $this->k8sService->$method_name([
        'metadata.name' => $entity->getName(),
      ], FALSE);

      return;
    }

    $name_plural_camel = $this->getShortEntityTypeNamePluralCamel($entity);
    $method_name = ($batch === TRUE)
      ? "update{$name_plural_camel}"
      : "update{$name_plural_camel}WithoutBatch";

    $params = [
      'metadata.name' => $entity->getName(),
    ];
    // Add namespace into query if possible.  Some entities can have
    // the same name.  Add namespace to make the query more specific.
    if (method_exists($entity, 'getNamespace')) {
      $params['metadata.namespace'] = $entity->getNamespace();
    }
    $this->k8sService->$method_name($params, FALSE);

  }

  /**
   * Create a Secret based on $yaml.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param array $yaml
   *   The yaml array.
   *
   * @return bool
   *   Whether it was created or not.
   */
  private function createScheduleSecret(string $cloud_context, string $namespace, array $yaml): bool {
    // Check if a Secret has already been created.
    $result = $this->k8sService->getSecrets([
      'metadata.name' => $yaml['metadata']['name'],
      'metadata.namespace' => $namespace,
    ]);

    if (!empty($result)) {
      return FALSE;
    }

    // Create a Secret.
    $result = $this->k8sService->createSecret($namespace, $yaml);
    $this->k8sService->updateSecrets([
      'metadata.name' => $result['metadata']['name'],
    ], FALSE);

    $entity = $this->k8sService->getEntity(
      'k8s_secret',
      $cloud_context,
      $result['metadata']['name'],
      $namespace
    );

    $this->processOperationStatus($entity, 'created');

    return TRUE;
  }

  /**
   * Create a ServiceAccount based on $yaml.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param array $yaml
   *   The yaml array.
   *
   * @return bool
   *   Whether it was created or not .
   */
  private function createScheduleServiceAccount(string $cloud_context, string $namespace, array $yaml): bool {
    // Check if a ServiceAccount has already been created.
    $result = $this->k8sService->getServiceAccounts([
      'metadata.name' => $yaml['metadata']['name'],
      'metadata.namespace' => $namespace,
    ]);

    if (!empty($result)) {
      return FALSE;
    }

    // Create a ServiceAccount.
    $result = $this->k8sService->createServiceAccount($namespace, $yaml);
    $this->k8sService->updateServiceAccounts([
      'metadata.name' => $result['metadata']['name'],
    ], FALSE);

    $entity = $this->k8sService->getEntity(
      'k8s_service_account',
      $cloud_context,
      $result['metadata']['name'],
      $namespace
    );

    $this->processOperationStatus($entity, 'created');

    return TRUE;
  }

  /**
   * Create a ClusterRoleBinding based on $yaml.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $yaml
   *   The yaml array.
   *
   * @return bool
   *   Whether it was created or not.
   */
  private function createScheduleClusterRoleBinding(string $cloud_context, array $yaml): bool {
    // Check if a ClusterRoleBinding has already been created.
    $result = $this->k8sService->getClusterRoleBindings([
      'metadata.name' => $yaml['metadata']['name'],
    ]);

    if (!empty($result)) {
      return FALSE;
    }

    // Create a ClusterRoleBinding.
    $result = $this->k8sService->createClusterRoleBinding($yaml);
    $this->k8sService->updateClusterRoleBindings([
      'metadata.name' => $result['metadata']['name'],
    ], FALSE);

    $entity = $this->k8sService->getEntity(
      'k8s_cluster_role_binding',
      $cloud_context,
      $result['metadata']['name']
    );

    $this->processOperationStatus($entity, 'created');

    return TRUE;
  }

  /**
   * Create Service Account and Cluster role binding for CronJob Scheduler.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   */
  private function createResourcesForCronJobScheduler(string $cloud_context, string $namespace): void {
    // Create Service Account.
    $service_account_yaml = $this->k8sService->createScheduleServiceAccountYaml($namespace);
    if (!empty($service_account_yaml)) {
      $this->createScheduleServiceAccount($cloud_context, $namespace, $service_account_yaml);
    }

    // Create Cluster role binding.
    $cluster_role_binding_yaml = $this->k8sService->createScheduleClusterRoleBindingYaml($namespace);
    if (!empty($cluster_role_binding_yaml)) {
      $this->createScheduleClusterRoleBinding($cloud_context, $cluster_role_binding_yaml);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createGitCronJobFromYaml(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $name,
    string $resource_name,
    string $yaml_file_name,
    array $yaml,
    int $start_hour,
    int $start_minute,
    int $stop_hour,
    int $stop_minute,
  ): bool {
    $git_repository_value = $entity->get('field_git_repository')->getValue();
    $git_repository = !empty($git_repository_value) ? $git_repository_value[0]['uri'] : '';
    $git_branch_value = $entity->get('field_git_branch')->getValue();
    $git_branch = !empty($git_branch_value) ? $git_branch_value[0]['value'] : '';
    $git_username = $entity->get('field_git_username')->value ?? '';
    $git_access_token = $entity->get('field_git_access_token')->value ?? '';
    $resource_name = $this->getKebabCase($resource_name);

    if (empty($git_repository)) {
      $this->messenger->addError($this->t('The @git_repository is invalid.', [
        '@git_repository' => $git_repository,
      ]));
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (empty($git_branch)) {
      $this->messenger->addError($this->t('The @git_branch is invalid.', [
        '@git_branch' => $git_branch,
      ]));
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (empty($namespace)) {
      $this->messenger->addError($this->t('The namespace is empty.'));
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (empty($yaml)) {
      $this->messenger->addError($this->t('The yaml is empty.'));
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    $this->k8sService->setCloudContext($cloud_context);

    // Create Service Account and Cluster role binding.
    $this->createResourcesForCronJobScheduler($cloud_context, $namespace);

    // Create Secret.
    if (!empty($git_username) && !empty($git_access_token)) {
      $secret_yaml = $this->k8sService->createScheduleSecretYaml(
        $namespace,
        $git_username,
        $git_access_token
      );
      if (!empty($secret_yaml)) {
        $this->createScheduleSecret($cloud_context, $namespace, $secret_yaml);
      }
    }

    // Get CronJob parameters.
    $current_hour_minute = date('Hi');
    $start_hour_minute = sprintf('%02d%02d', $start_hour, $start_minute);
    $stop_hour_minute = sprintf('%02d%02d', $stop_hour, $stop_minute);

    $is_in_period = (
      ($start_hour_minute <= $stop_hour_minute)
      && ($current_hour_minute >= $start_hour_minute && $current_hour_minute <= $stop_hour_minute)
    )
    || (
      ($start_hour_minute > $stop_hour_minute)
      && ($current_hour_minute >= $start_hour_minute || $current_hour_minute <= $stop_hour_minute)
    );

    // Create the start CronJob.
    $start_cronjob_yaml = !empty($name)
      ? $this->k8sService->createScheduleCronJobYaml(
        $name,
        $namespace,
        $start_hour,
        $start_minute,
        'start',
        $resource_name
      )
      : [];
    if (empty($start_cronjob_yaml)
      || empty($this->k8sService->createScheduleCronJob($cloud_context, $namespace, $start_cronjob_yaml))) {
      return FALSE;
    }
    $entity = $this->k8sService->getEntity(
      'k8s_cron_job',
      $cloud_context,
      $start_cronjob_yaml['metadata']['name'],
      $namespace
    );
    $this->processOperationStatus($entity, 'created');

    // Create the CronJob.
    $create_cronjob_yaml = !empty($name) && !empty($git_repository) && !empty($git_branch)
      ? $this->k8sService->createGitScheduleCronJobYaml(
        $name,
        $namespace,
        $resource_name,
        $git_repository,
        $git_branch,
        $yaml_file_name,
        $git_username,
        empty($is_in_period)
      )
      : [];

    if (empty($create_cronjob_yaml)
      || empty($this->k8sService->createScheduleCronJob($cloud_context, $namespace, $create_cronjob_yaml))) {
      return FALSE;
    }

    $entity = $this->k8sService->getEntity(
      'k8s_cron_job',
      $cloud_context,
      $create_cronjob_yaml['metadata']['name'],
      $namespace
    );
    $this->processOperationStatus($entity, 'created');

    $name = !empty($yaml['metadata']['name'])
      ? $yaml['metadata']['name']
      : '';

    // Create the stop CronJob.
    // Specify 1 minute before the stoptime,
    // since cronjob requires the pod to be created from 1 minute before.
    $stop_cronjob_yaml = !empty($name)
      ? $this->k8sService->createScheduleCronJobYaml(
        $name,
        $namespace,
        date("H", strtotime("$stop_hour:$stop_minute -1 minute")),
        date("i", strtotime("$stop_hour:$stop_minute -1 minute")),
        'stop',
        $resource_name
      )
      : [];

    if (empty($stop_cronjob_yaml)
      || empty($this->k8sService->createScheduleCronJob($cloud_context, $namespace, $stop_cronjob_yaml))) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    $entity = $this->k8sService->getEntity(
      'k8s_cron_job',
      $cloud_context,
      $stop_cronjob_yaml['metadata']['name'],
      $namespace
    );
    $this->processOperationStatus($entity, 'created');

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createCronJobFromYaml(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $yaml_url,
    int $start_hour,
    int $start_minute,
    int $stop_hour,
    int $stop_minute,
  ): bool {
    $context = !empty($yaml_url) ? file_get_contents($yaml_url) : '';
    if (empty($context)) {
      $this->messenger->addError($this->t('The @yaml_url is invalid.', [
        '@yaml_url' => $yaml_url,
      ]));
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (empty($namespace)) {
      $this->messenger->addError($this->t('The namespace is empty.'));
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    $this->k8sService->setCloudContext($cloud_context);

    // Create Service Account and Cluster role binding.
    $this->createResourcesForCronJobScheduler($cloud_context, $namespace);

    // Get CronJob parameters.
    $yamls = $this->k8sService->decodeMultipleDocYaml($context);
    $name = !empty($yamls) && !empty(current($yamls)['metadata']['name'])
      ? current($yamls)['metadata']['name']
      : '';
    $resource_name = $this->getKebabCase(
      !empty($yamls) && !empty(current($yamls)['kind'])
        ? current($yamls)['kind']
        : $this->getShortEntityTypeNameWhitespace($entity)
    );
    $current_hour_minute = date('Hi');
    $start_hour_minute = sprintf('%02d%02d', $start_hour, $start_minute);
    $stop_hour_minute = sprintf('%02d%02d', $stop_hour, $stop_minute);

    $is_in_period = (
      ($start_hour_minute <= $stop_hour_minute)
      && ($current_hour_minute >= $start_hour_minute && $current_hour_minute <= $stop_hour_minute)
    )
    || (
      ($start_hour_minute > $stop_hour_minute)
      && ($current_hour_minute >= $start_hour_minute || $current_hour_minute <= $stop_hour_minute)
    );

    // Create the start CronJob.
    $start_cronjob_yaml = !empty($name) && !empty($yaml_url)
      ? $this->k8sService->createScheduleCronJobYaml(
        $name,
        $namespace,
        $start_hour,
        $start_minute,
        'start',
        $resource_name,
        $yaml_url
      )
      : [];
    if (empty($start_cronjob_yaml)
      || empty($this->k8sService->createScheduleCronJob($cloud_context, $namespace, $start_cronjob_yaml))) {
      return FALSE;
    }
    $entity = $this->k8sService->getEntity(
      'k8s_cron_job',
      $cloud_context,
      $start_cronjob_yaml['metadata']['name'],
      $namespace
    );
    $this->processOperationStatus($entity, 'created');

    // Create the CronJob.
    $create_cronjob_yaml = !empty($name) && !empty($yaml_url)
      ? $this->k8sService->createScheduleCronJobYaml(
        $name,
        $namespace,
        '',
        '',
        'create',
        $resource_name,
        $yaml_url,
        empty($is_in_period)
      )
      : [];
    if (empty($create_cronjob_yaml)
      || empty($this->k8sService->createScheduleCronJob($cloud_context, $namespace, $create_cronjob_yaml))) {
      return FALSE;
    }
    $entity = $this->k8sService->getEntity(
      'k8s_cron_job',
      $cloud_context,
      $create_cronjob_yaml['metadata']['name'],
      $namespace
    );
    $this->processOperationStatus($entity, 'created');

    foreach ($yamls ?: [] as $yaml) {
      if (empty($yaml)) {
        continue;
      }

      $name = !empty($yaml['metadata']['name'])
        ? $yaml['metadata']['name']
        : '';

      // Create the stop CronJob.
      // Specify 1 minute before the stoptime,
      // since cronjob requires the pod to be created from 1 minute before.
      $stop_cronjob_yaml = !empty($name)
        ? $this->k8sService->createScheduleCronJobYaml(
          $name,
          $namespace,
          date("H", strtotime("$stop_hour:$stop_minute -1 minute")),
          date("i", strtotime("$stop_hour:$stop_minute -1 minute")),
          'stop',
          $resource_name
        )
        : [];
      if (empty($stop_cronjob_yaml)
        || empty($this->k8sService->createScheduleCronJob($cloud_context, $namespace, $stop_cronjob_yaml))) {
        continue;
      }
      $entity = $this->k8sService->getEntity(
        'k8s_cron_job',
        $cloud_context,
        $stop_cronjob_yaml['metadata']['name'],
        $namespace
      );
      $this->processOperationStatus($entity, 'created');

      // Create K8sSchedule.
      $this->createCronJobSchedule(
        $cloud_context,
        $namespace,
        $start_hour,
        $start_minute,
        $stop_hour,
        $stop_minute,
        $yaml,
        $yaml_url
      );
    }

    return TRUE;
  }

  /**
   * Update yaml information to entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $now_entity
   *   The entity to be created.
   * @param string $namespace
   *   The namespace.
   * @param bool $enable_time_scheduler
   *   Enable Time Scheduler flag.
   * @param int $start_hour
   *   The hour of the schedule.
   * @param int $start_minute
   *   The minute of the schedule.
   * @param int $stop_hour
   *   The hour of the schedule.
   * @param int $stop_minute
   *   The minute of the schedule.
   * @param array $yaml
   *   The yaml.
   */
  private function updateYamlToEntity(
    ContentEntityInterface $now_entity,
    string $namespace,
    bool $enable_time_scheduler,
    int $start_hour,
    int $start_minute,
    int $stop_hour,
    int $stop_minute,
    array $yaml,
  ): bool {
    $entity = $now_entity;
    $cloud_context = $entity->getCloudContext();

    $name_camel = !empty($yaml['kind']) ? $yaml['kind'] : $this->getShortEntityTypeNameCamel($entity);

    // For kind List.
    if (strtolower($name_camel) === 'list') {
      $result = TRUE;
      foreach ($yaml['items'] ?? [] as $item) {
        if (!$this->updateYamlToEntity(
          $now_entity,
          $namespace,
          $enable_time_scheduler,
          $start_hour,
          $start_minute,
          $stop_hour,
          $stop_minute,
          $item
        )) {
          $result = FALSE;
          break;
        }
      }

      return $result;
    }

    $name_underscore = self::getSnakeCase($name_camel);

    if (!empty($entity->getOwner())) {
      // Add owner uid to annotations.
      $yaml['metadata']['annotations'][$this->cloudService->getTagKeyCreatedByUid('k8s', $cloud_context)] = $entity->getOwner()->id();

      if ($name_underscore === 'deployment') {
        // Add owner uid to pod template.
        $yaml['spec']['template']['metadata']['annotations'][$this->cloudService->getTagKeyCreatedByUid('k8s', $cloud_context)] = $entity->getOwner()->id();
      }
    }

    try {
      $method_name = "create{$name_camel}";
      if (!method_exists($this->k8sService, $method_name)) {
        $this->logger('k8s')->error($this->t('The kind @name_camel is not supported.', ['@name_camel' => $name_camel]));
        $this->messenger->addWarning($this->t('The kind @name_camel is not supported.', ['@name_camel' => $name_camel]));
        $this->processOperationErrorStatus($entity, 'created');
        return FALSE;
      }

      // Check schedule.
      if (!empty($enable_time_scheduler)) {
        $current_hour_minute = date('Hi');
        $start_hour_minute = sprintf('%02d%02d', $start_hour, $start_minute);
        $stop_hour_minute = sprintf('%02d%02d', $stop_hour, $stop_minute);

        $is_in_period = (
          ($start_hour_minute <= $stop_hour_minute)
          && ($current_hour_minute >= $start_hour_minute && $current_hour_minute <= $stop_hour_minute)
        )
        || (
          ($start_hour_minute > $stop_hour_minute)
          && ($current_hour_minute >= $start_hour_minute || $current_hour_minute <= $stop_hour_minute)
        );

        if (!$is_in_period) {
          return TRUE;
        }
      }

      $result = method_exists($now_entity, 'getNamespace')
        ? $this->k8sService->$method_name($namespace, $yaml)
        : $this->k8sService->$method_name($yaml);

      $result = array_merge($yaml, $result);
      $entity = $this->k8sService->createEntityFromYaml($entity->getEntityTypeId(), $result);
      if (empty($entity)) {
        $this->logger('k8s')->warning($this->t('The kind @kind is not supported in @form_title page.', [
          '@kind' => !empty($result['kind']) ? $result['kind'] : $this->getShortEntityTypeNameCamel($now_entity),
          '@form_title' => $this->getShortEntityTypeNameCamel($now_entity),
        ]));
        $this->messenger->addWarning($this->t('The kind @kind is not supported in @form_title page.', [
          '@kind' => !empty($result['kind']) ? $result['kind'] : $this->getShortEntityTypeNameCamel($now_entity),
          '@form_title' => $this->getShortEntityTypeNameCamel($now_entity),
        ]));
        return FALSE;
      }

      $entity->setName($result['metadata']['name']);
      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        $now_entity = $entity;
        return TRUE;
      }

      if (method_exists($entity, 'setCreationYaml')) {
        $entity->setCreationYaml(Yaml::encode($yaml));
      }
      if (method_exists($entity, 'setNamespace')) {
        $entity->setNamespace($result['metadata']['namespace']);
      }

      $entity->save();

      // Update the entity.
      $this->refreshEntity($entity);

      $this->processOperationStatus($entity, 'created');

      $now_entity = $entity;
    }
    catch (K8sServiceException
    | EntityStorageException
    | EntityMalformedException $e) {
      $this->logger('k8s')->error($e->getMessage());

      try {
        $this->messenger->addError($this->t('An error occurred: %error', [
          '%error' => $e->getMessage(),
        ]));
        $this->processOperationErrorStatus($entity, 'created');
      }
      catch (EntityMalformedException $e) {
        $this->handleException($e);
      }
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Load Entity with access check.
   *
   * @param int $hour
   *   The hour.
   * @param int $minutes
   *   The minutes.
   *
   * @return string
   *   The time.
   */
  private function getTimeFormat($hour, $minutes): string {
    return sprintf('%02d:%02d', $hour, $minutes);
  }

  /**
   * Save the yaml of the entity detail.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be created.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param string $detail
   *   The detail data for K8s entity.
   * @param bool $enable_time_scheduler
   *   Enable Time Scheduler flag.
   * @param int $start_hour
   *   The hour of the schedule.
   * @param int $start_minute
   *   The minute of the schedule.
   * @param int $stop_hour
   *   The hour of the schedule.
   * @param int $stop_minute
   *   The minute of the schedule.
   */
  private function saveYamlsOfEntityDetail(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $detail,
    bool $enable_time_scheduler,
    int $start_hour,
    int $start_minute,
    int $stop_hour,
    int $stop_minute,
  ): bool {
    $yamls = $this->k8sService->decodeMultipleDocYaml($detail);
    foreach ($yamls ?: [] as $yaml) {
      if (empty($yaml)) {
        continue;
      }

      // The process of saving each yaml to the entity
      // is implemented by saveYamlInEntity().
      if (empty($this->updateYamlToEntity(
        $entity,
        $namespace,
        $enable_time_scheduler,
        $start_hour,
        $start_minute,
        $stop_hour,
        $stop_minute,
        $yaml
      ))) {
        return FALSE;
      }

      if ($enable_time_scheduler) {
        $timestamp = time();
        $name = $yaml['metadata']['name'];
        $schedule = K8sSchedule::create([
          'cloud_context' => $cloud_context,
          'name' => $namespace . '_' . $name,
          'kind' => $yaml['kind'],
          'namespace_name' => $namespace,
          'resource_name' => $name,
          'start_hour' => $start_hour,
          'start_minute' => $start_minute,
          'start_time' => $this->getTimeFormat($start_hour, $start_minute),
          'stop_hour' => $stop_hour,
          'stop_minute' => $stop_minute,
          'stop_time' => $this->getTimeFormat($stop_hour, $stop_minute),
          'manifest' => Yaml::encode($yaml),
          'created' => $timestamp,
          'changed' => $timestamp,
          'refreshed' => $timestamp,
        ]);
        $schedule->save();

        $this->messenger->addStatus($this->t('The @type %label has been created.', [
          '@type' => $schedule->getEntityType()->getSingularLabel(),
          '%label' => $schedule->toLink($schedule->getName())->toString(),
        ]));
        $this->logOperationMessage($schedule, 'created');
      }
    }

    return TRUE;
  }

  /**
   * Create a K8sSchedule for CronJob Scheduler.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param int $start_hour
   *   The hour of the schedule.
   * @param int $start_minute
   *   The minute of the schedule.
   * @param int $stop_hour
   *   The hour of the schedule.
   * @param int $stop_minute
   *   The minute of the schedule.
   * @param array $yaml
   *   The yaml.
   * @param string $yaml_url
   *   The YAML URL.
   */
  private function createCronJobSchedule(
    string $cloud_context,
    string $namespace,
    int $start_hour,
    int $start_minute,
    int $stop_hour,
    int $stop_minute,
    array $yaml,
    string $yaml_url,
  ): void {
    $timestamp = time();
    $name = $yaml['metadata']['name'];

    $schedule_entities = $this->entityTypeManager
      ->getStorage('k8s_schedule')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
        'namespace_name' => $namespace,
        'resource_name' => $name,
      ]);
    $entity = !empty($schedule_entities)
      ? array_shift($schedule_entities)
      : $schedule = K8sSchedule::create([
        'cloud_context' => $cloud_context,
        'name' => $namespace . '_' . $name,
        'kind' => $yaml['kind'],
        'namespace_name' => $namespace,
        'resource_name' => $name,
        'start_hour' => $start_hour,
        'start_minute' => $start_minute,
        'start_time' => $this->getTimeFormat($start_hour, $start_minute),
        'stop_hour' => $stop_hour,
        'stop_minute' => $stop_minute,
        'stop_time' => $this->getTimeFormat($stop_hour, $stop_minute),
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'schedule_type' => K8sContentFormInterface::CRONJOB_SCHEDULER,
        'yaml_url' => $yaml_url,
      ]);
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPod(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $detail,
    bool $enable_time_scheduler,
    string $time_scheduler_option,
    string $yaml_url,
    int $startHour,
    int $startMinute,
    int $stopHour,
    int $stopMinute,
  ): bool {
    $this->k8sService->setCloudContext($cloud_context);

    $enable_time_scheduler && $time_scheduler_option === K8sContentFormInterface::CRONJOB_SCHEDULER
      ? $this->createCronJobFromYaml(
        $entity,
        $cloud_context,
        $namespace,
        $yaml_url,
        $startHour,
        $startMinute,
        $stopHour,
        $stopMinute
      )
      : $this->saveYamlsOfEntityDetail(
        $entity,
        $cloud_context,
        $namespace,
        $detail,
        $enable_time_scheduler,
        $startHour,
        $startMinute,
        $stopHour,
        $stopMinute
      );

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createNamespace(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $name,
    array $labels,
  ): bool {
    // Get labels.
    $this->k8sService->setCloudContext($cloud_context);
    $params = [];
    $params['metadata']['name'] = $name;
    if (!empty($labels)) {
      $params['metadata']['labels'] = $labels;
    }

    if (!empty($entity->getOwner())) {
      // Add owner uid to annotations.
      $params['metadata']['annotations'][$this->cloudService->getTagKeyCreatedByUid('k8s', $cloud_context)] = $entity->getOwner()->id();
    }

    try {

      $result = $this->k8sService->createNamespace($params);
      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $entity->setStatus($result['status']['phase']);
      $entity->setCreated(strtotime($result['metadata']['creationTimestamp']));
      $entity->save();

      $this->processOperationStatus($entity, 'created');

      $this->cloudService->invalidateCacheTags($entity->getCacheTags());

      return TRUE;
    }
    catch (K8sServiceException
    | EntityStorageException
    | EntityMalformedException $e) {

      try {
        $this->processOperationErrorStatus($entity, 'created');
      }
      catch (EntityMalformedException $e) {
        $this->handleException($e);
      }
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createDeployment(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $detail,
    bool $enable_time_scheduler,
    string $time_scheduler_option,
    string $yaml_url,
    int $startHour,
    int $startMinute,
    int $stopHour,
    int $stopMinute,
  ): bool {
    $this->k8sService->setCloudContext($cloud_context);

    $enable_time_scheduler && $time_scheduler_option === K8sContentFormInterface::CRONJOB_SCHEDULER
      ? $this->createCronJobFromYaml(
        $entity,
        $cloud_context,
        $namespace,
        $yaml_url,
        $startHour,
        $startMinute,
        $stopHour,
        $stopMinute
      )
      : $this->saveYamlsOfEntityDetail(
        $entity,
        $cloud_context,
        $namespace,
        $detail,
        $enable_time_scheduler,
        $startHour,
        $startMinute,
        $stopHour,
        $stopMinute
      );

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createK8sEntity(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $detail,
    bool $enable_time_scheduler,
    int $startHour,
    int $startMinute,
    int $stopHour,
    int $stopMinute,
  ): bool {
    $this->k8sService->setCloudContext($cloud_context);

    return $this->saveYamlsOfEntityDetail(
      $entity,
      $cloud_context,
      $namespace,
      $detail,
      $enable_time_scheduler,
      $startHour,
      $startMinute,
      $stopHour,
      $stopMinute
    );
  }

  /**
   * {@inheritdoc}
   */
  public function scaleDeployment(
    ContentEntityInterface $entity,
    int $pod_number,
  ): bool {
    $this->k8sService->setCloudContext($entity->getCloudContext());
    try {
      $params = Yaml::decode($entity->getDetail());
      $params['spec']['replicas'] = $pod_number;

      $this->k8sService->updateDeployment(
        $entity->getNamespace(),
        $params
      );
      $entity->save();

      // Update the entity.
      $this->refreshEntity($entity);

      $this->processOperationStatus($entity, 'scaled');
      return TRUE;
    }
    catch (K8sServiceException $e) {
      try {
        $this->processOperationErrorStatus($entity, 'scaled');
      }
      catch (EntityMalformedException $e) {
        $this->handleException($e);
      }
      return FALSE;
    }
  }

  /**
   * Gets the entity of this method.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNamespace(ContentEntityInterface $entity): bool {
    $this->entity = $entity;

    try {
      $this->k8sService->setCloudContext($entity->getCloudContext());
      $result = $this->k8sService->deleteNamespace([
        'metadata' => [
          'name' => $entity->getName(),
        ],
      ]);

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'deleted remotely');
        return TRUE;
      }

      $entity->delete();

      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();

      // Delete the role if it exists.
      $roles = $this->entityTypeManager->getStorage('user_role')
        ->loadByProperties([
          'id' => $entity->getName(),
        ]);
      if (!empty($roles)) {
        $role = reset($roles);
        $role->delete();
      }
      return TRUE;
    }
    catch (K8sServiceException $e) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteK8sEntity(ContentEntityInterface $entity): bool {
    $this->entity = $entity;

    $name_camel = $this->getShortEntityTypeNameCamel($entity);

    $this->k8sService->setCloudContext($entity->getCloudContext());
    try {
      $method_name = "delete{$name_camel}";

      if (method_exists($entity, 'getNamespace')) {
        $result = $this->k8sService->$method_name($entity->getNamespace(), [
          'metadata' => [
            'name' => $entity->getName(),
          ],
        ]);
      }
      else {
        if (method_exists($this->k8sService, $method_name)) {
          $result = $this->k8sService->$method_name([
            'metadata' => [
              'name' => $entity->getName(),
            ],
          ]);
        }
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'deleted remotely');
        return TRUE;
      }

      $entity->delete();

      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
      return TRUE;
    }
    catch (K8sServiceException $e) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }
  }

}
