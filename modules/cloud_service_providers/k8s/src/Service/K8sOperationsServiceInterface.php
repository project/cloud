<?php

namespace Drupal\k8s\Service;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * The Interface for K8sOperationsService.
 */
interface K8sOperationsServiceInterface {

  /**
   * Update K8s namespace.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The K8s pod entity.
   * @param array $params
   *   Optional parameters array.
   * @param bool $batch
   *   Boolean to use batch processing.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function updateNamespace(ContentEntityInterface $entity, array $params, bool $batch = TRUE): bool;

  /**
   * Update K8s schedule.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The K8s schedule entity.
   * @param array $params
   *   Optional parameters array.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function updateSchedule(ContentEntityInterface $entity, array $params): bool;

  /**
   * Update K8s Entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The K8s entity.
   * @param array $params
   *   Optional parameters array.
   * @param bool $batch
   *   Boolean to trigger an API refresh of the entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function updateEntity(ContentEntityInterface $entity, array $params, bool $batch = TRUE): bool;

  /**
   * Create a cronjob that gets a yaml and schedules a pod in Git.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be created.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param string $name
   *   The resource name.
   * @param string $resource_name
   *   Specify the Kind defined in yaml. ex, Pod, Deployment.
   * @param string $yaml_file_name
   *   The yaml file name.
   * @param array $yamls
   *   The yaml file.
   * @param int $start_hour
   *   The hour of the schedule.
   * @param int $start_minute
   *   The minute of the schedule.
   * @param int $stop_hour
   *   The hour of the schedule.
   * @param int $stop_minute
   *   The minute of the schedule.
   */
  public function createGitCronJobFromYaml(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $name,
    string $resource_name,
    string $yaml_file_name,
    array $yamls,
    int $start_hour,
    int $start_minute,
    int $stop_hour,
    int $stop_minute,
  ): bool;

  /**
   * Create a cronjob that gets a yaml and schedules a pod.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be created.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param string $yaml_url
   *   The YAML URL of the pod to start.
   * @param int $start_hour
   *   The hour of the schedule.
   * @param int $start_minute
   *   The minute of the schedule.
   * @param int $stop_hour
   *   The hour of the schedule.
   * @param int $stop_minute
   *   The minute of the schedule.
   */
  public function createCronJobFromYaml(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $yaml_url,
    int $start_hour,
    int $start_minute,
    int $stop_hour,
    int $stop_minute,
  ): bool;

  /**
   * Create K8s pod.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The K8s schedule entity.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param string $detail
   *   The detail data for K8s entity.
   * @param bool $enable_time_scheduler
   *   Enable Time Scheduler flag.
   * @param string $time_scheduler_option
   *   Time scheduler option.
   * @param string $yaml_url
   *   The YAML URL of the pod to start.
   * @param int $startHour
   *   The start hour for K8s schedule.
   * @param int $startMinute
   *   The start minute for K8s schedule.
   * @param int $stopHour
   *   The stop hour for K8s schedule.
   * @param int $stopMinute
   *   The stop minute for K8s schedule.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createPod(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $detail,
    bool $enable_time_scheduler,
    string $time_scheduler_option,
    string $yaml_url,
    int $startHour,
    int $startMinute,
    int $stopHour,
    int $stopMinute,
  ): bool;

  /**
   * Create K8s namespace.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be created.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The name of the Namespace.
   * @param array $labels
   *   label data for K8s namespace.
   */
  public function createNamespace(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $name,
    array $labels,
  ): bool;

  /**
   * Create K8s deployment.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The K8s schedule entity.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param string $detail
   *   The detail data for K8s entity.
   * @param bool $enable_time_scheduler
   *   Enable Time Scheduler flag.
   * @param string $time_scheduler_option
   *   Time scheduler option.
   * @param string $yaml_url
   *   The YAML URL of the pod to start.
   * @param int $startHour
   *   The start hour for K8s schedule.
   * @param int $startMinute
   *   The start minute for K8s schedule.
   * @param int $stopHour
   *   The stop hour for K8s schedule.
   * @param int $stopMinute
   *   The stop minute for K8s schedule.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createDeployment(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $detail,
    bool $enable_time_scheduler,
    string $time_scheduler_option,
    string $yaml_url,
    int $startHour,
    int $startMinute,
    int $stopHour,
    int $stopMinute,
  ): bool;

  /**
   * Create K8s other entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The K8s schedule entity.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param string $detail
   *   The detail data for K8s entity.
   * @param bool $enable_time_scheduler
   *   Enable Time Scheduler flag.
   * @param int $startHour
   *   The start hour for K8s schedule.
   * @param int $startMinute
   *   The start minute for K8s schedule.
   * @param int $stopHour
   *   The stop hour for K8s schedule.
   * @param int $stopMinute
   *   The stop minute for K8s schedule.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createK8sEntity(
    ContentEntityInterface $entity,
    string $cloud_context,
    string $namespace,
    string $detail,
    bool $enable_time_scheduler,
    int $startHour,
    int $startMinute,
    int $stopHour,
    int $stopMinute,
  ): bool;

  /**
   * Scale K8s deployment.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be created.
   * @param int $pod_number
   *   Desire number of pods.
   */
  public function scaleDeployment(
    ContentEntityInterface $entity,
    int $pod_number,
  ): bool;

  /**
   * Delete K8s namespace.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be created.
   */
  public function deleteNamespace(ContentEntityInterface $entity): bool;

  /**
   * Delete K8s entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be deleted.
   */
  public function deleteK8sEntity(ContentEntityInterface $entity): bool;

  /**
   * Refresh a single K8s entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to refresh.
   * @param bool $batch
   *   TRUE to use batch, FALSE for no batch processing.
   */
  public function refreshEntity(EntityInterface $entity, bool $batch = TRUE): void;

}
