<?php

namespace Drupal\k8s\Service;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\GeneratedLink;
use Drupal\Core\Link;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudCacheServiceInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\CloudServiceBase;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\k8s\Controller\K8sCostsControllerBase;
use Drupal\k8s\Entity\K8sEntityBase;
use Drupal\k8s\Entity\K8sNamespace;
use Drupal\k8s\Entity\K8sNode;
use Drupal\k8s\Entity\K8sPod;
use Drupal\k8s\Form\K8sContentFormInterface;
use Drupal\k8s\Plugin\k8s\K8sRepositoryProviderManager;
use Drupal\k8s\Service\K8sClientExtension\K8sClient;
use Drupal\k8s\Service\K8sClientExtension\K8sRepositoryRegistry;
use Drupal\k8s\Service\K8sClientExtension\Models\ApiService;
use Drupal\k8s\Service\K8sClientExtension\Models\ClusterRole;
use Drupal\k8s\Service\K8sClientExtension\Models\ClusterRoleBinding;
use Drupal\k8s\Service\K8sClientExtension\Models\LimitRange;
use Drupal\k8s\Service\K8sClientExtension\Models\PriorityClass;
use Drupal\k8s\Service\K8sClientExtension\Models\StatefulSet;
use Drupal\k8s\Service\K8sClientExtension\Models\StorageClass;
use Drupal\user\Entity\User;
use GuzzleHttp\ClientInterface;
use Http\Adapter\Guzzle7\Client as GuzzleAdapter;
use Maclof\Kubernetes\Models\ConfigMap;
use Maclof\Kubernetes\Models\CronJob;
use Maclof\Kubernetes\Models\DaemonSet;
use Maclof\Kubernetes\Models\DeleteOptions;
use Maclof\Kubernetes\Models\Deployment;
use Maclof\Kubernetes\Models\Endpoint;
use Maclof\Kubernetes\Models\HorizontalPodAutoscaler;
use Maclof\Kubernetes\Models\Ingress;
use Maclof\Kubernetes\Models\Job;
use Maclof\Kubernetes\Models\NamespaceModel;
use Maclof\Kubernetes\Models\NetworkPolicy;
use Maclof\Kubernetes\Models\PersistentVolume;
use Maclof\Kubernetes\Models\PersistentVolumeClaim;
use Maclof\Kubernetes\Models\Pod;
use Maclof\Kubernetes\Models\QuotaModel;
use Maclof\Kubernetes\Models\ReplicaSet;
use Maclof\Kubernetes\Models\Role;
use Maclof\Kubernetes\Models\RoleBinding;
use Maclof\Kubernetes\Models\Secret;
use Maclof\Kubernetes\Models\Service;
use Maclof\Kubernetes\Models\ServiceAccount;
use PHPUnit\Runner\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * K8sService service interacts with the K8s API.
 */
class K8sService extends CloudServiceBase implements K8sServiceInterface {

  use CloudContentEntityTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Field type plugin manager.
   *
   * @var \Drupal\core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Entity field manager interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The lock interface.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Class Resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The Cloud cache service.
   *
   * @var \Drupal\cloud\Service\CloudCacheServiceInterface
   */
  protected $cloudCacheService;

  /**
   * The Cloud cache service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The k8s repository provider manager.
   *
   * @var \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderManager
   */
  protected $k8sRepositoryProviderManager;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * Constructs a new K8sService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager.
   * @param \Drupal\core\Field\FieldTypePluginManagerInterface $field_type_plugin_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock interface.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver service.
   * @param \Drupal\cloud\Service\CloudCacheServiceInterface $cloud_cache_service
   *   The Cloud cache service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderManager $k8s_repository_provider_manager
   *   The K8s repository provider manager.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    FieldTypePluginManagerInterface $field_type_plugin_manager,
    EntityFieldManagerInterface $entity_field_manager,
    LockBackendInterface $lock,
    QueueFactory $queue_factory,
    ModuleHandlerInterface $module_handler,
    ClassResolverInterface $class_resolver,
    CloudCacheServiceInterface $cloud_cache_service,
    CloudServiceInterface $cloud_service,
    K8sRepositoryProviderManager $k8s_repository_provider_manager,
    ClientInterface $http_client,
    ExtensionPathResolver $extension_path_resolver,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    // Set up the entity type manager for querying entities.
    $this->entityTypeManager = $entity_type_manager;

    // Set up the configuration factory.
    $this->configFactory = $config_factory;

    $this->currentUser = $current_user;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->lock = $lock;
    $this->queueFactory = $queue_factory;
    $this->moduleHandler = $module_handler;
    $this->classResolver = $class_resolver;
    $this->cloudCacheService = $cloud_cache_service;
    $this->cloudService = $cloud_service;
    $this->k8sRepositoryProviderManager = $k8s_repository_provider_manager;
    $this->httpClient = $http_client;
    $this->extensionPathResolver = $extension_path_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context): void {
    $this->cloudContext = $cloud_context;
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
  }

  /**
   * Load and return a K8sClient.
   *
   * @param string $namespace
   *   The namespace.
   *
   * @return \Drupal\k8s\Service\K8sClientExtension\K8sClient
   *   The value of created object.
   *
   * @throws K8sServiceException
   *   Throw an exception when a K8s API server is not reachable.
   */
  protected function getClient(string $namespace = ''): K8sClient {
    $client = NULL;
    $credentials = $this->cloudConfigPluginManager->loadCredentials();

    if (!$this->isAccessible($credentials['master'], $credentials['token'])) {
      throw new K8sServiceException("The K8s API server is not accessible: {$credentials['master']}");
    }

    try {
      $client = new K8sClient(
        [
          'master' => $credentials['master'],
          'token' => $credentials['token'],
          'verify' => FALSE,
          'namespace' => $namespace,
        ],
        new K8sRepositoryRegistry($this->k8sRepositoryProviderManager),
        GuzzleAdapter::createWithConfig(['verify' => FALSE])
      );
    }
    catch (\Exception $e) {
      $this->logger('k8s_service')->error($e->getMessage());
    }

    return $client;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   Returns an array of pods containing parameters.
   */
  public function getPods(array $params = []): array {
    return $this->getClient()
      ->pods()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createPod($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->pods()
      ->create(new Pod($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updatePod($namespace, array $params = []): ?array {
    return $this->updateResource(Pod::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deletePod($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->pods()
      ->delete(new Pod($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getPodLogs($namespace, array $params = [], $container = NULL) {
    $options = ['pretty' => TRUE];
    if (!empty($container)) {
      $options['container'] = $container;
    }
    return $this->getClient($namespace)
      ->pods()
      ->logs(new Pod($params), $options);
  }

  /**
   * {@inheritdoc}
   */
  public function getNodes(array $params = []): array {
    return $this->getClient()
      ->nodes()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function getNamespaces(array $params = []): array {
    return $this->getClient()
      ->namespaces()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createNamespace(array $params = []): array {
    return $this->getClient()
      ->namespaces()
      ->create(new NamespaceModel($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateNamespace(array $params = []): array {
    return $this->getClient()
      ->namespaces()
      ->update(new NamespaceModel($params));
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNamespace(array $params = []): array {
    return $this->getClient()
      ->namespaces()
      ->delete(new NamespaceModel($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getMetricsPods(array $params = []): array {
    return $this->getClient()
      ->metricsPods()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function getMetricsNodes(array $params = []): array {
    return $this->getClient()
      ->metricsNodes()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePods(array $params = [], $clear = TRUE): bool {
    $this->cloudCacheService->clearResourceDataCache($this->cloudContext, 'k8s_pod');
    $metrics = $this->getPodsMetricsMap();
    return $this->updateEntities(
      'k8s_pod',
      'Pod',
      'getPods',
      'updatePod',
      $params,
      $clear,
      'namespace',
      TRUE,
      $metrics
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updatePodsWithoutBatch(array $params = [], $clear = TRUE): bool {
    $this->cloudCacheService->clearResourceDataCache($this->cloudContext, 'k8s_pod');
    $metrics = $this->getPodsMetricsMap();
    return $this->updateEntities(
      'k8s_pod',
      'Pod',
      'getPods',
      'updatePod',
      $params,
      $clear,
      'namespace',
      FALSE,
      $metrics
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateNodes(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_node',
      'Node',
      'getNodes',
      'updateNode',
      $params,
      $clear,
      NULL,
      TRUE,
      ['resource_store_time' => time()]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateNodesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_node',
      'Node',
      'getNodes',
      'updateNode',
      $params,
      $clear,
      NULL,
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateNamespaces(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_namespace',
      'Namespace',
      'getNamespaces',
      'updateNamespace',
      $params,
      $clear,
      NULL,
      TRUE,
      ['resource_store_time' => time()]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateNamespacesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_namespace',
      'Namespace',
      'getNamespaces',
      'updateNamespace',
      $params,
      $clear,
      NULL,
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateDeploymentsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_deployment',
      'Deployment',
      'getDeployments',
      'updateDeployment',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateDeployments(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_deployment',
      'Deployment',
      'getDeployments',
      'updateDeployment',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateReplicaSets(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_replica_set',
      'ReplicaSet',
      'getReplicaSets',
      'updateReplicaSet',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateReplicaSetsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_replica_set',
      'ReplicaSet',
      'getReplicaSets',
      'updateReplicaSet',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateServices(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_service',
      'Service',
      'getServices',
      'updateService',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateServicesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_service',
      'Service',
      'getServices',
      'updateService',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateCronJobs(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_cron_job',
      'CronJob',
      'getCronJobs',
      'updateCronJob',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateCronJobsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_cron_job',
      'CronJob',
      'getCronJobs',
      'updateCronJob',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * Get entity.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The name.
   * @param string $namespace
   *   The namespace.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   return entity object.
   */
  public static function getEntity(string $entity_type_id, string $cloud_context, string $name, string $namespace = ''): ?EntityInterface {
    return K8sBatchOperations::getEntity($entity_type_id, $cloud_context, $name, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function updateJobs(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_job',
      'Job',
      'getJobs',
      'updateJob',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateJobsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_job',
      'Job',
      'getJobs',
      'updateJob',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateResourceQuotas(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_resource_quota',
      'Resource quota',
      'getResourceQuotas',
      'updateResourceQuota',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateResourceQuotasWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_resource_quota',
      'Resource quota',
      'getResourceQuotas',
      'updateResourceQuota',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateLimitRanges(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_limit_range',
      'Limit Range',
      'getLimitRanges',
      'updateLimitRange',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateLimitRangesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_limit_range',
      'Limit Range',
      'getLimitRanges',
      'updateLimitRange',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateSecrets(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_secret',
      'Secret',
      'getSecrets',
      'updateSecret',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateSecretsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_secret',
      'Secret',
      'getSecrets',
      'updateSecret',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateConfigMaps(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_config_map',
      'ConfigMap',
      'getConfigMaps',
      'updateConfigMap',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateConfigMapsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_config_map',
      'ConfigMap',
      'getConfigMaps',
      'updateConfigMap',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateRoles(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_role',
      'Role',
      'getRoles',
      'updateRole',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateRolesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_role',
      'Role',
      'getRoles',
      'updateRole',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateClusterRoles(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_cluster_role',
      'ClusterRole',
      'getClusterRoles',
      'updateClusterRole',
      $params,
      $clear
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateClusterRolesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_cluster_role',
      'ClusterRole',
      'getClusterRoles',
      'updateClusterRole',
      $params,
      $clear,
      NULL,
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateStorageClasses(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_storage_class',
      'StorageClass',
      'getStorageClasses',
      'updateStorageClass',
      $params,
      $clear
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateStorageClassesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_storage_class',
      'StorageClass',
      'getStorageClasses',
      'updateStorageClass',
      $params,
      $clear,
      NULL,
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolumes(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_persistent_volume',
      'PersistentVolume',
      'getPersistentVolumes',
      'updatePersistentVolume',
      $params,
      $clear
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolumesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_persistent_volume',
      'PersistentVolume',
      'getPersistentVolumes',
      'updatePersistentVolume',
      $params,
      $clear,
      NULL,
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateStatefulSets(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_stateful_set',
      'StatefulSet',
      'getStatefulSets',
      'updateStatefulSet',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateStatefulSetsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_stateful_set',
      'StatefulSet',
      'getStatefulSets',
      'updateStatefulSet',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateIngresses(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_ingress',
      'Ingress',
      'getIngresses',
      'updateIngress',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateIngressesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_ingress',
      'Ingress',
      'getIngresses',
      'updateIngress',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateDaemonSets(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_daemon_set',
      'DaemonSet',
      'getDaemonSets',
      'updateDaemonSet',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateDaemonSetsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_daemon_set',
      'DaemonSet',
      'getDaemonSets',
      'updateDaemonSet',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateEndpoints(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_endpoint',
      'Endpoint',
      'getEndpoints',
      'updateEndpoints',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateEndpointsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_endpoint',
      'Endpoint',
      'getEndpoints',
      'updateEndpoints',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolumeClaims(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_persistent_volume_claim',
      'PersistentVolumeClaim',
      'getPersistentVolumeClaims',
      'updatePersistentVolumeClaim',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolumeClaimsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_persistent_volume_claim',
      'PersistentVolumeClaim',
      'getPersistentVolumeClaims',
      'updatePersistentVolumeClaim',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateClusterRoleBindings(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_cluster_role_binding',
      'ClusterRoleBinding',
      'getClusterRoleBindings',
      'updateClusterRoleBinding',
      $params,
      $clear
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateClusterRoleBindingsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_cluster_role_binding',
      'ClusterRoleBinding',
      'getClusterRoleBindings',
      'updateClusterRoleBinding',
      $params,
      $clear,
      NULL,
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateApiServices(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_api_service',
      'ApiService',
      'getApiServices',
      'updateApiService',
      $params,
      $clear
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateApiServicesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_api_service',
      'ApiService',
      'getApiServices',
      'updateApiService',
      $params,
      $clear,
      NULL,
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateRoleBindings(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_role_binding',
      'RoleBinding',
      'getRoleBindings',
      'updateRoleBinding',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateRoleBindingsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_role_binding',
      'RoleBinding',
      'getRoleBindings',
      'updateRoleBinding',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateServiceAccounts(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_service_account',
      'ServiceAccount',
      'getServiceAccounts',
      'updateServiceAccount',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateServiceAccountsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_service_account',
      'ServiceAccount',
      'getServiceAccounts',
      'updateServiceAccount',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updatePriorityClasses(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_priority_class',
      'PriorityClass',
      'getPriorityClasses',
      'updatePriorityClass',
      $params,
      $clear
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updatePriorityClassesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_priority_class',
      'PriorityClass',
      'getPriorityClasses',
      'updatePriorityClass',
      $params,
      $clear,
      NULL,
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateHorizontalPodAutoscalers(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_horizontal_pod_autoscaler',
      'HorizontalPodAutoscaler',
      'getHorizontalPodAutoscalers',
      'updateHorizontalPodAutoscaler',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateHorizontalPodAutoscalersWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_horizontal_pod_autoscaler',
      'HorizontalPodAutoscaler',
      'getHorizontalPodAutoscalers',
      'updateHorizontalPodAutoscaler',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDeployments(array $params = []) {
    return $this->getClient()
      ->deployments()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createDeployment($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->deployments()
      ->create(new Deployment($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateDeployment($namespace, array $params = []): ?array {
    return $this->updateResource(Deployment::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDeployment($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->deployments()
      ->delete(new Deployment($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getReplicaSets(array $params = []): array {
    return $this->getClient()
      ->replicaSets()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createReplicaSet($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->replicaSets()
      ->create(new ReplicaSet($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateReplicaSet($namespace, array $params = []): ?array {
    return $this->updateResource(ReplicaSet::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteReplicaSet($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->replicaSets()
      ->delete(new ReplicaSet($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getServices(array $params = []): array {
    return $this->getClient()
      ->services()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createService($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->services()
      ->create(new Service($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateService($namespace, array $params = []): ?array {
    return $this->updateResource(Service::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteService($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->services()
      ->delete(new Service($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getCronJobs(array $params = []): array {
    return $this->getClient()
      ->cronJobs()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createCronJob($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->cronJobs()
      ->create(new CronJob($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateCronJob($namespace, array $params = []): ?array {
    return $this->updateResource(CronJob::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCronJob($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->cronJobs()
      ->delete(new CronJob($params), new DeleteOptions(['propagationPolicy' => 'Foreground']));
  }

  /**
   * {@inheritdoc}
   */
  public function getJobs(array $params = []): array {
    return $this->getClient()
      ->jobs()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createJob($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->jobs()
      ->create(new Job($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateJob($namespace, array $params = []): ?array {
    return $this->updateResource(Job::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteJob($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->jobs()
      ->delete(new Job($params), new DeleteOptions(['propagationPolicy' => 'Foreground']));
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceQuotas(array $params = []): array {
    return $this->getClient()
      ->quotas()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createResourceQuota($namespace, array $params = []): array {
    $params['kind'] = 'ResourceQuota';
    return $this->getClient($namespace)
      ->quotas()
      ->create(new QuotaModel($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateResourceQuota($namespace, array $params = []): ?array {
    $params['kind'] = 'ResourceQuota';
    return $this->updateResource(QuotaModel::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteResourceQuota($namespace, array $params = []): array {
    $params['kind'] = 'ResourceQuota';

    return $this->getClient($namespace)
      ->quotas()
      ->delete(new QuotaModel($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getLimitRanges(array $params = []): array {
    return $this->getClient()
      ->limitRanges()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createLimitRange($namespace, array $params = []): array {
    $params['kind'] = 'LimitRange';
    return $this->getClient($namespace)
      ->limitRanges()
      ->create(new LimitRange($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateLimitRange($namespace, array $params = []): ?array {
    return $this->updateResource(LimitRange::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteLimitRange($namespace, array $params = []): array {
    $params['kind'] = 'LimitRange';

    return $this->getClient($namespace)
      ->limitRanges()
      ->delete(new LimitRange($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getSecrets(array $params = []): array {
    return $this->getClient()
      ->secrets()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createSecret($namespace, array $params = []): array {
    $params['kind'] = 'Secret';
    return $this->getClient($namespace)
      ->secrets()
      ->create(new Secret($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateSecret($namespace, array $params = []): ?array {
    return $this->updateResource(Secret::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSecret($namespace, array $params = []): array {
    $params['kind'] = 'Secret';

    return $this->getClient($namespace)
      ->secrets()
      ->delete(new Secret($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigMaps(array $params = []): array {
    return $this->getClient()
      ->configMaps()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigMap($namespace, array $params = []): array {
    $params['kind'] = 'ConfigMap';
    return $this->getClient($namespace)
      ->configMaps()
      ->create(new ConfigMap($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateConfigMap($namespace, array $params = []): ?array {
    return $this->updateResource(ConfigMap::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteConfigMap($namespace, array $params = []): array {
    $params['kind'] = 'ConfigMap';

    return $this->getClient($namespace)
      ->configMaps()
      ->delete(new ConfigMap($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateNetworkPolicies(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_network_policy',
      'Network policy',
      'getNetworkPolicies',
      'updateNetworkPolicy',
      $params,
      $clear,
      'namespace'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateNetworkPoliciesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_network_policy',
      'Network policy',
      'getNetworkPolicies',
      'updateNetworkPolicy',
      $params,
      $clear,
      'namespace',
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkPolicies(array $params = []): array {
    return $this->getClient()
      ->networkPolicies()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createNetworkPolicy($namespace, array $params = []): array {
    $params['kind'] = 'NetworkPolicy';
    return $this->getClient($namespace)
      ->networkPolicies()
      ->create(new NetworkPolicy($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateNetworkPolicy($namespace, array $params = []): ?array {
    return $this->updateResource(NetworkPolicy::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNetworkPolicy($namespace, array $params = []): array {
    $params['kind'] = 'NetworkPolicy';

    return $this->getClient($namespace)
      ->networkPolicies()
      ->delete(new NetworkPolicy($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getRoles(array $params = []): array {
    return $this->getClient()
      ->roles()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createRole($namespace, array $params = []): array {
    $params['kind'] = 'Role';
    return $this->getClient($namespace)
      ->roles()
      ->create(new Role($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateRole($namespace, array $params = []): ?array {
    return $this->updateResource(Role::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRole($namespace, array $params = []): array {
    $params['kind'] = 'Role';

    return $this->getClient($namespace)
      ->roles()
      ->delete(new Role($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getRoleBindings(array $params = []): array {
    return $this->getClient()
      ->roleBindings()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createRoleBinding($namespace, array $params = []): array {
    $params['kind'] = 'RoleBinding';
    return $this->getClient($namespace)
      ->roleBindings()
      ->create(new RoleBinding($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateRoleBinding($namespace, array $params = []): ?array {
    return $this->updateResource(RoleBinding::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRoleBinding($namespace, array $params = []): array {
    $params['kind'] = 'RoleBinding';

    return $this->getClient($namespace)
      ->roleBindings()
      ->delete(new RoleBinding($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getServiceAccounts(array $params = []): array {
    return $this->getClient()
      ->serviceAccounts()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createServiceAccount($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->serviceAccounts()
      ->create(new ServiceAccount($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateServiceAccount($namespace, array $params = []): ?array {
    return $this->updateResource(ServiceAccount::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteServiceAccount($namespace, array $params = []): array {
    $params['kind'] = 'ServiceAccount';

    return $this->getClient($namespace)
      ->serviceAccounts()
      ->delete(new ServiceAccount($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getClusterRoles(array $params = []): array {
    return $this->getClient()
      ->clusterRoles()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createClusterRole(array $params = []): array {
    $params['kind'] = 'ClusterRole';
    return $this->getClient()
      ->clusterRoles()
      ->create(new ClusterRole($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateClusterRole(array $params = []): ?array {
    return $this->updateResource(ClusterRole::class, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteClusterRole(array $params = []): array {
    $params['kind'] = 'ClusterRole';

    return $this->getClient()
      ->clusterRoles()
      ->delete(new ClusterRole($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getClusterRoleBindings(array $params = []): array {
    return $this->getClient()
      ->clusterRoleBindings()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createClusterRoleBinding(array $params = []): array {
    $params['kind'] = 'ClusterRoleBinding';
    return $this->getClient()
      ->clusterRoleBindings()
      ->create(new ClusterRoleBinding($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateClusterRoleBinding(array $params = []): ?array {
    return $this->updateResource(ClusterRoleBinding::class, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteClusterRoleBinding(array $params = []): array {
    $params['kind'] = 'ClusterRoleBinding';

    return $this->getClient()
      ->clusterRoleBindings()
      ->delete(new ClusterRoleBinding($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistentVolumes(array $params = []): array {
    return $this->getClient()
      ->persistentVolume()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createPersistentVolume(array $params = []): array {
    $params['kind'] = 'PersistentVolume';
    return $this->getClient()
      ->persistentVolume()
      ->create(new PersistentVolume($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolume(array $params = []): ?array {
    return $this->updateResource(PersistentVolume::class, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deletePersistentVolume(array $params = []): array {
    $params['kind'] = 'PersistentVolume';

    return $this->getClient()
      ->persistentVolume()
      ->delete(new PersistentVolume($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageClasses(array $params = []): array {
    return $this->getClient()
      ->storageClasses()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createStorageClass(array $params = []): array {
    $params['kind'] = 'StorageClass';
    return $this->getClient()
      ->storageClasses()
      ->create(new StorageClass($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateStorageClass(array $params = []): ?array {
    return $this->updateResource(StorageClass::class, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteStorageClass(array $params = []): array {
    $params['kind'] = 'StorageClass';

    return $this->getClient()
      ->storageClasses()
      ->delete(new StorageClass($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getStatefulSets(array $params = []): array {
    return $this->getClient()
      ->statefulSets()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createStatefulSet($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->statefulSets()
      ->create(new StatefulSet($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateStatefulSet($namespace, array $params = []): ?array {
    return $this->updateResource(StatefulSet::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteStatefulSet($namespace, array $params = []): array {
    $params['kind'] = 'StatefulSet';

    return $this->getClient($namespace)
      ->statefulSets()
      ->delete(new StatefulSet($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getIngresses(array $params = []): array {
    return $this->getClient()
      ->ingresses()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createIngress($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->ingresses()
      ->create(new Ingress($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateIngress($namespace, array $params = []): ?array {
    return $this->updateResource(Ingress::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteIngress($namespace, array $params = []): array {
    $params['kind'] = 'Ingress';

    return $this->getClient($namespace)
      ->ingresses()
      ->delete(new Ingress($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getDaemonSets(array $params = []): array {
    return $this->getClient()
      ->daemonSets()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createDaemonSet($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->daemonSets()
      ->create(new DaemonSet($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateDaemonSet($namespace, array $params = []): ?array {
    return $this->updateResource(DaemonSet::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDaemonSet($namespace, array $params = []): array {
    $params['kind'] = 'DaemonSet';

    return $this->getClient($namespace)
      ->daemonSets()
      ->delete(new DaemonSet($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints(array $params = []): array {
    return $this->getClient()
      ->endpoints()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createEndpoints($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->endpoints()
      ->create(new Endpoint($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateEndpoint($namespace, array $params = []): ?array {

    $params['kind'] = 'Endpoints';

    // Remove empty properties.
    if (!empty($params)) {
      $params['subsets'] = array_filter($params['subsets']);
    }
    return $this->updateResource(Endpoint::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteEndpoint($namespace, array $params = []): array {
    $params['kind'] = 'Endpoints';

    return $this->getClient($namespace)
      ->endpoints()
      ->delete(new Endpoint($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getEvents(array $params = []): array {
    return $this->getClient()
      ->events()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function updateEvents(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_event',
      'Event',
      'getEvents',
      'updateEvent',
      $params,
      $clear
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateEventsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return $this->updateEntities(
      'k8s_event',
      'Event',
      'getEvents',
      'updateEvent',
      $params,
      $clear,
      NULL,
      FALSE
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistentVolumeClaims(array $params = []): array {
    return $this->getClient()
      ->persistentVolumeClaims()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createPersistentVolumeClaim($namespace, array $params = []): array {
    return $this->getClient($namespace)
      ->persistentVolumeClaims()
      ->create(new PersistentVolumeClaim($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolumeClaim($namespace, array $params = []): ?array {
    return $this->updateResource(PersistentVolumeClaim::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deletePersistentVolumeClaim($namespace, array $params = []): array {
    $params['kind'] = 'PersistentVolumeClaim';

    return $this->getClient($namespace)
      ->persistentVolumeClaims()
      ->delete(new PersistentVolumeClaim($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getApiServices(array $params = []): array {
    return $this->getClient()
      ->apiServices()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createApiService(array $params = []): array {
    $params['kind'] = 'APIService';
    unset($params['apiVersion']);
    return $this->getClient()
      ->apiServices()
      ->create(new ApiService($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateApiService(array $params = []): ?array {
    return $this->updateResource(ApiService::class, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteApiService(array $params = []): array {
    $params['kind'] = 'ApiService';

    return $this->getClient()
      ->apiServices()
      ->delete(new ApiService($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getPriorityClasses(array $params = []): array {
    return $this->getClient()
      ->priorityClasses()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createPriorityClass(array $params = []): array {
    $params['kind'] = 'PriorityClass';
    return $this->getClient()
      ->priorityClasses()
      ->create(new PriorityClass($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updatePriorityClass(array $params = []): ?array {
    return $this->updateResource(PriorityClass::class, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function deletePriorityClass(array $params = []): array {
    $params['kind'] = 'PriorityClass';

    return $this->getClient()
      ->priorityClasses()
      ->delete(new PriorityClass($params));
  }

  /**
   * {@inheritdoc}
   */
  public function getHorizontalPodAutoscalers(array $params = []): array {
    return $this->getClient()
      ->horizontalPodAutoscalers()
      ->setFieldSelector($params)
      ->find()
      ->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function createHorizontalPodAutoscaler($namespace, array $params = []): array {
    $params['kind'] = 'HorizontalPodAutoscaler';
    return $this->getClient($namespace)
      ->horizontalPodAutoscalers()
      ->create(new HorizontalPodAutoscaler($params));
  }

  /**
   * {@inheritdoc}
   */
  public function updateHorizontalPodAutoscaler($namespace, array $params = []): ?array {
    return $this->updateResource(HorizontalPodAutoscaler::class, $params, $namespace);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteHorizontalPodAutoscaler($namespace, array $params = []): array {
    $params['kind'] = 'HorizontalPodAutoscaler';

    return $this->getClient($namespace)
      ->horizontalPodAutoscalers()
      ->delete(new HorizontalPodAutoscaler($params));
  }

  /**
   * {@inheritdoc}
   */
  public function deleteResourcesWithEntities(array $entities, $immediately = FALSE): void {
    foreach ($entities ?: [] as $entity) {
      $this->setCloudContext($entity->getCloudContext());
      try {
        if ($immediately) {
          $this->deleteImmediately($entity);
        }
        else {
          $name_camel = $this->getShortEntityTypeNameCamel($entity);
          $method_name = "delete{$name_camel}";

          method_exists($entity, 'getNamespace')
            ? $this->$method_name($entity->getNamespace(), [
              'metadata' => ['name' => $entity->getName()],
            ])
            : $this->$method_name([
              'metadata' => ['name' => $entity->getName()],
            ]);
        }

        $entity->delete();

        $this->logOperationMessage($entity, 'deleted');

        $namespaceable = $entity->getEntityType()->get('namespaceable');
        $namespaces = [];

        // We cannot use empty() here since $namespaceable can be
        // one of two values such as FALSE or NULL.
        if (!isset($namespaceable) && !($entity instanceof K8sNamespace)) {
          $namespaces = $this->entityTypeManager
            ->getStorage('k8s_namespace')
            ->loadByProperties([
              'name' => $entity->getNamespace(),
              'cloud_context' => $entity->getCloudContext(),
            ]);
        }
        if (!empty($namespaces)) {
          $namespace = reset($namespaces);

          $this->messenger->addStatus($this->t('The @type @label (Namespace: <a href=":url">%name</a>) on @cloud_context has been deleted.', [
            '@type'  => $entity->getEntityType()->getSingularLabel(),
            '@label' => $entity->label(),
            '@cloud_context' => $entity->getCloudContext(),
            '%name' => $namespace->getName(),
            ':url' => $namespace->toUrl('canonical')->toString(),
          ]));
          continue;
        }

        $this->messenger->addStatus($this->t('The @type @label on @cloud_context has been deleted.', [
          '@type'  => $entity->getEntityType()->getSingularLabel(),
          '@label' => $entity->label(),
          '@cloud_context' => $entity->getCloudContext(),
        ]));

      }
      catch (K8sServiceException
        | \Exception $e) {

        // Using MessengerTrait::messenger().
        $this->messenger->addError($this->t('The @type %label on @cloud_context could not be deleted.', [
          '@type'  => $entity->getEntityType()->getSingularLabel(),
          '%label' => $entity->toLink($entity->label())->toString(),
          '@cloud_context' => $entity->getCloudContext(),
        ]));

        $this->logOperationErrorMessage($entity, 'deleted');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSchedules(array $entities): void {
    foreach ($entities as $entity) {
      try {
        $entity->delete();

        $this->messenger->addStatus($this->t('The @type @label on @cloud_context has been deleted.', [
          '@type'  => $entity->getEntityType()->getSingularLabel(),
          '@label' => $entity->label(),
          '@cloud_context' => $entity->getCloudContext(),
        ]));

        $this->logOperationMessage($entity, 'deleted');
      }
      catch (\Exception $e) {

        // Using MessengerTrait::messenger().
        $this->messenger->addError($this->t('The @type %label on @cloud_context could not be deleted.', [
          '@type'  => $entity->getEntityType()->getSingularLabel(),
          '%label' => $entity->toLink($entity->label())->toString(),
          '@cloud_context' => $entity->getCloudContext(),
        ]));

        $this->logOperationErrorMessage($entity, 'deleted');
      }
    }
  }

  /**
   * Set up the default parameters that all API calls will need.
   *
   * @return array
   *   Array of default parameters.
   */
  protected function getDefaultParameters(): array {
    return [];
  }

  /**
   * Initialize a new batch builder.
   *
   * @param string $batch_name
   *   The batch name.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   The initialized batch object.
   */
  protected function initBatch($batch_name): BatchBuilder {
    return (new BatchBuilder())
      ->setTitle($batch_name);
  }

  /**
   * Run the batch job to process entities.
   *
   * @param \Drupal\Core\Batch\BatchBuilder $batch_builder
   *   The batch builder object.
   */
  protected function runBatch(BatchBuilder $batch_builder): void {
    try {
      // Log the start time.
      $start = time();
      $batch_array = $batch_builder->toArray();
      batch_set($batch_array);

      // Reset the progressive so batch works w/o a web head.
      $batch = &batch_get();
      $batch['progressive'] = FALSE;
      if (PHP_SAPI === 'cli' && function_exists('drush_backend_batch_process')) {
        drush_backend_batch_process();
      }
      else {
        batch_process();
      }

      // Log the end time.
      $end = time();
      $this->logger('k8s_service')->info($this->t('@updater - @cloud_context: Batch operation took @time seconds.', [
        '@cloud_context' => $this->cloudContext,
        '@updater' => $batch_array['title'],
        '@time' => $end - $start,
      ]));
    }
    catch (Exception $e) {
      $this->handleException($e);
    }
    finally {
      // Reset the batch otherwise this operation hangs when using Drush.
      // https://www.drupal.org/project/drupal/issues/3166042
      $batch = [];
    }
  }

  /**
   * Update entities.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $entity_type_label
   *   The entity type label.
   * @param string $get_entities_method
   *   The method name of get entities.
   * @param string $update_entity_method
   *   The method name of update entity.
   * @param array $params
   *   The params for API Call.
   * @param bool $clear
   *   TRUE to clear stale entities.
   * @param string $extra_key_name
   *   The extra key name.
   * @param bool $batch_mode
   *   Whether updating entities in batch.
   * @param array $extra_data
   *   The extra data.
   *
   * @return bool
   *   True or false depending on lock name.
   *
   * @throws \Drupal\k8s\Service\K8sServiceException
   *   Thrown when unable to get get_entities_method.
   */
  private function updateEntities(
    $entity_type,
    $entity_type_label,
    $get_entities_method,
    $update_entity_method,
    array $params = [],
    $clear = TRUE,
    $extra_key_name = NULL,
    $batch_mode = TRUE,
    array $extra_data = [],
  ): bool {
    $updated = FALSE;
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $result = NULL;
    try {
      $result = $this->$get_entities_method($params);
    }
    catch (K8sServiceException $e) {
      $this->logger('k8s_service')->error($e->getMessage());
    }
    if ($result !== NULL) {
      $all_entities = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      foreach ($all_entities ?: [] as $entity) {
        $key = $entity->getName();
        if (!empty($extra_key_name)) {
          $extra_key_get_method = 'get' . ucfirst($extra_key_name);
          $key .= ':' . $entity->$extra_key_get_method();
        }
        $stale[$key] = $entity;
      }

      if ($batch_mode) {
        /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
        $batch_builder = $this->initBatch("$entity_type_label Update");
      }

      foreach ($result ?: [] as $entity) {
        if (is_object($entity)) {
          $entity = $entity->toArray();
        }

        // Keep track of snapshots that do not exist anymore
        // delete them after saving the rest of the snapshots.
        $key = $entity['metadata']['name'] ?? '';
        $metadata_namespace = $entity['metadata']['namespace'] ?? '';

        if (!empty($extra_key_name)) {
          $key .= ":{$metadata_namespace}";
        }

        if (isset($stale[$key])) {
          unset($stale[$key]);
        }

        if ($batch_mode) {
          $batch_builder->addOperation([
            K8sBatchOperations::class,
            $update_entity_method,
          ], [$this->cloudContext, $entity, $extra_data]);
        }
        else {
          K8sBatchOperations::$update_entity_method($this->cloudContext, $entity, $extra_data);
        }
      }

      if ($batch_mode) {
        $batch_builder->addOperation([
          K8sBatchOperations::class,
          'finished',
        ], [$entity_type, $stale, $clear]);
        $this->runBatch($batch_builder);
      }
      else {
        if (count($stale) && $clear === TRUE) {
          $this->entityTypeManager->getStorage($entity_type)->delete($stale);
        }
      }

      $updated = TRUE;
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Get the map of pods metrics.
   *
   * @return array
   *   The map of pods metrics. The key is "Namespace.Name".
   *
   * @throws \Drupal\k8s\Service\K8sServiceException
   *   Thrown when unable to retrieve CPU and Memory usage of pods.
   */
  private function getPodsMetricsMap(): array {
    $metrics_pods = [];
    try {
      $metrics_pods = $this->getMetricsPods();
    }
    catch (K8sServiceException $e) {
      $this->messenger->addWarning($this->t('Unable to retrieve CPU and Memory usage of pods. Install %metrics_server_link to K8s.', [
        '%metrics_server_link' => $this->getMetricsServerLink($this->cloudContext),
      ]));
    }

    $metrics = [];
    foreach ($metrics_pods ?: [] as $metrics_pod) {
      if (is_object($metrics_pod)) {
        $metrics_pod = $metrics_pod->toArray();
      }

      if (empty($metrics_pod['metadata']['namespace'])
        || empty($metrics_pod['metadata']['name'])) {
        continue;
      }

      $namespace = $metrics_pod['metadata']['namespace'];
      $name = $metrics_pod['metadata']['name'];
      $metrics["$namespace.$name"] = $metrics_pod;
    }

    return $metrics;
  }

  /**
   * Verify the connection of an API server's endpoint.
   *
   * @param string $api_server
   *   The api server's endpoint URL to validate.
   * @param string $token
   *   The token to validate w/ the api server's endpoint.
   *
   * @return bool
   *   Whether the endpoint is accessible or not.
   */
  public function isAccessible($api_server, $token): bool {
    $accessible = FALSE;
    $response = NULL;
    try {
      // Call API for verifying endpoint.
      $response = $this->httpClient->get($api_server, [
        'verify' => FALSE,
        'headers' => [
          'Authorization' => "Bearer {$token}",
        ],
      ]);

      // Get the status code of response.
      if (!empty($response) && $response->getStatusCode() === 200) {
        $accessible = TRUE;
      }
    }
    catch (\Exception $e) {
      $accessible = FALSE;
    }

    return $accessible;
  }

  /**
   * Helper method to handle Error cache.
   *
   * @param \Exception $e
   *   The Exception.
   * @param string $cloud_context
   *   The Cloud Context.
   * @param object $entity
   *   The Entity.
   */
  public function handleError(\Exception $e, $cloud_context = '', $entity = NULL): void {

    // Using MessengerTrait::messenger().
    $this->messenger->addError($this->t('The endpoint is unreachable.'));

    $cloud_service_providers = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'k8s',
        'cloud_context' => $cloud_context,
      ]);

    if (!empty($cloud_service_providers)
    && !empty($cloud_context)) {

      $cloud_service_provider = array_shift($cloud_service_providers);
      $name = $cloud_service_provider->getName();

      $page_link = Link::fromTextAndUrl(
        $name, Url::fromRoute('entity.cloud_config.edit_form', [
          'cloud_config' => $cloud_service_provider->id(),
        ])
      )->toString();

      // Using MessengerTrait::messenger().
      $this->messenger->addError($this->t('Check the API server and token: @page_link', [
        '@page_link' => $page_link,
      ]));
    }

    $this->handleException($e);

    // Basically Redirect to cloud service provider list view page.
    $route_name = 'entity.cloud_config.collection';

    // If an Entity is specified, redirect to the Entity's list view page.
    if ($entity !== NULL
    && !empty($cloud_context)) {
      $route_name = "entity.{$entity->getEntityTypeId()}.collection";
    }

    $redirect_url = Url::fromRoute($route_name, [
      'cloud_context' => $cloud_context,
    ]);

    $redirect_response = new RedirectResponse($redirect_url->toString());
    $redirect_response->send();
  }

  /**
   * Get link for the metrics server.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Drupal\Core\GeneratedLink
   *   The link for the metrics server.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMetricsServerLink($cloud_context = NULL): GeneratedLink {
    $metrics_server_url = Url::fromUri('https://github.com/kubernetes-sigs/metrics-server');
    if (!empty($cloud_context)) {
      $templates = $this->entityTypeManager
        ->getStorage('cloud_launch_template')
        ->loadByProperties(
          [
            'cloud_context' => $cloud_context,
            'name' => 'metrics_server',
          ]
        );

      if (!empty($templates)) {
        $template = reset($templates);
        $metrics_server_url = Url::fromRoute(
          'entity.cloud_launch_template.canonical',
          [
            'cloud_context' => $cloud_context,
            'cloud_launch_template' => $template->id(),
          ]
        );
      }
    }

    // NOTE: Link::fromTextAndUrl()->toString() generates not a string but
    // the HTML for the Link object. Therefore, this method returns a
    // GeneratedLink object.
    return Link::fromTextAndUrl(
      $this->t('Kubernetes Metrics Server'),
      $metrics_server_url
    )->toString();
  }

  /**
   * Not allocate resource to set namespace.
   *
   * @param string $label
   *   The name of namespace in Kubernetes cluster.
   * @param array $k8s_cluster_list
   *   The list of cloud context.
   * @param array $param
   *   Parameter set on resource quota.
   */
  public function changeResourceQuota($label, array $k8s_cluster_list, array $param): void {
    foreach ($k8s_cluster_list ?: [] as $cloud_context) {
      $this->setCloudContext($cloud_context);
      $this->updateResourceWithEntity('k8s_resource_quota', $cloud_context, $label, $param);
      $message_all = $this->messenger->all();
      $messages = array_shift($message_all);
      $output = implode('', $messages ?: []);
      $this->logger('k8s')->info($output);
    }
  }

  /**
   * Determine whether the current time is within specific time range or not.
   *
   * @param string $startup_time
   *   The start of specific time.
   * @param string $stop_time
   *   The end of specific time.
   * @param int $time
   *   The time to be validated with unix epoch time.
   *
   * @return bool
   *   TRUE means that input time is in specific time range
   *   defined by startup and stop time.
   */
  public function validateScheduledTime($startup_time, $stop_time, $time = NULL): bool {
    if (empty($time)) {
      $time = time();
    }
    $start_time = strtotime($startup_time);
    $end_time = strtotime($stop_time);
    $diff_current_time = $time - $start_time;
    $diff_end_time = $end_time - $start_time;
    if ($diff_end_time < 0) {
      $diff_current_time > $diff_end_time
        ? $end_time = strtotime('+1 day', $end_time)
        : $start_time = strtotime('-1 day', $start_time);
    }
    return $start_time <= $time && $time <= $end_time;
  }

  /**
   * Create/update resource with entity.
   *
   * @param string $type
   *   Entity type.
   * @param string $cloud_context
   *   Cloud context.
   * @param string $label
   *   The label.
   * @param array $params
   *   Parameter for type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateResourceWithEntity($type, $cloud_context, $label, array $params): void {

    $passive_operation = [
      'create' => 'created',
      'update' => 'updated',
    ];

    $entity_type = $this->entityTypeManager->getStorage($type);
    $entities = $entity_type->loadByProperties([
      'name' => $label,
      'cloud_context' => $cloud_context,
    ]);

    $action = empty($entities)
      ? 'create'
      : 'update';

    $dummy = $entity_type->create([]);
    $name_camel = $this->getShortEntityTypeNameCamel($dummy);
    $name_plural_camel = $this->getShortEntityTypeNamePluralCamel($dummy);
    $dummy->delete();

    try {
      if (empty($entities)) {
        $method = $action . $name_camel;
        $type === 'k8s_namespace'
          ? $this->$method($params)
          : $this->$method($label, $params);
      }
      else {
        $entity = array_shift($entities);
        $method = $action . $name_camel;
        $type === 'k8s_namespace'
          ? $this->$method($params)
          : $this->$method($label, $params);
        $entity->save();
      }
      $method = 'update' . $name_plural_camel;
      $this->$method([
        'metadata.name' => $label,
      ], FALSE);

      $entity_id = $this->cloudService->getEntityId($cloud_context, $type, 'name', $label);

      $this->messenger->addStatus($this->t('The @type %label on @cloud_context has been @passive_operation.', [
        '@type' => $entity_type->getEntityType()->getSingularLabel(),
        '%label' => $entity_type->load($entity_id)->toLink()->toString(),
        '@cloud_context' => $cloud_context,
        '@passive_operation' => $passive_operation[$action],
      ]));
    }
    catch (\Exception $e) {

      $this->messenger->addError($this->t('The @type %label on @cloud_context could not be @passive_operation.', [
        '@type' => $entity_type->getEntityType()->getSingularLabel(),
        '%label' => $label,
        '@cloud_context' => $cloud_context,
        '@passive_operation' => $passive_operation[$action],
      ]));
    }
  }

  /**
   * Export node metrics to log.
   *
   * @param array $metrics_nodes
   *   The metrics of nodes.
   * @param array $resources
   *   The resources.
   * @param array $extra_data
   *   The extra data.
   */
  public function exportNodeMetrics(array $metrics_nodes, array $resources, array $extra_data = []): void {
    $metrics = [];
    foreach ($metrics_nodes ?: [] as $metrics_node) {
      if (is_object($metrics_node)) {
        $metrics_node = $metrics_node->toArray();
      }
      if (empty($metrics_node['metadata']['name'])
        || empty($metrics_node['usage'])
        || !array_key_exists('cpu', $metrics_node['usage'])
        || !array_key_exists('memory', $metrics_node['usage'])) {
        continue;
      }

      $node_name = $metrics_node['metadata']['name'];
      $cpu = k8s_convert_cpu_to_float($metrics_node['usage']['cpu']);
      $memory = k8s_convert_memory_to_integer($metrics_node['usage']['memory']);
      $metrics[$this->cloudContext]['nodes'][$node_name] = [
        'cpu' => $cpu,
        'memory' => $memory,
      ];
      if (!empty($resources)) {
        $metrics[$this->cloudContext]['nodes'][$node_name] = array_merge($metrics[$this->cloudContext]['nodes'][$node_name], $resources);
      }

      $created = !empty($extra_data['resource_store_time'])
        ? $extra_data['resource_store_time']
        : time();
      // Writing metrics into cloud_store.
      $data = [
        'name' => $node_name,
        'type' => 'k8s_node_resource_store',
        'cloud_context' => $this->cloudContext,
        'field_resources' => Yaml::encode($metrics[$this->cloudContext]['nodes'][$node_name]),
        'created' => $created,
        'uid' => $this->getDefaultCloudStoreUid($this->cloudContext),
      ];

      $entity = $this->entityTypeManager
        ->getStorage('cloud_store')
        ->create($data);
      $entity->save();

      $this->cloudCacheService->setLatestCreatedTimeCache(
        $this->cloudContext,
        'k8s_node_resource_store',
        $node_name,
        $created
      );
    }

  }

  /**
   * Export pod metrics to log.
   *
   * @param array $metrics_pods
   *   The metrics of nodes.
   */
  public function exportPodMetrics(array $metrics_pods): void {
    $metrics = [];
    $pod_name = '';
    $pod_namespace = '';
    foreach ($metrics_pods ?: [] as $metrics_pod) {
      if (is_object($metrics_pod)) {
        $metrics_pod = $metrics_pod->toArray();
      }
      $pod_namespace = $metrics_pod['metadata']['namespace'];
      $pod_name = $metrics_pod['metadata']['name'];

      $cpu = 0;
      $memory = 0;
      foreach ($metrics_pod['containers'] ?: [] as $container) {
        $cpu += k8s_convert_cpu_to_float($container['usage']['cpu']);
        $memory += k8s_convert_memory_to_integer($container['usage']['memory']);

      }

      $metrics[$this->cloudContext]['pods']["$pod_namespace:$pod_name"] = [
        'cpu' => $cpu,
        'memory' => $memory,
      ];

      $created = time();
      // Writing metrics into cloud_store.
      $data = [
        'name' => "$pod_namespace:$pod_name",
        'type' => 'k8s_pod_resource_store',
        'cloud_context' => $this->cloudContext,
        'field_resources' => Yaml::encode($metrics[$this->cloudContext]['pods']["$pod_namespace:$pod_name"]),
        'created' => $created,
        'uid' => $this->getDefaultCloudStoreUid($this->cloudContext),
      ];

      $entity = $this->entityTypeManager
        ->getStorage('cloud_store')
        ->create($data);
      $entity->save();
    }

    $this->cloudCacheService->setLatestCreatedTimeCache(
      $this->cloudContext,
      'k8s_pod_resource_store',
      "$pod_namespace:$pod_name",
      $created ?? time()
    );

  }

  /**
   * {@inheritdoc}
   */
  public function createResourceQueueItems(): void {
    $queue_limit = $this->configFactory->get('k8s.settings')->get('k8s_queue_limit');
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    if (empty($cloud_config)) {
      return;
    }
    $method_names = [
      'updateApiServices',
      'updateClusterRoleBindings',
      'updateClusterRoles',
      'updateConfigMaps',
      'updateCronJobs',
      'updateDaemonSets',
      'updateDeployments',
      'updateEndpoints',
      'updateEvents',
      'updateHorizontalPodAutoscalers',
      'updateIngresses',
      'updateJobs',
      'updateLimitRanges',
      'updateNamespaces',
      'updateNetworkPolicies',
      'updateNodes',
      'updatePersistentVolumeClaims',
      'updatePersistentVolumes',
      'updatePods',
      'updatePriorityClasses',
      'updateReplicaSets',
      'updateResourceQuotas',
      'updateRoles',
      'updateRoleBindings',
      'updateSecrets',
      'updateServiceAccounts',
      'updateServices',
      'updateStatefulSets',
      'updateStorageClasses',
    ];
    self::updateResourceQueue($cloud_config, $method_names, $queue_limit);
  }

  /**
   * Format costs.
   *
   * @param int $costs
   *   The costs.
   * @param int $total_costs
   *   The total costs.
   * @param int $precision
   *   The precision.
   *
   * @return string
   *   The formatted costs string.
   */
  public function formatCosts($costs, $total_costs, $precision = 2): string {
    $costs_str = sprintf('%.' . $precision . 'f', $costs);
    $percentage = $total_costs > 0 ? sprintf('%.1f', $costs / $total_costs * 100) : '0.0';
    return "$costs_str ($percentage%)";
  }

  /**
   * Format cpu usage.
   *
   * @param float $cpu_usage
   *   The cpu usage.
   * @param float $cpu_capacity
   *   The cpu capacity.
   *
   * @return string
   *   The formatted cpu usage string.
   */
  public function formatCpuUsage($cpu_usage, $cpu_capacity): string {
    $cpu_str = sprintf('%.2f', $cpu_usage);
    $percentage = $cpu_capacity > 0 ? sprintf('%.1f', $cpu_usage / $cpu_capacity * 100) : '0.0';
    return "$cpu_str ($percentage%)";
  }

  /**
   * Format memory usage.
   *
   * @param int $memory_usage
   *   The memory usage.
   * @param int $memory_capacity
   *   The memory capacity.
   *
   * @return string
   *   The formatted memory usage string.
   */
  public function formatMemoryUsage($memory_usage, $memory_capacity): string {
    $memory_str = CloudService::formatMemory($memory_usage);
    $percentage = sprintf('%.1f', $memory_usage / $memory_capacity * 100);
    return "$memory_str ($percentage%)";
  }

  /**
   * Format pod usage.
   *
   * @param int $pod_usage
   *   The pod usage.
   * @param int $pod_capacity
   *   The pod capacity.
   *
   * @return string
   *   The formatted pod usage string.
   */
  public function formatPodUsage($pod_usage, $pod_capacity): string {
    $percentage = $pod_capacity > 0 ? sprintf('%.1f', $pod_usage / $pod_capacity * 100) : '0.0';
    return "$pod_usage/$pod_capacity ($percentage%)";
  }

  /**
   * Get total costs of nodes.
   *
   * @param array $nodes
   *   The k8s_node entities.
   * @param string $cost_type
   *   The cost type.
   * @param bool $refresh
   *   TRUE to refresh pricing data.
   *
   * @return int
   *   The total costs of nodes.
   */
  public function getTotalCosts(array $nodes, $cost_type, $refresh = TRUE): int {
    $costs = 0;

    if (!$this->moduleHandler->moduleExists('aws_cloud')) {
      return $costs;
    }

    /** @var \Drupal\aws_cloud\Service\Pricing\InstanceTypePriceDataProvider $price_data_provider */
    // @phpstan-ignore-next-line
    $price_date_provider = \Drupal::service('aws_cloud.instance_type_price_data_provider');
    if (empty($price_date_provider)) {
      return $costs;
    }

    foreach ($nodes ?: [] as $node) {
      // Get instance type and region.
      $region = NULL;
      $instance_type = NULL;
      $labels = $node->get('labels');
      foreach ($labels ?: [] as $item) {
        if ($item->getItemKey() === 'node.kubernetes.io/instance-type') {
          $instance_type = $item->getItemValue();
        }
        elseif ($item->getItemKey() === 'failure-domain.beta.kubernetes.io/region') {
          $region = $item->getItemValue();
        }
      }

      if (empty($instance_type) || empty($region)) {
        continue;
      }

      $price_data = $price_date_provider->getDataByRegion($region, NULL, NULL, NULL, $refresh);
      foreach ($price_data ?: [] as $item) {
        if ($item['instance_type'] === $instance_type) {
          if (!empty($item[$cost_type])) {
            $costs += $item[$cost_type];
          }
          break;
        }
      }
    }

    return $costs;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeCost(K8sNode $node, $refresh = TRUE): array {
    $costs = [];
    $controller = $this->classResolver->getInstanceFromDefinition(K8sCostsControllerBase::class);
    $cost_types = array_keys($controller->getEc2CostTypes() ?: []);

    if (!$this->moduleHandler->moduleExists('aws_cloud')) {
      return $costs;
    }

    /** @var \Drupal\aws_cloud\Service\Pricing\InstanceTypePriceDataProvider $price_data_provider */
    // @phpstan-ignore-next-line
    $price_date_provider = \Drupal::service('aws_cloud.instance_type_price_data_provider');
    if (empty($price_date_provider)) {
      return $costs;
    }

    // Get instance type and region.
    $region = NULL;
    $instance_type = NULL;
    $labels = $node->get('labels');
    foreach ($labels ?: [] as $item) {
      if ($item->getItemKey() === 'node.kubernetes.io/instance-type') {
        $instance_type = $item->getItemValue();
      }

      if ($item->getItemKey() === 'failure-domain.beta.kubernetes.io/region') {
        $region = $item->getItemValue();
      }
    }

    if (empty($instance_type) || empty($region)) {
      return $costs;
    }

    $price_data = $price_date_provider->getDataByRegion($region, NULL, NULL, NULL, $refresh);
    foreach ($price_data ?: [] as $item) {
      if ($item['instance_type'] !== $instance_type) {
        continue;
      }

      foreach ($cost_types ?: [] as $cost_type) {
        if (empty($item[$cost_type])) {
          continue;
        }
        $costs[$cost_type] = $item[$cost_type];
      }
      break;
    }

    return $costs;
  }

  /**
   * {@inheritdoc}
   */
  public function getNamespaceResourceUsage($cloud_context, $namespace): array {
    $row = [
      'cpu_usage' => 0,
      'memory_usage' => 0,
      'pod_usage' => 0,
      'collect_time' => time(),
      'namespace' => [
        'data' => [
          '#type' => 'link',
          '#title' => $namespace,
          '#url' => Url::fromRoute(
            'entity.k8s_namespace.canonical',
            [
              'cloud_context' => $cloud_context,
              'k8s_namespace' => $namespace,
            ]
          ),
        ],
      ],
    ];

    $cache_data = $this->cloudCacheService->getResourceDataCache($cloud_context, 'k8s_pod', $namespace);

    if (!empty($cache_data)) {
      foreach ($cache_data ?: [] as $data) {
        $row['cpu_usage'] += $data['cpu_usage'];
        $row['memory_usage'] += $data['memory_usage'];
      }
      $row['pod_usage'] = count($cache_data);
      return $row;
    }

    $pods = $this->entityTypeManager
      ->getStorage('k8s_pod')->loadByProperties([
        'cloud_context' => $cloud_context,
        'namespace' => $namespace,
      ]);
    if (empty($pods)) {
      return $row;
    }

    $cpu_usage = array_sum(array_map(static function ($pod) {
      return $pod->getCpuUsage();
    }, $pods));

    $memory_usage = array_sum(array_map(static function ($pod) {
      return $pod->getMemoryUsage();
    }, $pods));
    $pod_usage = count($pods);

    $row['cpu_usage'] = $cpu_usage;
    $row['memory_usage'] = $memory_usage;
    $row['pod_usage'] = $pod_usage;

    // Set pod resource data in cache.
    foreach ($pods ?: [] as $pod) {
      $this->setPodResourceDataCache($pod);
    }

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeResourceUsage($cloud_context): array {
    $row = [
      'cpu_capacity' => 0,
      'memory_capacity' => 0,
      'pod_capacity' => 0,
      'total_costs' => [],
    ];

    $cache_data = $this->cloudCacheService->getResourceDataCache($cloud_context, 'k8s_node');

    if (!empty($cache_data)) {
      foreach ($cache_data ?: [] as $data) {
        $row['cpu_capacity'] += $data['cpu_capacity'];
        $row['memory_capacity'] += $data['memory_capacity'];
        $row['pod_capacity'] += $data['pod_capacity'];
        foreach ($data['costs'] ?: [] as $cost_type => $cost) {
          if (empty($row['total_costs'][$cost_type])) {
            $row['total_costs'][$cost_type] = 0;
          }
          $row['total_costs'][$cost_type] += $cost;
        }
      }
      return $row;
    }

    $controller = $this->classResolver->getInstanceFromDefinition(K8sCostsControllerBase::class);
    $total_costs = array_fill_keys(
      array_keys($controller->getEc2CostTypes() ?: []), 0
    );

    $nodes = $this->entityTypeManager
      ->getStorage('k8s_node')->loadByProperties(
        [
          'cloud_context' => $cloud_context,
        ]
      );
    if (empty($nodes)) {
      return $row;
    }

    $self = $this;
    array_walk($total_costs, static function (&$cost, $cost_type) use ($self, $nodes) {
      $cost = $self->getTotalCosts($nodes, $cost_type);
    });

    $cpu_capacity = array_sum(array_map(static function ($node) {
      return $node->getCpuCapacity();
    }, $nodes ?: []));

    $memory_capacity = array_sum(array_map(static function ($node) {
      return $node->getMemoryCapacity();
    }, $nodes ?: []));

    $pod_capacity = array_sum(array_map(static function ($node) {
      return $node->getPodsCapacity();
    }, $nodes ?: []));

    $row['cpu_capacity'] = $cpu_capacity;
    $row['memory_capacity'] = $memory_capacity;
    $row['pod_capacity'] = $pod_capacity;
    $row['total_costs'] = $total_costs;

    // Set node resource data in cache.
    foreach ($nodes ?: [] as $node) {
      $this->setNodeResourceDataCache($node);
    }

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateCostPerNamespace($cloud_context, $namespace): array {
    // Get the metrics about resource usage.
    $namespaces_resource = $this->getNamespaceResourceUsage($cloud_context, $namespace);
    $nodes_resource = $this->getNodeResourceUsage($cloud_context);
    $cpu_usage = $namespaces_resource['cpu_usage'];
    $memory_usage = $namespaces_resource['memory_usage'];
    $pod_usage = $namespaces_resource['pod_usage'];
    $cpu_capacity = $nodes_resource['cpu_capacity'];
    $memory_capacity = $nodes_resource['memory_capacity'];
    $pod_capacity = $nodes_resource['pod_capacity'];
    $total_costs = $nodes_resource['total_costs'];
    $collect_time = $namespaces_resource['collect_time'];

    // Calculate cost.
    array_walk($total_costs,
      static function (
        &$cost,
        $cost_type,
      ) use (
        $cpu_usage,
        $cpu_capacity,
        $memory_usage,
        $memory_capacity,
        $pod_usage,
        $pod_capacity
      ) {
        $cost = $cost > 0
          ? ($cpu_usage / $cpu_capacity + $memory_usage / $memory_capacity + $pod_usage / $pod_capacity) / 3 * $cost
          : 0;
      });

    return [
      'resources' => [
        'cpu' => $cpu_usage,
        'memory' => $memory_usage,
        'pod_allocation' => $pod_usage,
        'cpu_capacity' => $cpu_capacity,
        'memory_capacity' => $memory_capacity,
        'pod_capacity' => $pod_capacity,
      ],
      'costs' => $total_costs,
      'collect_time' => $collect_time,
    ];
  }

  /**
   * Automatically select k8s cluster based on usage resource.
   *
   * @return string
   *   A selected k8s cluster.
   */
  public function selectK8sClusterByResourceUsage(): string {
    $entities = $this->cloudConfigPluginManager->loadConfigEntities('k8s');
    $cloud_contexts = array_map(static function ($entity) {
      return $entity->getCloudContext();
    }, $entities);
    if (empty($cloud_contexts)) {
      return '';
    }

    $response = [];
    foreach ($cloud_contexts ?: [] as $cloud_context) {
      // Confirm whether a current user has the permissions or not.
      $account = $this->currentUser;
      if (!$account->hasPermission('view all cloud service providers')
        && !$account->hasPermission('view ' . $cloud_context)) {
        continue;
      }

      if (!empty($cloud_context)) {
        $this->setCloudContext($cloud_context);
        try {
          $this->getMetricsNodes();
        }
        catch (K8sServiceException $e) {
          continue;
        }
      }

      $nodes = $this->entityTypeManager
        ->getStorage('k8s_node')
        ->loadByProperties([
          'cloud_context' => $cloud_context,
        ]);

      $cpu_capacity = array_sum(array_map(static function ($node) {
        return $node->getCpuCapacity();
      }, $nodes));

      $cpu_request = array_sum(array_map(static function ($node) {
        return $node->getCpuRequest();
      }, $nodes));

      $memory_capacity = array_sum(array_map(static function ($node) {
        return $node->getMemoryCapacity();
      }, $nodes));

      $memory_request = array_sum(array_map(static function ($node) {
        return $node->getMemoryRequest();
      }, $nodes));

      $pods_capacity = array_sum(array_map(static function ($node) {
        return $node->getPodsCapacity();
      }, $nodes));

      $pods_allocation = array_sum(array_map(static function ($node) {
        return $node->getPodsAllocation();
      }, $nodes));

      $response[$cloud_context] = [
        'sort_key' => round($cpu_request / $cpu_capacity + $memory_request / $memory_capacity + $pods_allocation / $pods_capacity, 3),
      ];
    }

    $sort_key = array_column($response, 'sort_key');
    array_multisort($sort_key, SORT_ASC, $response);
    return array_keys($response)[0];
  }

  /**
   * {@inheritdoc}
   */
  public function updateK8sNamespaceResourceStoreEntity($cloud_context, $namespace, array $extra_data = []): void {

    $result = $this->calculateCostPerNamespace($cloud_context, $namespace);
    $costs = $result['costs'];
    $collect_time = !empty($extra_data['resource_store_time'])
      ? $extra_data['resource_store_time']
      : $result['collect_time'];

    $data = [
      'cloud_context' => $cloud_context,
      'name' => $namespace,
      'type' => 'k8s_namespace_resource_store',
      'field_costs' => Yaml::encode($costs),
      'field_resources' => Yaml::encode($result['resources']),
      'created' => $collect_time,
      'uid' => $this->getDefaultCloudStoreUid($cloud_context),
    ];

    $entity = $this->entityTypeManager
      ->getStorage('cloud_store')
      ->create($data);
    $entity->save();

    $this->cloudCacheService->setLatestCreatedTimeCache(
      $cloud_context,
      'k8s_namespace_resource_store',
      $namespace,
      $collect_time
    );
  }

  /**
   * Defines the supported Cloud launch template types.
   *
   * @return array
   *   An array of supported templates.
   */
  public function supportedCloudLaunchTemplates(): array {
    $k8s_types = [];

    foreach ($this->entityTypeManager->getDefinitions() ?: [] as $name => $definition) {
      if ($definition->entityClassImplements(K8sEntityBase::class)) {
        $class_parts = explode('\\', $definition->getClass());
        $class_name = end($class_parts);
        $k8s_types[str_replace('k8s_', '', $name)] = str_replace('K8s', '', $class_name);
      }
    }

    // Those K8s resources (entities) are exceptions to handle the "Kind:" name.
    $k8s_types['service'] = 'Service';
    $k8s_types['api_service'] = 'APIService';
    $k8s_types['endpoint'] = 'Endpoints';
    $k8s_types['mixed'] = 'Mixed';

    natcasesort($k8s_types);
    return $k8s_types;
  }

  /**
   * Decode yaml file with multiple documents.
   *
   * @param string $yaml
   *   The yaml content.
   *
   * @return array
   *   The yaml array decoded.
   */
  public function decodeMultipleDocYaml(string $yaml): array {
    $docs = [];
    $lines = explode("\n", $yaml ?: '');
    $last_doc_lines = [];
    foreach ($lines ?: [] as $line) {
      if (rtrim($line ?: '') === '---') {
        if (!empty($last_doc_lines)) {
          $docs[] = implode("\n", $last_doc_lines);
          $last_doc_lines = [];
        }
        continue;
      }

      // Because the bug of the parser, skip the comment line.
      if (strpos(ltrim($line ?: ''), '#') === 0) {
        continue;
      }

      $last_doc_lines[] = rtrim($line ?: '');
    }
    if (!empty($last_doc_lines)) {
      $docs[] = implode("\n", $last_doc_lines);
    }

    $result = [];
    try {
      foreach ($docs ?: [] as $doc) {
        $result[] = Yaml::decode($doc);
      }
    }
    catch (\Exception $e) {
      $this->handleError($e);
      $this->logger('k8s_service')->error($this->t('An error occurred while decoding a manifest (YAML): @exception', [
        '@exception' => $e->getMessage(),
      ]));
    }

    return $result;

  }

  /**
   * Set allowed values for the K8s cluster.
   *
   * @return array
   *   An array of possible key and value options.
   */
  public function clusterAllowedValues(): array {
    $options = [];
    $account = $this->currentUser;

    $k8s_cloud_configs = $this->cloudConfigPluginManager->loadConfigEntities('k8s');
    foreach ($k8s_cloud_configs ?: [] as $k8s_cloud_config) {
      if ($account->hasPermission('view ' . $k8s_cloud_config->getCloudContext())
        || $account->hasPermission('view all cloud service providers')) {

        $options[$k8s_cloud_config->getCloudContext()] = $k8s_cloud_config->getName();
      }
    }

    natcasesort($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function isGitCommandAvailable(): bool {
    $available = FALSE;

    $output = [];
    $return_var = 0;
    exec('git --version', $output, $return_var);
    if ($return_var === 0) {
      $available = TRUE;
    }
    return $available;
  }

  /**
   * {@inheritdoc}
   */
  public function updateScheduledResources(): void {
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    $node_cpu_limit = $cloud_config->get('field_node_cpu_limit')->value ?: K8sServiceInterface::DEFAULT_NODE_CPU_LIMIT;
    $node_memory_limit = $cloud_config->get('field_node_memory_limit')->value ?: K8sServiceInterface::DEFAULT_NODE_MEMORY_LIMIT;
    $pod_delete_count = $cloud_config->get('field_pod_delete_count')->value ?: K8sServiceInterface::DEFAULT_POD_DELETE_COUNT;

    // Get all scheduled resources.
    $schedules = $this->entityTypeManager
      ->getStorage('k8s_schedule')
      ->loadByProperties([
        'cloud_context' => $this->cloudContext,
      ]);
    $scheduled_resources = [];
    foreach ($schedules ?: [] as $schedule) {
      $scheduled_resources[] = "{$schedule->getKind()}:{$schedule->getNamespaceName()}:{$schedule->getResourceName()}";
    }

    $scheduled_resources_deleted = [];
    $all_nodes_busy = TRUE;

    $nodes = $this->entityTypeManager
      ->getStorage('k8s_node')
      ->loadByProperties([
        'cloud_context' => $this->cloudContext,
      ]);
    foreach ($nodes ?: [] as $node) {
      // Skip if the resource usage of the node is not over the limit.
      if ($node->getCpuUsage() / $node->getCpuCapacity() * 100 <= $node_cpu_limit
        && $node->getMemoryUsage() / $node->getMemoryCapacity() * 100 <= $node_memory_limit) {
        $all_nodes_busy = FALSE;
        continue;
      }

      $this->logger('k8s_service')->info(
        $this->t('Node @node_name is over the limit.', ['@node_name' => $node->getName()])
      );

      // Get pods running in the node.
      $pods = $this->entityTypeManager
        ->getStorage('k8s_pod')
        ->loadByProperties([
          'cloud_context' => $this->cloudContext,
          'node_name' => $node->getName(),
          'status' => 'Running',
        ]);

      // Sort pods by oldest first.
      usort($pods, static function ($a, $b) {
        if ($a->created() === $b->created()) {
          return 0;
        }

        return $a->created() < $b->created() ? -1 : 1;
      });

      // Remove scheduled pods up to $pod_delete_count.
      $count = $pod_delete_count;
      foreach ($pods ?: [] as $pod) {
        $key = "Pod:{$pod->getNamespace()}:{$pod->getName()}";
        if (!in_array($key, $scheduled_resources)) {
          continue;
        }

        if (--$count < 0) {
          break;
        }

        $this->deletePod($pod->getNamespace(), [
          'metadata' => [
            'name' => $pod->getName(),
          ],
        ]);

        $pod->delete();

        $this->logger('k8s_service')->info($this->t('The pod "@pod" of namespace "@namespace" is deleted because the node "@node" is busy.', [
          '@pod' => $pod->getName(),
          '@namespace' => $pod->getNamespace(),
          '@node' => $node->getName(),
        ]));

        $scheduled_resources_deleted[] = $key;
      }
    }

    // Update a create cronjob scheduler.
    $cronjob_schedules = $this->entityTypeManager
      ->getStorage('k8s_schedule')
      ->loadByProperties([
        'cloud_context' => $this->cloudContext,
        'schedule_type' => K8sContentFormInterface::CRONJOB_SCHEDULER,
      ]);
    foreach ($cronjob_schedules ?: [] as $schedule) {
      if (!empty($schedule->getResourceName())
        && !empty($schedule->getNamespaceName())
        && !empty($schedule->getKind())
        && !empty($schedule->getYamlUrl())
      ) {
        $this->updateScheduleCreateCronJob(
          $schedule->getResourceName(),
          $schedule->getNamespaceName(),
          $this->getKebabCase($schedule->getKind()),
          $schedule->getYamlUrl()
        );
      }
    }

    // Create or delete resources according to schedules.
    foreach ($schedules ?: [] as $schedule) {
      if ($schedule->getScheduleType() === K8sContentFormInterface::CRONJOB_SCHEDULER) {
        continue;
      }
      $kind = $schedule->getKind() ?? '';
      $resource_name = $schedule->getResourceName();
      $namespace_name = $schedule->getNamespaceName();
      $start_hour = $schedule->getStartHour();
      $start_minute = $schedule->getStartMinute();
      $stop_hour = $schedule->getStopHour();
      $stop_minute = $schedule->getStopMinute();

      $resources = $this->entityTypeManager
        ->getStorage('k8s_' . strtolower($kind))
        ->loadByProperties([
          'cloud_context' => $this->cloudContext,
          'name' => $resource_name,
          'namespace' => $namespace_name,
        ]);

      $current_hour_minute = date('Hi');
      $start_hour_minute = sprintf('%02d%02d', $start_hour, $start_minute);
      $stop_hour_minute = sprintf('%02d%02d', $stop_hour, $stop_minute);

      // $start_hour_minute can be smaller than $stop_hour_minute.
      // For example, from 3:00 to 15:00.
      // Also, $start_hour_minute can be larger than $stop_hour_minute.
      // For example, from 23:00 to 6:00.
      // In this case, the period is combined by two parts. One is from 23:00
      // to 24:00, the other is from 0:00 to 6:00.
      $is_in_period = (
        ($start_hour_minute <= $stop_hour_minute)
        && ($current_hour_minute >= $start_hour_minute && $current_hour_minute <= $stop_hour_minute)
      )
      || (
        ($start_hour_minute > $stop_hour_minute)
        && ($current_hour_minute >= $start_hour_minute || $current_hour_minute <= $stop_hour_minute)
      );

      // Update schedule's state.
      if (empty($resources)) {
        $schedule->setState('');
        $schedule->save();
      }

      if (empty($resources) && $is_in_period) {
        // Skip if all nodes are busy.
        if ($all_nodes_busy) {
          $this->logger('k8s_service')->info($this->t(
            'Skip to create @resource_name because all nodes are busy.',
            ['@resource_name' => $resource_name]
          ));
          continue;
        }

        // Skip if the resource was deleted just now.
        if (in_array("$kind:$namespace_name:$resource_name", $scheduled_resources_deleted, TRUE)) {
          $this->logger('k8s_service')->info($this->t(
            'Skip to create @resource_name because the @resource_name was deleted just now.',
            ['@resource_name' => $resource_name]
          ));
          continue;
        }

        // Create a resource.
        $params = Yaml::decode($schedule->getManifest());
        // Set Owner ID.
        $uid_tag_key = $this->cloudService->getTagKeyCreatedByUid('k8s', $this->cloudContext);
        $uid = $schedule->getOwnerId();
        $params['metadata']['annotations'][$uid_tag_key] = "$uid";
        if ($kind === 'Deployment') {
          // Add owner uid to pod template.
          $params['spec']['template']['metadata']['annotations'][$uid_tag_key] = "$uid";
        }

        $create_method = 'create' . $kind;
        $this->$create_method($namespace_name, $params);

        $update_method = "update{$kind}sWithoutBatch";
        $this->$update_method([
          'metadata.name' => $resource_name,
        ], FALSE);

        $this->logger('k8s_service')->info($this->t('Time scheduler: the @kind "@resource_name" of namespace "@namespace_name" is created.', [
          '@kind' => $kind,
          '@resource_name' => $resource_name,
          '@namespace_name' => $namespace_name,
        ]));

        continue;
      }

      // Remove resources out of period.
      if (!empty($resources) && !$is_in_period) {
        // Delete a resource.
        $delete_method = 'delete' . $kind;
        $this->$delete_method($namespace_name, [
          'metadata' => [
            'name' => $resource_name,
          ],
        ]);

        // There should be single resource in the array $resources.
        $resource = array_values($resources)[0];
        $resource->delete();

        // Update schedule's state.
        $schedule->setState('');
        $schedule->save();

        $this->logger('k8s_service')->info($this->t('Time scheduler: the @kind "@resource_name" of namespace "@namespace_name" is deleted.', [
          '@kind' => $kind,
          '@resource_name' => $resource_name,
          '@namespace_name' => $namespace_name,
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteImmediately(K8sEntityBase $entity): array {
    $this->setCloudContext($entity->getCloudContext());

    $name_camel = $this->getShortEntityTypeNameCamel($entity);

    $classPath = NULL;
    if (class_exists("Maclof\\Kubernetes\\Models\\$name_camel")) {
      $classPath = "Maclof\\Kubernetes\\Models\\$name_camel";
    }
    elseif (class_exists("Drupal\\k8s\\Service\\K8sClientExtension\\Models\\K8s{$name_camel}Model")) {
      $classPath = "Drupal\\k8s\\Service\\K8sClientExtension\\Models\\K8s{$name_camel}Model";
    }

    if (empty($classPath)) {
      // Using MessengerTrait::messenger().
      $this->messenger->addError($this->t('There is no Model Class for @type %label on @cloud_context.', [
        '@type'  => $entity->getEntityType()->getSingularLabel(),
        '%label' => $entity->toLink($entity->label())->toString(),
        '@cloud_context' => $entity->getCloudContext(),
      ]));

      return [];
    }

    $apiVersion = (new $classPath)->getApiVersion() !== 'v1' ? (new $classPath)->getApiVersion() : NULL;

    $plural_label = $entity->getEntityType()->getPluralLabel() ?? '';
    $uri = str_replace(' ', '', (strtolower($plural_label)));

    // We cannot use empty() here since namespaceable can be
    // one of two values such as FALSE or NULL.
    $namespaceable = $entity->getEntityType()->get('namespaceable') === NULL;

    $namespace = $namespaceable ? $entity->getNamespace() : '';
    $query = ['gracePeriodSeconds' => 0];

    return $this->getClient($namespace)->sendRequest('DELETE', '/' . $uri . '/' . $entity->getName(), $query, NULL, $namespaceable, $apiVersion);
  }

  /**
   * {@inheritdoc}
   */
  public function hasResource($entity_type_id, array $params): bool {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $method = lcfirst($this->getShortEntityTypeNamePluralCamelByType($entity_type));

    $result = $this->getClient()
      ->$method()
      ->setFieldSelector($params)
      ->find()
      ->toArray();

    return !empty($result);
  }

  /**
   * Create entity from yaml.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $yaml
   *   The yaml.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   return entity object.
   */
  public function createEntityFromYaml(string $entity_type_id, array $yaml): ?EntityInterface {
    return K8sBatchOperations::createEntityFromYaml($entity_type_id, $this->cloudContext, $yaml);
  }

  /**
   * Get the template Yaml for ScheduleCronJob.
   *
   * @param string $type
   *   Specify whether to start/stop/create/create-git.
   *
   * @return array
   *   return yaml.
   */
  private function getTemplateYamlForScheduleCronJob(string $type): array {
    $templates_path = realpath(
      $this->extensionPathResolver->getPath('module', 'k8s')
    ) . '/templates';
    $filename = $templates_path . "/schedule-$type-cron-job.yml";
    $file = file_get_contents($filename);
    return !empty($file) ? Yaml::decode($file) : [];
  }

  /**
   * Get the template Yaml for ScheduleCronJob create Git.
   *
   * @return array
   *   return yaml.
   */
  private function getTemplateCreateGitYamlForScheduleCronJob(): array {
    return $this->getTemplateYamlForScheduleCronJob('create-git');
  }

  /**
   * Create a yaml for Schedule CronJob to be created from Git.
   *
   * @param string $name
   *   The resource name.
   * @param string $namespace
   *   The namespace.
   * @param string $resource_name
   *   The resource name.
   * @param string $git_repository
   *   The git repository.
   * @param string $git_branch
   *   The git branch.
   * @param string $resource_path
   *   The resource path.
   * @param string $git_username
   *   The git username.
   * @param bool $is_in_period
   *   Whether the schedule period is in progress or not.
   *
   * @return array
   *   return yaml.
   */
  public function createGitScheduleCronJobYaml(
    string $name,
    string $namespace,
    string $resource_name,
    string $git_repository,
    string $git_branch,
    string $resource_path,
    string $git_username,
    bool $is_in_period = FALSE,
  ): array {
    $yaml = $this->getTemplateCreateGitYamlForScheduleCronJob();
    if (empty($yaml)) {
      return [];
    }

    // Get CronJob parameters.
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    $node_cpu_limit = $cloud_config->get('field_node_cpu_limit')->value ?: K8sServiceInterface::DEFAULT_NODE_CPU_LIMIT;
    $node_memory_limit = $cloud_config->get('field_node_memory_limit')->value ?: K8sServiceInterface::DEFAULT_NODE_MEMORY_LIMIT;

    $yaml['metadata']['name'] = "$name-$resource_name-create-cron-job";
    $yaml['metadata']['namespace'] = $namespace;
    $yaml['spec']['suspend'] = !empty($is_in_period) ? TRUE : FALSE;
    $pod_yaml = $yaml['spec']['jobTemplate']['spec']['template'];
    $pod_yaml['spec']['containers'][0]['name'] = "$name-$resource_name-create";
    $env = [
      [
        'name' => 'RESOURCE_NAME',
        'value' => $name,
      ],
      [
        'name' => 'NAMESPACE',
        'value' => $namespace,
      ],
      [
        'name' => 'GIT_REPOSITORY',
        'value' => str_replace('https://', '', $git_repository),
      ],
      [
        'name' => 'GIT_BRANCH',
        'value' => $git_branch,
      ],
      [
        'name' => 'RESOURCE_PATH',
        'value' => $resource_path,
      ],
      [
        'name' => 'RESOURCE_TYPE',
        'value' => $resource_name,
      ],
      [
        'name' => 'NODE_CPU_LIMIT',
        'value' => $node_cpu_limit,
      ],
      [
        'name' => 'NODE_MEMORY_LIMIT',
        'value' => $node_memory_limit,
      ],
      [
        'name' => 'MY_CRONJOB_NAME',
        'value' => $yaml['metadata']['name'],
      ],
    ];
    if (!empty($git_username)) {
      $env[] = [
        'name' => 'GIT_USERNAME',
        'value' => $git_username,
      ];
      $env[] = [
        'name' => 'SECRET_USERNAME',
        'valueFrom' => [
          'secretKeyRef' => [
            'name' => "schedule-gitaccess-$git_username",
            'key' => 'username',
          ],
        ],
      ];
      $env[] = [
        'name' => 'SECRET_ACCESS_TOKEN',
        'valueFrom' => [
          'secretKeyRef' => [
            'name' => "schedule-gitaccess-$git_username",
            'key' => 'password',
          ],
        ],
      ];
    }

    $pod_yaml['spec']['containers'][0]['env'] = $env;
    $yaml['spec']['jobTemplate']['spec']['template'] = $pod_yaml;

    return $yaml;
  }

  /**
   * Create a yaml for Schedule CronJob.
   *
   * @param string $name
   *   The resource name.
   * @param string $namespace
   *   The namespace.
   * @param string $hour
   *   The hour of the schedule.
   * @param string $minute
   *   The minute of the schedule.
   * @param string $type
   *   Specify whether to start or stop.
   * @param string $resource_name
   *   The resource name.
   * @param string $yaml_url
   *   The YAML URL of the pod to start.
   * @param bool $is_in_period
   *   Whether the schedule period is in progress or not.
   *
   * @return array
   *   return yaml.
   */
  public function createScheduleCronJobYaml(
    string $name,
    string $namespace,
    string $hour,
    string $minute,
    string $type,
    string $resource_name,
    string $yaml_url = '',
    bool $is_in_period = FALSE,
  ): array {
    $yaml = $this->getTemplateYamlForScheduleCronJob($type);
    if (empty($yaml)) {
      return [];
    }

    // Convert k8s timezone.
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    $k8s_timezone = $cloud_config->get('field_timezone')->value ?: K8sServiceInterface::DEFAULT_TIMEZONE;
    $node_cpu_limit = $cloud_config->get('field_node_cpu_limit')->value ?: K8sServiceInterface::DEFAULT_NODE_CPU_LIMIT;
    $node_memory_limit = $cloud_config->get('field_node_memory_limit')->value ?: K8sServiceInterface::DEFAULT_NODE_MEMORY_LIMIT;
    if ($hour !== '' && $minute !== '') {
      $converted_time = DrupalDateTime::createFromTimestamp(strtotime("$hour:$minute"), $k8s_timezone);
      $converted_hour = $converted_time->format('H');
      $converted_minute = $converted_time->format('i');
    }

    $yaml['metadata']['name'] = "$name-$resource_name-$type-cron-job";
    $yaml['metadata']['namespace'] = $namespace;
    $yaml['spec']['schedule'] = !empty($converted_hour) && !empty($converted_minute)
      ? "$converted_minute $converted_hour * * *"
      : "* * * * *";
    $yaml['spec']['suspend'] = !empty($is_in_period) ? TRUE : FALSE;
    $pod_yaml = $yaml['spec']['jobTemplate']['spec']['template'];
    $pod_yaml['spec']['containers'][0]['name'] = "$name-$resource_name-$type";
    $env = [
      [
        'name' => 'RESOURCE_NAME',
        'value' => $name,
      ],
      [
        'name' => 'NAMESPACE',
        'value' => $namespace,
      ],
      [
        'name' => 'YAML_URL',
        'value' => !empty($yaml_url) ? $yaml_url : '',
      ],
      [
        'name' => 'RESOURCE_TYPE',
        'value' => $resource_name,
      ],
      [
        'name' => 'NODE_CPU_LIMIT',
        'value' => $node_cpu_limit,
      ],
      [
        'name' => 'NODE_MEMORY_LIMIT',
        'value' => $node_memory_limit,
      ],
      [
        'name' => 'MY_CRONJOB_NAME',
        'value' => $yaml['metadata']['name'],
      ],
    ];
    $pod_yaml['spec']['containers'][0]['env'] = $env;
    if ($type === 'stop') {
      $pod_yaml['spec']['containers'][1]['name'] = "$name-$resource_name-$type-suspend";
      $pod_yaml['spec']['containers'][1]['env'] = $env;
    }
    $yaml['spec']['jobTemplate']['spec']['template'] = $pod_yaml;

    return $yaml;
  }

  /**
   * Create a yaml for Schedule Secret with Git access.
   *
   * @param string $namespace
   *   The namespace.
   * @param string $username
   *   The username.
   * @param string $access_token
   *   The access token.
   *
   * @return array
   *   return yaml.
   */
  public function createScheduleSecretYaml(string $namespace, string $username, string $access_token): array {
    $templates_path = realpath(
      $this->extensionPathResolver->getPath('module', 'k8s')
    ) . '/templates';
    $filename = $templates_path . "/schedule-gitaccess-secret.yml";
    $file = file_get_contents($filename);
    if (empty($file)) {
      return [];
    }

    $yaml = Yaml::decode($file);
    $yaml['metadata']['name'] = "schedule-gitaccess-$username";
    $yaml['metadata']['namespace'] = $namespace;
    $yaml['data']['username'] = base64_encode($username);
    $yaml['data']['password'] = base64_encode($access_token);

    return $yaml;
  }

  /**
   * Create a yaml for Schedule ServiceAccount.
   *
   * @param string $namespace
   *   The namespace.
   *
   * @return array
   *   return yaml.
   */
  public function createScheduleServiceAccountYaml(string $namespace): array {
    $templates_path = realpath(
      $this->extensionPathResolver->getPath('module', 'k8s')
    ) . '/templates';
    $filename = $templates_path . "/schedule-service-account.yml";
    $file = file_get_contents($filename);
    if (empty($file)) {
      return [];
    }

    $yaml = Yaml::decode($file);
    $yaml['metadata']['namespace'] = $namespace;

    return $yaml;
  }

  /**
   * Create a yaml for Schedule ClusterRoleBinding.
   *
   * @param string $namespace
   *   The namespace.
   *
   * @return array
   *   return yaml.
   */
  public function createScheduleClusterRoleBindingYaml(string $namespace): array {
    $templates_path = realpath(
      $this->extensionPathResolver->getPath('module', 'k8s')
    ) . '/templates';
    $filename = $templates_path . "/schedule-cluster-role-binding.yml";
    $file = file_get_contents($filename);
    if (empty($file)) {
      return [];
    }

    $yaml = Yaml::decode($file);
    $yaml['metadata']['name'] = "schedule-pods-deployments-to-$namespace-service-account";
    $yaml['subjects'][0]['namespace'] = $namespace;

    return $yaml;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteK8sCloudStore(int $metrics_duration = 0, array $types = [], ?string $name = NULL): void {
    // Delete cloud store entities.
    $storage = $this->entityTypeManager->getStorage('cloud_store');
    $query = $storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $this->cloudContext);

    if (!empty($metrics_duration)) {
      $query->condition('created', time() - $metrics_duration * self::SECONDS_OF_ONE_DAY, '<');
    }
    if (!empty($types)) {
      $query->condition('type', $types, 'IN');
    }
    if (!empty($name)) {
      $query->condition('name', $name);
    }
    $ids = $query->execute();

    if (!empty($ids)) {
      $entities = $storage->loadMultiple($ids);
      $storage->delete($entities);
    }

    if (!empty($metrics_duration) || empty($types) || empty($name)) {
      return;
    }

    // Delete cached latest created time for cloud store.
    foreach ($types ?: [] as $type) {
      $this->cloudCacheService->clearLatestCreatedTimeCache($this->cloudContext, $type, $name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkCloudStoreLatestCreatedTime(string $cloud_store_bundle, string $name): bool {
    // Get the interval of create cloud store.
    $interval = $this->configFactory->get('k8s.settings')
      ->get('k8s_update_resources_queue_cron_time');

    $latest_created = $this->cloudCacheService
      ->getLatestCreatedTimeCache($this->cloudContext, $cloud_store_bundle, $name);

    if (empty($latest_created)) {
      // Confirm the latest created date of cloud store.
      $max_alias = 'max_created';
      $result = $this->entityTypeManager->getStorage('cloud_store')->getAggregateQuery()
        ->accessCheck(TRUE)
        ->condition('type', $cloud_store_bundle)
        ->condition('cloud_context', $this->cloudContext)
        ->condition('name', $name)
        ->aggregate('created', 'MAX', NULL, $max_alias)
        ->execute();

      if (empty($result)) {
        return TRUE;
      }
      $latest_created = $result[0][$max_alias];
      $this->cloudCacheService
        ->setLatestCreatedTimeCache($this->cloudContext, $cloud_store_bundle, $name, $latest_created);
    }

    $time_diff = time() - $latest_created;
    return $time_diff > $interval;
  }

  /**
   * {@inheritdoc}
   */
  public function setNodeResourceDataCache(K8sNode $node):void {
    $resource_data = [
      'cpu_capacity' => $node->getCpuCapacity(),
      'memory_capacity' => $node->getMemoryCapacity(),
      'pod_capacity' => $node->getPodsCapacity(),
      'costs' => $this->getNodeCost($node),
    ];

    $this->cloudCacheService->setResourceDataCache(
      $node->getCloudContext(),
      $node->bundle(),
      $node->getName(),
      NULL,
      $resource_data);
  }

  /**
   * {@inheritdoc}
   */
  public function setPodResourceDataCache(K8sPod $pod):void {
    $resource_data = [
      'cpu_usage' => $pod->getCpuUsage(),
      'memory_usage' => $pod->getMemoryUsage(),
    ];

    $this->cloudCacheService->setResourceDataCache(
      $pod->getCloudContext(),
      $pod->bundle(),
      $pod->getNamespace(),
      $pod->getName(),
      $resource_data);
  }

  /**
   * Create a CronJob based on $yaml.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param array $yaml
   *   The yaml array.
   *
   * @return bool
   *   Whether it was created or not.
   */
  public function createScheduleCronJob(
    string $cloud_context,
    string $namespace,
    array $yaml,
  ): bool {
    // Check if a CronJob has already been created.
    $result = $this->getCronJobs([
      'metadata.name' => $yaml['metadata']['name'],
      'metadata.namespace' => $namespace,
    ]);

    if (!empty($result)) {
      $this->deleteCronJob($namespace, [
        'metadata.name' => $yaml['metadata']['name'],
        'metadata.namespace' => $namespace,
      ]);
    }

    // Create a CronJob.
    $result = $this->createCronJob($namespace, $yaml);
    if (empty($result) || empty($result['metadata']['name'])) {
      return FALSE;
    }
    $this->updateCronJobs([
      'metadata.name' => $result['metadata']['name'],
      'metadata.namespace' => $namespace,
    ], FALSE);

    return TRUE;
  }

  /**
   * Create a Schedule Create CronJob.
   *
   * @param string $name
   *   The resource name.
   * @param string $namespace
   *   The namespace.
   * @param string $resource_name
   *   The resource name.
   * @param string $yaml_url
   *   The YAML URL of the pod to start.
   *
   * @return bool
   *   Whether CronJob could be created or not.
   */
  private function updateScheduleCreateCronJob(string $name, string $namespace, string $resource_name, string $yaml_url): bool {
    $create_cronjob_yaml = !empty($name) && !empty($namespace) && !empty($resource_name) && !empty($yaml_url)
      ? $this->createScheduleCronJobYaml(
        $name,
        $namespace,
        '',
        '',
        'create',
        $resource_name,
        $yaml_url
        )
      : [];

    $get_cronjob = !empty($create_cronjob_yaml['metadata']['name']) && !empty($namespace)
      ? $this->getCronJobs([
        'metadata.name' => $create_cronjob_yaml['metadata']['name'],
        'metadata.namespace' => $namespace,
      ])
      : [];

    $result = !empty($create_cronjob_yaml) && !empty($get_cronjob)
      ? $this->updateCronJob($namespace, $create_cronjob_yaml)
      : [];

    return !empty($result);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultCloudStoreUid(string $cloud_context): int {
    try {
      $entities = $this->entityTypeManager
        ->getStorage('cloud_config')
        ->loadByProperties([
          'cloud_context' => $cloud_context,
        ]);
      if (empty($entities)) {
        return User::getAnonymousUser()->id();
      }
      $entity = reset($entities);

      if ($entity->hasField('field_cloud_store_user') === FALSE || $entity->get('field_cloud_store_user')->isEmpty()) {
        return User::getAnonymousUser()->id();
      }

      return $entity->get('field_cloud_store_user')->target_id;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return User::getAnonymousUser()->id();
    }
  }

  /**
   * Update a K8s resource.
   *
   * @param string $class
   *   The class which is equivalent to the resource name.
   * @param array $params
   *   The parameters to pass the method.
   * @param string $namespace
   *   The namespace.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateResource(string $class, array $params = [], string $namespace = ''): ?array {

    // Remove empty properties.
    if (!empty($params['spec'])) {
      $params['spec'] = array_filter($params['spec']);
    }

    // Remove status, which should not be modified.
    if (!empty($params['status'])) {
      unset($params['status']);
    }

    // Set the kind.
    $kind = substr($class, strrpos($class, '\\') + 1);
    if (empty($params['kind'])) {
      $params['kind'] = $kind;
    }

    try {

      $resource = lcfirst($kind);
      $method = "{$resource}s";

      if (preg_match('/(.*)[sS]$/', $resource)) {
        $method = "{$resource}es";
      }

      if (preg_match('/(.*)[yY]$/', $resource)) {
        $method = substr($resource, 0, -1) . 'ies';
      }

      // NOTE: Resource Quota is defined as Maclof\Kubernetes\Models\QuotaModel.
      if (str_ends_with($resource, 'quotaModel')) {
        $params['kind'] = 'ResourceQuota';
        $method = 'quotas';
      }

      // NOTE: PersistentVolume is not plural.
      if (str_ends_with($resource, 'persistentVolume')) {
        $method = 'persistentVolume';
      }

      return $this->getClient($namespace)
        ->$method()
        ->update(new $class($params));
    }
    catch (K8sServiceException $e) {
      $this->handleException($e);

      return NULL;
    }
  }

}
