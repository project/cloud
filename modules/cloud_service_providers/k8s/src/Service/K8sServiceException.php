<?php

namespace Drupal\k8s\Service;

/**
 * K8s service exception.
 */
class K8sServiceException extends \Exception {

}
