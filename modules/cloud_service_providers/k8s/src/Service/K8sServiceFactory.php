<?php

namespace Drupal\k8s\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudCacheServiceInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\k8s\Plugin\k8s\K8sRepositoryProviderManager;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * K8sService service interacts with the K8s API.
 */
class K8sServiceFactory {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Cloud context string.
   *
   * @var string
   */
  protected $cloudContext;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method (or use
   * ConfigFormBaseTrait), which may be overridden to address specific needs
   * when loading config, rather than this property directly.
   * See \Drupal\Core\Form\ConfigFormBase::config() for an example of this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Field type plugin manager.
   *
   * @var \Drupal\core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Entity field manager interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The lock interface.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The K8s service.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  protected static $k8sService;

  /**
   * Class Resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The Cloud cache service.
   *
   * @var \Drupal\cloud\Service\CloudCacheServiceInterface
   */
  protected $cloudCacheService;

  /**
   * Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * The Cloud cache service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The k8s repository provider manager.
   *
   * @var \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderManager
   */
  protected $k8sRepositoryProviderManager;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * Constructs a new K8sService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\core\Field\FieldTypePluginManagerInterface $field_type_plugin_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock interface.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request Object.
   * @param \Drupal\cloud\Service\CloudCacheServiceInterface $cloud_cache_service
   *   The Cloud cache service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud cache service.
   * @param \Drupal\k8s\Plugin\k8s\K8sRepositoryProviderManager $k8s_repository_provider_manager
   *   The K8s repository provider manager.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    FieldTypePluginManagerInterface $field_type_plugin_manager,
    EntityFieldManagerInterface $entity_field_manager,
    LockBackendInterface $lock,
    QueueFactory $queue_factory,
    ModuleHandlerInterface $module_handler,
    ClassResolverInterface $class_resolver,
    RequestStack $request_stack,
    CloudCacheServiceInterface $cloud_cache_service,
    CloudServiceInterface $cloud_service,
    K8sRepositoryProviderManager $k8s_repository_provider_manager,
    ClientInterface $http_client,
    ExtensionPathResolver $extension_path_resolver,
  ) {
    // Set up the entity type manager for querying entities.
    $this->entityTypeManager = $entity_type_manager;

    // Set up the configuration factory.
    $this->configFactory = $config_factory;

    $this->currentUser = $current_user;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->lock = $lock;
    $this->queueFactory = $queue_factory;
    $this->moduleHandler = $module_handler;
    $this->classResolver = $class_resolver;
    $this->request = $request_stack->getCurrentRequest();
    $this->cloudCacheService = $cloud_cache_service;
    $this->cloudService = $cloud_service;
    $this->k8sRepositoryProviderManager = $k8s_repository_provider_manager;
    $this->httpClient = $http_client;
    $this->extensionPathResolver = $extension_path_resolver;
  }

  /**
   * Create service.
   *
   * @return \Drupal\k8s\Service\K8sServiceInterface
   *   K8s service.
   */
  public function create(): K8sServiceInterface {
    $test_mode = (bool) $this->configFactory->get('k8s.settings')->get('k8s_test_mode');
    $class_name = K8sService::class;
    if ($test_mode) {
      $class_name = K8sServiceMock::class;
    }
    else {
      $cloud_context = $this->request->get('cloud_context');
      if (!empty($cloud_context)) {
        $this->cloudConfigPluginManager->setCloudContext(is_array($cloud_context)
          ? $cloud_context[0]['value']
          : $cloud_context
        );
        $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
        if (!empty($cloud_config) && $cloud_config->isRemote()) {
          $class_name = K8sServiceRemote::class;
        }
      }
    }

    return new $class_name(
      $this->entityTypeManager,
      $this->configFactory,
      $this->currentUser,
      $this->cloudConfigPluginManager,
      $this->fieldTypePluginManager,
      $this->entityFieldManager,
      $this->lock,
      $this->queueFactory,
      $this->moduleHandler,
      $this->classResolver,
      $this->cloudCacheService,
      $this->cloudService,
      $this->k8sRepositoryProviderManager,
      $this->httpClient,
      $this->extensionPathResolver
    );
  }

}
