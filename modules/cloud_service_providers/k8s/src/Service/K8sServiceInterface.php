<?php

namespace Drupal\k8s\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\GeneratedLink;
use Drupal\k8s\Entity\K8sEntityBase;
use Drupal\k8s\Entity\K8sNode;
use Drupal\k8s\Entity\K8sPod;

/**
 * Provides K8sService interface.
 */
interface K8sServiceInterface {

  /**
   * Automatic selection.
   */
  public const AUTOMATIC_SELECTION = 'Automatic selection';

  /**
   * Default CPU usage threshold of a node.
   */
  public const DEFAULT_NODE_CPU_LIMIT = 80;

  /**
   * Default memory usage threshold of a node.
   */
  public const DEFAULT_NODE_MEMORY_LIMIT = 80;

  /**
   * Default deletion count of pods.
   */
  public const DEFAULT_POD_DELETE_COUNT = 5;

  /**
   * Default timezone.
   */
  public const DEFAULT_TIMEZONE = 'UTC';

  /**
   * Seconds of one day.
   */
  public const SECONDS_OF_ONE_DAY = 86400;

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function setCloudContext($cloud_context);

  /**
   * Get pods.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getPods(array $params = []): ?array;

  /**
   * Update the Pods.
   *
   * Delete old Pod entities, query the api for updated entities and store
   * them as Pod entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updatePods(array $params = [], $clear = TRUE): bool;

  /**
   * Update the Pods without batch.
   *
   * Delete old Pod entities, query the api for updated entities and store
   * them as Pod entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updatePodsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s pod.
   *
   * @param string $namespace
   *   The namespace of the pod.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createPod($namespace, array $params = []): ?array;

  /**
   * Update k8s pod.
   *
   * @param string $namespace
   *   The namespace of the pod.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updatePod($namespace, array $params = []): ?array;

  /**
   * Delete k8s pod.
   *
   * @param string $namespace
   *   The namespace of the pod.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deletePod($namespace, array $params = []): ?array;

  /**
   * Get k8s pod logs.
   *
   * @param string $namespace
   *   The namespace of the pod.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getPodLogs($namespace, array $params = []);

  /**
   * Get k8s nodes.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getNodes(array $params = []): ?array;

  /**
   * Get k8s namespaces.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getNamespaces(array $params = []): ?array;

  /**
   * Create k8s namespace.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createNamespace(array $params = []): ?array;

  /**
   * Update k8s namespace.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateNamespace(array $params = []): ?array;

  /**
   * Delete k8s namespace.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteNamespace(array $params = []): ?array;

  /**
   * Get metrics pods.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getMetricsPods(array $params = []): ?array;

  /**
   * Get metrics nodes.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getMetricsNodes(array $params = []): ?array;

  /**
   * Update the Nodes.
   *
   * Delete old Node entities, query the api for updated entities and store
   * them as Node entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateNodes(array $params = [], $clear = TRUE): bool;

  /**
   * Update the Nodes without batch.
   *
   * Delete old Node entities, query the api for updated entities and store
   * them as Node entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateNodesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the Namespaces.
   *
   * Delete old Namespace entities, query the api for updated entities and store
   * them as Namespace entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateNamespaces(array $params = [], $clear = TRUE): bool;

  /**
   * Update the Namespaces without batch.
   *
   * Delete old Namespace entities, query the api for updated entities and store
   * them as Namespace entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateNamespacesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Get deployments.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getDeployments(array $params = []);

  /**
   * Update the deployments without batch.
   *
   * Delete old deployment entities, query the api for updated entities
   * and store them as deployment entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateDeploymentsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the deployments.
   *
   * Delete old deployment entities, query the api for updated entities
   * and store them as deployment entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateDeployments(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s deployment.
   *
   * @param string $namespace
   *   The namespace of the deployment.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createDeployment($namespace, array $params = []): ?array;

  /**
   * Update k8s deployment.
   *
   * @param string $namespace
   *   The namespace of the deployment.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateDeployment($namespace, array $params = []): ?array;

  /**
   * Delete k8s deployment.
   *
   * @param string $namespace
   *   The namespace of the deployment.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteDeployment($namespace, array $params = []): ?array;

  /**
   * Get K8s ReplicaSets.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getReplicaSets(array $params = []): ?array;

  /**
   * Create K8s ReplicaSet.
   *
   * @param string $namespace
   *   The namespace of the K8s ReplicaSet.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createReplicaSet($namespace, array $params = []): ?array;

  /**
   * Update K8s ReplicaSet.
   *
   * @param string $namespace
   *   The namespace of the K8s ReplicaSet.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateReplicaSet($namespace, array $params = []): ?array;

  /**
   * Delete K8s ReplicaSet.
   *
   * @param string $namespace
   *   The namespace of the K8s ReplicaSet.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteReplicaSet($namespace, array $params = []): ?array;

  /**
   * Update the replica sets.
   *
   * Delete old replica sets entities, query the api for updated entities
   * and store them as replica sets entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateReplicaSets(array $params = [], $clear = TRUE): bool;

  /**
   * Update the replica sets without batch.
   *
   * Delete old replica sets entities, query the api for updated entities
   * and store them as replica sets entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateReplicaSetsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the resource quota.
   *
   * Delete old resource quota entities, query the api for updated entities
   * and store them as resource quota entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale entities.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateResourceQuotas(array $params = [], $clear = TRUE): bool;

  /**
   * Update the resource quota without batch.
   *
   * Delete old resource quota entities, query the api for updated entities
   * and store them as resource quota entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale entities.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateResourceQuotasWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the roles.
   *
   * Delete old roles entities, query the api for updated entities
   * and store them as roles entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale entities.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateRoles(array $params = [], $clear = TRUE): bool;

  /**
   * Update the roles without batch.
   *
   * Delete roles entities, query the api for updated entities
   * and store them as roles entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale entities.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateRolesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the network policies.
   *
   * Delete old network policy entities, query the api for updated entities
   * and store them as network policy entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateNetworkPolicies(array $params = [], $clear = TRUE): bool;

  /**
   * Update the network policies without batch.
   *
   * Delete old network policy entities, query the api for updated entities
   * and store them as network policy entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateNetworkPoliciesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Get network policies.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getNetworkPolicies(array $params = []): ?array;

  /**
   * Create k8s network policy.
   *
   * @param string $namespace
   *   The namespace of the network policy.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createNetworkPolicy($namespace, array $params = []): ?array;

  /**
   * Update k8s network policy.
   *
   * @param string $namespace
   *   The namespace of the deployment.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateNetworkPolicy($namespace, array $params = []): ?array;

  /**
   * Delete k8s network policy.
   *
   * @param string $namespace
   *   The namespace of the deployment.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteNetworkPolicy($namespace, array $params = []): ?array;

  /**
   * Get persistent volumes.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getPersistentVolumes(array $params = []): ?array;

  /**
   * Update persistent volumes.
   *
   * Delete old persistent volume entities, query the api for updated entities
   * and store them as persistent volume entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updatePersistentVolumes(array $params = [], $clear = TRUE): bool;

  /**
   * Update persistent volumes without batch.
   *
   * Delete old persistent volume entities, query the api for updated entities
   * and store them as persistent volume entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updatePersistentVolumesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s persistent volume.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createPersistentVolume(array $params = []): ?array;

  /**
   * Update k8s persistent volume.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updatePersistentVolume(array $params = []): ?array;

  /**
   * Delete k8s persistent volume.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deletePersistentVolume(array $params = []): ?array;

  /**
   * Get cluster roles.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getClusterRoles(array $params = []): ?array;

  /**
   * Update the cluster roles.
   *
   * Delete old cluster role entities, query the api for updated entities
   * and store them as cluster role entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateClusterRoles(array $params = [], $clear = TRUE): bool;

  /**
   * Update the cluster roles without batch.
   *
   * Delete old cluster role entities, query the api for updated entities
   * and store them as cluster role entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateClusterRolesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s cluster role.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createClusterRole(array $params = []): ?array;

  /**
   * Update k8s cluster role.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateClusterRole(array $params = []): ?array;

  /**
   * Delete k8s cluster role.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteClusterRole(array $params = []): ?array;

  /**
   * Get cluster roles binding.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getClusterRoleBindings(array $params = []): ?array;

  /**
   * Update the cluster roles binding.
   *
   * Delete old cluster role binding entities, query the API for updated
   *  entities and store them as cluster role binding entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateClusterRoleBindings(array $params = [], $clear = TRUE): bool;

  /**
   * Update the cluster roles binding without batch.
   *
   * Delete old cluster role binding entities, query the API for updated
   *  entities and store them as cluster role binding entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateClusterRoleBindingsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s cluster role binding.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createClusterRoleBinding(array $params = []): ?array;

  /**
   * Update k8s cluster role binding.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateClusterRoleBinding(array $params = []): ?array;

  /**
   * Delete k8s cluster role binding.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteClusterRoleBinding(array $params = []): ?array;

  /**
   * Get storage classes.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getStorageClasses(array $params = []): ?array;

  /**
   * Update the storage classes.
   *
   * Delete old storage class entities, query the api for updated entities
   * and store them as storage class entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateStorageClasses(array $params = [], $clear = TRUE): bool;

  /**
   * Update the storage classes without batch.
   *
   * Delete old storage class entities, query the api for updated entities
   * and store them as storage class entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateStorageClassesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s storage class.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createStorageClass(array $params = []): ?array;

  /**
   * Update k8s storage class.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateStorageClass(array $params = []): ?array;

  /**
   * Delete k8s storage class.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteStorageClass(array $params = []): ?array;

  /**
   * Get K8s StatefulSets.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getStatefulSets(array $params = []): ?array;

  /**
   * Update K8s StatefulSets.
   *
   * Delete old K8s StatefulSet entities, query the api for updated entities
   * and store them as K8s StatefulSet entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateStatefulSets(array $params = [], $clear = TRUE): bool;

  /**
   * Update K8s StatefulSets without batch.
   *
   * Delete old K8s StatefulSet entities, query the api for updated entities
   * and store them as K8s StatefulSet entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateStatefulSetsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create K8s StatefulSet.
   *
   * @param string $namespace
   *   The namespace of the K8s StatefulSet.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createStatefulSet($namespace, array $params = []): ?array;

  /**
   * Update K8s StatefulSet.
   *
   * @param string $namespace
   *   The namespace of the K8s StatefulSet.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateStatefulSet($namespace, array $params = []): ?array;

  /**
   * Delete K8s StatefulSet.
   *
   * @param string $namespace
   *   The namespace of the K8s StatefulSet.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteStatefulSet($namespace, array $params = []): ?array;

  /**
   * Get ingresses.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getIngresses(array $params = []): ?array;

  /**
   * Update the ingresses.
   *
   * Delete old ingress entities, query the api for updated entities
   * and store them as ingress entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateIngresses(array $params = [], $clear = TRUE): bool;

  /**
   * Update the ingresses without batch.
   *
   * Delete old ingress entities, query the api for updated entities
   * and store them as ingress entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateIngressesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the job.
   *
   * Delete old job entities, query the api for updated entities
   * and store them as job entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateJobs(array $params = [], $clear = TRUE): bool;

  /**
   * Update the job without batch.
   *
   * Delete old job entities, query the api for updated entities
   * and store them as job entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateJobsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the limit range.
   *
   * Delete old limit range entities, query the api for updated entities
   * and store them as limit range entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateLimitRanges(array $params = [], $clear = TRUE): bool;

  /**
   * Update the limit range without batch.
   *
   * Delete old limit range entities, query the api for updated entities
   * and store them as limit range entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateLimitRangesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s ingresses.
   *
   * @param string $namespace
   *   The namespace of the ingress.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createIngress($namespace, array $params = []): ?array;

  /**
   * Update k8s ingress.
   *
   * @param string $namespace
   *   The namespace of the ingress.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateIngress($namespace, array $params = []): ?array;

  /**
   * Delete k8s ingresses.
   *
   * @param string $namespace
   *   The namespace of the ingress.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteIngress($namespace, array $params = []): ?array;

  /**
   * Get K8s DaemonSets.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getDaemonSets(array $params = []): ?array;

  /**
   * Update K8s DaemonSets.
   *
   * Delete old K8s DaemonSet entities, query the api for updated entities
   * and store them as K8s DaemonSet entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateDaemonSets(array $params = [], $clear = TRUE): bool;

  /**
   * Update K8s DaemonSets without batch.
   *
   * Delete old K8s DaemonSet entities, query the api for updated entities
   * and store them as K8s DaemonSet entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateDaemonSetsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create K8s DaemonSet.
   *
   * @param string $namespace
   *   The namespace of the K8s DaemonSet.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createDaemonSet($namespace, array $params = []): ?array;

  /**
   * Update K8s DaemonSet.
   *
   * @param string $namespace
   *   The namespace of the K8s DaemonSet.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateDaemonSet($namespace, array $params = []): ?array;

  /**
   * Delete K8s DaemonSet.
   *
   * @param string $namespace
   *   The namespace of the K8s DaemonSet.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteDaemonSet($namespace, array $params = []): ?array;

  /**
   * Get endpoints.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getEndpoints(array $params = []): ?array;

  /**
   * Update the endpoints.
   *
   * Delete old endpoint entities, query the api for updated entities
   * and store them as endpoint entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateEndpoints(array $params = [], $clear = TRUE): bool;

  /**
   * Update the endpoints without batch.
   *
   * Delete old endpoint entities, query the api for updated entities
   * and store them as endpoint entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateEndpointsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s endpoints.
   *
   * The part of the method name should be the same as the Kind: field so that
   * this method name should be createEndpoints() by Kind: Endpoints.
   * See also: K8sCreateForm::updateYamlToEntity().
   *
   * @param string $namespace
   *   The namespace of the endpoint.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createEndpoints($namespace, array $params = []): ?array;

  /**
   * Update k8s endpoint.
   *
   * @param string $namespace
   *   The namespace of the endpoint.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateEndpoint($namespace, array $params = []): ?array;

  /**
   * Delete k8s endpoints.
   *
   * @param string $namespace
   *   The namespace of the endpoint.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteEndpoint($namespace, array $params = []): ?array;

  /**
   * Get k8s events.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getEvents(array $params = []): ?array;

  /**
   * Update the Events.
   *
   * Delete old Event entities, query the api for updated entities and store
   * them as Event entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateEvents(array $params = [], $clear = TRUE): bool;

  /**
   * Update the Events without batch.
   *
   * Delete old Event entities, query the api for updated entities and store
   * them as Event entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateEventsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the horizontal pod autoscalers.
   *
   * Delete old horizontal pod autoscalers entities, query the api for updated
   * entities and store them as horizontal pod autoscalers entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateHorizontalPodAutoscalers(array $params = [], $clear = TRUE): bool;

  /**
   * Update the horizontal pod autoscalers without batch.
   *
   * Delete old horizontal pod autoscalers entities, query the api for updated
   * entities and store them as horizontal pod autoscalers entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateHorizontalPodAutoscalersWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Get persistent volume claims.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getPersistentVolumeClaims(array $params = []): ?array;

  /**
   * Update the persistent volume claims.
   *
   * Delete old persistent volume claim entities,
   * query the api for updated entities and store them as
   * persistent volume claim entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updatePersistentVolumeClaims(array $params = [], $clear = TRUE): bool;

  /**
   * Update the persistent volume claims without batch.
   *
   * Delete old persistent volume claim entities,
   * query the api for updated entities and store them as
   * persistent volume claim entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updatePersistentVolumeClaimsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s persistent volume claim.
   *
   * @param string $namespace
   *   The namespace of the persistent volume claim.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createPersistentVolumeClaim($namespace, array $params = []): ?array;

  /**
   * Update k8s persistent volume claim.
   *
   * @param string $namespace
   *   The namespace of the persistent volume claim.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updatePersistentVolumeClaim($namespace, array $params = []): ?array;

  /**
   * Delete k8s persistent volume claims.
   *
   * @param string $namespace
   *   The namespace of the persistent volume claim.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deletePersistentVolumeClaim($namespace, array $params = []): ?array;

  /**
   * Get roles binding.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getRoleBindings(array $params = []): ?array;

  /**
   * Update the roles binding.
   *
   * Delete old role binding entities, query the api for updated entities
   * and store them as role binding entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateRoleBindings(array $params = [], $clear = TRUE): bool;

  /**
   * Update the roles binding without batch.
   *
   * Delete old role binding entities, query the api for updated entities
   * and store them as role binding entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateRoleBindingsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the secrets.
   *
   * Delete old secrets entities, query the api for updated entities
   * and store them as secrets entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSecrets(array $params = [], $clear = TRUE): bool;

  /**
   * Update the secrets without batch.
   *
   * Delete old secrets entities, query the api for updated entities
   * and store them as secrets entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSecretsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the services.
   *
   * Delete old services entities, query the api for updated entities
   * and store them as services entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateServices(array $params = [], $clear = TRUE): bool;

  /**
   * Update the services without batch.
   *
   * Delete old services entities, query the api for updated entities
   * and store them as services entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateServicesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s role binding.
   *
   * @param string $namespace
   *   The namespace of the api service.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createRoleBinding($namespace, array $params = []): ?array;

  /**
   * Update k8s role binding.
   *
   * @param string $namespace
   *   The namespace of the role binding.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateRoleBinding($namespace, array $params = []): ?array;

  /**
   * Delete k8s role binding.
   *
   * @param string $namespace
   *   The namespace of the role binding.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteRoleBinding($namespace, array $params = []): ?array;

  /**
   * Get API services.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getApiServices(array $params = []): ?array;

  /**
   * Update the API services.
   *
   * Delete old API service entities, query the API for updated entities
   * and store them as API service entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateApiServices(array $params = [], $clear = TRUE): bool;

  /**
   * Update the API services without batch.
   *
   * Delete old API service entities, query the API for updated entities
   * and store them as API service entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateApiServicesWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the ConfigMaps.
   *
   * Delete old Config map entities, query the API for updated entities
   * and store them as API service entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateConfigMaps(array $params = [], $clear = TRUE): bool;

  /**
   * Update the ConfigMaps without batch.
   *
   * Delete old Config map entities, query the API for updated entities
   * and store them as API service entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateConfigMapsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Update the Cronjobs.
   *
   * Delete old Cronjob entities, query the API for updated entities
   * and store them as API service entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateCronJobs(array $params = [], $clear = TRUE): bool;

  /**
   * Update the Cronjobs without batch.
   *
   * Delete old Cronjob entities, query the API for updated entities
   * and store them as API service entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateCronJobsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create k8s API service.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createApiService(array $params = []): ?array;

  /**
   * Update k8s API service.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateApiService(array $params = []): ?array;

  /**
   * Delete k8s API service.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteApiService(array $params = []): ?array;

  /**
   * Get K8s ServiceAccounts.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getServiceAccounts(array $params = []): ?array;

  /**
   * Update K8s ServiceAccounts.
   *
   * Delete old K8s ServiceAccount entities, query the api for updated entities
   * and store them as K8s ServiceAccount entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateServiceAccounts(array $params = [], $clear = TRUE): bool;

  /**
   * Update K8s ServiceAccounts without batch.
   *
   * Delete old K8s ServiceAccount entities, query the api for updated entities
   * and store them as K8s ServiceAccount entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateServiceAccountsWithoutBatch(array $params = [], $clear = TRUE): bool;

  /**
   * Create K8s ServiceAccount.
   *
   * @param string $namespace
   *   The namespace of the K8s ServiceAccount.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createServiceAccount($namespace, array $params = []): ?array;

  /**
   * Update K8s ServiceAccount.
   *
   * @param string $namespace
   *   The namespace of the K8s ServiceAccount.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateServiceAccount($namespace, array $params = []): ?array;

  /**
   * Delete K8s ServiceAccount.
   *
   * @param string $namespace
   *   The namespace of the K8s ServiceAccount.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteServiceAccount($namespace, array $params = []): ?array;

  /**
   * Get priority classes.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getPriorityClasses(array $params = []): ?array;

  /**
   * Create k8s priority classes.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createPriorityClass(array $params = []): ?array;

  /**
   * Update k8s priority class.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updatePriorityClass(array $params = []): ?array;

  /**
   * Delete k8s priority classes.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deletePriorityClass(array $params = []): ?array;

  /**
   * Update k8s priority classes.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updatePriorityClasses(array $params = [], $clear = TRUE): bool;

  /**
   * Update k8s priority classes without batch.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updatePriorityClassesWithoutBatch(array $params = []): bool;

  /**
   * Get k8s horizontal pod autoscalers.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function getHorizontalPodAutoscalers(array $params = []): ?array;

  /**
   * Create k8s horizontal pod autoscaler.
   *
   * @param string $namespace
   *   The namespace of the horizontal pod autoscaler.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function createHorizontalPodAutoscaler($namespace, array $params = []): ?array;

  /**
   * Update k8s horizontal pod autoscaler.
   *
   * @param string $namespace
   *   The namespace of the horizontal pod autoscaler.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array|null
   *   An array of results or NULL if there is an error.
   */
  public function updateHorizontalPodAutoscaler($namespace, array $params = []): ?array;

  /**
   * Delete k8s horizontal pod autoscaler.
   *
   * @param string $namespace
   *   The namespace of the horizontal pod autoscaler.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function deleteHorizontalPodAutoscaler($namespace, array $params = []): ?array;

  /**
   * Delete k8s resources and entities.
   *
   * @param array $entities
   *   The array entities to be deleted.
   * @param bool $immediately
   *   Whether delete immediately.
   */
  public function deleteResourcesWithEntities(array $entities, $immediately = FALSE): void;

  /**
   * Delete k8s schedule entities.
   *
   * @param array $entities
   *   The array entities to be deleted.
   */
  public function deleteSchedules(array $entities): void;

  /**
   * Get link for the metrics server.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Drupal\Core\GeneratedLink
   *   The link for the metrics server.
   */
  public function getMetricsServerLink($cloud_context = NULL): GeneratedLink;

  /**
   * Change the parameters for resource quota.
   *
   * @param string $label
   *   Name of namespace in kubernetes cluster.
   * @param array $config_entities
   *   The list of cloud context.
   * @param array $param
   *   Parameter set on resource quota.
   */
  public function changeResourceQuota($label, array $config_entities, array $param): void;

  /**
   * Determine whether the time is within specific time range or not.
   *
   * @param string $startup_time
   *   The start of specific time.
   * @param string $stop_time
   *   The end of specific time.
   * @param int $time
   *   The time to be validated with unix epoch time.
   *
   * @return bool
   *   TRUE means that input time is in specific time range
   *   defined by startup and stop time.
   */
  public function validateScheduledTime($startup_time, $stop_time, $time = NULL): bool;

  /**
   * Create/update resource with entity.
   *
   * @param string $type
   *   Entity type.
   * @param string $cloud_context
   *   Cloud context.
   * @param string $label
   *   The label.
   * @param array $params
   *   Parameter for type.
   */
  public function updateResourceWithEntity($type, $cloud_context, $label, array $params);

  /**
   * Export node metrics to log.
   *
   * @param array $metrics_nodes
   *   The metrics of nodes.
   * @param array $resources
   *   The resources.
   * @param array $extra_data
   *   The extra data.
   */
  public function exportNodeMetrics(array $metrics_nodes, array $resources, array $extra_data = []): void;

  /**
   * Export pod metrics to log.
   *
   * @param array $metrics_pods
   *   The metrics of nodes.
   */
  public function exportPodMetrics(array $metrics_pods);

  /**
   * Create queue items for update resources queue.
   */
  public function createResourceQueueItems(): void;

  /**
   * Format costs.
   *
   * @param int $costs
   *   The costs.
   * @param int $total_costs
   *   The total costs.
   * @param int $precision
   *   The precision.
   */
  public function formatCosts($costs, $total_costs, $precision = 2);

  /**
   * Format cpu usage.
   *
   * @param float $cpu_usage
   *   The cpu usage.
   * @param float $cpu_capacity
   *   The cpu capacity.
   */
  public function formatCpuUsage($cpu_usage, $cpu_capacity): string;

  /**
   * Format memory usage.
   *
   * @param int $memory_usage
   *   The memory usage.
   * @param int $memory_capacity
   *   The memory capacity.
   */
  public function formatMemoryUsage($memory_usage, $memory_capacity): string;

  /**
   * Format pod usage.
   *
   * @param int $pod_usage
   *   The pod usage.
   * @param int $pod_capacity
   *   The pod capacity.
   */
  public function formatPodUsage($pod_usage, $pod_capacity);

  /**
   * Get total costs of nodes.
   *
   * @param array $nodes
   *   The k8s_node entities.
   * @param string $cost_type
   *   The cost type.
   * @param bool $refresh
   *   TRUE to refresh pricing data.
   */
  public function getTotalCosts(array $nodes, $cost_type, $refresh = TRUE): int;

  /**
   * Get the costs of nodes.
   *
   * @param \Drupal\k8s\Entity\K8sNode $node
   *   The k8s node entity.
   * @param bool $refresh
   *   TRUE to refresh pricing data.
   *
   * @return array
   *   The cost of nodes.
   */
  public function getNodeCost(K8sNode $node, $refresh = TRUE): array;

  /**
   * Get amount of resource usage per a namespace.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace of kubernetes.
   */
  public function getNamespaceResourceUsage($cloud_context, $namespace);

  /**
   * Get amount of resource usage per a namespace.
   *
   * @param string $cloud_context
   *   The cloud context.
   */
  public function getNodeResourceUsage($cloud_context): array;

  /**
   * Get amount of resource usage per a namespace.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   */
  public function calculateCostPerNamespace($cloud_context, $namespace): array;

  /**
   * Automatically select k8s cluster based on usage resource.
   */
  public function selectK8sClusterByResourceUsage();

  /**
   * Defines the supported Cloud launch template types.
   *
   * @return array
   *   An array of supported templates.
   */
  public function supportedCloudLaunchTemplates(): array;

  /**
   * Decode yaml file with multiple documents.
   *
   * @param string $yaml
   *   The yaml content.
   *
   * @return array
   *   The yaml array decoded.
   */
  public function decodeMultipleDocYaml(string $yaml): array;

  /**
   * Set allowed values for the K8s cluster.
   *
   * @return array
   *   An array of possible key and value options.
   */
  public function clusterAllowedValues(): array;

  /**
   * Checks if git command is available.
   *
   * @return bool
   *   TRUE is available.
   */
  public function isGitCommandAvailable(): bool;

  /**
   * Update scheduled resources.
   */
  public function updateScheduledResources(): void;

  /**
   * Update k8s resource store entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param array $extra_data
   *   The extra data.
   */
  public function updateK8sNamespaceResourceStoreEntity($cloud_context, $namespace, array $extra_data = []): void;

  /**
   * Delete resource with 'gracePeriodSeconds' option.
   *
   * @param \Drupal\k8s\Entity\K8sEntityBase $entity
   *   The entity with the resource to be deleted.
   *
   * @return array
   *   An array of results.
   */
  public function deleteImmediately(K8sEntityBase $entity): array;

  /**
   * Check existence of a resource.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return bool
   *   TRUE if the resource exists, else FALSE.
   */
  public function hasResource($entity_type_id, array $params): bool;

  /**
   * Create entity from yaml.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $yaml
   *   The yaml.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   return entity object.
   */
  public function createEntityFromYaml(string $entity_type_id, array $yaml): ?EntityInterface;

  /**
   * Create yaml for Schedule CronJob to be created from Git.
   *
   * @param string $name
   *   The resource name.
   * @param string $namespace
   *   The namespace.
   * @param string $resource_name
   *   The resource name.
   * @param string $git_repository
   *   The git repository.
   * @param string $git_branch
   *   The git branch.
   * @param string $resource_path
   *   The resource path.
   * @param string $git_username
   *   The git username.
   * @param bool $is_in_period
   *   Whether the schedule period is in progress or not.
   *
   * @return array
   *   return yaml.
   */
  public function createGitScheduleCronJobYaml(
    string $name,
    string $namespace,
    string $resource_name,
    string $git_repository,
    string $git_branch,
    string $resource_path,
    string $git_username,
    bool $is_in_period = FALSE,
  ): array;

  /**
   * Create a yaml for Schedule CronJob.
   *
   * @param string $name
   *   The resource name.
   * @param string $namespace
   *   The namespace.
   * @param string $hour
   *   The hour of the schedule.
   * @param string $minute
   *   The minute of the schedule.
   * @param string $type
   *   Specify whether to start or stop.
   * @param string $resource_name
   *   The resource name.
   * @param string $yaml_url
   *   The YAML URL of the pod to start.
   *
   * @return array
   *   return yaml.
   */
  public function createScheduleCronJobYaml(
    string $name,
    string $namespace,
    string $hour,
    string $minute,
    string $type,
    string $resource_name,
    string $yaml_url = '',
  ): array;

  /**
   * Create a yaml for Schedule Secret with Git access.
   *
   * @param string $namespace
   *   The namespace.
   * @param string $username
   *   The username.
   * @param string $access_token
   *   The access token.
   *
   * @return array
   *   return yaml.
   */
  public function createScheduleSecretYaml(string $namespace, string $username, string $access_token): array;

  /**
   * Create a yaml for Schedule ServiceAccount.
   *
   * @param string $namespace
   *   The namespace.
   *
   * @return array
   *   return yaml.
   */
  public function createScheduleServiceAccountYaml(string $namespace): array;

  /**
   * Create a yaml for Schedule ClusterRoleBinding.
   *
   * @param string $namespace
   *   The namespace.
   *
   * @return array
   *   return yaml.
   */
  public function createScheduleClusterRoleBindingYaml(string $namespace): array;

  /**
   * Get entity.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The name.
   * @param string $namespace
   *   The namespace.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   return entity object.
   */
  public static function getEntity(string $entity_type_id, string $cloud_context, string $name, string $namespace = ''): ?EntityInterface;

  /**
   * Delete cloud store entries and cache.
   *
   * @param int $metrics_duration
   *   The metrics duration.
   * @param array $types
   *   The list of resource type.
   * @param string $name
   *   The name of resource.
   */
  public function deleteK8sCloudStore(int $metrics_duration = 0, array $types = [], ?string $name = NULL): void;

  /**
   * Create a CronJob based on $yaml.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $namespace
   *   The namespace.
   * @param array $yaml
   *   The yaml array.
   *
   * @return bool
   *   Whether it was created or not.
   */
  public function createScheduleCronJob(string $cloud_context, string $namespace, array $yaml): bool;

  /**
   * Check if the interval time has elapsed since the last data was created.
   *
   * @param string $cloud_store_bundle
   *   The cloud store bundle.
   * @param string $name
   *   The name.
   *
   * @return bool
   *   Whether the interval has passed.
   */
  public function checkCloudStoreLatestCreatedTime(string $cloud_store_bundle, string $name): bool;

  /**
   * Set node resource data in cache.
   *
   * @param \Drupal\k8s\Entity\K8sNode $node
   *   The k8s node entity.
   */
  public function setNodeResourceDataCache(K8sNode $node): void;

  /**
   * Set pod resource data in cache.
   *
   * @param \Drupal\k8s\Entity\K8sPod $pod
   *   The k8s pod entity.
   */
  public function setPodResourceDataCache(K8sPod $pod): void;

  /**
   * Helper function to get default cloud store uid.
   *
   * @param string $cloud_context
   *   The cloud context to retrieve.
   *
   * @return int
   *   Uid if set, or 0 for anonymous.
   */
  public function getDefaultCloudStoreUid(string $cloud_context): int;

}
