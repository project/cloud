<?php

namespace Drupal\k8s\Service;

/**
 * K8sServiceRemote service interacts with the K8s API.
 */
class K8sServiceRemote extends K8sService {

  /**
   * {@inheritdoc}
   */
  public function getPods(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function updatePods(array $params = [], $clear = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updatePodsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createPod($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updatePod($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deletePod($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getPodLogs($namespace, array $params = [], $container = NULL) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getNodes(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getNamespaces(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createNamespace(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateNamespace(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNamespace(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetricsPods(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetricsNodes(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function updateNodes(array $params = [], $clear = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateNodesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateNamespaces(array $params = [], $clear = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateNamespacesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeployments(array $params = []) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function updateDeploymentsWithoutBatch(array $params = [], $clear = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateDeployments(array $params = [], $clear = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createDeployment($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateDeployment($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDeployment($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getReplicaSets(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createReplicaSet($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateReplicaSet($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteReplicaSet($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getServices(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createService($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateService($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteService($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getCronJobs(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createCronJob($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateCronJob($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCronJob($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getJobs(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createJob($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateJob($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteJob($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceQuotas(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createResourceQuota($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateResourceQuota($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteResourceQuota($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getLimitRanges(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createLimitRange($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateLimitRange($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteLimitRange($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getSecrets(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createSecret($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateSecret($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSecret($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigMaps(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigMap($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateConfigMap($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteConfigMap($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateNetworkPolicies(array $params = [], $clear = TRUE): bool {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkPolicies(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createNetworkPolicy($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateNetworkPolicy($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNetworkPolicy($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getRoles(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createRole($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateRole($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRole($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getRoleBindings(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createRoleBinding($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateRoleBinding($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRoleBinding($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getServiceAccounts(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createServiceAccount($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateServiceAccount($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteServiceAccount($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getClusterRoles(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createClusterRole(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateClusterRole(array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteClusterRole(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getClusterRoleBindings(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createClusterRoleBinding(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateClusterRoleBinding(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteClusterRoleBinding(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistentVolumes(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolumes(array $params = [], $clear = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolumesWithoutBatch(array $params = [], $clear = TRUE): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createPersistentVolume(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolume(array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deletePersistentVolume(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageClasses(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createStorageClass(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateStorageClass(array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $params['kind'] = 'StorageClass';

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteStorageClass(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getStatefulSets(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createStatefulSet($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateStatefulSet($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteStatefulSet($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getIngresses(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createIngress($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateIngress($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteIngress($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getDaemonSets(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createDaemonSet($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateDaemonSet($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDaemonSet($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createEndpoints($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateEndpoint($namespace, array $params = []): array {
    // Remove empty properties.
    $params['subsets'] = array_filter($params['subsets']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteEndpoint($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getEvents(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function updateEvents(array $params = [], $clear = TRUE): bool {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistentVolumeClaims(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createPersistentVolumeClaim($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updatePersistentVolumeClaim($namespace, array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deletePersistentVolumeClaim($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getApiServices(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createApiService(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateApiService(array $params = []): array {
    // Remove empty properties.
    $params['spec'] = array_filter($params['spec']);

    // Remove status, which should not be modified.
    unset($params['status']);

    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteApiService(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getPriorityClasses(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createPriorityClass(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updatePriorityClass(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deletePriorityClass(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getHorizontalPodAutoscalers(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createHorizontalPodAutoscaler($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateHorizontalPodAutoscaler($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteHorizontalPodAutoscaler($namespace, array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * Save operation to queue.
   *
   * @param string $operation
   *   The operation to perform.
   * @param array $params
   *   An array of parameters.
   */
  private function saveOperation(string $operation, array $params): void {
    if (empty($this->cloudContext)) {
      return;
    }

    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    if (!$cloud_config->isRemote()) {
      return;
    }

    $entity_type_name = 'k8s_' . $this->getSnakeCase(preg_replace('/^[[:lower:]]+/', '', $operation));
    $entity_type_name_plural_camel = $this->getShortEntityTypeNamePluralCamelByTypeName($entity_type_name);
    if (empty($entity_type_name_plural_camel)) {
      $update_entities_method = NULL;
      $update_entities_method_params = [];
    }
    else {
      $update_entities_method = 'update' . $entity_type_name_plural_camel;
      $resource_name = is_array($params[0]) ? $params[0]['metadata']['name'] : $params[1]['metadata']['name'];
      $update_entities_method_params = [
        ['metadata.name' => $resource_name],
        FALSE,
      ];
    }

    $queue = $this->queueFactory->get('operation_queue_' . $cloud_config->get('cloud_cluster_name')->value . $cloud_config->get('cloud_cluster_worker_name')->value);
    $queue->createItem([
      'cloud_context' => $this->cloudContext,
      'operation' => $operation,
      'params' => $params,
      'service_name' => 'k8s',
      'update_entities_method' => $update_entities_method,
      'update_entities_method_params' => $update_entities_method_params,
    ]);
  }

}
