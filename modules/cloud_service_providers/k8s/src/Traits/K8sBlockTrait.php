<?php

namespace Drupal\k8s\Traits;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\k8s\Entity\K8sNamespaceInterface;
use Drupal\k8s\Entity\K8sNode;

/**
 * Traits for K8s blocks.
 */
trait K8sBlockTrait {

  use CloudContentEntityTrait;

  /**
   * Flag whether to refresh pricing data during block rendering.
   *
   * @var bool
   */
  protected $refresh = FALSE;

  /**
   * Helper method to build the allocated resource array.
   *
   * @param array $nodes
   *   Array of K8 nodes.
   * @param string $cloud_context
   *   Optional cloud context.
   *
   * @return array
   *   Array of nodes.
   */
  protected function buildAllocatedResourceArray(array $nodes, $cloud_context = NULL): array {
    $response = [];
    foreach ($nodes ?: [] as $node) {
      $pod_resources = $this->getPodResources($node->getName(), $cloud_context);

      // Return a JSON object of K8s node names, Pods capacity and Pods
      // allocation to construct a Node heatmap to display the Pods allocation
      // status by D3.js.
      $node_response = $this->generateNodeResponse($node);
      $node_response['pods'] = $pod_resources;
      $response[] = $node_response;
    }
    return $response;
  }

  /**
   * Load pod resources based on node name and/or cloud_context.
   *
   * @param string $node_name
   *   The node name to load.
   * @param string $cloud_context
   *   The cloud_context if passed.
   *
   * @return array
   *   Pod resource array.
   */
  protected function getPodResources($node_name, $cloud_context = NULL): array {
    $pod_resources = [];
    try {
      $params = [
        'node_name' => $node_name,
      ];
      if (!empty($cloud_context)) {
        $params['cloud_context'] = $cloud_context;
      }
      $pods = $this->loadEntities('k8s_pod', $params, FALSE);

      foreach ($pods ?: [] as $pod) {
        $pod_resources[] = [
          'name' => $pod->getName() ?: 'Unknown pod name',
          'cpuUsage' => $pod->getCpuUsage(),
          'memoryUsage' => $pod->getMemoryUsage(),
        ];
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $pod_resources;
  }

  /**
   * Return JSON object of K8s node names, Pods capacity and Pods allocation.
   *
   * Used to construct a Node heat map to display the Pods allocation
   * status by D3.js.
   *
   * @param \Drupal\k8s\Entity\K8sNode $node
   *   The K8sNode to generate for.
   *
   * @return array
   *   Formatted array of information.
   */
  protected function generateNodeResponse(K8sNode $node): array {
    // Return a JSON object of K8s node names, Pods capacity and Pods
    // allocation to construct a Node heatmap to display the Pods allocation
    // status by D3.js.
    return [
      // Node name.
      'name' => $node->getName() ?: 'Unknown node name',

      // CPU usage info.
      'cpuCapacity' => $node->getCpuCapacity(),
      'cpuRequest' => $node->getCpuRequest(),
      'cpuLimit' => $node->getCpuLimit(),

      // Memory usage info.
      'memoryCapacity' => $node->getMemoryCapacity(),
      'memoryRequest' => $node->getMemoryRequest(),
      'memoryLimit' => $node->getMemoryLimit(),

      // Pods usage info.
      'podsCapacity' => $node->getPodsCapacity(),
      'podsAllocation' => $node->getPodsAllocation(),
    ];
  }

  /**
   * Load all allocated node resources.
   *
   * @return array
   *   Array of allocated resources.
   */
  protected function loadAllNodeAllocatedResources(): array {
    $response = [];
    try {
      $nodes = $this->loadEntities('k8s_node');
      if (!empty($nodes)) {
        $response = $this->buildAllocatedResourceArray($nodes);
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $response = [];
    }

    return $response;
  }

  /**
   * Get the metrics information on the node(s)
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   * @param \Drupal\k8s\Entity\K8sNode $k8s_node
   *   The K8s node entity to expect to get the metrics information.  If this
   *   parameter is omitted, it assumes that all nodes are specified.
   *
   * @return array
   *   Array of allocated resources.
   */
  public function loadNodeAllocatedResources($cloud_context, ?CloudProjectInterface $cloud_project = NULL, ?K8sNode $k8s_node = NULL): array {
    $response = [];
    $nodes = [];

    try {
      if (!empty($cloud_project)) {
        $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
        $clusters = array_column($k8s_clusters, 'value');
        $nodes = $clusters
          ? $this->loadEntities('k8s_node', [
            'cloud_context' => $clusters,
          ])
        : [];
      }
      else {

        $nodes = isset($k8s_node)
          ? $this->loadEntities('k8s_node', [
            'cloud_context' => $cloud_context,
            'id' => $k8s_node->id(),
          ])
          : $this->loadEntities('k8s_node', [
            'cloud_context' => $cloud_context,
          ]);
      }

      if (!empty($nodes)) {
        $response = $this->buildAllocatedResourceArray($nodes, $cloud_context);
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $response = [];
    }
    return $response;
  }

  /**
   * Load K8s namespace entities.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\cloud\Entity\CloudProjectInterface $cloud_project
   *   The cloud project.
   * @param \Drupal\k8s\Entity\K8sNamespaceInterface $k8s_namespace
   *   K8s namespace entity.
   *
   * @return array
   *   Associative array of nodes and namespaces.
   */
  protected function loadK8sNamespaceEntities($cloud_context, ?CloudProjectInterface $cloud_project = NULL, ?K8sNamespaceInterface $k8s_namespace = NULL): array {
    $entities = [];
    $cloud_contexts = [];
    if (!empty($cloud_project)) {
      $k8s_clusters = $cloud_project->get('field_k8s_clusters')->getValue();
      foreach ($k8s_clusters ?: [] as $k8s_cluster) {
        $cloud_contexts[] = $k8s_cluster['value'];
      }
    }
    elseif (!empty($cloud_context)) {
      $cloud_contexts[] = $cloud_context;
    }
    // If cloud_context is passed, filter entities with it.
    if (!empty($cloud_contexts)) {
      $entities['nodes'] = [];
      $entities['namespaces'] = [];
      try {
        foreach ($cloud_contexts ?: [] as $loaded_cloud_context) {
          $k8s_nodes = $this->loadEntities('k8s_node', [
            'cloud_context' => $loaded_cloud_context,
          ]);

          $k8s_namespaces = isset($k8s_namespace)
            ? $this->loadEntities('k8s_namespace', [
              'id' => $k8s_namespace->id(),
            ])
            : $this->loadEntities('k8s_namespace', [
              'cloud_context' => $loaded_cloud_context,
            ]);

          $entities['nodes'] = array_merge($entities['nodes'], $k8s_nodes);
          $entities['namespaces'] = array_merge($entities['namespaces'], $k8s_namespaces);
        }
      }
      catch (\Exception $e) {
        $this->handleException($e);
      }
    }
    return $entities;
  }

  /**
   * Generate the calculated namespace costs.
   *
   * @param array $namespaces
   *   Array of K8s namespace entities.
   * @param string $cloud_context
   *   Optional cloud context.
   * @param string $cost_type
   *   Cost type.
   * @param int $period
   *   Period type.
   *
   * @return array
   *   Array of namespace cost data.
   */
  protected function generateNamespaceCosts(array $namespaces, $cloud_context = NULL, $cost_type = 'ri_one_year', $period = 14): array {
    $response = [];

    $plugin = NULL;
    try {
      $plugin = $this->cloudStorePluginManager->loadPluginVariant('k8s_namespace_resource_store');
    }
    catch (PluginNotFoundException $e) {
      return $response;
    }

    $from_time = time() - (int) $period * 24 * 60 * 60;
    $to_time = time();

    foreach ($namespaces ?: [] as $namespace) {
      $result = $plugin->extract('k8s_namespace_resource_store', $namespace->getCloudContext(), $namespace->getName(), $from_time, $to_time, $cost_type);

      if (count($result) > 1) {
        $data['namespace'] = !empty($cloud_context) ? $namespace->getName() : $namespace->getCloudContext() . ':' . $namespace->getName();
        $data['costs'] = [];
        foreach ($result ?: [] as $key => $record) {
          if ($key === 'total_cost') {
            continue;
          }

          $data['costs'][] = [
            'timestamp' => strtotime(date('Y-m-d H:i', $record['created'])),
            'cost' => !empty($record['cost']) ? $record['cost'] : 0,
          ];
        }
        $response[] = $data;
      }
    }

    return $response;
  }

  /**
   * Get total costs of nodes.
   *
   * @param array $nodes
   *   The k8s_node entities.
   * @param string $cost_type
   *   The cost type.
   *
   * @return int
   *   The total costs of nodes.
   */
  protected function getTotalNodesCosts(array $nodes, $cost_type = ''): int {
    $costs = 0;

    if (!\Drupal::moduleHandler()->moduleExists('aws_cloud')) {
      return $costs;
    }

    /** @var \Drupal\aws_cloud\Service\Pricing\InstanceTypePriceDataProvider $price_date_provider */
    $price_date_provider = \Drupal::service('aws_cloud.instance_type_price_data_provider');

    foreach ($nodes ?: [] as $node) {
      // Get instance type and region.
      $region = NULL;
      $instance_type = NULL;
      foreach ($node->get('labels') ?: [] as $item) {
        if ($item->getItemKey() === 'node.kubernetes.io/instance-type') {
          $instance_type = $item->getItemValue();
        }
        elseif ($item->getItemKey() === 'failure-domain.beta.kubernetes.io/region') {
          $region = $item->getItemValue();
        }
      }

      if (empty($instance_type) || empty($region)) {
        continue;
      }

      $price_data = $price_date_provider->getDataByRegion($region, NULL, NULL, NULL, $this->refresh);
      foreach ($price_data ?: [] as $item) {
        if ($item['instance_type'] === $instance_type) {
          if (!empty($item[$cost_type])) {
            if ($cost_type === 'on_demand_hourly') {
              $costs += $item[$cost_type] * 24 * 365;
            }
            elseif ($cost_type === 'on_demand_daily') {
              $costs += $item[$cost_type] * 365;
            }
            elseif ($cost_type === 'on_demand_monthly') {
              $costs += $item[$cost_type] * 12;
            }
            elseif ($cost_type === 'on_demand_yearly') {
              $costs += $item[$cost_type];
            }
            elseif ($cost_type === 'ri_one_year') {
              $costs += $item[$cost_type];
            }
            elseif ($cost_type === 'ri_three_year') {
              $costs += $item[$cost_type] / 3;
            }
          }
          break;
        }
      }
    }

    return $costs;
  }

  /**
   * Get all pods metrics.
   *
   * @param string $cloud_context
   *   Cloud context string.  If no cloud context passed, retrieve
   *   all metric messages.
   * @param int $period
   *   The period to get data.
   *
   * @return array
   *   The pod metrics array.
   */
  protected function getAllPodsMetrics($cloud_context = NULL, $period = 14): array {
    $plugin = NULL;
    try {
      $plugin = $this->cloudStorePluginManager->loadPluginVariant('k8s_pod_resource_store');
    }
    catch (PluginNotFoundException $e) {
      return [];
    }

    $from_time = time() - $period * 24 * 60 * 60;
    $to_time = time();

    $result = $plugin->extract('k8s_pod_resource_store', $cloud_context, NULL, $from_time, $to_time);

    $pod_metrics = [];
    foreach ($result ?: [] as $key => $record) {
      if ($key !== 'total_cost') {
        $date = date('Y-m-d H:i', $record['created']);
        $timestamp = strtotime($date);
        if (!isset($pod_metrics[$timestamp])) {
          $pod_metrics[$timestamp] = [];
        }
        $resources = $record['resources'];
        $name = $record['name'];
        $pod_metrics[$timestamp][$name] = [
          'cpu' => (float) $resources['cpu'],
          'memory' => (float) ($resources['memory'] ?? 0),
        ];
      }
    }

    return $pod_metrics;
  }

  /**
   * Helper method to load entities by entity type.
   *
   * @param string $entity_type
   *   Entity type to load.
   * @param bool $access_check
   *   Whether access check is needed.
   *
   * @return array
   *   Loaded entities array.
   */
  protected function getEntitiesByType($entity_type, $access_check = TRUE): array {
    $entities = [];
    try {
      $entities = $this->loadEntities($entity_type, [], $access_check);
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $entities;
  }

  /**
   * Load Entity with access check.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $values
   *   Query condition array.
   * @param bool $access_check
   *   Whether access check is needed.
   *
   * @return array
   *   The array of entities.
   */
  protected function loadEntities($entity_type_id, array $values = [], $access_check = TRUE): array {
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type_id);
    $entity_query = $storage->getQuery();
    $entity_query->accessCheck($access_check);
    if (!empty($values)) {
      foreach ($values as $name => $value) {
        $entity_query->condition($name, (array) $value, 'IN');
      }
    }
    $result = $entity_query->execute();
    $entities = $result ? $storage->loadMultiple($result) : [];
    if ($access_check && !empty($entities)) {
      foreach ($entities ?: [] as $id => $entity) {
        if (!$entity->access('view')) {
          unset($entities[$id]);
        }
      }
    }
    return $entities;
  }

}
