<?php

namespace Drupal\k8s\Traits;

use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * Traits for K8s form.
 */
trait K8sFormTrait {

  use CloudContentEntityTrait;

  /**
   * Load Entity with access check.
   *
   * @param int $hour
   *   The hour.
   * @param int $minutes
   *   The minutes.
   *
   * @return string
   *   The time.
   */
  public function getTimeFormat($hour, $minutes) : string {
    return sprintf('%02d:%02d', $hour, $minutes);
  }

}
