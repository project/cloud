@minimal
Feature: Create a cloud service provider for K8s as "Cloud administrator"
  In order to start BDD testing
  We need to create a cloud service provider

  @setup
  @api
  Scenario: Add a K8s Cloud service provider
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config/add/k8s"
    And I enter "{{ cloud_service_provider_name_entered }}" for "Name"
    And I enter "{{ api_server }}" for "API server"
    And I enter "{{ api_server_token }}" for "Token"
    And I press "Save"
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "has been created"
    And I should see no error message
    And I should see K8s in the "{{ cloud_service_provider_name }}" row

  @ignore @ci_job
  @api
  Scenario: Add a K8s Cloud service provider
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config/add/k8s"
    And I enter "{{ cloud_service_provider_name_entered }}" for "Name"
    And I enter "{{ api_server }}" for "API server"
    And I enter "{{ api_server_token }}" for "Token"
    And I press "Save"
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "has been created"
    And I should see no error message
    And I should see the warning message "The region {{ aws_region }} is not available"
    And I should see K8s in the "{{ cloud_service_provider_name }}" row
