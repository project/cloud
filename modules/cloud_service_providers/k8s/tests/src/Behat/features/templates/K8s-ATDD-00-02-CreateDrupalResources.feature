@minimal @ci_job @setup
Feature: Create a Drupal role for K8s and a user as "Administrator"

  @api
  Scenario: Create a role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/roles/add"
    And I enter "{{ drupal_role_name }}" for "Role name"
    And I enter "{{ drupal_role_name_machine }}" for "Machine-readable name"
    And I press "Save"
    And I should be on "/admin/people/roles"
    And I should see "{{ drupal_role_name }}"
    Then I visit "admin/people/permissions/{{ drupal_role_name_machine }}"
    # Access entities
    And I check the box "View any K8s entities belonging to a namespace: default"
    And I check the box "View any K8s entities belonging to a namespace: kube-node-lease"
    And I check the box "View any K8s entities belonging to a namespace: kube-public"
    And I check the box "View any K8s entities belonging to a namespace: kube-system"
    And I check the box "View any K8s entities belonging to a namespace: kubernetes-dashboard"
    And I check the box "View K8s event"
    And I check the box "View K8s node"
    # Allow to see the cloud service provider
    And I check the box "Access {{ cloud_service_provider_name }}"
    # Allow to launch an instance.
    And I check the box "Launch approved cloud launch template"
    And I press "Save permissions"
    And I should see the success message "The changes have been saved."
    And I should see neither error nor warning messages

  @api
  Scenario: Create a user
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/create"
    And I enter "{{ user_name }}" for "Username"
    And I enter "{{ user_password }}" for "Password"
    And I enter "{{ user_password }}" for "Confirm password"
    And I check the box "{{ drupal_role_name }}"
    And I press "Create new account"
    Then I should see the success message "Created a new user account for"
    And I should see neither error nor warning messages
    And I visit "/admin/people"
    And I should see "{{ user_name }}"

  @api
  Scenario Outline: View the permissions of the Cloud Administrator role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/permissions/<role_name_machine>"
    Then I should see "View entities in any K8s namespace"
     # DO NOT change <status> to "<status>" since <status> is not an :arg2.
    And the checkbox "View entities in any K8s namespace" should <status>

    Examples:
      | role_name_machine              | status         |
      | authenticated                  | not be checked |
      | {{ drupal_role_name_machine }} | not be checked |
      | administrator                  | be checked     |
      | cloud_admin                    | be checked     |
