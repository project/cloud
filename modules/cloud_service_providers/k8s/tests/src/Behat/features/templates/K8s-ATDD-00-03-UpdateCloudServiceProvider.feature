@ci_job @target
Feature: Read and update the created K8s Cloud service provider

  @api
  Scenario: Update and read the updated Cloud service provider
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config"
    And I should see the link "{{ cloud_service_provider_name }}" in the table
    And I should see "Edit" in the "{{ cloud_service_provider_name }}" row in the table
    And I wait {{ wait_dropdown }} milliseconds
    And I click "Edit" in the "{{ cloud_service_provider_name }}" row
    Then the url should match "/edit"
    And I enter "{{ cloud_service_provider_name_updated }}" for "Name"
    And I enter "{{ cloud_service_provider_tag }}" for "tags"
    # Remove the following step once the cloud admin can update it regardless of the owner status.
    And I fill in "Authored by" with "{{ cloud_service_provider_author }}"
    And I press "Save"
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "has been updated"
    And I click "{{ cloud_service_provider_name_updated }}" in the "success_message"
    Then the url should match "admin/structure/cloud_config/"
    And I should see the heading "{{ cloud_service_provider_name_updated }}"
    And I should see "{{ cloud_service_provider_name_updated }}" in the "details"
    And I should see the link "{{ cloud_service_provider_tag }}"

  @api
  Scenario: Revert the update and read the reverted Cloud service provider
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config"
    And I should see the link "{{ cloud_service_provider_name_updated }}" in the table
    And I should see "Edit" in the "{{ cloud_service_provider_name_updated }}" row in the table
    And I wait {{ wait_dropdown }} milliseconds
    And I click "Edit" in the "{{ cloud_service_provider_name_updated }}" row
    Then the url should match "/edit"
    And I enter "{{ cloud_service_provider_name }}" for "Name"
    And I fill in "{{ cloud_service_provider_tag_edit_field }}" with ""
    And I press "Save"
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "has been updated"
    And I click "{{ cloud_service_provider_name }}" in the "success_message"
    Then the url should match "admin/structure/cloud_config/"
    And I should see the heading "{{ cloud_service_provider_name }}"
    And I should see "{{ cloud_service_provider_name }}" in the details
    And I should not see the link "{{ cloud_service_provider_tag }}"
