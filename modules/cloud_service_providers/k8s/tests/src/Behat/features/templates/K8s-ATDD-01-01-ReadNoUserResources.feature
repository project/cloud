@ci_job
Feature: Confirm no K8s resources created by user as "Administrator"
  In order to be able to replicate BDD tests
  As an administrator
  I need to confirm no resources

  @api
  Scenario Outline: No items are listed in "<resource_type>"
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/k8s/{{ cloud_context }}/<resource_type>"
    And I click "Refresh"
    Then I should see "No items." in the table
    And I should see neither error nor warning messages

  Examples:
    | resource_type             |
    | cron_job                  |
    | job                       |
    | network_policy            |
    | resource_quota            |
    | limit_range               |
    | persistent_volume         |
    | persistent_volume_claim   |
    | stateful_set              |
    | ingress                   |

  @api
  Scenario Outline: Access is denied for authenticated users
    Given I am logged in as a user with the "Authenticated user" role
    When I go to "/clouds/k8s/{{ cloud_context }}/<resource_type>"
    Then I should get a 403 HTTP response
    And I should see the text "Access denied"

  Examples:
    | resource_type             |
    | cron_job                  |
    | job                       |
    | network_policy            |
    | resource_quota            |
    | limit_range               |
    | persistent_volume         |
    | persistent_volume_claim   |
    | stateful_set              |
    | ingress                   |

  @api
  Scenario: No schedules are listed
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    Then I should see "No items." in the table
    And I should see neither error nor warning messages

  @api @wip
  Scenario: No items are listed in "<resource_type>"
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler"
    And I click "Refresh"
    Then I should see "No items." in the table
    And I should see neither error nor warning messages
