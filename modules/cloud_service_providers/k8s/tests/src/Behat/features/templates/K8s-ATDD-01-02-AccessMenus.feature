@ci_job
Feature: View the created resources for K8s as "Administrator"
  In order to prepare BDD testing
  We need to make sure the role exists

  @api
  Scenario Outline: View the menu of K8s Cloud service providers
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "K8s" in the "nav_bar"
    When I go to "/clouds"
    Then I should get a 200 HTTP response
    And I should see the heading "Cloud service providers"
    And I should see neither error nor warning messages

    Examples:
      |  role               |
      | Authenticated user  |
      | Cloud administrator |

  @api
  Scenario: View the manage menu of K8s Cloud service providers
    Given I am logged in as a user with the "Cloud administrator" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "K8s" in the "nav_bar"
    When I visit "/clouds"
    Then I should see "Instance Pricing"
    And I should see "Location map"
    And I should see the button "Apply"
    And I should see neither error nor warning messages

  @api
  Scenario Outline: View the list of K8s Cloud service providers
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "K8s" in the "nav_bar"
    When I go to "/clouds"
    Then I should get a 200 HTTP response
    And I should see the heading "Cloud service providers"
    And I should see "Location map"
    And I should see the button "Apply"
    # DO NOT change <status> to "<status>" since <status> is not an :arg1.
    And I should <status> "No items." in the table
    And I should see neither error nor warning messages

    Examples:
      | role                | status  |
      | Authenticated user  | see     |
      | Cloud administrator | not see |

  @api
  Scenario Outline: View the list of K8s resources
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "K8s" in the "nav_bar"
    When I go to "/clouds/k8s/node"
    Then I should get a 200 HTTP response
    And I should see the heading "All K8s nodes"
    And I should see the button "Apply"
    And I should see the link "Nodes"
    And I should see neither error nor warning messages

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |
