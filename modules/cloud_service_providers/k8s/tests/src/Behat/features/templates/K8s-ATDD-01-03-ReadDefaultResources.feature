@minimal @ci_job
Feature: Confirm default K8s resources
  In order to be able to replicate BDD tests
  I need to confirm default resources

  @api
  Scenario Outline: Verify absence of default resources for authenticated user
    Given I am logged in as a user with the "Authenticated user" role
    When I visit "/clouds/k8s/<resource_type>"
    # DO NOT change <status> to "<status>" since <status> is not an :arg1.
    Then I should <status> "No items." in the table
    And I should see neither error nor warning messages

    Examples:
      | resource_type        | status |
      | node                 | see    |
      | namespace            | see    |
      | deployment           | see    |
      | pod                  | see    |
      | replica_set          | see    |
      | service              | see    |
      | priority_class       | see    |
      | config_map           | see    |
      | secret               | see    |
      | role                 | see    |
      | role_binding         | see    |
      | cluster_role         | see    |
      | cluster_role_binding | see    |
      | storage_class        | see    |
      | daemon_set           | see    |
      | endpoint             | see    |
      | api_service          | see    |
      | service_account      | see    |

  @api
  Scenario Outline: Verify absence of default resources for Cloud administrator
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/k8s/<resource_type>"
    # DO NOT change <status> to "<status>" since <status> is not an :arg1.
    Then I should <status> "No items." in the table
    And I should see neither error nor warning messages

  Examples:
    | resource_type        | status  |
    | node                 | not see |
    | namespace            | not see |
    | deployment           | not see |
    | pod                  | not see |
    | replica_set          | not see |
    | service              | not see |
    | priority_class       | not see |
    | config_map           | not see |
    | secret               | not see |
    | role                 | not see |
    | role_binding         | not see |
    | cluster_role         | not see |
    | cluster_role_binding | not see |
    | storage_class        | not see |
    | daemon_set           | not see |
    | endpoint             | not see |
    | api_service          | not see |
    | service_account      | not see |

  @api
  Scenario Outline: Verify absence of default resources for Administrator
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/k8s/<resource_type>"
    # DO NOT change <status> to "<status>" since <status> is not an :arg1.
    Then I should <status> "No items." in the table
    And I should see neither error nor warning messages

  Examples:
    | resource_type        | status  |
    | node                 | not see |
    | namespace            | not see |
    | deployment           | not see |
    | pod                  | not see |
    | replica_set          | not see |
    | service              | not see |
    | priority_class       | not see |
    | config_map           | not see |
    | secret               | not see |
    | role                 | not see |
    | role_binding         | not see |
    | cluster_role         | not see |
    | cluster_role_binding | not see |
    | storage_class        | not see |
    | daemon_set           | not see |
    | endpoint             | not see |
    | api_service          | not see |
    | service_account      | not see |
