@ci_job
Feature: Confirm the refresh button works for view-only resources

  @api
  Scenario Outline: Refresh the list of "<resource_type>"
    Given I am logged in as user '{{ user_name }}'
    When I visit "/clouds/k8s/<resource_type>"
    And I click "Refresh"
    Then I should see the success message "Updated"

    Examples:
      | resource_type |
      | node          |
      | event         |
