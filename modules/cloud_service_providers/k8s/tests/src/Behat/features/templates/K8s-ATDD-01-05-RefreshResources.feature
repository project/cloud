@ci_job
Feature: Confirm the refresh button works for editable resources

  @api
  Scenario Outline: Refresh the list of "<resource_type>" for Cloud administrator
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/k8s/<resource_type>"
    And I click "Refresh"
    Then I should see the success message "Updated"

    Examples:
      | resource_type           |
      | namespace               |
      | deployment              |
      | pod                     |
      | replica_set             |
      | cron_job                |
      | job                     |
      | service                 |
      | network_policy          |
      | resource_quota          |
      | limit_range             |
      | priority_class          |
      | config_map              |
      | secret                  |
      | role                    |
      | role_binding            |
      | cluster_role            |
      | cluster_role_binding    |
      | persistent_volume       |
      | persistent_volume_claim |
      | storage_class           |
      | stateful_set            |
      | ingress                 |
      | daemon_set              |
      | endpoint                |
      | api_service             |
      | service_account         |

  @api
  Scenario Outline: Refresh the list of "<resource_type>" for Administrator
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/k8s/<resource_type>"
    And I click "Refresh"
    Then I should see the success message "Updated"

    Examples:
      | resource_type           |
      | namespace               |
      | deployment              |
      | pod                     |
      | replica_set             |
      | cron_job                |
      | job                     |
      | service                 |
      | network_policy          |
      | resource_quota          |
      | limit_range             |
      | priority_class          |
      | config_map              |
      | secret                  |
      | role                    |
      | role_binding            |
      | cluster_role            |
      | cluster_role_binding    |
      | persistent_volume       |
      | persistent_volume_claim |
      | storage_class           |
      | stateful_set            |
      | ingress                 |
      | daemon_set              |
      | endpoint                |
      | api_service             |
      | service_account         |
