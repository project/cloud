@minimal @ci_job @setup
Feature: Create a namespace for K8s as "Authenticated User"

  @api
  Scenario: Create a namespace
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/namespace/add"
    And I should see the heading "Add Namespace"
    And I enter "{{ namespace }}" for "Name"
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/namespace"
    And I should see the success message "{{ namespace }} has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ namespace }}" in the table

  @api
  Scenario: Read the created namespace
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/namespace"
    And I should see "K8s namespaces"
    And I click "Refresh"
    And I should see the link "{{ namespace }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ namespace }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/namespace/"
    And I should see "{{ namespace }}" in the "page_header"
    And I should see "{{ namespace }}" in the "details"
    And I should see "Active" in the "details"
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the created namespace
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/namespace"
    And I should see the heading "K8s namespaces"
    And I click "Refresh"
    And I should see the link "{{ namespace }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ namespace }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/namespace/"
    And I click "Edit"
    And I fill in "{{ namespace_labels_item_key_field }}" with "{{ namespace_labels_item_key }}"
    And I fill in "{{ namespace_labels_item_value_field }}" with "{{ namespace_labels_item_value }}"
    And I fill in "{{ namespace_annotations_item_key_field }}" with "{{ namespace_annotations_item_key }}"
    And I fill in "{{ namespace_annotations_item_value_field }}" with "{{ namespace_annotations_item_value }}"
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/namespace"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should see the link "{{ namespace }}" in the table
    And I wait {{ wait }} milliseconds
    And I click "{{ namespace }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/namespace/"
    And I should see "{{ namespace_labels_item_key }}"
    And I should see "{{ namespace_labels_item_value }}"
    And I should see "{{ namespace_annotations_item_key }}"
    And I should see "{{ namespace_annotations_item_value }}"

    @api
    Scenario: Update the created namespace
      Given I am logged in as user "{{ user_name }}"
      When I visit "/clouds/k8s/{{ cloud_context }}/namespace"
      And I should see the heading "K8s namespaces"
      And I click "Refresh"
      And I should see the link "{{ namespace }}" in the table
      And I wait {{ wait }} milliseconds
      And I click "{{ namespace }}"
      Then the url should match "/clouds/k8s/{{ cloud_context }}/namespace/"
      And I click "Edit"
      And I fill in "{{ namespace_labels_item_key_field }}" with ""
      And I fill in "{{ namespace_labels_item_value_field }}" with ""
      And I fill in "{{ namespace_annotations_item_key_field }}" with ""
      And I fill in "{{ namespace_annotations_item_value_field }}" with ""
      And I press "Save"

  @api
  Scenario: Update the role
    Given I am logged in as a user with the "Administrator" role
    When I visit "admin/people/permissions/{{ drupal_role_name_machine }}"
    And I check the box "View any K8s entities belonging to a namespace: {{ namespace }}"
    And I press "Save permissions"
    Then I should see the success message "The changes have been saved."
    And I should see neither error nor warning messages
