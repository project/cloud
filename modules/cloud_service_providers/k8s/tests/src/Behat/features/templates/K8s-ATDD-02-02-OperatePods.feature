@minimal @ci_job
Feature: Create and delete a pod for K8s as "Authenticated User"

  @api
  Scenario: Create a pod
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/pod/add"
    And I should see the heading "Add Pod"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: Pod
    metadata:
      name: {{ pod_name }}
    spec:
      containers:
      - name: {{ container_name }}
        image: {{ image_version }}
        ports:
        - containerPort: {{ port }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/pod"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ pod_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the Pod
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/pod"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ pod_name }}"
    And I click "{{ pod_name }}"
    Then the url should match "clouds/k8s/{{ cloud_context }}/pod/"
    And I should see "{{ pod_name }}" in the "page_header"
    And I should see "{{ namespace }}"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Delete the pod
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/pod"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ pod_name }}"
    And I click "{{ pod_name }}"
    And the url should match "/k8s/{{ cloud_context }}/pod/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/pod"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see with "Running" in the "{{ pod_name }}" row, no rows, or no table
