@minimal @ci_job
Feature: Create, read, update and delete a deployment for K8s as "Authenticated User"

  @api
  Scenario: Create a deployment
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/deployment/add"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: {{ deployment_name }}
      labels:
        app: {{ app_name }}
    spec:
      replicas: {{ replica_count }}
      selector:
        matchLabels:
          app: {{ app_name }}
      template:
        metadata:
          labels:
            app: {{ app_name }}
        spec:
          containers:
          - name: {{ container_name }}
            image: {{ image_version }}
            ports:
            - containerPort: {{ port }}
    """
    And I press "Save"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/deployment"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ namespace }}" in the "{{ deployment_name }}" row

  @api
  Scenario: Read the deployment
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/deployment"
    And I click "Refresh"
    And I should see the link "{{ deployment_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ deployment_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/deployment/"
    And I should see "{{ deployment_name }}" in the "page_header"
    And I should see "{{ deployment_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the deployment
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/deployment"
    And I click "Refresh"
    And I should see the link "{{ deployment_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ deployment_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/deployment/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: {{ deployment_name }}
      labels:
        app: {{ app_name }}
    spec:
      replicas: {{ replica_count_edit }}
      selector:
        matchLabels:
          app: {{ app_name }}
      template:
        metadata:
          labels:
            app: {{ app_name }}
        spec:
          containers:
          - name: {{ container_name }}
            image: {{ image_version }}
            ports:
            - containerPort: {{ port }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/deployment"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I click "{{ deployment_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/deployment/"
    And I should see "{{ replica_count_edit }}"

  @api
  Scenario: Delete the deployment
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/deployment"
    And I click "Refresh"
    And I should see the link "{{ deployment_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ deployment_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/deployment/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/deployment"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ deployment_name }}"
