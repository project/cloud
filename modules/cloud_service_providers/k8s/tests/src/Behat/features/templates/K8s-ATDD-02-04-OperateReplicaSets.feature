@minimal @ci_job
Feature: Create and delete a ReplicaSet for K8s as "Authenticated User"

  @api
  Scenario: Create a ReplicaSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/replica_set/add"
    And I should see the heading "Add ReplicaSet"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: apps/v1
    kind: ReplicaSet
    metadata:
      name: {{ replicaset_name }}
    spec:
      replicas: {{ replica_count }}
      selector:
        matchLabels:
          app: {{ app_name }}
      template:
        metadata:
          labels:
            app: {{ app_name }}
        spec:
          containers:
          - name: {{ container_name }}
            image: {{ image_version }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/replica_set"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ replicaset_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the ReplicaSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/replica_set"
    And I click "Refresh"
    And I should see the link "{{ replicaset_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ replicaset_name }}"
    Then the url should match "/replica_set/"
    And I should see "{{ replicaset_name }}" in the "page_header"
    And I should see "{{ replicaset_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the ReplicaSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/replica_set"
    And I click "Refresh"
    And I should see the link "{{ replicaset_name }}"
    And I should see "{{ replica_count }}" in the "{{ namespace }}" row
    And I wait {{ wait }} milliseconds
    And I click "{{ replicaset_name }}"
    And the url should match "/replica_set/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: apps/v1
    kind: ReplicaSet
    metadata:
      name: {{ replicaset_name }}
    spec:
      replicas: {{ replica_count_edit }}
      selector:
        matchLabels:
          app: {{ app_name }}
      template:
        metadata:
          labels:
            app: {{ app_name }}
        spec:
          containers:
          - name: {{ container_name }}
            image: {{ image_version }}
    """
    And I press "Save"
    And the url should match "/clouds/k8s/{{ cloud_context }}/replica_set"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I should see "{{ replicaset_name }}" in the "{{ namespace }}" row
    And I should see "{{ replica_count_edit }}" in the "{{ namespace }}" row

  @api
  Scenario: Delete the ReplicaSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/replica_set"
    And I click "Refresh"
    And I should see the link "{{ replicaset_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ replicaset_name }}"
    And the url should match "/replica_set/"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    And I should be on "/clouds/k8s/{{ cloud_context }}/replica_set"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ replicaset_name }}"
