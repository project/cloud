@minimal @ci_job
Feature: Create, read, update and delete a Service for K8s as "Authenticated User"

  @api
  Scenario: Create a Service
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/service/add"
    And I should see the heading "Add Service"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: Service
    metadata:
      name: {{ service_name }}
    spec:
      selector:
        app: {{ app_name }}
      ports:
      - protocol: {{ protocol }}
        port: {{ port }}
        targetPort: {{ target_port }}
    """
    And I press "Save"
    Then the url should match "clouds/k8s/{{ cloud_context }}/service"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ service_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the service
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/service"
    And I click "Refresh"
    And I should see the link "{{ service_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ service_name }}"
    Then the url should match "clouds/k8s/{{ cloud_context }}/service/"
    And I should see "{{ service_name }}" in the "page_header"
    And I should see "{{ service_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Delete the service
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/service"
    And I click "Refresh"
    And I should see the link "{{ service_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ service_name }}"
    Then the url should match "clouds/k8s/{{ cloud_context }}/service/"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/service"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ service_name }}"
