@minimal @ci_job
Feature: Create, read, update and delete a CronJob for K8s as "Authenticated User"

  @api
  Scenario: Create a CronJob
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/cron_job/add"
    And I should see the heading "Add CronJob"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: batch/v1
    kind: CronJob
    metadata:
      name: {{ cronjob_name }}
    spec:
      schedule: "{{ cronjob_schedule }}"
      jobTemplate:
        spec:
          template:
            spec:
              containers:
              - name: {{ container_name }}
                image: {{ image_version }}
                imagePullPolicy: IfNotPresent
                command:
                - /bin/sh
                - -c
                - date; echo Hello from the Kubernetes cluster
              restartPolicy: OnFailure
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/cron_job"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ cronjob_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the CronJob
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/cron_job"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ cronjob_name }}"
    And I click "{{ cronjob_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/cron_job/"
    And I should see "{{ cronjob_name }}" in the "page_header"
    And I should see "{{ cronjob_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the CronJob
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/cron_job"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ cronjob_name }}"
    And I click "{{ cronjob_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/cron_job/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: batch/v1
    kind: CronJob
    metadata:
      name: {{ cronjob_name }}
    spec:
      schedule: "{{ cronjob_schedule_edit }}"
      jobTemplate:
        spec:
          template:
            spec:
              containers:
              - name: {{ container_name }}
                image: {{ image_version }}
                imagePullPolicy: IfNotPresent
                command:
                - /bin/sh
                - -c
                - date; echo Hello from the Kubernetes cluster
              restartPolicy: OnFailure
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/cron_job"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ cronjob_name }}"
    And the url should match "/clouds/k8s/{{ cloud_context }}/cron_job/"
    And I should see "*/2 * * * *"

  @api
  Scenario: Delete the CronJob
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/cron_job"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ cronjob_name }}"
    And I click "{{ cronjob_name }}"
    And the url should match "/k8s/{{ cloud_context }}/cron_job/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/cron_job"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ cronjob_name }}"
