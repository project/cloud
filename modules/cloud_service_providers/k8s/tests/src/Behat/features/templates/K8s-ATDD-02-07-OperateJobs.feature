@minimal @ci_job
Feature: Create delete a job for K8s as "Authenticated User"

  @api
  Scenario: Create a job
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/job/add"
    And I should see the heading "Add Job"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: batch/v1
    kind: Job
    metadata:
      name: {{ job_name }}
    spec:
      template:
        spec:
          containers:
          - name: {{ container_name }}
            image: {{ image_name }}
            command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
          restartPolicy: Never
      backoffLimit: 4
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/job"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ job_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the Job
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/job"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ job_name }}"
    And I click "{{ job_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/job/"
    And I should see "{{ job_name }}" in the "page_header"
    And I should see "{{ job_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Delete the job
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/job"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ job_name }}"
    And I click "{{ job_name }}"
    And the url should match "/k8s/{{ cloud_context }}/job/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/job"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I click "Refresh"
    Then I should not see "{{ job_name }}" in the table or no such region
