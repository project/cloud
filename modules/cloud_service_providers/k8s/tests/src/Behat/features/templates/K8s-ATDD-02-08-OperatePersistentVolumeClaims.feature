@minimal @ci_job
Feature: Create, read, update and delete a persistent volume claim for K8s as "Authenticated User"

  @api
  Scenario: Create a persistent volume claim
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/persistent_volume_claim/add"
    And I should see the heading "Add Persistent volume claim"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    kind: PersistentVolumeClaim
    apiVersion: v1
    metadata:
      name: {{ persistent_volume_claim_name }}
    spec:
      accessModes:
      - ReadWriteOnce
      resources:
        requests:
          storage: {{ volume_capacity }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/persistent_volume_claim"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ persistent_volume_claim_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the persistent volume claim
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/persistent_volume_claim"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ persistent_volume_claim_name }}"
    And I click "{{ persistent_volume_claim_name }}"
    Then the url should match "clouds/k8s/{{ cloud_context }}/persistent_volume_claim/"
    And I should see "{{ persistent_volume_claim_name }}" in the "page_header"
    And I should see "{{ persistent_volume_claim_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Delete the persistent volume claim
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/persistent_volume_claim"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ persistent_volume_claim_name }}"
    And I click "{{ persistent_volume_claim_name }}"
    Then the url should match "clouds/k8s/{{ cloud_context }}/persistent_volume_claim/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/persistent_volume_claim"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ persistent_volume_claim_name }}"
