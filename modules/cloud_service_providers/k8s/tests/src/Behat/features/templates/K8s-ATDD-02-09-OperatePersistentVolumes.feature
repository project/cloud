@minimal @ci_job
Feature: Create, read, update and delete a persistent volume for K8s as "Authenticated User"

  @api
  Scenario: Create a persistent volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/persistent_volume/add"
    And I should see the heading "Add Persistent volume"
    And I fill in "Detail" with:
    """
    kind: PersistentVolume
    apiVersion: v1
    metadata:
      name: {{ persistent_volume_name }}
    spec:
      capacity:
        storage: {{ volume_capacity }}
      volumeMode: Filesystem
      accessModes:
      - ReadWriteOnce
      persistentVolumeReclaimPolicy: Delete
      hostPath:
        path: {{ volume_local_path }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/persistent_volume"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ persistent_volume_name }}" in the table

  @api
  Scenario: Read the persistent volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/persistent_volume"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ persistent_volume_name }}"
    And I click "{{ persistent_volume_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/persistent_volume/"
    And I should see "{{ persistent_volume_name }}" in the "page_header"
    And I should see "{{ persistent_volume_name }}" in the "details"
    And I should see "{{ volume_capacity }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the persistent volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/persistent_volume"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ persistent_volume_name }}"
    And I click "{{ persistent_volume_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/persistent_volume/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    kind: PersistentVolume
    apiVersion: v1
    metadata:
      name: {{ persistent_volume_name }}
    spec:
      capacity:
        storage: {{ volume_capacity_edit }}
      volumeMode: Filesystem
      accessModes:
      - ReadWriteOnce
      persistentVolumeReclaimPolicy: Delete
      hostPath:
        path: {{ volume_local_path }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/persistent_volume"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see "{{ persistent_volume_name }}"
    And I should see "{{ volume_capacity_edit }}"

  @api
  Scenario: Delete the persistent volume
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/persistent_volume"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ persistent_volume_name }}"
    And I click "{{ persistent_volume_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/persistent_volume/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/persistent_volume"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ persistent_volume_name }}"
