@minimal @ci_job
Feature: Create, read, update and delete a storage class for K8s as "Authenticated User"

  @api
  Scenario: Create a storage class
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/storage_class/add"
    And I should see the heading "Add Storage class"
    # This manifest is only applicable to EKS.
    And I fill in "Detail" with:
    """
    apiVersion: storage.k8s.io/v1
    kind: StorageClass
    metadata:
      name: {{ storage_class_name }}
    provisioner: {{ storage_class_provisioner }}
    parameters:
      type: {{ storage_class_type }}
    reclaimPolicy: {{ storage_class_policy }}
    allowVolumeExpansion: {{ storage_class_expansion }}
    mountOptions:
      - {{ storage_class_options }}
    volumeBindingMode: {{ storage_class_mode }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/storage_class"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ storage_class_name }}" in the table

  @api
  Scenario: Read the storage class
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/storage_class"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ storage_class_name }}"
    And I click "{{ storage_class_name }}"
    Then the url should match "/k8s/{{ cloud_context }}/storage_class/"
    And I should see "{{ storage_class_name }}" in the "page_header"
    And I should see "{{ storage_class_name }}" in the "details"
    And I should see "{{ storage_class_type }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the storage class
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/storage_class"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ storage_class_name }}"
    And I click "{{ storage_class_name }}"
    Then the url should match "/k8s/{{ cloud_context }}/storage_class/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: storage.k8s.io/v1
    kind: StorageClass
    metadata:
      name: {{ storage_class_name }}
      labels:
        app: {{ app_name }}
    provisioner: {{ storage_class_provisioner }}
    parameters:
      type: {{ storage_class_type }}
    reclaimPolicy: {{ storage_class_policy }}
    allowVolumeExpansion: {{ storage_class_expansion }}
    mountOptions:
      - {{ storage_class_options }}
    volumeBindingMode: {{ storage_class_mode }}
    """
    And I press "Save"
    Then the url should match "/k8s/{{ cloud_context }}/storage_class"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ storage_class_name }}"
    Then the url should match "/k8s/{{ cloud_context }}/storage_class/"
    And I should see "{{ app_name }}"

  @api
  Scenario: Delete the storage class
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/storage_class"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ storage_class_name }}"
    And I click "{{ storage_class_name }}"
    And the url should match "/k8s/{{ cloud_context }}/storage_class/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/storage_class"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ storage_class_name }}"
