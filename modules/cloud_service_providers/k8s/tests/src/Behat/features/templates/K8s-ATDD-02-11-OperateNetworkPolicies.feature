@minimal @ci_job
Feature: Create, read, update and delete a network policy for K8s as "Authenticated User"

  @api
  Scenario: Create a network policy
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/network_policy/add"
    And I should see the heading "Add Network policy"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: networking.k8s.io/v1
    kind: NetworkPolicy
    metadata:
      name: {{ network_policy_name }}
      namespace: {{ namespace }}
    spec:
      podSelector:
        matchLabels:
          app: {{ app_name }}
      policyTypes:
        - Ingress
      ingress:
        - from:
            - podSelector:
                matchLabels:
                  app: {{ app_name }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/network_policy"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ network_policy_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the network policy
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/network_policy"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ network_policy_name }}"
    And I click "{{ network_policy_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/network_policy/"
    And I should see "{{ network_policy_name }}" in the "page_header"
    And I should see "{{ network_policy_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the network policy
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/network_policy"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ network_policy_name }}"
    And I click "{{ network_policy_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/network_policy/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: networking.k8s.io/v1
    kind: NetworkPolicy
    metadata:
      name: {{ network_policy_name }}
      namespace: {{ namespace }}
      labels:
        app: {{ app_name }}
    spec:
      podSelector:
        matchLabels:
          app: {{ app_name }}
      policyTypes:
        - Ingress
      ingress:
        - from:
            - podSelector:
                matchLabels:
                  app: {{ app_name }}
    """
    And I press "Save"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/network_policy"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ network_policy_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/network_policy/"
    And I should see "{{ app_name }}"

  @api
  Scenario: Delete the network policy
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/network_policy"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ network_policy_name }}"
    And I click "{{ network_policy_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/network_policy/"
    And I click "Delete" in the "actions"
    Then the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/network_policy"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ network_policy_name }}"
