@minimal @ci_job
Feature: Create, read, update and delete a resource quota for K8s as "Authenticated User"

  @api
  Scenario: Create a resource quota
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/resource_quota/add"
    And I should see the heading "Add Resource quota"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: ResourceQuota
    metadata:
      name: {{ resource_quota_name }}
    spec:
      hard:
        cpu: "{{ resource_quota_cpu }}"
        memory: "{{ resource_quota_memory }}"
        pods: "{{ resource_quota_pod_count }}"
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/resource_quota"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ resource_quota_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the resource quota
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/resource_quota"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ resource_quota_name }}"
    And I click "{{ resource_quota_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/resource_quota/"
    And I should see "{{ resource_quota_name }}" in the "page_header"
    And I should see "{{ resource_quota_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the resource quota
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/resource_quota"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ resource_quota_name }}"
    And I click "{{ resource_quota_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/resource_quota/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: ResourceQuota
    metadata:
      name: {{ resource_quota_name }}
    spec:
      hard:
        cpu: "{{ resource_quota_cpu_edit }}"
        memory: "{{ resource_quota_memory }}"
        pods: "{{ resource_quota_pod_count }}"
    """
    And I press "Save"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/resource_quota"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ resource_quota_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/resource_quota/"
    And I should see "{{ resource_quota_cpu_edit }}"

  @api
  Scenario: Delete the resource quota
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/resource_quota"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ resource_quota_name }}"
    And I click "{{ resource_quota_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/resource_quota/"
    And I click "Delete" in the "actions"
    Then the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/resource_quota"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ resource_quota_name }}"
