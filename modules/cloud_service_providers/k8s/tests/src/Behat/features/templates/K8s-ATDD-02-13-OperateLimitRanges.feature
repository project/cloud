@minimal @ci_job
Feature: Create and delete a LimitRange for K8s as "Authenticated User"

  @api
  Scenario: Create a LimitRange
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/limit_range/add"
    And I should see the heading "Add LimitRange"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
      """
      apiVersion: v1
      kind: LimitRange
      metadata:
        name: {{ limitrange_name }}
      spec:
        limits:
        - max:
            cpu: {{ max_cpu }}
          min:
            cpu: {{ min_cpu }}
          type: {{ type }}
      """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/limit_range"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ limitrange_name }}" in the "{{ namespace }}" row
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ limitrange_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the LimitRange
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/limit_range"
    And I click "Refresh"
    And I should see the link "{{ limitrange_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ limitrange_name }}"
    And the url should match "/limit_range/"
    And I should see "{{ limitrange_name }}" in the "page_header"
    And I should see "{{ limitrange_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the LimitRange
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/limit_range"
    And I click "Refresh"
    And I should see the link "{{ limitrange_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ limitrange_name }}"
    And the url should match "/limit_range/"
    And I should see "{{ max_cpu }}"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: LimitRange
    metadata:
      name: {{ limitrange_name }}
    spec:
      limits:
      - max:
          cpu: {{ max_cpu_edit }}
        min:
          cpu: {{ min_cpu }}
        type: {{ type }}
    """
    And I press "Save"
    And the url should match "/clouds/k8s/{{ cloud_context }}/limit_range"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ limitrange_name }}"
    And the url should match "/limit_range/"
    And I should see "{{ max_cpu_edit }}"

  @api
  Scenario: Delete the LimitRange
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/limit_range"
    And I click "Refresh"
    And I should see the link "{{ limitrange_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ limitrange_name }}"
    And the url should match "/limit_range/"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    And I should be on "/clouds/k8s/{{ cloud_context }}/limit_range"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ limitrange_name }}"
