@minimal @ci_job
Feature: Create and delete a ConfigMap for K8s as "Authenticated User"

  @api
  Scenario: Create a ConfigMap
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/config_map/add"
    And I should see the heading "Add ConfigMap"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: {{ configmap_name }}
    data:
      key: {{ configmap_value }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/config_map"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ configmap_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the ConfigMap
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/config_map"
    And I click "Refresh"
    And I should see the link "{{ configmap_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ configmap_name }}"
    Then the url should match "/config_map/"
    And I should see "{{ configmap_name }}" in the "page_header"
    And I should see "{{ configmap_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the ConfigMap
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/config_map"
    And I click "Refresh"
    And I should see the link "{{ configmap_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ configmap_name }}"
    Then the url should match "/config_map/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: {{ configmap_name }}
    data:
      key: {{ configmap_value }}
      env: {{ configmap_value_add }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/config_map"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ configmap_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/config_map/"
    And I should see "{{ configmap_value_add }}"

  @api
  Scenario: Delete the ConfigMap
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/config_map"
    And I click "Refresh"
    And I should see the link "{{ configmap_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ configmap_name }}"
    And the url should match "/config_map/"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    And I should be on "/clouds/k8s/{{ cloud_context }}/config_map"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ configmap_name }}"
