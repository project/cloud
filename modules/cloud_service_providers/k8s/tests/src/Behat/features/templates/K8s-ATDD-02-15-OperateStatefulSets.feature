@minimal @ci_job
Feature: Create, read, update, and delete a StatefulSet for K8s as "Authenticated User"

  @api
  Scenario: Create a StatefulSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/stateful_set/add"
    And I should see the heading "Add StatefulSet"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    # This manifest works only on EKS clusters.
    """
    apiVersion: apps/v1
    kind: StatefulSet
    metadata:
      name: {{ statefulset_name }}
    spec:
      serviceName: {{ service_name }}
      replicas: {{ replica_count }}
      selector:
        matchLabels:
          app: {{ app_name }}
      template:
        metadata:
          labels:
            app: {{ app_name }}
        spec:
          terminationGracePeriodSeconds: 0
          containers:
          - name: {{ container_name }}
            image: {{ image_version }}
            ports:
            - containerPort: {{ port }}
            volumeMounts:
            - name: {{ persistent_volume_claim_name }}
              mountPath: {{ volume_mount_path }}
      volumeClaimTemplates:
      - metadata:
          name: {{ persistent_volume_claim_name }}
        spec:
          storageClassName: gp2
          accessModes:
          - ReadWriteOnce
          resources:
            requests:
              storage: {{ volume_capacity }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/stateful_set"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ statefulset_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the StatefulSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/stateful_set"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ statefulset_name }}"
    And I click "{{ statefulset_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/stateful_set/"
    And I should see "{{ statefulset_name }}" in the "page_header"
    And I should see "{{ statefulset_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the StatefulSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/stateful_set"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ statefulset_name }}"
    And I click "{{ statefulset_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/stateful_set/"
    And I click "Edit"
    And I fill in "Detail" with:
    # This manifest works only on EKS clusters.
    """
    apiVersion: apps/v1
    kind: StatefulSet
    metadata:
      name: {{ statefulset_name }}
      labels:
        app: {{ app_name }}
    spec:
      serviceName: {{ service_name }}
      replicas: {{ replica_count }}
      selector:
        matchLabels:
          app: {{ app_name }}
      template:
        metadata:
          labels:
            app: {{ app_name }}
        spec:
          terminationGracePeriodSeconds: 0
          containers:
          - name: {{ container_name }}
            image: {{ image_version }}
            ports:
            - containerPort: {{ port }}
            volumeMounts:
            - name: {{ persistent_volume_claim_name }}
              mountPath: {{ volume_mount_path }}
      volumeClaimTemplates:
      - metadata:
          name: {{ persistent_volume_claim_name }}
        spec:
          storageClassName: gp2
          accessModes:
          - ReadWriteOnce
          resources:
            requests:
              storage: {{ volume_capacity }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/stateful_set"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ statefulset_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/stateful_set/"
    And I should see "{{ statefulset_name }}"
    And I should see "{{ app_name }}"

  @api
  Scenario: Delete the StatefulSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/stateful_set"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ statefulset_name }}"
    And I click "{{ statefulset_name }}"
    Then the url should match "/k8s/{{ cloud_context }}/stateful_set/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/stateful_set"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ statefulset_name }}"

  @api @javascript
  Scenario: Fail to clean up the remaining persistent volumes claim
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/persistent_volume_claim"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I select "{{ namespace }}" from "Namespace"
    And I press "Apply"
    And I wait {{ wait }} milliseconds
    # Without @javascript, the following step fails.
    And I should see the link "{{ persistent_volume_claim_name }}-{{ statefulset_name }}-0"
    And I click "{{ persistent_volume_claim_name }}-{{ statefulset_name }}-0"
    Then the url should match "/k8s/{{ cloud_context }}/persistent_volume_claim/"
    And I should see "Access denied"
    And I should not see "Delete"

  @api @javascript
  Scenario: Clean up the remaining persistent volumes claim
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/k8s/{{ cloud_context }}/persistent_volume_claim"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    # Delete all persistent volumes claims belonging to {{ namespace }}
    And I select "{{ namespace }}" from "Namespace"
    And I press "Apply"
    And I wait {{ wait }} milliseconds
    # The following step requires @javascript.
    And I check the box in the "Operations links" row
    And I select "Delete persistent volume claim(s)" from "Action"
    And I press "Apply to selected items"
    And the url should match "/delete_multiple"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/persistent_volume_claim"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    # Confirm that there are no persistent volumes claims in {{ namespace }}.
    And I should not see the link "{{ namespace }}"
