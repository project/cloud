@minimal @ci_job
Feature: Create, read, update and delete an ingress for K8s as "Authenticated User"

  @api
  Scenario: Create an ingress
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/ingress/add"
    And I should see the heading "Add Ingress"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: {{ ingress_name }}
      annotations:
        nginx.ingress.kubernetes.io/rewrite-target: /
    spec:
      ingressClassName: {{ ingress_class_name }}
      rules:
      - http:
          paths:
          - path: /testpath
            pathType: Prefix
            backend:
              service:
                name: {{ service_name }}
                port:
                  number: {{ port }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/ingress"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ ingress_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the ingress
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/ingress"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ ingress_name }}"
    And I click "{{ ingress_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/ingress/"
    And I should see "{{ ingress_name }}" in the "page_header"
    And I should see "{{ ingress_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the ingress
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/ingress"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ ingress_name }}"
    And I click "{{ ingress_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/ingress/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: {{ ingress_name }}
      labels:
        app: {{ app_name }}
      annotations:
        nginx.ingress.kubernetes.io/rewrite-target: /
    spec:
      ingressClassName: {{ ingress_class_name }}
      rules:
      - http:
          paths:
          - path: /testpath
            pathType: Prefix
            backend:
              service:
                name: {{ service_name }}
                port:
                  number: {{ port }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/ingress"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ ingress_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/ingress/"
    And I should see "{{ app_name }}"

  @api
  Scenario: Delete the ingress
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/ingress"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ ingress_name }}"
    And I click "{{ ingress_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/ingress/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/ingress"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ ingress_name }}"
