@minimal @ci_job
Feature: Create, read, update and delete a DaemonSet for K8s as "Authenticated User"

  @api
  Scenario: Create a DaemonSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/daemon_set/add"
    And I should see the heading "Add DaemonSet"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: apps/v1
    kind: DaemonSet
    metadata:
      name: {{ daemonset_name }}
    spec:
      selector:
        matchLabels:
          name: {{ app_name }}
      template:
        metadata:
          labels:
            name: {{ app_name }}
        spec:
          tolerations:
          # these tolerations are to have the daemonset runnable on control plane nodes
          # remove them if your control plane nodes should not run pods
          - key: node-role.kubernetes.io/control-plane
            operator: Exists
            effect: NoSchedule
          - key: node-role.kubernetes.io/master
            operator: Exists
            effect: NoSchedule
          containers:
          - name: {{ container_name }}
            image: {{ image_version }}
            resources:
              limits:
                cpu: {{ max_cpu }}
              requests:
                cpu: {{ min_cpu }}
            volumeMounts:
            - name: www
              mountPath: {{ volume_mount_path }}
          terminationGracePeriodSeconds: 30
          volumes:
          - name: www
            hostPath:
              path: {{ volume_local_path }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/daemon_set"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ daemonset_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the DaemonSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/daemon_set"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ daemonset_name }}"
    And I click "{{ daemonset_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/daemon_set/"
    And I should see "{{ daemonset_name }}" in the "page_header"
    And I should see "{{ daemonset_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the DaemonSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/daemon_set"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ daemonset_name }}"
    And I click "{{ daemonset_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/daemon_set/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: apps/v1
    kind: DaemonSet
    metadata:
      name: {{ daemonset_name }}
      labels:
        k8s-app: {{ app_name }}
    spec:
      selector:
        matchLabels:
          name: {{ app_name }}
      template:
        metadata:
          labels:
            name: {{ app_name }}
        spec:
          tolerations:
          # these tolerations are to have the daemonset runnable on control plane nodes
          # remove them if your control plane nodes should not run pods
          - key: node-role.kubernetes.io/control-plane
            operator: Exists
            effect: NoSchedule
          - key: node-role.kubernetes.io/master
            operator: Exists
            effect: NoSchedule
          containers:
          - name: {{ container_name }}
            image: {{ image_version }}
            resources:
              limits:
                cpu: {{ max_cpu }}
              requests:
                cpu: {{ min_cpu }}
            volumeMounts:
            - name: www
              mountPath: {{ volume_mount_path }}
          terminationGracePeriodSeconds: 30
          volumes:
          - name: www
            hostPath:
              path: {{ volume_local_path }}
    """
    And I press "Save"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/daemon_set"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ daemonset_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/daemon_set/"
    And I should see "{{ app_name }}"

  @api
  Scenario: Delete the DaemonSet
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/daemon_set"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ daemonset_name }}"
    And I click "{{ daemonset_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/daemon_set/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/daemon_set"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ daemonset_name }}"
