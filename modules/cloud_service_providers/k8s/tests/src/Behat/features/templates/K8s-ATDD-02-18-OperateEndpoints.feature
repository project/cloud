@minimal @ci_job
Feature: Create, read, update and delete an endpoint for K8s as "Authenticated User"

  @api @javascript
  Scenario: Create an endpoint
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/endpoint/add"
    And I should see the heading "Add Endpoint"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: Endpoints
    metadata:
      name: {{ endpoint_name }}
    subsets:
      - addresses:
          - ip: {{ endpoint_ip }}
        ports:
          - port: {{ targetPort }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/endpoint"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ endpoint_name }}" in the "{{ namespace }}" row

  @api @javascript
  Scenario: Read the endpoint
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/endpoint"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ endpoint_name }}"
    And I click "{{ endpoint_name }}"
    Then the url should match "clouds/k8s/{{ cloud_context }}/endpoint/"
    And I should see "{{ endpoint_name }}" in the "page_header"
    And I should see "{{ endpoint_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I press "Others"
    And I should see "{{ user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Update the endpoint
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/endpoint"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ endpoint_name }}"
    And I click "{{ endpoint_name }}"
    And the url should match "clouds/k8s/{{ cloud_context }}/endpoint/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: Endpoints
    metadata:
      name: {{ endpoint_name }}
    subsets:
      - addresses:
          - ip: {{ endpoint_ip_edit }}
        ports:
          - port: {{ targetPort }}
    """
    And I press "Save"
    Then the url should match "clouds/k8s/{{ cloud_context }}/endpoint"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ endpoint_name }}"
    Then the url should match "clouds/k8s/{{ cloud_context }}/endpoint/"
    And I should see "{{ endpoint_ip_edit }}"

  @api @javascript
  Scenario: Delete the endpoint
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/endpoint"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ endpoint_name }}"
    And I click "{{ endpoint_name }}"
    Then the url should match "clouds/k8s/{{ cloud_context }}/endpoint/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/endpoint"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ endpoint_name }}"
