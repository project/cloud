@minimal @ci_job
Feature: Create, read, update and delete a secret for K8s as "Authenticated User"

  @api
  Scenario: Create a secret
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/secret/add"
    And I should see the heading "Add Secret"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: Secret
    metadata:
      name: {{ secret_name }}
    data:
      username: {{ secret_key }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/secret"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ secret_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the secret
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/secret"
    And I click "Refresh"
    And I should see the link "{{ secret_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ secret_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/secret/"
    And I should see "{{ secret_name }}" in the "page_header"
    And I should see "{{ secret_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the secret
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/secret"
    And I click "Refresh"
    And I should see the link "{{ secret_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ secret_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/secret/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: Secret
    metadata:
      name: {{ secret_name }}
    data:
      username: {{ secret_key }}
      password: {{ secret_value }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/secret"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ secret_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/secret/"
    And I should see "{{ secret_value }}"

  @api
  Scenario: Delete the secret
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/secret"
    And I click "Refresh"
    And I should see the link "{{ secret_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ secret_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/secret/"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/secret"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ secret_name }}"
