@minimal @ci_job
Feature: Read events generated along with a pod creation as "Authenticated User"

  @api
  Scenario: Generate events by creating a pod
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/pod/add"
    And I should see the heading "Add Pod"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: Pod
    metadata:
      name: {{ pod_name }}
    spec:
      containers:
      - name: {{ container_name }}
        image: {{ image_version }}
        ports:
        - containerPort: {{ port }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/pod"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ pod_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Generate events by deleting the pod
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/pod"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ pod_name }}"
    And I click "{{ pod_name }}"
    And the url should match "/k8s/{{ cloud_context }}/pod/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/pod"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I should see with "Running" in the "{{ pod_name }}" row, no rows, or no table

  @api
  Scenario: Refresh events as "Cloud administrator"
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/k8s/{{ cloud_context }}/event"
    # The refresh button is displayed for only administrators
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/event"
    And I should see the success message "Updated Events"
    And I should see neither error nor warning messages

  @api @wip
  Scenario: Read events generated along with pod operations
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/event"
    And I select "10" from "Items per page"
    And I press "Apply"
    And I should see the link "Created"
    And I click "Created"
    Then I should see "K8s Events"
    And I should see "Started container {{ container_name }}"
    And I should see "Created container {{ container_name }}"
    And I should see "Successfully assigned {{ namespace }}/{{ pod_name }} to "
    And I should see "Stopping container {{ container_name }}"
    And I should see neither error nor warning messages
