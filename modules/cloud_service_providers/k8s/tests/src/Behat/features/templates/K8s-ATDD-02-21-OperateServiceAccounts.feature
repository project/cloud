@minimal @ci_job
Feature: Create, read, update and delete a ServiceAccount for K8s as "Authenticated User"

  @api
  Scenario: Create a ServiceAccount
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/service_account/add"
    And I should see the heading "Add ServiceAccount"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: {{ serviceaccount_name }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/service_account"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ serviceaccount_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the ServiceAccount
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/service_account"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ serviceaccount_name }}"
    And I click "{{ serviceaccount_name }}"
    Then the url should match "/k8s/{{ cloud_context }}/service_account/"
    And I should see "{{ serviceaccount_name }}" in the "page_header"
    And I should see "{{ serviceaccount_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the ServiceAccount
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/service_account"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ serviceaccount_name }}"
    And I click "{{ serviceaccount_name }}"
    Then the url should match "clouds/k8s/{{ cloud_context }}/service_account/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: {{ serviceaccount_name }}
      labels:
        app: {{ app_name }}
    """
    And I press "Save"
    Then the url should match "clouds/k8s/{{ cloud_context }}/service_account"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ serviceaccount_name }}"
    And I should see "{{ app_name }}"

  @api
  Scenario: Delete the ServiceAccount
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/service_account"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ serviceaccount_name }}"
    And I click "{{ serviceaccount_name }}"
    Then the url should match "clouds/k8s/{{ cloud_context }}/service_account/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/service_account"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ serviceaccount_name }}"
