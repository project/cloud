@minimal @ci_job
Feature: Create and delete a role binding for K8s as "Authenticated User"

  @api
  Scenario: Create a role binding
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/role_binding/add"
    And I should see the heading "Add Role binding"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: rbac.authorization.k8s.io/v1
    kind: RoleBinding
    metadata:
      name: {{ role_binding_name }}
    subjects:
    - kind: {{ role_binding_sub_kind }}
      name: {{ role_binding_sub_user_edit }}
      apiGroup: {{ role_binding_api }}
    roleRef:
      kind: {{ role_binding_role_kind }}
      name: {{ role_name }}
      apiGroup: {{ role_binding_api }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/role_binding"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ role_binding_name }}" in the "{{ namespace }}" row

  @api
  Scenario: Read the role binding
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/role_binding"
    And I click "Refresh"
    And I should see the link "{{ role_binding_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ role_binding_name }}"
    And the url should match "/role_binding/"
    And I should see "{{ role_binding_name }}" in the "page_header"
    And I should see "{{ role_binding_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the role binding
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/role_binding"
    And I click "Refresh"
    And I should see the link "{{ role_binding_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ role_binding_name }}"
    And the url should match "/role_binding/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: rbac.authorization.k8s.io/v1
    kind: RoleBinding
    metadata:
      name: {{ role_binding_name }}
    subjects:
    - kind: {{ role_binding_sub_kind }}
      name: {{ role_binding_sub_user_edit }}
      apiGroup: {{ role_binding_api }}
    roleRef:
      kind: {{ role_binding_role_kind }}
      name: {{ role_name }}
      apiGroup: {{ role_binding_api }}
    """
    And I press "Save"
    And the url should match "/clouds/k8s/{{ cloud_context }}/role_binding"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ role_binding_name }}"
    And the url should match "/role_binding/"
    And I should see "{{ role_binding_sub_user_edit }}"

  @api
  Scenario: Delete the role binding
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/role_binding"
    And I click "Refresh"
    And I should see the link "{{ role_binding_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ role_binding_name }}"
    And the url should match "/role_binding/"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    And I should be on "/clouds/k8s/{{ cloud_context }}/role_binding"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ role_binding_name }}"
