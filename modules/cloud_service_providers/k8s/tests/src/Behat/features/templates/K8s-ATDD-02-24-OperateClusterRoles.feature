@minimal @ci_job
Feature: Create, read, update and delete a cluster role K8s as "Authenticated User"

  @api
  Scenario: Create a cluster role
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/cluster_role/add"
    And I fill in "Detail" with:
    """
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRole
    metadata:
      name: {{ cluster_role_name }}
    rules:
    - apiGroups: ["{{ role_api }}"]
      resources: ["{{ role_resource }}"]
      verbs: ["{{ role_verb }}"]
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/cluster_role"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ cluster_role_name }}"

  @api
  Scenario: Read the cluster role
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/cluster_role"
    And I click "Refresh"
    And I should see the link "{{ cluster_role_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ cluster_role_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/cluster_role/"
    And I should see "{{ cluster_role_name }}" in the "page_header"
    And I should see "{{ cluster_role_name }}" in the "details"
    And I should see "{{ role_resource }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the cluster role
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/cluster_role"
    And I click "Refresh"
    And I should see the link "{{ cluster_role_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ cluster_role_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/cluster_role/"
    And I click "Edit"
    And I fill in "Detail" with:
    """
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRole
    metadata:
      name: {{ cluster_role_name }}
    rules:
    - apiGroups: ["{{ role_api }}"]
      resources: ["{{ role_resource }}"]
      verbs: ["{{ role_verb_edit }}"]
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/cluster_role"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ cluster_role_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/cluster_role/"
    And I should see "{{ role_verb_edit }}"

  @api
  Scenario: Delete the cluster role
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/cluster_role"
    And I click "Refresh"
    And I should see the link "{{ cluster_role_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ cluster_role_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/cluster_role/"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/cluster_role"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ cluster_role_name }}"
