@minimal @ci_job @wip # Remove @wip tag once k8s API library supports v2.
Feature: Create, read, update and delete a horizontal pod autoscaler for K8s as "Authenticated User"

  @api
  Scenario: Create a horizontal pod autoscaler
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler/add"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: autoscaling/v1
    kind: HorizontalPodAutoscaler
    metadata:
      name: {{ horizontal_pod_autoscaler_name }}
    spec:
      maxReplicas: {{ horizontal_pod_autoscaler_replica_max }}
      minReplicas: {{ horizontal_pod_autoscaler_replica_min }}
      scaleTargetRef:
        apiVersion: {{ horizontal_pod_autoscaler_api }}
        kind: {{ horizontal_pod_autoscaler_kind }}
        name: {{ horizontal_pod_autoscaler_scale_name }}
      targetCPUUtilizationPercentage: {{ horizontal_pod_autoscaler_cpu }}
    """
    And I press "Save"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ namespace }}" in the "{{ horizontal_pod_autoscaler_name }}" row

  @api
  Scenario: Read the horizontal pod autoscaler
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler"
    And I click "Refresh"
    And I should see the link "{{ horizontal_pod_autoscaler_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ horizontal_pod_autoscaler_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler/"
    And I should see "{{ horizontal_pod_autoscaler_name }}" in the "page_header"
    And I should see "{{ horizontal_pod_autoscaler_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the horizontal pod autoscaler
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler"
    And I click "Refresh"
    And I should see the link "{{ horizontal_pod_autoscaler_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ horizontal_pod_autoscaler_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler/"
    And I click "Edit"
    """
    apiVersion: autoscaling/v1
    kind: HorizontalPodAutoscaler
    metadata:
      name: {{ horizontal_pod_autoscaler_name }}
    spec:
      maxReplicas: {{ horizontal_pod_autoscaler_replica_max_edit }}
      minReplicas: {{ horizontal_pod_autoscaler_replica_min }}
      scaleTargetRef:
        apiVersion: {{ horizontal_pod_autoscaler_api }}
        kind: {{ horizontal_pod_autoscaler_kind }}
        name: {{ horizontal_pod_autoscaler_scale_name }}
      targetCPUUtilizationPercentage: {{ horizontal_pod_autoscaler_cpu }}
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ horizontal_pod_autoscaler_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler/"
    And I should see "{{ horizontal_pod_autoscaler_replica_max_edit }}"

  @api
  Scenario: Delete the horizontal pod autoscaler
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler"
    And I click "Refresh"
    And I should see the link "{{ horizontal_pod_autoscaler_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ horizontal_pod_autoscaler_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler/"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/horizontal_pod_autoscaler"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ horizontal_pod_autoscaler_name }}"
