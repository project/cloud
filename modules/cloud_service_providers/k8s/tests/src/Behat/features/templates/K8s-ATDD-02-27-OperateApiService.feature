@minimal @ci_job
Feature: Create, read and delete a API service for K8s as "Authenticated User"

  @api
  Scenario: Create a API service
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/api_service/add"
    And I should see the heading "Add API service"
    And I fill in "Detail" with:
    """
    apiVersion: apiregistration.k8s.io/v1
    kind: APIService
    metadata:
      name: {{ api_service_name }}
    spec:
      insecureSkipTLSVerify: true
      group: default.example.com
      groupPriorityMinimum: 1000
      versionPriority: 15
      service:
        name: api
        namespace: default
      version: v1alpha1
    """
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/api_service"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ api_service_name }}"

  @api
  Scenario: Read the API service
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/api_service"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ api_service_name }}"
    And I click "{{ api_service_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/api_service/"
    And I should see "{{ api_service_name }}" in the "page_header"
    And I should see "{{ api_service_name }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Delete the API service
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/api_service"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ api_service_name }}"
    And I click "{{ api_service_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/api_service/"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/api_service"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ api_service_name }}"
