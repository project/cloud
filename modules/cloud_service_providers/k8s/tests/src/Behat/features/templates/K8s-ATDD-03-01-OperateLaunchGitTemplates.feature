@minimal @ci_job
Feature: Create, edit, and launch a launch template for K8s as "Authenticated User"
  and approve it as "Cloud administrator"

  @api @javascript
  Scenario: Create a Launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "clouds/design/server_template/{{ cloud_context }}/k8s/add"
    And I enter "{{ launch_git_name }}" for "Name"
    And I select "{{ namespace }}" from "Namespace"
    And I click "GIT"
    And I should see "Configuration"
    And I should see "Credentials"
    # Enter credentials first, otherwise the form is somehow not found by Behat.
    And I enter "{{ launch_git_credentials_user }}" for "Git username"
    And I enter "{{ launch_git_credentials_pwd }}" for "Git password"
    And I wait for AJAX to finish
    And I should see "Source"
    And I select "{{ launch_git_source_type }}" from "Source type"
    And I wait for AJAX to finish
    And I enter "{{ launch_git_repository }}" for "Git repository"
    # Hidden the Source config to trigger to connect the Git repository above.
    And I press "Source"
    And I wait for AJAX to finish
    # Unhidden the Source config.
    And I press "Source"
    And I should see "Branch"
    And I select "{{ launch_git_branch_or_tag }}" from "Git branch or tag"
    And I enter "{{ launch_git_resource_path }}" for "Git resource path"
    And I select "Draft" from "Workflow status"
    And I press "Save"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "{{ launch_git_name }} has been created"
    And I should see neither error nor warning messages

  @api
  Scenario: View Launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "{{ launch_git_name }}"
    Then I should not see "Launch" in the "nav_tabs"
    And I should not see "Approve"
    And I click "View"
    Then I should see "{{ cloud_service_provider_name }}"
    And I should see "{{ namespace }}"
    And I should see "{{ launch_git_source_type }}"
    And I should see "{{ launch_git_repository }}"
    And I should see "{{ launch_git_branch_or_tag }}"
    And I should see "{{ launch_git_resource_path }}"
    And I should see "{{ user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Edit launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Edit" in the "{{ launch_git_name }}" row
    And I should see "Configuration"
    And I should see "Credentials"
    # Enter credentials first, otherwise the form is somehow not found by Behat.
    And I enter "{{ launch_git_credentials_user_edit }}" for "{{ launch_git_credentials_user_edit_label }}"
    And I enter "{{ launch_git_credentials_pwd_edit }}" for "{{ launch_git_credentials_pwd_edit_label }}"
    And I select "{{ launch_git_source_type_edit }}" from "Source type"
    And I wait for AJAX to finish
    And I enter "{{ launch_git_repository_edit }}" for "Git repository"
    # Hidden the Source config to trigger to connect the Git repository above.
    And I press "Source"
    And I wait for AJAX to finish
    # Unhidden the Source config.
    And I press "Source"
    And I select "{{ launch_git_branch_or_tag_edit }}" from "Git branch or tag"
    And I should see "Git resource path"
    # For editing, the resource path table has multiple rows.
    And I fill in "{{ launch_git_resource_path_edit_field }}" with "{{ launch_git_resource_path_edit }}"
    And I press "Save"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages

  @api
  Scenario: View the updated launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "{{ launch_git_name }}"
    Then I should not see "Launch" in the "nav_tabs"
    And I should not see "Approve"
    And I click "View"
    Then I should see "{{ cloud_service_provider_name }}"
    And I should see "{{ namespace }}"
    And I should see "{{ launch_git_source_type_edit }}"
    And I should see "{{ launch_git_repository_edit}}"
    And I should see "{{ launch_git_branch_or_tag_edit }}"
    And I should see "{{ launch_git_resource_path_edit }}"

  @api
  Scenario: Approve the launch template
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Edit" in the "{{ launch_git_name }}" row
    And the url should match "/edit"
    And I should see "Launch" in the "nav_tabs"
    And I select "{{ namespace }}" from "Namespace"
    And I select "Approved" from "Workflow status"
    And I press "Save"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "{{ launch_git_name }} has been updated"
    And I should see neither error nor warning messages

  @api
  Scenario: Launch a Launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "{{ launch_git_name }}"
    And I click "Launch"
    And the url should match "/launch"
    And I press "Launch"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "{{ launch_git_pod1 }} has been launched"
    And I should see neither error nor warning messages

  @api
  Scenario: Check Launch template deployment
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/deployment"
    And I should see "{{ namespace }}" in the "{{ launch_git_deployment1 }}" row
    And I should see "{{ namespace }}" in the "{{ launch_git_deployment2 }}" row

  @api
  Scenario: Check Launch template pod
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/pod"
    And I should see "{{ namespace }}" in the "{{ launch_git_pod1 }}" row

  @api
  Scenario: Delete a Launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Edit" in the "{{ launch_git_name }}" row
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "{{ launch_git_deployment1 }} (Namespace: {{ namespace }}) on {{ cloud_context }} has been deleted"
    And I should see the success message "{{ launch_git_deployment2 }} (Namespace: {{ namespace }}) on {{ cloud_context }} has been deleted"
    And I should see the success message "{{ launch_git_pod1 }} (Namespace: {{ namespace }}) on {{ cloud_context }} has been deleted"
    And I should see the success message "{{ launch_git_name }} has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I click "Refresh"
    And I should not see the link "{{ launch_git_name }}"

  @api
  Scenario: Check if the Deployment has been deleted
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/deployment"
    Then I should not see "{{ launch_git_deployment1 }}"
    And I should not see "{{ launch_git_deployment2 }}"

  @api
  Scenario: Check if the Pod has been deleted
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/pod"
    Then I wait {{ wait_deleted }} milliseconds
    And I click "Refresh"
    And I should see with "Running" in the "{{ launch_git_pod1 }}" row, no rows, or no table
