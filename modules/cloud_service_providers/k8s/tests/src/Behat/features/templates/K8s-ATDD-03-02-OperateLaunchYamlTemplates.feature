@minimal @ci_job
Feature: Create and launch a launch template for K8s as "Authenticated User" and
  approve it as "Cloud administrator"

  @api @javascript
  Scenario: Create a Launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "clouds/design/server_template/{{ cloud_context }}/k8s/add"
    And I enter "{{ launch_yaml_name }}" for "Name"
    And I select "{{ namespace }}" from "Namespace"
    And I click "YAML"
    And I enter "{{ launch_yaml_repository }}" for "YAML URL"
    And I select "Draft" from "Workflow status"
    And I press "Save"
    And I wait for AJAX to finish
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "{{ launch_yaml_name }} has been created"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: View Launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "{{ launch_yaml_name }}"
    Then I should not see "Launch" in the "nav_tabs"
    And I should not see "Approve"
    And I click "View"
    Then I should see "{{ cloud_context  }}"
    And I should see "{{ namespace  }}"
    And I should see "pod"
    # The following step requires @javascript.
    And I press "Configuration"
    And I should see "YAML"
    And I press "Others"
    And I should see "{{ user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Edit launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Edit" in the "{{ launch_yaml_name }}" row
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: Pod
    metadata:
      name: {{ launch_yaml_pod_edit }}
      labels:
        app: {{ launch_yaml_pod_edit }}
    spec:
      containers:
      - name: {{ launch_yaml_pod_edit }}
        image: {{ launch_yaml_image }}
        command: {{ launch_yaml_command|raw }}
        imagePullPolicy: {{ launch_yaml_imagepull }}
      restartPolicy: {{ launch_yaml_restart }}

      """
    And I press "Save"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "{{ launch_yaml_name }} has been updated"
    And I should see neither error nor warning messages

  @api
  Scenario: View the updated launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "{{ launch_yaml_name }}"
    Then I should not see "Launch" in the "nav_tabs"
    And I should not see "Approve"
    And I click "View"
    Then I should see "{{ cloud_context }}"
    And I should see "{{ namespace }}"
    And I should see "{{ launch_yaml_pod_edit }}"

  @api
  Scenario: Approve the launch template
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Edit" in the "{{ launch_yaml_name }}" row
    And the url should match "/edit"
    And I should see "Launch" in the "nav_tabs"
    And I select "{{ namespace }}" from "Namespace"
    And I select "Approved" from "Workflow status"
    And I press "Save"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "{{ launch_yaml_name }} has been updated"
    And I should see neither error nor warning messages

  @api
  Scenario: Launch a Launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "{{ launch_yaml_name }}"
    And I click "Launch"
    And the url should match "/launch"
    And I press "Launch"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/pod"
    And I should see the success message "{{ launch_yaml_pod_edit }} has been launched"
    And I should see neither error nor warning messages

  @api
  Scenario: Delete a Launch template
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Edit" in the "{{ launch_yaml_name }}" row
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "{{ launch_yaml_pod_edit }} (Namespace: {{ namespace }}) on {{ cloud_context }} has been deleted"
    And I should see the success message "{{ launch_yaml_name }} has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I click "Refresh"
    And I should not see the link "{{ launch_yaml_name }}"

  @api
  Scenario: Check if the Pod has been deleted
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/pod"
    Then I wait {{ wait_deleted }} milliseconds
    And I click "Refresh"
    And I should not see "{{ launch_yaml_pod_edit }}"
