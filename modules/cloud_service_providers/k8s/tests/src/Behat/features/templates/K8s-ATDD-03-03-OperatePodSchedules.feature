@minimal @ci_job
Feature: Create and delete a pod for K8s as "Authenticated User"

  @api
  Scenario: Create a Pod Schedules
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/pod/add"
    And I should see the heading "Add Pod"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: v1
    kind: Pod
    metadata:
      name: {{ pod_name }}
    spec:
      containers:
      - name: {{ container_name }}
        image: {{ image_version }}
        ports:
        - containerPort: {{ port }}
    """
    And I check the box "Enable time scheduler"
    And I select "{{ start_elapsed_minute }}" minutes later now from the start-up time
    And I select "{{ stop_elapsed_minute }}" minutes later now from the stop time
    When I press "Save"
    And the url should match "/clouds/k8s/{{ cloud_context }}/pod"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: View the scheduled Pod starts
    Given I am logged in as user "{{ user_name }}"
    # Visit schedule edit view to see start-up time.
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the link "{{ namespace }}_{{ pod_name }}"
    And I click "{{ namespace }}_{{ pod_name }}"
    And I click "Edit"
    And I wait until start-up time for schedule
    And I visit "/clouds/k8s/{{ cloud_context }}/pod"
    And I run drush cron
    And I click "Refresh"
    # The following step requires @javascript.
    Then I should see "{{ pod_name }}" in the "{{ namespace }}" row
    And I should see the link "{{ pod_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ pod_name }}"
    And I press "Others"
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: View the scheduled Pod stops
    Given I am logged in as user "{{ user_name }}"
    # Visit schedule edit view to see stop time.
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the link "{{ namespace }}_{{ pod_name }}"
    And I click "{{ namespace }}_{{ pod_name }}"
    And I click "Edit"
    And I wait until stop time for schedule
    And I visit "/clouds/k8s/{{ cloud_context }}/pod"
    And I run drush cron
    And I click "Refresh"
    Then I should not see the link "{{ pod_name }}"

  @api
  Scenario: Read the Pod Schedules
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see "{{ namespace }}_{{ pod_name }}" in the "{{ namespace }}" row
    And I click "{{ namespace }}_{{ pod_name }}"
    And the url should match "clouds/k8s/{{ cloud_context }}/schedule/"
    And I should see "{{ pod_name }}" in the "page_header"
    And I should see "{{ pod_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: Update the Pod Schedules
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the link "{{ namespace }}_{{ pod_name }}"
    And I click "{{ namespace }}_{{ pod_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/schedule/"
    And I click "Edit"
    And I select "{{ start_elapsed_minute }}" minutes later now from the start-up time
    And I select "{{ stop_elapsed_minute }}" minutes later now from the stop time
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ namespace }}_{{ pod_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/schedule/"
    # Visit schedule edit view to see start-up time.
    And I click "Edit"
    When I wait until start-up time for schedule
    And I visit "/clouds/k8s/{{ cloud_context }}/pod"
    Given I run drush cron
    Then I click "Refresh"
    And I should see "{{ pod_name }}" in the "{{ namespace }}" row
    # Visit schedule edit view to see stop time.
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    Then I should see the link "{{ namespace }}_{{ pod_name }}"
    And I click "{{ namespace }}_{{ pod_name }}"
    And I click "Edit"
    When I wait until stop time for schedule
    And I visit "/clouds/k8s/{{ cloud_context }}/pod"
    Given I run drush cron
    Then I click "Refresh"
    And I should not see the link "{{ pod_name }}"

  @api @javascript
  Scenario: Delete the Pod Schedules
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the link "{{ namespace }}_{{ pod_name }}"
    And I click "{{ namespace }}_{{ pod_name }}"
    And the url should match "/k8s/{{ cloud_context }}/schedule/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
