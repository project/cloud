@minimal @ci_job
Feature: Create and delete a deployment for K8s as "Authenticated User"

  @api
  Scenario: Create a Deployment Schedules
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/deployment/add"
    And I should see the heading "Add Deployment"
    And I select "{{ namespace }}" from "Namespace"
    And I fill in "Detail" with:
    """
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: {{ deployment_name }}
      labels:
        app: {{ app_name }}
    spec:
      replicas: {{ replica_count }}
      selector:
        matchLabels:
          app: {{ app_name }}
      template:
        metadata:
          labels:
            app: {{ app_name }}
        spec:
          containers:
          - name: {{ container_name }}
            image: {{ image_version }}
            ports:
            - containerPort: {{ port }}
    """
    And I check the box "Enable time scheduler"
    And I select "{{ start_elapsed_minute }}" minutes later now from the start-up time
    And I select "{{ stop_elapsed_minute }}" minutes later now from the stop time
    When I press "Save"
    And the url should match "/clouds/k8s/{{ cloud_context }}/deployment"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: View the scheduled Deployment starts
    Given I am logged in as user "{{ user_name }}"
    # Visit schedule edit view to see start-up time.
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the link "{{ namespace }}_{{ deployment_name }}"
    And I click "{{ namespace }}_{{ deployment_name }}"
    And I click "Edit"
    And I wait until start-up time for schedule
    And I visit "/clouds/k8s/{{ cloud_context }}/deployment"
    And I run drush cron
    And I click "Refresh"
    # The following step requires @javascript.
    Then I should see "{{ deployment_name }}" in the "{{ namespace }}" row
    And I should see the link "{{ deployment_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ deployment_name }}"
    And I press "Others"
    And I should see "{{ user_name }}" in the "Authored by"

  @api
  Scenario: View the scheduled Deployment stops
    Given I am logged in as user "{{ user_name }}"
    # Visit schedule edit view to see stop time.
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the link "{{ namespace }}_{{ deployment_name }}"
    And I click "{{ namespace }}_{{ deployment_name }}"
    And I click "Edit"
    And I wait until stop time for schedule
    And I visit "/clouds/k8s/{{ cloud_context }}/deployment"
    And I run drush cron
    And I click "Refresh"
    Then I should not see the link "{{ deployment_name }}"

  @api
  Scenario: Read the Deployment Schedules
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see "{{ namespace }}_{{ deployment_name }}" in the "{{ namespace }}" row
    And I click "{{ namespace }}_{{ deployment_name }}"
    And the url should match "clouds/k8s/{{ cloud_context }}/schedule/"
    And I should see "{{ deployment_name }}" in the "page_header"
    And I should see "{{ deployment_name }}" in the "details"
    And I should see "{{ namespace }}" in the "details"
    And I should see neither error nor warning messages
    And I should see "{{ user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Update the Deployment Schedules
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the link "{{ namespace }}_{{ deployment_name }}"
    And I click "{{ namespace }}_{{ deployment_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/schedule/"
    And I click "Edit"
    And I select "{{ start_elapsed_minute }}" minutes later now from the start-up time
    And I select "{{ stop_elapsed_minute }}" minutes later now from the stop time
    And I press "Save"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ namespace }}_{{ deployment_name }}"
    Then the url should match "/clouds/k8s/{{ cloud_context }}/schedule/"
    # Visit schedule edit view to see start-up time.
    And I click "Edit"
    When I wait until start-up time for schedule
    And I visit "/clouds/k8s/{{ cloud_context }}/deployment"
    Given I run drush cron
    Then I click "Refresh"
    And I should see "{{ deployment_name }}" in the "{{ namespace }}" row
    # Visit schedule edit view to see stop time.
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    Then I should see the link "{{ namespace }}_{{ deployment_name }}"
    And I click "{{ namespace }}_{{ deployment_name }}"
    And I click "Edit"
    When I wait until stop time for schedule
    And I visit "/clouds/k8s/{{ cloud_context }}/deployment"
    Given I run drush cron
    Then I click "Refresh"
    And I should not see the link "{{ deployment_name }}"

  @api @javascript
  Scenario: Delete the Deployment Schedules
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the link "{{ namespace }}_{{ deployment_name }}"
    And I click "{{ namespace }}_{{ deployment_name }}"
    And the url should match "/k8s/{{ cloud_context }}/schedule/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/schedule"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
