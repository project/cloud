@ci_job
Feature: Confirm "Authenticated user" cannot see resources or Refresh button
  as the user by default has no access to any cloud service providers

  @api
  Scenario Outline: Cannot refresh the list
    Given I am logged in as a user with the "Authenticated user" role
    When I visit "/clouds/k8s/<resource_type>"
    Then I should not see the link "Refresh"
    And I should see neither error nor warning messages

    Examples:
      | resource_type             |
      | node                      |
      | event                     |
      | namespace                 |
      | deployment                |
      | pod                       |
      | replica_set               |
      | cron_job                  |
      | job                       |
      | service                   |
      | network_policy            |
      | resource_quota            |
      | limit_range               |
      | priority_class            |
      | config_map                |
      | secret                    |
      | role                      |
      | role_binding              |
      | cluster_role              |
      | cluster_role_binding      |
      | persistent_volume         |
      | persistent_volume_claim   |
      | storage_class             |
      | stateful_set              |
      | ingress                   |
      | daemon_set                |
      | endpoint                  |
      | api_service               |
      | service_account           |
      | horizontal_pod_autoscaler |
