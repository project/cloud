@minimal @ci_job @cleanup
Feature: Delete the created namespace for K8s as "Authenticated User"

  @api
  Scenario: Delete the created namespace
    Given I am logged in as user "{{ user_name }}"
    When I visit "/clouds/k8s/{{ cloud_context }}/namespace"
    And I should see the heading "K8s namespaces"
    And I click "Refresh"
    And I should see the link "{{ namespace }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ namespace }}"
    And the url should match "/namespace/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/k8s/{{ cloud_context }}/namespace"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should see the link "{{ namespace }}" in the table
    And I should see the text "Terminating" in the "{{ namespace }}" row
