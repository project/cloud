@minimal @ci_job @cleanup
Feature: Delete the created Drupal resources as "Administrator"

  @api
  Scenario: Delete the created role
    Given I am logged in as a user with the "Administrator" role
    And I visit "/admin/people/roles"
    And I should see "{{ drupal_role_name }}"
    When I visit "/admin/people/roles/manage/{{ drupal_role_name_machine }}/delete?destination=/admin/people/roles"
    And I press "Delete"
    Then I should be on "/admin/people/roles"
    And I should see the success message "has been deleted."
    And I should see neither error nor warning messages
    And I should not see the link "{{ drupal_role_name }}"

  @api @javascript
  Scenario: Delete the created user
    Given I am logged in as a user with the "Administrator" role
    And I visit "/admin/people"
    And I should see "{{ user_name }}"
    When I check the box "{{ user_name }}"
    And I select "Cancel the selected user account(s)" from "Action"
    And I press "Apply to selected items"
    # The message of the option is altered by https://www.drupal.org/i/3313552.
    And I select the radio button "Delete the account and its content. Entities provided by Cloud modules are reassigned to Anonymous user. This action cannot be undone."
    And I press "Confirm"
    And I wait for the batch job to finish
    Then I should see the success message "has been deleted."
    And I should see neither error nor warning messages
    And I should not see the link "{{ user_name }}" in the table
