#!/bin/bash
#
# Clean up K8s resources.
#
readonly ALL=(
  'apiservices'
  'clusterrolebindings'
  'clusterroles'
  'configmaps'
  'cronjobs'
  'daemonsets'
  'deployments'
  'endpoints'
  'horizontalpodautoscalers'
  'ingresses'
  'jobs'
  'limitranges'
  'networkpolicies'
  'persistentvolumeclaims'
  'persistentvolumes'
  'pods'
  'priorityclasses'
  'replicasets'
  'resourcequotas'
  'roles'
  'rolebindings'
  'secrets'
  'serviceaccounts'
  'services'
  'statefulsets'
  'storageclasses'
  'namespace'
)

function usage() {
  cat <<EOF
Usage: $0 -n name_pattern [OPTION]

Example: $0 -n bdd-.*-random

OPTION:

  --dry-run | --no-dry-run (boolean)
    dry-run option set to kubectl delete commands. [default: --dry-run]

  -h, --help:               Show this usage.
  -n, --name:               Name pattern of K8s resources to clean up. Required.
  -c, --context:            K8s context to be used by kubectl.
                            [default: current context]
  -s, --namespace           K8s namespace contains target resources.
                            [default: default]
  -t, --target:             Target resources to clean up.
                            [default: ${ALL[@]}]
EOF
  exit 1
}

function main() {
  while [[ "x$1" != 'x' ]]; do
    case "$1" in
    --dry-run | --no-dry-run) dry_run_delete="$1"; shift;;
    -n | --name) shift; val_reg_exp="$1"; shift;;
    -c | --context) shift; context="$1"; shift;;
    -s | --namespace) shift; namespace="$1"; shift;;
    -t | --target) shift;
     while [[ ! $1 =~ '-' ]]; do targets+=($1); shift; done;;
    *) usage ;;
    esac
  done
  if [[ -z "${val_reg_exp}" ]]; then
    echo 'Missing name pattern.'
    usage
  fi
}

count=0
function echo_count() {
  echo
  echo "($((++count))/${TOTAL}) $1"
}

function run_cmd() {
  local cmd="$@"
  info "Executing ${cmd}"
  if ! result=$(eval ${cmd}); then
    err 'Failed.'
    return 1
  fi
  echo "${result}"
}

function info() {
  echo "  $*" >&2
}

function err() {
  echo "  [Error] $*" >&2
}

function error_if_command_not_found() {
  local cmd="${1}"
  if ! (command -v "${cmd}" &>/dev/null); then
    err "Command not found: ${cmd}"
    exit 1
  fi
}

function setup() {
  error_if_command_not_found 'kubectl'
  error_if_command_not_found 'jq'

  if [[ ! -v context ]]; then
    context="$(kubectl config current-context)"
  fi

  if [[ ! -v namespace ]]; then
    namespace='default'
  fi

  if [[ ! -v targets[@] ]]; then
    targets=("${ALL[@]}")
  fi
  # Add one for switching context
  readonly TOTAL=$(( 1+${#targets[@]} ))

  if [[ ! -v dry_run_delete ]]; then
    dry_run_delete='--dry-run'
  fi
}

function print_env() {
  cat << EOF
kubectl version:           $(kubectl version --client)
Dry-run option to delete:  ${dry_run_delete}
kubectl context:           ${context}
kubectl namespace:         ${namespace}
Name pattern of resources: ${val_reg_exp}
Resource types:            ${targets[@]}
EOF
}

function describe() {
  local resource_type="$1"

  local cmd="kubectl get ${resource_type} -n=${namespace} -o json \
| jq -r '.items[].metadata.name | select(.|test(\"^${val_reg_exp}\$\"))'"
  local results=$(run_cmd "${cmd}")

  if [[ "${results}" == 'None' ]] || [[ "${results}" == 'null' ]]; then
    err "Something went wrong. The returned value was ${results}."
    exit 1
  fi
  echo "${results}"
}

function delete() {
  local resource_type="$1"
  shift
  local resource_names="$@"

  if [[ "${dry_run_delete}" != '--no-dry-run' ]]; then
    dry_run_option='--dry-run=server'
  fi
  local cmd="kubectl delete ${resource_type} -n=${namespace} ${dry_run_option} ${resource_names}"
  run_cmd "${cmd}"
  cmd_status=$?

  if [[ -v dry_run_option ]]; then
    return 1
  else
    return "${cmd_status}"
  fi
}

## main
main "$@"
setup
echo '** Cleanup K8s resources'
print_env

echo_count "Scenario: Switch a K8s cluster context to '${context}'..."
if ! run_cmd "kubectl config use-context '${context}'"; then
  err "Failed to switch context to ${context}"
  exit 1
fi

for target in "${targets[@]}"; do
  echo_count "Scenario: Delete '${target}'..."
  results_to_delete=$(describe "${target}")
  if [[ -z "${results_to_delete}" ]]; then
    info 'Nothing to delete.'
    continue
  fi
  info 'Found the following resource(s) to delete:'
  info "${results_to_delete}"
  if ! delete "${target}" "${results_to_delete}"; then
    continue
  fi

  results_after_deletion=$(describe "${target}")
  if [[ -n "${results_after_deletion}" ]]; then
    err "Failed to delete or takes time to delete ${results_to_delete}."
    continue
  fi
  info 'Deleted the resources.'
done
