<?php

namespace Drupal\Tests\k8s\Functional\Config;

use Drupal\Tests\k8s\Functional\K8sTestBase;
use Drupal\Tests\k8s\Traits\K8sTestFormDataTrait;

/**
 * Test Case class for K8s Admin Setting forms.
 *
 * @group K8s
 */
class K8sAdminSettingsTest extends K8sTestBase {

  use K8sTestFormDataTrait;

  public const K8S_ADMIN_SETTINGS_REPEAT_COUNT = 2;

  /**
   * Get permissions of login user.
   *
   * @return array
   *   permissions of login user.
   */
  protected function getPermissions(): array {
    return [
      'administer k8s',
      'administer site configuration',
    ];
  }

  /**
   * Test for K8sAdminSettings.
   *
   * @param string $setting_type
   *   The setting type id.
   * @param array $edit
   *   The array of input data.
   */
  protected function runK8sAdminSettings($setting_type, array $edit): void {
    $this->drupalGet("/admin/config/services/cloud/k8s/{$setting_type}");
    $this->assertNoErrorMessage();

    $this->submitForm(
      $edit,
      $this->t('Save')->render()
    );
    $this->assertNoErrorMessage();
  }

  /**
   * Test K8s Admin Settings form on Notifications.
   */
  public function testK8sAdminNotificationsSettings(): void {

    $edit = $this->createK8sCloudNotificationsFormData(self::K8S_ADMIN_SETTINGS_REPEAT_COUNT);

    for ($i = 0; $i < self::K8S_ADMIN_SETTINGS_REPEAT_COUNT; $i++) {
      $edit[$i] = array_intersect_key($edit[$i], array_flip(
        (array) array_rand($edit[$i], random_int(1, count($edit[$i])))
      ));
      $this->runK8sAdminSettings('notifications', $edit[$i]);
    }
  }

  /**
   * Test K8s Admin Settings form on Settings.
   *
   * @throws \Exception
   */
  public function testK8sAdminSettings(): void {
    $edit = $this->createK8sCloudSettingsFormData(self::K8S_ADMIN_SETTINGS_REPEAT_COUNT);

    for ($i = 0; $i < self::K8S_ADMIN_SETTINGS_REPEAT_COUNT; $i++) {
      $edit[$i] = array_intersect_key($edit[$i], array_flip(
        (array) array_rand($edit[$i], random_int(1, count($edit[$i])))
      ));
      $this->runK8sAdminSettings('settings', $edit[$i]);
    }
  }

}
