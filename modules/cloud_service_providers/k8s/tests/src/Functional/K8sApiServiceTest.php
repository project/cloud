<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s API service.
 *
 * @group K8s
 */
class K8sApiServiceTest extends K8sTestBase {

  public const K8S_API_SERVICE_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list k8s api service',
      'add k8s api service',
      'edit any k8s api service',
      'delete any k8s api service',
      'view any k8s api service',
    ];
  }

  /**
   * Tests CRUD for API service.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testApiService(): void {

    $cloud_context = $this->cloudContext;

    // List API service for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/api_service");
    $this->assertNoErrorMessage();

    // Add a new API service.
    $add = $this->createApiServiceTestFormData(self::K8S_API_SERVICE_REPEAT_COUNT);

    for ($i = 0; $i < self::K8S_API_SERVICE_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addApiServiceMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/api_service/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'API service', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/api_service");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_API_SERVICE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all api_service listing exists.
      $this->drupalGet('/clouds/k8s/api_service');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/api_service/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit an API service.
    $edit = $this->createApiServiceTestFormData(self::K8S_API_SERVICE_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::K8S_API_SERVICE_REPEAT_COUNT; $i++, $num++) {

      $this->updateApiServiceMockData($edit[$i]);

      // unset($edit[$i]['post_data']['namespace']);.
      $this->drupalGet("/clouds/k8s/$cloud_context/api_service/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'API service', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/api_service/$num");
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );

    }

    // Delete API service.
    for ($i = 0, $num = 1; $i < self::K8S_API_SERVICE_REPEAT_COUNT; $i++, $num++) {

      $this->deleteApiServiceMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/api_service/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'API service', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/api_service");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting API response with bulk operation.
   *
   * @throws \Exception
   */
  public function testApiServiceBulk(): void {

    for ($i = 0; $i < self::K8S_API_SERVICE_REPEAT_COUNT; $i++) {
      // Create API service.
      $api_services = $this->createApiServicesRandomTestFormData();
      $entities = [];
      foreach ($api_services ?: [] as $api_service) {
        $entities[] = $this->createApiServiceTestEntity($api_service);
      }
      $this->deleteApiServiceMockData($api_services[0]);
      $this->runTestEntityBulk('api_service', $entities);
    }
  }

  /**
   * Test updating all API service list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllApiServiceList(): void {
    $cloud_configs = [];

    // List Api Service for K8s.
    $this->drupalGet('/clouds/k8s/api_service');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_API_SERVICE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Api Service.
      $add = $this->createApiServiceTestFormData(self::K8S_API_SERVICE_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_API_SERVICE_REPEAT_COUNT; $i++, $num++) {
        $this->reloadMockData();

        $this->addApiServiceMockData($add[$i], $cloud_context, $this->webUser->id());

        $this->drupalGet("/clouds/k8s/$cloud_context/api_service/add");
        $this->submitForm(
          $add[$i]['post_data'],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = ['@type' => 'API service', '%label' => $add[$i]['name']];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
      }
    }

    // List ApiService for K8s.
    $this->drupalGet('/clouds/k8s/api_service');
    $this->assertNoErrorMessage();

    // Make sure listing.
    for ($i = 0; $i < self::K8S_API_SERVICE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Api services',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Edit a Api Service.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createApiServiceTestFormData(self::K8S_API_SERVICE_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_API_SERVICE_REPEAT_COUNT; $i++, $num++) {

        $this->updateApiServiceMockData($edit[$i]);

        // Change Api service name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/api_service');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_API_SERVICE_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Api services are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Api services',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/api_service');
    $this->assertNoErrorMessage();
  }

}
