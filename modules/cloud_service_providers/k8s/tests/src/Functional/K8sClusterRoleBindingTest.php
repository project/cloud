<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s cluster role binding.
 *
 * @group K8s
 */
class K8sClusterRoleBindingTest extends K8sTestBase {

  public const K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list k8s cluster role binding',
      'view any k8s cluster role binding',
      'edit any k8s cluster role binding',
      'add k8s cluster role binding',
      'delete any k8s cluster role binding',
    ];
  }

  /**
   * Tests CRUD for cluster role binding.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testClusterRoleBinding(): void {

    $cloud_context = $this->cloudContext;

    // List cluster role binding for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role_binding");
    $this->assertNoErrorMessage();

    // Add a new cluster role binding.
    $add = $this->createClusterRoleBindingTestFormData(self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT);
    for ($i = 0; $i < self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addClusterRoleBindingMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role_binding/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cluster role binding',
        '%label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role_binding");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all cluster_role_binding listing exists.
      $this->drupalGet('/clouds/k8s/cluster_role_binding');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role_binding/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a cluster role binding.
    $edit = $this->createClusterRoleBindingTestFormData(self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT; $i++, $num++) {

      $this->updateClusterRoleBindingMockData($edit[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role_binding/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cluster role binding',
        '%label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role_binding/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete cluster role binding.
    for ($i = 0, $num = 1; $i < self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT; $i++, $num++) {

      $this->deleteClusterRoleBindingMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role_binding/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cluster role binding',
        '@label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags(
        $this->t('The @type @label has been deleted.', $t_args)
      ));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role_binding");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting cluster role bindings with bulk operation.
   *
   * @throws \Exception
   */
  public function testClusterRoleBindingBulk(): void {

    for ($i = 0; $i < self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT; $i++) {
      // Create cluster role bindings.
      $cluster_role_bindings = $this->createClusterRoleBindingsRandomTestFormData();
      $entities = [];
      foreach ($cluster_role_bindings ?: [] as $cluster_role_binding) {
        $entities[] = $this->createClusterRoleBindingTestEntity($cluster_role_binding);
      }
      $this->deleteClusterRoleBindingMockData($cluster_role_bindings[0]);
      $this->runTestEntityBulk('cluster_role_binding', $entities);
    }
  }

  /**
   * Test updating all cluster role binding list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllClusterRoleBindingList(): void {
    $cloud_configs = [];

    // List cluster role binding for K8s.
    $this->drupalGet('/clouds/k8s/cluster_role_binding');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();

      // Add a new cluster role binding.
      $add = $this->createClusterRoleBindingTestFormData(self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT; $i++, $num++) {
        $this->reloadMockData();
        $this->addClusterRoleBindingMockData($add[$i], $cloud_context, $this->webUser->id());
        $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role_binding/add");
        $this->submitForm(
          $add[$i]['post_data'],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = [
          '@type' => 'Cluster role binding',
          '%label' => $add[$i]['name'],
        ];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
      }
    }

    // List cluster role binding for K8s.
    $this->drupalGet('/clouds/k8s/cluster_role_binding');
    $this->assertNoErrorMessage();

    // Make sure listing.
    for ($i = 0; $i < self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Cluster role bindings',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Edit a cluster role binding.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createClusterRoleBindingTestFormData(self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT; $i++, $num++) {

        $this->updateClusterRoleBindingMockData($edit[$i]);

        // Change cluster role binding name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/cluster_role_binding');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_CLUSTER_ROLE_BINDING_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm cluster role bindings are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Cluster role bindings',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/cluster_role_binding');
    $this->assertNoErrorMessage();
  }

}
