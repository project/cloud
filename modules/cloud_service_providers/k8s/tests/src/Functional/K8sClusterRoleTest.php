<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s cluster role.
 *
 * @group K8s
 */
class K8sClusterRoleTest extends K8sTestBase {

  public const K8S_CLUSTER_ROLE_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list k8s cluster role',
      'view any k8s cluster role',
      'edit any k8s cluster role',
      'add k8s cluster role',
      'delete any k8s cluster role',
    ];
  }

  /**
   * Tests CRUD for cluster role.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testClusterRole(): void {

    $cloud_context = $this->cloudContext;

    // List cluster role for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role");
    $this->assertNoErrorMessage();

    // Add a new cluster role.
    $add = $this->createClusterRoleTestFormData(self::K8S_CLUSTER_ROLE_REPEAT_COUNT);
    for ($i = 0; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addClusterRoleMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Cluster role', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all cluster_role listing exists.
      $this->drupalGet('/clouds/k8s/cluster_role');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a cluster role.
    $edit = $this->createClusterRoleTestFormData(self::K8S_CLUSTER_ROLE_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++, $num++) {

      $this->updateClusterRoleMockData($edit[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Cluster role', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete cluster role.
    for ($i = 0, $num = 1; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++, $num++) {

      $this->deleteClusterRoleMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Cluster role', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/cluster_role");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting cluster roles with bulk operation.
   *
   * @throws \Exception
   */
  public function testClusterRoleBulk(): void {

    for ($i = 0; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++) {
      // Create cluster roles.
      $cluster_roles = $this->createClusterRolesRandomTestFormData();
      $entities = [];
      foreach ($cluster_roles ?: [] as $cluster_role) {
        $entities[] = $this->createClusterRoleTestEntity($cluster_role);
      }
      $this->deleteClusterRoleMockData($cluster_roles[0]);
      $this->runTestEntityBulk('cluster_role', $entities);
    }
  }

  /**
   * Test updating all cluster role list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllClusterRoleList(): void {
    $cloud_configs = [];

    // List cluster role for K8s.
    $this->drupalGet('/clouds/k8s/cluster_role');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new cluster role.
      $add = $this->createClusterRoleTestFormData(self::K8S_CLUSTER_ROLE_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++, $num++) {
        $this->addClusterRoleMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List cluster role for K8s.
    $this->drupalGet('/clouds/k8s/cluster_role');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Cluster roles',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a cluster role.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createClusterRoleTestFormData(self::K8S_CLUSTER_ROLE_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++, $num++) {

        $this->updateClusterRoleMockData($edit[$i]);

        // Change cluster role name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/cluster_role');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm cluster roles are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Cluster roles',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/cluster_role');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_CLUSTER_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
