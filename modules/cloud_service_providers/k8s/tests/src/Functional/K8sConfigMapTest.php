<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s ConfigMap.
 *
 * @group K8s
 */
class K8sConfigMapTest extends K8sTestBase {

  public const K8S_CONFIG_MAP_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s config map',
      'add k8s config map',
      'view any k8s config map',
      'edit any k8s config map',
      'delete any k8s config map',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for ConfigMap.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testConfigMap(): void {

    $cloud_context = $this->cloudContext;

    // List ConfigMap for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/config_map");
    $this->assertNoErrorMessage();

    // Add a new ConfigMap.
    $add = $this->createConfigMapTestFormData(self::K8S_CONFIG_MAP_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addConfigMapMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/config_map/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'ConfigMap', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/config_map");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all config_map listing exists.
      $this->drupalGet('/clouds/k8s/config_map');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/config_map/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a ConfigMap.
    $edit = $this->createConfigMapTestFormData(self::K8S_CONFIG_MAP_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++, $num++) {

      $this->updateConfigMapMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/config_map/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'ConfigMap', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/config_map/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete ConfigMap.
    for ($i = 0, $num = 1; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++, $num++) {

      $this->deleteConfigMapMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/config_map/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'ConfigMap', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/config_map");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting ConfigMaps with bulk operation.
   *
   * @throws \Exception
   */
  public function testConfigMapBulk(): void {

    for ($i = 0; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++) {
      // Create ConfigMaps.
      $config_maps = $this->createConfigMapsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($config_maps ?: [] as $config_map) {
        $entities[] = $this->createConfigMapTestEntity($config_map);
      }
      $this->deleteConfigMapMockData($config_maps[0]);
      $this->runTestEntityBulk('config_map', $entities);
    }
  }

  /**
   * Test updating all Config Map list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllConfigMapList(): void {
    $cloud_configs = [];

    // List Config Map for K8s.
    $this->drupalGet('/clouds/k8s/config_map');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Config Map.
      $add = $this->createConfigMapTestFormData(self::K8S_CONFIG_MAP_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++, $num++) {
        $this->addConfigMapMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List Config Map for K8s.
    $this->drupalGet('/clouds/k8s/config_map');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'ConfigMaps',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a Config Map.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createConfigMapTestFormData(self::K8S_CONFIG_MAP_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++, $num++) {

        $this->updateConfigMapMockData($edit[$i]);

        // Change config map name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/config_map');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Config Maps are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'ConfigMaps',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/config_map');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_CONFIG_MAP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
