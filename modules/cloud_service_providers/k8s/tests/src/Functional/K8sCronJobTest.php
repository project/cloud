<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s CronJob.
 *
 * @group K8s
 */
class K8sCronJobTest extends K8sTestBase {

  public const K8S_CRON_JOB_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s cron job',
      'view any k8s cron job',
      'edit any k8s cron job',
      'add k8s cron job',
      'delete any k8s cron job',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for K8s CronJob.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testCronJob(): void {

    $cloud_context = $this->cloudContext;

    // List K8s CronJob for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/cron_job");
    $this->assertNoErrorMessage();

    // Add a new K8s CronJob.
    $add = $this->createCronJobTestFormData(self::K8S_CRON_JOB_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addCronJobMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/cron_job/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'CronJob', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/cron_job");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all cron_job listing exists.
      $this->drupalGet('/clouds/k8s/cron_job');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/cron_job/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a CronJob.
    $edit = $this->createCronJobTestFormData(self::K8S_CRON_JOB_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++, $num++) {

      $this->updateCronJobMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/cron_job/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'CronJob', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/cron_job/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete one or more CronJobs.
    for ($i = 0, $num = 1; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++, $num++) {

      $this->deleteCronJobMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/cron_job/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'CronJob', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/cron_job");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting cron jobs with bulk operation.
   *
   * @throws \Exception
   */
  public function testCronJobBulk(): void {

    for ($i = 0; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++) {
      // Create CronJobs.
      $cron_jobs = $this->createCronJobsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($cron_jobs ?: [] as $cron_job) {
        $entities[] = $this->createCronJobTestEntity($cron_job);
      }
      $this->deleteCronJobMockData($cron_jobs[0]);
      $this->runTestEntityBulk('cron_job', $entities);
    }
  }

  /**
   * Test updating all cron jobs list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllCronJobList(): void {
    $cloud_configs = [];

    // List CronJobs for K8s.
    $this->drupalGet('/clouds/k8s/cron_job');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new CronJob.
      $add = $this->createCronJobTestFormData(self::K8S_CRON_JOB_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++, $num++) {
        $this->addCronJobMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List CronJobs for K8s.
    $this->drupalGet('/clouds/k8s/cron_job');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'CronJobs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a CronJob.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createCronJobTestFormData(self::K8S_CRON_JOB_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++, $num++) {

        $this->updateCronJobMockData($edit[$i]);

        // Change CronJob name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/cron_job');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm CronJobs are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'CronJobs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/cron_job');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_CRON_JOB_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
