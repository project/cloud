<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s DaemonSet.
 *
 * @group K8s
 */
class K8sDaemonSetTest extends K8sTestBase {

  public const K8S_DAEMON_SET_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s daemon set',
      'add k8s daemon set',
      'edit any k8s daemon set',
      'view own k8s daemon set',
      'delete any k8s daemon set',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for K8s DaemonSet.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testDaemonSet(): void {

    $cloud_context = $this->cloudContext;

    // List K8s DaemonSets.
    $this->drupalGet("/clouds/k8s/$cloud_context/daemon_set");
    $this->assertNoErrorMessage();

    // Add a new K8s DaemonSet.
    $add = $this->createDaemonSetTestFormData(self::K8S_DAEMON_SET_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addDaemonSetMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/daemon_set/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'DaemonSet', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/daemon_set");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all daemon_set listing exists.
      $this->drupalGet('/clouds/k8s/daemon_set');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/daemon_set/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a K8s DaemonSet.
    $edit = $this->createDaemonSetTestFormData(self::K8S_DAEMON_SET_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++, $num++) {

      $this->updateDaemonSetMockData($edit[$i], $cloud_context, $this->webUser->id());

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/daemon_set/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'DaemonSet', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/daemon_set/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete one or more K8s DaemonSet(s).
    for ($i = 0, $num = 1; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++, $num++) {

      $this->deleteDaemonSetMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/daemon_set/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'DaemonSet', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/daemon_set");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting a K8s DaemonSet with bulk operation.
   *
   * @throws \Exception
   */
  public function testDaemonSetBulk(): void {

    for ($i = 0; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++) {
      // Create a K8s DaemonSet.
      $daemon_sets = $this->createDaemonSetsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($daemon_sets ?: [] as $daemon_set) {
        $entities[] = $this->createDaemonSetTestEntity($daemon_set);
      }
      $this->deleteDaemonSetMockData($daemon_sets[0]);
      $this->runTestEntityBulk('daemon_set', $entities);
    }
  }

  /**
   * Test updating all K8s DaemonSet list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllDaemonSetList(): void {
    $cloud_configs = [];

    // List K8s DaemonSets.
    $this->drupalGet('/clouds/k8s/daemon_set');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new K8s DaemonSet.
      $add = $this->createDaemonSetTestFormData(self::K8S_DAEMON_SET_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++, $num++) {
        $this->addDaemonSetMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List K8s DaemonSets.
    $this->drupalGet('/clouds/k8s/daemon_set');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'DaemonSets',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a K8s DaemonSet.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $edit = $this->createDaemonSetTestFormData(self::K8S_DAEMON_SET_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++, $num++) {

        $this->updateDaemonSetMockData($edit[$i], $cloud_context, $this->webUser->id());

        // Change a K8s DaemonSet name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/daemon_set');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm K8s DaemonSets are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'DaemonSets',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/daemon_set');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_DAEMON_SET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
