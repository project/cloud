<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s deployment.
 *
 * @group K8s
 */
class K8sDeploymentTest extends K8sTestBase {

  public const K8S_DEPLOYMENT_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s deployment',
      'add k8s deployment',
      'view any k8s deployment',
      'edit any k8s deployment',
      'delete any k8s deployment',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for Deployment.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testDeployment(): void {

    $cloud_context = $this->cloudContext;

    // List Deployment for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/deployment");
    $this->assertNoErrorMessage();

    // Add a new Deployment.
    $add = $this->createDeploymentTestFormData(self::K8S_DEPLOYMENT_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addDeploymentMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/deployment/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Deployment', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/deployment");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all deployment listing exists.
      $this->drupalGet('/clouds/k8s/deployment');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/deployment/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Deployment.
    $edit = $this->createDeploymentTestFormData(self::K8S_DEPLOYMENT_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++, $num++) {

      $this->updateDeploymentMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/deployment/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Deployment', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/deployment/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete Deployment.
    for ($i = 0, $num = 1; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++, $num++) {

      $this->deleteDeploymentMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/deployment/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Deployment', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/deployment");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting deployments with bulk operation.
   *
   * @throws \Exception
   */
  public function testDeploymentBulk(): void {

    for ($i = 0; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++) {
      // Create Deployments.
      $deployments = $this->createDeploymentsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($deployments ?: [] as $deployment) {
        $entities[] = $this->createDeploymentTestEntity($deployment);
      }
      $this->deleteDeploymentMockData($deployments[0]);
      $this->runTestEntityBulk('deployment', $entities);
    }
  }

  /**
   * Test updating all deployment list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllDeploymentList(): void {
    $cloud_configs = [];

    // List Deployment for K8s.
    $this->drupalGet('/clouds/k8s/deployment');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Deployment.
      $add = $this->createDeploymentTestFormData(self::K8S_DEPLOYMENT_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++, $num++) {
        $this->addDeploymentMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List Deployment for K8s.
    $this->drupalGet('/clouds/k8s/deployment');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Deployments',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a Deployment.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createDeploymentTestFormData(self::K8S_DEPLOYMENT_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++, $num++) {

        $this->updateDeploymentMockData($edit[$i]);

        // Change Deployment name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/deployment');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Deployments are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Deployments',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/deployment');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_DEPLOYMENT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
