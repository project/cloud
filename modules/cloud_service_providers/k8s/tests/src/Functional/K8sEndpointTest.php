<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s endpoint.
 *
 * @group K8s
 */
class K8sEndpointTest extends K8sTestBase {

  public const K8S_ENDPOINT_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];
    return [
      'view all cloud service providers',
      'list k8s endpoint',
      'add k8s endpoint',
      'view any k8s endpoint',
      'edit any k8s endpoint',
      'delete any k8s endpoint',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for Endpoint.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testEndpoint(): void {

    $cloud_context = $this->cloudContext;

    // List endpoint for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/endpoint");
    $this->assertNoErrorMessage();

    // Add a new endpoint.
    $add = $this->createEndpointTestFormData(self::K8S_ENDPOINT_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addEndpointMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/endpoint/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Endpoint', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/endpoint");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all endpoint listing exists.
      $this->drupalGet('/clouds/k8s/endpoint');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/endpoint/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Endpoint.
    $edit = $this->createEndpointTestFormData(self::K8S_ENDPOINT_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++, $num++) {

      $this->updateEndpointMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/endpoint/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Endpoint', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/endpoint/$num");
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete Endpoint.
    for ($i = 0, $num = 1; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++, $num++) {

      $this->deleteEndpointMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/endpoint/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Endpoint', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/endpoint");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting endpoint with bulk operation.
   *
   * @throws \Exception
   */
  public function testEndpointBulk(): void {

    for ($i = 0; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++) {
      // Create Endpoint.
      $endpoints = $this->createEndpointsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($endpoints ?: [] as $endpoint) {
        $entities[] = $this->createEndpointTestEntity($endpoint);
      }
      $this->deleteEndpointMockData($endpoints[0]);
      $this->runTestEntityBulk('endpoint', $entities);
    }
  }

  /**
   * Test updating all endpoint list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllEndpointList(): void {
    $cloud_configs = [];

    // List Endpoint for K8s.
    $this->drupalGet('/clouds/k8s/endpoint');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Endpoint.
      $add = $this->createEndpointTestFormData(self::K8S_ENDPOINT_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++, $num++) {
        $this->addEndpointMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List Endpoint for K8s.
    $this->drupalGet('/clouds/k8s/endpoint');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Endpoints',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a Endpoint.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createEndpointTestFormData(self::K8S_ENDPOINT_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++, $num++) {

        $this->updateEndpointMockData($edit[$i]);

        // Change Endpoint name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/endpoint');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Endpoints are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Endpoints',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/endpoint');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_ENDPOINT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
