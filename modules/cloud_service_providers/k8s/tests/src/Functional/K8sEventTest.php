<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s event.
 *
 * @group K8s
 */
class K8sEventTest extends K8sTestBase {

  public const K8S_EVENT_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list k8s event',
      'view k8s event',
    ];
  }

  /**
   * Tests CRUD for K8s event.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testEvent(): void {
    $cloud_context = $this->cloudContext;

    $data = $this->createEventTestFormData(self::K8S_EVENT_REPEAT_COUNT);
    $this->updateEventsMockData($data);

    // Update k8s events.
    $this->drupalGet("/clouds/k8s/$cloud_context/event/update");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_EVENT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($data[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_EVENT_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all event listing exists.
      $this->drupalGet('/clouds/k8s/event');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($data[$j]['name']);
      }
    }
  }

  /**
   * Test updating all event list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllEventList(): void {
    $cloud_configs = [];

    // List Event for K8s.
    $this->drupalGet('/clouds/k8s/event');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_EVENT_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      // Add a new Event.
      $add = $this->createEventTestFormData(self::K8S_EVENT_REPEAT_COUNT);
      $this->updateEventsMockData($add);
    }

    // List Event for K8s.
    $this->drupalGet('/clouds/k8s/event');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Events',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_EVENT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Edit an Event.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createEventTestFormData(self::K8S_EVENT_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_EVENT_REPEAT_COUNT; $i++, $num++) {

        $this->updateEventsMockData($edit);

        // Change Event name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/event');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_EVENT_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Events are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Events',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/event');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_EVENT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[$i]['name']);
    }
  }

}
