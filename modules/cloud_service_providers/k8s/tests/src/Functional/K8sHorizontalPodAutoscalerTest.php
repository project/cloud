<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s horizontal pod autoscaler.
 *
 * @group K8s
 */
class K8sHorizontalPodAutoscalerTest extends K8sTestBase {

  public const K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s horizontal pod autoscaler',
      'add k8s horizontal pod autoscaler',
      'edit any k8s horizontal pod autoscaler',
      'delete any k8s horizontal pod autoscaler',
      'view any k8s horizontal pod autoscaler',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for horizontal pod autoscaler.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testHorizontalPodAutoscaler(): void {

    $cloud_context = $this->cloudContext;

    // List horizontal pod autoscaler for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/horizontal_pod_autoscaler");
    $this->assertNoErrorMessage();

    // Add a new horizontal pod autoscaler.
    $add = $this->createHorizontalPodAutoscalerTestFormData(self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addHorizontalPodAutoscalerMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/horizontal_pod_autoscaler/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Horizontal pod autoscaler',
        '%label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/horizontal_pod_autoscaler");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all horizontal_pod_autoscaler listing exists.
      $this->drupalGet('/clouds/k8s/horizontal_pod_autoscaler');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/horizontal_pod_autoscaler/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a horizontal pod autoscaler.
    $edit = $this->createHorizontalPodAutoscalerTestFormData(self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++, $num++) {

      $this->updateHorizontalPodAutoscalerMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/horizontal_pod_autoscaler/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Horizontal pod autoscaler',
        '%label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/horizontal_pod_autoscaler/$num");
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete horizontal pod autoscaler.
    for ($i = 0, $num = 1; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++, $num++) {

      $this->deleteHorizontalPodAutoscalerMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/horizontal_pod_autoscaler/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Horizontal pod autoscaler',
        '@label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/horizontal_pod_autoscaler");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting horizontal pod autoscalers with bulk operation.
   *
   * @throws \Exception
   */
  public function testHorizontalPodAutoscalerBulk(): void {

    for ($i = 0; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++) {
      // Create horizontal pod autoscaler.
      $horizontal_pod_autoscalers = $this->createHorizontalPodAutoscalersRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($horizontal_pod_autoscalers ?: [] as $horizontal_pod_autoscaler) {
        $entities[] = $this->createHorizontalPodAutoscalerTestEntity($horizontal_pod_autoscaler);
      }
      $this->deleteHorizontalPodAutoscalerMockData($horizontal_pod_autoscalers[0]);
      $this->runTestEntityBulk('horizontal_pod_autoscaler', $entities);
    }
  }

  /**
   * Test updating all horizontal pod autoscaler list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllHorizontalPodAutoscalerList(): void {
    $cloud_configs = [];

    // List horizontal pod autoscaler for K8s.
    $this->drupalGet('/clouds/k8s/horizontal_pod_autoscaler');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      // Add a new horizontal pod autoscaler.
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createHorizontalPodAutoscalerTestFormData(self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++, $num++) {
        $this->addHorizontalPodAutoscalerMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List horizontal pod autoscaler for K8s.
    $this->drupalGet('/clouds/k8s/horizontal_pod_autoscaler');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Horizontal pod autoscalers',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a horizontal pod autoscaler.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createHorizontalPodAutoscalerTestFormData(self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++, $num++) {

        $this->updateHorizontalPodAutoscalerMockData($edit[$i]);

        // Change horizontal pod autoscaler name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/horizontal_pod_autoscaler');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm horizontal pod autoscaler are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Horizontal pod autoscalers',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/horizontal_pod_autoscaler');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_HORIZONTAL_POD_AUTOSCALER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
