<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s ingress.
 *
 * @group K8s
 */
class K8sIngressTest extends K8sTestBase {

  public const K8S_INGRESS_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s ingress',
      'add k8s ingress',
      'view any k8s ingress',
      'edit any k8s ingress',
      'delete any k8s ingress',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for Ingress.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testIngress(): void {

    $cloud_context = $this->cloudContext;

    // List Ingress for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/ingress");
    $this->assertNoErrorMessage();

    // Add a new Ingress.
    $add = $this->createIngressTestFormData(self::K8S_INGRESS_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addIngressMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/ingress/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Ingress', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/ingress");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all ingress listing exists.
      $this->drupalGet('/clouds/k8s/ingress');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/ingress/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit an Ingress.
    $edit = $this->createIngressTestFormData(self::K8S_INGRESS_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++, $num++) {

      $this->updateIngressMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/ingress/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Ingress', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/ingress/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete Ingress.
    for ($i = 0, $num = 1; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++, $num++) {

      $this->deleteIngressMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/ingress/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Ingress', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/ingress");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting ingress with bulk operation.
   *
   * @throws \Exception
   */
  public function testIngressBulk(): void {

    for ($i = 0; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++) {
      // Create Ingress.
      $ingresses = $this->createIngressesRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($ingresses ?: [] as $ingress) {
        $entities[] = $this->createIngressTestEntity($ingress);
      }
      $this->deleteIngressMockData($ingresses[0]);
      $this->runTestEntityBulk('ingress', $entities);
    }
  }

  /**
   * Test updating all ingress list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllIngressList(): void {
    $cloud_configs = [];

    // List Ingress for K8s.
    $this->drupalGet('/clouds/k8s/ingress');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Ingress.
      $add = $this->createIngressTestFormData(self::K8S_INGRESS_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++, $num++) {
        $this->addIngressMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List Ingress for K8s.
    $this->drupalGet('/clouds/k8s/ingress');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Ingresses',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit an Ingress.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createIngressTestFormData(self::K8S_INGRESS_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++, $num++) {

        $this->updateIngressMockData($edit[$i]);

        // Change Ingress name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/ingress');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Ingresses are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Ingresses',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/ingress');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_INGRESS_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
