<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s job.
 *
 * @group K8s
 */
class K8sJobTest extends K8sTestBase {

  public const K8S_JOB_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s job',
      'view any k8s job',
      'edit any k8s job',
      'add k8s job',
      'delete any k8s job',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for Job.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testJob(): void {

    $cloud_context = $this->cloudContext;

    // List Job for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/job");
    $this->assertNoErrorMessage();

    // Add a new Job.
    $add = $this->createJobTestFormData(self::K8S_JOB_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_JOB_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addJobMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/job/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Job', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/job");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_JOB_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all job listing exists.
      $this->drupalGet('/clouds/k8s/job');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/job/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Job.
    $edit = $this->createJobTestFormData(self::K8S_JOB_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_JOB_REPEAT_COUNT; $i++, $num++) {

      $this->updateJobMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/job/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Job', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/job/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete Job.
    for ($i = 0, $num = 1; $i < self::K8S_JOB_REPEAT_COUNT; $i++, $num++) {

      $this->deleteJobMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/job/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Job', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/job");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting jobs with bulk operation.
   *
   * @throws \Exception
   */
  public function testJobBulk(): void {

    for ($i = 0; $i < self::K8S_JOB_REPEAT_COUNT; $i++) {
      // Create Jobs.
      $jobs = $this->createJobsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($jobs ?: [] as $job) {
        $entities[] = $this->createJobTestEntity($job);
      }
      $this->deleteJobMockData($jobs[0]);
      $this->runTestEntityBulk('job', $entities);
    }
  }

  /**
   * Test updating all jobs list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllJobList(): void {
    $cloud_configs = [];

    // List Job for K8s.
    $this->drupalGet('/clouds/k8s/job');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_JOB_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Job.
      $add = $this->createJobTestFormData(self::K8S_JOB_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_JOB_REPEAT_COUNT; $i++, $num++) {
        $this->addJobMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List Job for K8s.
    $this->drupalGet('/clouds/k8s/job');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Jobs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_JOB_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a Job.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createJobTestFormData(self::K8S_JOB_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_JOB_REPEAT_COUNT; $i++, $num++) {

        $this->updateJobMockData($edit[$i]);

        // Change Job name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/job');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_JOB_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Jobs are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Jobs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/job');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_JOB_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
