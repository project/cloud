<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s LimitRange.
 *
 * @group K8s
 */
class K8sLimitRangeTest extends K8sTestBase {

  public const K8S_LIMIT_RANGE_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s limit range',
      'view any k8s limit range',
      'edit any k8s limit range',
      'add k8s limit range',
      'delete any k8s limit range',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for K8s LimitRange.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testLimitRange(): void {

    $cloud_context = $this->cloudContext;

    // List K8s LimitRanges.
    $this->drupalGet("/clouds/k8s/$cloud_context/limit_range");
    $this->assertNoErrorMessage();

    // Add a new K8s LimitRange.
    $add = $this->createLimitRangeTestFormData(self::K8S_LIMIT_RANGE_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addLimitRangeMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/limit_range/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'LimitRange', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/limit_range");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all limit_range listing exists.
      $this->drupalGet('/clouds/k8s/limit_range');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/limit_range/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a K8s LimitRange.
    $edit = $this->createLimitRangeTestFormData(self::K8S_LIMIT_RANGE_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++, $num++) {

      $this->updateLimitRangeMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/limit_range/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'LimitRange', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/limit_range/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete one or more K8s LimitRange(s).
    for ($i = 0, $num = 1; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++, $num++) {

      $this->deleteLimitRangeMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/limit_range/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'LimitRange', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/limit_range");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting a K8s LimitRange with bulk operation.
   *
   * @throws \Exception
   */
  public function testLimitRangeBulk(): void {

    for ($i = 0; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++) {
      // Create a K8s LimitRange.
      $limit_ranges = $this->createLimitRangesRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($limit_ranges ?: [] as $limit_range) {
        $entities[] = $this->createLimitRangeTestEntity($limit_range);
      }
      $this->deleteLimitRangeMockData($limit_ranges[0]);
      $this->runTestEntityBulk('limit_range', $entities);
    }
  }

  /**
   * Test updating all K8s LimitRange list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllLimitRangeList(): void {
    $cloud_configs = [];

    // List K8s LimitRanges.
    $this->drupalGet('/clouds/k8s/limit_range');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new K8s LimitRange.
      $add = $this->createLimitRangeTestFormData(self::K8S_LIMIT_RANGE_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++, $num++) {
        $this->addLimitRangeMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List K8s LimitRanges.
    $this->drupalGet('/clouds/k8s/limit_range');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'LimitRanges',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a K8s Limit Range.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createLimitRangeTestFormData(self::K8S_LIMIT_RANGE_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++, $num++) {

        $this->updateLimitRangeMockData($edit[$i]);

        // Change a K8s LimitRange name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/limit_range');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm K8s LimitRanges are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'LimitRanges',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/limit_range');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_LIMIT_RANGE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
