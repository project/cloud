<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s namespace.
 *
 * @group K8s
 */
class K8sNamespaceTest extends K8sTestBase {

  public const K8S_NAMESPACE_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list k8s namespace',
      'view any k8s namespace',
      'edit any k8s namespace',
      'add k8s namespace',
      'delete any k8s namespace',
    ];
  }

  /**
   * Tests CRUD for Namespace.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testNamespace(): void {

    $cloud_context = $this->cloudContext;

    // List Namespace for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/namespace");
    $this->assertNoErrorMessage();

    // Add a new Namespace.
    $add = $this->createNamespaceTestFormData(self::K8S_NAMESPACE_REPEAT_COUNT);
    for ($i = 0; $i < self::K8S_NAMESPACE_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addNamespaceMockData($add[$i], $cloud_context, $this->webUser->id());

      // Create entity.
      $nodes = $this->createNodeTestFormData(self::K8S_NAMESPACE_REPEAT_COUNT);
      $pods = $this->createPodsRandomTestFormData($add[$i]['name']);
      foreach ($pods ?: [] as $pod) {
        $this->createPodTestEntity($pod);
      }
      foreach ($nodes ?: [] as $node) {
        $this->createNodeTestEntity($node);
      }
      // Create resources and cost mock data.
      $this->getNamespaceResourceUsageMockData($pods);
      $this->getNodeResourceUsageMockData($nodes);
      $this->calculateCostPerNamespaceMockData();

      $this->drupalGet("/clouds/k8s/$cloud_context/namespace/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Namespace', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      $add_to_validate = $this->createNamespaceTestFormData(self::K8S_NAMESPACE_REPEAT_COUNT);
      // Pick on of: '<space>' ! " # $ % & ' ( ) * + , . : ; < = > ? @
      // A B C E F G H I J K L M N O P Q R S T U V W X Y Z [ \ ] ^ _ `.
      // Except hyphen ('-') as a special character since it is allowed to use.
      $special = [
        chr(random_int(32, 44)),
        chr(random_int(46, 47)),
        chr(random_int(58, 96)),
      ];
      // Insert a special character into somewhere in namespace's name.
      $add_to_validate[$i]['name'] = substr_replace(
        $add_to_validate[$i]['name'],
        $special[array_rand($special)],
        random_int(0, strlen($add_to_validate[$i]['name'])),
        0
      );

      $this->drupalGet("/clouds/k8s/$cloud_context/namespace/add");
      $this->submitForm(
        $add_to_validate[$i],
        $this->t('Save')->render()
      );
      $this->assertSession()->pageTextContains(strip_tags($this->t("a lowercase RFC 1123 label must consist of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character (e.g. 'my-name', or '123-abc').")));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/namespace");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_NAMESPACE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all namespace listing exists.
      $this->drupalGet('/clouds/k8s/namespace');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/namespace/$num");
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Namespace.
    $edit = $this->createNamespaceTestFormData(self::K8S_NAMESPACE_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::K8S_NAMESPACE_REPEAT_COUNT; $i++, $num++) {

      $this->updateNamespaceMockData($edit[$i]);

      unset($edit[$i]['name']);
      $this->drupalGet("/clouds/k8s/$cloud_context/namespace/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Namespace', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure labels.
      $this->drupalGet("/clouds/k8s/$cloud_context/namespace/$num");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['labels[0][item_key]']);
      $this->assertSession()->pageTextContains($edit[$i]['labels[0][item_value]']);
      $this->assertSession()->pageTextContains($edit[$i]['annotations[0][item_key]']);
      $this->assertSession()->pageTextContains($edit[$i]['annotations[0][item_value]']);
      // Make sure uid.
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete Namespace.
    for ($i = 0, $num = 1; $i < self::K8S_NAMESPACE_REPEAT_COUNT; $i++, $num++) {

      $this->deleteNamespaceMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/namespace/$num/delete");
      $this->assertNoErrorMessage();

      $this->drupalGet("/clouds/k8s/$cloud_context/namespace/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Namespace', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/namespace");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting namespaces with bulk operation.
   *
   * @throws \Exception
   */
  public function testNamespaceBulk(): void {

    for ($i = 0; $i < self::K8S_NAMESPACE_REPEAT_COUNT; $i++) {
      // Create Namespaces.
      $namespaces = $this->createNamespacesRandomTestFormData();
      $entities = [];
      foreach ($namespaces ?: [] as $namespace) {
        $entities[] = $this->createNamespaceTestEntity($namespace);
      }
      $this->deleteNamespaceMockData($namespaces[0]);
      $this->runTestEntityBulk('namespace', $entities);
    }
  }

  /**
   * Test updating all namespace list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllNamespaceList(): void {
    $cloud_configs = [];

    // List Namespace for K8s.
    $this->drupalGet('/clouds/k8s/namespace');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_NAMESPACE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Namespace.
      $add = $this->createNamespaceTestFormData(self::K8S_NAMESPACE_REPEAT_COUNT);
      for ($i = 0; $i < self::K8S_NAMESPACE_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addNamespaceMockData($add[$i], $cloud_context, $this->webUser->id());

        // Create entity.
        $nodes = $this->createNodeTestFormData(self::K8S_NAMESPACE_REPEAT_COUNT);
        $pods = $this->createPodsRandomTestFormData($add[$i]['name']);
        foreach ($pods ?: [] as $pod) {
          $this->createPodTestEntity($pod);
        }
        foreach ($nodes ?: [] as $node) {
          $this->createNodeTestEntity($node);
        }
        // Create resources and cost mock data.
        $this->getNamespaceResourceUsageMockData($pods);
        $this->getNodeResourceUsageMockData($nodes);
        $this->calculateCostPerNamespaceMockData();

        $this->drupalGet("/clouds/k8s/$cloud_context/namespace/add");
        $this->submitForm(
          $add[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();
        $t_args = ['@type' => 'Namespace', '%label' => $add[$i]['name']];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
      }
    }

    // List Namespace for K8s.
    $this->drupalGet('/clouds/k8s/namespace');
    $this->assertNoErrorMessage();

    // Make sure listing.
    for ($i = 0; $i < self::K8S_NAMESPACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Namespaces',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Edit Namespace information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createNamespaceTestFormData(self::K8S_NAMESPACE_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_NAMESPACE_REPEAT_COUNT; $i++, $num++) {
        // Change Namespace name in mock data.
        $add[$i]['name'] = sprintf('Namespace-%s - %s', $this->random->name(8, TRUE), date('Y/m/d H:i:s'));
        $this->updateNamespaceMockData($edit[$i]);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/namespace');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_NAMESPACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Instances are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Namespaces',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }
  }

}
