<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s network policy.
 *
 * @group K8s
 */
class K8sNetworkPolicyTest extends K8sTestBase {

  public const K8S_NETWORK_POLICY_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s network policy',
      'add k8s network policy',
      'view any k8s network policy',
      'edit any k8s network policy',
      'delete any k8s network policy',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for network policy.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testNetworkPolicy(): void {

    $cloud_context = $this->cloudContext;

    // List network policy for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/network_policy");
    $this->assertNoErrorMessage();

    // Add a new network policy.
    $add = $this->createNetworkPolicyTestFormData(self::K8S_NETWORK_POLICY_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addNetworkPolicyMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/network_policy/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Network policy', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/network_policy");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all network_policy listing exists.
      $this->drupalGet('/clouds/k8s/network_policy');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/network_policy/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a network policy.
    $edit = $this->createNetworkPolicyTestFormData(self::K8S_NETWORK_POLICY_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++, $num++) {

      $this->updateNetworkPolicyMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/network_policy/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Network policy', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/network_policy/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete network policy.
    for ($i = 0, $num = 1; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++, $num++) {

      $this->deleteNetworkPolicyMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/network_policy/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Network policy', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/network_policy");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting network policies with bulk operation.
   *
   * @throws \Exception
   */
  public function testNetworkPolicyBulk(): void {

    for ($i = 0; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++) {
      // Create network policies.
      $network_policies = $this->createNetworkPoliciesRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($network_policies ?: [] as $network_policy) {
        $entities[] = $this->createNetworkPolicyTestEntity($network_policy);
      }
      $this->deleteNetworkPolicyMockData($network_policies[0]);
      $this->runTestEntityBulk('network_policy', $entities);
    }
  }

  /**
   * Test updating all network policy list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllNetworkPolicyList(): void {
    $cloud_configs = [];

    // List network policy for K8s.
    $this->drupalGet('/clouds/k8s/network_policy');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new network policy.
      $add = $this->createNetworkPolicyTestFormData(self::K8S_NETWORK_POLICY_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++, $num++) {
        $this->addNetworkPolicyMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List network policy for K8s.
    $this->drupalGet('/clouds/k8s/network_policy');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Network policies',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a network policy.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createNetworkPolicyTestFormData(self::K8S_NETWORK_POLICY_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++, $num++) {

        $this->updateNetworkPolicyMockData($edit[$i]);

        // Change network policy name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/network_policy');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm network policies are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Network policies',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/network_policy');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_NETWORK_POLICY_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
