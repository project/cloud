<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s persistent volume claim.
 *
 * @group K8s
 */
class K8sPersistentVolumeClaimTest extends K8sTestBase {

  public const K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s persistent volume claim',
      'add k8s persistent volume claim',
      'view any k8s persistent volume claim',
      'edit any k8s persistent volume claim',
      'delete any k8s persistent volume claim',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for persistent volume claim.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testPersistentVolumeClaim(): void {

    $cloud_context = $this->cloudContext;

    // List persistent volume claim for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume_claim");
    $this->assertNoErrorMessage();

    // Add a new persistent volume claim.
    $add = $this->createPersistentVolumeClaimTestFormData(self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addPersistentVolumeClaimMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume_claim/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Persistent volume claim',
        '%label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume_claim");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all persistent_volume_claim listing exists.
      $this->drupalGet('/clouds/k8s/persistent_volume_claim');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume_claim/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a persistent volume claim.
    $edit = $this->createPersistentVolumeClaimTestFormData(self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++, $num++) {

      $this->updatePersistentVolumeClaimMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume_claim/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Persistent volume claim',
        '%label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume_claim/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete persistent volume claim.
    for ($i = 0, $num = 1; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++, $num++) {

      $this->deletePersistentVolumeClaimMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume_claim/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Persistent volume claim',
        '@label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume_claim");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting persistent volume claim with bulk operation.
   *
   * @throws \Exception
   */
  public function testPersistentVolumeClaimBulk(): void {

    for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++) {
      // Create persistent volume claim.
      $persistent_volume_claims = $this->createPersistentVolumeClaimsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($persistent_volume_claims ?: [] as $persistent_volume_claim) {
        $entities[] = $this->createPersistentVolumeClaimTestEntity($persistent_volume_claim);
      }
      $this->deletePersistentVolumeClaimMockData($persistent_volume_claims[0]);
      $this->runTestEntityBulk('persistent_volume_claim', $entities);
    }
  }

  /**
   * Test updating all persistent volume claim list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllPersistentVolumeClaimList(): void {
    $cloud_configs = [];

    // List persistent volume claim for K8s.
    $this->drupalGet('/clouds/k8s/persistent_volume_claim');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new persistent volume claim.
      $add = $this->createPersistentVolumeClaimTestFormData(self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++, $num++) {
        $this->addPersistentVolumeClaimMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List persistent volume claim for K8s.
    $this->drupalGet('/clouds/k8s/persistent_volume_claim');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Persistent volume claims',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a persistent volume claim.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createPersistentVolumeClaimTestFormData(self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++, $num++) {

        $this->updatePersistentVolumeClaimMockData($edit[$i]);

        // Change persistent volume claim name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/persistent_volume_claim');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm persistent volume claims are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Persistent volume claims',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/persistent_volume_claim');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_CLAIM_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
