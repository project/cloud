<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s persistent volume.
 *
 * @group K8s
 */
class K8sPersistentVolumeTest extends K8sTestBase {

  public const K8S_PERSISTENT_VOLUME_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list k8s persistent volume',
      'view any k8s persistent volume',
      'edit any k8s persistent volume',
      'add k8s persistent volume',
      'delete any k8s persistent volume',
    ];
  }

  /**
   * Tests CRUD for persistent volume.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testPersistentVolume(): void {

    $cloud_context = $this->cloudContext;

    // List persistent volume for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume");
    $this->assertNoErrorMessage();

    // Add a new persistent volume.
    $add = $this->createPersistentVolumeTestFormData(self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT);
    for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addPersistentVolumeMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Persistent volume', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all persistent_volume listing exists.
      $this->drupalGet('/clouds/k8s/persistent_volume');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a persistent volume.
    $edit = $this->createPersistentVolumeTestFormData(self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT; $i++, $num++) {

      $this->updatePersistentVolumeMockData($edit[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Persistent volume', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete persistent volume.
    for ($i = 0, $num = 1; $i < self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT; $i++, $num++) {

      $this->deletePersistentVolumeMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Persistent volume', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/persistent_volume");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting persistent volume with bulk operation.
   *
   * @throws \Exception
   */
  public function testPersistentVolumeBulk(): void {

    for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT; $i++) {
      // Create persistent volume.
      $persistent_volumes = $this->createPersistentVolumeRandomTestFormData();
      $entities = [];
      foreach ($persistent_volumes ?: [] as $persistent_volume) {
        $entities[] = $this->createPersistentVolumeTestEntity($persistent_volume);
      }

      $this->deletePersistentVolumeMockData($persistent_volumes[0]);
      $this->runTestEntityBulk('persistent_volume', $entities);
    }
  }

  /**
   * Test updating all persistent volume list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllPersistentVolumeList(): void {
    $cloud_configs = [];

    // List persistent volumes for K8s.
    $this->drupalGet('/clouds/k8s/persistent_volume');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new persistent volume.
      $add = $this->createPersistentVolumeTestFormData(self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT; $i++, $num++) {
        $this->addPersistentVolumeMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List persistent volume for K8s.
    $this->drupalGet('/clouds/k8s/persistent_volume');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Persistent volumes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a persistent volume.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createPersistentVolumeTestFormData(self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT; $i++, $num++) {

        $this->updatePersistentVolumeMockData($edit[$i]);

        // Change persistent volume name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/persistent_volume');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_PERSISTENT_VOLUME_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm persistent volumes are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Persistent volumes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/persistent_volume');
    $this->assertNoErrorMessage();
  }

}
