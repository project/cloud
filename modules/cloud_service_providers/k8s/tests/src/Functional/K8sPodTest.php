<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s pod.
 *
 * @group K8s
 */
class K8sPodTest extends K8sTestBase {

  public const K8S_POD_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s pod',
      'add k8s pod',
      'view any k8s pod',
      'edit any k8s pod',
      'delete any k8s pod',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for Pod.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testPod(): void {

    $cloud_context = $this->cloudContext;

    // List Pod for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/pod");
    $this->assertNoErrorMessage();

    // Add a new Pod.
    $add = $this->createPodTestFormData(self::K8S_POD_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_POD_REPEAT_COUNT; $i++, $num++) {
      $this->reloadMockData();

      $this->addPodMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/pod/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Pod', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/pod");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);

      // Assert logs.
      $logs = $this->random->name(128, TRUE);
      $this->getPodLogsMockData($logs);
      $this->drupalGet("/clouds/k8s/$cloud_context/pod/$num/log");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($logs);

      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/pod/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    for ($i = 0, $num = 1; $i < self::K8S_POD_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all pod listing exists.
      $this->drupalGet('/clouds/k8s/pod');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit a Pod.
    $edit = $this->createPodTestFormData(self::K8S_POD_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_POD_REPEAT_COUNT; $i++, $num++) {

      $this->updatePodMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/pod/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Pod', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/pod/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete Pod.
    for ($i = 0, $num = 1; $i < self::K8S_POD_REPEAT_COUNT; $i++, $num++) {

      $this->deletePodMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/pod/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Pod', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/pod");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting pods with bulk operation.
   *
   * @throws \Exception
   */
  public function testPodBulk(): void {

    for ($i = 0; $i < self::K8S_POD_REPEAT_COUNT; $i++) {
      // Create Pods.
      $pods = $this->createPodsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($pods ?: [] as $pod) {
        $entities[] = $this->createPodTestEntity($pod);
      }
      $this->deletePodMockData($pods[0]);
      $this->runTestEntityBulk('pod', $entities);
    }
  }

  /**
   * Test updating all pod list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllPodList(): void {
    $cloud_configs = [];

    // List Pod for K8s.
    $this->drupalGet('/clouds/k8s/pod');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_POD_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Pod.
      $add = $this->createPodTestFormData(self::K8S_POD_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_POD_REPEAT_COUNT; $i++, $num++) {
        $this->addPodMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List Pod for K8s.
    $this->drupalGet('/clouds/k8s/pod');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Pods',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_POD_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a Pod.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createPodTestFormData(self::K8S_POD_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_POD_REPEAT_COUNT; $i++, $num++) {

        $this->updatePodMockData($edit[$i]);

        // Change Pod name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/pod');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_POD_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Pods are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Pods',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/pod');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_POD_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
