<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s priority class.
 *
 * @group K8s
 */
class K8sPriorityClassTest extends K8sTestBase {

  public const K8S_PRIORITY_CLASS_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list k8s priority class',
      'add k8s priority class',
      'edit any k8s priority class',
      'delete any k8s priority class',
      'view any k8s priority class',
    ];
  }

  /**
   * Tests CRUD for priority class.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testPriorityClass(): void {

    $cloud_context = $this->cloudContext;

    // List priority class for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/priority_class");
    $this->assertNoErrorMessage();

    // Add a new priority class.
    $add = $this->createPriorityClassTestFormData(self::K8S_PRIORITY_CLASS_REPEAT_COUNT);
    for ($i = 0; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addPriorityClassMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/priority_class/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Priority class', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/priority_class");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all priority_class listing exists.
      $this->drupalGet('/clouds/k8s/priority_class');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/priority_class/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit the description of a priority class.
    // k8sAPI v1 returns 422 Unprocessable Entity when trying to edit the value.
    $edit = $this->createPriorityClassTestFormData(self::K8S_PRIORITY_CLASS_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++, $num++) {

      $this->updatePriorityClassMockData($edit[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/priority_class/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Priority class', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/priority_class/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete priority class.
    for ($i = 0, $num = 1; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++, $num++) {

      $this->deletePriorityClassMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/priority_class/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Priority class', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/priority_class");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting priority classes with bulk operation.
   *
   * @throws \Exception
   */
  public function testPriorityClassBulk(): void {

    for ($i = 0; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++) {
      // Create priority classes.
      $priority_classes = $this->createPriorityClassesRandomTestFormData();
      $entities = [];
      foreach ($priority_classes ?: [] as $priority_class) {
        $entities[] = $this->createPriorityClassTestEntity($priority_class);
      }
      $this->deletePriorityClassMockData($priority_classes[0]);
      $this->runTestEntityBulk('priority_class', $entities);
    }
  }

  /**
   * Test updating all priority class list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllPriorityClassList(): void {
    $cloud_configs = [];

    // List priority class for K8s.
    $this->drupalGet('/clouds/k8s/priority_class');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new priority class.
      $add = $this->createPriorityClassTestFormData(self::K8S_PRIORITY_CLASS_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++, $num++) {
        $this->addPriorityClassMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List priority class for K8s.
    $this->drupalGet('/clouds/k8s/priority_class');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Priority classes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a priority class.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createPriorityClassTestFormData(self::K8S_PRIORITY_CLASS_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++, $num++) {

        $this->updatePriorityClassMockData($edit[$i]);

        // Change priority class name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/priority_class');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm priority classes are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Priority classes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/priority_class');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_PRIORITY_CLASS_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
