<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s resource quota.
 *
 * @group K8s
 */
class K8sResourceQuotaTest extends K8sTestBase {

  public const K8S_RESOURCE_QUOTA_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s resource quota',
      'view any k8s resource quota',
      'edit any k8s resource quota',
      'add k8s resource quota',
      'delete any k8s resource quota',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for resource quota.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testResourceQuota(): void {

    $cloud_context = $this->cloudContext;

    // List resource quota for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/resource_quota");
    $this->assertNoErrorMessage();

    // Add a new resource quota.
    $add = $this->createResourceQuotaTestFormData(self::K8S_RESOURCE_QUOTA_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addResourceQuotaMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/resource_quota/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Resource quota', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/resource_quota");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all resource_quota listing exists.
      $this->drupalGet('/clouds/k8s/resource_quota');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/resource_quota/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a resource quota.
    $edit = $this->createResourceQuotaTestFormData(self::K8S_RESOURCE_QUOTA_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++, $num++) {

      $this->updateResourceQuotaMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/resource_quota/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Resource quota', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/resource_quota/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete resource quota.
    for ($i = 0, $num = 1; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++, $num++) {

      $this->deleteResourceQuotaMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/resource_quota/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Resource quota', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/resource_quota");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting resource quotas with bulk operation.
   *
   * @throws \Exception
   */
  public function testResourceQuotaBulk(): void {

    for ($i = 0; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++) {
      // Create resource quotas.
      $resource_quotas = $this->createResourceQuotasRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($resource_quotas ?: [] as $resource_quota) {
        $entities[] = $this->createResourceQuotaTestEntity($resource_quota);
      }
      $this->deleteResourceQuotaMockData($resource_quotas[0]);
      $this->runTestEntityBulk('resource_quota', $entities);
    }
  }

  /**
   * Test updating all resource quota list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllResourceQuotaList(): void {
    $cloud_configs = [];

    // List resource quota for K8s.
    $this->drupalGet('/clouds/k8s/resource_quota');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new resource quota.
      $add = $this->createResourceQuotaTestFormData(self::K8S_RESOURCE_QUOTA_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++, $num++) {
        $this->addResourceQuotaMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List resource quota for K8s.
    $this->drupalGet('/clouds/k8s/resource_quota');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Resource quotas',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a resource quota.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createResourceQuotaTestFormData(self::K8S_RESOURCE_QUOTA_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++, $num++) {

        $this->updateResourceQuotaMockData($edit[$i]);

        // Change resource quota name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/resource_quota');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm resource quotas are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Resource quotas',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/resource_quota');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_RESOURCE_QUOTA_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
