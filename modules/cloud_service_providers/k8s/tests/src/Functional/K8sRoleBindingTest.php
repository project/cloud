<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s role binding.
 *
 * @group K8s
 */
class K8sRoleBindingTest extends K8sTestBase {

  public const K8S_ROLE_BINDING_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s role binding',
      'view own k8s role binding',
      'edit any k8s role binding',
      'add k8s role binding',
      'delete any k8s role binding',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for role binding.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testRoleBinding(): void {

    $cloud_context = $this->cloudContext;

    // List role binding for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/role_binding");
    $this->assertNoErrorMessage();

    // Add a new role binding.
    $add = $this->createRoleBindingTestFormData(self::K8S_ROLE_BINDING_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addRoleBindingMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/role_binding/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Role binding', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/role_binding");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all role_binding listing exists.
      $this->drupalGet('/clouds/k8s/role_binding');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/role_binding/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a role binding.
    $edit = $this->createRoleBindingTestFormData(self::K8S_ROLE_BINDING_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++, $num++) {

      $this->updateRoleBindingMockData($edit[$i], $cloud_context, $this->webUser->id());

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/role_binding/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Role binding', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/role_binding/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete role binding.
    for ($i = 0, $num = 1; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++, $num++) {

      $this->deleteRoleBindingMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/role_binding/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Role binding', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/role_binding");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting role bindings with bulk operation.
   *
   * @throws \Exception
   */
  public function testRoleBindingBulk(): void {

    for ($i = 0; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++) {
      // Create  Roles Binding.
      $role_bindings = $this->createRoleBindingsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($role_bindings ?: [] as $role_binding) {
        $entities[] = $this->createRoleBindingTestEntity($role_binding);
      }
      $this->deleteRoleBindingMockData($role_bindings[0]);
      $this->runTestEntityBulk('role_binding', $entities);
    }
  }

  /**
   * Test updating all role binding list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllRoleBindingList(): void {
    $cloud_configs = [];

    // List role binding for K8s.
    $this->drupalGet('/clouds/k8s/role_binding');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new role binding.
      $add = $this->createRoleBindingTestFormData(self::K8S_ROLE_BINDING_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++, $num++) {
        $this->addRoleBindingMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List role binding for K8s.
    $this->drupalGet('/clouds/k8s/role_binding');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Role bindings',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a role binding.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $edit = $this->createRoleBindingTestFormData(self::K8S_ROLE_BINDING_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++, $num++) {

        $this->updateRoleBindingMockData($edit[$i], $cloud_context, $this->webUser->id());

        // Change role binding name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/role_binding');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm role bindings are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Role bindings',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/role_binding');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_ROLE_BINDING_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
