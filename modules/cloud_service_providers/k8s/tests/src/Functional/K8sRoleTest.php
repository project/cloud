<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s role.
 *
 * @group K8s
 */
class K8sRoleTest extends K8sTestBase {

  public const K8S_ROLE_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s role',
      'view any k8s role',
      'edit any k8s role',
      'add k8s role',
      'delete any k8s role',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for Role.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testRole(): void {

    $cloud_context = $this->cloudContext;

    // List Role for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/role");
    $this->assertNoErrorMessage();

    // Add a new Role.
    $add = $this->createRoleTestFormData(self::K8S_ROLE_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_ROLE_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addRoleMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/role/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Role', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/role");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_ROLE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all role listing exists.
      $this->drupalGet('/clouds/k8s/role');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/role/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Role.
    $edit = $this->createRoleTestFormData(self::K8S_ROLE_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_ROLE_REPEAT_COUNT; $i++, $num++) {

      $this->updateRoleMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/role/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Role', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/role/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete Role.
    for ($i = 0, $num = 1; $i < self::K8S_ROLE_REPEAT_COUNT; $i++, $num++) {

      $this->deleteRoleMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/role/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Role', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/role");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting roles with bulk operation.
   *
   * @throws \Exception
   */
  public function testRoleBulk(): void {

    for ($i = 0; $i < self::K8S_ROLE_REPEAT_COUNT; $i++) {
      // Create Roles.
      $roles = $this->createRolesRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($roles ?: [] as $role) {
        $entities[] = $this->createRoleTestEntity($role);
      }
      $this->deleteRoleMockData($roles[0]);
      $this->runTestEntityBulk('role', $entities);
    }
  }

  /**
   * Test updating all role list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllRoleList(): void {
    $cloud_configs = [];

    // List Role for K8s.
    $this->drupalGet('/clouds/k8s/role');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_ROLE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Role.
      $add = $this->createRoleTestFormData(self::K8S_ROLE_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_ROLE_REPEAT_COUNT; $i++, $num++) {
        $this->addRoleMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List Role for K8s.
    $this->drupalGet('/clouds/k8s/role');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Roles',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a Role.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createRoleTestFormData(self::K8S_ROLE_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_ROLE_REPEAT_COUNT; $i++, $num++) {

        $this->updateRoleMockData($edit[$i]);

        // Change role name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/role');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_ROLE_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Roles are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Roles',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/role');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
