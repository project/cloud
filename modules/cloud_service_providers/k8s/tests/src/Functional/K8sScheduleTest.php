<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s schedule.
 *
 * @group K8s
 */
class K8sScheduleTest extends K8sTestBase {

  public const K8S_SCHEDULE_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    // Setup namespaces.
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->addNamespaceMockData($namespaces[0], $this->cloudContext, !empty($this->webUser) ? $this->webUser->id() : 0);
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];
    $this->getNamespaceMockData($namespaces[0]);

    return [
      'view all cloud service providers',
      'view k8s namespace ' . $this->namespace,

      // Schedule.
      'list k8s schedule',
      'view any k8s schedule',
      'edit any k8s schedule',
      'add k8s schedule',
      'delete any k8s schedule',

      // Pod.
      'list k8s pod',
      'add k8s pod',
      'view any k8s pod',
      'edit any k8s pod',
      'delete any k8s pod',

      // Deployment.
      'list k8s deployment',
      'add k8s deployment',
      'view any k8s deployment',
      'edit any k8s deployment',
      'delete any k8s deployment',

      // Launch template.
      'add cloud server templates',
      'list cloud server template',
      'view any published cloud server templates',
      'launch cloud server template',
      'edit any cloud server templates',
      'delete any cloud server templates',
      'launch approved cloud server template',

      'approve launch k8s resources',
    ];
  }

  /**
   * Tests CRUD for Schedule of pod.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testScheduleForPod(): void {
    $this->runTestForObject('pod');
  }

  /**
   * Tests CRUD for Schedule of deployment.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testScheduleForDeployment(): void {
    $this->runTestForObject('deployment');
  }

  /**
   * Runs tests for object.
   *
   * @param string $object_type
   *   The object type.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  private function runTestForObject($object_type): void {
    $object_type_capital = ucfirst($object_type);
    $cloud_context = $this->cloudContext;

    // List Schedule for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/schedule");
    $this->assertNoErrorMessage();

    // Add a new Schedule.
    $method_name = "create{$object_type_capital}TestFormData";
    $add = $this->$method_name(self::K8S_SCHEDULE_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_SCHEDULE_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $method_name = "add{$object_type_capital}MockData";
      $this->$method_name($add[$i], $cloud_context, $this->webUser->id());

      $current_hour = date('G');
      $this->drupalGet("/clouds/k8s/$cloud_context/$object_type/add");
      $this->submitForm(
        array_merge($add[$i]['post_data'], [
          'enable_time_scheduler' => TRUE,
          'start_hour' => $current_hour,
          'stop_hour' => ((int) $current_hour + 1) % 24,
        ]),
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => $object_type_capital, '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/schedule");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['post_data']['namespace'] . '_' . $add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_SCHEDULE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all schedule listing exists.
      $this->drupalGet('/clouds/k8s/schedule');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['post_data']['namespace'] . '_' . $add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/schedule/$num");
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Schedule.
    $edit = $this->createScheduleTestFormData(self::K8S_SCHEDULE_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_SCHEDULE_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/k8s/$cloud_context/schedule/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Schedule',
        '%label' => $add[$i]['post_data']['namespace'] . '_' . $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/schedule/$num");
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete Schedule.
    for ($i = 0, $num = 1; $i < self::K8S_SCHEDULE_REPEAT_COUNT; $i++, $num++) {
      $method_name = "delete{$object_type_capital}MockData";
      $this->$method_name($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/schedule/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Schedule',
        '@label' => $add[$i]['post_data']['namespace'] . '_' . $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));
      $t_args = [
        '@type' => $object_type_capital,
        '@label' => $add[$i]['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/schedule");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['post_data']['namespace'] . '_' . $add[$i]['name']);
      $this->drupalGet("/clouds/k8s/$cloud_context/$object_type_capital");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests CRUD for Schedule of pod template.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testScheduleForPodTemplate(): void {
    $this->runTestForTemplate('pod');
  }

  /**
   * Tests CRUD for Schedule of deployment template.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testScheduleForDeploymentTemplate(): void {
    $this->runTestForTemplate('deployment');
  }

  /**
   * Runs tests for template.
   *
   * @param string $object_type
   *   The object type.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  private function runTestForTemplate($object_type): void {
    $cloud_context = $this->cloudContext;

    $object_type_capital = ucfirst($object_type);

    $method_name = "create{$object_type_capital}TestFormData";
    $data = $this->$method_name(self::K8S_SCHEDULE_REPEAT_COUNT, $this->namespace);
    $add = $this->createLaunchTemplateTestFormData($data, 'yml', $object_type, [], self::K8S_SCHEDULE_REPEAT_COUNT);

    $current_hour = date('G');

    for ($i = 0; $i < self::K8S_SCHEDULE_REPEAT_COUNT; $i++) {
      // field_object is automatically calculated.
      unset($add[$i]['field_object']);
      $this->drupalGet("/clouds/design/server_template/$cloud_context/k8s/add");
      $this->submitForm(
        array_merge($add[$i], [
          'field_enable_time_scheduler[value]' => 1,
          'field_startup_time_hour' => $current_hour,
          'field_stop_time_hour' => ((int) $current_hour + 1) % 24,
        ]),
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
    }

    // Launch the templates.
    for ($i = 0; $i < self::K8S_SCHEDULE_REPEAT_COUNT; $i++) {
      // Update the mock data.
      if ($object_type === 'pod') {
        $this->getMetricsPodMockData([]);
      }

      $method_name = "add{$object_type_capital}MockData";
      $this->$method_name($data[$i], $cloud_context, $this->webUser->id());

      $template_id = $i + 1;
      $this->drupalGet("/clouds/design/server_template/$cloud_context/$template_id/launch");

      $this->submitForm(
        [],
        $this->t('Launch')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => ucfirst($object_type),
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been launched.', $t_args)));
    }

    // Confirm schedules.
    for ($i = 0, $num = 1; $i < self::K8S_SCHEDULE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all schedule listing exists.
      $this->drupalGet('/clouds/k8s/schedule');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $name = $add[$j]['field_namespace'] . '_' . $add[$j]['name[0][value]'];
        $this->assertSession()->pageTextContains($name);
      }
    }

    // Edit a Schedule.
    $edit = $this->createScheduleTestFormData(self::K8S_SCHEDULE_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_SCHEDULE_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/k8s/$cloud_context/schedule/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();
      $name = $add[$i]['field_namespace'] . '_' . $add[$i]['name[0][value]'];
      $t_args = [
        '@type' => 'Schedule',
        '%label' => $name,
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
    }

    // Delete Schedule.
    for ($i = 0, $num = 1; $i < self::K8S_SCHEDULE_REPEAT_COUNT; $i++, $num++) {
      $method_name = "delete{$object_type_capital}MockData";
      $this->$method_name($data[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/schedule/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $name = $add[$i]['field_namespace'] . '_' . $add[$i]['name[0][value]'];
      $t_args = [
        '@type' => 'Schedule',
        '@label' => $name,
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/schedule");
      $this->assertNoErrorMessage();
      $name = $add[$i]['field_namespace'] . '_' . $add[$i]['name[0][value]'];
      $this->assertSession()->pageTextNotContains($name);
      $this->drupalGet("/clouds/k8s/$cloud_context/$object_type_capital");
      $this->assertNoErrorMessage();
      $name = $add[$i]['name[0][value]'];
      $this->assertSession()->pageTextNotContains($name);
    }
  }

  /**
   * Tests deleting schedules with bulk operation.
   *
   * @throws \Exception
   */
  public function testScheduleBulk(): void {

    for ($i = 0; $i < self::K8S_SCHEDULE_REPEAT_COUNT; $i++) {
      // Create Schedules.
      $schedules = $this->createSchedulesRandomTestFormData();
      $entities = [];
      foreach ($schedules ?: [] as $schedule) {
        $entities[] = $this->createScheduleTestEntity($schedule);
      }
      // $this->deleteScheduleMockData($schedules[0]);
      $this->runTestEntityBulk('schedule', $entities);
    }
  }

}
