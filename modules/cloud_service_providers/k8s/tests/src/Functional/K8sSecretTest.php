<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s secret.
 *
 * @group K8s
 */
class K8sSecretTest extends K8sTestBase {

  public const K8S_SECRET_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s secret',
      'view any k8s secret',
      'edit any k8s secret',
      'add k8s secret',
      'delete any k8s secret',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for Secret.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testSecret(): void {

    $cloud_context = $this->cloudContext;

    // List Secret for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/secret");
    $this->assertNoErrorMessage();

    // Add a new Secret.
    $add = $this->createSecretTestFormData(self::K8S_SECRET_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_SECRET_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addSecretMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/secret/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Secret', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/secret");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_SECRET_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all secret listing exists.
      $this->drupalGet('/clouds/k8s/secret');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/secret/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Secret.
    $edit = $this->createSecretTestFormData(self::K8S_SECRET_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_SECRET_REPEAT_COUNT; $i++, $num++) {

      $this->updateSecretMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/secret/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Secret', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/secret/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete Secret.
    for ($i = 0, $num = 1; $i < self::K8S_SECRET_REPEAT_COUNT; $i++, $num++) {

      $this->deleteSecretMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/secret/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Secret', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/secret");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting secrets with bulk operation.
   *
   * @throws \Exception
   */
  public function testSecretBulk(): void {

    for ($i = 0; $i < self::K8S_SECRET_REPEAT_COUNT; $i++) {
      // Create Secrets.
      $secrets = $this->createSecretsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($secrets ?: [] as $secret) {
        $entities[] = $this->createSecretTestEntity($secret);
      }
      $this->deleteSecretMockData($secrets[0]);
      $this->runTestEntityBulk('secret', $entities);
    }
  }

  /**
   * Test updating all Secret list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllSecretList(): void {
    $cloud_configs = [];

    // List Secret for K8s.
    $this->drupalGet('/clouds/k8s/secret');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_SECRET_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Secret.
      $add = $this->createSecretTestFormData(self::K8S_SECRET_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_SECRET_REPEAT_COUNT; $i++, $num++) {
        $this->addSecretMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List Secret for K8s.
    $this->drupalGet('/clouds/k8s/secret');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Secrets',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_SECRET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a Secret.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createSecretTestFormData(self::K8S_SECRET_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_SECRET_REPEAT_COUNT; $i++, $num++) {

        $this->updateSecretMockData($edit[$i]);

        // Change secret name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/secret');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_SECRET_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Secrets are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Secrets',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/secret');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_SECRET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
