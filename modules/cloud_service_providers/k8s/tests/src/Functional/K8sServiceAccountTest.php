<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s ServiceAccount.
 *
 * @group K8s
 */
class K8sServiceAccountTest extends K8sTestBase {

  public const K8S_SERVICE_ACCOUNT_REPEAT_COUNT = 3;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s service account',
      'add k8s service account',
      'view any k8s service account',
      'edit any k8s service account',
      'delete any k8s service account',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for K8s ServiceAccount.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testServiceAccount(): void {

    $cloud_context = $this->cloudContext;

    // List K8s ServiceAccounts.
    $this->drupalGet("/clouds/k8s/$cloud_context/service_account");
    $this->assertNoErrorMessage();

    // Add a new K8s ServiceAccount.
    $add = $this->createServiceAccountTestFormData(self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addServiceAccountMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/service_account/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'ServiceAccount', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/service_account");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all service_account listing exists.
      $this->drupalGet('/clouds/k8s/service_account');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/service_account/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a K8s ServiceAccount.
    $edit = $this->createServiceAccountTestFormData(self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++, $num++) {

      $this->updateServiceAccountMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/service_account/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'ServiceAccount', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/service_account/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete one or more K8s ServiceAccount(s).
    for ($i = 0, $num = 1; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++, $num++) {

      $this->deleteServiceAccountMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/service_account/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'ServiceAccount', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/service_account");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting a K8s ServiceAccount with bulk operation.
   *
   * @throws \Exception
   */
  public function testServiceAccountBulk(): void {

    for ($i = 0; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++) {
      // Create a K8s ServiceAccount.
      $service_accounts = $this->createServiceAccountsRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($service_accounts ?: [] as $service_account) {
        $entities[] = $this->createServiceAccountTestEntity($service_account);
      }
      $this->deleteServiceAccountMockData($service_accounts[0]);
      $this->runTestEntityBulk('service_account', $entities);
    }
  }

  /**
   * Test updating all K8s ServiceAccount list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllServiceAccountList(): void {
    $cloud_configs = [];

    // List K8s ServiceAccounts.
    $this->drupalGet('/clouds/k8s/service_account');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new K8s ServiceAccount.
      $add = $this->createServiceAccountTestFormData(self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++, $num++) {
        $this->addServiceAccountMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List K8s ServiceAccounts.
    $this->drupalGet('/clouds/k8s/service_account');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'ServiceAccounts',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[2]['name']);
    }

    // Edit a K8s ServiceAccount.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createServiceAccountTestFormData(self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++, $num++) {

        $this->updateServiceAccountMockData($edit[$i]);

        // Change a K8s ServiceAccount name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/service_account');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm K8s ServiceAccounts are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'ServiceAccounts',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/service_account');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_SERVICE_ACCOUNT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[2]['name']);
    }
  }

}
