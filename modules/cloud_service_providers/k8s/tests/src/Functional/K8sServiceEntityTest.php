<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s service.
 *
 * @group K8s
 */
class K8sServiceEntityTest extends K8sTestBase {

  public const K8S_SERVICE_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $namespaces = $this->createNamespacesRandomTestFormData();
    $this->createNamespaceTestEntity($namespaces[0]);
    $this->namespace = $namespaces[0]['name'];

    return [
      'view all cloud service providers',
      'list k8s service',
      'view any k8s service',
      'edit any k8s service',
      'add k8s service',
      'delete any k8s service',
      'view k8s namespace ' . $this->namespace,
    ];
  }

  /**
   * Tests CRUD for Service.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testService(): void {

    $cloud_context = $this->cloudContext;

    // List Service for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/service");
    $this->assertNoErrorMessage();

    // Add a new Service.
    $add = $this->createServiceTestFormData(self::K8S_SERVICE_REPEAT_COUNT, $this->namespace);
    for ($i = 0; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addServiceMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/service/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Service', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/service");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all service listing exists.
      $this->drupalGet('/clouds/k8s/service');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/service/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a Service.
    $edit = $this->createServiceTestFormData(self::K8S_SERVICE_REPEAT_COUNT, $this->namespace);
    for ($i = 0, $num = 1; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++, $num++) {

      $this->updateServiceMockData($edit[$i]);

      unset($edit[$i]['post_data']['namespace']);
      $this->drupalGet("/clouds/k8s/$cloud_context/service/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Service', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/service/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete Service.
    for ($i = 0, $num = 1; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++, $num++) {

      $this->deleteServiceMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/service/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Service', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/service");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting services with bulk operation.
   *
   * @throws \Exception
   */
  public function testServiceBulk(): void {

    for ($i = 0; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++) {
      // Create services.
      $services = $this->createServicesRandomTestFormData($this->namespace);
      $entities = [];
      foreach ($services ?: [] as $service) {
        $entities[] = $this->createServiceTestEntity($service);
      }
      $this->deleteServiceMockData($services[0]);
      $this->runTestEntityBulk('service', $entities);
    }
  }

  /**
   * Test updating all service list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllServiceList(): void {
    $cloud_configs = [];

    // List Service for K8s.
    $this->drupalGet('/clouds/k8s/service');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new Service.
      $add = $this->createServiceTestFormData(self::K8S_SERVICE_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++, $num++) {
        $this->addServiceMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List Service for K8s.
    $this->drupalGet('/clouds/k8s/service');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Services',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a Service.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createServiceTestFormData(self::K8S_SERVICE_REPEAT_COUNT, $this->namespace);
      for ($i = 0, $num = 1; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++, $num++) {

        $this->updateServiceMockData($edit[$i]);

        // Change service name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/service');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Services are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Services',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/service');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_SERVICE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
