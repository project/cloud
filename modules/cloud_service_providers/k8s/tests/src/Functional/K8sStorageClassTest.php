<?php

namespace Drupal\Tests\k8s\Functional;

/**
 * Tests K8s storage class.
 *
 * @group K8s
 */
class K8sStorageClassTest extends K8sTestBase {

  public const K8S_STORAGE_CLASS_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list k8s storage class',
      'view any k8s storage class',
      'edit any k8s storage class',
      'add k8s storage class',
      'delete any k8s storage class',
    ];
  }

  /**
   * Tests CRUD for storage class.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testStorageClass(): void {

    $cloud_context = $this->cloudContext;

    // List storage class for K8s.
    $this->drupalGet("/clouds/k8s/$cloud_context/storage_class");
    $this->assertNoErrorMessage();

    // Add a new storage class.
    $add = $this->createStorageClassTestFormData(self::K8S_STORAGE_CLASS_REPEAT_COUNT);
    for ($i = 0; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addStorageClassMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/k8s/$cloud_context/storage_class/add");
      $this->submitForm(
        $add[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Storage class', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/storage_class");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all storage_class listing exists.
      $this->drupalGet('/clouds/k8s/storage_class');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/storage_class/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Edit a storage class.
    $edit = $this->createStorageClassTestFormData(self::K8S_STORAGE_CLASS_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++, $num++) {
      $this->updateStorageClassMockData($edit[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/storage_class/$num/edit");
      $this->submitForm(
        $edit[$i]['post_data'],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Storage class', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      // Make sure uid.
      $this->drupalGet("/clouds/k8s/$cloud_context/storage_class/$num");
      $this->assertSession()->pageTextContains(
        $this->cloudService->getTagKeyCreatedByUid(
          'k8s',
          $cloud_context
        )
      );
      $this->assertSession()->pageTextContains(
        $this->webUser->getAccountName()
      );
    }

    // Delete storage class.
    for ($i = 0, $num = 1; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++, $num++) {

      $this->deleteStorageClassMockData($add[$i]);

      $this->drupalGet("/clouds/k8s/$cloud_context/storage_class/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Storage class', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/k8s/$cloud_context/storage_class");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting storage classes with bulk operation.
   *
   * @throws \Exception
   */
  public function testStorageClassBulk(): void {

    for ($i = 0; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++) {
      // Create storage classes.
      $storage_classes = $this->createStorageClassesRandomTestFormData();
      $entities = [];
      foreach ($storage_classes ?: [] as $storage_class) {
        $entities[] = $this->createStorageClassTestEntity($storage_class);
      }
      $this->deleteStorageClassMockData($storage_classes[0]);
      $this->runTestEntityBulk('storage_class', $entities);
    }
  }

  /**
   * Test updating all storage class list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllStorageClassList(): void {
    $cloud_configs = [];

    // List storage class for K8s.
    $this->drupalGet('/clouds/k8s/storage_class');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Add a new storage class.
      $add = $this->createStorageClassTestFormData(self::K8S_STORAGE_CLASS_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++, $num++) {
        $this->addStorageClassMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // List storage class for K8s.
    $this->drupalGet('/clouds/k8s/storage_class');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Storage classes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[1]['name']);
    }

    // Edit a storage class.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $edit = $this->createStorageClassTestFormData(self::K8S_STORAGE_CLASS_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++, $num++) {

        $this->updateStorageClassMockData($edit[$i]);

        // Change storage class name in mock data.
        $add[$i]['name'] = "name-{$this->random->name(8, TRUE)}";
      }

      // Make sure listing.
      $this->drupalGet('/clouds/k8s/storage_class');
      $this->assertNoErrorMessage();

      for ($i = 0; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++) {
        $this->assertSession()->pageTextNotContains($add[$i]['name']);
      }
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm storage classes are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Storage classes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/k8s/storage_class');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::K8S_STORAGE_CLASS_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($edit[1]['name']);
    }
  }

}
