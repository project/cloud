<?php

namespace Drupal\Tests\k8s\Functional\cloud\cloud_store;

use Drupal\Component\Serialization\Yaml;
use Drupal\Tests\k8s\Functional\K8sTestBase;
use Drupal\cloud\Entity\CloudStore;
use Drupal\cloud\Entity\CloudStoreType;
use Drupal\k8s\Controller\K8sCostsControllerBase;

/**
 * Test K8s cloud store(K8sCloudStoreTest).
 *
 * @group K8s
 */
class K8sCloudStoreTest extends K8sTestBase {

  public const K8S_CLOUD_STORE_REPEAT_COUNT = 3;

  /**
   * Bundle for cloud store.
   *
   * @var string
   */
  protected $cloudStoreType;

  /**
   * The project.
   *
   * @var array
   */
  protected $project;

  /**
   * Create a bundle for cloud store.
   */
  protected function createCloudStoreType(): array {
    $random = $this->random;
    $params = [
      'id' => $random->name(8),
      'label' => 'cloud-store-' . $random->name(8, TRUE),
    ];
    $entity = CloudStoreType::create($params);
    $entity->save();

    return $params;
  }

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    // Add a bundle for cloud_store test.
    $data = $this->createCloudStoreType();
    $this->cloudStoreType = $data['id'];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getPermissions():array {
    $this->project = $this->createProjectTestFormData(1);
    $namespaces['name'] = $this->project[0]['name[0][value]'];
    $this->createNamespaceTestEntity($namespaces);
    $this->namespace = $namespaces['name'];

    return [
      'administer site configuration',
      'administer users',
      'administer cloud stores',
      'administer cloud service providers',
      'add cloud service providers',
      'edit cloud service providers',
      'edit own cloud service providers',
      'delete cloud service providers',
      'delete own cloud service providers',
      'view published cloud service providers',
      'view own published cloud service providers',
      'view all cloud service providers',
      'access dashboard',
      'list cloud store',
      'add cloud stores',
      'delete any cloud stores',
      'delete own cloud stores',
      'edit any cloud stores',
      'edit own cloud stores',
      'view any published cloud stores',
      'view own published cloud stores',
      'access cloud store revisions',
      'revert all cloud store revisions',
      'delete all cloud store revisions',
      'list k8s pod',
      'add k8s pod',
      'view any k8s pod',
      'edit any k8s pod',
      'delete any k8s pod',
      'list k8s namespace',
      'view any k8s namespace',
      'edit any k8s namespace',
      'add k8s namespace',
      'delete any k8s namespace',
      'launch cloud project',
      'add cloud projects',
      'list cloud project',
      'delete any cloud projects',
      'edit any cloud projects',
      'view any published cloud projects',
      'access cloud project revisions',
      'revert all cloud project revisions',
      'delete all cloud project revisions',
    ];
  }

  /**
   * CRUD test for k8s cloud store.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testK8sCloudStoreCrudTest(): void {
    $cloud_store_type = $this->cloudStoreType;

    // Check cloud store type.
    $this->drupalGet('/admin/structure/cloud_store_type');
    $this->assertSession()->pageTextContains($cloud_store_type);

    // Create new data.
    $add = $this->createCloudStoreTestFormData(self::K8S_CLOUD_STORE_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::K8S_CLOUD_STORE_REPEAT_COUNT; $i++, $num++) {
      $this->reloadMockData();
      $label = $add[$i]['name[0][value]'];
      $label_edit = $label . '-edit';
      $label_copy = $label . '-copy';

      // Add data.
      $this->drupalGet("/clouds/design/store/$cloud_store_type/add");
      $this->assertNoErrorMessage();
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );

      // Assertion check.
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($label);

      // Edit data.
      $edit = $add[$i];
      $edit['name[0][value]'] = $label_edit;

      // Go to edit page.
      $this->drupalGet("/clouds/design/store/$cloud_store_type/$num/edit");
      $this->assertNoErrorMessage();
      $this->submitForm(
        $edit,
        $this->t('Save')->render()
      );

      // Edit data.
      $copy = $add[$i];
      $copy['name[0][value]'] = $label_copy;

      // Assertion check.
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($label_edit);

      // Go to copy page.
      $this->drupalGet("/clouds/design/store/$cloud_store_type/$num/copy");
      $this->assertNoErrorMessage();
      $this->submitForm(
        $copy,
        $this->t('Copy')->render());

      // Go to list page.
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($label_copy);

      // Delete data.
      $this->drupalGet("/clouds/design/store/$cloud_store_type/$num/delete");
      $this->clickLink('Delete', 0);
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );

      // Go to a deleted page.
      $this->drupalGet("/clouds/design/store/$cloud_store_type/$num/delete");
      $this->assertSession()->statusCodeEquals(404);
    }
  }

  /**
   * Update k8s_namespace_resource store test.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testK8sUpdateResourceStorageTest(): void {
    // Set parameters.
    $cloud_context = $this->cloudContext;
    $k8s_service = \Drupal::service('k8s');
    $plugin = \Drupal::service('plugin.manager.cloud_store_plugin');

    // Create entity.
    $nodes = $this->createNodeTestFormData(self::K8S_CLOUD_STORE_REPEAT_COUNT);
    $pods = $this->createPodsRandomTestFormData($this->namespace);
    foreach ($pods ?: [] as $pod) {
      $this->createPodTestEntity($pod);
    }
    foreach ($nodes ?: [] as $node) {
      $this->createNodeTestEntity($node);
    }

    // Add mock data.
    $this->getNamespaceResourceUsageMockData($pods);
    $this->getNodeResourceUsageMockData($nodes);
    $this->calculateCostPerNamespaceMockData();

    // Get result.
    $result_node = $k8s_service->getNodeResourceUsage($cloud_context);
    $result_pod = $k8s_service->getNamespaceResourceUsage($cloud_context, $this->namespace);
    $result = $k8s_service->calculateCostPerNamespace($cloud_context, $this->namespace);

    $this->assertEquals($result['resources']['cpu'], $result_pod['cpu_usage']);
    $this->assertEquals($result['resources']['memory'], $result_pod['memory_usage']);
    $this->assertEquals($result['resources']['cpu_capacity'], $result_node['cpu_capacity']);
    $this->assertEquals($result['resources']['memory_capacity'], $result_node['memory_capacity']);

    // Process item in queue.
    $queue_name = 'k8s_update_resources_queue:' . $cloud_context;
    /** @var \Drupal\Core\Queue\QueueWorkerInterface $queue_worker */
    $queue_worker = \Drupal::service('plugin.manager.queue_worker')->createInstance($queue_name);
    $queue = \Drupal::queue($queue_name);
    // Clear the queue just in case.
    $queue->deleteQueue();

    // Update resource storage.
    $definitions = $plugin->getDefinitions();
    if (array_key_exists('k8s.cloud.cloud_store.k8s_cost_store:k8s_cost_store', $definitions)) {
      $plugin->createInstance('k8s.cloud.cloud_store.k8s_cost_store:k8s_cost_store')->updateK8sResourceStoreEntity();
    }

    // Loop through the items and process them.
    $item = $queue->claimItem();
    self::assertSame($item->data['k8s_method_name'], 'updateK8sNamespaceResourceStoreEntity',
      'Queue item that was imported function does not belong to the correct function.');
    $queue_worker->processItem($item->data);
    $queue->deleteItem($item);

    self::assertSame(0, $queue->numberOfItems(), 'Item was not removed from the queue.');

    // Access the list.
    $this->drupalGet('/clouds/design/store/k8s_namespace_resource_store');
    $this->assertSession()->pageTextContains($this->namespace);

    // Get data from test entity.
    $resource_entities = \Drupal::entityTypeManager()
      ->getStorage('cloud_store')
      ->loadByProperties([]);

    $resources = 0;
    foreach ($resource_entities ?: [] as $entity) {
      $resources = Yaml::decode($entity->get('field_resources')->value);
    }

    $this->assertEquals(round($resources['cpu'], 3), round($result_pod['cpu_usage'], 3));
    $this->assertEquals(round($resources['memory'], 3), round($result_pod['memory_usage'], 3));
  }

  /**
   * K8s cost resource store test.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testK8sCostResourceStoreTest(): void {
    // Set parameters.
    $cloud_context = $this->cloudContext;
    $k8s_service = \Drupal::service('k8s');

    // Create entity.
    $nodes = $this->createNodeTestFormData(self::K8S_CLOUD_STORE_REPEAT_COUNT);
    $pods = $this->createPodsRandomTestFormData($this->namespace);
    foreach ($pods ?: [] as $pod) {
      $this->createPodTestEntity($pod);
    }
    foreach ($nodes ?: [] as $node) {
      $this->createNodeTestEntity($node);
    }

    // Add mock data.
    $this->getNamespaceResourceUsageMockData($pods);
    $this->getNodeResourceUsageMockData($nodes);
    $this->calculateCostPerNamespaceMockData();

    // Scenario No.1:
    // No project entity will expect no update to cost store entity.
    // Update cost store.
    $this->callUpdateCostStoreEntity();

    // Get result.
    $cost_entities = \Drupal::entityTypeManager()
      ->getStorage('cloud_store')
      ->loadByProperties([
        'type' => 'k8s_cost_store',
      ]);

    // Assertion.
    $this->assertEquals(count($cost_entities), 0);

    // To access to check.
    $this->drupalGet('/clouds/design/store/k8s_namespace_resource_store');
    $this->assertSession()->pageTextNotContains($this->namespace);

    // Scenario No.2: If project entity exists and no resource entity,
    // it will expect updating namespace_resource_store entity.
    // Prepare project entity.
    $this->project['name'] = $this->namespace;
    $this->project['cloud_context'] = $cloud_context;
    $this->project['field_k8s_clusters'][0]['value'] = $cloud_context;
    $this->createProjectTestEntity($this->project);

    // To access to check.
    $this->drupalGet("/clouds/design/project/$cloud_context");
    $this->assertSession()->pageTextContains($this->namespace);

    // Update cost store.
    $this->callUpdateCostStoreEntity();

    // Get result.
    $resource_entities = \Drupal::entityTypeManager()
      ->getStorage('cloud_store')
      ->loadByProperties([
        'type' => 'k8s_namespace_resource_store',
      ]);

    // Assertion.
    $this->assertEquals(count($resource_entities), 1);

    // To access to check.
    $this->drupalGet('/clouds/design/store/k8s_namespace_resource_store');
    $this->assertSession()->pageTextContains($this->namespace);

    // Scenario No.3:
    // If latest cost storage entity updated within 1 hour exists,
    // it will expect skipping the update of cost store.
    // Prepare k8s_namespace_store entity with nearby current time.
    $cost_store = [
      'name' => $this->namespace,
      'type' => 'k8s_cost_store',
      'cloud_context' => $this->cloudContext,
      'field_costs' => 0,
      'field_resources' => 0,
      'created' => time() - random_int(1, 60 * 59),
    ];
    $entity = CloudStore::create($cost_store);
    $entity->save();

    $this->drupalGet('/clouds/design/store/k8s_namespace_resource_store');
    $this->assertSession()->pageTextContains($this->namespace);

    // Update cost store.
    $this->callUpdateCostStoreEntity();

    // Get result.
    $cost_entities = \Drupal::entityTypeManager()
      ->getStorage('cloud_store')
      ->loadByProperties([
        'name' => $this->namespace,
        'type' => 'k8s_cost_store',
        'cloud_context' => $this->cloudContext,
      ]);

    // Assertion.
    $this->assertEquals(count($cost_entities), 1);

    // To access to check.
    $this->drupalGet('/clouds/design/store/k8s_cost_store');
    $this->assertSession()->pageTextContains($this->namespace);

    // Scenario No.4:
    // If latest cost storage entity updated within 1 hour does not exist,
    // it will expect updating cost storage referring resource storage.
    // Clean cost entity used in Scenario No.3.
    foreach ($resource_entities ?: [] as $resource_entity) {
      $resource_entity->delete();
    }
    foreach ($cost_entities ?: [] as $cost_entity) {
      $cost_entity->delete();
    }

    // Prepare for cost and resource storage updated over one hour before.
    $cost_store = [
      'name' => $this->namespace,
      'type' => 'k8s_cost_store',
      'cloud_context' => $this->cloudContext,
      'field_costs' => 0,
      'field_resources' => 0,
      'created' => time() - random_int(60 * 60 * 5, 60 * 60 * 8),
    ];
    $entity = CloudStore::create($cost_store);
    $entity->save();

    $result = $k8s_service->calculateCostPerNamespace($cloud_context, $this->namespace);
    $resource_storage = [
      'cloud_context' => $cloud_context,
      'name' => $this->namespace,
      'type' => 'k8s_namespace_resource_store',
      'field_costs' => Yaml::encode($result['costs']),
      'field_resources' => Yaml::encode($result['resources']),
      'created' => time() - random_int(60 * 60 * 2, 60 * 60 * 5),
    ];
    $entity = CloudStore::create($resource_storage);
    $entity->save();

    // To access to check.
    $this->drupalGet('/clouds/design/store/k8s_namespace_resource_store');
    $this->assertSession()->pageTextContains($this->namespace);

    // Update cost store.
    $this->callUpdateCostStoreEntity();

    // Get result.
    $cost_entities = \Drupal::entityTypeManager()
      ->getStorage('cloud_store')
      ->loadByProperties([
        'name' => $this->namespace,
        'type' => 'k8s_cost_store',
        'cloud_context' => $this->cloudContext,
      ]);

    // To access to check.
    $this->drupalGet('/clouds/design/store/k8s_cost_store');
    $this->assertSession()->pageTextContains($this->namespace);

    // Assertion.
    $this->assertEquals(count($cost_entities), 2);

    // Fetch the cost_entity with field_costs.
    $cost_entity = NULL;
    foreach ($cost_entities ?: [] as $entity) {
      if (!empty($entity->get('field_costs')->value)) {
        $cost_entity = $entity;
        break;
      }
    }
    if (empty($cost_entity)) {
      echo 'Warning: $cost_entity object is NULL';
    }

    $costs = Yaml::decode(!empty($cost_entity->get('field_costs')->value) ? $cost_entity->get('field_costs')->value : '');
    $controller = \Drupal::classResolver()->getInstanceFromDefinition(K8sCostsControllerBase::class);
    $cost_types = array_keys($controller->getEc2CostTypes());
    foreach ($cost_types ?: [] as $cost_type) {
      if (empty($costs[$cost_type])) {
        // Display a warning.
        echo "Warning: \$costs[\$cost_type] - K8s cost type '{$cost_type}' is not set in either costs or result arrays.\n";
      }

      if (empty($result['costs'][$cost_type])) {
        // Display a warning.
        echo "Warning: \$result['costs'][\$cost_type] - K8s cost type '{$cost_type}' is not set in either costs or result arrays.\n";
      }
      $this->assertEquals(round($costs[$cost_type], 3), round($result['costs'][$cost_type], 3));
    }
  }

  /**
   * Call UpdateCostStoreEntity function.
   */
  private function callUpdateCostStoreEntity(): void {
    $plugin = \Drupal::service('plugin.manager.cloud_store_plugin');
    $definitions = $plugin->getDefinitions();
    if (array_key_exists('k8s.cloud.cloud_store.k8s_cost_store:k8s_cost_store', $definitions)) {
      $plugin->createInstance('k8s.cloud.cloud_store.k8s_cost_store:k8s_cost_store')->updateCostStoreEntity();
    }
  }

}
