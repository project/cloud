<?php

namespace Drupal\Tests\k8s\Functional\cloud\launch_template;

use Drupal\Tests\cloud\Traits\CloudLaunchTemplateTrait;
use Drupal\Tests\k8s\Functional\K8sTestBase;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\k8s\Entity\K8sNamespace;

/**
 * Test K8s cloud launch templates (CloudLaunchTemplate).
 *
 * @group Cloud
 */
class CloudLaunchTemplateTest extends K8sTestBase {

  use CloudLaunchTemplateTrait;

  public const CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT = 2;
  public const CLOUD_LAUNCH_TEMPLATES_MAX_OBJECT_COUNT = 10;

  private const OPERATION_ADD = 'add';
  private const OPERATION_EDIT = 'edit';

  /**
   * The generic login user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser;

  /**
   * The generic login user who will launch the template.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webLaunchUser;

  /**
   * A user with administrative permissions.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * The namespaces.
   *
   * @var array
   */
  protected $namespaces = [];

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getPermissions(): array {
    $permissions = [];
    // Setup namespaces.
    $namespaces = $this->createNamespaceTestFormData(2);
    foreach ($namespaces ?: [] as $namespace) {
      $this->getNamespaceMockData($namespace);
      $this->createNamespaceTestEntity($namespace);
      $this->namespaces[] = $namespace['name'];
      $permissions[] = "view k8s namespace {$namespace['name']}";
    }
    $this->namespace = $namespaces[0]['name'];

    $permissions += [

      // Cloud service provider.
      "view {$this->cloudContext}",
      'edit cloud service providers',

      // Launch template.
      'add cloud server templates',
      'list cloud server template',
      'view any published cloud server templates',
      'edit any cloud server templates',
      'delete any cloud server templates',
      'launch approved cloud server template',

      // Launch template revisions.
      'access cloud server template revisions',
      'revert all cloud server template revisions',
      'delete all cloud server template revisions',

      // Pod.
      'add k8s pod',
      'edit any k8s pod',
      'delete any k8s pod',

      // Deployment.
      'add k8s deployment',
      'edit any k8s deployment',
      'delete any k8s deployment',
    ];

    $available_resources = \Drupal::service('k8s')->supportedCloudLaunchTemplates();

    unset(
      $available_resources['config_map'],
      $available_resources['event'],
      $available_resources['namespace'],
      $available_resources['node'],
      $available_resources['mixed']
    );

    $k8s_types = array_keys($available_resources);

    foreach ($k8s_types ?: [] as $type) {
      $permissions[] = 'list k8s ' . str_replace('_', ' ', $type);
      $permissions[] = 'view any k8s ' . str_replace('_', ' ', $type);
    }
    $permissions[] = 'list k8s config map';
    $permissions[] = 'view any k8s config map';

    return $permissions;
  }

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {

    parent::setUp();

    $this->webUser = $this->createLoginUser();
    $this->webLaunchUser = $this->createLoginUser();
    $perms = [
      'approve launch k8s resources',
      'launch cloud server template',
    ];
    $this->adminUser = $this->createLoginUser($perms);
    $this->drupalLogin($this->adminUser);

  }

  /**
   * CRUD test for k8s launch template.
   */
  public function testK8sLaunchTemplate(): void {
    $objects = $this->getK8sObjects();
    foreach ($objects ?: [] as $object) {
      $this->runLaunchTemplateCrudTest($object);
    }
  }

  /**
   * CRUD test for k8s launch template launch.
   */
  public function testK8sLaunchTemplateLaunch(): void {
    $this->runLaunchTemplateLaunchTest();
  }

  /**
   * Launch before approval by admin test for k8s launch template.
   */
  public function testCloudLaunchTemplateLaunchBeforeApproval(): void {
    $objects = $this->getK8sObjects();
    foreach ($objects ?: [] as $i => $object) {
      $this->runLaunchTemplateLaunchBeforeApprovalTest($object, $i + 1);
    }
  }

  /**
   * Workflow update test for k8s launch template launch.
   */
  public function testCloudLaunchTemplateWorkflowUpdate(): void {
    $objects = $this->getK8sObjects();
    foreach ($objects ?: [] as $i => $object) {
      $this->runLaunchTemplateLaunchWorkflowUpdateTest($object, $i + 1);
    }
  }

  /**
   * Delete resources test for k8s launch template launch.
   */
  public function testK8sLaunchTemplateDeleteResources(): void {
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {
      $this->runLaunchTemplateDeleteResourcesTest();
    }
  }

  /**
   * CRUD test for k8s pod launch template.
   *
   * @param string $object
   *   Object to test.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  private function runLaunchTemplateCrudTest($object): void {

    $cloud_context = $this->cloudContext;

    // Create test.
    $pods = $this->createPodTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT, $this->namespace);
    $add = $this->createLaunchTemplateTestFormData($pods, 'yml', $object, [], self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {
      // field_object is automatically calculated.
      unset($add[$i]['field_object']);
      $this->drupalGet("/clouds/design/server_template/$cloud_context/k8s/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
    }

    // List test.
    $this->drupalGet("/clouds/design/server_template/$cloud_context");
    $this->assertNoErrorMessage();

    // Edit test.
    $pods = $this->createPodTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT, $this->namespace);
    $edit = $this->createLaunchTemplateTestFormData($pods, 'yml', $object, [], self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {
      // field_object is automatically calculated.
      unset($edit[$i]['field_object']);

      // Go to listing page.
      $this->drupalGet("/clouds/design/server_template/$cloud_context");
      $this->clickLink('Edit', $i);

      $this->drupalGet($this->getUrl());
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $edit[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
    }

    // Delete test.
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {

      // Go to listing page.
      $this->drupalGet("/clouds/design/server_template/$cloud_context");

      $this->clickLink('Delete', 0);
      $this->drupalGet($this->getUrl());
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();
    }

    // Make sure the deleted templates are not listed anymore.
    $this->drupalGet("/clouds/design/server_template/$cloud_context");
    $this->assertNoErrorMessage();

    for ($j = 0; $j < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $j++) {
      $this->assertSession()->pageTextNotContains($edit[$j]['name[0][value]']);
    }
  }

  /**
   * Launch test for k8s pod launch template.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  private function runLaunchTemplateLaunchTest(): void {
    $cloud_context = $this->cloudContext;
    $objects = \Drupal::service('k8s')->supportedCloudLaunchTemplates();

    // Pick up several $objects randomly to shorten the test time.
    $objects = array_intersect_key($objects, array_flip(
      array_rand(
        $objects, random_int(2, self::CLOUD_LAUNCH_TEMPLATES_MAX_OBJECT_COUNT)
      )
    ));

    foreach ($objects as $object => $name) {

      $count = random_int(1, self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
      $createFormDataFunction = "create{$name}TestFormData";
      if (!method_exists($this, $createFormDataFunction)) {
        continue;
      }

      $data = $this->$createFormDataFunction($count, $this->namespace);
      if ($object === 'pod') {
        $this->getMetricsPodMockData([]);
      }

      $entity_type = \Drupal::entityTypeManager()->getDefinition("k8s_$object");
      $addMockDataFunction = "add{$name}MockData";
      $deleteMockDataFunction = "delete{$name}MockData";

      if (!method_exists($this, $addMockDataFunction)) {
        continue;
      }

      if (empty($data[0]['post_data']['detail[0][value]'])) {
        continue;
      }

      $add = $this->createLaunchTemplateTestFormData($data, 'yml', $object, [], $count);

      for ($i = 0; $i < $count; $i++) {
        // field_object is automatically calculated.
        unset($add[$i]['field_object']);
        $this->drupalGet("/clouds/design/server_template/$cloud_context/k8s/add");
        $this->submitForm(
          $add[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        $t_args = [
          '@type' => 'launch template',
          '%label' => $add[$i]['name[0][value]'],
        ];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
      }

      // Launch the templates.
      for ($i = 0; $i < $count; $i++) {
        $this->drupalGet("/clouds/design/server_template/$cloud_context");
        // Update the mock data.
        $this->$addMockDataFunction($data[$i], $cloud_context, $this->webUser->id());

        // Navigate to the launch template, and launch it.
        $this->clickLink($data[$i]['name']);
        $this->clickLink('Launch');

        $this->drupalGet($this->getUrl());
        $launch_data = [];
        $launch_data['launch_user'] = $this->webLaunchUser->getAccountName();
        $this->submitForm(
          $launch_data,
          $this->t('Launch')->render());
        $this->assertNoErrorMessage();

        $t_args = [
          '@type' => $entity_type->getSingularLabel(),
          '%label' => $add[$i]['name[0][value]'],
        ];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been launched.', $t_args)));
        $this->drupalGet("/clouds/k8s/$cloud_context/$object");
        $this->clickLink($data[$i]['name']);
        $this->assertSession()->pageTextContains($this->webLaunchUser->getAccountName());
        $this->assertSession()->pageTextNotContains($this->adminUser->getAccountName());

        $namespaceable = $entity_type->get('namespaceable') === NULL ? TRUE : FALSE;
        if ($namespaceable) {
          $data[$i]['post_data']['namespace'] = $this->namespaces[1];
          // Update the mock data.
          $this->$addMockDataFunction($data[$i], $cloud_context, $this->webUser->id());

          // Change workflow status to 'Approved'.
          $this->drupalGet("/clouds/design/server_template/$cloud_context");
          $this->clickLink($data[$i]['name']);
          $this->clickLink('Edit');
          $this->submitForm(
            $add[$i],
            $this->t('Save')->render()
          );
          $this->assertNoErrorMessage();

          // Navigate to the launch template, and launch it.
          $this->clickLink('Launch');

          $this->assertSession()->pageTextContains($this->t('Delete following resources'));
          $this->assertSession()->pageTextContains(strip_tags($this->t('@type: %label (Namespace: %namespace)', [
            '@type' => $entity_type->getSingularLabel(),
            '%label' => $add[$i]['name[0][value]'],
            '%namespace' => $this->namespace,
          ])));

          // Launch with other namespace.
          $launch_data['field_namespace'] = $this->namespaces[1];
          $this->submitForm(
            $launch_data,
            $this->t('Launch')->render());

          $this->assertNoErrorMessage();
          $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been launched.', $t_args)));

          $this->drupalGet("/clouds/k8s/$cloud_context/$object");
          $this->assertSession()->linkExists($add[$i]['name[0][value]'], 1);
          foreach ($this->namespaces ?: [] as $namespace) {
            $this->assertSession()->linkExists($namespace);
          }
        }

        // Change workflow status to 'Approved'.
        $this->drupalGet("/clouds/design/server_template/$cloud_context");
        $this->clickLink($data[$i]['name']);
        $this->clickLink('Edit');
        $this->submitForm(
            $add[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();

        // Navigate to the launch template, and launch it.
        $this->clickLink('Launch');

        $this->assertSession()->pageTextContains($this->t('Delete following resources'));
        if ($namespaceable) {
          foreach ($this->namespaces ?: [] as $namespace) {
            $this->assertSession()->pageTextContains(strip_tags($this->t('@type: %label (Namespace: %namespace)', [
              '@type' => $entity_type->getSingularLabel(),
              '%label' => $add[$i]['name[0][value]'],
              '%namespace' => $namespace,
            ])));
          }
          unset($launch_data['field_namespace']);
        }
        else {
          $this->assertSession()->pageTextContains(strip_tags($this->t('@type: %label', $t_args)));
        }

        $this->$deleteMockDataFunction($data[$i]);

        // Launch with delete option.
        $launch_data['field_delete_all_resources'] = TRUE;
        $this->submitForm(
          $launch_data,
          $this->t('Launch')->render()
        );
        $this->assertNoErrorMessage();

        if ($namespaceable) {
          foreach ($this->namespaces ?: [] as $namespace) {
            $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label (Namespace: %namespace) on @cloud_context has been deleted.', [
              '@type' => $entity_type->getSingularLabel(),
              '%label' => $add[$i]['name[0][value]'],
              '@cloud_context' => $this->cloudContext,
              '%namespace' => $namespace,
            ])));
          }
        }
        else {
          $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label on @cloud_context has been deleted.', [
            '@type' => $entity_type->getSingularLabel(),
            '%label' => $add[$i]['name[0][value]'],
            '@cloud_context' => $this->cloudContext,
          ])));
        }
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been launched.', $t_args)));

        $this->drupalGet("/clouds/k8s/$cloud_context/$object");
        $this->assertSession()->linkExists($add[$i]['name[0][value]']);

        if (!$namespaceable) {
          continue;
        }
        $this->assertSession()->linkNotExists($this->namespaces[0]);
        $this->assertSession()->linkExists($this->namespaces[1]);
      }
    }
  }

  /**
   * Launch before approval by admin test for k8s pod launch template.
   *
   * @param string $object
   *   Object to test.
   * @param int $index
   *   The template index (1-based).
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \UnhandledMatchError
   */
  private function runLaunchTemplateLaunchBeforeApprovalTest($object, $index): void {
    $cloud_context = $this->cloudContext;
    $repeat_count = 1;
    $module_name = 'k8s';

    // Create templates.
    $data = match ($object) {
      'pod' => $this->createPodTestFormData($repeat_count, $this->namespace),
      'deployment' => $this->createDeploymentTestFormData($repeat_count, $this->namespace),
    };

    $add = $this->createLaunchTemplateTestFormData($data, 'yml', $object, [], $repeat_count, CloudLaunchTemplateInterface::DRAFT);
    $edit = $add[0];

    $list_url = "/clouds/design/server_template/$cloud_context";
    $view_url = "/clouds/design/server_template/$cloud_context/$index";

    // Login as admin and create launch template.
    $this->drupalLogin($this->adminUser);
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_ADD, $module_name, $index);

    // Verify that the status is Draft.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT, TRUE);

    $this->drupalGet("$view_url/launch");
    $this->assertSession()->pageTextContains($this->t('Launch as'));

    // Update the mock data.
    switch ($object) {
      case 'pod':
        $this->getMetricsPodMockData([]);
        $this->addPodMockData($data[0], $cloud_context, $this->webUser->id());
        break;

      case 'deployment':
        $this->addDeploymentMockData($data[0], $cloud_context, $this->webUser->id());
        break;
    }

    // The admin user launches an instance from the launch template.
    $this->submitForm(
      [],
      $this->t('Launch')->render()
    );
    $this->assertNoErrorMessage();

    $t_args = [
      '@type' => ucfirst($object),
      '%label' => $edit['name[0][value]'],
    ];
    $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been launched.', $t_args)));

    // After launching the instance.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT, TRUE);
    // Delete template.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/$index/delete");
    $this->submitForm(
      ['field_delete_option' => 'application'],
      $this->t('Delete')->render()
    );
    $this->assertNoErrorMessage();
  }

  /**
   * Workflow update test for k8s pod launch template.
   *
   * @param string $object
   *   Object to test.
   * @param int $index
   *   The template index (1-based).
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  private function runLaunchTemplateLaunchWorkflowUpdateTest($object, $index): void {
    $cloud_context = $this->cloudContext;
    $repeat_count = 1;
    $module_name = 'k8s';

    // Create templates.
    $data = [];
    switch ($object) {
      case 'pod':
        $data = $this->createPodTestFormData($repeat_count, $this->namespace);
        break;

      case 'deployment':
        $data = $this->createDeploymentTestFormData($repeat_count, $this->namespace);
        break;

      default:
        break;
    }

    $add = $this->createLaunchTemplateTestFormData($data, 'yml', $object, [], $repeat_count, CloudLaunchTemplateInterface::DRAFT);
    $edit = $add[0];

    $list_url = "/clouds/design/server_template/$cloud_context";
    $view_url = "/clouds/design/server_template/$cloud_context/$index";

    // 1. The generic user creates a launch template.
    $this->drupalLogin($this->webUser);
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_ADD, $module_name, $index);
    // Validate the status is Draft.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT);

    // 2 .The generic user changes the launch template status from 'Draft' to
    // 'Review'.
    $this->drupalLogin($this->webUser);
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::REVIEW;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name, $index);

    // Validate the status is 'Review'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::REVIEW);

    // 3. The generic user changes the launch template status from 'Review' to
    // 'Draft'.
    $this->drupalLogin($this->webUser);

    // Launch as is not displayed.
    $this->drupalGet("$view_url/review");
    $this->assertSession()->pageTextNotContains($this->t('Launch as'));
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::DRAFT;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name, $index);

    // Validate the status is still 'Draft'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT);

    // 4. The generic user changes the launch template status from 'Draft' to
    // 'Review'.
    $this->drupalLogin($this->webUser);
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::REVIEW;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name, $index);

    // Validate the status is still 'Review'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::REVIEW);

    // Make sure that the generic user cannot approve or launch.
    $this->drupalGet($view_url);
    $this->assertSession()->linkNotExistsExact($this->t('Approve'));
    $this->assertSession()->linkByHrefNotExists("server_template/$cloud_context/$index/approve");
    $this->assertSession()->linkNotExistsExact($this->t('Launch'));
    $this->assertSession()->linkByHrefNotExists("server_template/$cloud_context/$index/launch");

    // 5. The IT admin changes the launch template status from 'Review' to
    // 'Approved'.
    $this->drupalLogin($this->adminUser);

    // 'Launch as' is not displayed.
    $this->drupalGet("$view_url/approve");
    $this->assertSession()->pageTextNotContains($this->t('Launch as'));

    // Make sure that the admin user can approve and launch.
    $this->drupalGet($view_url);
    $this->assertSession()->linkExistsExact($this->t('Approve'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/$index/approve");
    $this->assertSession()->linkExistsExact($this->t('Launch'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/$index/launch");
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::APPROVED;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name, $index);

    // Validate the status is still 'Approved'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::APPROVED, TRUE);

    // Validate the 'Launch' tab is shown in the generic user's launch template.
    $this->drupalLogin($this->webUser);
    $this->drupalGet($view_url);
    $this->assertSession()->linkExistsExact($this->t('Launch'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/$index/launch");

    // 'Launch as' is displayed.
    $this->drupalGet("$view_url/launch");
    $this->assertSession()->pageTextContains($this->t('Launch as'));

    // 6. The generic user saves the launch template as it is w/o any change.
    $this->drupalLogin($this->webUser);
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name, $index);
    // Validate the status is still APPROVED.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::APPROVED);

    // 7. The generic user saves the launch template w/ some changes.
    $this->drupalLogin($this->webUser);
    $template_name = $edit['name[0][value]'];
    $edit['name[0][value]'] .= '-edit';
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name, $index);

    // Validate if the status is still 'Draft'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT);
    $edit['name[0][value]'] = $template_name;

    // 8. The generic user changes the launch template status from 'Draft' to
    // 'Review'.
    $this->drupalLogin($this->webUser);
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::REVIEW;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name, $index);

    // Validate if the status is still 'Review'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::REVIEW);

    // Make sure that generic user cannot approve or launch.
    $this->drupalGet($view_url);
    $this->assertSession()->linkNotExistsExact($this->t('Approve'));
    $this->assertSession()->linkByHrefNotExists("server_template/$cloud_context/$index/approve");
    $this->assertSession()->linkNotExistsExact($this->t('Launch'));
    $this->assertSession()->linkByHrefNotExists("server_template/$cloud_context/$index/launch");

    // 9. The IT admin changes the launch template status from 'Review' to
    // 'Approved'.
    $this->drupalLogin($this->adminUser);

    // Make sure that the Admin user can approve and launch.
    $this->drupalGet($view_url);
    $this->assertSession()->linkExistsExact($this->t('Approve'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/$index/approve");
    $this->assertSession()->linkExistsExact($this->t('Launch'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/$index/launch");
    $edit['field_workflow_status'] = CloudLaunchTemplateInterface::APPROVED;
    $this->updateLaunchTemplate($cloud_context, $edit, CloudLaunchTemplateInterface::OPERATION_EDIT, $module_name, $index);

    // Validate the status is still 'Approved'.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::APPROVED, TRUE);

    // Validate if the 'Launch' tab is shown in the generic user's launch
    // template.
    $this->drupalLogin($this->webUser);
    $this->drupalGet($view_url);
    $this->assertSession()->linkExistsExact($this->t('Launch'));
    $this->assertSession()->linkByHrefExists("server_template/$cloud_context/$index/launch");

    // 10. The generic user launches an instance from the launch template.
    $this->drupalLogin($this->webUser);
    $this->drupalGet("/clouds/design/server_template/$cloud_context/$index/launch");

    // Update the mock data.
    switch ($object) {
      case 'pod':
        $this->getMetricsPodMockData([]);
        $this->addPodMockData($data[0], $cloud_context, $this->webUser->id());
        break;

      case 'deployment':
        $this->addDeploymentMockData($data[0], $cloud_context, $this->webUser->id());
        break;

      default;
        break;
    }

    $this->submitForm(
      [],
      $this->t('Launch')->render()
    );
    $this->assertNoErrorMessage();

    $t_args = [
      '@type' => ucfirst($object),
      '%label' => $edit['name[0][value]'],
    ];
    $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been launched.', $t_args)));
    // After launching the instance.
    $this->checkPageTextContains($list_url, $view_url, CloudLaunchTemplateInterface::DRAFT);

    // Delete template.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/$index/delete");
    $this->submitForm(
      ['field_delete_option' => 'application'],
      $this->t('Delete')->render()
    );
    $this->assertNoErrorMessage();
  }

  /**
   * Delete resources test for k8s pod launch template.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  private function runLaunchTemplateDeleteResourcesTest(): void {
    $cloud_context = $this->cloudContext;
    $available_resources = \Drupal::service('k8s')->supportedCloudLaunchTemplates();

    // Because there is a special type "mixed", the number of resources
    // should be larger than 1.
    $keys = array_rand($available_resources, random_int(2, count($available_resources)));
    if (!is_array($keys)) {
      $keys = [$keys];
    }

    $keys = array_flip($keys);
    $objects = array_intersect_key($available_resources, $keys);

    // Create resources.
    $entities = [];
    foreach ($objects as $object => $name) {
      $count = random_int(1, self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
      $createFormDataFunction = "create{$name}TestFormData";
      if (!method_exists($this, $createFormDataFunction)) {
        continue;
      }

      $items = $this->$createFormDataFunction($count, $this->namespace);
      if ($object === 'pod') {
        $this->getMetricsPodMockData([]);
      }

      $addMockDataFunction = "add{$name}MockData";
      $createTestEntityFunction = "create{$name}TestEntity";
      $deleteMockDataFunction = "delete{$name}MockData";

      if (!method_exists($this, $addMockDataFunction)) {
        continue;
      }

      foreach ($items ?: [] as $item) {
        $this->$addMockDataFunction($item, $cloud_context, $this->webUser->id());
        $param = ['name' => $item['name']];
        $param += !empty($item['post_data']['namespace']) ? ['namespace' => $item['post_data']['namespace']] : [];
        $entities[] = $this->$createTestEntityFunction($param);
        $this->$deleteMockDataFunction($item);
      }
    }

    $adds = $this->createLaunchTemplateTestFormData([], 'git', $object ?? '', $entities);
    $adds[0]['name'] = $adds[0]['name[0][value]'];
    $adds[0]['cloud_context'] = $adds[0]['cloud_context[0][value]'];
    $server_template = $this->createLaunchTemplateTestEntity($adds[0]);
    $id = $server_template->id();
    $this->drupalGet("/clouds/design/server_template/$cloud_context/$id/delete");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains(
      $this->t("Make sure the following resources will be deleted if you select 'Delete both application and resources' or 'Delete only resources' option."));
    $this->assertSession()->fieldEnabled('field_delete_option');

    foreach ($entities ?: [] as $entity) {
      $this->assertSession()->pageTextContains(
        $this->t('@type: @name', [
          '@type' => $entity->getEntityType()->getSingularLabel(),
          '@name' => $entity->getName(),
          ':url' => $entity->toUrl('canonical')->toString(),
        ])
      );
    }
    $options = ['both', 'resources', 'application'];
    $option = $options[array_rand($options)];

    $this->drupalGet("/clouds/design/server_template/$cloud_context/$id/delete");
    $this->submitForm(
      ['field_delete_option' => $option],
      $this->t('Delete')->render()
    );
    $this->assertNoErrorMessage();

    if ($option === 'both' || $option === 'resources') {
      foreach ($entities ?: [] as $entity) {
        $namespaceable = $entity->getEntityType()->get('namespaceable') === NULL ? TRUE : FALSE;
        if ($namespaceable && !($entity instanceof K8sNamespace)) {
          $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label (Namespace: %namespace) on @cloud_context has been deleted.', [
            '@type' => $entity->getEntityType()->getSingularLabel(),
            '%label' => $entity->label(),
            '@cloud_context' => $this->cloudContext,
            '%namespace' => $this->namespace,
          ])));
        }
        else {
          $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label on @cloud_context has been deleted.', [
            '@type' => $entity->getEntityType()->getSingularLabel(),
            '%label' => $entity->label(),
            '@cloud_context' => $this->cloudContext,
          ])));
        }
      }
    }

    if ($option === 'both' || $option === 'application') {

      $t_args = [
        '@type'  => $server_template->getEntityType()->getSingularLabel(),
        '@label' => $server_template->label(),
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));
    }
  }

  /**
   * Get K8s objects to test.
   *
   * @return array
   *   Array of K8s objects.
   */
  protected function getK8sObjects(): array {
    return [
      'pod',
      'deployment',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function updateMockDataToConfig(array $mock_data): void {
    $config_mock_data = $this->getMockDataFromConfig();
    if (empty($mock_data['getNamespaces']) || empty($config_mock_data)) {
      parent::updateMockDataToConfig($mock_data);
      return;
    }

    $namespaces = [];
    foreach ($mock_data['getNamespaces'] ?: [] as $namespace) {
      $namespaces[] = $namespace['metadata']['name'];
    }

    foreach ($config_mock_data['getNamespaces'] ?: [] as $namespace) {
      if (in_array($namespace['metadata']['name'], $namespaces, TRUE)) {
        continue;
      }
      $mock_data['getNamespaces'][] = $namespace;
    }

    parent::updateMockDataToConfig($mock_data);
  }

}
