<?php

namespace Drupal\Tests\k8s\Functional\cloud\project;

use Drupal\Tests\k8s\Functional\K8sTestBase;

/**
 * Test K8s cloud project (CloudProject).
 *
 * @group Cloud
 */
class CloudProjectTest extends K8sTestBase {

  public const CLOUD_PROJECTS_REPEAT_COUNT = 3;

  /**
   * The generic login user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser;

  /**
   * The generic login user who will launch the template.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webLaunchUser;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getPermissions():array {
    return [
      'add k8s namespace',
      'list k8s namespace',
      'view any k8s namespace',
      'edit any k8s namespace',
      'delete any k8s namespace',
      'add k8s resource quota',
      'list k8s resource quota',
      'view any k8s resource quota',
      'edit any k8s resource quota',
      'delete any k8s resource quota',
      'add cloud projects',
      'launch cloud project',
      'list cloud project',
      'edit any cloud projects',
      'delete any cloud projects',
      'access cloud project revisions',
      'view any published cloud projects',
      'revert all cloud project revisions',
      'delete all cloud project revisions',
    ];
  }

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {

    parent::setUp();

    $this->webUser = $this->createLoginUser();
    $this->webLaunchUser = $this->createLoginUser();
    $this->drupalLogin($this->webUser);

  }

  /**
   * CRUD test for k8s project test.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testK8sProjectCrudTest(): void {

    $cloud_context = $this->cloudContext;

    // Create a new project.
    $add = $this->createProjectTestFormData(self::CLOUD_PROJECTS_REPEAT_COUNT);
    $edit = $this->createProjectTestFormData(self::CLOUD_PROJECTS_REPEAT_COUNT);
    for ($i = 0; $i < self::CLOUD_PROJECTS_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      // Create project.
      $this->drupalGet("/clouds/design/project/$cloud_context/k8s/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );

      if ($add[$i]['field_pod_count[0][value]'] === 0) {
        $this->assertSession()->pageTextContains($this->t('Pod count must be higher than or equal to 1'));
        $add[$i]['field_pod_count[0][value]'] = 1;
        $this->drupalGet("/clouds/design/project/$cloud_context/k8s/add");
        $this->submitForm(
          $add[$i],
          $this->t('Save')->render()
        );
      }
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cloud project',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // List test.
      $this->drupalGet("/clouds/design/project/$cloud_context");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name[0][value]']);

      // Edit test.
      unset(
        $edit[$i]['name[0][value]']
      );
      $this->clickLink($add[$i]['name[0][value]']);
      $this->clickLink('Edit', 0);
      $this->drupalGet($this->getUrl());
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );

      if ($edit[$i]['field_pod_count[0][value]'] === 0) {
        $this->assertSession()->pageTextContains($this->t('Pod count must be higher than or equal to 1'));
        $edit[$i]['field_pod_count[0][value]'] = 1;

        $this->drupalGet("/clouds/design/project/$cloud_context");
        $this->clickLink($add[$i]['name[0][value]']);
        $this->clickLink('Edit', 0);
        $this->drupalGet($this->getUrl());
        $this->submitForm(
          $edit[$i],
          $this->t('Save')->render()
        );
      }
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cloud project',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Click 'Refresh'.
      // @todo Need tests for the entities from the mock objects.
      $this->clickLink($this->t('Refresh'));
      $this->assertSession()->pageTextContains($this->t('Unnecessary to update cloud projects.'));
      $this->assertNoErrorMessage();

      // Copy test.
      $copy[$i] = $edit[$i];
      $copy[$i]['name[0][value]'] = $add[$i]['name[0][value]'] . '-copy';

      // Go to listing page.
      $this->drupalGet("/clouds/design/project/$cloud_context");
      $this->clickLink('Copy', 0);

      $this->drupalGet($this->getUrl());
      $this->submitForm(
        $copy[$i],
        $this->t('Copy')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cloud project',
        '%label' => $copy[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Go to listing page.
      $this->drupalGet("/clouds/design/project/$cloud_context");
      $this->assertSession()->pageTextContains($add[$i]['name[0][value]']);
      $this->assertSession()->pageTextContains($copy[$i]['name[0][value]']);

      // Delete test.
      $this->clickLink($add[$i]['name[0][value]']);
      $this->clickLink('Delete', 0);
      $this->drupalGet($this->getUrl());
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cloud project',
        '@label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Go to listing page.
      $this->drupalGet("/clouds/design/project/$cloud_context");
      $this->clickLink($copy[$i]['name[0][value]']);
      $this->clickLink('Delete', 0);
      $this->drupalGet($this->getUrl());
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cloud project',
        '@label' => $copy[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure the deleted projects are not listed anymore.
      $this->drupalGet("/clouds/design/project/$cloud_context");
      $this->assertSession()->pageTextNotContains($add[$i]['name[0][value]']);
      $this->assertSession()->pageTextNotContains($copy[$i]['name[0][value]']);
    }
  }

  /**
   * Tests launch a project.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testLaunchProject(): void {

    $cloud_context = $this->cloudContext;

    // List Namespace for K8s.
    $this->drupalGet("/clouds/design/project/$cloud_context");
    $this->assertNoErrorMessage();

    // Create a new project.
    $add = $this->createProjectTestFormData(self::CLOUD_PROJECTS_REPEAT_COUNT);
    $edit = $this->createProjectTestFormData(self::CLOUD_PROJECTS_REPEAT_COUNT);
    for ($i = 0; $i < self::CLOUD_PROJECTS_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      // Make sure resource quota is created.
      $add[$i]['field_enable_resource_scheduler[value]'] = 1;

      // Create project.
      $this->drupalGet("/clouds/design/project/$cloud_context/k8s/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );

      if ($add[$i]['field_pod_count[0][value]'] === 0) {
        $this->assertSession()->pageTextContains($this->t('Pod count must be higher than or equal to 1'));
        $add[$i]['field_pod_count[0][value]'] = 1;
        $this->drupalGet("/clouds/design/project/$cloud_context/k8s/add");
        $this->submitForm(
          $add[$i],
          $this->t('Save')->render()
        );
        $this->assertNoErrorMessage();
      }

      $t_args = [
        '@type' => 'Cloud project',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      $this->drupalGet("/clouds/design/project/$cloud_context");
      // Create data of Namespace and resource quota.
      $namespace = $this->arrangeDataForNamespace($add[$i]);
      $this->addNamespaceMockData($namespace, $cloud_context, $this->webUser->id());
      $this->createNamespaceTestEntity($namespace);
      $resource_quota = $this->arrangeDataForResourceQuota($add[$i]);
      $this->addResourceQuotaMockData($resource_quota, $cloud_context, $this->webUser->id());

      // Create entity.
      $nodes = $this->createNodeTestFormData(self::CLOUD_PROJECTS_REPEAT_COUNT);
      $pods = $this->createPodsRandomTestFormData($namespace['name']);
      foreach ($pods ?: [] as $pod) {
        $this->createPodTestEntity($pod);
      }
      foreach ($nodes ?: [] as $node) {
        $this->createNodeTestEntity($node);
      }
      // Create resources and cost mock data.
      $this->getNamespaceResourceUsageMockData($pods);
      $this->getNodeResourceUsageMockData($nodes);
      $this->calculateCostPerNamespaceMockData();

      // Launch after Editing project.
      $edit[$i]['name[0][value]'] = $add[$i]['name[0][value]'];
      $namespace_edit = $this->arrangeDataForNamespace($edit[$i]);
      $this->updateNamespaceMockData($namespace_edit);
      $resource_quota_edit = [];
      if (!empty($edit[$i]['field_enable_resource_scheduler[value]'])) {
        $resource_quota_edit = $this->arrangeDataForResourceQuota($edit[$i]);
        if (!empty($add[$i]['field_enable_resource_scheduler[value]'])) {
          $this->updateResourceQuotaMockData($resource_quota_edit);
        }
        else {
          $this->addResourceQuotaMockData($resource_quota_edit, $cloud_context, $this->webUser->id());
        }
      }
      else {
        if (!empty($add[$i]['field_enable_resource_scheduler[value]'])) {
          $this->deleteResourceQuotaMockData($resource_quota);
        }
      }

      unset(
        $edit[$i]['name[0][value]']
      );
      $this->drupalGet("/clouds/design/project/$cloud_context");
      $this->clickLink($namespace_edit['name']);
      $this->clickLink('Edit', 0);
      $this->drupalGet($this->getUrl());
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );

      if ($edit[$i]['field_pod_count[0][value]'] === 0) {
        $this->assertSession()->pageTextContains($this->t('Pod count must be higher than or equal to 1'));
        $edit[$i]['field_pod_count[0][value]'] = 1;

        $this->drupalGet("/clouds/design/project/$cloud_context");
        $this->clickLink($namespace_edit['name']);
        $this->clickLink('Edit', 0);
        $this->drupalGet($this->getUrl());
        $this->submitForm(
          $edit[$i],
          $this->t('Save')->render()
        );
      }
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cloud project',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/design/project/$cloud_context");
      $this->clickLink($namespace_edit['name']);
      $this->clickLink('Launch');
      $this->drupalGet($this->getUrl());
      $launch_data['launch_user'] = $this->webLaunchUser->getAccountName();
      $this->submitForm(
        $launch_data,
        $this->t('Launch')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cloud project',
        '%label' => $namespace_edit['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been launched.', $t_args)));

      $t_args = [
        '@type' => 'Namespace',
        '%label' => $namespace_edit['name'],
        '@cloud_context' => $cloud_context,
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label on @cloud_context has been updated.', $t_args)));

      if (!empty($edit[$i]['field_enable_resource_scheduler[value]'])) {
        if (!empty($add[$i]['field_enable_resource_scheduler[value]'])) {
          // Initial field_enable_resource_scheduler: on.
          // Updating field_enable_resource_scheduler: on.
          // Expected result: updated.
          $t_args = [
            '@type' => 'Resource quota',
            '%label' => $namespace_edit['name'],
            '@cloud_context' => $cloud_context,
            '@passive_operation' => 'updated',
          ];
        }
        else {
          // Initial field_enable_resource_scheduler: off.
          // Updating field_enable_resource_scheduler: on.
          // Expected result: created.
          $t_args = [
            '@type' => 'Resource quota',
            '%label' => $namespace_edit['name'],
            '@cloud_context' => $cloud_context,
            '@passive_operation' => 'created',
          ];
        }
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label on @cloud_context has been @passive_operation.', $t_args)));
      }
      elseif (!empty($add[$i]['field_enable_resource_scheduler[value]'])) {
        // Initial field_enable_resource_scheduler: on.
        // Updating field_enable_resource_scheduler: off.
        // Expected result: deleted.
        $t_args = [
          '@type' => 'Resource quota',
          '@label' => $namespace['name'],
          '@namespace' => $namespace['name'],
          '@cloud_context' => $cloud_context,
          '@passive_operation' => 'deleted',
        ];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label (Namespace: @namespace) on @cloud_context has been @passive_operation.', $t_args)));
      }

      $this->drupalGet("/clouds/k8s/$cloud_context/namespace");
      $this->clickLink($namespace_edit['name']);

      $this->assertSession()->pageTextContains($this->webLaunchUser->getAccountName());
      $this->assertSession()->pageTextNotContains($this->webUser->getAccountName());

      // Delete test.
      if (!empty($edit[$i]['field_enable_resource_scheduler[value]'])) {
        $this->deleteResourceQuotaMockData($resource_quota_edit);
      }
      else {
        if (!empty($add[$i]['field_enable_resource_scheduler[value]'])) {
          $this->deleteResourceQuotaMockData($resource_quota);
        }
      }
      $this->deleteNamespaceMockData($namespace_edit);

      $this->drupalGet("/clouds/design/project/$cloud_context");
      $this->clickLink($namespace_edit['name']);
      $this->clickLink('Delete', 0);
      $this->drupalGet($this->getUrl());
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'Cloud project',
        '@label' => $namespace_edit['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      $t_args = [
        '@type' => 'Namespace',
        '@label' => $namespace_edit['name'],
        '@cloud_context' => $cloud_context,
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label on @cloud_context has been deleted.', $t_args)));

      $t_args = [
        '@type' => 'Role',
        '@label' => $namespace_edit['name'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Check the result.
      $this->drupalGet("/clouds/k8s/$cloud_context/resource_quota");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($namespace_edit['name']);

      $this->drupalGet("/clouds/k8s/$cloud_context/namespace");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * The arrangement of data for resource quota from test data.
   *
   * @param array $data
   *   Test data for namespace.
   *
   * @return array
   *   Arrangement of test data.
   */
  private function arrangeDataForNamespace(array $data): array {
    $namespace = [];
    $namespace['name'] = $data['name[0][value]'];
    if (!empty($data['field_enable_resource_scheduler[value]'])) {
      $namespace['metadata']['annotations']['request_cpu'] = $data['field_request_cpu[0][value]'] . 'm';
      $namespace['metadata']['annotations']['request_memory'] = $data['field_request_memory[0][value]'] . 'Mi';
      $namespace['metadata']['annotations']['pod_count'] = $data['field_pod_count[0][value]'];
    }
    return $namespace;
  }

  /**
   * The arrangement of data for resource quota from test data.
   *
   * @param array $data
   *   Test data for resource quota.
   *
   * @return array
   *   Arrangement of test data.
   */
  private function arrangeDataForResourceQuota(array $data): array {
    $resource_quota = [
      'post_data' => [
        'namespace' => $data['name[0][value]'],
      ],
      'name' => $data['name[0][value]'],
      'spec' => [
        'hard' => [
          'limits.cpu' => $data['field_request_cpu[0][value]'] . 'm',
          'limits.memory' => $data['field_request_memory[0][value]'] . 'Mi',
          'pods' => $data['field_pod_count[0][value]'],
        ],
      ],
    ];

    return $resource_quota;
  }

}
