<?php

namespace Drupal\Tests\k8s\Unit\Plugin\cloud\project;

use Drupal\Component\Utility\Random;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\cloud\Traits\CloudTestEntityTrait;
use Drupal\Tests\k8s\Traits\K8sTestFormDataTrait;
use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\k8s\Plugin\cloud\project\K8sCloudProjectPlugin;
use Drupal\k8s\Service\K8sServiceInterface;

/**
 * Tests K8s Cloud Template plugin.
 *
 * @group Cloud
 */
class K8sCloudProjectPluginTest extends UnitTestCase {

  use CloudTestEntityTrait;
  use K8sTestFormDataTrait;

  public const K8S_CLOUD_PROJECT_PLUGIN_REPEAT_COUNT = 1;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'cloud',
    'k8s',
  ];

  /**
   * Plugin.
   *
   * @var string
   */
  private $plugin;

  /**
   * K8s service mock.
   *
   * @var \Drupal\k8s\Service\K8sServiceInterface
   */
  private $k8sServiceMock;

  /**
   * Cloud service mock.
   *
   * @var \Drupal\k8s\Service\k8sServiceMock
   */
  private $cloudServiceMock;

  /**
   * Creating random data utility.
   *
   * @var \Drupal\Component\Utility\Random
   */
  private $random;

  /**
   * User account.
   *
   * @var \Drupal\Core\Session\Account
   */
  private $account;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    // Create messenger, logger.factory and string_translation container.
    $container = new ContainerBuilder();

    // Messenger.
    $mock_messenger = $this->createMock(Messenger::class);

    // Logger.
    $mock_logger = $this->createMock(LoggerChannelInterface::class);
    $mock_logger_factory = $this->createMock(LoggerChannelFactoryInterface::class);
    $mock_logger_factory->expects($this->any())
      ->method('get')
      ->willReturn($mock_logger);

    $mock_config_factory = $this->createMock(ConfigFactoryInterface::class);

    $this->account = $this->createMock('Drupal\user\Entity\User');

    // Set containers.
    $container->set('messenger', $mock_messenger);
    $container->set('logger.factory', $mock_logger_factory);
    $container->set('string_translation', $this->getStringTranslationStub());
    $container->set('current_user', $this->account);
    \Drupal::setContainer($container);

    $this->k8sServiceMock = $this->createMock(K8sServiceInterface::class);
    $mock_query = $this->createMock(QueryInterface::class);
    $mock_query->expects($this->any())
      ->method('condition')
      ->willReturn($mock_query);

    $mock_query->expects($this->any())
      ->method('execute')
      ->willReturn([]);

    $mock_storage = $this->createMock(EntityStorageInterface::class);
    $mock_storage->expects($this->any())
      ->method('getQuery')
      ->willReturn($mock_query);

    $mock_storage->expects($this->any())
      ->method('loadMultiple')
      ->willReturn([]);

    $mock_entity_type_manager = $this->createMock(EntityTypeManagerInterface::class);
    $mock_entity_type_manager->expects($this->any())
      ->method('getStorage')
      ->willReturn($mock_storage);

    $mock_entity_type_manager->expects($this->any())
      ->method('getDefinition')
      ->with('entity')
      ->willReturn(['class' => '\Drupal\Core\Entity\ContentEntityTypeInterface']);

    $mock_uuid = $this->createMock(UuidInterface::class);
    $mock_uuid->expects($this->any())
      ->method('generate')
      ->willReturn('');

    $mock_user = $this->createMock(AccountProxyInterface::class);
    $mock_config_factory = $this->createMock(ConfigFactoryInterface::class);
    $mock_cloud_config_plugin_manager = $this->createMock(CloudConfigPluginManagerInterface::class);
    $mock_entity_link_renderer = $this->createMock(EntityLinkRendererInterface::class);
    $mock_file_system = $this->createMock(FileSystemInterface::class);
    $this->cloudServiceMock = $this->createMock(CloudServiceInterface::class);
    $mock_cloud_service = $this->createMock(CloudServiceInterface::class);
    $this->plugin = new K8sCloudProjectPlugin(
      [], '', [],
      $this->k8sServiceMock,
      $mock_entity_type_manager,
      $mock_uuid,
      $mock_user,
      $mock_config_factory,
      $mock_cloud_config_plugin_manager,
      $mock_entity_link_renderer,
      $mock_file_system,
      $mock_cloud_service
    );

    $this->random = new Random();
  }

  /**
   * Tests launching an instance.
   */
  public function testLaunch(): void {
    $random = $this->random;

    $project_name = $random->name(8, TRUE);

    $mock_project = $this->createMock(CloudProjectInterface::class);

    $project_values = $this->createProjectTestFormData(self::K8S_CLOUD_PROJECT_PLUGIN_REPEAT_COUNT, TRUE);
    $project_values = array_shift($project_values);

    $project_value_map = [
      [
        'name', (object) [
          'value' => $project_values['name[0][value]'],
        ],
      ], [
        'field_enable_resource_scheduler', (object) [
          'value' => $project_values['field_enable_resource_scheduler[value]'],
        ],
      ], [
        'field_k8s_clusters', (object) [
          'value' => $project_values['field_k8s_clusters'],
        ],
      ], [
        'field_request_cpu', (object) [
          'value' => $project_values['field_request_cpu[0][value]'],
        ],
      ], [
        'field_request_memory', (object) [
          'value' => $project_values['field_request_memory[0][value]'],
        ],
      ], [
        'field_pod_count', (object) [
          'value' => $project_values['field_pod_count[0][value]'],
        ],
      ],
    ];

    $mock_project->expects($this->any())
      ->method('get')
      ->willReturnMap($project_value_map);

    $mock_project->expects($this->any())
      ->method('getName')
      ->willReturn($project_name);

    $mock_entity_type = $this->createMock(EntityTypeInterface::class);
    $mock_entity_type->expects($this->any())
      ->method('getProvider')
      ->willReturn('k8s');

    $mock_project->expects($this->any())
      ->method('getEntityType')
      ->willReturn($mock_entity_type);

    $return = $this->plugin->launch(
      $mock_project,
      $this->createMock(FormStateInterface::class)
    );

    $this->assertNotNull($return);
  }

}
