(function () {
  'use strict';

  // Resets the for attribute of Label rewritten by tom-select to its original value.
  const resetLabel = function (label_for) {
    const control_id	= label_for + '-ts-control';
    const query = "label[for='" + control_id + "']";
    const label = document.querySelector(query);
    if (label) {
      label.setAttribute('for', label_for);
    }
  }

  // Flavor.
  new TomSelect("#edit-field-flavor", {
    create: false,
    dropdownParent: 'body',
    plugins: [
      'dropdown_input',
      'no_backspace_delete',
    ],
    render: {
      dropdown: function () {
        return '<div><div style="font-weight: bold;">' +
            '<span style="width: 50%; display: inline-block;">Name</span>' +
            '<span style="width: 25%; display: inline-block;">vCPUs</span>' +
            '<span style="width: 25%; display: inline-block;">RAM (MB)</span>' +
            '</div></div>';
      },
      option: function (data, escape) {
        const splitLabel = escape(data.text).split(':');
        while (splitLabel.length < 3) {
          splitLabel.push('');
        }
        return '<div>' +
            '<span style="width: 50%; display: inline-block;">' + splitLabel[0] + '</span>' +
            '<span style="width: 25%; display: inline-block;">' + splitLabel[1] + '</span>' +
            '<span style="width: 25%; display: inline-block;">' + splitLabel[2] + '</span>' +
            '</div>';
      },
      item: function (data, escape) {
        const splitLabel = escape(data.text).split(':');
        return '<div title="' + splitLabel[0] + '">' + splitLabel[0] + '</div>';
      }
    }
  });
  resetLabel("edit-field-flavor");
})();
