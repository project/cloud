(function (Drupal) {

  Drupal.behaviors.openStackInstanceConsole = {
    attach: function (context, settings) {
      // Get the iframe element.
      const iframe = document.getElementById('console-iframe');
      // Flag to track whether the iframe has loaded.
      let iframeLoaded = false;

      // Add an event listener for the 'load' event of the iframe.
      iframe.addEventListener('load', function() {
        // Update the flag when the iframe has loaded.
        iframeLoaded = true;
        // Cancel the timeout since the iframe has loaded.
        clearTimeout(timeoutId);
      });

      // Set the timeout duration in milliseconds.
      const timeoutDuration = 10000; // 10 seconds

      // Set a timeout to handle the case when the iframe does not load within
      // the specified time.
      const timeoutId = setTimeout(function() {
        if (!iframeLoaded) {
          // If the iframe has not loaded, stop its loading and display a blank
          // page.
          iframe.src = 'about:blank';
        }
      }, timeoutDuration);
    }
  };

})(Drupal);
