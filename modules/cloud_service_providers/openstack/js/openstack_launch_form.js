(function (Drupal) {
  'use strict';

  Drupal.behaviors.myCustomBehavior = {
    attach: function (context, settings) {
      let flavorElement = document.querySelector('.field--name-field-flavor .field--item');
      let flavor = flavorElement ? flavorElement.innerHTML : null;

      if (!flavor) {
        return;
      }

      let tableCells = Array.from(document.querySelectorAll('td.views-field-name'));
      let td = tableCells.find(td => td.textContent.includes(flavor));

      if (!td) {
        return;
      }

      // Highlight the row.
      td.parentElement.classList.add('highlight');
    }
  };

})(Drupal);
