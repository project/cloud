(function (Drupal) {
  Drupal.behaviors.pieChart = {
    attach: function(context, settings) {
      let charts = document.querySelectorAll('.usage-pie-chart');
      charts.forEach(function(chartElement) {
        let used = JSON.parse(chartElement.dataset.used);
        let total = JSON.parse(chartElement.dataset.total);
        let background_colors = ['#428bca', '#ddd'];

        let pieChart = new Chart(chartElement, {
          type: 'pie',
          data: {
            labels: ['Used', 'Not used'],
            datasets: [{
              data: [used, total - used],
              backgroundColor: background_colors,
              hoverBackgroundColor: background_colors,
              borderWidth: 0
            }]
          },
          options: {
            legend: {
              display: false
            }
          }
        });
      });
    }
  };
})(Drupal);
