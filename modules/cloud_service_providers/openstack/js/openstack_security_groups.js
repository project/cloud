(function (Drupal) {
  'use strict';

  Drupal.OpenStackSecurityGroup = Drupal.OpenStackSecurityGroup || {};

  Drupal.OpenStackSecurityGroup.changeField = {
    hidePortFields: function (protocol, field, key) {
      let from_port = document.querySelector('.form-item-' + field + '-' + key + '-from-port');
      let to_port = document.querySelector('.form-item-' + field + '-' + key + '-to-port');

      let from_port_input = document.querySelector('.form-item-' + field + '-' + key + '-from-port input');
      let to_port_input = document.querySelector('.form-item-' + field + '-' + key + '-to-port input');

      if (['tcp', 'udp', 'icmp', 'icmpv6'].includes(protocol)) {

        from_port.style.display = 'block';
        to_port.style.display = 'block';

        if (protocol !== 'icmp' && from_port_input.value === '-1' && to_port_input.value === '-1') {

          from_port_input.value = '';
          to_port_input.value = '';
        }

        return;
      }

      from_port.style.display = 'none';
      to_port.style.display = 'none';
      from_port_input.value = -1;
      to_port_input.value = -1;
    },
  };

  Drupal.behaviors.openstackSecurityPermissions = {
    attach: function (context, settings) {
      // Inbound rules.
      let inbound_rules = context.querySelector('table.ip-permission-values');
      if (!inbound_rules) return;

      let inbound_ip_protocol_selects = inbound_rules.querySelectorAll('.ip-protocol-select.form-select.form-control');

      inbound_ip_protocol_selects.forEach(function (el, k) {
        el.addEventListener('change', function () {
          Drupal.OpenStackSecurityGroup.changeField.hidePortFields(this.value, 'ip-permission', k);
        });

        Drupal.OpenStackSecurityGroup.changeField.hidePortFields(el.value, 'ip-permission', k);
      });

      // Outbound rules.
      let outbound_rules = context.querySelector('table.outbound-permission-values');
      if (!outbound_rules) return;

      let outbound_ip_protocol_selects = outbound_rules.querySelectorAll('.ip-protocol-select.form-select.form-control');

      outbound_ip_protocol_selects.forEach(function (el, k) {
        el.addEventListener('change', function () {
          Drupal.OpenStackSecurityGroup.changeField.hidePortFields(this.value, 'outbound-permission', k);
        });

        Drupal.OpenStackSecurityGroup.changeField.hidePortFields(el.value, 'outbound-permission', k);
      });

    },
  };
})(Drupal);
