<?php

/**
 * @file
 * Builds placeholder replacement tokens for openstack-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function openstack_token_info(): array {

  $types['openstack_quota'] = [
    'name' => t('OpenStack quota'),
    'description' => t('Tokens related to individual OpenStack quota.'),
    'needs-data' => 'openstack_quota',
  ];
  $quota['name'] = [
    'name' => t('Quota name'),
    'description' => t('Enter the name of the quota entity.'),
  ];
  $quota['quota_link'] = [
    'name' => t('Quota link'),
    'description' => t('An absolute link to quota.'),
  ];
  $quota['quota_edit_link'] = [
    'name' => t('Quota edit link'),
    'description' => t('An absolute link to edit the quota.'),
  ];
  $quota['changed'] = [
    'name' => t('Change date'),
    'description' => t('The quota change date.'),
  ];

  $types['openstack_quota_request'] = [
    'name' => t('OpenStack quota request'),
    'description' => t('Tokens related to individual OpenStack quota request.'),
    'needs-data' => 'openstack_quota_request',
  ];
  $quota_request['name'] = [
    'name' => t('Quota name'),
    'description' => t('Enter the name of the quota entity.'),
  ];
  $quota_request['quota_link'] = [
    'name' => t('Quota link'),
    'description' => t('An absolute link to quota.'),
  ];
  $quota_request['quota_edit_link'] = [
    'name' => t('Quota edit link'),
    'description' => t('An absolute link to edit the quota.'),
  ];
  $quota_request['changed'] = [
    'name' => t('Change date'),
    'description' => t('The quota change date.'),
  ];
  $quota_request['quota_button_approve'] = [
    'name' => t('Approve button'),
    'description' => t('The quota approve button.'),
  ];

  $types['openstack_quota_email'] = [
    'name' => t('OpenStack quota email'),
    'description' => t('Tokens related to individual OpenStack quota email.'),
    'needs-data' => 'openstack_quota_email',
  ];
  $quota_email['quotas'] = [
    'name' => t('List of quotas'),
    'description' => t('List of quotas to display to a user.'),
  ];

  $types['openstack_quota_request_email'] = [
    'name' => t('OpenStack quota request email'),
    'description' => t('Tokens related to individual OpenStack quota request email.'),
    'needs-data' => 'openstack_quota_request_email',
  ];
  $openstack_request_email['quotas_request'] = [
    'name' => t('List of request quotas'),
    'description' => t('List of request quotas to display to a user.'),
  ];
  return [
    'types' => $types,
    'tokens' => [
      'openstack_quota' => $quota,
      'openstack_quota_request' => $quota_request,
      'openstack_quota_email' => $quota_email,
      'openstack_quota_request_email' => $openstack_request_email,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function openstack_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $replacements = [];
  if ($type === 'openstack_quota' && !empty($data['openstack_quota'])) {
    $replacements = openstack_quota_tokens($tokens, $data);
  }
  elseif ($type === 'openstack_quota_request' && !empty($data['openstack_quota_request'])) {
    $replacements = openstack_quota_request_tokens($tokens, $data);
  }
  elseif ($type === 'openstack_quota_email') {
    $replacements = openstack_quota_email_tokens($tokens, $data);
  }
  elseif ($type === 'openstack_quota_request_email') {
    $replacements = openstack_quota_request_email_tokens($tokens, $data);
  }
  return $replacements;
}

/**
 * Setup quota email token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function openstack_quota_email_tokens(array $tokens, array $data): array {
  $replacements = [];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'quotas':
        $replacements[$original] = $data['quotas'];
        break;
    }
  }
  return $replacements;
}

/**
 * Setup quota request email token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function openstack_quota_request_email_tokens(array $tokens, array $data): array {
  $replacements = [];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'quotas_request':
        $replacements[$original] = $data['quotas_request'];
        break;

      case 'site_url':
        global $base_url;
        $replacements[$original] = t('<a href=":url">:url</a>', [
          ':url' => $base_url,
        ]);
    }
  }
  return $replacements;
}

/**
 * Setup quota token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function openstack_quota_tokens(array $tokens, array $data): array {
  $replacements = [];
  /** @var Drupal\openstack\Entity\OpenStackQuota $quota */
  $quota = $data['openstack_quota'];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'name':
        $replacements[$original] = $quota->getName();
        break;

      case 'quota_link':
        $replacements[$original] = $quota->toUrl('canonical', ['absolute' => TRUE])->toString();
        break;

      case 'quota_link_edit':
        $replacements[$original] = $quota->toUrl('edit-form', ['absolute' => TRUE])
          ->toString();
        break;

      case 'changed':
        $replacements[$original] = \Drupal::service('date.formatter')->format($quota->changed->value, 'custom', 'Y-m-d H:i:s O');
        break;
    }
  }
  return $replacements;
}

/**
 * Setup quota token substitution.
 *
 * @param array $tokens
 *   Tokens array.
 * @param array $data
 *   Data array.
 *
 * @return array
 *   The initialized replacement array.
 */
function openstack_quota_request_tokens(array $tokens, array $data): array {
  $replacements = [];
  /** @var Drupal\cloud\Entity\CloudLaunchTemplate $quota */
  $quota = $data['openstack_quota_request'];
  foreach ($tokens ?: [] as $name => $original) {
    switch ($name) {
      case 'name':
        $replacements[$original] = $quota->getName();
        break;

      case 'quota_link':
        $replacements[$original] = t('<a href=":url">:url</a>', [
          ':url' => $quota->toUrl('canonical', ['absolute' => TRUE])->toString(),
        ]);
        break;

      case 'quota_link_edit':
        $replacements[$original] = t('<a href=":url">:url</a>', [
          ':url' => $quota->toUrl('edit-form', ['absolute' => TRUE])->toString(),
        ]);
        break;

      case 'quota_button_approve':
        $replacements[$original] = $quota->toUrl('edit-form', ['absolute' => TRUE])->toString();
        break;

      case 'changed':
        $replacements[$original] = \Drupal::service('date.formatter')->format($quota->changed->value, 'custom', 'Y-m-d H:i:s O');
        break;
    }
  }
  return $replacements;
}
