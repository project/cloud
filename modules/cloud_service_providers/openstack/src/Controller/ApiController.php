<?php

namespace Drupal\openstack\Controller;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\cloud\Controller\CloudApiControllerBase;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Entity\IntermediateFormState;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Traits\AccessCheckTrait;
use Drupal\cloud\Traits\CloudResourceTrait;
use Drupal\openstack\Entity\OpenStackImageInterface;
use Drupal\openstack\Service\OpenStackOperationsServiceInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Drupal\openstack\Service\OpenStackServiceInterface;
use Drupal\openstack\Traits\OpenStackFormSelectTrait;
use Drupal\views\Views;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Route;

/**
 * Api controller for interacting with OpenStack.
 */
class ApiController extends CloudApiControllerBase implements ApiControllerInterface {

  use CloudResourceTrait;
  use AccessCheckTrait;
  use OpenStackFormSelectTrait;

  /**
   * The OpenStack Service.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  private $openStackServiceFactory;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Cloud service interface.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The configuration data.
   *
   * @var array
   */
  protected $configuration = ['cloud_context' => ''];

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The OpenStack Operations Service.
   *
   * @var \Drupal\openstack\Service\OpenStackOperationsServiceInterface
   */
  protected $openStackOperationsService;

  /**
   * The OpenStack Service.
   *
   * @var \Drupal\openstack\Service\Rest\OpenStackServiceInterface
   */
  protected $openStackService;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * Guzzle Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * ApiController constructor.
   *
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   Cloud service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\openstack\Service\OpenStackOperationsServiceInterface $openstack_operations_service
   *   The OpenStack Operations service.
   * @param \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service
   *   The OpenStack Service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    OpenStackServiceFactoryInterface $openstack_service_factory,
    EntityTypeManagerInterface $entityTypeManager,
    RequestStack $request_stack,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    RendererInterface $renderer,
    CloudServiceInterface $cloud_service,
    AccountInterface $current_user,
    OpenStackOperationsServiceInterface $openstack_operations_service,
    OpenStackServiceInterface $openstack_service,
    ClientInterface $http_client,
    TranslationInterface $string_translation,
  ) {
    $this->openStackServiceFactory = $openstack_service_factory;
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $request_stack;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->renderer = $renderer;
    $this->cloudService = $cloud_service;
    $this->currentUser = $current_user;
    $this->openStackOperationsService = $openstack_operations_service;
    $this->openStackService = $openstack_service;
    $this->httpClient = $http_client;
    $this->stringTranslation = $string_translation;

    $this->messenger();
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return ApiController
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openstack.factory'),
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('renderer'),
      $container->get('cloud'),
      $container->get('current_user'),
      $container->get('openstack.operations'),
      $container->get('openstack.rest'),
      $container->get('http_client'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route, string $entity_type_id, string $entity_id, string $command): AccessResultInterface {
    $entity = $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->load($entity_id);

    if (!empty($entity) && $entity instanceof EntityInterface) {
      return $entity->access($command, $account, TRUE);
    }
    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function updateMessages($resource_type, array $updated): void {
    $labels = $this->getDisplayLabels($resource_type);
    if (isset($updated[0])) {
      if ($updated[0]) {
        $this->messageUser($this->t('Updated @resource_name.', ['@resource_name' => $labels['plural']]));
      }
      else {
        $this->messageUser($this->t('Unable to update @resource_name.', ['@resource_name' => $labels['plural']]), 'error');
      }
    }
    $this->cloudService->invalidateCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function updateInstanceList($cloud_context = ''): RedirectResponse {
    $response = $this->updateEntityList('openstack_instance', $cloud_context);
    // Update availability zones when updating instances.
    return $this->updateEntityList('openstack_availability_zone', $cloud_context, $response);
  }

  /**
   * {@inheritdoc}
   */
  public function updateImageList($cloud_context = ''): RedirectResponse {
    if (empty($cloud_context)) {
      return $this->updateEntityList('openstack_image', $cloud_context);
    }

    /** @var \Drupal\cloud\Entity\CloudConfigInterface[] $cloud_configs */
    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
      ]);

    $redirect_response = NULL;
    // Update the status of other regions
    // so that the status of the image sharing project can be updated as well.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $response = $this->updateEntityList('openstack_image', $cloud_config->getCloudContext());
      $redirect_response = $cloud_context === $cloud_config->getCloudContext() ? $response : $redirect_response;
    }
    return $redirect_response;
  }

  /**
   * {@inheritdoc}
   */
  public function updateKeyPairList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_key_pair', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateSecurityGroupList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_security_group', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateVolumeList($cloud_context = ''): RedirectResponse {
    $response = $this->updateEntityList('openstack_volume', $cloud_context);
    // Update availability zones when updating volumes.
    return $this->updateEntityList('openstack_availability_zone', $cloud_context, $response);
  }

  /**
   * {@inheritdoc}
   */
  public function updateSnapshotList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_snapshot', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateNetworkList($cloud_context = ''): RedirectResponse {
    $response = $this->updateEntityList('openstack_network', $cloud_context);
    // Update availability zones when updating networks.
    return $this->updateEntityList('openstack_availability_zone', $cloud_context, $response);
  }

  /**
   * {@inheritdoc}
   */
  public function updateSubnetList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_subnet', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updatePortList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_port', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateRouterList($cloud_context = ''): RedirectResponse {
    $response = $this->updateEntityList('openstack_router', $cloud_context);
    // Update availability zones when updating routers.
    return $this->updateEntityList('openstack_availability_zone', $cloud_context, $response);
  }

  /**
   * {@inheritdoc}
   */
  public function updateQuotaList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_quota', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateStackList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_stack', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateTemplateVersionList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_template_version', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateProjectList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_project', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateRoleList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_role', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateUserList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_user', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateFloatingIpList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_floating_ip', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateServerGroupList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_server_group', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateFlavorList($cloud_context = ''): RedirectResponse {
    return $this->updateEntityList('openstack_flavor', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAll(): RedirectResponse {
    $regions = $this->requestStack->getCurrentRequest()->query->get('regions');
    if ($regions === NULL) {
      $this->messageUser($this->t('No region specified'), 'error');
    }
    else {
      $regions_array = explode(',', $regions);

      foreach ($regions_array ?: [] as $region) {
        $entity = $this->entityTypeManager()->getStorage('cloud_config')
          ->loadByProperties(
            [
              'cloud_context' => $region,
            ]);
        if ($entity) {
          openstack_update_resources(array_shift($entity));
        }
      }

      $this->messageUser($this->t('Creating cloud service provider was performed successfully.'));
      drupal_flush_all_caches();
    }
    return $this->redirect('entity.cloud_config.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function listImageCallback(): Response {
    return $this->getViewResponse('openstack_image');
  }

  /**
   * Helper method to get views output.
   *
   * @param string $view_id
   *   The ID of list view.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response of list view.
   *
   * @throws \Exception
   *   Thrown when an object is passed in which cannot be printed.
   */
  private function getViewResponse($view_id): Response {
    $view = Views::getView($view_id);

    // Set the display machine name.
    $view->setDisplay('list');

    // Render the view as html, and return it as a response object.
    $build = $view->executeDisplay();
    return new Response($this->renderer->render($build));
  }

  /**
   * Helper method to add messages for the end user.
   *
   * @param string $message
   *   The message.
   * @param string $type
   *   The message type: error or message.
   */
  private function messageUser($message, $type = 'message'): void {
    switch ($type) {
      case 'error':
        $this->messenger->addError($message);
        break;

      case 'message':
        $this->messenger->addStatus($message);
        break;

      default:
        break;
    }
  }

  /**
   * Helper method to update entities.
   *
   * @param string $entity_type_name
   *   The entity type name.
   * @param string $cloud_context
   *   The cloud context.
   * @param \Symfony\Component\HttpFoundation\RedirectResponse|null $response
   *   The redirect response.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   */
  private function updateEntityList($entity_type_name, $cloud_context, ?RedirectResponse $response = NULL): RedirectResponse {
    // If cloud context is not set, all cloud contexts are targeted.
    if (empty($cloud_context)) {
      return $this->updateAllEntityList($entity_type_name, $response);
    }

    // Calculate the entity label from the entity type name.
    $plural_label = $this->getShortEntityTypeNamePluralCamelByTypeName($entity_type_name, TRUE);

    // Update the resource list.
    $update_method_name = 'update' . $this->getShortEntityTypeNamePluralCamelByTypeName($entity_type_name);
    $openstack_service = $this->openStackServiceFactory->get($cloud_context);
    $updated = $openstack_service->$update_method_name();

    if ($updated !== FALSE) {
      $this->messageUser($this->t('Updated @name.', ['@name' => $plural_label]));
    }
    else {
      $this->messageUser($this->t('Unable to update @name.', ['@name' => $plural_label]), 'error');
    }

    // Update the cache.
    $this->cloudService->invalidateCacheTags();

    if ($response) {
      return $response;
    }

    return $this->redirect("view.$entity_type_name.list", [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Helper method to update all entity list.
   *
   * @param string $entity_type_name
   *   The entity type name.
   * @param \Symfony\Component\HttpFoundation\RedirectResponse|null $response
   *   The redirect response.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function updateAllEntityList($entity_type_name, ?RedirectResponse $response = NULL): RedirectResponse {
    /** @var \Drupal\cloud\Entity\CloudConfigInterface[] $cloud_configs */
    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
      ]);

    // Calculate the entity label from the entity type name.
    $plural_label = $this->getShortEntityTypeNamePluralCamelByTypeName($entity_type_name, TRUE);
    $update_method_name = 'update' . $this->getShortEntityTypeNamePluralCamelByTypeName($entity_type_name);

    foreach ($cloud_configs ?: [] as $cloud_config) {

      $cloud_context = $cloud_config->getCloudContext();
      $resource_link = $this->hasEntityLink($entity_type_name, 'collection')
        ? $cloud_config->getResourceLink($plural_label, $entity_type_name)
        : $plural_label;
      $cloud_config_link = !empty($cloud_config->getCloudConfigLink()) ? $cloud_config->getCloudConfigLink() : $cloud_config->getName();
      try {
        $openstack_service = $this->openStackServiceFactory->get($cloud_context);
        $updated = $openstack_service->$update_method_name();

        $success_message = $this->t('Updated %resources of %cloud_config cloud service provider.', [
          '%resources' => $resource_link ?? $plural_label,
          '%cloud_config' => $cloud_config_link,
        ]);

        $error_message = $this->t('Unable to update %resources of %cloud_config cloud service provider.', [
          '%resources' => $resource_link ?? $plural_label,
          '%cloud_config' => $cloud_config_link,
        ]);

        $updated === TRUE
        ? $this->messageUser($success_message)
        : $this->messageUser($error_message, 'error');
      }
      catch (\Exception $e) {
        $this->messenger->addError($this->t('Unable to update @resources of @cloud_config cloud service provider.', [
          '@resources' => $plural_label,
          '@cloud_config' => $cloud_config->getName(),
        ]));
      }
    }

    // Update the cache.
    $this->cloudService->invalidateCacheTags();

    if (!empty($response)) {
      return $response;
    }

    return $this->redirect("view.$entity_type_name.all");
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityCount(string $cloud_context, string $entity_type_id): JsonResponse {
    $params = !empty($cloud_context)
      ? ['cloud_context' => $cloud_context]
      : [];

    if (!empty($cloud_context)) {
      $this->configuration['cloud_context'] = $cloud_context;
    }

    $count = $this->getResourceCountWithAccessCheck(
      $entity_type_id,
      $params
    );

    return new JsonResponse(['count' => $count]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsoleOutput(string $cloud_context, string $entity_id): JsonResponse {
    $entity = $this->entityTypeManager
      ->getStorage('openstack_instance')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The entity has already been deleted.',
      ], 404);
    }

    $form = [];
    $form_state = new IntermediateFormState();
    $output = $this->openStackOperationsService->getConsoleOutput($entity, $form, $form_state);

    return new JsonResponse([
      'result' => 'OK',
      'log' => $output,
    ], 200);
  }

  /**
   * Read data source from file.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   * @param string $key
   *   The key of HTTP request.
   *
   * @return string|null
   *   If data cannot be read from a file, NULL is returned.
   */
  private function readSourceFromFile(Request $request, string $key): string|NULL {
    /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
    $file = $request->files->get($key);
    if (empty($file)) {
      return NULL;
    }
    $path = $file->getRealPath();
    $handle = fopen($path, 'r');
    if (empty($handle)) {
      return NULL;
    }
    $output = fread($handle, filesize($path));
    if (empty($output)) {
      return NULL;
    }
    if (empty(fclose($handle))) {
      return NULL;
    }
    return $output;
  }

  /**
   * Read data source from URL.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   * @param string $key
   *   The key of HTTP request.
   *
   * @return string|null
   *   If data cannot be read from a URL, NULL is returned.
   */
  private function readSourceFromUrl(Request $request, string $key): string|NULL {
    try {
      return (string) $this->httpClient->request('GET', $request->get($key, ''))->getBody();
    }
    catch (\Exception $e) {
      $this->logger('openstack')->error($e->getMessage());
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preCreateOpenStackStack(Request $request): JsonResponse {
    // Read the template source.
    $template_source = $request->get('template_source', '');
    if (empty($template_source)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => "Must set template source's type.",
      ], 400);
    }
    $template_text = '';
    switch ($template_source) {
      case 'file':
        $template_text = $this->readSourceFromFile($request, 'template_file');
        if (empty($template_text)) {
          return new JsonResponse([
            'result' => 'NG',
            'reason' => "Can't read the template file.",
          ], 400);
        }
        break;

      case 'url':
        $template_text = $this->readSourceFromUrl($request, 'template_url');
        if (empty($template_text)) {
          return new JsonResponse([
            'result' => 'NG',
            'reason' => "Can't read the URL.",
          ], 400);
        }
        break;

      case 'data':
        $template_text = $request->get('template_data', '');
        if (empty($template_text)) {
          return new JsonResponse([
            'result' => 'NG',
            'reason' => "Can't read the template Input.",
          ], 400);
        }
        break;

    }

    $template_data = [];
    try {
      $template_data = Yaml::decode($template_text);
      if (!empty($template_data['heat_template_version']) && is_int($template_data['heat_template_version'])) {
        $template_data['heat_template_version'] = gmdate('Y-m-d', $template_data['heat_template_version']);
      }
    }
    catch (\Exception $e) {
      $this->logger('openstack')->error($e->getMessage());
    }
    if (empty($template_data)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => "Can't read template's YAML.",
      ], 400);
    }

    // Read the environment source.
    $environment_source = $request->get('environment_source', '');
    $environment_text = '';
    switch ($environment_source) {
      case 'file':
        $environment_text = $this->readSourceFromFile($request, 'environment_file');
        if (empty($environment_text)) {
          return new JsonResponse([
            'result' => 'NG',
            'reason' => "Can't read the environment file.",
          ], 400);
        }
        break;

      case 'data':
        $environment_text = $request->get('environment_data', '');
        if (empty($environment_text)) {
          return new JsonResponse([
            'result' => 'NG',
            'reason' => "Can't read the environment Input.",
          ], 400);
        }
        break;

    }

    return new JsonResponse([
      'templateData' => $template_data,
      'template' => $template_text,
      'environment' => $environment_text,
    ], 200);
  }

  /**
   * {@inheritdoc}
   */
  private function createEntity(string $cloud_context, string $entity_type_id, string $entity_id, string $command) {
    if ($command !== 'create' && $command !== 'import' && $command !== 'preview' && !empty($entity_id)) {
      return $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->load($entity_id);
    }

    $parameters = $entity_type_id === 'cloud_launch_template'
      ? [
        'cloud_context' => $cloud_context,
        'type' => 'openstack',
      ]
      : [
        'cloud_context' => $cloud_context,
      ];
    return $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->create($parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function operateEntity(Request $request, string $cloud_context, string $entity_type_id, string $entity_id, string $command): JsonResponse {
    // Create an instance of the entity.
    // The process splits between adding a new entity
    // and when reading an existing entity.
    $entity = $this->createEntity($cloud_context, $entity_type_id, $entity_id, $command);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The entity has already been deleted.',
      ], 404);
    }

    // Depending on the content of the process,
    // branching is performed with a switch statement.
    $form = [];
    $form_state = new IntermediateFormState();
    $method_name = '';
    $extra_parameters = [];
    switch ($command . '_' . $entity_type_id) {
      case 'create_image_openstack_instance':
        $form_state->setValue('image_name', $request->get('image_name', ''));

        $method_name = 'createOpenStackImageFromInstance';
        break;

      case 'edit_openstack_instance':
        /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity */
        $entity->setName($request->get('name', ''));
        $entity->set('termination_protection', json_decode($request->get('termination_protection', '"false"'), TRUE));
        $form_state->setValue('tags', json_decode($request->get('tags', '[]'), TRUE));

        $raw_security_groups = json_decode($request->get('security_groups', '[]'), TRUE);
        $form_state->setValue('security_groups', array_combine($raw_security_groups, $raw_security_groups));
        $form['network']['security_groups']['#default_value'] = array_combine(
          range(0, count($raw_security_groups) - 1),
          $raw_security_groups
        );

        $method_name = 'editOpenStackInstance';

        break;

      case 'terminate_openstack_instance':
        $method_name = 'deleteOpenStackInstance';
        break;

      case 'start_openstack_instance':
        $method_name = 'startOpenStackInstance';
        break;

      case 'stop_openstack_instance':
        $method_name = 'stopOpenStackInstance';
        break;

      case 'reboot_openstack_instance':
        $form_state->setValue('type', $request->get('type', '') === 'true');
        $method_name = 'rebootOpenStackInstance';
        break;

      case 'attach_interface_openstack_instance':
        $form_state->setValue('attach_type', $request->get('type', ''));
        $form_state->setValue('network', $request->get('network', ''));
        $form_state->setValue('fixed_ip', $request->get('fixed_ip', ''));
        $form_state->setValue('port', $request->get('port', ''));
        $method_name = 'attachOpenStackNetworkInterface';
        break;

      case 'detach_interface_openstack_instance':
        $form_state->setValue('port_id', $request->get('port_id', ''));
        $method_name = 'detachOpenStackNetworkInterface';
        break;

      case 'create_openstack_image':
        /** @var \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity */
        $entity->setName($request->get('name', ''));
        $entity = $this->setOpenStackImageSharedProjectsFromRequest($entity, $request);
        $form_state->set('name', $request->get('name', ''));
        $method_name = 'createOpenStackImage';
        break;

      case 'edit_openstack_image':
        /** @var \Drupal\aws_cloud\Entity\Ec2\ImageInterface $entity */
        $entity->setName($request->get('name', ''));
        $entity->setImageId($request->get('image_id', ''));
        $entity = $this->setOpenStackImageSharedProjectsFromRequest($entity, $request);
        $method_name = 'editOpenStackImage';
        break;

      case 'delete_openstack_image':
        $method_name = 'deleteOpenStackImage';
        break;

      case 'create_openstack_key_pair':
        /** @var \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity */
        $entity->set('key_pair_name', $request->get('key_pair_name', ''));
        $method_name = 'createOpenStackKeyPair';
        break;

      case 'edit_openstack_key_pair':
        $method_name = 'editOpenStackKeyPair';
        break;

      case 'delete_openstack_key_pair':
        $method_name = 'deleteOpenStackKeyPair';
        break;

      case 'import_openstack_key_pair':
        /** @var \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity */
        $entity->set('key_pair_name', $request->get('key_pair_name', ''));
        /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
        $file = $request->files->get('key_pair_public_key');
        if (empty($file)) {
          return new JsonResponse([
            'result' => 'NG',
            'reason' => 'Internal Server Error',
          ], 500);
        }
        $form_state->setValue('key_pair_public_key', $file->getRealPath());
        $method_name = 'importOpenStackKeyPair';
        break;

      case 'create_openstack_floating_ip':
        /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
        $entity->setName($request->get('name', ''));
        $form_state->setValue('floating_network_id', $request->get('floating_network_id', ''));
        $method_name = 'createOpenStackFloatingIp';
        break;

      case 'edit_openstack_floating_ip':
        /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity */
        $entity->setName($request->get('name', ''));
        $method_name = 'editOpenStackFloatingIp';
        break;

      case 'delete_openstack_floating_ip':
        $method_name = 'deleteOpenStackFloatingIp';
        break;

      case 'associate_openstack_floating_ip':
        $form_state->setValue('resource_type', $request->get('resource_type', ''));
        $form_state->setValue('instance_id', $request->get('instance_id', '-1'));
        $form_state->setValue('instance_private_ip', $request->get('instance_private_ip', ''));
        $form_state->setValue('network_id', $request->get('network_id', '-1'));
        $form_state->setValue('network_private_ip', $request->get('network_private_ip', ''));
        $form_state->setValue('port_id_ip', $request->get('port_id_ip', ''));
        $method_name = 'associateOpenStackFloatingIp';
        break;

      case 'disassociate_openstack_floating_ip':
        $method_name = 'disassociateOpenStackFloatingIp';
        break;

      case 'create_openstack_security_group':
        /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
        $entity->setName($request->get('group_name', ''));
        $entity->setGroupName($request->get('group_name', ''));
        $entity->setDescription($request->get('description', ''));
        $method_name = 'createOpenStackSecurityGroup';
        break;

      case 'delete_openstack_security_group':
        $method_name = 'deleteOpenStackSecurityGroup';
        break;

      case 'edit_openstack_security_group':
        /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
        $entity->setName($request->get('name', ''));
        $entity->setGroupName($request->get('group_name', ''));
        $form_state->set('ip_permission', json_decode($request->get('ip_permission', '[]'), TRUE));
        $form_state->set('outbound_permission', json_decode($request->get('outbound_permission', '[]'), TRUE));
        $method_name = 'editOpenStackSecurityGroup';
        break;

      case 'revoke_openstack_security_group':
        $method_name = 'revokeOpenStackSecurityGroup';
        break;

      case 'create_openstack_snapshot':
        /** @var \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity */
        $entity->setName($request->get('name', ''));
        $entity->set('volume_id', $request->get('volume_id', ''));
        $entity->set('description', $request->get('description', ''));
        $method_name = 'createOpenStackSnapshot';
        break;

      case 'delete_openstack_snapshot':
        $method_name = 'deleteOpenStackSnapshot';
        break;

      case 'edit_openstack_snapshot':
        /** @var \Drupal\aws_cloud\Entity\Ec2\Snapshot $entity */
        $entity->setName($request->get('name', ''));
        $entity->setDescription($request->get('description', ''));
        $method_name = 'editOpenStackSnapshot';
        break;

      case 'create_openstack_volume':
        /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity */
        $entity->setName($request->get('name', ''));
        $entity->setSnapshotId($request->get('snapshot_id', ''));
        $entity->setSize($request->get('size', ''));
        $entity->setVolumeType($request->get('volume_type', ''));
        $entity->set('availability_zone', $request->get('availability_zone', ''));
        $method_name = 'createOpenStackVolume';
        break;

      case 'delete_openstack_volume':
        $method_name = 'deleteOpenStackVolume';
        break;

      case 'edit_openstack_volume':
        /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity */
        $entity->setName($request->get('name', ''));
        $method_name = 'editOpenStackVolume';
        break;

      case 'attach_openstack_volume':
        $form_state->setValue('device_name', $request->get('device_name', ''));
        $form_state->setValue('instance_id', $request->get('instance_id', ''));
        $method_name = 'attachOpenStackVolume';
        break;

      case 'detach_openstack_volume':
        $method_name = 'detachOpenStackVolume';
        break;

      case 'create_cloud_launch_template':
        /** @var \Drupal\cloud\Entity\CloudLaunchTemplate $entity */
        $entity->set('name', $request->get('name', ''));
        $entity->set('field_description', $request->get('description', ''));
        $entity->set('field_flavor', $request->get('field_flavor', ''));
        $entity->set('field_test_only', json_decode($request->get('field_test_only', '"false"'), FALSE));
        $entity->set('field_openstack_image_id', $request->get('field_openstack_image_id', ''));
        $entity->set('field_os_availability_zone', $request->get('field_os_availability_zone', ''));
        $entity->set('field_openstack_security_group', json_decode($request->get('security_groups', '[]'), TRUE));
        $entity->set('field_openstack_ssh_key', $request->get('ssh_key', ''));
        $entity->set('field_openstack_network', $request->get('network_id', ''));
        $server_group = $request->get('field_openstack_server_group', '');
        $server_group = empty($server_group) || $server_group === "'_none'" ? NULL : $server_group;
        $entity->set('field_openstack_server_group', $server_group);
        $entity->set('field_tags', json_decode($request->get('field_tags', '[]'), TRUE));
        $entity->set('field_user_data', $request->get('field_user_data', ''));
        $method_name = 'createOpenStackCloudLaunchTemplate';
        break;

      case 'copy_cloud_launch_template':
        /** @var \Drupal\cloud\Entity\CloudLaunchTemplate $entity */
        $entity->set('name', $request->get('name', ''));
        $entity->set('field_description', $request->get('field_description', ''));
        $entity->set('field_instance_type', $request->get('field_instance_type', ''));
        $entity->set('field_flavor', $request->get('field_flavor', ''));
        $entity->set('field_test_only', json_decode($request->get('field_test_only', '"false"'), FALSE));
        $entity->set('field_openstack_image_id', $request->get('field_openstack_image_id', ''));
        $entity->set('field_os_availability_zone', $request->get('field_os_availability_zone', ''));
        $entity->set('field_openstack_security_group', json_decode($request->get('security_groups', '[]'), TRUE));
        $entity->set('field_openstack_ssh_key', $request->get('ssh_key', ''));
        $entity->set('field_openstack_network', $request->get('network_id', ''));
        $entity->set('field_openstack_server_group', $request->get('field_openstack_server_group', ''));
        $entity->set('field_tags', json_decode($request->get('tags', '[]'), TRUE));
        $entity->set('revision_log_message', $request->get('revision_log_message', ''));
        $method_name = 'copyOpenStackCloudLaunchTemplate';
        break;

      case 'edit_cloud_launch_template':
        /** @var \Drupal\cloud\Entity\CloudLaunchTemplate $entity */
        $entity->set('field_description', $request->get('field_description', ''));
        $entity->set('field_flavor', $request->get('field_flavor', ''));
        $entity->set('field_test_only', json_decode($request->get('field_test_only', '"false"'), FALSE));
        $entity->set('field_openstack_image_id', $request->get('field_openstack_image_id', ''));
        $entity->set('field_os_availability_zone', $request->get('field_os_availability_zone', ''));
        $entity->set('field_openstack_security_group', json_decode($request->get('security_groups', '[]'), TRUE));
        $entity->set('field_openstack_ssh_key', $request->get('ssh_key', ''));
        $entity->set('field_openstack_network', $request->get('network_id', ''));
        $server_group = $request->get('field_openstack_server_group', '');
        $server_group = empty($server_group) || $server_group === "'_none'" ? NULL : $server_group;
        $entity->set('field_openstack_server_group', $server_group);
        $entity->set('field_tags', json_decode($request->get('field_tags', '[]'), TRUE));
        $entity->set('revision_log_message', $request->get('revision_log_message', ''));
        $method_name = 'editOpenStackCloudLaunchTemplate';
        break;

      case 'review_cloud_launch_template':
        $method_name = 'reviewOpenStackCloudLaunchTemplate';
        break;

      case 'create_openstack_network':
        /** @var \Drupal\openstack\Entity\OpenStackNetwork $entity */
        $entity->setName($request->get('name', ''));
        $entity->setAdminStateUp($request->get('admin_state_up', '') === 'true');
        $entity->setShared($request->get('shared', '') === 'true');
        $entity->setExternal($request->get('external', '') === 'true');
        $entity->setAvailabilityZones(json_decode($request->get('availability_zone', '[]'), TRUE));
        $method_name = 'createOpenStackNetwork';
        break;

      case 'delete_openstack_network':
        $method_name = 'deleteOpenStackNetwork';
        break;

      case 'edit_openstack_network':
        /** @var \Drupal\openstack\Entity\OpenStackNetwork $entity */
        $entity->setName($request->get('name', ''));
        $entity->setAdminStateUp($request->get('admin_state_up', '') === 'true');
        $entity->setShared($request->get('shared', '') === 'true');
        $entity->setExternal($request->get('external', '') === 'true');
        $method_name = 'editOpenStackNetwork';
        break;

      case 'create_openstack_subnet':
        /** @var \Drupal\openstack\Entity\OpenStackSubnetInterface $entity */
        $entity->setName($request->get('name', ''));
        $entity->setNetworkId($request->get('network_id', ''));
        $entity->setCidr($request->get('cidr', ''));
        $entity->setIpVersion($request->get('ip_version', ''));
        $entity->setGatewayIp($request->get('gateway_ip', ''));
        $form_state->setValue('enable_dhcp', $request->get('enable_dhcp', ''));
        $form_state->setValue('disable_gateway', $request->get('disable_gateway', '') === 'true');
        $form_state->setValue('allocation_pools', $request->get('allocation_pools', ''));
        $form_state->setValue('dns_name_servers', $request->get('dns_name_servers', ''));
        $form_state->setValue('host_routes', $request->get('host_routes', ''));
        $method_name = 'createOpenStackSubnet';
        break;

      case 'delete_openstack_subnet':
        $method_name = 'deleteOpenStackSubnet';
        break;

      case 'edit_openstack_subnet':
        /** @var \Drupal\openstack\Entity\OpenStackSubnetInterface $entity */
        $entity->setName($request->get('name', ''));
        $entity->setGatewayIp($request->get('gateway_ip', ''));
        $form_state->setValue('enable_dhcp', $request->get('enable_dhcp', ''));
        $form_state->setValue('disable_gateway', $request->get('disable_gateway', '') === 'true');
        $form_state->setValue('allocation_pools', $request->get('allocation_pools', ''));
        $form_state->setValue('dns_name_servers', $request->get('dns_name_servers', ''));
        $form_state->setValue('host_routes', $request->get('host_routes', ''));
        $method_name = 'editOpenStackSubnet';
        break;

      case 'create_openstack_port':
        /** @var \Drupal\openstack\Entity\OpenStackPortInterface $entity */
        $entity->setName($request->get('name', ''));
        $entity->setNetworkId($request->get('network_id', ''));
        $entity->setDeviceId($request->get('device_id', ''));
        $entity->setDeviceOwner($request->get('device_owner', ''));
        $entity->setMacAddress($request->get('mac_address', ''));
        $entity->setPortSecurityEnabled($request->get('port_security_enabled', '') === 'true');
        $entity->setBindingVifType($request->get('binding_vnic_type', ''));
        $form_state->setValue('ip_address_or_subnet', $request->get('ip_address_or_subnet', ''));
        $form_state->setValue('fixed_ips', $request->get('fixed_ips', ''));
        $form_state->setValue('subnet', $request->get('subnet', ''));
        $security_groups = json_decode($request->get('security_groups', '[]'), TRUE);
        $security_groups = array_flip($security_groups);
        $form_state->setValue('security_groups', $security_groups);
        $method_name = 'createOpenStackPort';
        break;

      case 'delete_openstack_port':
        $method_name = 'deleteOpenStackPort';
        break;

      case 'edit_openstack_port':
        /** @var \Drupal\openstack\Entity\OpenStackPortInterface $entity */
        $entity->setName($request->get('name', ''));
        $entity->setAdminStateUp($request->get('admin_state_up', '') === 'true');
        $entity->setBindingVifType($request->get('binding_vnic_type', ''));
        $entity->setPortSecurityEnabled($request->get('port_security_enabled', '') === 'true');
        $allowed_address_pairs = json_decode($request->get('allowed_address_pairs', '[]'), TRUE);
        foreach ($allowed_address_pairs ?: [] as $idx => $value) {
          if (empty($value['item_key'])) {
            unset($allowed_address_pairs[$idx]);
          }
        }
        $entity->setAllowedAddressPairs($allowed_address_pairs);
        $security_groups = json_decode($request->get('security_groups', '[]'), TRUE);
        $security_groups = array_flip($security_groups);
        $form_state->setValue('security_groups', $security_groups);
        $method_name = 'editOpenStackPort';
        break;

      case 'add_interface_openstack_router':
        /** @var \Drupal\openstack\Entity\OpenStackRouter $entity */
        $form_state->setValue('subnet_id', $request->get('subnet_id', ''));
        $form_state->setValue('ip_address', $request->get('ip_address', ''));
        $method_name = 'addOpenStackRouterInterface';
        break;

      case 'remove_interface_openstack_router':
        /** @var \Drupal\openstack\Entity\OpenStackRouter $entity */
        $form_state->setValue('port_id', $request->get('port_id', ''));
        $method_name = 'removeOpenStackRouterInterface';
        break;

      case 'create_openstack_router':
        /** @var \Drupal\openstack\Entity\OpenStackRouter $entity */
        $entity->setName($request->get('name', ''));
        $entity->setAdminStateUp($request->get('admin_state_up', '') === 'true');
        $entity->setExternalGatewayNetworkId($request->get('external_gateway_network_id', ''));
        $entity->setExternalGatewayEnableSnat($request->get('external_gateway_enable_snat', '') === 'true');
        $entity->setAvailabilityZones(json_decode($request->get('availability_zone', '[]'), TRUE));
        $method_name = 'createOpenStackRouter';
        break;

      case 'delete_openstack_router':
        $method_name = 'deleteOpenStackRouter';
        break;

      case 'edit_openstack_router':
        /** @var \Drupal\openstack\Entity\OpenStackRouter $entity */
        $entity->setName($request->get('name', ''));
        $entity->setAdminStateUp($request->get('admin_state_up', '') === 'true');
        $entity->setExternalGatewayNetworkId($request->get('external_gateway_network_id', ''));
        $entity->setExternalGatewayEnableSnat($request->get('external_gateway_enable_snat', '') === 'true');
        $form_state->set('external_gateway_enable_snat', $request->get('external_gateway_enable_snat', NULL) === 'true');
        $routes = json_decode($request->get('routes', '[]'), TRUE);
        $new_routes = array_filter($routes, fn($route) => $route['item_key'] !== '');
        $entity->setRoutes($new_routes);
        $method_name = 'editOpenStackRouter';
        break;

      case 'edit_openstack_quota':
        /** @var \Drupal\openstack\Entity\OpenStackQuota $entity */
        $entity->setInstances($request->get('instances', 0));
        $entity->setCores($request->get('cores', 0));
        $entity->setRam($request->get('ram', 0));
        $entity->setMetadataItems($request->get('metadata_items', 0));
        $entity->setKeyPairs($request->get('key_pairs', 0));
        $entity->setServerGroups($request->get('server_groups', 0));
        $entity->setServerGroupMembers($request->get('server_group_members', 0));
        $entity->setInjectedFiles($request->get('injected_files', 0));
        $entity->setInjectedFileContentBytes($request->get('injected_file_content_bytes', 0));
        $entity->setInjectedFilePathBytes($request->get('injected_file_path_bytes', 0));
        $entity->setVolumes($request->get('volumes', 0));
        $entity->setSnapshots($request->get('snapshots', 0));
        $entity->setGigaBytes($request->get('gigabytes', 0));
        $entity->setNetwork($request->get('network', 0));
        $entity->setSubnet($request->get('subnet', 0));
        $entity->setPort($request->get('port', 0));
        $entity->setRouter($request->get('router', 0));
        $entity->setFloatingIp($request->get('floatingip', 0));
        $entity->setSecurityGroup($request->get('security_group', 0));
        $entity->setSecurityGroupRule($request->get('security_group_rule', 0));
        $entity->setStatus($request->get('status', 'Draft'));

        $entity->set('instances_new', $request->get('instances', 0));
        $entity->set('cores_new', $request->get('cores', 0));
        $entity->set('ram_new', $request->get('ram', 0));
        $entity->set('metadata_items_new', $request->get('metadata_items', 0));
        $entity->set('key_pairs_new', $request->get('key_pairs', 0));
        $entity->set('server_groups_new', $request->get('server_groups', 0));
        $entity->set('server_group_members_new', $request->get('server_group_members', 0));
        $entity->set('injected_files_new', $request->get('injected_files', 0));
        $entity->set('injected_file_content_bytes_new', $request->get('injected_file_content_bytes', 0));
        $entity->set('injected_file_path_bytes_new', $request->get('injected_file_path_bytes', 0));
        $entity->set('volumes_new', $request->get('volumes', 0));
        $entity->set('snapshots_new', $request->get('snapshots', 0));
        $entity->set('gigabytes_new', $request->get('gigabytes', 0));
        $entity->set('network_new', $request->get('network', 0));
        $entity->set('subnet_new', $request->get('subnet', 0));
        $entity->set('port_new', $request->get('port', 0));
        $entity->set('router_new', $request->get('router', 0));
        $entity->set('floatingip_new', $request->get('floatingip', 0));
        $entity->set('security_group_new', $request->get('security_group', 0));
        $entity->set('security_group_rule_new', $request->get('security_group_rule', 0));
        $method_name = 'editOpenStackQuota';
        break;

      case 'create_openstack_server_group':
        /** @var \Drupal\openstack\Entity\OpenStackServerGroup $entity */
        $entity->setName($request->get('name', ''));
        $entity->setPolicy($request->get('policy', ''));
        $method_name = 'createOpenStackServerGroup';
        break;

      case 'delete_openstack_server_group':
        $method_name = 'deleteOpenStackServerGroup';
        break;

      case 'delete_openstack_stack':
        $method_name = 'deleteOpenStackStack';
        break;

      case 'create_openstack_stack':
        /** @var \Drupal\openstack\Entity\OpenStackStack $entity */
        $entity->setName($request->get('name', ''));
        $form_state->setValue('timeout_mins', $request->get('timeout_mins', 60));
        $form_state->setValue('rollback', json_decode($request->get('rollback', '"false"'), FALSE));

        $decoded_template = Yaml::decode($request->get('template', ''));
        if (!empty($decoded_template['heat_template_version']) && is_int($decoded_template['heat_template_version'])) {
          $decoded_template['heat_template_version'] = gmdate('Y-m-d', $decoded_template['heat_template_version']);
        }
        $template_parameters = [];
        foreach ($decoded_template['parameters'] ?: [] as $name => $parameter) {
          $template_parameters[$name] = $request->get('parameter_' . $name, '');
        }
        $form_state->setValue('template_parameters', $template_parameters);
        $method_name = 'createOpenStackStack';
        $extra_parameters = [
          'template'      => $decoded_template,
          'template_url'  => $request->get('template_url', ''),
          'environment'   => Yaml::decode($request->get('environment', '')),
        ];

        $form_state->setValue('uid', [['target_id' => $this->currentUser->id()]]);
        break;

      case 'preview_openstack_stack':
        /** @var \Drupal\openstack\Entity\OpenStackStack $entity */
        $entity->setName($request->get('name', ''));
        $form_state->setValue('timeout_mins', $request->get('timeout_mins', 60));
        $form_state->setValue('rollback', json_decode($request->get('rollback', '"false"'), FALSE));

        $decoded_template = Yaml::decode($request->get('template', ''));
        if (!empty($decoded_template['heat_template_version']) && is_int($decoded_template['heat_template_version'])) {
          $decoded_template['heat_template_version'] = gmdate('Y-m-d', $decoded_template['heat_template_version']);
        }
        $template_parameters = [];
        foreach ($decoded_template['parameters'] ?: [] as $name => $parameter) {
          $template_parameters[$name] = $request->get('parameter_' . $name, '');
        }
        $form_state->setValue('template_parameters', $template_parameters);
        $method_name = 'previewOpenStackStack';
        $extra_parameters = [
          'template'      => $decoded_template,
          'template_url'  => $request->get('template_url', ''),
          'environment'   => Yaml::decode($request->get('environment', '')),
        ];
        break;

      case 'check_openstack_stack':
        $method_name = 'checkOpenStackStack';
        break;

      case 'edit_openstack_stack':
        /** @var \Drupal\openstack\Entity\OpenStackStack $entity */
        $form_state->setValue('timeout_mins', $request->get('timeout_mins', 60));
        $form_state->setValue('rollback', json_decode($request->get('rollback', '"false"'), FALSE));

        $decoded_template = Yaml::decode($request->get('template', ''));
        if (!empty($decoded_template['heat_template_version']) && is_int($decoded_template['heat_template_version'])) {
          $decoded_template['heat_template_version'] = gmdate('Y-m-d', $decoded_template['heat_template_version']);
        }
        $template_parameters = [];
        foreach ($decoded_template['parameters'] ?: [] as $name => $parameter) {
          $template_parameters[$name] = $request->get('parameter_' . $name, '');
        }
        $form_state->setValue('template_parameters', $template_parameters);
        $method_name = 'updateOpenStackStack';
        $extra_parameters = [
          'template'      => $decoded_template,
          'template_url'  => $request->get('template_url', ''),
          'environment'   => Yaml::decode($request->get('environment', '')),
        ];
        $form_state->setValue('uid', [['target_id' => $this->currentUser->id()]]);
        break;

      case 'resume_openstack_stack':
        $method_name = 'resumeOpenStackStack';
        break;

      case 'suspend_openstack_stack':
        $method_name = 'suspendOpenStackStack';
        break;

      case 'create_openstack_project':
        /** @var \Drupal\openstack\Entity\OpenStackProject $entity */
        $entity->setName($request->get('name', ''));
        $entity->setDescription($request->get('description', ''));
        $entity->setEnabled($request->get('enabled', '') === 'true');
        $method_name = 'createOpenStackProject';
        break;

      case 'delete_openstack_project':
        $method_name = 'deleteOpenStackProject';
        break;

      case 'edit_openstack_project':
        /** @var \Drupal\openstack\Entity\OpenStackProject $entity */
        $entity->setName($request->get('name', ''));
        $entity->setDescription($request->get('description', ''));
        $entity->setEnabled($request->get('enabled', '') === 'true');
        $user_roles = json_decode($request->get('user_roles', '[]'), TRUE);
        foreach ($user_roles ?: [] as &$user) {
          $roles = explode(',', $user['roles']);
          $user['roles'] = array_combine($roles, $roles);
        }
        $entity->setUserRoles($user_roles);
        $method_name = 'editOpenStackProject';
        break;

      case 'create_openstack_role':
        /** @var \Drupal\openstack\Entity\OpenStackRole $entity */
        $entity->setName($request->get('name', ''));
        $entity->setDescription($request->get('description', ''));
        $method_name = 'createOpenStackRole';
        break;

      case 'delete_openstack_role':
        $method_name = 'deleteOpenStackRole';
        break;

      case 'edit_openstack_role':
        /** @var \Drupal\openstack\Entity\OpenStackRole $entity */
        $entity->setName($request->get('name', ''));
        $entity->setDescription($request->get('description', ''));
        $method_name = 'editOpenStackRole';
        break;

      case 'create_openstack_user':
        /** @var \Drupal\openstack\Entity\OpenStackUser $entity */
        $entity->setName($request->get('name', ''));
        $entity->setDescription($request->get('description', ''));
        $entity->setEnabled($request->get('enabled', '') === 'true');
        $entity->setEmail($request->get('email', ''));
        $form_state->setValue('password', $request->get('password', ''));
        $form_state->setValue('default_project_id', $request->get('default_project_id', ''));
        $form_state->setValue('role', $request->get('role', ''));
        $method_name = 'createOpenStackUser';
        break;

      case 'delete_openstack_user':
        $method_name = 'deleteOpenStackUser';
        break;

      case 'edit_openstack_user':
        /** @var \Drupal\openstack\Entity\OpenStackUser $entity */
        $entity->setName($request->get('name', ''));
        $entity->setDescription($request->get('description', ''));
        $entity->setEnabled($request->get('enabled', '') === 'true');
        $entity->setEmail($request->get('email', ''));
        $entity->setDefaultProjectId($request->get('default_project_id', ''));
        $method_name = 'editOpenStackUser';
        break;

      case 'change_password_openstack_user':
        $form_state->setValue('password', $request->get('password', ''));
        $method_name = 'changePasswordOpenStackUser';
        break;
    }

    // Execute the process.
    $result = NULL;
    if (method_exists($this->openStackOperationsService, $method_name)) {
      $result = $this->openStackOperationsService->$method_name(
        $entity,
        $form,
        $form_state,
        $extra_parameters
      );
    }
    // Store all messages before removing all in the next code.
    $messages = $this->messenger->all();
    // Remove all messages for cleanup before returning JSON object.
    $this->messenger->deleteAll();

    return !empty($result)
      ? new JsonResponse([
        'result' => 'OK',
        'id' => $entity->id(),
        'messages' => $messages,
      ], 200)
      : new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
        'messages' => $messages,
      ], 500);
  }

  /**
   * Process an entity and related cloud resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   * @param array $method_names
   *   The methods list.
   *
   * @return bool
   *   Succeeded or failed.
   */
  protected function process(CloudContentEntityBase $entity, array $method_names): bool {

    try {
      if ($method_names['processCloudResource']($entity)) {

        if (!empty($method_names['processEntity'])) {
          $method_names['processEntity']($entity);
        }
        $this->processOperationStatus($entity, 'processed');

        return TRUE;
      }

      $this->processOperationErrorStatus($entity, 'processed');

      return FALSE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    // Exception return type handling.
    // Return unable to process.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function operateEntities(Request $request, string $cloud_context, string $entity_type_id, string $command): JsonResponse {

    // Depending on the content of the process,
    // branching is performed with a switch statement.
    $id_list = json_decode($request->get('idList', '[]'), TRUE);
    $method_names = [
      'submitForm' => NULL,
      'process' => NULL,
      'processCloudResource' => NULL,
      'processEntity' => NULL,
      'getProcessedMessage' => NULL,
    ];
    switch ($command . '_' . $entity_type_id) {
      case 'delete_openstack_instance':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack instance cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackInstanceCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'start_openstack_instance':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->startProcess(
            $entity,
            // Delete the OpenStack instance cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->startOpenStackInstanceCloudResource($entity);
            },
            // Perform entity deletion.
            static function () {}
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($openstack_service, $string_translation) {
          $openstack_service->updateInstances();
          return $string_translation->formatPlural($count, 'Started @count item.', 'Started @count items.');
        };
        break;

      case 'stop_openstack_instance':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->stopProcess(
            $entity,
            // Delete the OpenStack instance cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->stopOpenStackInstanceCloudResource($entity);
            },
            // Perform entity deletion.
            static function () {}
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($openstack_service, $string_translation) {
          $openstack_service->updateInstances();
          return $string_translation->formatPlural($count, 'Stopped @count item.', 'Stopped @count items.');
        };
        break;

      case 'reboot_openstack_instance':
        $openstack_service = $this->openStackOperationsService;
        $reboot_type = $request->get('type', '') === 'true';
        $method_names['process'] = static function ($entity) use ($reboot_type, $openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->rebootProcess(
            $entity,
            // Delete the OpenStack instance cloud resource.
            static function ($entity) use ($reboot_type, $openstack_service) {
              return $openstack_service->rebootOpenStackInstanceCloudResource($entity, $reboot_type);
            },
            // Perform entity deletion.
            static function () {}
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($openstack_service, $string_translation) {
          $openstack_service->updateInstances();
          return $string_translation->formatPlural($count, 'Rebooted @count item.', 'Rebooted @count items.');
        };
        break;

      case 'delete_openstack_image':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackImageCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'delete_openstack_security_group':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          return $openstack_service->deleteProcess(
            $entity,
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackSecurityGroupCloudResource($entity);
            },
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'delete_openstack_floating_ip':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          return $openstack_service->deleteProcess(
            $entity,
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackFloatingIpCloudResource($entity);
            },
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($openstack_service, $string_translation) {
          $openstack_service->updateFloatingIps();
          $openstack_service->updateInstances();
          $openstack_service->updateNetworks();

          return $string_translation->formatPlural($count, 'Deleted @count Floating IP.', 'Deleted @count Floating IPs.');
        };
        break;

      case 'delete_openstack_key_pair':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack key pair cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackKeyPairCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'delete_openstack_volume':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackVolumeCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'detach_openstack_volume':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->detachProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->detachOpenStackVolumeCloudResource($entity);
            },
            // Perform entity deletion.
            static function () {}
          );
        };
        $method_names['getProcessedMessage'] = static function ($count) use ($openstack_service) {
          $openstack_service->updateVolumes();
          $openstack_service->updateInstances();

          return self::formatPlural($count, 'Detached @count volume.', 'Detached @count volumes.');
        };
        break;

      case 'delete_openstack_snapshot':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackSnapshotCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'delete_openstack_network':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackNetworkCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'delete_openstack_port':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackPortCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'delete_openstack_stack':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackStackCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'check_openstack_stack':
        $openstack_service = $this->openStackOperationsService;
        $method_names['processCloudResource'] = static function ($entity) use ($openstack_service) {
          return $openstack_service->checkOpenStackStackCloudResource($entity);
        };
        break;

      case 'resume_openstack_stack':
        $openstack_service = $this->openStackOperationsService;
        $method_names['processCloudResource'] = static function ($entity) use ($openstack_service) {
          return $openstack_service->resumeOpenStackStackCloudResource($entity);
        };
        break;

      case 'suspend_openstack_stack':
        $openstack_service = $this->openStackOperationsService;
        $method_names['processCloudResource'] = static function ($entity) use ($openstack_service) {
          return $openstack_service->suspendOpenStackStackCloudResource($entity);
        };
        break;

      case 'delete_openstack_server_group':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackServerGroupCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'delete_openstack_project':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackProjectCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'delete_openstack_role':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackRoleCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;

      case 'delete_openstack_user':
        $openstack_service = $this->openStackOperationsService;
        $method_names['process'] = static function ($entity) use ($openstack_service) {
          // Delete the process and associated resources.
          return $openstack_service->deleteProcess(
            $entity,
            // Delete the OpenStack image cloud resource.
            static function ($entity) use ($openstack_service) {
              return $openstack_service->deleteOpenStackUserCloudResource($entity);
            },
            // Perform entity deletion.
            static function ($entity) {
              $entity->delete();
            }
          );
        };
        $string_translation = $this->stringTranslation;
        $method_names['getProcessedMessage'] = static function ($count) use ($string_translation) {
          return $string_translation->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
        };
        break;
    }

    // Execute the process.
    $result = FALSE;
    if (!empty($method_names['submitForm'])) {
      $result = $method_names['submitForm']();
    }
    else {
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      $entities = $storage->loadMultiple($id_list);
      $total_count = 0;
      $first_entity = NULL;

      foreach ($entities ?: [] as $entity) {
        if (!empty($method_names['process'])
          ? $method_names['process']($entity)
          : $this->process($entity, $method_names)) {
          $total_count++;
        }
        if ($total_count === 1) {
          $first_entity = $entity;
        }
      }

      if ($total_count) {
        $this->messenger->addStatus(
          !empty($method_names['getProcessedMessage'])
            ? $method_names['getProcessedMessage']($total_count)
            : $this->formatPlural($total_count, 'Processed @count item.', 'Processed @count items.')
        );
      }

      // Fire the dispatch event. Send the first entity along.
      if (!empty($first_entity)) {
        $this->dispatchSubmitEvent($first_entity);
      }

      $this->clearCacheValues();
      $result = TRUE;
    }
    // Store all messages before removing all in the next code.
    $messages = $this->messenger->all();
    // Remove all messages for cleanup before returning JSON object.
    $this->messenger->deleteAll();

    return !empty($result)
      ? new JsonResponse([
        'result' => 'OK',
        'messages' => $messages,
      ], 200)
      : new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
        'messages' => $messages,
      ], 500);

  }

  /**
   * {@inheritdoc}
   */
  public function getVolumeOptions($cloud_context): JsonResponse {
    $options = [];
    $params = [
      'cloud_context' => $cloud_context,
    ];

    if (!$this->currentUser->hasPermission("view any openstack volume")) {
      $params['uid'] = $this->currentUser->id();
    }

    /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface[] $volumes */
    $volumes = $this->entityTypeManager
      ->getStorage("openstack_volume")
      ->loadByProperties($params);

    foreach ($volumes ?: [] as $volume) {
      $options[] = [
        'value' => $volume->getVolumeId(),
        'label' => $volume->getName() !== $volume->getVolumeId()
          ? "{$volume->getName()} ({$volume->getVolumeId()})"
          : $volume->getVolumeId(),
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getUnassociatedInstanceOptions(string $entity_id): JsonResponse {
    /** @var \Drupal\openstack\Entity\OpenStackFloatingIp $entity */
    $entity = $this->entityTypeManager
      ->getStorage('openstack_floating_ip')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The Floating IP has already been deleted.',
      ], 404);
    }

    // Get module name, network border group.
    $module_name = $this->getModuleName($entity);
    $network_border_group = $entity->getNetworkBorderGroup();

    $options = [];
    $account = $this->currentUser();

    $query = $this->entityTypeManager
      ->getStorage("{$module_name}_instance")
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $entity->getCloudContext());

    // Filter by network border group.
    if (!empty($network_border_group)) {
      $subnets = $this->entityTypeManager
        ->getStorage("{$module_name}_subnet")
        ->loadByProperties([
          'cloud_context' => $entity->getCloudContext(),
          'network_border_group' => $network_border_group,
        ]);
      $subnet_ids = array_map(static function ($subnet) {
        return $subnet->getSubnetId();
      }, $subnets);
      if (!empty($subnet_ids)) {
        $query->condition('subnet_id', $subnet_ids, 'IN');
      }
    }

    // Get cloud service provider name.
    // Use a static trait method through CloudService.
    $cloud_name = CloudService::convertUnderscoreToWhitespace($module_name);

    if (!$account->hasPermission("view any {$cloud_name} instance")) {
      $query->condition('uid', $account->id());
    }

    $results = $query->execute();

    foreach ($results ?: [] as $result) {

      $instance_list = $this->entityTypeManager
        ->getStorage("{$module_name}_instance")
        ->loadByProperties([
          'id' => $result,
          'cloud_context' => $entity->getCloudContext(),
        ]);

      /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
      $instance = count($instance_list) === 1
        ? array_shift($instance_list)
        : $instance_list;

      $private_ips = explode(', ', $instance->getPrivateIps());

      foreach ($private_ips ?: [] as $private_ip) {

        $elastic_ips = $this->entityTypeManager
          ->getStorage($entity->getEntityTypeId())
          ->loadbyProperties([
            'private_ip_address' => $private_ip,
            'cloud_context' => $entity->getCloudContext(),
          ]);

        if (count($elastic_ips) === 0) {
          $options[] = [
            'value' => $instance->id(),
            'label' => "{$instance->getName()} ({$instance->getInstanceId()}) - {$instance->getInstanceState()}",
          ];
        }
      }
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getSnapshotOptions($cloud_context): JsonResponse {
    $options = [];
    $params = [
      'cloud_context' => $cloud_context,
    ];

    if (!$this->currentUser->hasPermission('view any openstack snapshot')) {
      $params['uid'] = $this->currentUser->id();
    }

    /** @var \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface[] $snapshots */
    $snapshots = $this->entityTypeManager
      ->getStorage('openstack_snapshot')
      ->loadByProperties($params);

    foreach ($snapshots ?: [] as $snapshot) {
      $options[] = [
        'value' => $snapshot->getSnapshotId(),
        'label' => $snapshot->getName() !== $snapshot->getSnapshotId()
          ? "{$snapshot->getName()} ({$snapshot->getSnapshotId()})"
          : $snapshot->getSnapshotId(),
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getVolumeTypes($cloud_context): JsonResponse {
    $dummy_options = [
      '' => 'No volume type',
      'lvmdriver-1' => 'lvmdriver-1',
      '__DEFAULT__' => '__DEFAULT__',
    ];

    $options = [];
    foreach ($dummy_options ?: [] as $key => $option) {
      $options[] = [
        'value' => $key,
        'label' => $option,
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstancePrivateIps(string $entity_id, string $instance_id): JsonResponse {
    if (empty($instance_id)) {
      return new JsonResponse([]);
    }
    /** @var \Drupal\openstack\Entity\OpenStackFloatingIp $entity */
    $entity = $this->entityTypeManager
      ->getStorage('openstack_floating_ip')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The Floating IP has already been deleted.',
      ], 404);
    }

    $options = [];
    $ips = $this->openStackOperationsService->getPrivateIps($entity, $instance_id);
    foreach ($ips ?: [] as $value => $label) {
      $options[] = [
        'value' => $value,
        'label' => $label,
      ];
    }
    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  private function getAvailabilityZonesImpl($cloud_context): array {
    $params = [
      'cloud_context' => $cloud_context,
    ];
    $component_name = $this->requestStack->getCurrentRequest()->query->get('component_name') ?? '';
    $zone_resource = $this->requestStack->getCurrentRequest()->query->get('zone_resource') ?? '';

    if (!empty($component_name)) {
      $params['component_name'] = $component_name;
    }
    if (!empty($zone_resource)) {
      $params['zone_resource'] = $zone_resource;
    }

    $this->openStackService->setCloudContext($cloud_context);
    $this->openStackService->updateAvailabilityZonesWithoutBatch();

    /** @var \Drupal\aws_cloud\Entity\Ec2\AvailabilityZoneInterface[] $availability_zones */
    $availability_zones = $this->entityTypeManager
      ->getStorage('openstack_availability_zone')
      ->loadByProperties($params);

    $options = [];
    $seenValues = [];
    foreach ($availability_zones ?: [] as $availability_zone) {
      $value = $availability_zone->getZoneName();
      $label = $availability_zone->getZoneName();
      if (isset($seenValues[$value])) {
        continue;
      }

      $options[] = [
        'value' => $value,
        'label' => $label,
      ];
      $seenValues[$value] = TRUE;
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZones($cloud_context): JsonResponse {
    return new JsonResponse($this->getAvailabilityZonesImpl($cloud_context));
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceOptions(string $cloud_context, string $entity_id): JsonResponse {
    /** @var \Drupal\openstack\Entity\OpenStackVolume $entity */
    $entity = $this->entityTypeManager
      ->getStorage('openstack_volume')
      ->load($entity_id);

    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The volume has already been deleted.',
      ], 404);
    }

    $account = $this->currentUser();
    $properties = [
      'availability_zone' => $entity->getAvailabilityZone(),
      'cloud_context' => $cloud_context,
    ];

    if (!$account->hasPermission("view any openstack instance")) {
      $properties['uid'] = $account->id();
    }

    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface[] $results */
    $results = $this->entityTypeManager
      ->getStorage('openstack_instance')
      ->loadByProperties($properties);

    $options = [];
    foreach ($results ?: [] as $result) {
      $options[] = [
        'value' => $result->getInstanceId(),
        'label' => $this->t('@name - @instance_id', [
          '@name' => $result->getName(),
          '@instance_id' => $result->getInstanceId(),
        ]),
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkOptionsAsJson($cloud_context, array $properties = []): JsonResponse {
    return new JsonResponse($this->convertOptionsForJson(
      $this->getNetworkOptions($cloud_context, $properties)
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroupOptionsAsJson($cloud_context): JsonResponse {
    return new JsonResponse($this->convertOptionsForJson(
      $this->getSecurityGroupOptions($cloud_context)
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getSubnetOptions($cloud_context, $network_id = NULL): JsonResponse {
    $properties = [
      'cloud_context' => $cloud_context,
    ];

    if (!empty($network_id)) {
      $properties['network_id'] = $network_id;
    }

    $account = $this->currentUser;
    if (!$account->hasPermission('view any openstack subnet')) {
      $properties['uid'] = $account->id();
    }

    /** @var \Drupal\aws_cloud\Entity\Ec2\SubnetInterface[] $entities */
    $entities = $this->entityTypeManager
      ->getStorage('openstack_subnet')
      ->loadByProperties($properties);

    $options = [];
    foreach ($entities ?: [] as $entity) {
      $options[] = [
        'value' => $entity->getSubnetId(),
        'label' => $entity->getName(),
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubnetOptionsForRouter(string $entity_id): JsonResponse {

    /** @var \Drupal\openstack\Entity\OpenStackRouter $entity */
    $entity = $this->entityTypeManager
      ->getStorage('openstack_router')
      ->load($entity_id);

    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The volume has already been deleted.',
      ], 404);
    }

    /** @var \Drupal\openstack\Entity\OpenStackPort[] $ports */
    $ports = $this->entityTypeManager
      ->getStorage('openstack_port')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'device_id' => $entity->getRouterId(),
      ]);

    $subnet_ids_used = [];
    foreach ($ports ?: [] as $port) {
      if (empty($port->getFixedIps())) {
        continue;
      }

      $fixed_ips = $port->getFixedIps();
      foreach ($fixed_ips ?: [] as $fixed_ip) {
        $parts = explode(',', $fixed_ip['value']);
        if (count($parts) < 2 || in_array($parts[1], $subnet_ids_used)) {
          continue;
        }

        $subnet_ids_used[] = $parts[1];
      }
    }

    /** @var \Drupal\openstack\Entity\OpenStackSubnet[] $subnets */
    $subnets = $this->entityTypeManager
      ->getStorage('openstack_subnet')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
      ]);

    $options = [];
    foreach ($subnets ?: [] as $subnet) {
      if (in_array($subnet->getSubnetId(), $subnet_ids_used)) {
        continue;
      }

      /** @var \Drupal\openstack\Entity\OpenStackNetwork[] $networks */
      $networks = $this->entityTypeManager
        ->getStorage('openstack_network')
        ->loadByProperties([
          'cloud_context' => $entity->getCloudContext(),
          'network_id' => $subnet->getNetworkId(),
        ]);
      if (empty($networks)) {
        continue;
      }

      $network = reset($networks);
      $options[] = [
        'value' => $subnet->getSubnetId(),
        'label' => sprintf('%s: %s (%s)', $network->getName(), $subnet->getCidr(), $subnet->getName()),
      ];
    }

    return new JsonResponse($options);

  }

  /**
   * {@inheritdoc}
   */
  public function getFlavorOptions($cloud_context): JsonResponse {
    /** @var \Drupal\openstack\Entity\OpenStackFlavorInterface[] $flavors */
    $flavors = $this->entityTypeManager
      ->getStorage("openstack_flavor")
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($flavors as $flavor) {
      $options[$flavor->getFlavorId()] = $flavor->getName();
    }

    natcasesort($options);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getFlavorTableOptions($cloud_context): JsonResponse {
    /** @var \Drupal\openstack\Entity\OpenStackFlavorInterface[] $flavors */
    $flavors = $this->entityTypeManager
      ->getStorage("openstack_flavor")
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $output = array_values(array_map(static fn($flavor) => [
      'id' => $flavor->getFlavorId(),
      'name' => $flavor->getName(),
      'vcpus' => $flavor->getVcpus(),
      'ram' => $flavor->getRam(),
      'disk' => $flavor->getDisk(),
      'ephemeral' => $flavor->getEphemeral(),
      'swap' => $flavor->getSwap(),
      'rxtx_factor' => $flavor->getRxtxFactor(),
      'is_public' => $flavor->getIsPublic(),
    ], $flavors ?: []));

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getServerGroupOptions($cloud_context): JsonResponse {
    /** @var \Drupal\openstack\Entity\OpenStackServerGroupInterface[] $entities */
    $entities = $this->entityTypeManager
      ->getStorage("openstack_server_group")
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($entities as $entity) {
      $options[$entity->id()] = $entity->getName();
    }

    natcasesort($options);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getServerGroupPolicyOptions(): JsonResponse {
    $policies = OpenStackServiceInterface::SERVER_GROUP_POLICY_OPTION;

    $options = [];
    foreach ($policies ?: [] as $value => $label) {
      $options[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachedPortOptions(string $cloud_context, string $entity_id): JsonResponse {
    /** @var \Drupal\openstack\Entity\OpenStackInstance[] $entities */
    $entities = $this->entityTypeManager
      ->getStorage("openstack_instance")
      ->loadByProperties([
        'cloud_context' => $cloud_context,
        'id' => $entity_id,
      ]);

    $options = [];
    foreach ($entities as $entity) {
      $portIpAddresses = $entity->getPortIpAddresses();
      foreach ($portIpAddresses as $portIpAddress) {
        $options[$portIpAddress['item_key']] = $portIpAddress['item_value'];
      }
    }

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachPortOptions(string $cloud_context): JsonResponse {

    $ports = $this->openStackOperationsService->getAttachPortOptions($cloud_context);

    $output = [];
    foreach ($ports ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getAssociatedPortOptions(string $cloud_context): JsonResponse {
    $options = $this->openStackOperationsService->getAssociatedPortOptions($cloud_context);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalGatewayNetworkOptions(string $cloud_context): JsonResponse {
    $options = $this->openStackOperationsService->getExternalGatewayNetworkOptions($cloud_context);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectOptionsAsJson($cloud_context): JsonResponse {
    return new JsonResponse($this->convertOptionsForJson(
      $this->getProjectOptions($cloud_context)
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getRoleOptions($cloud_context): JsonResponse {
    $properties['cloud_context'] = $cloud_context;

    /** @var \Drupal\aws_cloud\Entity\Ec2\RoleInterface[] $entities */
    $entities = $this->entityTypeManager
      ->getStorage('openstack_role')
      ->loadByProperties($properties);

    $options = [];
    foreach ($entities ?: [] as $entity) {
      $options[] = [
        'value' => $entity->getRoleId(),
        'label' => $entity->getName(),
      ];
    }

    return new JsonResponse($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachNetworkOptions(string $cloud_context): JsonResponse {
    $options = $this->openStackOperationsService->getAttachNetworkOptions($cloud_context);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateNetworkOptionsAsJson($cloud_context): JsonResponse {
    /** @var \Drupal\openstack\Entity\OpenStackNetworkInterface[] $networks */
    $networks = $this->entityTypeManager
      ->getStorage('openstack_network')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($networks as $network) {
      $options[$network->id()] = $network->getName();
    }

    natcasesort($options);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }

    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateKeyPairOptionsAsJson($cloud_context): JsonResponse {
    /** @var \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface[] $keyPairs */
    $keyPairs = $this->entityTypeManager
      ->getStorage('openstack_key_pair')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($keyPairs as $keyPair) {
      $options[$keyPair->id()] = $keyPair->getName();
    }

    natcasesort($options);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }
    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateSecurityGroupOptionsAsJson($cloud_context): JsonResponse {
    /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface[] $entities */
    $entities = $this->entityTypeManager
      ->getStorage('openstack_security_group')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($entities ?: [] as $entity) {
      $options[$entity->id()] = $entity->getName();
    }

    natcasesort($options);

    $output = array_map(static function ($value, $label) {
      return [
        'value' => $value,
        'label' => $label,
      ];
    }, array_keys($options ?: []), array_values($options ?: []));
    return new JsonResponse($output, 200);
  }

  /**
   * Convert options to adapt format for JSON output.
   *
   * @param array $options
   *   The options.
   *
   * @return array
   *   The options converted.
   */
  private function convertOptionsForJson(array $options): array {
    return array_map(static function ($key, $value) {
      return [
        'value' => $key,
        'label' => $value,
      ];
    }, array_keys($options), array_values($options));
  }

  /**
   * Confirm the entity type has the link type.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $link_type
   *   The link type.
   *
   * @return bool
   *   The result.
   */
  private function hasEntityLink($entity_type, $link_type): bool {
    $type = $this->entityTypeManager
      ->getStorage($entity_type)
      ->getEntityType();
    $link = $type->getLinkTemplate($link_type);
    return !empty($link);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultVolumeType(string $cloud_context): JsonResponse {
    $open_stack_service = $this->openStackServiceFactory->get($cloud_context);
    return new JsonResponse($open_stack_service->getDefaultVolumeType(), Response::HTTP_OK);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultAvailabilityZone(string $cloud_context): JsonResponse {
    $list = $this->getAvailabilityZonesImpl($cloud_context);
    return new JsonResponse(array_shift($list)['value'], Response::HTTP_OK);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsole(string $entity_id): JsonResponse {

    // Create an instance of the entity.
    // The process splits between adding a new entity
    // and when reading an existing entity.
    $entity = $this->entityTypeManager
      ->getStorage('openstack_instance')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The entity has already been deleted.',
      ], 404);
    }
    $form = [];
    $form_state = new IntermediateFormState();
    $console_data = $this->openStackOperationsService->getConsole($entity, $form, $form_state);

    $this->messenger->deleteAll();
    return !empty($console_data)
      ? new JsonResponse([
        'result' => 'OK',
        'console' => $console_data,
      ], 200)
      : new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
      ], 500);

  }

  /**
   * {@inheritdoc}
   */
  public function getActionLog(string $entity_id): JsonResponse {

    // Create an instance of the entity.
    // The process splits between adding a new entity
    // and when reading an existing entity.
    $entity = $this->entityTypeManager
      ->getStorage('openstack_instance')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The entity has already been deleted.',
      ], 404);
    }
    $form = [];
    $form_state = new IntermediateFormState();
    $action_log = $this->openStackOperationsService->getActionLog($entity, $form, $form_state);

    $this->messenger->deleteAll();
    return !empty($action_log)
      ? new JsonResponse([
        'result' => 'OK',
        'action_log' => $action_log,
      ], 200)
      : new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
      ], 500);

  }

  /**
   * {@inheritdoc}
   */
  public function getWellknownProtocols(): JsonResponse {
    $well_known_ports = OpenStackServiceInterface::WELL_KNOWN_PORTS;
    return new JsonResponse($well_known_ports);
  }

  /**
   * {@inheritdoc}
   */
  public function hasAdminRole(): JsonResponse {
    return new JsonResponse([
      'has_admin_role' => TRUE,
    ]);
  }

  /**
   * Set OpenStack image properties based on data from the request.
   *
   * @param \Drupal\openstack\Entity\OpenStackImageInterface $entity
   *   The OpenStack image entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\openstack\Entity\OpenStackImageInterface
   *   The modified OpenStack image entity.
   */
  private function setOpenStackImageSharedProjectsFromRequest(OpenStackImageInterface $entity, Request $request): OpenStackImageInterface {
    $image_visibility_key = $request->get('image_visibility', '');
    $imageVisibility = array_key_exists($image_visibility_key, OpenStackImageInterface::IMAGE_VISIBILITY_INDEX)
      ? OpenStackImageInterface::IMAGE_VISIBILITY_INDEX[$image_visibility_key]
      : '';
    $entity->setImageVisibility($imageVisibility);
    // Initialize shared projects if image visibility is other than 'shared'.
    $shared_projects = $imageVisibility === OpenStackImageInterface::VISIBILITY_SHARED
      ? json_decode($request->get('shared_projects', '[]'))
      : [];
    $converted_shared_projects = array_map(function ($item) {
      return is_object($item) ? (array) $item : $item;
    }, $shared_projects);
    $entity->setSharedProjects($converted_shared_projects);

    return $entity;
  }

}
