<?php

namespace Drupal\openstack\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Api Controller interface.
 */
interface ApiControllerInterface {

  /**
   * Update all instances in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateInstanceList($cloud_context): RedirectResponse;

  /**
   * Update all images in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateImageList($cloud_context): RedirectResponse;

  /**
   * Update all key pairs in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateKeyPairList($cloud_context): RedirectResponse;

  /**
   * Update all security groups in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateSecurityGroupList($cloud_context): RedirectResponse;

  /**
   * Update all volumes in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateVolumeList($cloud_context): RedirectResponse;

  /**
   * Update all snapshots in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateSnapshotList($cloud_context): RedirectResponse;

  /**
   * Update all networks in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateNetworkList($cloud_context): RedirectResponse;

  /**
   * Update all subnets in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateSubnetList($cloud_context): RedirectResponse;

  /**
   * Update all ports in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updatePortList($cloud_context): RedirectResponse;

  /**
   * Update all routers in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateRouterList($cloud_context): RedirectResponse;

  /**
   * Update all quotas in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateQuotaList($cloud_context): RedirectResponse;

  /**
   * Update all stacks in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateStackList($cloud_context): RedirectResponse;

  /**
   * Update all projects in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateProjectList($cloud_context): RedirectResponse;

  /**
   * Update all roles in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateRoleList($cloud_context): RedirectResponse;

  /**
   * Update all users in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateUserList($cloud_context): RedirectResponse;

  /**
   * Update all Floating IPs in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateFloatingIpList($cloud_context): RedirectResponse;

  /**
   * Update all server groups in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateServerGroupList($cloud_context): RedirectResponse;

  /**
   * Update all flavors in particular cloud region.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateFlavorList($cloud_context): RedirectResponse;

  /**
   * Update all entities in a given region.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateAll(): RedirectResponse;

  /**
   * Get the count of OpenStack entities.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getEntityCount(string $cloud_context, string $entity_type_id): JsonResponse;

  /**
   * Get console output from OpenStack instance.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_id
   *   The entity ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getConsoleOutput(string $cloud_context, string $entity_id): JsonResponse;

  /**
   * Operate OpenStack entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $entity_id
   *   The entity ID.
   * @param string $command
   *   Operation command.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function operateEntity(Request $request, string $cloud_context, string $entity_type_id, string $entity_id, string $command): JsonResponse;

  /**
   * Operate OpenStack entities.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $command
   *   Operation command.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function operateEntities(Request $request, string $cloud_context, string $entity_type_id, string $command): JsonResponse;

  /**
   * Helper function to get volume options.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getVolumeOptions($cloud_context): JsonResponse;

  /**
   * Helper function to get unassociated instance options.
   *
   * @param string $entity_id
   *   The entity id of the OpenStack floating IP.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUnassociatedInstanceOptions(string $entity_id): JsonResponse;

  /**
   * Helper function that loads all the private IPs for an instance.
   *
   * @param string $entity_id
   *   The entity id of the OpenStack floating IP.
   * @param string $instance_id
   *   The instance ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getInstancePrivateIps(string $entity_id, string $instance_id): JsonResponse;

  /**
   * Helper function to get snapshot options.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSnapshotOptions($cloud_context): JsonResponse;

  /**
   * Helper function to get volume types.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getVolumeTypes($cloud_context): JsonResponse;

  /**
   * Helper function to get Availability Zones.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAvailabilityZones($cloud_context): JsonResponse;

  /**
   * Helper function that loads all instances.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   * @param string $entity_id
   *   The entity id of the OpenStack volume.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getInstanceOptions(string $cloud_context, string $entity_id): JsonResponse;

  /**
   * Get network options.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $properties
   *   The properties of EntityTypeManager.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getNetworkOptionsAsJson($cloud_context, array $properties = []): JsonResponse;

  /**
   * Get security group options.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getSecurityGroupOptionsAsJson($cloud_context): JsonResponse;

  /**
   * Get subnet options.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $network_id
   *   The network ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getSubnetOptions($cloud_context, $network_id = NULL): JsonResponse;

  /**
   * Get flavor options.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getFlavorOptions($cloud_context): JsonResponse;

  /**
   * Get flavor options.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getFlavorTableOptions($cloud_context): JsonResponse;

  /**
   * Get server group options.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getServerGroupOptions($cloud_context): JsonResponse;

  /**
   * Get server group policy options.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getServerGroupPolicyOptions(): JsonResponse;

  /**
   * Get ports attached to OpenStack instance.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   * @param string $entity_id
   *   The entity id of the OpenStack instance.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getAttachedPortOptions(string $cloud_context, string $entity_id): JsonResponse;

  /**
   * Get attach port options.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getAttachPortOptions(string $cloud_context): JsonResponse;

  /**
   * Get the ports that can be associated with the OpenStack floating IP.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getAssociatedPortOptions(string $cloud_context): JsonResponse;

  /**
   * Get external gateway network options.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getExternalGatewayNetworkOptions(string $cloud_context): JsonResponse;

  /**
   * Get project options as Json.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getProjectOptionsAsJson($cloud_context): JsonResponse;

  /**
   * Get role options.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getRoleOptions($cloud_context): JsonResponse;

  /**
   * Get attach network options.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getAttachNetworkOptions(string $cloud_context): JsonResponse;

  /**
   * Get select options of network.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getTemplateNetworkOptionsAsJson($cloud_context): JsonResponse;

  /**
   * Get select options of key pair.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getTemplateKeyPairOptionsAsJson($cloud_context): JsonResponse;

  /**
   * Get select options of security group.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getTemplateSecurityGroupOptionsAsJson($cloud_context): JsonResponse;

  /**
   * Get select options of well-known protocols.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getWellknownProtocols(): JsonResponse;

  /**
   * Get the status that the account has admin role.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function hasAdminRole(): JsonResponse;

}
