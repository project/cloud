<?php

namespace Drupal\openstack\Controller;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Controller\Ec2\InstanceAccessControlHandler;

/**
 * OpenStack instance access handler.
 *
 * This class extends checkAccess() to provide custom OpenStack access control
 * for certain entities.
 */
class OpenStackInstanceAccessControlHandler extends InstanceAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {

    // Get cloud service provider name.
    $cloud_name = $this->getModuleNameWithWhitespace($entity);
    switch ($operation) {
      case 'detach_network':
        // Only allow access to detach network route if the instance has
        // port ids.
        if (!empty($entity->getPortIds())) {
          return $this->allowedIfCanAccessCloudConfigWithOwner(
            $entity,
            $account,
            "edit own {$cloud_name} instance",
            "edit any {$cloud_name} instance"
          );
        }
        break;
    }

    // If no custom checks are performed, call parent checkAccess().
    return parent::checkAccess($entity, $operation, $account);
  }

}
