<?php

namespace Drupal\openstack\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Traits\AccessCheckTrait;
use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * Access controller for the OpenStack key pair entity.
 *
 * @see Drupal\openstack\Entity\KeyPair.
 */
class OpenStackKeyPairAccessControlHandler extends EntityAccessControlHandler {

  use AccessCheckTrait;
  use CloudContentEntityTrait;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {

    // Get cloud service provider name.
    $cloud_name = $this->getModuleNameWithWhitespace($entity);

    switch ($operation) {
      case 'view':
      case 'update':
      case 'edit':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          "view own {$cloud_name} key pair",
          "view any {$cloud_name} key pair"
        );

      case 'delete':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          "delete own {$cloud_name} key pair",
          "delete any {$cloud_name} key pair"
        );
    }
    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
