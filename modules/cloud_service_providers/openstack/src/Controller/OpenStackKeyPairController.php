<?php

namespace Drupal\openstack\Controller;

use Drupal\aws_cloud\Controller\Ec2\AwsCloudKeyPairController;

/**
 * Controller responsible for OpenStack key pair.
 */
class OpenStackKeyPairController extends AwsCloudKeyPairController {

  /**
   * Download Key.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param object $key_pair
   *   OpenStack key pair.
   * @param string $entity_type
   *   The entity type, such as cloud_launch_template.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\RedirectResponse
   *   A binary file response object or redirect if key file does not exist.
   */
  public function downloadKey($cloud_context, $key_pair, $entity_type = 'aws_cloud'): object {
    return parent::downloadKey($cloud_context, $key_pair, 'openstack');
  }

}
