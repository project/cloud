<?php

namespace Drupal\openstack\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Traits\AccessCheckTrait;
use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * Access controller for the Server group entity.
 *
 * @see \Drupal\openstack\Entity\Entity\OpenStackServerGroup.
 */
class OpenStackServerGroupAccessControlHandler extends EntityAccessControlHandler {

  use AccessCheckTrait;
  use CloudContentEntityTrait;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {

    // Get cloud service provider name.
    $cloud_name = $this->getModuleNameWithWhitespace($entity);

    switch ($operation) {
      case 'view':
        return $this->allowedIfCanAccessCloudConfig(
          $entity,
          $account,
          "view {$cloud_name} server group"
        );

      case 'update':
        return $this->allowedIfCanAccessCloudConfig(
          $entity,
          $account,
          "edit {$cloud_name} server group"
        );

      case 'delete':
        return $this->allowedIfCanAccessCloudConfig(
          $entity,
          $account,
          "delete {$cloud_name} server group"
        );
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
