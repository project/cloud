<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\aws_cloud\Entity\Ec2\AvailabilityZone;
use Drupal\aws_cloud\Entity\Ec2\AvailabilityZoneInterface;

/**
 * Defines the AvailabilityZone entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_availability_zone",
 *   id_plural = "openstack_availability_zones",
 *   label = @Translation("Availability Zone"),
 *   label_collection = @Translation("Availability Zones"),
 *   label_singular = @Translation("Availability Zone"),
 *   label_plural = @Translation("Availability Zones"),
 *   handlers = {
 *     "view_builder" = "Drupal\aws_cloud\Entity\Ec2\AvailabilityZoneViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackAvailabilityZoneViewsData",
 *     "form" = {
 *     },
 *     "access"       = "Drupal\aws_cloud\Controller\Ec2\AvailabilityZoneAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "openstack_availability_zone",
 *   admin_permission = "administer openstack Availability Zone",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "zone_name",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *   },
 *   field_ui_base_route = "openstack_availability_zone.settings"
 * )
 */
class OpenStackAvailabilityZone extends AvailabilityZone {

  /**
   * {@inheritdoc}
   */
  public function getComponentName(): ?string {
    return $this->get('component_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setComponentName($component_name = ''): AvailabilityZoneInterface {
    return $this->set('component_name', $component_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getZoneResource(): ?string {
    return $this->get('zone_resource')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setZoneResource($zone_resource = ''): AvailabilityZoneInterface {
    return $this->set('zone_resource', $zone_resource);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['component_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Component name'))
      ->setDescription(t('The component name of the OpenStack Availability Zone.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['zone_resource'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Zone Resource'))
      ->setDescription(t('The resource of the OpenStack Availability Zone.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    return $fields;
  }

}
