<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the OpenStackFlavor entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_flavor",
 *   id_plural = "openstack_flavors",
 *   label = @Translation("Flavor"),
 *   label_collection = @Translation("Flavors"),
 *   label_singular = @Translation("Flavor"),
 *   label_plural = @Translation("Flavors"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackFlavorViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackFlavorViewsData",
 *     "form" = {
 *     },
 *     "access"       = "Drupal\openstack\Controller\OpenStackFlavorAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "openstack_flavor",
 *   admin_permission = "administer openstack flavor",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/flavor/{openstack_flavor}",
 *   },
 *   field_ui_base_route = "openstack_flavor.settings"
 * )
 */
class OpenStackFlavor extends CloudContentEntityBase implements OpenStackFlavorInterface {

  /**
   * {@inheritdoc}
   */
  public function getFlavorId(): ?string {
    return $this->get('flavor_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFlavorId($flavor_id = ''): OpenStackFlavorInterface {
    return $this->set('flavor_id', $flavor_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackFlavorInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getVcpus(): ?int {
    return $this->get('vcpus')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVcpus($vcpus): OpenStackFlavorInterface {
    return $this->set('vcpus', $vcpus);
  }

  /**
   * {@inheritdoc}
   */
  public function getRam(): ?int {
    return $this->get('ram')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRam($ram): OpenStackFlavorInterface {
    return $this->set('ram', $ram);
  }

  /**
   * {@inheritdoc}
   */
  public function getDisk(): ?int {
    return $this->get('disk')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDisk($disk): OpenStackFlavorInterface {
    return $this->set('disk', $disk);
  }

  /**
   * {@inheritdoc}
   */
  public function getEphemeral(): ?int {
    return $this->get('ephemeral')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEphemeral($ephemeral): OpenStackFlavorInterface {
    return $this->set('ephemeral', $ephemeral);
  }

  /**
   * {@inheritdoc}
   */
  public function getSwap(): ?int {
    return $this->get('swap')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSwap($swap): OpenStackFlavorInterface {
    return $this->set('swap', $swap);
  }

  /**
   * {@inheritdoc}
   */
  public function getRxtxFactor(): ?float {
    return $this->get('rxtx_factor')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRxtxFactor($rxtx_factor): OpenStackFlavorInterface {
    return $this->set('rxtx_factor', $rxtx_factor);
  }

  /**
   * {@inheritdoc}
   */
  public function getIsPublic(): ?bool {
    return $this->get('is_public')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setIsPublic($is_public): OpenStackFlavorInterface {
    return $this->set('is_public', $is_public);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackFlavorInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the flavor entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the flavor entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['flavor_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Flavor ID'))
      ->setDescription(t('The ID of the Flavor.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of Flavor.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['vcpus'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('vCPUs'))
      ->setDescription(t('The number of virtual CPUs that will be allocated to the server.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['ram'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('RAM'))
      ->setDescription(t('The amount of RAM a flavor has, in MiB.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => -5,
        'settings' => [
          'thousand_separator' => ',',
        ],
      ])
      ->setSettings([
        'suffix' => t('MB'),
      ])
      ->setReadOnly(TRUE);

    $fields['disk'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Root Disk'))
      ->setDescription(t('The size of the root disk that will be created in GiB.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => -5,
        'settings' => [
          'thousand_separator' => ',',
        ],
      ])
      ->setSettings([
        'suffix' => t('GB'),
      ])
      ->setReadOnly(TRUE);

    $fields['ephemeral'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Ephemeral Disk'))
      ->setDescription(t('The size of the ephemeral disk that will be created, in GiB.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => -5,
        'settings' => [
          'thousand_separator' => ',',
        ],
      ])
      ->setSettings([
        'suffix' => t('GB'),
      ])
      ->setReadOnly(TRUE);

    $fields['swap'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Swap Disk'))
      ->setDescription(t('The size of a dedicated swap disk that will be allocated, in MiB.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => -5,
        'settings' => [
          'thousand_separator' => ',',
        ],
      ])
      ->setSettings([
        'suffix' => t('MB'),
      ])
      ->setReadOnly(TRUE);

    $fields['rxtx_factor'] = BaseFieldDefinition::create('float')
      ->setLabel(t('RX/TX Factor'))
      ->setDescription(t('The receive / transmit factor (as a float) that will be set on ports if the network backend supports the QOS extension. Otherwise it will be ignored. It defaults to 1.0.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_decimal',
        'weight' => -5,
        'settings' => [
          'scale' => 1,
        ],
      ])
      ->setReadOnly(TRUE);

    $fields['is_public'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Public'))
      ->setDescription(t('Whether the flavor is public (available to all projects) or scoped to a set of projects. Default is True if not specified.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('Yes'),
        'off_label' => t('No'),
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the AMI was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the flavor entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
