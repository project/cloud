<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackFlavor entity.
 *
 * @ingroup openstack
 */
interface OpenStackFlavorInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getFlavorId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setFlavorId($flavor_id = ''): OpenStackFlavorInterface;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackFlavorInterface;

  /**
   * {@inheritdoc}
   */
  public function getVcpus(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setVcpus($vcpus): OpenStackFlavorInterface;

  /**
   * {@inheritdoc}
   */
  public function getRam(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setRam($ram): OpenStackFlavorInterface;

  /**
   * {@inheritdoc}
   */
  public function getDisk(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setDisk($disk): OpenStackFlavorInterface;

  /**
   * {@inheritdoc}
   */
  public function getEphemeral(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setEphemeral($ephemeral): OpenStackFlavorInterface;

  /**
   * {@inheritdoc}
   */
  public function getSwap(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setSwap($swap): OpenStackFlavorInterface;

  /**
   * {@inheritdoc}
   */
  public function getRxtxFactor(): ?float;

  /**
   * {@inheritdoc}
   */
  public function setRxtxFactor($rxtx_factor): OpenStackFlavorInterface;

  /**
   * {@inheritdoc}
   */
  public function getIsPublic(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function setIsPublic($is_public): OpenStackFlavorInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackFlavorInterface;

}
