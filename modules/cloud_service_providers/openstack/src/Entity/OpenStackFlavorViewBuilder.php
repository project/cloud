<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the flavor view builders.
 */
class OpenStackFlavorViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'flavor',
        'title' => $this->t('Flavor'),
        'open' => TRUE,
        'fields' => [
          'flavor_id',
          'name',
          'vcpus',
          'ram',
          'disk',
          'ephemeral',
          'swap',
          'rxtx_factor',
          'flavor_id',
          'is_public',
          'created',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
