<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\aws_cloud\Entity\Ec2\ElasticIp;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;

/**
 * Defines the Floating IP entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_floating_ip",
 *   id_plural = "openstack_floating_ips",
 *   label = @Translation("Floating IP"),
 *   label_collection = @Translation("Floating IPs"),
 *   label_singular = @Translation("Floating IP"),
 *   label_plural = @Translation("Floating IPs"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackFloatingIpViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackFloatingIpViewsData",
 *     "form" = {
 *       "add" = "Drupal\openstack\Form\OpenStackFloatingIpCreateForm",
 *       "default" = "Drupal\openstack\Form\OpenStackFloatingIpEditForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackFloatingIpEditForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackFloatingIpDeleteForm",
 *       "associate" = "Drupal\openstack\Form\OpenStackFloatingIpAssociateForm",
 *       "disassociate" = "Drupal\openstack\Form\OpenStackFloatingIpDisassociateForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackFloatingIpDeleteMultipleForm",
 *     },
 *     "access" = "Drupal\aws_cloud\Controller\Ec2\ElasticIpAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "openstack_floating_ip",
 *   admin_permission = "administer openstack floating ip",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id"  ,
 *     "label" = "name",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *     "canonical"            = "/clouds/openstack/{cloud_context}/floating_ip/{openstack_floating_ip}",
 *     "edit-form"            = "/clouds/openstack/{cloud_context}/floating_ip/{openstack_floating_ip}/edit",
 *     "delete-form"          = "/clouds/openstack/{cloud_context}/floating_ip/{openstack_floating_ip}/delete",
 *     "collection"           = "/clouds/openstack/{cloud_context}/floating_ip",
 *     "associate-form"       = "/clouds/openstack/{cloud_context}/floating_ip/{openstack_floating_ip}/associate",
 *     "disassociate-form"    = "/clouds/openstack/{cloud_context}/floating_ip/{openstack_floating_ip}/disassociate",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/floating_ip/delete_multiple",
 *   },
 *   field_ui_base_route = "openstack_floating_ip.settings"
 * )
 */
class OpenStackFloatingIp extends ElasticIp implements OpenStackFloatingIpInterface {

  /**
   * {@inheritdoc}
   */
  public function getFloatingIpId(): ?string {
    return $this->get('floating_ip_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFloatingIpId($floating_ip_id): OpenStackFloatingIpInterface {
    return $this->set('floating_ip_id', $floating_ip_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkId(): ?string {
    return $this->get('network_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNetworkId($network_id): OpenStackFloatingIpInterface {
    return $this->set('network_id', $network_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkOwner(): ?string {
    return $this->get('network_owner')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNetworkOwner($network_owner): OpenStackFloatingIpInterface {
    return $this->set('network_owner', $network_owner);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['instance_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Instance ID'))
      ->setDescription(t('The instance the @label address is associated with, if applicable.', [
        '@label' => $entity_type->getLabel(),
      ]))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_instance',
          'field_name' => 'instance_id',
          'html_generator_class' => EntityLinkWithNameHtmlGenerator::class,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['floating_ip_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('FloatingIp ID'))
      ->setDescription(t('The ID of the FloatingIp.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['network_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Network ID'))
      ->setDescription(t('The ID of the Network.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_network',
          'field_name' => 'network_id',
          'html_generator_class' => EntityLinkWithNameHtmlGenerator::class,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['network_owner'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Network owner'))
      ->setDescription(t('The account number of the network owner.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    return $fields;
  }

}
