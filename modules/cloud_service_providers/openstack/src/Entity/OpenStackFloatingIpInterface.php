<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackFloatingIp entity.
 *
 * @ingroup openstack
 */
interface OpenStackFloatingIpInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getFloatingIpId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setFloatingIpId($floating_ip_id): OpenStackFloatingIpInterface;

  /**
   * {@inheritdoc}
   */
  public function getNetworkId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNetworkId($network_id): OpenStackFloatingIpInterface;

  /**
   * {@inheritdoc}
   */
  public function getNetworkOwner(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNetworkOwner($network_owner): OpenStackFloatingIpInterface;

}
