<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the flavor view builders.
 */
class OpenStackFloatingIpViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'ip_address',
        'title' => $this->t('IP address'),
        'open' => TRUE,
        'fields' => [
          'public_ip',
          'private_ip_address',
          'created',
        ],
      ],
      [
        'name' => 'assign',
        'title' => $this->t('Assign'),
        'open' => TRUE,
        'fields' => [
          'instance_id',
          'network_id',
          'allocation_id',
          'association_id',
          'domain',
          'network_owner',
          'network_border_group',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
