<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\aws_cloud\Entity\Ec2\Image;

/**
 * Defines the image entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_image",
 *   id_plural = "openstack_images",
 *   label = @Translation("Image"),
 *   label_collection = @Translation("Images"),
 *   label_singular = @Translation("Image"),
 *   label_plural = @Translation("Images"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackImageViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackImageViewsData",
 *     "form" = {
 *       "add"     = "Drupal\openstack\Form\OpenStackImageCreateForm",
 *       "default" = "Drupal\openstack\Form\OpenStackImageEditForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackImageEditForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackImageDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackImageDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\aws_cloud\Controller\Ec2\ImageAccessControlHandler",
 *   },
 *   base_table = "openstack_image",
 *   admin_permission = "administer openstack image",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/image/{openstack_image}",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/image/{openstack_image}/edit",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/image/{openstack_image}/delete",
 *     "collection" = "/clouds/openstack/{cloud_context}/image",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/image/delete_multiple",
 *   },
 *   field_ui_base_route = "openstack_image.settings"
 * )
 */
class OpenStackImage extends Image implements OpenStackImageInterface {

  /**
   * {@inheritdoc}
   */
  public function getImageVisibility(): ?string {
    return $this->get('image_visibility')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setImageVisibility($image_visibility): OpenStackImageInterface {
    return $this->set('image_visibility', $image_visibility);
  }

  /**
   * {@inheritdoc}
   */
  public function getSharedProjects(): array {
    return $this->get('shared_projects')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setSharedProjects(array $shared_projects): OpenStackImageInterface {
    return $this->set('shared_projects', $shared_projects);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['image_visibility'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Visibility'))
      ->setDescription(t('The Image visibility.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['shared_projects'] = BaseFieldDefinition::create('shared_projects')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Shared projects'))
      ->setDescription(t('The project to share the Image.'))
      ->setDisplayOptions('view', [
        'type' => 'shared_projects_formatter',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'shared_projects_item',
      ]);

    return $fields;
  }

}
