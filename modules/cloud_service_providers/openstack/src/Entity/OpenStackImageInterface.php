<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\ImageInterface;

/**
 * Provides an interface defining an OpenStackImage entity.
 *
 * @ingroup openstack
 */
interface OpenStackImageInterface extends ImageInterface {

  public const VISIBILITY_PRIVATE = 'private';

  public const VISIBILITY_PUBLIC = 'public';

  public const VISIBILITY_SHARED = 'shared';

  public const IMAGE_VISIBILITY = [
    self::VISIBILITY_PRIVATE => 'Private',
    self::VISIBILITY_PUBLIC => 'Public',
    self::VISIBILITY_SHARED => 'Shared',
  ];

  public const IMAGE_VISIBILITY_INDEX = [
    '0' => self::VISIBILITY_PRIVATE,
    '1' => self::VISIBILITY_PUBLIC,
    '2' => self::VISIBILITY_SHARED,
  ];

  public const SHARED_PROJECT_TAG_NAME_PREFIX = 'cloud-orchestrator-shared';

  /**
   * Defines the format of image tag for shared projects.
   */
  public const SHARED_IMAGE_TAG_VALUE_FORMAT = '{cloud_context}.{project_id}.{image_id}';

  public const IMAGE_SHARED_PROJECTS_STATUS = [
    'available' => 'accepted',
    'pending' => 'being set up',
  ];

  /**
   * {@inheritdoc}
   */
  public function getImageVisibility(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setImageVisibility($image_visibility): OpenStackImageInterface;

  /**
   * {@inheritdoc}
   */
  public function getSharedProjects(): array;

  /**
   * {@inheritdoc}
   */
  public function setSharedProjects(array $shared_projects): OpenStackImageInterface;

}
