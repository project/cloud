<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the image view builders.
 */
class OpenStackImageViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'image',
        'title' => $this->t('Image'),
        'open' => TRUE,
        'fields' => [
          'name',
          'description',
          'ami_name',
          'image_id',
          'account_id',
          'source',
          'status',
          'image_type',
          'platform',
          'architecture',
          'virtualization_type',
          'product_code',
          'root_device_name',
          'root_device_type',
          'kernel_id',
          'ramdisk_id',
          'block_device_mappings',
          'state_reason',
          'created',
        ],
      ],
      [
        'name' => 'security',
        'title' => $this->t('Security'),
        'open' => TRUE,
        'fields' => [
          'image_visibility',
          'shared_projects',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
