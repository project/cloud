<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\aws_cloud\Entity\Ec2\Instance;
use Drupal\aws_cloud\Entity\Ec2\InstanceInterface;
use Drupal\aws_cloud\Entity\Ec2\PublicIpEntityLinkHtmlGenerator;
use Drupal\aws_cloud\Plugin\Field\Util\AwsCloudValueConverter;
use Drupal\cloud\Service\Util\EntityLinkWithShortNameHtmlGenerator;
use Drupal\openstack\Plugin\Field\Util\OpenStackReservedKeyChecker;

/**
 * Defines the OpenStack instance entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_instance",
 *   label = @Translation("Instance"),
 *   id_plural = "openstack_instances",
 *   label_collection = @Translation("Instances"),
 *   label_singular = @Translation("Instance"),
 *   label_plural = @Translation("Instances"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackInstanceViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data" = "Drupal\openstack\Entity\OpenStackInstanceViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackInstanceEditForm",
 *       "add" = "Drupal\openstack\Form\OpenStackInstanceLaunchForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackInstanceEditForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackInstanceDeleteForm",
 *       "start" = "Drupal\openstack\Form\OpenStackInstanceStartForm",
 *       "stop" = "Drupal\openstack\Form\OpenStackInstanceStopForm",
 *       "reboot" = "Drupal\openstack\Form\OpenStackInstanceRebootForm",
 *       "create_image" = "Drupal\openstack\Form\OpenStackInstanceCreateImageForm",
 *       "console_output" = "Drupal\openstack\Form\OpenStackInstanceConsoleOutputForm",
 *       "console" = "Drupal\openstack\Form\OpenStackInstanceConsoleForm",
 *       "action_log" = "Drupal\openstack\Form\OpenStackInstanceActionLogForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackInstanceDeleteMultipleForm",
 *       "attach_network" = "Drupal\openstack\Form\OpenStackAttachNetworkInterfaceForm",
 *       "detach_network" = "Drupal\openstack\Form\OpenStackDetachNetworkInterfaceForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackInstanceAccessControlHandler",
 *   },
 *   base_table = "openstack_instance",
 *   admin_permission = "administer openstack instance entities",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/instance/{openstack_instance}",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/instance/{openstack_instance}/edit",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/instance/{openstack_instance}/terminate",
 *     "collection" = "/clouds/openstack/{cloud_context}/instance",
 *     "start-form" = "/clouds/openstack/{cloud_context}/instance/{openstack_instance}/start",
 *     "stop-form" = "/clouds/openstack/{cloud_context}/instance/{openstack_instance}/stop",
 *     "reboot-form" = "/clouds/openstack/{cloud_context}/instance/{openstack_instance}/reboot",
 *     "create-image-form" = "/clouds/openstack/{cloud_context}/instance/{openstack_instance}/create_image",
 *     "console-output-form" = "/clouds/openstack/{cloud_context}/instance/{openstack_instance}/console_output",
 *     "console-form" = "/clouds/openstack/{cloud_context}/instance/{openstack_instance}/console",
 *     "action-log-form" = "/clouds/openstack/{cloud_context}/instance/{openstack_instance}/action_log",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/instance/delete_multiple",
 *     "attach-network-form" = "/clouds/openstack/{cloud_context}/instance/attach_network",
 *     "detach-network-form" = "/clouds/openstack/{cloud_context}/instance/detach_network"
 *   },
 *   field_ui_base_route = "openstack_instance.settings"
 * )
 */
class OpenStackInstance extends Instance implements InstanceInterface {

  /**
   * {@inheritdoc}
   */
  public function setPowerState($power_state): OpenStackInstance {
    return $this->set('power_state', $power_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getPortIds(): array {
    return $this->get('port_id')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setPortIds(array $port_ids): InstanceInterface {
    return $this->set('port_id', $port_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getPortIpAddresses(): array {
    return $this->get('port_ip_address')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setPortIpAddresses(array $port_ip_addresses): InstanceInterface {
    return $this->set('port_ip_address', $port_ip_addresses);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['instance_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Flavor'))
      ->setDescription(t("The type of instance determines your instance's CPU capacity, memory, and storage (e.g., m1.small, c1.xlarge)."))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['security_groups'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Security groups'))
      ->setDescription(t('The security groups to which the instance belongs. A security group is a collection of firewall rules that restrict the network traffic for the instance. Click View rules to see the rules for the specific group.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_security_group',
          'field_name' => 'name',
          'comma_separated' => TRUE,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['public_ip'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Public IP'))
      ->setDescription(t('The Public IP.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_floating_ip',
          'field_name' => 'public_ip',
          'html_generator_class' => PublicIpEntityLinkHtmlGenerator::class,
        ],
        'weight' => -8,
      ])
      ->setReadOnly(TRUE);

    $fields['private_ip'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Private IPs'))
      ->setDescription(t('The private IP address of the instance (multiple IP addresses are listed if there is more than one network interface to the instance).'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_floating_ip',
          'field_name' => 'private_ip',
          'html_generator_class' => PublicIpEntityLinkHtmlGenerator::class,
        ],
        'weight' => -7,
      ])
      ->setReadOnly(TRUE);

    $fields['key_pair_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Key pair name'))
      ->setDescription(t('The name of the key pair that you must use to log in to the instance securely.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_key_pair',
          'field_name' => 'key_pair_name',
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['block_devices'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Volume'))
      ->setDescription(t('The Amazon EBS volumes attached to this instance.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_volume',
          'field_name' => 'volume_id',
          'comma_separated' => TRUE,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    // Insert power_state after instance_state.
    $pos = array_search('instance_state', array_keys($fields));
    $field_power_state = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Power state'))
      ->setDescription(t('The power state of the instance.'))
      ->setSetting('allowed_values', [
        '0' => 'nostate',
        '1' => 'running',
        '3' => 'paused',
        '4' => 'shutdown',
        '6' => 'crashed',
        '7' => 'suspended',
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);
    $fields = array_merge(
      array_slice($fields, 0, $pos + 1),
      ['power_state' => $field_power_state],
      array_slice($fields, $pos + 1)
    );

    $fields['image_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('OpenStack image name'))
      ->setDescription(t('The name of the OpenStack image with which the instance was launched.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_image',
          'field_name' => 'image_id',
          'comma_separated' => TRUE,
          'html_generator_class' => EntityLinkWithShortNameHtmlGenerator::class,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['port_id'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Port ID'))
      ->setDescription(t('The ID of the port.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_port',
          'field_name' => 'port_id',
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['port_ip_address'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Port IP Address'))
      ->setDescription(t('The IP Address of the port.'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'settings' => [
          'key_label' => t('Port ID'),
          'value_label' => t('IP Address'),
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['tags']
      ->setDisplayOptions('form', [
        'settings' => [
          'reserved_key_checker_class' => OpenStackReservedKeyChecker::class,
          'value_converter_class' => AwsCloudValueConverter::class,
        ],
      ]);

    return $fields;
  }

}
