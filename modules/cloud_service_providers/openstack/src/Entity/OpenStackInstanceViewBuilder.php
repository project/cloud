<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\InstanceViewBuilder;

/**
 * Provides the instance view builders.
 */
class OpenStackInstanceViewBuilder extends InstanceViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    $defs = parent::getFieldsetDefs();

    $defs[0]['fields'][] = 'power_state';

    $defs[1]['fields'][] = 'port_id';

    // Remove account_id.
    $pos = array_search('account_id', $defs[0]['fields']);
    if ($pos !== FALSE) {
      unset($defs[0]['fields'][$pos]);
    }

    return $defs;
  }

}
