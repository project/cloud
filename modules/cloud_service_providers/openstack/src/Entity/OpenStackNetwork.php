<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the OpenStack network entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_network",
 *   id_plural = "openstack_networks",
 *   label = @Translation("Network"),
 *   label_collection = @Translation("Networks"),
 *   label_singular = @Translation("Network"),
 *   label_plural = @Translation("Networks"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackNetworkViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackNetworkViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackNetworkEditForm",
 *       "add" = "Drupal\openstack\Form\OpenStackNetworkCreateForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackNetworkEditForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackNetworkDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackNetworkDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackNetworkAccessControlHandler",
 *   },
 *   base_table = "openstack_network",
 *   admin_permission = "administer openstack network",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/network/{openstack_network}",
 *     "add-form" = "/clouds/openstack/{cloud_context}/network/{openstack_network}/add",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/network/{openstack_network}/edit",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/network/{openstack_network}/delete",
 *     "collection" = "/clouds/openstack/{cloud_context}/network",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/network/delete_multiple",
 *   },
 *   field_ui_base_route = "openstack_network.settings"
 * )
 */
class OpenStackNetwork extends CloudContentEntityBase implements OpenStackNetworkInterface {

  /**
   * {@inheritdoc}
   */
  public function getNetworkId(): ?string {
    return $this->get('network_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNetworkId($network_id = ''): OpenStackNetworkInterface {
    return $this->set('network_id', $network_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackNetworkInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackNetworkInterface {
    return $this->set('project_id', $project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status = ''): OpenStackNetworkInterface {
    return $this->set('status', $status);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getAdminStateUp(): bool {
    return $this->get('admin_state_up')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAdminStateUp(bool $admin_state_up): OpenStackNetworkInterface {
    return $this->set('admin_state_up', $admin_state_up);
  }

  /**
   * {@inheritdoc}
   */
  public function getShared(): ?bool {
    return $this->get('shared')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setShared(bool $shared): OpenStackNetworkInterface {
    return $this->set('shared', $shared);
  }

  /**
   * {@inheritdoc}
   */
  public function getExternal(): ?bool {
    return $this->get('external')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setExternal(bool $external): OpenStackNetworkInterface {
    return $this->set('external', $external);
  }

  /**
   * {@inheritdoc}
   */
  public function setMtu($mtu = ''): OpenStackNetworkInterface {
    return $this->set('mtu', $mtu);
  }

  /**
   * {@inheritdoc}
   */
  public function getMtu(): ?int {
    return $this->get('mtu')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNetworkType($network_type = ''): OpenStackNetworkInterface {
    return $this->set('network_type', $network_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkType(): ?string {
    return $this->get('network_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPhysicalNetwork($physical_network = ''): OpenStackNetworkInterface {
    return $this->set('physical_network', $physical_network);
  }

  /**
   * {@inheritdoc}
   */
  public function getPhysicalNetwork(): ?string {
    return $this->get('physical_network')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSegmentationId($segmentation_id = ''): OpenStackNetworkInterface {
    return $this->set('segmentation_id', $segmentation_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getSegmentationId(): ?int {
    return $this->get('segmentation_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAvailabilityZones(array $availability_zones): OpenStackNetworkInterface {
    return $this->set('availability_zones', $availability_zones);
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZones(): array {
    return $this->get('availability_zones')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setSubnets(array $subnets): OpenStackNetworkInterface {
    return $this->set('subnets', $subnets);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubnets(): array {
    return $this->get('subnets')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackNetworkInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Network entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Network entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of network.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['network_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Network ID'))
      ->setDescription(t('The ID of the network.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The ID of project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of network.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['admin_state_up'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Admin state'))
      ->setDescription(t('The administrative state of the network, which is up (true) or down (false).'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('up'),
        'off_label' => t('down'),
      ])
      ->setReadOnly(TRUE);

    $fields['shared'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Shared'))
      ->setDescription(t('Indicates whether this network is shared across all tenants. By default, only administrative users can change this value.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('yes'),
        'off_label' => t('no'),
      ])
      ->setReadOnly(TRUE);

    $fields['external'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('External'))
      ->setDescription(t('Defines whether the network may be used for creation of floating IPs.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('yes'),
        'off_label' => t('no'),
      ])
      ->setReadOnly(TRUE);

    $fields['mtu'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('MTU'))
      ->setDescription(t('The maximum transmission unit (MTU) value to address fragmentation. Minimum value is 68 for IPv4, and 1280 for IPv6.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['network_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Network type'))
      ->setDescription(t('The type of physical network that this network is mapped to. For example, flat, vlan, vxlan, or gre. Valid values depend on a networking back-end.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['physical_network'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Physical Network'))
      ->setDescription(t('The physical network where this network/segment is implemented.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['segmentation_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Segmentation ID'))
      ->setDescription(t('The ID of the isolated segment on the physical network. The network_type attribute defines the segmentation model.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['subnets'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Subnets'))
      ->setDescription(t('The associated subnets.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['availability_zones'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Availability Zones'))
      ->setDescription(t('The Availability Zones for the network.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Network entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
