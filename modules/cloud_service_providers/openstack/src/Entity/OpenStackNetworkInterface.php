<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackNetwork entity.
 *
 * @ingroup openstack
 */
interface OpenStackNetworkInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getNetworkId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNetworkId($network_id = ''): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status = ''): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getAdminStateUp(): bool;

  /**
   * {@inheritdoc}
   */
  public function setAdminStateUp(bool $admin_state_up): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getShared(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function setShared(bool $shared): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getExternal(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function setExternal(bool $external): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function setMtu($mtu = ''): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getMtu(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setNetworkType($network_type = ''): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getNetworkType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setPhysicalNetwork($physical_network = ''): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getPhysicalNetwork(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setSegmentationId($segmentation_id = ''): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getSegmentationId(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setAvailabilityZones(array $availability_zones): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZones(): array;

  /**
   * {@inheritdoc}
   */
  public function setSubnets(array $subnets): OpenStackNetworkInterface;

  /**
   * {@inheritdoc}
   */
  public function getSubnets(): array;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackNetworkInterface;

}
