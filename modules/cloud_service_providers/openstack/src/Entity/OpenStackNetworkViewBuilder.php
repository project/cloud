<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the network view builders.
 */
class OpenStackNetworkViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'network',
        'title' => $this->t('Network'),
        'open' => TRUE,
        'fields' => [
          'name',
          'network_id',
          'project_id',
          'status',
          'admin_state_up',
          'shared',
          'external',
          'mtu',
          'network_type',
          'physical_network',
          'segmentation_id',
          'subnets',
          'availability_zones',
          'created',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
