<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;

/**
 * Defines the OpenStack port entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_port",
 *   id_plural = "openstack_ports",
 *   label = @Translation("Port"),
 *   label_collection = @Translation("Ports"),
 *   label_singular = @Translation("Port"),
 *   label_plural = @Translation("Ports"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackPortViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackPortViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackPortEditForm",
 *       "add" = "Drupal\openstack\Form\OpenStackPortCreateForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackPortEditForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackPortDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackPortDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackPortAccessControlHandler",
 *   },
 *   base_table = "openstack_port",
 *   admin_permission = "administer openstack port",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/port/{openstack_port}",
 *     "add-form" = "/clouds/openstack/{cloud_context}/port/{openstack_port}/add",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/port/{openstack_port}/edit",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/port/{openstack_port}/delete",
 *     "collection" = "/clouds/openstack/{cloud_context}/port",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/port/delete_multiple",
 *   },
 *   field_ui_base_route = "openstack_port.settings"
 * )
 */
class OpenStackPort extends CloudContentEntityBase implements OpenStackPortInterface {

  /**
   * {@inheritdoc}
   */
  public function getPortId(): ?string {
    return $this->get('port_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPortId($port_id = ''): OpenStackPortInterface {
    return $this->set('port_id', $port_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackPortInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackPortInterface {
    return $this->set('project_id', $project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkId(): ?string {
    return $this->get('network_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNetworkId($network_id = ''): OpenStackPortInterface {
    return $this->set('network_id', $network_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getMacAddress(): ?string {
    return $this->get('mac_address')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMacAddress($mac_address = ''): OpenStackPortInterface {
    return $this->set('mac_address', $mac_address);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status = ''): OpenStackPortInterface {
    return $this->set('status', $status);
  }

  /**
   * {@inheritdoc}
   */
  public function getAdminStateUp(): bool {
    return $this->get('admin_state_up')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAdminStateUp($admin_state_up): OpenStackPortInterface {
    return $this->set('admin_state_up', $admin_state_up);
  }

  /**
   * {@inheritdoc}
   */
  public function getPortSecurityEnabled(): bool {
    return $this->get('port_security_enabled')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPortSecurityEnabled($port_security_enabled): OpenStackPortInterface {
    return $this->set('port_security_enabled', $port_security_enabled);
  }

  /**
   * {@inheritdoc}
   */
  public function getDnsName(): ?string {
    return $this->get('dns_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDnsName($dns_name = ''): OpenStackPortInterface {
    return $this->set('dns_name', $dns_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getDnsAssignment(): array {
    return $this->get('dns_assignment')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setDnsAssignment(array $dns_assignment): OpenStackPortInterface {
    return $this->set('dns_assignment', $dns_assignment);
  }

  /**
   * {@inheritdoc}
   */
  public function getFixedIps(): array {
    return $this->get('fixed_ips')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setFixedIps(array $fixed_ips): OpenStackPortInterface {
    return $this->set('fixed_ips', $fixed_ips);
  }

  /**
   * {@inheritdoc}
   */
  public function getDeviceOwner(): ?string {
    return $this->get('device_owner')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeviceOwner($device_owner = ''): OpenStackPortInterface {
    return $this->set('device_owner', $device_owner);
  }

  /**
   * {@inheritdoc}
   */
  public function getDeviceId(): ?string {
    return $this->get('device_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeviceId($device_id = ''): OpenStackPortInterface {
    return $this->set('device_id', $device_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroups(): array {
    return $this->get('security_groups')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setSecurityGroups(array $security_groups): OpenStackPortInterface {
    return $this->set('security_groups', $security_groups);
  }

  /**
   * {@inheritdoc}
   */
  public function getBindingVnicType(): ?string {
    return $this->get('binding_vnic_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBindingVnicType($binding_vnic_type = ''): OpenStackPortInterface {
    return $this->set('binding_vnic_type', $binding_vnic_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getBindingHostId(): ?string {
    return $this->get('binding_host_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBindingHostId($binding_host_id = ''): OpenStackPortInterface {
    return $this->set('binding_host_id', $binding_host_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getBindingProfile(): ?string {
    return $this->get('binding_profile')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBindingProfile($binding_profile): OpenStackPortInterface {
    return $this->set('binding_profile', $binding_profile);
  }

  /**
   * {@inheritdoc}
   */
  public function getBindingVifType(): ?string {
    return $this->get('binding_vif_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBindingVifType($binding_vif_type = ''): OpenStackPortInterface {
    return $this->set('binding_vif_type', $binding_vif_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getBindingVifDetails(): array {
    return $this->get('binding_vif_details')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setBindingVifDetails(array $binding_vif_details): OpenStackPortInterface {
    return $this->set('binding_vif_details', $binding_vif_details);
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedAddressPairs(): array {
    return $this->get('allowed_address_pairs')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setAllowedAddressPairs(array $allowed_address_pairs): OpenStackPortInterface {
    return $this->set('allowed_address_pairs', $allowed_address_pairs);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackPortInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Port entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Port entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of port.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['port_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Port ID'))
      ->setDescription(t('The ID of the port.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The ID of project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['network_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Network'))
      ->setDescription(t('The network.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_network',
          'field_name' => 'network_id',
          'comma_separated' => TRUE,
          'html_generator_class' => EntityLinkWithNameHtmlGenerator::class,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['mac_address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('MAC address'))
      ->setDescription(t('The MAC address of the port.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The port status. Values are ACTIVE, DOWN, BUILD and ERROR.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['admin_state_up'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Admin state'))
      ->setDescription(t('The administrative state of the resource, which is up (true) or down (false).'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('up'),
        'off_label' => t('down'),
      ])
      ->setReadOnly(TRUE);

    $fields['port_security_enabled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Port security'))
      ->setDescription(t('The port security status. A valid value is enabled (true) or disabled (false).'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('enabled'),
        'off_label' => t('disabled'),
      ])
      ->setReadOnly(TRUE);

    $fields['dns_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('DNS name'))
      ->setDescription(t('The DNS name of the port.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['dns_assignment'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('DNS Assignment'))
      ->setDescription(t('Data assigned to a port by the Networking internal DNS including the hostname, ip_address and fqdn.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['fixed_ips'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Fixed IPs'))
      ->setDescription(t('The IP addresses for the port.'))
      ->setDisplayOptions('view', [
        'type' => 'openstack_fixed_ips_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['device_owner'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Device owner'))
      ->setDescription(t('The entity type that uses this port.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['device_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Device ID'))
      ->setDescription(t('The ID of the device that uses this port.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['security_groups'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Security groups'))
      ->setDescription(t('The IDs of security groups applied to the port.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_security_group',
          'field_name' => 'group_id',
          'html_generator_class' => EntityLinkWithNameHtmlGenerator::class,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['binding_vnic_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('VNIC type'))
      ->setDescription(t('The type of VNIC which this port should be attached to.'))
      ->setSettings([
        'allowed_values' => self::getVnicTypeAllowedValues(),
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['binding_host_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Host'))
      ->setDescription(t('The ID of the host where the port resides.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['binding_profile'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Profile'))
      ->setDescription(t('A JSON string that enables the application running on the specific host to pass and receive vif port information specific to the networking back-end.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['binding_vif_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('VIF type'))
      ->setDescription(t('The type of which mechanism is used for the port.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['binding_vif_details'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('VIF details'))
      ->setDescription(t('A dictionary which contains additional information on the port.'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'weight' => -5,
      ]);

    $fields['allowed_address_pairs'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Allowed Address Pairs'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'settings' => [
          'key_label' => t('IP address or CIDR'),
          'value_label' => t('MAC address'),
        ],
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'key_value_item',
        'settings' => [
          'key_label' => t('IP address or CIDR'),
          'value_label' => t('MAC address'),
        ],
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Port entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Get VNIC type allowed values.
   *
   * @return array
   *   The VNIC type allowed values.
   */
  public static function getVnicTypeAllowedValues(): array {
    return [
      'normal' => t('Normal'),
      'direct' => t('Direct'),
      'direct-physical' => t('Direct Physical'),
      'macvtap' => t('MacVTap'),
      'baremetal' => t('Bare Metal'),
      'virtio-forwarder' => t('Virtio Forwarder'),
    ];
  }

}
