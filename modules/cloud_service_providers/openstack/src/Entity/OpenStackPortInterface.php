<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackPort entity.
 *
 * @ingroup openstack
 */
interface OpenStackPortInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getPortId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setPortId($port_id = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getNetworkId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNetworkId($network_id = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getMacAddress(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setMacAddress($mac_address = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getAdminStateUp(): bool;

  /**
   * {@inheritdoc}
   */
  public function setAdminStateUp($admin_state_up): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getPortSecurityEnabled(): bool;

  /**
   * {@inheritdoc}
   */
  public function setPortSecurityEnabled($port_security_enabled): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getDnsName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDnsName($dns_name = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getDnsAssignment(): array;

  /**
   * {@inheritdoc}
   */
  public function setDnsAssignment(array $dns_assignment): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getFixedIps(): array;

  /**
   * {@inheritdoc}
   */
  public function setFixedIps(array $fixed_ips): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getDeviceOwner(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDeviceOwner($device_owner = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getDeviceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDeviceId($device_id = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroups(): array;

  /**
   * {@inheritdoc}
   */
  public function setSecurityGroups(array $security_groups): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getBindingVnicType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setBindingVnicType($binding_vnic_type = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getBindingHostId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setBindingHostId($binding_host_id = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getBindingProfile(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setBindingProfile($binding_profile): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getBindingVifType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setBindingVifType($binding_vif_type = ''): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getBindingVifDetails(): array;

  /**
   * {@inheritdoc}
   */
  public function setBindingVifDetails(array $binding_vif_details): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function getAllowedAddressPairs(): array;

  /**
   * {@inheritdoc}
   */
  public function setAllowedAddressPairs(array $allowed_address_pairs): OpenStackPortInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackPortInterface;

}
