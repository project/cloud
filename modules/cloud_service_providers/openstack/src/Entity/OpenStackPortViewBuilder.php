<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the port view builders.
 */
class OpenStackPortViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'port',
        'title' => $this->t('Port'),
        'open' => TRUE,
        'fields' => [
          'name',
          'port_id',
          'project_id',
          'network_id',
          'mac_address',
          'status',
          'admin_state_up',
          'port_security_enabled',
          'dns_name',
          'dns_assignment',
          'fixed_ips',
          'allowed_address_pairs',
          'device_owner',
          'device_id',
          'security_groups',
          'binding_vnic_type',
          'binding_host_id',
          'binding_profile',
          'binding_vif_type',
          'binding_vif_details',
          'created',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
