<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewsData;

/**
 * Provides the views data for the Port entity type.
 */
class OpenStackPortViewsData extends AwsCloudViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $table_name = $this->storage->getEntityTypeId();
    $fields = $this->getFieldStorageDefinitions();

    $data[$table_name]['openstack_port_bulk_form'] = [
      'title' => $this->t('Port operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple ports.'),
      'field' => [
        'id' => 'openstack_port_bulk_form',
      ],
    ];

    $data[$table_name]['openstack_remove_router_interface'] = [
      'field' => [
        'title' => $this->t('Remove router interface'),
        'help' => $this->t('Remove router interface.'),
        'id' => 'openstack_remove_router_interface',
      ],
    ];

    $data[$table_name]['router']['relationship'] = [
      'id' => 'standard',
      'title' => $this->t('Router'),
      'label' => $this->t('Router'),
      'group' => 'Port',
      'help' => $this->t('Reference to router'),
      'base' => 'openstack_router',
      'base field' => 'router_id',
      'relationship field' => 'device_id',
    ];

    // The following is a list of fields to turn from text search to
    // select list.  This list can be expanded through hook_views_data_alter().
    $selectable = [];

    // Add an access query tag.
    $data[$table_name]['table']['base']['access query tag'] = 'openstack_port_views_access';

    $this->addDropdownSelector($data, $table_name, $fields, $selectable);

    return $data;
  }

}
