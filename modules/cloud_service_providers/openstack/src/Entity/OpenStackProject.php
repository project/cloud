<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the OpenStack project entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_project",
 *   id_plural = "openstack_projects",
 *   label = @Translation("Project"),
 *   label_collection = @Translation("Projects"),
 *   label_singular = @Translation("Project"),
 *   label_plural = @Translation("Projects"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackProjectViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackProjectViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackProjectEditForm",
 *       "add" = "Drupal\openstack\Form\OpenStackProjectCreateForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackProjectEditForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackProjectDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackProjectDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackProjectAccessControlHandler",
 *   },
 *   base_table = "openstack_project",
 *   admin_permission = "administer openstack project",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/project/{openstack_project}",
 *     "add-form" = "/clouds/openstack/{cloud_context}/project/{openstack_project}/add",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/project/{openstack_project}/edit",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/project/{openstack_project}/delete",
 *     "collection" = "/clouds/openstack/{cloud_context}/project",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/project/delete_multiple",
 *   },
 *   field_ui_base_route = "openstack_project.settings"
 * )
 */
class OpenStackProject extends CloudContentEntityBase implements OpenStackProjectInterface {

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackProjectInterface {
    return $this->set('project_id', $project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackProjectInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): OpenStackProjectInterface {
    return $this->set('description', $description);
  }

  /**
   * {@inheritdoc}
   */
  public function getDomainId(): ?string {
    return $this->get('domain_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDomainId($domain_id = ''): OpenStackProjectInterface {
    return $this->set('domain_id', $domain_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getIsDomain(): bool {
    return $this->get('is_domain')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setIsDomain($is_domain): OpenStackProjectInterface {
    return $this->set('is_domain', $is_domain);
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabled(): bool {
    return $this->get('enabled')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled($enabled): OpenStackProjectInterface {
    return $this->set('enabled', $enabled);
  }

  /**
   * {@inheritdoc}
   */
  public function getUserRoles(): array {
    return $this->get('user_roles')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setUserRoles(array $user_roles): OpenStackProjectInterface {
    return $this->set('user_roles', $user_roles);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackProjectInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Project entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Project entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The ID of the project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('The description of the project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['is_domain'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is domain'))
      ->setDescription(t('Indicates whether the project also acts as a domain.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('yes'),
        'off_label' => t('no'),
      ])
      ->setReadOnly(TRUE);

    $fields['domain_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Domain ID'))
      ->setDescription(t('The ID of the domain for the project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['enabled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setDescription(t('If set to true, project is enabled. If set to false, project is disabled.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('yes'),
        'off_label' => t('no'),
      ])
      ->setReadOnly(TRUE);

    $fields['user_roles'] = BaseFieldDefinition::create('user_roles')
      ->setLabel(t('User Roles'))
      ->setDescription(t("User roles."))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'type' => 'user_roles_formatter',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'user_roles_item',
        'weight' => -5,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Project entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
