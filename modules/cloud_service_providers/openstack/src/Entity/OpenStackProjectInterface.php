<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackProject entity.
 *
 * @ingroup openstack
 */
interface OpenStackProjectInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackProjectInterface;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackProjectInterface;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): OpenStackProjectInterface;

  /**
   * {@inheritdoc}
   */
  public function getDomainId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDomainId($domain_id = ''): OpenStackProjectInterface;

  /**
   * {@inheritdoc}
   */
  public function getIsDomain(): bool;

  /**
   * {@inheritdoc}
   */
  public function setIsDomain($is_domain): OpenStackProjectInterface;

  /**
   * {@inheritdoc}
   */
  public function getEnabled(): bool;

  /**
   * {@inheritdoc}
   */
  public function setEnabled($enabled): OpenStackProjectInterface;

  /**
   * {@inheritdoc}
   */
  public function getUserRoles(): array;

  /**
   * {@inheritdoc}
   */
  public function setUserRoles(array $user_roles): OpenStackProjectInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackProjectInterface;

}
