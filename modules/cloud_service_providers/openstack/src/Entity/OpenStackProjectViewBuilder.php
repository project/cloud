<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the project view builders.
 */
class OpenStackProjectViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'project',
        'title' => $this->t('Project'),
        'open' => TRUE,
        'fields' => [
          'name',
          'project_id',
          'description',
          'is_domain',
          'domain_id',
          'enabled',
          'created',
        ],
      ],
      [
        'name' => 'users',
        'title' => $this->t('Users'),
        'open' => TRUE,
        'fields' => [
          'user_roles',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
