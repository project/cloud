<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewsData;

/**
 * Provides the views data for the Project entity type.
 */
class OpenStackProjectViewsData extends AwsCloudViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $table_name = $this->storage->getEntityTypeId();
    $fields = $this->getFieldStorageDefinitions();

    $data[$table_name]['openstack_project_bulk_form'] = [
      'title' => $this->t('Project operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple projects.'),
      'field' => [
        'id' => 'openstack_project_bulk_form',
      ],
    ];

    // The following is a list of fields to turn from text search to
    // select list.  This list can be expanded through hook_views_data_alter().
    $selectable = [];

    $this->addDropdownSelector($data, $table_name, $fields, $selectable);

    return $data;
  }

}
