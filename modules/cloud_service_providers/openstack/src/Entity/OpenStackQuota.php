<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the OpenStack quota entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_quota",
 *   id_plural = "openstack_quotas",
 *   label = @Translation("Quota"),
 *   label_collection = @Translation("Quotas"),
 *   label_singular = @Translation("Quota"),
 *   label_plural = @Translation("Quotas"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackQuotaViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackQuotaViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackQuotaEditForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackQuotaEditForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackQuotaAccessControlHandler",
 *   },
 *   base_table = "openstack_quota",
 *   admin_permission = "administer openstack quota",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/quota/{openstack_quota}",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/quota/{openstack_quota}/edit",
 *     "collection" = "/clouds/openstack/{cloud_context}/quota",
 *   },
 *   field_ui_base_route = "openstack_quota.settings"
 * )
 */
class OpenStackQuota extends CloudContentEntityBase implements OpenStackQuotaInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackQuotaInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackQuotaInterface {
    return $this->set('project_id', $project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstances(): ?int {
    return $this->get('instances')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInstances($instances): OpenStackQuotaInterface {
    return $this->set('instances', $instances);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstancesUsage(): ?int {
    return $this->get('instances_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInstancesUsage($instances): OpenStackQuotaInterface {
    return $this->set('instances_usage', $instances);
  }

  /**
   * {@inheritdoc}
   */
  public function getCores(): ?int {
    return $this->get('cores')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCores($cores): OpenStackQuotaInterface {
    return $this->set('cores', $cores);
  }

  /**
   * {@inheritdoc}
   */
  public function getCoresUsage(): ?int {
    return $this->get('cores_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCoresUsage($cores): OpenStackQuotaInterface {
    return $this->set('cores_usage', $cores);
  }

  /**
   * {@inheritdoc}
   */
  public function getRam(): ?int {
    return $this->get('ram')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRam($ram): OpenStackQuotaInterface {
    return $this->set('ram', $ram);
  }

  /**
   * {@inheritdoc}
   */
  public function getRamUsage(): ?int {
    return $this->get('ram_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRamUsage($ram): OpenStackQuotaInterface {
    return $this->set('ram_usage', $ram);
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataItems(): ?int {
    return $this->get('metadata_items')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMetadataItems($metadata_items): OpenStackQuotaInterface {
    return $this->set('metadata_items', $metadata_items);
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataItemsUsage(): ?int {
    return $this->get('metadata_items_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMetadataItemsUsage($metadata_items): OpenStackQuotaInterface {
    return $this->set('metadata_items_usage', $metadata_items);
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyPairs(): ?int {
    return $this->get('key_pairs')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setKeyPairs($key_pairs): OpenStackQuotaInterface {
    return $this->set('key_pairs', $key_pairs);
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyPairsUsage(): ?int {
    return $this->get('key_pairs_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setKeyPairsUsage($key_pairs): OpenStackQuotaInterface {
    return $this->set('key_pairs_usage', $key_pairs);
  }

  /**
   * {@inheritdoc}
   */
  public function getServerGroups(): ?int {
    return $this->get('server_groups')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setServerGroups($server_groups): OpenStackQuotaInterface {
    return $this->set('server_groups', $server_groups);
  }

  /**
   * {@inheritdoc}
   */
  public function getServerGroupsUsage(): ?int {
    return $this->get('server_groups_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setServerGroupsUsage($server_groups): OpenStackQuotaInterface {
    return $this->set('server_groups_usage', $server_groups);
  }

  /**
   * {@inheritdoc}
   */
  public function getServerGroupMembers(): ?int {
    return $this->get('server_group_members')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setServerGroupMembers($server_group_members): OpenStackQuotaInterface {
    return $this->set('server_group_members', $server_group_members);
  }

  /**
   * {@inheritdoc}
   */
  public function getServerGroupMembersUsage(): ?int {
    return $this->get('server_group_members_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setServerGroupMembersUsage($server_group_members): OpenStackQuotaInterface {
    return $this->set('server_group_members_usage', $server_group_members);
  }

  /**
   * {@inheritdoc}
   */
  public function getInjectedFiles(): ?int {
    return $this->get('injected_files')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInjectedFiles($injected_files): OpenStackQuotaInterface {
    return $this->set('injected_files', $injected_files);
  }

  /**
   * {@inheritdoc}
   */
  public function getInjectedFilesUsage(): ?int {
    return $this->get('injected_files_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInjectedFilesUsage($injected_files): OpenStackQuotaInterface {
    return $this->set('injected_files_usage', $injected_files);
  }

  /**
   * {@inheritdoc}
   */
  public function getInjectedFileContentBytes(): ?int {
    return $this->get('injected_file_content_bytes')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInjectedFileContentBytes($injected_file_content_bytes): OpenStackQuotaInterface {
    return $this->set('injected_file_content_bytes', $injected_file_content_bytes);
  }

  /**
   * {@inheritdoc}
   */
  public function getInjectedFileContentBytesUsage(): ?int {
    return $this->get('injected_file_content_bytes_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInjectedFileContentBytesUsage($injected_file_content_bytes): OpenStackQuotaInterface {
    return $this->set('injected_file_content_bytes_usage', $injected_file_content_bytes);
  }

  /**
   * {@inheritdoc}
   */
  public function getInjectedFilePathBytes(): ?int {
    return $this->get('injected_file_path_bytes')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInjectedFilePathBytes($injected_file_path_bytes): OpenStackQuotaInterface {
    return $this->set('injected_file_path_bytes', $injected_file_path_bytes);
  }

  /**
   * {@inheritdoc}
   */
  public function getInjectedFilePathBytesUsage(): ?int {
    return $this->get('injected_file_path_bytes_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInjectedFilePathBytesUsage($injected_file_path_bytes): OpenStackQuotaInterface {
    return $this->set('injected_file_path_bytes_usage', $injected_file_path_bytes);
  }

  /**
   * {@inheritdoc}
   */
  public function getVolumes(): ?int {
    return $this->get('volumes')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVolumes($volumes): OpenStackQuotaInterface {
    return $this->set('volumes', $volumes);
  }

  /**
   * {@inheritdoc}
   */
  public function getVolumesUsage(): ?int {
    return $this->get('volumes_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVolumesUsage($volumes): OpenStackQuotaInterface {
    return $this->set('volumes_usage', $volumes);
  }

  /**
   * {@inheritdoc}
   */
  public function getSnapshots(): ?int {
    return $this->get('snapshots')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSnapshots($snapshots): OpenStackQuotaInterface {
    return $this->set('snapshots', $snapshots);
  }

  /**
   * {@inheritdoc}
   */
  public function getSnapshotsUsage(): ?int {
    return $this->get('snapshots_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSnapshotsUsage($snapshots): OpenStackQuotaInterface {
    return $this->set('snapshots_usage', $snapshots);
  }

  /**
   * {@inheritdoc}
   */
  public function getGigaBytes(): ?int {
    return $this->get('gigabytes')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGigaBytes($gigabytes): OpenStackQuotaInterface {
    return $this->set('gigabytes', $gigabytes);
  }

  /**
   * {@inheritdoc}
   */
  public function getGigaBytesUsage(): ?int {
    return $this->get('gigabytes_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGigaBytesUsage($gigabytes): OpenStackQuotaInterface {
    return $this->set('gigabytes_usage', $gigabytes);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetwork(): ?int {
    return $this->get('network')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNetwork($network): OpenStackQuotaInterface {
    return $this->set('network', $network);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkUsage(): ?int {
    return $this->get('network_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNetworkUsage($network): OpenStackQuotaInterface {
    return $this->set('network_usage', $network);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubnet(): ?int {
    return $this->get('subnet')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubnet($subnet): OpenStackQuotaInterface {
    return $this->set('subnet', $subnet);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubnetUsage(): ?int {
    return $this->get('subnet_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubnetUsage($subnet): OpenStackQuotaInterface {
    return $this->set('subnet_usage', $subnet);
  }

  /**
   * {@inheritdoc}
   */
  public function getPort(): ?int {
    return $this->get('port')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPort($port): OpenStackQuotaInterface {
    return $this->set('port', $port);
  }

  /**
   * {@inheritdoc}
   */
  public function getPortUsage(): ?int {
    return $this->get('port_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPortUsage($port): OpenStackQuotaInterface {
    return $this->set('port_usage', $port);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouter(): ?int {
    return $this->get('router')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRouter($router): OpenStackQuotaInterface {
    return $this->set('router', $router);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouterUsage(): ?int {
    return $this->get('router_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRouterUsage($router): OpenStackQuotaInterface {
    return $this->set('router_usage', $router);
  }

  /**
   * {@inheritdoc}
   */
  public function getFloatingIp(): ?int {
    return $this->get('floatingip')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFloatingIp($floating_ip): OpenStackQuotaInterface {
    return $this->set('floatingip', $floating_ip);
  }

  /**
   * {@inheritdoc}
   */
  public function getFloatingIpUsage(): ?int {
    return $this->get('floatingip_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFloatingIpUsage($floating_ip): OpenStackQuotaInterface {
    return $this->set('floatingip_usage', $floating_ip);
  }

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroup(): ?int {
    return $this->get('security_group')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSecurityGroup($security_group): OpenStackQuotaInterface {
    return $this->set('security_group', $security_group);
  }

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroupUsage(): ?int {
    return $this->get('security_group_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSecurityGroupUsage($security_group): OpenStackQuotaInterface {
    return $this->set('security_group_usage', $security_group);
  }

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroupRule(): ?int {
    return $this->get('security_group_rule')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSecurityGroupRule($security_group_rule): OpenStackQuotaInterface {
    return $this->set('security_group_rule', $security_group_rule);
  }

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroupRuleUsage(): ?int {
    return $this->get('security_group_rule_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSecurityGroupRuleUsage($security_group_rule): OpenStackQuotaInterface {
    return $this->set('security_group_rule_usage', $security_group_rule);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): OpenStackQuotaInterface {
    return $this->set('status', $status);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackQuotaInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public function copyToNewFields(): OpenStackQuotaInterface {
    $editable_fields = self::getEditableFields();
    array_walk($editable_fields, function ($field) {
      $this->set("{$field}_new", $this->get($field)->value);
    });

    return $this;
  }

  /**
   * Get editable field sets.
   *
   * @return array
   *   Editable field sets.
   */
  public static function getEditableFieldSets(): array {
    return [
      [
        'name' => 'quota_compute',
        'title' => t('Compute'),
        'fields' => [
          'instances',
          'cores',
          'ram',
          'metadata_items',
          'key_pairs',
          'server_groups',
          'server_group_members',
          'injected_files',
          'injected_file_content_bytes',
          'injected_file_path_bytes',
        ],
      ],
      [
        'name' => 'quota_volume',
        'title' => t('Volume'),
        'fields' => [
          'volumes',
          'snapshots',
          'gigabytes',
        ],
      ],
      [
        'name' => 'quota_network',
        'title' => t('Network'),
        'fields' => [
          'network',
          'subnet',
          'port',
          'router',
          'floatingip',
          'security_group',
          'security_group_rule',
        ],
      ],
    ];
  }

  /**
   * Get editable fields.
   *
   * @return array
   *   Editable fields.
   */
  public static function getEditableFields(): array {
    $fields = [];

    foreach (self::getEditableFieldSets() as $field_set) {
      foreach ($field_set['fields'] as $field_name) {
        $fields[] = $field_name;
      }
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Quota entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Quota entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of quota.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The ID of project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['instances'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Instances'))
      ->setDescription(t('The number of allowed servers for each tenant.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['instances_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Instances usage'))
      ->setDescription(t('The usage number of allowed servers for each tenant.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['cores'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('vCPUs'))
      ->setDescription(t('The number of allowed server cores for each tenant.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['cores_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('vCPUs usage'))
      ->setDescription(t('The used number of allowed server cores for each tenant.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['ram'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('RAM(MiB)'))
      ->setDescription(t('The amount of allowed server RAM, in MiB, for each tenant.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['ram_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('RAM(MiB) Usage'))
      ->setDescription(t('The used amount of allowed server RAM, in MiB, for each tenant.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['metadata_items'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Metadata Items'))
      ->setDescription(t('The number of allowed metadata items for each server.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['metadata_items_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Metadata Items Usage'))
      ->setDescription(t('The used number of allowed metadata items for each server.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['key_pairs'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Key Pairs'))
      ->setDescription(t('The number of allowed key pairs for each user.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['key_pairs_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Key Pairs Usage'))
      ->setDescription(t('The used number of allowed key pairs for each user.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['server_groups'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Server Groups'))
      ->setDescription(t('The number of allowed server groups for each tenant.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['server_groups_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Server Groups Usage'))
      ->setDescription(t('The used number of allowed server groups for each tenant.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['server_group_members'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Server Group Members'))
      ->setDescription(t('The number of allowed members for each server group.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['server_group_members_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Server Group Members Usage'))
      ->setDescription(t('The used number of allowed members for each server group.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['injected_files'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Injected Files'))
      ->setDescription(t('The number of allowed injected files for each tenant.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['injected_files_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Injected Files Usage'))
      ->setDescription(t('The used number of allowed injected files for each tenant.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['injected_file_content_bytes'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Injected File Content(Bytes)'))
      ->setDescription(t('The number of allowed bytes for each injected file path.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['injected_file_content_bytes_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Injected File Content(Bytes) Usage'))
      ->setDescription(t('The used number of allowed bytes for each injected file path.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['injected_file_path_bytes'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Length of Injected File Path'))
      ->setDescription(t('The number of allowed bytes for each injected file path.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['injected_file_path_bytes_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Length of Injected File Path Usage'))
      ->setDescription(t('The used number of allowed bytes for each injected file path.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['volumes'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Volumes'))
      ->setDescription(t('The number of volumes that are allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['volumes_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Volumes usage'))
      ->setDescription(t('The used number of volumes that are allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['snapshots'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Volume snapshots'))
      ->setDescription(t('The number of snapshots that are allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['snapshots_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Volume snapshots Usage'))
      ->setDescription(t('The used number of snapshots that are allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['gigabytes'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total size of volumes and snapshots (GiB)'))
      ->setDescription(t('The size (GB) of volumes and snapshots that are allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['gigabytes_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total used size of volumes and snapshots (GiB)'))
      ->setDescription(t('The used size (GB) of volumes and snapshots that are allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['network'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Networks'))
      ->setDescription(t('The number of networks allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['network_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Networks Usage'))
      ->setDescription(t('The used number of networks allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['subnet'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Subnets'))
      ->setDescription(t('The number of subnets allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['subnet_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Subnets Usage'))
      ->setDescription(t('The used number of subnets allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['port'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Ports'))
      ->setDescription(t('The number of ports allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['port_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Ports Usage'))
      ->setDescription(t('The used number of ports allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['router'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Routers'))
      ->setDescription(t('The number of routers allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['router_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Routers Usage'))
      ->setDescription(t('The used number of routers allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['floatingip'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Floating IPs'))
      ->setDescription(t('The number of floating IP addresses allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['floatingip_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Floating IPs Usage'))
      ->setDescription(t('The used number of floating IP addresses allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['security_group'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Security groups'))
      ->setDescription(t('The number of security groups allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['security_group_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Security groups usage'))
      ->setDescription(t('The used number of security groups allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['security_group_rule'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Security group rules'))
      ->setDescription(t('The number of security group rules allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ]);

    $fields['security_group_rule_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Security group rules usage'))
      ->setDescription(t('The used number of security group rules allowed for each project.'))
      ->setSetting('min', -1)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'quota_usage_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setRequired(TRUE)
      ->setLabel(t('Status'))
      ->setDescription(t('The status of the application.'))
      ->setSetting('allowed_values_function', 'Drupal\openstack\Entity\OpenStackQuota::getAllowedStatusValues')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'weight' => -5,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Quota entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Add *_new field for the editable field.
    foreach (self::getEditableFields() as $field_name) {
      $definition = $fields[$field_name];
      $definition->setName("{$field_name}_new");

      // Create a new field.
      $new_definition = BaseFieldDefinition::createFromFieldStorageDefinition($definition)
        ->setDisplayOptions('form', [
          'type' => 'number',
          'weight' => -5,
        ]);
      $fields["{$field_name}_new"] = $new_definition;

      // Restore the name of definition.
      $definition->setName($field_name);
    }

    return $fields;
  }

  /**
   * Set dynamic allowed values for the status field.
   *
   * @param \Drupal\Core\Field\BaseFieldDefinition $definition
   *   The field definition.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
   *   The entity being created if applicable.
   * @param bool $cacheable
   *   Boolean indicating if the results are cacheable.
   *
   * @return array
   *   An array of possible key and value options.
   */
  public static function getAllowedStatusValues(BaseFieldDefinition $definition, ?ContentEntityInterface $entity = NULL, bool $cacheable = FALSE): array {
    $allowed_values = [
      self::DRAFT => self::DRAFT,
      self::REVIEW => self::REVIEW,
    ];

    if (empty($entity)) {
      return $allowed_values;
    }

    $account = \Drupal::currentUser();
    if ($account->hasPermission(self::PERMISSION_TO_APPROVE)
      || $entity->getStatus() === self::APPROVED) {
      $allowed_values += [self::APPROVED => self::APPROVED];
    }

    return $allowed_values;
  }

}
