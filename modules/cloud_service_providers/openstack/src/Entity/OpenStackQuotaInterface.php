<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackQuota entity.
 *
 * @ingroup openstack
 */
interface OpenStackQuotaInterface extends ContentEntityInterface, EntityOwnerInterface {

  public const DRAFT = 'Draft';
  public const REVIEW = 'Review';
  public const APPROVED = 'Approved';
  public const PERMISSION_TO_APPROVE = 'approve openstack quota';

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getInstances(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setInstances($instances): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getCores(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setCores($cores): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getRam(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setRam($ram): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getMetadataItems(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setMetadataItems($metadata_items): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getKeyPairs(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setKeyPairs($key_pairs): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getServerGroups(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setServerGroups($server_groups): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getServerGroupMembers(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setServerGroupMembers($server_group_members): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getInjectedFiles(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setInjectedFiles($injected_files): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getInjectedFileContentBytes(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setInjectedFileContentBytes($injected_file_content_bytes): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getInjectedFilePathBytes(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setInjectedFilePathBytes($injected_file_path_bytes): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getVolumes(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setVolumes($volumes): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getSnapshots(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setSnapshots($snapshots): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getGigaBytes(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setGigaBytes($gigabytes): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getNetwork(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setNetwork($network): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getSubnet(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setSubnet($subnet): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getPort(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setPort($port): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getRouter(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setRouter($router): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getFloatingIp(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setFloatingIp($floating_ip): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroup(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setSecurityGroup($security_group): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getSecurityGroupRule(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setSecurityGroupRule($security_group_rule): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackQuotaInterface;

  /**
   * {@inheritdoc}
   */
  public function copyToNewFields(): OpenStackQuotaInterface;

}
