<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the quota view builders.
 */
class OpenStackQuotaViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'quota',
        'title' => $this->t('Quota'),
        'open' => TRUE,
        'fields' => [
          'name',
          'status',
          'created',
        ],
      ],
      [
        'name' => 'quota_compute',
        'title' => $this->t('Compute'),
        'open' => TRUE,
        'fields' => [
          'instances_usage',
          'cores_usage',
          'ram_usage',
          'metadata_items_usage',
          'key_pairs_usage',
          'server_groups_usage',
          'server_group_members_usage',
          'injected_files_usage',
          'injected_file_content_bytes_usage',
          'injected_file_path_bytes_usage',
        ],
      ],
      [
        'name' => 'quota_volume',
        'title' => $this->t('Volume'),
        'open' => TRUE,
        'fields' => [
          'volumes_usage',
          'snapshots_usage',
          'gigabytes_usage',
        ],
      ],
      [
        'name' => 'quota_network',
        'title' => $this->t('Network'),
        'open' => TRUE,
        'fields' => [
          'network_usage',
          'subnet_usage',
          'port_usage',
          'router_usage',
          'floatingip_usage',
          'security_group_usage',
          'security_group_rule_usage',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL): array {
    $build = parent::view($entity, $view_mode, $langcode);

    $fieldset_defs = $this->getFieldsetDefs();
    foreach ($fieldset_defs ?: [] as $fieldset_def) {
      $fieldset_name = $fieldset_def['name'];
      if (empty($build[$fieldset_name][0]['#openstack_quota'])) {
        continue;
      }
      $tmp_entity = $build[$fieldset_name][0]['#openstack_quota'];
      foreach ($fieldset_def['fields'] ?: [] as $field_name) {
        if (strpos($field_name, '_usage') === FALSE) {
          continue;
        }
        $field_limit = str_replace('_usage', '', $field_name);
        $value = $entity->get($field_limit)->value;
        $tmp_entity->set($field_limit, $value);
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    foreach ($build as $key => $field) {
      if (array_key_exists("{$key}_usage", $build)) {
        $build[$key]['#access'] = FALSE;
      }
    }
  }

}
