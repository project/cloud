<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the OpenStack Role entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_role",
 *   id_plural = "openstack_roles",
 *   label = @Translation("Role"),
 *   label_collection = @Translation("Roles"),
 *   label_singular = @Translation("Role"),
 *   label_plural = @Translation("Roles"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackRoleViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackRoleViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackRoleEditForm",
 *       "add" = "Drupal\openstack\Form\OpenStackRoleCreateForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackRoleEditForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackRoleDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackRoleDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackRoleAccessControlHandler",
 *   },
 *   base_table = "openstack_role",
 *   admin_permission = "administer openstack role",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/role/{openstack_role}",
 *     "add-form" = "/clouds/openstack/{cloud_context}/role/{openstack_role}/add",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/role/{openstack_role}/edit",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/role/{openstack_role}/delete",
 *     "collection" = "/clouds/openstack/{cloud_context}/role",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/role/delete_multiple",
 *   },
 *   field_ui_base_route = "openstack_role.settings"
 * )
 */
class OpenStackRole extends CloudContentEntityBase implements OpenStackRoleInterface {

  /**
   * {@inheritdoc}
   */
  public function getRoleId(): ?string {
    return $this->get('role_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRoleId($role_id = ''): OpenStackRoleInterface {
    return $this->set('role_id', $role_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackRoleInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): OpenStackRoleInterface {
    return $this->set('description', $description);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackRoleInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Role entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Role entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of role.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['role_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Role ID'))
      ->setDescription(t('The ID of the role.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('The description of the role.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Role entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
