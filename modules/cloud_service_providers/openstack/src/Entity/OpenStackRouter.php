<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;

/**
 * Defines the OpenStack router entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_router",
 *   id_plural = "openstack_routers",
 *   label = @Translation("Router"),
 *   label_collection = @Translation("Routers"),
 *   label_singular = @Translation("Router"),
 *   label_plural = @Translation("Routers"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackRouterViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackRouterViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackRouterEditForm",
 *       "add" = "Drupal\openstack\Form\OpenStackRouterCreateForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackRouterEditForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackRouterDeleteForm",
 *       "add_interface" = "Drupal\openstack\Form\OpenStackRouterAddInterfaceForm",
 *       "remove_interface" = "Drupal\openstack\Form\OpenStackRouterRemoveInterfaceForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackRouterDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackRouterAccessControlHandler",
 *   },
 *   base_table = "openstack_router",
 *   admin_permission = "administer openstack router",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/router/{openstack_router}",
 *     "add-form" = "/clouds/openstack/{cloud_context}/router/{openstack_router}/add",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/router/{openstack_router}/edit",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/router/{openstack_router}/delete",
 *     "collection" = "/clouds/openstack/{cloud_context}/router",
 *     "add-interface-form" = "/clouds/openstack/{cloud_context}/router/{openstack_router}/add_interface",
 *     "remove-interface-form" = "/clouds/openstack/{cloud_context}/router/{openstack_router}/remove_interface/{openstack_port}",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/router/delete_multiple",
 *   },
 *   field_ui_base_route = "openstack_router.settings"
 * )
 */
class OpenStackRouter extends CloudContentEntityBase implements OpenStackRouterInterface {

  /**
   * {@inheritdoc}
   */
  public function getRouterId(): ?string {
    return $this->get('router_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRouterId($router_id = ''): OpenStackRouterInterface {
    return $this->set('router_id', $router_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackRouterInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackRouterInterface {
    return $this->set('project_id', $project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status = ''): OpenStackRouterInterface {
    return $this->set('status', $status);
  }

  /**
   * {@inheritdoc}
   */
  public function getAdminStateUp(): bool {
    return $this->get('admin_state_up')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAdminStateUp($admin_state_up): OpenStackRouterInterface {
    return $this->set('admin_state_up', $admin_state_up);
  }

  /**
   * {@inheritdoc}
   */
  public function setAvailabilityZones(array $availability_zones): OpenStackRouterInterface {
    return $this->set('availability_zones', $availability_zones);
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZones(): array {
    return $this->get('availability_zones')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalGatewayNetworkId(): ?string {
    return $this->get('external_gateway_network_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setExternalGatewayNetworkId($external_gateway_network_id = ''): OpenStackRouterInterface {
    return $this->set('external_gateway_network_id', $external_gateway_network_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalGatewayEnableSnat(): ?bool {
    return $this->get('external_gateway_enable_snat')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setExternalGatewayEnableSnat($external_gateway_enable_snat): OpenStackRouterInterface {
    return $this->set('external_gateway_enable_snat', $external_gateway_enable_snat);
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalGatewayExternalFixedIps(): array {
    return $this->get('external_gateway_external_fixed_ips')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setExternalGatewayExternalFixedIps(array $external_gateway_external_fixed_ips): OpenStackRouterInterface {
    return $this->set('external_gateway_external_fixed_ips', $external_gateway_external_fixed_ips);
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalGatewayQosPolicyId(): ?string {
    return $this->get('external_gateway_qos_policy_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setExternalGatewayQosPolicyId($external_gateway_qos_policy_id = ''): OpenStackRouterInterface {
    return $this->set('external_gateway_qos_policy_id', $external_gateway_qos_policy_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getRoutes(): array {
    return $this->get('routes')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setRoutes(array $routes): OpenStackRouterInterface {
    return $this->set('routes', $routes);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackRouterInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Router entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Router entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of router.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['router_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Router ID'))
      ->setDescription(t('The ID of the router.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The ID of project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The router status. Values are ACTIVE, DOWN, BUILD and ERROR.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['admin_state_up'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Admin state'))
      ->setDescription(t('The administrative state of the resource, which is up (true) or down (false).'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('up'),
        'off_label' => t('down'),
      ])
      ->setReadOnly(TRUE);

    $fields['availability_zones'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Availability Zones'))
      ->setDescription(t('The Availability Zones for the router.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['external_gateway_network_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Network ID'))
      ->setDescription(t('The network ID of the external gateway.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_network',
          'field_name' => 'network_id',
          'comma_separated' => TRUE,
          'html_generator_class' => EntityLinkWithNameHtmlGenerator::class,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['external_gateway_enable_snat'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('SNAT'))
      ->setDescription(t('SNAT of the external gateway.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('enabled'),
        'off_label' => t('disabled'),
      ])
      ->setReadOnly(TRUE);

    $fields['external_gateway_external_fixed_ips'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('External fixed IPs'))
      ->setDescription(t('The IP addresses for the external gateway.'))
      ->setDisplayOptions('view', [
        'type' => 'openstack_fixed_ips_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['external_gateway_qos_policy_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('QOS Policy ID'))
      ->setDescription(t('The QOS policy ID of the external gateway.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['routes'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Static Routes'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'settings' => [
          'key_label' => t('Destination CIDR'),
          'value_label' => t('Next Hop'),
        ],
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'key_value_item',
        'settings' => [
          'key_label' => t('Destination CIDR'),
          'value_label' => t('Next Hop'),
        ],
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Router entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
