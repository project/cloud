<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackRouter entity.
 *
 * @ingroup openstack
 */
interface OpenStackRouterInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getRouterId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setRouterId($router_id = ''): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status = ''): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function getAdminStateUp(): bool;

  /**
   * {@inheritdoc}
   */
  public function setAdminStateUp($admin_state_up): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function setAvailabilityZones(array $availability_zones): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZones(): array;

  /**
   * {@inheritdoc}
   */
  public function getExternalGatewayNetworkId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setExternalGatewayNetworkId($external_gateway_network_id = ''): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function getExternalGatewayEnableSnat(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function setExternalGatewayEnableSnat($external_gateway_enable_snat): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function getExternalGatewayExternalFixedIps(): array;

  /**
   * {@inheritdoc}
   */
  public function setExternalGatewayExternalFixedIps(array $external_gateway_external_fixed_ips): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function getExternalGatewayQosPolicyId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setExternalGatewayQosPolicyId($external_gateway_qos_policy_id = ''): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function getRoutes(): array;

  /**
   * {@inheritdoc}
   */
  public function setRoutes(array $routes): OpenStackRouterInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackRouterInterface;

}
