<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the router view builders.
 */
class OpenStackRouterViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'router',
        'title' => $this->t('Router'),
        'open' => TRUE,
        'fields' => [
          'name',
          'router_id',
          'project_id',
          'status',
          'admin_state_up',
          'availability_zones',
          'routes',
          'created',
        ],
      ],
      [
        'name' => 'external_gateway',
        'title' => $this->t('External Gateway'),
        'open' => TRUE,
        'fields' => [
          'external_gateway_network_id',
          'external_gateway_enable_snat',
          'external_gateway_qos_policy_id',
          'external_gateway_external_fixed_ips',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
