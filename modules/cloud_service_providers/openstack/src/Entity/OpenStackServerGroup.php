<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the OpenStack server group entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_server_group",
 *   id_plural = "openstack_server_groups",
 *   label = @Translation("Server group"),
 *   label_collection = @Translation("Server groups"),
 *   label_singular = @Translation("Server group"),
 *   label_plural = @Translation("Server groups"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackServerGroupViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackServerGroupViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackServerGroupCreateForm",
 *       "add" = "Drupal\openstack\Form\OpenStackServerGroupCreateForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackServerGroupDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackServerGroupDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackServerGroupAccessControlHandler",
 *   },
 *   base_table = "openstack_server_group",
 *   admin_permission = "administer openstack server group",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/server_group/{openstack_server_group}",
 *     "add-form" = "/clouds/openstack/{cloud_context}/server_group/{openstack_server_group}/add",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/server_group/{openstack_server_group}/delete",
 *     "collection" = "/clouds/openstack/{cloud_context}/server_group",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/server_group/delete_multiple",
 *   },
 *   field_ui_base_route = "openstack_server_group.settings"
 * )
 */
class OpenStackServerGroup extends CloudContentEntityBase implements OpenStackServerGroupInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackServerGroupInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId(string $project_id = ''): OpenStackServerGroupInterface {
    return $this->set('project_id', $project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getServerGroupId(): ?string {
    return $this->get('server_group_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setServerGroupId(string $server_group_id = ''): OpenStackServerGroupInterface {
    return $this->set('server_group_id', $server_group_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getPolicy(): ?string {
    return $this->get('policy')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPolicy(string $policy): OpenStackServerGroupInterface {
    return $this->set('policy', $policy);
  }

  /**
   * {@inheritdoc}
   */
  public function getMembers(): array {
    return $this->get('members')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setMembers(array $members): OpenStackServerGroupInterface {
    return $this->set('members', $members);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackServerGroupInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Server group entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Server group entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['server_group_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Server group ID'))
      ->setDescription(t('The ID of the server group.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of server group.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The ID of project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['policy'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Policy'))
      ->setDescription(t('The policy field represents the name of the policy.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['members'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Members'))
      ->setDescription(t('A list of members in the server group.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'server_group_members_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Server group entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
