<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackServerGroup entity.
 *
 * @ingroup openstack
 */
interface OpenStackServerGroupInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackServerGroupInterface;

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setProjectId(string $project_id = ''): OpenStackServerGroupInterface;

  /**
   * {@inheritdoc}
   */
  public function getServerGroupId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setServerGroupId(string $server_group_id = ''): OpenStackServerGroupInterface;

  /**
   * {@inheritdoc}
   */
  public function getPolicy(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setPolicy(string $policy): OpenStackServerGroupInterface;

  /**
   * {@inheritdoc}
   */
  public function getMembers(): array;

  /**
   * {@inheritdoc}
   */
  public function setMembers(array $members): OpenStackServerGroupInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackServerGroupInterface;

}
