<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the OpenStack stack entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_stack",
 *   id_plural = "openstack_stacks",
 *   label = @Translation("Stack"),
 *   label_collection = @Translation("Stacks"),
 *   label_singular = @Translation("Stack"),
 *   label_plural = @Translation("Stacks"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackStackViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackStackViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackStackEditForm",
 *       "add" = "Drupal\openstack\Form\OpenStackStackCreateForm",
 *       "add-extra" = "Drupal\openstack\Form\OpenStackStackCreateExtraForm",
 *       "preview" = "Drupal\openstack\Form\OpenStackStackPreviewForm",
 *       "preview-extra" = "Drupal\openstack\Form\OpenStackStackPreviewExtraForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackStackEditForm",
 *       "edit-extra" = "Drupal\openstack\Form\OpenStackStackEditExtraForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackStackDeleteForm",
 *       "check" = "Drupal\openstack\Form\OpenStackStackCheckForm",
 *       "suspend" = "Drupal\openstack\Form\OpenStackStackSuspendForm",
 *       "resume" = "Drupal\openstack\Form\OpenStackStackResumeForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackStackDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackStackAccessControlHandler",
 *   },
 *   base_table = "openstack_stack",
 *   admin_permission = "administer openstack stack",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}",
 *     "add-form" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/add",
 *     "add-extra-form" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/add_extra",
 *     "preview-form" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/preview",
 *     "preview-extra-form" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/preview_extra",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/edit",
 *     "edit-extra-form" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/edit_extra",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/delete",
 *     "collection" = "/clouds/openstack/{cloud_context}/stack",
 *     "check-form" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/check",
 *     "suspend-form" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/suspend",
 *     "resume-form" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/resume",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/stack/delete_multiple",
 *   },
 *   field_ui_base_route = "openstack_stack.settings"
 * )
 */
class OpenStackStack extends CloudContentEntityBase implements OpenStackStackInterface {

  /**
   * {@inheritdoc}
   */
  public function getStackId(): ?string {
    return $this->get('stack_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackId($stack_id = ''): OpenStackStackInterface {
    return $this->set('stack_id', $stack_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackStackInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): OpenStackStackInterface {
    return $this->set('description', $description);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackStackInterface {
    return $this->set('project_id', $project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getStackStatus(): ?string {
    return $this->get('stack_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackStatus($stack_status = ''): OpenStackStackInterface {
    return $this->set('stack_status', $stack_status);
  }

  /**
   * {@inheritdoc}
   */
  public function getStackStatusReason(): ?string {
    return $this->get('stack_status_reason')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackStatusReason($stack_status_reason = ''): OpenStackStackInterface {
    return $this->set('stack_status_reason', $stack_status_reason);
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeoutMins(): ?int {
    return $this->get('timeout_mins')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeoutMins($timeout_mins): OpenStackStackInterface {
    return $this->set('timeout_mins', $timeout_mins);
  }

  /**
   * {@inheritdoc}
   */
  public function getRollback(): ?bool {
    return $this->get('rollback')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRollback($rollback): OpenStackStackInterface {
    return $this->set('rollback', $rollback);
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters(): array {
    return $this->get('parameters')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setParameters($parameters): OpenStackStackInterface {
    return $this->set('parameters', $parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function getOutputs(): array {
    return $this->get('outputs')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setOutputs($outputs): OpenStackStackInterface {
    return $this->set('outputs', $outputs);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplate(): ?string {
    return $this->get('template')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTemplate($template = ''): OpenStackStackInterface {
    return $this->set('template', $template);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackStackInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Stack entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Stack entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of stack.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('A user-defined description associated with the stack.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stack_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stack ID'))
      ->setDescription(t('The ID of the stack.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The ID of project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stack_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stack Status'))
      ->setDescription(t('The status of the stack.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stack_status_reason'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stack Status Reason'))
      ->setDescription(t('The reason for the current status of the stack.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['timeout_mins'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Timout(minutes)'))
      ->setDescription(t('The timeout for stack creation in minutes.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['rollback'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Rollback'))
      ->setDescription(t('Whether deletion of all stack resources when stack creation fails.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('enabled'),
        'off_label' => t('disabled'),
      ])
      ->setReadOnly(TRUE);

    $fields['outputs'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Outputs'))
      ->setDescription(t('Stack outputs.'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'settings' => [
          'key_label' => t('Name'),
          'value_label' => t('Value'),
        ],
        'weight' => -5,
      ]);

    $fields['parameters'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Stack Parameters'))
      ->setDescription(t('A group of key-value pairs where each key contains either a user-provided parameter name or a built-in parameter name (e.g. OS::project_id).'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'settings' => [
          'key_label' => t('Name'),
          'value_label' => t('Value'),
        ],
        'weight' => -5,
      ]);

    $fields['template'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Template'))
      ->setDescription(t('Stack template.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'pre_string_formatter',
        'weight' => -5,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Stack entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
