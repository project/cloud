<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the OpenStack stack event entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_stack_event",
 *   id_plural = "openstack_stack_events",
 *   label = @Translation("Stack Event"),
 *   label_collection = @Translation("Stack Events"),
 *   label_singular = @Translation("Stack Event"),
 *   label_plural = @Translation("Stack Events"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackStackEventViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackStackEventViewsData",
 *
 *     "form" = {
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackStackEventAccessControlHandler",
 *   },
 *   base_table = "openstack_stack_event",
 *   admin_permission = "administer openstack stack event",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/event/{openstack_stack_event}",
 *     "collection" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/event",
 *   },
 *   field_ui_base_route = "openstack_stack_event.settings"
 * )
 */
class OpenStackStackEvent extends CloudContentEntityBase implements OpenStackStackEventInterface {

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    // Add openstack stack entity ID.
    $uri_route_parameters['openstack_stack'] = $this->getStackEntityId();

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackStackEventInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackStackEventInterface {
    return $this->set('project_id', $project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getStackEntityId(): int {
    return $this->get('stack_entity_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackEntityId($stack_entity_id): OpenStackStackEventInterface {
    return $this->set('stack_entity_id', $stack_entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getStackId(): ?string {
    return $this->get('stack_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackId($stack_id = ''): OpenStackStackEventInterface {
    return $this->set('stack_id', $stack_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getEventId(): ?string {
    return $this->get('event_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEventId($event_id = ''): OpenStackStackEventInterface {
    return $this->set('event_id', $event_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceName(): ?string {
    return $this->get('resource_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceName($resource_name = ''): OpenStackStackEventInterface {
    return $this->set('resource_name', $resource_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceId(): ?string {
    return $this->get('resource_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceId($resource_id = ''): OpenStackStackEventInterface {
    return $this->set('resource_id', $resource_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceStatus(): ?string {
    return $this->get('resource_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceStatus($resource_status = ''): OpenStackStackEventInterface {
    return $this->set('resource_status', $resource_status);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceStatusReason(): ?string {
    return $this->get('resource_status_reason')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceStatusReason($resource_status_reason = ''): OpenStackStackEventInterface {
    return $this->set('resource_status_reason', $resource_status_reason);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackStackEventInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Quota entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Quota entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of event.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The ID of project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stack_entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Stack Entity ID'))
      ->setDescription(t('The ID of the stack entity.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stack_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stack ID'))
      ->setDescription(t('The ID of the stack.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['event_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Event ID'))
      ->setDescription(t('The ID of the stack physical event.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['resource_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Resource name'))
      ->setDescription(t('The name of resource.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['resource_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Resource ID'))
      ->setDescription(t('The ID of the stack physical resource.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['resource_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of the resource.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['resource_status_reason'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status Reason'))
      ->setDescription(t('The reason for the stack resource state.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Quota entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
