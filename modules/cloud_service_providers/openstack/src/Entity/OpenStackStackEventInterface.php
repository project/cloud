<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackStackEvent entity.
 *
 * @ingroup openstack
 */
interface OpenStackStackEventInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackStackEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackStackEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getStackEntityId(): int;

  /**
   * {@inheritdoc}
   */
  public function setStackEntityId($stack_entity_id): OpenStackStackEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getStackId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStackId($stack_id = ''): OpenStackStackEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getEventId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setEventId($event_id = ''): OpenStackStackEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getResourceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setResourceId($resource_id = ''): OpenStackStackEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getResourceStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setResourceStatus($resource_status = ''): OpenStackStackEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getResourceStatusReason(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setResourceStatusReason($resource_status_reason = ''): OpenStackStackEventInterface;

  /**
   * {@inheritdoc}
   */
  public function getResourceName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setResourceName($resource_name = ''): OpenStackStackEventInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackStackEventInterface;

}
