<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the stack event view builders.
 */
class OpenStackStackEventViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'stack_event',
        'title' => $this->t('Stack Event'),
        'open' => TRUE,
        'fields' => [
          'name',
          'stack_id',
          'project_id',
          'resource_name',
          'resource_id',
          'resource_status',
          'resource_status_reason',
          'created',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
