<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackStack entity.
 *
 * @ingroup openstack
 */
interface OpenStackStackInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getStackId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStackId($stack_id = ''): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function getStackStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStackStatus($stack_status = ''): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function getStackStatusReason(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStackStatusReason($stack_status_reason = ''): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function getTimeoutMins(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setTimeoutMins($timeout_mins): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function getRollback(): ?bool;

  /**
   * {@inheritdoc}
   */
  public function setRollback($rollback): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function getParameters(): array;

  /**
   * {@inheritdoc}
   */
  public function setParameters($parameters): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function getOutputs(): array;

  /**
   * {@inheritdoc}
   */
  public function setOutputs($outputs): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function getTemplate(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setTemplate($template = ''): OpenStackStackInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackStackInterface;

}
