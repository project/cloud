<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the OpenStack stack resource entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_stack_resource",
 *   id_plural = "openstack_stack_resources",
 *   label = @Translation("Stack Resource"),
 *   label_collection = @Translation("Stack Resources"),
 *   label_singular = @Translation("Stack Resource"),
 *   label_plural = @Translation("Stack Resources"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackStackResourceViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackStackResourceViewsData",
 *
 *     "form" = {
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackStackResourceAccessControlHandler",
 *   },
 *   base_table = "openstack_stack_resource",
 *   admin_permission = "administer openstack stack resource",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/resource/{openstack_stack_resource}",
 *     "collection" = "/clouds/openstack/{cloud_context}/stack/{openstack_stack}/resource",
 *   },
 *   field_ui_base_route = "openstack_stack_resource.settings"
 * )
 */
class OpenStackStackResource extends CloudContentEntityBase implements OpenStackStackResourceInterface {

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    // Add openstack stack entity ID.
    $uri_route_parameters['openstack_stack'] = $this->getStackEntityId();

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackStackResourceInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackStackResourceInterface {
    return $this->set('project_id', $project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getStackEntityId(): int {
    return $this->get('stack_entity_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackEntityId($stack_entity_id): OpenStackStackResourceInterface {
    return $this->set('stack_entity_id', $stack_entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getStackId(): ?string {
    return $this->get('stack_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackId($stack_id = ''): OpenStackStackResourceInterface {
    return $this->set('stack_id', $stack_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceId(): ?string {
    return $this->get('resource_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceId($resource_id = ''): OpenStackStackResourceInterface {
    return $this->set('resource_id', $resource_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceStatus(): ?string {
    return $this->get('resource_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceStatus($resource_status = ''): OpenStackStackResourceInterface {
    return $this->set('resource_status', $resource_status);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceStatusReason(): ?string {
    return $this->get('resource_status_reason')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceStatusReason($resource_status_reason = ''): OpenStackStackResourceInterface {
    return $this->set('resource_status_reason', $resource_status_reason);
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceType(): ?string {
    return $this->get('resource_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceType($resource_type = ''): OpenStackStackResourceInterface {
    return $this->set('resource_type', $resource_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackStackResourceInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Quota entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Quota entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of resource.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The ID of project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stack_entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Stack Entity ID'))
      ->setDescription(t('The ID of the stack entity.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['stack_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stack ID'))
      ->setDescription(t('The ID of the stack.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['resource_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Resource ID'))
      ->setDescription(t('The ID of the stack physical resource.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['resource_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of the resource.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['resource_status_reason'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status Reason'))
      ->setDescription(t('The reason for the current stack resource state.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['resource_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Resource Type'))
      ->setDescription(t('The resource type.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Date Updated'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Quota entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
