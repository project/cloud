<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackStackResource entity.
 *
 * @ingroup openstack
 */
interface OpenStackStackResourceInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackStackResourceInterface;

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackStackResourceInterface;

  /**
   * {@inheritdoc}
   */
  public function getStackEntityId(): int;

  /**
   * {@inheritdoc}
   */
  public function setStackEntityId($stack_entity_id): OpenStackStackResourceInterface;

  /**
   * {@inheritdoc}
   */
  public function getStackId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setStackId($stack_id = ''): OpenStackStackResourceInterface;

  /**
   * {@inheritdoc}
   */
  public function getResourceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setResourceId($resource_id = ''): OpenStackStackResourceInterface;

  /**
   * {@inheritdoc}
   */
  public function getResourceStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setResourceStatus($resource_status = ''): OpenStackStackResourceInterface;

  /**
   * {@inheritdoc}
   */
  public function getResourceStatusReason(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setResourceStatusReason($resource_status_reason = ''): OpenStackStackResourceInterface;

  /**
   * {@inheritdoc}
   */
  public function getResourceType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setResourceType($resource_type = ''): OpenStackStackResourceInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackStackResourceInterface;

}
