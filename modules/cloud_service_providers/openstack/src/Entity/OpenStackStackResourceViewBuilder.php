<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the stack resource view builders.
 */
class OpenStackStackResourceViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'stack_resource',
        'title' => $this->t('Stack Resource'),
        'open' => TRUE,
        'fields' => [
          'name',
          'stack_id',
          'project_id',
          'resource_id',
          'resource_type',
          'resource_status',
          'resource_status_reason',
          'created',
          'refreshed',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
