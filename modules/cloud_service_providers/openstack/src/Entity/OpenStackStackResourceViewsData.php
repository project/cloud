<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewsData;

/**
 * Provides the views data for the Stack Resource entity type.
 */
class OpenStackStackResourceViewsData extends AwsCloudViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $table_name = $this->storage->getEntityTypeId();
    $fields = $this->getFieldStorageDefinitions();

    // The following is a list of fields to turn from text search to
    // select list.  This list can be expanded through hook_views_data_alter().
    $selectable = [];

    // Add an access query tag.
    $data[$table_name]['table']['base']['access query tag'] = 'openstack_stack_resource_views_access';

    $this->addDropdownSelector($data, $table_name, $fields, $selectable);

    return $data;
  }

}
