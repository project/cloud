<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the stack view builders.
 */
class OpenStackStackViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'stack',
        'title' => $this->t('Stack'),
        'open' => TRUE,
        'fields' => [
          'name',
          'stack_id',
          'project_id',
          'stack_status',
          'stack_status_reason',
          'timeout_mins',
          'rollback',
          'created',
          'refreshed',
        ],
      ],
      [
        'name' => 'stack_outputs',
        'title' => $this->t('Outputs'),
        'open' => TRUE,
        'fields' => [
          'outputs',
        ],
      ],
      [
        'name' => 'stack_parameters',
        'title' => $this->t('Parameters'),
        'open' => TRUE,
        'fields' => [
          'parameters',
        ],
      ],
      [
        'name' => 'stack_template',
        'title' => $this->t('Template'),
        'open' => TRUE,
        'fields' => [
          'template',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
