<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;

/**
 * Defines the OpenStack subnet entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_subnet",
 *   id_plural = "openstack_subnets",
 *   label = @Translation("Subnet"),
 *   label_collection = @Translation("Subnets"),
 *   label_singular = @Translation("Subnet"),
 *   label_plural = @Translation("Subnets"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackSubnetViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackSubnetViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackSubnetEditForm",
 *       "add" = "Drupal\openstack\Form\OpenStackSubnetCreateForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackSubnetEditForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackSubnetDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackSubnetDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackSubnetAccessControlHandler",
 *   },
 *   base_table = "openstack_subnet",
 *   admin_permission = "administer openstack subnet",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/subnet/{openstack_subnet}",
 *     "add-form" = "/clouds/openstack/{cloud_context}/subnet/{openstack_subnet}/add",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/subnet/{openstack_subnet}/edit",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/subnet/{openstack_subnet}/delete",
 *     "collection" = "/clouds/openstack/{cloud_context}/subnet",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/subnet/delete_multiple",
 *   },
 *   field_ui_base_route = "openstack_subnet.settings"
 * )
 */
class OpenStackSubnet extends CloudContentEntityBase implements OpenStackSubnetInterface {

  /**
   * {@inheritdoc}
   */
  public function getSubnetId(): ?string {
    return $this->get('subnet_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubnetId($subnet_id = ''): OpenStackSubnetInterface {
    return $this->set('subnet_id', $subnet_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackSubnetInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackSubnetInterface {
    return $this->set('project_id', $project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkId(): ?string {
    return $this->get('network_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNetworkId($network_id = ''): OpenStackSubnetInterface {
    return $this->set('network_id', $network_id);
  }

  /**
   * {@inheritdoc}
   */
  public function setSubnetPoolId($subnet_pool_id = ''): OpenStackSubnetInterface {
    return $this->set('subnet_pool_id', $subnet_pool_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubnetPoolId(): ?string {
    return $this->get('subnet_pool_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setIpVersion($ip_version = ''): OpenStackSubnetInterface {
    return $this->set('ip_version', $ip_version);
  }

  /**
   * {@inheritdoc}
   */
  public function getIpVersion(): ?string {
    return $this->get('ip_version')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCidr($cidr = ''): OpenStackSubnetInterface {
    return $this->set('cidr', $cidr);
  }

  /**
   * {@inheritdoc}
   */
  public function getCidr(): ?string {
    return $this->get('cidr')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAllocationPools(array $allocation_pools): OpenStackSubnetInterface {
    return $this->set('allocation_pools', $allocation_pools);
  }

  /**
   * {@inheritdoc}
   */
  public function getAllocationPools(): array {
    return $this->get('allocation_pools')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setGatewayIp($gateway_ip = ''): OpenStackSubnetInterface {
    return $this->set('gateway_ip', $gateway_ip);
  }

  /**
   * {@inheritdoc}
   */
  public function getGatewayIp(): ?string {
    return $this->get('gateway_ip')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnableDhcp(bool $enable_dhcp): OpenStackSubnetInterface {
    return $this->set('enable_dhcp', $enable_dhcp);
  }

  /**
   * {@inheritdoc}
   */
  public function getEnableDhcp(): bool {
    return $this->get('enable_dhcp')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHostRoutes(array $host_routes): OpenStackSubnetInterface {
    return $this->set('host_routes', $host_routes);
  }

  /**
   * {@inheritdoc}
   */
  public function getHostRoutes(): array {
    return $this->get('host_routes')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setDnsNameServers(array $dns_name_servers): OpenStackSubnetInterface {
    return $this->set('dns_name_servers', $dns_name_servers);
  }

  /**
   * {@inheritdoc}
   */
  public function getDnsNameServers(): array {
    return $this->get('dns_name_servers')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackSubnetInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Subnet entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Subnet entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of subnet.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['subnet_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subnet ID'))
      ->setDescription(t('The ID of the subnet.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('The ID of project.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['network_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Network'))
      ->setDescription(t('The network.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_network',
          'field_name' => 'network_id',
          'comma_separated' => TRUE,
          'html_generator_class' => EntityLinkWithNameHtmlGenerator::class,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['subnet_pool_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subnet Pool ID'))
      ->setDescription(t('The ID of the subnet pool.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['ip_version'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IP version'))
      ->setDescription(t('The IP version.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['cidr'] = BaseFieldDefinition::create('string')
      ->setLabel(t('CIDR'))
      ->setDescription(t('The CIDR of the subnet.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['allocation_pools'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Allocation pools'))
      ->setDescription(t('Allocation pools with start and end IP addresses for this subnet.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['gateway_ip'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Gateway IP'))
      ->setDescription(t('Gateway IP of this subnet. If the value is null that implies no gateway is associated with the subnet.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['enable_dhcp'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable DHCP'))
      ->setDescription(t('Indicates whether dhcp is enabled or disabled for the subnet.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('yes'),
        'off_label' => t('no'),
      ])
      ->setReadOnly(TRUE);

    $fields['host_routes'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Host Routes'))
      ->setDescription(t('Additional routes for the subnet. A list of dictionaries with destination and nexthop parameters.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['dns_name_servers'] = BaseFieldDefinition::create('string')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('DNS name servers'))
      ->setDescription(t('List of dns name servers associated with the subnet.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Subnet entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
