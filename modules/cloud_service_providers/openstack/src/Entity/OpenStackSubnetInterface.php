<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackSubnet entity.
 *
 * @ingroup openstack
 */
interface OpenStackSubnetInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getSubnetId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setSubnetId($subnet_id = ''): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setProjectId($project_id = ''): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getNetworkId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setNetworkId($network_id = ''): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function setSubnetPoolId($subnet_pool_id = ''): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getSubnetPoolId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setIpVersion($ip_version = ''): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getIpVersion(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setCidr($cidr = ''): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getCidr(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setAllocationPools(array $allocation_pools): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getAllocationPools(): array;

  /**
   * {@inheritdoc}
   */
  public function setGatewayIp($gateway_ip = ''): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getGatewayIp(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setEnableDhcp(bool $enable_dhcp): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getEnableDhcp(): bool;

  /**
   * {@inheritdoc}
   */
  public function setHostRoutes(array $host_routes): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getHostRoutes(): array;

  /**
   * {@inheritdoc}
   */
  public function setDnsNameServers(array $dns_name_servers): OpenStackSubnetInterface;

  /**
   * {@inheritdoc}
   */
  public function getDnsNameServers(): array;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackSubnetInterface;

}
