<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the subnet view builders.
 */
class OpenStackSubnetViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'subnet',
        'title' => $this->t('Subnet'),
        'open' => TRUE,
        'fields' => [
          'name',
          'subnet_id',
          'project_id',
          'network_id',
          'subnet_pool_id',
          'ip_version',
          'cidr',
          'allocation_pools',
          'gateway_ip',
          'enable_dhcp',
          'host_routes',
          'dns_name_servers',
          'created',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
