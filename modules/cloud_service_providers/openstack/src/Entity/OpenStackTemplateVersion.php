<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Defines the OpenStackTemplateVersion entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_template_version",
 *   id_plural = "openstack_template_versions",
 *   label = @Translation("Template version"),
 *   label_collection = @Translation("Template versions"),
 *   label_singular = @Translation("Template version"),
 *   label_plural = @Translation("Template versions"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackTemplateVersionViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackTemplateVersionViewsData",
 *     "form" = {
 *     },
 *     "access"       = "Drupal\openstack\Controller\OpenStackTemplateVersionAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "openstack_template_version",
 *   admin_permission = "administer openstack template version",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/template_version/{openstack_template_version}",
 *     "collection" = "/clouds/openstack/{cloud_context}/template_version",
 *   },
 *   field_ui_base_route = "openstack_template_version.settings"
 * )
 */
class OpenStackTemplateVersion extends CloudContentEntityBase implements OpenStackTemplateVersionInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackTemplateVersionInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): ?string {
    return $this->get('type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setType($type = ''): OpenStackTemplateVersionInterface {
    return $this->set('type', $type);
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return $this->get('functions')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setFunctions(array $functions): OpenStackTemplateVersionInterface {
    return $this->set('functions', $functions);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackTemplateVersionInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the template version entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the template version entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of template version.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('The type of template version.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['functions'] = BaseFieldDefinition::create('key_value')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Functions'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'settings' => [
          'key_label' => t('Function'),
          'value_label' => t('Description'),
        ],
        'weight' => -5,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the template version entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
