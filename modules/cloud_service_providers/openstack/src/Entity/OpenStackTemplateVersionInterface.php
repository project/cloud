<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackTemplateVersion entity.
 *
 * @ingroup openstack
 */
interface OpenStackTemplateVersionInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackTemplateVersionInterface;

  /**
   * {@inheritdoc}
   */
  public function getType(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setType($type = ''): OpenStackTemplateVersionInterface;

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array;

  /**
   * {@inheritdoc}
   */
  public function setFunctions(array $functions): OpenStackTemplateVersionInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackTemplateVersionInterface;

}
