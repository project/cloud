<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;

/**
 * Defines the OpenStack User entity.
 *
 * @ingroup openstack
 *
 * @ContentEntityType(
 *   id = "openstack_user",
 *   id_plural = "openstack_users",
 *   label = @Translation("User"),
 *   label_collection = @Translation("Users"),
 *   label_singular = @Translation("User"),
 *   label_plural = @Translation("Users"),
 *   handlers = {
 *     "view_builder" = "Drupal\openstack\Entity\OpenStackUserViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\openstack\Entity\OpenStackUserViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack\Form\OpenStackUserEditForm",
 *       "add" = "Drupal\openstack\Form\OpenStackUserCreateForm",
 *       "edit" = "Drupal\openstack\Form\OpenStackUserEditForm",
 *       "delete" = "Drupal\openstack\Form\OpenStackUserDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\openstack\Form\OpenStackUserDeleteMultipleForm",
 *       "change-password" = "Drupal\openstack\Form\OpenStackUserChangePasswordForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack\Controller\OpenStackUserAccessControlHandler",
 *   },
 *   base_table = "openstack_user",
 *   admin_permission = "administer openstack user",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/clouds/openstack/{cloud_context}/user/{openstack_user}",
 *     "add-form" = "/clouds/openstack/{cloud_context}/user/{openstack_user}/add",
 *     "edit-form" = "/clouds/openstack/{cloud_context}/user/{openstack_user}/edit",
 *     "delete-form" = "/clouds/openstack/{cloud_context}/user/{openstack_user}/delete",
 *     "collection" = "/clouds/openstack/{cloud_context}/user",
 *     "delete-multiple-form" = "/clouds/openstack/{cloud_context}/user/delete_multiple",
 *     "change-password-form" = "/clouds/openstack/{cloud_context}/user/{openstack_user}/change_password",
 *   },
 *   field_ui_base_route = "openstack_user.settings"
 * )
 */
class OpenStackUser extends CloudContentEntityBase implements OpenStackUserInterface {

  /**
   * {@inheritdoc}
   */
  public function getUserId(): ?string {
    return $this->get('user_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserId($user_id = ''): OpenStackUserInterface {
    return $this->set('user_id', $user_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackUserInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): OpenStackUserInterface {
    return $this->set('description', $description);
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail(): ?string {
    return $this->get('email')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($email = ''): OpenStackUserInterface {
    return $this->set('email', $email);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultProjectId(): ?string {
    return $this->get('default_project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultProjectId($default_project_id = ''): OpenStackUserInterface {
    return $this->set('default_project_id', $default_project_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getDomainId(): ?string {
    return $this->get('domain_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDomainId($domain_id = ''): OpenStackUserInterface {
    return $this->set('domain_id', $domain_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabled(): bool {
    return $this->get('enabled')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled($enabled): OpenStackUserInterface {
    return $this->set('enabled', $enabled);
  }

  /**
   * {@inheritdoc}
   */
  public function getPasswordExpiresAt(): int {
    return $this->get('password_expires_at')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPasswordExpiresAt($password_expires_at): OpenStackUserInterface {
    return $this->set('password_expires_at', $password_expires_at);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int {
    return $this->get('refreshed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackUserInterface {
    return $this->set('refreshed', $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the User entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the User entity.'))
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of user.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('User ID'))
      ->setDescription(t('The ID of the user.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('The description of the user.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['email'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Email'))
      ->setDescription(t('The email of the user.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['default_project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Default Project ID'))
      ->setDescription(t('The ID of the default project for the user.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_link',
        'settings' => [
          'target_type' => 'openstack_project',
          'field_name' => 'project_id',
          'html_generator_class' => EntityLinkWithNameHtmlGenerator::class,
        ],
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['domain_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Domain ID'))
      ->setDescription(t('The ID of the domain.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['enabled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setDescription(t('If the user is enabled, this value is true. If the user is disabled, this value is false.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setSettings([
        'on_label' => t('yes'),
        'off_label' => t('no'),
      ])
      ->setReadOnly(TRUE);

    $fields['password_expires_at'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Password Expires At'))
      ->setDescription(t('The date and time when the password expires.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['refreshed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Refreshed'))
      ->setDescription(t('The time that the entity was last refreshed.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the User entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
