<?php

namespace Drupal\openstack\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an OpenStackUser entity.
 *
 * @ingroup openstack
 */
interface OpenStackUserInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getUserId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setUserId($user_id = ''): OpenStackUserInterface;

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): OpenStackUserInterface;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = ''): OpenStackUserInterface;

  /**
   * {@inheritdoc}
   */
  public function getEmail(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setEmail($email = ''): OpenStackUserInterface;

  /**
   * {@inheritdoc}
   */
  public function getDefaultProjectId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDefaultProjectId($default_project_id = ''): OpenStackUserInterface;

  /**
   * {@inheritdoc}
   */
  public function getDomainId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setDomainId($domain_id = ''): OpenStackUserInterface;

  /**
   * {@inheritdoc}
   */
  public function getEnabled(): bool;

  /**
   * {@inheritdoc}
   */
  public function setEnabled($enabled): OpenStackUserInterface;

  /**
   * {@inheritdoc}
   */
  public function getPasswordExpiresAt(): int;

  /**
   * {@inheritdoc}
   */
  public function setPasswordExpiresAt($password_expires_at): OpenStackUserInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): int;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): OpenStackUserInterface;

}
