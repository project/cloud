<?php

namespace Drupal\openstack\Entity;

use Drupal\aws_cloud\Entity\Ec2\AwsCloudViewBuilder;

/**
 * Provides the user view builders.
 */
class OpenStackUserViewBuilder extends AwsCloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'user',
        'title' => $this->t('User'),
        'open' => TRUE,
        'fields' => [
          'name',
          'user_id',
          'description',
          'email',
          'default_project_id',
          'domain_id',
          'enabled',
          'password_expires_at',
          'created',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
