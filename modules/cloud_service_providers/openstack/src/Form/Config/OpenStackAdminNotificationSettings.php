<?php

namespace Drupal\openstack\Form\Config;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OpenStack admin notification settings.
 */
class OpenStackAdminNotificationSettings extends ConfigFormBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a OpenStackAdminNotificationSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    ModuleHandlerInterface $module_handler,
    TypedConfigManagerInterface $typed_config_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->fileSystem = $file_system;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('module_handler'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'openstack_admin_notification_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['openstack.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('openstack.settings');

    $form['quota'] = [
      '#type' => 'details',
      '#title' => $this->t('Quota'),
      '#open' => TRUE,
    ];

    $form['quota']['email_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Email Settings'),
      '#open' => TRUE,
    ];

    $form['quota']['email_settings']['openstack_quota_notification_request_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email addresses'),
      '#description' => $this->t('Email addresses to be notified for an approval request. The email addresses can be comma separated.'),
      '#default_value' => $config->get('openstack_quota_notification_request_emails'),
    ];

    $form['quota']['email_settings']['notification_request'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Notification of request'),
    ];

    $form['quota']['email_settings']['notification_request']['openstack_quota_notification_request_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject of the quota request.'),
      '#default_value' => $config->get('openstack_quota_notification_request_subject'),
    ];

    $form['quota']['email_settings']['notification_request']['openstack_quota_notification_request_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#description' => $this->t('Available tokens are: [openstack_quota_request_email:quotas_request], [openstack_quota_request_email:site_url].  The [openstack_quota_request_email:quotas_request] variable can be configured in the quota request information below.'),
      '#default_value' => $config->get('openstack_quota_notification_request_msg'),
    ];

    $form['quota']['email_settings']['notification_request']['openstack_quota_notification_quota_request_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Quota request information'),
      '#default_value' => $config->get('openstack_quota_notification_quota_request_info'),
      '#description' => $this->t('Available tokens are: [openstack_quota_request:name], [openstack_quota_request:quota_link], [openstack_quota_request:quota_link_edit], [openstack_quota_request:quota_button_approve], [openstack_quota_request:changed].'),
    ];

    $form['quota']['email_settings']['notification_approved'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Notification of approval'),
    ];

    $form['quota']['email_settings']['notification_approved']['openstack_quota_notification_approved_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#description' => $this->t('Edit the email subject of the quota approval.'),
      '#default_value' => $config->get('openstack_quota_notification_approved_subject'),
    ];

    $form['quota']['email_settings']['notification_approved']['openstack_quota_notification_approved_msg'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email message'),
      '#description' => $this->t('Available tokens are: [openstack_quota_email:quotas], [site:url].  The [openstack_quota_email:quotas] variable can be configured in the quota information below.'),
      '#default_value' => $config->get('openstack_quota_notification_approved_msg'),
    ];

    $form['quota']['email_settings']['openstack_quota_notification_quota_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Quota information'),
      '#default_value' => $config->get('openstack_quota_notification_quota_info'),
      '#description' => $this->t('Available tokens are: [openstack_quota:name], [openstack_quota:quota_link], [openstack_quota:quota_link_edit], [openstack_quota:changed].'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('openstack.settings');
    $form_state->cleanValues();

    foreach ($form_state->getValues() ?: [] as $key => $value) {
      $config->set($key, Html::escape($value));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
