<?php

namespace Drupal\openstack\Form\Config;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OpenStack Admin Settings.
 */
class OpenStackAdminSettings extends ConfigFormBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * Constructs an OpenStackAdminSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    EntityTypeManagerInterface $entity_type_manager,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    TypedConfigManagerInterface $typed_config_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('config.typed')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId(): string {
    return 'openstack_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['openstack.settings'];
  }

  /**
   * Defines the settings form for OpenStack.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('openstack.settings');
    $aws_cloud_configs = $this->cloudConfigPluginManager->loadConfigEntities('aws_cloud');

    $form['test_mode'] = [
      '#type' => 'details',
      '#title' => $this->t('Test Mode'),
      '#open' => TRUE,
    ];

    $form['test_mode']['openstack_test_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable test mode?'),
      '#default_value' => $config->get('openstack_test_mode'),
      '#description' => $this->t('This enables you to test the OpenStack module settings without accessing OpenStack.'),
    ];

    $form['icon'] = [
      '#type' => 'details',
      '#title' => $this->t('Icon'),
      '#open' => TRUE,
    ];

    $form['icon']['openstack_cloud_config_icon'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('OpenStack Cloud Config Icon'),
      '#default_value' => [
        'fids' => $config->get('openstack_cloud_config_icon'),
      ],
      '#description' => $this->t('Upload the default image to represent OpenStack.'),
      '#upload_location' => 'public://images/cloud/icons',
      '#upload_validators' => [
        'file_validate_is_image' => [],
      ],
    ];

    $form['views'] = [
      '#type' => 'details',
      '#title' => $this->t('View'),
      '#open' => TRUE,
    ];

    $form['views']['pager_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Pager options'),
      '#open' => TRUE,
    ];

    $form['views']['pager_options']['openstack_limit_resources_retrieve'] = [
      '#type' => 'number',
      '#title' => 'Limit the number of resources to retrieve',
      '#description' => $this->t('Limit the number of resources to retrieve.'),
      '#default_value' => $config->get('openstack_limit_resources_retrieve'),
      '#min' => 1,
      '#max' => 9999,
      '#field_suffix' => 'items',
    ];

    $form['volume'] = [
      '#type' => 'details',
      '#title' => $this->t('Volume'),
      '#open' => TRUE,
    ];

    $form['volume']['notification_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Notification Settings'),
      '#open' => TRUE,
    ];

    $form['volume']['notification_settings']['openstack_unused_volume_criteria'] = [
      '#type' => 'select',
      '#title' => $this->t('Unused volume criteria'),
      '#description' => $this->t('A volume is considered unused if it has been created and available for the specified number of days.'),
      '#options' => [
        30 => $this->t('30 days'),
        60 => $this->t('60 days'),
        90 => $this->t('90 days'),
        180 => $this->t('180 days'),
        365 => $this->t('One year'),
      ],
      '#default_value' => $config->get('openstack_unused_volume_criteria'),
    ];

    $form['schedule'] = [
      '#type' => 'details',
      '#title' => $this->t('Schedule'),
      '#open' => TRUE,
    ];

    $form['schedule']['termination_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Termination Options'),
      '#open' => TRUE,
    ];

    $form['schedule']['termination_options']['openstack_instance_terminate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically terminate instance'),
      '#description' => $this->t('Terminate instance automatically.'),
      '#default_value' => $config->get('openstack_instance_terminate'),
    ];

    $form['cron'] = [
      '#type' => 'details',
      '#title' => $this->t('Cron'),
      '#open' => TRUE,
    ];

    $form['cron']['openstack_update_resources_queue_cron_time'] = [
      '#type' => 'number',
      '#title' => 'Update Resources Queue Cron Time',
      '#description' => $this->t('The cron time for queue update resources.'),
      '#default_value' => $config->get('openstack_update_resources_queue_cron_time'),
      '#min' => 1,
      '#max' => 9999,
      '#field_suffix' => 'seconds',
    ];
    $form['cron']['openstack_queue_limit'] = [
      '#type' => 'number',
      '#title' => 'Queue Limit',
      '#description' => $this->t('Number of items allowed in the update queue. New items are added when the queue count falls below this threshold.'),
      '#default_value' => $config->get('openstack_queue_limit'),
    ];
    $form['s3'] = [
      '#type' => 'details',
      '#title' => $this->t('S3'),
      '#open' => TRUE,
    ];

    // The following code applies a function to each element of the
    // $aws_cloud_configs array (each $aws_cloud_config). The function creates
    // a new entry in the $carry array for each $aws_cloud_config, with the key
    // being the result of getCloudContext() and the value being the result of
    // getName().  The last argument [] sets the initial value of $carry to be
    // an empty array.
    $aws_clouds_options = array_reduce($aws_cloud_configs ?: [], static function ($carry, $aws_cloud_config) {
      $carry[$aws_cloud_config->getCloudContext()] = $aws_cloud_config->getName();
      return $carry;
    }, []);

    $form['s3']['openstack_aws_cloud'] = [
      '#title' => $this->t('AWS Cloud'),
      '#description' => $this->t('Select the AWS Cloud whose S3 bucket will be used.'),
      '#type' => 'select',
      '#options' => $aws_clouds_options,
      '#default_value' => $config->get('openstack_aws_cloud'),
    ];

    $form['s3']['openstack_s3_bucket'] = [
      '#title' => $this->t('Amazon S3 Bucket'),
      '#description' => $this->t('Specify the destination to which the resources of OpenStack will be saved to.'),
      '#type' => 'textfield',
      '#default_value' => $config->get('openstack_s3_bucket'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('openstack.settings');
    $form_state->cleanValues();

    foreach ($form_state->getValues() ?: [] as $key => $value) {
      if ($key === 'openstack_cloud_config_icon') {
        $icon = $form_state->getValue('openstack_cloud_config_icon');
        $fileStorage = $this->entityTypeManager->getStorage('file');
        $file = $fileStorage->load($icon[0]);
        // Save the icon.
        if (!empty($file)) {
          $file->setPermanent();
          $file->save();
          $config->set('openstack_cloud_config_icon', $icon[0]);
        }
        else {
          $config->set('openstack_cloud_config_icon', '');
        }
        continue;
      }
      $config->set($key, Html::escape($value));
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
