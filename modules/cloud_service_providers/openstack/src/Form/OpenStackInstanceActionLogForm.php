<?php

namespace Drupal\openstack\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\TableSort;

/**
 * OpenStack instance action log form.
 */
class OpenStackInstanceActionLogForm extends OpenStackInstanceConsoleOutputForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'openstack_instance_action_log_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $action_log = $this->openStackOperationsService->getActionLog($this->entity, $form, $form_state);

    $header = [
      [
        'data' => $this->t('Request ID'),
        'field' => 'request_id',
      ],
      [
        'data' => $this->t('Action'),
        'field' => 'action',
      ],
      [
        'data' => $this->t('Start Time'),
        'field' => 'start_time',
      ],
      [
        'data' => $this->t('User ID'),
        'field' => 'user_id',
      ],
      [
        'data' => $this->t('Message'),
        'field' => 'message',
      ],
    ];

    $request = $this->getRequest();
    $order = TableSort::getOrder($header, $request);
    $sort = TableSort::getSort($header, $request);

    if (empty($sort)) {
      $sort = 'asc';
    }

    if (!empty($order)) {
      usort($action_log, function ($a, $b) use ($order, $sort) {
        $item1 = $a[$order['sql']];
        $item2 = $b[$order['sql']];
        if ($item1 === $item2) {
          return 0;
        }

        if (strtolower($sort) === 'asc') {
          return ($item1 < $item2) ? -1 : 1;
        }

        if (strtolower($sort) === 'desc') {
          return ($item1 > $item2) ? -1 : 1;
        }

        return 0;
      });
    }

    $form['action_log_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $this->getRows($action_log),
      '#empty' => $this->t('No data available.'),
    ];

    return $form;
  }

  /**
   * Get row data.
   *
   * @param array $action_log
   *   Action log.
   *
   * @return array
   *   Rows for table.
   */
  private function getRows(array $action_log): array {
    $rows = [];
    foreach ($action_log as $content) {
      $rows[] = [
        $content['request_id'],
        $content['action'],
        $this->dateFormatter->format(strtotime($content['start_time']), 'short'),
        $content['user_id'],
        $content['message'],
      ];
    }

    return $rows;
  }

}
