<?php

namespace Drupal\openstack\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * OpenStack instance console form.
 */
class OpenStackInstanceConsoleForm extends OpenStackInstanceConsoleOutputForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'openstack_instance_console_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $console = $this->openStackOperationsService->getConsole($this->entity, $form, $form_state);

    if (empty($console)) {
      $this->messenger->addError('Cannot display the console because there is no available console.');
      return $form;
    }

    $form['notice'] = [
      '#type' => 'inline_template',
      '#template' => 'If console is not responding to keyboard input: click the grey status bar below. '
      . '<a href="{{href}}">Click here to show only console</a>'
      . "<br>To exit the fullscreen mode, click the browser's back button.",
      '#context' => ['href' => $console['url']],
    ];

    $form['console'] = [
      '#type' => 'inline_template',
      '#template' => '<iframe src="{{src}}" onload="handleOpenStackInstanceConsoleLoad()" style="width: 100%; aspect-ratio: 4/3;" sandbox="allow-same-origin allow-forms allow-scripts" id ="console-iframe"></iframe>',
      '#context' => ['src' => $console['url']],
    ];
    $form['console']['#attached']['library'][] = 'openstack/openstack_instance_console';

    return $form;
  }

}
