<?php

namespace Drupal\openstack\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Entity\Ec2\PublicIpEntityLinkHtmlGenerator;
use Drupal\aws_cloud\Form\Ec2\InstanceEditForm;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\openstack\Entity\OpenStackFloatingIp;
use Drupal\openstack\Service\OpenStackOperationsServiceInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Drupal\openstack\Service\Rest\OpenStackService as OpenStackRestService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for OpenStack instance edit forms.
 *
 * @ingroup openstack
 */
class OpenStackInstanceEditForm extends InstanceEditForm {

  public const SECURITY_GROUP_DELIMITER = ', ';

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  protected $openStackServiceFactory;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The OpenStack operations Service.
   *
   * @var \Drupal\openstack\Service\OpenStackOperationsServiceInterface
   */
  protected $openStackOperationsService;

  /**
   * OpenStackInstanceEditForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\openstack\Service\OpenStackOperationsServiceInterface $openstack_operations_service
   *   The OpenStack Operations service.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    OpenStackServiceFactoryInterface $openstack_service_factory,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    Renderer $renderer,
    CloudServiceInterface $cloud_service,
    OpenStackOperationsServiceInterface $openstack_operations_service,
  ) {

    parent::__construct(
      $aws_cloud_operations_service,
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->openStackServiceFactory = $openstack_service_factory;
    $this->openStackOperationsService = $openstack_operations_service;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return \Drupal\openstack\Entity\OpenStackInstanceEditForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_cloud.operations'),
      $container->get('openstack.factory'),
      $container->get('openstack.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud'),
      $container->get('openstack.operations')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {

    // Switch OpenStack EC2 or Rest service based on $cloud_context.
    $this->ec2Service = $this->openStackServiceFactory->get($cloud_context);

    $form = parent::buildForm($form, $form_state);
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;

    // Custom buildForm logic for OpenStack REST API.
    if ($this->ec2Service instanceof OpenStackRestService) {

      unset($form['instance']['cost']);
      unset($form['instance']['kernel_id']);
      unset($form['instance']['ramdisk_id']);
      unset($form['instance']['virtualization']);
      unset($form['instance']['reservation']);
      unset($form['network']['vpc_id']);
      unset($form['network']['subnet_id']);
      unset($form['storage']['root_device_type']);
      unset($form['storage']['root_device']);
      unset($form['storage']['ebs_optimized']);
      unset($form['network']['public_ip']);

      $weight = $form['options']['#weight'];
      unset($form['options']);
      $form['options'] = [
        '#type'          => 'details',
        '#title'         => $this->t('Options'),
        '#open'          => TRUE,
        '#weight'        => $weight,
      ];

      $form['options']['termination_protection'] = [
        '#title'         => $this->t('Termination protection'),
        '#type'          => 'checkbox',
        '#description'   => $this->t('Enable the termination protection. If enabled, this instance cannot be terminated until termination protection is disabled.'),
        '#default_value' => $entity->getTerminationProtection(),
      ];

      // Set key pair link.
      $form['network']['key_pair_name'] = $this->entityLinkRenderer->renderFormElements(
        $entity->getKeyPairName(),
        'openstack_key_pair',
        'key_pair_name',
        ['#title' => $this->getItemTitle($this->t('Key pair name'))]
      );

      // Set floating ip link.
      $public_ip = $entity->getPublicIp();
      $current_floating_ip = !empty($public_ip) ? $this->getFloatingIpByIpLookup($public_ip) : NULL;

      // Public IP is not a Floating IP. It is an OpenStack assigned IP address.
      // Just display it.
      if (!empty($public_ip) && empty($current_floating_ip)) {
        $form['network']['public_ip'] = $this->entityLinkRenderer->renderFormElements(
          $public_ip,
          'openstack_floating_ip',
          'public_ip',
          ['#title' => $this->getItemTitle($this->t('Public IP'))]
        );
      }
      elseif ($entity->getInstanceState() === 'stopped') {
        // If no floating_ip assigned and no available IPs, prompt user
        // to create one.
        if (count($this->getAvailableFloatingIpCount()) === 0 && empty($current_floating_ip)) {
          $link = Link::createFromRoute($this->t('Create a new Floating IP'), 'entity.openstack_floating_ip.add_form', [
            'cloud_context' => $entity->getCloudContext(),
          ])->toString();
          $form['network']['elastic_ip_link'] = [
            '#type'          => 'item',
            '#title'         => $this->getItemTitle($this->t('Floating IP')),
            '#markup'        => $link,
            '#not_field'     => TRUE,
          ];
        }
        else {
          $form['network']['elastic_ip_link'] = $this->entityLinkRenderer->renderFormElements(
            $public_ip,
            'openstack_floating_ip',
            'public_ip',
            [
              '#title' => $this->getItemTitle($this->t('Public IP')),
            ],
            '',
            PublicIpEntityLinkHtmlGenerator::class
          );
          // Set #not_field otherwise there is an error thrown and
          // form will not get saved.
          $form['network']['elastic_ip_link']['#not_field'] = TRUE;
        }
      }
      elseif (!empty($current_floating_ip)) {
        $form['network']['public_ip'] = $this->entityLinkRenderer->renderFormElements(
          $public_ip,
          'openstack_floating_ip',
          'public_ip',
          [
            '#title' => $this->getItemTitle($this->t('Floating IP')),
          ],
          '',
          PublicIpEntityLinkHtmlGenerator::class
        );
        // Set #not_field otherwise there is an error thrown and
        // form will not get saved.
        $form['network']['public_ip']['#not_field'] = TRUE;
      }

      $form['network']['public_ip']['#weight'] = -1;
      $form['network']['elastic_ip_link']['#weight'] = -1;

      $form['network']['security_groups']['#type'] = 'checkboxes';

      // Set security group link.
      $selected_security_group = $form['network']['security_groups']['#default_value'] ?? [];

      $sg_link_markup = [];
      foreach ($selected_security_group ?: [] as $group_name) {

        $security_group = $this->entityTypeManager
          ->getStorage('openstack_security_group')
          ->loadByProperties([
            'cloud_context' => $entity->getCloudContext(),
            'name' => $group_name,
          ]);
        $security_group = array_shift($security_group);

        $sg_link = $this->entityLinkRenderer->renderFormElements(
          $group_name,
          'openstack_security_group',
          'name',
          ['#title' => $this->getItemTitle($this->t('Name'))]
        );

        $sg_link_markup[] = $sg_link['#markup'];
      }

      // Render volume ID elements.
      $blockDevices = [];
      $block_devices = explode(', ', $entity->getBlockDevices() ?? '');
      foreach ($block_devices ?: [] as $block_device) {
        $render_element = $this->entityLinkRenderer->renderFormElements(
          $block_device,
          'openstack_volume',
          'volume_id',
          ['#title' => $this->getItemTitle($this->t('Volume'))],
          '',
          PublicIpEntityLinkHtmlGenerator::class
        );
        $blockDevices[] = $render_element['#markup'];
      }
      $form['storage']['block_devices'] = [
        '#type' => 'item',
        '#title' => $this->getItemTitle($this->t('Volume')),
        '#markup' => implode(', ', $blockDevices),
      ];
    }
    $form['instance']['instance_type']['#access'] = FALSE;
    $form['instance']['iam_role']['#access'] = FALSE;
    $form['options']['schedule']['#access'] = FALSE;

    return $form;
  }

  /**
   * Submit handler.
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->openStackOperationsService->editOpenStackInstance($this->entity, $form, $form_state);
  }

  /**
   * Helper function to query db for available Floating IPs.
   */
  private function getAvailableFloatingIpCount(): array {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;
    return $this->entityTypeManager->getStorage('openstack_floating_ip')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $entity->getCloudContext())
      ->notExists('allocation_id')
      ->execute();
  }

  /**
   * Helper to look up a Floating IP row given the Floating IP address.
   *
   * @param string $ip
   *   IP address used to look up the row.
   *
   * @return \Drupal\openstack\Entity\OpenStackFloatingIp
   *   NULL  if no row found or the FloatingIp entity.
   */
  private function getFloatingIpByIpLookup($ip): ?OpenStackFloatingIp {
    $floating_ip = NULL;
    if (empty($ip)) {
      return NULL;
    }

    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;
    $result = $this->entityTypeManager->getStorage('openstack_floating_ip')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'public_ip' => $ip,
      ]);
    if (count($result) === 1) {
      $floating_ip = array_shift($result);
    }
    return $floating_ip;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSecurityGroupsOptions(): array {
    $options = [];
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;
    $groups = $this->entityTypeManager->getStorage('openstack_security_group')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
      ]);

    array_walk($groups, static function ($group) use (&$options) {
      $options[$group->getGroupName()] = $group->getGroupName();
    });

    natcasesort($options);
    return $options;
  }

}
