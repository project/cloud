<?php

namespace Drupal\openstack\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\aws_cloud\Form\Ec2\InstanceRebootMultipleForm;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\openstack\Service\Ec2\OpenStackService as OpenStackEc2Service;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an entities reboot confirmation form.
 */
class OpenStackInstanceRebootMultipleForm extends InstanceRebootMultipleForm {

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  protected $openStackServiceFactory;

  /**
   * Default reboot type.
   *
   * @var string
   */
  protected $type = 'HARD';

  /**
   * OpenStackInstanceRebootMultipleForm constructor.
   *
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   */
  public function __construct(
    OpenStackServiceFactoryInterface $openstack_service_factory,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    PrivateTempStoreFactory $temp_store_factory,
    MessengerInterface $messenger,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    RouteMatchInterface $route_match,
    Ec2ServiceInterface $ec2_service,
  ) {

    parent::__construct(
      $current_user,
      $entity_type_manager,
      $temp_store_factory,
      $messenger,
      $cloud_config_plugin_manager,
      $route_match,
      $ec2_service
    );

    $this->openStackServiceFactory = $openstack_service_factory;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return OpenStackInstanceRebootMultipleForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openstack.factory'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('tempstore.private'),
      $container->get('messenger'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_route_match'),
      $container->get('openstack.ec2')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL): array {
    $form = parent::buildForm($form, $form_state, $entity_type_id);
    $form['reboot'] = [
      '#type' => 'details',
      '#title' => $this->t('Reboot option'),
      '#open' => TRUE,
    ];
    $form['reboot']['type'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Soft reboot'),
      '#description' => $this->t('A soft reboot attempts a graceful shutdown and restart of the server'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->type = (bool) $form_state->getValue('type') === TRUE ? 'SOFT' : 'HARD';
    parent::submitForm($form, $form_state);
  }

  /**
   * Process a Cloud Resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   *
   * @return bool
   *   Succeeded or failed.
   */
  public function processCloudResource(CloudContentEntityBase $entity): bool {
    // Switch OpenStack EC2 or Rest service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    if ($this->ec2Service instanceof OpenStackEc2Service) {
      return parent::processCloudResource($entity);
    }

    $result = $this->ec2Service->rebootInstances([
      'InstanceId' => $entity->getInstanceId(),
      'type' => $this->type,
    ]);

    return $result !== NULL;
  }

}
