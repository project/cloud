<?php

namespace Drupal\openstack\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Form\Ec2\AwsCloudContentForm;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\openstack\Entity\OpenStackPort;
use Drupal\openstack\Entity\OpenStackPortInterface;
use Drupal\openstack\Service\OpenStackOperationsServiceInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Drupal\openstack\Traits\OpenStackFormSelectTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the Port entity create form.
 *
 * @ingroup openstack
 */
class OpenStackPortCreateForm extends AwsCloudContentForm {

  use AwsCloudFormTrait;
  use CloudContentEntityTrait;
  use OpenStackFormSelectTrait;

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  protected $openStackServiceFactory;

  /**
   * The OpenStack operations Service.
   *
   * @var \Drupal\openstack\Service\OpenStackOperationsServiceInterface
   */
  protected $openStackOperationsService;

  /**
   * OpenStackPortCreateForm constructor.
   *
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\openstack\Service\OpenStackOperationsServiceInterface $openstack_operations_service
   *   The OpenStack Operations service.
   */
  public function __construct(
    OpenStackServiceFactoryInterface $openstack_service_factory,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    Renderer $renderer,
    CloudServiceInterface $cloud_service,
    OpenStackOperationsServiceInterface $openstack_operations_service,
  ) {

    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->openStackServiceFactory = $openstack_service_factory;
    $this->openStackOperationsService = $openstack_operations_service;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return \Drupal\openstack\Entity\OpenStackPortCreateForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openstack.factory'),
      $container->get('openstack.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud'),
      $container->get('openstack.operations')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {

    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $weight = -50;

    $form['port'] = [
      '#type' => 'details',
      '#title' => $this->t('@title', ['@title' => $entity->getEntityType()->getSingularLabel()]),
      '#open' => TRUE,
      '#weight' => $weight++,
      '#attributes' => [
        'id' => 'port-container',
      ],
    ];

    $form['port']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->label(),
      '#required'      => TRUE,
    ];

    $form['port']['network_id'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Network'),
      '#required'      => TRUE,
      '#options'       => $this->getNetworkOptions($cloud_context),
      '#ajax'          => [
        'callback' => '::getSubnetsAjaxCallback',
        'event' => 'change',
        'wrapper'  => 'port-container',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Retrieving...'),
        ],
      ],
    ];

    $form['port']['device_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Device ID'),
      '#description'   => $this->t('Device ID attached to the port.'),
      '#required'      => FALSE,
    ];

    $form['port']['device_owner'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Device owner'),
      '#description'   => $this->t('Owner of the device attached to the port.'),
      '#required'      => FALSE,
    ];

    $form['port']['ip_address_or_subnet'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Specify IP address or subnet'),
      '#description'   => $this->t('Specify a subnet or a fixed IP, select any options.'),
      '#options'       => [
        'unspecified' => $this->t('Unspecified'),
        'subnet' => $this->t('Subnet'),
        'fixed_ip' => $this->t('Fixed IP address'),
      ],
      '#required'      => TRUE,
    ];

    $form['port']['subnet'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Subnet'),
      '#options'       => $this->getSubnetOptions($cloud_context, $form_state->getValue('network_id')),
      '#required'      => FALSE,
      '#states'        => [
        'visible' => [
          [
            ':input[name=ip_address_or_subnet]' => ['value' => 'subnet'],
          ],
        ],
      ],
    ];

    $form['port']['fixed_ips'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Fixed IP address'),
      '#required'      => FALSE,
      '#states'        => [
        'visible' => [
          [
            ':input[name=ip_address_or_subnet]' => ['value' => 'fixed_ip'],
          ],
        ],
      ],
    ];

    $form['port']['mac_address'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('MAC address'),
      '#description'   => $this->t('MAC address for the port.'),
      '#required'      => FALSE,
    ];

    $form['port']['port_security_enabled'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Port security'),
      '#description'   => $this->t('Enable security rules for the port.'),
      '#default_value' => TRUE,
      '#required'      => FALSE,
    ];

    $options = $this->getSecurityGroupOptions($cloud_context);
    $default_security_group_id = NULL;
    foreach ($options as $id => $name) {
      if ($name === 'default') {
        $default_security_group_id = $id;
        break;
      }
    }
    $form['port']['security_groups'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Security group'),
      '#options'       => $options,
      '#multiple'      => TRUE,
      '#default_value' => $default_security_group_id,
      '#required'      => FALSE,
      '#states'        => [
        'visible' => [
          [
            ':input[name=port_security_enabled]' => ['checked' => TRUE],
          ],
        ],
      ],
    ];

    $form['port']['binding_vnic_type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('VNIC type'),
      '#description'   => $this->t('The VNIC type that is bound to the port.'),
      '#options'       => OpenStackPort::getVnicTypeAllowedValues(),
      '#required'      => FALSE,
    ];

    $form['port']['binding_profile'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Binding profile'),
      '#description'   => $this->t('A JSON string that enables the application running on the specific host to pass and receive vif port information specific to the networking back-end.'),
      '#default_value' => $entity->getBindingProfile(),
      '#required'      => FALSE,
    ];

    unset($form['allowed_address_pairs']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): ?OpenStackPortInterface {
    parent::validateForm($form, $form_state);

    // Validate binding_profile is json string.
    $binding_profile = $form_state->getValue('binding_profile');
    if (empty($binding_profile)) {
      return $this->entity;
    }

    if (empty(json_decode($binding_profile, TRUE))) {
      $form_state->setErrorByName('binding_profile', 'The binding profile is not a valid JSON string.');
    }

    return $this->entity;
  }

  /**
   * Submit handler.
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->openStackOperationsService->createOpenStackPort($this->entity, $form, $form_state);
  }

  /**
   * Ajax callback when the network dropdown changes.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface element.
   *
   * @return array
   *   Form element for port.
   */
  public function getSubnetsAjaxCallback(array $form, FormStateInterface $form_state): array {
    return $form['port'];
  }

}
