<?php

namespace Drupal\openstack\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Form\Ec2\AwsCloudContentForm;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\openstack\Entity\OpenStackPort;
use Drupal\openstack\Service\OpenStackOperationsServiceInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Drupal\openstack\Traits\OpenStackFormSelectTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the Port entity edit form.
 *
 * @ingroup openstack
 */
class OpenStackPortEditForm extends AwsCloudContentForm {

  use AwsCloudFormTrait;
  use CloudContentEntityTrait;
  use OpenStackFormSelectTrait;

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  protected $openStackServiceFactory;

  /**
   * The OpenStack operations Service.
   *
   * @var \Drupal\openstack\Service\OpenStackOperationsServiceInterface
   */
  protected $openStackOperationsService;

  /**
   * OpenStackPortEditForm constructor.
   *
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\openstack\Service\OpenStackOperationsServiceInterface $openstack_operations_service
   *   The OpenStack Operations service.
   */
  public function __construct(
    OpenStackServiceFactoryInterface $openstack_service_factory,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    Renderer $renderer,
    CloudServiceInterface $cloud_service,
    OpenStackOperationsServiceInterface $openstack_operations_service,
  ) {

    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->openStackServiceFactory = $openstack_service_factory;
    $this->openStackOperationsService = $openstack_operations_service;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return \Drupal\openstack\Entity\OpenStackPortEditForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openstack.factory'),
      $container->get('openstack.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud'),
      $container->get('openstack.operations')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {

    // Switch OpenStack EC2 or REST service based on $cloud_context.
    $this->ec2Service = $this->openStackServiceFactory->get($cloud_context);

    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    if (!$this->updateEntityBuildForm($entity)) {
      return $this->redirectUrl;
    }

    $weight = -50;

    $form['port'] = [
      '#type' => 'details',
      '#title' => $this->t('@title', ['@title' => $entity->getEntityType()->getSingularLabel()]),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['port']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->label(),
      '#weight'        => -5,
      '#required'      => TRUE,
    ];

    $form['port']['network_id'] = $this->entityLinkRenderer->renderFormElements(
      $entity->getNetworkId(),
      'openstack_network',
      'network_id',
      ['#title' => $this->getItemTitle($this->t('Network'))],
      '',
      EntityLinkWithNameHtmlGenerator::class
    );

    $form['port']['admin_state_up'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable admin state'),
      '#description'   => $this->t('When the admin state of the port is enabled, the networking service forward packets on the port. Otherwise, it does not forward any packets on the port.'),
      '#default_value' => $entity->getAdminStateUp(),
      '#weight'        => -5,
      '#required'      => FALSE,
    ];

    $form['port']['binding_vnic_type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('VNIC type'),
      '#description'   => $this->t('The VNIC type that is bound to the port.'),
      '#options'       => OpenStackPort::getVnicTypeAllowedValues(),
      '#default_value' => $entity->getBindingVnicType(),
      '#required'      => FALSE,
    ];

    $form['port']['port_security_enabled'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Port security'),
      '#description'   => $this->t('Enable security rules for the port.'),
      '#default_value' => $entity->getPortSecurityEnabled(),
      '#required'      => FALSE,
    ];

    $default_values = [];
    foreach ($entity->getSecurityGroups() ?: [] as $item) {
      $default_values[] = $item['value'];
    }
    $options = $this->getSecurityGroupOptions($cloud_context);
    $form['port']['security_groups'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Security group'),
      '#options'       => $options,
      '#multiple'      => TRUE,
      '#default_value' => $default_values,
      '#required'      => FALSE,
      '#states'        => [
        'visible' => [
          [
            ':input[name=port_security_enabled]' => ['checked' => TRUE],
          ],
        ],
      ],
    ];

    $form['port']['allowed_address_pairs'] = $form['allowed_address_pairs'];
    unset($form['port']['allowed_address_pairs']['#weight']);
    $form['port']['allowed_address_pairs']['#states'] = [
      'visible' => [
        [
          ':input[name=port_security_enabled]' => ['checked' => TRUE],
        ],
      ],
    ];
    unset($form['allowed_address_pairs']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    return $form;
  }

  /**
   * Submit handler.
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->openStackOperationsService->editOpenStackPort($this->entity, $form, $form_state);
  }

}
