<?php

namespace Drupal\openstack\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\aws_cloud\Form\Ec2\AwsDeleteUpdateEntityForm;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\openstack\Service\OpenStackOperationsServiceInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for add an interface.
 *
 * @ingroup openstack
 */
class OpenStackRouterAddInterfaceForm extends AwsDeleteUpdateEntityForm {

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  protected $openStackServiceFactory;

  /**
   * The OpenStack operations Service.
   *
   * @var \Drupal\openstack\Service\OpenStackOperationsServiceInterface
   */
  protected $openStackOperationsService;

  /**
   * OpenStackRouterCreateInterfaceForm constructor.
   *
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud EC2 service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\openstack\Service\OpenStackOperationsServiceInterface $openstack_operations_service
   *   The OpenStack Operations service.
   */
  public function __construct(
    OpenStackServiceFactoryInterface $openstack_service_factory,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    EntityLinkRendererInterface $entity_link_renderer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    OpenStackOperationsServiceInterface $openstack_operations_service,
  ) {
    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $entity_link_renderer,
      $cloud_config_plugin_manager
    );
    $this->openStackServiceFactory = $openstack_service_factory;
    $this->openStackOperationsService = $openstack_operations_service;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return OpenStackRouterCreateInterfaceForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openstack.factory'),
      $container->get('openstack.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('entity.link_renderer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('openstack.operations')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    /** @var \Drupal\openstack\Entity\OpenStackRouter $entity */
    $entity = $this->entity;

    return $this->t('Add an interface for router %name (@router_id)', [
      '%name' => $entity->label(),
      '@router_id' => $entity->getRouterId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Add interface');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    if (!$this->updateEntityBuildForm($this->entity)) {
      return $this->redirectUrl;
    }
    $form['interface'] = [
      '#type' => 'details',
      '#title' => $this->t('Interface'),
      '#open' => TRUE,
    ];

    $options = $this->getSubnetOptions();
    $form['interface']['subnet_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Subnet'),
      '#options' => $options,
      '#required' => TRUE,
    ];

    $form['interface']['ip_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP address'),
      '#description' => $this->t('Specify an IP address for the interface created (e.g. 192.168.0.254).'),
    ];
    return $form;
  }

  /**
   * Submit handler.
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->openStackOperationsService->addOpenStackRouterInterface($this->entity, $form, $form_state);
  }

  /**
   * Get subnet options.
   *
   * @return array
   *   Subnet options.
   */
  private function getSubnetOptions(): array {
    /** @var \Drupal\openstack\Entity\OpenStackRouter $entity */
    $entity = $this->entity;
    /** @var \Drupal\openstack\Entity\OpenStackPort[] $ports */
    $ports = $this->entityTypeManager
      ->getStorage('openstack_port')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'device_id' => $entity->getRouterId(),
      ]);

    $subnet_ids_used = [];
    foreach ($ports ?: [] as $port) {
      if (empty($port->getFixedIps())) {
        continue;
      }

      $fixed_ips = $port->getFixedIps();
      foreach ($fixed_ips ?: [] as $fixed_ip) {
        $parts = explode(',', $fixed_ip['value']);
        if (count($parts) < 2 || in_array($parts[1], $subnet_ids_used)) {
          continue;
        }

        $subnet_ids_used[] = $parts[1];
      }
    }

    /** @var \Drupal\openstack\Entity\OpenStackSubnet[] $subnets */
    $subnets = $this->entityTypeManager
      ->getStorage('openstack_subnet')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
      ]);

    $options = [];
    foreach ($subnets ?: [] as $subnet) {
      if (in_array($subnet->getSubnetId(), $subnet_ids_used)) {
        continue;
      }

      /** @var \Drupal\openstack\Entity\OpenStackNetwork[] $networks */
      $networks = $this->entityTypeManager
        ->getStorage('openstack_network')
        ->loadByProperties([
          'cloud_context' => $entity->getCloudContext(),
          'network_id' => $subnet->getNetworkId(),
        ]);
      if (empty($networks)) {
        continue;
      }

      $network = reset($networks);
      $options[$subnet->getSubnetId()] = sprintf('%s: %s (%s)', $network->getName(), $subnet->getCidr(), $subnet->getName());
    }

    return $options;
  }

}
