<?php

namespace Drupal\openstack\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Form\Ec2\AwsCloudContentForm;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\openstack\Service\OpenStackOperationsServiceInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Drupal\openstack\Service\OpenStackServiceInterface;
use Drupal\openstack\Traits\OpenStackFormSelectTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the Router entity edit form.
 *
 * @ingroup openstack
 */
class OpenStackRouterEditForm extends AwsCloudContentForm {

  use AwsCloudFormTrait;
  use CloudContentEntityTrait;
  use OpenStackFormSelectTrait;

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  protected $openStackServiceFactory;

  /**
   * The OpenStack operations Service.
   *
   * @var \Drupal\openstack\Service\OpenStackOperationsServiceInterface
   */
  protected $openStackOperationsService;

  /**
   * OpenStackRouterEditForm constructor.
   *
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\openstack\Service\OpenStackOperationsServiceInterface $openstack_operations_service
   *   The OpenStack Operations service.
   */
  public function __construct(
    OpenStackServiceFactoryInterface $openstack_service_factory,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    Renderer $renderer,
    CloudServiceInterface $cloud_service,
    OpenStackOperationsServiceInterface $openstack_operations_service,
  ) {

    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->openStackServiceFactory = $openstack_service_factory;
    $this->openStackOperationsService = $openstack_operations_service;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return \Drupal\openstack\Entity\OpenStackRouterEditForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openstack.factory'),
      $container->get('openstack.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud'),
      $container->get('openstack.operations')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {

    // Switch OpenStack EC2 or REST service based on $cloud_context.
    $this->ec2Service = $this->openStackServiceFactory->get($cloud_context);

    $form = parent::buildForm($form, $form_state);
    /** @var \Drupal\openstack\Entity\OpenStackRouterInterface $entity */
    $entity = $this->entity;

    if (!$this->updateEntityBuildForm($entity)) {
      return $this->redirectUrl;
    }

    $weight = -50;

    $form['router'] = [
      '#type' => 'details',
      '#title' => $this->t('@title', ['@title' => $entity->getEntityType()->getSingularLabel()]),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['router']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->label(),
      '#required'      => TRUE,
    ];

    $form['router']['admin_state_up'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Admin state up'),
      '#description'   => $this->t('The administrative state of the router, which is up (true) or down (false).'),
      '#default_value' => $entity->getAdminStateUp(),
      '#required'      => FALSE,
    ];

    $form['router']['external_gateway_network_id'] = [
      '#type'          => 'select',
      '#title'         => $this->t('External network'),
      '#required'      => FALSE,
      '#options'       => $this->getNetworkOptions($cloud_context, ['external' => TRUE]),
      '#empty_value'   => '',
      '#default_value' => $entity->getExternalGatewayNetworkId(),
    ];

    $roles = $this->ec2Service->describeCurrentUserRoles();
    if (in_array(OpenStackServiceInterface::ADMIN_ROLE, $roles)) {
      $form['router']['external_gateway_enable_snat'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Enable SNAT'),
        '#description'   => $this->t('Enable SNAT will only have an effect if an external network is set.'),
        '#default_value' => $entity->getExternalGatewayEnableSnat(),
        '#required'      => FALSE,
        '#states'        => [
          'invisible' => [
            [
              ':input[name=external_gateway_network_id]' => ['value' => ''],
            ],
          ],
        ],
      ];
    }

    $form['router']['routes'] = $form['routes'];
    unset($form['router']['routes']['#weight']);
    unset($form['routes']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    return $form;
  }

  /**
   * Submit handler.
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->openStackOperationsService->editOpenStackRouter($this->entity, $form, $form_state);
  }

}
