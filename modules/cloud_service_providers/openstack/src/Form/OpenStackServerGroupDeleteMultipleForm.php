<?php

namespace Drupal\openstack\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\aws_cloud\Form\Ec2\AwsCloudDeleteMultipleForm;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an entities deletion confirmation form.
 */
class OpenStackServerGroupDeleteMultipleForm extends AwsCloudDeleteMultipleForm {

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  protected $openStackServiceFactory;

  /**
   * OpenStackServerGroupDeleteMultipleForm constructor.
   */
  public function __construct(
    OpenStackServiceFactoryInterface $openstack_service_factory,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    PrivateTempStoreFactory $temp_store_factory,
    MessengerInterface $messenger,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    RouteMatchInterface $route_match,
    Ec2ServiceInterface $ec2_service,
  ) {
    parent::__construct(
      $current_user,
      $entity_type_manager,
      $temp_store_factory,
      $messenger,
      $cloud_config_plugin_manager,
      $route_match,
      $ec2_service
    );

    $this->openStackServiceFactory = $openstack_service_factory;
  }

  /**
   * Dependency Injection.
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('openstack.factory'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('tempstore.private'),
      $container->get('messenger'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_route_match'),
      $container->get('openstack.ec2')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function processCloudResource(CloudContentEntityBase $entity): bool {
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deleteServerGroup(
      ['ServerGroupId' => $entity->getServerGroupId()]
      ) !== NULL;
  }

}
