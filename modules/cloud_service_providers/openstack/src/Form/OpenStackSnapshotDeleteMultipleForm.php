<?php

namespace Drupal\openstack\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\aws_cloud\Form\Ec2\SnapshotDeleteMultipleForm;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an entities deletion confirmation form.
 */
class OpenStackSnapshotDeleteMultipleForm extends SnapshotDeleteMultipleForm {

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  protected $openStackServiceFactory;

  /**
   * OpenStackSnapshotDeleteMultipleForm constructor.
   *
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud EC2 service.
   */
  public function __construct(
    OpenStackServiceFactoryInterface $openstack_service_factory,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    PrivateTempStoreFactory $temp_store_factory,
    MessengerInterface $messenger,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    RouteMatchInterface $route_match,
    Ec2ServiceInterface $ec2_service,
  ) {
    parent::__construct(
      $current_user,
      $entity_type_manager,
      $temp_store_factory,
      $messenger,
      $cloud_config_plugin_manager,
      $route_match,
      $ec2_service
    );

    $this->openStackServiceFactory = $openstack_service_factory;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return OpenStackSnapshotDeleteMultipleForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('openstack.factory'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('tempstore.private'),
      $container->get('messenger'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_route_match'),
      $container->get('openstack.ec2')
    );

  }

  /**
   * Process a Cloud Resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   *
   * @return bool
   *   Succeeded or failed.
   */
  public function processCloudResource(CloudContentEntityBase $entity): bool {
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    return parent::processCloudResource($entity);
  }

}
