<?php

namespace Drupal\openstack\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\aws_cloud\Form\Ec2\AwsCloudContentForm;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\openstack\Service\OpenStackOperationsServiceInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Drupal\openstack\Traits\OpenStackFormSelectTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the Stack entity create form.
 *
 * @ingroup openstack
 */
class OpenStackStackCreateExtraForm extends AwsCloudContentForm {

  use AwsCloudFormTrait;
  use CloudContentEntityTrait;
  use OpenStackFormSelectTrait;

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  protected $openStackServiceFactory;

  /**
   * The OpenStack operations Service.
   *
   * @var \Drupal\openstack\Service\OpenStackOperationsServiceInterface
   */
  protected $openStackOperationsService;

  /**
   * The tempstore.
   *
   * @var \Drupal\Core\TempStore\SharedTempStore
   */
  protected $tempStore;

  /**
   * OpenStackStackCreateForm constructor.
   *
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\openstack\Service\OpenStackOperationsServiceInterface $openstack_operations_service
   *   The OpenStack Operations service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   */
  public function __construct(
    OpenStackServiceFactoryInterface $openstack_service_factory,
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    Renderer $renderer,
    CloudServiceInterface $cloud_service,
    OpenStackOperationsServiceInterface $openstack_operations_service,
    PrivateTempStoreFactory $temp_store_factory,
  ) {

    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->openStackServiceFactory = $openstack_service_factory;
    $this->openStackOperationsService = $openstack_operations_service;
    $this->tempStore = $temp_store_factory->get($this->getTempStoreId());
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return OpenStackStackCreateForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openstack.factory'),
      $container->get('openstack.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud'),
      $container->get('openstack.operations'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {

    // Switch OpenStack EC2 or REST service based on $cloud_context.
    $this->ec2Service = $this->openStackServiceFactory->get($cloud_context);

    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $weight = -50;

    $form['stack'] = [
      '#type' => 'details',
      '#title' => $this->t('@title', ['@title' => $entity->getEntityType()->getSingularLabel()]),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['stack']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => $entity->label(),
      '#required'      => TRUE,
    ];

    $form['stack']['timeout_mins'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Creation timeout (minutes)'),
      '#description'   => $this->t('The timeout for stack creation in minutes.'),
      '#required'      => TRUE,
      '#default_value' => 60,
    ];

    $form['stack']['rollback'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Rollback on failure'),
      '#description'   => $this->t('Enable rollback on create/update failure.'),
      '#required'      => FALSE,
      '#default_value' => FALSE,
    ];

    $form['stack']['template_parameters'] = $this->buildTemplateParameters();

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    return $form;
  }

  /**
   * Submit handler.
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->openStackOperationsService->createOpenStackStack($this->entity, $form, $form_state, [
      'template'      => $this->tempStore->get('template'),
      'template_url'  => $this->tempStore->get('template_url'),
      'environment'   => $this->tempStore->get('environment'),
    ]);

    $this->tempStore->delete('template');
    $this->tempStore->delete('template_url');
    $this->tempStore->delete('environment');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $name = $form_state->getValue('name') ?? '';

    // @see https://docs.openstack.org/api-ref/orchestration/v1/index.html#create-stack
    if (!empty($name)
      && !preg_match('/^[a-zA-Z][a-zA-Z0-9_.-]{0,254}$/', $name)) {
      $form_state->setError($form, $this->t("The @type %name is invalid:<ul><li>Must be unique within the project.</li><li>Must begin with an ASCII character.</li><li>It cannot contain anything other than ASCII characters, numbers, underscores, periods, and hyphens.</li></ul>", [
        '@type' => $this->getShortEntityTypeNameWhitespace($this->entity),
        '%name' => $name,
      ]));
    }
  }

  /**
   * Build template parameters.
   *
   * @return array
   *   The form elements for template parameters .
   */
  private function buildTemplateParameters(): array {
    $build = [
      '#type'          => 'details',
      '#title'         => $this->t('Template parameters'),
      '#open'          => TRUE,
      '#tree'          => TRUE,
    ];

    $template = $this->tempStore->get('template');
    foreach ($template['parameters'] ?: [] as $name => $parameter) {
      switch ($parameter['type']) {
        case 'number':
          $type = 'number';
          $default_value = $parameter['default'] ?? 0;
          break;

        case 'boolean':
          $type = 'checkbox';
          $default_value = $this->convertToBoolean($parameter['default']);
          break;

        case 'json':
          $type = 'textfield';
          $default_value = json_encode($parameter['default'] ?? '');
          break;

        case 'comma_delimited_list':
          $type = 'textfield';
          $default_value = json_encode($parameter['default'] ?? '');
          break;

        default:
          $type = 'textfield';
          $default_value = $parameter['default'] ?? '';
          break;
      }

      $build[$name] = [
        '#title' => $name,
        '#type' => $type,
        '#description' => $parameter['description'] ?? '',
        '#default_value' => $default_value,
      ];
    }

    return $build;
  }

  /**
   * Convert to boolean value.
   *
   * Refer to https://docs.openstack.org/heat/latest/template_guide/hot_spec.html#parameters-section.
   * Boolean type value, which can be equal “t”, “true”, “on”, “y”, “yes”,
   * or “1” for true value and “f”, “false”, “off”, “n”, “no”, or “0”
   * for false value.
   *
   * @param mixed $value
   *   The value to be converted.
   *
   * @return bool
   *   The boolean value.
   */
  private function convertToBoolean($value): bool {
    if (is_bool($value)) {
      return $value;
    }

    if (is_int($value)) {
      return $value === 1;
    }

    return in_array(strtolower($value), ['t', 'true', 'on', 'y', 'yes', '1']);
  }

  /**
   * Get ID of temp store.
   *
   * @return string
   *   The ID of temp store.
   */
  protected function getTempStoreId(): string {
    return 'openstack_stack_create';
  }

}
