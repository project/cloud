<?php

namespace Drupal\openstack\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\aws_cloud\Form\Ec2\AwsCloudContentForm;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Traits\AwsCloudFormTrait;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\openstack\Traits\OpenStackFormSelectTrait;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the Stack entity create form.
 *
 * @ingroup openstack
 */
class OpenStackStackCreateForm extends AwsCloudContentForm {

  use AwsCloudFormTrait;
  use CloudContentEntityTrait;
  use OpenStackFormSelectTrait;

  /**
   * The tempstore.
   *
   * @var \Drupal\Core\TempStore\SharedTempStore
   */
  protected $tempStore;

  /**
   * Guzzle Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * OpenStackStackCreateForm constructor.
   *
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud or OpenStack EC2 Service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The general renderer.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   */
  public function __construct(
    Ec2ServiceInterface $ec2_service,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    EntityLinkRendererInterface $entity_link_renderer,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cacheRender,
    CachedDiscoveryClearerInterface $plugin_cache_clearer,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    RouteMatchInterface $route_match,
    DateFormatterInterface $date_formatter,
    Renderer $renderer,
    CloudServiceInterface $cloud_service,
    PrivateTempStoreFactory $temp_store_factory,
    ClientInterface $http_client,
  ) {

    parent::__construct(
      $ec2_service,
      $entity_repository,
      $entity_type_bundle_info,
      $time,
      $messenger,
      $entity_link_renderer,
      $entity_type_manager,
      $cacheRender,
      $plugin_cache_clearer,
      $cloud_config_plugin_manager,
      $current_user,
      $route_match,
      $date_formatter,
      $renderer,
      $cloud_service
    );

    $this->tempStore = $temp_store_factory->get($this->getTempStoreId());
    $this->httpClient = $http_client;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return OpenStackStackCreateForm
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openstack.ec2'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('entity.link_renderer'),
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.cache_clearer'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('cloud'),
      $container->get('tempstore.private'),
      $container->get('http_client')
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::buildForm().
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $cloud_context
   *   A cloud_context string value from URL "path".
   *
   * @return array
   *   Array of form object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {

    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $weight = -50;

    $form['stack'] = [
      '#type' => 'details',
      '#title' => $this->t('@title', ['@title' => $entity->getEntityType()->getSingularLabel()]),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['stack']['template_source'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Template source'),
      '#required'      => TRUE,
      '#options'       => [
        'file'         => $this->t('File'),
        'raw'          => $this->t('Direct Input'),
        'url'          => $this->t('URL'),
      ],
    ];

    $form['stack']['template_file_container'] = [
      '#type'          => 'container',
      '#states'        => [
        'visible' => [
          [
            ':input[name=template_source]' => ['value' => 'file'],
          ],
        ],
      ],
    ];

    $form['stack']['template_file_container']['template_file'] = [
      '#type'              => 'managed_file',
      '#title'             => $this->t('Template File'),
      '#required'          => FALSE,
      '#upload_validators' => [
        'file_validate_extensions'     => ['yml yaml YML YAML'],
        'openstack_file_validate_yaml' => [],
      ],
      '#element_validate'  => ['::validateTemplateFile'],
    ];

    $form['stack']['template_data'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Template Data'),
      '#required'      => FALSE,
      '#element_validate'  => ['::validateTemplateData'],
      '#states'        => [
        'visible' => [
          [
            ':input[name=template_source]' => ['value' => 'raw'],
          ],
        ],
      ],
    ];

    $form['stack']['template_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Template URL'),
      '#required'      => FALSE,
      '#element_validate'  => ['::validateTemplateUrl'],
      '#states'        => [
        'visible' => [
          [
            ':input[name=template_source]' => ['value' => 'url'],
          ],
        ],
      ],
    ];

    $form['stack']['environment_source'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Environment source'),
      '#required'      => FALSE,
      '#empty_value'   => '',
      '#options'       => [
        'file'         => $this->t('File'),
        'raw'          => $this->t('Direct input'),
      ],
    ];

    $form['stack']['environment_file_container'] = [
      '#type'          => 'container',
      '#states'        => [
        'visible' => [
          [
            ':input[name=environment_source]' => ['value' => 'file'],
          ],
        ],
      ],
    ];

    $form['stack']['environment_file_container']['environment_file'] = [
      '#type'               => 'managed_file',
      '#title'              => $this->t('Environment File'),
      '#required'           => FALSE,
      '#upload_validators'  => [
        'file_validate_extensions'     => ['yml yaml YML YAML'],
        'openstack_file_validate_yaml' => [],
      ],
      '#element_validate'  => ['::validateEnvironmentFile'],
    ];

    $form['stack']['environment_data'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Environment Data'),
      '#required'      => FALSE,
      '#element_validate'  => ['::validateEnvironmentData'],
      '#states'        => [
        'visible' => [
          [
            ':input[name=environment_source]' => ['value' => 'raw'],
          ],
        ],
      ],
    ];

    $this->addOthersFieldset($form, $weight++, $cloud_context);
    unset($form['others']['uid']);

    $form['actions']['submit']['#value'] = $this->t('Next');

    return $form;
  }

  /**
   * Validate callback for field template_file.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form for the form this element belongs to.
   */
  public function validateTemplateFile(array $element, FormStateInterface $form_state): void {
    if ($form_state->getValue('template_source') !== 'file') {
      return;
    }

    $values = $form_state->getValue('template_file', []);
    if (empty($values['fids'])) {
      $form_state->setError($element, $this->t('The template file is required.'));
    }
  }

  /**
   * Validate callback for field template_data.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form for the form this element belongs to.
   */
  public function validateTemplateData(array $element, FormStateInterface $form_state): void {
    if ($form_state->getValue('template_source') !== 'raw') {
      return;
    }

    if (empty($template_data = $form_state->getValue('template_data'))) {
      $form_state->setError($element, $this->t('The template data is required.'));
      return;
    }

    $template_yaml = [];
    try {
      $template_yaml = Yaml::decode($template_data);
    }
    catch (\Exception $e) {
      $form_state->setError($element, $this->t('The template data is invalid YAML. The YAML error: @message', [
        '@message' => $e->getMessage(),
      ]));
      return;
    }

    if (empty($template_yaml) || !is_array($template_yaml)) {
      $form_state->setError($element, $this->t('The template data is not formatted correctly.'));
      return;
    }
  }

  /**
   * Validate callback for field template_url.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form for the form this element belongs to.
   */
  public function validateTemplateUrl(array $element, FormStateInterface $form_state): void {
    if ($form_state->getValue('template_source') !== 'url') {
      return;
    }

    if (empty($template_url = $form_state->getValue('template_url'))) {
      $form_state->setError($element, $this->t('The template URL is required.'));
      return;
    }

    $content = NULL;
    try {
      $content = (string) $this->httpClient->request('GET', $template_url)->getBody();
    }
    catch (\Exception $e) {
      $form_state->setError($element, $this->t("The template content can't be retrieved. The error: @message", [
        '@message' => $e->getMessage(),
      ]));
      return;
    }

    if (empty($content)) {
      $form_state->setError($element, $this->t('The template content is empty.'));
      return;
    }

    try {
      Yaml::decode($content);
    }
    catch (\Exception $e) {
      $form_state->setError($element, $this->t('The template content is invalid YAML. The YAML error: @message', [
        '@message' => $e->getMessage(),
      ]));
    }
  }

  /**
   * Callback for field environment_file.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form for the form this element belongs to.
   */
  public function validateEnvironmentFile(array $element, FormStateInterface $form_state): void {
    if ($form_state->getValue('environment_source') !== 'file') {
      return;
    }

    $values = $form_state->getValue('environment_file', []);
    if (empty($values['fids'])) {
      $form_state->setError($element, $this->t('The environment file is required.'));
    }
  }

  /**
   * Validate callback for field environment_data.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form for the form this element belongs to.
   */
  public function validateEnvironmentData(array $element, FormStateInterface $form_state): void {
    if ($form_state->getValue('environment_source') !== 'raw') {
      return;
    }

    if (empty($environment_data = $form_state->getValue('environment_data'))) {
      $form_state->setError($element, $this->t('The environment data is required.'));
      return;
    }

    try {
      Yaml::decode($environment_data);
    }
    catch (\Exception $e) {
      $form_state->setError($element, $this->t('The environment data is invalid YAML. The YAML error: @message', [
        '@message' => $e->getMessage(),
      ]));
    }
  }

  /**
   * Submit handler.
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function save(array $form, FormStateInterface $form_state): void {
    /** @var \Drupal\openstack\Entity\OpenStackStack $entity */
    $entity = $this->entity;

    // Template.
    switch ($form_state->getValue('template_source')) {
      case 'file':
        $this->tempStore->set('template', $this->decodeYamlContent($this->getContentAndDeleteFile($form_state, 'template_file')));

        break;

      case 'raw':
        $this->tempStore->set('template', $this->decodeYamlContent($form_state->getValue('template_data')));
        break;

      case 'url':
        $url = $form_state->getValue('template_url');
        $this->tempStore->set('template', $this->decodeYamlContent($this->getRemoteContent($url)));
        $this->tempStore->set('template_url', $url);
        break;

      default:
        break;
    }

    // Environment.
    switch ($form_state->getValue('environment_source')) {
      case 'file':
        $this->tempStore->set('environment', Yaml::decode($this->getContentAndDeleteFile($form_state, 'environment_file')));

        break;

      case 'raw':
        $this->tempStore->set('environment', Yaml::decode($form_state->getValue('environment_data')));
        break;

      default:
        break;
    }

    $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.add_extra_form", [
      'cloud_context' => $entity->getCloudContext(),
    ]);
  }

  /**
   * Get ID of temp store.
   *
   * @return string
   *   The ID of temp store.
   */
  protected function getTempStoreId(): string {
    return 'openstack_stack_create';
  }

  /**
   * Get content from a file and delete the file.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form for the form this element belongs to.
   * @param string $element_name
   *   The element name.
   *
   * @return string
   *   The file content.
   */
  private function getContentAndDeleteFile(FormStateInterface $form_state, string $element_name): string {
    $values = $form_state->getValue($element_name, []);
    if (empty($values['fids'])) {
      return '';
    }

    /** @var \Drupal\file\FileInterface|null $file */
    $file = $this->entityTypeManager
      ->getStorage('file')
      ->load($values['fids'][0]);
    if (empty($file)) {
      return '';
    }

    $content = file_get_contents($file->getFileUri());
    $file->delete();
    return $content;
  }

  /**
   * Get content via URL.
   *
   * @param string $url
   *   The URL.
   *
   * @return string
   *   The content.
   */
  private function getRemoteContent(string $url): string {
    $content = '';
    try {
      $content = (string) $this->httpClient->request('GET', $url)->getBody();
    }
    catch (\Exception $e) {
    }

    return $content;
  }

  /**
   * Decode YAML content.
   *
   * Deal with the special case "heat_template_version: 2013-05-23".
   *
   * @param string $content
   *   The YAML content.
   *
   * @return array
   *   The YAML array.
   */
  private function decodeYamlContent(string $content): array {
    $yaml = Yaml::decode($content);
    if (!empty('heat_template_version') && is_int($yaml['heat_template_version'])) {
      $yaml['heat_template_version'] = gmdate('Y-m-d', $yaml['heat_template_version']);
    }
    return $yaml;
  }

}
