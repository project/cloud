<?php

namespace Drupal\openstack\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the Stack entity create form.
 *
 * @ingroup openstack
 */
class OpenStackStackEditExtraForm extends OpenStackStackCreateExtraForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {

    $form = parent::buildForm($form, $form_state, $cloud_context);
    $entity = $this->entity;

    $form['stack']['name'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Name')),
      '#markup'        => $entity->label(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->copyFormItemValues($form);

    $this->openStackOperationsService->updateOpenStackStack($this->entity, $form, $form_state, [
      'template'      => $this->tempStore->get('template'),
      'template_url'  => $this->tempStore->get('template_url'),
      'environment'   => $this->tempStore->get('environment'),
    ]);

    $this->tempStore->delete('template');
    $this->tempStore->delete('template_url');
    $this->tempStore->delete('environment');
  }

  /**
   * {@inheritdoc}
   */
  protected function getTempStoreId(): string {
    return 'openstack_stack_edit';
  }

}
