<?php

namespace Drupal\openstack\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the Stack entity edit form.
 *
 * @ingroup openstack
 */
class OpenStackStackEditForm extends OpenStackStackCreateForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    parent::save($form, $form_state);

    $entity = $this->entity;
    $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.edit_extra_form", [
      'cloud_context' => $entity->getCloudContext(),
      'openstack_stack' => $entity->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getTempStoreId(): string {
    return 'openstack_stack_edit';
  }

}
