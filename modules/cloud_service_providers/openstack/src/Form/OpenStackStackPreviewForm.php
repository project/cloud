<?php

namespace Drupal\openstack\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the Stack entity preview form.
 *
 * @ingroup openstack
 */
class OpenStackStackPreviewForm extends OpenStackStackCreateForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    parent::save($form, $form_state);

    $entity = $this->entity;
    $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.preview_extra_form", [
      'cloud_context' => $entity->getCloudContext(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getTempStoreId(): string {
    return 'openstack_stack_preview';
  }

}
