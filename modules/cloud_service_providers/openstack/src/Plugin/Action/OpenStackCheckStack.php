<?php

namespace Drupal\openstack\Plugin\Action;

use Drupal\cloud\Plugin\Action\OperateAction;

/**
 * Check selected Stack(s).
 *
 * @Action(
 *   id = "openstack_stack_check_action",
 *   label = @Translation("Check Stack"),
 *   type = "openstack_stack",
 *   confirm_form_route_name
 *     = "entity.openstack_stack.check_multiple_form"
 * )
 */
class OpenStackCheckStack extends OperateAction {

  /**
   * {@inheritdoc}
   */
  protected function getOperation(): string {
    return 'check';
  }

}
