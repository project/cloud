<?php

namespace Drupal\openstack\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a network deletion form.
 *
 * @Action(
 *   id = "entity:delete_action:openstack_network",
 *   label = @Translation("Delete Network"),
 *   type = "openstack_network"
 * )
 */
class OpenStackDeleteNetwork extends DeleteAction {

}
