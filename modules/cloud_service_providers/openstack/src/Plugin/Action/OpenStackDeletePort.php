<?php

namespace Drupal\openstack\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a port deletion form.
 *
 * @Action(
 *   id = "entity:delete_action:openstack_port",
 *   label = @Translation("Delete Port"),
 *   type = "openstack_port"
 * )
 */
class OpenStackDeletePort extends DeleteAction {

}
