<?php

namespace Drupal\openstack\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a router deletion form.
 *
 * @Action(
 *   id = "entity:delete_action:openstack_router",
 *   label = @Translation("Delete Router"),
 *   type = "openstack_router"
 * )
 */
class OpenStackDeleteRouter extends DeleteAction {

}
