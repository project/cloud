<?php

namespace Drupal\openstack\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a server group deletion form.
 *
 * @Action(
 *   id = "entity:delete_action:openstack_server_group",
 *   label = @Translation("Delete server group"),
 *   type = "openstack_server_group"
 * )
 */
class OpenStackDeleteServerGroup extends DeleteAction {

}
