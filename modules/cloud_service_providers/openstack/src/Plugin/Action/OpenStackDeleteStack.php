<?php

namespace Drupal\openstack\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a stack deletion form.
 *
 * @Action(
 *   id = "entity:delete_action:openstack_stack",
 *   label = @Translation("Delete Stack"),
 *   type = "openstack_stack"
 * )
 */
class OpenStackDeleteStack extends DeleteAction {

}
