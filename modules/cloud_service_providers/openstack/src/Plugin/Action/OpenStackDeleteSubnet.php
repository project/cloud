<?php

namespace Drupal\openstack\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a subnet deletion form.
 *
 * @Action(
 *   id = "entity:delete_action:openstack_subnet",
 *   label = @Translation("Delete Subnet"),
 *   type = "openstack_subnet"
 * )
 */
class OpenStackDeleteSubnet extends DeleteAction {

}
