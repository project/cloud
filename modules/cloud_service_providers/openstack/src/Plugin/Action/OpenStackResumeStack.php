<?php

namespace Drupal\openstack\Plugin\Action;

use Drupal\cloud\Plugin\Action\OperateAction;

/**
 * Resume selected Stack(s).
 *
 * @Action(
 *   id = "openstack_stack_resume_action",
 *   label = @Translation("Resume Stack"),
 *   type = "openstack_stack",
 *   confirm_form_route_name
 *     = "entity.openstack_stack.resume_multiple_form"
 * )
 */
class OpenStackResumeStack extends OperateAction {

  /**
   * {@inheritdoc}
   */
  protected function getOperation(): string {
    return 'resume';
  }

}
