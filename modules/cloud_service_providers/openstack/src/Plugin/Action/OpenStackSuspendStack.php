<?php

namespace Drupal\openstack\Plugin\Action;

use Drupal\cloud\Plugin\Action\OperateAction;

/**
 * Suspend selected Stack(s).
 *
 * @Action(
 *   id = "openstack_stack_suspend_action",
 *   label = @Translation("Suspend Stack"),
 *   type = "openstack_stack",
 *   confirm_form_route_name
 *     = "entity.openstack_stack.suspend_multiple_form"
 * )
 */
class OpenStackSuspendStack extends OperateAction {

  /**
   * {@inheritdoc}
   */
  protected function getOperation(): string {
    return 'suspend';
  }

}
