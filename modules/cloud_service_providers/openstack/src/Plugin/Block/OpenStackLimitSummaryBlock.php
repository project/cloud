<?php

namespace Drupal\openstack\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a block displaying limit summary.
 *
 * @Block(
 *   id = "openstack_limit_summary_block",
 *   admin_label = @Translation("OpenStack limit summary"),
 *   category = @Translation("OpenStack")
 * )
 */
class OpenStackLimitSummaryBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The page cache kill switch service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Creates a OpenStackLimitSummaryBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   The page cache kill switch service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RequestStack $request_stack,
    EntityTypeManager $entity_type_manager,
    KillSwitch $kill_switch,
    ModuleHandlerInterface $module_handler,
    AccountInterface $current_user,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->request = $request_stack->getCurrentRequest();
    $this->entityTypeManager = $entity_type_manager;
    $this->killSwitch = $kill_switch;
    $this->moduleHandler = $module_handler;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('page_cache_kill_switch'),
      $container->get('module_handler'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];

    // Check permission.
    if (!$this->currentUser->hasPermission('list openstack quota')) {
      return $build;
    }

    $cloud_context = $this->request->get('cloud_context');
    $params = !empty($cloud_context) ? ['cloud_context' => $cloud_context] : [];

    $quotas = $this->entityTypeManager
      ->getStorage('openstack_quota')
      ->loadByProperties($params);

    $account = $this->currentUser;
    $quotas = array_filter(
      $quotas,
      static fn($quota) => $quota->access('view', $account, TRUE)->isAllowed()
    );

    if (empty($quotas)) {
      return $build;
    }

    $build['compute'] = [
      '#type' => 'details',
      '#title' => $this->t('Compute'),
      '#open' => TRUE,
    ];

    $build['compute']['pie_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['row'],
      ],
    ];

    $build['compute']['pie_container']['instance'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('Instances'),
      '#used' => $this->sumByMethodName($quotas, 'getInstancesUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getInstances'),
    ];

    $build['compute']['pie_container']['vcpu'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('vCPUs'),
      '#used' => $this->sumByMethodName($quotas, 'getCoresUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getCores'),
    ];

    $build['compute']['pie_container']['ram'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('RAM'),
      '#used' => $this->sumByMethodName($quotas, 'getRamUsage') / 1024,
      '#total' => $this->sumByMethodName($quotas, 'getRam') / 1024,
      '#unit' => $this->t('GB'),
    ];

    $build['volume'] = [
      '#type' => 'details',
      '#title' => $this->t('Volume'),
      '#open' => TRUE,
    ];

    $build['volume']['pie_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['row'],
      ],
    ];

    $build['volume']['pie_container']['volume'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('Volumes'),
      '#used' => $this->sumByMethodName($quotas, 'getVolumesUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getVolumes'),
    ];

    $build['volume']['pie_container']['volume_snapshot'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('Volume snapshots'),
      '#used' => $this->sumByMethodName($quotas, 'getSnapshotsUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getSnapshots'),
    ];

    $build['volume']['pie_container']['volume_storage'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('Volume storage'),
      '#used' => $this->sumByMethodName($quotas, 'getGigabytesUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getGigabytes'),
      '#unit' => $this->t('GB'),
    ];

    $build['network'] = [
      '#type' => 'details',
      '#title' => $this->t('Network'),
      '#open' => TRUE,
    ];

    $build['network']['pie_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['row'],
      ],
    ];

    $build['network']['pie_container']['floating_ip'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('Floating IPs'),
      '#used' => $this->sumByMethodName($quotas, 'getFloatingIpUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getFloatingIp'),
      '#description_template' => $this->t('Allocated %d%s of %d%s'),
    ];

    $build['network']['pie_container']['security_group'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('Security groups'),
      '#used' => $this->sumByMethodName($quotas, 'getSecurityGroupUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getSecurityGroup'),
    ];

    $build['network']['pie_container']['security_group_rule'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('Security group rules'),
      '#used' => $this->sumByMethodName($quotas, 'getSecurityGroupRuleUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getSecurityGroupRule'),
    ];

    $build['network']['pie_container']['network'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('Networks'),
      '#used' => $this->sumByMethodName($quotas, 'getNetworkUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getNetwork'),
    ];

    $build['network']['pie_container']['port'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('Ports'),
      '#used' => $this->sumByMethodName($quotas, 'getPortUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getPort'),
    ];

    $build['network']['pie_container']['router'] = [
      '#theme' => 'usage_pie_chart',
      '#title' => $this->t('Routers'),
      '#used' => $this->sumByMethodName($quotas, 'getRouterUsage'),
      '#total' => $this->sumByMethodName($quotas, 'getRouter'),
    ];

    $build['#attached']['library'] = 'openstack/openstack_limit_summary';

    return $build;
  }

  /**
   * Sum the result of method call.
   *
   * @param array $quotas
   *   The quota entities.
   * @param string $method_name
   *   The method name.
   *
   * @return int
   *   The result of sum.
   */
  private function sumByMethodName(array $quotas, $method_name): int {
    return array_sum(array_map(
      static fn($quota) => $quota->$method_name(),
      $quotas
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    $this->killSwitch->trigger();
    // BigPipe/#cache/max-age is breaking my block javascript
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2016-07-17/bigpipecachemax-age-is-breaking-my
    // "a slight delay of a second or two before the charts are built.
    // That seems to work, though it is a junky solution.".
    return $this->moduleHandler->moduleExists('big_pipe') ? 1 : 0;
  }

}
