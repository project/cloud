<?php

namespace Drupal\openstack\Plugin\Block;

use Drupal\cloud\Plugin\Block\CloudBaseResourcesBlock;

/**
 * Provides a block displaying OpenStack resources.
 *
 * @Block(
 *   id = "openstack_resources_block",
 *   admin_label = @Translation("OpenStack resources"),
 *   category = @Translation("OpenStack")
 * )
 */
class OpenStackResourcesBlock extends CloudBaseResourcesBlock {

  /**
   * {@inheritdoc}
   */
  protected function getEntityBundleType(): string {
    return 'openstack';
  }

  /**
   * Build resource table.
   *
   * @return array
   *   table render array.
   */
  protected function buildResourceTable(): array {
    $resources = [
      'openstack_instance' => [
        'view any openstack instance',
        [],
      ],
      'openstack_image' => [
        'view any openstack image',
        [],
      ],
      'openstack_security_group' => [
        'view any openstack security group',
        [],
      ],
      'openstack_floating_ip' => [
        'view any openstack floating ip',
        [],
      ],
      'openstack_key_pair' => [
        'view any openstack cloud key pair',
        [],
      ],
      'openstack_volume' => [
        'view any openstack volume',
        [],
      ],
      'openstack_snapshot' => [
        'view any openstack snapshot',
        [],
      ],
      'openstack_network' => [
        'view any openstack network',
        [],
      ],
      'openstack_subnet' => [
        'view any openstack subnet',
        [],
      ],
      'openstack_port' => [
        'view any openstack port',
        [],
      ],
      'openstack_router' => [
        'view any openstack router',
        [],
      ],
      'openstack_stack' => [
        'view any openstack stack',
        [],
      ],
      'openstack_server_group' => [
        'view openstack server group',
        [],
      ],
      'openstack_quota' => [
        'view openstack quota',
        [],
      ],
      'openstack_project' => [
        'view openstack project',
        [],
      ],
      'openstack_user' => [
        'view openstack user',
        [],
      ],
      'openstack_role' => [
        'view openstack role',
        [],
      ],
    ];

    $rows = $this->buildResourceTableRows($resources);

    return [
      '#type' => 'table',
      '#rows' => $rows,
    ];
  }

}
