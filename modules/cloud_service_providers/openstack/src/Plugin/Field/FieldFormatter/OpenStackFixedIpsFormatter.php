<?php

namespace Drupal\openstack\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Service\Util\EntityLinkWithNameHtmlGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'openstack_fixed_ips_formatter' formatter.
 *
 * @todo To be deleted.
 *
 * @FieldFormatter(
 *   id = "openstack_fixed_ips_formatter",
 *   label = @Translation("OpenStack fixed IPs formatter"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class OpenStackFixedIpsFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  protected $entityLinkRenderer;

  /**
   * Constructs an EntityLinkFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    EntityLinkRendererInterface $entity_link_renderer,
  ) {

    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings);

    $this->entityLinkRenderer = $entity_link_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity.link_renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $rows = [];

    foreach ($items ?: [] as $item) {
      /** @var \Drupal\aws_cloud\Plugin\Field\FieldType\Tag $item */
      if ($item->isEmpty()) {
        continue;
      }

      $parts = explode(',', $item->value);
      $rows[] = [
        $parts[0],
        [
          'data' => $this->entityLinkRenderer->renderViewElement(
            $parts[1],
            'openstack_subnet',
            'subnet_id',
            [],
            '',
            EntityLinkWithNameHtmlGenerator::class
          ),
        ],
      ];
    }

    if (count($rows)) {
      $elements[0] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('IP address'),
          $this->t('Subnet ID'),
        ],
        '#rows' => $rows,
      ];
    }

    return $elements;
  }

}
