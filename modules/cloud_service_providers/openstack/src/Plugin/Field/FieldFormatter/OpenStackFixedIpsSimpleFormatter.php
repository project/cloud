<?php

namespace Drupal\openstack\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin of the 'openstack_fixed_ips_simple_formatter' formatter.
 *
 * @todo To be deleted.
 *
 * @FieldFormatter(
 *   id = "openstack_fixed_ips_simple_formatter",
 *   label = @Translation("OpenStack fixed IPs simple formatter"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class OpenStackFixedIpsSimpleFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $ips = [];
    foreach ($items ?: [] as $item) {
      /** @var \Drupal\aws_cloud\Plugin\Field\FieldType\Tag $item */
      if ($item->isEmpty()) {
        continue;
      }

      $parts = explode(',', $item->value);
      $ips[] = $parts[0];
    }

    if (!empty($ips)) {
      $elements[0] = [
        '#markup' => implode(', ', $ips),
      ];
    }

    return $elements;
  }

}
