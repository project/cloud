<?php

namespace Drupal\openstack\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\openstack\Service\OpenStackServiceInterface;

/**
 * Plugin implementation of the 'usage_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "quota_usage_formatter",
 *   label = @Translation("OpenStack quota usage formatter"),
 *   field_types = {
 *     "integer",
 *   }
 * )
 */
class OpenStackQuotaUsageFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $entity = $items->getEntity();
    foreach ($items ?: [] as $delta => $item) {
      if ($item->isEmpty()) {
        continue;
      }
      if (empty(OpenStackServiceInterface::QUOTA_LIMIT_FIELD_CAMEL_CASE[$items->getName()])) {
        $elements[$delta] = $item->value;
        continue;
      }
      $limit_camel_case = OpenStackServiceInterface::QUOTA_LIMIT_FIELD_CAMEL_CASE[$items->getName()];
      $limit_function = "get{$limit_camel_case}";
      if (!method_exists($entity, $limit_function)) {
        $elements[$delta] = $item->value;
        continue;
      }
      $limit = $entity->$limit_function();
      if (is_null($limit)) {
        $elements[$delta] = ['#markup' => "-1 (N/A)"];
        continue;
      }
      $usage = $item->value;
      $usage_str = $limit < 0
        ? "$usage (No Limit)"
        : $this->t('@usage of @limit', [
          '@usage' => $usage,
          '@limit' => $limit,
        ]);
      $percentage = $limit > 0
        ? sprintf(' (%.1f%%)', $usage / $limit * 100)
        : '';
      $elements[$delta] = ['#markup' => "$usage_str$percentage"];
    }

    return $elements;
  }

}
