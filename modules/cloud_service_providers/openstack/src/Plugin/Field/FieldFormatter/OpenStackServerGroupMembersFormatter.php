<?php

namespace Drupal\openstack\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\openstack\Entity\OpenStackInstance;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'server_group_members_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "server_group_members_formatter",
 *   label = @Translation("OpenStack server group members formatter"),
 *   field_types = {
 *     "key_value"
 *   }
 * )
 */
class OpenStackServerGroupMembersFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * Constructs an EntityLinkFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The cloud service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    RouteMatchInterface $route_match,
    CloudServiceInterface $cloud_service,
  ) {

    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );

    $this->routeMatch = $route_match;
    $this->cloudService = $cloud_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_route_match'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $entity = $items->getEntity();
    $cloud_context = $entity->getCloudContext();
    if (empty($cloud_context)) {
      $cloud_context = $this->routeMatch->getParameter('cloud_context');
    }

    $elements = [];
    $rows = [];

    $instances = [];
    foreach ($items ?: [] as $item) {
      $instances[] = $item->item_key;
    }

    if (empty($instances) || empty(count($instances))) {
      return $elements;
    }

    foreach ($instances ?: [] as $instance) {
      if (empty($instance)) {
        continue;
      }

      $instance_entity_id = $this->cloudService->getEntityId(
        $cloud_context,
        'openstack_instance',
        'instance_id',
        $instance
      );
      $instance_entity = OpenStackInstance::load($instance_entity_id);

      $instance_name = '';
      if (!empty($instance_entity_id) && !empty($instance_entity)) {
        $instance_name = Link::createFromRoute(
          $instance_entity->getName(),
          "entity.openstack_instance.canonical", [
            'cloud_context' => $cloud_context,
            'openstack_instance' => $instance_entity_id,
          ]
        )->toString();
      }

      $rows[] = [
        $instance_name,
        $instance,
      ];
    }

    if (count($rows)) {
      $elements[0] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('Instance name'),
          $this->t('Instance ID'),
        ],
        '#rows' => $rows,
      ];
    }

    return $elements;
  }

}
