<?php

namespace Drupal\openstack\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\openstack\Entity\OpenStackRole;
use Drupal\openstack\Entity\OpenStackUser;

/**
 * Plugin implementation of the 'user_roles_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "user_roles_formatter",
 *   label = @Translation("User roles formatter"),
 *   field_types = {
 *     "user_roles"
 *   }
 * )
 */
class UserRolesFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element = [];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $user_roles_list = [];
    foreach ($items ?: [] as $item) {
      if ($item->isEmpty()) {
        continue;
      }

      $user_id = $item->user;
      $role_ids = explode(',', $item->roles);

      $user = OpenStackUser::load($user_id);
      if (empty($user)) {
        continue;
      }

      $roles = [];
      foreach ($role_ids ?: [] as $role_id) {
        $role = OpenStackRole::load($role_id);
        if (empty($role)) {
          continue;
        }

        $roles[] = $role;
      }

      // Sort $roles by the label of role.
      usort($roles, static function ($a, $b) {
        return ($a->label() < $b->label()) ? -1 : 1;
      });

      $user_roles_list[] = [
        $user,
        $roles,
      ];
    }

    // Sort $user_roles by the label of user.
    usort($user_roles_list, static function ($a, $b) {
      return ($a[0]->label() < $b[0]->label()) ? -1 : 1;
    });

    $rows = [];
    foreach ($user_roles_list ?: [] as $user_roles) {
      $user = $user_roles[0];
      $user_url = $user->toLink($user->label());

      $role_urls = [];
      foreach ($user_roles[1] ?: [] as $role) {
        $role_urls[] = $role->toLink($role->label())->toString();
      }

      $rows[] = [
        $user_url,
        [
          'data' => [
            '#markup' => implode(', ', $role_urls),
          ],
        ],
      ];
    }

    $elements = [];
    if (count($rows)) {
      $elements[0] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('User'),
          $this->t('Roles'),
        ],
        '#rows' => $rows,
      ];
    }

    return $elements;
  }

}
