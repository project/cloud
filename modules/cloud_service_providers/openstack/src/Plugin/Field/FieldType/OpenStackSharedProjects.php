<?php

namespace Drupal\openstack\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'shared_projects' field type.
 *
 * @FieldType(
 *   id = "shared_projects",
 *   label = @Translation("Shared Projects"),
 *   description = @Translation("Shared projects field."),
 *   default_widget = "shared_projects_item",
 *   default_formatter = "shared_projects_formatter"
 * )
 */
class OpenStackSharedProjects extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    return [
      'max_length' => 255,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['cloud_context'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Cloud context'))
      ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'))
      ->setRequired(TRUE);

    $properties['project_id'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Project'))
      ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'))
      ->setRequired(FALSE);

    $properties['status'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Status'))
      ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $schema = [
      'columns' => [
        'cloud_context' => [
          'type' => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
        ],
        'project_id' => [
          'type' => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
        ],
        'status' => [
          'type' => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    $elements = [];

    $elements['max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum length'),
      '#default_value' => $this->getSetting('max_length'),
      '#required' => TRUE,
      '#description' => $this->t('The maximum length of the field in characters.'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return empty($this->get('cloud_context')->getValue())
      || empty($this->get('project_id')->getValue())
      || empty($this->get('status')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE): void {
    // Treat the values as property value of the main property, if no array is
    // given.
    if (isset($values) && !is_array($values)) {
      $values = [static::mainPropertyName() => $values];
    }
    if (is_array($values)) {
      $values += [
        'options' => [],
      ];
    }
    // Unserialize the values.
    // @todo The storage controller should take care of this, see
    //   SqlContentEntityStorage::loadFieldItems, see
    //   https://www.drupal.org/node/2414835
    if (!empty($values['options']) && is_string($values['options'])) {
      $values['options'] = unserialize($values['options'], ['allowed_classes' => FALSE]);
    }
    parent::setValue($values, $notify);
  }

  /**
   * Get the cloud context.
   */
  public function getCloudContext(): ?string {
    return $this->get('cloud_context')->getValue();
  }

  /**
   * Get the project id.
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->getValue();
  }

  /**
   * Get the status.
   */
  public function getStatus(): ?string {
    return $this->get('status')->getValue();
  }

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return $this
   */
  public function setCloudContext($cloud_context): OpenStackSharedProjects {
    return $this->set('cloud_context', $cloud_context);
  }

  /**
   * Set the project id.
   *
   * @param string $project_id
   *   The project id.
   *
   * @return $this
   */
  public function setProjectId($project_id): OpenStackSharedProjects {
    return $this->set('project_id', $project_id);
  }

  /**
   * Set the status.
   *
   * @param string $status
   *   The status.
   *
   * @return $this
   */
  public function setStatus(string $status): OpenStackSharedProjects {
    return $this->set('status', $status);
  }

}
