<?php

namespace Drupal\openstack\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'user_roles' field type.
 *
 * @FieldType(
 *   id = "user_roles",
 *   label = @Translation("User Roles"),
 *   description = @Translation("User roles field."),
 *   default_widget = "user_roles_item",
 *   default_formatter = "user_roles_formatter"
 * )
 */
class UserRoles extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    return [
      'max_length' => 255,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['user'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('User'))
      ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'))
      ->setRequired(TRUE);

    $properties['roles'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Roles'))
      ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $schema = [
      'columns' => [
        'user' => [
          'type' => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
        ],
        'roles' => [
          'type' => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    $elements = [];

    $elements['max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum length'),
      '#default_value' => $this->getSetting('max_length'),
      '#required' => TRUE,
      '#description' => $this->t('The maximum length of the field in characters.'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return empty($this->get('user')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE): void {
    // Treat the values as property value of the main property, if no array is
    // given.
    if (isset($values) && !is_array($values)) {
      $values = [static::mainPropertyName() => $values];
    }

    // Convert roles to string.
    if (isset($values['roles']) && is_array($values['roles'])) {
      $values['roles'] = implode(',', $values['roles']);
    }

    if (isset($values)) {
      $values += [
        'options' => [],
      ];
    }

    if (is_string($values['options'])) {
      $values['options'] = unserialize($values['options'], ['allowed_classes' => FALSE]);
    }

    parent::setValue($values, $notify);
  }

  /**
   * Get the user.
   */
  public function getUser(): ?string {
    return $this->get('user')->getValue();
  }

  /**
   * Get the roles.
   */
  public function getRoles(): array {
    $roles = $this->get('roles')->getValue();
    return !empty($roles) ? explode(',', $roles) : [];
  }

  /**
   * Set the user.
   *
   * @param string $user
   *   The user.
   *
   * @return $this
   */
  public function setUser($user): UserRoles {
    return $this->set('user', $user);
  }

  /**
   * Set the roles.
   *
   * @param array $roles
   *   The value.
   *
   * @return $this
   */
  public function setRoles(array $roles): UserRoles {
    return $this->set('roles', implode(',', $roles));
  }

}
