<?php

namespace Drupal\openstack\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\aws_cloud\Plugin\Field\FieldWidget\IpPermissionItem;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Drupal\openstack\Service\OpenStackServiceInterface;
use Drupal\openstack\Service\Rest\OpenStackService as OpenStackRestService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'ip_permission_item' widget.
 *
 * @FieldWidget(
 *   id = "ip_permission_item",
 *   label = @Translation("OpenStack IP permission"),
 *   field_types = {
 *     "ip_permission"
 *   }
 * )
 */
class OpenStackIpPermissionItem extends IpPermissionItem implements ContainerFactoryPluginInterface {

  /**
   * The OpenStack Service.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  private $openStackServiceFactory;

  /**
   * The EC2 Service.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  private $ec2Service;

  /**
   * Constructs an EntityLinkFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    OpenStackServiceFactoryInterface $openstack_service_factory,
  ) {

    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings);

    $this->openStackServiceFactory = $openstack_service_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('openstack.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    /** @var \Drupal\Core\Entity\Plugin\DataType\EntityAdapter $parent */
    $parent = $items->getParent();
    $security_group = $parent->getValue();

    $cloud_context = $security_group->getCloudContext();
    // Switch OpenStack EC2 or REST service based on $cloud_context.
    $this->ec2Service = $this->openStackServiceFactory->get($cloud_context);

    $protocols = [
      'tcp' => $this->t('TCP'),
      'udp' => $this->t('UDP'),
      'icmp' => $this->t('ICMP'),
    ];

    $source = [
      'ip4' => 'IP',
      'group' => 'Group',
    ];

    // Add IPv6 protocols if the group is a vpc.
    if (!empty($security_group->getVpcId())) {
      $protocols = [
        '-1' => $this->t('All traffic'),
        'tcp' => $this->t('TCP'),
        'udp' => $this->t('UDP'),
        'icmp' => $this->t('ICMP'),
        'icmpv6' => $this->t('ICMPv6'),
      ];

      $source = [
        'ip4' => 'IP',
        'ip6' => 'IPv6',
        'group' => 'Group',
        'prefix' => 'Prefix list Id',
      ];
    }

    if (empty($security_group->getVpcId()) && $this->ec2Service instanceof OpenStackRestService) {
      $protocols = [
        'tcp' => $this->t('TCP'),
        'udp' => $this->t('UDP'),
        'icmp' => $this->t('ICMP'),
        'icmpv6' => $this->t('ICMPv6'),
      ];

      // Add protocols with well-known ports to existing protocols.
      $well_known_ports = OpenStackServiceInterface::WELL_KNOWN_PORTS;
      foreach ($well_known_ports ?: [] as $key => $protocol_option) {
        $protocols[$key] = $protocol_option['name'];
      }

      $source = [
        'ip4' => 'IP',
        'ip6' => 'IPv6',
        'group' => 'Group',
      ];
    }

    $element['ip_protocol'] = [
      '#type' => 'select',
      '#title' => $this->t('IP protocol'),
      '#options' => $protocols,
      '#attributes' => [
        'class' => [
          'ip-protocol-select',
        ],
      ],
      '#default_value' => $items[$delta]->ip_protocol ?? 'tcp',
    ];

    $element['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Source'),
      '#options' => $source,
      '#attributes' => [
        'class' => [
          'ip-permission-select',
          'ip-type-select',
        ],
      ],
      '#default_value' => $items[$delta]->source ?? NULL,
    ];

    $element['rule_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rule Id'),
      '#default_value' => $items[$delta]->rule_id ?? NULL,
      '#placeholder' => 0,
    ];

    $element['#attached']['library'][] = 'openstack/openstack_security_groups';

    return $element;
  }

}
