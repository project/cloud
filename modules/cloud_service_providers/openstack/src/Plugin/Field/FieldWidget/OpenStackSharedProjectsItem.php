<?php

namespace Drupal\openstack\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'shared_projects_item' widget.
 *
 * @FieldWidget(
 *   id = "shared_projects_item",
 *   label = @Translation("Shared projects"),
 *   field_types = {
 *     "shared_projects"
 *   }
 * )
 */
class OpenStackSharedProjectsItem extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a OpenStackSharedProjectsItem instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManagerInterface $entity_type_manager,
  ) {

    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element = [];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = [];

    $element['cloud_context'] = [
      '#type' => 'select',
      '#title' => $this->t('Cloud context'),
      '#multiple' => FALSE,
      '#options' => $this->getCloudConfigOptions(),
      '#default_value' => key($this->getCloudConfigOptions($items[$delta]->cloud_context)) ?? '',
      '#prefix' => '<div class="row"><div class="col-sm-6">',
      '#suffix' => '</div>',
    ];

    $element['project_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project ID'),
      '#default_value' => $items[$delta]->project_id ?? '',
      '#prefix' => '<div class="col-sm-6">',
      '#suffix' => '</div></div>',
    ];

    return $element;
  }

  /**
   * Get cloud context options.
   *
   * @return array
   *   The cloud context options.
   */
  private function getCloudConfigOptions($cloud_context = ''): array {
    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
      ]);

    return !empty($cloud_configs) ? array_reduce(
        // Extract only the elements from the $cloud_configs
        // whose field_image_api_endpoint is not NULL.
        // or, extract only matching elements if $cloud_context is set.
        array_filter($cloud_configs, static function ($cloud_config) use ($cloud_context) {
          return $cloud_context === $cloud_config->getCloudContext()
            || (empty($cloud_context) && $cloud_config->get('field_image_api_endpoint')->value !== NULL);
        }),
        // Create the $options from the extracted elements.
        static function ($options, $cloud_config) {
          $options[$cloud_config->get('field_image_api_endpoint')->value] = $cloud_config->getCloudContext();
          return $options;
        },
        []
      ) : [];
  }

}
