<?php

namespace Drupal\openstack\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'user_roles_item' widget.
 *
 * @FieldWidget(
 *   id = "user_roles_item",
 *   label = @Translation("User roles"),
 *   field_types = {
 *     "user_roles"
 *   }
 * )
 */
class UserRolesItem extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a UserRolesItem instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManagerInterface $entity_type_manager,
  ) {

    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element = [];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = [];

    $parent = $items->getParent();
    $project = $parent->getValue();
    $cloud_context = $project->getCloudContext();

    $element['user'] = [
      '#type' => 'select',
      '#title' => $this->t('User'),
      '#options' => $this->getEntityOptions($cloud_context, 'openstack_user'),
      '#default_value' => $items[$delta]->user,
      '#empty_value' => '',
      '#prefix' => '<div class="row"><div class="col-sm-6">',
      '#suffix' => '</div>',
    ];

    $element['roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Roles'),
      '#multiple' => TRUE,
      '#options' => $this->getEntityOptions($cloud_context, 'openstack_role'),
      '#default_value' => !empty($items[$delta]->roles) ? explode(',', $items[$delta]->roles) : '',
      '#prefix' => '<div class="col-sm-6">',
      '#suffix' => '</div></div>',
    ];

    return $element;
  }

  /**
   * Get user options.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type
   *   The entity type.
   *
   * @return array
   *   The user options.
   */
  private function getEntityOptions($cloud_context, $entity_type): array {
    $properties['cloud_context'] = $cloud_context;

    $entities = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadByProperties($properties);

    // Sort by name.
    usort($entities, static function ($a, $b) {
      return $a->getName() < $b->getName() ? -1 : 1;
    });

    $options = [];
    foreach ($entities ?: [] as $entity) {
      $options[$entity->id()] = $entity->getName();
    }

    return $options;
  }

}
