<?php

namespace Drupal\openstack\Plugin\Field\Util;

use Drupal\cloud\Plugin\Field\Util\ReservedKeyCheckerInterface;

/**
 * OpenStack reserved key checker interface for key_value field type.
 */
class OpenStackReservedKeyChecker implements ReservedKeyCheckerInterface {

  private const RESERVED_KEYS = [
    'Name',
  ];

  /**
   * {@inheritdoc}
   */
  public function isReservedWord($key): bool {
    if (empty($key)) {
      return FALSE;
    }

    if (in_array($key, self::RESERVED_KEYS)) {
      return TRUE;
    }

    // Reserve for special tags.
    if (strpos($key, 'openstack:') === 0) {
      return TRUE;
    }

    // Reserve for openstack_*__uuid__context tags.
    if (preg_match('/^drupal-openstack.+[a-z_]+.+[a-z0-9_]+.+[a-z0-9-]+$/', $key)) {
      return TRUE;
    }

    // Reserve for cloud_server_template_*__uuid__context tags.
    if (preg_match('/^cloud_server_template_+[a-z_]+__+[a-z0-9_]+__+[a-z0-9-]+$/', $key)) {
      return TRUE;
    }

    // Reserve for termination_protection.
    if ($key === 'termination_protection') {
      return TRUE;
    }

    return FALSE;
  }

}
