<?php

namespace Drupal\openstack\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\aws_cloud\Service\S3\S3Service;
use Drupal\cloud\Plugin\QueueWorker\CloudBaseUpdateResourcesQueueWorker;
use Drupal\openstack\Entity\OpenStackImageInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes for OpenStack share images queue.
 *
 * @QueueWorker(
 *   id = "openstack_share_images_queue",
 *   title = @Translation("OpenStack Share Images Queue"),
 *   deriver = "Drupal\cloud\Plugin\Derivative\CloudUpdateQueueDerivative",
 * )
 */
class OpenStackShareImagesQueueWorker extends CloudBaseUpdateResourcesQueueWorker implements ContainerFactoryPluginInterface {

  /**
   * The OpenStack Service.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  private $openStackServiceFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * The Amazon S3 Service.
   *
   * @var \Drupal\aws_cloud\Service\S3\S3ServiceInterface
   */
  private $s3Service;

  /**
   * Constructs a new LocaleTranslation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory instance.
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\aws_cloud\Service\S3\S3Service $s3_service
   *   The S3 service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    LoggerChannelFactoryInterface $logger_factory,
    OpenStackServiceFactoryInterface $openstack_service_factory,
    EntityTypeManager $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    S3Service $s3_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger_factory->get('openstack_image_queue');
    $this->openStackServiceFactory = $openstack_service_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->fileSystem = $file_system;
    $this->s3Service = $s3_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('openstack.factory'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('aws_cloud.s3')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $cloud_context = $data['cloud_context'] ?? '';
    $image_id = $data['image_id'] ?? '';
    $to_cloud_context = $data['to_cloud_context'] ?? '';
    $to_image_id = $data['to_image_id'] ?? '';

    $images = $this->entityTypeManager
      ->getStorage('openstack_image')
      ->loadByProperties([
        'image_id' => $image_id,
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\openstack\Entity\OpenStackImageInterface $image */
    $image = array_shift($images);

    if (empty($image)) {
      return;
    }

    $save_file_path = $this->prepareAndDownloadImageFromOpenStack($image);
    $this->uploadImageToS3($image, $save_file_path);
    $this->downloadImageFromS3($image, $save_file_path);
    $this->uploadImage($image, $to_cloud_context, $save_file_path, $to_image_id);
    $this->cleanupTemporaryFiles($save_file_path);
  }

  /**
   * Imports an image to the specified cloud context.
   *
   * @param \Drupal\openstack\Entity\OpenStackImageInterface $image
   *   The image to import.
   * @param string $to_cloud_context
   *   The target cloud context.
   * @param string $image_file_path
   *   The file path of the image.
   * @param string $to_image_id
   *   The target image ID.
   *
   * @return bool
   *   True if the image import was successful, false otherwise.
   */
  private function uploadImage(
    OpenStackImageInterface $image,
    string $to_cloud_context,
    string $image_file_path,
    string $to_image_id,
  ): bool {
    try {
      $openstack_service = $this->openStackServiceFactory->get($image->getCloudContext());
      // Upload the image to OpenStack.
      $openstack_service->uploadImage(
        $to_cloud_context,
        $to_image_id,
        $image_file_path
      );
      return TRUE;
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Prepares the directory, downloads the image from OpenStack.
   *
   * @param \Drupal\openstack\Entity\OpenStackImageInterface $image
   *   The image entity.
   *
   * @return string|null
   *   The file name of the saved image, or NULL if an exception occurs.
   */
  private function prepareAndDownloadImageFromOpenStack(OpenStackImageInterface $image): ?string {
    $file_default_scheme = $this->configFactory->get('system.file')->get('default_scheme');
    $file_directory = $this->fileSystem->realpath($file_default_scheme . "://");
    $image_file_name = "{$image->getName()}.{$image->getImageType()}";

    $save_file_directory = implode('/', [
      rtrim($file_directory ?: '', '/'),
      'openstack_image',
      $image->getCloudContext(),
    ]);
    $save_file_path = implode('/', [
      rtrim($save_file_directory ?: '', '/'),
      $image_file_name,
    ]);

    // Prepare a directory for saving the downloaded images.
    $this->fileSystem->prepareDirectory($save_file_directory, FileSystemInterface::CREATE_DIRECTORY);

    try {
      $openstack_service = $this->openStackServiceFactory->get($image->getCloudContext());
      // Download the image from OpenStack.
      $openstack_service->downloadImage(
        [
          'ImageId' => $image->getImageId(),
        ],
        $save_file_path
      );

      $this->logger->info($this->t(
        'The @image_file_name has been downloaded from @cloud_context and saved in the @save_file_path.', [
          '@image_file_name' => $image_file_name,
          '@cloud_context' => $image->getCloudContext(),
          '@save_file_path' => $save_file_path,
        ]
      ));

      return $save_file_path;
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      return NULL;
    }
  }

  /**
   * Cleans up temporary files.
   *
   * @param string $save_file_path
   *   The file path of the saved image.
   */
  private function cleanupTemporaryFiles($save_file_path): void {
    // Delete the temporary file.
    $this->fileSystem->delete($save_file_path);
  }

  /**
   * Generates an S3 key using the OpenStack image information.
   *
   * @param \Drupal\openstack\Entity\OpenStackImageInterface $image
   *   The image entity.
   *
   * @return string
   *   The generated S3 key.
   */
  private function generateS3KeyWithOpenStackImage(OpenStackImageInterface $image) :string {
    return implode('/', [
      'openstack_image',
      $image->getCloudContext(),
      "{$image->getName()}.{$image->getImageType()}",
    ]);
  }

  /**
   * Uploads the image to S3.
   *
   * @param \Drupal\openstack\Entity\OpenStackImageInterface $image
   *   The image entity.
   * @param string $save_file_path
   *   The file path of the saved image.
   *
   * @return bool
   *   TRUE if the upload is successful, FALSE otherwise.
   */
  private function uploadImageToS3(OpenStackImageInterface $image, string $save_file_path): bool {
    $config = $this->configFactory->getEditable('openstack.settings');
    $aws_cloud_context = $config->get('openstack_aws_cloud', '');
    $s3_bucket = $config->get('openstack_s3_bucket', '');
    $s3_key = $this->generateS3KeyWithOpenStackImage($image);

    try {
      // Upload the image to S3.
      $this->s3Service->setCloudContext($aws_cloud_context);
      $result = $this->s3Service->putObject([
        'Bucket' => $s3_bucket,
        'Key' => $s3_key,
        'Body' => file_get_contents($save_file_path),
      ]);

      // Output error messages in messenger to the logger.
      if (empty($result)) {
        $this->logger->error($this->t('Failed to export an entity to s3://@s3_bucket/@key.', [
          '@s3_bucket' => $s3_bucket,
          '@key' => $s3_key,
        ]));
        return FALSE;
      }

      $this->logger->info($this->t(
        'The @save_file_path has been uploaded to s3://@s3_bucket/@key', [
          '@save_file_path' => $save_file_path,
          '@s3_bucket' => $s3_bucket,
          '@key' => $s3_key,
        ]
      ));

      return TRUE;
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Downloads the image from S3.
   *
   * @param \Drupal\openstack\Entity\OpenStackImageInterface $image
   *   The image entity.
   * @param string $save_file_path
   *   The file path of the saved image.
   */
  private function downloadImageFromS3(
    OpenStackImageInterface $image,
    string $save_file_path,
  ): bool {

    $config = $this->configFactory->getEditable('openstack.settings');
    $aws_cloud_context = $config->get('openstack_aws_cloud', '');
    $s3_bucket = $config->get('openstack_s3_bucket', '');

    /** @var \Drupal\openstack\Entity\OpenStackImageInterface $image */
    $s3_key = $this->generateS3KeyWithOpenStackImage($image);

    $this->s3Service->setCloudContext($aws_cloud_context);

    try {
      $result = $this->s3Service->getObject([
        'Bucket' => $s3_bucket,
        'Key' => $s3_key,
      ]);

      // Validation.
      if (empty($result)
        || empty($result['ContentLength'])
        || empty($result['Body'])) {
        $this->logger->error($this->t('Failed to download an image file from s3://@s3_bucket/@key.', [
          '@s3_bucket' => $s3_bucket,
          '@key' => $s3_key,
        ]));

        return FALSE;
      }

      file_put_contents($save_file_path, $result['Body']);

      $this->logger->info($this->t(
        'The @save_file_path has been downloaded from s3://@s3_bucket/@key', [
          '@save_file_path' => $save_file_path,
          '@s3_bucket' => $s3_bucket,
          '@key' => $s3_key,
        ]
      ));

      return TRUE;
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());

      return FALSE;
    }
  }

}
