<?php

namespace Drupal\openstack\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\cloud\Plugin\QueueWorker\CloudBaseUpdateResourcesQueueWorker;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginException;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes for OpenStack Update Resources Queue.
 *
 * @QueueWorker(
 *   id = "openstack_update_resources_queue",
 *   title = @Translation("OpenStack Update Resources Queue"),
 *   deriver = "Drupal\cloud\Plugin\Derivative\CloudUpdateQueueDerivative",
 * )
 */
class OpenStackUpdateResourcesQueueWorker extends CloudBaseUpdateResourcesQueueWorker implements ContainerFactoryPluginInterface {

  /**
   * The OpenStack Service.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  private $openStackServiceFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new LocaleTranslation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    OpenStackServiceFactoryInterface $openstack_service_factory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->openStackServiceFactory = $openstack_service_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('openstack.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $cloud_context = $data['cloud_context'];
    $openstack_method_name = $data['openstack_method_name'];

    try {
      $openstack_service = $this->openStackServiceFactory->get($cloud_context);
      $this->checkServiceMethod($openstack_service, $openstack_method_name);
      $openstack_service->$openstack_method_name();
    }
    catch (\Exception $e) {
      if ($e instanceof CloudConfigPluginException) {
        $this->logger('openstack')->info($this->t(
          'Attempted to process a queue for %openstack_method_name; however the queue has been simply deleted since it could not be processed due to a missing $cloud_context %cloud_context. The OpenStack cloud service provider %cloud_context might be deleted before the queue is processed.', [
            '%openstack_method_name' => "OpenStackServiceInterface::{$openstack_method_name}()",
            '%cloud_context' => $cloud_context,
          ]
        ));
      }
      $this->handleException($e);
    }
  }

}
