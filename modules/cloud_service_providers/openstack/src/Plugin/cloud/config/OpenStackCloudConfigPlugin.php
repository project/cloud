<?php

namespace Drupal\openstack\Plugin\cloud\config;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Plugin\cloud\config\AwsCloudConfigPlugin;
use Symfony\Component\Routing\Route;

/**
 * OpenStack cloud service provider (CloudConfig) plugin class.
 */
class OpenStackCloudConfigPlugin extends AwsCloudConfigPlugin {

  /**
   * {@inheritdoc}
   */
  public function getInstanceCollectionTemplateName(): string {
    return 'view.openstack_instance.list';
  }

  /**
   * {@inheritdoc}
   */
  public function loadCredentials($cloud_context): array {
    /** @var \Drupal\cloud\Entity\CloudConfig $entity */
    $entity = $this->loadConfigEntity($cloud_context);
    $credentials = [];
    $openstack_ec2_api = $entity->get('field_use_openstack_ec2_api')->value;

    if (empty($openstack_ec2_api) && !empty($entity)) {
      $credentials = [
        'username'                   => $entity->get('field_username')->value,
        'password'                   => $entity->get('field_password')->value,
        'domain_id'                  => $entity->get('field_domain_id')->value,
        'project_id'                 => $entity->get('field_project_id')->value,
        'region'                     => $entity->get('field_os_region')->value,
        'self_signed_cert_path'      => $entity->get('field_self_signed_cert_path')->value,
        'identity_api_endpoint'      => $entity->get('field_identity_api_endpoint')->value,
        'identity_version'           => $entity->get('field_identity_version')->value,
        'compute_api_endpoint'       => $entity->get('field_compute_api_endpoint')->value,
        'compute_version'            => $entity->get('field_compute_version')->value,
        'image_api_endpoint'         => $entity->get('field_image_api_endpoint')->value,
        'image_version'              => $entity->get('field_image_version')->value,
        'network_api_endpoint'       => $entity->get('field_network_api_endpoint')->value,
        'network_version'            => $entity->get('field_network_version')->value,
        'volume_api_endpoint'        => $entity->get('field_volume_api_endpoint')->value,
        'volume_version'             => $entity->get('field_volume_version')->value,
        'orchestration_api_endpoint' => $entity->get('field_orchestration_api_endpoint')->value,
        'orchestration_version'      => $entity->get('field_orchestration_version')->value,
        'authUrl'                    => $entity->get('field_api_endpoint')->value,
        'user_id'                    => $entity->get('field_access_key')->value,
        'user_password'              => $entity->get('field_secret_key')->value,
      ];

    }
    elseif (!empty($entity)) {
      $credentials = [
        'region'   => $entity->get('field_os_region')->value,
        'endpoint' => $entity->get('field_api_endpoint')->value,
        'ini_file' => $this->fileSystem->realpath(aws_cloud_ini_file_path($entity->get('cloud_context')->value)),
      // @todo this is hard-coded.
        'version'  => 'latest',
      ];
    }

    return $credentials;
  }

  /**
   * {@inheritdoc}
   */
  public function getPricingPageRoute(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface {
    return AccessResult::allowed();
  }

}
