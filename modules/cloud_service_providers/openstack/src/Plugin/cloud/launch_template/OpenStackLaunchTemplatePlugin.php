<?php

namespace Drupal\openstack\Plugin\cloud\launch_template;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Service\Util\EntityLinkWithShortNameHtmlGenerator;
use Drupal\openstack\Entity\OpenStackInstance;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Drupal\openstack\Service\Rest\OpenStackService as OpenStackRestService;
use Drupal\openstack\Service\Rest\OpenStackServiceMock;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * OpenStack cloud launch template plugin.
 */
class OpenStackLaunchTemplatePlugin extends CloudPluginBase implements CloudLaunchTemplatePluginInterface, ContainerFactoryPluginInterface {

  /**
   * The OpenStack EC2 Service.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  protected $ec2Service;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Current login user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  protected $entityLinkRenderer;

  /**
   * The OpenStack Rest Service.
   *
   * @var \Drupal\openstack\Service\Rest\OpenStackService
   */
  protected $openStackRestService;

  /**
   * The OpenStack Rest Service.
   *
   * @var \Drupal\openstack\Service\Rest\OpenStackServiceMock
   */
  protected $openStackMockRestService;

  /**
   * The OpenStack Service.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  private $openStackServiceFactory;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * OpenStackCloudLaunchTemplatePlugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud EC2 service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The uuid service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current login user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\openstack\Service\Rest\OpenStackService $openstack_rest_service
   *   The openstack rest service.
   * @param \Drupal\openstack\Service\Rest\OpenStackServiceMock $openstack_mock_rest_service
   *   The openstack rest mock service.
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Ec2ServiceInterface $ec2_service,
    EntityTypeManagerInterface $entity_type_manager,
    UuidInterface $uuid_service,
    AccountProxyInterface $current_user,
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityLinkRendererInterface $entity_link_renderer,
    OpenStackRestService $openstack_rest_service,
    OpenStackServiceMock $openstack_mock_rest_service,
    OpenStackServiceFactoryInterface $openstack_service_factory,
    CloudServiceInterface $cloud_service,
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->ec2Service = $ec2_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->uuidService = $uuid_service;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityLinkRenderer = $entity_link_renderer;
    $this->openStackRestService = $openstack_rest_service;
    $this->openStackMockRestService = $openstack_mock_rest_service;
    $this->openStackServiceFactory = $openstack_service_factory;
    $this->cloudService = $cloud_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('openstack.ec2'),
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity.link_renderer'),
      $container->get('openstack.rest'),
      $container->get('openstack.rest_mock'),
      $container->get('openstack.factory'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityBundleName(): string {
    return $this->pluginDefinition['entity_bundle'];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   */
  public function launch(CloudLaunchTemplateInterface $cloud_launch_template, ?FormStateInterface $form_state = NULL): array {
    $cloud_context = $cloud_launch_template->getCloudContext() ?? '';

    // In order to make compatible with unit test
    // Use this function in this class to check EC2 or REST.
    $openstack_ec2_service = $this->openStackServiceFactory->isEc2ServiceType('cloud_config', $cloud_context);

    // Switch OpenStack REST or REST mock service based on $test_mode.
    if (!$openstack_ec2_service) {
      $test_mode = !empty($this->configFactory->get('openstack.settings'))
        ? $this->configFactory->get('openstack.settings')->get('openstack_test_mode') : '';
      $this->ec2Service = $test_mode ? $this->openStackMockRestService : $this->openStackRestService;
    }
    $this->ec2Service->setCloudContext($cloud_context);

    $tags = $params = $security_groups = $networks = [];
    // Set API parameters for OpenStack EC2.
    if (!($this->ec2Service instanceof OpenStackRestService)) {
      $image_id = $cloud_launch_template->get('field_openstack_image_id')->value;

      $images = $this->entityTypeManager
        ->getStorage('openstack_image')->loadByProperties([
          'image_id' => $image_id,
        ]
      );

      $vpc_id = NULL;
      if ($cloud_launch_template->get('field_openstack_subnet')->value !== NULL) {
        $subnet_id = $cloud_launch_template->get('field_openstack_subnet')->value ?? '';
        $vpc_id = $cloud_launch_template->get('field_openstack_vpc')->value;
      }

      foreach ($cloud_launch_template->get('field_openstack_security_group') ?: [] as $group) {
        $security_groups = !empty($group->entity)
        && $vpc_id !== NULL
        && $vpc_id === $group->entity->getVpcId()
          ? $group->entity->getGroupId() : [];
      }

      $networks = !empty($cloud_launch_template->get('field_openstack_network')->entity)
        ? ['NetworkId' => $cloud_launch_template->get('field_openstack_network')->entity->getNetworkId()]
        : [];

      $params = [
        'ImageId' => $image_id,
        'MaxCount' => $cloud_launch_template->get('field_max_count')->value,
        'MinCount' => $cloud_launch_template->get('field_min_count')->value,
        'Monitoring' => ['Enabled' => $cloud_launch_template->get('field_monitoring')->value === '0' ? FALSE : TRUE],
        'InstanceType' => $cloud_launch_template->get('field_instance_type')->value,
        'KeyName' => !empty($cloud_launch_template->get('field_openstack_ssh_key')->entity)
          ? $cloud_launch_template->get('field_openstack_ssh_key')->entity->get('key_pair_name')->value
          : '',
        'InstanceInitiatedShutdownBehavior' => !empty($images) && array_shift($images)->get('root_device_type') === 'ebs'
          ? $cloud_launch_template->get('field_instance_shutdown_behavior')->value : '',
        'Placement' => [
          'AvailabilityZone' => !empty($cloud_launch_template->get('field_os_availability_zone')->value)
            ? $cloud_launch_template->get('field_os_availability_zone')->value : '',
        ],
        'SubnetId' => $subnet_id ?? '',
        'SecurityGroupIds' => [$security_groups] ?? [],
        'NetworkInterfaces' => [$networks] ?? [],
        'ServerGroupId' => !empty($cloud_launch_template->get('field_openstack_server_group')->entity)
          ? $cloud_launch_template->get('field_openstack_server_group')->entity->getServerGroupId() : '',
      ];

      // Setup optional parameters.
      if (!empty($cloud_launch_template->get('field_kernel_id')->value)) {
        $params['KernelId'] = $cloud_launch_template->get('field_kernel_id')->value;
      }
      if (!empty($cloud_launch_template->get('field_ram')->value)) {
        $params['RamdiskId'] = $cloud_launch_template->get('field_ram')->value;
      }
      if (!empty($cloud_launch_template->get('field_user_data')->value)) {
        $params['UserData'] = base64_encode($cloud_launch_template->get('field_user_data')->value ?? '');
      }

      if (empty($security_groups)) {
        unset($params['SecurityGroupIds']);
      }

      $tags_map = [];

      // Add tags from the template.
      foreach ($cloud_launch_template->get('field_tags') ?: [] as $tag_item) {
        $tags_map[$tag_item->getItemKey()] = $tag_item->getItemValue();
      }

      if (!empty($form_state->getValue('termination_protection'))) {
        $params['DisableApiTermination'] = $form_state->getValue('termination_protection') === '0' ? FALSE : TRUE;
      }
      elseif (!empty($form_state->getValue('terminate'))) {
        /** @var \Drupal\Core\Datetime\DrupalDateTime $timestamp */
        $timestamp = $form_state->getValue('termination_date');
        $tags_map['openstack_' . OpenStackInstance::TAG_TERMINATION_TIMESTAMP] = $timestamp->getTimeStamp();
      }

      $tags_map['Name'] = $cloud_launch_template->getName();
      if ($params['MaxCount'] > 1) {
        $cloud_launch_uuid = $this->uuidService->generate();
        $tags_map['Name'] .= $cloud_launch_uuid;
      }

      $tags = [];
      foreach ($tags_map ?: [] as $item_key => $item_value) {
        $tags[] = [
          'Key' => $item_key,
          'Value' => $item_value,
        ];
      }
    }

    // Set API parameters for OpenStack REST.
    if ($this->ec2Service instanceof OpenStackRestService) {
      foreach ($cloud_launch_template->get('field_openstack_security_group') ?: [] as $group) {
        if (empty($group->entity)) {
          continue;
        }
        $security_groups[] = ['name' => $group->entity->getGroupId()];
      }

      if (empty($security_groups)) {
        $this->messenger->addError($this->t('Failed to launch an instance because a security group is missing or undefined.'));
        $return_route = [
          'route_name' => 'view.openstack_instance.list',
          'params' => ['cloud_context' => $cloud_launch_template->getCloudContext()],
        ];
        return $return_route;
      }

      $networks = !empty($cloud_launch_template->get('field_openstack_network')->entity)
        ? [['uuid' => $cloud_launch_template->get('field_openstack_network')->entity->getNetworkId()]] : [];

      $params = [
        'Name' => $cloud_launch_template->getName(),
        'FlavorId' => $cloud_launch_template->get('field_flavor')->value,
        'ImageId' => $cloud_launch_template->get('field_openstack_image_id')->value,
        'KeyName' => !empty($cloud_launch_template->get('field_openstack_ssh_key')->entity)
          ? $cloud_launch_template->get('field_openstack_ssh_key')->entity->get('key_pair_name')->value
          : '',
        'Placement' => [
          'AvailabilityZone' => !empty($cloud_launch_template->get('field_os_availability_zone')->value)
            ? $cloud_launch_template->get('field_os_availability_zone')->value : '',
        ],
        'SecurityGroupIds' => $security_groups,
        'NetworkInterfaces' => $networks,
        'ServerGroupId' => !empty($cloud_launch_template->get('field_openstack_server_group')->entity)
          ? $cloud_launch_template->get('field_openstack_server_group')->entity->getServerGroupId() : '',
      ];

      if (!empty($cloud_launch_template->get('field_user_data')->value)) {
        $params['UserData'] = base64_encode(str_replace(["\r\n", "\n"], PHP_EOL, $cloud_launch_template->get('field_user_data')->value ?? ''));
      }

      $tags_map = [];

      // Add tags from the template.
      foreach ($cloud_launch_template->get('field_tags') ?: [] as $tag_item) {
        $tags_map[$tag_item->getItemKey()] = $tag_item->getItemValue();
      }

      $tags = [];
      $uid_key_name = $this->ec2Service->getTagKeyCreatedByUid(
        'openstack',
        $cloud_context
      );
      foreach ($tags_map ?: [] as $item_key => $item_value) {
        if ($item_key === $uid_key_name) {
          // Set the user who launches the LaunchTemplate.
          $tags[] = [
            'Key' => $item_key,
            'Value' => $this->currentUser->id(),
          ];
        }
      }

      $tags[] = [
        'Key' => 'termination_protection',
        'Value' => empty($form_state->getValue('termination_protection')) ? 0 : 1,
      ];
    }

    $result = $this->ec2Service->runInstances($params, $tags, 'openstack');
    if (empty($result)) {
      $this->processOperationErrorStatus($cloud_launch_template, 'launched');
      return [
        'route_name' => 'entity.cloud_launch_template.canonical',
        'params' => [
          'cloud_launch_template' => $cloud_launch_template->id(),
          'cloud_context' => $cloud_launch_template->getCloudContext(),
        ],
      ];
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($cloud_launch_template, 'launched remotely');

      return [
        'route_name' => 'view.openstack_instance.list',
        'params' => ['cloud_context' => $cloud_launch_template->getCloudContext()],
      ];
    }

    // Update instances after launch.
    $this->ec2Service->updateInstances();
    if (!($this->ec2Service instanceof OpenStackRestService) && $params['MaxCount'] > 1) {
      $this->updateInstanceName($cloud_launch_template, $cloud_launch_uuid ?? '');
    }

    // Update server group after launch.
    !$this->ec2Service instanceof OpenStackRestService ?: $this->ec2Service->updateServerGroups();
    // Update port after launch.
    !$this->ec2Service instanceof OpenStackRestService ?: $this->ec2Service->updatePorts();

    $this->processOperationStatus($cloud_launch_template, 'launched');

    // Update instances after launch.
    $this->ec2Service->updateInstances();

    return [
      'route_name' => 'view.openstack_instance.list',
      'params' => ['cloud_context' => $cloud_launch_template->getCloudContext()],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildListHeader(): array {
    return [
      [
        'data' => $this->t('Image name'),
        'specifier' => 'field_openstack_image_id',
        'field' => 'field_openstack_image_id',
      ],
      [
        'data' => $this->t('Flavor'),
        'specifier' => 'field_flavor',
        'field' => 'field_flavor',
      ],
      [
        'data' => $this->t('Security group'),
        'specifier' => 'field_openstack_security_group',
        'field' => 'field_openstack_security_group',
      ],
      [
        'data' => $this->t('Key pair'),
        'specifier' => 'field_openstack_ssh_key',
        'field' => 'field_openstack_ssh_key',
      ],
      [
        'data' => $this->t('VPC'),
        'specifier' => 'field_openstack_vpc',
        'field' => 'field_openstack_vpc',
      ],
      [
        'data' => $this->t('Max count'),
        'specifier' => 'field_max_count',
        'field' => 'field_max_count',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRow(CloudLaunchTemplateInterface $entity): array {
    $row = [];
    $empty_row = ['data' => ['#markup' => '']];

    // AMI image.
    $image = NULL;
    $image_id = $entity->get('field_openstack_image_id')->value;
    if (!empty($image_id)) {
      $images = $this->entityTypeManager
        ->getStorage('openstack_image')->loadByProperties([
          'image_id' => $image_id,
        ]);
      $image = array_shift($images);
    }

    if ($image !== NULL) {
      $element = $this->entityLinkRenderer->renderViewElement(
        $image->getImageId(),
        'openstack_image',
        'image_id',
        [],
        $image->getName()
      );
      $row[]['data'] = ['#markup' => $element['#markup']];
    }
    else {
      $row[] = $empty_row;
    }

    // Flavor.
    $row[] = [
      'data' => $entity->field_flavor->view([
        'label' => 'hidden',
        'type' => 'string',
      ]),
    ];

    // Security groups.
    $htmls = [];
    foreach ($entity->get('field_openstack_security_group') ?: [] as $group) {
      if ($group->entity !== NULL) {
        $group_id = $group->entity->getGroupId();
        $element = $this->entityLinkRenderer->renderViewElement(
          $group_id,
          'openstack_security_group',
          'group_id',
          [],
          $group->entity->getName()
        );

        $htmls[] = $element['#markup'];
      }
      else {
        $htmls[] = '';
      }
    }
    $row[] = [
      'data' => ['#markup' => implode(', ', $htmls)],
    ];

    // SSH key.
    if ($entity->get('field_openstack_ssh_key')->entity !== NULL) {

      $row[] = [
        'data' => $this->entityLinkRenderer->renderViewElement(
          $entity->get('field_openstack_ssh_key')->entity->getKeyPairName(),
          'openstack_key_pair',
          'key_pair_name'
        ),
      ];
    }
    else {
      $row[] = $empty_row;
    }

    // VPC.
    if ($entity->get('field_openstack_vpc')->value !== NULL) {
      $row[] = [
        'data' => $this->entityLinkRenderer->renderViewElement(
          $entity->get('field_openstack_vpc')->value,
          'aws_cloud_vpc',
          'vpc_id',
          [],
          '',
          EntityLinkWithShortNameHtmlGenerator::class
        ),
      ];
    }
    else {
      $row[] = $empty_row;
    }

    $row[]['data']['#markup'] = $entity->get('field_max_count')->value;

    return $row;
  }

  /**
   * Update instance name based on the name of the cloud launch template.
   *
   * If the same instance name exists, the number suffix (#2, #3...) can be
   * added at the end of the cloud launch template name.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template
   *   The cloud launch template used to launch an instance.
   * @param string $cloud_launch_uuid
   *   The uuid to specify instances.
   */
  private function updateInstanceName(
    CloudLaunchTemplateInterface $cloud_launch_template,
    $cloud_launch_uuid,
  ): void {
    $template_name = $cloud_launch_template->getName();
    $cloud_context = $cloud_launch_template->getCloudContext();

    $instance_storage = $this->entityTypeManager->getStorage('openstack_instance');
    $instance_ids = $instance_storage
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('name', $template_name . $cloud_launch_uuid)
      ->condition('cloud_context', $cloud_context)
      ->execute();

    $instances = $instance_storage->loadMultiple($instance_ids);
    $count = 1;
    $prefix = $this->getInstanceNamePrefix($template_name, $cloud_context);
    foreach ($instances ?: [] as $instance) {
      $name = $prefix . $count++;
      $params = [
        'Resources' => [$instance->getInstanceId()],
      ];
      $params['Tags'][] = [
        'Key' => 'Name',
        'Value' => $name,
      ];
      $this->ec2Service->createTags($params);
    }

    if (count($instances) > 0) {
      $this->ec2Service->updateInstances();
    }
  }

  /**
   * Get the prefix of instance name.
   *
   * The prefix will be something like below.
   * 1. 1st Launch:
   *   Cloud Orchestrator #1, Cloud Orchestrator #2.
   * 2. 2nd Launch:
   *   Cloud Orchestrator #2-1, Cloud Orchestrator #2-2.
   * 3. 3rd Launch:
   *   Cloud Orchestrator #3-1, Cloud Orchestrator #3-2.
   *
   * @param string $template_name
   *   The template name.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return string
   *   The prefix of instance name.
   */
  private function getInstanceNamePrefix($template_name, $cloud_context): string {
    $instance_storage = $this->entityTypeManager->getStorage('openstack_instance');
    $instance_ids = $instance_storage
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('name', "$template_name #%", 'like')
      ->condition('cloud_context', $cloud_context)
      ->execute();

    $instances = $instance_storage->loadMultiple($instance_ids);

    $instance_names = array_map(static function ($instance) {
      return $instance->getName();
    }, $instances);

    $prefix = "$template_name #";
    if (!in_array($prefix . '1', $instance_names)) {
      return $prefix;
    }

    $index = 2;
    $prefix = "$template_name #$index-";
    while (in_array($prefix . '1', $instance_names)) {
      $index++;
      $prefix = "$template_name #$index-";
    }

    return $prefix;
  }

  /**
   * {@inheritdoc}
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function validateGit(
    CloudLaunchTemplateInterface $entity,
    array &$files_arr,
    string &$tmp_dir_name,
    bool $delete_tmp_dir = FALSE,
  ): string {
    return '';
  }

  /**
   * Update availability zones after updating cloud launch template list.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function postUpdateCloudLaunchTemplateList($cloud_context): void {
    $openstack_ec2_service = $this->openStackServiceFactory->isEc2ServiceType('cloud_config', $cloud_context);

    // Switch OpenStack REST or REST mock service based on $test_mode.
    if (!$openstack_ec2_service) {
      $test_mode = !empty($this->configFactory->get('openstack.settings'))
      ? $this->configFactory->get('openstack.settings')->get('openstack_test_mode') : '';
      $this->ec2Service = $test_mode ? $this->openStackMockRestService : $this->openStackRestService;
    }
    $this->ec2Service->setCloudContext($cloud_context);
    $updated = $this->ec2Service->updateAvailabilityZones();

    if ($updated) {
      $this->messenger->addStatus($this->t('Updated @name.', ['@name' => 'Availability Zones']));
    }
  }

}
