<?php

namespace Drupal\openstack\Plugin\cloud\launch_template;

use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginUninstallValidator;

/**
 * Validates module uninstall readiness based on existing content entities.
 */
class OpenStackLaunchTemplatePluginUninstallValidator extends CloudLaunchTemplatePluginUninstallValidator {

}
