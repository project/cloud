<?php

namespace Drupal\openstack\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Network operations bulk form element.
 *
 * @ViewsField("openstack_network_bulk_form")
 */
class OpenStackNetworkBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): string {
    return $this->t('No Network selected.');
  }

}
