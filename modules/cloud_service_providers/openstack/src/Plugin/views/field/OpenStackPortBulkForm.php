<?php

namespace Drupal\openstack\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Port operations bulk form element.
 *
 * @ViewsField("openstack_port_bulk_form")
 */
class OpenStackPortBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): string {
    return $this->t('No Port selected.');
  }

}
