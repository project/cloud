<?php

namespace Drupal\openstack\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Project operations bulk form element.
 *
 * @ViewsField("openstack_project_bulk_form")
 */
class OpenStackProjectBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): string {
    return $this->t('No Project selected.');
  }

}
