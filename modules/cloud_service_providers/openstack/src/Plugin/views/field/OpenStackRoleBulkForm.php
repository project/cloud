<?php

namespace Drupal\openstack\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Role operations bulk form element.
 *
 * @ViewsField("openstack_role_bulk_form")
 */
class OpenStackRoleBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): string {
    return $this->t('No Role selected.');
  }

}
