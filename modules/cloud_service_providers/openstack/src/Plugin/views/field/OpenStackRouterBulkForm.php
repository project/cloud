<?php

namespace Drupal\openstack\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Router operations bulk form element.
 *
 * @ViewsField("openstack_router_bulk_form")
 */
class OpenStackRouterBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): string {
    return $this->t('No Router selected.');
  }

}
