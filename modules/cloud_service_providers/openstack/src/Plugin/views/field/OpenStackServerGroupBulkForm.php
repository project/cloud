<?php

namespace Drupal\openstack\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Server group operations bulk form element.
 *
 * @ViewsField("openstack_server_group_bulk_form")
 */
class OpenStackServerGroupBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): string {
    return $this->t('No Server group selected.');
  }

}
