<?php

namespace Drupal\openstack\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Stack operations bulk form element.
 *
 * @ViewsField("openstack_stack_bulk_form")
 */
class OpenStackStackBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): string {
    return $this->t('No Stack selected.');
  }

}
