<?php

namespace Drupal\openstack\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Subnet operations bulk form element.
 *
 * @ViewsField("openstack_subnet_bulk_form")
 */
class OpenStackSubnetBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): string {
    return $this->t('No Subnet selected.');
  }

}
