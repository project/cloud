<?php

namespace Drupal\openstack\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a User operations bulk form element.
 *
 * @ViewsField("openstack_user_bulk_form")
 */
class OpenStackUserBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): string {
    return $this->t('No User selected.');
  }

}
