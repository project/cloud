<?php

namespace Drupal\openstack\Service\Ec2;

use Aws\Api\DateTimeResult;
use Aws\Credentials\CredentialProvider;
use Aws\Ec2\Ec2Client;
use Aws\Ec2\Exception\Ec2Exception;
use Aws\MockHandler;
use Aws\Result;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Session\AccountInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2Service;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceException;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudCacheServiceInterface;
use Drupal\cloud\Service\CloudResourceTagInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\openstack\Service\OpenStackBatchOperations;
use Drupal\openstack\Service\OpenStackServiceInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Interacts with OpenStack using the Amazon EC2 API.
 */
class OpenStackService extends Ec2Service implements OpenStackServiceInterface, CloudResourceTagInterface {


  /**
   * TRUE or FALSE whether to be in test mode.
   *
   * @var bool
   */
  private $testMode;

  /**
   * Constructs a new OpenStackService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\core\Field\FieldTypePluginManagerInterface $field_type_plugin_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock interface.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\cloud\Service\CloudCacheServiceInterface $cloud_cache_service
   *   The Cloud cache service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    FieldTypePluginManagerInterface $field_type_plugin_manager,
    EntityFieldManagerInterface $entity_field_manager,
    LockBackendInterface $lock,
    QueueFactory $queue_factory,
    ClientInterface $http_client,
    ModuleHandlerInterface $module_handler,
    CloudServiceInterface $cloud_service,
    CloudCacheServiceInterface $cloud_cache_service,
    RequestStack $request_stack,
  ) {

    parent::__construct($entity_type_manager,
                        $config_factory,
                        $current_user,
                        $cloud_config_plugin_manager,
                        $field_type_plugin_manager,
                        $entity_field_manager,
                        $lock,
                        $queue_factory,
                        $http_client,
                        $cloud_service,
                        $cloud_cache_service,
                        $module_handler,
                        $request_stack);

    // Set up the testMode flag.
    $this->testMode = (bool) $this->configFactory->get('openstack.settings')->get('openstack_test_mode');
  }

  /**
   * Get Client object for OpenStackEc2 API execution.
   *
   * @param array $credentials
   *   The array of credentials.
   *
   * @return object
   *   The EC2 client.
   */
  public function getClient(array $credentials = []) {
    return $this->getEc2Client($credentials);
  }

  /**
   * Get Ec2Client object for API execution.
   *
   * @param array $credentials
   *   The array of credentials.
   *
   * @return \Aws\Ec2\Ec2Client|null
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  protected function getEc2Client(array $credentials = []): ?Ec2Client {
    if (empty($credentials)) {
      $credentials = $this->cloudConfigPluginManager->loadCredentials();
    }

    try {
      $ec2_params = [
        'region' => $credentials['region'],
        'version' => $credentials['version'],
        'endpoint' => $credentials['endpoint'],
      ];

      if (!empty($credentials['env'])) {
        $this->setCredentialsToEnv(
          $credentials['env']['access_key'],
          $credentials['env']['secret_key']
        );
        $provider = CredentialProvider::env();
      }
      else {
        $provider = CredentialProvider::ini('default', $credentials['ini_file']);
      }
      $provider = CredentialProvider::memoize($provider);

      $ec2_params['credentials'] = $provider;
      $ec2_client = new Ec2Client($ec2_params);
    }
    catch (\Exception $e) {
      $ec2_client = NULL;
      $this->logger('openstack_ec2_service')->error($e->getMessage());
    }
    if ($this->testMode) {
      $this->addMockHandler($ec2_client);
    }
    return $ec2_client;
  }

  /**
   * Add a mock handler of aws sdk for testing.
   *
   * The mock data of openstack response is saved
   * in configuration "openstack_mock_data".
   *
   * @param \Aws\Ec2\Ec2Client $ec2_client
   *   The ec2 client.
   *
   * @see https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_handlers-and-middleware.html
   */
  private function addMockHandler(Ec2Client $ec2_client): void {
    $mock_data = $this->configFactory->get('openstack.settings')
      ->get('openstack_mock_data');
    if (empty($this->testMode) || empty($mock_data)) {
      return;
    }

    $mock_data = json_decode($mock_data, TRUE);
    $result = static function ($command, $request) use ($mock_data) {

      $command_name = $command->getName();
      $response_data = $mock_data[$command_name] ?? [];

      // Support EC2-Classic.
      // See also: https://www.drupal.org/i/3032292.
      if ($command_name === 'DescribeAccountAttributes'
        && empty($response_data)) {
        $response_data = [
          'AccountAttributes' => [
            [
              'AttributeName' => 'supported-platforms',
              'AttributeValues' => [
                [
                  'AttributeValue' => 'VPC',
                ],
              ],
            ],
          ],
        ];
      }
      // Because launch time is special,
      // we need to convert it from string to DateTimeResult.
      if ($command_name === 'DescribeInstances'
        && !empty($response_data)) {
        // NOTE: We won't use foreach ($items ?: [] as $item) here since
        // $response_data['Reservations'] is used as a reference.
        foreach ($response_data['Reservations'] as &$reservation) {
          foreach ($reservation['Instances'] as &$instance) {
            // Initialize $instance['LaunchTime'].
            $instance['LaunchTime'] = !empty($instance['LaunchTime'])
              ? new DateTimeResult($instance['LaunchTime'])
              : new DateTimeResult();
          }
        }
      }

      return new Result($response_data);
    };

    // Set a mock handler with the number of mocked results in the queue.
    // The mock queue count should be the number of API calls and is similar
    // to the prepared mocked results. TO be safe, an additional queue is set
    // for an async call or a repeated call.
    $results_on_queue = array_pad([], count($mock_data) + 1, $result);
    $ec2_client
      ->getHandlerList()
      ->setHandler(new MockHandler($results_on_queue));
  }

  /**
   * Execute the API of OpenStack EC2 Service.
   *
   * @param string $operation
   *   The operation to perform.
   * @param array $params
   *   An array of parameters.
   * @param array $credentials
   *   An array of credentials.
   *
   * @return array
   *   Array of execution result or NULL if there is an error.
   *
   * @throws \Drupal\aws_cloud\Service\Ec2\Ec2ServiceException
   *   If the $params is empty or $ec2_client (Ec2Client) is NULL.
   */
  private function execute($operation, array $params = [], array $credentials = []) {
    $results = NULL;

    $ec2_client = $this->getClient($credentials);
    if (empty($ec2_client)) {
      throw new Ec2ServiceException('No EC2 Client found.  Cannot perform API operations');
    }

    try {
      // Let other modules alter the parameters
      // before they are sent through the API.
      $this->moduleHandler->invokeAll('openstack_pre_execute_alter', [
        &$params,
        $operation,
        $this->cloudContext,
      ]);

      $command = $ec2_client->getCommand($operation, $params);
      $results = $ec2_client->execute($command);

      // Let other modules alter the results before the module processes it.
      $this->moduleHandler->invokeAll('openstack_post_execute_alter', [
        &$results,
        $operation,
        $this->cloudContext,
      ]);
    }
    catch (Ec2Exception $e) {
      $this->messenger->addError($this->t('Error: The operation "@operation" could not be performed.', [
        '@operation' => $operation,
      ]));

      $this->messenger->addError($this->t('Error Info: @error_info', [
        '@error_info' => $e->getAwsErrorCode(),
      ]));

      $this->messenger->addError($this->t('Error from: @error_type-side', [
        '@error_type' => $e->getAwsErrorType(),
      ]));

      $this->messenger->addError($this->t('Status Code: @status_code', [
        '@status_code' => $e->getStatusCode(),
      ]));

      $this->messenger->addError($this->t('Message: @msg', ['@msg' => $e->getAwsErrorMessage()]));

    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $results;
  }

  /**
   * Call the API for updated entities and store them as instance entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateInstanceEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = ''): bool {
    $updated = FALSE;
    // Call the api and get all instances.
    $result = $this->describeInstances($params);
    if ($result !== NULL) {
      $all_instances = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the instances by setting up
      // the array with the instance_id.
      foreach ($all_instances ?: [] as $instance) {
        $stale[$instance->getInstanceId()] = $instance;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Instance Update');
      // Loop through the reservations and store each one as an instance entity.
      foreach ($result['Reservations'] ?: [] as $reservation) {

        foreach ($reservation['Instances'] ?: [] as $instance) {
          // Keep track of instances that do not exist anymore
          // delete them after saving the rest of the instances.
          if (isset($stale[$instance['InstanceId']])) {
            unset($stale[$instance['InstanceId']]);
          }
          // Store the Reservation OwnerId in instance so batch
          // callback has access.
          $instance['reservation_ownerid'] = $reservation['OwnerId'];
          $instance['reservation_id'] = $reservation['ReservationId'];

          $batch_builder->addOperation([
            OpenStackBatchOperations::class,
            'updateInstance',
          ], [$cloud_context, $instance]);

        }
      }
      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Update the OpenStackInstances.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateInstances(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_instance';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateInstanceEntities($params, $clear, $entity_type, $this->cloudContext);

    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateInstancesWithoutBatch(array $params = [], $clear = TRUE, $to_update = TRUE) {
    $updated = FALSE;
    $entity_type = 'openstack_instance';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    // Override to handle OpenStack specific instances.
    $results = $this->describeInstances($params);
    if ($results !== NULL) {
      $all_instances = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);

      $stale = [];
      // Make it easier to look up the images by setting up
      // the array with the image_id.
      foreach ($all_instances ?: [] as $instance) {
        $stale[$instance->getInstanceId()] = $instance;
      }

      foreach ($results['Reservations'] ?: [] as $reservation) {
        foreach ($reservation['Instances'] ?: [] as $instance) {
          if (isset($stale[$instance['InstanceId']])) {
            unset($stale[$instance['InstanceId']]);
          }
          // Store the Reservation OwnerId in instance so batch
          // callback has access.
          $instance['reservation_ownerid'] = $reservation['OwnerId'];
          $instance['reservation_id'] = $reservation['ReservationId'];

          OpenStackBatchOperations::updateInstance($this->cloudContext, $instance);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = TRUE;
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as image entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateImageEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = ''): bool {
    $updated = FALSE;

    // Load all entities by $this->cloudContext.
    $image_entities = $this->entityTypeManager->getStorage($entity_type)->loadByProperties(
      ['cloud_context' => $this->cloudContext]
    );

    $result = $this->describeImages($params);

    if (isset($result)) {

      $stale = [];
      // Make it easier to look up the images by setting up
      // the array with the image_id.
      foreach ($image_entities ?: [] as $image) {
        $stale[$image->getImageId()] = $image;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Image Update');

      foreach ($result['Images'] ?: [] as $image) {
        // Keep track of images that do not exist anymore
        // delete them after saving the rest of the images.
        if (isset($stale[$image['ImageId']])) {
          unset($stale[$image['ImageId']]);
        }

        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateImage',
        ], [$cloud_context, $image]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Update the OpenStackImages.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param bool $save_operation
   *   TRUE to save operation and not to execute.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateImages(array $params = [], $clear = TRUE, $save_operation = FALSE): bool {
    $entity_type = 'openstack_image';
    $lock_name = $this->getLockKey($entity_type);
    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateImageEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as KeyPair entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   Key name info.
   * @param bool $update
   *   TRUE to update Key info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateKeyPairEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE): bool {
    $updated = FALSE;
    $result = $this->describeKeyPairs();

    if (isset($result)) {

      $all_keys = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the groups by setting up
      // the array with the group_id.
      foreach ($all_keys ?: [] as $key) {
        $stale[$key->getKeyPairName()] = $key;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Keypair Update');

      foreach ($result['KeyPairs'] ?: [] as $key_pair) {
        // Keep track of key pair that do not exist anymore
        // delete them after saving the rest of the key pair.
        if (isset($stale[$key_pair['KeyName']])) {
          unset($stale[$key_pair['KeyName']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateKeyPair',
        ], [$cloud_context, $key_pair]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the OpenStackKeyPairs.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateKeyPairs(): bool {
    $entity_type = 'openstack_key_pair';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateKeyPairEntities($entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as SecurityGroup entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update SG info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSecurityGroupEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    $updated = FALSE;
    $result = $this->describeSecurityGroups($params);

    if (isset($result)) {

      $all_groups = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the groups by setting up
      // the array with the group_id.
      foreach ($all_groups ?: [] as $group) {
        $stale[$group->getGroupId()] = $group;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Security group update');

      foreach ($result['SecurityGroups'] ?: [] as $security_group) {

        // Keep track of instances that do not exist anymore
        // delete them after saving the rest of the instances.
        if (isset($stale[$security_group['GroupId']])) {
          unset($stale[$security_group['GroupId']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateSecurityGroup',
        ], [$cloud_context, $security_group]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the OpenStackSecurityGroups.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSecurityGroups(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_security_group';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSecurityGroupEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as Volume entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   The volume info.
   * @param bool $update
   *   TRUE to update volume.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateVolumeEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE): bool {
    $updated = FALSE;
    $result = $this->describeVolumes();

    if (isset($result)) {

      $all_volumes = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the groups by setting up
      // the array with the group_id.
      foreach ($all_volumes ?: [] as $volume) {

        if (empty($volume)) {
          continue;
        }

        $stale[$volume->getVolumeId()] = $volume;
      }
      $snapshot_id_name_map = $this->getSnapshotIdNameMap($result['Volumes'] ?: []);

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Volume update');

      foreach ($result['Volumes'] ?: [] as $volume) {

        if (empty($volume)) {
          continue;
        }

        // Keep track of network interfaces that do not exist anymore
        // delete them after saving the rest of the network interfaces.
        if (isset($stale[$volume['VolumeId']])) {
          unset($stale[$volume['VolumeId']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateVolume',
        ], [$cloud_context, $volume, $snapshot_id_name_map]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the OpenStackVolumes.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateVolumes(): bool {
    $entity_type = 'openstack_volume';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateVolumeEntities($entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update the availability zones.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param bool $clear
   *   TRUE to clear stale instances.
   *
   * @return bool
   *   Indicates success or failure.
   */
  public function updateAvailabilityZones(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_availability_zone';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateAvailabilityZoneEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as AvailabilityZones.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale availability zones.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateAvailabilityZoneEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = ''): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $result = $this->describeAvailabilityZones($params);

    if (!empty($result)) {
      $properties = ['cloud_context' => $this->cloudContext];
      $all_availability_zones = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);

      $stale = [];
      // Make it easier to look up the groups by setting up the array with the
      // group_id.
      foreach ($all_availability_zones ?: [] as $all_availability_zone) {
        $stale[$all_availability_zone->getZoneName()] = $all_availability_zone;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('AvailabilityZone Update');

      foreach ($result['AvailabilityZones'] ?: [] as $availability_zone) {
        // Keep track of availability zones that do not exist anymore
        // delete them after saving the rest of the availability zones.
        if (!empty($stale[$availability_zone['ZoneName']])) {
          unset($stale[$availability_zone['ZoneName']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateAvailabilityZone',
        ], [$cloud_context, $availability_zone]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateAvailabilityZonesWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE): bool {
    $updated = FALSE;
    $entity_type = 'openstack_availability_zone';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = ['cloud_context' => $this->cloudContext];
    $all_availability_zones = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up
    // the array with the zone_name.
    foreach ($all_availability_zones ?: [] as $availability_zone) {
      $stale[$availability_zone->getZoneName()] = $availability_zone;
    }

    $result = $this->describeAvailabilityZones($params);

    if (!empty($result)) {
      foreach ($result['AvailabilityZones'] ?: [] as $availability_zone) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$availability_zone['ZoneName']])) {
          unset($stale[$availability_zone['ZoneName']]);
        }
        if ($update) {
          OpenStackBatchOperations::updateAvailabilityZone($this->cloudContext, $availability_zone);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = TRUE;
    }
    elseif ($clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateServerGroupsWithoutBatch(array $params = [], bool $clear = TRUE, bool $update = TRUE): bool {
    $updated = FALSE;
    $entity_type = 'openstack_server_group';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = ['cloud_context' => $this->cloudContext];
    $all_server_groups = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    foreach ($all_server_groups ?: [] as $server_group) {
      $stale[$server_group->getName()] = $server_group;
    }

    $result = $this->describeServerGroups($params);

    if (!empty($result)) {
      foreach ($result['ServerGroups'] ?: [] as $server_group) {
        if (!empty($stale[$server_group['Name']])) {
          unset($stale[$server_group['Name']]);
        }
        if ($update) {
          OpenStackBatchOperations::updateServerGroup($this->cloudContext, $server_group);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = TRUE;
    }
    elseif ($clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as snapshot entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   The snapshot IDs.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSnapshotEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE): bool {
    $updated = FALSE;
    $result = $this->describeSnapshots();

    if (isset($result)) {

      $all_snapshots = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the snapshot by setting up
      // the array with the snapshot_id.
      foreach ($all_snapshots ?: [] as $snapshot) {
        $stale[$snapshot->getSnapshotId()] = $snapshot;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Snapshot Update');

      foreach ($result['Snapshots'] ?: [] as $snapshot) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (isset($stale[$snapshot['SnapshotId']])) {
          unset($stale[$snapshot['SnapshotId']]);
        }

        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateSnapshot',
        ], [$cloud_context, $snapshot]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the OpenStackSnapshots.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSnapshots(): bool {
    $entity_type = 'openstack_snapshot';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSnapshotEntities($entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as FloatingIp entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateFloatingIpEntities($entity_type = '', $cloud_context = ''): bool {
    $updated = FALSE;
    $result = $this->describeAddresses();

    if (isset($result)) {

      $all_ips = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the groups by setting up
      // the array with the group_id.
      foreach ($all_ips ?: [] as $ip) {
        $stale[$ip->getPublicIp()] = $ip;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('FloatingIp Update');

      foreach ($result['Addresses'] ?: [] as $floating_ip) {
        // Keep track of IPs that do not exist anymore
        // delete them after saving the rest of the IPs.
        if (isset($stale[$floating_ip['PublicIp']])) {
          unset($stale[$floating_ip['PublicIp']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateFloatingIp',
        ], [$cloud_context, $floating_ip]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the OpenStackFloatingIps.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateFloatingIps(): bool {
    $entity_type = 'openstack_floating_ip';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateFloatingIpEntities($entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Get regions list from API.
   *
   * @param array $params
   *   The array of API parameters.
   * @param array $credentials
   *   The array of credentials.
   *
   * @return object
   *   The array of API result.
   */
  public function describeRegions(array $params = [], array $credentials = []) {
    $results = $this->execute('DescribeRegions', $params, $credentials);
    return $results;
  }

  /**
   * Launch instance.
   *
   * @param array $params
   *   The array of API parameters.
   * @param array $tags
   *   The array of tags.
   * @param string $bundle
   *   The bundle.
   */
  public function runInstances(array $params = [], array $tags = [], string $bundle = 'openstack') {
    $params += $this->getDefaultParameters();

    $results = $this->execute('RunInstances', $params);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultParameters() {
    return [];
  }

  /**
   * Function will get instances that needs to be terminated.
   *
   * Query the openstack_instance table and return instances that are past a
   * certain timestamp.
   *
   * @return array
   *   An array of expired instance objects.
   */
  private function getExpiredInstances() {
    $expired_instances = [];
    /** @var \Drupal\Core\Entity\EntityStorageInterface $entity_storage $entity_storage */
    $entity_storage = $this->entityTypeManager->getStorage('openstack_instance');
    $entity_ids = $entity_storage
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('termination_timestamp', time(), '<')
      ->condition('termination_timestamp', 0, '!=')
      ->condition('termination_timestamp', NULL, 'IS NOT NULL')
      ->condition('instance_state', ['running', 'stopped'], 'IN')
      ->condition('cloud_context', $this->cloudContext)
      ->execute();
    $entities = $entity_storage->loadMultiple($entity_ids);
    foreach ($entities ?: [] as $entity) {
      /** @var \Drupal\openstack\Entity\OpenStackInstance $entity */
      $expired_instances['InstanceIds'][] = $entity->getInstanceId();
    }
    return $expired_instances;
  }

  /**
   * {@inheritdoc}
   */
  public function terminateExpiredInstances() {
    // Terminate instances past the timestamp.
    $instances = $this->getExpiredInstances();
    if (!empty($instances)) {

      $this->logger('openstack')->info($this->t('Terminating the following instances %instance', [
        '%instance' => implode(', ', $instances['InstanceIds']),
      ]));

      $this->terminateInstances($instances);
    }
  }

  /**
   * Get image IDs of pending images.
   *
   * @return array
   *   Images IDs of pending images.
   */
  private function getPendingImageIds() {
    $image_ids = [];
    $entity_storage = $this->entityTypeManager->getStorage('openstack_image');
    $entity_ids = $entity_storage
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', 'pending')
      ->condition('cloud_context', $this->cloudContext)
      ->execute();
    $entities = $entity_storage->loadMultiple($entity_ids);
    foreach ($entities ?: [] as $entity) {
      $image_ids[] = $entity->getImageId();
    }
    return $image_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function updatePendingImages() {
    $image_ids = $this->getPendingImageIds();
    if (count($image_ids)) {
      $this->updateImages([
        'ImageIds' => $image_ids,
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function describeVolumeTypes($param = []) {
    // This is a dummy implementation.
    return ['lvmdriver-1' => 'lvmdriver-1', '__DEFAULT__' => '__DEFAULT__'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultVolumeType($param = []) {
    // This is a dummy implementation.
    return 'lvmdriver-1';
  }

  /**
   * {@inheritdoc}
   */
  public function createResourceQueueItems(CloudConfigInterface $cloud_config): void {
    $queue_limit = $this->configFactory->get('openstack.settings')->get('openstack_queue_limit');
    $method_names = [
      'terminateExpiredInstances',
      'updateInstances',
      'updateImages',
      'updateKeyPairs',
      'updateSecurityGroups',
      'updateVolumes',
      'updateSnapshots',
      'updateNetworks',
      'updateFloatingIps',
      'updatePendingImages',
    ];
    self::updateResourceQueue($cloud_config, $method_names, $queue_limit);
  }

  /**
   * {@inheritdoc}
   */
  public function updateSecurityGroup(array $params = []): array {
    // This is a stub method, which is unnecessary to implement.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createSecurityGroupRule(array $params = []): array {
    // This is a stub method, which is unnecessary to implement.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function revokeSecurityGroupRule(array $params = []): array {
    // This is a stub method, which is unnecessary to implement.
    return ['Success' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function getTagKeyCreatedByUid(
    string $bundle,
    string $cloud_context,
    int $uid = 0,
  ): string {
    $tag_name = $this->cloudService->getTagKeyCreatedByUid($bundle, $cloud_context);
    return strlen($tag_name . ".{$uid}") >= OpenStackServiceInterface::MAX_TAG_LENGTH
      ? substr($tag_name, 0, OpenStackServiceInterface::MAX_TAG_LENGTH - strlen(".{$uid}") - 1)
      : $tag_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getTagValueCreatedByUid(
    string $bundle,
    string $cloud_context,
    int $uid,
  ): string {
    return $this->cloudService->getTagValueCreatedByUid($bundle, $cloud_context, $uid);
  }

  /**
   * {@inheritdoc}
   */
  public function updateNetworks(array $params = [], $clear = TRUE): bool {
    // @todo Implement updateNetworks() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateNetworkEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    // @todo Implement updateNetworkEntities() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateSubnets(array $params = [], $clear = TRUE): bool {
    // @todo Implement updateSubnets() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateSubnetEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    // @todo Implement updateSubnetEntities() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updatePorts(array $params = [], $clear = TRUE): bool {
    // @todo Implement updatePorts() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updatePortEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    // @todo Implement updatePortEntities() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateRouters(array $params = [], $clear = TRUE): bool {
    // @todo Implement updateRouters() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateRouterEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    // @todo Implement updateRouterEntities() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function describeQuotas(array $params = []): array {
    // @todo Implement describeQuotas() method.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function updateQuotas(array $params = [], $clear = TRUE): bool {
    // @todo Implement updateQuotas() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateQuotaEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    // @todo Implement updateQuotaEntities() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function describeStacks(array $params = []): array {
    // @todo Implement describeStacks() method.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function showStack(array $params = []): array {
    // @todo Implement showStack() method.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function updateStacks(array $params = [], $clear = TRUE): bool {
    // @todo Implement updateStacks() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateStackEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    // @todo Implement updateStackEntities() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function describeServerGroups(array $params = []): array {
    // @todo Implement describeServerGroups() method.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function updateServerGroups(array $params = [], $clear = TRUE): bool {
    // @todo Implement updateServerGroups() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateServerGroupEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    // @todo Implement updateQuotaEntities() method.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createImageFromInstance(array $params = []): ?array {
    // @todo Implement createImageFromInstance() method.
    $this->messenger->addWarning($this->t('OpenStack EC2 service does not support creating image from an instance at this time.'));
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function createPortInterface(array $params = []): ?array {
    $this->messenger->addWarning($this->t('OpenStack EC2 service does not support creating port interface at this time.'));
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deletePortInterface(array $params = []): ?array {
    $this->messenger->addWarning($this->t('OpenStack EC2 service does not support deleting port interface at this time.'));
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function modifyInstance(array $params = []): array {
    $this->messenger->addWarning($this->t('OpenStack EC2 service does not support modifying instance at this time.'));
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function describeCurrentUserRoles(): array {
    $this->messenger->addWarning($this->t('OpenStack EC2 service does not support getting the roles of current user at this time.'));
    return [];
  }

}
