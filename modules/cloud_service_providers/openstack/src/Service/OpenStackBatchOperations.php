<?php

namespace Drupal\openstack\Service;

use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\openstack\Entity\OpenStackAvailabilityZone;
use Drupal\openstack\Entity\OpenStackFlavor;
use Drupal\openstack\Entity\OpenStackFloatingIp;
use Drupal\openstack\Entity\OpenStackImage;
use Drupal\openstack\Entity\OpenStackImageInterface;
use Drupal\openstack\Entity\OpenStackInstance;
use Drupal\openstack\Entity\OpenStackKeyPair;
use Drupal\openstack\Entity\OpenStackNetwork;
use Drupal\openstack\Entity\OpenStackPort;
use Drupal\openstack\Entity\OpenStackProject;
use Drupal\openstack\Entity\OpenStackQuota;
use Drupal\openstack\Entity\OpenStackRole;
use Drupal\openstack\Entity\OpenStackRouter;
use Drupal\openstack\Entity\OpenStackSecurityGroup;
use Drupal\openstack\Entity\OpenStackServerGroup;
use Drupal\openstack\Entity\OpenStackSnapshot;
use Drupal\openstack\Entity\OpenStackStack;
use Drupal\openstack\Entity\OpenStackStackEvent;
use Drupal\openstack\Entity\OpenStackStackResource;
use Drupal\openstack\Entity\OpenStackSubnet;
use Drupal\openstack\Entity\OpenStackTemplateVersion;
use Drupal\openstack\Entity\OpenStackUser;
use Drupal\openstack\Entity\OpenStackVolume;
use Drupal\openstack\Service\Rest\OpenStackService as OpenStackRestService;

/**
 * Entity update methods for Batch API processing.
 */
class OpenStackBatchOperations {

  use CloudContentEntityTrait;

  /**
   * The finish callback function.
   *
   * Deletes stale entities from the database.
   *
   * @param string $entity_type
   *   The entity type.
   * @param array $stale
   *   The stale entities to delete.
   * @param bool $clear
   *   TRUE to clear entities, FALSE keep them.
   */
  public static function finished($entity_type, array $stale, $clear = TRUE): void {
    $entity_type_manager = \Drupal::entityTypeManager();
    if (count($stale) && $clear === TRUE) {
      $entity_type_manager->getStorage($entity_type)->delete($stale);
    }
  }

  /**
   * Update or create an instance entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $instance
   *   The instance array.
   */
  public static function updateInstance(string $cloud_context, array $instance): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);
    $timestamp = time();

    // Get volumeIDs associated to instances.
    $block_devices = [];
    if (isset($instance['BlockDeviceMappings'])) {
      foreach ($instance['BlockDeviceMappings'] ?: [] as $block_device) {
        $block_devices[] = $block_device['Ebs']['VolumeId'] ?? '';
      }
    }

    $instance_name = '';
    $uid = 0;
    $termination_timestamp = NULL;
    $tags = [];
    $uid_key_name = $openstack_service->getTagKeyCreatedByUid(
      'openstack',
      $cloud_context
    );
    if (!isset($instance['Tags'])) {
      $instance['Tags'] = [];
    }

    $termination_protection = FALSE;
    foreach ($instance['Tags'] ?: [] as $tag) {
      if ($tag['Key'] === 'Name') {
        $instance_name = $tag['Value'];
      }
      if ($tag['Key'] === $uid_key_name) {
        $uid = $tag['Value'];
      }
      if ($tag['Key'] === 'openstack_' . OpenStackInstance::TAG_TERMINATION_TIMESTAMP) {
        if ($tag['Value'] !== '') {
          $termination_timestamp = (int) $tag['Value'];
        }
      }

      if ($tag['Key'] === 'termination_protection') {
        $termination_protection = $tag['Value'] === 1;
      }

      $tags[] = ['item_key' => $tag['Key'], 'item_value' => $tag['Value']];
    }

    usort($tags, static function ($a, $b) {
      if ($a['item_key'] === 'Name') {
        return -1;
      }

      if ($b['item_key'] === 'Name') {
        return 1;
      }

      return strcmp($a['item_key'], $b['item_key']);
    });

    $instance_name_value = !empty($instance['InstanceName']) ? $instance['InstanceName'] : '';

    // Default to instance_id.
    if (empty($instance_name)) {
      $instance_name = !empty($instance_name_value) ? $instance_name_value : $instance['InstanceId'];
    }

    $security_groups = [];
    foreach ($instance['SecurityGroups'] ?: [] as $security_group) {
      $security_groups[] = $security_group['GroupName'];
    }

    if (method_exists($openstack_service, 'describeInstanceAttribute')) {

      // Termination protection.
      $attribute_result = $openstack_service->describeInstanceAttribute([
        'InstanceId' => $instance['InstanceId'],
        'Attribute' => 'disableApiTermination',
      ]);
      $termination_protection = $attribute_result['DisableApiTermination']['Value'];

      // Get user data.
      $attribute_result = $openstack_service->describeInstanceAttribute([
        'InstanceId' => $instance['InstanceId'],
        'Attribute' => 'userData',
      ]);
    }

    $user_data = !empty($attribute_result['UserData']['Value'])
      ? base64_decode($attribute_result['UserData']['Value'])
      : '';

    // Use NetworkInterface to look up private IPs.  In EC2-VPC,
    // an instance can have more than one private IP.
    $network_interfaces = [];
    $private_ips = FALSE;

    if (isset($instance['NetworkInterfaces'])) {
      $private_ips = $openstack_service->getPrivateIps($instance['NetworkInterfaces']);
      foreach ($instance['NetworkInterfaces'] ?: [] as $interface) {
        $network_interfaces[] = $interface['NetworkInterfaceId'];
      }
    }
    elseif (isset($instance['PrivateIpAddress'])) {
      $private_ips = $instance['PrivateIpAddress'];
    }

    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_instance', 'instance_id', $instance['InstanceId']);
    $launch_time = is_object($instance['LaunchTime']) ? strtotime($instance['LaunchTime']->__toString()) : strtotime($instance['LaunchTime']);
    $launch_time += (int) date('Z');

    $port_id = $instance['PortId'] ?? [];
    $port_ip_address = $instance['PortIpAddress'] ?? [];

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      $entity = OpenStackInstance::load($entity_id);
      $entity->setName($instance_name);
      $entity->setInstanceState($instance['State']['Name']);
      $entity->setPowerState($instance['PowerState'] ?? NULL);
      $entity->setInstanceId($instance['InstanceId']);

      // Set attributes that are available when system starts up.
      $public_ip = NULL;

      if ($private_ips !== FALSE) {
        $entity->setPrivateIps($private_ips);
      }
      if (isset($instance['PublicIpAddress'])) {
        $public_ip = $instance['PublicIpAddress'];
      }
      if (isset($instance['PublicDnsName'])) {
        $entity->setPublicDns($instance['PublicDnsName']);
      }
      if (isset($instance['PrivateDnsName'])) {
        $entity->setPrivateDns($instance['PrivateDnsName']);
      }

      $entity->setPublicIp($public_ip);
      $entity->setSecurityGroups(implode(', ', $security_groups));
      $entity->setInstanceType($instance['InstanceType']);
      $entity->setRefreshed($timestamp);
      $entity->setLaunchTime($launch_time);
      $entity->setPortIds($port_id);
      $entity->setPortIpAddresses($port_ip_address);
      $entity->setTerminationTimestamp($termination_timestamp ?? NULL);
      $entity->setTerminationProtection($termination_protection ?? NULL);
      $entity->setUserData($user_data);
      $entity->setTags($tags);
      $entity->setNetworkInterfaces($network_interfaces);
      $entity->setBlockDevices(implode(', ', $block_devices));
      $entity->setOwnerById($uid > 0 ? $uid : 0);
      $entity->save();
    }
    else {
      $entity = OpenStackInstance::create([
        'cloud_context' => $cloud_context,
        'name' => $instance_name ?? $instance['InstanceId'],
        'account_id' => $instance['reservation_ownerid'],
        'security_groups' => implode(', ', $security_groups),
        'instance_id' => $instance['InstanceId'],
        'instance_type' => $instance['InstanceType'] ?? NULL,
        'availability_zone' => $instance['Placement']['AvailabilityZone'],
        'tenancy' => $instance['Placement']['Tenancy'] ?? NULL,
        'instance_state' => $instance['State']['Name'],
        'power_state' => $instance['PowerState'] ?? NULL,
        'public_dns' => $instance['PublicDnsName'],
        'public_ip' => $instance['PublicIpAddress'] ?? NULL,
        'port_id' => $port_id,
        'port_ip_address' => $port_ip_address,
        'private_dns' => $instance['PrivateDnsName'] ?? NULL,
        'key_pair_name' => $instance['KeyName'] ?? NULL,
        'is_monitoring' => $instance['Monitoring']['State'] ?? NULL,
        'vpc_id' => $instance['VpcId'] ?? NULL,
        'subnet_id' => $instance['SubnetId'] ?? NULL,
        'source_dest_check' => $instance['SourceDestCheck'] ?? NULL,
        'ebs_optimized' => $instance['EbsOptimized'] ?? NULL,
        'root_device_type' => $instance['RootDeviceType'] ?? NULL,
        'root_device' => $instance['RootDeviceName'] ?? NULL,
        'image_id' => $instance['ImageId'],
        'placement_group' => $instance['Placement']['GroupName'] ?? NULL,
        'virtualization' => $instance['VirtualizationType'] ?? NULL,
        'reservation' => $instance['reservation_id'] ?? NULL,
        'ami_launch_index' => $instance['AmiLaunchIndex'] ?? NULL,
        'host_id' => $instance['Placement']['HostId'] ?? NULL,
        'affinity' => $instance['Placement']['Affinity'] ?? NULL,
        'state_transition_reason' => $instance['StateTransitionReason'] ?? NULL,
        'instance_lock' => FALSE,
        'launch_time' => $launch_time,
        'created' => $launch_time,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
        'termination_timestamp' => $termination_timestamp ?? NULL,
        'termination_protection' => $termination_protection ?? NULL,
        'user_data' => $user_data,
        'tags' => $tags,
        'block_devices' => implode(', ', $block_devices),
      ]);

      if ($private_ips !== FALSE) {
        $entity->setPrivateIps($private_ips);
      }
      $entity->setNetworkInterfaces($network_interfaces);
      $entity->save();
    }
  }

  /**
   * Update or create an image entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $image
   *   The image array.
   */
  public static function updateImage(string $cloud_context, array $image): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);
    $timestamp = time();

    $block_device_mappings = [];
    $image_block_device = $image['BlockDeviceMappings'] ?? [];
    foreach ($image_block_device ?: [] as $block_device) {
      $device = [
        'device_name' => $block_device['DeviceName'],
        'virtual_name' => $block_device['VirtualName'] ?? '',
      ];
      if (!empty($block_device['Ebs'])) {
        $device['delete_on_termination'] = $block_device['Ebs']['DeleteOnTermination'] ?? '';
        $device['snapshot_id'] = $block_device['Ebs']['SnapshotId'] ?? '';
        $device['volume_size'] = $block_device['Ebs']['VolumeSize'] ?? '';
        $device['volume_type'] = $block_device['Ebs']['VolumeType'] ?? '';
        $device['encrypted'] = $block_device['Ebs']['Encrypted'] ?? '';
      }
      $block_device_mappings[] = $device;
    }

    // Shared Projects.
    $shared_projects = [];
    if ($openstack_service instanceof OpenStackRestService
      && !empty($image['ImageVisibility'])
      && $image['ImageVisibility'] === OpenStackImageInterface::VISIBILITY_SHARED) {

      $result = $openstack_service->describeProjectSharedImage([
        'ImageId' => $image['ImageId'],
        'CloudContext' => $cloud_context,
      ]);

      if (!empty($result['Members'])) {
        $shared_projects = array_map(static function ($item) {
          if (!empty($item['ProjectId'])
            && !empty($item['Status'])) {
            return [
              'cloud_context' => $item['CloudContext'],
              'project_id' => $item['ProjectId'],
              'status' => $item['Status'],
            ];
          }
        }, $result['Members'] ?: []);
      }
      if (!empty($image['Tags'])) {
        // Set inter region to members.
        $shared_projects = array_merge($shared_projects, $openstack_service->getImageSharedProjectsTagValue($image['Tags']));
      }
    }

    $image_name = $image['Name'] ?? '';
    $name = $openstack_service->getTagName($image, $image_name);
    $uid = $openstack_service->getUidTagValue($image, 'openstack_image');
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_image', 'image_id', $image['ImageId']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      try {
        $entity = OpenStackImage::load($entity_id);
        $entity->setName($name);
        $entity->setRefreshed($timestamp);
        $entity->setImageVisibility($image['ImageVisibility']);
        $entity->setStatus($image['State']);
        $entity->setBlockDeviceMappings($block_device_mappings);
        $entity->setSharedProjects($shared_projects);
        $entity->setOwnerById($uid > 0 ? $uid : 0);
        $entity->save();
      }
      catch (\Exception $e) {
        \Drupal::service('cloud')->handleException($e);
      }
    }
    else {
      $entity = OpenStackImage::create([
        'cloud_context' => $cloud_context,
        'image_id' => $image['ImageId'],
        'account_id' => $image['OwnerId'],
        'architecture' => $image['Architecture'] ?? '',
        'virtualization_type' => $image['VirtualizationType'] ?? '',
        'root_device_type' => $image['RootDeviceType'] ?? '',
        'root_device_name' => $image['RootDeviceName'] ?? '',
        'ami_name' => '',
        'name' => $image['Name'] ?? '',
        'kernel_id' => $image['KernelId'] ?? '',
        'ramdisk_id' => $image['RamdiskId'] ?? '',
        'image_type' => $image['ImageType'],
        'product_code' => isset($image['ProductCodes']) ? implode(',', array_column($image['ProductCodes'], 'ProductCode')) : '',
        'source' => $image['ImageLocation'] ?? '',
        'state_reason' => isset($image['StateReason']) ? $image['StateReason']['Message'] : '',
        'platform' => $image['Platform'] ?? '',
        'description' => $image['Description'] ?? '',
        'image_visibility' => $image['ImageVisibility'],
        'block_device_mappings' => $block_device_mappings,
        'shared_projects' => $shared_projects,
        'status' => $image['State'],
        'created' => strtotime($image['CreationDate'] ?? ''),
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
      $entity->save();
    }
  }

  /**
   * Update or create a key pair entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $key_pair
   *   The key_pair array.
   */
  public static function updateKeyPair(string $cloud_context, array $key_pair): void {
    $timestamp = time();

    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_key_pair', 'key_pair_name', $key_pair['KeyName']);

    if (!empty($entity_id)) {
      $entity = OpenStackKeyPair::load($entity_id);
      if (!empty($entity)) {
        $entity->setRefreshed($timestamp);
        $entity->save();
      }
    }
    else {
      $entity = OpenStackKeyPair::create([
        'cloud_context' => $cloud_context,
        'key_pair_name' => $key_pair['KeyName'],
        'key_fingerprint' => $key_pair['KeyFingerprint'],
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
      ]);
      $entity->save();
    }
  }

  /**
   * Update or create a security group entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $security_group
   *   The security_group array.
   */
  public static function updateSecurityGroup(string $cloud_context, array $security_group): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);

    $timestamp = time();
    $name = $openstack_service->getTagName($security_group, $security_group['GroupName']);
    $uid = $openstack_service->getUidTagValue($security_group, 'openstack_security_group');
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_security_group', 'group_id', $security_group['GroupId']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\SecurityGroup $entity */
      try {
        $entity = OpenStackSecurityGroup::load($entity_id);
        $entity->setName($name);
        $entity->setRefreshed($timestamp);
        $entity->setOwnerById($uid > 0 ? $uid : 0);
      }
      catch (\Exception $e) {
        \Drupal::service('cloud')->handleException($e);
        return;
      }
    }
    else {
      // Create a brand new SecurityGroup entity.
      $entity = OpenStackSecurityGroup::create([
        'cloud_context' => $cloud_context,
        'name' => $security_group['GroupName'] ?? $security_group['GroupId'],
        'group_id' => $security_group['GroupId'],
        'group_name' => $security_group['GroupName'],
        'description' => $security_group['Description'],
        'vpc_id' => $security_group['VpcId'] ?? NULL,
        'account_id' => $security_group['OwnerId'],
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
    }

    if ((!$openstack_service instanceof OpenStackRestService)
      && !empty($security_group['VpcId'])) {
      // Check if VPC is default.  This involves another API call.
      $vpcs = $openstack_service->describeVpcs([
        'VpcIds' => [$security_group['VpcId']],
      ]);
      if ($vpcs['Vpcs']) {
        $default = $vpcs['Vpcs'][0]['IsDefault'];
        $entity->setDefaultVpc($default);
      }
    }

    // Set up the Inbound permissions.
    if (isset($security_group['IpPermissions'])) {
      $openstack_service->setupIpPermissions($entity, 'ip_permission', $security_group['IpPermissions']);
    }

    // Setup outbound permissions of REST service.
    if ($openstack_service instanceof OpenStackRestService
      && isset($security_group['IpPermissionsEgress'])) {
      $openstack_service->setupIpPermissions($entity, 'outbound_permission', $security_group['IpPermissionsEgress']);
    }

    // Setup outbound permissions of EC2 service.
    if (!($openstack_service instanceof OpenStackRestService)
      && isset($security_group['VpcId'])
      && isset($security_group['IpPermissionsEgress'])) {
      $openstack_service->setupIpPermissions($entity, 'outbound_permission', $security_group['IpPermissionsEgress']);
    }

    $entity->save();
  }

  /**
   * Update or create a volume entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $volume
   *   The volume array.
   * @param array $snapshot_id_name_map
   *   The snapshot map.
   */
  public static function updateVolume(string $cloud_context, array $volume, array $snapshot_id_name_map): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);

    $timestamp = time();

    $attachments = [];
    $device_names = [];
    foreach ($volume['Attachments'] ?: [] as $attachment) {
      $attachments[] = !empty($attachment['InstanceId']) ? $attachment['InstanceId'] : '';
      $device_names[] = !empty($attachment['Device']) ? $attachment['Device'] : '';
    }

    $volume_name = !empty($volume['Name'])
      ? $volume['Name'] : $volume['VolumeId'];

    $name = $openstack_service->getTagName($volume, $volume_name);
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_volume', 'volume_id', $volume['VolumeId']);
    $uid = $openstack_service->getUidTagValue($volume, 'openstack_volume');

    if ($uid === 0) {
      // Inherit the volume uid from the instance that launched it.
      if (count($attachments) && !empty($attachments[0])) {
        $uid = $openstack_service->getInstanceUid($attachments[0]);
      }
    }

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackVolume $entity */
      $entity = OpenStackVolume::load($entity_id);
      $entity->setName(!empty($name) ? $name : $volume['VolumeId']);
      $entity->setRefreshed($timestamp);
      $entity->setState($volume['State']);
      $entity->setAttachmentInformation(implode(', ', $attachments));
      $entity->setAttachmentDeviceName(implode(', ', $device_names));
      $entity->setCreated(strtotime($volume['CreateTime']));
      $entity->setSnapshotId($volume['SnapshotId']);
      $entity->setSnapshotName(empty($volume['SnapshotId'])
        ? ''
        : $snapshot_id_name_map[$volume['SnapshotId']]);
      $entity->setSize($volume['Size']);
      $entity->setVolumeType($volume['VolumeType']);

      $uid > 0 ? $entity->setOwnerById($uid) : 0;
      $entity->setDescription(empty($volume['Description'])
        ? ''
        : $volume['Description']);
      $entity->save();
    }
    else {
      $entity = OpenStackVolume::create([
        'cloud_context' => $cloud_context,
        'name' => !empty($name) ? $name : $volume['VolumeId'],
        'volume_id' => $volume['VolumeId'],
        'size' => $volume['Size'],
        'state' => $volume['State'],
        'volume_status' => $volume['VirtualizationType'] ?? NULL,
        'attachment_information' => implode(', ', $attachments),
        'attachment_device_name' => implode(', ', $device_names),
        'volume_type' => $volume['VolumeType'],
        'snapshot_id' => $volume['SnapshotId'],
        'snapshot_name' => empty($volume['SnapshotId']) ? '' : $snapshot_id_name_map[$volume['SnapshotId']],
        'availability_zone' => $volume['AvailabilityZone'],
        'encrypted' => $volume['Encrypted'],
        'kms_key_id' => $volume['KmsKeyId'] ?? NULL,
        'created' => strtotime($volume['CreateTime']),
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'description' => empty($volume['Description'])
          ? ''
          : $volume['Description'],
        'uid' => $uid,
      ]);
      $entity->save();
    }
  }

  /**
   * Update or create a snapshot entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $snapshot
   *   The snapshot array.
   */
  public static function updateSnapshot($cloud_context, array $snapshot): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);

    $timestamp = time();
    $name = $openstack_service->getTagName($snapshot, $snapshot['SnapshotId']);
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_snapshot', 'snapshot_id', $snapshot['SnapshotId']);
    $uid = $openstack_service->getUidTagValue($snapshot, 'openstack_snapshot');

    $snapshot_encrypted = NULL;
    if (isset($snapshot['Encrypted'])) {
      $snapshot_encrypted = $snapshot['Encrypted'] === FALSE ? 'Not Encrypted' : 'Encrypted';
    }

    if (!empty($entity_id)) {
      $entity = OpenStackSnapshot::load($entity_id);
      if (!empty($entity)) {
        $entity->setName($name);
        $entity->setStatus($snapshot['State']);
        $entity->setSize($snapshot['VolumeSize']);
        $entity->setRefreshed($timestamp);
        $uid > 0 ? $entity->setOwnerById($uid) : 0;
        $entity->setCreated(strtotime($snapshot['StartTime']));
        $entity->save();
      }
    }
    else {
      $entity = OpenStackSnapshot::create([
        'cloud_context' => $cloud_context,
        'name' => $name,
        'snapshot_id' => $snapshot['SnapshotId'],
        'size' => $snapshot['VolumeSize'],
        'description' => $snapshot['Description'],
        'status' => $snapshot['State'],
        'volume_id' => $snapshot['VolumeId'],
        'progress' => $snapshot['Progress'],
        'encrypted' => $snapshot_encrypted ?? NULL,
        'kms_key_id' => $snapshot['KmsKeyId'] ?? NULL,
        'account_id' => $snapshot['OwnerId'],
        'owner_aliases' => $snapshot['OwnerAlias'] ?? NULL,
        'state_message' => $snapshot['StateMessage'] ?? NULL,
        'created' => strtotime($snapshot['StartTime']),
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
      $entity->save();
    }
  }

  /**
   * Update or create a network entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $network
   *   The network array.
   */
  public static function updateNetwork(string $cloud_context, array $network): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);

    $timestamp = time();

    // The tag key of the network is 'TagSet'.
    // So changing the key to align to other entities.
    // If this key changes to 'Tags' on OpenStack API, this block needs to be
    // deleted.
    if (!empty($network['TagSet'])) {
      $network['Tags'] = $network['TagSet'];
    }

    $network_name = $network['Name'] ?? $network['NetworkId'];
    $name = $openstack_service->getTagName($network, $network_name);
    $uid = $openstack_service->getUidTagValue($network, 'openstack_network');
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_network', 'network_id', $network['NetworkId']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackNetwork $entity */
      $entity = OpenStackNetwork::load($entity_id);
      $entity->setName(!empty($name) ? $name : $network['NetworkId']);
      $entity->setNetworkId($network['NetworkId']);
      $entity->setProjectId($network['ProjectId']);
      $entity->setStatus($network['Status']);
      $entity->setAdminStateUp(!empty($network['AdminStateUp']));
      $entity->setShared(!empty($network['Shared']));
      $entity->setExternal(!empty($network['External']));
      $entity->setMtu($network['Mtu']);
      $entity->setNetworkType($network['NetworkType']);
      $entity->setPhysicalNetwork($network['PhysicalNetwork']);
      $entity->setSegmentationId($network['SegmentationId']);
      $entity->setSubnets($network['Subnets']);
      $entity->setAvailabilityZones($network['AvailabilityZones']);

      $entity->setOwnerById($uid > 0 ? $uid : 0);
      $entity->setRefreshed($timestamp);
      $entity->save();
    }
    else {
      $entity = OpenStackNetwork::create([
        'cloud_context' => $cloud_context,
        'name' => !empty($name) ? $name : $network['NetworkId'],
        'network_id' => $network['NetworkId'],
        'project_id' => $network['ProjectId'],
        'status' => $network['Status'],
        'admin_state_up' => !empty($network['AdminStateUp']),
        'shared' => !empty($network['Shared']),
        'external' => !empty($network['External']),
        'mtu' => $network['Mtu'],
        'network_type' => $network['NetworkType'],
        'physical_network' => $network['PhysicalNetwork'],
        'segmentation_id' => $network['SegmentationId'],
        'subnets' => $network['Subnets'],
        'availability_zones' => $network['AvailabilityZones'],
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
      $entity->save();
    }
  }

  /**
   * Update or create a subnet entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $subnet
   *   The subnet array.
   */
  public static function updateSubnet(string $cloud_context, array $subnet): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);

    $timestamp = time();

    // The tag key of the network is 'TagSet'.
    // So changing the key to align to other entities.
    // If this key changes to 'Tags' on OpenStack API, this block needs to be
    // deleted.
    if (!empty($subnet['TagSet'])) {
      $subnet['Tags'] = $subnet['TagSet'];
    }

    $subnet_name = $subnet['Name'] ?? $subnet['SubnetId'];
    $name = $openstack_service->getTagName($subnet, $subnet_name);
    $uid = $openstack_service->getUidTagValue($subnet, 'openstack_subnet');
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_subnet', 'subnet_id', $subnet['SubnetId']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\Subnet $entity */
      $entity = OpenStackSubnet::load($entity_id);
      $entity->setName(!empty($name) ? $name : $subnet['SubnetId']);
      $entity->setSubnetId($subnet['SubnetId']);
      $entity->setProjectId($subnet['ProjectId']);
      $entity->setNetworkId($subnet['NetworkId']);
      $entity->setSubnetPoolId($subnet['SubnetPoolId']);
      $entity->setIpVersion($subnet['IpVersion']);
      $entity->setCidr($subnet['Cidr']);
      $entity->setAllocationPools($subnet['AllocationPools']);
      $entity->setGatewayIp($subnet['GatewayIp']);
      $entity->setEnableDhcp(!empty($subnet['EnableDhcp']));
      $entity->setHostRoutes($subnet['HostRoutes']);
      $entity->setDnsNameServers($subnet['DnsNameServers']);

      $entity->setOwnerById($uid > 0 ? $uid : 0);
      $entity->setRefreshed($timestamp);
      $entity->save();
    }
    else {
      $entity = OpenStackSubnet::create([
        'cloud_context' => $cloud_context,
        'name' => !empty($name) ? $name : $subnet['SubnetId'],
        'subnet_id' => $subnet['SubnetId'],
        'project_id' => $subnet['ProjectId'],
        'network_id' => $subnet['NetworkId'],
        'subnet_pool_id' => $subnet['SubnetPoolId'],
        'ip_version' => $subnet['IpVersion'],
        'cidr' => $subnet['Cidr'],
        'allocation_pools' => $subnet['AllocationPools'],
        'gateway_ip' => $subnet['GatewayIp'],
        'enable_dhcp' => !empty($subnet['EnableDhcp']),
        'host_routes' => $subnet['HostRoutes'],
        'dns_name_servers' => $subnet['DnsNameServers'],

        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
      $entity->save();
    }
  }

  /**
   * Update or create a port entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $port
   *   The port array.
   */
  public static function updatePort(string $cloud_context, array $port): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);

    $timestamp = time();

    // The tag key of the network is 'TagSet'.
    // So changing the key to align to other entities.
    // If this key changes to 'Tags' on OpenStack API, this block needs to be
    // deleted.
    if (!empty($port['TagSet'])) {
      $port['Tags'] = $port['TagSet'];
    }

    $port_name = empty($port['Name']) ? $port['PortId'] : $port['Name'];
    $name = $openstack_service->getTagName($port, $port_name);
    $uid = $openstack_service->getUidTagValue($port, 'openstack_port');
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_port', 'port_id', $port['PortId']);

    $profile = empty($port['BindingProfile'])
      ? ''
      : json_encode($port['BindingProfile']);

    $details_key_value = [];
    foreach ($port['BindingVifDetails'] ?: [] as $key => $value) {
      $details_key_value[] = [
        'item_key' => $key,
        'item_value' => json_encode($value),
      ];
    }

    $address_pairs = [];
    foreach ($port['AllowedAddressPairs'] ?: [] as $item) {
      $address_pairs[] = [
        'item_key' => $item['ip_address'],
        'item_value' => $item['mac_address'],
      ];
    }

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackPort $entity */

      $entity = OpenStackPort::load($entity_id);
      $entity->setName(!empty($name) ? $name : $port['PortId']);
      $entity->setPortId($port['PortId']);
      $entity->setProjectId($port['ProjectId']);
      $entity->setNetworkId($port['NetworkId']);
      $entity->setMacAddress($port['MacAddress']);
      $entity->setStatus($port['Status']);
      $entity->setAdminStateUp($port['AdminStateUp']);
      $entity->setPortSecurityEnabled($port['PortSecurityEnabled']);
      $entity->setDnsName($port['DnsName']);
      $entity->setDnsAssignment($port['DnsAssignment']);
      $entity->setFixedIps($port['FixedIps']);
      $entity->setDeviceOwner($port['DeviceOwner']);
      $entity->setDeviceId($port['DeviceId']);
      $entity->setSecurityGroups($port['SecurityGroups']);
      $entity->setBindingVnicType($port['BindingVnicType']);
      $entity->setBindingHostId($port['BindingHostId']);
      $entity->setBindingProfile($profile);
      $entity->setBindingVifType($port['BindingVifType']);
      $entity->setBindingVifDetails($details_key_value);
      $entity->setAllowedAddressPairs($address_pairs);

      $entity->setOwnerById($uid > 0 ? $uid : 0);
      $entity->setRefreshed($timestamp);
      $entity->save();
    }
    else {
      $entity = OpenStackPort::create([
        'cloud_context' => $cloud_context,
        'name' => !empty($name) ? $name : $port['PortId'],
        'port_id' => $port['PortId'],
        'project_id' => $port['ProjectId'],
        'network_id' => $port['NetworkId'],
        'mac_address' => $port['MacAddress'],
        'status' => $port['Status'],
        'admin_state_up' => $port['AdminStateUp'],
        'port_security_enabled' => $port['PortSecurityEnabled'],
        'dns_name' => $port['DnsName'],
        'dns_assignment' => !empty($port['DnsAssignment']),
        'fixed_ips' => $port['FixedIps'],
        'device_owner' => $port['DeviceOwner'],
        'device_id' => $port['DeviceId'],
        'security_groups' => $port['SecurityGroups'],
        'binding_vnic_type' => $port['BindingVnicType'],
        'binding_host_id' => $port['BindingHostId'],
        'binding_profile' => $profile,
        'binding_vif_type' => $port['BindingVifType'],
        'binding_vif_details' => $details_key_value,
        'allowed_address_pairs' => $address_pairs,

        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
      $entity->save();
    }
  }

  /**
   * Update or create a router entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $router
   *   The router array.
   */
  public static function updateRouter(string $cloud_context, array $router): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);

    $timestamp = time();

    // The tag key of the network is 'TagSet'.
    // So changing the key to align to other entities.
    // If this key changes to 'Tags' on OpenStack API, this block needs to be
    // deleted.
    if (!empty($router['TagSet'])) {
      $router['Tags'] = $router['TagSet'];
    }

    $router_name = empty($router['Name']) ? $router['PortId'] : $router['Name'];
    $name = $openstack_service->getTagName($router, $router_name);
    $uid = $openstack_service->getUidTagValue($router, 'openstack_router');
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_router', 'router_id', $router['RouterId']);

    $external_gateway_external_fixed_ips = [];
    $external_gateway_network_id = NULL;
    $external_gateway_enable_snat = NULL;
    $external_gateway_qos_policy_id = NULL;
    if (!empty($router['ExternalGateway'])) {
      foreach ($router['ExternalGateway']['external_fixed_ips'] ?: [] as $item) {
        $external_gateway_external_fixed_ips[] = sprintf('%s,%s', $item['ip_address'], $item['subnet_id']);
      }

      $external_gateway_network_id = $router['ExternalGateway']['network_id'] ?? NULL;
      $external_gateway_enable_snat = $router['ExternalGateway']['enable_snat'] ?? NULL;
      $external_gateway_qos_policy_id = $router['ExternalGateway']['qos_policy_id'] ?? NULL;
    }

    $routes = [];
    foreach ($router['Routes'] ?: [] as $item) {
      $routes[] = [
        'item_key' => $item['destination'],
        'item_value' => $item['nexthop'],
      ];
    }

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackRouter $entity */

      $entity = OpenStackRouter::load($entity_id);
      $entity->setName(!empty($name) ? $name : $router['RouterId']);
      $entity->setRouterId($router['RouterId']);
      $entity->setProjectId($router['ProjectId']);
      $entity->setStatus($router['Status']);
      $entity->setAdminStateUp($router['AdminStateUp']);
      $entity->setAvailabilityZones($router['AvailabilityZones']);
      $entity->setRoutes($routes);

      $entity->setExternalGatewayNetworkId($external_gateway_network_id);
      $entity->setExternalGatewayEnableSnat($external_gateway_enable_snat);
      $entity->setExternalGatewayQosPolicyId($external_gateway_qos_policy_id);
      $entity->setExternalGatewayExternalFixedIps($external_gateway_external_fixed_ips);

      $entity->setOwnerById($uid > 0 ? $uid : 0);
      $entity->setRefreshed($timestamp);
      $entity->save();
    }
    else {
      $entity = OpenStackRouter::create([
        'cloud_context' => $cloud_context,
        'name' => !empty($name) ? $name : $router['RouterId'],
        'router_id' => $router['RouterId'],
        'project_id' => $router['ProjectId'],
        'status' => $router['Status'],
        'admin_state_up' => $router['AdminStateUp'],
        'availability_zones' => $router['AvailabilityZones'],
        'routes' => $routes,

        'external_gateway_network_id' => $external_gateway_network_id,
        'external_gateway_enable_snat' => $external_gateway_enable_snat,
        'external_gateway_qos_policy_id' => $external_gateway_qos_policy_id,
        'external_gateway_external_fixed_ips' => $external_gateway_external_fixed_ips,

        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
      $entity->save();
    }
  }

  /**
   * Update or create a quota entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $quota
   *   The quota array.
   */
  public static function updateQuota(string $cloud_context, array $quota): void {
    $timestamp = time();

    $uid = 0;
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_quota', 'name', $quota['Name']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackQuota $entity */

      $entity = OpenStackQuota::load($entity_id);

      $entity->setOwnerById($uid);
      $entity->setRefreshed($timestamp);
    }
    else {
      $entity = OpenStackQuota::create([
        'cloud_context' => $cloud_context,
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
        'label' => 'Quota',
      ]);
    }

    foreach ($quota as $key => $value) {
      $set_method = 'set' . $key;
      $entity->$set_method($value);
    }

    $entity->setStatus(OpenStackQuota::APPROVED);
    $entity->copyToNewFields();
    $entity->save();
  }

  /**
   * Update or create a stack entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $stack
   *   The stack array.
   */
  public static function updateStack(string $cloud_context, array $stack): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);

    $timestamp = time();

    // The tag key of the network is 'TagSet'.
    // So changing the key to align to other entities.
    // If this key changes to 'Tags' on OpenStack API, this block needs to be
    // deleted.
    if (!empty($stack['TagSet'])) {
      $stack['Tags'] = $stack['TagSet'];
    }

    $stack_name = $stack['Name'] ?? $stack['StackId'];
    $name = $openstack_service->getTagName($stack, $stack_name);
    $uid = $openstack_service->getUidTagValue($stack, 'openstack_stack');
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_stack', 'stack_id', $stack['StackId']);

    $params = [
      'StackName' => $stack_name,
      'StackId' => $stack['StackId'],
      'ProjectId' => $stack['ProjectId'],
    ];

    $detail = $openstack_service->showStack($params);

    // Parameters.
    $parameters = [];
    foreach ($detail['Parameters'] ?: [] as $key => $value) {
      $parameters[] = [
        'item_key' => $key,
        'item_value' => $value,
      ];
    }

    // Outputs.
    $outputs = [];
    foreach ($detail['Outputs'] ?: [] as $item) {
      $outputs[] = [
        'item_key' => "{$item['output_key']}({$item['description']})",
        'item_value' => $item['output_value'] ?? '',
      ];
    }

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackStack $entity */
      $entity = OpenStackStack::load($entity_id);
      $entity->setRefreshed(!empty($detail['UpdatedTime']) ? strtotime($detail['UpdatedTime']) : $timestamp);
      $entity->setOwnerById($uid > 0 ? $uid : 0);
    }
    else {
      $created_time = !empty($detail['CreationTime']) ? strtotime($detail['CreationTime']) : $timestamp;
      $entity = OpenStackStack::create([
        'cloud_context' => $cloud_context,
        'name' => !empty($name) ? $name : $stack['StackId'],
        'created' => $created_time,
        'changed' => $created_time,
        'refreshed' => $created_time,
        'uid' => $uid,
      ]);
    }

    $entity->setDescription($stack['Description']);
    $entity->setStackId($stack['StackId']);
    $entity->setProjectId($stack['ProjectId']);
    $entity->setStackStatus($stack['StackStatus']);
    $entity->setStackStatusReason($stack['StackStatusReason']);

    $entity->setParameters($parameters);
    $entity->setOutputs($outputs);
    $entity->setRollback($detail['Rollback']);
    $entity->setTimeoutMins($detail['TimeoutMins']);
    $entity->setTemplate($detail['Template']);

    $entity->setOwnerById($uid > 0 ? $uid : 0);

    $entity->save();
  }

  /**
   * Update or create a stack resource entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $resource
   *   The resource array.
   */
  public static function updateStackResource(string $cloud_context, array $resource): void {
    $timestamp = time();
    $name = $resource['Name'] ?? $resource['ResourceId'];
    $uid = $resource['Uid'] ?? 0;
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_stack_resource', 'resource_id', $resource['ResourceId']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackStackResource $entity */
      $entity = OpenStackStackResource::load($entity_id);
      $entity->setRefreshed(!empty($resource['UpdatedTime']) ? strtotime($resource['UpdatedTime']) : $timestamp);
    }
    else {
      $created_time = !empty($resource['CreationTime']) ? strtotime($resource['CreationTime']) : $timestamp;
      $entity = OpenStackStackResource::create([
        'cloud_context' => $cloud_context,
        'name' => $name,
        'created' => $created_time,
        'changed' => $created_time,
        'refreshed' => $created_time,
      ]);
    }

    $entity->setStackEntityId($resource['StackEntityId']);
    $entity->setStackId($resource['StackId']);
    $entity->setProjectId($resource['ProjectId']);
    $entity->setResourceId($resource['ResourceId']);
    $entity->setResourceStatus($resource['ResourceStatus']);
    $entity->setResourceStatusReason($resource['ResourceStatusReason']);
    $entity->setResourceType($resource['ResourceType']);

    $entity->setOwnerById($uid > 0 ? $uid : 0);

    $entity->save();
  }

  /**
   * Update or create a stack event entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $event
   *   The event array.
   */
  public static function updateStackEvent(string $cloud_context, array $event): void {
    $timestamp = time();
    $name = $event['Name'] ?? $event['EventId'];
    $uid = $event['Uid'] ?? 0;
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_stack_event', 'event_id', $event['EventId']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackStackEvent $entity */
      $entity = OpenStackStackEvent::load($entity_id);
    }
    else {
      $created_time = !empty($event['EventTime']) ? strtotime($event['EventTime']) : $timestamp;
      $entity = OpenStackStackEvent::create([
        'cloud_context' => $cloud_context,
        'name' => $name,
        'created' => $created_time,
        'changed' => $created_time,
        'refreshed' => $created_time,
      ]);
    }

    $entity->setStackEntityId($event['StackEntityId']);
    $entity->setStackId($event['StackId']);
    $entity->setProjectId($event['ProjectId']);
    $entity->setEventId($event['EventId']);
    $entity->setResourceId($event['ResourceId']);
    $entity->setResourceName($event['ResourceName']);
    $entity->setResourceStatus($event['ResourceStatus']);
    $entity->setResourceStatusReason($event['ResourceStatusReason']);

    $entity->setOwnerById($uid > 0 ? $uid : 0);

    $entity->save();
  }

  /**
   * Update or create a template version entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $template_version
   *   The template version array.
   */
  public static function updateTemplateVersion(string $cloud_context, array $template_version): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);

    $timestamp = time();

    $uid = 0;
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_template_version', 'name', $template_version['Name']);

    $params = [
      'ProjectId' => $template_version['ProjectId'],
      'TemplateVersion' => $template_version['Name'],
    ];

    $result = $openstack_service->describeTemplateVersionFunctions($params);

    // Functions.
    $functions = [];
    foreach ($result['TemplateFunctions'] ?: [] as $item) {
      $functions[] = [
        'item_key' => $item['Name'],
        'item_value' => $item['Description'],
      ];
    }

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackTemplateVersion $entity */
      $entity = OpenStackTemplateVersion::load($entity_id);
      $entity->setOwnerById($uid);
    }
    else {
      $entity = OpenStackTemplateVersion::create([
        'cloud_context' => $cloud_context,
        'name' => $template_version['Name'],
        'type' => $template_version['Type'],
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
    }

    $entity->setFunctions($functions);
    $entity->save();
  }

  /**
   * Update or create a project entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $project
   *   The project array.
   * @param array $user_map
   *   The user map.
   * @param array $role_map
   *   The role map.
   */
  public static function updateProject(string $cloud_context, array $project, array $user_map, array $role_map): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);
    $result = $openstack_service->describeProjectUserRoles([
      'ProjectId' => $project['Id'],
    ]);

    $user_roles = [];
    $project_user_roles = array_key_exists('UserRoles', $result) ? $result['UserRoles'] : [];
    foreach ($project_user_roles ?: [] as $user_id => $role_ids) {
      if (empty($user_map[$user_id])) {
        continue;
      }

      $roles = [];
      foreach ($role_ids ?: [] as $role_id) {
        if (empty($role_map[$role_id])) {
          continue;
        }

        $roles[] = $role_map[$role_id];
      }

      // Sort by role name.
      usort($roles, static function ($a, $b) {
        return $a->getName() < $b->getName() ? -1 : 1;
      });

      $role_ids = array_map(static function ($role) {
        return $role->id();
      }, $roles);

      $user_roles[] = [
        'user' => $user_map[$user_id],
        'roles' => implode(',', $role_ids),
      ];
    }

    // Sort by username.
    usort($user_roles, static function ($a, $b) {
      return $a['user']->getName() < $b['user']->getName() ? -1 : 1;
    });

    // Update property user to id.
    $user_roles = array_map(static function ($item) {
      return [
        'user' => $item['user']->id(),
        'roles' => $item['roles'],
      ];
    }, $user_roles);

    $timestamp = time();

    $uid = 0;
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_project', 'project_id', $project['Id']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackProject $entity */
      $entity = OpenStackProject::load($entity_id);
      $entity->setOwnerById($uid);
    }
    else {
      $entity = OpenStackProject::create([
        'cloud_context' => $cloud_context,
        'name' => $project['Name'],
        'project_id' => $project['Id'],
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
    }

    $entity->setName($project['Name']);
    $entity->setDescription($project['Description']);
    $entity->setIsDomain($project['IsDomain']);
    $entity->setDomainId($project['DomainId']);
    $entity->setEnabled($project['Enabled']);
    $entity->setUserRoles($user_roles);
    $entity->save();
  }

  /**
   * Update or create a role entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $role
   *   The role array.
   */
  public static function updateRole(string $cloud_context, array $role): void {
    $timestamp = time();

    $uid = 0;
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_role', 'role_id', $role['Id']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackRole $entity */
      $entity = OpenStackRole::load($entity_id);
      $entity->setOwnerById($uid);
    }
    else {
      $entity = OpenStackRole::create([
        'cloud_context' => $cloud_context,
        'name' => $role['Name'],
        'role_id' => $role['Id'],
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
    }

    $entity->setName($role['Name']);
    $entity->setDescription($role['Description']);
    $entity->save();
  }

  /**
   * Update or create a user entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $user
   *   The user array.
   */
  public static function updateUser(string $cloud_context, array $user): void {
    $timestamp = time();

    $uid = 0;
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_user', 'user_id', $user['Id']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      /** @var \Drupal\openstack\Entity\OpenStackUser $entity */
      $entity = OpenStackUser::load($entity_id);
      $entity->setOwnerById($uid);
    }
    else {
      $entity = OpenStackUser::create([
        'cloud_context' => $cloud_context,
        'name' => $user['Name'],
        'user_id' => $user['Id'],
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
    }

    $entity->setName($user['Name']);
    $entity->setDescription($user['Description']);
    $entity->setEmail($user['Email']);
    $entity->setEnabled($user['Enabled']);
    $entity->setDefaultProjectId($user['DefaultProjectId']);
    $entity->setDomainId($user['DomainId']);
    $entity->setPasswordExpiresAt(!empty($user['PasswordExpiresAt']) ? strtotime($user['PasswordExpiresAt']) : NULL);
    $entity->save();
  }

  /**
   * Update or create a floating IP entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $floating_ip
   *   The Floating IP array.
   */
  public static function updateFloatingIp(string $cloud_context, array $floating_ip): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);
    $timestamp = time();

    $name = $openstack_service->getTagName($floating_ip, $floating_ip['PublicIp']);
    $uid = $openstack_service->getUidTagValue($floating_ip, 'openstack_floating_ip');
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_floating_ip', 'public_ip', $floating_ip['PublicIp']);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      $entity = OpenStackFloatingIp::load($entity_id);

      // Update fields.
      try {
        $entity->setName($name);
        $entity->setInstanceId($floating_ip['InstanceId'] ?? '');
        $entity->setNetworkId($floating_ip['NetworkId'] ?? '');
        $entity->setPrivateIpAddress($floating_ip['PrivateIpAddress'] ?? '');
        $entity->setNetworkOwner($floating_ip['NetworkOwnerId'] ?? '');
        $entity->setAllocationId($floating_ip['AllocationId'] ?? '');
        $entity->setAssociationId($floating_ip['AssociationId'] ?? '');
        $entity->setDomain($floating_ip['Domain'] ?? '');
        $entity->setFloatingIpId($floating_ip['FloatingIpId'] ?? '');

        $entity->setRefreshed($timestamp);
        $entity->setOwnerById($uid > 0 ? $uid : 0);
        $entity->save();
      }
      catch (\Exception $e) {
        \Drupal::service('cloud')->handleException($e);
      }
    }
    else {
      $entity = OpenStackFloatingIp::create([
        'cloud_context' => $cloud_context,
        'name' => $name,
        'floating_ip_id' => $floating_ip['FloatingIpId'] ?? NULL,
        'public_ip' => $floating_ip['PublicIp'],
        'instance_id' => $floating_ip['InstanceId'] ?? '',
        'network_id' => $floating_ip['NetworkId'] ?? '',
        'private_ip_address' => $floating_ip['PrivateIpAddress'] ?? '',
        'network_owner' => $floating_ip['NetworkOwnerId'] ?? '',
        'allocation_id' => $floating_ip['AllocationId'] ?? '',
        'association_id' => $floating_ip['AssociationId'] ?? '',
        'domain' => $floating_ip['Domain'] ?? '',
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
      $entity->save();
    }
  }

  /**
   * Update or create a server group entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $server_group
   *   The server group array.
   */
  public static function updateServerGroup(string $cloud_context, array $server_group): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);
    $timestamp = time();

    $server_group_name = $server_group['Name'] ?? $server_group['ServerGroupId'];
    $name = $openstack_service->getTagName($server_group, $server_group_name);
    $uid = $openstack_service->getUidTagValue($server_group, 'openstack_server_group');
    $entity_id = \Drupal::service('cloud')->getEntityId(
      $cloud_context,
      'openstack_server_group',
      'server_group_id',
      $server_group['ServerGroupId']
    );

    // Members.
    $members = array_map(static function ($item) use ($cloud_context) {
      $instance_entity_id = \Drupal::service('cloud')->getEntityId(
        $cloud_context,
        'openstack_instance',
        'instance_id',
        $item
      );
      $entity = !empty($instance_entity_id) ? OpenStackInstance::load($instance_entity_id) : '';
      return [
        'item_key' => $item,
        'item_value' => !empty($entity) ? $entity->getName() : '',
      ];
    }, $server_group['Members'] ?: []);

    // Skip if $entity already exists, by updating 'refreshed' time.
    if (!empty($entity_id)) {
      $entity = OpenStackServerGroup::load($entity_id);

      // Update fields.
      try {
        $entity->setName($name);
        $entity->setServerGroupId($server_group['ServerGroupId'] ?? '');
        $entity->setPolicy($server_group['Policy'] ?? '');
        $entity->setMembers($members);
        $entity->setRefreshed($timestamp);
        $entity->setOwnerById($uid > 0 ? $uid : 0);
        $entity->save();
      }
      catch (\Exception $e) {
        \Drupal::service('cloud')->handleException($e);
      }
    }
    else {
      $entity = OpenStackServerGroup::create([
        'cloud_context' => $cloud_context,
        'name' => $name,
        'server_group_id' => $server_group['ServerGroupId'],
        'project_id' => $server_group['ProjectId'],
        'policy' => $server_group['Policy'] ?? '',
        'members' => implode(', ', $server_group['Members']) ?? '',
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
      $entity->save();
    }
  }

  /**
   * Update or create an Availability Zone entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $availability_zone
   *   The Availability Zone array.
   */
  public static function updateAvailabilityZone(string $cloud_context, array $availability_zone): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);
    $openstack_service->setCloudContext($cloud_context);

    $zone_name = $availability_zone['ZoneName'] ?? NULL;
    $component_name = $availability_zone['ComponentName'] ?? NULL;
    $zone_resource = $availability_zone['ZoneResource'] ?? NULL;

    $extra_conditions = [];
    if (!empty($component_name)) {
      $extra_conditions['component_name'] = $component_name;
    }
    if (!empty($zone_resource)) {
      $extra_conditions['zone_resource'] = $zone_resource;
    }

    $timestamp = time();
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_availability_zone', 'zone_name', $zone_name, $extra_conditions);
    $uid = $openstack_service->getUidTagValue($availability_zone, 'openstack_availability_zone');

    if (!empty($entity_id)) {
      $entity = OpenStackAvailabilityZone::load($entity_id);
      if (!empty($entity)) {
        $entity->setRefreshed($timestamp);
        $entity->setOwnerById($uid > 0 ? $uid : 0);
      }
    }
    else {
      $entity = OpenStackAvailabilityZone::create([
        'cloud_context' => $cloud_context,
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
    }

    $entity->setRegionName($availability_zone['RegionName'] ?? NULL);
    $entity->setZoneName($availability_zone['ZoneName'] ?? NULL);
    $entity->setZoneState($availability_zone['State'] ?? NULL);
    $entity->setComponentName($availability_zone['ComponentName'] ?? NULL);
    $entity->setZoneResource($availability_zone['zoneResource'] ?? NULL);

    $entity->save();
  }

  /**
   * Update or create a flavor entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $flavor
   *   The flavor array.
   */
  public static function updateFlavor(string $cloud_context, array $flavor): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = \Drupal::service('openstack.factory')->get($cloud_context);
    $openstack_service->setCloudContext($cloud_context);

    $flavor_id = $flavor['id'] ?? NULL;

    $timestamp = time();
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'openstack_flavor', 'flavor_id', $flavor_id);
    $uid = $openstack_service->getUidTagValue($flavor, 'openstack_flavor');

    if (!empty($entity_id)) {
      $entity = OpenStackFlavor::load($entity_id);
      if (!empty($entity)) {
        $entity->setRefreshed($timestamp);
        $entity->setOwnerById($uid > 0 ? $uid : 0);
      }
    }
    else {
      $entity = OpenStackFlavor::create([
        'cloud_context' => $cloud_context,
        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => $uid,
      ]);
    }

    $entity->setName($flavor['name'] ?? NULL);
    $entity->setFlavorId($flavor['id'] ?? NULL);
    $entity->setVcpus($flavor['vcpus'] ?? NULL);
    $entity->setRam($flavor['ram'] ?? NULL);
    $entity->setDisk($flavor['disk'] ?? NULL);
    $entity->setEphemeral($flavor['OS-FLV-EXT-DATA:ephemeral'] ?? NULL);
    $entity->setSwap(empty($flavor['swap']) ? 0 : $flavor['swap']);
    $entity->setRxtxFactor($flavor['rxtx_factor'] ?? NULL);
    $entity->setIsPublic($flavor['os-flavor-access:is_public'] ?? NULL);

    $entity->save();
  }

}
