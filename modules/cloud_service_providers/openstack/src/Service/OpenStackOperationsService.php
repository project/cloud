<?php

namespace Drupal\openstack\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityDeleteFormTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface;
use Drupal\aws_cloud\Entity\Ec2\InstanceInterface;
use Drupal\aws_cloud\Entity\Ec2\KeyPairInterface;
use Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface;
use Drupal\aws_cloud\Entity\Ec2\SnapshotInterface;
use Drupal\aws_cloud\Entity\Ec2\VolumeInterface;
use Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\aws_cloud\Traits\AwsCloudEntityCheckTrait;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\openstack\Entity\OpenStackFloatingIpInterface;
use Drupal\openstack\Entity\OpenStackImageInterface;
use Drupal\openstack\Entity\OpenStackInstance;
use Drupal\openstack\Entity\OpenStackNetwork;
use Drupal\openstack\Entity\OpenStackNetworkInterface;
use Drupal\openstack\Entity\OpenStackPortInterface;
use Drupal\openstack\Entity\OpenStackProjectInterface;
use Drupal\openstack\Entity\OpenStackQuota;
use Drupal\openstack\Entity\OpenStackQuotaInterface;
use Drupal\openstack\Entity\OpenStackRole;
use Drupal\openstack\Entity\OpenStackRoleInterface;
use Drupal\openstack\Entity\OpenStackRouterInterface;
use Drupal\openstack\Entity\OpenStackServerGroupInterface;
use Drupal\openstack\Entity\OpenStackStackInterface;
use Drupal\openstack\Entity\OpenStackSubnetInterface;
use Drupal\openstack\Entity\OpenStackUser;
use Drupal\openstack\Entity\OpenStackUserInterface;
use Drupal\openstack\Service\Ec2\OpenStackService as OpenStackEc2Service;
use Drupal\openstack\Service\Rest\OpenStackService as OpenStackRestService;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Interacts with OpenStack using OpenStackService API.
 */
class OpenStackOperationsService implements OpenStackOperationsServiceInterface {

  use AwsCloudEntityCheckTrait;
  use CloudContentEntityTrait;
  use EntityDeleteFormTrait;
  use StringTranslationTrait;

  public const SECURITY_GROUP_DELIMITER = ', ';

  /**
   * The AWS Cloud Operations service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface
   */
  private $awsCloudOperationsService;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  private $cloudService;

  /**
   * The AWS Cloud EC2 service.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  private $ec2Service;

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface|\Drupal\Core\Entity\RevisionLogInterface
   */
  private $entity;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  private $openStackServiceFactory;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  private $entityLinkRenderer;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * OpenStackOperationsService constructor.
   *
   * @param \Drupal\aws_cloud\Service\AwsCloud\AwsCloudOperationsServiceInterface $aws_cloud_operations_service
   *   The AWS Cloud Operations service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The AWS Cloud EC2 service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory
   *   Object for interfacing with OpenStack Service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match object.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   */
  public function __construct(
    AwsCloudOperationsServiceInterface $aws_cloud_operations_service,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    CloudServiceInterface $cloud_service,
    Ec2ServiceInterface $ec2_service,
    EntityTypeManagerInterface $entity_type_manager,
    Messenger $messenger,
    OpenStackServiceFactoryInterface $openstack_service_factory,
    RequestStack $request_stack,
    RouteMatchInterface $route_match,
    EntityLinkRendererInterface $entity_link_renderer,
    TimeInterface $time,
    ModuleHandlerInterface $module_handler,
    QueueFactory $queue_factory,
    AccountInterface $current_user,
    ConfigFactoryInterface $config_factory,
  ) {
    $this->awsCloudOperationsService = $aws_cloud_operations_service;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->cloudService = $cloud_service;
    $this->ec2Service = $ec2_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->openStackServiceFactory = $openstack_service_factory;
    $this->requestStack = $request_stack;
    $this->routeMatch = $route_match;
    $this->entityLinkRenderer = $entity_link_renderer;
    $this->time = $time;
    $this->moduleHandler = $module_handler;
    $this->queueFactory = $queue_factory;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
  }

  /**
   * Gets the entity of this form.
   *
   * Provided by \Drupal\Core\Entity\EntityForm.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackImageFromInstance(InstanceInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

      if (!($result = $this->ec2Service->createImageFromInstance([
        'Name' => $form_state->getValue('image_name'),
        'InstanceId' => $entity->getInstanceId(),
      ]))) {
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        return TRUE;
      }

      $this->messenger->addStatus($this->t('Image @image_name of instance @instance_name created.', [
        '@image_name' => $form_state->getValue('image_name'),
        '@instance_name' => $entity->getName(),
      ]));

      // Update the images.
      $this->ec2Service->updateImages();
      $form_state->setRedirect("view.openstack_image.list", ['cloud_context' => $entity->getCloudContext()]);

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackInstance(InstanceInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      // Switch OpenStack service based on $entity->getCloudContext().
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

      if ($this->ec2Service instanceof OpenStackEc2Service) {
        return $this->awsCloudOperationsService->deleteInstance($this->entity, $form, $form_state);
      }

      if (!$this->updateEntitySubmitForm($form_state, $entity)) {
        return FALSE;
      }

      if ($entity->getInstanceState() !== 'error' && $entity->getTerminationProtection()) {
        $this->messenger->addWarning($this->t('The instance cannot be terminated because it is protected.'));
        $this->processOperationErrorStatus($entity, 'deleted');
        return FALSE;
      }

      $this->ec2Service->setCloudContext($entity->getCloudContext());

      $result = $this->ec2Service->terminateInstances([
        'InstanceId' => $entity->getInstanceId(),
      ]);

      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

      if (empty($result)) {
        $this->processOperationErrorStatus($entity, 'deleted');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->messenger->addStatus($this->t('The @type @label has been deleted remotely.', [
          '@type'  => $entity->getEntityType()->getSingularLabel(),
          '@label' => $entity->getName(),
        ]));
        return TRUE;
      }

      $entity->delete();
      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
      $this->clearCacheValues($entity->getCacheTags());

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteProcess(CloudContentEntityBase $entity, callable $process_cloud_resource, callable $process_entity): bool {
    try {
      $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

      if ($cloud_config->isRemote()) {
        $process_cloud_resource($entity);
        $this->processOperationStatus($entity, 'deleted remotely');
        return TRUE;
      }

      if (!$process_cloud_resource($entity)) {
        $this->processOperationErrorStatus($entity, 'deleted');

        return FALSE;
      }

      $process_entity($entity);

      // Since we do not use EntityDeleteFormTrait since it is an abstract
      // class and requires to implement a getEntity() method, so we use our
      // own method here instead.
      $this->processOperationStatus($entity, 'deleted');

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    // Exception return type handling.
    // Return unable to process.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackInstanceCloudResource(InstanceInterface $entity): bool {
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    if ($this->ec2Service instanceof OpenStackEc2Service) {
      return $this->awsCloudOperationsService->deleteInstancesImpl($entity);
    }

    if ($entity->getInstanceState() !== 'error' && $entity->getTerminationProtection()) {
      $this->messenger->addWarning($this->t('The instance cannot be terminated because it is protected.'));
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    return !empty($this->ec2Service->terminateInstances([
      'InstanceId' => $entity->getInstanceId(),
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function startOpenStackInstance(InstanceInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;

      // Switch OpenStack EC2 or Rest service based on
      // $entity->getCloudContext().
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

      $params = [
        'InstanceId' => $entity->getInstanceId(),
      ];

      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

      $result = $this->ec2Service->startInstances($params);
      if (empty($result)) {
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'started remotely');
        return TRUE;
      }

      $current_state = "running";
      /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
      $instance = $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->load($entity->id());
      $instance->setInstanceState($current_state);
      $instance->save();

      $this->processOperationStatus($instance, 'started');
      $this->clearCacheValues($entity->getCacheTags());
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function startProcess(CloudContentEntityBase $entity, callable $process_cloud_resource, callable $process_entity): bool {
    try {
      $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
      if ($cloud_config->isRemote()) {
        $process_cloud_resource($entity);
        $this->processOperationStatus($entity, 'started remotely');
        return TRUE;
      }

      if (!empty($entity) && $process_cloud_resource($entity)) {

        $process_entity($entity);
        $this->processOperationStatus($entity, 'started');

        return TRUE;
      }

      $this->processOperationErrorStatus($entity, 'started');

      return FALSE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    // Exception return type handling.
    // Return unable to process.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function startOpenStackInstanceCloudResource(InstanceInterface $entity): bool {
    // Switch OpenStack EC2 or Rest service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    // Custom buildForm logic for OpenStack REST API.
    if ($this->ec2Service instanceof OpenStackEc2Service) {
      return $this->awsCloudOperationsService->startInstanceCloudResource($entity);
    }

    $result = $this->ec2Service->startInstances([
      'InstanceId' => $entity->getInstanceId(),
    ]);

    // Refresh instance as need to update the instance state to running in the
    // entity.
    $this->ec2Service->updateInstances();
    return $result !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function stopOpenStackInstance(InstanceInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;

      // Switch OpenStack EC2 or Rest service based on
      // $entity->getCloudContext().
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

      if (!$this->updateEntitySubmitForm($form_state, $entity)) {
        return FALSE;
      }

      $params = [
        'InstanceId' => $entity->getInstanceId(),
      ];

      $result = $this->ec2Service->stopInstances($params);

      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

      if (empty($result)) {
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'stopped remotely');
        return TRUE;
      }

      $current_state = "stopped";

      /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
      $instance = $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->load($entity->id());
      $instance->setInstanceState($current_state);
      $instance->save();

      $this->processOperationStatus($instance, 'stopped');

      $this->clearCacheValues($entity->getCacheTags());
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function stopProcess(CloudContentEntityBase $entity, callable $process_cloud_resource, callable $process_entity): bool {
    try {
      $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
      if ($cloud_config->isRemote()) {
        $process_cloud_resource($entity);
        $this->processOperationStatus($entity, 'stopped remotely');
        return TRUE;
      }

      if (!empty($entity) && $process_cloud_resource($entity)) {

        $process_entity($entity);
        $this->processOperationStatus($entity, 'stopped');

        return TRUE;
      }

      $this->processOperationErrorStatus($entity, 'stopped');

      return FALSE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    // Exception return type handling.
    // Return unable to process.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stopOpenStackInstanceCloudResource(InstanceInterface $entity): bool {
    // Switch OpenStack EC2 or Rest service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $result = $this->ec2Service instanceof OpenStackEc2Service
      ? $this->awsCloudOperationsService->stopInstanceCloudResource($entity)
      : $this->ec2Service->stopInstances([
        'InstanceId' => $entity->getInstanceId(),
      ]);

    // Refresh instance as need to update the instance state to stopped in the
    // entity.
    $this->ec2Service->updateInstances();

    return $result !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function rebootOpenStackInstance(InstanceInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $this->entity = $entity;

    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    if ($this->ec2Service instanceof OpenStackEc2Service) {
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
      return $this->awsCloudOperationsService->rebootInstance($this->entity, $form, $form_state);
    }

    if (!$this->updateEntitySubmitForm($form_state, $entity)) {
      return FALSE;
    }

    $params = [
      'InstanceId' => $entity->getInstanceId(),
      'type' => (bool) $form_state->getValue('type') === TRUE ? 'SOFT' : 'HARD',
    ];

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->rebootInstances($params);
    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'rebooted remotely');
      return TRUE;
    }

    $this->processOperationStatus($entity, 'rebooted');
    $this->clearCacheValues($entity->getCacheTags());
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function rebootProcess(CloudContentEntityBase $entity, callable $process_cloud_resource, callable $process_entity): bool {
    try {
      $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
      if ($cloud_config->isRemote()) {
        $process_cloud_resource($entity);
        $this->processOperationStatus($entity, 'rebooted remotely');
        return TRUE;
      }

      try {
        if (!empty($entity) && $process_cloud_resource($entity)) {

          $process_entity($entity);
          $this->processOperationStatus($entity, 'rebooted');

          return TRUE;
        }

        $this->processOperationErrorStatus($entity, 'rebooted');

        return FALSE;
      }
      catch (\Exception $e) {
        $this->handleException($e);
      }
      // Exception return type handling.
      // Return unable to process.
      return FALSE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    // Exception return type handling.
    // Return unable to process.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function rebootOpenStackInstanceCloudResource(InstanceInterface $entity, bool $reboot_type): bool {
    // Switch OpenStack EC2 or Rest service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    if ($this->ec2Service instanceof OpenStackEc2Service) {
      return $this->awsCloudOperationsService->rebootInstanceCloudResource($entity);
    }

    $result = $this->ec2Service->rebootInstances([
      'InstanceId' => $entity->getInstanceId(),
      'type' => $reboot_type,
    ]);

    return $result !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackImage(OpenStackImageInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;

      // Switch OpenStack service based on $entity->getCloudContext().
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

      if ($this->ec2Service instanceof OpenStackEc2Service) {
        $result = $this->awsCloudOperationsService->createImage($this->entity, $form, $form_state);
        return !empty($result) || !empty($result['SendToWorker']);
      }

      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

      $params = [
        'ImageId' => $entity->getImageId(),
        'Name' => $entity->getName(),
        'ImageVisibility' => $entity->getImageVisibility(),
      ];

      $result = $this->ec2Service->createImage($params);

      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'created');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $shared_projects = !empty($result['ImageId']) && $entity->getImageVisibility() == OpenStackImageInterface::VISIBILITY_SHARED
        ? $this->updateOpenStackSharedProjects($result['ImageId'], $entity->getSharedProjects())
        : [];

      $entity->setImageId($result['ImageId']);
      $entity->setSharedProjects($shared_projects);
      $entity->save();

      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getImageId());

      // Update the Image entity.
      $this->ec2Service->updateImages([
        'ImageId' => $entity->getImageId(),
      ]);

      $this->processOperationStatus($entity, 'created');

      $this->clearCacheValues($entity->getCacheTags());
      $this->dispatchSaveEvent($entity);

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * Check if the cloud context is inter-region.
   *
   * @param string $image_api_endpoint
   *   The image api endpoint.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return bool
   *   TRUE if cloud context is inter-region.
   */
  private function isInterRegionImageApiEndpoint(string $image_api_endpoint, string $cloud_context): bool {
    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    return !empty($cloud_configs)
      && !empty(array_filter($cloud_configs, static function ($cloud_config) use ($image_api_endpoint) {
        return $cloud_config->get('field_image_api_endpoint')->value !== $image_api_endpoint;
      }));
  }

  /**
   * Update Shared projects.
   *
   * @param string $image_id
   *   The image id.
   * @param array $projects
   *   The projects.
   *
   * @return array
   *   The shared projects.
   */
  private function updateOpenStackSharedProjects(string $image_id, array $projects): array {
    /** @var \Drupal\openstack\Entity\OpenStackImageInterface */
    $entity = $this->entity;
    /** @var \Drupal\openstack\Service\Rest\OpenStackService */
    $ec2_service = $this->ec2Service;
    $inter_region_members = [];
    $tags = [];
    foreach ($projects ?: [] as $project) {
      if (empty($project['project_id'])) {
        continue;
      }

      if (!empty($project['cloud_context'])
        && !empty($this->isInterRegionImageApiEndpoint($project['cloud_context'], $entity->getCloudContext()))) {
        // Share image to inter-region.
        /** @var \Drupal\cloud\Entity\CloudConfig[] */
        $to_cloud_configs = $this->entityTypeManager
          ->getStorage('cloud_config')
          ->loadByProperties([
            'type' => 'openstack',
            'field_image_api_endpoint' => $project['cloud_context'],
            'field_project_id' => $project['project_id'],
          ]);
        $to_cloud_config = array_shift($to_cloud_configs);
        // Create image.
        $openstack_service = $this->openStackServiceFactory->get($to_cloud_config->getCloudContext());
        $image = $openstack_service->createImage([
          'Name' => $entity->getName(),
          'ImageVisibility' => OpenStackImageInterface::VISIBILITY_PRIVATE,
        ]);
        $tags[] = [
          'CloudContext' => $to_cloud_config->getCloudContext(),
          'ProjectId' => $project['project_id'],
          'ImageId' => $image['ImageId'],
        ];

        // Add image information to the queue to share files.
        $resources_queue = $this->queueFactory->get("openstack_share_images_queue:{$entity->getCloudContext()}");
        $resources_queue->createItem([
          'cloud_context' => $entity->getCloudContext(),
          'image_id' => $entity->getImageId(),
          'to_cloud_context' => $to_cloud_config->getCloudContext(),
          'to_image_id' => $image['ImageId'],
        ]);
        $inter_region_members[] = [
          'cloud_context' => $to_cloud_config->getCloudContext(),
          'project_id' => $project['project_id'],
          'status' => $this->t('Being set up'),
        ];

        $this->messenger->addMessage($this->t('The image is being set up so that it can be shared with @cloud_context.', [
          '@cloud_context' => $to_cloud_config->getCloudContext(),
        ]));
        continue;
      }

      // Check if the project id exists.
      $check_project = $ec2_service->describeProjects(
        [
          'ProjectId' => $project['project_id'],
        ]
      );
      if (empty($check_project) || empty($check_project['Projects'])) {
        $this->messenger->addError($this->t('The Project does not exist. @label.', [
          '@label' => $project['project_id'],
        ]));
        continue;
      }

      $params = [
        'ImageId' => $image_id,
        'ProjectId' => $project['project_id'],
      ];
      $result = $ec2_service->updateImageSharedProjects($params);
    }

    // Create tag.
    /** @var \Drupal\openstack\Service\Rest\OpenStackService */
    $openstack_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $openstack_service->addImageTags([
      'ImageId' => $image_id,
      'Tags' => $tags,
    ]);

    $result = $ec2_service->describeProjectSharedImage([
      'ImageId' => $image_id,
      'CloudContext' => $entity->getCloudContext(),
    ]);

    // Get members in the same region using the OpenStack API.
    $same_region_members = !empty($result['Members'])
      ? array_map(static function ($item) {
        if (!empty($item['ProjectId'])
          && !empty($item['Status'])) {
          return [
            'cloud_context' => $item['CloudContext'],
            'project_id' => $item['ProjectId'],
            'status' => $item['Status'],
          ];
        }
      }, $result['Members'] ?: [])
      : [];

    // Get members in the inter region using the OpenStack image tags.
    return array_merge($same_region_members, $inter_region_members);
  }

  /**
   * Save an image.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function saveImage(array $form, FormStateInterface $form_state): void {
    /** @var \Drupal\openstack\Entity\OpenStackImageInterface */
    $entity = $this->entity;

    /** @var \Drupal\openstack\Entity\OpenStackImageInterface */
    $original_entity = $this->entityTypeManager
      ->getStorage($entity->getEntityTypeId())
      ->load($entity->id());

    $this->awsCloudOperationsService->saveAwsCloudContent($entity, $form, $form_state);

    $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getImageId());

    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

    // Call 'modifyImageAttribute'
    // only when OwnerId is same as Account ID of Cloud Config.
    // It's to avoid AuthFailure error from AWS API.
    if ($cloud_config->get('field_account_id')->value !== $entity->getAccountId()) {
      return;
    }

    $form_values = $form_state->getValues();
    $visibility = $form_values['visibility'] ?? 0;

    $permission = [
      [
        'Group' => 'all',
        'UserId' => $entity->getAccountId(),
      ],
    ];

    // 1 => Public, 0 => Private.
    $launch_permission =
      $visibility
        ? ['Add' => $permission]
        : ['Remove' => $permission];

    // Update image.
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    if (!method_exists($this->ec2Service, 'modifyImageAttribute')) {
      return;
    }

    // Update image description.
    $this->ec2Service->modifyImageAttribute([
      'ImageId' => $entity->getImageId(),
      'Description' => ['Value' => $entity->getDescription()],
    ]);

    // These fields (Description , LaunchPermission , or ProductCode) cannot
    // be specified at the same time.
    // See also:
    // https://docs.aws.amazon.com/cli/latest/reference/ec2/modify-image-attribute.html
    // Update image launch permission.
    $this->ec2Service->modifyImageAttribute([
      'ImageId' => $entity->getImageId(),
      'LaunchPermission' => $launch_permission,
    ]);

    $new_account_ids = [];
    foreach ($entity->getLaunchPermissionAccountIds() ?: [] as $account_id) {
      if (empty($account_id->value)) {
        continue;
      }
      $new_account_ids[] = $account_id->value;
    }

    $old_account_ids = [];
    foreach ($original_entity->getLaunchPermissionAccountIds() ?: [] as $account_id) {
      if (empty($account_id->value)) {
        continue;
      }
      $old_account_ids[] = $account_id->value;
    }

    $account_ids_to_remove = array_diff($old_account_ids, $new_account_ids);
    $account_ids_to_add = array_diff($new_account_ids, $old_account_ids);

    if (!empty($account_ids_to_remove)) {
      $this->ec2Service->modifyImageAttribute([
        'ImageId' => $entity->getImageId(),
        'LaunchPermission' => [
          'Remove' => array_map(static function ($item) {
            return ['UserId' => $item];
          }, $account_ids_to_remove),
        ],
      ]);
    }

    if (!empty($account_ids_to_add)) {
      $this->ec2Service->modifyImageAttribute([
        'ImageId' => $entity->getImageId(),
        'LaunchPermission' => [
          'Add' => array_map(static function ($item) {
            return ['UserId' => $item];
          }, $account_ids_to_add),
        ],
      ]);
    }

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackImage(OpenStackImageInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
      $shared_projects = $entity->getSharedProjects();
      $this->saveImage($form, $form_state);
      $entity->setSharedProjects($shared_projects);
      // Update image of OpenStack REST API.
      if ($this->ec2Service instanceof OpenStackRestService) {
        // Make an API call to update image.
        $result = $this->ec2Service->updateImage([
          'ImageId' => $entity->getImageId(),
          'Name' => $entity->getName(),
          'ImageVisibility' => $entity->getImageVisibility(),
          'SharedProjects' => $entity->getSharedProjects(),
        ]);

        $old_tags = $result['Tags'] ?? [];

        foreach ($old_tags ?: [] as $tag) {
          // Delete old tags.
          $this->ec2Service->deleteTags(
            [
              'ImageId'    => $entity->getImageId(),
              'Tag'        => $tag,
              'EntityType' => $entity->getEntityTypeId(),
            ]);
        }

        $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getImageId());

        if ($entity->getImageVisibility() != OpenStackImageInterface::VISIBILITY_SHARED) {
          return TRUE;
        }

        $result = $this->ec2Service->describeProjectSharedImage([
          'ImageId' => $entity->getImageId(),
          'CloudContext' => $entity->getCloudContext(),
        ]);

        foreach (!empty($result['Members']) ? $result['Members'] : [] as $member) {
          // Delete old members.
          $this->ec2Service->deleteProjectSharedMembers(
            [
              'ImageId'   => $entity->getImageId(),
              'ProjectId' => $member['ProjectId'],
            ]
          );
        }

        $shared_projects = $entity->getImageVisibility() == OpenStackImageInterface::VISIBILITY_SHARED
          ? $this->updateOpenStackSharedProjects($entity->getImageId(), $entity->getSharedProjects())
          : [];

        $entity->setSharedProjects($shared_projects);
        $entity->save();
      }

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateOpenStackImageForm(FormStateInterface $form_state): void {
    $image_visibility = $form_state->getValue('image_visibility');

    // If a button other than the submit button is clicked,
    // no validation is performed thereafter.
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element['#name'])
      && $triggering_element['#name'] !== 'op') {
      return;
    }

    if ($image_visibility !== OpenStackImageInterface::VISIBILITY_SHARED) {
      return;
    }

    $shared_projects = $form_state->getValue('shared_projects');
    if (empty($shared_projects) || count(array_filter($shared_projects, static function ($project) {
      return is_array($project) && !empty($project['project_id']);
    })) === 0) {
      $form_state->setErrorByName('shared_projects', $this->t("At least one valid project ID is required when you select the 'Shared' option as 'Visibility'."));
      return;
    }

    // Validate each project in $shared_projects array.
    $invalid_project_id_found = in_array(TRUE, array_map(static function ($project) {
      // Ensure each project_id is a non-empty alphanumeric string.
      return is_array($project) && !empty($project['project_id']) && !preg_match('/^[a-zA-Z0-9]+$/', $project['project_id']);
    }, $shared_projects));

    // If an invalid project_id is found, set an error message and return.
    if (!empty($invalid_project_id_found)) {
      $form_state->setErrorByName('shared_projects', $this->t('The project ID is invalid.'));
      return;
    }

    // Return if there are no inter region project in projects.
    /** @var \Drupal\Core\Entity\EntityFormInterface $form_object */
    $form_object = $form_state->getFormObject();
    /** @var \Drupal\openstack\Entity\OpenStackImageInterface */
    $entity = $form_object->getEntity();
    $cloud_context = $entity->getCloudContext();
    if (empty($shared_projects) || count(array_filter($shared_projects, function ($project) use ($cloud_context) {
      return is_array($project) && !empty($project['project_id']) && !empty($this->isInterRegionImageApiEndpoint($project['cloud_context'], $cloud_context));
    })) === 0) {
      return;
    }

    $config = $this->configFactory->get('openstack.settings');
    $aws_cloud_context = $config->get('openstack_aws_cloud', '');
    $openstack_settings = Link::fromTextAndUrl($this->t('OpenStack settings'), Url::fromRoute('openstack.settings.options'))->toString();
    if (empty($aws_cloud_context)) {
      $form_state->setErrorByName('shared_projects', $this->t('The configuration <em>openstack_aws_cloud</em> is empty. Select an AWS Cloud provider in %openstack_settings page.', [
        '%openstack_settings' => $openstack_settings,
      ]));
      return;
    }

    $s3_bucket = $config->get('openstack_s3_bucket', '');
    if (empty($s3_bucket)) {
      $form_state->setErrorByName('shared_projects', $this->t('The configuration <em>s3_bucket is empty</em>. Input an S3 Bucket in %openstack_settings.', [
        '%openstack_settings' => $openstack_settings,
      ]));
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackImage(OpenStackImageInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
      return $this->awsCloudOperationsService->deleteImage($this->entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackImageCloudResource(OpenStackImageInterface $entity): bool {
    // Switch OpenStack EC2 or Rest service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->awsCloudOperationsService->deleteImageCloudResource($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackKeyPair(KeyPairInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
      return $this->awsCloudOperationsService->createKeyPair($this->entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackKeyPair(KeyPairInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
      return $this->awsCloudOperationsService->editKeyPair($this->entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackKeyPair(KeyPairInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
      return $this->awsCloudOperationsService->deleteKeyPair($this->entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackKeyPairCloudResource(KeyPairInterface $entity): bool {
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deleteKeyPair(
        ['KeyName' => $entity->getKeyPairName()]
      ) !== NULL;
  }

  /**
   * Helper function to update field tags.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  private function updateTagsField(FormStateInterface $form_state): void {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;

    $form_tags = $form_state->getValue('tags');
    usort($form_tags, static function ($a, $b) {
      return (int) $a['_weight'] - (int) $b['_weight'];
    });
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface|\Drupal\cloud\Service\CloudResourceTagInterface $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $uid_key_name = $ec2_service->getTagKeyCreatedByUid(
      'openstack',
      $entity->getCloudContext()
    );

    $fixed_tags = [];
    $fixed_tags[$uid_key_name] = $entity->getOwner() === NULL
    ? 0
    : $entity->getOwner()->id();

    $tags = [];
    foreach ($form_tags ?: [] as $form_tag) {
      $item_key = $form_tag['item_key'];
      if ($item_key === '') {
        continue;
      }

      // Skip special tags.
      if (strpos($item_key, 'openstack:') === 0) {
        continue;
      }

      $item_value = $form_tag['item_value'];

      if ($item_key === 'termination_protection') {
        $item_value = empty($form_state->getValue('termination_protection')) ? 0 : 1;
      }

      if ($form_tag['item_key'] !== $uid_key_name) {
        $tags[] = ['item_key' => $item_key, 'item_value' => $item_value];
      }
    }

    if (!empty($fixed_tags)) {
      foreach ($fixed_tags ?: [] as $item_key => $item_value) {
        if ($item_value) {
          $tags[] = ['item_key' => $item_key, 'item_value' => $item_value];
        }
      }
    }

    $entity->setTags($tags);
    $entity->save();
  }

  /**
   * Helper function to update OpenStack tags.
   */
  private function updateOpenStackTags(): void {
    /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface */
    $entity = $this->entity;
    /** @var \Drupal\openstack\Service\Rest\OpenStackServiceInterface $ec2_service */
    $ec2_service = $this->ec2Service;

    $params = [
      'Resources' => [$entity->getInstanceId()],
    ];

    // Delete old tags.
    $this->ec2Service->deleteTags(
      [
        'InstanceId' => $entity->getInstanceId(),
        'EntityType' => $entity->getEntityTypeId(),
        'UidKeyName' => $ec2_service->getTagKeyCreatedByUid(
          'openstack',
          $entity->getCloudContext()
        ),
      ]);

    foreach ($entity->getTags() ?: [] as $tag) {
      $params['Tags'][] = [
        'Key' => $tag['item_key'],
        'Value' => $tag['item_value'],
      ];
    }

    // Create Tags with different parameters for OpenStack.
    $this->ec2Service->createTags([
      'Resources' => [$entity->getInstanceId()],
      'Tags' => $params['Tags'],
      'EntityType' => $entity->getEntityTypeId(),
    ]);

    $this->ec2Service->updateInstances();

  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $entity = (static fn($entity): OpenStackInstance => $entity)($entity);
      $this->entity = $entity;
      $this->awsCloudOperationsService->copyFormItemValues($entity, $form);
      // Switch OpenStack EC2 or Rest service
      // based on $entity->getCloudContext().
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

      if (!($this->ec2Service instanceof OpenStackRestService)) {
        return $this->awsCloudOperationsService->editInstance($entity, $form, $form_state);
      }

      $termination_timestamp = $form_state->getValue('termination_timestamp')[0]['value'] ?? '';

      $this->updateTagsField($form_state);

      $this->updateOpenStackTags();

      if ($termination_timestamp === NULL) {
        // Unset the termination timestamp.
        $entity->set('termination_timestamp', NULL);
      }

      $port_ids = array_filter(array_map(static function ($port_id_with_value) {
        if (!empty($port_id_with_value['value'])) {
          return $port_id_with_value['value'];
        }
      }, $entity->getPortIds() ?: []));

      $entity->set('security_groups', implode(', ', array_keys($form_state->getValue('security_groups'))));

      $security_groups = $this->entityTypeManager
        ->getStorage('openstack_security_group')
        ->loadByProperties([
          'cloud_context' => $entity->getCloudContext(),
        ]);
      $security_group_name_id_map = [];
      foreach ($security_groups ?: [] as $security_group) {
        $group_name = $security_group->getGroupName();
        if (empty($group_name)) {
          continue;
        }
        $security_group_name_id_map[$group_name] = $security_group->getGroupId();
      }

      $new_security_groups = array_filter(array_map(static function ($value) use ($security_group_name_id_map) {
        return $security_group_name_id_map[$value] ?? NULL;
      }, array_keys($form_state->getValue('security_groups'))));

      $old_security_groups = array_filter(array_map(static function ($value) use ($security_group_name_id_map) {
        return $security_group_name_id_map[$value] ?? NULL;
      }, $form['network']['security_groups']['#default_value']));

      // Update instance of OpenStack REST API.
      $result = $this->ec2Service->modifyInstance([
        'InstanceId' => $entity->getInstanceId(),
        'Name' => $entity->getName(),
        'NewSecurityGroups' => $new_security_groups,
        'OldSecurityGroups' => $old_security_groups,
        'PortIds' => $port_ids,
      ]);

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'updated remotely');
        return TRUE;
      }

      $entity->save();

      $this->processOperationStatus($entity, 'updated');

      $this->clearCacheValues($entity->getCacheTags());
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function attachOpenStackNetworkInterface(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->awsCloudOperationsService->copyFormItemValues($this->entity, $form);
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

      $params = $form_state->getValue('attach_type') === 'network'
        ? [
          'InstanceId' => $entity->getInstanceId(),
          'net_id' => $form_state->getValue('network', ''),
          'fixed_ips' => $form_state->getValue('fixed_ip', ''),
        ]
        : [
          'InstanceId' => $entity->getInstanceId(),
          'port_id' => $form_state->getValue('port', ''),
        ];

      $result = $this->ec2Service->createPortInterface($params);

      if ($result === NULL) {
        $this->messenger()->addError($this->t('Unable to attach network interface.'));
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $this->messenger->addMessage($this->t('Network interface attached.'));
      $this->ec2Service->updateInstances();
      $this->ec2Service->updatePorts();
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function detachOpenStackNetworkInterface(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->awsCloudOperationsService->copyFormItemValues($this->entity, $form);
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

      $port_id = $form_state->getValue('port_id', '');

      if (empty($port_id)) {
        $this->messenger()->addError($this->t('No port id.  Unable to detach network interface.'));
        return FALSE;
      }

      $params = [
        'InstanceId' => $entity->getInstanceId(),
        'port_id' => $port_id,
      ];

      $result = $this->ec2Service->deletePortInterface($params);

      if ($result === NULL) {
        $this->messenger()->addError($this->t('Unable to detach network interface.'));
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $this->messenger->addMessage($this->t('Network interface detached.'));
      $this->ec2Service->updateInstances();
      $this->ec2Service->updatePorts();
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackNetwork(OpenStackNetworkInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $zone_names = [];
    foreach ($entity->getAvailabilityZones() ?: [] as $availability_zone) {
      $zone_names[] = array_values($availability_zone)[0];
    }

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->createNetwork([
      'Name' => $entity->getName(),
      'AdminStateUp' => $entity->getAdminStateUp(),
      'Shared' => $entity->getShared(),
      'External' => $entity->getExternal(),
      'AvailabilityZones' => $zone_names,
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $entity->setNetworkId($result['Network']['NetworkId']);
    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getNetworkId());
    }

    // Update the Network entity.
    $this->ec2Service->updateNetworks([
      'NetworkId' => $entity->getNetworkId(),
    ]);

    $this->processOperationStatus($entity, 'created');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackSubnet(OpenStackSubnetInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $params = [
      'Name' => $entity->getName(),
      'NetworkId' => $entity->getNetworkId(),
      'Cidr' => $entity->getCidr(),
      'IpVersion' => $entity->getIpVersion(),
      'EnableDhcp' => $form_state->getValue('enable_dhcp'),
    ];

    // Gateway IP.
    if (!empty($form_state->getValue('disable_gateway'))) {
      $params['GatewayIp'] = NULL;
    }
    elseif (!empty($entity->getGatewayIp())) {
      $params['GatewayIp'] = $entity->getGatewayIp();
    }

    // Allocation pools.
    $allocation_pools = [];
    $lines = explode("\n", $form_state->getValue('allocation_pools') ?: '');
    foreach ($lines as $line) {
      $addresses = explode(',', $line);
      if (count($addresses) !== 2) {
        continue;
      }
      $allocation_pools[] = [
        'start' => trim($addresses[0] ?: ''),
        'end' => trim($addresses[1] ?: ''),
      ];
    }
    if (!empty($allocation_pools)) {
      $params['AllocationPools'] = $allocation_pools;
    }

    // DNS name servers.
    $dns_name_servers = [];
    $lines = explode("\n", $form_state->getValue('dns_name_servers') ?: '');
    foreach ($lines as $line) {
      $line = trim($line ?: '');
      if (empty($line)) {
        continue;
      }

      $dns_name_servers[] = $line;
    }
    if (!empty($dns_name_servers)) {
      $params['DnsNameServers'] = $dns_name_servers;
    }

    // Host routes.
    $host_routes = [];
    $lines = explode("\n", $form_state->getValue('host_routes'));
    foreach ($lines as $line) {
      $addresses = explode(',', $line ?: '');
      if (count($addresses) !== 2) {
        continue;
      }

      $host_routes[] = [
        'destination' => trim($addresses[0] ?: ''),
        'nexthop' => trim($addresses[1] ?: ''),
      ];
    }
    if (!empty($host_routes)) {
      $params['HostRoutes'] = $host_routes;
    }

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->createSubnet($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $entity->setSubnetId($result['Subnet']['SubnetId']);
    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getSubnetId());
    }

    // Update the Subnet entity.
    $this->ec2Service->updateSubnets([
      'SubnetId' => $entity->getSubnetId(),
    ]);

    $this->processOperationStatus($entity, 'created');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackPort(OpenStackPortInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $params = [
      'Name' => $entity->getName(),
      'NetworkId' => $entity->getNetworkId(),
    ];

    // Device ID.
    if (!empty($entity->getDeviceId())) {
      $params['DeviceId'] = $entity->getDeviceId();
    }

    // Device owner.
    if (!empty($entity->getDeviceOwner())) {
      $params['DeviceOwner'] = $entity->getDeviceOwner();
    }

    // Fixed IP or subnet.
    if ($form_state->getValue('ip_address_or_subnet') === 'fixed_ip') {
      $params['FixedIps'][] = ['ip_address' => $form_state->getValue('fixed_ips')];
    }
    elseif ($form_state->getValue('ip_address_or_subnet') === 'subnet') {
      $params['FixedIps'][] = ['subnet_id' => $form_state->getValue('subnet')];
    }

    // MAC address.
    if (!empty($entity->getMacAddress())) {
      $params['MacAddress'] = $entity->getMacAddress();
    }

    // Port security.
    if (!empty($entity->getPortSecurityEnabled())) {
      $params['PortSecurityEnabled'] = $entity->getPortSecurityEnabled();
      $params['SecurityGroups'] = array_keys($form_state->getValue('security_groups'));
    }

    // VNIC type.
    if (!empty($entity->getBindingVnicType())) {
      $params['BindingVnicType'] = $entity->getBindingVnicType();
    }

    // Binding profile.
    if (!empty($entity->getBindingProfile())) {
      $params['BindingProfile'] = $entity->getBindingProfile();
    }

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->createPort($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $entity->setPortId($result['Port']['PortId']);
    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getPortId());
    }

    // Update the Port entity.
    $this->ec2Service->updatePorts([
      'PortId' => $entity->getPortId(),
    ]);

    $this->processOperationStatus($entity, 'created');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackRouter(OpenStackRouterInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $params = [
      'Name' => $entity->getName(),
      'AdminStateUp' => $entity->getAdminStateUp(),
    ];

    // External network.
    if (!empty($entity->getExternalGatewayNetworkId())) {
      $params['ExternalGatewayNetworkId'] = $entity->getExternalGatewayNetworkId();
      if ($entity->getExternalGatewayEnableSnat() !== NULL) {
        $params['ExternalGatewayEnableSnat'] = $entity->getExternalGatewayEnableSnat();
      }
    }

    // Availability Zones.
    $zone_names = [];
    foreach ($entity->getAvailabilityZones() ?: [] as $availability_zone) {
      $zone_names[] = array_values($availability_zone)[0];
    }

    if (!empty($zone_names)) {
      $params['AvailabilityZoneHints'] = $zone_names;
    }

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->createRouter($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $entity->setRouterId($result['Router']['RouterId']);
    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getRouterId());
    }

    // Update the Router entity.
    $this->ec2Service->updateRouters([
      'RouterId' => $entity->getRouterId(),
    ]);

    // Update related Port entities.
    $this->ec2Service->updatePorts([
      'DeviceId' => $entity->getRouterId(),
    ]);

    $this->processOperationStatus($entity, 'created');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state, array $extra_parameters): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $params = [
      'StackName' => $entity->getName(),
      'TimeoutMins' => $form_state->getValue('timeout_mins'),
      'DisableRollback' => !$form_state->getValue('rollback'),
    ];

    // Template.
    $template_data = $extra_parameters['template'];
    if (!empty($extra_parameters['template_url'])) {
      $params['TemplateUrl'] = $extra_parameters['template_url'];
    }
    else {
      $params['Template'] = $extra_parameters['template'];
    }

    $template_parameter_defs = $template_data['parameters'];
    $params['Parameters'] = [];
    foreach ($form_state->getValue('template_parameters') ?: [] as $name => $value) {
      if (!array_key_exists($name, $template_parameter_defs)) {
        continue;
      }

      if ($template_parameter_defs[$name]['type'] === 'comma_delimited_list' || $template_parameter_defs[$name]['type'] === 'json') {
        $value = json_decode($value, TRUE);
      }
      $params['Parameters'][$name] = $value;
    }

    // Environment.
    if (!empty($extra_parameters['environment'])) {
      $params['Environment'] = $extra_parameters['environment'];
    }

    // Tags.
    $form_values = $form_state->getValues();
    $uid = $form_values['uid'][0]['target_id'] ?? 0;
    $params['Tags'] = $this->ec2Service->getTagValueCreatedByUid(
      'openstack',
      $entity->getCloudContext(),
      $uid
    );

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->createStack($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $entity->setStackId($result['Stack']['StackId']);
    $entity->save();

    // Update the Stack entity.
    $this->ec2Service->updateStacks([
      'StackId' => $entity->getStackId(),
    ]);

    $this->processOperationStatus($entity, 'created');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state, array $extra_parameters): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $params = [
      'StackName' => $entity->getName(),
      'StackId' => $entity->getStackId(),
      'TimeoutMins' => $form_state->getValue('timeout_mins'),
      'DisableRollback' => !$form_state->getValue('rollback'),
    ];

    // Template.
    $template_data = $extra_parameters['template'];
    if (!empty($extra_parameters['template_url'])) {
      $params['TemplateUrl'] = $extra_parameters['template_url'];
    }
    else {
      $params['Template'] = $extra_parameters['template'];
    }

    $template_parameter_defs = $template_data['parameters'];
    $params['Parameters'] = [];
    foreach ($form_state->getValue('template_parameters') ?: [] as $name => $value) {
      if (!array_key_exists($name, $template_parameter_defs)) {
        continue;
      }

      if ($template_parameter_defs[$name]['type'] === 'comma_delimited_list' || $template_parameter_defs[$name]['type'] === 'json') {
        $value = json_decode($value, TRUE);
      }
      $params['Parameters'][$name] = $value;
    }

    // Environment.
    if (!empty($extra_parameters['environment'])) {
      $params['Environment'] = $extra_parameters['environment'];
    }

    // Tags.
    $form_values = $form_state->getValues();
    $uid = $form_values['uid'][0]['target_id'] ?? 0;
    $params['Tags'] = $this->ec2Service->getTagValueCreatedByUid(
      'openstack',
      $entity->getCloudContext(),
      $uid
    );

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->updateStack($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'updated');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'updated remotely');
      return TRUE;
    }

    $entity->save();

    // Update the Stack entity.
    $this->ec2Service->updateStacks([
      'StackId' => $entity->getStackId(),
    ]);

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function previewOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state, array $extra_parameters): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $params = [
      'StackName' => $entity->getName(),
      'TimeoutMins' => $form_state->getValue('timeout_mins'),
      'DisableRollback' => !$form_state->getValue('rollback'),
    ];

    // Template.
    $template_data = $extra_parameters['template'];
    if (!empty($extra_parameters['template_url'])) {
      $params['TemplateUrl'] = $extra_parameters['template_url'];
    }
    else {
      $params['Template'] = $extra_parameters['template'];
    }

    $template_parameter_defs = $template_data['parameters'];
    $params['Parameters'] = [];
    foreach ($form_state->getValue('template_parameters') ?: [] as $name => $value) {
      if (!array_key_exists($name, $template_parameter_defs)) {
        continue;
      }

      if ($template_parameter_defs[$name]['type'] === 'comma_delimited_list' || $template_parameter_defs[$name]['type'] === 'json') {
        $value = json_decode($value, TRUE);
      }
      $params['Parameters'][$name] = $value;
    }

    // Environment.
    if (!empty($extra_parameters['environment'])) {
      $params['Environment'] = $extra_parameters['environment'];
    }

    // Tags.
    $form_values = $form_state->getValues();
    $uid = $form_values['uid'][0]['target_id'] ?? 0;
    $params['Tags'] = $this->ec2Service->getTagValueCreatedByUid(
      'openstack',
      $entity->getCloudContext(),
      $uid
    );

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->previewStack($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'previewed');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'previewed remotely');
      return TRUE;
    }

    $this->processOperationStatus($entity, 'previewed');

    $stack_detail = '';
    $stack_parameters = '';
    $stack_links = '';
    $stack_resources = '';
    foreach ($result ?: [] as $name => $value) {
      if ($name === 'parameters') {
        foreach ($value ?: [] as $param_key => $param_value) {
          $stack_parameters .= sprintf('%s: %s', $param_key, json_encode($param_value, JSON_UNESCAPED_SLASHES)) . '<br>';
        }
        continue;
      }

      if ($name === 'links') {
        foreach ($value ?: [] as $link) {
          $stack_links .= sprintf('%s: %s', $link['rel'], json_encode($link['href'], JSON_UNESCAPED_SLASHES)) . '<br>';
        }
        continue;
      }

      if ($name === 'resources') {
        foreach ($value ?: [] as $resource) {
          $stack_resources .= '<pre>';
          foreach ($resource as $resource_name => $resource_value) {
            $stack_resources .= sprintf('%s: %s', $resource_name, json_encode($resource_value, JSON_UNESCAPED_SLASHES)) . '<br>';
          }
          $stack_resources .= '</pre><br>';
        }
        continue;
      }

      $stack_detail .= sprintf('%s: %s', $name, json_encode($value, JSON_UNESCAPED_SLASHES)) . '<br>';
    }

    // Stack detail.
    $this->messenger->addStatus(Markup::create('<h4>Stack Detail</h4>'));
    $this->messenger->addStatus(Markup::create('<pre>' . $stack_detail . '</pre>'));

    // Stack Parameters.
    $this->messenger->addStatus(Markup::create('<h4>Stack Parameters</h4>'));
    $this->messenger->addStatus(Markup::create('<pre>' . $stack_parameters . '</pre>'));

    // Stack Links.
    $this->messenger->addStatus(Markup::create('<h4>Stack Links</h4>'));
    $this->messenger->addStatus(Markup::create('<pre>' . $stack_links . '</pre>'));

    // Stack Resources.
    $this->messenger->addStatus(Markup::create('<h4>Stack Resources</h4>'));
    $this->messenger->addStatus(Markup::create($stack_resources));

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function checkOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state): bool {
    return $this->callOpenStackStackAction($entity, $form, $form_state, 'check', 'checked');
  }

  /**
   * {@inheritdoc}
   */
  public function checkOpenStackStackCloudResource(OpenStackStackInterface $entity): bool {
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    if (empty($this->ec2Service->checkStack([
      'StackId' => $entity->getStackId(),
      'StackName' => $entity->getName(),
    ]))) {
      return FALSE;
    }

    $this->ec2Service->updateStacks([
      'StackId' => $entity->getStackId(),
    ]);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function suspendOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state): bool {
    return $this->callOpenStackStackAction($entity, $form, $form_state, 'suspend', 'suspended');
  }

  /**
   * {@inheritdoc}
   */
  public function suspendOpenStackStackCloudResource(OpenStackStackInterface $entity): bool {
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    if (empty($this->ec2Service->suspendStack([
      'StackId' => $entity->getStackId(),
      'StackName' => $entity->getName(),
    ]))) {
      return FALSE;
    }

    $this->ec2Service->updateStacks([
      'StackId' => $entity->getStackId(),
    ]);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function resumeOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state): bool {
    return $this->callOpenStackStackAction($entity, $form, $form_state, 'resume', 'resumed');
  }

  /**
   * {@inheritdoc}
   */
  public function resumeOpenStackStackCloudResource(OpenStackStackInterface $entity): bool {
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    if (empty($this->ec2Service->resumeStack([
      'StackId' => $entity->getStackId(),
      'StackName' => $entity->getName(),
    ]))) {
      return FALSE;
    }

    $this->ec2Service->updateStacks([
      'StackId' => $entity->getStackId(),
    ]);

    return TRUE;
  }

  /**
   * Do action on an OpenStack stack.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param string $action
   *   The action.
   * @param string $action_passive
   *   The passive format of the action.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  private function callOpenStackStackAction(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state, string $action, string $action_passive): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $params = [
      'StackName' => $entity->getName(),
      'StackId' => $entity->getStackId(),
    ];

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $action_method = $action . 'Stack';
    $result = $this->ec2Service->$action_method($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, $action_passive);
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, "$action_method remotely");
      return TRUE;
    }

    // Update the Stack entity.
    $this->ec2Service->updateStacks([
      'StackId' => $entity->getStackId(),
    ]);

    $this->processOperationStatus($entity, $action_passive);

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function addOpenStackRouterInterface(OpenStackRouterInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect('view.openstack_port.router_interface', [
      'cloud_context' => $entity->getCloudContext(),
      'openstack_router' => $entity->id(),
    ]);

    $subnets = $this->entityTypeManager
      ->getStorage('openstack_subnet')
      ->loadByProperties([
        'cloud_context' => $entity->getCloudContext(),
        'subnet_id' => $form_state->getValue('subnet_id'),
      ]);
    /** @var \Drupal\openstack\Entity\OpenStackSubnet $subnet */
    $subnet = reset($subnets);

    $ip_address = $form_state->getValue('ip_address');
    if (!empty($ip_address)) {
      $ip_address = trim($ip_address ?: '');
    }
    $params = [
      'RouterId' => $entity->getRouterId(),
      'NetworkId' => $subnet->getNetworkId(),
      'SubnetId' => $form_state->getValue('subnet_id'),
      'IpAddress' => $ip_address,
    ];

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->addRouterInterface($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'added to a a port');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'added to a port remotely');
      return TRUE;
    }

    // Update the Port entity.
    $this->ec2Service->updatePorts([
      'PortId' => $result['PortId'],
    ]);

    // Update port tags.
    if ($this->ec2Service instanceof OpenStackRestService) {
      $ports = $this->entityTypeManager
        ->getStorage('openstack_port')
        ->loadByProperties([
          'cloud_context' => $entity->getCloudContext(),
          'port_id' => $result['PortId'],
        ]);

      if (empty($ports)) {
        $this->processOperationErrorStatus($entity, 'added to a port');
        return FALSE;
      }

      /** @var \Drupal\openstack\Entity\OpenStackPortInterface $port */
      $port = reset($ports);
      $port->setOwnerId($entity->getOwnerId());
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($port, $result['PortId']);

      // Update the Port entity.
      $this->ec2Service->updatePorts([
        'PortId' => $result['PortId'],
      ]);
    }

    $this->processOperationStatus($entity, 'added to a port');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function removeOpenStackRouterInterface(OpenStackRouterInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect('view.openstack_port.router_interface', [
      'cloud_context' => $entity->getCloudContext(),
      'openstack_router' => $entity->id(),
    ]);

    $port_id = $form_state->getValue('port_id');
    if (empty($port_id)) {
      $this->messenger()->addError("The Port entity doesn't exist.");
      $this->processOperationErrorStatus($entity, 'removed an interface');
      return FALSE;
    }

    $params = [
      'RouterId' => $entity->getRouterId(),
      'PortId' => $port_id,
    ];

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    /** @var \Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->ec2Service;
    $result = $ec2_service->removeRouterInterface($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'removed an interface');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'removed an interface remotely');
      return TRUE;
    }

    // Update the Port entity.
    /** @var \Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->ec2Service;
    $ec2_service->updatePorts([
      'PortId' => $result['PortId'],
    ]);

    $this->processOperationStatus($entity, 'removed an interface');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackNetwork(OpenStackNetworkInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->updateNetwork([
      'NetworkId' => $entity->getNetworkId(),
      'Name' => $entity->getName(),
      'AdminStateUp' => $entity->getAdminStateUp(),
      'Shared' => $entity->getShared(),
      'External' => $entity->getExternal(),
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'updated');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'updated remotely');
      return TRUE;
    }

    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      // Delete old tags.
      $this->ec2Service->deleteTags(
        [
          'NetworkId' => $entity->getNetworkId(),
          'EntityType'         => $entity->getEntityTypeId(),
        ]);
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getNetworkId());
    }

    // Update the Network entity.
    $this->ec2Service->updateNetworks([
      'NetworkId' => $entity->getNetworkId(),
    ]);

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackSubnet(OpenStackSubnetInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $params = [
      'SubnetId' => $entity->getSubnetId(),
      'Name' => $entity->getName(),
      'EnableDhcp' => $form_state->getValue('enable_dhcp'),
    ];

    // Gateway IP.
    if (!empty($form_state->getValue('disable_gateway'))) {
      $params['GatewayIp'] = NULL;
    }
    elseif (!empty($entity->getGatewayIp())) {
      $params['GatewayIp'] = $entity->getGatewayIp();
    }

    // Allocation pools.
    $allocation_pools = [];
    $lines = explode("\n", $form_state->getValue('allocation_pools') ?: '');
    foreach ($lines as $line) {
      $addresses = explode(',', $line ?: '');
      if (count($addresses) !== 2) {
        continue;
      }
      $allocation_pools[] = [
        'start' => trim($addresses[0] ?: ''),
        'end' => trim($addresses[1] ?: ''),
      ];
    }
    if (!empty($allocation_pools)) {
      $params['AllocationPools'] = $allocation_pools;
    }

    // DNS name servers.
    $dns_name_servers = [];
    $lines = explode("\n", $form_state->getValue('dns_name_servers') ?: '');
    foreach ($lines as $line) {
      $line = trim($line ?: '');
      if (empty($line)) {
        continue;
      }

      $dns_name_servers[] = $line;
    }
    if (!empty($dns_name_servers)) {
      $params['DnsNameServers'] = $dns_name_servers;
    }

    // Host routes.
    $host_routes = [];
    $lines = explode("\n", $form_state->getValue('host_routes') ?: '');
    foreach ($lines as $line) {
      $addresses = explode(',', $line);
      if (count($addresses) !== 2) {
        continue;
      }

      $host_routes[] = [
        'destination' => trim($addresses[0] ?: ''),
        'nexthop' => trim($addresses[1] ?: ''),
      ];
    }
    if (!empty($host_routes)) {
      $params['HostRoutes'] = $host_routes;
    }

    $result = $this->ec2Service->updateSubnet($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'updated');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'updated remotely');
      return TRUE;
    }

    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      // Delete old tags.
      $this->ec2Service->deleteTags(
        [
          'SubnetId'   => $entity->getSubnetId(),
          'EntityType'  => $entity->getEntityTypeId(),
        ]);
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getSubnetId());
    }

    // Update the Subnet entity.
    $this->ec2Service->updateSubnets([
      'SubnetId' => $entity->getSubnetId(),
    ]);

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackPort(OpenStackPortInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $params = [
      'PortId' => $entity->getPortId(),
      'Name' => $entity->getName(),
      'AdminStateUp' => $entity->getAdminStateUp(),
      'BindingVnicType' => $entity->getBindingVnicType(),
    ];

    // Port security.
    $params['PortSecurityEnabled'] = !empty($entity->getPortSecurityEnabled());
    $params['SecurityGroups'] = [];
    $params['AllowedAddressPairs'] = [];
    if (!empty($entity->getPortSecurityEnabled())) {
      $params['SecurityGroups'] = array_keys($form_state->getValue('security_groups'));
      foreach ($entity->getAllowedAddressPairs() as $item) {
        $pair = [
          'ip_address' => $item['item_key'],
        ];
        if (!empty($item['item_value'])) {
          $pair['mac_address'] = $item['item_value'];
        }
        $params['AllowedAddressPairs'][] = $pair;
      }
    }

    $result = $this->ec2Service->updatePort($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'updated');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'updated remotely');
      return TRUE;
    }

    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      // Delete old tags.
      $this->ec2Service->deleteTags(
        [
          'PortId'     => $entity->getPortId(),
          'EntityType' => $entity->getEntityTypeId(),
        ]);
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getPortId());
    }

    // Update the Port entity.
    $this->ec2Service->updatePorts([
      'PortId' => $entity->getPortId(),
    ]);

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackRouter(OpenStackRouterInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $params = [
      'RouterId' => $entity->getRouterId(),
      'Name' => $entity->getName(),
      'AdminStateUp' => $entity->getAdminStateUp(),
    ];

    // External network.
    if (!empty($entity->getExternalGatewayNetworkId())) {
      $params['ExternalGatewayNetworkId'] = $entity->getExternalGatewayNetworkId();
      if ($form_state->getValue('external_gateway_enable_snat') !== NULL) {
        $params['ExternalGatewayEnableSnat'] = $entity->getExternalGatewayEnableSnat();
      }
    }

    // Routes.
    $routes = [];
    foreach ($entity->getRoutes() as $item) {
      $routes[] = [
        'destination' => $item['item_key'],
        'nexthop' => $item['item_value'],
      ];
    }
    $params['routes'] = $routes;

    $result = $this->ec2Service->updateRouter($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'updated');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'updated remotely');
      return TRUE;
    }

    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      // Delete old tags.
      $this->ec2Service->deleteTags(
        [
          'RouterId'     => $entity->getRouterId(),
          'EntityType' => $entity->getEntityTypeId(),
        ]);
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getRouterId());
    }

    // Update the Router entity.
    $this->ec2Service->updateRouters([
      'RouterId' => $entity->getRouterId(),
    ]);

    // Update related Port entities.
    $this->ec2Service->updatePorts([
      'DeviceId' => $entity->getRouterId(),
    ]);

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackQuota(OpenStackQuotaInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var \Drupal\openstack\Entity\OpenStackQuotaInterface $original_entity */
    $original_entity = OpenStackQuota::load($entity->id());

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->copyFormItemValues($entity, $form);
    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    if ($entity->getStatus() === OpenStackQuotaInterface::APPROVED
      && !$this->currentUser->hasPermission(OpenStackQuotaInterface::PERMISSION_TO_APPROVE)) {
      $entity->setStatus(OpenStackQuotaInterface::DRAFT);
    }

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
    if ($entity->getStatus() !== OpenStackQuotaInterface::APPROVED) {
      $this->notifyQuotaStatusChanged($entity, $original_entity->getStatus());
      $entity->setOwnerId($this->currentUser->id());
      $entity->save();
      $this->processOperationStatus($entity, 'updated');
      return TRUE;
    }

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $params = [
      'Name' => $entity->getName(),
      'ProjectId' => $entity->getProjectId(),
      'Instances' => (int) $entity->get('instances_new')->value,
      'Cores' => (int) $entity->get('cores_new')->value,
      'Ram' => (int) $entity->get('ram_new')->value,
      'MetadataItems' => (int) $entity->get('metadata_items_new')->value,
      'KeyPairs' => (int) $entity->get('key_pairs_new')->value,
      'ServerGroups' => (int) $entity->get('server_groups_new')->value,
      'ServerGroupMembers' => (int) $entity->get('server_group_members_new')->value,
      'InjectedFiles' => (int) $entity->get('injected_files_new')->value,
      'InjectedFileContentBytes' => (int) $entity->get('injected_file_content_bytes_new')->value,
      'InjectedFilePathBytes' => (int) $entity->get('injected_file_path_bytes_new')->value,
      'Volumes' => (int) $entity->get('volumes_new')->value,
      'Snapshots' => (int) $entity->get('snapshots_new')->value,
      'GigaBytes' => (int) $entity->get('gigabytes_new')->value,
      'Network' => (int) $entity->get('network_new')->value,
      'Subnet' => (int) $entity->get('subnet_new')->value,
      'Port' => (int) $entity->get('port_new')->value,
      'Router' => (int) $entity->get('router_new')->value,
      'FloatingIp' => (int) $entity->get('floatingip_new')->value,
      'SecurityGroup' => (int) $entity->get('security_group_new')->value,
      'SecurityGroupRule' => (int) $entity->get('security_group_rule_new')->value,
    ];

    $result = $this->ec2Service->updateQuota($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'updated');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'updated remotely');
      return TRUE;
    }

    $this->notifyQuotaStatusChanged($entity, $original_entity->getStatus());

    $entity->save();

    // Update the Quota entity.
    $this->ec2Service->updateQuotas();

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * Notify that quota status is changed.
   *
   * @param \Drupal\openstack\Entity\OpenStackQuotaInterface $entity
   *   The quota entity.
   * @param string $original_status
   *   The original status of quota.
   */
  private function notifyQuotaStatusChanged(OpenStackQuotaInterface $entity, $original_status): void {
    $status = $entity->getStatus();

    // Send request mail.
    if ($original_status !== OpenStackQuotaInterface::REVIEW
    && $status === OpenStackQuotaInterface::REVIEW) {
      $this->sendQuotaRequestMail($entity);
    }

    // Send approved mail.
    if ($original_status !== OpenStackQuotaInterface::APPROVED
    && $status === OpenStackQuotaInterface::APPROVED) {
      $this->sendQuotaApprovedMail($entity);
    }
  }

  /**
   * Send the quota request mail.
   *
   * @param \Drupal\openstack\Entity\OpenStackQuotaInterface $quota
   *   The quota entity.
   */
  private function sendQuotaRequestMail(OpenStackQuotaInterface $quota): void {
    $config = $this->configFactory->get('openstack.settings');

    // Mail to.
    $to_emails = $config->get('openstack_quota_notification_request_emails');
    if (empty($to_emails)) {
      return;
    }

    // Mail subject.
    $subject = $config->get('openstack_quota_notification_request_subject');

    // Mail message.
    $quotas[] = $quota;
    /** @var \Drupal\cloud\Service\CloudService $cloud_service */
    $cloud_service = $this->cloudService;
    $message = $cloud_service->getEntityNotificationMessage(
      $quotas,
      $config->get('openstack_quota_notification_quota_request_info'),
      'openstack_quota_request',
      $config->get('openstack_quota_notification_request_msg'),
      'quotas_request'
    );

    $message = Html::decodeEntities($message);
    $approve_url = $quota->toUrl('edit-form', ['absolute' => TRUE])->toString();
    $button_html = $cloud_service->getHtmlEmailButton($approve_url, 'Approve This Quota');
    $message = str_replace($approve_url, $button_html, $message);

    // Return characters are converted to br due to html mail.
    $message = nl2br($message);

    $cloud_service->sendNotificationEmail(
      $subject,
      $message,
      $to_emails,
      'quotas_request',
      'openstack_quota'
    );
  }

  /**
   * Send the quota approved mail.
   *
   * @param \Drupal\openstack\Entity\OpenStackQuotaInterface $quota
   *   The quota entity.
   */
  private function sendQuotaApprovedMail(OpenStackQuotaInterface $quota): void {
    $config = $this->configFactory->get('openstack.settings');

    // Mail to.
    $to_emails = $quota->getOwner()->getEmail();
    if (empty($to_emails)) {
      return;
    }

    // Mail subject.
    $subject = $config->get('openstack_quota_notification_approved_subject');

    // Mail message.
    $quotas[] = $quota;
    /** @var \Drupal\cloud\Service\CloudService $cloud_service */
    $cloud_service = $this->cloudService;
    $message = $cloud_service->getEntityNotificationMessage(
      $quotas,
      $config->get('openstack_quota_notification_quota_info'),
      'openstack_quota',
      $config->get('openstack_quota_notification_approved_msg'),
      'quotas'
    );

    $cloud_service->sendNotificationEmail(
      $subject,
      $message,
      $to_emails,
      'quotas_approved',
      'openstack_quota'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackProject(OpenStackProjectInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $params = [
      'ProjectId' => $entity->getProjectId(),
      'Name' => $entity->getName(),
      'Description' => $entity->getDescription(),
      'Enabled' => $entity->getEnabled(),
    ];

    $result = $this->ec2Service->updateProject($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'updated');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'updated remotely');
      return TRUE;
    }

    // Update user and roles.
    /** @var \Drupal\openstack\Entity\OpenStackProjectInterface $original_entity */
    $original_entity = $this->entityTypeManager
      ->getStorage($entity->getEntityTypeId())
      ->loadUnchanged($entity->id());

    $new_user_roles_list = $entity->getUserRoles();
    $original_user_roles_list = $original_entity->getUserRoles();

    // Convert to array like ['user1,role1', 'user1,role2', ...].
    $new_flat_user_role_list = [];
    foreach ($new_user_roles_list ?: [] as $user_roles) {
      if (empty($user_roles['user']) || empty($user_roles['roles'])) {
        continue;
      }

      foreach (explode(',', $user_roles['roles']) ?: [] as $role) {
        $new_flat_user_role_list[] = "{$user_roles['user']},{$role}";
      }
    }
    $new_flat_user_role_list = array_unique($new_flat_user_role_list);

    $original_flat_user_role_list = [];
    foreach ($original_user_roles_list ?: [] as $user_roles) {
      if (empty($user_roles['user']) || empty($user_roles['roles'])) {
        continue;
      }

      foreach (explode(',', $user_roles['roles']) ?: [] as $role) {
        $original_flat_user_role_list[] = "{$user_roles['user']},{$role}";
      }
    }
    $original_flat_user_role_list = array_unique($original_flat_user_role_list);

    $params = [
      'ProjectId' => $entity->getProjectId(),
    ];

    // Get the user and role list to be added.
    $user_role_list_to_assign = array_diff($new_flat_user_role_list, $original_flat_user_role_list);

    // Assign roles to users on the project.
    foreach ($user_role_list_to_assign ?: [] as $user_role) {
      $user_role_ids = explode(',', $user_role);
      $user_entity = OpenStackUser::load($user_role_ids[0]);
      $role_entity = OpenStackRole::load($user_role_ids[1]);
      if (empty($user_entity) || empty($role_entity)) {
        continue;
      }

      $params['UserId'] = $user_entity->getUserId();
      $params['RoleId'] = $role_entity->getRoleId();
      $this->ec2Service->assignRoleToUserOnProject($params);
    }

    // Get the user and role list to be unassigned.
    $user_role_list_to_unassign = array_diff($original_flat_user_role_list, $new_flat_user_role_list);

    // Unassign roles from users on the project.
    foreach ($user_role_list_to_unassign ?: [] as $user_role) {
      $user_role_ids = explode(',', $user_role);
      $user_entity = OpenStackUser::load($user_role_ids[0]);
      $role_entity = OpenStackRole::load($user_role_ids[1]);
      if (empty($user_entity) || empty($role_entity)) {
        continue;
      }

      $params['UserId'] = $user_entity->getUserId();
      $params['RoleId'] = $role_entity->getRoleId();
      $this->ec2Service->unassignRoleFromUserOnProject($params);
    }

    $entity->save();

    // Update the Project entity.
    $this->ec2Service->updateProjects([
      'ProjectId' => $entity->getProjectId(),
    ], FALSE);

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackRole(OpenStackRoleInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $params = [
      'RoleId' => $entity->getRoleId(),
      'Name' => $entity->getName(),
      'Description' => $entity->getDescription(),
    ];

    $result = $this->ec2Service->updateRole($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'updated');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'updated remotely');
      return TRUE;
    }

    $entity->save();

    // Update the Role entity.
    $this->ec2Service->updateRoles([
      'RoleId' => $entity->getRoleId(),
    ], FALSE);

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackUser(OpenStackUserInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $params = [
      'UserId' => $entity->getUserId(),
      'Name' => $entity->getName(),
      'Description' => $entity->getDescription(),
      'Email' => $entity->getEmail(),
      'DefaultProjectId' => $entity->getDefaultProjectId(),
      'Enabled' => $entity->getEnabled(),
    ];

    $result = $this->ec2Service->updateUser($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'updated');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'updated remotely');
      return TRUE;
    }

    $entity->save();

    // Update the User entity.
    $this->ec2Service->updateUsers([
      'UserId' => $entity->getUserId(),
    ], FALSE);

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function changePasswordOpenStackUser(OpenStackUserInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $params = [
      'UserId' => $entity->getUserId(),
      'Password' => $form_state->getValue('password'),
    ];

    $result = $this->ec2Service->updateUser($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'updated');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'updated remotely');
      return TRUE;
    }

    $entity->save();

    // Update the User entity.
    $this->ec2Service->updateUsers([
      'UserId' => $entity->getUserId(),
    ], FALSE);

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackNetwork(OpenStackNetworkInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->deleteNetwork([
      'NetworkId' => $entity->getNetworkId(),
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'deleted remotely');
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackNetworkCloudResource(OpenStackNetworkInterface $entity): bool {
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deleteNetwork(
      ['NetworkId' => $entity->getNetworkId()]
      ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackSubnet(OpenStackSubnetInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->deleteSubnet([
      'SubnetId' => $entity->getSubnetId(),
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'deleted remotely');
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackPort(OpenStackPortInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->deletePort([
      'PortId' => $entity->getPortId(),
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'deleted remotely');
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackPortCloudResource(OpenStackPortInterface $entity): bool {
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deletePort(
      ['PortId' => $entity->getPortId()]
      ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackRouter(OpenStackRouterInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->deleteRouter([
      'RouterId' => $entity->getRouterId(),
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'deleted remotely');
      return TRUE;
    }

    // Update related Port entities.
    $this->ec2Service->updatePorts([
      'DeviceId' => $entity->getRouterId(),
    ]);

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->deleteStack([
      'ProjectId' => $entity->getProjectId(),
      'StackId' => $entity->getStackId(),
      'StackName' => $entity->getName(),
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'deleted remotely');
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackStackCloudResource(OpenStackStackInterface $entity): bool {
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deleteStack([
      'ProjectId' => $entity->getProjectId(),
      'StackId' => $entity->getStackId(),
      'StackName' => $entity->getName(),
    ]) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackProject(OpenStackProjectInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->deleteProject([
      'ProjectId' => $entity->getProjectId(),
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'deleted remotely');
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackProjectCloudResource(OpenStackProjectInterface $entity): bool {
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deleteProject(
      ['ProjectId' => $entity->getProjectId()]
      ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackRole(OpenStackRoleInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->deleteRole([
      'RoleId' => $entity->getRoleId(),
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'deleted remotely');
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackRoleCloudResource(OpenStackRoleInterface $entity): bool {
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deleteRole(
      ['RoleId' => $entity->getRoleId()]
      ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackUser(OpenStackUserInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->deleteUser([
      'UserId' => $entity->getUserId(),
    ]);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'deleted remotely');
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackUserCloudResource(OpenStackUserInterface $entity): bool {
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deleteUser(
      ['UserId' => $entity->getUserId()]
      ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackFloatingIp(ElasticIpInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    if (!($this->ec2Service instanceof OpenStackRestService)) {
      return $this->awsCloudOperationsService->createElasticIp($this->entity, $form, $form_state);
    }

    // Refresh the instances.
    $this->ec2Service->updateInstances();

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
    $params = [
      'AllocationId' => $form_state->getValue('floating_network_id'),
    ];

    $result = $this->ec2Service->allocateAddress($params);

    if (empty($result)) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $floating_network_id = $form_state->getValue('floating_network_id');
    $entity->setAllocationId($floating_network_id);
    $entity->save();

    $public_ip = $result['PublicIp'] ?? NULL;
    $floating_ip_type = !empty($result['PublicIp']) ? 'Public IP' : '';
    /** @var \Drupal\openstack\Entity\OpenStackFloatingIp $entity */
    if (!empty($public_ip)
      && $entity->setFloatingIpId($result['FloatingIpId'] ?? '')
      && $entity->setPublicIp($public_ip)
      && $entity->setElasticIpType($floating_ip_type)
      && $entity->save()) {

      // Create uid tag.
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getFloatingIpId());
      $this->processOperationStatus($entity, 'created');
      $this->clearCacheValues($entity->getCacheTags());
    }
    else {
      $this->processOperationErrorStatus($entity, 'created');
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackFloatingIp(ElasticIpInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    // Copy values of the form elements whose type are item to entity.
    // If not, the properties corresponding to the form elements
    // will be saved as NULL.
    $this->awsCloudOperationsService->copyFormItemValues($this->entity, $form);

    $this->awsCloudOperationsService->trimTextfields($this->entity, $form, $form_state);

    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    if (!$this->ec2Service instanceof OpenStackRestService) {
      $this->saveFloatingIp();
      return TRUE;
    }

    // Delete old tags.
    /** @var \Drupal\openstack\Entity\OpenStackFloatingIp $entity */
    $this->ec2Service->deleteTags(
      [
        'FloatingIpId' => $entity->getFloatingIpId(),
        'EntityType'   => $entity->getEntityTypeId(),
      ]);

    $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getFloatingIpId());

    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    if ($cloud_config->isRemote()) {
      $this->processOperationStatus($entity, 'updated remotely');
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
      return TRUE;
    }

    if ($entity->save()) {
      $this->processOperationStatus($entity, 'updated');
      $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
      return TRUE;
    }

    $this->processOperationErrorStatus($entity, 'updated');
    return TRUE;
  }

  /**
   * Save a floating IP.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function saveFloatingIp(): void {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface */
    $entity = $this->entity;
    $entity->save();

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    // Update the name.
    $this->awsCloudOperationsService->updateNameAndCreatedByTags(
      $entity,
      $entity->getAllocationId()
    );

    $this->processOperationStatus($entity, 'updated');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackFloatingIp(ElasticIpInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    if ($this->ec2Service instanceof OpenStackEc2Service) {
      return $this->awsCloudOperationsService->deleteElasticIp($this->entity, $form, $form_state);
    }

    /** @var \Drupal\openstack\Entity\OpenStackFloatingIp $entity */
    $params = ['FloatingIpId' => $entity->getFloatingIpId()];

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    if (empty($entity)
      || empty($params)
      || empty($this->ec2Service->releaseAddress($params))) {
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();
    $this->clearCacheValues($entity->getCacheTags());
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackFloatingIpCloudResource(ElasticIpInterface $entity): bool {
    // Switch OpenStack EC2 or Rest service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    if (!($this->ec2Service instanceof OpenStackRestService)) {
      return $this->awsCloudOperationsService->deleteElasticIpCloudResource($entity);
    }

    if ($this->ec2Service instanceof OpenStackRestService) {
      /** @var \Drupal\openstack\Entity\OpenStackFloatingIpInterface $entity */
      $params = ['FloatingIpId' => $entity->getFloatingIpId()];

      return $this->ec2Service->releaseAddress($params) !== NULL;
    }
    // Return unable to process.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function disassociateOpenStackFloatingIp(OpenStackFloatingIpInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    /** @var \Drupal\openstack\Entity\OpenStackFloatingIp $entity */
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    if (!($this->ec2Service instanceof OpenStackRestService)) {
      return $this->disassociateFloatingIp($form, $form_state);
    }

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    /** @var \Drupal\openstack\Entity\OpenStackFloatingIp $entity */
    $result = $this->ec2Service->disassociateAddress([
      'FloatingIpId' => $entity->getFloatingIpId(),
      'AssociationId' => $entity->getAssociationId(),
    ]);

    if (empty($result)) {
      $this->messenger->addError($this->t('Unable to disassociate Floating IP.'));
      return FALSE;
    }

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $instance_id = $entity->getInstanceId();
    $network_id = $entity->getNetworkId();

    if (!empty($instance_id)) {
      $instance = $this->getInstanceById($instance_id, $module_name, $entity->getCloudContext());
      $instance_link = $this->entityLinkRenderer->renderViewElement(
        $instance_id,
        "{$module_name}_instance",
        'instance_id',
        [],
        !empty($instance->getName()) !== $instance->getInstanceId()
          ? $this->t('@instance_name (@instance_id)', [
            '@instance_name' => $instance->getName(),
            '@instance_id' => $instance_id,
          ])
          : $instance_id
      );
    }

    if (!empty($network_id)) {
      $network = $this->getNetworkById($network_id, $module_name, $entity->getCloudContext());
      $network_link = $this->entityLinkRenderer->renderViewElement(
        $network_id,
        "{$module_name}_network",
        'network_id',
        [],
        $network->getName() !== $network->getNetworkId()
          ? $this->t('@network_name (@network_id)', [
            '@network_name' => $network->getName(),
            '@network_id' => $network_id,
          ])
          : $network_id
      );
    }

    if (!empty($result['SendToWorker'])) {
      !empty($instance_link) && !empty($network_link)
        ? $this->messenger->addStatus($this->t('Floating IP disassociated remotely from: <ul><li>Instance: @instance_id </li> <li>Network: @network_id</li></ul>', [
          '@instance_id' => Markup::create($instance_link['#markup']),
          '@network_id' => Markup::create($network_link['#markup']),
        ]))
        : $this->messenger->addStatus($this->t('Floating IP disassociated remotely'));

      return TRUE;
    }

    !empty($instance_link) && !empty($network_link)
      ? $this->messenger->addStatus($this->t('Floating IP disassociated from: <ul><li>Instance: @instance_id </li> <li>Network: @network_id</li></ul>', [
        '@instance_id' => Markup::create($instance_link['#markup']),
        '@network_id' => Markup::create($network_link['#markup']),
      ]))
      : $this->messenger->addStatus($this->t('Floating IP disassociated'));

    $this->ec2Service->updateFloatingIps();
    $this->ec2Service->updateInstances();
    $this->ec2Service->updateNetworks();

    $this->clearCacheValues($entity->getCacheTags());

    // Refresh the instances.
    $this->ec2Service->updateInstances();
    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);
    return TRUE;
  }

  /**
   * Disassociate a floating IP.
   *
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function disassociateFloatingIp(array $form, FormStateInterface $form_state): bool {
    /** @var \Drupal\openstack\Entity\OpenStackFloatingIp @entity */
    $entity = $this->entity;
    $this->ec2Service->setCloudContext($entity->getCloudContext());

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $result = $this->ec2Service->disassociateAddress([
      'AssociationId' => $entity->getAssociationId(),
    ]);

    if ($result === NULL) {
      $this->messenger->addError($this->t('Unable to disassociate Elastic IP.'));
      return FALSE;
    }

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $instance_id = $entity->getInstanceId();
    $network_id = $entity->getNetworkId();

    if (!empty($instance_id)) {
      $instance = $this->getInstanceById($instance_id, $module_name, $entity->getCloudContext());
      $instance_link = $this->entityLinkRenderer->renderViewElement(
        $instance_id,
        "{$module_name}_instance",
        'instance_id',
        [],
        !empty($instance->getName()) !== $instance->getInstanceId()
          ? $this->t('@instance_name (@instance_id)', [
            '@instance_name' => $instance->getName(),
            '@instance_id' => $instance_id,
          ])
          : $instance_id
      );
    }

    if (!empty($network_id)) {
      $network = $this->getNetworkById($network_id, $module_name, $entity->getCloudContext());
      $network_link = $this->entityLinkRenderer->renderViewElement(
      $network_id,
      "{$module_name}_network",
      'network_id',
      [],
      $network->getName() !== $network->getNetworkId()
        ? $this->t('@network_name (@network_id)', [
          '@network_name' => $network->getName(),
          '@network_id' => $network_id,
        ])
        : $network_id
      );
    }

    if (!empty($result['SendToWorker'])) {
      !empty($instance_link) && !empty($network_link)
        ? $this->messenger->addStatus($this->t('Elastic IP disassociated from remotely: <ul><li>Instance: @instance_id </li> <li>Network: @network_id</li></ul>', [
          '@instance_id' => Markup::create($instance_link['#markup']),
          '@network_id' => Markup::create($network_link['#markup']),
        ]))
        : $this->messenger->addStatus($this->t('Elastic IP disassociated remotely'));

      return TRUE;
    }

    !empty($instance_link) && !empty($network_link)
      ? $this->messenger->addStatus($this->t('Elastic IP disassociated from: <ul><li>Instance: @instance_id </li> <li>Network: @network_id</li></ul>', [
        '@instance_id' => Markup::create($instance_link['#markup']),
        '@network_id' => Markup::create($network_link['#markup']),
      ]))
      : $this->messenger->addStatus($this->t('Elastic IP disassociated'));

    $this->ec2Service->updateElasticIps();
    $this->ec2Service->updateInstances();
    /** @var \Drupal\openstack\Service\Ec2\OpenStackService */
    $ec2_service = $this->ec2Service;
    $ec2_service->updateNetworks();

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSubmitEvent($entity);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackSecurityGroup(SecurityGroupInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
      return $this->awsCloudOperationsService->createSecurityGroup($entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackSecurityGroup(SecurityGroupInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
      return $this->awsCloudOperationsService->deleteSecurityGroup($this->entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackSecurityGroupCloudResource(SecurityGroupInterface $entity): bool {
    // Switch OpenStack EC2 or Rest service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    return $this->awsCloudOperationsService->deleteSecurityGroupCloudResource($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function revokeOpenStackSecurityGroup(
    SecurityGroupInterface $entity,
    array &$form,
    FormStateInterface $form_state,
  ): bool {
    $this->entity = $entity;
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $referer = $this->requestStack->getCurrentRequest()->headers->get('referer');
    $url = parse_url($referer);
    parse_str($url['query'], $results);
    $type = $results['type'];
    $position = $results['position'];

    // Use parent save function in case of OpenStack EC2.
    if (!($this->ec2Service instanceof OpenStackRestService)) {
      return $this->awsCloudOperationsService->revokeSecurityGroup($entity, $form, $form_state, $type, $position);
    }

    /** @var \Drupal\cloud\Entity\CloudContentEntityBase|\Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
    $permission = $this->awsCloudOperationsService->getPermission($type, $position, $entity);
    if ($permission !== FALSE) {

      $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.canonical", [
        'cloud_context' => $entity->getCloudContext(),
        $entity->getEntityTypeId() => $entity->id(),
      ]);

      // Revoke security group rule.
      $perm_array['IpPermissions'][] = [
        'RuleId' => $permission->rule_id,
      ];
      $param_array = [
        'RuleId' => $perm_array['IpPermissions'][0]['RuleId'],
      ];
      $result = $this->ec2Service->revokeSecurityGroupRule($param_array);

      if (!empty($result['SendToWorker'])) {
        $this->messenger->addStatus($this->t('Permission revoked remotely'));
        return TRUE;
      }

      // Have the system refresh the security group.
      $this->ec2Service->updateSecurityGroups([
        'GroupIds' => [$entity->getGroupId()],
      ], FALSE);

      $this->validateRevoke($type, $entity)
        ? $this->messenger->addStatus($this->t('Permission revoked'))
        : $this->messenger->addError($this->t('Permission not revoked'));

      $this->clearCacheValues($entity->getCacheTags());
    }

    return TRUE;
  }

  /**
   * Verify that the revoke was successful.
   *
   * Since OpenStack REST does not return any error codes in the revoke API
   * calls, the only way to verify is to count the permissions array from the
   * current entity, and the entity that is newly updated from the
   * updateSecurityGroups API call.
   *
   * @param string $type
   *   The type of permission.
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   The SecurityGroup.
   *
   * @return bool
   *   True or false.
   */
  private function validateRevoke(string $type, CloudContentEntityBase $entity): bool {
    /** @var \Drupal\openstack\Entity\OpenStackSecurityGroup $updated_group */
    $updated_group = $this->entityTypeManager
      ->getStorage($entity->getEntityTypeId())
      ->load($entity->id());

    /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity */
    return $type === 'outbound_permission'
      ? $entity->getOutboundPermission()->count() === $updated_group->getOutboundPermission()->count()
      : $entity->getIpPermission()->count() === $updated_group->getIpPermission()->count();
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackSecurityGroup(SecurityGroupInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;

      // Switch OpenStack EC2 or REST service based on $cloud_context.
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
      if ($this->ec2Service instanceof OpenStackEc2Service) {
        return $this->awsCloudOperationsService->editSecurityGroup($entity, $form, $form_state);
      }
      /** @var \Drupal\openstack\Service\Rest\OpenStackServiceInterface $ec2_service */
      $ec2_service = $this->ec2Service;
      // Call copyFormItemValues() to ensure the form array is intact.
      $this->awsCloudOperationsService->copyFormItemValues($entity, $form);

      $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

      $ec2_service->setCloudContext($entity->getCloudContext());

      // Delete old tags.
      $ec2_service->deleteTags(
        [
          'SecurityGroupId' => $entity->getGroupId(),
          'EntityType'      => $entity->getEntityTypeId(),
        ]);

      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getGroupId());

      /** @var \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $existing_group */
      $existing_group = $this->entityTypeManager
        ->getStorage($entity->getEntityTypeId())
        ->load($entity->id());

      if (!empty($existing_group)) {

        // Update the inbound/outbound permissions.
        $existing_permissions_inbound = $existing_group->getIpPermission() ?? [];
        // Clear out the existing permissions.
        foreach ($existing_permissions_inbound ?: [] as $inbound_permission) {
          if (empty($inbound_permission->rule_id)) {
            continue;
          }

          $ip_param_array = [
            'RuleId' => $inbound_permission->rule_id,
          ];

          // @todo Add the validation of the return value: TRUE or FALSE.
          $ec2_service->revokeSecurityGroupRule($ip_param_array);
        }

        // Update the inbound/outbound permissions.
        $existing_permissions_outbound = $existing_group->getOutboundPermission() ?? [];
        // Clear out the existing permissions.
        foreach ($existing_permissions_outbound ?: [] as $outbound_permission) {
          if (empty($outbound_permission->rule_id)) {
            continue;
          }

          $ip_param_array = [
            'RuleId' => $outbound_permission->rule_id,
          ];

          // @todo Add the validation of the return value: TRUE or FALSE.
          $ec2_service->revokeSecurityGroupRule($ip_param_array);
        }

        // Have the system refresh the security group.
        $ec2_service->updateSecurityGroups([
          'GroupIds' => [$entity->getGroupId()],

        ], FALSE);

        $this->clearCacheValues($entity->getCacheTags());
      }
      else {
        $this->messenger->addError($this->t('Cannot update security group.'));
      }

      if ($entity->save()) {

        // If the request was sent using Drupal Form.
        $ip_perm_arr = ($entity->get('ip_permission') !== NULL)
          ? $entity->get('ip_permission')->getValue() : [];
        $outbound_perm_arr = ($entity->get('outbound_permission') !== NULL)
          ? $entity->get('outbound_permission')->getValue() : [];

        // If the request was sent using SPA.
        if (empty($form) && empty($ip_perm_arr) && empty($outbound_perm_arr)) {
          $ip_perm_arr = ($form_state->get('ip_permission') !== NULL)
            ? $form_state->get('ip_permission') : [];
          $outbound_perm_arr = ($form_state->get('outbound_permission') !== NULL)
            ? $form_state->get('outbound_permission') : [];
        }

        $ec2_service->updateSecurityGroup([
          'GroupId' => $entity->getGroupId(),
          'GroupName' => $entity->getName(),
        ]);

        $well_known_ports = OpenStackServiceInterface::WELL_KNOWN_PORTS;
        $ip_protocol_names = array_keys($well_known_ports);

        // Create inbound security group rule.
        foreach ($ip_perm_arr ?: [] as $ip_perm) {
          if (!empty($ip_perm)) {
            $inbound_rule_param = [
              'Direction' => 'ingress',
              'FromPort' => in_array($ip_perm['ip_protocol'], $ip_protocol_names) ? $well_known_ports[$ip_perm['ip_protocol']]['from_port'] : $ip_perm['from_port'],
              'ToPort' => in_array($ip_perm['ip_protocol'], $ip_protocol_names) ? $well_known_ports[$ip_perm['ip_protocol']]['to_port'] : $ip_perm['to_port'],
              'IpProtocol' => in_array($ip_perm['ip_protocol'], $ip_protocol_names) ? $well_known_ports[$ip_perm['ip_protocol']]['ip_protocol'] : $ip_perm['ip_protocol'],
              'GroupId' => $entity->getGroupId(),
              'Description' => $ip_perm['description'],
            ];

            switch ($ip_perm['source']) {
              case 'ip4':
                $inbound_rule_param += [
                  'Source' => 'IPv4',
                  'CidrIp' => $ip_perm['cidr_ip'],
                ];
                $ec2_service->createSecurityGroupRule($inbound_rule_param);
                break;

              case 'group':
                $inbound_rule_param += [
                  'Source' => 'IPv4',
                  'RemoteGroupId' => $ip_perm['group_id'],
                  'RemoteUserId' => $ip_perm['user_id'],
                ];
                $ec2_service->createSecurityGroupRule($inbound_rule_param);
                break;

              case 'ip6':
                $inbound_rule_param += [
                  'Source' => 'IPv6',
                  'CidrIpv6' => $ip_perm['cidr_ip_v6'],
                ];
                $ec2_service->createSecurityGroupRule($inbound_rule_param);
                break;
            }
          }
        }

        // Create outbound security group rule.
        foreach ($outbound_perm_arr ?: [] as $outbound_perm) {
          if (!empty($outbound_perm)) {
            $outbound_rule_param = [
              'Direction' => 'egress',
              'FromPort' => in_array($outbound_perm['ip_protocol'], $ip_protocol_names) ? $well_known_ports[$outbound_perm['ip_protocol']]['from_port'] : $outbound_perm['from_port'],
              'ToPort' => in_array($outbound_perm['ip_protocol'], $ip_protocol_names) ? $well_known_ports[$outbound_perm['ip_protocol']]['to_port'] : $outbound_perm['to_port'],
              'IpProtocol' => in_array($outbound_perm['ip_protocol'], $ip_protocol_names) ? $well_known_ports[$outbound_perm['ip_protocol']]['ip_protocol'] : $outbound_perm['ip_protocol'],
              'GroupId' => $entity->getGroupId(),
              'Description' => $outbound_perm['description'],
            ];

            switch ($outbound_perm['source']) {
              case 'ip4':
                $outbound_rule_param += [
                  'Source' => 'IPv4',
                  'CidrIp' => $outbound_perm['cidr_ip'],
                ];
                $ec2_service->createSecurityGroupRule($outbound_rule_param);
                break;

              case 'group':
                $outbound_rule_param += [
                  'Source' => 'IPv4',
                  'RemoteGroupId' => $outbound_perm['group_id'],
                  'RemoteUserId' => $outbound_perm['user_id'],
                ];
                $ec2_service->createSecurityGroupRule($outbound_rule_param);
                break;

              case 'ip6':
                $outbound_rule_param += [
                  'Source' => 'IPv6',
                  'CidrIpv6' => $outbound_perm['cidr_ip_v6'],
                ];
                $ec2_service->createSecurityGroupRule($outbound_rule_param);
                break;
            }
          }
        }

        // Have the system refresh the security group.
        $ec2_service->updateSecurityGroups([
          'GroupIds' => [$entity->getGroupId()],
        ], FALSE);

        if (count($this->messenger->messagesByType('error')) === 0) {
          $this->processOperationStatus($entity, $ec2_service->isWorkerResource() ? 'updated remotely' : 'updated');
          $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.canonical", [
            'cloud_context' => $entity->getCloudContext(),
            $entity->getEntityTypeId() => $entity->id(),
          ]);
        }
        return TRUE;
      }
      else {
        $this->processOperationErrorStatus($this->entity, 'updated');
        return FALSE;
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackSnapshot(SnapshotInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->awsCloudOperationsService->createSnapshot($this->entity, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackSnapshot(SnapshotInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->awsCloudOperationsService->deleteSnapshot($this->entity, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackSnapshotCloudResource(SnapshotInterface $entity): bool {
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->awsCloudOperationsService->deleteSnapshotCloudResource($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackSnapshot(SnapshotInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    $this->awsCloudOperationsService->editSnapshot($this->entity, $form, $form_state);

    // Update the snapshot of OpenStack REST API.
    if ($this->ec2Service instanceof OpenStackRestService) {
      $this->ec2Service->updateSnapshot([
        'SnapshotId'  => $entity->getSnapshotId(),
        'Name'        => $entity->getName(),
        'Description' => $entity->getDescription(),
      ]);
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity */
    $this->entity = $entity;

    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->awsCloudOperationsService->createVolume($entity, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity */
    $this->entity = $entity;

    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->awsCloudOperationsService->deleteVolume($entity, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackVolumeCloudResource(VolumeInterface $entity): bool {
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->awsCloudOperationsService->deleteVolumeCloudResource($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity */
    $this->entity = $entity;

    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    $this->awsCloudOperationsService->editVolume($entity, $form, $form_state);

    // Update the volume of OpenStack REST API.
    if ($this->ec2Service instanceof OpenStackRestService) {
      $this->ec2Service->updateVolume([
        'VolumeId' => $entity->getVolumeId(),
        'Name' => $entity->getName(),
        'Description' => !empty($entity->getDescription())
          ? $entity->getDescription()
          : '',
      ]);
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function attachOpenStackVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity */
    $this->entity = $entity;

    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->awsCloudOperationsService->attachVolume($entity, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function detachOpenStackVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity */
    $this->entity = $entity;

    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    $this->awsCloudOperationsService->detachVolume($entity, $form, $form_state);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function detachProcess(CloudContentEntityBase $entity, callable $process_cloud_resource, callable $process_entity): bool {
    try {

      if (!empty($entity) && $process_cloud_resource($entity)) {

        $process_entity($entity);
        $this->processOperationStatus($entity, 'detached');

        return TRUE;
      }

      $this->processOperationErrorStatus($entity, 'detached');

      return FALSE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    // Exception return type handling.
    // Return unable to process.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function detachOpenStackVolumeCloudResource(VolumeInterface $entity): bool {
    // Switch OpenStack EC2 or REST service based on $entity->getCloudContext().
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
    return $this->awsCloudOperationsService->detachVolumeCloudResource($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceById($instance_id, $module_name, $cloud_context): ?InstanceInterface {
    $instances = $this->entityTypeManager
      ->getStorage("{$module_name}_instance")
      ->loadByProperties([
        'instance_id' => $instance_id,
        'cloud_context' => $cloud_context,
      ]);
    return array_shift($instances);
  }

  /**
   * Helper method to load network by ID.
   *
   * @param string $network_id
   *   Network ID to load.
   * @param string $module_name
   *   Module name.
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return \Drupal\openstack\Entity\OpenStackNetwork
   *   The Network entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getNetworkById($network_id, $module_name, $cloud_context): OpenStackNetwork {
    $network = $this->entityTypeManager
      ->getStorage("{$module_name}_network")
      ->loadByProperties([
        'network_id' => $network_id,
        'cloud_context' => $cloud_context,
      ]);
    return array_shift($network);
  }

  /**
   * {@inheritdoc}
   */
  public function associateOpenStackFloatingIp(ElasticIpInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->entity = $entity;

      // Switch OpenStack EC2 or REST service
      // based on $entity->getCloudContext().
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

      if ($this->ec2Service instanceof OpenStackEc2Service) {
        return $this->awsCloudOperationsService->associateElasticIp($entity, $form, $form_state);
      }

      $form_state->setRedirect("entity.{$entity->getEntityTypeId()}.canonical", [
        'cloud_context' => $entity->getCloudContext(),
        "{$entity->getEntityTypeId()}" => $entity->id(),
      ]);

      // Associate a floating IP.
      $parts = explode('_', $form_state->getValue('port_id_ip'));
      $port_id = $parts[0];
      $ip = $parts[1];
      /** @var \Drupal\openstack\Entity\OpenStackFloatingIp $entity */
      $result = $this->ec2Service->associateAddress([
        'FloatingIpId' => $entity->getFloatingIpId(),
        'PortId' => $port_id,
      ]);

      if (empty($result)) {
        $this->messenger->addError($this->t('Unable to associate @label.', [
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]));
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $message = $this->t("@label @ip_address associated remotely with @private_ip", [
          '@ip_address' => $entity->getPublicIp(),
          '@private_ip' => $ip,
          '@label' => $entity->getEntityType()->getSingularLabel(),
        ]);

        $this->messenger->addStatus($message);
        return TRUE;
      }

      $message = $this->t("@label @ip_address associated with @private_ip", [
        '@ip_address' => $entity->getPublicIp(),
        '@private_ip' => $ip,
        '@label' => $entity->getEntityType()->getSingularLabel(),
      ]);

      $this->updateFloatingIpEntity($message);
      $this->clearCacheValues($entity->getCacheTags());

      // Refresh the instances.
      $this->ec2Service->updateInstances();
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'associated');
      return FALSE;
    }
  }

  /**
   * Helper function to update the current openstack_elastic_ip entity.
   *
   * @param string $message
   *   Message to display to use.
   */
  private function updateFloatingIpEntity($message): void {
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $ec2_service */
    $ec2_service = $this->ec2Service;

    // Update the following entities from OpenStack.
    $ec2_service->updateFloatingIps();
    $ec2_service->updateNetworks();
    $ec2_service->updateInstances();
    $this->messenger->addStatus($message);
  }

  /**
   * {@inheritdoc}
   */
  public function getPrivateIps(ElasticIpInterface $entity, $instance_id): array {

    // Get module name.
    $module_name = $this->getModuleName($entity);

    $instances = $this->entityTypeManager
      ->getStorage("{$module_name}_instance")
      ->loadByProperties(['id' => $instance_id]);

    /** @var \Drupal\openstack\Entity\OpenStackInstance $instance */
    $instance = count($instances) === 1
      ? array_shift($instances)
      : $instances;

    $ip_addresses = !empty($instance) && !empty($instance->getPortIpAddresses())
      ? $instance->getPortIpAddresses()
      : [];
    $private_ips = [];
    foreach ($ip_addresses ?: [] as $ip) {
      // Set Port ID for key and IP Address for value.
      $private_ips[$ip['item_key']] = $ip['item_value'];
    }

    return $private_ips;
  }

  /**
   * {@inheritdoc}
   */
  public function importOpenStackKeyPair(KeyPairInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      // Switch OpenStack EC2 or REST service
      // based on $entity->getCloudContext().
      $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());
      $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
      return $this->awsCloudOperationsService->importKeyPair($this->entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackServerGroup(OpenStackServerGroupInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $params = [
      'Name' => $entity->getName(),
      'Policy' => $entity->getPolicy(),
    ];

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->createServerGroup($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $entity->setServerGroupId($result['ServerGroup']['ServerGroupId']);
    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getServerGroupId());
    }

    // Update the Server group entity.
    $this->ec2Service->updateServerGroups([
      'ServerGroupId' => $entity->getServerGroupId(),
    ]);

    $this->processOperationStatus($entity, 'created');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackProject(OpenStackProjectInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    $domain_id = $cloud_config->get('field_domain_id')->value;
    $params = [
      'DomainId' => $domain_id,
      'Name' => $entity->getName(),
      'Description' => $entity->getDescription(),
      'Enabled' => $entity->getEnabled(),
    ];

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->createProject($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $entity->setProjectId($result['Project']['Id']);
    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getProjectId());
    }

    // Update the Project entity.
    $this->ec2Service->updateProjects([
      'ProjectId' => $entity->getProjectId(),
    ], FALSE);

    $this->processOperationStatus($entity, 'created');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackRole(OpenStackRoleInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $params = [
      'Name' => $entity->getName(),
      'Description' => $entity->getDescription(),
    ];

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->createRole($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $entity->setRoleId($result['Role']['Id']);
    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getRoleId());
    }

    // Update the Role entity.
    $this->ec2Service->updateRoles([
      'RoleId' => $entity->getRoleId(),
    ], FALSE);

    $this->processOperationStatus($entity, 'created');

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackUser(OpenStackUserInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $this->awsCloudOperationsService->trimTextfields($entity, $form, $form_state);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $params = [
      'Name' => $entity->getName(),
      'Description' => $entity->getDescription(),
      'Enabled' => $entity->getEnabled(),
    ];

    if (!empty($entity->getEmail())) {
      $params['Email'] = $entity->getEmail();
    }

    $params['Password'] = $form_state->getValue('password');

    if (!empty($default_project_id = $form_state->getValue('default_project_id'))) {
      $params['DefaultProjectId'] = $default_project_id;
    }

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->createUser($params);

    if ($result === NULL) {
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      $this->processOperationStatus($entity, 'created remotely');
      return TRUE;
    }

    $entity->setUserId($result['User']['Id']);
    $entity->save();

    if ($this->ec2Service instanceof OpenStackRestService) {
      $this->awsCloudOperationsService->updateNameAndCreatedByTags($entity, $entity->getUserId());
    }

    // Update the User entity.
    $this->ec2Service->updateUsers([
      'UserId' => $entity->getUserId(),
    ], FALSE);

    $this->processOperationStatus($entity, 'created');

    // Assigne a role.
    if (!empty($default_project_id) && !empty($role_id = $form_state->getValue('role'))) {
      $this->ec2Service->setCloudContext($entity->getCloudContext());
      $params = [
        'ProjectId' => $default_project_id,
        'RoleId' => $role_id,
        'UserId' => $entity->getUserId(),
      ];

      $result = $this->ec2Service->assignRoleToUserOnProject($params);
      if ($result === NULL) {
        $this->processOperationErrorStatus($entity, 'assigned on a project');
        return FALSE;
      }

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'assigned on a project remotely');
        return TRUE;
      }

      $this->processOperationStatus($entity, 'assigned on a project');

      // Update the Project entity.
      $this->ec2Service->updateProjects([
        'ProjectId' => $default_project_id,
      ], FALSE);
    }

    $this->clearCacheValues($entity->getCacheTags());
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackServerGroup(OpenStackServerGroupInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $this->entity = $entity;

    /** @var Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);

    $form_state->setRedirect("view.{$entity->getEntityTypeId()}.list", ['cloud_context' => $entity->getCloudContext()]);

    $this->ec2Service->setCloudContext($entity->getCloudContext());
    $result = $this->ec2Service->deleteServerGroup([
      'ServerGroupId' => $entity->getServerGroupId(),
    ]);

    if ($result === NULL) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $this->processOperationErrorStatus($entity, 'deleted');
      return FALSE;
    }

    if (!empty($result['SendToWorker'])) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $this->processOperationStatus($entity, 'deleted remotely');
      return TRUE;
    }

    $entity->delete();

    $this->messenger->addStatus($this->getDeletionMessage());
    $this->logDeletionMessage();

    $this->clearCacheValues($entity->getCacheTags());
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $this->dispatchSaveEvent($entity);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOpenStackServerGroupCloudResource(OpenStackServerGroupInterface $entity): bool {
    $this->ec2Service = $this->openStackServiceFactory->get($entity->getCloudContext());

    $this->ec2Service->setCloudContext($entity->getCloudContext());

    return $this->ec2Service->deleteServerGroup(
      ['ServerGroupId' => $entity->getServerGroupId()]
      ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function editOpenStackCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->alterCloudLaunchTemplateEditForm($entity, $form, $form_state);
      return $this->submitCloudLaunchTemplateEditForm($entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * Common alter function for edit and add forms.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  private function alterCloudLaunchTemplateCommonForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state) {
    $form['openstack_security_group_title'] = [
      '#label_display' => 'inline',
      '#type' => 'item',
      '#title' => 'Security groups',
    ];

    $this->cloudService->reorderForm($form, openstack_launch_template_field_orders());

    $route = $this->routeMatch;
    $cloud_context = $route->getParameter('cloud_context');
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_ec2_service = $this->openStackServiceFactory->isEc2ServiceType('cloud_config', $cloud_context);

    if (empty($openstack_ec2_service)) {
      $form['instance']['field_min_count']['#access'] = FALSE;
      $form['instance']['field_max_count']['#access'] = FALSE;
      $form['ami']['field_kernel_id']['#access'] = FALSE;
      $form['ami']['field_ram']['#access'] = FALSE;
      $form['network']['field_openstack_vpc']['#access'] = FALSE;
      $form['network']['field_openstack_subnet']['#access'] = FALSE;

      unset($form['options']['field_instance_shutdown_behavior']);
      unset($form['options']['field_monitoring']);
    }

    $form['field_instance_type']['widget']['#default_value'] = ['m1.nano'];
    $form['field_instance_type']['#access'] = FALSE;

    $form['network']['field_openstack_security_group']['widget']['#type'] = 'checkboxes';
    unset($form['network']['field_openstack_security_group']['widget']['#options']['_none']);

    if (!empty($openstack_ec2_service)) {
      $form['instance']['field_flavor']['widget']['#default_value'] = '1';
      $form['instance']['field_flavor']['#access'] = FALSE;

      $form['network']['field_openstack_vpc']['widget']['#ajax'] = [
        'callback' => 'openstack_ajax_callback_get_fields',
      ];

      $vpc_id = '_none';
      if (!empty($form['network']['field_openstack_vpc']['widget']['#default_value'])) {
        $vpc_id = $form['network']['field_openstack_vpc']['widget']['#default_value'][0];
      }

      // If validation happened, we should get vpc_id from user input.
      $user_input = $form_state->getUserInput();
      if (!empty($user_input['field_openstack_vpc'])) {
        $vpc_id = $user_input['field_openstack_vpc'];
      }

      /** @var \Drupal\Core\Entity\EntityFormInterface $form_object */
      $form_object = $form_state->getFormObject();
      $server_template = $entity->createDuplicate();
      $form_object->setEntity($server_template);
      $subnet_options = openstack_get_subnet_options_by_vpc_id($vpc_id, $form_object->getEntity());
      $form['#attached']['drupalSettings']['openstack']['field_openstack_subnet_default_values']
        = array_keys($subnet_options);

      $security_group_options = openstack_get_security_group_options_by_vpc_id($vpc_id);
      $security_group_default_values = [];
      foreach ($security_group_options ?: [] as $id => $security_group_option) {
        $security_group_default_values[] = (string) $id;
      }

      $form['#attached']['drupalSettings']['openstack']['field_openstack_security_group_default_values']
        = $security_group_default_values;
    }

    $form['#attached']['library'][] = 'openstack/openstack_form';

    // Hide labels of field_tags.
    $form['tags']['field_tags']['widget']['#title'] = NULL;
    if (empty($openstack_ec2_service)) {
      $form['#validate'][] = 'openstack_form_cloud_launch_template_form_validate';
    }
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  private function alterCloudLaunchTemplateAddForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state) {
    $this->alterCloudLaunchTemplateCommonForm($entity, $form, $form_state);

    $cloud_context = $entity->getCloudContext();

    $openstack_service = $this->openStackServiceFactory->isEc2ServiceType('cloud_config', $cloud_context);
    if (empty($openstack_service)) {
      // Overwrite function ::save.
      $form['actions']['submit']['#submit'][1] = 'openstack_form_cloud_launch_template_openstack_add_form_submit';
    }
  }

  /**
   * Submit function for form cloud_launch_template_openstack_add_form.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  private function submitCloudLaunchTemplateAddForm(CloudLaunchTemplateInterface $entity, array $form, FormStateInterface $form_state): void {
    $cloud_context = $entity->getCloudContext();

    $form_values = $form_state->getValues();
    $uid = $form_values['uid'][0]['target_id'] ??
      $entity->get('uid')->getValue()[0]['target_id'] ?? 0;
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface $openstack_service */
    $openstack_service = $this->openStackServiceFactory->get($cloud_context);

    $uid_key_name = $openstack_service->getTagKeyCreatedByUid(
      'openstack',
      $cloud_context,
      $uid
    );

    // Create uid tag automatically.
    $tags = [
      [
        'item_key' => $uid_key_name,
        'item_value' => $uid,
        'options' => [],
      ],
    ];

    $entity->set('field_tags', $tags);
    $entity->save();

    $this->processOperationStatus($entity, 'created');

    $form_state->setRedirect(
      'entity.cloud_launch_template.canonical',
      [
        'cloud_context' => $cloud_context,
        'cloud_launch_template' => $entity->id(),
      ]
    );
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  private function alterCloudLaunchTemplateEditForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state) {
    $this->alterCloudLaunchTemplateCommonForm($entity, $form, $form_state);

    // Hide new revision checkbox.
    $form['new_revision']['#access'] = FALSE;

    // Disable name field.
    $form['instance']['name']['#disabled'] = TRUE;

    $cloud_context = $entity->getCloudContext();

    $openstack_service = $this->openStackServiceFactory->isEc2ServiceType('cloud_config', $cloud_context);
    if (empty($openstack_service)) {
      // Overwrite function ::save.
      $form['actions']['submit']['#submit'][1] = 'openstack_form_cloud_launch_template_openstack_edit_form_submit';
    }
  }

  /**
   * Submit function for form cloud_launch_template_aws_cloud_edit_form.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  private function submitCloudLaunchTemplateEditForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    $cloud_context = $entity->getCloudContext();

    $form_values = $form_state->getValues();
    $uid = $form_values['uid'][0]['target_id'] ?? 0;
    /** @var \Drupal\openstack\Service\OpenStackServiceInterface|\Drupal\cloud\Service\CloudResourceTagInterface $openstack_service */
    $openstack_service = $this->openStackServiceFactory->get($cloud_context);
    $uid_key_name = $openstack_service->getTagKeyCreatedByUid(
      'openstack',
      $cloud_context,
      intval($uid)
    );

    $form_tags = ($entity->hasField('field_tags')
      && !$entity->get('field_tags')->isEmpty())
      ? $entity->get('field_tags')->getValue() : [];

    // Update tags if authored id is changed.
    foreach ($form_tags ?: [] as $number => $form_tag) {
      if ($form_tag['item_key'] === $uid_key_name) {
        $form_tags[$number]['item_value'] = $uid;
      }
    }

    $entity->set('field_tags', $form_tags);
    $entity->save();

    $this->processOperationStatus($entity, 'updated');

    $form_state->setRedirect(
      'entity.cloud_launch_template.canonical',
      [
        'cloud_context' => $cloud_context,
        'cloud_launch_template' => $entity->id(),
      ]
    );
    return TRUE;
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  private function alterCloudLaunchTemplateCopyForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state) {
    $this->alterCloudLaunchTemplateCommonForm($entity, $form, $form_state);

    // Change name for copy.
    $name = $form['instance']['name']['widget'][0]['value']['#default_value'];
    $form['instance']['name']['widget'][0]['value']['#default_value'] = $this->t('copy_of_@name',
      [
        '@name' => $name,
      ]);

    // Hide new revision checkbox.
    $form['new_revision']['#access'] = FALSE;

    // Clear the revision log message.
    $form['others']['revision_log_message']['widget'][0]['value']['#default_value'] = NULL;

    // Change value of the submit button.
    $form['actions']['submit']['#value'] = $this->t('Copy');

    // Delete the delete button.
    $form['actions']['delete']['#access'] = FALSE;

    // Overwrite function ::save.
    $form['actions']['submit']['#submit'][1] = 'openstack_form_cloud_launch_template_openstack_copy_form_submit';
  }

  /**
   * Submit function for form cloud_launch_template_openstack_copy_form.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  private function submitCloudLaunchTemplateCopyForm(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    /** @var \Drupal\Core\Entity\EntityFormInterface $form_object */
    $form_object = $form_state->getFormObject();
    $cloud_context = $entity->getCloudContext();

    $server_template = $entity->createDuplicate();
    if (!empty($form_object)) {
      $form_object->setEntity($server_template);
    }

    $is_ec2_service = $this->openStackServiceFactory->isEc2ServiceType('cloud_config', $cloud_context);
    if (empty($is_ec2_service)) {
      $form_values = $form_state->getValues();
      $uid = $form_values['uid'][0]['target_id'] ?? 0;
      /** @var \Drupal\openstack\Service\OpenStackServiceInterface|\Drupal\cloud\Service\CloudResourceTagInterface $openstack_service */
      $openstack_service = $this->openStackServiceFactory->get($cloud_context);
      $uid_key_name = $openstack_service->getTagKeyCreatedByUid(
        'openstack',
        $cloud_context,
        $uid
      );

      $form_tags = ($server_template->hasField('field_tags') && !$server_template->get('field_tags')->isEmpty())
        ? $server_template->get('field_tags')->getValue() : [];

      // Update tags if authored id is changed.
      $tags = [];
      foreach ($form_tags ?: [] as $form_tag) {
        $item_key = $form_tag['item_key'];
        if ($form_tag['item_key'] === $uid_key_name) {
          $tags[] = ['item_key' => $item_key, 'item_value' => $uid];
        }
      }

      $server_template->set('field_tags', $tags);
    }
    $server_template->save();

    $this->processOperationStatus($server_template, 'created');

    $form_state->setRedirect(
      'entity.cloud_launch_template.canonical',
      [
        'cloud_context' => $cloud_context,
        'cloud_launch_template' => $server_template->id(),
      ]
    );

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->alterCloudLaunchTemplateAddForm($entity, $form, $form_state);
      $this->submitCloudLaunchTemplateAddForm($entity, $form, $form_state);
      return $this->awsCloudOperationsService->createCloudLaunchTemplate($entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'created');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function copyOpenStackCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $this->alterCloudLaunchTemplateCopyForm($entity, $form, $form_state);
      return $this->submitCloudLaunchTemplateCopyForm($entity, $form, $form_state);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'edited');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function reviewOpenStackCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool {
    try {
      $form_state->cleanValues();
      // Update the changed timestamp of the entity.
      if ($entity instanceof EntityChangedInterface) {
        $entity->setChangedTime($this->time->getRequestTime());
      }

      // Launch the instance here.
      $redirect_route = [
        'route_name' => 'entity.cloud_launch_template.canonical',
        'params' => [
          'cloud_launch_template' => $entity->id(),
          'cloud_context' => $entity->getCloudContext(),
        ],
      ];
      // Let other modules alter the redirect after a cloud launch template has
      // been launched.
      $this->moduleHandler->invokeAll('cloud_launch_template_post_launch_redirect_alter', [
        &$redirect_route,
        $entity,
      ]);
      $form_state->setRedirectUrl(new Url($redirect_route['route_name'], $redirect_route['params']));

      // Clear block and menu cache.
      $this->clearCacheValues($entity->getCacheTags());
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      $this->processOperationErrorStatus($entity, 'changed');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConsoleOutput(InstanceInterface $entity, array &$form, FormStateInterface $form_state): string {
    /** @var \Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->ec2Service->getConsoleOutput([
      'InstanceId' => $entity->getInstanceId(),
      // Default to 150 lines if no options are passed.
      'lines' => empty($form_state->getValue('lines')) ? 150 : $form_state->getValue('lines'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConsole(InstanceInterface $entity, array &$form, FormStateInterface $form_state): array {
    /** @var \Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->ec2Service->getConsole([
      'InstanceId' => $entity->getInstanceId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getActionLog(InstanceInterface $entity, array &$form, FormStateInterface $form_state): array {
    /** @var \Drupal\openstack\Service\Rest\OpenStackService $ec2_service */
    $ec2_service = $this->openStackServiceFactory->get($entity->getCloudContext());
    $this->ec2Service = $ec2_service;
    $this->awsCloudOperationsService->setEc2Service($this->ec2Service);
    return $this->ec2Service->getActionLog([
      'InstanceId' => $entity->getInstanceId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getAssociatedPortOptions(string $cloud_context): array {
    $instance_map = [];
    $instances = $this->entityTypeManager
      ->getStorage('openstack_instance')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);
    foreach ($instances ?: [] as $instance) {
      $instance_map[$instance->getInstanceId()] = $instance->getName();
    }

    $options = [];
    $ports = $this->entityTypeManager
      ->getStorage('openstack_port')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    // Get reachable subnets.
    $reachable_subnets = $this->getReachableSubnets($ports, $cloud_context);
    foreach ($ports ?: [] as $port) {

      if (empty($port)) {
        continue;
      }

      if (str_starts_with($port->getDeviceOwner() ?? '', 'network:')) {
        continue;
      }

      $instance_name = '';
      if (!empty($port->getDeviceId())) {
        if (!array_key_exists($port->getDeviceId() ?? '', $instance_map)) {
          continue;
        }
        $instance_name = $instance_map[$port->getDeviceId()] ?? '';
      }

      foreach ($port->getFixedIps() ?: [] as $fixed_ip) {
        $parts = explode(',', $fixed_ip['value']);
        $ip = $parts[0];
        $subnet_id = $parts[1];

        // Check if the IP address is IPv4.
        if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
          continue;
        }

        // Check if subnet is reachable network.
        if (!in_array($subnet_id, $reachable_subnets)) {
          continue;
        }
        $port_name = !empty($port->getName()) ? "{$port->getName()} ($ip)" : $ip;
        $instance_name = !empty($instance_name) ? " | $instance_name" : '';
        $options["{$port->getPortId()}_{$ip}"] = $port_name . $instance_name;
      }
    }

    // Sort by value.
    uasort($options, static function ($a, $b) {
      return $a < $b ? -1 : 1;
    });

    return $options;
  }

  /**
   * Get reachable subnet IDs.
   *
   * @param array $ports
   *   Array of port entity.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return array
   *   The IDs of reachable subnets.
   */
  private function getReachableSubnets(array $ports, $cloud_context): array {
    $ext_networks = $this->entityTypeManager
      ->getStorage('openstack_network')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
        'external' => TRUE,
      ]);

    $ext_network_ids = array_map(static function ($item) {
      return $item->getNetworkId();
    }, $ext_networks);

    $router_ports = array_filter($ports, static function ($port) {
      return in_array($port->getDeviceOwner(), OpenStackServiceInterface::ROUTER_INTERFACE_OWNERS);
    });

    $gw_routers = $this->entityTypeManager
      ->getStorage('openstack_router')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
        'external_gateway_network_id' => $ext_network_ids,
      ]);
    $gw_router_ids = array_map(static function ($item) {
      return $item->getRouterId();
    }, $gw_routers);

    $reachable_subnet_ids = [];
    foreach ($router_ports ?: [] as $port) {
      if (!in_array($port->getDeviceId(), $gw_router_ids)) {
        continue;
      }

      $parts = explode(',', $port->getFixedIps()[0]['value']);
      $reachable_subnet_ids[] = $parts[1];
    }

    $shared_networks = $this->entityTypeManager
      ->getStorage('openstack_network')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
        'shared' => TRUE,
      ]);

    $shared_network_ids = array_map(static function ($item) {
      return $item->getNetworkId();
    }, $shared_networks);

    $shared_subnets = $this->entityTypeManager
      ->getStorage('openstack_subnet')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
        'network_id' => $shared_network_ids,
      ]);

    $shared_subnet_ids = array_map(static function ($item) {
      return $item->getSubnetId();
    }, $shared_subnets);

    return array_merge($reachable_subnet_ids, $shared_subnet_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalGatewayNetworkOptions(string $cloud_context): array {
    $options = [];
    $networks = $this->entityTypeManager
      ->getStorage('openstack_network')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
        'external' => TRUE,
      ]);

    foreach ($networks ?: [] as $network) {
      $options[$network->getNetworkId()] = $network->getName();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachNetworkOptions(string $cloud_context): array {
    $options = [];
    /** @var \Drupal\openstack\Entity\OpenStackNetworkInterface[] $networks */
    $networks = $this->entityTypeManager
      ->getStorage('openstack_network')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    if (empty($networks)) {
      return [];
    }

    foreach ($networks as $network) {
      if (empty($network->getSubnets())) {
        continue;
      }

      $options[$network->getNetworkId()] = $this->t('@network_name (@cidr)',
        [
          '@network_name' => $network->getName(),
          '@cidr' => $this->getSubnetCidr($network, $cloud_context),
        ]);
    }

    asort($options);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachPortOptions(string $cloud_context): array {
    $options = [];
    /** @var \Drupal\openstack\Entity\OpenStackPort[] $ports */
    $ports = $this->entityTypeManager
      ->getStorage('openstack_port')
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    if (empty($ports)) {
      return [];
    }

    foreach ($ports ?: [] as $port) {
      if (!empty($port->getDeviceOwner())) {
        continue;
      }

      $network = $this->getNetworkById($port->getNetworkId(), 'openstack', $cloud_context);
      if (empty($network)) {
        continue;
      }

      $ips = [];
      foreach ($port->getFixedIps() ?: [] as $fixed_ip) {
        $parts = explode(',', $fixed_ip['value']);
        if (count($parts) < 2) {
          continue;
        }

        $ips[] = $parts[0];
      }

      $options[$port->getPortId()] = $this->t('@port_name (@ips) - @network_name', [
        '@port_name' => $port->getName(),
        '@ips' => implode(',', $ips),
        '@network_name' => $network->getName(),
      ]);
    }

    asort($options);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageVisibilityOptions(bool $is_shareable): array {
    // If user does not have permission to share openstack images,
    // remove the shared visibility option.
    return !empty($is_shareable)
      ? OpenStackImageInterface::IMAGE_VISIBILITY
      : array_filter(OpenStackImageInterface::IMAGE_VISIBILITY,
        static fn ($key) => $key !== OpenStackImageInterface::VISIBILITY_SHARED,
        ARRAY_FILTER_USE_KEY);
  }

  /**
   * Helper to get subnet cidr given an OpenStack network.
   *
   * @param \Drupal\openstack\Entity\OpenStackNetwork $network
   *   OpenStackNetwork entity.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return string
   *   Comma delimited CIDR string.
   */
  private function getSubnetCidr(OpenStackNetwork $network, string $cloud_context): string {
    $subnets = array_merge(
      $this->getSubnet($network->getNetworkId(), 'IPv4', $cloud_context),
      $this->getSubnet($network->getNetworkId(), 'IPv6', $cloud_context)
    );

    $cidrs = array_map(static function ($subnet) {
      return $subnet->getCidr();
    }, $subnets);

    return implode(', ', $cidrs);
  }

  /**
   * Helper to get OpenStack subnets from the database.
   *
   * @param string $network_id
   *   Network id to retrieve.
   * @param string $ip_version
   *   Optionally, retrieve the IPv4 or IPv6 subnet.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return array
   *   Array of OpenStackSubnet entities.
   */
  private function getSubnet(string $network_id, string $ip_version = '', string $cloud_context = ''): array {
    $params = [
      'cloud_context' => $cloud_context,
      'network_id' => $network_id,
    ];

    $ip_versions = [
      'IPv4',
      'IPv6',
    ];

    // Only add ip_version if it matches IPv4 or IPv6.
    if (!empty($ip_version) && in_array($ip_version, $ip_versions)) {
      $params['ip_version'] = $ip_version;
    }
    return $this->entityTypeManager
      ->getStorage('openstack_subnet')
      ->loadByProperties($params);
  }

  /**
   * {@inheritdoc}
   */
  public function updateFloatingIps(): bool {
    return $this->ec2Service->updateFloatingIps();
  }

  /**
   * {@inheritdoc}
   */
  public function updateInstances(array $params = [], $clear = TRUE): bool {
    return $this->ec2Service->updateInstances($params, $clear);
  }

  /**
   * {@inheritdoc}
   */
  public function updateNetworks(array $params = [], $clear = TRUE): bool {
    return $this->ec2Service->updateNetworks($params, $clear);
  }

  /**
   * {@inheritdoc}
   */
  public function updateVolumes(array $params = [], $clear = TRUE): bool {
    return $this->ec2Service->updateVolumes($params, $clear);
  }

}
