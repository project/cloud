<?php

namespace Drupal\openstack\Service;

use Drupal\Core\Form\FormStateInterface;
use Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface;
use Drupal\aws_cloud\Entity\Ec2\InstanceInterface;
use Drupal\aws_cloud\Entity\Ec2\KeyPairInterface;
use Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface;
use Drupal\aws_cloud\Entity\Ec2\SnapshotInterface;
use Drupal\aws_cloud\Entity\Ec2\VolumeInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\openstack\Entity\OpenStackFloatingIpInterface;
use Drupal\openstack\Entity\OpenStackImageInterface;
use Drupal\openstack\Entity\OpenStackNetworkInterface;
use Drupal\openstack\Entity\OpenStackPortInterface;
use Drupal\openstack\Entity\OpenStackProjectInterface;
use Drupal\openstack\Entity\OpenStackQuotaInterface;
use Drupal\openstack\Entity\OpenStackRoleInterface;
use Drupal\openstack\Entity\OpenStackRouterInterface;
use Drupal\openstack\Entity\OpenStackServerGroupInterface;
use Drupal\openstack\Entity\OpenStackStackInterface;
use Drupal\openstack\Entity\OpenStackSubnetInterface;
use Drupal\openstack\Entity\OpenStackUserInterface;

/**
 * Provides OpenStackOperationsService interface.
 */
interface OpenStackOperationsServiceInterface {

  /**
   * Delete an OpenStack instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackInstance(InstanceInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Process an entity and related OpenStack resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   * @param callable $process_cloud_resource
   *   The process cloud resource callback.
   * @param callbable $process_entity
   *   The process entity callback.
   *
   * @return bool
   *   Succeeded or failed.
   */
  public function deleteProcess(CloudContentEntityBase $entity, callable $process_cloud_resource, callable $process_entity): bool;

  /**
   * The implementation function to Delete an OpenStack instances.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackInstanceCloudResource(InstanceInterface $entity): bool;

  /**
   * Get console output an OpenStack instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return string
   *   Console output.
   */
  public function getConsoleOutput(InstanceInterface $entity, array &$form, FormStateInterface $form_state): string;

  /**
   * Get console for an OpenStack instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The console.
   */
  public function getConsole(InstanceInterface $entity, array &$form, FormStateInterface $form_state): array;

  /**
   * Get action log of an OpenStack instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   Action log.
   */
  public function getActionLog(InstanceInterface $entity, array &$form, FormStateInterface $form_state): array;

  /**
   * Create an OpenStack image.
   *
   * @param \Drupal\openstack\Entity\OpenStackImageInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackImage(OpenStackImageInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create an image from an instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackImageFromInstance(InstanceInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Start an OpenStack instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function startOpenStackInstance(InstanceInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Process an entity and related OpenStack resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   * @param callable $process_cloud_resource
   *   The process cloud resource callback.
   * @param callbable $process_entity
   *   The process entity callback.
   *
   * @return bool
   *   Succeeded or failed.
   */
  public function startProcess(CloudContentEntityBase $entity, callable $process_cloud_resource, callable $process_entity): bool;

  /**
   * The implementation function to start OpenStack instances.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function startOpenStackInstanceCloudResource(InstanceInterface $entity): bool;

  /**
   * Stop an OpenStack instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function stopOpenStackInstance(InstanceInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Process an entity and related OpenStack resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   * @param callable $process_cloud_resource
   *   The process cloud resource callback.
   * @param callbable $process_entity
   *   The process entity callback.
   *
   * @return bool
   *   Succeeded or failed.
   */
  public function stopProcess(CloudContentEntityBase $entity, callable $process_cloud_resource, callable $process_entity): bool;

  /**
   * The implementation function to stop OpenStack instances.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function stopOpenStackInstanceCloudResource(InstanceInterface $entity): bool;

  /**
   * Reboot an OpenStack instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function rebootOpenStackInstance(InstanceInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Process an entity and related OpenStack resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   * @param callable $process_cloud_resource
   *   The process cloud resource callback.
   * @param callbable $process_entity
   *   The process entity callback.
   *
   * @return bool
   *   Succeeded or failed.
   */
  public function rebootProcess(CloudContentEntityBase $entity, callable $process_cloud_resource, callable $process_entity): bool;

  /**
   * The implementation function to reboot OpenStack instances.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The OpenStack instance entity.
   * @param bool $reboot_type
   *   The reboot type.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function rebootOpenStackInstanceCloudResource(InstanceInterface $entity, bool $reboot_type): bool;

  /**
   * Edit an OpenStack image.
   *
   * @param \Drupal\openstack\Entity\OpenStackImageInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackImage(OpenStackImageInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Validate an OpenStack image form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function validateOpenStackImageForm(FormStateInterface $form_state): void;

  /**
   * Delete an OpenStack image.
   *
   * @param \Drupal\openstack\Entity\OpenStackImageInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackImage(OpenStackImageInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack images.
   *
   * @param \Drupal\openstack\Entity\OpenStackImageInterface $entity
   *   The OpenStack image entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackImageCloudResource(OpenStackImageInterface $entity): bool;

  /**
   * Create an OpenStack key pair.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity
   *   The OpenStack key pair entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackKeyPair(KeyPairInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack key pair.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity
   *   The OpenStack key pair entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackKeyPair(KeyPairInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack key pair.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity
   *   The OpenStack key pair entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackKeyPair(KeyPairInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack key pairs.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity
   *   The OpenStack key pair entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackKeyPairCloudResource(KeyPairInterface $entity): bool;

  /**
   * Create an OpenStack network.
   *
   * @param \Drupal\openstack\Entity\OpenStackNetworkInterface $entity
   *   The OpenStack network entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackNetwork(OpenStackNetworkInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create an OpenStack subnet.
   *
   * @param \Drupal\openstack\Entity\OpenStackSubnetInterface $entity
   *   The OpenStack network entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackSubnet(OpenStackSubnetInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create an OpenStack port.
   *
   * @param \Drupal\openstack\Entity\OpenStackPortInterface $entity
   *   The OpenStack network entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackPort(OpenStackPortInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create an OpenStack router.
   *
   * @param \Drupal\openstack\Entity\OpenStackRouterInterface $entity
   *   The OpenStack network entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackRouter(OpenStackRouterInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Add an interface to OpenStack router.
   *
   * @param \Drupal\openstack\Entity\OpenStackRouterInterface $entity
   *   The OpenStack network entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function addOpenStackRouterInterface(OpenStackRouterInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Remove an interface to OpenStack router.
   *
   * @param \Drupal\openstack\Entity\OpenStackRouterInterface $entity
   *   The OpenStack network entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function removeOpenStackRouterInterface(OpenStackRouterInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackInstance(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Attach a network interface to an OpenStack instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function attachOpenStackNetworkInterface(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Detach a network interface to an OpenStack instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $entity
   *   The instance entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function detachOpenStackNetworkInterface(InstanceInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack network.
   *
   * @param \Drupal\openstack\Entity\OpenStackNetworkInterface $entity
   *   The OpenStack network entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackNetwork(OpenStackNetworkInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack subnet.
   *
   * @param \Drupal\openstack\Entity\OpenStackSubnetInterface $entity
   *   The OpenStack subnet entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackSubnet(OpenStackSubnetInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack port.
   *
   * @param \Drupal\openstack\Entity\OpenStackPortInterface $entity
   *   The OpenStack port entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackPort(OpenStackPortInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack router.
   *
   * @param \Drupal\openstack\Entity\OpenStackRouterInterface $entity
   *   The OpenStack router entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackRouter(OpenStackRouterInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack quota.
   *
   * @param \Drupal\openstack\Entity\OpenStackQuotaInterface $entity
   *   The OpenStack quota entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackQuota(OpenStackQuotaInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack network.
   *
   * @param \Drupal\openstack\Entity\OpenStackNetworkInterface $entity
   *   The OpenStack network interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackNetwork(OpenStackNetworkInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack networks.
   *
   * @param \Drupal\openstack\Entity\OpenStackNetworkInterface $entity
   *   The OpenStack network entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackNetworkCloudResource(OpenStackNetworkInterface $entity): bool;

  /**
   * Delete an OpenStack subnet.
   *
   * @param \Drupal\openstack\Entity\OpenStackSubnetInterface $entity
   *   The OpenStack subnet interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackSubnet(OpenStackSubnetInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack port.
   *
   * @param \Drupal\openstack\Entity\OpenStackPortInterface $entity
   *   The OpenStack port entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackPort(OpenStackPortInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack ports.
   *
   * @param \Drupal\openstack\Entity\OpenStackPortInterface $entity
   *   The OpenStack port entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackPortCloudResource(OpenStackPortInterface $entity): bool;

  /**
   * Delete an OpenStack router.
   *
   * @param \Drupal\openstack\Entity\OpenStackRouterInterface $entity
   *   The OpenStack router interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackRouter(OpenStackRouterInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create an OpenStack Project.
   *
   * @param \Drupal\openstack\Entity\OpenStackProjectInterface $entity
   *   The OpenStack project interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackProject(OpenStackProjectInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack Project.
   *
   * @param \Drupal\openstack\Entity\OpenStackProjectInterface $entity
   *   The OpenStack project entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackProject(OpenStackProjectInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack Project.
   *
   * @param \Drupal\openstack\Entity\OpenStackProjectInterface $entity
   *   The OpenStack project entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackProject(OpenStackProjectInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack projects.
   *
   * @param \Drupal\openstack\Entity\OpenStackProjectInterface $entity
   *   The OpenStack project entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackProjectCloudResource(OpenStackProjectInterface $entity): bool;

  /**
   * Create an OpenStack Role.
   *
   * @param \Drupal\openstack\Entity\OpenStackRoleInterface $entity
   *   The OpenStack role interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackRole(OpenStackRoleInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack Role.
   *
   * @param \Drupal\openstack\Entity\OpenStackRoleInterface $entity
   *   The OpenStack role entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackRole(OpenStackRoleInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack Role.
   *
   * @param \Drupal\openstack\Entity\OpenStackRoleInterface $entity
   *   The OpenStack role entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackRole(OpenStackRoleInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack roles.
   *
   * @param \Drupal\openstack\Entity\OpenStackRoleInterface $entity
   *   The OpenStack role entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackRoleCloudResource(OpenStackRoleInterface $entity): bool;

  /**
   * Create an OpenStack User.
   *
   * @param \Drupal\openstack\Entity\OpenStackUserInterface $entity
   *   The OpenStack user interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackUser(OpenStackUserInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack User.
   *
   * @param \Drupal\openstack\Entity\OpenStackUserInterface $entity
   *   The OpenStack user entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackUser(OpenStackUserInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Change the password an OpenStack User.
   *
   * @param \Drupal\openstack\Entity\OpenStackUserInterface $entity
   *   The OpenStack user entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function changePasswordOpenStackUser(OpenStackUserInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack User.
   *
   * @param \Drupal\openstack\Entity\OpenStackUserInterface $entity
   *   The OpenStack user entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackUser(OpenStackUserInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack users.
   *
   * @param \Drupal\openstack\Entity\OpenStackUserInterface $entity
   *   The OpenStack user entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackUserCloudResource(OpenStackUserInterface $entity): bool;

  /**
   * Create an OpenStack Stack.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param array $extra_parameters
   *   The extra parameters.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state, array $extra_parameters): bool;

  /**
   * Update an OpenStack Stack.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param array $extra_parameters
   *   The extra parameters.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function updateOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state, array $extra_parameters): bool;

  /**
   * Preview an OpenStack Stack.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param array $extra_parameters
   *   The extra parameters.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function previewOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state, array $extra_parameters): bool;

  /**
   * Delete an OpenStack Stack.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack stacks.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackStackCloudResource(OpenStackStackInterface $entity): bool;

  /**
   * Check an OpenStack Stack.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function checkOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to check OpenStack stacks.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function checkOpenStackStackCloudResource(OpenStackStackInterface $entity): bool;

  /**
   * Suspend an OpenStack Stack.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function suspendOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to suspend OpenStack stacks.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function suspendOpenStackStackCloudResource(OpenStackStackInterface $entity): bool;

  /**
   * Resume an OpenStack Stack.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack interface entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function resumeOpenStackStack(OpenStackStackInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to resume OpenStack stacks.
   *
   * @param \Drupal\openstack\Entity\OpenStackStackInterface $entity
   *   The OpenStack stack entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function resumeOpenStackStackCloudResource(OpenStackStackInterface $entity): bool;

  /**
   * Create an OpenStack floating IP.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The OpenStack floating IP entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackFloatingIp(ElasticIpInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack floating IP.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackFloatingIp(ElasticIpInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack floating IP.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The elastic ip entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackFloatingIp(ElasticIpInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack security group.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The elastic ip entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackFloatingIpCloudResource(ElasticIpInterface $entity): bool;

  /**
   * Disassociate an OpenStack floating IP.
   *
   * @param \Drupal\openstack\Entity\OpenStackFloatingIpInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function disassociateOpenStackFloatingIp(OpenStackFloatingIpInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create an OpenStack SecurityGroup.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The OpenStack security group entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackSecurityGroup(SecurityGroupInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack SecurityGroup.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The OpenStack security group entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackSecurityGroup(SecurityGroupInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack security group.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The OpenStack security group entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackSecurityGroupCloudResource(SecurityGroupInterface $entity): bool;

  /**
   * Revoke an OpenStack SecurityGroup.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The OpenStack security group entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function revokeOpenStackSecurityGroup(SecurityGroupInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack SecurityGroup.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SecurityGroupInterface $entity
   *   The OpenStack security group entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackSecurityGroup(SecurityGroupInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Create an OpenStack snapshot.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackSnapshot(SnapshotInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack snapshot.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity
   *   The OpenStack snapshot entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackSnapshot(SnapshotInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack snapshots.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity
   *   The OpenStack snapshot entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackSnapshotCloudResource(SnapshotInterface $entity): bool;

  /**
   * Associate an OpenStack floating IP.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The OpenStack floating IP entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function associateOpenStackFloatingIp(ElasticIpInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Helper function that loads all the private IPs for an instance.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\ElasticIpInterface $entity
   *   The OpenStack floating IP entity.
   * @param int $instance_id
   *   The instance ID.
   *
   * @return array
   *   An array of IP addresses.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPrivateIps(ElasticIpInterface $entity, $instance_id): array;

  /**
   * Edit an OpenStack snapshot.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\SnapshotInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackSnapshot(SnapshotInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Create an OpenStack volume.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack volume.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The OpenStack volume entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack volumes.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The OpenStack volume entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackVolumeCloudResource(VolumeInterface $entity): bool;

  /**
   * Edit an OpenStack volume.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Import an OpenStack key pair.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\KeyPairInterface $entity
   *   The openStack key pair entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE when the process succeeds.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function importOpenStackKeyPair(KeyPairInterface $entity, array $form, FormStateInterface $form_state): bool;

  /**
   * Attach an OpenStack volume.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function attachOpenStackVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Detach an OpenStack volume.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The OpenStack image entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function detachOpenStackVolume(VolumeInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Process an entity and related OpenStack resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   * @param callable $process_cloud_resource
   *   The process cloud resource callback.
   * @param callbable $process_entity
   *   The process entity callback.
   *
   * @return bool
   *   Succeeded or failed.
   */
  public function detachProcess(CloudContentEntityBase $entity, callable $process_cloud_resource, callable $process_entity): bool;

  /**
   * The implementation function to detach an OpenStack volumes.
   *
   * @param \Drupal\aws_cloud\Entity\Ec2\VolumeInterface $entity
   *   The OpenStack volume entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function detachOpenStackVolumeCloudResource(VolumeInterface $entity): bool;

  /**
   * Create an OpenStack server group.
   *
   * @param \Drupal\openstack\Entity\OpenStackServerGroupInterface $entity
   *   The OpenStack key pair entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackServerGroup(OpenStackServerGroupInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Delete an OpenStack server group.
   *
   * @param Drupal\openstack\Entity\OpenStackServerGroupInterface $entity
   *   The OpenStack server group entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackServerGroup(OpenStackServerGroupInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * The implementation function to Delete an OpenStack server groups.
   *
   * @param Drupal\openstack\Entity\OpenStackServerGroupInterface $entity
   *   The OpenStack server group entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteOpenStackServerGroupCloudResource(OpenStackServerGroupInterface $entity): bool;

  /**
   * Create an OpenStack CloudLaunchTemplate.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createOpenStackCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Copy an OpenStack CloudLaunchTemplate.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function copyOpenStackCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Edit an OpenStack CloudLaunchTemplate.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editOpenStackCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Review OpenStack CloudLaunchTemplate.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The CloudLaunchTemplate entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function reviewOpenStackCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array &$form, FormStateInterface $form_state): bool;

  /**
   * Get the ports that can be associated with the OpenStack floating IP.
   *
   * @param string $cloud_context
   *   Cloud context to use in the query.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getAssociatedPortOptions(string $cloud_context): array;

  /**
   * Get options of external gateway network.
   *
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return array
   *   The options of network.
   */
  public function getExternalGatewayNetworkOptions(string $cloud_context): array;

  /**
   * Get options of attach network.
   *
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return array
   *   The options of network.
   */
  public function getAttachNetworkOptions(string $cloud_context): array;

  /**
   * Get options of attach port.
   *
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return array
   *   The options of port.
   */
  public function getAttachPortOptions(string $cloud_context): array;

  /**
   * Get options of image visibility.
   *
   * @param bool $is_shareable
   *   Whether Image can be shared or not.
   *
   * @return array
   *   The options of image visibility.
   */
  public function getImageVisibilityOptions(bool $is_shareable): array;

  /**
   * Helper method to load instance by ID.
   *
   * @param string $instance_id
   *   Instance ID to load.
   * @param string $module_name
   *   Module name.
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return \Drupal\aws_cloud\Entity\Ec2\InstanceInterface
   *   The instance entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getInstanceById($instance_id, $module_name, $cloud_context): ?InstanceInterface;

  /**
   * Update the OpenStackFloatingIps.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateFloatingIps(): bool;

  /**
   * Update the instances.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param bool $clear
   *   TRUE to clear stale instances.
   *
   * @return bool
   *   Indicates success or failure.
   */
  public function updateInstances(array $params = [], $clear = TRUE): bool;

  /**
   * Update the OpenStackNetworks.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale networks.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateNetworks(array $params = [], $clear = TRUE): bool;

  /**
   * Update the OpenStackVolumes.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale volumes.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateVolumes(array $params = [], $clear = TRUE): bool;

}
