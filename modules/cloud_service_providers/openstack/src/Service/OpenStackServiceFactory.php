<?php

namespace Drupal\openstack\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudCacheServiceInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\openstack\Service\Ec2\OpenStackService as OpenStackEc2Service;
use Drupal\openstack\Service\Rest\OpenStackService as OpenStackRestService;
use Drupal\openstack\Service\Rest\OpenStackServiceMock;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * OpenStackService service interacts with the OpenStack API.
 */
class OpenStackServiceFactory implements OpenStackServiceFactoryInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The lock interface.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Field type plugin manager.
   *
   * @var \Drupal\core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Entity field manager interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Guzzle Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routeMatch;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The Cloud cache service.
   *
   * @var \Drupal\cloud\Service\CloudCacheServiceInterface
   */
  protected $cloudCacheService;

  /**
   * The Twig service.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twig;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new OpenStackServiceFactory object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock interface.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\core\Field\FieldTypePluginManagerInterface $field_type_plugin_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\Core\Template\TwigEnvironment $twig
   *   The twig service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\cloud\Service\CloudCacheServiceInterface $cloud_cache_service
   *   The Cloud cache service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    LockBackendInterface $lock,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    FieldTypePluginManagerInterface $field_type_plugin_manager,
    EntityFieldManagerInterface $entity_field_manager,
    QueueFactory $queue_factory,
    ClientInterface $http_client,
    ModuleHandlerInterface $module_handler,
    RouteMatchInterface $route_match,
    CloudServiceInterface $cloud_service,
    TwigEnvironment $twig,
    FileSystemInterface $file_system,
    CloudCacheServiceInterface $cloud_cache_service,
    RequestStack $request_stack,
  ) {
    // Set up the entity type manager for querying entities.
    $this->entityTypeManager = $entity_type_manager;

    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->lock = $lock;

    // Set up the configuration factory.
    $this->configFactory = $config_factory;

    $this->currentUser = $current_user;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->queueFactory = $queue_factory;
    $this->httpClient = $http_client;
    $this->moduleHandler = $module_handler;
    $this->routeMatch = $route_match;
    $this->cloudService = $cloud_service;
    $this->cloudCacheService = $cloud_cache_service;
    $this->twig = $twig;
    $this->fileSystem = $file_system;
    $this->requestStack = $request_stack;
  }

  /**
   * Switch OpenStackService.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return null|object
   *   OpenStack service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function get($cloud_context): ?object {
    if (empty($cloud_context)) {
      return NULL;
    }

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    $cloud_config = array_shift($cloud_configs);

    $test_mode = (bool) $this->configFactory->get('openstack.settings')->get('openstack_test_mode');
    $class_name = $test_mode ? OpenStackServiceMock::class : OpenStackRestService::class;

    $openstack_service = !empty($cloud_config->field_use_openstack_ec2_api->value)
    ? new OpenStackEc2Service(
        $this->entityTypeManager,
        $this->configFactory,
        $this->currentUser,
        $this->cloudConfigPluginManager,
        $this->fieldTypePluginManager,
        $this->entityFieldManager,
        $this->lock,
        $this->queueFactory,
        $this->httpClient,
        $this->moduleHandler,
        $this->cloudService,
        $this->cloudCacheService,
        $this->requestStack
      )
    : new $class_name(
        $this->entityTypeManager,
        $this->cloudConfigPluginManager,
        $this->httpClient,
        $this->lock,
        $this->queueFactory,
        $this->entityFieldManager,
        $this->fieldTypePluginManager,
        $this->configFactory,
        $this->cloudService,
        $this->twig,
        $this->fileSystem,
      );

    $openstack_service->setCloudContext($cloud_context);
    return $openstack_service;
  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  public function isEc2ServiceType($entity_type_id, $cloud_context): bool {
    $cloud_config = $this->entityTypeManager
      ->getStorage($entity_type_id)->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $cloud_config = array_shift($cloud_config);

    $openstack_ec2_api = !empty($cloud_config) && !empty($cloud_config->field_use_openstack_ec2_api !== NULL)
      ? $cloud_config->field_use_openstack_ec2_api->value : FALSE;

    return $openstack_ec2_api;
  }

}
