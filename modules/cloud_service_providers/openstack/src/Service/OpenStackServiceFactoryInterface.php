<?php

namespace Drupal\openstack\Service;

/**
 * Provides OpenStackServiceFactoryInterface interface.
 */
interface OpenStackServiceFactoryInterface {

  /**
   * Switch OpenStackService.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Drupal\openstack\Service\OpenStackServiceInterface
   *   OpenStack service.
   */
  public function get($cloud_context): ?object;

  /**
   * Check if service type is EC2 or REST.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  public function isEc2ServiceType($entity_type_id, $cloud_context): bool;

}
