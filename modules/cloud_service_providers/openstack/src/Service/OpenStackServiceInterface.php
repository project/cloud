<?php

namespace Drupal\openstack\Service;

use Drupal\cloud\Entity\CloudConfigInterface;

/**
 * Provides OpenStackServiceInterface interface.
 */
interface OpenStackServiceInterface {

  /**
   * Defines OpenStack Nova API version.
   */
  public const OPENSTACK_NOVA_API_VERSION = 2.65;

  /**
   * Defines IPV4 version.
   */
  public const IPV4 = 4;

  /**
   * Defines OpenStack keystone API service type.
   */
  public const IDENTITY_SERVICE = 'identity';

  /**
   * Defines OpenStack Nova API service type.
   */
  public const COMPUTE_SERVICE = 'compute';

  /**
   * Defines OpenStack Glance API service type.
   */
  public const IMAGE_SERVICE = 'image';

  /**
   * Defines OpenStack Neutron API service type.
   */
  public const NETWORK_SERVICE = 'network';

  /**
   * Defines OpenStack Cinder API service type.
   */
  public const VOLUME_SERVICE = 'volume';

  /**
   * Defines OpenStack Orchestration API service type.
   */
  public const ORCHESTRATION_SERVICE = 'orchestration';

  /**
   * Defines maximum tag length.
   */
  public const MAX_TAG_LENGTH = 60;

  /**
   * Defines interval (in seconds) for checking the status of the instance.
   */
  public const OPENSTACK_INSTANCE_STATUS_CHECK_INTERVAL = 1;

  /**
   * Defines the time (in seconds) to wait for an instance to become active.
   */
  public const WAIT_SECONDS_FOR_OPENSTACK_INSTANCE_ACTIVE = 60;

  /**
   * Defines instance console types.
   */
  public const INSTANCE_CONSOLES = [
    [
      'protocol' => 'vnc',
      'type' => 'novnc',
    ],
    [
      'protocol' => 'spice',
      'type' => 'spice-html5',
    ],
    [
      'protocol' => 'rdp',
      'type' => 'rdp-html5',
    ],
    [
      'protocol' => 'mks',
      'type' => 'webmks',
    ],
  ];

  /**
   * Define a resource to store the Drupal uid in OpenStack's Metadata.
   *
   * Basically, we register the resource in Tags,
   * and for resources that do not have Tags, we register them in Metadata.
   * Instances are exceptionally registered in Metadata.
   * It may take some time to start up,
   * and a 404 error will occur if you try to create a Tag during startup.
   * Therefore, we register the uid in Metadata when creating the instance.
   *
   * @see https://docs.openstack.org/yoga/api/
   *
   * @var string[]
   */
  public const RESOURCES_THAT_STORE_UID_IN_OPENSTACK_METADATA = [
    'openstack_instance',
    'openstack_snapshot',
    'openstack_volume',
  ];

  /**
   * List of policy names that can be set in the Server group.
   *
   * @var string[]
   */
  public const SERVER_GROUP_POLICY_OPTION = [
    'anti-affinity' => 'anti-affinity',
    'affinity' => 'affinity',
    'soft-anti-affinity' => 'soft-anti-affinity',
    'soft-affinity' => 'soft-affinity',
  ];

  /**
   * Defines OpenStack quota camel case.
   */
  public const QUOTA_LIMIT_FIELD_CAMEL_CASE = [
    'instances_usage' => 'Instances',
    'cores_usage' => 'Cores',
    'ram_usage' => 'Ram',
    'metadata_items_usage' => 'MetadataItems',
    'key_pairs_usage' => 'KeyPairs',
    'server_groups_usage' => 'ServerGroups',
    'server_group_members_usage' => 'ServerGroupMembers',
    'injected_files_usage' => 'InjectedFiles',
    'injected_file_content_bytes_usage' => 'InjectedFileContentBytes',
    'injected_file_path_bytes_usage' => 'InjectedFilePathBytes',
    'volumes_usage' => 'Volumes',
    'snapshots_usage' => 'Snapshots',
    'gigabytes_usage' => 'GigaBytes',
    'network_usage' => 'Network',
    'subnet_usage' => 'Subnet',
    'port_usage' => 'Port',
    'router_usage' => 'Router',
    'floatingip_usage' => 'FloatingIp',
    'security_group_usage' => 'SecurityGroup',
    'security_group_rule_usage' => 'SecurityGroupRule',
  ];

  /**
   * Defines router interface owners.
   */
  public const ROUTER_INTERFACE_OWNERS = [
    'network:router_interface',
    'network:router_interface_distributed',
    'network:ha_router_replicated_interface',
  ];

  /**
   * Defines target ports of security group rule.
   */
  public const SECURITY_GROUP_RULE_TARGET_PORTS = [
    20,
    21,
    22,
    23,
    513,
    3389,
  ];

  /**
   * Defines any IP.
   */
  public const ANY_IP = [
    'IPv4' => '0.0.0.0/0',
    'IPv6' => '::/0',
  ];

  /**
   * Defines well-known ports.
   */
  public const WELL_KNOWN_PORTS = [
    'all_icmp' => [
      'name' => 'All ICMP',
      'ip_protocol' => 'icmp',
      'from_port' => '-1',
      'to_port' => '-1',
    ],
    'all_tcp' => [
      'name' => 'All TCP',
      'ip_protocol' => 'tcp',
      'from_port' => '1',
      'to_port' => '65535',
    ],
    'all_udp' => [
      'name' => 'All UDP',
      'ip_protocol' => 'udp',
      'from_port' => '1',
      'to_port' => '65535',
    ],
    'dns' => [
      'name' => 'DNS',
      'ip_protocol' => 'tcp',
      'from_port' => '53',
      'to_port' => '53',
    ],
    'http' => [
      'name' => 'HTTP',
      'ip_protocol' => 'tcp',
      'from_port' => '80',
      'to_port' => '80',
    ],
    'https' => [
      'name' => 'HTTPS',
      'ip_protocol' => 'tcp',
      'from_port' => '443',
      'to_port' => '443',
    ],
    'imap' => [
      'name' => 'IMAP',
      'ip_protocol' => 'tcp',
      'from_port' => '143',
      'to_port' => '143',
    ],
    'imaps' => [
      'name' => 'IMAPS',
      'ip_protocol' => 'tcp',
      'from_port' => '993',
      'to_port' => '993',
    ],
    'ldap' => [
      'name' => 'LDAP',
      'ip_protocol' => 'tcp',
      'from_port' => '389',
      'to_port' => '389',
    ],
    'ms_sql' => [
      'name' => 'MS SQL',
      'ip_protocol' => 'tcp',
      'from_port' => '1433',
      'to_port' => '1433',
    ],
    'mysql' => [
      'name' => 'MySQL',
      'ip_protocol' => 'tcp',
      'from_port' => '3306',
      'to_port' => '3306',
    ],
    'pop3' => [
      'name' => 'POP3',
      'ip_protocol' => 'tcp',
      'from_port' => '110',
      'to_port' => '110',
    ],
    'pop3s' => [
      'name' => 'POP3S',
      'ip_protocol' => 'tcp',
      'from_port' => '995',
      'to_port' => '995',
    ],
    'rdp' => [
      'name' => 'RDP',
      'ip_protocol' => 'tcp',
      'from_port' => '3389',
      'to_port' => '3389',
    ],
    'smtp' => [
      'name' => 'SMTP',
      'ip_protocol' => 'tcp',
      'from_port' => '25',
      'to_port' => '25',
    ],
    'smtps' => [
      'name' => 'SMTPS',
      'ip_protocol' => 'tcp',
      'from_port' => '465',
      'to_port' => '465',
    ],
    'ssh' => [
      'name' => 'SSH',
      'ip_protocol' => 'tcp',
      'from_port' => '22',
      'to_port' => '22',
    ],
  ];

  /**
   * Defines the admin role.
   */
  public const ADMIN_ROLE = 'admin';

  /**
   * Get Client object for API execution.
   *
   * @param array $credentials
   *   The array of credentials.
   *
   * @return array
   *   The client.
   */
  public function getClient(array $credentials = []);

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function setCloudContext($cloud_context);

  /**
   * Get instance list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The instance list API response.
   */
  public function describeInstances(array $params = []);

  /**
   * Update instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The instance update API response.
   */
  public function modifyInstance(array $params = []);

  /**
   * Start instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return bool
   *   The instance Start API response.
   */
  public function startInstances(array $params = []);

  /**
   * Stop instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return bool
   *   The instance Stop API response.
   */
  public function stopInstances(array $params = []);

  /**
   * Reboot instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return bool
   *   The instance Reboot API response.
   */
  public function rebootInstances(array $params = []);

  /**
   * Terminate|Delete instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return bool
   *   The instance terminate|Delete API response.
   */
  public function terminateInstances(array $params = []);

  /**
   * Run instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   * @param array $tags
   *   The tags array.
   *
   * @return bool
   *   The create instance list API response.
   */
  public function runInstances(array $params = [], array $tags = []);

  /**
   * Create an image from an instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Response array.
   */
  public function createImageFromInstance(array $params = []): ?array;

  /**
   * Get image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image list API response.
   */
  public function describeImages(array $params = []);

  /**
   * Create image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image create API response.
   */
  public function createImage(array $params = []);

  /**
   * Delete image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image delete API response.
   */
  public function deregisterImage(array $params = []);

  /**
   * Create Tags.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return mixied
   *   The Tags create API response.
   */
  public function createTags(array $params = []);

  /**
   * Delete tags.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return mixed
   *   The instance delete list API response.
   */
  public function deleteTags(array $params = []);

  /**
   * Get key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The key pairs list API response.
   */
  public function describeKeyPairs(array $params = []);

  /**
   * Create key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The key pairs create API response.
   */
  public function createKeyPair(array $params = []);

  /**
   * Import key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The key pairs import API response.
   */
  public function importKeyPair(array $params = []);

  /**
   * Delete key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The key pairs delete API response.
   */
  public function deleteKeyPair(array $params = []);

  /**
   * Get volume list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume list API response.
   */
  public function describeVolumes(array $params = []);

  /**
   * Get volume types.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume list API response.
   */
  public function describeVolumeTypes(array $params = []);

  /**
   * Get a default volume type.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume list API response.
   */
  public function getDefaultVolumeType(array $params = []);

  /**
   * Create volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume create API response.
   */
  public function createVolume(array $params = []);

  /**
   * Delete volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return bool
   *   Succeeded or failed.
   */
  public function deleteVolume(array $params = []);

  /**
   * Attach volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function attachVolume(array $params = []);

  /**
   * Detach volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function detachVolume(array $params = []);

  /**
   * Get snapshot list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The snapshot list API response.
   */
  public function describeSnapshots(array $params = []);

  /**
   * Create snapshot.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The snapshot create API response.
   */
  public function createSnapshot(array $params = []);

  /**
   * Delete snapshot.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return bool
   *   Succeeded or failed.
   */
  public function deleteSnapshot(array $params = []);

  /**
   * Get security group list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group list API response.
   */
  public function describeSecurityGroups(array $params = []);

  /**
   * Create security group list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group create API response.
   */
  public function createSecurityGroup(array $params = []);

  /**
   * Delete security group List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return bool
   *   Succeeded or failed.
   */
  public function deleteSecurityGroup(array $params = []);

  /**
   * Get Floating IP List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP list API response.
   */
  public function describeAddresses(array $params = []);

  /**
   * Create Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP create API response.
   */
  public function allocateAddress(array $params = []);

  /**
   * Associate Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP associate API response.
   */
  public function associateAddress(array $params = []);

  /**
   * Disassociate Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP associate API response.
   */
  public function disassociateAddress(array $params = []);

  /**
   * Delete Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return bool
   *   The Floating IP delete API response.
   */
  public function releaseAddress(array $params = []);

  /**
   * Call the API for updated entities and store them as instance entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateInstanceEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = ''): bool;

  /**
   * Update the instances.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param bool $clear
   *   TRUE to clear stale instances.
   *
   * @return bool
   *   Indicates success or failure.
   */
  public function updateInstances(array $params = [], $clear = TRUE): bool;

  /**
   * Update the OpenStackImages.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param bool $save_operation
   *   TRUE to save operation and not to execute.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateImages(array $params = [], $clear = TRUE, $save_operation = FALSE): bool;

  /**
   * Call the API for updated entities and store them as image entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateImageEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = ''): bool;

  /**
   * Call the API for updated entities and store them as key pair entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   Key name info.
   * @param bool $update
   *   TRUE to update Key info.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateKeyPairEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE): bool;

  /**
   * Update the OpenStack key pairs.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateKeyPairs(): bool;

  /**
   * Call the API for updated entities and store them as volume entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   The volume info.
   * @param bool $update
   *   TRUE to update volume.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateVolumeEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE): bool;

  /**
   * Update the volumes.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateVolumes(): bool;

  /**
   * Get snapshot id and name.
   *
   * @param array $volumes
   *   The volume array.
   *
   * @return array
   *   Return map array.
   */
  public function getSnapshotIdNameMap(array $volumes = []);

  /**
   * Call the API for updated entities and store them as snapshot entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   The snapshot IDs.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSnapshotEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE): bool;

  /**
   * Update the snapshots.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSnapshots(): bool;

  /**
   * Update the OpenStackSecurityGroups.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSecurityGroups(array $params = [], $clear = TRUE): bool;

  /**
   * Call the API for updated entities and store them as SecurityGroup entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update SG info.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSecurityGroupEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool;

  /**
   * Update the OpenStackFloatingIps.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateFloatingIps(): bool;

  /**
   * Call the API for updated entities and store them as FloatingIp entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateFloatingIpEntities($entity_type = '', $cloud_context = ''): bool;

  /**
   * Update the OpenStackNetworks.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale networks.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateNetworks(array $params = [], $clear = TRUE): bool;

  /**
   * Call API for updated entities and store them as Network entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale networks.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateNetworkEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool;

  /**
   * Update the OpenStackSubnets.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale subnets.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSubnets(array $params = [], $clear = TRUE): bool;

  /**
   * Call API for updated entities and store them as Subnet entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale subnets.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSubnetEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool;

  /**
   * Update the OpenStackPorts.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale ports.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updatePorts(array $params = [], $clear = TRUE): bool;

  /**
   * Call API for updated entities and store them as Port entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale ports.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updatePortEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool;

  /**
   * Update the OpenStackRouters.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale routers.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateRouters(array $params = [], $clear = TRUE): bool;

  /**
   * Call API for updated entities and store them as Router entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale routers.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateRouterEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool;

  /**
   * Get quota list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The quota list API response.
   */
  public function describeQuotas(array $params = []): array;

  /**
   * Update the OpenStackQuotas.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale quotas.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateQuotas(array $params = [], $clear = TRUE): bool;

  /**
   * Call API for updated entities and store them as Quota entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale quotas.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateQuotaEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool;

  /**
   * Get stack list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack list API response.
   */
  public function describeStacks(array $params = []): array;

  /**
   * Get stack detail.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack detail API response.
   */
  public function showStack(array $params = []): array;

  /**
   * Update the OpenStackStacks.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale quotas.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateStacks(array $params = [], $clear = TRUE): bool;

  /**
   * Call API for updated entities and store them as Stack entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale quotas.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateStackEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool;

  /**
   * Get server group list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The server group list API response.
   */
  public function describeServerGroups(array $params = []): array;

  /**
   * Update the OpenStackServerGroups.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale server groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateServerGroups(array $params = [], $clear = TRUE): bool;

  /**
   * Call API for updated entities and store them as Server group entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale server groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateServerGroupEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool;

  /**
   * Setup IP Permissions.
   *
   * @param object $security_group
   *   The security group entity.
   * @param string $field
   *   Field to used for lookup.
   * @param array $ec2_permissions
   *   Permissions array from Ec2.
   */
  public function setupIpPermissions(&$security_group, $field, array $ec2_permissions);

  /**
   * Set up the ip_permission field given the inbound security group array.
   *
   * The array comes from DescribeSecurityGroup Amazon EC2 API call.
   *
   * @param array $ec2_permission
   *   An array object of EC2permission.
   *
   * @return array
   *   An array of \Drupal\Core\Field\FieldItemInterface.
   */
  public function setupIpPermissionObject(array $ec2_permission);

  /**
   * Get Tag name.
   *
   * @param array $openstack_obj
   *   The openstack object.
   * @param string $default_value
   *   The default value.
   *
   * @return string
   *   Return name of corresponding resource.
   */
  public function getTagName(array $openstack_obj, $default_value);

  /**
   * Get UID tag value.
   *
   * @param array $tags_array
   *   The tags array of API.
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return int
   *   Return user id.
   */
  public function getUidTagValue(array $tags_array, string $entity_type_id): int;

  /**
   * Get instance uid.
   *
   * @param string $instance_id
   *   The instance id.
   *
   * @return int
   *   Return user id.
   */
  public function getInstanceUid($instance_id);

  /**
   * Create queue items for update resources queue.
   *
   * @param \Drupal\cloud\Entity\CloudConfigInterface $cloud_config
   *   The Cloud Config entity.
   */
  public function createResourceQueueItems(CloudConfigInterface $cloud_config): void;

  /**
   * Check whether the cloud config is the resource of a worker.
   *
   * @return bool
   *   Whether the cloud config is from a worker or not.
   */
  public function isWorkerResource(): bool;

  /**
   * Update security group List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group update API response.
   */
  public function updateSecurityGroup(array $params = []): array;

  /**
   * Create security group Rule.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group rule create API response.
   */
  public function createSecurityGroupRule(array $params = []): array;

  /**
   * Revoke security group list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group rule revoke API response.
   */
  public function revokeSecurityGroupRule(array $params = []): array;

  /**
   * Create a port interface for a given instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The port create interface response.
   */
  public function createPortInterface(array $params = []): ?array;

  /**
   * Delete a port interface for a given instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The delete port interface response.
   */
  public function deletePortInterface(array $params = []): ?array;

  /**
   * Get the roles of the current user.
   *
   * @return array
   *   The user and roles API response.
   */
  public function describeCurrentUserRoles(): array;

}
