<?php

namespace Drupal\openstack\Service\Rest;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\Core\Utility\Error;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudResourceTagInterface;
use Drupal\cloud\Service\CloudServiceBase;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\openstack\Entity\OpenStackImageInterface;
use Drupal\openstack\Entity\OpenStackKeyPair;
use Drupal\openstack\Entity\OpenStackPort;
use Drupal\openstack\Service\OpenStackBatchOperations;
use Drupal\openstack\Service\OpenStackServiceInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * Interacts with OpenStack using Native API.
 */
class OpenStackService extends CloudServiceBase implements OpenStackServiceInterface, CloudResourceTagInterface {

  use CloudContentEntityTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The lock interface.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Credentials array.
   *
   * @var array
   */
  protected $credentials;

  /**
   * Entity field manager interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Field type plugin manager.
   *
   * @var \Drupal\core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The Twig service.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twig;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Constructs a new OpenStackService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock interface.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\core\Field\FieldTypePluginManagerInterface $field_type_plugin_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\Core\Template\TwigEnvironment $twig
   *   The twig service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    ClientInterface $http_client,
    LockBackendInterface $lock,
    QueueFactory $queue_factory,
    EntityFieldManagerInterface $entity_field_manager,
    FieldTypePluginManagerInterface $field_type_plugin_manager,
    ConfigFactoryInterface $config_factory,
    CloudServiceInterface $cloud_service,
    TwigEnvironment $twig,
    FileSystemInterface $file_system,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    // Set up the entity type manager for querying entities.
    $this->entityTypeManager = $entity_type_manager;

    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->httpClient = $http_client;
    $this->lock = $lock;
    $this->queueFactory = $queue_factory;
    $this->entityFieldManager = $entity_field_manager;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
    $this->configFactory = $config_factory;
    $this->cloudService = $cloud_service;
    $this->twig = $twig;
    $this->fileSystem = $file_system;
  }

  /**
   * Helper method to call API.
   *
   * @param array $credentials
   *   The credentials array.
   * @param bool $quiet
   *   FALSE to output error.
   *
   * @return array
   *   Array interpretation of the response body.
   */
  public function getClient(array $credentials = [], $quiet = FALSE) {
    $http_method = $credentials['http_method'] ?? '';
    $endpoint = $credentials['endpoint'] ?? '';
    $data = $credentials['params'] ?? '';

    if (strtolower($http_method) !== 'get' && !empty($data)) {
      $params['body'] = json_encode($data);
    }

    $auth_token = $this->generateAuthToken();
    $params['headers']['X-Auth-Token'] = $auth_token;

    $params['headers']['Content-Type'] = !empty($credentials['Content-Type'])
      ? $credentials['Content-Type'] : 'application/json';

    if (!empty($credentials['X-OpenStack-Nova-API-Version'])) {
      $params['headers']['X-OpenStack-Nova-API-Version'] = $credentials['X-OpenStack-Nova-API-Version'];
    }

    $this->loadCredentials();
    $params['verify'] = !empty($this->credentials['self_signed_cert_path'])
      ? $this->credentials['self_signed_cert_path']
      : TRUE;

    return $this->request($http_method, $endpoint, $params, $quiet);
  }

  /**
   * Retrieves the image import client for the specified image ID and file path.
   *
   * @param string $image_id
   *   The ID of the image.
   * @param string $image_file_path
   *   The path to the image file.
   *
   * @return array|null
   *   Returns an array with the 'Success' key set to true on success,
   *   null on failure.
   */
  public function getImageFileDownloadClient(string $image_id, string $image_file_path): ?array {
    $auth_token = $this->generateAuthToken();
    $download_image_file_endpoint = $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, "/images/{$image_id}/file");
    $result = !empty(is_string($auth_token))
      ? $this->downloadImageFile($auth_token, $download_image_file_endpoint, $image_file_path)
      : NULL;

    return !empty($result) ? ['Success' => TRUE] : NULL;
  }

  /**
   * Retrieves the image import client for the specified image ID and file path.
   *
   * @param string $image_id
   *   The ID of the image.
   * @param string $image_file_path
   *   The path to the image file.
   *
   * @return array|null
   *   Returns an array with the 'Success' key set to true on success,
   *   null on failure.
   */
  public function getImageImportClient(string $image_id, string $image_file_path): ?array {
    $auth_token = $this->generateAuthToken();
    $upload_image_file_endpoint = $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, "/images/{$image_id}/file");
    $result = !empty(is_string($auth_token))
      ? $this->uploadImageFile($auth_token, $upload_image_file_endpoint, $image_file_path)
      : NULL;

    return !empty($result) ? ['Success' => TRUE] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context) {
    $this->cloudContext = $cloud_context;
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
  }

  /**
   * Load credentials.
   *
   * @param array $credentials
   *   The credentials array.
   *
   * @return array
   *   The credentials array.
   */
  private function loadCredentials(array $credentials = []) {
    if (!empty($credentials)) {
      $this->credentials = $credentials;
    }
    elseif (empty($this->credentials)) {
      $this->credentials = $this->cloudConfigPluginManager->loadCredentials();
    }

    return $this->credentials;
  }

  /**
   * Set the credential array.
   *
   * Gives implementing code a chance to set credentials without setting up
   * a OpenStack cloud service provider.
   */
  public function setCredentials(array $credentials): void {
    $this->credentials = $credentials;
  }

  /**
   * Authenticate.
   *
   * @param array $credentials
   *   The credentials array.
   *
   * @return array
   *   Array of parameters.
   */
  private function authenticate(array $credentials = []): array {
    $this->loadCredentials($credentials);

    $body = [
      'auth' => [
        'identity' => [
          'methods' => ['password'],
          'password' => [
            'user' => [
              'name' => $this->credentials['username'],
              'domain' => [
                'id' => $this->credentials['domain_id'],
              ],
              'password' => $this->credentials['password'],
            ],
          ],
        ],
        'scope' => [
          'project' => [
            'id' => $this->credentials['project_id'],
          ],
        ],
      ],
    ];

    $endpoint = $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, '/auth/tokens');
    return $this->request('POST', $endpoint, [
      'json' => $body,
      'verify' => !empty($this->credentials['self_signed_cert_path'])
        ? $this->credentials['self_signed_cert_path']
        : TRUE,
    ]);
  }

  /**
   * Setup any default parameters for the Guzzle request.
   *
   * @param array $credentials
   *   The credentials array.
   *
   * @return array|string
   *   Array of parameters.
   */
  public function generateAuthToken(array $credentials = []): array|string {
    $response = $this->authenticate($credentials);
    return $response['auth_token'] ?? $response;
  }

  /**
   * Get instance list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The instance list API response.
   */
  public function describeInstances(array $params = []) {

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, '/servers/detail'),
      'params' => $params,
    ];

    $response = $this->getClient($credentials);

    $results = $volume_id_arr = $security_groups = [];
    $instances = $response['body']['servers'] ?? [];

    foreach ($instances ?: [] as $key => $instance) {
      $instance_tags = $this->getInstanceTags(['InstanceId' => $instance['id']]);

      $groups_added = [];
      foreach ($instance['security_groups'] ?? [] as $group) {
        if (empty($group['name']) || in_array($group['name'], $groups_added)) {
          continue;
        }
        $security_groups[] = [
          'GroupName' => $group['name'],
        ];
        $groups_added[] = $group['name'];
      }

      // Get the volume Response.
      $volume_ids = $instance['os-extended-volumes:volumes_attached'];
      foreach ($volume_ids ?: [] as $volume_id_val) {
        $volume_id = $volume_id_val['id'];
        $volume_data = $this->describeVolumeById([], $volume_id);
        $image_id = $volume_data['volume_image_metadata']['image_id'] ?? NULL;
        $instance['image'] = empty($instance['image'])
          ? ['id' => $image_id] : [];
        $volume_id_arr = [
          'Ebs' => ['VolumeId' => $volume_id ?? ''],
        ];
      }

      // Get flavor Response.
      $flavor_data = !empty($instance['flavor']['id'])
        ? $this->describeFlavorById([], $instance['flavor']['id'])
        : [];

      // Get port response.
      $port_result = !empty($instance['OS-EXT-STS:vm_state'])
      && $instance['OS-EXT-STS:vm_state'] !== 'building'
        ? $this->instancePortInterface([
          'InstanceId' => $instance['id'],
        ])
        : [];

      $port_ids = [];
      $port_ip_addresses = [];
      if (!empty($port_result)) {
        foreach ($port_result ?: [] as $port) {
          $port_ids[] = $port['port_id'];
          $ips = [];
          foreach ($port['fixed_ips'] ?: [] as $ip) {
            $ips[] = $ip['ip_address'];
          }
          // Summarize Address for each Port with comma separators.
          $port_ip_addresses[] = [
            'item_key' => $port['port_id'],
            'item_value' => implode(', ', $ips),
          ];
        }
      }

      foreach ($instance['addresses'] ?: [] as $add_type => $address_val) {
        foreach ($address_val ?: [] as $add_value) {
          if ($add_type === 'private') {
            if ($add_value['OS-EXT-IPS:type'] === 'floating') {
              $public_ip = $add_value['addr'] ?? '';
            }
            elseif ($add_value['OS-EXT-IPS:type'] === 'fixed'
              && $add_value['version'] === OpenStackServiceInterface::IPV4) {
              $private_ip = $add_value['addr'] ?? '';
            }
          }
          elseif ($add_type === 'public'
            && $add_value['OS-EXT-IPS:type'] === 'fixed'
            && $add_value['version'] === OpenStackServiceInterface::IPV4) {
            $public_ip = $add_value['addr'] ?? '';
          }
        }
      }

      $state = strtolower($instance['status']);
      if ($state === 'active') {
        $state = 'running';
      }
      elseif ($state === 'shutoff') {
        $state = 'stopped';
      }

      $results['Reservations'][$key] = [
        'OwnerId'    => $instance['tenant_id'],
        'Instances'  => [
          [
            'AmiLaunchIndex'      => NULL,
            'ImageId'             => $instance['image']['id'] ?? NULL,
            'InstanceId'          => $instance['id'] ?? NULL,
            'InstanceName'        => $instance['name'] ?? NULL,
            'InstanceType'        => $flavor_data['name'] ?? NULL,
            'KeyName'             => $instance['key_name'] ?? NULL,
            'LaunchTime'          => !empty($instance['OS-SRV-USG:launched_at'])
              ? str_replace('T', ' ', $instance['OS-SRV-USG:launched_at'])
              : str_replace('Z', '', str_replace('T', ' ', $instance['created'])),
            'Placement'           => ['AvailabilityZone' => $instance['OS-EXT-AZ:availability_zone'] ?? ''] ?? NULL,
            'PrivateDnsName'      => $instance['name'] ?? NULL,
            'PrivateIpAddress'    => $private_ip ?? '',
            'PublicIpAddress'     => $public_ip ?? '',
            'PublicDnsName'       => $instance['dns_name'] ?? NULL,
            'State'               => ['Name' => $state],
            'PowerState'          => $instance['OS-EXT-STS:power_state'] ?? NULL,
            'RootDeviceName'      => NULL,
            'RootDeviceType'      => NULL,
            'SecurityGroups'      => $security_groups,
            'BlockDeviceMappings' => [$volume_id_arr],
            'PortId'              => $port_ids,
            'PortIpAddress'       => $port_ip_addresses,
            'Tags'                => $instance_tags,
          ],
        ],
      ];
    }

    return $results;
  }

  /**
   * Update instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The instance update API response.
   */
  public function modifyInstance(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateInstances');

      return ['SendToWorker' => TRUE];
    }

    // Update security groups.
    $old_security_groups = $params['OldSecurityGroups'];
    $new_security_groups = $params['NewSecurityGroups'];
    if (!empty(array_diff($new_security_groups, $old_security_groups))
      || !empty(array_diff($old_security_groups, $new_security_groups))) {

      foreach ($params['PortIds'] ?? [] as $port_id) {
        $this->updatePort([
          'PortId' => $port_id,
          'SecurityGroups' => $new_security_groups,
        ]);
      }
    }

    $body['server'] = [
      'name' => $params['Name'],
    ];

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $body['server'] = [
      "name" => $params['Name'],
    ];

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $instance = $response['body']['server'] ?? [];

    $volume_id_arr = $security_groups = [];

    // Get the volume Response.
    $volume_ids = $instance['os-extended-volumes:volumes_attached'] ?? [];
    foreach ($volume_ids ?: [] as $volume_id_val) {
      $volume_id = $volume_id_val['id'];
      $volume_data = $this->describeVolumeById([], $volume_id);
      $image_id = $volume_data['volume_image_metadata']['image_id'] ?? NULL;
      $instance['image'] = empty($instance['image'])
        ? ['id' => $image_id] : [];
      $volume_id_arr = [
        'Ebs' => ['VolumeId' => $volume_id ?? ''],
      ];
    }

    // Get flavor Response.
    $flavor_data = !empty($instance['flavor']['id'])
      ? $this->describeFlavorById([], $instance['flavor']['id'])
      : [];

    // Get port response.
    $port_result = !empty($instance['OS-EXT-STS:vm_state'])
    && $instance['OS-EXT-STS:vm_state'] !== 'building'
      ? $this->instancePortInterface([
        'InstanceId' => $instance['id'],
      ])
      : [];

    $port_ids = [];
    if (!empty($port_result)) {
      foreach ($port_result ?: [] as $port) {
        $port_ids[] = $port['port_id'];
      }
    }

    foreach ($instance['addresses'] ?: [] as $address_val) {
      foreach ($address_val ?: [] as $add_value) {
        if (!empty($add_value['OS-EXT-IPS:type'])) {
          if ($add_value['OS-EXT-IPS:type'] === 'floating') {
            $public_ip = $add_value['addr'] ?? '';
          }
          if ($add_value['OS-EXT-IPS:type'] === 'fixed'
            && $add_value['version'] === OpenStackServiceInterface::IPV4) {
            $private_ip = $add_value['addr'] ?? '';
          }
        }
      }
    }

    $instance_tags = $this->getInstanceTags(
      ['InstanceId' => $params['InstanceId']]
    );

    return [
      'Reservations' => [
        'OwnerId'    => $instance['tenant_id'],
        'Instances'  => [
          [
            'AmiLaunchIndex'      => NULL,
            'ImageId'             => $instance['image']['id'] ?? NULL,
            'InstanceId'          => $instance['id'] ?? NULL,
            'InstanceName'        => $instance['name'] ?? NULL,
            'InstanceType'        => $flavor_data['name'] ?? NULL,
            'KeyName'             => $instance['key_name'] ?? NULL,
            'LaunchTime'          => !empty($instance['OS-SRV-USG:launched_at'])
              ? str_replace('T', ' ', $instance['OS-SRV-USG:launched_at']) : NULL,
            'Placement'           => ['AvailabilityZone' => $instance['OS-EXT-AZ:availability_zone'] ?? ''] ?? NULL,
            'PrivateDnsName'      => $instance['name'] ?? NULL,
            'PrivateIpAddress'    => $private_ip ?? '',
            'PublicIpAddress'     => $public_ip ?? '',
            'PublicDnsName'       => $instance['dns_name'] ?? NULL,
            'State'               => ['Name' => $instance['status'] === 1 ? 'running' : 'stopped'],
            'RootDeviceName'      => NULL,
            'RootDeviceType'      => NULL,
            'SecurityGroups'      => $security_groups,
            'BlockDeviceMappings' => [$volume_id_arr],
            'PortId'              => $port_ids,
            'Tags'                => $instance_tags,
          ],
        ],
      ],
    ];
  }

  /**
   * Start instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The instance Start API response.
   */
  public function startInstances(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateInstances');

      return ['SendToWorker' => TRUE];
    }

    $body = [
      'os-start' => NULL,
    ];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/action"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 202 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Stop instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The instance Stop API response.
   */
  public function stopInstances(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateInstances');

      return ['SendToWorker' => TRUE];
    }

    $body = [
      'os-stop' => NULL,
    ];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/action"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 202 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Reboot instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The instance Reboot API response.
   */
  public function rebootInstances(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateInstances');

      return ['SendToWorker' => TRUE];
    }

    // Default to HARD if params['type'] is not passed.
    $body['reboot'] = [
      'type' => !empty($params['type']) ? $params['type'] : 'HARD',
    ];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/action"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 202 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Terminate|Delete instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The instance Terminate|Delete API response.
   */
  public function terminateInstances(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateInstances');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Run instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   * @param array $tags
   *   The tags array.
   *
   * @return array|null
   *   The create instance list API response.
   */
  public function runInstances(array $params = [], array $tags = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateInstances');

      return ['SendToWorker' => TRUE];
    }

    $metadata = [];
    foreach ($tags as $tag) {
      $metadata[$tag['Key']] = (string) $tag['Value'];
    }

    $body = [
      'server' => [
        'name' => $params['Name'],
        'flavorRef' => $params['FlavorId'],
        'imageRef' => $params['ImageId'],
        'key_name' => $params['KeyName'],
        'availability_zone' => $params['Placement']['AvailabilityZone'],
        'security_groups' => $params['SecurityGroupIds'],
        'networks' => $params['NetworkInterfaces'],
        'metadata' => $metadata,
      ],
    ];

    $body += !empty($params['ServerGroupId'])
    ? [
      'os:scheduler_hints' => [
        'group' => $params['ServerGroupId'],
      ],
    ] : [];

    $body['server'] += !empty($params['UserData'])
    ? [
      'user_data' => $params['UserData'],
    ] : [];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, '/servers'),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    // Update instances.
    $this->updateInstances();

    return $response['status_code'] === 202 ? ['Success' => TRUE] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createImageFromInstance(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateInstances');

      return ['SendToWorker' => TRUE];
    }

    $body = [
      'createImage' => [
        'name' => $params['Name'],
      ],
    ];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/action"),
      'params' => $body,
    ];
    $response = $this->getClient($credentials);
    return ($response['status_code'] === 202) ? ['Success' => TRUE] : NULL;
  }

  /**
   * Delete tags.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The instance delete list API response.
   */
  public function deleteTags(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params]);

      return ['SendToWorker' => TRUE];
    }

    $endpoint = $this->getDeleteTagsEndpointUrl($params);

    // If the endpoint cannot be obtained, the process is canceled.
    if (!array_key_exists($params['EntityType'], $endpoint)) {
      return NULL;
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $endpoint[$params['EntityType']],
    ];
    if ($params['EntityType'] === 'openstack_instance') {
      $credentials['X-OpenStack-Nova-API-Version'] = OpenStackServiceInterface::OPENSTACK_NOVA_API_VERSION;
    }

    $response = $this->getClient($credentials);

    return ($response['status_code'] === 202 || $response['status_code'] === 204) ? ['Success' => TRUE] : NULL;
  }

  /**
   * Get image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image list API response.
   */
  public function describeImages(array $params = []) {

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, '/images'),
      'params' => $params,
    ];

    $images = [];
    $limit_retrieve_count = $this->configFactory->get('openstack.settings')->get('openstack_limit_resources_retrieve');
    $total_retrieved_count = 0;
    while (!empty($credentials['endpoint']) && $total_retrieved_count < $limit_retrieve_count) {

      $response = $this->getClient($credentials);

      // Get image data and append it to $images.
      $retrieved_images = $response['body']['images'] ?? [];
      // Calculate how many more images can be retrieved
      // without exceeding the limit.
      $remaining_images_count = $limit_retrieve_count - $total_retrieved_count;

      // Append only the images that keep us within the limit.
      $images_to_add = count($retrieved_images) <= $remaining_images_count
        ? $retrieved_images
        : array_slice($retrieved_images, 0, $remaining_images_count);
      $images = array_merge($images, $images_to_add);
      $total_retrieved_count += count($images_to_add);

      // If there's a next URL, set the new endpoint.
      $next_url = !empty($response['body']['next']) ? $response['body']['next'] : NULL;
      $credentials['endpoint'] = !empty($next_url)
        ? $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, $next_url, FALSE)
        : '';
    }

    $results = [];

    foreach ($images ?: [] as $key => $value) {
      $results['Images'][$key]['Architecture'] = NULL;
      $results['Images'][$key]['CreationDate'] = $value['created_at'];
      $results['Images'][$key]['ImageId'] = $value['id'];
      $results['Images'][$key]['ImageLocation'] = NULL;
      $results['Images'][$key]['ImageType'] = $value['disk_format'];
      $results['Images'][$key]['ImageVisibility'] = $value['visibility'];
      $results['Images'][$key]['OwnerId'] = $value['owner'];
      $results['Images'][$key]['State'] = $value['status'] === 'active' ? 'available' : 'pending';
      $results['Images'][$key]['Name'] = $value['name'];
      $results['Images'][$key]['RootDeviceType'] = $value['container_format'];
      $results['Images'][$key]['Tags'] = $value['tags'];
    }

    return $results;
  }

  /**
   * Get image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image list API response.
   */
  public function describeProjectSharedImage(array $params = []): array {
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, "/images/{$params['ImageId']}/members"),
      'params' => $params,
    ];

    $response = $this->getClient($credentials);
    $members = $response['body']['members'] ?? [];

    $results = [];
    // Get shared projects in the same region.
    $results['Members'] = array_map(static function ($member) use ($params) {
      return [
        'CloudContext' => $params['CloudContext'] ?? NULL,
        'ProjectId' => $member['member_id'] ?? NULL,
        'Status' => $member['status'] ?? NULL,
      ];
    }, $members ?: []);

    // @todo Get shared projects in the inter region.
    return $results;
  }

  /**
   * Delete shared members.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image list API response.
   */
  public function deleteProjectSharedMembers(array $params = []): ?array {
    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, "/images/{$params['ImageId']}/members/{$params['ProjectId']}"),
    ];

    $response = $this->getClient($credentials);

    return ($response['status_code'] === 202 || $response['status_code'] === 204) ? ['Success' => TRUE] : NULL;
  }

  /**
   * Get image file.
   *
   * @param array $params
   *   Parameters to pass to the API.
   * @param string $save_file_path
   *   The save file path.
   *
   * @return array
   *   The image list API response.
   */
  public function downloadImage(array $params, string $save_file_path): ?array {
    $response = $this->getImageFileDownloadClient($params['ImageId'], $save_file_path);

    return !empty($response) && array_key_exists('status_code', $response)
      && ($response['status_code'] === 200 || $response['status_code'] === 204) ? ['Success' => TRUE] : NULL;
  }

  /**
   * Upload an image to the specified cloud context.
   *
   * @param string $to_cloud_context
   *   The target cloud context.
   * @param string $image_id
   *   The ID of the image.
   * @param string $image_file_path
   *   The file path of the image.
   *
   * @return array
   *   The response indicating the success or failure of the import.
   */
  public function uploadImage(
    string $to_cloud_context,
    string $image_id,
    string $image_file_path,
  ): ?array {

    $from_cloud_context = $this->cloudContext;

    // Switch to the target cloud context.
    $this->setCloudContext($to_cloud_context);

    // Image ID validation.
    if (empty($image_id)) {
      // Restore the original cloud context.
      $this->setCloudContext($from_cloud_context);

      return NULL;
    }

    // Import image.
    $response = $this->getImageImportClient($image_id, $image_file_path);

    // Restore the original cloud context.
    $this->setCloudContext($from_cloud_context);

    // Response validation.
    if (empty($response['status_code'])) {

      return NULL;
    }

    return in_array($response['status_code'], [200, 204]) ? ['Success' => TRUE] : NULL;
  }

  /**
   * Create image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image create API response.
   */
  public function createImage(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateImages');

      return ['SendToWorker' => TRUE];
    }

    $body = [
      'container_format' => 'bare',
      'disk_format' => 'qcow2',
      'name' => $params['Name'],
      'visibility' => $params['ImageVisibility'],
    ];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, '/images'),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    return !empty($response) && !empty($response['body']) ? [
      'ImageId'          => $response['body']['id'],
      'name'             => $response['body']['name'],
      'disk_format'      => $response['body']['disk_format'],
      'container_format' => $response['body']['container_format'],
      'visibility'       => $response['body']['visibility'],
      'created_at'       => $response['body']['created_at'],
      'updated_at'       => $response['body']['updated_at'],
      'Tags'             => $response['body']['tags'],
      'status'           => $response['body']['status'],
    ]
    : [];
  }

  /**
   * Update image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image update API response.
   */
  public function updateImage(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateImages');

      return ['SendToWorker' => TRUE];
    }

    $body = [
      [
        'op' => 'replace',
        'path' => '/name',
        'value' => $params['Name'],
      ],
      [
        'op' => 'replace',
        'path' => '/visibility',
        'value' => $params['ImageVisibility'],
      ],
    ];

    $credentials = [
      'http_method' => 'patch',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, "/images/{$params['ImageId']}"),
      'params' => $body,
    ];

    $credentials['Content-Type'] = 'application/openstack-images-v2.1-json-patch';

    $response = $this->getClient($credentials);

    return !empty($response) && !empty($response['body']) ? [
      'ImageId'          => $response['body']['id'],
      'name'             => $response['body']['name'],
      'disk_format'      => $response['body']['disk_format'],
      'container_format' => $response['body']['container_format'],
      'visibility'       => $response['body']['visibility'],
      'created_at'       => $response['body']['created_at'],
      'updated_at'       => $response['body']['updated_at'],
      'Tags'             => $response['body']['tags'],
      'status'           => $response['body']['status'],
    ]
    : [];
  }

  /**
   * Add image tags.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image update API response.
   */
  public function addImageTags(array $params = []): bool {
    $tag_value_format = OpenStackImageInterface::SHARED_IMAGE_TAG_VALUE_FORMAT;
    $tags = array_filter(array_map(static function ($tag) use ($tag_value_format) {
      return (empty($tag['CloudContext']) || empty($tag['ProjectId']) || empty($tag['ImageId']))
        ? NULL
        : [
          'Key' => OpenStackImageInterface::SHARED_PROJECT_TAG_NAME_PREFIX,
          'Value' => md5(strtr($tag_value_format, [
            '{cloud_context}' => $tag['CloudContext'],
            '{project_id}' => $tag['ProjectId'],
            '{image_id}' => $tag['ImageId'],
          ])),
        ];
    }, $params['Tags']));

    $result = $this->createTags([
      'Resources' => [$params['ImageId']],
      'EntityType' => 'openstack_image',
      'Tags' => $tags,
    ]);

    return !empty($result['Success']);
  }

  /**
   * Get UID tag value.
   *
   * @param array $tags_array
   *   The tags array of API.
   *
   * @return int
   *   Return user id.
   */
  public function getImageSharedProjectsTagValue(array $tags_array): ?array {
    $inter_region_images = [];
    $inter_region_configs = [];
    $shared_projects = [];

    if (empty($tags_array)) {
      return $shared_projects;
    }

    $openstack_configs = $this->cloudConfigPluginManager->loadConfigEntities('openstack');
    $cloud_context = $this->cloudContext;
    // Get cloudcontext and projectid for inter regions.
    array_walk($openstack_configs, static function ($openstack_config) use ($cloud_context, &$inter_region_configs) {
      if ($cloud_context !== $openstack_config->getCloudContext()) {
        $inter_region_configs[$openstack_config->getCloudContext()] = $openstack_config->get('field_project_id')->value ?? NULL;
      }
    });

    // Fetch all images for other regions and convert them to hashes
    // with cloud_context, project_id and image_id.
    $tag_value_format = OpenStackImageInterface::SHARED_IMAGE_TAG_VALUE_FORMAT;
    foreach ($inter_region_configs ?: [] as $cloud_context => $project_id) {
      $images = $this->entityTypeManager
        ->getStorage('openstack_image')
        ->loadByProperties([
          'cloud_context' => $cloud_context,
        ]);

      foreach ($images ?: [] as $image) {
        $inter_region_images[] = [
          'cloud_context' => $image->getCloudContext(),
          'project_id' => $project_id,
          'image_id' => $image->getImageId(),
          'hash' => md5(strtr($tag_value_format, [
            '{cloud_context}' => $image->getCloudContext(),
            '{project_id}' => $project_id,
            '{image_id}' => $image->getImageId(),
          ])),
          'status' => $this->t('@status', [
            '@status' => OpenStackImageInterface::IMAGE_SHARED_PROJECTS_STATUS[$image->getStatus()] ?? '',
          ]),
        ];
      }
    }

    $shared_projects = array_filter(
      array_map(
        static function ($tag) use ($inter_region_images) {
          if (strpos($tag, OpenStackImageInterface::SHARED_PROJECT_TAG_NAME_PREFIX . '.') === 0) {
            $hash = substr($tag, strlen(OpenStackImageInterface::SHARED_PROJECT_TAG_NAME_PREFIX . '.'));
            $index = array_search($hash, array_column($inter_region_images, 'hash'));
            return !empty($index) ? $inter_region_images[$index] : NULL;
          }
          return NULL;
        },
        $tags_array ?: []
      )
    );

    return $shared_projects;
  }

  /**
   * Update image shared projects.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image create API response.
   */
  public function updateImageSharedProjects(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateImages');

      return ['SendToWorker' => TRUE];
    }

    // Adding a project to members.
    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, "/images/{$params['ImageId']}/members"),
      'params' => [
        'member' => $params['ProjectId'],
      ],
    ];
    $response = $this->getClient($credentials);

    if (empty($response) || empty($response['body'])) {
      return [];
    }

    // Update the status of the added Project to 'accepted'.
    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, "/images/{$params['ImageId']}/members/{$params['ProjectId']}"),
      'params' => [
        'status' => 'accepted',
      ],
    ];
    $response = $this->getClient($credentials);

    return !empty($response) && !empty($response['body']) ? [
      'ImageId'   => $response['body']['image_id'],
      'ProjectId' => $response['body']['member_id'],
      'Status'    => $response['body']['status'],
      'Schema'    => $response['body']['schema'],
    ]
    : [];
  }

  /**
   * Register uid tag in the OpenStack resource's Tags.
   *
   * For resources that have Tags, register the uid with Tags.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $endpoint
   *   The endpoint.
   * @param array $tags
   *   The tags.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  private function setUidToTags(string $entity_type, string $endpoint, array $tags): bool {
    $success = FALSE;
    foreach ($tags as $tag) {
      $key_value = "{$tag['Key']}.{$tag['Value']}";

      if (strlen($key_value) > OpenStackServiceInterface::MAX_TAG_LENGTH) {
        $this->messenger->addError($this->t('Failed to create tag @tag because it was longer than @max_length characters.', [
          '@tag' => $key_value,
          '@max_length' => OpenStackServiceInterface::MAX_TAG_LENGTH,
        ]));

        $success = FALSE;
        break;
      }

      $credentials = [
        'http_method' => 'put',
        'endpoint' => $endpoint . '/' . $key_value,
      ];

      if ($entity_type === 'openstack_instance') {
        $credentials['X-OpenStack-Nova-API-Version'] = OpenStackServiceInterface::OPENSTACK_NOVA_API_VERSION;
      }

      $response = $this->getClient($credentials);

      $success = $response['status_code'] === 204 || $response['status_code'] === 201 || $response['status_code'] === 200;
      if (!$success) {
        break;
      }
    }

    return $success;
  }

  /**
   * Register uid tag in the OpenStack resource's Metadata.
   *
   * Basically, the uid is registered in Tags.
   * But if the OpenStack resource's has no Tags, it is registered in Metadata.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $endpoint
   *   The endpoint.
   * @param array $tags
   *   The tags.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  private function setUidToMetadata(string $entity_type, string $endpoint, array $tags): bool {
    $request_tags = [];
    foreach ($tags as $tag) {
      $request_tags['metadata'][$tag['Key']] = (string) $tag['Value'];
    }

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $endpoint,
      'params' => $request_tags,
    ];

    if ($entity_type === 'openstack_instance') {
      $credentials['X-OpenStack-Nova-API-Version'] = OpenStackServiceInterface::OPENSTACK_NOVA_API_VERSION;
    }

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 || $response['status_code'] === 201 || $response['status_code'] === 200;
  }

  /**
   * Create Tags.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The Tags create API response.
   */
  public function createTags(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params]);

      return ['SendToWorker' => TRUE];
    }

    if (empty($params['EntityType'])) {
      return NULL;
    }

    $endpoint = $this->getTagsEndpointUrl($params);

    // If the endpoint doesn't exist, skip.
    if (empty($endpoint[$params['EntityType']])) {
      return NULL;
    }

    $success = TRUE;
    if (empty($params['Tags'])) {
      return NULL;
    }
    $tags = empty($params['Tags']) ? [] : $params['Tags'];

    $success = NULL;
    if (empty($params['EntityType'])) {
      return NULL;
    }

    $success = in_array($params['EntityType'], OpenStackServiceInterface::RESOURCES_THAT_STORE_UID_IN_OPENSTACK_METADATA)
      ? $this->setUidToMetadata($params['EntityType'], $endpoint[$params['EntityType']], $tags)
      : $this->setUidToTags($params['EntityType'], $endpoint[$params['EntityType']], $tags);

    return $success ? ['Success' => TRUE] : NULL;
  }

  /**
   * Get instance tags.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The instance tag list API response.
   */
  public function getInstanceTags(array $params = []): array {

    $credentials = [
      'http_method' => 'GET',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}"),
      'X-OpenStack-Nova-API-Version' => OpenStackServiceInterface::OPENSTACK_NOVA_API_VERSION,
    ];

    $response = $this->getClient($credentials);

    $result = $response['body']['server']['metadata'] ?? [];

    $tags = [];
    if (empty($result)) {
      return $tags;
    }

    $uid_key_name = $this->getTagKeyCreatedByUid(
      'openstack',
      $this->cloudContext,
    );

    $tags = [
      [
        'Key' => $uid_key_name,
        'Value' => !empty($result[$uid_key_name]) ? intval($result[$uid_key_name]) : 0,
      ],
    ];

    // Add termination protection.
    if (array_key_exists('termination_protection', $result)) {
      $tags[] = [
        'Key' => 'termination_protection',
        'Value' => !empty($result['termination_protection']) ? 1 : 0,
      ];
    }

    return $tags;
  }

  /**
   * Create Tags.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The endpoint URL array.
   */
  public function getTagsEndpointUrl(array $params = []): array {
    $project_id = $this->credentials['project_id'] ?? NULL;

    return [
      'openstack_image'             => $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, "/images/{$params['Resources'][0]}/tags"),
      'openstack_network'           => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/networks/{$params['Resources'][0]}/tags"),
      'openstack_subnet'            => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/subnets/{$params['Resources'][0]}/tags"),
      'openstack_port'              => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/ports/{$params['Resources'][0]}/tags"),
      'openstack_router'            => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/routers/{$params['Resources'][0]}/tags"),
      'openstack_security_group'    => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/security-groups/{$params['Resources'][0]}/tags"),
      'openstack_floating_ip'       => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/floatingips/{$params['Resources'][0]}/tags"),
      'openstack_instance'          => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['Resources'][0]}/metadata"),
      'openstack_volume'            => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/volumes/{$params['Resources'][0]}/metadata"),
      'openstack_snapshot'          => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/snapshots/{$params['Resources'][0]}/metadata"),
    ];
  }

  /**
   * Get delete tags endpoints.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The endpoint URL array.
   */
  public function getDeleteTagsEndpointUrl(array $params = []): array {
    $urls = [];

    $endpoints = [
      'openstack_image' => [
        'service' => OpenStackServiceInterface::IMAGE_SERVICE,
        'path' => '/images/{{ImageId}}/tags/{{Tag}}',
        'path_params' => [
          'ImageId',
          'Tag',
        ],
      ],
      'openstack_security_group' => [
        'service' => OpenStackServiceInterface::NETWORK_SERVICE,
        'path' => '/security-groups/{{SecurityGroupId}}/tags/',
        'path_params' => [
          'SecurityGroupId',
        ],
      ],
      'openstack_floating_ip' => [
        'service' => OpenStackServiceInterface::NETWORK_SERVICE,
        'path' => '/floatingips/{{FloatingIpId}}/tags/',
        'path_params' => [
          'FloatingIpId',
        ],
      ],
      'openstack_instance' => [
        'service' => OpenStackServiceInterface::COMPUTE_SERVICE,
        'path' => '/servers/{{InstanceId}}/metadata/{{UidKeyName}}',
        'path_params' => [
          'InstanceId',
          'UidKeyName',
        ],
      ],
      'openstack_network' => [
        'service' => OpenStackServiceInterface::NETWORK_SERVICE,
        'path' => '/networks/{{NetworkId}}/tags/',
        'path_params' => [
          'NetworkId',
        ],
      ],
      'openstack_subnet' => [
        'service' => OpenStackServiceInterface::NETWORK_SERVICE,
        'path' => '/subnets/{{SubnetId}}/tags/',
        'path_params' => [
          'SubnetId',
        ],
      ],
      'openstack_port' => [
        'service' => OpenStackServiceInterface::NETWORK_SERVICE,
        'path' => '/ports/{{PortId}}/tags/',
        'path_params' => [
          'PortId',
        ],
      ],
      'openstack_router' => [
        'service' => OpenStackServiceInterface::NETWORK_SERVICE,
        'path' => '/routers/{{RouterId}}/tags/',
        'path_params' => [
          'RouterId',
        ],
      ],
      'openstack_snapshot' => [
        'service' => OpenStackServiceInterface::VOLUME_SERVICE,
        'path' => '{{ProjectId}}/snapshots/{{SnapshotId}}/metadata/{{UidKeyName}}',
        'path_params' => [
          'SnapshotId',
          'UidKeyName',
          'ProjectId',
        ],
      ],
      'openstack_volume' => [
        'service' => OpenStackServiceInterface::VOLUME_SERVICE,
        'path' => '/{{ProjectId}}/volumes/{{VolumeId}}/metadata/{{UidKeyName}}',
        'path_params' => [
          'VolumeId',
          'UidKeyName',
          'ProjectId',
        ],
      ],
    ];

    foreach ($endpoints as $entity_type => $endpoint) {
      // If some path parameters aren't in $params, skip.
      if (!empty(array_diff($endpoint['path_params'], array_keys($params)))) {
        continue;
      }

      $urls[$entity_type] = $this->getEndpoint($endpoint['service'], $this->twig->renderInline($endpoint['path'], $params));
    }

    return $urls;
  }

  /**
   * Delete image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The image delete API response.
   */
  public function deregisterImage(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateImages');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'DELETE',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IMAGE_SERVICE, "/images/{$params['ImageId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Get key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The key pairs list API response.
   */
  public function describeKeyPairs(array $params = []) {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'GET',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, '/os-keypairs'),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);

    $key_pairs = $response['body']['keypairs'] ?? [];

    $results['KeyPairs'] = [];
    foreach ($key_pairs ?: [] as $key => $key_pair) {
      $results['KeyPairs'][$key]['KeyName'] = $key_pair['keypair']['name'];
      $results['KeyPairs'][$key]['KeyFingerprint'] = $key_pair['keypair']['fingerprint'];
      $results['KeyPairs'][$key]['KeyMaterial'] = $key_pair['keypair']['public_key'];
    }

    return $results;
  }

  /**
   * Create key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The key pairs create API response.
   */
  public function createKeyPair(array $params = []) {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'createKeyPairEntity');

      return ['SendToWorker' => TRUE];
    }

    $body = [
      'keypair' => [
        'name' => $params['KeyName'],
      ],
    ];

    $credentials = [
      'http_method' => 'POST',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, '/os-keypairs'),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    return !empty($response)
      && !empty($response['body'])
      && !empty($response['body']['keypair'])
      ? [
        'KeyPairId' => NULL,
        'KeyName' => $response['body']['keypair']['name'],
        'KeyFingerprint' => $response['body']['keypair']['fingerprint'],
        'KeyMaterial' => $response['body']['keypair']['public_key'],
      ]
      : [];
  }

  /**
   * {@inheritdoc}
   */
  public function createKeyPairEntity(array $params): void {
    $result = $params['result'];
    $uid = $this->getUidTagValue($result, 'openstack_key_pair');
    $timestamp = time();

    $entity = OpenStackKeyPair::create([
      'cloud_context' => $this->cloudContext,
      'key_pair_name' => $result['KeyName'],
      'key_pair_id' => $result['KeyPairId'],
      'key_fingerprint' => $result['KeyFingerprint'],
      'key_material' => $result['KeyMaterial'],
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $uid,
    ]);
    $entity->save();
  }

  /**
   * Import key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The key pairs import API response.
   */
  public function importKeyPair(array $params = []) {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params]);

      return ['SendToWorker' => TRUE];
    }

    $body = [
      'keypair' => [
        'name' => $params['KeyName'],
        'public_key' => $params['PublicKeyMaterial'],
      ],
    ];

    $credentials = [
      'http_method' => 'POST',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, '/os-keypairs'),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 200
      && !empty($response)
      && !empty($response['body'])
      && !empty($response['body']['keypair'])
      ? [
        'KeyPairId' => NULL,
        'KeyName' => $response['body']['keypair']['name'],
        'KeyFingerprint' => $response['body']['keypair']['fingerprint'],
        'KeyMaterial' => $response['body']['keypair']['public_key'],
      ]
      : [];
  }

  /**
   * Delete key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The result.
   */
  public function deleteKeyPair(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params]);

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'DELETE',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/os-keypairs/{$params['KeyName']}"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);

    return ($response['status_code'] === 202 || $response['status_code'] === 204) ? ['Success' => TRUE] : NULL;
  }

  /**
   * Get volume list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume list API response.
   */
  public function describeVolumes(array $params = []) {

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/volumes/detail"),
    ];
    $response = $this->getClient($credentials);

    $volumes = $response['body']['volumes'] ?? [];

    $results['Volumes'] = [];

    foreach ($volumes ?: [] as $key => $value) {
      $attachments = [];
      if (!empty($value['attachments'])) {
        foreach ($value['attachments'] as $attachment_key => $attachment) {
          $attachments[$attachment_key]['Device'] = $attachment['device'];
          $attachments[$attachment_key]['InstanceId'] = $attachment['server_id'];
          $attachments[$attachment_key]['VolumeId'] = $attachment['volume_id'];
        }
      }
      $tags = [];
      if (!empty($value['metadata'])) {
        foreach ($value['metadata'] as $tag_key => $tag_value) {
          $tags[] = "{$tag_key}.{$tag_value}";
        }
      }

      $results['Volumes'][$key]['Attachments'] = $attachments;
      $results['Volumes'][$key]['Name'] = $value['name'];
      $results['Volumes'][$key]['Description'] = $value['description'];
      $results['Volumes'][$key]['VolumeId'] = $value['id'];
      $results['Volumes'][$key]['State'] = $value['status'];
      $results['Volumes'][$key]['CreateTime'] = str_replace("T", " ", $value['created_at']);
      $results['Volumes'][$key]['SnapshotId'] = $value['snapshot_id'];
      $results['Volumes'][$key]['VolumeType'] = $value['volume_type'];
      $results['Volumes'][$key]['Size'] = $value['size'];
      $results['Volumes'][$key]['VirtualizationType'] = $value['replication_status'];
      $results['Volumes'][$key]['AvailabilityZone'] = $value['availability_zone'];
      $results['Volumes'][$key]['Encrypted'] = $value['encrypted'];
      $results['Volumes'][$key]['Tags'] = $tags;
    }

    return $results;
  }

  /**
   * Get volume types.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume list API response.
   */
  public function describeVolumeTypes(array $params = []) {

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/types"),
    ];
    $response = $this->getClient($credentials);

    $types = [];
    array_map(static function ($type) use (&$types) {
      return $types[$type] = $type;
    }, array_column($response['body']['volume_types'], 'name'));

    return $types;
  }

  /**
   * Get a default volume type.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return string
   *   The volume list API response.
   */
  public function getDefaultVolumeType(array $params = []) {

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/types/default"),
    ];
    $response = $this->getClient($credentials);

    return !empty($response['body']['volume_type']['name'])
      ? $response['body']['volume_type']['name']
      : '';
  }

  /**
   * Create volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume create API response.
   */
  public function createVolume(array $params = []) {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateVolumes');

      return ['SendToWorker' => TRUE];
    }

    $body['volume'] = [
      'name' => $params['Name'],
      'size' => $params['Size'],
      'volume_type' => $params['VolumeType'],
      'availability_zone' => $params['AvailabilityZone'],
      'description' => !empty($params['Description']) ? $params['Description'] : '',
    ];

    if (!empty($params['SnapshotId'])) {
      $body['volume'] += ['snapshot_id' => $params['SnapshotId']];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/volumes"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $volume = $response['body']['volume'] ?? [];

    $attachments = [];

    if (!empty($volume['attachments'])) {
      foreach ($volume['attachments'] ?: [] as $attachment_key => $attachment) {
        $attachments[$attachment_key]['Device'] = $attachment['device'];
        $attachments[$attachment_key]['InstanceId'] = $attachment['server_id'];
        $attachments[$attachment_key]['VolumeId'] = $attachment['volume_id'];
      }
    }

    return [
      'Attachments'        => $attachments,
      'Name'               => $volume['name'] ?? NULL,
      'Description'        => $volume['description'] ?? NULL,
      'VolumeId'           => $volume['id'] ?? NULL,
      'State'              => $volume['status'] ?? NULL,
      'CreateTime'         => !empty($volume['created_at']) ? str_replace('T', ' ', $volume['created_at']) : NULL,
      'SnapshotId'         => $volume['snapshot_id'] ?? NULL,
      'VolumeType'         => $volume['volume_type'] ?? NULL,
      'Size'               => $volume['size'] ?? NULL,
      'VirtualizationType' => $volume['replication_status'] ?? NULL,
      'AvailabilityZone'   => $volume['availability_zone'] ?? NULL,
      'Encrypted'          => $volume['encrypted'] ?? NULL,
    ];
  }

  /**
   * Update volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume update API response.
   */
  public function updateVolume(array $params = []) {

    $body['volume'] = [
      'name' => $params['Name'],
      'description' => !empty($params['Description']) ? $params['Description'] : '',
    ];

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/volumes/{$params['VolumeId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $volume = $response['body']['volume'] ?? [];

    $attachments = [];

    if (!empty($volume['attachments'])) {
      foreach ($volume['attachments'] ?: [] as $attachment_key => $attachment) {
        $attachments[$attachment_key]['Device'] = $attachment['device'];
        $attachments[$attachment_key]['InstanceId'] = $attachment['server_id'];
        $attachments[$attachment_key]['VolumeId'] = $attachment['volume_id'];
      }
    }

    return [
      'Attachments'        => $attachments,
      'Name'               => $volume['name'] ?? NULL,
      'Description'        => $volume['description'] ?? NULL,
      'VolumeId'           => $volume['id'] ?? NULL,
      'State'              => $volume['status'] ?? NULL,
      'CreateTime'         => !empty($volume['created_at']) ? str_replace('T', ' ', $volume['created_at']) : NULL,
      'SnapshotId'         => $volume['snapshot_id'] ?? NULL,
      'VolumeType'         => $volume['volume_type'] ?? NULL,
      'Size'               => $volume['size'] ?? NULL,
      'VirtualizationType' => $volume['replication_status'] ?? NULL,
      'AvailabilityZone'   => $volume['availability_zone'] ?? NULL,
      'Encrypted'          => $volume['encrypted'] ?? NULL,
    ];
  }

  /**
   * Delete volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteVolume(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateVolumes');

      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/volumes/{$params['VolumeId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 202 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Attach volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function attachVolume(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateVolumes');

      return ['SendToWorker' => TRUE];
    }

    $body['volumeAttachment'] = [
      'volumeId' => $params['VolumeId'],
      'device' => $params['Device'],
    ];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/os-volume_attachments"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 200 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Detach volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function detachVolume(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateVolumes');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/os-volume_attachments/{$params['VolumeId']}"),
      'params' => [],
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 202 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Get snapshot list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The snapshot list API response.
   */
  public function describeSnapshots(array $params = []) {

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/snapshots/detail"),
    ];
    $response = $this->getClient($credentials);

    $snapshots = $response['body']['snapshots'] ?? [];

    $results['Snapshots'] = [];

    foreach ($snapshots ?: [] as $key => $snapshot) {
      $tags = [];
      if (!empty($snapshot['metadata'])) {
        foreach ($snapshot['metadata'] as $tag_key => $tag_value) {
          $tags[] = "{$tag_key}.{$tag_value}";
        }
      }
      $results['Snapshots'][$key]['Description'] = $snapshot['description'] ?? NULL;
      $results['Snapshots'][$key]['OwnerId'] = $snapshot['os-extended-snapshot-attributes:project_id'] ?? NULL;
      $results['Snapshots'][$key]['Progress'] = $snapshot['os-extended-snapshot-attributes:progress'] ?? NULL;
      $results['Snapshots'][$key]['name'] = $snapshot['name'] ?? NULL;
      $results['Snapshots'][$key]['SnapshotId'] = $snapshot['id'] ?? NULL;
      $results['Snapshots'][$key]['StartTime'] = $snapshot['created_at'] ?? NULL;
      $results['Snapshots'][$key]['State'] = $snapshot['status'] ?? NULL;
      $results['Snapshots'][$key]['VolumeId'] = $snapshot['volume_id'] ?? NULL;
      $results['Snapshots'][$key]['VolumeSize'] = $snapshot['size'] ?? NULL;
      $results['Snapshots'][$key]['Tags'] = $tags;
    }

    return $results;
  }

  /**
   * Create snapshot.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The snapshot create API response.
   */
  public function createSnapshot(array $params = []) {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSnapshots');

      return ['SendToWorker' => TRUE];
    }

    $body['snapshot'] = [
      'name' => $params['Name'],
      'description' => $params['Description'],
      'volume_id' => $params['VolumeId'],
    ];

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/snapshots"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $snapshot = $response['body']['snapshot'] ?? [];

    return [
      'Description' => $snapshot['description'] ?? NULL,
      'OwnerId'     => $snapshot['os-extended-snapshot-attributes:project_id'] ?? NULL,
      'Progress'    => $snapshot['os-extended-snapshot-attributes:progress'] ?? NULL,
      'name'        => $snapshot['name'] ?? NULL,
      'SnapshotId'  => $snapshot['id'] ?? NULL,
      'StartTime'   => !empty($snapshot['created_at']) ? $snapshot['created_at'] : NULL,
      'State'       => $snapshot['status'] ?? NULL,
      'VolumeId'    => $snapshot['volume_id'] ?? NULL,
      'VolumeSize'  => $snapshot['size'] ?? NULL,
      'Tags'        => $snapshot['metadata'] ?? [],
    ];
  }

  /**
   * Update snapshot.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The snapshot update API response.
   */
  public function updateSnapshot(array $params = []) {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSnapshots');

      return ['SendToWorker' => TRUE];
    }

    $body['snapshot'] = [
      'name' => $params['Name'],
      'description' => $params['Description'],
    ];

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/snapshots/{$params['SnapshotId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $snapshot = $response['body']['snapshot'] ?? [];

    return [
      'Description' => $snapshot['description'] ?? NULL,
      'OwnerId'     => $snapshot['os-extended-snapshot-attributes:project_id'] ?? NULL,
      'Progress'    => $snapshot['os-extended-snapshot-attributes:progress'] ?? NULL,
      'name'        => $snapshot['name'] ?? NULL,
      'SnapshotId'  => $snapshot['id'] ?? NULL,
      'StartTime'   => $snapshot['created_at'] ?? NULL,
      'State'       => $snapshot['status'] ?? NULL,
      'VolumeId'    => $snapshot['volume_id'] ?? NULL,
      'VolumeSize'  => $snapshot['size'] ?? NULL,
      'Tags'        => $snapshot['metadata'] ?? [],
    ];
  }

  /**
   * Delete snapshot.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteSnapshot(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSnapshots');

      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/snapshots/{$params['SnapshotId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 202 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Get flavors.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The flavor list API response.
   */
  public function describeFlavors(array $params = []) {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, '/flavors/detail'),
      'params' => $params,
    ];

    $response = $this->getClient($credentials);

    return $response['body']['flavors'] ?? [];
  }

  /**
   * Get the flavor detail of specific flavor ID.
   *
   * @param array $params
   *   Parameters to pass to the API.
   * @param string $flavor_id
   *   The flavor ID.
   *
   * @return array
   *   The flavor list API response.
   */
  public function describeFlavorById(array $params = [], $flavor_id = ''): array {

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/flavors/{$flavor_id}"),
      'params' => $params,
    ];

    $response = $this->getClient($credentials);

    return !empty($response['body']['flavor']) ? $response['body']['flavor'] : [];
  }

  /**
   * Get the volume detail of specific volume ID.
   *
   * @param array $params
   *   Parameters to pass to the API.
   * @param string $volume_id
   *   The volume ID.
   *
   * @return array
   *   The volume list API response.
   */
  public function describeVolumeById(array $params = [], $volume_id = '') {

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/volumes/{$volume_id}"),
      'params' => $params,
    ];

    $response = $this->getClient($credentials);

    return $response['body']['volume'] ?? [];
  }

  /**
   * Get security group list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group list API response.
   */
  public function describeSecurityGroups(array $params = []) {

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'] ?? NULL;

    $endpoint = !empty($params['id'])
      ? $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/security-groups?project_id={$project_id}&id={$params['id']}")
      : $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/security-groups?project_id={$project_id}");

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $endpoint,
      'params' => $params,
    ];
    $response = $this->getClient($credentials);

    $security_groups = $response['body']['security_groups'] ?? [];

    $results['SecurityGroups'] = [];

    foreach ($security_groups ?: [] as $key => $value) {

      $ip_permission_cnt = $ip_permission_egress_cnt = 0;

      $ip_permissions = $ip_permissions_egress = [];

      $security_group_rules = [];
      if (!empty($value['security_group_rules'])) {
        $security_group_rules = $value['security_group_rules'];
      }

      foreach ($security_group_rules ?: [] as $value_rules) {
        if (!empty($value_rules['direction']) && $value_rules['direction'] === 'egress') {

          $ip_permissions_egress[$ip_permission_egress_cnt] = [
            'RuleId'     => $value_rules['id'] ?? NULL,
            'FromPort'   => $value_rules['port_range_min'] ?? ($value_rules['protocol'] === 'icmp' ? '-1' : 1),
            'IpProtocol' => $value_rules['protocol'] ?? '-1',
            'ToPort'     => $value_rules['port_range_max'] ?? ($value_rules['protocol'] === 'icmp' ? '-1' : 65535),
          ];

          if (!empty($value_rules['remote_group_id'])) {
            $ip_permissions_egress[$ip_permission_egress_cnt] += [
              'UserIdGroupPairs' => [
                [
                  'GroupId' => $value_rules['remote_group_id'] ?? NULL,
                  'UserId'  => $value_rules['tenant_id'] ?? NULL,
                ],
              ],
            ];
          }

          if ((!empty($value_rules['ethertype'])
            && $value_rules['ethertype'] === 'IPv6')
            && empty($value_rules['remote_group_id'])) {
            $ip_permissions_egress[$ip_permission_egress_cnt] += [
              'Ipv6Ranges' => [
                [
                  'CidrIpv6'    => !empty($value_rules['remote_ip_prefix']) ? $value_rules['remote_ip_prefix'] : '::/0',
                  'Description' => !empty($value_rules['description']) ? $value_rules['description'] : '',
                ],
              ],
            ];
          }

          if ((!empty($value_rules['ethertype'])
            && $value_rules['ethertype'] === 'IPv4')
            && empty($value_rules['remote_group_id'])) {
            $ip_permissions_egress[$ip_permission_egress_cnt] += [
              'IpRanges' => [
                [
                  'CidrIp'      => !empty($value_rules['remote_ip_prefix']) ? $value_rules['remote_ip_prefix'] : '0.0.0.0/0',
                  'Description' => !empty($value_rules['description']) ? $value_rules['description'] : '',
                ],
              ],
            ];
          }

          $ip_permission_egress_cnt++;

        }
        else {
          $ip_permissions[$ip_permission_cnt] = [
            'RuleId'     => $value_rules['id'] ?? NULL,
            'FromPort'   => $value_rules['port_range_min'] ?? ($value_rules['protocol'] === 'icmp' ? '-1' : 1),
            'IpProtocol' => $value_rules['protocol'] ?? '-1',
            'ToPort'     => $value_rules['port_range_max'] ?? ($value_rules['protocol'] === 'icmp' ? '-1' : 65535),
          ];

          if (!empty($value_rules['remote_group_id'])) {
            $ip_permissions[$ip_permission_cnt] += [
              'UserIdGroupPairs' => [
                [
                  'GroupId' => $value_rules['remote_group_id'] ?? NULL,
                  'UserId'  => $value_rules['tenant_id'] ?? NULL,
                ],
              ],
            ];
          }

          if (!empty($value_rules['ethertype'])
            && $value_rules['ethertype'] === 'IPv6'
            && empty($value_rules['remote_group_id'])) {
            $ip_permissions[$ip_permission_cnt] += [
              'Ipv6Ranges' => [
                [
                  'CidrIpv6'    => !empty($value_rules['remote_ip_prefix']) ? $value_rules['remote_ip_prefix'] : '::/0',
                  'Description' => !empty($value_rules['description']) ? $value_rules['description'] : '',
                ],
              ],
            ];
          }
          if (!empty($value_rules['ethertype'])
            && $value_rules['ethertype'] === 'IPv4'
            && empty($value_rules['remote_group_id'])) {
            $ip_permissions[$ip_permission_cnt] += [
              'IpRanges' => [
                [
                  'CidrIp'      => !empty($value_rules['remote_ip_prefix']) ? $value_rules['remote_ip_prefix'] : '0.0.0.0/0',
                  'Description' => !empty($value_rules['description']) ? $value_rules['description'] : '',
                ],
              ],
            ];
          }

          $ip_permission_cnt++;
        }
      }

      $results['SecurityGroups'][$key] = [
        'Description'         => $value['description'] ?? NULL,
        'GroupName'           => $value['name'] ?? NULL,
        'IpPermissions'       => $ip_permissions,
        'OwnerId'             => $value['tenant_id'] ?? NULL,
        'GroupId'             => $value['id'] ?? NULL,
        'IpPermissionsEgress' => $ip_permissions_egress,
        'Tags'                => $value['tags'] ?? [],
      ];
    }

    return $results;
  }

  /**
   * Create security group list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group create API response.
   */
  public function createSecurityGroup(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSecurityGroups');

      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $body['security_group'] = [
      'name' => $params['GroupName'],
      'description' => $params['Description'],
    ];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/security-groups?project_id={$project_id}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $security_group = $response['body']['security_group'] ?? [];

    return [
      'Description'         => $security_group['description'] ?? NULL,
      'GroupName'           => $security_group['name'] ?? NULL,
      'IpPermissions'       => [],
      'OwnerId'             => $security_group['tenant_id'] ?? NULL,
      'GroupId'             => $security_group['id'] ?? NULL,
      'IpPermissionsEgress' => [],
      'Tags'                => $security_group['tags'] ?? [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function updateSecurityGroup(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSecurityGroups');

      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $body['security_group'] = [
      'name' => $params['GroupName'],
    ];

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/security-groups/{$params['GroupId']}?project_id={$project_id}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $security_group = $response['body']['security_group'] ?? [];

    return [
      'Description'         => $security_group['description'] ?? NULL,
      'GroupName'           => $security_group['name'] ?? NULL,
      'IpPermissions'       => [],
      'OwnerId'             => $security_group['tenant_id'] ?? NULL,
      'GroupId'             => $security_group['id'] ?? NULL,
      'IpPermissionsEgress' => [],
      'Tags'                => $security_group['tags'] ?? [],
    ];
  }

  /**
   * Delete security group List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteSecurityGroup(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSecurityGroups');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/security-groups/{$params['GroupId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createSecurityGroupRule(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSecurityGroups', ['GroupIds' => [$params['GroupId']]]);

      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $cidr_ip = !empty($params['CidrIp']) ? $params['CidrIp'] : NULL;
    $remote_ip = !empty($params['CidrIpv6']) ? $params['CidrIpv6'] : $cidr_ip;
    $remote_group_id = !empty($params['RemoteGroupId']) ? $params['RemoteGroupId'] : NULL;

    $body = [];
    $body['security_group_rule'] = [
      'direction'         => $params['Direction'],
      'port_range_min'    => $params['FromPort'],
      'port_range_max'    => $params['ToPort'],
      'ethertype'         => $params['Source'],
      'protocol'          => $params['IpProtocol'],
      'remote_group_id'   => $remote_group_id,
      'security_group_id' => $params['GroupId'],
      'remote_ip_prefix'  => $remote_ip,
      'description'       => $params['Description'],
    ];

    // If 'All ICMP' is selected, port range must not be sent.
    $well_known_ports = OpenStackServiceInterface::WELL_KNOWN_PORTS;
    $all_icmp = $well_known_ports['all_icmp'];
    if ($params['IpProtocol'] === $all_icmp['ip_protocol'] && $params['FromPort'] === $all_icmp['from_port'] && $params['ToPort'] === $all_icmp['to_port']) {
      unset($body['security_group_rule']['port_range_min']);
      unset($body['security_group_rule']['port_range_max']);
    }

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/security-group-rules?project_id={$project_id}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $security_group_rule = $response['body']['security_group_rule'] ?? [];

    $ip_permissions = $ip_permissions_egress = [];

    if (!empty($security_group_rule['direction'])
      && $security_group_rule['direction'] === 'egress') {
      $ip_permissions_egress = [
        'RuleId'     => $security_group_rule['id'] ?? NULL,
        'FromPort'   => $security_group_rule['port_range_min'] ?? 1,
        'IpProtocol' => $security_group_rule['protocol'] ?? '-1',
        'ToPort'     => $security_group_rule['port_range_max'] ?? 65535,
      ];

      if (!empty($security_group_rule['remote_group_id'])) {
        $ip_permissions_egress += [
          'UserIdGroupPairs' => [
            [
              'GroupId' => $security_group_rule['remote_group_id'] ?? NULL,
              'UserId'  => $security_group_rule['tenant_id'] ?? NULL,
            ],
          ],
        ];
      }

      if ((!empty($security_group_rule['ethertype'])
        && $security_group_rule['ethertype'] === 'IPv6')
        && empty($security_group_rule['remote_group_id'])) {
        $ip_permissions_egress += [
          'Ipv6Ranges' => [
            [
              'CidrIpv6'    => $security_group_rule['remote_ip_prefix'] ?? NULL,
              'Description' => $security_group_rule['description'] ?? NULL,
            ],
          ],
        ];
      }
      if ((!empty($security_group_rule['ethertype'])
        && $security_group_rule['ethertype'] === 'IPv4')
        && empty($security_group_rule['remote_group_id'])) {
        $ip_permissions_egress += [
          'IpRanges' => [
            [
              'CidrIp'      => $security_group_rule['remote_ip_prefix'] ?? NULL,
              'Description' => $security_group_rule['description'] ?? NULL,
            ],
          ],
        ];
      }
    }
    else {
      $ip_permissions = [
        'RuleId'     => $security_group_rule['id'] ?? NULL,
        'FromPort'   => $security_group_rule['port_range_min'] ?? 1,
        'IpProtocol' => $security_group_rule['protocol'] ?? '-1',
        'ToPort'     => $security_group_rule['port_range_max'] ?? 65535,
      ];

      if (!empty($security_group_rule['remote_group_id'])) {
        $ip_permissions += [
          'UserIdGroupPairs' => [
            [
              'GroupId' => $security_group_rule['remote_group_id'] ?? NULL,
              'UserId'  => $security_group_rule['tenant_id'] ?? NULL,
            ],
          ],
        ];
      }

      if ((!empty($security_group_rule['ethertype'])
        && $security_group_rule['ethertype'] === 'IPv6')
        && empty($security_group_rule['remote_group_id'])) {
        $ip_permissions += [
          'Ipv6Ranges' => [
            [
              'CidrIpv6'    => $security_group_rule['remote_ip_prefix'] ?? NULL,
              'Description' => $security_group_rule['description'] ?? NULL,
            ],
          ],
        ];
      }
      if ((!empty($security_group_rule['ethertype'])
        && $security_group_rule['ethertype'] === 'IPv4')
        && empty($security_group_rule['remote_group_id'])) {
        $ip_permissions += [
          'IpRanges' => [
            [
              'CidrIp'      => $security_group_rule['remote_ip_prefix'] ?? NULL,
              'Description' => $security_group_rule['description'] ?? NULL,
            ],
          ],
        ];
      }
    }

    $this->addWarningForSecurityGroupRule($params);

    return [
      'RuleId'              => $security_group_rule['id'] ?? NULL,
      'SecurityGroups'      => ['ip_permissions' => $ip_permissions],
      'IpPermissionsEgress' => $ip_permissions_egress,
    ];
  }

  /**
   * Add warning message for security group rule.
   *
   * @param array $params
   *   The parameters.
   */
  private function addWarningForSecurityGroupRule(array $params): void {
    if ($params['Direction'] !== 'ingress') {
      return;
    }

    if ($params['FromPort'] !== $params['ToPort']) {
      return;
    }

    if (!in_array(intval($params['FromPort']), self::SECURITY_GROUP_RULE_TARGET_PORTS)) {
      return;
    }

    if ($params['Source'] === 'IPv4' && $params['CidrIp'] === self::ANY_IP['IPv4']) {
      $this->messenger->addWarning($this->t('Using 0.0.0.0/0 for %port is not recommended because it makes the network too open.', [
        '%port' => $params['FromPort'],
      ]));
      return;
    }

    if ($params['Source'] === 'IPv6' && $params['CidrIpv6'] === self::ANY_IP['IPv6']) {
      $this->messenger->addWarning($this->t('Using ::/0 is for %port not recommended because it makes the network too open.', [
        '%port' => $params['FromPort'],
      ]));
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function revokeSecurityGroupRule(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSecurityGroups');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/security-group-rules/{$params['RuleId']}"),
    ];

    $response = $this->getClient($credentials);

    return ['Success' => ($response['status_code'] === 204)];
  }

  /**
   * Get the subnet detail.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The subnet detail list API response.
   */
  public function describeSubnetDetail(array $params = []) {

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/subnets/{$params['SubnetId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['body']['subnet'] ?? [];
  }

  /**
   * Get Floating IP List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP list API response.
   */
  public function describeAddresses(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/floatingips?project_id={$project_id}"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);

    $floating_ips = $response['body']['floatingips'] ?? [];

    $results['Addresses'] = [];

    foreach ($floating_ips ?: [] as $key => $value) {
      $results['Addresses'][$key] = [
        'FloatingIpId'     => $value['id'],
        'PublicIp'         => $value['floating_ip_address'] ?? NULL,
        'PrivateIpAddress' => $value['fixed_ip_address'] ?? NULL,
        'NetworkOwnerId'   => $value['tenant_id'] ?? NULL,
        'NetworkId'        => $value['port_details'] ? $value['port_details']['network_id'] : NULL,
        'InstanceId'       => $value['port_details'] ? $value['port_details']['device_id'] : NULL,
        'AssociationId'    => $value['port_id'] ?? NULL,
        'AllocationId'     => $value['floating_network_id'] ?? NULL,
        'Tags'             => $value['tags'] ?? [],
      ];
    }
    return $results;
  }

  /**
   * Create Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP create API response.
   */
  public function allocateAddress(array $params = []) {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params]);
      return ['SendToWorker' => TRUE];
    }

    $body['floatingip'] = [
      'floating_network_id' => $params['AllocationId'],
    ];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/floatingips'),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if (empty($response) || empty($response['status_code']) || $response['status_code'] !== 201) {
      return [];
    }

    $floating_ip = $response['body']['floatingip'] ?? [];

    return [
      'FloatingIpId'     => $floating_ip['id'] ?? NULL,
      'PublicIp'         => $floating_ip['floating_ip_address'] ?? NULL,
      'PrivateIpAddress' => $floating_ip['fixed_ip_address'] ?? NULL,
      'NetworkOwnerId'   => $floating_ip['tenant_id'] ?? NULL,
      'NetworkId'        => $floating_ip['port_details'] ? $floating_ip['port_details']['network_id'] : NULL,
      'InstanceId'       => $floating_ip['port_details'] ? $floating_ip['port_details']['device_id'] : NULL,
      'Domain'           => $floating_ip['dns_domain'] ?? NULL,
      'AssociationId'    => $floating_ip['port_id'] ?? NULL,
      'AllocationId'     => $floating_ip['floating_network_id'] ?? NULL,
      'Tags'             => $floating_ip['tags'] ?? [],
    ];
  }

  /**
   * Associate Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP associate API response.
   */
  public function associateAddress(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params]);
      return ['SendToWorker' => TRUE];
    }

    $body['floatingip'] = [
      'port_id' => $params['PortId'],
    ];

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/floatingips/{$params['FloatingIpId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if (empty($response) || empty($response['status_code']) || $response['status_code'] !== 200) {
      return [];
    }

    $floating_ip = $response['body']['floatingip'] ?? [];

    return [
      'FloatingIpId'     => $floating_ip['id'] ?? NULL,
      'PublicIp'         => $floating_ip['floating_ip_address'] ?? NULL,
      'PrivateIpAddress' => $floating_ip['fixed_ip_address'] ?? NULL,
      'NetworkOwnerId'   => $floating_ip['tenant_id'] ?? NULL,
      'NetworkId'        => !empty($floating_ip['port_details']) ? $floating_ip['port_details']['network_id'] : NULL,
      'InstanceId'       => !empty($floating_ip['port_details']) ? $floating_ip['port_details']['device_id'] : NULL,
      'Domain'           => $floating_ip['dns_domain'] ?? NULL,
      'AssociationId'    => $floating_ip['port_id'] ?? NULL,
      'Tags'             => $floating_ip['tags'] ?? [],
    ];
  }

  /**
   * Disassociate Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP associate API response.
   */
  public function disassociateAddress(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params]);
      return ['SendToWorker' => TRUE];
    }

    $body['floatingip'] = [
      'port_id' => NULL,
    ];

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/floatingips/{$params['FloatingIpId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $floating_ip = $response['body']['floatingip'] ?? [];

    return [
      'FloatingIpId'     => $floating_ip['id'] ?? NULL,
      'PublicIp'         => $floating_ip['floating_ip_address'] ?? NULL,
      'PrivateIpAddress' => $floating_ip['fixed_ip_address'] ?? NULL,
      'NetworkOwnerId'   => $floating_ip['tenant_id'] ?? NULL,
      'NetworkId'        => $floating_ip['port_details'] ? $floating_ip['port_details']['network_id'] : NULL,
      'InstanceId'       => $floating_ip['port_details'] ? $floating_ip['port_details']['device_id'] : NULL,
      'Domain'           => $floating_ip['dns_domain'] ?? NULL,
      'AssociationId'    => $floating_ip['port_id'] ?? NULL,
      'Tags'             => $floating_ip['tags'] ?? [],
    ];
  }

  /**
   * Instance port interface.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The instance port interface list API response.
   */
  public function instancePortInterface(array $params = []): array {

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/os-interface"),
    ];

    $response = $this->getClient($credentials);

    return $response['body']['interfaceAttachments'] ?? [];
  }

  /**
   * Delete Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The Floating IP delete API response.
   */
  public function releaseAddress(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params]);
      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/floatingips/{$params['FloatingIpId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function describeAvailabilityZones(array $params = []) {
    $computeResults = $this->describeComputeAvailabilityZones($params);
    $networkResults = $this->describeNetworkAvailabilityZones();
    $volumeResults = $this->describeVolumeAvailabilityZones();
    $results = array_merge_recursive($computeResults, $networkResults, $volumeResults);

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  private function describeComputeAvailabilityZones(array $params = []) {
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, '/os-availability-zone'),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $availability_zones = $response['body']['availabilityZoneInfo'] ?? [];
    $results['AvailabilityZones'] = [];
    foreach ($availability_zones ?: [] as $key => $value) {
      $results['AvailabilityZones'][$key] = [
        'ZoneName' => $value['zoneName'] ?? NULL,
        'State'   => $value['zoneState']['available'] === TRUE ? 'available' : '',
        'ComponentName' => 'compute',
      ];
    }
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  private function describeNetworkAvailabilityZones() {
    if (!$this->isNetworkExtensionSupported('network_availability_zone') && !$this->isNetworkExtensionSupported('router_availability_zone')) {
      return [];
    }
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/availability_zones'),
    ];
    $response = $this->getClient($credentials);
    $availability_zones = $response['body']['availability_zones'] ?? [];
    $results['AvailabilityZones'] = [];
    foreach ($availability_zones ?: [] as $key => $value) {
      $results['AvailabilityZones'][$key] = [
        'ZoneName' => $value['name'] ?? NULL,
        'State' => $value['state'] ?? NULL,
        'ComponentName' => 'network',
        'zoneResource' => $value['resource'] ?? NULL,
      ];
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  private function describeVolumeAvailabilityZones() {
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "/{$project_id}/os-availability-zone"),
    ];
    $response = $this->getClient($credentials);
    $availability_zones = $response['body']['availabilityZoneInfo'] ?? [];
    $results['AvailabilityZones'] = [];
    foreach ($availability_zones ?: [] as $key => $value) {
      $results['AvailabilityZones'][$key] = [
        'ZoneName' => $value['zoneName'] ?? NULL,
        'State'   => $value['zoneState']['available'] === TRUE ? 'available' : '',
        'ComponentName' => 'volume',
      ];
    }

    return $results;
  }

  /**
   * Check if openStack network extensions are enabled.
   *
   * @return bool
   *   Whether the extension is enabled.
   */
  public function isNetworkExtensionSupported(string $extension_alias): bool {
    $enabled = FALSE;
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/extensions'),
    ];
    $response = $this->getClient($credentials);
    $enableExtensions = $response['body']['extensions'] ?? [];
    foreach ($enableExtensions ?: [] as $extension) {
      if ($extension['alias'] == $extension_alias) {
        $enabled = TRUE;
        break;
      }
    }

    return $enabled;
  }

  /**
   * Call the API for updated entities and store them as instance entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateInstanceEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = ''): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    // Call the api and get all instances.
    $result = $this->describeInstances($params);
    if ($result !== NULL) {
      $all_instances = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the instances by setting up
      // the array with the instance_id.
      foreach ($all_instances ?: [] as $instance) {
        $stale[$instance->getInstanceId()] = $instance;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Instance Update');
      // Loop through the reservations and store each one as an instance entity.
      // Testcase throws an undefined index: Reservations error to resolve
      // that error needs to be added into the isset condition.
      if (isset($result['Reservations'])) {
        foreach ($result['Reservations'] ?: [] as $reservation) {

          foreach ($reservation['Instances'] ?: [] as $instance) {
            // Keep track of instances that do not exist anymore
            // delete them after saving the rest of the instances.
            if (!empty($stale[$instance['InstanceId']])) {
              unset($stale[$instance['InstanceId']]);
            }
            // Store the Reservation OwnerId in instance so batch
            // callback has access.
            $instance['reservation_ownerid'] = $reservation['OwnerId'] ?? NULL;
            $instance['reservation_id'] = $reservation['ReservationId'] ?? NULL;

            $batch_builder->addOperation([
              OpenStackBatchOperations::class,
              'updateInstance',
            ], [$cloud_context, $instance]);

          }
        }
      }
      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Update the instances.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param bool $clear
   *   TRUE to clear stale instances.
   *
   * @return bool
   *   Indicates success or failure.
   */
  public function updateInstances(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_instance';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateInstanceEntities($params, $clear, $entity_type, $this->cloudContext);
    $this->lock->release($lock_name);

    return $updated;
  }

  /**
   * Update the OpenStackImages.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param bool $save_operation
   *   TRUE to save operation and not to execute.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateImages(array $params = [], $clear = TRUE, $save_operation = FALSE): bool {
    $entity_type = 'openstack_image';
    $lock_name = $this->getLockKey($entity_type);
    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateImageEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as image entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateImageEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = ''): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;

    // Load all entities by $this->cloudContext.
    $image_entities = $this->entityTypeManager->getStorage($entity_type)->loadByProperties(
      ['cloud_context' => $this->cloudContext]
    );

    $result = $this->describeImages($params);

    // Keep isset(), and do not replace !empty() here. We need to check for
    // instance testcase notice because the instance testcase throws an error
    // of undefined index notice.
    if (isset($result)) {

      $stale = [];
      // Make it easier to look up the images by setting up the array with the
      // image_id.
      foreach ($image_entities ?: [] as $image) {
        $stale[$image->getImageId()] = $image;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Image Update');

      if (!empty($result['Images'])) {
        foreach ($result['Images'] ?: [] as $image) {
          // Keep track of images that do not exist anymore
          // delete them after saving the rest of the images.
          if (!empty($stale[$image['ImageId']])) {
            unset($stale[$image['ImageId']]);
          }

          $batch_builder->addOperation([
            OpenStackBatchOperations::class,
            'updateImage',
          ], [$cloud_context, $image]);
        }
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as key pair entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   Key name info.
   * @param bool $update
   *   TRUE to update Key info.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateKeyPairEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $result = $this->describeKeyPairs();

    if (!empty($result)) {

      $all_keys = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the groups by setting up the array with the
      // group_id.
      foreach ($all_keys ?: [] as $key) {
        $stale[$key->getKeyPairName()] = $key;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Keypair Update');

      foreach ($result['KeyPairs'] ?: [] as $key_pair) {
        // Keep track of key pair that do not exist anymore
        // delete them after saving the rest of the key pair.
        if (!empty($stale[$key_pair['KeyName']])) {
          unset($stale[$key_pair['KeyName']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateKeyPair',
        ], [$cloud_context, $key_pair]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the OpenStack key pairs.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateKeyPairs(): bool {
    $entity_type = 'openstack_key_pair';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateKeyPairEntities($entity_type, $this->cloudContext);
    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as volume entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   The volume info.
   * @param bool $update
   *   TRUE to update volume.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateVolumeEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $result = $this->describeVolumes();

    $properties = empty($params['VolumeIds'][0])
      ? ['cloud_context' => $cloud_context]
      : [
        'cloud_context' => $cloud_context,
        'volume_id' => $params['VolumeIds'][0],
      ];
    $all_volumes = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the groups by setting up the array with the
    // group_id.
    foreach ($all_volumes ?: [] as $volume) {
      $stale[$volume->getVolumeId()] = $volume;
    }
    $snapshot_id_name_map = $this->getSnapshotIdNameMap(!empty($result['Volumes']) ? $result['Volumes'] : []);

    /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
    $batch_builder = $this->initBatch('Volume update');

    if (!empty($result)) {
      foreach ($result['Volumes'] ?: [] as $volume) {
        // Keep track of network interfaces that do not exist anymore
        // delete them after saving the rest of the network interfaces.
        if (!empty($stale[$volume['VolumeId']])) {
          unset($stale[$volume['VolumeId']]);
        }
        if ($update) {
          $batch_builder->addOperation([
            OpenStackBatchOperations::class,
            'updateVolume',
          ], [$cloud_context, $volume, $snapshot_id_name_map]);
        }
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = empty($params['VolumeIds'][0]) ? TRUE : !empty($result['Volumes']);
    }
    elseif (!empty($params['VolumeIds'][0])) {
      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
    }
    return $updated;
  }

  /**
   * Update the volumes.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateVolumes(): bool {
    $entity_type = 'openstack_volume';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateVolumeEntities($entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update the availability zones.
   *
   * @param array $params
   *   Parameters array to send to API.
   * @param bool $clear
   *   TRUE to clear stale instances.
   *
   * @return bool
   *   Indicates success or failure.
   */
  public function updateAvailabilityZones(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_availability_zone';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateAvailabilityZoneEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as AvailabilityZones.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale availability zones.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateAvailabilityZoneEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = ''): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $result = $this->describeAvailabilityZones($params);

    if (!empty($result)) {
      $properties = ['cloud_context' => $this->cloudContext];
      $all_availability_zones = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);

      $stale = [];
      // Make it easier to look up the groups by setting up the array with the
      // group_id.
      foreach ($all_availability_zones ?: [] as $all_availability_zone) {
        $stale[$all_availability_zone->getZoneName()] = $all_availability_zone;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('AvailabilityZone Update');

      foreach ($result['AvailabilityZones'] ?: [] as $availability_zone) {
        // Keep track of availability zones that do not exist anymore
        // delete them after saving the rest of the availability zones.
        if (!empty($stale[$availability_zone['ZoneName']])) {
          unset($stale[$availability_zone['ZoneName']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateAvailabilityZone',
        ], [$cloud_context, $availability_zone]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateAvailabilityZonesWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE) {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $entity_type = 'openstack_availability_zone';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = ['cloud_context' => $this->cloudContext];
    $all_availability_zones = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the snapshot by setting up the array with the
    // zone_name.
    foreach ($all_availability_zones ?: [] as $availability_zone) {
      $stale[$availability_zone->getZoneName()] = $availability_zone;
    }

    $result = $this->describeAvailabilityZones($params);

    if (!empty($result)) {
      foreach ($result['AvailabilityZones'] ?: [] as $availability_zone) {
        // Keep track of availability zones that do not exist anymore
        // delete them after saving the rest of the availability zones.
        if (!empty($stale[$availability_zone['ZoneName']])) {
          unset($stale[$availability_zone['ZoneName']]);
        }
        if ($update) {
          OpenStackBatchOperations::updateAvailabilityZone($this->cloudContext, $availability_zone);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = TRUE;
    }
    elseif (empty($result) && $clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update the OpenStackFlavors.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale flavors.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateFlavors(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_flavor';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateFlavorEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as Flavor entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale flavors.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update Flavor info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateFlavorEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $all_flavors = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
    $stale = [];

    // Make it easier to look up the flavor by setting up the array with the
    // flavor id.
    foreach ($all_flavors ?: [] as $flavor) {
      $stale[$flavor->getFlavorId()] = $flavor;
    }

    /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
    $batch_builder = $this->initBatch('Flavor update');

    $updated = TRUE;
    $result = $this->describeFlavors($params);
    if (empty($result)) {
      $result = [];
      $updated = FALSE;
    }

    foreach ($result ?: [] as $flavor) {

      // Keep track of flavors that do not exist anymore
      // delete them after saving the rest of the flavors.
      if (!empty($stale[$flavor['id']])) {
        unset($stale[$flavor['id']]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'updateFlavor',
      ], [$cloud_context, $flavor]);
    }

    $batch_builder->addOperation([
      OpenStackBatchOperations::class,
      'finished',
    ], [$entity_type, $stale, $clear]);
    $this->runBatch($batch_builder);

    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateFlavorsWithoutBatch(array $params = [], $clear = TRUE, $update = TRUE) {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $entity_type = 'openstack_flavor';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = ['cloud_context' => $this->cloudContext];
    $all_flavors = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    // Make it easier to look up the flavor by setting up the array with the
    // flavor id.
    foreach ($all_flavors ?: [] as $flavor) {
      $stale[$flavor->getFlavorId()] = $flavor;
    }

    $updated = TRUE;
    $result = $this->describeFlavors($params);
    if (empty($result)) {
      $result = [];
      $updated = FALSE;
    }

    foreach ($result ?: [] as $flavor) {
      // Keep track of snapshot that do not exist anymore
      // delete them after saving the rest of the snapshots.
      if (!empty($stale[$flavor['id']])) {
        unset($stale[$flavor['id']]);
      }
      if ($update) {
        OpenStackBatchOperations::updateFlavor($this->cloudContext, $flavor);
      }
    }

    if (count($stale) && $clear === TRUE) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * {@inheritdoc}
   */
  public function updateServerGroupsWithoutBatch(array $params = [], bool $clear = TRUE, bool $update = TRUE): bool {
    $updated = FALSE;
    $entity_type = 'openstack_server_group';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $properties = ['cloud_context' => $this->cloudContext];
    $all_server_groups = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $properties);
    $stale = [];
    foreach ($all_server_groups ?: [] as $server_group) {
      $stale[$server_group->getName()] = $server_group;
    }

    $result = $this->describeServerGroups($params);

    if (!empty($result)) {
      foreach ($result['ServerGroups'] ?: [] as $server_group) {
        if (!empty($stale[$server_group['Name']])) {
          unset($stale[$server_group['Name']]);
        }
        if ($update) {
          OpenStackBatchOperations::updateServerGroup($this->cloudContext, $server_group);
        }
      }

      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }

      $updated = TRUE;
    }
    elseif ($clear) {
      $this->entityTypeManager->getStorage($entity_type)->delete($stale);
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Get snapshot id and name.
   *
   * @param array $volumes
   *   The volumes array.
   *
   * @return array
   *   Return map array.
   */
  public function getSnapshotIdNameMap(array $volumes = []) {
    $snapshot_ids = array_filter(array_column($volumes, 'SnapshotId'));
    if (empty($snapshot_ids)) {
      return [];
    }

    $map = [];
    foreach ($snapshot_ids ?: [] as $snapshot_id) {
      $map[$snapshot_id] = $snapshot_id;
    }

    $result = $this->describeSnapshots();

    if (!empty($result)) {
      foreach ($result['Snapshots'] ?: [] as $snapshot) {
        $snapshot_id = $snapshot['SnapshotId'];
        if (!array_key_exists($snapshot_id, $map)) {
          continue;
        }

        $map[$snapshot_id] = $this->getTagName($snapshot, '');
      }
    }

    return $map;
  }

  /**
   * Call the API for updated entities and store them as snapshot entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param array $params
   *   The snapshot IDs.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSnapshotEntities($entity_type = '', $cloud_context = '', array $params = [], $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;

    $result = $this->describeSnapshots();

    if (!empty($result)) {

      $all_snapshots = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the snapshot by setting up the array with the
      // snapshot_id.
      foreach ($all_snapshots ?: [] as $snapshot) {
        $stale[$snapshot->getSnapshotId()] = $snapshot;
      }
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Snapshot Update');

      foreach ($result['Snapshots'] ?: [] as $snapshot) {
        // Keep track of snapshot that do not exist anymore
        // delete them after saving the rest of the snapshots.
        if (!empty($stale[$snapshot['SnapshotId']])) {
          unset($stale[$snapshot['SnapshotId']]);
        }

        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateSnapshot',
        ], [$cloud_context, $snapshot]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the snapshots.
   *
   * @return bool
   *   Indicates success so failure.
   */
  public function updateSnapshots(): bool {
    $entity_type = 'openstack_snapshot';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSnapshotEntities($entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Update the OpenStackSecurityGroups.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSecurityGroups(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_security_group';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSecurityGroupEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as SecurityGroup entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale security groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update SG info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSecurityGroupEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);

    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;

    $updated = FALSE;
    $result = $this->describeSecurityGroups($params);

    if (!empty($result)) {

      $all_groups = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the groups by setting up the array with the
      // group_id.
      foreach ($all_groups ?: [] as $group) {
        $stale[$group->getGroupId()] = $group;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Security group update');

      foreach ($result['SecurityGroups'] ?: [] as $security_group) {

        // Keep track of instances that do not exist anymore
        // delete them after saving the rest of the instances.
        if (!empty($stale[$security_group['GroupId']])) {
          unset($stale[$security_group['GroupId']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateSecurityGroup',
        ], [$cloud_context, $security_group]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Create network.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The network create API response.
   */
  public function createNetwork(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateNetworks');

      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $body['network'] = [
      'name' => $params['Name'],
      'admin_state_up' => $params['AdminStateUp'],
    ];
    if (!empty($params['Shared'])) {
      $body['network']['shared'] = $params['Shared'];
    }
    if (!empty($params['External'])) {
      $body['network']['router:external'] = $params['External'];
    }

    if (!empty($params['AvailabilityZones'])) {
      $body['network']['availability_zone_hints'] = $params['AvailabilityZones'];
    }

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/networks?project_id={$project_id}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);

    $network = $response['body']['network'] ?? [];

    return [
      'Network' => [
        'NetworkId' => $network['id'] ?? NULL,
      ],
    ];
  }

  /**
   * Get network list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The network list API response.
   */
  public function describeNetworks(array $params = []) {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $project_id = $params['project_id'];

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/networks'),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);

    $networks = $response['body']['networks'] ?? [];

    $results['Networks'] = [];
    foreach ($networks ?: [] as $network) {

      // According to neutron.py#L1247 of Horizon,
      // the following networks should be shown.
      // 1. The project_id is same.
      // 2. The shared is TRUE.
      // 3. The router:external is TRUE and shared is FALSE.
      if (!(
        $network['project_id'] === $project_id
        || !empty($network['shared'])
        || (!empty($network['router:external']) && empty($network['shared']))
      )) {
        continue;
      }

      $subnets = [];
      foreach ($network['subnets'] ?: [] as $subnet_id) {
        // If the network is not owned by the project and is not shared, skip.
        if ($network['project_id'] !== $project_id
          && empty($network['shared'])) {
          continue;
        }
        $subnet_result = $this->describeSubnetDetail(['SubnetId' => $subnet_id]);
        $subnets[] = $subnet_result['name'];
      }

      $results['Networks'][] = [
        'NetworkId'          => $network['id'] ?? NULL,
        'Name'               => $network['name'] ?? NULL,
        'ProjectId'          => $network['project_id'],
        'Status'             => $network['status'] === 'ACTIVE' ? 'available' : '',
        'AdminStateUp'       => $network['admin_state_up'] ?? FALSE,
        'TagSet'             => empty($network['tags']) ? [] : $network['tags'],
        'External'           => $network['router:external'] ?? FALSE,
        'Shared'             => $network['shared'] ?? FALSE,
        'Mtu'                => $network['mtu'] ?? -1,
        'NetworkType'        => $network['provider:network_type'] ?? NULL,
        'PhysicalNetwork'    => $network['provider:physical_network'] ?? NULL,
        'SegmentationId'     => $network['provider:segmentation_id'] ?? NULL,
        'Subnets'            => $subnets,
        'AvailabilityZones'  => $network['availability_zones'] ?? [],
      ];
    }

    return $results;
  }

  /**
   * Delete network.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteNetwork(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateNetworks');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/networks/{$params['NetworkId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Update network.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The network update API response.
   */
  public function updateNetwork(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateNetworks');

      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $body['network'] = [
      'name' => $params['Name'],
      'admin_state_up' => $params['AdminStateUp'],
    ];
    if (!empty($params['Shared'])) {
      $body['network']['shared'] = $params['Shared'];
    }
    if (!empty($params['External'])) {
      $body['network']['router:external'] = $params['External'];
    }

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/networks/{$params['NetworkId']}?project_id={$project_id}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    $network = $response['body']['network'] ?? [];

    return [
      'Network' => [
        'Name' => $network['name'] ?? NULL,
        'NetworkId' => $network['network_id'] ?? NULL,
      ],
    ];
  }

  /**
   * Update the OpenStackNetworks.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale networks.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateNetworks(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_network';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateNetworkEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call API for updated entities and store them as Network entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale networks.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateNetworkEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);

    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;

    $updated = FALSE;
    $result = $this->describeNetworks($params);
    if (!empty($result)) {

      $all_networks = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];

      foreach ($all_networks ?: [] as $network) {
        $stale[$network->getNetworkId()] = $network;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Network update');

      foreach ($result['Networks'] ?: [] as $network) {
        // Keep track of networks that do not exist anymore
        // delete them after saving the rest of the networks.
        if (!empty($stale[$network['NetworkId']])) {
          unset($stale[$network['NetworkId']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateNetwork',
        ], [$cloud_context, $network]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Get subnet list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The subnet list API response.
   */
  public function describeSubnets(array $params = []) {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/subnets'),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);

    $networks = $this->entityTypeManager
      ->getStorage('openstack_network')
      ->loadByProperties([
        'cloud_context' => $this->cloudContext,
      ]);
    $network_ids = array_map(static function ($item) {
      return !empty($item) ? $item->getNetworkId() : '';
    }, $networks ?: []);

    $subnets = $response['body']['subnets'] ?? [];
    $results['Subnets'] = [];
    foreach ($subnets ?: [] as $subnet) {
      // $params['_ignore_network'] is a custom parameter used in this module.
      // If $params['_ignore_network'] is passed, ignore network checking.
      // Useful when using the OpenStack API without adding an OpenStack
      // cloud service provider.
      if (empty($params['_ignore_network'])
        && (empty($subnet['network_id']) || !in_array($subnet['network_id'], $network_ids))) {
        continue;
      }

      $pools = [];
      foreach ($subnet['allocation_pools'] ?: [] as $allocation_pool) {
        $pools[] = $allocation_pool['start'] . ' - ' . $allocation_pool['end'];
      }

      $routes = [];
      foreach ($subnet['host_routes'] ?: [] as $host_route) {
        if (empty($host_route['nexthop']) || empty($host_route['destination'])) {
          continue;
        }

        $routes[] = sprintf('nexthop: %s, destination: %s', $host_route['nexthop'], $host_route['destination']);
      }

      $results['Subnets'][] = [
        'SubnetId'        => $subnet['id'] ?? NULL,
        'Name'            => $subnet['name'] ?? NULL,
        'ProjectId'       => $subnet['project_id'],
        'NetworkId'       => $subnet['network_id'] ?? NULL,
        'SubnetPoolId'    => $subnet['subnetpool_id'] ?? NULL,
        'IpVersion'       => empty($subnet['ip_version']) || $subnet['ip_version'] === 4 ? 'IPv4' : 'IPv6',
        'TagSet'          => empty($subnet['tags']) ? [] : $subnet['tags'],
        'Cidr'            => $subnet['cidr'],
        'AllocationPools' => $pools,
        'GatewayIp'       => $subnet['gateway_ip'],
        'EnableDhcp'      => $subnet['enable_dhcp'],
        'HostRoutes'      => $routes,
        'DnsNameServers'  => $subnet['dns_nameservers'],
      ];
    }

    return $results;
  }

  /**
   * Create subnet.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The subnet create API response.
   */
  public function createSubnet(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSubnets');

      return ['SendToWorker' => TRUE];
    }

    $body['subnet'] = [
      'name' => $params['Name'],
      'network_id' => $params['NetworkId'],
      'cidr' => $params['Cidr'],
      'ip_version' => $params['IpVersion'] === 'IPv4' ? 4 : 6,
      'enable_dhcp' => $params['EnableDhcp'],
    ];
    if (array_key_exists('GatewayIp', $params)) {
      $body['subnet']['gateway_ip'] = $params['GatewayIp'];
    }
    if (!empty($params['AllocationPools'])) {
      $body['subnet']['allocation_pools'] = $params['AllocationPools'];
    }
    if (!empty($params['DnsNameServers'])) {
      $body['subnet']['dns_nameservers'] = $params['DnsNameServers'];
    }
    if (!empty($params['HostRoutes'])) {
      $body['subnet']['host_routes'] = $params['HostRoutes'];
    }

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/subnets'),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 201) {
      return NULL;
    }

    $subnet = $response['body']['subnet'] ?? [];

    return [
      'Subnet' => [
        'SubnetId' => $subnet['id'] ?? NULL,
      ],
    ];
  }

  /**
   * Delete subnet.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteSubnet(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSubnets');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/subnets/{$params['SubnetId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Update subnet.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The subnet update API response.
   */
  public function updateSubnet(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateSubnets');

      return ['SendToWorker' => TRUE];
    }

    $body['subnet'] = [
      'name' => $params['Name'],
      'enable_dhcp' => $params['EnableDhcp'],
    ];
    if (array_key_exists('GatewayIp', $params)) {
      $body['subnet']['gateway_ip'] = $params['GatewayIp'];
    }
    if (!empty($params['AllocationPools'])) {
      $body['subnet']['allocation_pools'] = $params['AllocationPools'];
    }
    if (!empty($params['DnsNameServers'])) {
      $body['subnet']['dns_nameservers'] = $params['DnsNameServers'];
    }
    if (!empty($params['HostRoutes'])) {
      $body['subnet']['host_routes'] = $params['HostRoutes'];
    }

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/subnets/{$params['SubnetId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    $subnet = $response['body']['subnet'] ?? [];

    return [
      'Subnet' => [
        'Name' => $subnet['name'] ?? NULL,
        'SubnetId' => $subnet['subnet_id'] ?? NULL,
      ],
    ];
  }

  /**
   * Update the OpenStackSubnets.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale subnets.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSubnets(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_subnet';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateSubnetEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call API for updated entities and store them as Subnet entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale subnets.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateSubnetEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);

    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;

    $updated = FALSE;
    $result = $this->describeSubnets($params);
    if (!empty($result)) {
      $all_subnets = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];

      foreach ($all_subnets ?: [] as $subnet) {
        $stale[$subnet->getSubnetId()] = $subnet;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Subnet update');

      foreach ($result['Subnets'] ?: [] as $subnet) {
        // Keep track of subnets that do not exist anymore
        // delete them after saving the rest of the networks.
        if (!empty($stale[$subnet['SubnetId']])) {
          unset($stale[$subnet['SubnetId']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateSubnet',
        ], [$cloud_context, $subnet]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Get port list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The port list API response.
   */
  public function describePorts(array $params = []) {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/ports'),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);

    $networks = $this->entityTypeManager
      ->getStorage('openstack_network')
      ->loadByProperties([
        'cloud_context' => $this->cloudContext,
      ]);
    $network_ids = array_map(static function ($item) {
      return !empty($item) ? $item->getNetworkId() : '';
    }, $networks ?: []);

    $ports = $response['body']['ports'] ?? [];
    $results['Ports'] = [];
    foreach ($ports ?: [] as $port) {
      // $params['_ignore_network'] is a custom parameter used in this module.
      // If $params['_ignore_network'] is passed, ignore network checking.
      // Useful when using the OpenStack API without adding an OpenStack
      // cloud service provider.
      if (empty($params['_ignore_network'])
        && (empty($port['network_id']) || !in_array($port['network_id'], $network_ids))) {
        continue;
      }

      $dns_assignment = [];
      foreach ($port['dns_assignment'] ?? [] as $item) {
        $dns_assignment[] = sprintf('%s,%s,%s', $item['hostname'], $item['ip_address'], $item['fqdn']);
      }

      $fixed_ips = [];
      foreach ($port['fixed_ips'] ?: [] as $item) {
        $fixed_ips[] = sprintf('%s,%s', $item['ip_address'], $item['subnet_id']);
      }

      $results['Ports'][] = [
        'PortId' => $port['id'] ?? NULL,
        'Name' => $port['name'] ?? NULL,
        'ProjectId' => $port['project_id'],
        'NetworkId' => $port['network_id'] ?? NULL,
        'MacAddress' => $port['mac_address'] ?? NULL,
        'TagSet' => empty($port['tags']) ? [] : $port['tags'],
        'Status' => $port['status'],
        'AdminStateUp' => $port['admin_state_up'],
        'PortSecurityEnabled' => $port['port_security_enabled'],
        'DnsName' => $port['dns_name'] ?? NULL,
        'DnsAssignment' => $dns_assignment,
        'FixedIps' => $fixed_ips,
        'DeviceOwner' => $port['device_owner'] ?? NULL,
        'DeviceId' => $port['device_id'] ?? NULL,
        'SecurityGroups' => $port['security_groups'] ?? [],
        'BindingVnicType' => $port['binding:vnic_type'] ?? NULL,
        'BindingHostId' => $port['binding:host_id'] ?? NULL,
        'BindingProfile' => $port['binding:profile'] ?? [],
        'BindingVifType' => $port['binding:vif_type'] ?? NULL,
        'BindingVifDetails' => $port['binding:vif_details'] ?? [],
        'AllowedAddressPairs' => $port['allowed_address_pairs'] ?? [],
      ];
    }

    return $results;
  }

  /**
   * Create port.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The subnet create API response.
   */
  public function createPort(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updatePorts');

      return ['SendToWorker' => TRUE];
    }

    $body = [];
    foreach ($params as $key => $value) {
      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      if ($key === 'binding_vnic_type') {
        $key = 'binding:vnic_type';
      }
      elseif ($key === 'binding_profile') {
        $key = 'binding:profile';
        $value = json_decode($value, TRUE);
      }

      $body['port'][$key] = $value;
    }

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/ports'),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 201) {
      return NULL;
    }

    $port = $response['body']['port'] ?? [];

    return [
      'Port' => [
        'PortId' => $port['id'] ?? NULL,
      ],
    ];
  }

  /**
   * Delete port.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deletePort(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updatePorts');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/ports/{$params['PortId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Update port.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The port update API response.
   */
  public function updatePort(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updatePorts');

      return ['SendToWorker' => TRUE];
    }

    $body = [];
    foreach ($params as $key => $value) {
      if ($key === 'PortId') {
        continue;
      }

      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      if ($key === 'binding_vnic_type') {
        $key = 'binding:vnic_type';
      }
      $body['port'][$key] = $value;
    }

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/ports/{$params['PortId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    $port = $response['body']['port'] ?? [];

    return [
      'Port' => [
        'Name' => $port['name'] ?? NULL,
        'PortId' => $port['port_id'] ?? NULL,
      ],
    ];
  }

  /**
   * Update the OpenStackPorts.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale ports.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updatePorts(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_port';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updatePortEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call API for updated entities and store them as Port entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale ports.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updatePortEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);

    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;

    $updated = FALSE;
    $result = $this->describePorts($params);
    if (!empty($result)) {
      $all_ports = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];

      foreach ($all_ports ?: [] as $port) {
        $stale[$port->getPortId()] = $port;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Port update');

      foreach ($result['Ports'] ?: [] as $port) {
        // Keep track of ports that do not exist anymore
        // delete them after saving the rest of the networks.
        if (!empty($stale[$port['PortId']])) {
          unset($stale[$port['PortId']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updatePort',
        ], [$cloud_context, $port]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Get router list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The router list API response.
   */
  public function describeRouters(array $params = []) {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $project_id = $params['project_id'];

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/routers'),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $routers = $response['body']['routers'] ?? [];
    $results['Routers'] = [];
    foreach ($routers ?: [] as $router) {
      if ($router['project_id'] !== $project_id) {
        continue;
      }

      $results['Routers'][] = [
        'RouterId' => $router['id'] ?? NULL,
        'Name' => $router['name'] ?? NULL,
        'ProjectId' => $router['project_id'],
        'TagSet' => empty($router['tags']) ? [] : $router['tags'],
        'Status' => $router['status'],
        'AdminStateUp' => $router['admin_state_up'],
        'AvailabilityZones' => $router['availability_zones'],
        'ExternalGateway' => $router['external_gateway_info'] ?? [],
        'Routes' => $router['routes'],
      ];
    }

    return $results;
  }

  /**
   * Get quota list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The quota list API response.
   */
  public function describeQuotas(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $result = $this->describeProjects();
    $project_id_name_map = [];
    foreach ($result['Projects'] as $project) {
      $project_id_name_map[$project['Id']] = $project['Name'];
    }

    $project_id = $params['project_id'];

    // Network quota detail.
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/quotas/$project_id/details.json"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $network_quota = $response['body']['quota'] ?? [];

    // Compute quota detail.
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/os-quota-sets/$project_id/detail"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $compute_quota = $response['body']['quota_set'] ?? [];

    // Volume quota detail.
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "$project_id/os-quota-sets/$project_id?usage=True"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $volume_quota = $response['body']['quota_set'] ?? [];
    $results['Quotas'] = [
      [
        'Name' => $project_id_name_map[$project_id] ?? 'quota',
        'ProjectId' => $project_id,
        'Instances' => $compute_quota['instances']['limit'] ?? NULL,
        'InstancesUsage' => $compute_quota['instances']['in_use'] ?? 0,
        'Cores' => $compute_quota['cores']['limit'] ?? NULL,
        'CoresUsage' => $compute_quota['cores']['in_use'] ?? 0,
        'Ram' => $compute_quota['ram']['limit'] ?? NULL ,
        'RamUsage' => $compute_quota['ram']['in_use'] ?? 0,
        'MetadataItems' => $compute_quota['metadata_items']['limit'] ?? NULL,
        'MetadataItemsUsage' => $compute_quota['metadata_items']['in_use'] ?? 0,
        'KeyPairs' => $compute_quota['key_pairs']['limit'] ?? NULL,
        'KeyPairsUsage' => $compute_quota['key_pairs']['in_use'] ?? 0,
        'ServerGroups' => $compute_quota['server_groups']['limit'] ?? NULL,
        'ServerGroupsUsage' => $compute_quota['server_groups']['in_use'] ?? 0,
        'ServerGroupMembers' => $compute_quota['server_group_members']['limit'] ?? NULL,
        'ServerGroupMembersUsage' => $compute_quota['server_group_members']['in_use'] ?? 0,
        'InjectedFiles' => $compute_quota['injected_files']['limit'] ?? NULL,
        'InjectedFilesUsage' => $compute_quota['injected_files']['in_use'] ?? 0,
        'InjectedFileContentBytes' => $compute_quota['injected_file_content_bytes']['limit'] ?? NULL,
        'InjectedFileContentBytesUsage' => $compute_quota['injected_file_content_bytes']['in_use'] ?? 0,
        'InjectedFilePathBytes' => $compute_quota['injected_file_path_bytes']['limit'] ?? NULL,
        'InjectedFilePathBytesUsage' => $compute_quota['injected_file_path_bytes']['in_use'] ?? 0,
        'Volumes' => $volume_quota['volumes']['limit'] ?? NULL,
        'VolumesUsage' => $volume_quota['volumes']['in_use'] ?? 0,
        'Snapshots' => $volume_quota['snapshots']['limit'] ?? NULL,
        'SnapshotsUsage' => $volume_quota['snapshots']['in_use'] ?? 0,
        'GigaBytes' => $volume_quota['gigabytes']['limit'] ?? NULL,
        'GigaBytesUsage' => $volume_quota['gigabytes']['in_use'] ?? 0,
        'Network' => $network_quota['network']['limit'] ?? NULL,
        'NetworkUsage' => $network_quota['network']['used'] ?? 0,
        'Subnet' => $network_quota['subnet']['limit'] ?? NULL,
        'SubnetUsage' => $network_quota['subnet']['used'] ?? 0,
        'Port' => $network_quota['port']['limit'] ?? NULL,
        'PortUsage' => $network_quota['port']['used'] ?? 0,
        'Router' => $network_quota['router']['limit'] ?? NULL,
        'RouterUsage' => $network_quota['router']['used'] ?? 0,
        'FloatingIp' => $network_quota['floatingip']['limit'] ?? NULL,
        'FloatingIpUsage' => $network_quota['floatingip']['used'] ?? 0,
        'SecurityGroup' => $network_quota['security_group']['limit'] ?? NULL,
        'SecurityGroupUsage' => $network_quota['security_group']['used'] ?? 0,
        'SecurityGroupRule' => $network_quota['security_group_rule']['limit'] ?? NULL,
        'SecurityGroupRuleUsage' => $network_quota['security_group_rule']['used'] ?? 0,
      ],
    ];

    return $results;
  }

  /**
   * Get stack list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack list API response.
   */
  public function describeStacks(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $project_id = $params['project_id'];

    // Network quota.
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "/$project_id/stacks"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $stacks = $response['body']['stacks'] ?? [];
    $results['Stacks'] = [];
    foreach ($stacks ?: [] as $stack) {
      $results['Stacks'][] = [
        'StackId' => $stack['id'] ?? NULL,
        'Name' => $stack['stack_name'] ?? NULL,
        'Description' => $stack['description'] ?? NULL,
        'ProjectId' => $project_id,
        'TagSet' => empty($stack['tags']) ? [] : $stack['tags'],
        'StackStatus' => $stack['stack_status'] ?? NULL,
        'StackStatusReason' => $stack['stack_status_reason'] ?? NULL,
      ];
    }

    return $results;
  }

  /**
   * Get template version list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The template version list API response.
   */
  public function describeTemplateVersions(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $project_id = $params['project_id'];

    // Network quota.
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "/$project_id/template_versions"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $template_versions = $response['body']['template_versions'] ?? [];
    $results['TemplateVersions'] = [];
    foreach ($template_versions ?: [] as $template_version) {
      $results['TemplateVersions'][] = [
        'ProjectId' => $project_id,
        'Name' => $template_version['version'] ?? NULL,
        'Type' => $template_version['type'] ?? NULL,
      ];
    }

    return $results;
  }

  /**
   * Get template version function list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The template version function list API response.
   */
  public function describeTemplateVersionFunctions(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $project_id = $params['ProjectId'];
    $template_version = $params['TemplateVersion'];

    // Template versions.
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "/$project_id/template_versions/$template_version/functions"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $template_functions = $response['body']['template_functions'] ?? [];
    $results['TemplateFunctions'] = [];
    foreach ($template_functions ?: [] as $template_function) {
      $results['TemplateFunctions'][] = [
        'Name' => $template_function['functions'] ?? NULL,
        'Description' => $template_function['description'] ?? NULL,
      ];
    }

    return $results;
  }

  /**
   * Get project list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The project list API response.
   */
  public function describeProjects(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/projects"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $projects = $response['body']['projects'] ?? [];
    $results['Projects'] = [];
    foreach ($projects ?: [] as $project) {
      if (!empty($params['ProjectId']) && $params['ProjectId'] !== $project['id']) {
        continue;
      }

      $results['Projects'][] = [
        'Id' => $project['id'],
        'Name' => $project['name'],
        'Description' => $project['description'] ?? NULL,
        'Enabled' => $project['enabled'] ?? FALSE,
        'IsDomain' => $project['is_domain'] ?? FALSE,
        'DomainId' => $project['domain_id'],
      ];
    }

    return $results;
  }

  /**
   * Get role list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The role list API response.
   */
  public function describeRoles(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/roles"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $roles = $response['body']['roles'] ?? [];
    $results['Roles'] = [];
    foreach ($roles ?: [] as $role) {
      if (!empty($params['RoleId']) && $params['RoleId'] !== $role['id']) {
        continue;
      }

      $results['Roles'][] = [
        'Id' => $role['id'],
        'Name' => $role['name'],
        'Description' => $role['description'] ?? NULL,
      ];
    }

    return $results;
  }

  /**
   * Get user and roles for a project.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The user and roles API response.
   */
  public function describeProjectUserRoles(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/role_assignments?scope.project.id={$params['ProjectId']}&include_subtree=true"),
      'params' => [],
    ];
    $response = $this->getClient($credentials);

    $role_assignments = $response['body']['role_assignments'] ?? [];
    $results['UserRoles'] = [];
    foreach ($role_assignments ?: [] as $role_assignment) {
      if (empty($role_assignment['user']) || empty($role_assignment['role'])) {
        continue;
      }

      $user_id = $role_assignment['user']['id'];
      $role_id = $role_assignment['role']['id'];
      if (!array_key_exists($user_id, $results['UserRoles'])) {
        $results['UserRoles'][$user_id] = [];
      }

      $results['UserRoles'][$user_id][] = $role_id;
    }

    return $results;
  }

  /**
   * Get the roles of the current user.
   *
   * @return array
   *   The user and roles API response.
   */
  public function describeCurrentUserRoles(): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $response = $this->authenticate([]);

    // Return the list of the role names.
    return array_map(static fn($role) => $role['name'], $response['body']['token']['roles'] ?? []);
  }

  /**
   * Get user list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The user list API response.
   */
  public function describeUsers(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/users"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $users = $response['body']['users'] ?? [];
    $results['Users'] = [];
    foreach ($users ?: [] as $user) {
      if (!empty($params['UserId']) && $params['UserId'] !== $user['id']) {
        continue;
      }

      $results['Users'][] = [
        'Id' => $user['id'],
        'Name' => $user['name'],
        'Description' => $user['description'] ?? NULL,
        'Email' => $user['email'] ?? NULL,
        'DefaultProjectId' => $user['default_project_id'] ?? NULL,
        'DomainId' => $user['domain_id'],
        'Enabled' => $user['enabled'],
        'PasswordExpiresAt' => $user['password_expires_at'],
      ];
    }

    return $results;
  }

  /**
   * Get a stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack detail API response.
   */
  public function showStack(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $project_id = $params['ProjectId'];

    // Stack detail.
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "/{$project_id}/stacks/{$params['StackName']}/{$params['StackId']}"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $stack = $response['body']['stack'];

    $result = [
      'CreationTime' => $stack['creation_time'],
      'DeletionTime' => $stack['deletion_time'],
      'Description' => $stack['description'],
      'Rollback' => empty($stack['disable_rollback']),
      'Outputs' => $stack['outputs'],
      'Parameters' => $stack['parameters'],
      'StackName' => $stack['stack_name'],
      'StackOwner' => $stack['stack_owner'],
      'StackStatus' => $stack['stack_status'],
      'StackStatusReason' => $stack['stack_status_reason'],
      'StackUserProjectId' => $stack['stack_user_project_id'],
      'TemplateDescription' => $stack['template_description'],
      'TimeoutMins' => $stack['timeout_mins'],
      'UpdatedTime' => $stack['updated_time'],
    ];

    // Stack template.
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "/{$project_id}/stacks/{$params['StackName']}/{$params['StackId']}/template"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $result['Template'] = Yaml::encode($response['body']);

    return $result;
  }

  /**
   * Get stack resource list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack resource list API response.
   */
  public function describeStackResources(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $project_id = $params['project_id'];

    // Network quota.
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "/{$project_id}/stacks/{$params['StackName']}/{$params['StackId']}/resources"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $resources = $response['body']['resources'] ?? [];
    $results['Resources'] = [];
    foreach ($resources ?: [] as $resource) {
      $results['Resources'][] = [
        'ResourceId' => $resource['physical_resource_id'] ?? NULL,
        'Name' => $resource['resource_name'] ?? NULL,
        'ProjectId' => $project_id,
        'StackId' => $params['StackId'],
        'ResourceStatus' => $resource['resource_status'] ?? NULL,
        'ResourceStatusReason' => $resource['resource_status_reason'] ?? NULL,
        'ResourceType' => $resource['resource_type'] ?? NULL,
        'CreationTime' => $resource['creation_time'],
        'UpdatedTime' => $resource['updated_time'],
      ];
    }

    return $results;
  }

  /**
   * Get stack event list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack event list API response.
   */
  public function describeStackEvents(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $project_id = $params['project_id'];

    // Network quota.
    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "/{$project_id}/stacks/{$params['StackName']}/{$params['StackId']}/events"),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $events = $response['body']['events'] ?? [];

    $results['Events'] = [];
    foreach ($events ?: [] as $event) {
      $results['Events'][] = [
        'EventId' => $event['id'],
        'Name' => $event['id'],
        'ProjectId' => $project_id,
        'StackId' => $params['StackId'],
        'ResourceId' => $event['physical_resource_id'] ?? NULL,
        'ResourceStatus' => $event['resource_status'] ?? NULL,
        'ResourceStatusReason' => $event['resource_status_reason'] ?? NULL,
        'ResourceName' => $event['resource_name'] ?? NULL,
        'EventTime' => $event['event_time'],
      ];
    }

    return $results;
  }

  /**
   * Update the OpenStackRouters.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale routers.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateRouters(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_router';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateRouterEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call API for updated entities and store them as Router entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale routers.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateRouterEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);

    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;

    $updated = FALSE;
    $result = $this->describeRouters($params);
    if (!empty($result)) {
      $all_routers = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];

      foreach ($all_routers ?: [] as $router) {
        $stale[$router->getRouterId()] = $router;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Router update');

      foreach ($result['Routers'] ?: [] as $router) {
        // Keep track of routers that do not exist anymore
        // delete them after saving the rest of the networks.
        if (!empty($stale[$router['RouterId']])) {
          unset($stale[$router['RouterId']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateRouter',
        ], [$cloud_context, $router]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Create router.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The router create API response.
   */
  public function createRouter(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateRouters');

      return ['SendToWorker' => TRUE];
    }

    $external_gateway_info = [];
    foreach ($params as $key => $value) {
      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      if (strpos($key, 'external_gateway') === 0) {
        $external_gateway_info[substr($key, strlen('external_gateway_'))] = $value;
        continue;
      }

      $body['router'][$key] = $value;
    }
    $body['router']['external_gateway_info'] = $external_gateway_info;

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/routers'),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 201) {
      return NULL;
    }

    $router = $response['body']['router'] ?? [];

    return [
      'Router' => [
        'RouterId' => $router['id'] ?? NULL,
      ],
    ];
  }

  /**
   * Add router interface.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The router add interface API response.
   */
  public function addRouterInterface(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updatePorts');

      return ['SendToWorker' => TRUE];
    }

    if (!empty($params['IpAddress'])) {
      // Create a new port if ip address is specified.
      $body['port'] = [
        'network_id' => $params['NetworkId'],
        'fixed_ips' => [
          [
            'subnet_id' => $params['SubnetId'],
            'ip_address' => $params['IpAddress'],
          ],
        ],
      ];

      $credentials = [
        'http_method' => 'post',
        'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, '/ports'),
        'params' => $body,
      ];

      $response = $this->getClient($credentials);
      if ($response['status_code'] !== 201) {
        return NULL;
      }

      $port_id = $response['body']['port']['id'];

      // Create a port entity.
      $timestamp = time();
      $entity = OpenStackPort::create([
        'cloud_context' => $this->cloudContext,
        'name' => $port_id,
        'port_id' => $port_id,

        'created' => $timestamp,
        'changed' => $timestamp,
        'refreshed' => $timestamp,
        'uid' => 0,
      ]);
      $entity->save();

      $body = [
        'port_id' => $port_id,
      ];
    }
    else {
      $body = [
        'subnet_id' => $params['SubnetId'],
      ];
    }

    // Add an interface.
    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/routers/{$params['RouterId']}/add_router_interface"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    return [
      'PortId' => $response['body']['port_id'],
    ];
  }

  /**
   * Remove router interface.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The router delete interface API response.
   */
  public function removeRouterInterface(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updatePorts');

      return ['SendToWorker' => TRUE];
    }

    // Delete an interface.
    $body = [
      'port_id' => $params['PortId'],
    ];

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/routers/{$params['RouterId']}/remove_router_interface"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    return [
      'PortId' => $response['body']['port_id'],
    ];
  }

  /**
   * Delete router.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteRouter(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateRouters');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/routers/{$params['RouterId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Update router.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The router update API response.
   */
  public function updateRouter(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateRouters');

      return ['SendToWorker' => TRUE];
    }

    $external_gateway_info = [];
    foreach ($params as $key => $value) {
      if ($key === 'RouterId') {
        continue;
      }

      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      if (strpos($key, 'external_gateway') === 0) {
        $external_gateway_info[substr($key, strlen('external_gateway_'))] = $value;
        continue;
      }

      $body['router'][$key] = $value;
    }
    $body['router']['external_gateway_info'] = $external_gateway_info;

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/routers/{$params['RouterId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    $router = !empty($response['body']['router']) ? $response['body']['router'] : [];

    return [
      'Router' => [
        'Name' => !empty($router['name']) ? $router['name'] : NULL,
        'RouterId' => !empty($router['router_id']) ? $router['router_id'] : NULL,
      ],
    ];
  }

  /**
   * Update the OpenStackQuotas.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale quotas.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateQuotas(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_quota';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateQuotaEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call API for updated entities and store them as Quota entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale quotas.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateQuotaEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);

    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;
    $updated = FALSE;
    $result = $this->describeQuotas($params);
    if (!empty($result)) {
      $all_quotas = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];

      foreach ($all_quotas ?: [] as $quota) {
        $stale[$quota->getName()] = $quota;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Quota update');

      foreach ($result['Quotas'] ?: [] as $quota) {
        // Keep track of quotas that do not exist anymore
        // delete them after saving the rest of the networks.
        if (!empty($stale[$quota['Name']])) {
          unset($stale[$quota['Name']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateQuota',
        ], [$cloud_context, $quota]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Update quota.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The router update API response.
   */
  public function updateQuota(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateQuotas');

      return ['SendToWorker' => TRUE];
    }

    $project_id = $params['ProjectId'];

    // Update network quota.
    $body = [
      'quota' => [
        'network' => $params['Network'],
        'subnet' => $params['Subnet'],
        'port' => $params['Port'],
        'router' => $params['Router'],
        'floatingip' => $params['FloatingIp'],
        'security_group' => $params['SecurityGroup'],
        'security_group_rule' => $params['SecurityGroupRule'],
      ],
    ];
    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::NETWORK_SERVICE, "/quotas/$project_id"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    // Update compute quota.
    $body = [
      'quota_set' => [
        'instances' => $params['Instances'],
        'cores' => $params['Cores'],
        'ram' => $params['Ram'],
        'metadata_items' => $params['MetadataItems'],
        'key_pairs' => $params['KeyPairs'],
        'server_groups' => $params['ServerGroups'],
        'server_group_members' => $params['ServerGroupMembers'],
        'injected_files' => $params['InjectedFiles'],
        'injected_file_content_bytes' => $params['InjectedFileContentBytes'],
        'injected_file_path_bytes' => $params['InjectedFilePathBytes'],
      ],
    ];
    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/os-quota-sets/$project_id"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    // Update volume quota.
    $body = [
      'quota_set' => [
        'volumes' => $params['Volumes'],
        'snapshots' => $params['Snapshots'],
        'gigabytes' => $params['GigaBytes'],
      ],
    ];
    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::VOLUME_SERVICE, "$project_id/os-quota-sets/$project_id"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    return [
      'Quota' => [
        'Name' => 'quota',
      ],
    ];
  }

  /**
   * Create project.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The project create API response.
   */
  public function createProject(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateProjects');

      return ['SendToWorker' => TRUE];
    }

    $body = [];
    foreach ($params as $key => $value) {
      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      $body['project'][$key] = $value;
    }

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/projects"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 201) {
      return NULL;
    }

    $project = $response['body']['project'] ?? [];

    return [
      'Project' => [
        'Id' => $project['id'] ?? NULL,
      ],
    ];
  }

  /**
   * Delete project.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteProject(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateProjects');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/projects/{$params['ProjectId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Create role.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The role create API response.
   */
  public function createRole(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateRoles');

      return ['SendToWorker' => TRUE];
    }

    $body = [];
    foreach ($params as $key => $value) {
      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      $body['role'][$key] = $value;
    }

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/roles"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 201) {
      return NULL;
    }

    $role = $response['body']['role'] ?? [];

    return [
      'Role' => [
        'Id' => $role['id'] ?? NULL,
      ],
    ];
  }

  /**
   * Delete role.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteRole(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateRoles');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/roles/{$params['RoleId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Create user.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The role create API response.
   */
  public function createUser(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateUsers');

      return ['SendToWorker' => TRUE];
    }

    $body = [];
    foreach ($params as $key => $value) {
      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      $body['user'][$key] = $value;
    }

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/users"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 201) {
      return NULL;
    }

    $user = $response['body']['user'] ?? [];

    return [
      'User' => [
        'Id' => $user['id'] ?? NULL,
      ],
    ];
  }

  /**
   * Delete user.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteUser(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateUsers');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/users/{$params['UserId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Assign a role to a user on a project.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The assign API response.
   */
  public function assignRoleToUserOnProject(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateUsers');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(
        OpenStackServiceInterface::IDENTITY_SERVICE,
        "/projects/{$params['ProjectId']}/users/{$params['UserId']}/roles/{$params['RoleId']}"
      ),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Unassign a role from a user on a project.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The remove API response.
   */
  public function unassignRoleFromUserOnProject(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateUsers');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(
        OpenStackServiceInterface::IDENTITY_SERVICE,
        "/projects/{$params['ProjectId']}/users/{$params['UserId']}/roles/{$params['RoleId']}"
      ),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Create stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The stack create API response.
   */
  public function createStack(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateStacks');

      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $body = [];
    foreach ($params as $key => $value) {
      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      $body[$key] = $value;
    }

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "{$project_id}/stacks"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 201) {
      return NULL;
    }

    $stack = $response['body']['stack'] ?? [];

    return [
      'Stack' => [
        'StackId' => $stack['id'] ?? NULL,
      ],
    ];
  }

  /**
   * Update stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The stack update API response.
   */
  public function updateStack(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateStacks');

      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $body = [];
    foreach ($params as $key => $value) {
      // Ignore stack name and stack ID.
      if ($key === 'StackName' || $key === 'StackId') {
        continue;
      }

      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      $body[$key] = $value;
    }

    $credentials = [
      'http_method' => 'put',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "{$project_id}/stacks/{$params['StackName']}/{$params['StackId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    return $response['status_code'] === 202 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Preview stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The stack preview API response.
   */
  public function previewStack(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $body = [];
    foreach ($params as $key => $value) {
      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      $body[$key] = $value;
    }

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "{$project_id}/stacks/preview"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    $stack = $response['body']['stack'] ?? [];

    return $stack;
  }

  /**
   * Delete stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteStack(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateStacks');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "{$params['ProjectId']}/stacks/{$params['StackName']}/{$params['StackId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Check stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function checkStack(array $params = []): ?array {
    return $this->callStackAction('check', $params);
  }

  /**
   * Suspend stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function suspendStack(array $params = []): ?array {
    return $this->callStackAction('suspend', $params);
  }

  /**
   * Resume stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function resumeStack(array $params = []): ?array {
    return $this->callStackAction('resume', $params);
  }

  /**
   * Call stack action API.
   *
   * @param string $action
   *   Action.
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  private function callStackAction(string $action, array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateStacks');

      return ['SendToWorker' => TRUE];
    }

    $this->loadCredentials();
    $project_id = $this->credentials['project_id'];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::ORCHESTRATION_SERVICE, "{$project_id}/stacks/{$params['StackName']}/{$params['StackId']}/actions"),
      'params' => [
        $action => NULL,
      ],
    ];

    $response = $this->getClient($credentials);
    return !empty($response['status_code']) && $response['status_code'] === 200 ? ['Success' => TRUE] : NULL;
  }

  /**
   * Update the OpenStackStacks.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale stacks.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateStacks(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_stack';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateStackEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);

    $this->updateStackResources($params, $clear);
    $this->updateStackEvents($params, $clear);

    return $updated;
  }

  /**
   * Call API for updated entities and store them as Stack entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale stacks.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateStackEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);

    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;

    $updated = FALSE;
    $result = $this->describeStacks($params);
    if (!empty($result)) {
      $all_stacks = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];

      foreach ($all_stacks ?: [] as $stack) {
        $stale[$stack->getStackId()] = $stack;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Stack update');

      foreach ($result['Stacks'] ?: [] as $stack) {
        // Keep track of stacks that do not exist anymore
        // delete them after saving the rest of the networks.
        if (!empty($stale[$stack['StackId']])) {
          unset($stale[$stack['StackId']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateStack',
        ], [$cloud_context, $stack]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the OpenStackStackResources.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale resources.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateStackResources(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_stack_resource';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateStackResourceEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call API for updated entities and store them as Stack Resource entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale stacks.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $batch_mode
   *   TRUE to run in batch mode.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateStackResourceEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $batch_mode = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $conditions = [];
    if (!empty($params['StackId'])) {
      $conditions['stack_id'] = $params['StackId'];
    }
    $all_resources = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $conditions);
    $stale = [];
    foreach ($all_resources ?: [] as $resource) {
      $stale[$resource->getResourceId()] = $resource;
    }

    $updated = FALSE;

    if ($batch_mode) {
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Stack Resource update');
    }

    $all_stacks = $this->cloudService->loadAllEntities($this->cloudContext, 'openstack_stack', $conditions);
    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);
    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;

    foreach ($all_stacks ?: [] as $stack) {
      $params['StackId'] = $stack->getStackId();
      $params['StackName'] = $stack->getName();
      $result = $this->describeStackResources($params);
      if (empty($result)) {
        continue;
      }

      foreach ($result['Resources'] ?: [] as $resource) {
        // Keep track of stack resources that do not exist anymore
        // delete them after saving the rest of the stack resources.
        if (!empty($stale[$resource['ResourceId']])) {
          unset($stale[$resource['ResourceId']]);
        }

        $resource['StackEntityId'] = $stack->id();
        $resource['Uid'] = $stack->getOwner()->id();

        if ($batch_mode) {
          $batch_builder->addOperation([
            OpenStackBatchOperations::class,
            'updateStackResource',
          ], [$cloud_context, $resource]);
        }
        else {
          OpenStackBatchOperations::updateStackResource($cloud_context, $resource);
        }
      }

      $updated = TRUE;
    }

    if ($batch_mode) {
      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
    }
    else {
      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }
    }

    return $updated;
  }

  /**
   * Update the OpenStack Events.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale events.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateStackEvents(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_stack_event';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateStackEventEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call API for updated entities and store them as Stack Event entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale stacks.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $batch_mode
   *   TRUE to run in batch mode.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateStackEventEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $batch_mode = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $conditions = [];
    if (!empty($params['StackId'])) {
      $conditions['stack_id'] = $params['StackId'];
    }
    $all_events = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $conditions);
    $stale = [];
    foreach ($all_events ?: [] as $event) {
      $stale[$event->getEventId()] = $event;
    }

    $updated = FALSE;

    if ($batch_mode) {
      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Stack Event update');
    }

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);
    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;

    /** @var \Drupal\openstack\Entity\OpenStackStackInterface[] $all_stacks */
    $all_stacks = $this->cloudService->loadAllEntities($this->cloudContext, 'openstack_stack', $conditions);
    foreach ($all_stacks as $stack) {
      $params['StackId'] = $stack->getStackId();
      $params['StackName'] = $stack->getName();
      $result = $this->describeStackEvents($params);
      if (empty($result)) {
        continue;
      }

      foreach ($result['Events'] ?: [] as $event) {
        // Keep track of stack resources that do not exist anymore
        // delete them after saving the rest of the stack resources.
        if (!empty($stale[$event['EventId']])) {
          unset($stale[$event['EventId']]);
        }

        $event['StackEntityId'] = $stack->id();
        $event['Uid'] = $stack->getOwner()->id();

        if ($batch_mode) {
          $batch_builder->addOperation([
            OpenStackBatchOperations::class,
            'updateStackEvent',
          ], [$cloud_context, $event]);
        }
        else {
          OpenStackBatchOperations::updateStackEvent($cloud_context, $event);
        }
      }

      $updated = TRUE;
    }

    if ($batch_mode) {
      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
    }
    else {
      if (count($stale) && $clear === TRUE) {
        $this->entityTypeManager->getStorage($entity_type)->delete($stale);
      }
    }

    return $updated;
  }

  /**
   * Update the OpenStack template versions.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateTemplateVersions(): bool {
    $entity_type = 'openstack_template_version';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateTemplateVersionEntities($entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as TemplateVersions.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateTemplateVersionEntities($entity_type = '', $cloud_context = ''): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);

    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;
    $result = $this->describeTemplateVersions($params);

    if (!empty($result)) {

      $all_template_versions = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the groups by setting up the array with the
      // group_id.
      foreach ($all_template_versions ?: [] as $template_version) {
        $stale[$template_version->getName()] = $template_version;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('TemplateVersion Update');

      foreach ($result['TemplateVersions'] ?: [] as $template_version) {
        // Keep track of template versions that do not exist anymore
        // delete them after saving the rest of the template versions.
        if (!empty($stale[$template_version['Name']])) {
          unset($stale[$template_version['Name']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateTemplateVersion',
        ], [$cloud_context, $template_version]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update the OpenStack projects.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale projects.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateProjects(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_project';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateProjectEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);

    return $updated;
  }

  /**
   * Call API for updated entities and store them as Project entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale projects.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateProjectEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $result = $this->describeProjects($params);
    if (!empty($result)) {
      $all_projects = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];

      foreach ($all_projects ?: [] as $project) {
        $stale[$project->getProjectId()] = $project;
      }

      $user_map = [];
      $all_users = $this->cloudService->loadAllEntities($this->cloudContext, 'openstack_user');
      foreach ($all_users ?: [] as $user) {
        $user_map[$user->getUserId()] = $user;
      }

      $role_map = [];
      $all_roles = $this->cloudService->loadAllEntities($this->cloudContext, 'openstack_role');
      foreach ($all_roles ?: [] as $role) {
        $role_map[$role->getRoleId()] = $role;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Project update');

      foreach ($result['Projects'] ?: [] as $project) {
        // Keep track of project that do not exist anymore
        // delete them after saving the rest of the projects.
        if (!empty($stale[$project['Id']])) {
          unset($stale[$project['Id']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateProject',
        ], [$cloud_context, $project, $user_map, $role_map]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update project.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The project update API response.
   */
  public function updateProject(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateProjects');

      return ['SendToWorker' => TRUE];
    }

    $body = [];
    foreach ($params as $key => $value) {
      if ($key === 'ProjectId') {
        continue;
      }

      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      $body['project'][$key] = $value;
    }

    $credentials = [
      'http_method' => 'patch',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/projects/{$params['ProjectId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    $project = !empty($response['body']['project']) ? $response['body']['project'] : [];

    return [
      'Project' => [
        'Name' => !empty($project['name']) ? $project['name'] : NULL,
        'Id' => !empty($project['id']) ? $project['id'] : NULL,
      ],
    ];
  }

  /**
   * Update the OpenStack Roles.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale roles.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateRoles(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_role';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateRoleEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);

    return $updated;
  }

  /**
   * Call API for updated entities and store them as Role entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale roles.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateRoleEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $result = $this->describeRoles($params);
    if (!empty($result)) {
      $all_roles = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];

      foreach ($all_roles ?: [] as $role) {
        $stale[$role->getRoleId()] = $role;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Role update');

      foreach ($result['Roles'] ?: [] as $role) {
        // Keep track of role that do not exist anymore
        // delete them after saving the rest of the roles.
        if (!empty($stale[$role['Id']])) {
          unset($stale[$role['Id']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateRole',
        ], [$cloud_context, $role]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update role.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The project update API response.
   */
  public function updateRole(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateRoles');

      return ['SendToWorker' => TRUE];
    }

    $body = [];
    foreach ($params as $key => $value) {
      if ($key === 'RoleId') {
        continue;
      }

      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      $body['role'][$key] = $value;
    }

    $credentials = [
      'http_method' => 'patch',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/roles/{$params['RoleId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    $role = !empty($response['body']['role']) ? $response['body']['role'] : [];

    return [
      'Role' => [
        'Name' => !empty($role['name']) ? $role['name'] : NULL,
        'Id' => !empty($role['id']) ? $role['id'] : NULL,
      ],
    ];
  }

  /**
   * Update the OpenStack Users.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale users.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateUsers(array $params = [], $clear = TRUE): bool {
    $entity_type = 'openstack_user';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateUserEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);

    return $updated;
  }

  /**
   * Call API for updated entities and store them as User entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale users.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateUserEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $result = $this->describeUsers($params);
    if (!empty($result)) {
      $all_users = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];

      foreach ($all_users ?: [] as $user) {
        $stale[$user->getUserId()] = $user;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('User update');

      foreach ($result['Users'] ?: [] as $user) {
        // Keep track of role that do not exist anymore
        // delete them after saving the rest of the users.
        if (!empty($stale[$user['Id']])) {
          unset($stale[$user['Id']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateUser',
        ], [$cloud_context, $user]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Update user.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The project update API response.
   */
  public function updateUser(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateUsers');

      return ['SendToWorker' => TRUE];
    }

    $body = [];
    foreach ($params as $key => $value) {
      if ($key === 'UserId') {
        continue;
      }

      // Skip the parameter default_project_id if it is empty.
      if ($key === 'DefaultProjectId' && empty($value)) {
        continue;
      }

      // Convert to snake case.
      $key = strtolower(ltrim(preg_replace('/([A-Z])/', '_$1', $key), '_'));
      $body['user'][$key] = $value;
    }

    $credentials = [
      'http_method' => 'patch',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::IDENTITY_SERVICE, "/users/{$params['UserId']}"),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    $user = !empty($response['body']['user']) ? $response['body']['user'] : [];

    return [
      'User' => [
        'Name' => !empty($user['name']) ? $user['name'] : NULL,
        'Id' => !empty($user['id']) ? $user['id'] : NULL,
      ],
    ];
  }

  /**
   * Update the OpenStackFloatingIps.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateFloatingIps(): bool {
    $entity_type = 'openstack_floating_ip';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateFloatingIpEntities($entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call the API for updated entities and store them as FloatingIp entities.
   *
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateFloatingIpEntities($entity_type = '', $cloud_context = ''): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $updated = FALSE;
    $result = $this->describeAddresses();

    if (!empty($result)) {

      $all_ips = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];
      // Make it easier to look up the groups by setting up the array with the
      // group_id.
      foreach ($all_ips ?: [] as $ip) {
        $stale[$ip->getPublicIp()] = $ip;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('FloatingIp Update');

      foreach ($result['Addresses'] ?: [] as $floating_ip) {
        // Keep track of IPs that do not exist anymore
        // delete them after saving the rest of the IPs.
        if (!empty($stale[$floating_ip['PublicIp']])) {
          unset($stale[$floating_ip['PublicIp']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateFloatingIp',
        ], [$cloud_context, $floating_ip]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, TRUE]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Get server group list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The server group list API response.
   */
  public function describeServerGroups(array $params = []): array {
    if ($this->isWorkerResource()) {
      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, '/os-server-groups'),
      'params' => $params,
    ];
    $response = $this->getClient($credentials);
    $server_groups = $response['body']['server_groups'] ?? [];

    $results['ServerGroups'] = [];
    foreach ($server_groups ?: [] as $key => $server_group) {
      $results['ServerGroups'][$key]['Name'] = $server_group['name'];
      $results['ServerGroups'][$key]['ProjectId'] = $params['project_id'] ?? '';
      $results['ServerGroups'][$key]['ServerGroupId'] = $server_group['id'];
      $results['ServerGroups'][$key]['Policy'] = array_shift($server_group['policies']);
      $results['ServerGroups'][$key]['Members'] = $server_group['members'];
    }

    return $results;
  }

  /**
   * Create server group.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The router create API response.
   */
  public function createServerGroup(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateServerGroups');
      return ['SendToWorker' => TRUE];
    }

    $body = [
      'server_group' => [
        'name' => $params['Name'],
        'policies' => [$params['Policy']],
      ],
    ];

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, '/os-server-groups'),
      'params' => $body,
    ];

    $response = $this->getClient($credentials);
    if ($response['status_code'] !== 200) {
      return NULL;
    }

    $server_group = $response['body']['server_group'] ?? [];

    return [
      'ServerGroup' => [
        'ServerGroupId' => $server_group['id'] ?? NULL,
      ],
    ];
  }

  /**
   * Update the OpenStackServerGroups.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale server groups.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateServerGroups(array $params = [], $clear = TRUE): bool {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateServerGroups');

      return ['SendToWorker' => TRUE];
    }

    $entity_type = 'openstack_server_group';
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $updated = $this->updateServerGroupEntities($params, $clear, $entity_type, $this->cloudContext);

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Call API for updated entities and store them as ServerGroup entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale server groups.
   * @param string $entity_type
   *   Entity type string.
   * @param string $cloud_context
   *   Cloud context string.
   * @param bool $update
   *   TRUE to update info.
   *
   * @return bool
   *   indicates success so failure.
   */
  public function updateServerGroupEntities(array $params = [], $clear = TRUE, $entity_type = '', $cloud_context = '', $update = TRUE): bool {
    if ($this->isWorkerResource()) {
      return TRUE;
    }

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'openstack',
        'cloud_context' => $cloud_context,
      ]);

    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    $cloud_config = array_shift($cloud_configs);

    $params['project_id'] = $cloud_config->get('field_project_id')->value ?? NULL;

    $updated = FALSE;
    $result = $this->describeServerGroups($params);
    if (!empty($result)) {
      $all_server_groups = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type);
      $stale = [];

      foreach ($all_server_groups ?: [] as $server_group) {
        $stale[$server_group->getName()] = $server_group;
      }

      /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
      $batch_builder = $this->initBatch('Server group update');

      foreach ($result['ServerGroups'] ?: [] as $server_group) {
        // Keep track of server groups that do not exist anymore
        // delete them after saving the rest of the networks.
        if (!empty($stale[$server_group['Name']])) {
          unset($stale[$server_group['Name']]);
        }
        $batch_builder->addOperation([
          OpenStackBatchOperations::class,
          'updateServerGroup',
        ], [$cloud_context, $server_group]);
      }

      $batch_builder->addOperation([
        OpenStackBatchOperations::class,
        'finished',
      ], [$entity_type, $stale, $clear]);
      $this->runBatch($batch_builder);
      $updated = TRUE;
    }
    return $updated;
  }

  /**
   * Delete server group.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteServerGroup(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'updateServerGroups');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/os-server-groups/{$params['ServerGroupId']}"),
    ];

    $response = $this->getClient($credentials);

    return $response['status_code'] === 204 ? ['Success' => TRUE] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZones($with_id = FALSE, array $credentials = []): array {
    $zones = [];
    $result = $this->describeAvailabilityZones([]);
    if (!empty($result)) {
      foreach ($result['AvailabilityZones'] ?: [] as $zone_info) {
        $zones[$zone_info['ZoneName']] = $zone_info['ZoneName'];
      }
    }
    return $zones;
  }

  /**
   * Helper method to do a Guzzle HTTP Request.
   *
   * @param string $http_method
   *   The http method in lower case.
   * @param string $endpoint
   *   The endpoint to access.
   * @param array $params
   *   Parameters to pass to the request.
   * @param bool $quiet
   *   FALSE to output error.
   *
   * @return array
   *   Array interpretation of the response body.
   */
  private function request($http_method, $endpoint, array $params = [], $quiet = FALSE): array {
    $output = [];

    try {
      $response = $this->httpClient->$http_method($endpoint, $params);
      if (!empty($response)) {
        $status_code = $response->getStatusCode();
        $headers = $response->getHeaders();
        $output['auth_token'] = $headers['X-Subject-Token'][0] ?? '';
        $output['status_code'] = $status_code;
        $body = $response->getBody()->getContents();
      }
    }
    catch (ClientException $error) {
      /** @var \GuzzleHttp\Exception\RequestException $error */
      $response = $error->getResponse();
      $error_content = [];
      $content = '';
      $status_code = '';
      if (!empty($response)) {
        $content = $response->getBody()->getContents();
        $status_code = $response->getStatusCode();
        $output['status_code'] = $status_code;
        $error_content = json_decode($content, TRUE);
      }

      $error_content = !empty($error_content) ? reset($error_content) : [];

      if (!empty($error_content) && is_string($error_content)) {

        // If OpenStackServiceInterface::getDefaultVolumeType() returns 404,
        // we do not recognize it as the error.
        if ($error_content['message'] === 'Default volume type can not be found.'
          && strpos($endpoint, '/types/default')) {
          return [];
        }

        if (!$quiet) {
          $this->messenger->addError($this->t('Status Code: @status_code', [
            '@status_code' => $status_code,
          ]));

          $this->messenger->addError($this->t('Error Message: @msg', [
            '@msg' => $error_content['message'],
          ]));
        }
      }
      else {
        if (!$quiet) {
          $this->messenger->addError($this->t('OpenStack API error. Error details are as follows: @response', [
            '@response' => $error->getMessage(),
          ]));
        }
      }

      if (!$quiet) {
        $message = new FormattableMarkup(
          'OpenStack API error for @endpoint. Error details are as follows:<pre>@response</pre>', [
            '@endpoint' => $endpoint,
            '@response' => !empty($content)
              ? $content
              : $error->getMessage(),
          ]
        );
        $this->logError('Remote API Connection', $error, $message, FALSE);
      }
    }
    catch (\Exception $error) {
      if (!$quiet) {
        $this->messenger->addError($this->t('An unknown error occurred while trying to connect to the API endpoint at: %endpoint (HTTP %http_method method). This is not a Guzzle error, not an error in the API endpoint, rather a generic local error occurred: "%error"', [
          '%endpoint' => $endpoint,
          '%error' => $error->getMessage(),
          '%http_method' => $http_method,
        ]));

        $this->logError('openstack_rest', $error, $this->t('An unknown error occurred while trying to connect to the API endpoint at: %endpoint (HTTP %http_method method). This is not a Guzzle error, not an error in the API endpoint, rather a generic local error occurred: "%error".', [
          '%endpoint' => $endpoint,
          '%error' => $error->getMessage(),
          '%http_method' => $http_method,
        ]
        ), FALSE);
      }
    }
    $body = empty($body) ? [] : json_decode($body, TRUE);
    $output['body'] = $body;
    return $output;
  }

  /**
   * Downloads an image file using the given authentication token and endpoint.
   *
   * @param string $auth_token
   *   The authentication token.
   * @param string $download_image_file_endpoint
   *   The endpoint for downloading the image file.
   * @param string $image_file_path
   *   The path to the image file.
   *
   * @return bool
   *   True if the image was downloaded successfully, false otherwise.
   */
  private function downloadImageFile(
    string $auth_token,
    string $download_image_file_endpoint,
    string $image_file_path,
  ): bool {

    try {
      $response = $this->httpClient->request('GET', $download_image_file_endpoint, [
        'headers' => [
          'X-Auth-Token' => $auth_token,
          'Content-Type' => 'application/octet-stream',
        ],
        'stream' => TRUE,
      ]);

      if ($response->getStatusCode() !== 200) {
        $this->logger('openstack_rest')->error($this->t('Failed to download image. Status code: @status_code', [
          '@status_code' => $response->getStatusCode(),
        ]));

        return FALSE;
      }

      $stream = $response->getBody();
      $chunk_size = 1024;
      $fileStream = fopen($image_file_path, 'wb');

      while (!$stream->eof()) {
        fwrite($fileStream, $stream->read($chunk_size));
      }

      fclose($fileStream);

      $this->logger('openstack_rest')->info($this->t('Image download successfully.'));

      return TRUE;
    }
    catch (\Exception $e) {
      $this->logger('openstack_rest')->error($this->t('An unknown error occurred while trying to connect to the API endpoint at: %endpoint. This is not a Guzzle error, not an error in the API endpoint, rather a generic local error occurred: "%error"', [
        '%endpoint' => $download_image_file_endpoint,
        '%error' => $e,
      ]));

      return FALSE;
    }
  }

  /**
   * Uploads an image file using the given authentication token and endpoint.
   *
   * @param string $auth_token
   *   The authentication token.
   * @param string $upload_image_file_endpoint
   *   The endpoint for uploading the image file.
   * @param string $image_file_path
   *   The path to the image file.
   *
   * @return bool
   *   True if the image was uploaded successfully, false otherwise.
   */
  private function uploadImageFile(string $auth_token, string $upload_image_file_endpoint, string $image_file_path): bool {
    try {
      // Initialize the request.
      $request = $this->httpClient->requestAsync('PUT', $upload_image_file_endpoint, [
        'headers' => [
          'X-Auth-Token' => $auth_token,
          'Content-Type' => 'application/octet-stream',
        ],
        'body' => fopen($image_file_path, 'r'),
        'stream' => TRUE,
      ]);

      // Execute the request and get the response.
      $response = $request->wait();

      // Log and return based on the status code.
      if ($response->getStatusCode() === 204) {
        $this->logger('openstack_rest')->info($this->t('Image uploaded successfully. Endpoint: @endpoint', [
          '@endpoint' => $upload_image_file_endpoint,
        ]));

        return TRUE;
      }

      // Handle non-204 status codes.
      $this->logger('openstack_rest')->error($this->t('Failed to upload image. Status code: @status_code', [
        '@status_code' => $response->getStatusCode(),
      ]));

      return FALSE;
    }
    catch (\Exception $e) {
      $this->logger('openstack_rest')->error($this->t('An unknown error occurred while trying to connect to the API endpoint at: @endpoint. This is not a Guzzle error, not an error in the API endpoint, rather a generic local error occurred: "@error"', [
        '@endpoint' => $upload_image_file_endpoint,
        '@error' => $e,
      ]));

      return FALSE;
    }
  }

  /**
   * Initialize a new batch builder.
   *
   * @param string $batch_name
   *   The batch name.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   The initialized batch object.
   */
  protected function initBatch($batch_name) {
    return (new BatchBuilder())
      ->setTitle($batch_name);
  }

  /**
   * Run the batch job to process entities.
   *
   * @param \Drupal\Core\Batch\BatchBuilder $batch_builder
   *   The batch builder object.
   */
  protected function runBatch(BatchBuilder $batch_builder) {
    try {
      // Log the start time.
      $start = $this->getTimestamp();
      $batch_array = $batch_builder->toArray();
      batch_set($batch_array);
      // Reset the progressive so batch works w/o a web head.
      $batch = &batch_get();
      $batch['progressive'] = FALSE;
      // Check if using Drush to process the queue.
      // using drush_backend_batch_process() will fix the "Route not found"
      // error.
      if (PHP_SAPI === 'cli' && function_exists('drush_backend_batch_process')) {
        drush_backend_batch_process();
      }
      else {
        batch_process();
      }
      // Log the end time.
      $end = $this->getTimestamp();
      $this->logger('openstack_rest')->info($this->t('@updater - @cloud_context: Batch operation took @time seconds.', [
        '@cloud_context' => $this->cloudContext,
        '@updater' => $batch_array['title'],
        '@time' => $end - $start,
      ]));
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    finally {
      // Reset the batch otherwise this operation hangs when using Drush.
      // https://www.drupal.org/project/drupal/issues/3166042
      $batch = [];
    }
  }

  /**
   * Log errors to watchdog and throw an exception.
   *
   * @param string $type
   *   The error type.
   * @param \Exception $exception
   *   The exception object.
   * @param string $message
   *   The message to log.
   * @param bool $throw
   *   TRUE to throw exception.
   *
   * @throws \Drupal\openstack\Service\Rest\OpenStackServiceException
   */
  private function logError($type, \Exception $exception, $message, $throw = TRUE) {
    // See also: https://www.drupal.org/node/2932520.
    Error::logException($this->logger($type), $exception, $message);
    if ($throw === TRUE) {
      throw new OpenStackServiceException($message);
    }
  }

  /**
   * Get Tag name.
   *
   * @param array $openstack_obj
   *   The openstack object.
   * @param string $default_value
   *   The default value.
   *
   * @return string
   *   Return name of corresponding resource.
   */
  public function getTagName(array $openstack_obj, $default_value) {
    $name = $default_value;

    if (!empty($openstack_obj['name'])) {
      return $openstack_obj['name'];
    }

    $tags = empty($openstack_obj['Tags']) ? [] : $openstack_obj['Tags'];
    foreach ($tags as $tag) {

      if (strpos($tag, 'Name.') === 0) {
        $name = substr($tag, strlen('Name.'));
        break;
      }
    }
    return $name;
  }

  /**
   * Setup IP Permissions.
   *
   * @param object $security_group
   *   The security group entity.
   * @param string $field
   *   Field to used for lookup.
   * @param array $ec2_permissions
   *   Permissions array from Ec2.
   */
  public function setupIpPermissions(&$security_group, $field, array $ec2_permissions) {
    // Permissions are always overwritten with the latest from
    // EC2.  The reason is that there is no way to guarantee a 1
    // to 1 mapping from the $security_group['IpPermissions'] array.
    // There is no IP permission ID coming back from EC2.
    // Clear out all items before re-adding them.
    $i = $security_group->$field->count() - 1;

    while ($i >= 0) {
      if ($security_group->$field->get($i)) {
        $security_group->$field->removeItem($i);
      }
      $i--;
    }

    // Setup all permission objects.
    $count = 0;
    foreach ($ec2_permissions ?: [] as $permissions) {
      $permission_objects = $this->setupIpPermissionObject($permissions);
      // Loop through the permission objects and add them to the
      // security group ip_permission field.
      foreach ($permission_objects ?: [] as $permission) {
        $security_group->$field->set($count, $permission);
        $count++;
      }
    }
  }

  /**
   * Set up the ip_permission field given the inbound security group array.
   *
   * The array comes from DescribeSecurityGroup Amazon EC2 API call.
   *
   * @param array $ec2_permission
   *   An array object of EC2permission.
   *
   * @return array
   *   An array of \Drupal\Core\Field\FieldItemInterface.
   */
  public function setupIpPermissionObject(array $ec2_permission) {
    $ip_permissions = [];

    // $file_path = \Drupal::service('file_system')->realpath("public://");
    // $handle = fopen($file_path . "/myfile.txt", "a+");
    // fwrite($handle, json_encode($ec2_permission));
    // fclose($handle);
    // Get the field definition for an IpPermission object.
    $definition = $this->entityFieldManager->getBaseFieldDefinitions('openstack_security_group');

    /* Set up the more global attributes.
     * NOTE: DO NOT change from isset() to !empty() here since
     * !empty($ec2_permission['FromPort']) is considered empty
     * in PHP when $ec2_permission['FromPort'] is a zero (0) value.
     */
    $rule_id = $ec2_permission['RuleId'] ?? NULL;
    $from_port = (string) ($ec2_permission['FromPort']);
    $to_port = (string) ($ec2_permission['ToPort']);

    $ip_protocol = $ec2_permission['IpProtocol'];

    // To keep things consistent, if ip_protocol is -1,
    // set from_port and to_port as 1-65535.
    if ($ip_protocol === '-1') {
      $from_port = '1';
      $to_port = '65535';
    }

    if (!empty($ec2_permission['IpRanges'])) {
      // Create a IPv4 permission object.
      foreach ($ec2_permission['IpRanges'] ?: [] as $ip_range) {
        $ip_range_permission = $this->fieldTypePluginManager->createInstance('ip_permission', [
          'field_definition' => $definition['ip_permission'],
          'parent' => NULL,
          'name' => NULL,
        ]);
        // Source is an internal identifier.  Does not come from EC2.
        $ip_range_permission->source = 'ip4';
        $ip_range_permission->cidr_ip = $ip_range['CidrIp'];
        $ip_range_permission->description = $ip_range['Description'] ?? NULL;

        $ip_range_permission->rule_id = $rule_id;
        $ip_range_permission->from_port = $from_port;
        $ip_range_permission->to_port = $to_port;
        $ip_range_permission->ip_protocol = $ip_protocol;

        $ip_permissions[] = $ip_range_permission;
      }
    }

    if (!empty($ec2_permission['Ipv6Ranges'])) {
      // Create IPv6 permissions object.
      foreach ($ec2_permission['Ipv6Ranges'] ?: [] as $ip_range) {
        $ip_v6_permission = $this->fieldTypePluginManager->createInstance('ip_permission', [
          'field_definition' => $definition['ip_permission'],
          'parent' => NULL,
          'name' => NULL,
        ]);
        // Source is an internal identifier.  Does not come from EC2.
        $ip_v6_permission->rule_id = $rule_id;
        $ip_v6_permission->source = 'ip6';
        $ip_v6_permission->cidr_ip_v6 = $ip_range['CidrIpv6'];
        $ip_v6_permission->description = $ip_range['Description'] ?? NULL;
        $ip_v6_permission->from_port = $from_port;
        $ip_v6_permission->to_port = $to_port;
        $ip_v6_permission->ip_protocol = $ip_protocol;
        $ip_permissions[] = $ip_v6_permission;
      }
    }

    if (!empty($ec2_permission['UserIdGroupPairs'])) {
      // Create Group permissions object.
      foreach ($ec2_permission['UserIdGroupPairs'] ?: [] as $group) {
        $group_permission = $this->fieldTypePluginManager->createInstance('ip_permission', [
          'field_definition' => $definition['ip_permission'],
          'parent' => NULL,
          'name' => NULL,
        ]);
        // Source is an internal identifier.  Does not come from EC2.
        $group_permission->rule_id = $rule_id;
        $group_permission->source = 'group';
        $group_permission->group_id = $group['GroupId'] ?? NULL;
        $group_permission->group_name = $group['GroupName'] ?? NULL;
        $group_permission->user_id = $group['UserId'] ?? NULL;
        $group_permission->peering_status = $group['PeeringStatus'] ?? NULL;
        $group_permission->vpc_id = $group['VpcId'] ?? NULL;
        $group_permission->peering_connection_id = $group['VpcPeeringConnectionId'] ?? NULL;
        $group_permission->description = $group['Description'] ?? NULL;
        $group_permission->from_port = $from_port;
        $group_permission->to_port = $to_port;
        $group_permission->ip_protocol = $ip_protocol;
        $ip_permissions[] = $group_permission;
      }
    }

    if (!empty($ec2_permission['PrefixListIds'])) {
      foreach ($ec2_permission['PrefixListIds'] ?: [] as $prefix) {
        $prefix_permission = $this->fieldTypePluginManager->createInstance('ip_permission', [
          'field_definition' => $definition['ip_permission'],
          'parent' => NULL,
          'name' => NULL,
        ]);
        // Source is an internal identifier.  Does not come from EC2.
        $prefix_permission->rule_id = $rule_id;
        $prefix_permission->source = 'prefix';
        $prefix_permission->prefix_list_id = $prefix['PrefixListId'];
        $prefix_permission->description = $prefix['Description'] ?? NULL;
        $prefix_permission->from_port = $from_port;
        $prefix_permission->to_port = $to_port;
        $prefix_permission->ip_protocol = $ip_protocol;
        $ip_permissions[] = $prefix_permission;
      }
    }

    return $ip_permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function createResourceQueueItems(CloudConfigInterface $cloud_config): void {
    $queue_limit = $this->configFactory->get('openstack.settings')->get('openstack_queue_limit');
    $method_names = [
      'updateInstances',
      'updateServerGroups',
      'updateImages',
      'updateKeyPairs',
      'updateVolumes',
      'updateSnapshots',
      'updateSecurityGroups',
      'updateFloatingIps',
      'updateNetworks',
      'updateSubnets',
      'updatePorts',
      'updateRouters',
      'updateQuotas',
      'updateProjects',
      'updateRoles',
      'updateUsers',
    ];
    self::updateResourceQueue($cloud_config, $method_names, $queue_limit);
  }

  /**
   * Get UID tag value.
   *
   * @param array $tags_array
   *   The tags array of API.
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return int
   *   Return user id.
   */
  public function getUidTagValue(array $tags_array, string $entity_type_id): int {
    $bundle = $this->entityTypeManager->getDefinition($entity_type_id)->getProvider();

    $key = $this->getTagKeyCreatedByUid(
      $bundle,
      $this->cloudContext
    );

    $uid = 0;
    if (!empty($tags_array['Tags'])) {
      foreach ($tags_array['Tags'] ?: [] as $tag) {
        if (strpos($tag, $key . '.') === 0) {
          $uid = substr($tag, strlen($key . '.'));
          break;
        }
      }
    }
    return is_numeric($uid) ? $uid : 0;
  }

  /**
   * Get InstanceUID.
   *
   * @param string $instance_id
   *   The instance id.
   *
   * @return int
   *   Return user id.
   */
  public function getInstanceUid($instance_id) {
    $uid = 0;
    $instances = $this->entityTypeManager
      ->getStorage('openstack_instance')
      ->loadByProperties([
        'instance_id' => $instance_id,
      ]);

    if (count($instances) > 0) {
      /** @var \Drupal\aws_cloud\Entity\Ec2\InstanceInterface $instance */
      $instance = array_shift($instances);
      $uid = $instance->getOwnerId();
    }
    return $uid;
  }

  /**
   * Get InstanceUID.
   *
   * @param string $service
   *   The OpenStack service name such as 'identity', 'compute', 'image',
   *   'network', and 'volume'.
   * @param string $path
   *   The endpoint path such as `/servers/detail`.
   * @param bool $include_version
   *   Whether to include the service version in the endpoint URL.
   *   Defaults to TRUE.
   *
   * @return string
   *   The full endpoint.
   */
  private function getEndpoint(string $service, string $path, bool $include_version = TRUE): string {
    $credentials = $this->loadCredentials();
    $endpoint = rtrim($credentials["{$service}_api_endpoint"] ?: '', '/');
    $version = trim($credentials["{$service}_version"] ?: '', '/');
    if (!empty($include_version)) {
      $endpoint .= '/' . $version;
    }
    $endpoint .= '/' . trim($path, '/');

    return $endpoint;
  }

  /**
   * {@inheritdoc}
   */
  public function isWorkerResource(): bool {
    if (empty($this->cloudContext)) {
      return FALSE;
    }

    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    return $cloud_config->isRemote();
  }

  /**
   * Save operation to queue.
   *
   * @param string $operation
   *   The operation to perform.
   * @param array $params
   *   An array of parameters.
   * @param string $update_entities_method
   *   The method name of updating entities.
   * @param array $update_entities_method_params
   *   The method parameters of updating entities.
   * @param array $refs
   *   The array of references.
   */
  private function saveOperation(string $operation, array $params, string $update_entities_method = '', ?array $update_entities_method_params = NULL, array $refs = []): void {
    if (empty($this->cloudContext)) {
      return;
    }

    // Skip describe operations.
    if (strpos($operation, 'Describe') === 0) {
      return;
    }

    // Skip dry run operations.
    if (!empty($params[0]['DryRun'])) {
      return;
    }

    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    if (!$cloud_config->isRemote()) {
      return;
    }

    if (empty($update_entities_method)) {
      $update_entities_method = preg_replace('/^[[:lower:]]+/', 'update', lcfirst($operation));
    }

    $update_entities_method_params = [
      $update_entities_method_params ?? $params[0],
      FALSE,
    ];

    $queue = $this->queueFactory->get('operation_queue_' . $cloud_config->get('cloud_cluster_name')->value . $cloud_config->get('cloud_cluster_worker_name')->value);
    $queue->createItem([
      'cloud_context' => $this->cloudContext,
      'operation' => $operation,
      'params' => $params,
      'service_name' => 'openstack.rest',
      'update_entities_method' => $update_entities_method,
      'update_entities_method_params' => $update_entities_method_params,
      'refs' => $refs,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getTagKeyCreatedByUid(
    string $bundle,
    string $cloud_context,
    int $uid = 0,
  ): string {
    $tag_name = $this->cloudService->getTagKeyCreatedByUid($bundle, $cloud_context);
    return strlen($tag_name . ".{$uid}") >= OpenStackServiceInterface::MAX_TAG_LENGTH
      ? substr($tag_name, 0, OpenStackServiceInterface::MAX_TAG_LENGTH - strlen(".{$uid}") - 1)
      : $tag_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getTagValueCreatedByUid(
    string $bundle,
    string $cloud_context,
    int $uid,
  ): string {
    $tag_format = '{tag}.{uid}';

    $context = [
      '{tag}' => $this->getTagKeyCreatedByUid($bundle, $cloud_context, $uid),
      '{uid}' => $uid,
    ];

    return strtr($tag_format, $context);
  }

  /**
   * Get server console output.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return string
   *   Console output.
   */
  public function getConsoleOutput(array $params = []): string {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'getConsoleOutput');

      return ['SendToWorker' => TRUE];
    }

    $body = [
      'os-getConsoleOutput' => [
        'length' => $params['lines'] === 'ALL' ? NULL : $params['lines'],
      ],
    ];
    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/action"),
      'params' => $body,
    ];
    $response = $this->getClient($credentials);
    return $response['status_code'] === 200 ? $response['body']['output'] : '';
  }

  /**
   * Get server console.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   Console.
   */
  public function getConsole(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'getConsole');

      return ['SendToWorker' => TRUE];
    }

    foreach (self::INSTANCE_CONSOLES as $console) {
      $body = [
        'remote_console' => [
          'protocol' => $console['protocol'],
          'type' => $console['type'],
        ],
      ];

      $credentials = [
        'http_method' => 'post',
        'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/remote-consoles"),
        'params' => $body,
        'X-OpenStack-Nova-API-Version' => OpenStackServiceInterface::OPENSTACK_NOVA_API_VERSION,
      ];

      $response = $this->getClient($credentials, TRUE);
      if ($response['status_code'] === 200) {
        return $response['body']['remote_console'];
      }
    }

    return [];
  }

  /**
   * Get server action log.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   Action log.
   */
  public function getActionLog(array $params = []): array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'getActionLog');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'get',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/os-instance-actions"),
      'params' => [],
    ];
    $response = $this->getClient($credentials);
    return $response['status_code'] === 200 ? $response['body']['instanceActions'] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function createPortInterface(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'createPortInterface');

      return ['SendToWorker' => TRUE];
    }

    if (!empty($params['port_id'])) {
      $body = [
        'interfaceAttachment' => [
          'port_id' => $params['port_id'],
        ],
      ];
    }
    else {
      $body = [
        'interfaceAttachment' => [
          'net_id' => $params['net_id'],
        ],
      ];

      if (!empty($params['fixed_ips'])) {
        $body['interfaceAttachment']['fixed_ips'] = [
          [
            'ip_address' => $params['fixed_ips'],
          ],
        ];
      }
    }

    $credentials = [
      'http_method' => 'post',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/os-interface"),
      'params' => $body,
    ];
    $response = $this->getClient($credentials);

    return $response['status_code'] === 200 ? ['Success' => TRUE] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deletePortInterface(array $params = []): ?array {
    if ($this->isWorkerResource()) {
      $this->saveOperation(__FUNCTION__, [$params], 'deletePortInterface');

      return ['SendToWorker' => TRUE];
    }

    $credentials = [
      'http_method' => 'delete',
      'endpoint' => $this->getEndpoint(OpenStackServiceInterface::COMPUTE_SERVICE, "/servers/{$params['InstanceId']}/os-interface/{$params['port_id']}"),
    ];
    $response = $this->getClient($credentials);

    return $response['status_code'] === 202 ? ['Success' => TRUE] : NULL;
  }

}
