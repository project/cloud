<?php

namespace Drupal\openstack\Service\Rest;

/**
 * OpenStack Native Service Exception.
 */
class OpenStackServiceException extends \Exception {

}
