<?php

namespace Drupal\openstack\Service\Rest;

use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\openstack\Service\OpenStackServiceInterface;

/**
 * OpenStackServiceMock service interacts with the OpenStack API.
 */
class OpenStackServiceMock extends OpenStackService {

  use ConfigFormBaseTrait;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['openstack.settings'];
  }

  /**
   * Generate auth token.
   *
   * @param array $credentials
   *   The credentials array.
   *
   * @return array
   *   Return auth token API response.
   */
  public function generateAuthToken(array $credentials = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get instance list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The instance list API response.
   */
  public function describeInstances(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The instance update API response.
   */
  public function modifyInstance(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Start instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The instance Start API response.
   */
  public function startInstances(array $params = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * Stop instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The instance Stop API response.
   */
  public function stopInstances(array $params = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * Reboot instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The instance reboot API response.
   */
  public function rebootInstances(array $params = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * Terminate|Delete instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The instance terminate|Delete API response.
   */
  public function terminateInstances(array $params = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * Delete tags.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The instance delete list API response.
   */
  public function deleteTags(array $params = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function describeImages(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image create API response.
   */
  public function createImage(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image update API response.
   */
  public function updateImage(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The image delete API response.
   */
  public function deregisterImage(array $params = []): ?array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get shared image list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image update API response.
   */
  public function describeProjectSharedImage(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete shared members list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image update API response.
   */
  public function deleteProjectSharedMembers(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update shared project list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The image update API response.
   */
  public function updateImageSharedProjects(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create Tags.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The Tags create API response.
   */
  public function createTags(array $params = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * Get key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The key pairs list API response.
   */
  public function describeKeyPairs(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The key pairs create API response.
   */
  public function createKeyPair(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Import key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The key pairs import API response.
   */
  public function importKeyPair(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete key pairs List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteKeyPair(array $params = []): ?array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get volume list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume list API response.
   */
  public function describeVolumes(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get volume types.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume list API response.
   */
  public function describeVolumeTypes(array $params = []) {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get a default volume type.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume list API response.
   */
  public function getDefaultVolumeType(array $params = []) {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume create API response.
   */
  public function createVolume(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The volume update API response.
   */
  public function updateVolume(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteVolume(array $params = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * Attach volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function attachVolume(array $params = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * Detach volume.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function detachVolume(array $params = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * Get snapshot list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The snapshot list API response.
   */
  public function describeSnapshots(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create snapshot.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The snapshot create API response.
   */
  public function createSnapshot(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update snapshot.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The snapshot update API response.
   */
  public function updateSnapshot(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete snapshot.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteSnapshot(array $params = []): ?array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get flavors.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The flavor list API response.
   */
  public function describeFlavors(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get the flavor detail of specific flavor ID.
   *
   * @param array $params
   *   Parameters to pass to the API.
   * @param string $flavor_id
   *   The flavor ID.
   *
   * @return array
   *   The flavor list API response.
   */
  public function describeFlavorById(array $params = [], $flavor_id = ''): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get tbe volume detail of specific volume ID.
   *
   * @param array $params
   *   Parameters to pass to the API.
   * @param string $volume_id
   *   The volume ID.
   *
   * @return array
   *   The volume list API response.
   */
  public function describeVolumeById(array $params = [], $volume_id = ''): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get security group list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group list API response.
   */
  public function describeSecurityGroups(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create security group list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group create API response.
   */
  public function createSecurityGroup(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update security group List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group update API response.
   */
  public function updateSecurityGroup(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete security group List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   Succeeded or failed.
   */
  public function deleteSecurityGroup(array $params = []): ?array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create security group Rule.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group rule create API response.
   */
  public function createSecurityGroupRule(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Revoke security group list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The security group rule revoke API response.
   */
  public function revokeSecurityGroupRule(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get the subnet detail.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The subnet detail list API response.
   */
  public function describeSubnetDetail(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get network list.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The network list API response.
   */
  public function describeNetworks(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create network.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The network create API response.
   */
  public function createNetwork(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update network.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The network update API response.
   */
  public function updateNetwork(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete network.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The network delete API response.
   */
  public function deleteNetwork(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get quotas.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The quota API response.
   */
  public function describeQuotas(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update quota.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The quota update API response.
   */
  public function updateQuota(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get stacks.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function describeStacks(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get stack resources.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function describeStackResources(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get stack events.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function describeStackEvents(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Show stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function showStack(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Check stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function checkStack(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Suspend stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function suspendStack(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Resume stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function resumeStack(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack create API response.
   */
  public function createStack(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack update API response.
   */
  public function updateStack(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Preview stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack update API response.
   */
  public function previewStack(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete stack.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack delete API response.
   */
  public function deleteStack(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get subnets.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The subnet API response.
   */
  public function describeSubnets(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create subnet.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The subnet create API response.
   */
  public function createSubnet(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update subnet.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The subnet update API response.
   */
  public function updateSubnet(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete subnet.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The subnet delete API response.
   */
  public function deleteSubnet(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get ports.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The port API response.
   */
  public function describePorts(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create an OpenStack port.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The port create API response.
   */
  public function createPort(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update an OpenStack port.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The port update API response.
   */
  public function updatePort(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete an OpenStack port.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The port delete API response.
   */
  public function deletePort(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get routers.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The router API response.
   */
  public function describeRouters(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create router.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The router create API response.
   */
  public function createRouter(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update router.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The router update API response.
   */
  public function updateRouter(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete router.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The router delete API response.
   */
  public function deleteRouter(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get server groups.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The server group API response.
   */
  public function describeServerGroups(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create server group.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The router create API response.
   */
  public function createServerGroup(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete server group.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The router create API response.
   */
  public function deleteServerGroup(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get Floating IP List.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP list API response.
   */
  public function describeAddresses(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP create API response.
   */
  public function allocateAddress(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Associate Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP associate API response.
   */
  public function associateAddress(array $params = []):array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Disassociate Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Floating IP associate API response.
   */
  public function disassociateAddress(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Instance Port Interface.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The Instance Port Interface list API response.
   */
  public function instancePortInterface(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get template versions.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function describeTemplateVersions(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get template version functions.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function describeTemplateVersionFunctions(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create project.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function createProject(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update project.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function updateProject(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete project.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function deleteProject(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get projects.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function describeProjects(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get project user roles.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function describeProjectUserRoles(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create role.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function createRole(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update role.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function updateRole(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete role.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function deleteRole(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get role.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function describeRoles(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Create user.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function createUser(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Assign a role to a user on a project.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The assign API response.
   */
  public function assignRoleToUserOnProject(array $params = []): ?array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Update user.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function updateUser(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete user.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function deleteUser(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Get user.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array
   *   The stack API response.
   */
  public function describeUsers(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * Delete Floating IP.
   *
   * @param array $params
   *   Parameters to pass to the API.
   *
   * @return array|null
   *   The Floating IP delete API response.
   */
  public function releaseAddress(array $params = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function describeAvailabilityZones(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityZones($with_id = FALSE, array $credentials = []): array {
    $result = $this->describeAvailabilityZones([]);
    $zones = [];
    if (!empty($result)) {
      foreach ($result['AvailabilityZones'] ?: [] as $zone_info) {
        $zones[$zone_info['ZoneName']] = $zone_info['ZoneName'];
      }
    }
    return $zones;
  }

  /**
   * Check if openStack network extensions are enabled.
   *
   * @return bool
   *   Whether the extension is enabled.
   */
  public function isNetworkExtensionSupported(string $extension_alias): bool {
    return TRUE;
  }

  /**
   * Run instance.
   *
   * @param array $params
   *   Parameters to pass to the API.
   * @param array $tags
   *   The tags array.
   *
   * @return array|null
   *   The create instance list API response.
   */
  public function runInstances(array $params = [], array $tags = []): ?array {
    return ['Success' => TRUE];
  }

  /**
   * Get the roles of the current user.
   *
   * @return array
   *   The user and roles API response.
   */
  public function describeCurrentUserRoles(): array {
    return [OpenStackServiceInterface::ADMIN_ROLE];
  }

  /**
   * Get mock data for a method.
   *
   * @param string $method_name
   *   The method name.
   *
   * @return array
   *   An array of the mock data for a method.
   */
  private function getMockData($method_name): array {
    return json_decode($this->config('openstack.settings')->get('openstack_mock_data'), TRUE)[ucfirst($method_name)] ?? [];
  }

}
