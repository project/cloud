<?php

namespace Drupal\openstack\Traits;

/**
 * The trait for OpenStack form select control.
 */
trait OpenStackFormSelectTrait {

  /**
   * Get network options.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $properties
   *   The properties.
   *
   * @return array
   *   The network options.
   */
  private function getNetworkOptions($cloud_context, array $properties = []): array {
    $properties['cloud_context'] = $cloud_context;

    $account = \Drupal::currentUser();
    if (!$account->hasPermission('view any openstack network')) {
      $properties['uid'] = $account->id();
    }

    $entities = $this->entityTypeManager
      ->getStorage('openstack_network')
      ->loadByProperties($properties);

    $options = [];
    foreach ($entities ?: [] as $entity) {
      $options[$entity->getNetworkId()] = $entity->getName();
    }

    return $options;
  }

  /**
   * Get subnet options.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $network_id
   *   The network ID.
   *
   * @return array
   *   The subnet options.
   */
  private function getSubnetOptions($cloud_context, $network_id = NULL): array {
    $properties = [
      'cloud_context' => $cloud_context,
    ];

    if (!empty($network_id)) {
      $properties['network_id'] = $network_id;
    }

    $account = \Drupal::currentUser();
    if (!$account->hasPermission('view any openstack subnet')) {
      $properties['uid'] = $account->id();
    }

    $entities = $this->entityTypeManager
      ->getStorage('openstack_subnet')
      ->loadByProperties($properties);

    $options = [];
    foreach ($entities ?: [] as $entity) {
      $options[$entity->getSubnetId()] = $entity->getName();
    }

    return $options;
  }

  /**
   * Get security group options.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return array
   *   The security group options.
   */
  private function getSecurityGroupOptions($cloud_context): array {
    $properties = [
      'cloud_context' => $cloud_context,
    ];

    $account = \Drupal::currentUser();
    if (!$account->hasPermission('view any openstack security group')) {
      $properties['uid'] = $account->id();
    }

    $entities = $this->entityTypeManager
      ->getStorage('openstack_security_group')
      ->loadByProperties($properties);

    $options = [];
    foreach ($entities ?: [] as $entity) {
      $options[$entity->getGroupId()] = $entity->getName();
    }

    return $options;
  }

  /**
   * Get project options.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $properties
   *   The properties.
   *
   * @return array
   *   The project options.
   */
  private function getProjectOptions($cloud_context, array $properties = []): array {
    $properties['cloud_context'] = $cloud_context;

    $entities = $this->entityTypeManager
      ->getStorage('openstack_project')
      ->loadByProperties($properties);

    $options = [];
    foreach ($entities ?: [] as $entity) {
      $options[$entity->getProjectId()] = $entity->getName();
    }

    return $options;
  }

}
