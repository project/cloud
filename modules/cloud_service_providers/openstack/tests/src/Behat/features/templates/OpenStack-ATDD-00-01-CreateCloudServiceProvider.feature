@minimal @ci_job @setup @launch_instance
Feature: Create a cloud service provider for OpenStack as a "Cloud administrator"
  In order to start BDD testing
  We need to create a Cloud service provider

  @api @javascript
  Scenario: Add a Cloud service provider
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config/add/openstack"
    And I take screenshot
    And I enter "{{ cloud_service_provider_name_entered }}" for "Name"
    And I enter "{{ user_name }}" for "Username"
    And I enter "{{ user_password }}" for "Password"
    And I enter "{{ domain_id }}" for "Domain ID"
    And I enter "{{ project_id }}" for "Project ID"
    And I enter "{{ region }}" for "Region"
    And I enter "{{ ca_path }}" for "Self-signed CA certificate path"
    And I enter "{{ api_keystone }}" for "Identity API endpoint"
    And I enter "{{ api_nova }}" for "Compute API endpoint"
    And I enter "{{ api_glance }}" for "Image API endpoint"
    And I enter "{{ api_neutron }}" for "Network API endpoint"
    And I enter "{{ api_cinder }}" for "Volume API endpoint"
    And I enter "{{ api_heat }}" for "Orchestration API endpoint"
    And I press "Save"
    And I wait for AJAX to finish
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see OpenStack in the "{{ cloud_service_provider_name }}" row
