@minimal @ci_job @setup @launch_instance
Feature: Create a Drupal role for OpenStack and a user as an "administrator"

  @api
  Scenario: Create a role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/roles/add"
    And I enter "{{ drupal_role_name }}" for "Role name"
    And I enter "{{ drupal_role_name_machine }}" for "Machine-readable name"
    And I press "Save"
    And I should be on "/admin/people/roles"
    And I should see "{{ drupal_role_name }}"
    Then I visit "admin/people/permissions/{{ drupal_role_name_machine }}"
    # Allow to see the cloud service provider
    And I check the box "Access {{ cloud_service_provider_name }}"
    # Allow to view and edit private image by public image case
    And I check the box "View any OpenStack image"
    And I check the box "Edit any OpenStack image"
    # Allow to launch an instance from any launch templates
    And I check the box "View any cloud launch templates"
    And I check the box "Launch approved cloud launch template"
    And I check the box "Update resource list"
    And I press "Save permissions"
    And I should see the success message "The changes have been saved."
    And I should see neither error nor warning messages

  @api
  Scenario Outline: Create users
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/create"
    And I enter "<drupal_user_name>" for "Username"
    And I enter "<drupal_user_password>" for "Password"
    And I enter "<drupal_user_password>" for "Confirm password"
    And I check the box "<drupal_role_name>"
    And I press "Create new account"
    Then I should see the success message "Created a new user account for"
    And I should see neither error nor warning messages
    And I visit "/admin/people"
    And I should see "<drupal_user_name>"

    Examples:
    | drupal_user_name              | drupal_user_password              | drupal_role_name       |
    | {{ drupal_user_name }}        | {{ drupal_user_password }}        | {{ drupal_role_name }} |
    | {{ drupal_cloud_admin_name }} | {{ drupal_cloud_admin_password }} | Cloud administrator    |
