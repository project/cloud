@ci_job
Feature: Create, view, edit and delete a cloud service provider for OpenStack as a "Cloud administrator"

  @api
  Scenario Outline: Add a Cloud service provider
    Given I am logged in as a user with the "<role>" role
    When I visit "/admin/structure/cloud_config/add/openstack"
    And I enter "<cloud_service_provider_name>" for "Name"
    And I enter "{{ user_name }}" for "Username"
    And I enter "{{ user_password }}" for "Password"
    And I enter "{{ domain_id }}" for "Domain ID"
    And I enter "{{ project_id }}" for "Project ID"
    And I enter "{{ region }}" for "Region"
    And I enter "{{ ca_path }}" for "Self-signed CA certificate path"
    And I enter "{{ api_keystone }}" for "Identity API endpoint"
    And I enter "{{ api_nova }}" for "Compute API endpoint"
    And I enter "{{ api_glance }}" for "Image API endpoint"
    And I enter "{{ api_neutron }}" for "Network API endpoint"
    And I enter "{{ api_cinder }}" for "Volume API endpoint"
    And I enter "{{ api_heat }}" for "Orchestration API endpoint"
    And I press "Save"
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    # No type shows.
    # And I should see OpenStack in the "{{ cloud_service_provider_name_operate }}" row

    Examples:
      | role                | cloud_service_provider_name                   |
      | Administrator       | {{ cloud_service_provider_name_admin }}       |
      | Cloud administrator | {{ cloud_service_provider_name_cloud_admin }} |

  @api @javascript
  Scenario Outline: View the Cloud service provider
    Given I am logged in as a user with the "<role>" role
    When I visit "/admin/structure/cloud_config"
    And I take screenshot
    And I should see the link "<cloud_service_provider_name>" in the table
    And I wait {{ wait }} milliseconds
    And I click "Edit" in the "<cloud_service_provider_name>" row
    And the url should match "/edit"
    And I take screenshot
    And I click "View"
    Then the url should match "/admin/structure/cloud_config"
    And I take screenshot
    And I should not see "<cloud_context>"
    And I should see the heading "<cloud_service_provider_name>"
    And I should see "<cloud_service_provider_name>" in the "Name"
    And I should see "{{ user_name }}" in the "Username"
    And I should see "{{ user_password }}" in the "Password"
    And I should see "{{ domain_id }}" in the "Domain ID"
    And I should see "{{ project_id }}" in the "Project ID"
    And I should see "{{ region }}" in the "Region"
    # No field shown when the value is empty.
    # And I should see "{{ ca_path }}" in the "Self-signed CA certificate path"
    And I should see "{{ api_keystone }}" in the "Identity API endpoint"
    And I should see "{{ api_nova }}" in the "Compute API endpoint"
    And I should see "{{ api_glance }}" in the "Image API endpoint"
    And I should see "{{ api_neutron }}" in the "Network API endpoint"
    And I should see "{{ api_cinder }}" in the "Volume API endpoint"
    And I should see "{{ api_heat }}" in the "Orchestration API endpoint"
    And I should see neither error nor warning messages

    Examples:
      | role                | cloud_context                  | cloud_service_provider_name                  |
      | Administrator       | {{cloud_context_admin}}        | {{ cloud_service_provider_name_admin }}      |
      | Cloud administrator | {{cloud_context_cloud_admin}}  |{{ cloud_service_provider_name_cloud_admin }} |

  @api
  Scenario Outline: Update the Cloud service provider
    Given I am logged in as a user with the "<role>" role
    When I visit "/admin/structure/cloud_config"
    And I should see the link "<cloud_service_provider_name>" in the table
    And I wait {{ wait }} milliseconds
    And I click "Edit" in the "<cloud_service_provider_name>" row
    Then the url should match "/edit"
    And I enter "<cloud_service_provider_name_updated>" for "Name"
    #  Needs to add the password as it is empty.
    And I enter "{{ user_password }}" for "Password"
    # press is supported by @javascript, but this causes other issues.
    # TypeError: Cannot read properties of null (reading 'getBoundingClientRect')
    # And I press "Others"
    # Remove the following step once the cloud admin can update it regardless of the owner status.
    And I fill in "Authored by" with "{{ cloud_service_provider_author }}"
    And I press "Save"
    #And I wait for AJAX to finish
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    # Need to check the link, instead of text as it is still seen as cloud_context.
    And I should not see the link "<cloud_service_provider_name>" in the table
    And I click "<cloud_service_provider_name_updated>" in the "success_message"
    And the url should match "admin/structure/cloud_config/"
    And I should see the heading "<cloud_service_provider_name_updated>"
    And I should see "<cloud_service_provider_name_updated>" in the "details"

    Examples:
      | role                 | cloud_service_provider_name                   | cloud_service_provider_name_updated                   |
      | Administrator        | {{ cloud_service_provider_name_admin }}       | {{ cloud_service_provider_name_admin_updated }}       |
      | Cloud administrator  | {{ cloud_service_provider_name_cloud_admin }} | {{ cloud_service_provider_name_cloud_admin_updated }} |

  @api @javascript
  Scenario Outline: Take a screenshot of the delete form by Cloud administrator
    Given I am logged in as a user with the "<role>" role
    When I visit "/admin/structure/cloud_config"
    And I should see the link "<cloud_service_provider_name_updated>" in the table
    And I click "Edit" in the "<cloud_service_provider_name_updated>" row
    And I click "Delete" in the "nav_tabs"
    Then the url should match "/delete"
    And I take screenshot

    Examples:
      | role                | cloud_service_provider_name_updated                   |
      | Cloud administrator | {{ cloud_service_provider_name_cloud_admin_updated }} |

  @api @javascript
  Scenario Outline: Take a screenshot of the Delete multiple form by Administrator
    Given I am logged in as a user with the "<role>" role
    When I visit "/admin/structure/cloud_config"
    And I check the box in the "<cloud_service_provider_name_updated>" row
    And I select "Delete cloud service provider(s)" from "Action"
    And I press "Apply to selected items"
    Then the url should match "/delete_multiple"
    And I take screenshot

    Examples:
      | role          | cloud_service_provider_name_updated             |
      | Administrator | {{ cloud_service_provider_name_admin_updated }} |

  @api
  Scenario Outline: Delete the updated Cloud service provider
    Given I am logged in as a user with the "<role>" role
    When I visit "/admin/structure/cloud_config"
    And I check the box in the "<cloud_service_provider_name_updated>" row
    And I select "Delete cloud service provider(s)" from "Action"
    And I press "Apply to selected items"
    And I press "Delete"
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "Deleted 1 item."
    And I should see neither error nor warning messages
    And I should not see the link "<cloud_service_provider_name_updated>"
    # Set the longer name first as the multiple boxes with a matched name are checked.

    Examples:
      | role                | cloud_service_provider_name_updated                   |
      | Cloud administrator | {{ cloud_service_provider_name_cloud_admin_updated }} |
      | Administrator       | {{ cloud_service_provider_name_admin_updated  }}      |
