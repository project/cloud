@minimal @ci_job
Feature: Read OpenStack configurations as "Administrator"

  @api @javascript
  Scenario: View OpenStack Notification Settings
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/config/services/cloud/openstack/notification"
    And I take screenshot

  @api @javascript
  Scenario: View OpenStack Settings
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/config/services/cloud/openstack/settings"
    And I take screenshot
