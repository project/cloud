@minimal @ci_job
Feature: Confirm OpenStack resources created through the previous BDD tests do not exist

  @api
  Scenario Outline: Confirm no resources are listed
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/openstack/{{ cloud_context }}/<resource_type>"
    And I should see the heading "OpenStack <resource_type_name>"
    And I click "Refresh"
    Then I should not see "BDD" in the table or no such region
    And I should see neither error nor warning messages

    Examples:
      | resource_type    | resource_type_name |
      | instance         | instance           |
      | image            | images             |
      | security_group   | security groups    |
      | floating_ip      | floating IP        |
      | key_pair         | key pairs          |
      | volume           | volumes            |
      | snapshot         | snapshots          |
      | network          | networks           |
      | subnet           | subnets            |
      | port             | ports              |
      | router           | routers            |
      | quota            | quotas             |
      | stack            | stacks             |
      | template_version | template versions  |
      | server_group     | server groups      |
      | project          | projects           |
      | role             | roles              |
      | user             | users              |

  @api
  Scenario Outline: Access is denied for authenticated users
    Given I am logged in as a user with the "Authenticated user" role
    When I go to "/clouds/openstack/{{ cloud_context }}/<resource_type>"
    Then I should get a 403 HTTP response
    And I should see the text "Access denied"

    Examples:
      | resource_type    |
      | instance         |
      | image            |
      | security_group   |
      | floating_ip      |
      | key_pair         |
      | volume           |
      | snapshot         |
      | network          |
      | subnet           |
      | port             |
      | router           |
      | quota            |
      | stack            |
      | template_version |
      | server_group     |
      | project          |
      | role             |
      | user             |
