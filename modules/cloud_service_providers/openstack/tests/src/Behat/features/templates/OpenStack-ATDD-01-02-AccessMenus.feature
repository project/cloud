@ci_job
Feature: View the created resources for OpenStack as a "Cloud administrator"

  @api
  Scenario Outline: View the menu of OpenStack Cloud service providers
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds"
    Then I should get a 200 HTTP response
    And I should see the heading "Cloud service providers"
    Then I should see "Location map"
    And I should see the button "Apply"
    # DO NOT change <status> to "<status>" since <status> is not an :arg1.
    And I should <status> "No items." in the table
    And I should see neither error nor warning messages

    Examples:
      | role                | status  |
      | Authenticated user  | see     |
      | Cloud administrator | not see |

  @api @javascript
  Scenario Outline: View the list of OpenStack resources
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/instance"
    Then I should get a 200 HTTP response
    And I take screenshot
    And I should see the heading "All OpenStack instances"
    And I should see the button "Apply"
    And I should see the link "Instances"

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack floating ips
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/floating_ip"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack images
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/image"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack key pairs
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/key_pair"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack projects
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/project"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack ports
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/port"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack quotas
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/quota"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack networks
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/network"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack volumes
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/volume"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack users
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/user"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack snapshots
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/snapshot"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack security groups
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/security_group"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack routers
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/router"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack flavors
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/flavor"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack template versions
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/template_version"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack roles
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/role"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack server groups
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/server_group"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack stacks
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/stack"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |

  @api @javascript
  Scenario Outline: View the list of OpenStack subnets
    Given I am logged in as a user with the "<role>" role
    And I should see the link "All" in the "nav_bar"
    And I should see the link "OpenStack" in the "nav_bar"
    When I go to "/clouds/openstack/subnet"
    Then I should get a 200 HTTP response
    And I take screenshot

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |
