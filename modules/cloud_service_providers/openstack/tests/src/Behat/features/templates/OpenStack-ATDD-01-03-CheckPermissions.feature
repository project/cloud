@minimal @ci_job
Feature: Check the permission setting as an "authenticated user"

  @api
  Scenario Outline: Confirm the permissions of the authenticated user role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/permissions/authenticated"
    Then the checkbox "Add OpenStack <resource_type>" should be checked
    Then the checkbox "List OpenStack <resource_type>" should be checked
    And the checkbox "View own OpenStack <resource_type>" should be checked
    And the checkbox "Edit own OpenStack <resource_type>" should be checked
    And the checkbox "Delete own OpenStack <resource_type>" should be checked

    Examples:
      | resource_type  |
      | image          |
      | security group |
      | floating IP    |
      | volume         |
      | snapshot       |
      | network        |
      | subnet         |
      | port           |
      | router         |
      | stack          |

  @api
  Scenario Outline: Confirm the permissions for view-only resources
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/permissions/authenticated"
    Then the checkbox "List OpenStack <resource_type>" should be checked
    And the checkbox "View OpenStack <resource_type>" should be checked

    Examples:
      | resource_type    |
      | quota            |
      | template version |
      | server group     |
      | project          |
      | role             |
      | user             |

  @api
  Scenario: View the key pair permissions of the authenticated user role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/permissions/authenticated"
    Then the checkbox "Add OpenStack key pair" should be checked
    Then the checkbox "List OpenStack key pair" should be checked
    And the checkbox "View own OpenStack key pair" should be checked
    # a key pair does not have any field to update
    And the checkbox "Delete own OpenStack key pair" should be checked

  @api
  Scenario: View the instance permissions of the authenticated user role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/people/permissions/authenticated"
    Then the checkbox "Add / Launch OpenStack instance" should be checked
    Then the checkbox "List OpenStack instance" should be checked
    And the checkbox "View own OpenStack instance" should be checked
    And the checkbox "Edit own OpenStack instance" should be checked
    And the checkbox "Delete / Terminate own OpenStack instance" should be checked
