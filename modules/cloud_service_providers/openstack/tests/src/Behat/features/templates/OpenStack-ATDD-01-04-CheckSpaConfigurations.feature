@spa
Feature: Check the SPA configurations as a "Administrator".

  @api @javascript
  Scenario: Check the "SPA" checkbox to use React SPA
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/config/services/cloud/settings"
    And I check the box "Use React single page application (SPA)"
    And I enter "{{ redirect_uri }}" for "Redirect URI"
    And I press "Save configuration"
    Then the url should match "/admin/config/services/cloud/settings"
    And I should see the success message "The configuration options have been saved"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Check the "Debug" checkbox to bypass the animated loading spinner icon in SPA
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/config/services/cloud_dashboard/settings"
    And I press "Debug"
    And I check the box "Bypass the animated loading spinner icon in SPA"
    And I press "Save configuration"
    Then the url should match "/admin/config/services/cloud_dashboard/settings"
    And I should see the success message "The configuration options have been saved"
    And I should see neither error nor warning messages

  @api
  Scenario Outline: Check the permission setting to use React SPA
    Given I am logged in as a user with the "Administrator" role
    When I visit "admin/people/permissions/<drupal_role_name_machine>"
    And I check the box "Grant OAuth2 codes"
    And I press "Save permissions"
    Then I should see the success message "The changes have been saved."
    And I should see neither error nor warning messages

    Examples:
      | drupal_role_name_machine       |
      | {{ drupal_role_name_machine }} |
      | cloud_admin                    |
