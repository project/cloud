@spa
Feature: Login and grant to use React SPA as an "authenticated user"

  @api @javascript
  Scenario Outline: Press "Login" and "Grant" button to use React SPA
    When I visit "/user/login"
    And I enter "<drupal_user_name>" for "Username"
    And I enter "<drupal_user_password>" for "Password"
    And I press "Log in"
    And I wait {{ wait_login }} milliseconds
    And I press "Login"
    And I wait {{ wait_login }} milliseconds
    And I press "Grant"
    And I wait {{ wait_login }} milliseconds
    Then the url should match "/clouds/dashboard/providers"
    And I should see neither error nor warning messages

    Examples:
      | drupal_user_name              | drupal_user_password            |
      | {{ drupal_user_name }}        | {{drupal_user_password}}        |
      | {{ drupal_cloud_admin_name }} | {{drupal_cloud_admin_password}} |
