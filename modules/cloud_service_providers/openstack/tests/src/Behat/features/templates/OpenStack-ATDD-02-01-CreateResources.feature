@minimal @ci_job @launch_instance
Feature: Create resources for OpenStack as an "authenticated user"

  @api
  Scenario: Create a key pair
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/openstack/{{ cloud_context }}/key_pair/add"
    And I should see the heading "Add OpenStack key pair"
    And I enter "{{ key_pair_name }}" for "Key pair name"
    And I press "Save"
    # The page transitions to the detailed view
    Then the url should match "/clouds/openstack/{{ cloud_context }}/key_pair/"
    And I should see the success message "has been created"
    And I should see no error message
    And I should see the link "{{ key_pair_name }}" in the success_message

  @api
  Scenario: Create a security group
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/openstack/{{ cloud_context }}/security_group/add"
    And I should see the heading "Add OpenStack security group"
    And I enter "{{ security_group_name }}" for "Security group name"
    And I enter "{{ security_group_description }}" for "Description"
    And I press "Save"
    # The page transitions to the detailed view
    Then the url should match "/clouds/openstack/{{ cloud_context }}/security_group/"
    And I should see the success message "has been created"
    And I should see the success message "Set up the IP permissions"
    And I should see the link "{{ security_group_name }}" in the success_message
    And I should see neither error nor warning messages

  @api
  Scenario: Create a network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/openstack/{{ cloud_context }}/network/add"
    And I should see the heading "Add OpenStack network"
    And I enter "{{ network_name }}" for "Name"
    # Temporarily comment out because of the error when selecting Availability Zone
    # And I select "{{ availability_zone }}" from "Availability Zone"
    And I press "Save"
    Then the url should match "/clouds/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_name }}" in the table

  @api
  Scenario: Create a subnet
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/openstack/{{ cloud_context }}/subnet/add"
    And I should see the heading "Add OpenStack subnet"
    And I enter "{{ subnet_name }}" for "Name"
    And I select "{{ network_name }}" from "Network"
    And I enter "{{ network_address }}" for "CIDR"
    And I select "{{ ip_version }}" from "IP version"
    And I enter "{{ gateway_ip }}" for "Gateway IP"
    And I press "Save"
    Then the url should match "/clouds/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ subnet_name }}" in the table

  @api
  Scenario: Create a server group
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/openstack/{{ cloud_context }}/server_group/add"
    And I should see the heading "Add OpenStack server group"
    And I enter "{{ server_group_name }}" for "Name"
    And I select "{{ server_group_policy }}" from "Policy"
    And I press "Save"
    Then the url should match "/clouds/openstack/{{ cloud_context }}/server_group"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ server_group_name }}" in the table

  @api
  Scenario: Refresh resources
    Given I am logged in as a user with the "Administrator" role
    When I visit "/admin/config/system/cron"
    Then I should see the heading "Cron"
    And I click "Run cron"
    And I run drush cr
