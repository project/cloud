@minimal @ci_job @launch_instance
Feature: Create an instance for OpenStack as an "authenticated user"

  @api
  Scenario: Read an image
    # The prerequisite is having an image at OpenStack Horizon dashboard.
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/openstack/{{ cloud_context }}/image"
    And I run drush cron
    And I should see the link "{{ image_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ image_name }}"
    Then the url should match "/clouds/openstack/{{ cloud_context }}/image/"
    And I should see "{{ image_name }}"
    And I should see "{{ status }}"
    And I should see neither error nor warning messages

  @api
  Scenario: Create a launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}/openstack/add"
    And I take screenshot
    And I should see the heading "Add OpenStack"
    And I enter "{{ instance_name }}" for "Name"
    And I select "{{ instance_flavor }}" from "Flavor"
    And I select "{{ image_name }}" from "Image ID"
    And I select "{{ availability_zone }}" from "Availability Zone"
    And I check the box "{{ security_group_name }}"
    And I select "{{ key_pair_name }}" from "SSH key"
    And I select "{{ network_name }}" from "Network"
    And I select "{{ server_group_name }}" from "Server group name"
    And I fill in "Customization script" with:
    """
    #!/bin/sh

    echo "$(date) : {{customization_script}}." > message.txt
    cat message.txt
    """
    And I press "Save"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}/"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the heading "{{ instance_name }}"
