@minimal @ci_job @launch_instance
Feature: Launch an instance for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Launch an instance
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "{{ instance_name }}"
    And I should see the heading "{{ instance_name }}"
    And I click "Launch"
    Then the url should match "/launch"
    And I take screenshot
    And I press "Launch"
    Then the url should match "/clouds/openstack/{{ cloud_context }}/instance"
    And I take screenshot
    And I should see the success message "has been launched"
    And I should see neither error nor warning messages
    And I wait {{ wait }} milliseconds
    And I run drush cron
    And I should see "{{ instance_flavor }}" in the "{{ instance_name }}" row
    And I should see "{{ availability_zone }}" in the "{{ instance_name }}" row
