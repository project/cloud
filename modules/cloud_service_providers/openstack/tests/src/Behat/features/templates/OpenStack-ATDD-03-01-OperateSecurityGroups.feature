@minimal @ci_job
Feature: Create, read, update and delete a security groups for OpenStack as "Authenticated User"

  @api @javascript
  Scenario: Create a security group
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group/add"
    And I take screenshot
    And I should see the heading "{{ prefix_add_form }} security group"
    And I enter "{{ security_group_name_operate }}" for "Security group name"
    And I enter "{{ security_group_description }}" for "Description"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    # The page transitions to the detailed view
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I should see the success message "has been created"
    And I should see the success message "Set up the IP permissions"
    And I should see the link "{{ security_group_name_operate }}" in the success_message
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Read the security group
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ security_group_name_operate }}"
    And I take screenshot
    And I click "{{ security_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/security_group/"
    And I should see "{{ security_group_name_operate }}" in the "page_header"
    And I should see "{{ security_group_name_operate }}" in the "Security group name"
    And I should see "{{ security_group_description }}" in the "Description"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Add rules to the security group
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ security_group_name_operate }}"
    And I click "{{ security_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/security_group/"
    And I click "Edit" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/edit"
    And I take screenshot
    # Add a rule to Inbound rules
    And I fill in "From port" with "{{ security_group_port }}" in the row "0" in the "Inbound rules"
    And I fill in "To port" with "{{ security_group_port }}" in the row "0" in the "Inbound rules"
    And I select "{{ security_group_source }}" from "Source" in the row "0" in the "Inbound rules"
    And I fill in "CIDR IP" with "{{ security_group_ip }}" in the row "0" in the "Inbound rules"
    And I fill in "Description" with "{{ security_group_description }}" in the row "0" in the "Inbound rules"
    # Add a rule to Outbound rules
    And I fill in "From port" with "{{ security_group_port }}" in the row "1" in the "Outbound rules"
    And I fill in "To port" with "{{ security_group_port }}" in the row "1" in the "Outbound rules"
    And I select "{{ security_group_source }}" from "Source" in the row "1" in the "Outbound rules"
    And I fill in "CIDR IP" with "{{ security_group_ip }}" in the row "1" in the "Outbound rules"
    And I fill in "Description" with "{{ security_group_description }}" in the row "1" in the "Outbound rules"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ security_group_name_operate }}"
    And I click "{{ security_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ security_group_port }}" in the "{{ security_group_description }}" row in the "Inbound rules"
    And I should see "{{ security_group_port }}" in the "{{ security_group_description }}" row in the "Inbound rules"
    And I should see "{{ security_group_ip }}" in the "{{ security_group_description }}" row in the "Inbound rules"
    And I should see "{{ security_group_port }}" in the "{{ security_group_description }}" row in the "Outbound rules"
    And I should see "{{ security_group_port }}" in the "{{ security_group_description }}" row in the "Outbound rules"
    And I should see "{{ security_group_ip }}" in the "{{ security_group_description }}" row in the "Outbound rules"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Remove the rules from the security group
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ security_group_name_operate }}"
    And I click "{{ security_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/security_group/"
    # Confirm rule in Outbound rules
    And I should see "{{ security_group_ip }}" in the "Outbound rules"
    And I click "Edit" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/edit"
    And I follow "Remove rule" in a row with "{{ security_group_ip }}" set in "CIDR IP" in the "Outbound rules"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ security_group_name_operate }}"
    And I click "{{ security_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    # Without @javascript, the following step fails.
    And I should not see "{{ security_group_ip }}" in the "Outbound rules"

  @api @javascript
  Scenario: Take a screenshot of the Revoke form
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I wait {{ wait_render }} milliseconds
    And I click "{{ security_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And I click "Revoke"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Take a screenshot of the Delete multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ security_group_name_operate }}" row
    And I wait {{ wait_render }} milliseconds
    And I select "Delete security group(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the security group
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ security_group_name_operate }}"
    And I click "{{ security_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/security_group/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ security_group_name_operate }}"
