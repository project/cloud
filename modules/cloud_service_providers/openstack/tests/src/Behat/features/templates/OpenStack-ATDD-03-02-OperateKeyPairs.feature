@minimal @ci_job
Feature: Create, read, and delete a key pair for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Create a key pair
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/key_pair/add"
    And I take screenshot
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} key pair"
    And I enter "{{ key_pair_name_operate }}" for "Key pair name"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    # The page transitions to the detailed view
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/key_pair"
    And I should see the success message "has been created"
    And I should see no error message
    And I should see the link "{{ key_pair_name_operate }}" in the success_message

  @api @javascript
  Scenario: View the key pair
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/key_pair"
    And I take screenshot
    And I run drush cron
    And I should see the heading "{{ prefix_key_pair_list_view }}"
    And I should see "{{ key_pair_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "{{ key_pair_name_operate }}" in the "Key pair name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/key_pair/"
    And I take screenshot
    And I should see "{{ key_pair_name_operate }}" in the "page_header"
    And I should see "{{ key_pair_name_operate }}" in the "Key pair name"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Take a screenshot of the Edit, Import and Delete multiple form
    Given I am logged in as a user with the "Administrator" role
    When I run drush cron
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/key_pair/import"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/key_pair"
    And I wait {{ wait_render }} milliseconds
    And I click the link in the row with "{{ key_pair_name_operate }}" in the "Key pair name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And I click "Edit" in the actions
    And I take screenshot
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/key_pair"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ key_pair_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I select "Delete key pair(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete a key pair
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/key_pair"
    And I run drush cron
    And I should see the link "{{ key_pair_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ key_pair_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/key_pair/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/key_pair"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ key_pair_name_operate }}"
