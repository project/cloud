@minimal @ci_job
Feature: Create, read, update and delete a network for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Create a network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network/add"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the heading "{{ prefix_add_form }} network"
    And I enter "{{ network_name_operate }}" for "Name"
    # Temporarily comment out because of the error when selecting Availability Zone
    # And I select "{{ availability_zone }}" from "Availability Zone"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_name_operate }}" in the table

  @api @javascript
  Scenario: Read the network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I run drush cron
    And I should see the link "{{ network_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I take screenshot
    And I click "{{ network_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network/"
    And I take screenshot
    And I should see "{{ network_name_operate }}" in the "page_header"
    And I should see "{{ network_name_operate }}" in the "Name"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Update the network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I run drush cron
    And I should see the link "{{ network_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ network_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network/"
    And I click "Edit" in the actions
    And the url should match "/edit"
    And I take screenshot
    And I enter "{{ network_name_operate_updated }}" for "Name"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_name_operate_updated }}" in the table
    And I should not see the link "{{ network_name_operate }}" in the table

  @api @javascript
  Scenario: Take a screenshot of the Delete multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    When I run drush cron
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ network_name_operate_updated }}" row
    And I wait {{ wait }} milliseconds
    And I select "Delete network(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I run drush cron
    And I should see the link "{{ network_name_operate_updated }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ network_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ network_name_operate_updated }}"
