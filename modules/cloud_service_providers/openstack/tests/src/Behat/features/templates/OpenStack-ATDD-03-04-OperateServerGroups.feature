@minimal @ci_job
Feature: Create, read, and delete a server group for OpenStack

  @api @javascript
  Scenario: Create a server group as an administrator
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/server_group/add"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the heading "{{ prefix_add_form }} server group"
    And I enter "{{ server_group_name_operate }}" for "Name"
    And I enter "{{ server_group_policy }}" for "Policy"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    # The page transitions to the detailed view
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/server_group"
    And I should see the success message "has been created"
    And I should see the link "{{ server_group_name_operate }}" in the table
    And I should see neither error nor warning messages

  @api @javascript
  Scenario Outline: Read the server group as a user and administrator
    # DO NOT change <role> to "<role>" since <role> contains an :arg1.
    Given I am logged in as <role>
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/server_group"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the link "{{ server_group_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ server_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/server_group/"
    And I take screenshot
    And I should see "{{ server_group_name_operate }}" in the "page_header"
    And I should see "{{ server_group_name_operate }}" in the "Name"
    And I should see "{{ server_group_policy }}" in the "Policy"
    And I should see neither error nor warning messages

    Examples:
      | role                                 |
      | user "{{ drupal_user_name }}"        |
      | a user with the "Administrator" role |

  @api @javascript
  Scenario: Take a screenshot of the Delete multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/server_group"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ server_group_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I select "Delete server group(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the server group as an administrator
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/server_group"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ server_group_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ server_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/server_group/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/server_group"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ server_group_name_operate }}"
