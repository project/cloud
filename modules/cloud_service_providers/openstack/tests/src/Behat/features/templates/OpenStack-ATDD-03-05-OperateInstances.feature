@minimal @ci_job
Feature: Launch, reboot, stop, and terminate an instance for OpenStack as an "authenticated user"

  @api @javascript
  Scenario Outline: Create a launch template by a user and a cloud admin
    Given I am logged in as user "<user>"
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}/openstack/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_launch_template_form }}"
    And I enter "<instance_name>" for "Name"
    And I select "{{ instance_flavor }}" from "Flavor"
    And I select "{{ image_name }}" from "Image ID"
    And I select "{{ availability_zone }}" from "Availability Zone"
    And I check the box "{{ security_group_name }}"
    And I select "{{ key_pair_name }}" from "SSH key"
    And I select "{{ network_name }}" from "Network"
    And I select "{{ server_group_name }}" from "Server group name"
    And I fill in "Customization script" with:
    """
    #!/bin/sh

    echo "$(date) : {{customization_script_operate}}." > message.txt
    cat message.txt
    """
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    # Temporarily comment out because an error occurs when transitioning to the list screen after creation in SPA.
    # And I should see the heading "<instance_name>"

    Examples:
      | user                          | instance_name                 |
      | {{ drupal_user_name }}        | {{ instance_name_operate_1 }} |
      | {{ drupal_cloud_admin_name }} | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario Outline: Launch an instance by a user
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render_long }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "<instance_name>"
    And I click "Launch"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/launch"
    And I press "Launch"
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I should see the success message "has been launched"
    And I should see neither error nor warning messages

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario: Refresh instance status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_resumed }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds

  @api @javascript
  Scenario Outline: View the created instance and its author
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ instance_flavor }}" in the "<instance_name>" row
    And I should see "{{ availability_zone }}" in the "<instance_name>" row
    And I should see the link "<instance_name>"
    And I click "<instance_name>"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I take screenshot
    And I should see "<instance_name>" in the "page_header"
    And I should see "{{ instance_flavor }}" in the "Instance type"
    And I should not see "{{ instance_flavor_as_first_option }}"
    And I should see "{{ image_name }}" in the "Image"
    And I should see "{{ availability_zone }}" in the "Availability Zone"
    And I should see "{{ security_group_name }}" in the "Security groups"
    And I should see "{{ key_pair_name }}" in the "Key pair name"
    # The author should be the launcher, not the author of the launch template.
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I should see the link "{{ drupal_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario: Take a screenshot of the edit form
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I click "{{ instance_name_operate_1 }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I click "Edit" in the actions
    Then the url should match "/edit"
    And I take screenshot

  @api @javascript
  Scenario Outline: View the log of the created instance
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "<instance_name>"
    And I wait {{ wait_render }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    And I click "Log" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/console_output"
    And I take screenshot
    And I should see "Console log of instance" in the "page_header"
    And I should see "{{ line_number_default }}" in the "select_wrapper"
    And I select "{{ line_number_updated }}" from "Number of lines"

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario Outline: View the console of the created instance
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "<instance_name>"
    And I wait {{ wait_render }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    And I click "Console" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/console"
    And I take screenshot
    And I should see "Console of instance" in the "page_header"

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario Outline: View the Action log of the created instance
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "<instance_name>"
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    And I click "Action log" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/action_log"
    And I take screenshot
    And I should see "Action log of instance" in the "page_header"

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario Outline: Confirm the log is updated
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "<instance_name>"
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    And I click "Log" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/console_output"
    And I should see "{{ line_number_updated }}" in the "select_wrapper"
    And I should see "{{ customization_script_operate }}" in the "Console log"

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario Outline: Attach an interface
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I wait {{ wait_resumed }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I wait {{ wait_resumed }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "<instance_name>"
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    And I click "Attach interface" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/attach_network"
    And I take screenshot
    And I should see neither error nor warning messages
    And I select "Network" from "Attach Type"
    And I select "{{ network_name }} ({{ network_address }})" from "Network"
    And I enter "<fixed_ip_address>" for "Fixed IP address"
    And I press "Attach Interface"
    And I wait {{ wait_render_long }} milliseconds
    And I run drush cron
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I should see the success message "Network interface attached."
    And I should see neither error nor warning messages

    Examples:
      | instance_name                 | fixed_ip_address                  |
      | {{ instance_name_operate_1 }} | {{ instance_fixed_ip_address_1 }} |
      | {{ instance_name_operate_2 }} | {{ instance_fixed_ip_address_2 }} |

  @api @javascript
  Scenario Outline: Hard-reboot the running instances
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see "<instance_name>" in the table
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    # To reboot, both states should be "running".
    And I should see "running" in the "Instance State"
    And I should see "running" in the "Power state"
    And I click "Reboot" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/reboot"
    And I take screenshot
    And I should see neither error nor warning messages
    And I should see "<instance_name>"
    And I press "Reboot"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I should see the success message "has been rebooted"
    And I should see neither error nor warning messages

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario Outline: Soft-reboot the running instances
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see "<instance_name>" in the table
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    # To reboot, both states should be "running".
    And I should see "running" in the "Instance State"
    And I should see "running" in the "Power state"
    And I click "Reboot" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/reboot"
    # An error message might appear if either the power or instance state is not "running"
    And I should see neither error nor warning messages
    # Check the box to "soft-reboot"
    And I check the box "Soft reboot"
    And I press "Reboot"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I should see the success message "has been rebooted"
    And I should see neither error nor warning messages

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario: Refresh instance status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    # Soft-rebooting multiple instances take some time.
    And I wait {{ wait_reboot }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds

  @api @javascript
  Scenario Outline: Make sure images can't be created from running instances
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "<instance_name>"
    And I click "<instance_name>"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    And I should see neither error nor warning messages
    And I should see "running" in the "Instance State"
    And I should see "running" in the "Power state"
    And I click "Create an OpenStack image" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/create_image"
    And I should not see the button "Create image"
    And I click "Cancel"
    And I wait for AJAX to finish
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see neither error nor warning messages

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario: Take a screenshot of the Reboot and Stop multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    When I run drush cron
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ instance_name_operate_1 }}" row
    And I wait {{ wait }} milliseconds
    And I select "Reboot Instance(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ instance_name_operate_1 }}" row
    And I wait {{ wait }} milliseconds
    And I select "Stop Instance(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario Outline: Stop the running instances
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see "<instance_name>" in the table
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    # To stop, both states should be "running".
    And I should see "running" in the "Instance State"
    And I should see "running" in the "Power state"
    And I click "Stop" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/stop"
    And I take screenshot
    # An error message might appear if either the power or instance state is not "running"
    And I should see neither error nor warning messages
    And I press "Stop"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I should see the success message "has been stopped"
    And I should see neither error nor warning messages

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario: Refresh instance status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    # Soft-rebooting multiple instances take some time.
    And I wait {{ wait_stopped }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds

  @api @javascript
  Scenario Outline: Make sure running instances are stopped
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see "stopped" in the "<instance_name>" row
    And I should see "shutdown" in the "<instance_name>" row

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario: Take a screenshot of the Start multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    When I run drush cron
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ instance_name_operate_1 }}" row
    And I wait {{ wait }} milliseconds
    And I select "Start Instance(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario Outline: Create an image from the stopped instance
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "<instance_name>"
    And I click "<instance_name>"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    And I click "Create an OpenStack image" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/create_image"
    And I take screenshot
    And I should see the button "Create image"
    And I enter "<image_name>" for "Image name"
    And I press "Create image"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/image"
    And I should see the success message "created"
    And I should see neither error nor warning messages
    And I should see the link "<image_name>"

    Examples:
      | instance_name                 | image_name                 |
      | {{ instance_name_operate_1 }} | {{ image_name_operate }} |

  @api @javascript
  Scenario: Refresh image status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/image"
    And I wait {{ wait_resumed }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds

  @api @javascript
  Scenario: Check the details of the created image
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/image"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ image_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "{{ image_name_operate }}" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/image/"
    And I should see "{{ image_name_operate }}" in the "page_header"
    And I should see neither error nor warning messages
    And I should see "{{ image_name_operate }}" in the "Name"

  @api @javascript
  Scenario Outline: Detach an interface
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I wait {{ wait_resumed }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "<instance_name>"
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    And I click "Detach interface" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/detach_network"
    And I take screenshot
    And I should see neither error nor warning messages
    And I select "<fixed_ip_address>" from "Port"
    And I press "Detach Interface"
    And I wait {{ wait_render_long }} milliseconds
    And I run drush cron
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I should see the success message "Network interface detached."
    And I should see neither error nor warning messages

    Examples:
      | instance_name                 | fixed_ip_address                  |
      | {{ instance_name_operate_1 }} | {{ instance_fixed_ip_address_1 }} |
      | {{ instance_name_operate_2 }} | {{ instance_fixed_ip_address_2 }} |

  @api @javascript
  Scenario: Terminate the stopped instances
    Given I am logged in as user "{{ drupal_user_name }}"
    When I run drush cron
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see "stopped" in the "{{ instance_name_operate_1 }}" row
    And I should see "stopped" in the "{{ instance_name_operate_2 }}" row
    And I wait {{ wait }} milliseconds
    And I check the box in the "{{ instance_name_operate_1 }}" row
    And I wait {{ wait }} milliseconds
    And I check the box in the "{{ instance_name_operate_2 }}" row
    And I select "Terminate Instance(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/delete_multiple"
    And I take screenshot
    And I should see "{{ instance_name_operate_1 }}"
    And I should see "{{ instance_name_operate_2 }}"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    And I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I run drush cron
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I should see with "stopped" in the "{{ instance_name_operate_1 }}" row, no rows, or no table
    And I should see with "stopped" in the "{{ instance_name_operate_2 }}" row, no rows, or no table

  @api @javascript
  Scenario Outline: Launch an instance by a user
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render_long }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "<instance_name>"
    And I click "Launch"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/launch"
    And I press "Launch"
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I should see the success message "has been launched"
    And I should see neither error nor warning messages

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario: Refresh instance status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_resumed }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds
    And I wait {{ wait_render_long }} milliseconds

  @api @javascript
  Scenario Outline: Delete an instance by a user
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I wait {{ wait_render }} milliseconds
    And I should see "<instance_name>" in the table
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/instance/"
    And I should see "<instance_name>" in the "page_header"
    And I click "Delete" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/terminate"
    And I take screenshot
    And I press "Delete | Terminate"
    And I wait {{ wait_render }} milliseconds
    And I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/instance"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I run drush cron
    And I should not see the link "<instance_name>"

    Examples:
      | instance_name                 |
      | {{ instance_name_operate_1 }} |
      | {{ instance_name_operate_2 }} |

  @api @javascript
  Scenario Outline: Delete the created launch template by the authored user
    Given I am logged in as user "<user>"
    When I run drush cron
    And I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "<instance_name>"
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "<instance_name>" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}/"
    And I click "Delete" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    And I wait for AJAX to finish
    And I should be on "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I run drush cron
    And I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I should not see the link "<instance_name>"

    Examples:
      | user                          | instance_name                 |
      | {{ drupal_user_name }}        | {{ instance_name_operate_1 }} |
      | {{ drupal_cloud_admin_name }} | {{ instance_name_operate_2 }} |
