@minimal @ci_job
Feature: Update and delete an image for OpenStack

  @api @javascript
  Scenario: Update the name of the image
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/image"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ image_name_operate }}"
    And I click "{{ image_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/image/"
    And I should see "{{ image_name_operate }}" in the "page_header"
    And I should see neither error nor warning messages
    And I should see "{{ image_name_operate }}" in the "Name"
    And I click "Edit" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/edit"
    And I take screenshot
    And I enter "{{ image_name_operate_updated }}" for "Name"
    And I check the "Private" radio button
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Update the visibility of the image
    Given I am logged in as a user with the "Administrator" role
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/image"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the link "{{ image_name_operate_updated }}"
    And I click "{{ image_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/image/"
    And I take screenshot
    And I should see "{{ image_name_operate_updated }}" in the "page_header"
    And I should see neither error nor warning messages
    And I should see "{{ image_name_operate_updated }}" in the "Name"
    And I click "Edit" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/edit"
    And I enter "{{ image_name_operate_updated }}" for "Name"
    And I check the "Shared" radio button
    And I wait for AJAX to finish
    And I wait {{ wait_render }} milliseconds
    And I enter "{{ project_id }}" for "Project ID"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/image"
    And I wait {{ wait_render }} milliseconds
    And I click the link in the row with "{{ image_name_operate_updated }}" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ image_name_operate_updated }}" in the "Name"
    And I should see "shared" in the "Visibility"
    And I should see "{{ cloud_context }}" in the table
    And I should see "{{ project_id }}" in the table

  @api @javascript
  Scenario: Take a screenshot of the add form
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/image"
    And I wait {{ wait_render }} milliseconds
    And I click "Add OpenStack image"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: delete multiple images
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/image"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ image_name }}" row
    And I select "Delete Image(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the image
    Given I am logged in as a user with the "Administrator" role
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/image"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ image_name_operate }}"
    And I should see the link "{{ image_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And I click "{{ image_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/image/"
    And I should see "{{ image_name_operate_updated }}" in the "page_header"
    And I should see neither error nor warning messages
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/image"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I should not see the link "{{ image_name_operate_updated }}"
