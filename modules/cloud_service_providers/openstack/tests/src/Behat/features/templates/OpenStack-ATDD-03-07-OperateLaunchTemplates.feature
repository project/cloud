@minimal @ci_job
Feature: Create and copy a launch template for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Create a launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}/openstack/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_launch_template_form }}"
    And I enter "{{ launch_template_name }}" for "Name"
    And I enter "{{ description }}" for "Description"
    And I select "{{ instance_flavor }}" from "Flavor"
    And I select "{{ image_name }}" from "Image ID"
    And I select "{{ availability_zone }}" from "Availability Zone"
    And I check the box "{{ security_group_name }}"
    And I select "{{ key_pair_name }}" from "SSH key"
    And I select "{{ network_name }}" from "Network"
    And I fill in "Customization script" with:
    """
    #!/bin/sh

    echo "$(date) : {{customization_script_operate}}." > message.txt
    cat message.txt
    """
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    # Temporarily comment out because an error occurs when transitioning to the list screen after creation in SPA.
    # And I should see the heading "{{ launch_template_name }}"

  @api @javascript
  Scenario: View the created launch template and its author
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the link "{{ launch_template_name }}"
    And I click "{{ launch_template_name }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}/"
    And I take screenshot
    And I should see the heading "{{ launch_template_name }}"
    And I should see "{{ description }}"
    And I should see "{{ instance_flavor }}" in the "Flavor"
    And I should not see "{{ instance_flavor_as_first_option }}"
    And I should see "{{ image_name }}" in the "Image ID"
    And I should see "{{ availability_zone }}" in the "Availability Zone"
    And I should see "{{ security_group_name }}" in the "Security group"
    And I should see "{{ key_pair_name }}" in the "SSH key"
    # Temporarily comment out as the following field doest not show with Behat.
    # And I should see "{{ network_name }}" in the "Network interface"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Edit the launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ launch_template_name }}"
    And I click "{{ launch_template_name }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}/"
    And I click "Edit" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/edit"
    And I take screenshot
    And I should see the heading "Edit {{ launch_template_name }}"
    And I enter "{{ description_updated }}" for "Description"
    And I select "{{ instance_flavor_edit }}" from "Flavor"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: View the edited launch template and its author
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ launch_template_name }}"
    And I click "{{ launch_template_name }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}/"
    And I should see the heading "{{ launch_template_name }}"
    And I should see "{{ description_updated }}"
    And I should see "{{ instance_flavor_edit }}" in the "Flavor"
    And I should see "{{ customization_script_operate }}" in the "Customization script"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Create a network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network/add"
    And I should see the heading "{{ prefix_add_form }} network"
    And I enter "{{ network_name_operate }}" for "Name"
    # Temporarily comment out because of the error when selecting Availability Zone
    # And I select "{{ availability_zone }}" from "Availability Zone"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_name_operate }}" in the table

  @api @javascript
  Scenario: Attach the network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ launch_template_name }}"
    And I click "{{ launch_template_name }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}/"
    And I click "Edit" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/edit"
    And I should see the heading "Edit {{ launch_template_name }}"
    And I select "{{ network_name_operate }}" from "Network"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    # Temporarily comment out because an error occurs when transitioning to the list screen after creation in SPA.
    # And I should see the heading "{{ launch_template_name }}"

  @api @javascript
  Scenario: Detach the network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ launch_template_name }}"
    And I click "{{ launch_template_name }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}/"
    And I click "Edit" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/edit"
    And I should see the heading "Edit {{ launch_template_name }}"
    And I select "{{ network_name }}" from "Network"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    # Temporarily comment out because an error occurs when transitioning to the list screen after creation in SPA.
    # And I should see the heading "{{ launch_template_name }}"

  @api @javascript
  Scenario: Delete the network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I run drush cron
    And I should see the link "{{ network_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ network_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ network_name_operate }}"

  @api @javascript
  Scenario: Copy the created launch template by a Cloud administrator
    Given I am logged in as user {{ drupal_cloud_admin_name }}
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ launch_template_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ launch_template_name }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}/"
    And I click "Copy"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/copy"
    And I take screenshot
    # No need to set the name since we expect the copy_of name is automatically set.
    And I select "{{ instance_flavor }}" from "Flavor"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I fill in "Authored by" with "{{ drupal_cloud_admin_name }}"
    And I press "Copy"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been created"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: View the copied launch template by a Cloud administrator
    Given I am logged in as user {{ drupal_cloud_admin_name }}
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ launch_template_name_copy }}"
    And I click "{{ launch_template_name_copy }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}/"
    And I should see the heading "{{ launch_template_name_copy }}"
    And I should see "{{ instance_flavor }}" in the "Flavor"
    And I should see "{{ image_name }}" in the "Image ID"
    And I should see "{{ availability_zone }}" in the "Availability Zone"
    And I should see "{{ security_group_name }}" in the "Security group"
    And I should see "{{ key_pair_name }}" in the "SSH key"
    And I should see "{{ network_name }}" in the "Network interface"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_cloud_admin_name }}" in the "Authored by"

  @api @javascript
  Scenario: Delete the copied launch template by an administrator
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ launch_template_name_copy }}"
    And I wait {{ wait }} milliseconds
    And I check the box in the "{{ launch_template_name_copy }}" row
    And I select "Delete launch template(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}/delete_multiple"
    And I take screenshot
    And I should see "{{ launch_template_name_copy }}"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    And I should be on "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ launch_template_name_copy }}"

  @api @javascript
  Scenario: Delete the launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I wait {{ wait_render }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ launch_template_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ launch_template_name }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/design/server_template/{{ cloud_context }}/"
    And I click "Delete" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    And I should be on "/{{ root_path }}/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ launch_template_name }}"
