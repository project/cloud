@minimal @ci_job
Feature: Create, associate, disassociate, and delete a floating IP for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Create a security group
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} security group"
    And I enter "{{ security_group_name_operate }}" for "Security group name"
    And I enter "{{ security_group_description }}" for "Description"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    # The page transitions to the detailed view
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I should see the success message "has been created"
    And I should see the success message "Set up the IP permissions"
    And I should see the link "{{ security_group_name_operate }}" in the success_message
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Create an internal network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} network"
    And I enter "{{ network_name_operate }}" for "Name"
    # Temporarily comment out because of the error when selecting Availability Zone
    # And I select "{{ availability_zone }}" from "Availability Zone"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_name_operate }}" in the table

  @api @javascript
  Scenario: Create a subnet with the internal network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} subnet"
    And I enter "{{ subnet_name_operate }}" for "Name"
    And I select "{{ network_name_operate }}" from "Network"
    And I enter "{{ network_address }}" for "CIDR"
    And I select "{{ ip_version }}" from "IP version"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ subnet_name_operate }}" in the table

  @api @javascript
  Scenario: Create a port for the internal network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/port/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} port"
    And I enter "{{ port_name_operate }}" for "Name"
    And I select "{{ network_name_operate }}" from "Network"
    And I wait for AJAX to finish
    And I select "{{ network_for_port_fixed_ip_address }}" from "Specify IP address or subnet"
    And I enter "{{ fixed_ip_address }}" for "Fixed IP address"
    And I select "{{ security_group_name_operate }}" from "Security group"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ port_name_operate }}" in the table

  @api @javascript
  Scenario: Create an external network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} network"
    And I enter "{{ network_external_name_operate }}" for "Name"
    And I check the box "External network"
    # Temporarily comment out because of the error when selecting Availability Zone
    # And I select "{{ availability_zone }}" from "Availability Zone"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_external_name_operate }}" in the table

  @api @javascript
  Scenario: Create a subnet with the external network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} subnet"
    And I enter "{{ subnet_external_name_operate }}" for "Name"
    And I select "{{ network_external_name_operate }}" from "Network"
    And I enter "{{ network_address_external }}" for "CIDR"
    And I select "{{ ip_version }}" from "IP version"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ subnet_external_name_operate }}" in the table

  @api @javascript
  Scenario: Create a router with the external network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} router"
    And I enter "{{ router_name_operate }}" for "Name"
    And I select "{{ network_availability_zone }}" from "Availability Zone"
    And I select "{{ network_external_name_operate }}" from "External network"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ router_name_operate }}" in the table
    And I should see "ACTIVE" in the {{ router_name_operate }} row

  @api @javascript
  Scenario: Add an internal network interface to the router
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ router_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ router_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/"
    And I click "Interfaces"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/interface"
    And I click "Add interface"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/add_interface"
    And I select "{{ network_name_operate }}" from "Subnet"
    And I enter "{{ router_internal_interface_ip }}" for "IP address"
    And I press "Add interface"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/interface"
    And I should see the success message "has been added to a port"
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ router_internal_interface_ip }}"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Create a floating IP
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip/add"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the heading "{{ prefix_add_form }} floating IP"
    And I enter "{{ floating_ip_name_operate }}" for "Name"
    And I select "{{ network_external_name_operate }}" from "Floating network ID"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ floating_ip_name_operate }}" in the table

  @api @javascript
  Scenario: Read the floating IP
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ floating_ip_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ floating_ip_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip/"
    And I take screenshot
    And I should see "{{ floating_ip_name_operate }}" in the "page_header"
    And I should see "{{ floating_ip_name_operate }}" in the "Name"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Associate the floating IP with the internal network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ floating_ip_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ floating_ip_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip/"
    And I click "Associate Floating IP" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/associate"
    And I take screenshot
    And I select "{{ port_name_operate }}" from "Port to be associated"
    And I press "Associate"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I wait {{ wait_render }} milliseconds
    And I should see the success message "associated"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Confirm the floating IP is associated with the internal network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ floating_ip_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ floating_ip_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip/"
    And I should see "{{ floating_ip_name_operate }}" in the "page_header"
    And I should see "{{ floating_ip_name_operate }}" in the "Name"
    And I should see "{{ network_name_operate }}"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Disassociate the floating IP with the internal network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ floating_ip_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ floating_ip_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip/"
    And I click "Disassociate Floating IP" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/disassociate"
    And I take screenshot
    And I press "Disassociate Address"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I wait {{ wait_render }} milliseconds
    And I should see the success message "disassociated"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Confirm the floating IP is disassociated
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ floating_ip_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ floating_ip_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip/"
    And I should see "{{ floating_ip_name_operate }}" in the "page_header"
    And I should see "{{ floating_ip_name_operate }}" in the "Name"
    And I should not see "{{ network_name_operate }}" in the table or no such region
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Update the floating IP
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ floating_ip_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ floating_ip_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip/"
    And I click "Edit" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/edit"
    And I take screenshot
    And I enter "{{ floating_ip_name_operate_updated }}" for "Name"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ floating_ip_name_operate_updated }}" in the table
    And I should not see the link "{{ floating_ip_name_operate }}" in the table

  @api @javascript
  Scenario: Clear external gateway from the router
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ router_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ router_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/"
    And I click "Edit" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/edit"
    And I select "{{ network_external_name_unselected }}" from "External network"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ router_name_operate }}"
    And I wait {{ wait_render }} milliseconds

  @api @javascript
  Scenario: Remove an internal network interface from the router
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ router_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ router_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/"
    And I click "Interfaces"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/interface"
    And I should not see "External Gateway"
    And I click "Remove interface"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/remove_interface/"
    And I press "Remove interface"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/interface"
    And I should see the success message "has been removed an interface"
    And I should not see "{{ router_internal_interface_ip }}"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Delete the router
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ router_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ router_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ router_name_operate }}"

  @api @javascript
  Scenario: Delete multiple floating IPs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ floating_ip_name_operate_updated }}" row
    And I select "Delete Floating IP(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the floating IP
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ floating_ip_name_operate_updated }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ floating_ip_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    And I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/floating_ip"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    # No tables exist.
    And I should not see the link "{{ floating_ip_name_operate_updated }}"

  @api @javascript
  Scenario: Delete the external subnet
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ subnet_external_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ subnet_external_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ subnet_external_name_operate }}"

  @api @javascript
  Scenario: Delete the external network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ network_external_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ network_external_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ network_external_name_operate }}"

  @api @javascript
  Scenario: Delete the port
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ port_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ port_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/port/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ port_name_operate }}"

  @api @javascript
  Scenario: Delete the internal subnet
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ subnet_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ subnet_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ subnet_name_operate }}"

  @api @javascript
  Scenario: Delete the internal network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ network_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ network_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ network_name_operate }}"

  @api @javascript
  Scenario: Delete the security group
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ security_group_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ security_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/security_group/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ security_group_name_operate }}"
