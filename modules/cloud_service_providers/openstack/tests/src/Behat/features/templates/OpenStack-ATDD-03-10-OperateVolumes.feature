@minimal @ci_job
Feature: Create and delete a volume for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Create a volume
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} volume"
    And I enter "{{ volume_name_operate }}" for "Name"
    And I enter "{{ volume_size }}" for "Size (GiB)"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ volume_name_operate }}" in the table
    And I should see the text "creating" or "available" in the "{{ volume_name_operate }}" row

  @api @javascript
  Scenario: Refresh volume status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see "available" in the "{{ volume_name_operate }}" row

  @api @javascript
  Scenario: Read the volume
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I run drush cron
    And I should see the link "{{ volume_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ volume_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/volume/"
    And I should see "{{ volume_name_operate }}" in the "page_header"
    And I should see "{{ volume_size }}" in the "Size"
    And I should see "available" in the "Status"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Update the volume
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I run drush cron
    And I should see the link "{{ volume_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ volume_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/volume/"
    And I click "Edit" in the actions
    And the url should match "/edit"
    And I take screenshot

  @api @javascript
  Scenario: Attach the volume to an instance
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I run drush cron
    And I should see the link "{{ volume_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "{{ volume_name_operate }}" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/volume/"
    And I click "Attach" in the actions
    And I wait {{ wait }} milliseconds
    And the url should match "/attach"
    And I enter "{{ volume_device_name }}" for "Device name"
    # an instance must be created beforehand
    And I select "{{ instance_name }}" from "Instance ID"
    And I press "Attach"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I should see the success message "is attaching to"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should see "{{ instance_name }}" in the "{{ volume_name_operate }}" row
    And I should see "in-use" in the "{{ volume_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "{{ volume_name_operate }}" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ instance_name }}"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Detach the volume from an instance
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I run drush cron
    And I should see the link "{{ volume_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "{{ volume_name_operate }}" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/volume/"
    And I click "Detach" in the actions
    And the url should match "/detach"
    And I press "Detach"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I should see the success message "is detaching from"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I run drush cron
    And I should see "available" in the "{{ volume_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "{{ volume_name_operate }}" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    And I should not see "{{ instance_name }}"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Take a screenshot of the Delete multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ volume_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I select "Delete volume(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Take a screenshot of the Detach multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ volume_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I select "Detach volume(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the volume
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I run drush cron
    And I should see the link "{{ volume_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ volume_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/volume/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    And I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I run drush cron
    # No tables exist.
    And I should not see the link "{{ volume_name_operate }}"
