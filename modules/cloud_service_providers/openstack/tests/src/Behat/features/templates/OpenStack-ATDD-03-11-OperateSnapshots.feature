@minimal @ci_job
Feature: Create, read, update and delete a snapshot for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Create a volume
    Given I am logged in as user "{{ drupal_user_name }}"
    # Add a volume
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} volume"
    And I enter "{{ volume_name_operate }}" for "Name"
    And I enter "{{ volume_size }}" for "Size (GiB)"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ volume_name_operate }}" in the table
    And I should see the text "creating" or "available" in the "{{ volume_name_operate }}" row

  @api @javascript
  Scenario: Refresh volume status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see "available" in the "{{ volume_name_operate }}" row

  @api @javascript
  Scenario: Create a snapshot
    Given I am logged in as user "{{ drupal_user_name }}"
    # Confirm the volume is available.
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I wait {{ wait_render }} milliseconds
    And I should see "available" in the "{{ volume_name_operate }}" row
    # Add a snapshot
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot/add"
    And I should see the heading "{{ prefix_add_form }} snapshot"
    And I take screenshot
    And I enter "{{ snapshot_name_operate }}" for "Name"
    And I enter "{{ description }}" for "Description"
    And I select "{{ volume_name_operate }}" from "Volume ID"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see "{{ snapshot_name_operate }}" in the table

  @api @javascript
  Scenario: Refresh snapshot status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds

  @api @javascript
  Scenario: Read the snapshot
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the link "{{ snapshot_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ snapshot_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot/"
    And I take screenshot
    And I should see "{{ snapshot_name_operate }}" in the "page_header"
    And I should see "{{ description }}" in the "Description"
    And I should see "{{ volume_size }}" in the "Size"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Update the snapshot
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot"
    And I run drush cron
    And I should see the link "{{ snapshot_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ snapshot_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot/"
    And I click "Edit" in the actions
    And the url should match "/edit"
    And I take screenshot
    And I enter "{{ snapshot_name_operate_updated }}" for "Name"
    And I enter "{{ description_updated }}" for "Description"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I should see "{{ snapshot_name_operate_updated }}"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ snapshot_name_operate_updated }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ snapshot_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ snapshot_name_operate_updated }}"
    And I should see "{{ description_updated }}"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Take a screenshot of the Delete multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ snapshot_name_operate_updated }}" row
    And I wait {{ wait }} milliseconds
    And I select "Delete snapshot(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the snapshot
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot"
    And I run drush cron
    And I should see the link "{{ snapshot_name_operate_updated }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ snapshot_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot/"
    And I click "Delete" in the actions
    And the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/snapshot"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I run drush cron
    And I should not see the link "{{ snapshot_name_operate_updated }}"

  @api @javascript
  Scenario: Delete the volume
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I run drush cron
    And I should see the link "{{ volume_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ volume_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/volume/"
    And I click "Delete" in the actions
    And the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/volume"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I run drush cron
    And I should not see the link "{{ volume_name_operate }}"
