@minimal @ci_job
Feature: Create, read, update and delete a subnet for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Create a network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network/add"
    And I should see the heading "{{ prefix_add_form }} network"
    And I enter "{{ network_name_operate }}" for "Name"
    # Temporarily comment out because of the error when selecting Availability Zone
    # And I select "{{ availability_zone }}" from "Availability Zone"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_name_operate }}" in the table

  @api @javascript
  Scenario: Create a subnet
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/add"
    And I take screenshot
    And I should see the heading "{{ prefix_add_form }} subnet"
    And I enter "{{ subnet_name_operate }}" for "Name"
    And I select "{{ network_name_operate }}" from "Network"
    And I enter "{{ network_address }}" for "CIDR"
    And I select "{{ ip_version }}" from "IP version"
    And I enter "{{ gateway_ip }}" for "Gateway IP"
    And I enter "{{ allocation_pools_start }},{{ allocation_pools_end }}" for "Allocation pools"
    And I enter "{{ dns_name_servers }}" for "DNS name servers"
    And I enter "{{ host_routes_dest }}, {{ host_routes_next_hop }}" for "Host Routes"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ subnet_name_operate }}" in the table

  @api @javascript
  Scenario: Create a subnet v6
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/add"
    And I should see the heading "{{ prefix_add_form }} subnet"
    And I enter "{{ subnet_v6_name_operate }}" for "Name"
    And I select "{{ network_name_operate }}" from "Network"
    And I enter "{{ network_address_v6 }}" for "CIDR"
    And I select "IPv6" from "IP version"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ subnet_v6_name_operate }}" in the table
    And I click "{{ subnet_v6_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ network_address_v6 }}"

  @api @javascript
  Scenario: Read the subnet
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I run drush cron
    And I take screenshot
    And I should see the link "{{ subnet_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ subnet_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/"
    And I should see "{{ subnet_name_operate }}" in the "page_header"
    And I should see "{{ subnet_name_operate }}" in the "Name"
    And I should see "{{ network_name_operate }}" in the "Network"
    And I should see "{{ network_address }}" in the "CIDR"
    And I should see "{{ ip_version }}" in the "IP version"
    And I should see "{{ gateway_ip }}" in the "Gateway IP"
    And I should see "{{ allocation_pools_start }}"
    And I should see "{{ allocation_pools_end }}"
    And I should see "{{ dns_name_servers }}"
    And I should see "{{ host_routes_dest }}"
    And I should see "{{ host_routes_next_hop }}"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Update the subnet
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I run drush cron
    And I should see the link "{{ subnet_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ subnet_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/"
    And I click "Edit" in the actions
    And the url should match "/edit"
    And I take screenshot
    And I enter "{{ subnet_name_operate_updated }}" for "Name"
    And I enter "{{ gateway_ip_updated }}" for "Gateway IP"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ subnet_name_operate_updated }}" in the table
    And I should not see the link "{{ subnet_name_operate }}" in the table

  @api @javascript
  Scenario: Take a screenshot of the Delete multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ subnet_name_operate_updated }}" row
    And I wait {{ wait }} milliseconds
    And I select "Delete subnet(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the subnet
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I run drush cron
    And I should see the link "{{ subnet_name_operate_updated }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ subnet_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ subnet_name_operate_updated }}"

  @api @javascript
  Scenario: Delete the subnet v6
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I run drush cron
    And I should see the link "{{ subnet_v6_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ subnet_v6_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ subnet_v6_name_operate }}"

  @api @javascript
  Scenario: Delete the network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I run drush cron
    And I should see the link "{{ network_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ network_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ network_name_operate }}"
