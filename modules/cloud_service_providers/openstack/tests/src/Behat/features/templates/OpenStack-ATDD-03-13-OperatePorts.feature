@minimal @ci_job
Feature: Create, read, update and delete a port for OpenStack as an "authenticated user"

 @api @javascript
  Scenario: Create a security group
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} security group"
    And I enter "{{ security_group_name_operate }}" for "Security group name"
    And I enter "{{ security_group_description }}" for "Description"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    # The page transitions to the detailed view
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I should see the success message "has been created"
    And I should see the success message "Set up the IP permissions"
    And I should see the link "{{ security_group_name_operate }}" in the success_message
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Create a network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} network"
    And I enter "{{ network_name_operate }}" for "Name"
    # Temporarily comment out because of the error when selecting Availability Zone
    # And I select "{{ availability_zone }}" from "Availability Zone"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_name_operate }}" in the table

  @api @javascript
  Scenario: Create a port
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/port/add"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the heading "{{ prefix_add_form }} port"
    And I enter "{{ port_name_operate }}" for "Name"
    And I select "{{ network_name_operate }}" from "Network"
    And I wait for AJAX to finish
    And I select "{{ network_for_port_unspecified }}" from "Specify IP address or subnet"
    And I select "{{ security_group_name_operate }}" from "Security group"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ port_name_operate }}" in the table

  @api @javascript
  Scenario: Read the port
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ port_name_operate }}"
    And I click "{{ port_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/port/"
    And I take screenshot
    And I should see "{{ port_name_operate }}" in the "page_header"
    And I should see "{{ port_name_operate }}" in the "Name"
    And I should see "{{ network_name_operate }}" in the "Network"
    And I should see "{{ security_group_name_operate }}" in the "Security groups"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Update the port
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ port_name_operate }}"
    And I click "{{ port_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/port/"
    And I click "Edit" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/edit"
    And I take screenshot
    And I enter "{{ port_name_operate_updated }}" for "Name"
    And I enter "{{ allowed_address_pairs_cidr }}" for "IP address or CIDR"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ port_name_operate_updated }}" in the table
    And I should not see the link "{{ port_name_operate }}" in the table
    And I click "{{ port_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ allowed_address_pairs_cidr }}"

  @api @javascript
  Scenario: Check the multiple-deletion screen
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ port_name_operate_updated }}"
    And I check the box in the "{{ port_name_operate_updated }}" row
    And I select "Delete port(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I should see the text "{{ port_name_operate_updated }}"
    And I take screenshot

  @api @javascript
  Scenario: Delete the port
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ port_name_operate_updated }}"
    And I click "{{ port_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/port/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/port"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ port_name_operate_updated }}"

  @api @javascript
  Scenario: Delete the network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ network_name_operate }}"
    And I click "{{ network_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ network_name_operate }}"

  @api @javascript
  Scenario: Delete the security group
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ security_group_name_operate }}"
    And I click "{{ security_group_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/security_group/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/security_group"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should not see the link "{{ security_group_name_operate }}"
