@minimal @ci_job
Feature: Create, read, update and delete a router for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Create a router
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router/add"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the heading "{{ prefix_add_form }} router"
    And I enter "{{ router_name_operate }}" for "Name"
    And I select "{{ network_availability_zone }}" from "Availability Zone"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ router_name_operate }}" in the table
    And I should see "ACTIVE" in the {{ router_name_operate }} row

  @api @javascript
  Scenario: Read the router
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ router_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I click "{{ router_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/"
    And I take screenshot
    And I should see "{{ router_name_operate }}" in the "page_header"
    And I should see "{{ router_name_operate }}" in the "Name"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Create a network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} network"
    And I enter "{{ network_name_operate }}" for "Name"
    # Temporarily comment out because of the error when selecting Availability Zone
    # And I select "{{ availability_zone }}" from "Availability Zone"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_name_operate }}" in the table

  @api @javascript
  Scenario: Create a subnet
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} subnet"
    And I enter "{{ subnet_name_operate }}" for "Name"
    And I select "{{ network_name_operate }}" from "Network"
    And I enter "{{ network_address }}" for "CIDR"
    And I select "{{ ip_version }}" from "IP version"
    And I enter "{{ gateway_ip }}" for "Gateway IP"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ subnet_name_operate }}" in the table

  @api @javascript
  Scenario: Create a network external
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} network"
    And I enter "{{ network_external_name_operate }}" for "Name"
    # Temporarily comment out because of the error when selecting Availability Zone
    # And I select "{{ availability_zone }}" from "Availability Zone"
    And I check "External network"
    And I press "Save"
    And I wait {{ wait_render_long }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ network_external_name_operate }}" in the table

  @api @javascript
  Scenario: Create a subnet external
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/add"
    And I wait {{ wait_render }} milliseconds
    And I should see the heading "{{ prefix_add_form }} subnet"
    And I enter "{{ subnet_external_name_operate }}" for "Name"
    And I select "{{ network_external_name_operate }}" from "Network"
    And I enter "{{ network_address_external }}" for "CIDR"
    And I select "{{ ip_version }}" from "IP version"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ subnet_external_name_operate }}" in the table

  @api @javascript
  Scenario: Add an internal network interface to a router
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ router_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And I click "{{ router_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/"
    And I click "Interfaces"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/interface"
    And I click "Add interface"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/add_interface"
    And I take screenshot
    And I select "{{ network_name_operate }}" from "Subnet"
    And I enter "{{ router_internal_interface_ip }}" for "IP address"
    And I press "Add interface"
    And I wait {{ wait_render }} milliseconds
    And I wait {{ wait_render }} milliseconds
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/interface"
    And I should see the success message "has been added to a port"
    And I take screenshot
    And I should see "{{ router_internal_interface_ip }}"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Update the router's name and set static route and external gateway
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ router_name_operate }}"
    And I click "{{ router_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/"
    And I click "Edit" in the actions
    And I wait {{ wait_render }} milliseconds
    And the url should match "/edit"
    And I take screenshot
    And I enter "{{ router_name_operate_updated }}" for "Name"
    And I select "{{ network_external_name_operate }}" from "External network"
    And I enter "{{ destination_cidr }}" for "Destination CIDR"
    And I enter "{{ next_hop }}" for "Next Hop"
    And I press "Save"
    And I wait {{ wait_render_long }} milliseconds
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ router_name_operate_updated }}" in the table
    And I should not see the link "{{ router_name_operate }}" in the table
    And I click "{{ router_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ destination_cidr }}"
    And I should see "{{ next_hop }}"

  @api @javascript
  Scenario: Clear static route and external gateway
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ router_name_operate_updated }}"
    And I click "{{ router_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/"
    And I click "Edit" in the actions
    And I wait {{ wait_render_long }} milliseconds
    And the url should match "/edit"
    And I select "{{ network_external_name_unselected }}" from "External network"
    And I enter "" for "Destination CIDR"
    And I enter "" for "Next Hop"
    And I press "Save"
    And I wait {{ wait_render_long }} milliseconds
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "{{ router_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And I should not see "{{ destination_cidr }}"
    And I should not see "{{ next_hop }}"

  @api @javascript
  Scenario: Remove an internal network interface from a router
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ router_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And I click "{{ router_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/"
    And I click "Interfaces"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/interface"
    And I click "Remove interface"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/remove_interface/"
    And I take screenshot
    And I press "Remove interface"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/.*/interface"
    And I should see the success message "has been removed an interface"
    And I should not see "{{ router_internal_interface_ip }}"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Delete the subnet
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ subnet_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And I click "{{ subnet_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/subnet/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/subnet"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ subnet_name_operate }}"

  @api @javascript
  Scenario: Delete the network
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ network_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And I click "{{ network_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ network_name_operate }}"

  @api @javascript
  Scenario: Delete the network external
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ network_external_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And I click "{{ network_external_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/network/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/network"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ network_external_name_operate }}"

  @api @javascript
  Scenario: Take a screenshot of the Delete multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ router_name_operate_updated }}" row
    And I wait {{ wait }} milliseconds
    And I select "Delete router(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the router
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I wait {{ wait_render }} milliseconds
    And I run drush cron
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ router_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And I click "{{ router_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/router/"
    And I click "Delete" in the actions
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/router"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ router_name_operate_updated }}"
