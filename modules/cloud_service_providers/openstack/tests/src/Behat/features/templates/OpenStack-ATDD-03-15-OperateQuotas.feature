@minimal @ci_job
Feature: Read the quota for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Refresh quotas as an "administrator"
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/quota"
    And I wait {{ wait }} milliseconds
    # The refresh button is displayed for only administrators
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the success message "Updated Quotas"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Read the quota as an "authenticated user"
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/quota"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ quota_name }}" in the table
    And I take screenshot
    And I click "{{ quota_name }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/quota/"
    And I take screenshot
    And I should see "{{ quota_name }}" in the "page_header"
    And I should see "{{ instance_number }}" in the "Instances usage"

  @api @javascript
  Scenario: Update the quota as an "administrator"
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/quota/"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ quota_name }}"
    And I click "{{ quota_name }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/quota/"
    And I click "Edit" in the actions
    And the url should match "/edit"
    And I enter "{{ instance_number_updated }}" for "Instances"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ instance_number_updated }}" in the {{ quota_name }} row

  @api @javascript
  Scenario: Revert the changes as an "administrator"
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/quota/"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ quota_name }}"
    And I click "{{ quota_name }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/quota/"
    And I click "Edit" in the actions
    And the url should match "/edit"
    And I take screenshot
    And I enter "{{ instance_number }}" for "Instances"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see "{{ instance_number }}" in the {{ quota_name }} row
