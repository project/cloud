@minimal @ci_job
Feature: Create, read, update, and delete a stack for OpenStack as an "authenticated user"

  @api @javascript
  Scenario Outline: Preview a stack
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack/preview"
    And I take screenshot
    And I should see the heading "{{ prefix_preview_stack_template_form }} stack - template"
    And I select "{{ stack_template_source }}" from "Template source"
    And I fill in "Template Data" with:
    """
    description: sample heat for neutron
    heat_template_version: <heat_template_version>
    outputs:
      status:
        description: status of sample_net
        value:
          get_attr:
          - sample_net
          - status
    parameters:
      {{ stack_net_name }}:
        default: {{ stack_net_default }}
        description: Name of Sample network to be created
        type: string
      sample_net_name1:
        default: 1
        description: Name of Sample network to be created
        type: number
      sample_net_name2:
        default: ['one', "two"]
        description: Name of Sample network to be created
        type: comma_delimited_list
      sample_net_name3:
        default: {"key": "value"}
        description: Name of Sample network to be created
        type: json
      sample_net_name4:
        default: "on"
        description: Name of Sample network to be created
        type: boolean
    resources:
      {{ stack_resource_name }}:
        properties:
          name:
            get_param: sample_net_name
        type: {{ stack_resource_type }}
    """
    And I press "Next"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack/preview_extra"
    And I take screenshot
    And I enter "<stack_name_operate>" for "Name"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I should see the success message "has been previewed"
    And I should see the success message 'stack_name: "<stack_name_operate>"'
    And I should see neither error nor warning messages

    Examples:
      | stack_name_operate                | heat_template_version                           |
      | {{ stack_name_operate }}          | {{ heat_template_version_with_quotation|raw }}  |
      | {{ stack_name_operate_another }}  | {{ heat_template_version_without_quotation }}   |

  @api @javascript
  Scenario Outline: Create a stack
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack/add"
    And I take screenshot
    And I should see the heading "{{ prefix_add_form }} stack - template"
    And I select "{{ stack_template_source }}" from "Template source"
    And I fill in "Template Data" with:
    """
    description: sample heat for neutron
    heat_template_version: <heat_template_version>
    outputs:
      status:
        description: status of sample_net
        value:
          get_attr:
          - sample_net
          - status
    parameters:
      {{ stack_net_name }}:
        default: {{ stack_net_default }}
        description: Name of Sample network to be created
        type: string
      sample_net_name1:
        default: 1
        description: Name of Sample network to be created
        type: number
      sample_net_name2:
        default: ['one', "two"]
        description: Name of Sample network to be created
        type: comma_delimited_list
      sample_net_name3:
        default: {"key": "value"}
        description: Name of Sample network to be created
        type: json
      sample_net_name4:
        default: "on"
        description: Name of Sample network to be created
        type: boolean
    resources:
      {{ stack_resource_name }}:
        properties:
          name:
            get_param: sample_net_name
        type: {{ stack_resource_type }}
    """
    And I press "Next"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack/add_extra"
    And I take screenshot
    And I enter "<stack_name_operate>" for "Name"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "<stack_name_operate>" in the table

    Examples:
      | stack_name_operate                | heat_template_version                           |
      | {{ stack_name_operate }}          | {{ heat_template_version_with_quotation|raw }}  |
      | {{ stack_name_operate_another }}  | {{ heat_template_version_without_quotation }}   |

  @api @javascript
  Scenario: Read the stack
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I run drush cron
    And I take screenshot
    And I should see the link "{{ stack_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ stack_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack/"
    And I take screenshot
    And I should see "{{ stack_name_operate }}" in the "page_header"
    And I should see "{{ stack_name_operate }}" in the "Name"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Read the resource types
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I run drush cron
    And I should see the link "{{ stack_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ stack_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack/"
    And I should see "{{ stack_name_operate }}" in the "page_header"
    And I click "Resources" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/resource"
    And I take screenshot
    And I should see "{{ prefix_stack_resource_list_view }}" in the "page_header"
    And I should see "{{ stack_resource_name }}" in the table
    And I should see "Resource ID" in the table
    And I should see "{{ stack_resource_type }}" in the "{{ stack_resource_name }}" row
    # the status should be either "CREATE_IN_PROGRESS" or "CREATE_COMPLETE"
    And I should see "CREATE" in the "{{ stack_resource_name }}" row
    # the status reason varies based on the status
    And I should see "Status Reason" in the table
    And I click "{{ stack_resource_name }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/resource/"
    And I take screenshot
    And I should see "{{ stack_resource_name }}" in the "page_header"
    And I should see "{{ stack_resource_name }}" in the "Name"
    And I should see "{{ stack_resource_type }}" in the "Resource Type"
    # Temporarily comment out because an error will occur when checking the 'Authored by' field in SPA.
    # And I press "Others"
    # And I should see "{{ drupal_user_name }}" in the "Authored by"

  @api @javascript
  Scenario: Read the events
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I run drush cron
    And I should see the link "{{ stack_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ stack_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack/"
    And I should see "{{ stack_name_operate }}" in the "page_header"
    And I click "Events" in the "nav_tabs"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/event"
    And I take screenshot
    And I should see "{{ prefix_stack_event_list_view }}" in the "page_header"
    And I should see "{{ stack_name_operate }}" in the table
    # The status should be either "CREATE_COMPLETE" or "CREATE_IN_PROGRESS"
    And I should see "CREATE" in the "{{ stack_resource_name }}" row
    And I should see "{{ stack_resource_name }}" in the table
    And I should see "CREATE" in the "{{ stack_resource_name }}" row
    # Take a screenshot of the event form
    And I visit the current URL with "/1" append
    And I take screenshot

  @api @javascript
  Scenario: Take a screenshot of the Check, Delete, Resume and Suspend multiple form
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ stack_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I select "Check Stack(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ stack_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I select "Delete stack(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ stack_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I select "Resume Stack(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ stack_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I select "Suspend Stack(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Refresh stack status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"

  @api @javascript
  Scenario Outline: Update the stack
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "<stack_name_operate>"
    And I should see "CREATE_COMPLETE" in the "<stack_name_operate>" row
    And I wait {{ wait }} milliseconds
    And I click "<stack_name_operate>"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack/"
    And I click "Edit" in the actions
    And the url should match "/edit"
    And I take screenshot
    And I select "{{ stack_template_source }}" from "Template source"
    And I fill in "Template Data" with:
    """
    description: sample heat for neutron
    heat_template_version: <heat_template_version>
    outputs:
      status:
        description: status of sample_net
        value:
          get_attr:
          - sample_net
          - status
    parameters:
      {{ stack_net_name }}:
        default: {{ stack_net_default }}
        description: Name of Sample network to be created
        type: string
      sample_net_name1:
        default: 1
        description: Name of Sample network to be created
        type: number
      sample_net_name2:
        default: ['one', "two"]
        description: Name of Sample network to be created
        type: comma_delimited_list
      sample_net_name3:
        default: {"key": "value"}
        description: Name of Sample network to be created
        type: json
      sample_net_name4:
        default: "on"
        description: Name of Sample network to be created
        type: boolean
    resources:
      {{ stack_resource_name }}:
        properties:
          name:
            get_param: sample_net_name
        type: {{ stack_resource_type }}
    """
    And I press "Next"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/edit_extra"
    And I take screenshot
    And I should see "<stack_name_operate>"
    And I enter "{{ stack_net_default_updated }}" for "{{ stack_net_name }}"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages

    Examples:
      | stack_name_operate                | heat_template_version                           |
      | {{ stack_name_operate }}          | {{ heat_template_version_with_quotation|raw }}  |
      | {{ stack_name_operate_another }}  | {{ heat_template_version_with_quotation|raw }}  |

  @api @javascript
  Scenario: Refresh stack status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"

  @api @javascript
  Scenario: Confirm that the update is complete
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I wait {{ wait_render }} milliseconds
    And I should see "UPDATE_COMPLETE" in the "{{ stack_name_operate }}" row

  @api @javascript
  Scenario: Check the stack
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I run drush cron
    And I should see the link "{{ stack_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ stack_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack/"
    And I should see "{{ stack_name_operate }}" in the "page_header"
    And I click "Check" in the actions
    And the url should match "/check"
    And I take screenshot
    And I press "Check"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been checked"
    And I should see neither error nor warning messages
    And I should see "CHECK_COMPLETE" in the "{{ stack_name_operate }}" row

  @api @javascript
  Scenario: Suspend the stack
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I run drush cron
    And I should see the link "{{ stack_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ stack_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack/"
    And I should see "{{ stack_name_operate }}" in the "page_header"
    And I click "Suspend" in the actions
    And the url should match "/suspend"
    And I take screenshot
    And I press "Suspend"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been suspended"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Refresh stack status
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"

  @api @javascript
  Scenario: Confirm that the suspend is complete
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I wait {{ wait_render }} milliseconds
    And I should see "SUSPEND_COMPLETE" in the "{{ stack_name_operate }}" row

  @api @javascript
  Scenario: Resume the stack
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I run drush cron
    And I should see the link "{{ stack_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ stack_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack/"
    And I should see "{{ stack_name_operate }}" in the "page_header"
    And I click "Resume" in the actions
    And the url should match "/resume"
    And I take screenshot
    And I press "Resume"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been resumed"
    And I should see neither error nor warning messages
    And I wait {{ wait_resumed }} milliseconds
    And I run drush cron
    And I should see the text "RESUME_IN_PROGRESS" or "RESUME_COMPLETE" in the "{{ stack_name_operate }}" row

  @api @javascript
  Scenario Outline: Delete the stack
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I run drush cron
    And I should see the link "<stack_name_operate>"
    And I wait {{ wait }} milliseconds
    And I click "<stack_name_operate>"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/stack/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    And I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/stack"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    # No tables exist.
    And I should not see the link "<stack_name_operate>"

    Examples:
      | stack_name_operate                |
      | {{ stack_name_operate }}          |
      | {{ stack_name_operate_another }}  |
