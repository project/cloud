@ci_job
Feature: View and refresh a template version as an "administrator"

  @api @javascript
  Scenario: Refresh template versions as an "administrator"
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/template_version"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see "{{ template_version_name_operate }}" in the table
    # The refresh button is displayed for only administrators
    And I click "Refresh"
    And I wait "{{ wait_render }} milliseconds
    And I should see the success message "Updated Template versions"
    And I should see neither error nor warning messages
    And I should see "{{ template_version_name_operate }}" in the table

  @api @javascript
  Scenario Outline: Read a template version as a user and administrator
    # DO NOT change <role> to "<role>" since <role> contains an :arg1.
    Given I am logged in as <role>
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/template_version"
    And I wait {{ wait_render }} milliseconds
    And I should see "{{ template_version_name_operate }}" in the table
    And I wait {{ wait }} milliseconds
    And I click the link in the row with "{{ template_version_name_operate }}" in the "Name" column in the "{{ resource_list }}" table
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/template_version/"
    And I take screenshot
    And I should see "{{ template_version_name_operate }}" in the "page_header"
    And I should see "{{ template_version_name_operate }}" in the "Name"
    And I should see neither error nor warning messages
    # Read the table of functions
    And I should see "Function" in the "Functions"
    And I should see "Description" in the "Functions"

    Examples:
      | role                                 |
      | user "{{ drupal_user_name }}"        |
      | a user with the "Administrator" role |
