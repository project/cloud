@minimal @ci_job
Feature: Create, read, update, and delete a role for OpenStack as an "administrator"

  @api @javascript
  Scenario: Create a role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/role/add"
    And I take screenshot
    And I should see the heading "{{ prefix_add_form }} role"
    And I enter "{{ role_name_operate }}" for "Name"
    And I enter "{{ description }}" for "Description"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/role"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ role_name_operate }}" in the table

  @api @javascript
  Scenario Outline: Read the role as a user and administrator
    # DO NOT change <role> to "<role>" since <role> contains an :arg1.
    Given I am logged in as <role>
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/role"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the link "{{ role_name_operate }}"
    And I should see "{{ description }}" in the "{{ role_name_operate }}" row
    And I wait {{ wait }} milliseconds
    And I click "{{ role_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/role/"
    And I take screenshot
    And I should see "{{ role_name_operate }}" in the "page_header"
    And I wait {{ wait_render_long }} milliseconds
    And I should see "{{ role_name_operate }}" in the "Name"
    And I should see "{{ description }}" in the "Description"
    And I should see neither error nor warning messages

    Examples:
      | role                                 |
      | user "{{ drupal_user_name }}"        |
      | a user with the "Administrator" role |

  @api @javascript
  Scenario: Update the role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/role"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ role_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ role_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/role/"
    And I click "Edit" in the actions
    And the url should match "/edit"
    And I take screenshot
    And I enter "{{ role_name_operate_updated }}" for "Name"
    And I enter "{{ description_updated }}" for "Description"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/role"
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ role_name_operate_updated }}" in the table
    And I should see "{{ description_updated }}" in the "{{ role_name_operate_updated }}" row
    And I should not see the link "{{ role_name_operate }}" in the table

  @api @javascript
  Scenario: Take a screenshot of the Delete multiple form
    Given I am logged in as a user with the "Administrator" role
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/role"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ role_name_operate_updated }}" row
    And I wait {{ wait }} milliseconds
    And I select "Delete role(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/role"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ role_name_operate_updated }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ role_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/role/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/role"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ role_name_operate_updated }}"
