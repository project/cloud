@minimal @ci_job
Feature: Create, read, update, and delete a user for OpenStack as an "administrator"

  @api @javascript
  Scenario: Create a project
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/project/add"
    And I should see the heading "{{ prefix_add_form }} project"
    And I enter "{{ project_name_operate }}" for "Name"
    And I enter "{{ description }}" for "Description"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/project"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ project_name_operate }}" in the table

  @api @javascript
  Scenario: Create a role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/role/add"
    And I should see the heading "{{ prefix_add_form }} role"
    And I enter "{{ role_name_operate }}" for "Name"
    And I enter "{{ description }}" for "Description"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/role"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ role_name_operate }}" in the table

  @api @javascript
  Scenario: Create a user
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/user/add"
    And I take screenshot
    And I should see the heading "{{ prefix_add_form }} user"
    And I enter "{{ user_name_operate }}" for "Name"
    And I enter "{{ description }}" for "Description"
    And I enter "{{ user_password_operate }}" for "Password"
    And I enter "{{ user_password_operate }}" for "Confirm password"
    And I select "{{ project_name_operate }}" from "Primary Project"
    And I select "{{ role_name_operate }}" from "Role"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/user"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ user_name_operate }}" in the table

  @api @javascript
  Scenario Outline: Read the user as a user and administrator
    # DO NOT change <role> to "<role>" since <role> contains an :arg1.
    Given I am logged in as <role>
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/user"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the link "{{ user_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ user_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/user/"
    And I take screenshot
    And I should see "{{ user_name_operate }}" in the "page_header"
    And I should see "{{ user_name_operate }}" in the "Name"
    And I should see "{{ description }}" in the "Description"
    And I should see "{{ project_name_operate }}" in the "Default Project ID"
    And I should see neither error nor warning messages

    Examples:
      | role                                 |
      | user "{{ drupal_user_name }}"        |
      | a user with the "Administrator" role |

  @api @javascript
  Scenario: Read the assigned user in OpenStack project
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/project"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ project_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ project_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/project/"
    And I should see "{{ project_name_operate }}" in the "page_header"
    And I should see "{{ project_name_operate }}" in the "Name"
    And I should see "{{ user_name_operate }}" in the "User Roles"
    And I should see "{{ role_name_operate }}" in the "User Roles"

  @api @javascript
  Scenario: Update the password of the user
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/user"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ user_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ user_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/user/"
    And I click "Change Password" in the actions
    And the url should match "/change_password"
    And I take screenshot
    And I should see "Change password" in the "page_header"
    And I enter "{{ user_password_operate_updated }}" for "Password"
    And I enter "{{ user_password_operate_updated }}" for "Confirm password"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/user"
    And I should see the success message "has been updated"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Update the user
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/user"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ user_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ user_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/user/"
    And I click "Edit" in the actions
    And the url should match "/edit"
    And I take screenshot
    And I enter "{{ user_name_operate_updated }}" for "Name"
    And I enter "{{ description_updated }}" for "Description"
    And I press "Save"
    And I wait {{ wait_render }} milliseconds
    Then I should see the success message "has been updated"
    And I should see neither error nor warning messages
    And I should see the link "{{ user_name_operate_updated }}" in the table
    And I should not see the link "{{ user_name_operate }}" in the table

  @api @javascript
  Scenario: Take a screenshot of the Delete multiple form
    Given I am logged in as a user with the "Administrator" role
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/user"
    And I wait {{ wait_render }} milliseconds
    And I check the box in the "{{ user_name_operate_updated }}" row
    And I wait {{ wait }} milliseconds
    And I select "Delete user(s)" from "Action"
    And I press "Apply to selected items"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot

  @api @javascript
  Scenario: Delete the user
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/user"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ user_name_operate_updated }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ user_name_operate_updated }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/user/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I take screenshot
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/user"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ user_name_operate_updated }}"

  @api @javascript
  Scenario: Delete the role
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/role"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ role_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ role_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/role/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/role"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ role_name_operate }}"

  @api @javascript
  Scenario: Delete the project
    Given I am logged in as a user with the "Administrator" role
    When I visit "/{{ root_path }}/openstack/{{ cloud_context }}/project"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I wait {{ wait_render }} milliseconds
    And I should see the link "{{ project_name_operate }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ project_name_operate }}"
    And I wait {{ wait_render }} milliseconds
    And the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/project/"
    And I click "Delete" in the actions
    Then the url should match "/delete"
    And I press "Delete"
    And I wait {{ wait_render }} milliseconds
    Then I should be on "/{{ root_path }}/openstack/{{ cloud_context }}/project"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ project_name_operate }}"
