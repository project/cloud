@minimal @ci_job
Feature: Read the flavor for OpenStack as an "authenticated user"

  @api @javascript
  Scenario: Read the flavor
    Given I am logged in as user "{{ drupal_user_name }}"
    And I visit "/{{ root_path }}/openstack/{{ cloud_context }}/flavor"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    And I should see the link "{{ instance_flavor }}"
    And I click "{{ instance_flavor }}"
    And I wait {{ wait_render }} milliseconds
    And I take screenshot
    Then the url should match "/{{ root_path }}/openstack/{{ cloud_context }}/flavor/"
    And I should see "{{ instance_flavor }}" in the "Name"
