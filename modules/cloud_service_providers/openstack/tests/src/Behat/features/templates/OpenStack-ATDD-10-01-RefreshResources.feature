@ci_job
Feature: Confirm the availability of the refresh button

  @api
  Scenario Outline: Refresh the list of resource as an administrator
    Given I am logged in as a user with the "Administrator" role
    When I visit "/clouds/openstack/<resource_type>"
    And I click "Refresh"
    Then I should see the success message "Updated"

    Examples:
      | resource_type    |
      | instance         |
      | image            |
      | security_group   |
      | floating_ip      |
      | key_pair         |
      | volume           |
      | snapshot         |
      | network          |
      | subnet           |
      | port             |
      | router           |
      | quota            |
      | stack            |
      | template_version |
      | server_group     |
      | project          |
      | role             |
      | user             |

  @api
  Scenario Outline: Refresh the list of resource as Cloud administrator
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/openstack/<resource_type>"
    And I click "Refresh"
    Then I should see the success message "Updated"

    Examples:
      | resource_type    |
      | instance         |
      | image            |
      | security_group   |
      | floating_ip      |
      | key_pair         |
      | volume           |
      | snapshot         |
      | network          |
      | subnet           |
      | port             |
      | router           |
      | quota            |
      | stack            |
      | template_version |
      | server_group     |
      | project          |
      | role             |
      | user             |

  @api
  Scenario Outline: Confirm the refresh button is not visible to Authenticated user
    Given I am logged in as a user with the "Authenticated user" role
    When I visit "/clouds/openstack/<resource_type>"
    And I should not see "Refresh"

    Examples:
      | resource_type    |
      | instance         |
      | image            |
      | security_group   |
      | floating_ip      |
      | key_pair         |
      | volume           |
      | snapshot         |
      | network          |
      | subnet           |
      | port             |
      | router           |
      | quota            |
      | stack            |
      | template_version |
      | server_group     |
      | project          |
      | role             |
      | user             |
