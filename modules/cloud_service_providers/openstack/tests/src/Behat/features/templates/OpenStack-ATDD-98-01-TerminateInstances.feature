@minimal @ci_job @launch_instance
Feature: Terminate an instance as an "authenticated user"

  @api
  Scenario: Terminate the created instance
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/openstack/{{ cloud_context }}/instance"
    And I run drush cron
    And I should see "running" in the "{{ instance_name }}" row
    And I should see the link "{{ instance_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ instance_name }}"
    Then the url should match "/clouds/openstack/{{ cloud_context }}/instance/"
    And I wait {{ wait }} milliseconds
    And I click "Delete"
    Then the url should match "/terminate"
    And I press "Delete | Terminate"
    Then I should be on "/clouds/openstack/{{ cloud_context }}/instance"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I run drush cron
    And I should see with "stopped" in the "{{ instance_name }}" row, no rows, or no table

  @api
  Scenario: Delete the Cloud launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I run drush cron
    And I should see the link "{{ instance_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ instance_name }}"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}/"
    And I click "Delete"
    Then the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I wait {{ wait_deleted }} milliseconds
    And I run drush cron
    And I should not see the link "{{ instance_name }}"
