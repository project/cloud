@minimal @ci_job @launch_instance
Feature: Delete the created resources for OpenStack as an "authenticated user"

  @api
  Scenario Outline: Delete the OpenStack resource
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/openstack/{{ cloud_context }}/<resource_type>"
    And I run drush cron
    And I should see the link "<resource_name>"
    And I wait {{ wait }} milliseconds
    And I click "<resource_name>"
    Then the url should match "/clouds/openstack/{{ cloud_context }}/<resource_type>/"
    And I click "Delete"
    Then the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/openstack/{{ cloud_context }}/<resource_type>"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "<resource_name>"

    Examples:
    | resource_type  | resource_name             |
    | key_pair       | {{ key_pair_name }}       |
    | security_group | {{ security_group_name }} |
    | subnet         | {{ subnet_name }}         |
    | network        | {{ network_name }}        |

  @api
  Scenario: Delete the server group
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/clouds/openstack/{{ cloud_context }}/server_group"
    And I run drush cron
    And I should see the link "{{ server_group_name }}"
    And I wait {{ wait }} milliseconds
    And I click "{{ server_group_name }}"
    And the url should match "/clouds/openstack/{{ cloud_context }}/server_group/"
    And I click "Delete"
    Then the url should match "/delete"
    And I press "Delete"
    Then I should be on "/clouds/openstack/{{ cloud_context }}/server_group"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I run drush cron
    And I should not see the link "{{ server_group_name }}"
