#!/bin/bash
#
# Clean up OpenStack resources.
#
readonly ALL=(
  'instance' 'servergroup' 'keypair' 'image' 'volume' 'snapshot'
  'securitygroup' 'subnet' 'port' 'router' 'network' 'orchestration'
  'project' 'role' 'user'
)
readonly TOKEN_PLACEHOLDER='%TOKEN%'

readonly DEFAULT_DRY_RUN_OPTION='--dry-run'
readonly DEFAULT_USERNAME='admin'
readonly DEFAULT_DOMAIN_ID='default'
readonly DEFAULT_COMPUTE_API_ENDPOINT_SUFFIX='/compute'
readonly DEFAULT_COMPUTE_API_VERSION='v2.1'
readonly DEFAULT_IDENTITY_API_ENDPOINT_SUFFIX='/identity'
readonly DEFAULT_IDENTITY_API_VERSION='v3'
readonly DEFAULT_IMAGE_API_ENDPOINT_SUFFIX='/image'
readonly DEFAULT_IMAGE_API_VERSION='v2'
readonly DEFAULT_NETWORK_API_ENDPOINT_SUFFIX=':9696/networking'
readonly DEFAULT_NETWORK_API_VERSION='v2.0'
readonly DEFAULT_VOLUME_API_ENDPOINT_SUFFIX='/volume'
readonly DEFAULT_VOLUME_API_VERSION='v3'
readonly DEFAULT_ORCHESTRATION_API_ENDPOINT_SUFFIX='/heat-api'
readonly DEFAULT_ORCHESTRATION_API_VERSION='v1'

function info() {
  echo "  $*" >&2
}

function err() {
  echo "  [Error] $*" >&2
}

function install_if_command_not_found() {
  local cmd="${1}"
  if ! (command -v "${cmd}" &>/dev/null); then
    info "Command not found: ${cmd}"
    apt-get update && apt-get -y install "${cmd}"

    if ! (command -v "${cmd}" &>/dev/null); then
      err "Failed to install ${cmd}"
      exit 1
    fi
  fi
}

function usage() {
  cat <<EOF
Usage: $0 -n name_pattern --project-id project_id [OPTION]

Example:
  OPENSTACK_PASSWORD=password  \\
  $0  \\
    -n BDD_.*_random -t instance  \\
    --api-endpoint-base https://example.com  \\
    --project-id 177eb30821644194a9bb2befab82d4cd

OPTION:

  --dry-run | --no-dry-run (boolean)
    dry-run option set to kubectl delete commands. [default: ${DEFAULT_DRY_RUN_OPTION}]

  -h, --help:               Show this usage.
  -n, --name:               Name pattern of OpenStack resources to clean up.
                            Required.
  -t, --target:             Target resources to clean up.
                            [default: ${ALL[@]}]

  --username:               Username. [default: ${DEFAULT_USERNAME}]
  --domain-id:              Domain ID. [default: ${DEFAULT_DOMAIN_ID}]
  --project-id:             Project ID. Required.

  --api-endpoint-base:      API endpoint base, e.g. \`https://example.com'
                            At least one of --api-endpoint-base or
                            --identity-api-endpoint is required
  --identity-api-endpoint:  Identity API endpoint.
                            At least one of --api-endpoint-base or
                            --identity-api-endpoint is required
  --identity-api-version:       Identity API version. [default: ${DEFAULT_IDENTITY_API_VERSION}]
  --compute-api-endpoint:       Compute API endpoint.
  --compute-api-version:        Compute API version. [default: ${DEFAULT_COMPUTE_API_VERSION}]
  --image-api-endpoint:         Image API endpoint.
  --image-api-version:          Image API version. [default: ${DEFAULT_IMAGE_API_VERSION}]
  --network-api-endpoint:       Network API endpoint.
  --network-api-version:        Network API version. [default: ${DEFAULT_NETWORK_API_VERSION}]
  --volume-api-endpoint:        Volume API endpoint.
  --volume-api-version:         Volume API version. [default: ${DEFAULT_VOLUME_API_VERSION}]
  --orchestration-api-endpoint: Orchestration API endpoint.
  --orchestration-version:      Orchestration API version. [default: ${DEFAULT_ORCHESTRATION_API_VERSION}]
EOF
  exit 1
}

function main() {
  while [[ "x$1" != 'x' ]]; do
    case "$1" in
    --dry-run | --no-dry-run) dry_run_delete="$1"; shift;;
    -n | --name) shift; val_reg_exp="$1"; shift;;
    -t | --target) shift;
     while [[ -n "$1" ]] && [[ ! $1 =~ '-' ]]; do targets+=($1); shift; done;;

    --username) shift; username="$1"; shift;;
    --domain-id) shift; domain_id="$1"; shift;;
    --project-id) shift; project_id="$1"; shift;;

    --api-endpoint-base)          shift; api_endpoint_base="$1";          shift;;
    --identity-api-endpoint)      shift; identity_api_endpoint="$1";      shift;;
    --identity-api-version)       shift; identity_api_version="$1";       shift;;
    --compute-api-endpoint)       shift; compute_api_endpoint="$1";       shift;;
    --compute-api-version)        shift; compute_api_version="$1";        shift;;
    --image-api-endpoint)         shift; image_api_endpoint="$1";         shift;;
    --image-api-version)          shift; image_api_version="$1";          shift;;
    --network-api-endpoint)       shift; network_api_endpoint="$1";       shift;;
    --network-api-version)        shift; network_api_version="$1";        shift;;
    --volume-api-endpoint)        shift; volume_api_endpoint="$1";        shift;;
    --volume-api-version)         shift; volume_api_version="$1";         shift;;
    --orchestration-api-endpoint) shift; orchestration_api_endpoint="$1"; shift;;
    --orchestration-api-version)  shift; orchestration_api_version="$1";  shift;;
    *) usage ;;
    esac
  done
  if [[ -z "${val_reg_exp}" ]]; then
    echo 'Missing name pattern.'
    usage
  fi
  if [[ -z "${project_id}" ]]; then
    echo 'Missing project ID.'
    usage
  fi
  if [[ -z "${identity_api_endpoint}" ]] && [[ -z "${api_endpoint_base}" ]]; then
    err 'At least one of --api-endpoint-base or --identity-api-endpoint'  \
        'must be passed.'
    usage
  fi
}

function setup() {
  install_if_command_not_found 'curl'
  install_if_command_not_found 'jq'

  if [[ ! -v OPENSTACK_PASSWORD ]]; then
    err 'OPENSTACK_PASSWORD variable is not set.'
    exit 1
  fi

  if [[ ! -v targets ]]; then
    targets=("${ALL[@]}")
  fi
  # Add one for authentication
  readonly TOTAL=$(( 1+${#targets[@]} ))

  dry_run_delete="${dry_run_delete:-${DEFAULT_DRY_RUN_OPTION}}"
  username="${username:-${DEFAULT_USERNAME}}"
  domain_id="${domain_id:-${DEFAULT_DOMAIN_ID}}"

  compute_api_endpoint="${compute_api_endpoint:-${api_endpoint_base}${DEFAULT_COMPUTE_API_ENDPOINT_SUFFIX}}"
  compute_api_version="${compute_api_version:-${DEFAULT_COMPUTE_API_VERSION}}"

  identity_api_endpoint="${identity_api_endpoint:-${api_endpoint_base}${DEFAULT_IDENTITY_API_ENDPOINT_SUFFIX}}"
  identity_api_version="${identity_api_version:-${DEFAULT_IDENTITY_API_VERSION}}"

  image_api_endpoint="${image_api_endpoint:-${api_endpoint_base}${DEFAULT_IMAGE_API_ENDPOINT_SUFFIX}}"
  image_api_version="${image_api_version:-${DEFAULT_IMAGE_API_VERSION}}"

  network_api_endpoint="${network_api_endpoint:-${api_endpoint_base}${DEFAULT_NETWORK_API_ENDPOINT_SUFFIX}}"
  network_api_version="${network_api_version:-${DEFAULT_NETWORK_API_VERSION}}"

  volume_api_endpoint="${volume_api_endpoint:-${api_endpoint_base}${DEFAULT_VOLUME_API_ENDPOINT_SUFFIX}}"
  volume_api_version="${volume_api_version:-${DEFAULT_VOLUME_API_VERSION}}"

  orchestration_api_endpoint="${orchestration_api_endpoint:-${api_endpoint_base}${DEFAULT_ORCHESTRATION_API_ENDPOINT_SUFFIX}}"
  orchestration_api_version="${orchestration_api_version:-${DEFAULT_ORCHESTRATION_API_VERSION}}"
}

count=0
function echo_count() {
  echo
  echo "($((++count))/${TOTAL}) $1"
}

function print_env() {
  cat << EOF
Username:                   ${username}
Domain ID:                  ${domain_id}
Project ID:                 ${project_id}

Identity API endpoint:      ${identity_api_endpoint}
Identity API version:       ${identity_api_version}
Compute API endpoint:       ${compute_api_endpoint}
Compute API version:        ${compute_api_version}
Image API endpoint:         ${image_api_endpoint}
Image API version:          ${image_api_version}
Network API endpoint:       ${network_api_endpoint}
Network API version:        ${network_api_version}
Volume API endpoint:        ${volume_api_endpoint}
Volume API version:         ${volume_api_version}
Orchestration API endpoint: ${orchestration_api_endpoint}
Orchestration API version:  ${orchestration_api_version}

curl version:               $(curl --version)
Dry-run option to delete:   ${dry_run_delete}
Name pattern of resources:  ${val_reg_exp}
Resource types:             ${targets[@]}
EOF
}

function get_token() {
  local auth_data="$(cat << EOD
{
  "auth": {
    "identity": {
      "methods": ["password"],
      "password": {
        "user": {
          "name": "${username}",
          "domain": {"id": "${domain_id}"},
          "password": "${OPENSTACK_PASSWORD}"
        }
      }
    },
    "scope": {
      "project": {
        "id": "${project_id}",
        "domain": {"id": "${domain_id}"}
      }
    }
  }
}
EOD
  )"

  curl -isL -H "Content-Type: application/json" -d "${auth_data}" \
    "${identity_api_endpoint}/${identity_api_version}/auth/tokens" \
  | grep 'X-Subject-Token:' \
  | sed -e 's/X-Subject-Token: //' \
  | tr -d '\r' | tr -d '\n'

  for status in ${PIPESTATUS[@]}; do
    [[ ${status} -eq 0 ]] || return 1
  done
  return 0
}

function api_url_of() {
  set -u
  case "$1" in
    'instance')
      # https://docs.openstack.org/api-ref/compute/#delete-server
      echo "${compute_api_endpoint}/${compute_api_version}/servers";;

    'servergroup')
      # https://docs.openstack.org/api-ref/compute/#delete-server-group
      echo "${compute_api_endpoint}/${compute_api_version}/os-server-groups";;

    'keypair')
      # https://docs.openstack.org/api-ref/compute/#delete-keypair
      echo "${compute_api_endpoint}/${compute_api_version}/os-keypairs";;

    'image')
      # https://docs.openstack.org/api-ref/image/v2/index.html#delete-image
      echo "${image_api_endpoint}/${image_api_version}/images";;

    'volume')
      # https://docs.openstack.org/api-ref/block-storage/v3/index.html#delete-a-volume
      echo "${volume_api_endpoint}/${volume_api_version}/volumes";;

    'snapshot')
      # https://docs.openstack.org/api-ref/block-storage/v3/index.html#delete-a-snapshot
      echo "${volume_api_endpoint}/${volume_api_version}/snapshots";;

    'securitygroup')
      # https://docs.openstack.org/api-ref/network/v2/index.html#delete-security-group
      echo "${network_api_endpoint}/${network_api_version}/security-groups";;

    'subnet')
      # https://docs.openstack.org/api-ref/network/v2/index.html#delete-subnet
      echo "${network_api_endpoint}/${network_api_version}/subnets";;

    'port')
      # https://docs.openstack.org/api-ref/network/v2/index.html#delete-port
      echo "${network_api_endpoint}/${network_api_version}/ports";;

    'router')
      # https://docs.openstack.org/api-ref/network/v2/index.html#delete-router
      echo "${network_api_endpoint}/${network_api_version}/routers";;

    'network')
      # https://docs.openstack.org/api-ref/network/v2/index.html#delete-network
      echo "${network_api_endpoint}/${network_api_version}/networks";;

    'orchestration')
      # https://docs.openstack.org/api-ref/orchestration/v1/#delete-stack
      echo "${orchestration_api_endpoint}/${orchestration_api_version}/stacks";;

    'project')
      # https://docs.openstack.org/api-ref/identity/v3/index.html#delete-project
      echo "${identity_api_endpoint}/${identity_api_version}/projects";;

    'role')
      # https://docs.openstack.org/api-ref/identity/v3/index.html#delete-role
      echo "${identity_api_endpoint}/${identity_api_version}/roles";;

    'user')
      # https://docs.openstack.org/api-ref/identity/v3/index.html#delete-user
      echo "${identity_api_endpoint}/${identity_api_version}/users";;

    *) err "Unknown resource type '$1'"; set +u; return 1;;
  esac
  set +u
}

function topic_attribute_of() {
  case "$1" in
    'instance')
      # https://docs.openstack.org/api-ref/compute/#delete-server
      echo 'servers';;

    'servergroup')
      # https://docs.openstack.org/api-ref/compute/#delete-server-group
      echo 'server_groups';;

    'keypair')
      # https://docs.openstack.org/api-ref/compute/#delete-keypair
      echo 'keypairs';;

    'image')
      # https://docs.openstack.org/api-ref/image/v2/index.html#delete-image
      echo 'images';;

    'volume')
      # https://docs.openstack.org/api-ref/block-storage/v3/index.html#delete-a-volume
      echo 'volumes';;

    'snapshot')
      # https://docs.openstack.org/api-ref/block-storage/v3/index.html#delete-a-snapshot
      echo 'snapshots';;

    'securitygroup')
      # https://docs.openstack.org/api-ref/network/v2/index.html#delete-security-group
      echo 'security_groups';;

    'subnet')
      # https://docs.openstack.org/api-ref/network/v2/index.html#delete-subnet
      echo 'subnets';;

    'port')
      # https://docs.openstack.org/api-ref/network/v2/index.html#delete-port
      echo 'ports';;

    'router')
      # https://docs.openstack.org/api-ref/network/v2/index.html#delete-router
      echo 'routers';;

    'network')
      # https://docs.openstack.org/api-ref/compute/#delete-network
      echo 'networks';;

    'orchestration')
      # https://docs.openstack.org/api-ref/orchestration/v1/#delete-stack
      echo 'stacks';;

    'project')
      # https://docs.openstack.org/api-ref/identity/v3/index.html#delete-project
      echo 'projects';;

    'role')
      # https://docs.openstack.org/api-ref/identity/v3/index.html#delete-role
      echo 'roles';;

    'user')
      # https://docs.openstack.org/api-ref/identity/v3/index.html#delete-user
      echo 'users';;

    *) err "Unknown resource type '$1'"; return 1;;
  esac
}

function request_api() {
  local method="$1"
  local url="$2"

  local curl_command="curl -X ${method} -sL -H 'X-Auth-Token: ${TOKEN_PLACEHOLDER}' ${url}"
  info "Executing \"${curl_command}\"."
  curl_command="$(echo "${curl_command}" | sed -e "s/${TOKEN_PLACEHOLDER}/${token}/")"

  if ! raw_response="$(eval "${curl_command}")"; then
    err "Failed to execute curl."
    return 1
  fi

  if [[ "${response}" == '' ]]; then
    return 0
  fi

  if ! response="$(echo "${raw_response}" | jq)"; then
    err "Failed to parse API response: ${raw_response}"
    return 1
  fi

  if [[ "$(echo "${response}" | jq 'has("error")')" == 'true' ]]; then
    err "The API server responded an error: ${response}"
    return 1
  fi

  echo "${response}"

  return 0
}

function list_resources() {
  local resource_type="$1"

  api_url="$(api_url_of "${resource_type}")" || return 1
  response="$(request_api 'GET' "${api_url}")" || return 1
  attr="$(topic_attribute_of "${resource_type}")" || return 1
  if [[ "${resource_type}" == 'keypair' ]]; then
    results="$(
      echo "${response}"  \
      | jq -r ".${attr}[].keypair.name | select(.|test(\"^${val_reg_exp}\$\"))"
    )" || return 1
  else
    results="$(
      echo "${response}"  \
      | jq -r ".${attr}[] | select(.name|test(\"^${val_reg_exp}\$\")) | .id"
    )" || return 1
  fi
  echo "${results}"
}

function get_status() {
  local resource_type="$1"
  local resource_id="$2"

  api_url="$(api_url_of "${resource_type}")" || return 1
  response="$(request_api 'GET' "${api_url}/${resource_id}")" || return 1
  attr="$(topic_attribute_of "${resource_type}")" || return 1
  status="$(echo ${response} | jq -r ".${attr}.status")" || return 1
  echo "${status}"
}

function delete() {
  local resource_type="$1"
  shift
  local resource_ids="$@"

  api_url="$(api_url_of "${resource_type}")" || return 1
  if [[ "${dry_run_delete}" == '--no-dry-run' ]]; then
    method='DELETE'
  else
    info 'Make a GET request instead of DELETE since "--dry-run" option was'  \
         'passed.'
    method='GET'
  fi

  for resource_id in ${resource_ids[@]}; do
    request_api "${method}" "${api_url}/${resource_id}"
    request_status="${?}"
    if [[ "${request_status}" -ne '0' ]] ; then
      return "${request_status}"
    fi
  done

  if [[ "${dry_run_delete}" == '--no-dry-run' ]]; then
    return 0
  else
    return 1
  fi
}


## main
main "$@"
setup
echo '** Cleanup OpenStack resources'
print_env

echo_count 'Scenario: Authentication'
if ! token="$(get_token)"; then
  err "Failed to authenticate"
  exit 1
fi

for target in "${targets[@]}"; do
  echo_count "Scenario: Delete the ${target}"
  if ! results_to_delete="$(list_resources "${target}")"; then
    continue
  fi

  if [[ -z "${results_to_delete}" ]]; then
    info 'Nothing to delete.'
    continue
  fi
  info 'Found the following resource(s) to delete:'
  info "${results_to_delete}"
  if ! delete "${target}" "${results_to_delete}"; then
    continue
  fi

  results_after_deletion="$(list_resources "${target}")"
  if [[ -z "${results_after_deletion}" ]]; then
    info 'Deleted the resources.'
  fi
  for resource_id in ${results_after_deletion}; do
    status="$(get_status "${target}" "${resource_id}")"
    if [[ "${status}" == 'ACTIVE' ]]; then
      err "Failed to delete ${resource_id}"
    else
      info "Takes time to delete ${resource_id}"
    fi
  done
done
