<?php

namespace Drupal\Tests\openstack\Functional\Config;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\Tests\openstack\Traits\OpenStackTestFormDataTrait;

/**
 * Test Case class for OpenStack Admin Setting forms.
 *
 * @group OpenStack
 */
class OpenStackAdminSettingsTest extends OpenStackTestBase {

  use OpenStackTestFormDataTrait;

  public const OPENSTACK_ADMIN_SETTINGS_REPEAT_COUNT = 2;

  /**
   * Get permissions of login user.
   *
   * @return array
   *   permissions of login user.
   */
  protected function getPermissions(): array {
    return [
      'administer openstack',
      'administer site configuration',
    ];
  }

  /**
   * Test for OpenStackAdminSettings.
   *
   * @param string $setting_type
   *   The setting type id.
   * @param array $edit
   *   The array of input data.
   */
  protected function runOpenStackAdminSettings($setting_type, array $edit): void {
    $this->drupalGet("/admin/config/services/cloud/openstack/{$setting_type}");
    $this->assertNoErrorMessage();

    $this->submitForm(
      $edit,
      $this->t('Save')->render()
    );
    $this->assertNoErrorMessage();
  }

  /**
   * Test OpenStack Admin Setting forms on Settings.
   */
  public function testOpenStackAdminSettings(): void {
    $edit = $this->createOpenStackSettingsFormData(self::OPENSTACK_ADMIN_SETTINGS_REPEAT_COUNT);

    for ($i = 0; $i < self::OPENSTACK_ADMIN_SETTINGS_REPEAT_COUNT; $i++) {
      $edit[$i] = array_intersect_key($edit[$i], array_flip(
        (array) array_rand($edit[$i], random_int(1, count($edit[$i])))
      ));
      $this->runOpenStackAdminSettings('settings', $edit[$i]);
    }
  }

}
