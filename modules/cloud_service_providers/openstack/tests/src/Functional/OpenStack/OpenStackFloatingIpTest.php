<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\cloud\Functional\Utils;
use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackInstance;
use Drupal\openstack\Entity\OpenStackNetwork;

/**
 * Tests OpenStack floating IP.
 *
 * @group OpenStack
 */
class OpenStackFloatingIpTest extends OpenStackTestBase {

  public const OPENSTACK_FLOATING_IP_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack floating ip',
      'add openstack floating ip',
      'view any openstack floating ip',
      'edit any openstack floating ip',
      'delete any openstack floating ip',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      // For Floating IP.
      'public_ip' => Utils::getRandomPublicIp(),
      'allocation_id' => 'eipalloc-' . $this->getRandomId(),
      'domain' => 'vpc',

      // For instance.
      'instance_id' => 'i-' . $this->getRandomId(),

      // For Network.
      'network_id' => 'network' . $this->getRandomId(),
      'vpc_id' => 'vpc-' . $this->getRandomId(),
      'description' => 'description-' . $this->random->name(64, TRUE),
      'subnet_id' => 'subnet_id-' . $this->getRandomId(),
      'is_primary' => TRUE,
      'primary_private_ip' => Utils::getRandomPrivateIp(),
      'secondary_private_ip' => Utils::getRandomPrivateIp(),
      'attachment_id' => 'attachment-' . $this->getRandomId(),
      'external' => TRUE,
      'name' => $this->random->name(8, TRUE),
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for Floating IP information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testFloatingIp(): void {
    $cloud_context = $this->cloudContext;
    $regions = ['RegionOne'];
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;
    $networks = [];
    if (!$openstack_ec2_api) {
      for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {

        $floating_ips = $this->createElasticIpRandomTestFormData();
        $floating_ips_count = count($floating_ips);
        $network_data = $this->createNetworkTestFormData($floating_ips_count);

        for ($j = 0; $j < $floating_ips_count; $j++) {

          $floating_ip = $floating_ips[$j];

          // Set up a test instance.
          $instance_id = 'i-' . $this->getRandomId();
          $instance = $this->createInstanceTestEntity(OpenStackInstance::class, $j, $regions, $floating_ip['PublicIp'], '', $instance_id);
          $instance_id = $this->addOpenStackInstanceMockData(
            OpenStackInstanceTest::class,
            $instance->getName(),
            $instance->getKeyPairName(),
            $regions,
            'running',
            '',
            $cloud_context
          );

          // Set up a test network interface.
          $this->createNetworkTestEntity(OpenStackNetwork::class, $j, $cloud_context);
          $this->addNetworkMockData($network_data[$j], $cloud_context, $this->webUser->id());
        }
      }
    }

    // List Floating IP for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
    $this->assertNoErrorMessage();

    $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/add");

    // Add a new Floating IP.
    $add = $this->createElasticIpTestFormData(self::OPENSTACK_FLOATING_IP_REPEAT_COUNT, $networks);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {
      $this->reloadMockData();
      if ($openstack_ec2_api) {
        unset($add[$i]['floating_network_id']);
        $domain = $this->getRandomDomain();
        $this->updateDomainMockData($domain);
      }

      $defaults = $this->latestTemplateVars;

      unset($add[$i]['network_border_group']);

      if (!$openstack_ec2_api) {
        unset($add[$i]['domain']);
        // Set up a test network interface.
        $add[$i]['floating_network_id'] = $defaults['network_id'];
        $this->createNetworkTestEntity(OpenStackNetwork::class, 0, $cloud_context, $defaults['network_id'], $defaults['network_id']);
      }

      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Floating IP', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/$num");
      $this->assertNoErrorMessage();

      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());

      if ($openstack_ec2_api) {
        // Make sure domain is updated.
        // @phpstan-ignore-next-line
        $this->assertSession()->pageTextContains($domain);
      }

      // Make sure listing.
      $add_mock_data = $this->getMockDataFromConfig();
      $add_mock_public_ip = $add_mock_data['AllocateAddress']['PublicIp'];

      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add_mock_public_ip);
      }
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all floating_ip listing exists.
      $this->drupalGet('/clouds/openstack/floating_ip');
      $this->assertNoErrorMessage();

      $mock_data = $this->getMockDataFromConfig();
      $mock_public_ip = $mock_data['AllocateAddress']['PublicIp'];

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($mock_public_ip);
      }
    }

    // Edit a Floating IP information.
    $edit = $this->createElasticIpTestFormData(self::OPENSTACK_FLOATING_IP_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {

      unset(
        $edit[$i]['domain'],
        $edit[$i]['network_border_group'],
        $edit[$i]['floating_network_id']
      );

      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Floating IP', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Delete Floating IP
    // 3 times.
    $this->updateInstanceMockData(OpenStackInstanceTest::class, 0, '', $regions, 'stopped', $cloud_context);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Floating IP', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextNotContains($edit[$j]['name']);
      }
    }
  }

  /**
   * Tests deleting Floating IPs with bulk delete operation.
   *
   * @throws \Exception
   */
  public function testFloatingIpBulkDelete(): void {

    $cloud_context = $this->cloudContext;
    $regions = ['RegionOne'];
    $this->deleteAllElasticIpInMockData();

    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {

      // Create Floating IPs.
      $floating_ips = $this->createElasticIpRandomTestFormData();
      $floating_ips_count = count($floating_ips);
      $network_data = $this->createNetworkTestFormData($floating_ips_count);

      for ($j = 0; $j < $floating_ips_count; $j++) {

        $floating_ip = $floating_ips[$j];

        // Set up a test instance.
        $instance_id = 'i-' . $this->getRandomId();
        $instance = $this->createInstanceTestEntity(OpenStackInstance::class, $j, $regions, $floating_ip['PublicIp'], '', $instance_id);
        $instance_id = $this->addOpenStackInstanceMockData(
          OpenStackInstanceTest::class,
          $instance->getName(),
          $instance->getKeyPairName(),
          $regions,
          'running',
          '',
          $cloud_context
        );

        // Set up a test network interface.
        $this->createNetworkTestEntity(OpenStackNetwork::class, $j, '', '', $instance_id);
        $this->addNetworkMockData($network_data[$j], $cloud_context, $this->webUser->id());

        // Set up a test Floating IP.
        $this->createFloatingIpTestEntity($j, $floating_ip['Name'], $floating_ip['PublicIp'], $cloud_context);
        $this->addElasticIpMockData($floating_ip['Name'], $floating_ip['PublicIp'], 'standard');
      }

      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");

      $data = [];
      $data['action'] = 'openstack_floating_ip_delete_action';

      $checkboxes = $this->cssSelect('input[type=checkbox]');
      foreach ($checkboxes ?: [] as $checkbox) {
        if ($checkbox->getAttribute('name') === NULL) {
          continue;
        }

        $data[$checkbox->getAttribute('name')] = $checkbox->getAttribute('value');
      }

      // Confirm.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
      $this->submitForm(
        $data,
        $this->t('Apply to selected items')->render()
      );
      $this->assertNoErrorMessage();

      // Al lower case of Floating IP is correct since the original @label
      // linked
      // \Drupal\core\Entity\EntityType::getSingularLabel makes the string
      // lowercase.
      $message = 'Are you sure you want to delete these Floating IPs?';
      if ($floating_ips_count === 1) {
        $message = 'Are you sure you want to delete this Floating IP?';
      }
      $this->assertSession()->pageTextContains($message);

      foreach ($floating_ips ?: [] as $floating_ip) {
        $this->assertSession()->pageTextContains($floating_ip['Name']);
      }

      // Delete.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/delete_multiple");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      if ($floating_ips_count === 1) {
        $this->assertSession()->pageTextContains("Deleted $floating_ips_count Floating IP.");
      }
      else {
        $this->assertSession()->pageTextContains("Deleted $floating_ips_count Floating IPs.");
      }

      foreach ($floating_ips ?: [] as $floating_ip) {
        $t_args = ['@type' => 'Floating IP', '@label' => $floating_ip['Name']];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));
        $this->deleteFirstElasticIpMockData();
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      $this->assertSession()->pageTextContains($this->t('Updated Floating IPs.'));

      foreach ($floating_ips ?: [] as $floating_ip) {
        $name = $floating_ip['Name'];
        $this->assertSession()->pageTextNotContains($name);
      }
    }
  }

  /**
   * Test updating Floating IPs.
   *
   * @throws \Exception
   */
  public function testUpdateFloatingIpList(): void {

    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;
    $this->deleteAllElasticIpInMockData();

    // Add a new Floating IP.
    $add = $this->createElasticIpTestFormData(self::OPENSTACK_FLOATING_IP_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $add[$i]['public_ip'] = Utils::getRandomPublicIp();
      $this->addElasticIpMockData($add[$i]['name'], $add[$i]['public_ip'], $add[$i]['domain']);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Floating IPs.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/floating_ip/$num/edit");
      $this->assertSession()->linkExists($this->t('Associate Floating IP'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/floating_ip/$num/associate");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/floating_ip/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack floating IPs'));
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack floating IPs'));
      $this->assertNoErrorMessage();

      // Confirm the edit view.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/$num/edit");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/floating_ip/$num/edit");
      $this->assertSession()->linkExists($this->t('Associate Floating IP'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/floating_ip/$num/associate");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/floating_ip/$num/delete");
    }

    // Edit Floating IP information.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {

      // Set up a test instance.
      $instance_id = 'i-' . $this->getRandomId();

      // Change Floating IP name in mock data.
      $add[$i]['name'] = sprintf('eip-entity #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE));
      $add[$i]['association_id'] = $this->random->name(8, TRUE);
      $this->updateElasticIpMockData($i, $add[$i]['name'], $add[$i]['association_id'], $instance_id);

    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Floating IPs'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/$num");
      $this->assertSession()->linkExists($this->t('Disassociate'));

      // Confirm the edit view.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip/$num/edit");
      $this->assertSession()->linkExists($this->t('Disassociate Floating IP'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/floating_ip/$num/disassociate");
    }

    if ($openstack_ec2_api) {
      // Update Floating IP tags.
      for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {

        // Update tags.
        $add[$i]['tags_name'] = $this->getRandomId();
        $this->updateTagsInMockData($num - 1, 'Addresses', 'Name', $add[$i]['tags_name'], FALSE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
      for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkExists($add[$i]['tags_name']);
      }

      // Update Floating IP tags for empty.
      for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {

        // Update tags.
        $this->updateTagsInMockData($i, 'Addresses', 'Name', '', FALSE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
      for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkExists($add[$i]['tags_name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['public_ip']);
      }

      // Delete Floating IP tags.
      for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {

        // Update tags.
        $this->updateTagsInMockData($i, 'Addresses', 'Name', '', TRUE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
      for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['public_ip']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['public_ip']);
      }
    }

    // Delete Floating IP in mock data.
    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->deleteFirstElasticIpMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/floating_ip");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Floating IPs.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test updating all Floating IPs.
   *
   * @throws \Exception
   */
  public function testUpdateAllFloatingIpList(): void {
    $cloud_configs = [];

    // Delete init mock data.
    $this->deleteAllElasticIpInMockData();

    // List Floating IP for OpenStack.
    $this->drupalGet('/clouds/openstack/floating_ip');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new Floating IP.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createElasticIpTestFormData(self::OPENSTACK_FLOATING_IP_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
        $add[$i]['public_ip'] = Utils::getRandomPublicIp();
        $this->addElasticIpMockData($add[$i]['name'], $add[$i]['public_ip'], $add[$i]['domain']);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/floating_ip');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm Floating IP are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Floating IPs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/floating_ip/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/floating_ip/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Associate Floating IP'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/floating_ip/{$num}/associate");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/floating_ip/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack floating IPs'));
        // Click 'List OpenStack floating IPs'.
        $this->clickLink($this->t('List OpenStack floating IPs'));
        $this->assertNoErrorMessage();

        // Confirm the edit view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/floating_ip/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/floating_ip/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Associate Floating IP'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/floating_ip/{$num}/associate");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/floating_ip/{$num}/delete");
      }
    }

    // Edit Floating IP information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0, $num = 1; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++, $num++) {

        // Set up a test instance.
        $instance_id = "i-{$this->getRandomId()}";

        // Change Floating IP name in mock data.
        $add[$i]['name'] = sprintf('eip-entity #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE));
        $add[$i]['association_id'] = $this->random->name(8, TRUE);
        $this->updateElasticIpMockData($i, $add[$i]['name'], $add[$i]['association_id'], $instance_id);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/floating_ip');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm Floating IP are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Floating IPs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete Floating IP in mock data.
    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->deleteFirstElasticIpMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/floating_ip');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm Floating IP are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Floating IPs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_FLOATING_IP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

}
