<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackImage;

/**
 * Tests OpenStack image.
 *
 * @group OpenStack
 */
class OpenStackImageTest extends OpenStackTestBase {

  public const OPENSTACK_IMAGE_REPEAT_COUNT = 2;

  public const OPENSTACK_UPDATE_IMAGE_LIST_REFRESH_TIME_ADJUSTMENT = 10 * 60;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'add openstack image',
      'list openstack image',
      'view any openstack image',
      'edit any openstack image',
      'delete any openstack image',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    $architecture = ['x86_64', 'arm64'];
    $image_type = ['machine', 'kernel', 'ramdisk'];
    $state = ['available', 'pending', 'failed'];
    $hypervisor = ['ovm', 'xen'];
    $public = [0, 1];
    return [
      'image_id' => 'ami-' . $this->getRandomId(),
      'account_id' => random_int(100000000000, 999999999999),
      'name' => "OpenStackImageTest::getMockDataTemplateVars - {$this->random->name(8, TRUE)}",
      'kernel_id' => 'aki-' . $this->getRandomId(),
      'ramdisk_id' => 'ari-' . $this->getRandomId(),
      'product_code1' => $this->random->name(8, TRUE),
      'product_code2' => $this->random->name(8, TRUE),
      'image_location' => $this->random->name(16, TRUE),
      'state_reason_message' => $this->random->name(8, TRUE),
      'platform' => $this->random->name(8, TRUE),
      'description' => $this->random->string(8, TRUE),
      'creation_date' => date('c'),
      'architecture' => $architecture[array_rand($architecture)],
      'image_type' => $image_type[array_rand($image_type)],
      'state' => $state[array_rand($state)],
      'hypervisor' => $hypervisor[array_rand($hypervisor)],
      'public' => $public[array_rand($public)],
    ];
  }

  /**
   * Tests CRUD for image information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testImage(): void {
    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;
    $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
      'openstack',
      $cloud_context
    );

    $images = [];

    // Initialize all mock data in DescribeImages.
    $this->updateMockDataToConfig([
      'DescribeImages' => [
        'Images' => [],
      ],
    ]);

    // List image for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/image");
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Images.'));
    $this->assertNoErrorMessage();

    // Register a new image.
    $add = $this->createImageTestFormData(self::OPENSTACK_IMAGE_REPEAT_COUNT);

    // 3 times.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++, $num++) {

      // Need to process addImageMockData before saving the form data since
      // ImageCreateForm::save uses createImage method.
      $images[] = $this->addImageMockData($add[$i]['name'], $add[$i]['description'], $cloud_context, $this->webUser->id(), $tag_created_uid, $openstack_ec2_api);
      $this->addProjectSharedImageMockData($add[$i]);

      if (!$openstack_ec2_api) {
        unset(
          $add[$i]['instance_id'],
          $add[$i]['description']
        );
      }

      $this->drupalGet("/clouds/openstack/$cloud_context/image/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Image', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/image/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      // Click 'Refresh'.
      $this->drupalGet("/clouds/openstack/$cloud_context/image");
      $this->clickLink($this->t('Refresh'));
      $this->assertSession()->pageTextContains($this->t('Updated Images.'));
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/image/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all image listing exists.
      $this->drupalGet('/clouds/openstack/image');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit an image information.
    $edit = $this->createImageTestFormData(self::OPENSTACK_IMAGE_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++, $num++) {

      unset($edit[$i]['instance_id']);

      if (!$openstack_ec2_api) {
        unset($edit[$i]['description']);
      }

      $this->drupalGet("/clouds/openstack/$cloud_context/image/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure the description.
      if ($openstack_ec2_api) {
        $this->assertSession()->fieldValueEquals('description', $edit[$i]['description']);
      }

      $t_args = ['@type' => 'Image', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/image");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/image/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());

    }

    // Delete image.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++, $num++) {

      // Delete image.
      $this->deleteImage($cloud_context, $num, $edit[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/image");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextNotContains($edit[$i]['name']);
      }
      break;
    }
  }

  /**
   * Tests deleting images with bulk operation.
   *
   * @throws \Exception
   */
  public function testImageBulk(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
      // Create images.
      $images = $this->createImagesRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($images ?: [] as $image) {
        $entities[] = $this->createImageTestEntity(OpenStackImage::class, $index++, $image['ImageId'], $cloud_context);
      }

      // The first parameter type should be 'image' in OpenStack.
      $this->runTestEntityBulk('image', $entities);
    }
  }

  /**
   * Test updating image list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testUpdateImageList(): void {
    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;
    $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
      'openstack',
      $cloud_context
    );

    // Delete init mock data.
    $this->deleteFirstImageMockData();

    // Add a new image.
    $add = $this->createImageTestFormData(self::OPENSTACK_IMAGE_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
      $this->addImageMockData($add[$i]['name'], $add[$i]['description'], $cloud_context, $this->webUser->id(), $tag_created_uid, $openstack_ec2_api);
      $this->addProjectSharedImageMockData($add[$i]);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/image");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Images.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    $num = 1;
    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/image/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/image/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/image/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack images'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack images'));
      $this->assertNoErrorMessage();

      // Confirm the edit view.
      $this->drupalGet("/clouds/openstack/$cloud_context/image/$num/edit");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/image/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/image/$num/delete");
    }

    // Add a new image.
    $num++;
    $data = [
      'name'        => "Image #$num - " . date('Y/m/d - ') . $this->random->name(8, TRUE),
      'instance_id' => 'i-' . $this->getRandomId(),
      'description' => 'description-' . $this->random->name(64),
    ];
    $this->addImageMockData($data['name'], $data['description'], $cloud_context, $this->webUser->id(), $tag_created_uid, $openstack_ec2_api);
    $this->addProjectSharedImageMockData($data);

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextNotContains($data['name']);

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Images.'));
    $add = array_merge($add, [$data]);
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT + 1; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    if ($openstack_ec2_api) {
      // Update tags.
      for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {

        $add[$i]['tags_name'] = $this->getRandomId();
        $this->updateTagsInMockData($i, 'Images', 'Name', $add[$i]['tags_name'], FALSE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/image");
      for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      if ($openstack_ec2_api) {
        for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
          $this->assertSession()->linkExists($add[$i]['tags_name']);
        }
      }

      // Update tags for empty.
      for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {

        // Update tags.
        $this->updateTagsInMockData($i, 'Images', 'Name', '', FALSE);
      }

      // Make sure listing.
      if ($openstack_ec2_api) {
        $this->drupalGet("/clouds/openstack/$cloud_context/image");
        for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
          $this->assertSession()->linkExists($add[$i]['tags_name']);
        }
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['name']);
      }

      // Delete name tags.
      for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {

        // Update tags.
        $this->updateTagsInMockData($i, 'Images', 'Name', '', TRUE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/image");
      for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['name']);
      }
    }

    // Change refreshed time of entities.
    $entity_type_manager = \Drupal::entityTypeManager();
    $entities = $entity_type_manager->getStorage('openstack_image')->loadByProperties(
      ['cloud_context' => $cloud_context]
    );

    foreach ($entities ?: [] as $entity) {
      $timestamp = time();
      $timestamp -= self::OPENSTACK_UPDATE_IMAGE_LIST_REFRESH_TIME_ADJUSTMENT;
      $entity->setRefreshed($timestamp);
      $entity->save();
    }

    // Delete image in mock data.
    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT + 1; $i++) {
      $this->deleteFirstImageMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/image");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT + 1; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Images.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT + 1; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test updating all image list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function testUpdateAllImageList(): void {
    $cloud_configs = [];

    // List image for OpenStack.
    $this->drupalGet('/clouds/openstack/image');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Delete init mock data.
    $this->deleteFirstImageMockData();

    // Add a new image.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $openstack_ec2_api = $cloud_config->get('field_use_openstack_ec2_api')->value;
      $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
        'openstack',
        $cloud_context
      );
      $add = $this->createImageTestFormData(self::OPENSTACK_IMAGE_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
        $this->addImageMockData($add[$i]['name'], $add[$i]['description'], $cloud_context, $this->webUser->id(), $tag_created_uid, $openstack_ec2_api);
        $this->addProjectSharedImageMockData($add[$i]);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/image');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm images are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Images',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    $num = 1;
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/image/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/image/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/image/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack images'));
        // Click 'List OpenStack images'.
        $this->clickLink($this->t('List OpenStack images'));
        $this->assertNoErrorMessage();

        // Confirm the edit view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/image/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/image/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/image/{$num}/delete");
      }
    }

    // Add a new image.
    $num++;
    $data = [
      'name'        => "Image #{$num} - " . date('Y/m/d - ') . $this->random->name(8, TRUE),
      'instance_id' => "i-{$this->getRandomId()}",
      'description' => "description-{$this->random->name(64)}",
    ];
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $openstack_ec2_api = $cloud_config->get('field_use_openstack_ec2_api')->value;
      $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
        'openstack',
        $cloud_config->getCloudContext()
      );
      $this->addImageMockData($data['name'], $data['description'], $cloud_config->getCloudContext(), $this->webUser->id(), $tag_created_uid, $openstack_ec2_api);
      $this->addProjectSharedImageMockData($data);
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/image');
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextNotContains($data['name']);

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm images are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Images',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    $add = array_merge($add, [$data]);
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT + 1; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete image in mock data.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT + 1; $i++) {
        $this->deleteFirstImageMockData();
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/image');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT + 1; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm images are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Images',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_IMAGE_REPEAT_COUNT + 1; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Delete image.
   *
   * @param string $cloud_context
   *   Cloud context.
   * @param int $num
   *   Delete image number.
   * @param string $name
   *   Delete image name.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  private function deleteImage($cloud_context, $num, $name): void {
    $this->drupalGet("/clouds/openstack/$cloud_context/image/$num/delete");
    $this->assertNoErrorMessage();
    $this->drupalGet("/clouds/openstack/$cloud_context/image/$num/delete");
    $this->submitForm(
      [],
      $this->t('Delete')->render()
    );
    $this->assertNoErrorMessage();

    $t_args = ['@type' => 'Image', '@label' => $name];
    $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));
  }

}
