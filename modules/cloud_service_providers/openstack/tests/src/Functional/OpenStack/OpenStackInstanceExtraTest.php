<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\cloud\Functional\Utils;
use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackImage;
use Drupal\openstack\Entity\OpenStackSecurityGroup;

/**
 * Tests OpenStack instance.
 *
 * @group OpenStack
 */
class OpenStackInstanceExtraTest extends OpenStackTestBase {

  /**
   * Create three Instances for a test case.
   */
  public const OPENSTACK_INSTANCE_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'add openstack instance',
      'list openstack instance',
      'edit own openstack instance',
      'delete own openstack instance',
      'view own openstack instance',
      'edit any openstack instance',

      'list cloud server template',
      'view own published cloud server templates',
      'launch cloud server template',

      'add openstack image',
      'list openstack image',
      'view any openstack image',
      'edit any openstack image',
      'delete any openstack image',

      'add openstack security group',
      'list openstack security group',
      'view any openstack security group',
      'edit any openstack security group',
      'delete any openstack security group',

      'administer openstack',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    $public_ip = Utils::getRandomPublicIp();
    $private_ip = Utils::getRandomPrivateIp();
    $regions = ['us-west-1', 'us-west-2'];
    $region = $regions[array_rand($regions)];

    return [
      // 12 digits.
      'account_id' => random_int(100000000000, 999999999999),
      'reservation_id' => 'r-' . $this->getRandomId(),
      'group_name' => $this->random->name(8, TRUE),
      'host_id' => $this->random->name(8, TRUE),
      'affinity' => $this->random->name(8, TRUE),
      'launch_time' => date('c'),
      'security_group_id' => 'sg-' . $this->getRandomId(),
      'security_group_name' => $this->random->name(10, TRUE),
      'public_dns_name' => Utils::getPublicDns($region, $public_ip),
      'public_ip_address' => $public_ip,
      'private_dns_name' => Utils::getPrivateDns($region, $private_ip),
      'private_ip_address' => $private_ip,
      'vpc_id' => 'vpc-' . $this->getRandomId(),
      'subnet_id' => 'subnet-' . $this->getRandomId(),
      'image_id' => 'ami-' . $this->getRandomId(),
      'reason' => $this->random->string(16, TRUE),
      'instance_id' => 'i-' . $this->getRandomId(),
      'state' => 'running',
    ];
  }

  /**
   * Tests updating instance attributes.
   */
  public function testUpdateInstanceAttributes(): void {
    try {
      $this->repeatTestUpdateInstanceAttributes(self::OPENSTACK_INSTANCE_REPEAT_COUNT);
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Repeating test update instance attributes.
   *
   * @param int $max_test_repeat_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  private function repeatTestUpdateInstanceAttributes($max_test_repeat_count = 1): void {
    $cloud_context = $this->cloudContext;
    $regions = ['RegionOne'];
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    $vpc_ids = [];
    $add = $this->createOpenStackInstanceTestFormData($max_test_repeat_count);
    $edit = $this->createOpenStackInstanceTestFormData($max_test_repeat_count);
    for ($i = 0, $num = 1; $i < $max_test_repeat_count; $i++, $num++) {
      $vpc_id = "vpc-{$this->getRandomId()}";
      $vpc_ids[] = $vpc_id;

      // Create image.
      $image = $this->createImageTestEntity(OpenStackImage::class, $i, $add[$i]['image_id'], $cloud_context);

      // Make sure if the image entity is created or not.
      $this->drupalGet("/clouds/openstack/{$cloud_context}/image");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($image->getName());
      $this->assertSession()->pageTextContains($image->getImageId());
      $this->assertSession()->pageTextContains($add[$i]['image_id']);

      // Create security group.
      $security_group = $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, $i, $add[$i]['security_groups[]'], '', '', $cloud_context);

      // Make sure if the security group entity is created or not.
      $this->drupalGet("/clouds/openstack/{$cloud_context}/security_group");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($security_group->getName());
      $this->assertSession()->pageTextContains($security_group->getGroupId());
      $this->assertSession()->pageTextContains($add[$i]['security_groups[]']);

      // Setup cloud launch template and instance.
      $server_template = $this->createOpenStackLaunchTemplateTestEntity([], $image, $cloud_context);
      $server_template->set('field_openstack_security_group', $security_group->id());
      $server_template->save();

      // Make sure if the cloud_launch_template entity is created or not.
      $this->drupalGet("/clouds/design/server_template/{$cloud_context}");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($server_template->name->value);

      // Launch a stopped instance.
      $this->addOpenStackInstanceMockData(
        OpenStackInstanceTest::class,
        $add[$i]['name'],
        $add[$i]['key_pair_name'],
        $regions,
        'stopped',
        '',
        $cloud_context,
        $vpc_id
      );
      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/launch");
      $this->submitForm(
        [],
        $this->t('Launch')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains('stopped');

      // Edit instance.
      unset(
        $edit[$i]['image_id'],
        $edit[$i]['image_name'],
        $edit[$i]['min_count'],
        $edit[$i]['max_count'],
        $edit[$i]['key_pair_name'],
        $edit[$i]['is_monitoring'],
        $edit[$i]['availability_zone'],
        $edit[$i]['kernel_id'],
        $edit[$i]['ramdisk_id'],
        $edit[$i]['instance_type'],
        $edit[$i]['security_groups[]']
      );

      // Change security groups.
      $security_groups = $this->createSecurityGroupRandomTestFormData();
      $security_group_index = 0;
      foreach ($security_groups ?: [] as $security_group) {
        $security_group_name = $security_group['GroupName'];
        $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, $security_group_index++, $security_group['GroupId'], $security_group_name, $vpc_ids[$i], $cloud_context);

        $edit_key = $openstack_ec2_api ? 'security_groups[]' : "security_groups[{$security_group_name}]";
        $edit[$i][$edit_key] = $openstack_ec2_api ? array_column($security_groups, 'GroupName') : $security_group_name;
      }

      if (!$openstack_ec2_api) {
        unset(
          $edit[$i]['tags[0][item_key]'],
          $edit[$i]['tags[0][item_value]'],
          $edit[$i]['user_data']
        );
      }

      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $this->updateInstanceMockData(OpenStackInstanceTest::class, $i, $edit[$i]['name'], $regions, $cloud_context);

      $t_args = ['@type' => 'Instance', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
      $t_args = ['@type' => 'Instance', '%label' => $edit[$i]['name']];

      if ($openstack_ec2_api) {
        $this->assertSession()->pageTextContains(strip_tags($this->t('The user data of @type %label has been updated. Start the @type to reflect the user data.', $t_args)));
      }

      // Verify instance attributes.
      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num");
      $this->assertNoErrorMessage();
      if ($openstack_ec2_api) {
        $groups = implode(', ', $edit[$i]['security_groups[]']);
        $this->assertSession()->pageTextContains($groups);
        $this->assertSession()->pageTextContains($edit[$i]['user_data']);
      }
    }
  }

  /**
   * Tests setting the configuration of instance terminating.
   */
  public function testInstanceTerminateConfiguration(): void {
    try {
      $this->repeatTestInstanceTerminateConfiguration(self::OPENSTACK_INSTANCE_REPEAT_COUNT);
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Repeating test instance terminate configuration.
   *
   * @param int $max_test_repeat_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  private function repeatTestInstanceTerminateConfiguration($max_test_repeat_count = 1): void {
    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    $terminate_allowed_values = [TRUE, FALSE];
    $add = $this->createOpenStackInstanceTestFormData($max_test_repeat_count);
    for ($i = 0, $num = 1; $i < $max_test_repeat_count; $i++, $num++) {

      $terminate_value = $terminate_allowed_values[array_rand($terminate_allowed_values)];
      $this->drupalGet('admin/config/services/cloud/openstack/settings');
      $this->submitForm(
        ['openstack_instance_terminate' => $terminate_value],
        $this->t('Save configuration')->render()
      );
      $this->assertNoErrorMessage();

      // Create image.
      $image = $this->createImageTestEntity(OpenStackImage::class, $i, $add[$i]['image_id'], $cloud_context);

      // Make sure if the image entity is created or not.
      $this->drupalGet("/clouds/openstack/{$cloud_context}/image");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($image->getName());
      $this->assertSession()->pageTextContains($image->getImageId());
      $this->assertSession()->pageTextContains($add[$i]['image_id']);

      // Setup cloud launch template and instance.
      $server_template = $this->createOpenStackLaunchTemplateTestEntity([], $image, $cloud_context);

      // Make sure if the cloud_launch_template entity is created or not.
      $this->drupalGet("/clouds/design/server_template/{$cloud_context}");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($server_template->name->value);

      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/launch");
      if (!$openstack_ec2_api) {
        return;
      }

      if ($terminate_value) {
        $this->assertSession()->checkboxChecked('edit-terminate');
      }
      else {
        $this->assertSession()->checkboxNotChecked('edit-terminate');
      }
    }
  }

}
