<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\cloud\Functional\Utils;
use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackImage;
use Drupal\openstack\Entity\OpenStackSecurityGroup;

/**
 * Tests OpenStack instance for basic operations.
 *
 * @group OpenStack
 */
class OpenStackInstanceTest extends OpenStackTestBase {

  /**
   * Create three Instances for a test case.
   */
  public const OPENSTACK_INSTANCE_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',

      'add openstack instance',
      'list openstack instance',
      'edit own openstack instance',
      'delete own openstack instance',
      'view own openstack instance',
      'edit any openstack instance',

      'list cloud server template',
      'view own published cloud server templates',
      'launch cloud server template',

      'add openstack image',
      'list openstack image',
      'view any openstack image',
      'edit any openstack image',
      'delete any openstack image',

      'add openstack security group',
      'list openstack security group',
      'view any openstack security group',
      'edit any openstack security group',
      'delete any openstack security group',

      'administer openstack',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    $public_ip = Utils::getRandomPublicIp();
    $private_ip = Utils::getRandomPrivateIp();
    $regions = ['RegionOne'];
    $region = $regions[array_rand($regions)];

    return [
      // 12 digits.
      'account_id' => mt_rand(100000000000, 999999999999),
      'reservation_id' => 'r-' . $this->getRandomId(),
      'group_name' => $this->random->name(8, TRUE),
      'host_id' => $this->random->name(8, TRUE),
      'affinity' => $this->random->name(8, TRUE),
      'launch_time' => date('c'),
      'security_group_id' => 'sg-' . $this->getRandomId(),
      'security_group_name' => $this->random->name(10, TRUE),
      'public_dns_name' => Utils::getPublicDns($region, $public_ip),
      'public_ip_address' => $public_ip,
      'private_dns_name' => Utils::getPrivateDns($region, $private_ip),
      'private_ip_address' => $private_ip,
      'vpc_id' => 'vpc-' . $this->getRandomId(),
      'subnet_id' => 'subnet-' . $this->getRandomId(),
      'image_id' => 'ami-' . $this->getRandomId(),
      'reason' => $this->random->string(16, TRUE),
      'instance_id' => 'i-' . $this->getRandomId(),
      'state' => 'running',
    ];
  }

  /**
   * Tests EC2 instance.
   *
   * @throws \Exception
   */
  public function testInstance(): void {
    $cloud_context = $this->cloudContext;
    $regions = ['RegionOne'];
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    // List instance for Amazon EC2.
    $this->drupalGet("/clouds/openstack/{$cloud_context}/instance");
    $this->assertNoErrorMessage();

    // Launch a new instance.
    $vpc_ids = [];
    $add = $this->createOpenStackInstanceTestFormData(self::OPENSTACK_INSTANCE_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {
      $vpc_id = "vpc-{$this->getRandomId()}";
      $vpc_ids[] = $vpc_id;

      // Create image.
      $image = $this->createImageTestEntity(OpenStackImage::class, $i, $add[$i]['image_id'], $cloud_context);

      // Make sure if the image entity is created or not.
      $this->drupalGet("/clouds/openstack/{$cloud_context}/image");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($image->getName());
      $this->assertSession()->pageTextContains($image->getImageId());
      $this->assertSession()->pageTextContains($add[$i]['image_id']);

      // Create security group.
      $security_group = $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, $i, $add[$i]['security_groups[]'], '', '', $cloud_context);

      // Make sure if the security group entity is created or not.
      $this->drupalGet("/clouds/openstack/{$cloud_context}/security_group");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($security_group->getName());
      $this->assertSession()->pageTextContains($security_group->getGroupId());
      $this->assertSession()->pageTextContains($add[$i]['security_groups[]']);

      // Setup cloud launch template and instance.
      $server_template = $this->createOpenStackLaunchTemplateTestEntity([], $image, $cloud_context);
      $server_template->set('field_openstack_security_group', $security_group->id());
      $server_template->save();

      // Make sure if the cloud_launch_template entity is created or not.
      $this->drupalGet("/clouds/design/server_template/{$cloud_context}");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($server_template->name->value);

      $this->addOpenStackInstanceMockData(
        OpenStackInstanceTest::class,
        $add[$i]['name'],
        $add[$i]['key_pair_name'],
        $regions,
        'running',
        '',
        $cloud_context,
        $vpc_id
      );
      $this->drupalGet("/clouds/design/server_template/{$cloud_context}/{$server_template->id()}/launch");
      $this->submitForm(
        [],
        $this->t('Launch')->render()
      );
      $this->assertNoErrorMessage();

      // Make sure listing.
      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all instance listing exists.
      $this->drupalGet('/clouds/openstack/instance');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Edit an instance information.
    $edit = $this->createOpenStackInstanceTestFormData(self::OPENSTACK_INSTANCE_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {

      unset(
        $edit[$i]['image_id'],
        $edit[$i]['min_count'],
        $edit[$i]['max_count'],
        $edit[$i]['key_pair_name'],
        $edit[$i]['is_monitoring'],
        $edit[$i]['availability_zone'],
        $edit[$i]['instance_type'],
        $edit[$i]['kernel_id'],
        $edit[$i]['ramdisk_id'],
        $edit[$i]['user_data'],
        $edit[$i]['security_groups[]']
      );

      // Change security groups.
      $security_groups = $this->createSecurityGroupRandomTestFormData();
      $security_group_index = 0;
      foreach ($security_groups ?: [] as $security_group) {
        $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, $security_group_index++, $security_group['GroupId'], $security_group['GroupName'], $vpc_ids[$i], $cloud_context);

        $security_group_name = $security_group['GroupName'];
        $edit_key = $openstack_ec2_api ? 'security_groups[]' : "security_groups[{$security_group_name}]";
        $edit[$i][$edit_key] = $openstack_ec2_api ? array_column($security_groups, 'GroupName') : $security_group_name;
      }

      if ($openstack_ec2_api) {
        // Termination.
        $edit[$i]['termination_timestamp[0][value][date]'] = date('Y-m-d', time() + (int) (365.25 * 3));
        $edit[$i]['termination_timestamp[0][value][time]'] = '00:00:00';
        $edit[$i]['termination_protection'] = '1';
      }

      if (!$openstack_ec2_api) {
        unset(
          $edit[$i]['tags[0][item_key]'],
          $edit[$i]['tags[0][item_value]']
        );
      }

      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render());

      if ($openstack_ec2_api) {
        // Termination validation.
        $this->assertSession()->pageTextContains(
          $this->t('"@name1" should be left blank if "@name2" is selected. Please leave "@name1" blank or unselect "@name2".', [
            '@name1' => $this->t('Termination date'),
            '@name2' => $this->t('Termination protection'),
          ])
        );
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());

      unset(
        $edit[$i]['termination_timestamp[0][value][date]'],
        $edit[$i]['termination_timestamp[0][value][time]'],
        $edit[$i]['termination_protection']
      );

      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );

      $this->updateInstanceMockData(OpenStackInstanceTest::class, $i, $edit[$i]['name'], $regions, 'stopped', $cloud_context);

      $t_args = ['@type' => 'Instance', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/instance");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Terminate instance.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num/terminate");
      $this->assertNoErrorMessage();

      $this->deleteFirstInstanceMockData();

      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num/terminate");
      $this->submitForm(
        [],
        $this->t('Delete | Terminate')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['name']);

      $t_args = ['@type' => 'Instance', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/instance");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextNotContains($edit[$i]['name']);
      }
    }
  }

  /**
   * Tests updating instances.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateInstances(): void {
    $cloud_context = $this->cloudContext;
    $regions = ['RegionOne'];

    // Create image.
    $add = $this->createOpenStackInstanceTestFormData(1);
    $image = $this->createImageTestEntity(OpenStackImage::class, 0, $add[0]['image_id'], $cloud_context);

    // Make sure if the image entity is created or not.
    $this->drupalGet("/clouds/openstack/{$cloud_context}/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($image->getName());
    $this->assertSession()->pageTextContains($image->getImageId());
    $this->assertSession()->pageTextContains($add[0]['image_id']);

    // Create security group.
    $security_group = $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, 0, $add[0]['security_groups[]'], '', '', $cloud_context);

    // Make sure if the security group entity is created or not.
    $this->drupalGet("/clouds/openstack/{$cloud_context}/security_group");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($security_group->getName());
    $this->assertSession()->pageTextContains($security_group->getGroupId());
    $this->assertSession()->pageTextContains($add[0]['security_groups[]']);

    // Setup cloud launch template and instance.
    $server_template = $this->createOpenStackLaunchTemplateTestEntity([], $image, $cloud_context);
    $server_template->set('field_openstack_security_group', $security_group->id());
    $server_template->save();

    // Make sure if the cloud_launch_template entity is created or not.
    $this->drupalGet("/clouds/design/server_template/{$cloud_context}");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($server_template->name->value);

    // Launch a new instance.
    $this->addOpenStackInstanceMockData(
      OpenStackInstanceTest::class,
      $add[0]['name'],
      $add[0]['key_pair_name'],
      $regions,
      'running',
      '',
      $cloud_context
    );
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/launch");
    $this->submitForm(
      [],
      $this->t('Launch')->render()
    );
    $this->assertNoErrorMessage();

    // Change security groups.
    $security_group_name1 = $this->random->name(8, TRUE);
    $security_group_name2 = $this->random->name(8, TRUE);
    $this->updateSecurityGroupsMockData($security_group_name1, $security_group_name2);

    // Change instance type.
    $instance_type = $this->random->name(6, TRUE);
    $this->updateInstanceTypeMockData($instance_type);

    // Run cron job to update instances.
    $key = \Drupal::state()->get('system.cron_key');
    $this->drupalGet('/cron/' . $key);
    $this->assertSession()->statusCodeEquals(204);

    // Verify security group.
    $this->drupalGet("/clouds/openstack/$cloud_context/instance/1");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($security_group_name1 . ', ' . $security_group_name2);
    $this->assertSession()->pageTextContains($instance_type);
    // Make sure uid.
    $this->assertSession()->pageTextContains($this->webUser->getAccountName());
  }

  /**
   * Test validation of launching instances.
   */
  public function testLaunchValidation(): void {
    try {
      $this->repeatTestLaunchValidation(self::OPENSTACK_INSTANCE_REPEAT_COUNT);
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Repeat testing validation of launching instances.
   *
   * @param int $max_test_repeat_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  private function repeatTestLaunchValidation($max_test_repeat_count = 1): void {
    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    // Launch a new instance, with termination protection.
    $add = $this->createOpenStackInstanceTestFormData($max_test_repeat_count);
    for ($i = 0, $num = 1; $i < $max_test_repeat_count; $i++, $num++) {

      // Create image.
      $image = $this->createImageTestEntity(OpenStackImage::class, $i, $add[$i]['image_id'], $cloud_context);

      // Make sure if the image entity is created or not.
      $this->drupalGet("/clouds/openstack/{$cloud_context}/image");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($image->getName());
      $this->assertSession()->pageTextContains($image->getImageId());
      $this->assertSession()->pageTextContains($add[$i]['image_id']);

      // Create security group.
      $security_group = $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, 0, $add[0]['security_groups[]'], '', '', $cloud_context);

      // Make sure if the security group entity is created or not.
      $this->drupalGet("/clouds/openstack/{$cloud_context}/security_group");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($security_group->getName());
      $this->assertSession()->pageTextContains($security_group->getGroupId());
      $this->assertSession()->pageTextContains($add[0]['security_groups[]']);

      // Setup cloud launch template and instance.
      $server_template = $this->createOpenStackLaunchTemplateTestEntity([], $image, $cloud_context);
      $server_template->set('field_openstack_security_group', $security_group->id());
      $server_template->save();

      // Make sure if the cloud_launch_template entity is created or not.
      $this->drupalGet("/clouds/design/server_template/{$cloud_context}");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($server_template->name->value);

      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/launch");

      if (empty($openstack_ec2_api)) {
        continue;
      }

      $this->submitForm(
        ['terminate' => '1', 'termination_protection' => '1'],
        $this->t('Launch')->render()
      );
      $this->assertSession()->pageTextContains(
        $this->t('"@name1" and "@name2" cannot be selected both. Unselect one of them.',
          [
            '@name1' => $this->t('Termination protection'),
            '@name2' => $this->t('Automatically terminate instance'),
          ]
        )
      );
    }
  }

  /**
   * Tests updating instances.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateInstanceList(): void {
    $cloud_context = $this->cloudContext;
    $regions = ['RegionOne'];

    // Create image.
    $add = $this->createOpenStackInstanceTestFormData(self::OPENSTACK_INSTANCE_REPEAT_COUNT);
    $image = $this->createImageTestEntity(OpenStackImage::class, 0, $add[0]['image_id'], $cloud_context);

    // Make sure if the image entity is created or not.
    $this->drupalGet("/clouds/openstack/{$cloud_context}/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($image->getName());
    $this->assertSession()->pageTextContains($image->getImageId());
    $this->assertSession()->pageTextContains($add[0]['image_id']);

    // Setup cloud launch template and instance.
    $server_template = $this->createOpenStackLaunchTemplateTestEntity([], $image, $cloud_context);

    // Make sure if the cloud_launch_template entity is created or not.
    $this->drupalGet("/clouds/design/server_template/{$cloud_context}");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($server_template->name->value);

    // Create Instances in mock data.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {
      $instance_id = $this->addOpenStackInstanceMockData(
        OpenStackInstanceTest::class,
        $add[$i]['name'],
        $add[$i]['key_pair_name'],
        $regions,
        'running',
        '',
        $cloud_context
      );
      $add[$i]['instance_id'] = $instance_id;
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/instance");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated instances.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }
    // Make sure uid.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/edit");
      $this->assertSession()->linkNotExists($this->t('Start'));
      $this->assertSession()->linkExists($this->t('Stop'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/stop");
      $this->assertSession()->linkExists($this->t('Reboot'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/reboot");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/terminate");
      $this->assertSession()->linkExists($this->t('List OpenStack instances'));
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack instances'));
      $this->assertNoErrorMessage();

      // Confirm the edit view.
      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num/edit");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/edit");
      $this->assertSession()->linkNotExists($this->t('Start'));
      $this->assertSession()->linkExists($this->t('Stop'));
      $this->assertSession()->linkExists($this->t('Reboot'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/reboot");
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/stop");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/terminate");
    }

    // Edit instance information.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {

      $this->createElasticIpTestEntity($i);

      // Change instance name in mock data.
      $add[$i]['name'] = sprintf('instance-entity #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(8, TRUE));
      $this->updateInstanceMockData(OpenStackInstanceTest::class, $i, $add[$i]['name'], $regions, 'stopped', $cloud_context);

    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/instance");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated instances.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure uid.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/edit");
      $this->assertSession()->linkExists($this->t('Start'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/start");
      $this->assertSession()->linkNotExists($this->t('Stop'));
      $this->assertSession()->linkNotExists($this->t('Reboot'));
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/terminate");

      // Confirm the edit view.
      $this->drupalGet("/clouds/openstack/$cloud_context/instance/$num/edit");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/edit");
      $this->assertSession()->linkExists($this->t('Start'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/start");
      $this->assertSession()->linkNotExists($this->t('Stop'));
      $this->assertSession()->linkNotExists($this->t('Reboot'));
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/instance/$num/terminate");
    }

    // Update tags for empty.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateInstanceTagsInMockData($i, 'Name', '', FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/instance");
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['instance_id']);
    }

    // Delete name tags.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {

      // Update tags.
      $this->updateInstanceTagsInMockData($i, 'Name', '', TRUE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/instance");
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['instance_id']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['name']);
      $this->assertSession()->linkExists($add[$i]['instance_id']);
    }

    // Update tags.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {

      $this->updateInstanceTagsInMockData($i, 'Name', $add[$i]['name'], FALSE);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/instance");
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkNotExists($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->linkExists($add[$i]['name']);
    }

    // Delete Instances in mock data.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->deleteFirstInstanceMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/instance");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated instances.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests all updating instances.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllInstanceList(): void {
    $regions = ['RegionOne'];
    $cloud_configs = [];

    // List instance for OpenStack.
    $this->drupalGet('/clouds/openstack/instance');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      // Launch a new instance.
      $add = $this->createOpenStackInstanceTestFormData(self::OPENSTACK_INSTANCE_REPEAT_COUNT);
      for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {

        // Create image.
        $image = $this->createImageTestEntity(OpenStackImage::class, $i, $add[$i]['image_id'], $cloud_context);

        // Make sure if the image entity is created or not.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/image");
        $this->assertNoErrorMessage();
        $this->assertSession()->pageTextContains($image->getName());
        $this->assertSession()->pageTextContains($image->getImageId());
        $this->assertSession()->pageTextContains($add[$i]['image_id']);

        // Create security group.
        $security_group = $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, 0, $add[0]['security_groups[]'], '', '', $cloud_context);

        // Make sure if the security group entity is created or not.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/security_group");
        $this->assertNoErrorMessage();
        $this->assertSession()->pageTextContains($security_group->getName());
        $this->assertSession()->pageTextContains($security_group->getGroupId());
        $this->assertSession()->pageTextContains($add[0]['security_groups[]']);

        // Setup cloud launch template and instance.
        $server_template = $this->createOpenStackLaunchTemplateTestEntity([], $image, $cloud_context);
        $server_template->set('field_openstack_security_group', $security_group->id());
        $server_template->save();

        // Make sure if the cloud_launch_template entity is created or not.
        $this->drupalGet("/clouds/design/server_template/{$cloud_context}");
        $this->assertNoErrorMessage();
        $this->assertSession()->pageTextContains($server_template->name->value);

        $this->addOpenStackInstanceMockData(
          OpenStackInstanceTest::class,
          $add[$i]['name'],
          $add[$i]['key_pair_name'],
          $regions,
          'running',
          '',
          $cloud_context
        );
        $this->drupalGet("/clouds/design/server_template/{$cloud_context}/{$server_template->id()}/launch");
        $this->submitForm(
          [],
          $this->t('Launch')->render()
        );
        $this->assertNoErrorMessage();

        // Make sure listing.
        for ($j = 0; $j < $num; $j++) {
          $this->assertSession()->pageTextContains($add[$j]['name']);
        }

        // Create Instances in mock data.
        for ($k = 0; $k < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $k++) {
          $instance_id = $this->addOpenStackInstanceMockData(
            OpenStackInstanceTest::class,
            $add[$k]['name'],
            $add[$k]['key_pair_name'],
            $regions,
            'running',
            '',
            $cloud_context
          );
          $add[$k]['instance_id'] = $instance_id;
        }
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/instance');
    $this->assertNoErrorMessage();

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm Instances are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Instances',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/instance/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/instance/{$num}/edit");
        $this->assertSession()->linkNotExists($this->t('Start'));
        $this->assertSession()->linkExists($this->t('Stop'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/instance/{$num}/stop");
        $this->assertSession()->linkExists($this->t('Reboot'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/instance/{$num}/reboot");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/instance/{$num}/terminate");
        $this->assertSession()->linkExists($this->t('List OpenStack instances'));
        // Click 'List OpenStack instances'.
        $this->clickLink($this->t('List OpenStack instances'));
        $this->assertNoErrorMessage();

        // Confirm the edit view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/instance/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/instance/{$num}/edit");
        $this->assertSession()->linkNotExists($this->t('Start'));
        $this->assertSession()->linkExists($this->t('Stop'));
        $this->assertSession()->linkExists($this->t('Reboot'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/instance/{$num}/reboot");
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/instance/{$num}/stop");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/instance/{$num}/terminate");
      }
    }

    // Edit instance information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0, $num = 1; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++, $num++) {

        $this->createElasticIpTestEntity($i);

        // Change instance name in mock data.
        $add[$i]['name'] = sprintf('instance-entity #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(8, TRUE));
        $this->updateInstanceMockData(OpenStackInstanceTest::class, $i, $add[$i]['name'], $regions, 'stopped', $cloud_context);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/instance');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Instances are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Instances',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete Instances in mock data.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->deleteFirstInstanceMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/instance');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));

    // Confirm Instances are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Instances',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_INSTANCE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

}
