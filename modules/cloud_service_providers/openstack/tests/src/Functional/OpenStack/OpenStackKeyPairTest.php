<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackKeyPair;

/**
 * Tests OpenStack key pair.
 *
 * @group OpenStack
 */
class OpenStackKeyPairTest extends OpenStackTestBase {

  public const OPENSTACK_KEY_PAIR_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack key pair',
      'add openstack key pair',
      'view any openstack key pair',
      'delete any openstack key pair',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    $key_fingerprint_parts = [];
    for ($i = 0; $i < 20; $i++) {
      $key_fingerprint_parts[] = sprintf('%02x', random_int(0, 255));
    }

    $key_material = '---- BEGIN RSA PRIVATE KEY ----'
      . $this->random->name(871, TRUE)
      . '-----END RSA PRIVATE KEY-----';
    return [
      'key_name' => $this->random->name(15, TRUE),
      'key_fingerprint' => implode(':', $key_fingerprint_parts),
      'key_material' => $key_material,
      'key_id' => $this->getRandomId(),
    ];
  }

  /**
   * Tests CRUD for key pair information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testOpenStackKeyPair(): void {
    $cloud_context = $this->cloudContext;

    // List key pair for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/key_pair");
    $this->assertNoErrorMessage();

    // Add a new key pair.
    $add = $this->createKeyPairTestFormData(self::OPENSTACK_KEY_PAIR_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {
      $this->reloadMockData();

      $this->drupalGet("/clouds/openstack/$cloud_context/key_pair/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );

      $t_args = ['@type' => 'Key pair', '%label' => $add[$i]['key_pair_name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/key_pair");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $i + 1; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['key_pair_name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/key_pair/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all key_pair listing exists.
      $this->drupalGet('/clouds/openstack/key_pair');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['key_pair_name']);
      }
    }

    // Delete key pair.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/openstack/$cloud_context/key_pair/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/key_pair/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);

      $t_args = ['@type' => 'Key pair', '@label' => $add[$i]['key_pair_name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/key_pair");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
      }
    }
  }

  /**
   * Tests deleting key pairs with bulk operation.
   *
   * @throws \Exception
   */
  public function testOpenStackKeyPairBulk(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      // Create key pairs.
      $key_pairs = $this->createKeyPairsRandomTestFormData();
      $index = 1;
      $entities = [];
      foreach ($key_pairs ?: [] as $key_pair) {
        $entities[] = $this->createKeyPairTestEntity(OpenStackKeyPair::class, $index++, $key_pair['Name'], $key_pair['KeyFingerprint'], $cloud_context);
      }

      // The first parameter type should be 'key_pair' in OpenStack.
      $this->runTestEntityBulk('key_pair', $entities);
    }
  }

  /**
   * Test updating key pair list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateOpenStackKeyPairList(): void {

    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;
    $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
      'openstack',
      $cloud_context
    );

    // Add a new key pair.
    $add = $this->createKeyPairTestFormData(self::OPENSTACK_KEY_PAIR_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->addKeyPairMockData($add[$i]['key_pair_name'], $tag_created_uid, $cloud_context, $this->webUser->id(), $openstack_ec2_api);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/key_pair");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated key pairs.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/key_pair/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/key_pair/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/key_pair/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack key pairs'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack key pairs'));
      $this->assertNoErrorMessage();

      // Confirm the edit view.
      $this->drupalGet("/clouds/openstack/$cloud_context/key_pair/$num/edit");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/key_pair/$num/delete");
    }

    // Edit key pair information.
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {

      // Change key pair name in mock data.
      $add[$i]['key_pair_name'] = $this->random->name(15, TRUE);
      $this->updateKeyPairMockData($i, $add[$i]['key_pair_name']);

    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/key_pair");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated key pairs.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Delete key pair in mock data.
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->deleteFirstKeyPairMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/key_pair");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated key pairs.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }
  }

  /**
   * Test updating all key pair list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllKeyPairList(): void {
    $cloud_configs = [];

    // List KeyPair for OpenStack.
    $this->drupalGet('/clouds/openstack/key_pair');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $openstack_ec2_api = $cloud_config->get('field_use_openstack_ec2_api')->value;
      $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
        'openstack',
        $cloud_context
      );
      // Add a new key pair.
      $add = $this->createKeyPairTestFormData(self::OPENSTACK_KEY_PAIR_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
        $this->addKeyPairMockData($add[$i]['key_pair_name'], $tag_created_uid, $cloud_context, $this->webUser->id(), $openstack_ec2_api);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/key_pair');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Key pairs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/key_pair/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/key_pair/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/key_pair/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack key pairs'));
        // Click 'List OpenStack key pairs'.
        $this->clickLink($this->t('List OpenStack key pairs'));
        $this->assertNoErrorMessage();

        // Make sure uid.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/key_pair/{$num}");
        $this->assertSession()->pageTextContains($this->webUser->getAccountName());

        // Confirm the edit view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/key_pair/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/key_pair/{$num}/delete");
      }
    }

    // Edit key pair information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++, $num++) {
        // Change key pair name in mock data.
        $add[$i]['key_pair_name'] = $this->random->name(15, TRUE);
        $this->updateKeyPairMockData($i, $add[$i]['key_pair_name']);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/key_pair');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Key pairs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Delete key pair in mock data.
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->deleteFirstKeyPairMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/key_pair');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['key_pair_name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Key pairs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_KEY_PAIR_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['key_pair_name']);
    }
  }

}
