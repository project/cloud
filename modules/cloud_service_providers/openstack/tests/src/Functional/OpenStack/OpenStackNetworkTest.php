<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackAvailabilityZone;
use Drupal\openstack\Entity\OpenStackNetwork;

/**
 * Tests OpenStack network.
 *
 * @group OpenStack
 */
class OpenStackNetworkTest extends OpenStackTestBase {

  public const OPENSTACK_NETWORK_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack network',
      'add openstack network',
      'view any openstack network',
      'edit any openstack network',
      'delete any openstack network',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'network_id' => 'eni-' . $this->getRandomId(),
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for network information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testNetwork(): void {
    $cloud_context = $this->cloudContext;

    // List network for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/network");
    $this->assertNoErrorMessage();

    // Add a new network.
    $add = $this->createNetworkTestFormData(self::OPENSTACK_NETWORK_REPEAT_COUNT);

    // Create availability zones.
    $this->createAvailabilityZoneTestEntity(OpenStackAvailabilityZone::class, 0, $cloud_context, '', $add[0]['availability_zones[]'], 'network', 'network');

    for ($i = 0, $num = 1; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++, $num++) {
      $this->addNetworkMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/openstack/$cloud_context/network/add");
      $this->assertSession()->pageTextContains($this->t('Add OpenStack network'));
      $this->assertSession()->pageTextContains($add[0]['availability_zones[]']);
      $this->assertSession()->pageTextContains($this->t('External Network'));
      $this->assertNoErrorMessage();
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Network', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/network/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/network");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/network/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all network listing exists.
      $this->drupalGet('/clouds/openstack/network');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit a network information.
    $edit = $this->createNetworkTestFormData(self::OPENSTACK_NETWORK_REPEAT_COUNT, TRUE);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++, $num++) {
      $this->updateNetworkMockData($i, $edit[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/network/$num/edit");

      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Network', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/network");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/network");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/network/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Delete network.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/network/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/network/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Network', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/network");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests deleting networks with bulk operation.
   *
   * @throws \Exception
   */
  public function testNetworkBulk(): void {
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      // Create network.
      $networks = $this->createNetworksRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($networks ?: [] as $network) {
        $entities[] = $this->createNetworkTestEntity(OpenStackNetwork::class, $index++, $this->cloudContext, $network['Name']);
      }

      // The first parameter type should be 'network' in OpenStack.
      $this->runTestEntityBulk('network', $entities);
    }
  }

  /**
   * Test updating network.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateNetworkList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new network.
    $add = $this->createNetworkTestFormData(self::OPENSTACK_NETWORK_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addNetworkMockData($add[$i], $cloud_context, $this->webUser->id());
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/network");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated networks.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/network/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/network/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/network/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack networks'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack networks'));
      $this->assertNoErrorMessage();
    }

    // Edit network information.
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++, $num++) {

      // Change network name in mock data.
      $add[$i]['name'] = 'eni-' . $this->getRandomId();
      $this->updateNetworkMockData($i, $add[$i]['name']);

    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/network");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated networks.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete network in mock data.
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->deleteFirstNetworkMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/network");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated networks.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Test updating all network.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllNetworkList(): void {
    $cloud_configs = [];

    // List network for OpenStack.
    $this->drupalGet('/clouds/openstack/network');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new network.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createNetworkTestFormData(self::OPENSTACK_NETWORK_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addNetworkMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/network');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Networks',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/network/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/network/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/network/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack networks'));
        // Click 'List OpenStack networks'.
        $this->clickLink($this->t('List OpenStack networks'));
        $this->assertNoErrorMessage();
      }
    }

    // Edit Network information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++, $num++) {
        // Change network name in mock data.
        $add[$i]['name'] = "eni-{$this->getRandomId()}";
        $this->updateNetworkMockData($i, $add[$i]['name']);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/network');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Networks',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete network in mock data.
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->deleteFirstNetworkMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/network');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Networks',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_NETWORK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack network uses REST only.
    return FALSE;
  }

}
