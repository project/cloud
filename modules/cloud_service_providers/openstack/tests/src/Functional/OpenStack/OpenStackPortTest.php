<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackNetwork;
use Drupal\openstack\Entity\OpenStackPort;
use Drupal\openstack\Entity\OpenStackSubnet;

/**
 * Tests OpenStack port.
 *
 * @group OpenStack
 */
class OpenStackPortTest extends OpenStackTestBase {

  public const OPENSTACK_PORT_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack port',
      'add openstack port',
      'view any openstack port',
      'edit any openstack port',
      'delete any openstack port',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'port_id' => 'port-' . $this->getRandomId(),
      'network_id' => 'network-' . $this->getRandomId(),
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for port information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testPort(): void {
    $cloud_context = $this->cloudContext;

    // List port for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/port");
    $this->assertNoErrorMessage();

    // Create random networks.
    $networks = $this->createNetworksRandomTestFormData();
    $this->updateDescribeNetworksMockData($networks);

    // Create the network entities.
    $i = 1;
    $subnets = [];
    foreach ($networks ?: [] as $network) {
      $this->createNetworkTestEntity(OpenStackNetwork::class, $i++, $cloud_context, $network['Name'], $network['NetworkId']);

      // Create random subnets.
      $subnets = $this->createopenstackSubnetsRandomTestFormData();
      $this->updateDescribeSubnetsMockData($subnets);
      $subnet_index = 1;
      foreach ($subnets ?: [] as $subnet) {
        $this->createOpenStackSubnetTestEntity(OpenStackSubnet::class, $subnet_index++, $cloud_context, $subnet['Name'], $subnet['SubnetId'], $network['NetworkId']);

        $subnet_index++;
      }

      $i++;
    }

    // Add a new port.
    $add = $this->createPortTestFormData(self::OPENSTACK_PORT_REPEAT_COUNT);

    for ($i = 0, $num = 1; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++, $num++) {
      $this->addPortMockData($add[$i], $cloud_context, $this->webUser->id());

      // Set network ID.
      $add[$i]['network_id'] = $networks[array_rand($networks)]['NetworkId'];
      // Set subnet ID.
      $add[$i]['subnet'] = $subnets[array_rand($subnets)]['SubnetId'];

      $this->drupalGet("/clouds/openstack/$cloud_context/port/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Port', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/port/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/port");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/port/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all port listing exists.
      $this->drupalGet('/clouds/openstack/port');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit port information.
    $edit = $this->createPortTestFormData(self::OPENSTACK_PORT_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++, $num++) {
      unset(
        $edit[$i]['network_id'],
        $edit[$i]['subnet'],
        $edit[$i]['fixed_ips'],
        $edit[$i]['device_id'],
        $edit[$i]['device_owner'],
        $edit[$i]['ip_address_or_subnet'],
      );
      $this->updatePortMockData($i, $edit[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/port/$num/edit");

      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Port', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/port");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/port");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/port/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Delete port.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/port/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/port/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Port', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/port");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests deleting ports with bulk operation.
   *
   * @throws \Exception
   */
  public function testPortBulk(): void {
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      // Create port.
      $ports = $this->createPortsRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($ports ?: [] as $port) {
        $entities[] = $this->createPortTestEntity(OpenStackPort::class, $index++, $this->cloudContext, $port['Name']);
      }

      // The first parameter type should be 'port' in OpenStack.
      $this->runTestEntityBulk('port', $entities);
    }
  }

  /**
   * Test updating port.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdatePortList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new port.
    $add = $this->createPortTestFormData(self::OPENSTACK_PORT_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addPortMockData($add[$i], $cloud_context, $this->webUser->id());
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/port");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated ports.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/port/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/port/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/port/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack ports'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack ports'));
      $this->assertNoErrorMessage();
    }

    // Edit port information.
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++, $num++) {

      // Change port name in mock data.
      $add[$i]['name'] = 'eni-' . $this->getRandomId();
      $this->updatePortMockData($i, $add[$i]['name']);

    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/port");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated ports.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete port in mock data.
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->deleteFirstPortMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/port");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated ports.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Test updating all ports.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllPortList(): void {
    $cloud_configs = [];

    // List port for OpenStack.
    $this->drupalGet('/clouds/openstack/port');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new port.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createPortTestFormData(self::OPENSTACK_PORT_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addPortMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/port');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Ports',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/port/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/port/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/port/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack ports'));
        // Click 'List OpenStack ports'.
        $this->clickLink($this->t('List OpenStack ports'));
        $this->assertNoErrorMessage();
      }
    }

    // Edit Port information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++, $num++) {
        // Change port name in mock data.
        $add[$i]['name'] = "eni-{$this->getRandomId()}";
        $this->updatePortMockData($i, $add[$i]['name']);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/port');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Ports',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete port in mock data.
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->deleteFirstPortMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/port');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Ports',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PORT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack port uses REST only.
    return FALSE;
  }

}
