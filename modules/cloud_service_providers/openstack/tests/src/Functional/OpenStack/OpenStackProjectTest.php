<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackProject;

/**
 * Tests OpenStack project.
 *
 * @group OpenStack
 */
class OpenStackProjectTest extends OpenStackTestBase {

  public const OPENSTACK_PROJECT_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack project',
      'add openstack project',
      'view openstack project',
      'edit openstack project',
      'delete openstack project',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for project information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testProject(): void {
    $cloud_context = $this->cloudContext;

    // List project for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/project");
    $this->assertNoErrorMessage();

    // Add a new project.
    $add = $this->createProjectTestFormData(self::OPENSTACK_PROJECT_REPEAT_COUNT);

    for ($i = 0, $num = 1; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++, $num++) {
      $this->addProjectMockData($add[$i], $cloud_context);
      $this->addProjectUserRoleMockData($add[$i], $cloud_context);

      $this->drupalGet("/clouds/openstack/$cloud_context/project/add");
      $this->assertSession()->pageTextContains($this->t('Add OpenStack project'));
      $this->assertNoErrorMessage();
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Project', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/project/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/project");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all project listing exists.
      $this->drupalGet('/clouds/openstack/project');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit a project information.
    $edit = $this->createProjectTestFormData(self::OPENSTACK_PROJECT_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++, $num++) {
      $this->updateProjectMockData($i, $edit[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/project/$num/edit");

      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Project', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/project");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/project");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }
    }

    // Delete project.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/project/$num/delete");
      $this->assertNoErrorMessage();
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Project', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/project");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests deleting projects with bulk operation.
   *
   * @throws \Exception
   */
  public function testProjectBulk(): void {
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      // Create project.
      $projects = $this->createProjectsRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($projects ?: [] as $project) {
        $entities[] = $this->createProjectTestEntity(OpenStackProject::class, $index++, $this->cloudContext, $project['Name']);
      }

      // The first parameter type should be 'project' in OpenStack.
      $this->runTestEntityBulk('project', $entities);
    }
  }

  /**
   * Test updating project.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateProjectList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new project.
    $add = $this->createProjectTestFormData(self::OPENSTACK_PROJECT_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addProjectMockData($add[$i], $cloud_context);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/project");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated projects.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/project/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/project/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/project/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack projects'));
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack projects'));
      $this->assertNoErrorMessage();
    }

    // Edit project information.
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++, $num++) {
      // Change project name in mock data.
      $add[$i]['name'] = 'project-' . $this->getRandomId();
      $this->updateProjectMockData($i, $add[$i]['name']);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/project");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated projects.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete project in mock data.
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->deleteFirstProjectMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/project");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated projects.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test updating all project.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllProjectList(): void {
    $cloud_configs = [];

    // List project for OpenStack.
    $this->drupalGet('/clouds/openstack/project');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new project.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createProjectTestFormData(self::OPENSTACK_PROJECT_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addProjectMockData($add[$i], $cloud_context);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/project');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Projects',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/project/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/project/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/project/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack projects'));
        // Click 'List OpenStack projects'.
        $this->clickLink($this->t('List OpenStack projects'));
        $this->assertNoErrorMessage();
      }
    }

    // Edit Project information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++, $num++) {
        // Change project name in mock data.
        $add[$i]['name'] = "project-{$this->getRandomId()}";
        $this->updateProjectMockData($i, $add[$i]['name']);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/project');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Projects',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete project in mock data.
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->deleteFirstProjectMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/project');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Projects',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_PROJECT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack project uses REST only.
    return FALSE;
  }

}
