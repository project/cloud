<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;

/**
 * Tests OpenStack quota.
 *
 * @group OpenStack
 */
class OpenStackQuotaTest extends OpenStackTestBase {

  public const OPENSTACK_QUOTA_REPEAT_COUNT = 1;

  public const OPENSTACK_QUOTA_CLOUD_CONFIG_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack quota',
      'view openstack quota',
      'edit openstack quota',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for quota information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testQuota(): void {
    $cloud_context = $this->cloudContext;

    // List quota for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/quota");
    $this->assertNoErrorMessage();

    // Add a new quota.
    $add = $this->createQuotaTestFormData(self::OPENSTACK_QUOTA_REPEAT_COUNT);

    for ($i = 0, $num = 1; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++, $num++) {
      $this->addQuotaMockData($add[$i], $cloud_context, $this->webUser->id());
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated quotas.'));
    $this->assertNoErrorMessage();

    for ($i = 0, $num = 1; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++, $num++) {
      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/quota/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/quota");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all quota listing exists.
      $this->drupalGet('/clouds/openstack/quota');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit a quota information.
    $edit = $this->createQuotaTestFormData(self::OPENSTACK_QUOTA_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++, $num++) {
      $this->updateQuotaMockData($i, $cloud_context, $this->webUser->id());
      $this->drupalGet("/clouds/openstack/$cloud_context/quota/$num/edit");

      unset(
        $edit[$i]['name'],
        $edit[$i]['instances'],
        $edit[$i]['cores'],
        $edit[$i]['ram'],
        $edit[$i]['metadata_items'],
        $edit[$i]['key_pairs'],
        $edit[$i]['server_groups'],
        $edit[$i]['server_group_members'],
        $edit[$i]['injected_files'],
        $edit[$i]['injected_file_content_bytes'],
        $edit[$i]['injected_file_path_bytes'],
        $edit[$i]['volumes'],
        $edit[$i]['snapshots'],
        $edit[$i]['gigabytes'],
        $edit[$i]['network'],
        $edit[$i]['subnet'],
        $edit[$i]['port'],
        $edit[$i]['router'],
        $edit[$i]['floatingip'],
        $edit[$i]['security_group'],
        $edit[$i]['security_group_rule'],
      );

      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Quota', '%label' => 'quota'];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/quota");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains('quota');

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/quota");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Test updating quota.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateQuotaList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new quota.
    $add = $this->createQuotaTestFormData(self::OPENSTACK_QUOTA_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addQuotaMockData($add[$i], $cloud_context, $this->webUser->id());
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/quota");

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated quotas.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/quota/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/quota/$num/edit");
      $this->assertSession()->linkExists($this->t('List OpenStack quotas'));
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack quotas'));
      $this->assertNoErrorMessage();
    }

    // Edit quota information.
    for ($i = 0; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++, $num++) {

      // Change quota instances in mock data.
      $add[$i]['instances'] = random_int(1, 10);
      $this->updateQuotaMockData($i, $add[$i]['instances']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated quotas.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }
  }

  /**
   * Test updating all quota.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllQuotaList(): void {
    $cloud_configs = [];

    // List quota for OpenStack.
    $this->drupalGet('/clouds/openstack/quota');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_QUOTA_CLOUD_CONFIG_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new quota.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createQuotaTestFormData(self::OPENSTACK_QUOTA_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addQuotaMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // Click 'Refresh'.
    $this->drupalGet('/clouds/openstack/quota');
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Quotas',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/quota/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/quota/{$num}/edit");
        $this->assertSession()->linkExists($this->t('List OpenStack quotas'));
        // Click 'List OpenStack quotas'.
        $this->clickLink($this->t('List OpenStack quotas'));
        $this->assertNoErrorMessage();
      }
    }

    // Edit Quota information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++, $num++) {
        // Change quota instances in mock data.
        $add[$i]['instances'] = random_int(1, 10);
        $this->updateQuotaMockData($i, $add[$i]['instances']);
      }
    }

    // Click 'Refresh'.
    $this->drupalGet('/clouds/openstack/quota');
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Quotas',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_QUOTA_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Quotas',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }
  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack quota uses REST only.
    return FALSE;
  }

}
