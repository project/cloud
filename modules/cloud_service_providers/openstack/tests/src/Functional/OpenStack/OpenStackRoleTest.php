<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackRole;

/**
 * Tests OpenStack role.
 *
 * @group OpenStack
 */
class OpenStackRoleTest extends OpenStackTestBase {

  public const OPENSTACK_ROLE_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack role',
      'add openstack role',
      'view openstack role',
      'edit openstack role',
      'delete openstack role',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for role information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testRole(): void {
    $cloud_context = $this->cloudContext;

    // List role for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/role");
    $this->assertNoErrorMessage();

    // Add a new role.
    $add = $this->createRoleTestFormData(self::OPENSTACK_ROLE_REPEAT_COUNT);

    for ($i = 0, $num = 1; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++, $num++) {
      $this->addRoleMockData($add[$i], $cloud_context);

      $this->drupalGet("/clouds/openstack/$cloud_context/role/add");
      $this->assertSession()->pageTextContains($this->t('Add OpenStack role'));
      $this->assertNoErrorMessage();
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Role', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/role/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/role");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all role listing exists.
      $this->drupalGet('/clouds/openstack/role');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit a role information.
    $edit = $this->createRoleTestFormData(self::OPENSTACK_ROLE_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++, $num++) {
      $this->updateRoleMockData($i, $edit[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/role/$num/edit");

      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Role', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/role");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/role");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }
    }

    // Delete role.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/role/$num/delete");
      $this->assertNoErrorMessage();
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Role', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/role");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests deleting roles with bulk operation.
   *
   * @throws \Exception
   */
  public function testRoleBulk(): void {
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      // Create role.
      $roles = $this->createRolesRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($roles ?: [] as $role) {
        $entities[] = $this->createRoleTestEntity(OpenStackRole::class, $index++, $this->cloudContext, $role['Name']);
      }

      // The first parameter type should be 'role' in OpenStack.
      $this->runTestEntityBulk('role', $entities);
    }
  }

  /**
   * Test updating role.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateRoleList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new role.
    $add = $this->createRoleTestFormData(self::OPENSTACK_ROLE_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addRoleMockData($add[$i], $cloud_context);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/role");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated roles.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/role/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/role/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/role/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack roles'));
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack roles'));
      $this->assertNoErrorMessage();
    }

    // Edit role information.
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++, $num++) {
      // Change role name in mock data.
      $add[$i]['name'] = 'role-' . $this->getRandomId();
      $this->updateRoleMockData($i, $add[$i]['name']);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/role");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated roles.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete role in mock data.
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->deleteFirstRoleMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/role");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated roles.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test updating all role.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllRoleList(): void {
    $cloud_configs = [];

    // List role for OpenStack.
    $this->drupalGet('/clouds/openstack/role');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new role.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createRoleTestFormData(self::OPENSTACK_ROLE_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addRoleMockData($add[$i], $cloud_context);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/role');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Roles',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/role/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/role/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/role/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack roles'));
        // Click 'List OpenStack roles'.
        $this->clickLink($this->t('List OpenStack roles'));
        $this->assertNoErrorMessage();
      }
    }

    // Edit Role information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++, $num++) {
        // Change role name in mock data.
        $add[$i]['name'] = "role-{$this->getRandomId()}";
        $this->updateRoleMockData($i, $add[$i]['name']);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/role');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Roles',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete role in mock data.
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->deleteFirstRoleMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/role');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Roles',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROLE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack role uses REST only.
    return FALSE;
  }

}
