<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackAvailabilityZone;
use Drupal\openstack\Entity\OpenStackNetwork;
use Drupal\openstack\Entity\OpenStackRouter;

/**
 * Tests OpenStack router.
 *
 * @group OpenStack
 */
class OpenStackRouterTest extends OpenStackTestBase {

  public const OPENSTACK_ROUTER_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack router',
      'add openstack router',
      'view any openstack router',
      'edit any openstack router',
      'delete any openstack router',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'router_id' => 'eni-' . $this->getRandomId(),
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for router information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testRouter(): void {
    $cloud_context = $this->cloudContext;

    // List router for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/router");
    $this->assertNoErrorMessage();

    // Create random networks.
    $networks = $this->createNetworksRandomTestFormData();
    $this->updateDescribeNetworksMockData($networks);

    // Create the network entities.
    $i = 1;
    foreach ($networks ?: [] as $network) {
      $this->createNetworkTestEntity(OpenStackNetwork::class, $i++, $cloud_context, $network['Name'], $network['NetworkId'], '', '', '', TRUE);
      $i++;
    }

    // Add a new router.
    $add = $this->createRouterTestFormData(self::OPENSTACK_ROUTER_REPEAT_COUNT);

    // Create availability zones.
    $this->createAvailabilityZoneTestEntity(OpenStackAvailabilityZone::class, 0, $cloud_context, '', $add[0]['availability_zones[]'], 'network', 'router');

    for ($i = 0, $num = 1; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++, $num++) {
      $this->addRouterMockData($add[$i], $cloud_context, $this->webUser->id());

      // Set network ID.
      $add[$i]['external_gateway_network_id'] = $networks[array_rand($networks)]['NetworkId'];

      $this->drupalGet("/clouds/openstack/$cloud_context/router/add");
      $this->assertSession()->pageTextContains($add[0]['availability_zones[]']);
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Router', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/router/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/router");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/router/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all router listing exists.
      $this->drupalGet('/clouds/openstack/router');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit a router information.
    $edit = $this->createRouterTestFormData(self::OPENSTACK_ROUTER_REPEAT_COUNT, TRUE);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++, $num++) {
      $this->updateRouterMockData($i, $edit[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/router/$num/edit");

      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Router', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/router");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/router");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/router/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Delete router.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/router/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/router/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Router', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/router");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests deleting routers with bulk operation.
   *
   * @throws \Exception
   */
  public function testRouterBulk(): void {
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      // Create router.
      $routers = $this->createRoutersRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($routers ?: [] as $router) {
        $entities[] = $this->createRouterTestEntity(OpenStackRouter::class, $index++, $this->cloudContext, $router['Name']);
      }

      // The first parameter type should be 'router' in OpenStack.
      $this->runTestEntityBulk('router', $entities);
    }
  }

  /**
   * Test updating router.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateRouterList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new router.
    $add = $this->createRouterTestFormData(self::OPENSTACK_ROUTER_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addRouterMockData($add[$i], $cloud_context, $this->webUser->id());
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/router");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated routers.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/router/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/router/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/router/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack routers'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack routers'));
      $this->assertNoErrorMessage();
    }

    // Edit router information.
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++, $num++) {

      // Change router name in mock data.
      $add[$i]['name'] = 'eni-' . $this->getRandomId();
      $this->updateRouterMockData($i, $add[$i]['name']);

    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/router");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated routers.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete router in mock data.
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->deleteFirstRouterMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/router");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated routers.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Test updating all router.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllRouterList(): void {
    $cloud_configs = [];

    // List router for OpenStack.
    $this->drupalGet('/clouds/openstack/router');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new router.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createRouterTestFormData(self::OPENSTACK_ROUTER_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addRouterMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/router');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Routers',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/router/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/router/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/router/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack routers'));
        // Click 'List OpenStack routers'.
        $this->clickLink($this->t('List OpenStack routers'));
        $this->assertNoErrorMessage();
      }
    }

    // Edit Router information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++, $num++) {
        // Change router name in mock data.
        $add[$i]['name'] = "eni-{$this->getRandomId()}";
        $this->updateRouterMockData($i, $add[$i]['name']);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/router');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Routers',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete router in mock data.
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->deleteFirstRouterMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/router');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Routers',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_ROUTER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack router uses REST only.
    return FALSE;
  }

}
