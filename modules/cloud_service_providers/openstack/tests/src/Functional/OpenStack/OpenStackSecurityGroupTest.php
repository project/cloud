<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\cloud\Functional\Utils;
use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackSecurityGroup;

/**
 * Tests OpenStack security group.
 *
 * @group OpenStack
 */
class OpenStackSecurityGroupTest extends OpenStackTestBase {

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack security group',
      'add openstack security group',
      'view any openstack security group',
      'edit any openstack security group',
      'delete any openstack security group',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'vpc_id' => 'vpc-' . $this->getRandomId(),
      'cidr_block' => Utils::getRandomCidr(),
      'group_id' => 'sg-' . $this->getRandomId(),
      'group_name' => $this->random->name(8, TRUE),
    ];
  }

  /**
   * Tests CRUD for security group information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testSecurityGroup(): void {
    $cloud_context = $this->cloudContext;

    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    if ($openstack_ec2_api) {

      $this->deleteVpcMockData(0);

      // List security group for OpenStack.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group");
      $this->assertNoErrorMessage();

      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/add");
      $this->assertSession()->pageTextContains($this->t('You do not have any VPCs. You need a VPC in order to create a security group. You can create a VPC.'));
    }

    // Add a new security group.
    $add = $this->createSecurityGroupTestFormData(self::$openStackSecurityGroupRepeatCount);

    if ($openstack_ec2_api) {
      $addVpc = $this->createVpcTestFormData(self::$openStackSecurityGroupRepeatCount);
    }

    $this->updateDescribeSecurityGroupsMockData([]);
    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {

      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );

      if ($openstack_ec2_api) {
        $this->assertSession()->pageTextContains($this->t('VPC CIDR (ID) field is required.'));

        $defaults = $this->latestTemplateVars;
        $add[$i]['vpc_id'] = $defaults['vpc_id'];

        // Create VPC.
        $this->addVpcMockData($addVpc[$i], $add[$i]['vpc_id'], $cloud_context, $this->webUser->id());
      }

      $this->updateCreateSecurityGroupMockData();
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['group_name[0][value]']);

      $t_args = [
        '@type' => 'Security group',
        '%label' => $add[$i]['group_name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group");
      $this->assertNoErrorMessage();
      // 3 times.
      for ($j = 0; $j < $i + 1; $j++) {
        $this->assertSession()->pageTextContains($add[$i]['group_name[0][value]']);
      }
    }

    for ($i = 0, $num = 1; $i < self::$openStackSecurityGroupRepeatCount; $i++, $num++) {
      // Make sure the all security_group listing exists.
      $this->drupalGet('/clouds/openstack/security_group');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['group_name[0][value]']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Security group does not have an edit operation.
    // Edit a security group information.
    $edit = $this->createSecurityGroupTestFormData(self::$openStackSecurityGroupRepeatCount, TRUE);
    for ($i = 0, $num = 1; $i < self::$openStackSecurityGroupRepeatCount; $i++, $num++) {

      unset($edit[$i]['description']);

      // Initialize the mock data. Run security_group update so the data
      // gets imported.
      $this->reloadMockData();

      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Security group', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Delete security group.
    for ($i = 0, $num = 1; $i < self::$openStackSecurityGroupRepeatCount; $i++, $num++) {

      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Security group', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests deleting security groups with bulk operation.
   *
   * @throws \Exception
   */
  public function testSecurityGroupBulk(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
      // Create security groups.
      $security_groups = $this->createSecurityGroupRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($security_groups ?: [] as $security_group) {
        $entities[] = $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, $index++, $security_group['GroupId'], $security_group['Name'], '', $cloud_context);
      }

      $this->runTestEntityBulk('security_group', $entities);
    }
  }

  /**
   * Tests updating security groups.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateSecurityGroupList(): void {
    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;
    $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
      'openstack',
      $this->cloudContext
    );

    // Delete init mock data.
    $this->deleteFirstSecurityGroupMockData();

    // Add new security groups.
    $add = $this->createSecurityGroupTestFormData(self::$openStackSecurityGroupRepeatCount);
    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
      $this->addSecurityGroupMockData(
        $add[$i]['group_name[0][value]'],
        $add[$i]['description'],
        NULL,
        $tag_created_uid,
        $cloud_context,
        $this->webUser->id(),
        $openstack_ec2_api
      );
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/security_group");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['group_name[0][value]']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated security groups.'));
    // Make sure listing.
    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['group_name[0][value]']);
    }

    // Make sure detailed and edit view.
    $num = 1;
    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/security_group/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/security_group/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack security groups'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack security groups'));
      $this->assertNoErrorMessage();

      // Confirm the edit view.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/$num/edit");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/security_group/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/security_group/$num/delete");
      $this->assertSession()->linkExists('Edit');
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/security_group/$num/edit");
    }

    // Add a new security group.
    $num++;
    $data = [
      'description' => $this->getRandomSecurityGroupDescription(1, self::$awsCloudSecurityGroupDescriptionMaxLength, 'description-#$num - '),
      'group_name[0][value]' => "group-name-#$num - {$this->random->name(15, TRUE)}",
    ];
    $this->addSecurityGroupMockData(
      $data['group_name[0][value]'],
      $data['description'],
      NULL,
      $tag_created_uid,
      $cloud_context,
      $this->webUser->id(),
      $openstack_ec2_api);

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/security_group");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextNotContains($data['group_name[0][value]']);

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated security groups.'));
    $add = array_merge($add, [$data]);
    // Make sure listing.
    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount + 1; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['group_name[0][value]']);
    }

    // Make sure uid.
    for ($i = 0, $num = 1; $i < self::$openStackSecurityGroupRepeatCount; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    if ($openstack_ec2_api) {
      // Update tags.
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
        $add[$i]['tags_name'] = $this->getRandomId();
        $this->updateTagsInMockData($i, 'SecurityGroups', 'Name', $add[$i]['tags_name'], FALSE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group");
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
        $this->assertSession()->linkExists($add[$i]['tags_name']);
      }

      // Update tags for empty.
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {

        // Update tags.
        $this->updateTagsInMockData($i, 'SecurityGroups', 'Name', '', FALSE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group");
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
        $this->assertSession()->linkExists($add[$i]['tags_name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));

      // Make sure listing.
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['group_name[0][value]']);
      }

      // Delete name tags.
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {

        // Update tags.
        $this->updateTagsInMockData($i, 'SecurityGroups', 'Name', '', TRUE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/security_group");
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['group_name[0][value]']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['group_name[0][value]']);
      }
    }

    // Delete SecurityGroup in mock data.
    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount + 1; $i++) {
      $this->deleteFirstSecurityGroupMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/security_group");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount + 1; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['group_name[0][value]']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated security groups.'));
    // Make sure listing.
    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount + 1; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['group_name[0][value]']);
    }
  }

  /**
   * Tests updating all security groups.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllSecurityGroupList(): void {
    $cloud_configs = [];

    // Delete init mock data.
    $this->deleteFirstSecurityGroupMockData();

    // List security group for OpenStack.
    $this->drupalGet('/clouds/openstack/security_group');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add new security groups.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $openstack_ec2_api = $cloud_config->get('field_use_openstack_ec2_api')->value;
      $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
        'openstack',
        $cloud_context
      );
      $add = $this->createSecurityGroupTestFormData(self::$openStackSecurityGroupRepeatCount);
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
        $this->addSecurityGroupMockData(
          $add[$i]['group_name[0][value]'],
          $add[$i]['description'],
          NULL,
          $tag_created_uid,
          $cloud_context,
          $this->webUser->id(),
          $openstack_ec2_api
        );
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/security_group');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
      // @phpstan-ignore-next-line
      $this->assertSession()->pageTextNotContains($add[$i]['group_name[0][value]']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm security groups are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Security groups',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++) {
      // @phpstan-ignore-next-line
      $this->assertSession()->pageTextContains($add[$i]['group_name[0][value]']);
    }

    // Make sure detailed and edit view.
    $num = 1;
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $openstack_ec2_api = $cloud_config->get('field_use_openstack_ec2_api')->value;
      $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
        'openstack',
        $cloud_context
      );
      for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/security_group/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/security_group/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/security_group/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack security groups'));
        // Click 'List OpenStack security groups'.
        $this->clickLink($this->t('List OpenStack security groups'));
        $this->assertNoErrorMessage();

        // Confirm the edit view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/security_group/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/security_group/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/security_group/{$num}/delete");
        $this->assertSession()->linkExists('Edit');
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/security_group/{$num}/edit");
      }
    }

    // Add a new security group.
    $num++;
    $data = [
      'description' => $this->getRandomSecurityGroupDescription(1, self::$awsCloudSecurityGroupDescriptionMaxLength, 'description-#{$num} - '),
      'group_name[0][value]' => "group-name-#{$num} - {$this->random->name(15, TRUE)}",
    ];
    $this->addSecurityGroupMockData(
      $data['group_name[0][value]'],
      $data['description'],
      NULL,
      // @phpstan-ignore-next-line
      $tag_created_uid,
      // @phpstan-ignore-next-line
      $cloud_context,
      $this->webUser->id(),
      // @phpstan-ignore-next-line
      $openstack_ec2_api
    );

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/security_group');
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextNotContains($data['group_name[0][value]']);

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm security groups are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Security groups',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // @phpstan-ignore-next-line
    $add = array_merge($add, [$data]);
    // Make sure listing.
    for ($i = 0; $i < self::$openStackSecurityGroupRepeatCount + 1; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['group_name[0][value]']);
    }
  }

}
