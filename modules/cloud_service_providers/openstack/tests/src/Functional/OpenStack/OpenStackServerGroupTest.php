<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;

/**
 * Tests OpenStack server group.
 *
 * @group OpenStack
 */
class OpenStackServerGroupTest extends OpenStackTestBase {

  public const OPENSTACK_SERVER_GROUP_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack server group',
      'add openstack server group',
      'view openstack server group',
      'edit openstack server group',
      'delete openstack server group',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'server_group_id' => 'eni-' . $this->getRandomId(),
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for server group information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testServerGroup(): void {
    $cloud_context = $this->cloudContext;

    // List server group for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/server_group");
    $this->assertNoErrorMessage();

    // Add a new server group.
    $add = $this->createServerGroupTestFormData(self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT);

    for ($i = 0, $num = 1; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++, $num++) {
      $this->addServerGroupMockData($add[$i], $cloud_context, $this->webUser->id());

      $this->drupalGet("/clouds/openstack/$cloud_context/server_group/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Server group', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/server_group/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/server_group");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all server group listing exists.
      $this->drupalGet('/clouds/openstack/server_group');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Delete server group.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/server_group/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/server_group/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Server group', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/server_group");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Test updating server group.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateServerGroupList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new server group.
    $add = $this->createServerGroupTestFormData(self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addServerGroupMockData($add[$i], $cloud_context, $this->webUser->id());
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/server_group");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated server groups.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/server_group/$num");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/server_group/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack server groups'));
      // Click 'List OpenStack server groups'.
      $this->clickLink($this->t('List OpenStack server groups'));
      $this->assertNoErrorMessage();
    }

    // Delete server group in mock data.
    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->deleteFirstServerGroupMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/server_group");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated server groups.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Test updating all server group.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllServerGroupList(): void {
    $cloud_configs = [];

    // List server group for OpenStack.
    $this->drupalGet('/clouds/openstack/server_group');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new server group.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createServerGroupTestFormData(self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addServerGroupMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/server_group');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Server groups',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/server_group/{$num}");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/server_group/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack server groups'));
        // Click 'List OpenStack server groups'.
        $this->clickLink($this->t('List OpenStack server groups'));
        $this->assertNoErrorMessage();
      }
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete server group in mock data.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
        $this->deleteFirstServerGroupMockData();
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/server_group');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Server groups',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SERVER_GROUP_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack server group uses REST only.
    return FALSE;
  }

}
