<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\cloud\Functional\Utils;
use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackSnapshot;
use Drupal\openstack\Entity\OpenStackVolume;

/**
 * Tests OpenStack snapshot.
 *
 * @group OpenStack
 */
class OpenStackSnapshotTest extends OpenStackTestBase {

  public const OPENSTACK_SNAPSHOT_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack snapshot',
      'add openstack snapshot',
      'view any openstack snapshot',
      'edit any openstack snapshot',
      'delete any openstack snapshot',

      'view any openstack volume',
      'add openstack volume',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'snapshot_id' => 'snap-' . $this->getRandomId(),
      'cidr_block' => Utils::getRandomCidr(),
      'group_id' => 'sg-' . $this->getRandomId(),
      'start_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for snapshot information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testSnapshot(): void {
    $cloud_context = $this->cloudContext;

    // List the snapshots for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
    $this->assertNoErrorMessage();

    // Create random volumes.
    $volumes = $this->createVolumesRandomTestFormData();
    $this->updateDescribeVolumesMockData($volumes);

    // Create the volume entities.
    $v = 1;
    foreach ($volumes ?: [] as $volume) {
      $this->createVolumeTestEntity(
        OpenStackVolume::class,
        $v,
        $volume['VolumeId'],
        $volume['Name'],
        $cloud_context,
        Utils::getRandomUid()
      );
      $v++;
    }

    // Add a new snapshot.
    $add = $this->createSnapshotTestFormData(self::OPENSTACK_SNAPSHOT_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++, $num++) {
      $this->reloadMockData();

      // Set volume ID.
      $add[$i]['volume_id'] = $volumes[array_rand($volumes)]['VolumeId'];

      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains(
        $this->t('The snapshot @name', ['@name' => $add[$i]['name']]));
      $this->assertSession()->pageTextContains($add[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
      $this->assertNoErrorMessage();

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all snapshot listing exists.
      $this->drupalGet('/clouds/openstack/snapshot');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit a snapshot information.
    $edit = $this->createSnapshotTestFormData(self::OPENSTACK_SNAPSHOT_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++, $num++) {

      unset(
        $edit[$i]['volume_id'],
        $edit[$i]['description']
      );

      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Snapshot', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Delete a snapshot.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['name']);

      $t_args = ['@type' => 'Snapshot', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Test updating snapshots.
   */
  public function testUpdateSnapshot(): void {
    try {
      $this->repeatTestUpdateSnapshot(self::OPENSTACK_SNAPSHOT_REPEAT_COUNT);
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Repeating test updating snapshot.
   *
   * @param int $max_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  private function repeatTestUpdateSnapshot($max_count): void {
    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    for ($i = 0; $i < $max_count; $i++) {
      $test_cases = $this->createUpdateSnapshotTestCases();
      $this->updateDescribeSnapshotsMockData(
        $test_cases,
        $openstack_ec2_api,
        $cloud_context,
        $this->webUser->id()
      );
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot/update");
      $this->assertNoErrorMessage();

      foreach ($test_cases ?: [] as $test_case) {
        $this->assertSession()->linkExists(
          $test_case['name'] ?? $test_case['id']
        );
      }
    }
  }

  /**
   * Test updating snapshot list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateSnapshotList(): void {

    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;
    $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
      'openstack',
      $cloud_context
    );

    // Add a new snapshot.
    $add = $this->createSnapshotTestFormData(self::OPENSTACK_SNAPSHOT_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $add[$i]['snapshot_id'] = $this->addSnapshotMockData(
        $add[$i]['name'],
        $add[$i]['volume_id'],
        $add[$i]['description'],
        $tag_created_uid,
        $cloud_context,
        $this->webUser->id(),
        $openstack_ec2_api
      );
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated snapshots.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/snapshot/$num/edit");
      $this->assertSession()->linkExists($this->t('Create Volume'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/add?snapshot_id={$add[$i]['name']}");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/snapshot/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack snapshots'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack snapshots'));
      $this->assertNoErrorMessage();

      // Confirm the edit view.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot/$num/edit");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/snapshot/$num/edit");
      $this->assertSession()->linkExists($this->t('Create Volume'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/add?snapshot_id={$add[$i]['name']}");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/snapshot/$num/delete");
      $this->assertSession()->linkExists('Edit');
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/snapshot/$num/edit");

      // Click "Create Volume" link.
      $this->clickLink($this->t('Create Volume'));

      // Make sure creating page.
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('Add OpenStack volume'));

      // Make sure the default value of field snapshot_id.
      $this->assertSession()->fieldValueEquals('snapshot_id', $add[$i]['name']);
    }

    // Edit the snapshot information.
    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {

      // Change the snapshot name in mock data.
      $add[$i]['name'] = 'snap-' . $this->getRandomId();
      $this->updateSnapshotMockData($i, $add[$i]['name'], $openstack_ec2_api);

    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated snapshots.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    if ($openstack_ec2_api) {
      // Update tags for empty.
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {

        // Update tags.
        $this->updateTagsInMockData($i, 'Snapshots', 'Name', '', FALSE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkExists($add[$i]['name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['name']);
        $this->assertSession()->linkExists($add[$i]['snapshot_id']);
      }

      // Delete name tags.
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {

        // Update tags.
        $this->updateTagsInMockData($i, 'Snapshots', 'Name', '', TRUE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['name']);
        $this->assertSession()->linkExists($add[$i]['snapshot_id']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['name']);
        $this->assertSession()->linkExists($add[$i]['snapshot_id']);
      }

      // Update tags.
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {

        $add[$i]['name'] = 'snap-' . $this->getRandomId();
        $this->updateTagsInMockData($i, 'Snapshots', 'Name', $add[$i]['name'], FALSE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkExists($add[$i]['name']);
      }
    }

    // Delete a snapshot in mock data.
    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->deleteFirstSnapshotMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated snapshots.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test updating all snapshot list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllSnapshotList(): void {
    $cloud_configs = [];

    // List the snapshots for OpenStack.
    $this->drupalGet('/clouds/openstack/snapshot');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $openstack_ec2_api = $cloud_config->get('field_use_openstack_ec2_api')->value;
      $tag_created_uid = $this->cloudService->getTagKeyCreatedByUid(
        'openstack',
        $cloud_context
      );
      // Add a new snapshot.
      $add = $this->createSnapshotTestFormData(self::OPENSTACK_SNAPSHOT_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
        $add[$i]['snapshot_id'] = $this->addSnapshotMockData(
          $add[$i]['name'],
          $add[$i]['volume_id'],
          $add[$i]['description'],
          $tag_created_uid,
          $cloud_context,
          $this->webUser->id(),
          $openstack_ec2_api
        );
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/snapshot');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Snapshots',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/snapshot/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/snapshot/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/snapshot/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack snapshots'));
        // Click 'Refresh'.
        $this->clickLink($this->t('List OpenStack snapshots'));
        $this->assertNoErrorMessage();

        // Confirm the edit view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/snapshot/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/snapshot/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/snapshot/{$num}/delete");
        $this->assertSession()->linkExists('Edit');
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/snapshot/{$num}/edit");

        // Click "Create Volume" link.
        $this->clickLink($this->t('Create Volume'));

        // Make sure creating page.
        $this->assertNoErrorMessage();
        $this->assertSession()->pageTextContains($this->t('Add OpenStack volume'));
      }
    }

    // Edit the snapshot information.
    foreach ($cloud_configs ?: [] as $cloud_config) {

      $openstack_ec2_api = $cloud_config->get('field_use_openstack_ec2_api')->value;
      for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
        // Change a snapshot name in mock data.
        $add[$i]['name'] = "snap-{$this->getRandomId()}";
        $this->updateSnapshotMockData($i, $add[$i]['name'], $openstack_ec2_api);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/snapshot');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Snapshots',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Delete snapshot in mock data.
    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->deleteFirstSnapshotMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/snapshot');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Snapshots',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test the operation of creating volume.
   */
  public function testCreateVolumeOperation(): void {
    try {
      $this->repeatTestCreateVolumeOperation(
        self::OPENSTACK_SNAPSHOT_REPEAT_COUNT
      );
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Repeat testing the operation of creating volume.
   *
   * @param int $max_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  private function repeatTestCreateVolumeOperation($max_count): void {
    $cloud_context = $this->cloudContext;

    // Create random volumes.
    $volumes = $this->createVolumesRandomTestFormData();
    $this->updateDescribeVolumesMockData($volumes);

    // Create the volume entities.
    $v = 1;
    foreach ($volumes ?: [] as $volume) {
      $this->createVolumeTestEntity(
        OpenStackVolume::class,
        $v,
        $volume['VolumeId'],
        $volume['Name'],
        $cloud_context,
        Utils::getRandomUid()
      );
      $v++;
    }

    // Add a new snapshot.
    $add = $this->createSnapshotTestFormData(self::OPENSTACK_SNAPSHOT_REPEAT_COUNT);
    for ($i = 0; $i < $max_count; $i++) {
      $this->reloadMockData();

      // Set volume ID.
      $add[$i]['volume_id'] = $volumes[array_rand($volumes)]['VolumeId'];
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/snapshot");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('Create Volume'));

      // Add snapshot to DescribeSnapshots of Mock data.
      $snapshot_id = $this->latestTemplateVars['snapshot_id'];
      $this->addDescribeSnapshotsMockData($snapshot_id);

      // Click "Create Volume" link.
      $this->clickLink($this->t('Create Volume'), $i);

      // Make sure creating page.
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('Add OpenStack volume'));

      // Make sure the default value of field snapshot_id.
      $this->assertSession()->fieldValueEquals('snapshot_id', $snapshot_id);
    }
  }

  /**
   * Tests deleting snapshots with bulk operation.
   *
   * @throws \Exception
   */
  public function testSnapshotBulk(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0; $i < self::OPENSTACK_SNAPSHOT_REPEAT_COUNT; $i++) {
      // Create snapshots.
      $snapshots = $this->createSnapshotsRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($snapshots ?: [] as $snapshot) {
        $entities[] = $this->createSnapshotTestEntity(OpenStackSnapshot::class, $index++, $snapshot['SnapshotId'], $snapshot['Name'], $cloud_context);
      }

      $this->runTestEntityBulk('snapshot', $entities);
    }
  }

}
