<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Component\Serialization\Yaml;
use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackStack;
use Drupal\openstack\Entity\OpenStackStackEvent;
use Drupal\openstack\Entity\OpenStackStackResource;

/**
 * Tests OpenStack stack.
 *
 * @group OpenStack
 */
class OpenStackStackTest extends OpenStackTestBase {

  public const OPENSTACK_STACK_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack stack',
      'add openstack stack',
      'view any openstack stack',
      'edit any openstack stack',
      'delete any openstack stack',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'stack_id' => 'eni-' . $this->getRandomId(),
      'resource_id' => 'rni-' . $this->getRandomId(),
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for stack information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testStack(): void {
    $cloud_context = $this->cloudContext;

    // List stack for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/stack");
    $this->assertNoErrorMessage();

    // Add a new stack.
    $add = $this->createStackTestFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $add_extra = $this->createStackTestExtraFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $mock_data = array_merge_recursive($add[$i], $add_extra[$i]);
      $this->addStackMockData($mock_data, $cloud_context, $this->webUser->id());
      $this->showStackMockData($mock_data, $cloud_context, $this->webUser->id());
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/add");
      $this->submitForm(
        $add[$i],
        $this->t('Next')->render()
      );
      $this->assertNoErrorMessage();

      unset(
        $add_extra[$i]['stack_id']
      );

      $this->submitForm(
        $add_extra[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Stack', '%label' => $add_extra[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add_extra[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Make sure stack status.
      $this->assertSession()->pageTextContains('CREATE_COMPLETE');

      $template_data = Yaml::decode($add[$i]['template_data']);
      foreach ($template_data['resources'] ?: [] as $name => $resource) {
        $this->assertSession()->pageTextContains($name);
      }
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all stack listing exists.
      $this->drupalGet('/clouds/openstack/stack');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add_extra[$j]['name']);
      }
    }

    // Edit a stack information.
    $edit = $this->createStackTestFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $edit_extra = $this->createStackTestExtraFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $this->updateStackMockData($i);
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num/edit");

      $this->submitForm(
        $edit[$i],
        $this->t('Next')->render()
      );
      $this->assertNoErrorMessage();

      unset(
        $edit_extra[$i]['stack_id']
      );

      $this->submitForm(
        $edit_extra[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Stack', '%label' => $add_extra[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($add_extra[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertNoErrorMessage();

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());

      // Make sure stack status.
      $this->assertSession()->pageTextContains('UPDATE_COMPLETE');
    }

    // Check stack.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num/check");
      $this->assertNoErrorMessage();
      $this->checkStackMockData();
      $this->submitForm(
        [],
        $this->t('Check')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Stack', '@label' => $add_extra[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been checked.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertNoErrorMessage();
    }

    // Suspend stack.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num/suspend");
      $this->assertNoErrorMessage();
      $this->suspendStackMockData();
      $this->submitForm(
        [],
        $this->t('Suspend')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Stack', '@label' => $add_extra[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been suspended.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertNoErrorMessage();
    }

    // Resume stack.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num/resume");
      $this->assertNoErrorMessage();
      $this->resumeStackMockData();
      $this->submitForm(
        [],
        $this->t('Resume')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Stack', '@label' => $add_extra[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been resumed.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertNoErrorMessage();
    }

    // Delete stack.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Stack', '@label' => $add_extra[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests Read for stack resource information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testStackResource(): void {
    $cloud_context = $this->cloudContext;

    // List stack for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/stack");
    $this->assertNoErrorMessage();

    // Add a new stack.
    $add = $this->createStackTestFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $add_extra = $this->createStackTestExtraFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $add_resource = $this->createStackResourceTestFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $default = $this->getMockDataTemplateVars();
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $mock_data = array_merge_recursive($add[$i], $add_extra[$i]);
      $this->createStackResourceTestEntity(
        OpenStackStackResource::class,
        $cloud_context,
        $add_resource[$i]['resource_name'],
        $mock_data['stack_id'],
        $num,
        $default['project_id'],
        $add_resource[$i]['resource_id'],
        $add_resource[$i]['resource_status'],
        $add_resource[$i]['resource_status_reason'],
        $add_resource[$i]['resource_type'],
      );

      $this->drupalGet("/clouds/openstack/$cloud_context/stack/add");
      $this->submitForm(
        $add[$i],
        $this->t('Next')->render()
      );
      $this->assertNoErrorMessage();

      unset(
        $add_extra[$i]['stack_id']
      );

      $this->submitForm(
        $add_extra[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Stack', '%label' => $add_extra[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertNoErrorMessage();

      // Make sure stack resource.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num/resource/$num");
      $this->assertSession()->pageTextContains($add_resource[$i]['resource_id']);
      $this->assertSession()->pageTextContains($add_resource[$i]['resource_status']);
      $this->assertSession()->pageTextContains($add_resource[$i]['resource_status_reason']);
      $this->assertSession()->pageTextContains($add_resource[$i]['resource_type']);
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all stack listing exists.
      $this->drupalGet('/clouds/openstack/stack');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add_extra[$j]['name']);
      }
    }
  }

  /**
   * Tests Read for stack event information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testStackEvent(): void {
    $cloud_context = $this->cloudContext;

    // List stack for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/stack");
    $this->assertNoErrorMessage();

    // Add a new stack.
    $add = $this->createStackTestFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $add_extra = $this->createStackTestExtraFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $add_event = $this->createStackEventTestFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $default = $this->getMockDataTemplateVars();
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $mock_data = array_merge_recursive($add[$i], $add_extra[$i]);
      $this->createStackEventTestEntity(
        OpenStackStackEvent::class,
        $cloud_context,
        $add_event[$i]['name'],
        $mock_data['stack_id'],
        $num,
        $default['project_id'],
        $add_event[$i]['resource_id'],
        $add_event[$i]['resource_name'],
        $add_event[$i]['resource_status'],
        $add_event[$i]['resource_status_reason']
      );

      $this->drupalGet("/clouds/openstack/$cloud_context/stack/add");
      $this->submitForm(
        $add[$i],
        $this->t('Next')->render()
      );
      $this->assertNoErrorMessage();

      unset(
        $add_extra[$i]['stack_id']
      );

      $this->submitForm(
        $add_extra[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Stack', '%label' => $add_extra[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertNoErrorMessage();

      // Make sure stack resource.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num/event/$num");
      $this->assertSession()->pageTextContains($add_event[$i]['name']);
      $this->assertSession()->pageTextContains($add_event[$i]['resource_id']);
      $this->assertSession()->pageTextContains($add_event[$i]['resource_status']);
      $this->assertSession()->pageTextContains($add_event[$i]['resource_status_reason']);
      $this->assertSession()->pageTextContains($add_event[$i]['resource_name']);
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all stack listing exists.
      $this->drupalGet('/clouds/openstack/stack');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add_extra[$j]['name']);
      }
    }
  }

  /**
   * Tests CRUD for stack preview information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testStackPreview(): void {
    $cloud_context = $this->cloudContext;
    $default = $this->getMockDataTemplateVars();

    // List stack for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/stack");
    $this->assertNoErrorMessage();

    // Add a new stack.
    $add = $this->createStackTestFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $add_extra = $this->createStackTestExtraFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $preview = $this->previewStackTestFormData(self::OPENSTACK_STACK_REPEAT_COUNT, $default['project_id']);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $this->previewStackMockData($preview[$i], $cloud_context);
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/preview");
      $this->submitForm(
        $add[$i],
        $this->t('Next')->render()
      );
      $this->assertNoErrorMessage();

      unset(
        $add_extra[$i]['stack_id']
      );

      $this->submitForm(
        $add_extra[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Stack', '%label' => $add_extra[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been previewed.', $t_args)));

      // Make sure Resources.
      foreach ($preview[$i]['resources'] ?: [] as $name => $resource) {
        $this->assertSession()->pageTextContains($name);
        $this->assertSession()->pageTextContains(sprintf('%s: "%s"', $name, $resource));
      }
      // Make sure Parameters.
      foreach ($preview[$i]['parameters'] ?: [] as $name => $parameter) {
        $this->assertSession()->pageTextContains($name);
        $this->assertSession()->pageTextContains(sprintf('%s: "%s"', $name, $parameter));
      }
      // Make sure Links.
      $this->assertSession()->pageTextContains(sprintf('%s: "%s"', $preview[$i]['links']['rel'], $preview[$i]['links']['href']));
    }
  }

  /**
   * Tests deleting stacks with bulk operation.
   *
   * @throws \Exception
   */
  public function testStackBulk(): void {
    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      // Create stack.
      $stacks = $this->createStacksRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($stacks ?: [] as $stack) {
        $entities[] = $this->createStackTestEntity(OpenStackStack::class, $index++, $this->cloudContext, $stack['Name']);
      }

      // The first parameter type should be 'stack' in OpenStack.
      $this->runTestEntityBulk('stack', $entities);
    }
  }

  /**
   * Test updating stack.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateStackList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new stack.
    $add = $this->createStackTestFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    $add_extra = $this->createStackTestExtraFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $mock_data = array_merge_recursive($add[$i], $add_extra[$i]);
      $this->reloadMockData();

      $this->addStackMockData($mock_data, $cloud_context, $this->webUser->id());
      $this->showStackMockData($mock_data, $cloud_context, $this->webUser->id());
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/stack");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add_extra[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated stacks.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add_extra[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/stack/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/stack/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack stacks'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack stacks'));
      $this->assertNoErrorMessage();
    }

    // Edit stack information.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      // Change stack name in mock data.
      $this->updateStackMockData($i);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/stack");
    $this->assertNoErrorMessage();

    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertSession()->pageTextContains($add_extra[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num");
      $this->assertSession()->pageTextContains('CREATE_COMPLETE');
    }

    // Click 'Refresh'.
    $this->drupalGet("/clouds/openstack/$cloud_context/stack");
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated stacks.'));
    // Make sure listing.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/stack");
      $this->assertSession()->pageTextContains($add_extra[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num");
      $this->assertSession()->pageTextContains('UPDATE_COMPLETE');
    }

    // Delete stack in mock data.
    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $this->deleteFirstStackMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/stack");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add_extra[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated stacks.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add_extra[$i]['name']);
    }

  }

  /**
   * Test updating all stack.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllStackList(): void {
    $cloud_configs = [];

    // List stack for OpenStack.
    $this->drupalGet('/clouds/openstack/stack');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new stack.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createStackTestFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
      $add_extra = $this->createStackTestExtraFormData(self::OPENSTACK_STACK_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
        $mock_data = array_merge_recursive($add[$i], $add_extra[$i]);
        $this->reloadMockData();

        $this->addStackMockData($mock_data, $cloud_context, $this->webUser->id());
        $this->showStackMockData($mock_data, $cloud_context, $this->webUser->id());
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/stack');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add_extra[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Stacks',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add_extra[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/stack/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/stack/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/stack/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack stacks'));
        // Click 'List OpenStack stacks'.
        $this->clickLink($this->t('List OpenStack stacks'));
        $this->assertNoErrorMessage();
      }
    }

    // Edit Stack information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
        // Change stack name in mock data.
        $this->updateStackMockData($i);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/stack');
    $this->assertNoErrorMessage();

    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/stack");
      $this->assertSession()->pageTextContains($add_extra[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num");
      $this->assertSession()->pageTextContains('CREATE_COMPLETE');
    }

    // Click 'Refresh'.
    $this->drupalGet("/clouds/openstack/stack");
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Stacks',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/stack");
      $this->assertSession()->pageTextContains($add_extra[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/stack/$num");
      $this->assertSession()->pageTextContains('UPDATE_COMPLETE');
    }

    // Delete stack in mock data.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
        $this->deleteFirstStackMockData();
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/stack');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add_extra[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Stacks',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_STACK_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add_extra[$i]['name']);
    }

  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack Stack uses REST only.
    return FALSE;
  }

}
