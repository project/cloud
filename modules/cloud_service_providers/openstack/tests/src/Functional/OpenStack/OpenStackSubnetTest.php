<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackNetwork;
use Drupal\openstack\Entity\OpenStackSubnet;

/**
 * Tests OpenStack subnet.
 *
 * @group OpenStack
 */
class OpenStackSubnetTest extends OpenStackTestBase {

  public const OPENSTACK_SUBNET_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack subnet',
      'add openstack subnet',
      'view any openstack subnet',
      'edit any openstack subnet',
      'delete any openstack subnet',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'subnet_id' => 'subnet-' . $this->getRandomId(),
      'network_id' => 'network-' . $this->getRandomId(),
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for subnet information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testSubnet(): void {
    $cloud_context = $this->cloudContext;

    // List subnet for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/subnet");
    $this->assertNoErrorMessage();

    // Create random networks.
    $networks = $this->createNetworksRandomTestFormData();
    $this->updateDescribeNetworksMockData($networks);

    // Create the network entities.
    $i = 1;
    foreach ($networks ?: [] as $network) {
      $this->createNetworkTestEntity(OpenStackNetwork::class, $i++, $cloud_context, $network['Name'], $network['NetworkId']);
      $i++;
    }

    // Add a new subnet.
    $add = $this->createOpenStackSubnetTestFormData(self::OPENSTACK_SUBNET_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++, $num++) {
      $this->reloadMockData();
      $this->addOpenStackSubnetMockData($add[$i], $cloud_context, $this->webUser->id());

      // Set network ID.
      $add[$i]['network_id'] = $networks[array_rand($networks)]['NetworkId'];

      $this->drupalGet("/clouds/openstack/$cloud_context/subnet/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Subnet', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/subnet/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/subnet");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/subnet/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all subnet listing exists.
      $this->drupalGet('/clouds/openstack/subnet');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit a subnet information.
    $edit = $this->createOpenStackSubnetTestFormData(self::OPENSTACK_SUBNET_REPEAT_COUNT, TRUE);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++, $num++) {
      unset(
        $edit[$i]['network_id']
      );

      $this->updateOpenStackSubnetMockData($i, $edit[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/subnet/$num/edit");

      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Subnet', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/subnet");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/subnet");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/subnet/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Delete subnet.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/subnet/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/subnet/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Subnet', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/subnet");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests deleting subnets with bulk operation.
   *
   * @throws \Exception
   */
  public function testSubnetBulk(): void {
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      // Create subnet.
      $subnets = $this->createOpenStackSubnetsRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($subnets ?: [] as $subnet) {
        $entities[] = $this->createOpenStackSubnetTestEntity(OpenStackSubnet::class, $index++, $this->cloudContext, $subnet['Name'], $subnet['SubnetId']);
      }

      // The first parameter type should be 'subnet' in OpenStack.
      $this->runTestEntityBulk('subnet', $entities);
    }
  }

  /**
   * Test updating subnet.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateSubnetList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new subnet.
    $add = $this->createOpenStackSubnetTestFormData(self::OPENSTACK_SUBNET_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addOpenStackSubnetMockData($add[$i], $cloud_context, $this->webUser->id());
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/subnet");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated subnets.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/subnet/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/subnet/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/subnet/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack subnets'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack subnets'));
      $this->assertNoErrorMessage();
    }

    // Edit subnet information.
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++, $num++) {

      // Change subnet name in mock data.
      $add[$i]['name'] = 'eni-' . $this->getRandomId();
      $this->updateOpenStackSubnetMockData($i, $add[$i]['name']);

    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/subnet");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated subnets.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete subnet in mock data.
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->deleteOpenStackSubnetMockData($i);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/subnet");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated subnets.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Test updating all subnet.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllSubnetList(): void {
    $cloud_configs = [];

    // List subnet for OpenStack.
    $this->drupalGet('/clouds/openstack/subnet');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new subnet.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createOpenStackSubnetTestFormData(self::OPENSTACK_SUBNET_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addOpenStackSubnetMockData($add[$i], $cloud_context, $this->webUser->id());
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/subnet');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Subnets',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/subnet/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/subnet/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/subnet/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack subnets'));
        // Click 'List OpenStack subnets'.
        $this->clickLink($this->t('List OpenStack subnets'));
        $this->assertNoErrorMessage();
      }
    }

    // Edit Subnet information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++, $num++) {
        // Change subnet name in mock data.
        $add[$i]['name'] = "eni-{$this->getRandomId()}";
        $this->updateOpenStackSubnetMockData($i, $add[$i]['name']);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/subnet');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Subnets',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete subnet in mock data.
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->deleteFirstOpenStackSubnetMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/subnet');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Subnets',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_SUBNET_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack subnet uses REST only.
    return FALSE;
  }

}
