<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;

/**
 * Tests OpenStack template version.
 *
 * @group OpenStack
 */
class OpenStackTemplateVersionTest extends OpenStackTestBase {

  public const OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT = 1;

  public const OPENSTACK_CLOUD_CONFIG_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack template version',
      'view openstack template version',
      'edit openstack template version',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests for template version information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testTemplateVersion(): void {
    $cloud_context = $this->cloudContext;

    // List template version for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/template_version");
    $this->assertNoErrorMessage();

    // Add a new template version.
    $add = $this->createTemplateVersionTestFormData(self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT);

    for ($i = 0, $num = 1; $i < self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT; $i++, $num++) {
      $this->addTemplateVersionMockData($add[$i]);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated template versions.'));
    $this->assertNoErrorMessage();

    for ($i = 0, $num = 1; $i < self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT; $i++, $num++) {
      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/template_version/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/template_version");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all template version listing exists.
      $this->drupalGet('/clouds/openstack/template_version');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }
  }

  /**
   * Test updating template version.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateTemplateVersionList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new template version.
    $add = $this->createTemplateVersionTestFormData(self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addTemplateVersionMockData($add[$i]);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/template_version");

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated template versions.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/template_version/$num");
      $this->assertSession()->linkExists($this->t('List OpenStack template versions'));
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack template versions'));
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Test updating all template version.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllTemplateVersionList(): void {
    $cloud_configs = [];

    // List template version for OpenStack.
    $this->drupalGet('/clouds/openstack/template_version');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_CLOUD_CONFIG_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new template version.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createTemplateVersionTestFormData(self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addTemplateVersionMockData($add[$i]);
      }
    }

    // Click 'Refresh'.
    $this->drupalGet('/clouds/openstack/template_version');
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Template versions',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_TEMPLATE_VERSION_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/template_version/{$num}");
        $this->assertSession()->linkExists($this->t('List OpenStack template versions'));
        // Click 'List OpenStack template versions'.
        $this->clickLink($this->t('List OpenStack template versions'));
        $this->assertNoErrorMessage();
      }
    }
  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack template version uses REST only.
    return FALSE;
  }

}
