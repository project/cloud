<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackProject;
use Drupal\openstack\Entity\OpenStackRole;
use Drupal\openstack\Entity\OpenStackUser;

/**
 * Tests OpenStack user.
 *
 * @group OpenStack
 */
class OpenStackUserTest extends OpenStackTestBase {

  public const OPENSTACK_USER_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack user',
      'add openstack user',
      'view openstack user',
      'edit openstack user',
      'delete openstack user',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'project_id' => $this->cloudConfig->get('field_project_id')->value,
      'create_time' => date('c'),
    ];
  }

  /**
   * Tests CRUD for user information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUser(): void {
    $cloud_context = $this->cloudContext;

    // List user for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/user");
    $this->assertNoErrorMessage();

    // Add a new user.
    $add = $this->createUserTestFormData(self::OPENSTACK_USER_REPEAT_COUNT);

    for ($i = 0, $num = 1; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++, $num++) {
      $this->createProjectTestEntity(OpenStackProject::class, $i, $cloud_context, '', $add[$i]['default_project_id']);
      $this->createRoleTestEntity(OpenStackRole::class, $i, $cloud_context, '', $add[$i]['role']);

      $this->addUserMockData($add[$i], $cloud_context);

      $this->drupalGet("/clouds/openstack/$cloud_context/user/add");
      $this->assertSession()->pageTextContains($this->t('Add OpenStack user'));
      $this->assertNoErrorMessage();
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'User', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure View.
      $this->drupalGet("/clouds/openstack/$cloud_context/user/$num");
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/user");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all user listing exists.
      $this->drupalGet('/clouds/openstack/user');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Change password.
    $edit = $this->createUserTestFormData(self::OPENSTACK_USER_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/user/$num/change_password");

      unset(
        $edit[$i]['name'],
        $edit[$i]['description'],
        $edit[$i]['email'],
        $edit[$i]['default_project_id'],
        $edit[$i]['role'],
        $edit[$i]['enabled'],
      );

      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'User', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/user");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($add[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/user");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit a user information.
    $edit = $this->createUserTestFormData(self::OPENSTACK_USER_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++, $num++) {
      $this->updateUserMockData($i, $edit[$i]['name']);
      $this->drupalGet("/clouds/openstack/$cloud_context/user/$num/edit");

      unset(
        $edit[$i]['password[pass1]'],
        $edit[$i]['password[pass2]'],
        $edit[$i]['default_project_id'],
        $edit[$i]['role'],
      );

      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'User', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->drupalGet("/clouds/openstack/$cloud_context/user");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($edit[$i]['name']);

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/user");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }
    }

    // Delete user.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++, $num++) {
      $this->drupalGet("/clouds/openstack/$cloud_context/user/$num/delete");
      $this->assertNoErrorMessage();
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'User', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/user");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Tests deleting users with bulk operation.
   *
   * @throws \Exception
   */
  public function testUserBulk(): void {
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      // Create user.
      $users = $this->createUsersRandomTestFormData();
      $index = 0;
      $entities = [];
      foreach ($users ?: [] as $user) {
        $entities[] = $this->createUserTestEntity(OpenStackUser::class, $index++, $this->cloudContext, $user['Name']);
      }

      // The first parameter type should be 'user' in OpenStack.
      $this->runTestEntityBulk('user', $entities);
    }
  }

  /**
   * Test updating user.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateUserList(): void {
    $cloud_context = $this->cloudContext;

    // Add a new user.
    $add = $this->createUserTestFormData(self::OPENSTACK_USER_REPEAT_COUNT);
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addUserMockData($add[$i], $cloud_context);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/user");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated users.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++, $num++) {

      // Confirm the detailed view.
      $this->drupalGet("/clouds/openstack/$cloud_context/user/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/user/$num/edit");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/user/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack users'));
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack users'));
      $this->assertNoErrorMessage();
    }

    // Edit user information.
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++, $num++) {
      // Change user name in mock data.
      $add[$i]['name'] = 'user-' . $this->getRandomId();
      $this->updateUserMockData($i, $add[$i]['name']);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/user");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated users.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete user in mock data.
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->deleteFirstUserMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/user");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated users.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test updating all user.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllUserList(): void {
    $cloud_configs = [];

    // List user for OpenStack.
    $this->drupalGet('/clouds/openstack/user');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new user.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createUserTestFormData(self::OPENSTACK_USER_REPEAT_COUNT);
      for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
        $this->reloadMockData();

        $this->addUserMockData($add[$i], $cloud_context);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/user');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Users',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/openstack/{$cloud_context}/user/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/user/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/user/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack users'));
        // Click 'List OpenStack users'.
        $this->clickLink($this->t('List OpenStack users'));
        $this->assertNoErrorMessage();
      }
    }

    // Edit User information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++, $num++) {
        // Change user name in mock data.
        $add[$i]['name'] = "user-{$this->getRandomId()}";
        $this->updateUserMockData($i, $add[$i]['name']);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/user');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Users',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete user in mock data.
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->deleteFirstUserMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/user');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Users',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_USER_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    // OpenStack's user uses REST only.
    return FALSE;
  }

}
