<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackInstance;
use Drupal\openstack\Entity\OpenStackVolume;

/**
 * Tests OpenStack volume for attach and detach operations.
 *
 * @group OpenStack
 */
class OpenStackVolumeAttachDetachTest extends OpenStackTestBase {

  /**
   * Number of times to repeat the test.
   */
  public const MAX_TEST_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'update resource list',
      'list openstack volume',
      'add openstack volume',
      'view any openstack volume',
      'edit any openstack volume',
      'delete any openstack volume',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'create_time' => date('c'),
    ];
  }

  /**
   * Test volume attach.
   */
  public function testVolumeAttachDetach(): void {
    try {
      $this->repeatTestVolumeAttachDetach(self::MAX_TEST_REPEAT_COUNT);
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Repeating test volume attach detach.
   *
   * @param int $max_test_repeat_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  private function repeatTestVolumeAttachDetach($max_test_repeat_count = 1): void {

    $regions = ['RegionOne'];
    for ($i = 1; $i <= $max_test_repeat_count; $i++) {
      // Setup for testing.
      $device_name = $this->random->name(8, TRUE);

      // Set up a test instance.
      $instance = $this->createInstanceTestEntity(OpenStackInstance::class, $i, $regions);
      $instance_id = $instance->getInstanceId();

      // Set up a test volume.
      $volume = $this->createVolumeTestEntity(
        OpenStackVolume::class,
        $i,
        'vol-' . $this->getRandomId(),
        "volume-name #$i - {$this->random->name(32, TRUE)}",
        $this->cloudContext,
        $this->webUser->id()
      );
      $volume_id = $volume->getVolumeId();

      $attach_data = [
        'device_name' => $device_name,
        'instance_id' => $instance_id,
      ];

      // Test attach.
      $this->updateAttachDetachVolumeMockData('AttachVolume', $device_name, $volume_id, $instance_id, 'attaching');
      $this->drupalGet("/clouds/openstack/$this->cloudContext/volume/$i/attach");
      $this->submitForm(
        $attach_data,
        $this->t('Attach')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('The volume @volume is attaching to @instance on @device_name', [
        '@instance' => $instance_id,
        '@volume' => $volume_id,
        '@device_name' => $device_name,
      ]));

      // Test detach.
      $volume->setState('in-use');
      $volume->setAttachmentInformation($instance_id);
      $volume->setAttachmentDeviceName($device_name);
      $volume->save();
      $this->updateAttachDetachVolumeMockData('DetachVolume', $device_name, $volume_id, $instance_id, 'detaching');
      $this->drupalGet("/clouds/openstack/$this->cloudContext/volume/$i/detach");
      $this->submitForm(
        [],
        $this->t('Detach')->render()
      );
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($this->t('The volume @volume is detaching from @instance on @device_name', [
        '@instance' => $instance_id,
        '@volume' => $volume_id,
        '@device_name' => $device_name,
      ]));
    }

  }

  /**
   * Tests detaching Volumes with bulk operation.
   *
   * @throws \Exception
   */
  public function testVolumeBulkDetach(): void {

    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;
    $total_count = 0;
    $total_volumes = [];
    $regions = ['RegionOne'];

    // NOTE: $num needs to be incremented outside $j loop.
    for ($i = 0, $num = 1; $i < self::MAX_TEST_REPEAT_COUNT; $i++) {

      $volumes_count = random_int(1, self::MAX_TEST_REPEAT_COUNT);
      // Create Volumes.
      $volumes = $this->createVolumeTestFormData($volumes_count, TRUE);

      for ($j = 0; $j < $volumes_count; $j++, $num++) {
        // Setup for testing.
        $device_name = $this->random->name(8, TRUE);

        // Set up a test instance.
        $instance = $this->createInstanceTestEntity(OpenStackInstance::class, $j, $regions);
        $instance_id = $instance->getInstanceId();
        $this->addOpenStackInstanceMockData(
          OpenStackInstanceTest::class,
          $instance->getName(),
          $instance->getKeyPairName(),
          $regions,
          'running',
          '',
          $cloud_context,
          ''
        );

        // Set up a test volume.
        $volume = $this->createVolumeTestEntity(
          OpenStackVolume::class,
          $j,
          $volumes[$j]['name'],
          $volumes[$j]['name'],
          $this->cloudContext,
          $this->loggedInUser->id()
        );
        $volume_id = $volume->getVolumeId();
        $this->addVolumeMockData(
          $volumes[$j],
          $this->cloudService->getTagKeyCreatedByUid(
            'openstack',
            $cloud_context
          ),
          $openstack_ec2_api,
          $cloud_context,
          $this->webUser->id()
        );

        $attach_data = [
          'device_name' => $device_name,
          'instance_id' => $instance_id,
        ];
        $this->drupalGet("/clouds/openstack/$cloud_context/volume");

        // Test attach.
        $this->updateAttachDetachVolumeMockData('AttachVolume', $device_name, $volume_id, $instance_id, 'attaching');
        $this->drupalGet("/clouds/openstack/$this->cloudContext/volume/$num/attach");
        $this->submitForm(
          $attach_data,
          $this->t('Attach')->render()
        );
        $this->assertNoErrorMessage();

        $volume->setState('in-use');
        $volume->setAttachmentInformation($instance_id);
        $volume->setAttachmentDeviceName($device_name);
        $volume->save();
        $volumes[$j]['instance_id'] = $instance_id;

      }

      $total_count += $volumes_count;
      foreach ($volumes ?: [] as $volume) {
        $total_volumes[] = $volume;
      }

      $this->drupalGet("/clouds/openstack/$cloud_context/volume");

      $data = [];
      $data['action'] = 'openstack_volume_detach_action';

      $checkboxes = $this->cssSelect('input[type=checkbox]');
      foreach ($checkboxes ?: [] as $checkbox) {
        if ($checkbox->getAttribute('name') === NULL) {
          continue;
        }

        $data[$checkbox->getAttribute('name')] = $checkbox->getAttribute('value');
      }

      // Confirm.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume");
      $this->submitForm(
        $data,
        $this->t('Apply to selected items')->render());
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($total_count === 1
        ? $this->t('Are you sure you want to detach this Volume?')
        : $this->t('Are you sure you want to detach these Volumes?')
      );

      foreach ($total_volumes ?: [] as $volume) {
        $this->assertSession()->pageTextContains($volume['name']);
      }

      // Disassociate.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume/detach_multiple");
      $this->submitForm(
        [],
        $this->t('Detach')->render()
      );
      $this->assertNoErrorMessage();

      if ($total_count === 1) {
        $this->assertSession()->pageTextContains("Detached $volumes_count volume.");
      }
      else {
        $this->assertSession()->pageTextContains("Detached $total_count volumes.");
      }

      for ($j = 0; $j < $total_count; $j++) {
        $volume = $total_volumes[$j];
        $t_args = ['@type' => 'Volume', '%label' => $volume['name']];
        $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been detached.', $t_args)));
        $this->updateVolumeMockData($j, $volume['name'], '');
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      $this->assertSession()->pageTextContains($this->t('Updated Volumes.'));

      // Make sure if disassociated from an instance.
      foreach ($total_volumes ?: [] as $volume) {
        $this->assertSession()->pageTextNotContains($volume['instance_id']);
      }
    }
  }

}
