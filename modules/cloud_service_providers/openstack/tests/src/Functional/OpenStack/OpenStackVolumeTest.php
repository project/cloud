<?php

namespace Drupal\Tests\openstack\Functional\OpenStack;

use Drupal\Tests\cloud\Functional\Utils;
use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackAvailabilityZone;
use Drupal\openstack\Entity\OpenStackInstance;
use Drupal\openstack\Entity\OpenStackSnapshot;
use Drupal\openstack\Entity\OpenStackVolume;

/**
 * Tests OpenStack volume.
 *
 * @group OpenStack
 */
class OpenStackVolumeTest extends OpenStackTestBase {

  public const OPENSTACK_VOLUME_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'update resource list',
      'list openstack volume',
      'add openstack volume',
      'view any openstack volume',
      'edit any openstack volume',
      'delete any openstack volume',

      'add openstack snapshot',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'volume_id' => 'vol-' . $this->getRandomId(),
      'volume_type' => 'lvmdriver-1',
      'create_time' => date('c'),
      'uid' => Utils::getRandomUid(),
    ];
  }

  /**
   * Tests CRUD for the volume information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testVolume(): void {
    $cloud_context = $this->cloudContext;

    // List the volumes for OpenStack.
    $this->drupalGet("/clouds/openstack/$cloud_context/volume");
    $this->assertNoErrorMessage();
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    // Add a new Volume.
    $delete_count = 0;
    $add = $this->createVolumeTestFormData(self::OPENSTACK_VOLUME_REPEAT_COUNT, TRUE);

    // Create Availability Zones.
    $this->createAvailabilityZoneTestEntity(OpenStackAvailabilityZone::class, 0, $cloud_context, '', $add[0]['availability_zone'], 'volume');

    for ($i = 0, $num = 1; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++, $num++) {
      $this->reloadMockData();

      $state = $this->createRandomState();
      $volume_id = 'vol-' . $this->getRandomId();
      $snapshot_name = 'snapshot-name' . $this->random->name(10, TRUE);
      $this->updateCreateVolumeMockData($state, $volume_id);
      $this->createSnapshotTestEntity(OpenStackSnapshot::class, $i, $add[$i]['snapshot_id'], $snapshot_name, $cloud_context);
      $this->updateDescribeSnapshotsMockData(
        [
          [
            'id' => $add[$i]['snapshot_id'],
            'name' => $snapshot_name,
          ],
        ],
        $openstack_ec2_api,
        $cloud_context,
        $this->webUser->id()
      );
      if ($state !== 'in-use') {
        $delete_count++;
      }

      $this->drupalGet("/clouds/openstack/$cloud_context/volume/add");
      $this->assertSession()->pageTextContains($add[0]['availability_zone']);
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Volume', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$i]['name']);
      }

      // Assert delete link count.
      if ($delete_count > 0) {
        $this->assertSession()->linkExists($this->t('Delete'), $delete_count - 1);
      }

      // Make sure view.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume/$num");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($volume_id);
      $this->assertSession()->pageTextContains($add[$i]['snapshot_id']);
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      if ($openstack_ec2_api) {
        $this->assertSession()->pageTextContains($snapshot_name);
      }
    }

    for ($i = 0, $num = 1; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all volume listing exists.
      $this->drupalGet('/clouds/openstack/volume');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Edit a volume information.
    $edit = $this->createVolumeTestFormData(self::OPENSTACK_VOLUME_REPEAT_COUNT, TRUE);
    for ($i = 0, $num = 1; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++, $num++) {

      unset(
        $edit[$i]['snapshot_id'],
        $edit[$i]['size'],
        $edit[$i]['availability_zone'],
        $edit[$i]['volume_type']
      );

      $this->drupalGet("/clouds/openstack/$cloud_context/volume/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Volume', '%label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$i]['name']);
      }
      // Make sure uid.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Delete Volume.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/openstack/$cloud_context/volume/$num/delete");
      $this->assertNoErrorMessage();
      $this->drupalGet("/clouds/openstack/$cloud_context/volume/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Volume', '@label' => $edit[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume");
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Test updating volume list.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateVolumeList(): void {

    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    // Add a new Volume.
    $add = $this->createVolumeTestFormData(self::OPENSTACK_VOLUME_REPEAT_COUNT, TRUE);
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->addVolumeMockData(
        $add[$i],
        $this->cloudService->getTagKeyCreatedByUid(
          'openstack',
          $cloud_context
        ),
        $openstack_ec2_api,
        $cloud_context,
        $this->webUser->id()
      );
      $snapshot_name = 'snapshot-name' . $this->random->name(10, TRUE);
      $this->createSnapshotTestEntity(OpenStackSnapshot::class, $i, $add[$i]['snapshot_id'], $snapshot_name, $cloud_context);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/volume");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Volumes.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/openstack/$cloud_context/volume/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/$num/edit");
      $this->assertSession()->linkExists($this->t('Attach'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/$num/attach");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/$num/delete");
      $this->assertSession()->linkExists($this->t('List OpenStack volumes'));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
      // Click 'Refresh'.
      $this->clickLink($this->t('List OpenStack volumes'));
      $this->assertNoErrorMessage();

      $this->drupalGet("/clouds/openstack/$cloud_context/volume/$num/edit");
      $this->assertSession()->linkExists('Edit');
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/$num/edit");
      $this->assertSession()->linkExists($this->t('Attach'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/$num/attach");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/$num/delete");
    }

    $regions = ['RegionOne'];
    // Edit the volume information.
    for ($i = 0, $num = 1; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++, $num++) {

      // Set up a test instance.
      $instance = $this->createInstanceTestEntity(OpenStackInstance::class, $i, $regions);
      $instance_id = $instance->getInstanceId();

      // Change the volume name in mock data.
      $add[$i]['name'] = "volume-name #$num - {$this->random->name(32, TRUE)}";

      $this->updateVolumeMockData($i, $add[$i]['name'], $instance_id);
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/volume");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Volumes.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {

      $num = $i + self::OPENSTACK_VOLUME_REPEAT_COUNT + 1;

      $this->drupalGet("/clouds/openstack/$cloud_context/volume/$num");
      $this->assertSession()->linkExists($this->t('Edit'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/$num/edit");
      $this->assertSession()->linkExists($this->t('Detach'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/$num/detach");
      $this->assertSession()->linkExists($this->t('Delete'));
      $this->assertSession()->linkByHrefExists("/clouds/openstack/$cloud_context/volume/$num/delete");
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    if ($openstack_ec2_api) {
      // Update tags.
      for ($i = 0, $num = 1; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++, $num++) {
        // Update tags.
        $add[$i]['tags_name'] = $this->getRandomId();
        $this->updateTagsInMockData($i, 'Volumes', 'Name', $add[$i]['tags_name'], FALSE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume");
      for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkExists($add[$i]['tags_name']);
      }

      // Update tags for empty.
      for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {

        // Update tags.
        $this->updateTagsInMockData($i, 'Volumes', 'Name', '', FALSE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume");
      for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkExists($add[$i]['tags_name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['name']);
      }

      // Delete name tags.
      for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {

        // Update tags.
        $this->updateTagsInMockData($i, 'Volumes', 'Name', '', TRUE);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume");
      for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['name']);
      }

      // Click 'Refresh'.
      $this->clickLink($this->t('Refresh'));
      // Make sure listing.
      for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
        $this->assertSession()->linkNotExists($add[$i]['tags_name']);
        $this->assertSession()->linkExists($add[$i]['name']);
      }
    }

    // Delete a volume in mock data.
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->deleteFirstVolumeMockData();
    }

    // Make sure listing.
    $this->drupalGet("/clouds/openstack/$cloud_context/volume");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Updated Volumes.'));
    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test updating all volume list.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testUpdateAllVolumeList(): void {
    $cloud_configs = [];

    // List the volumes for OpenStack.
    $this->drupalGet('/clouds/openstack/volume');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createCloudConfigTestEntity($this->cloudContext, $this->cloudConfig->get('field_use_openstack_ec2_api')->value);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $openstack_ec2_api = $cloud_config->get('field_use_openstack_ec2_api')->value;
      // Add a new Volume.
      $add = $this->createVolumeTestFormData(self::OPENSTACK_VOLUME_REPEAT_COUNT, TRUE);
      for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
        $this->addVolumeMockData($add[$i],
          $this->cloudService->getTagKeyCreatedByUid(
            'openstack',
            $cloud_context
          ),
          $openstack_ec2_api,
          $cloud_context,
          $this->webUser->id()
        );
        $snapshot_name = "snapshot-name{$this->random->name(10, TRUE)}";
        $this->createSnapshotTestEntity(OpenStackSnapshot::class, $i, $add[$i]['snapshot_id'], $snapshot_name, $cloud_context);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/volume');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Volumes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0, $num = 1; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++, $num++) {

        $this->drupalGet("/clouds/openstack/{$cloud_context}/volume/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/volume/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Attach'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/volume/{$num}/attach");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/volume/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List OpenStack volumes'));
        // Click 'List OpenStack volumes'.
        $this->clickLink($this->t('List OpenStack volumes'));
        $this->assertNoErrorMessage();

        $this->drupalGet("/clouds/openstack/{$cloud_context}/volume/{$num}/edit");
        $this->assertSession()->linkExists('Edit');
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/volume/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Attach'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/volume/{$num}/attach");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/openstack/{$cloud_context}/volume/{$num}/delete");
      }
    }

    $regions = ['RegionOne'];
    // Edit the volume information.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0, $num = 1; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++, $num++) {
        // Set up a test instance.
        $instance = $this->createInstanceTestEntity(OpenStackInstance::class, $i, $regions);
        $instance_id = $instance->getInstanceId();

        // Change the volume name in mock data.
        $add[$i]['name'] = "volume-name #{$num} - {$this->random->name(32, TRUE)}";

        $this->updateVolumeMockData($i, $add[$i]['name'], $instance_id);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/volume');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Volumes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Delete the volume in mock data.
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->deleteFirstVolumeMockData();
    }

    // Make sure listing.
    $this->drupalGet('/clouds/openstack/volume');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Volumes',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Test the operation of creating snapshot.
   */
  public function testCreateSnapshotOperation(): void {
    $this->repeatTestCreateSnapshotOperation(
      self::OPENSTACK_VOLUME_REPEAT_COUNT
    );
  }

  /**
   * Repeat testing the operation of creating snapshot.
   *
   * @param int $max_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  private function repeatTestCreateSnapshotOperation($max_count): void {
    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    $add = $this->createVolumeTestFormData(self::OPENSTACK_VOLUME_REPEAT_COUNT, TRUE);

    // Create Availability Zones.
    $this->createAvailabilityZoneTestEntity(OpenStackAvailabilityZone::class, 0, $cloud_context, '', $add[0]['availability_zone'], 'volume');

    for ($i = 0; $i < $max_count; $i++) {
      $this->reloadMockData();

      $snapshot_name = 'snapshot-name' . $this->random->name(10, TRUE);
      $this->updateDescribeSnapshotsMockData(
        [
          [
            'id' => $add[$i]['snapshot_id'],
            'name' => $snapshot_name,
          ],
        ],
        $openstack_ec2_api,
        $cloud_context,
        $this->webUser->id()
      );
      $this->createSnapshotTestEntity(OpenStackSnapshot::class, $i, $add[$i]['snapshot_id'], $snapshot_name, $cloud_context);

      $this->drupalGet("/clouds/openstack/$cloud_context/volume/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/openstack/$cloud_context/volume");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('Create snapshot'));

      // Add a volume to DescribeVolumes.
      $volume_id = $this->latestTemplateVars['volume_id'];
      $add[$i]['name'] = $volume_id;
      $this->addVolumeMockData(
        $add[$i],
        $this->cloudService->getTagKeyCreatedByUid(
          'openstack',
          $cloud_context
        ),
        $openstack_ec2_api,
        $cloud_context,
        $this->webUser->id()
      );

      // Click "Create snapshot" link.
      $this->clickLink($this->t('Create snapshot'), $i);

      // Make sure creating page.
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($this->t('Add OpenStack snapshot'));

      // Make sure the default value of field volume_id.
      $this->assertSession()->fieldValueEquals('volume_id', $volume_id);
    }
  }

  /**
   * Tests deleting volumes with bulk operation.
   *
   * @throws \Exception
   */
  public function testVolumeBulk(): void {
    $cloud_context = $this->cloudContext;

    for ($i = 0, $num = 0; $i < self::OPENSTACK_VOLUME_REPEAT_COUNT; $i++) {
      // Create volumes.
      $volumes = $this->createVolumesRandomTestFormData();
      $entities = [];
      foreach ($volumes ?: [] as $volume) {
        $entities[] = $this->createVolumeTestEntity(
          OpenStackVolume::class,
          $num++,
          $volume['VolumeId'],
          $volume['Name'],
          $cloud_context,
          Utils::getRandomUid()
        );
      }

      $this->runTestEntityBulk('volume', $entities);
    }
  }

}
