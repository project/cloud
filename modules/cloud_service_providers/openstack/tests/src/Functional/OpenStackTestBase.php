<?php

namespace Drupal\Tests\openstack\Functional;

use Drupal\Tests\cloud\Functional\CloudTestBase;
use Drupal\Tests\openstack\Traits\OpenStackTestEntityTrait;
use Drupal\Tests\openstack\Traits\OpenStackTestFormDataTrait;
use Drupal\Tests\openstack\Traits\OpenStackTestMockTrait;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Base Test Case class for OpenStack.
 */
abstract class OpenStackTestBase extends CloudTestBase {

  use OpenStackTestEntityTrait;
  use OpenStackTestFormDataTrait;
  use OpenStackTestMockTrait;

  /**
   * The region.
   *
   * @var string
   */
  protected $cloudRegion;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'cloud',
    'openstack',
  ];

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {

    parent::setUp();

    $this->init(__CLASS__, $this);
  }

  /**
   * Create cloud context.
   *
   * @param string $bundle
   *   The cloud service provide bundle type ('openstack').
   *
   * @return \Drupal\cloud\Entity\CloudConfig
   *   The cloud service provider (CloudConfig) entity.
   *
   * @throws \Exception
   */
  protected function createCloudContext($bundle = 'openstack'): CloudContentEntityBase {
    $random = $this->random;
    $cloud_context_name = strtolower($random->name(8));
    $this->cloudContext = "{$cloud_context_name}_regionone";
    $self_signed_cert_path = ['', $random->name(64)];

    return $this->createTestEntity(CloudConfig::class, [
      'type'                              => $bundle,
      'cloud_context'                     => $cloud_context_name,
      'name'                              => $cloud_context_name,
      'label'                             => "OpenStack RegionOne - {$random->name(8, TRUE)}",
      'field_username'                    => $random->name(32, TRUE),
      'field_password'                    => $random->name(32, TRUE),
      'field_domain_id'                   => $random->name(32, TRUE),
      'field_project_id'                  => $random->name(32, TRUE),
      'field_os_region'                   => 'RegionOne',
      'field_self_signed_cert_path'       => $self_signed_cert_path[array_rand($self_signed_cert_path)],
      'field_identity_api_endpoint'       => 'https://openstack.endpoint:5000',
      'field_identity_version'            => $random->name(2, TRUE),
      'field_compute_api_endpoint'        => 'https://openstack.endpoint:8774',
      'field_compute_version'             => $random->name(2, TRUE),
      'field_image_api_endpoint'          => 'https://openstack.endpoint:9292',
      'field_image_version'               => $random->name(2, TRUE),
      'field_network_api_endpoint'        => 'https://openstack.endpoint:9696',
      'field_network_version'             => $random->name(2, TRUE),
      'field_volume_api_endpoint'         => 'https://openstack.endpoint:8776',
      'field_volume_version'              => $random->name(2, TRUE),
      'field_orchestration_api_endpoint'  => 'https://openstack.endpoint:8004',
      'field_orchestration_version'       => $random->name(2, TRUE),
      'field_use_openstack_ec2_api'       => $this->getEc2ServiceType(),
      'field_account_id'                  => '',
      'field_api_endpoint'                => 'https://openstack.endpoint:8788',
      'field_api_endpoint_port'           => random_int(1000, 9999),
      'field_access_key'                  => $random->name(20, TRUE),
      'field_secret_key'                  => $random->name(40, TRUE),
    ]);
  }

  /**
   * Test bulk operation for entities.
   *
   * @param string $type
   *   The name of the entity type. For example, instance.
   * @param array $entities
   *   The entities.
   * @param string $operation
   *   The operation.
   * @param string $passive_operation
   *   The passive voice of operation.
   * @param string $path_prefix
   *   The URL path of prefix.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function runTestEntityBulk(
    $type,
    array $entities,
    $operation = 'delete',
    $passive_operation = 'deleted',
    $path_prefix = '/clouds/openstack',
  ): void {

    $this->runTestEntityBulkImpl(
      $type,
      $entities,
      $operation,
      $passive_operation,
      $path_prefix
    );
  }

  /**
   * Check if service type is EC2 or REST.
   *
   * @return bool
   *   TRUE if cloud config type is EC2 API, otherwise FALSE.
   */
  protected function getEc2ServiceType(): bool {
    return (bool) random_int(0, 1);
  }

}
