<?php

namespace Drupal\Tests\openstack\Functional\cloud\config;

use Drupal\Tests\cloud\Functional\cloud\config\CloudConfigTestBase;
use Drupal\Tests\openstack\Traits\OpenStackTestFormDataTrait;
use Drupal\Tests\openstack\Traits\OpenStackTestMockTrait;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Tests openstack resource permission while creating cloud service provider.
 *
 * @group Cloud
 */
class OpenStackResourcePermissionTest extends CloudConfigTestBase {

  use OpenStackTestFormDataTrait;
  use OpenStackTestMockTrait;

  /**
   * OPENSTACK_CLOUD_CONFIG_REPEAT_COUNT.
   *
   * @var int
   */
  public const OPENSTACK_CLOUD_CONFIG_REPEAT_COUNT = 1;

  /**
   * OPENSTACK_CLOUD_CONFIG_TYPE_COUNT.
   *
   * @var int
   */
  public const OPENSTACK_CLOUD_CONFIG_TYPE_COUNT = 2;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'cloud',
    'openstack',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'administer cloud service providers',
      'add cloud service providers',
      'edit cloud service providers',
      'edit own cloud service providers',
      'delete cloud service providers',
      'delete own cloud service providers',
      'view published cloud service providers',
      'view own published cloud service providers',
      'access dashboard',
      'view cloud service provider admin list',
      'list cloud server template',
      'administer openstack',
      'list openstack instance',
      'list openstack image',
      'list openstack security group',
      'list openstack volume',
      'list openstack network',
    ];
  }

  /**
   * Create cloud context.
   *
   * @param string $bundle
   *   The CloudConfig bundle type ('openstack').
   *
   * @return \Drupal\cloud\Entity\CloudConfig
   *   The cloud service provider (CloudConfig) entity.
   */
  protected function createCloudContext($bundle = __CLASS__): CloudContentEntityBase {
    return parent::createCloudContext($this->getModuleName($bundle));
  }

  /**
   * Set up test.
   */
  protected function setUp(): void {

    parent::setUp();

    $this->drupalPlaceBlock('system_menu_block:main', [
      'region' => 'header',
      'theme' => 'claro',
    ]);

    // Giving the parameters init(NULL, NULL): Invalidate $this->initMockData.
    $this->init(__CLASS__, $this);

    // Delete the existing $this->cloudContext since we test a CloudConfig
    // entities multiple deletion for themselves.
    if (!empty($this->cloudConfig)) {
      $this->cloudConfig->delete();
    }
  }

  /**
   * Tests permissions of a logged-in user for all resources.
   */
  public function testResourcePermissionsValidate(): void {
    try {
      $this->repeatTestResourcePermission(self::OPENSTACK_CLOUD_CONFIG_REPEAT_COUNT);
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Repeating test update permission of a logged-in user.
   *
   * @param int $max_test_repeat_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function repeatTestResourcePermission($max_test_repeat_count): void {
    $region_name = 'RegionOne';

    // List OpenStack cloud service providers.
    $this->drupalGet('/admin/structure/cloud_config');
    $this->assertNoErrorMessage();

    $openstack_ec2_api = (bool) random_int(0, 1);

    // Add a new Config information.
    for ($j = 0; $j < self::OPENSTACK_CLOUD_CONFIG_TYPE_COUNT; $j++) {
      $add = $this->createOpenStackCloudConfigTestFormData(self::OPENSTACK_CLOUD_CONFIG_REPEAT_COUNT, $openstack_ec2_api);
      for ($i = 0; $i < self::OPENSTACK_CLOUD_CONFIG_REPEAT_COUNT; $i++) {

        // Add new CloudConfig.
        $this->drupalGet('/admin/structure/cloud_config/add');
        $this->assertNoErrorMessage();

        // Test w/ a correct region.
        $add[$i]['field_os_region[0][value]'] = 'RegionOne';
        $this->drupalGet('/admin/structure/cloud_config/add/openstack');
        $this->submitForm(
          $add[$i],
          $this->t('Save')->render()
        );

        // We need to grant the 'view <CLOUD_CONTEXT>' permission.
        // Note: grantPermissions() can be called after "Save", and will be in
        // effect later on the refreshed list.
        $label = $add[$i]['name[0][value]'];
        $this->grantPermissions(
          Role::load(RoleInterface::AUTHENTICATED_ID), [
            'view ' . aws_cloud_form_cloud_config_aws_cloud_add_form_create_cloud_context(
              $label,
              $region_name
            ),
          ]
        );

        // Validate if an OpenStack cloud service provider is created
        // successfully or not (Expect no error message).
        // Warning message to enable corresponding (recommended) permissions.
        $this->assertWarningMessage();
        $this->assertSession()->pageTextNotContains($this->t('Region is invalid. Enter valid region.'));
        $t_args = [
          '@type' => 'cloud service provider',
          '%label' => $label,
        ];
        $j === 1 ? $this->assertSession()->pageTextContains($this->t('Creating cloud service provider was performed successfully.'))
          : $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

        $this->assertSession()->pageTextContains($label);

        // Make sure listing for '/admin/structure/cloud_config'.
        $this->drupalGet('/admin/structure/cloud_config');
        $this->assertNoErrorMessage();
        $this->assertSession()->linkExists($label);

        // Make sure listing for '/clouds'.
        $this->drupalGet('/clouds');
        $this->assertNoErrorMessage();
        $this->assertSession()->linkExists($label);
      }

      // Edit Config case.
      $edit = $this->createOpenStackCloudConfigTestFormData(self::OPENSTACK_CLOUD_CONFIG_REPEAT_COUNT, $openstack_ec2_api);
      // This is CloudConfig test case, so we do not require default
      // $this->cloudContext, which has been already deleted in this setUp().
      // The entity number of $this->cloudContext was '1'. Therefore, the entity
      // number starts from '2', not '1', here.
      for ($i = 0, $num = 2; $i < self::OPENSTACK_CLOUD_CONFIG_REPEAT_COUNT; $i++, $num++) {
        $label = $edit[$i]['name[0][value]'];

        $this->drupalGet("/admin/structure/cloud_config/{$num}/edit");
        $this->submitForm(
          $edit[$i],
          $this->t('Save')->render()
        );

        // Warning message to enable corresponding (recommended) permissions.
        $this->assertWarningMessage();
        $this->assertSession()->pageTextContains($label);

        // Make sure listing for '/admin/structure/cloud_config'.
        $this->drupalGet('/admin/structure/cloud_config');
        $this->assertNoErrorMessage();
        $this->assertSession()->linkExists($label);

        // Make sure listing for '/clouds'.
        $this->drupalGet('/clouds');
        $this->assertNoErrorMessage();
        $this->assertSession()->linkExists($label);
      }
    }
  }

}
