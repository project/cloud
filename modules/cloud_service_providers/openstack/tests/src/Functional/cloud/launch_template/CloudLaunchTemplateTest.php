<?php

namespace Drupal\Tests\openstack\Functional\cloud\launch_template;

use Drupal\Tests\openstack\Functional\OpenStackTestBase;
use Drupal\openstack\Entity\OpenStackImage;
use Drupal\openstack\Entity\OpenStackKeyPair;
use Drupal\openstack\Entity\OpenStackNetwork;
use Drupal\openstack\Entity\OpenStackSecurityGroup;
use Drupal\openstack\Entity\OpenStackServerGroup;

/**
 * Tests cloud launch templates (OpenStackCloudLaunchTemplate).
 *
 * @group Cloud
 */
class CloudLaunchTemplateTest extends OpenStackTestBase {

  public const CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT = 2;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'cloud',
    'openstack',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'add cloud server templates',
      'list cloud server template',
      'view any published cloud server templates',
      'edit any cloud server templates',
      'delete any cloud server templates',
      'access cloud server template revisions',
      'revert all cloud server template revisions',
      'delete all cloud server template revisions',

      'add openstack image',
      'list openstack image',
      'view any openstack image',
      'edit any openstack image',
      'delete any openstack image',

      'list openstack server group',
      'view openstack server group',
      'edit openstack server group',
    ];
  }

  /**
   * Tests CRUD for server_template information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testCloudLaunchTemplate(): void {

    $cloud_context = $this->cloudContext;

    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    $image = $this->createImageTestEntity(OpenStackImage::class, 0, $image_id = '', $cloud_context);
    $image_id = $image->getImageId();

    // Make sure if the image entity is created or not.
    $this->drupalGet("/clouds/openstack/{$cloud_context}/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($image->getName());
    $this->assertSession()->pageTextContains($image->getImageId());

    $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, 0, '', '', '', $this->cloudContext);
    $this->createKeyPairTestEntity(OpenStackKeyPair::class, 0, '', '', $this->cloudContext);
    $this->createNetworkTestEntity(OpenStackNetwork::class, 0, $this->cloudContext);
    $this->createServerGroupTestEntity(OpenStackServerGroup::class, 0, '', '', '', $this->cloudContext);

    if (!$openstack_ec2_api) {
      $flavors = $this->createFlavorsRandomTestFormData();
    }

    // List cloud launch template for OpenStack.
    $this->drupalGet("/clouds/design/server_template/$cloud_context");
    $this->assertNoErrorMessage();

    if ($openstack_ec2_api) {
      $vpcs = $this->createVpcsRandomTestFormData();
      $subnets = $this->createSubnetsRandomTestFormData();
      $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);
    }

    // Add a new server_template information.
    $add = $this->createOpenStackLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {

      $add[$i]['field_openstack_image_id'] = $image_id;

      if (!$openstack_ec2_api) {
        unset($add[$i]['field_openstack_vpc'],
          $add[$i]['field_openstack_subnet'],
          $add[$i]['field_monitoring[value]'],
          $add[$i]['field_min_count[0][value]'],
          $add[$i]['field_max_count[0][value]'],
          $add[$i]['field_kernel_id[0][value]'],
          $add[$i]['field_ram[0][value]'],
          $add[$i]['field_tags'],
          $add[$i]['field_tags[0][item_key]'],
          $add[$i]['field_tags[0][item_value]']
        );

        $flavor_index = array_rand($flavors);
        $this->addDescribeFlavorsMockData($flavors[$flavor_index]['id'], $flavors[$flavor_index]['name']);

        $add[$i]['field_flavor'] = $flavors[$flavor_index]['id'];
      }

      if ($openstack_ec2_api) {
        $vpc_index = array_rand($vpcs);
        $add[$i]['field_openstack_vpc'] = $vpcs[$vpc_index]['VpcId'];
        $vpc_name = $this->getNameFromArray($vpcs, $vpc_index, 'VpcId');

        $subnet_index = array_rand($subnets);
        $add[$i]['field_openstack_subnet'] = $subnets[$subnet_index]['SubnetId'];
        $subnet_name = $this->getNameFromArray($subnets, $subnet_index, 'SubnetId');
      }

      $this->drupalGet("/clouds/design/server_template/$cloud_context/openstack/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      $this->assertSession()->pageTextContains($add[$i]['name[0][value]']);

      if ($openstack_ec2_api) {
        // @phpstan-ignore-next-line
        $this->assertSession()->pageTextContains($vpc_name);
        // @phpstan-ignore-next-line
        $this->assertSession()->pageTextContains($subnet_name);
        $this->assertSession()->pageTextContains($add[$i]['field_tags[0][item_key]']);
        $this->assertSession()->pageTextContains($add[$i]['field_tags[0][item_value]']);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/design/server_template/$cloud_context");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $i + 1; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name[0][value]']);
      }
    }

    // Click 'Refresh'.
    // @todo Need tests for the entities from the mock objects.
    $this->clickLink($this->t('Refresh'));
    $this->assertSession()->pageTextContains($this->t('Unnecessary to update launch templates.'));
    $this->assertSession()->pageTextContains($this->t('Updated Availability Zones.'));
    $this->assertNoErrorMessage();

    // Edit case.
    $edit = $this->createOpenStackLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++, $num++) {

      // Name cannot be changed.
      $edit[$i]['name[0][value]'] = $add[$i]['name[0][value]'];

      $edit[$i]['field_openstack_image_id'] = $image_id;

      if (!$openstack_ec2_api) {
        unset($edit[$i]['field_openstack_vpc'],
          $edit[$i]['field_openstack_subnet'],
          $edit[$i]['field_monitoring[value]'],
          $edit[$i]['field_min_count[0][value]'],
          $edit[$i]['field_max_count[0][value]'],
          $edit[$i]['field_kernel_id[0][value]'],
          $edit[$i]['field_ram[0][value]'],
          $edit[$i]['field_tags'],
          $edit[$i]['field_tags[0][item_key]'],
          $edit[$i]['field_tags[0][item_value]']
        );

        $flavor_index = array_rand($flavors);
        $edit[$i]['field_flavor'] = $flavors[$flavor_index]['id'];
      }

      if ($openstack_ec2_api) {
        $vpc_index = array_rand($vpcs);
        $edit[$i]['field_openstack_vpc'] = $vpcs[$vpc_index]['VpcId'];
        $vpc_name = $this->getNameFromArray($vpcs, $vpc_index, 'VpcId');

        $subnet_index = array_rand($subnets);
        $edit[$i]['field_openstack_subnet'] = $subnets[$subnet_index]['SubnetId'];
        $subnet_name = $this->getNameFromArray($subnets, $subnet_index, 'SubnetId');
      }

      // Make sure uid.
      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());

      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $edit[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      $this->assertSession()->pageTextContains($edit[$i]['name[0][value]']);

      if ($openstack_ec2_api) {
        // @phpstan-ignore-next-line
        $this->assertSession()->pageTextContains($vpc_name);
        // @phpstan-ignore-next-line
        $this->assertSession()->pageTextContains($subnet_name);
      }

      // Make sure listing.
      $this->drupalGet("/clouds/design/server_template/$cloud_context");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($edit[$j]['name[0][value]']);
      }

      // Make sure uid.
      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num");
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());
    }

    // Delete server_template Items
    // 3 times.
    for ($i = 0, $num = 1; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++, $num++) {

      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      // Make sure listing.
      $this->drupalGet("/clouds/design/server_template/$cloud_context");
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextNotContains($edit[$j]['name[0][value]']);
      }
    }
  }

  /**
   * Tests CRUD for server_template revision information.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function testCloudLaunchTemplateRevision(): void {
    $cloud_context = $this->cloudContext;

    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    $image = $this->createImageTestEntity(OpenStackImage::class, 0, $image_id = '', $cloud_context);
    $image_id = $image->getImageId();

    // Make sure if the image entity is created or not.
    $this->drupalGet("/clouds/openstack/{$cloud_context}/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($image->getName());
    $this->assertSession()->pageTextContains($image->getImageId());

    $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, 0, '', '', '', $cloud_context);
    $this->createKeyPairTestEntity(OpenStackKeyPair::class, 0, '', '', $cloud_context);
    $this->createNetworkTestEntity(OpenStackNetwork::class, 0, $cloud_context);
    $this->createServerGroupTestEntity(OpenStackServerGroup::class, 0, '', '', '', $cloud_context);

    if ($openstack_ec2_api) {
      $vpcs = $this->createVpcsRandomTestFormData();
      $subnets = $this->createSubnetsRandomTestFormData();
      $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);
    }

    if (!$openstack_ec2_api) {
      $flavors = $this->createFlavorsRandomTestFormData();
    }

    // Create a cloud launch template.
    $add = $this->createOpenStackLaunchTemplateTestFormData();

    $add[0]['field_openstack_image_id'] = $image_id;

    if (!$openstack_ec2_api) {
      unset($add[0]['field_openstack_vpc'],
        $add[0]['field_openstack_subnet'],
        $add[0]['field_monitoring[value]'],
        $add[0]['field_min_count[0][value]'],
        $add[0]['field_max_count[0][value]'],
        $add[0]['field_kernel_id[0][value]'],
        $add[0]['field_ram[0][value]'],
        $add[0]['field_tags'],
        $add[0]['field_tags[0][item_key]'],
        $add[0]['field_tags[0][item_value]']
      );

      // $flavor_index = array_rand($flavors);
      $this->addDescribeFlavorsMockData($flavors[0]['id'], $flavors[0]['name']);
      $add[0]['field_flavor'] = $flavors[0]['id'];
    }

    if ($openstack_ec2_api) {
      $vpc_index = array_rand($vpcs);
      $add[0]['field_openstack_vpc'] = $vpcs[$vpc_index]['VpcId'];

      $subnet_index = array_rand($subnets);
      $add[0]['field_openstack_subnet'] = $subnets[$subnet_index]['SubnetId'];
    }

    $this->drupalGet("/clouds/design/server_template/$cloud_context/openstack/add");
    $this->submitForm(
      $add[0],
      $this->t('Save')->render()
    );
    $this->assertNoErrorMessage();

    $t_args = [
      '@type' => 'launch template',
      '%label' => $add[0]['name[0][value]'],
    ];
    $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

    // Make sure listing revisions.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/revisions");
    $this->assertNoErrorMessage();

    // Create a new revision.
    $edit = $add[0];
    $revision_desc = $this->random->name(32, TRUE);
    $edit['field_description[0][value]'] = $revision_desc;
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/edit");
    $this->submitForm(
      $edit,
      $this->t('Save')->render()
      );
    $this->assertNoErrorMessage();

    $t_args = [
      '@type' => 'launch template',
      '%label' => $edit['name[0][value]'],
    ];
    $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

    // Make sure listing revisions.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/revisions");
    $this->assertNoErrorMessage();

    // View the revision.
    $this->drupalGet("/clouds/design/server_template/$cloud_context/1/revisions/1/view");
    $this->assertNoErrorMessage();

    // Make sure uid.
    $this->assertSession()->pageTextContains($this->webUser->getAccountName());

    // Test copy function.
    $this->drupalGet("/clouds/design/server_template/{$cloud_context}");
    $this->clickLink($add[0]['name[0][value]']);
    $this->clickLink('Copy');
    $copy_url = $this->getUrl();
    $this->drupalGet($copy_url);
    $this->submitForm(
      [],
      $this->t('Copy')->render()
    );
    $this->assertNoErrorMessage();

    $t_args = [
      '@type' => 'launch template',
      '%label' => "copy_of_{$add[0]['name[0][value]']}",
    ];
    $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
    $this->assertSession()->pageTextContains("copy_of_{$add[0]['name[0][value]']}");
  }

  /**
   * Tests CRUD for server_template information.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Exception
   */
  public function testCloudLaunchTemplateCopy(): void {
    $cloud_context = $this->cloudContext;
    $openstack_ec2_api = $this->cloudConfig->get('field_use_openstack_ec2_api')->value;

    $image = $this->createImageTestEntity(OpenStackImage::class, 0, $image_id = '', $cloud_context);
    $image_id = $image->getImageId();

    // Make sure if the image entity is created or not.
    $this->drupalGet("/clouds/openstack/{$cloud_context}/image");
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($image->getName());
    $this->assertSession()->pageTextContains($image->getImageId());

    $this->createSecurityGroupTestEntity(OpenStackSecurityGroup::class, 0, '', '', '', $cloud_context);
    $this->createKeyPairTestEntity(OpenStackKeyPair::class, 0, '', '', $cloud_context);
    $this->createNetworkTestEntity(OpenStackNetwork::class, 0, $cloud_context);
    $this->createServerGroupTestEntity(OpenStackServerGroup::class, 0, '', '', '', $this->cloudContext);

    if ($openstack_ec2_api) {
      $vpcs = $this->createVpcsRandomTestFormData();
      $subnets = $this->createSubnetsRandomTestFormData();
      $this->updateVpcsAndSubnetsMockData($vpcs, $subnets);
    }

    if (!$openstack_ec2_api) {
      $flavors = $this->createFlavorsRandomTestFormData();
    }

    // Add a new server_template information.
    $add = $this->createOpenStackLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    $copy = $this->createOpenStackLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);

    $num = 1;
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {

      if (!$openstack_ec2_api) {
        unset($add[$i]['field_openstack_vpc'],
          $add[$i]['field_openstack_subnet'],
          $add[$i]['field_monitoring[value]'],
          $add[$i]['field_min_count[0][value]'],
          $add[$i]['field_max_count[0][value]'],
          $add[$i]['field_kernel_id[0][value]'],
          $add[$i]['field_ram[0][value]'],
          $add[$i]['field_tags'],
          $add[$i]['field_tags[0][item_key]'],
          $add[$i]['field_tags[0][item_value]']
        );

        unset($copy[$i]['field_openstack_vpc'],
          $copy[$i]['field_openstack_subnet'],
          $copy[$i]['field_monitoring[value]'],
          $copy[$i]['field_min_count[0][value]'],
          $copy[$i]['field_max_count[0][value]'],
          $copy[$i]['field_kernel_id[0][value]'],
          $copy[$i]['field_ram[0][value]'],
          $copy[$i]['field_tags'],
          $copy[$i]['field_tags[0][item_key]'],
          $copy[$i]['field_tags[0][item_value]']
        );

        $flavor_index = array_rand($flavors);
        $this->addDescribeFlavorsMockData($flavors[$flavor_index]['id'], $flavors[$flavor_index]['name']);
        $add[$i]['field_flavor'] = $copy[$i]['field_flavor'] = $flavors[$flavor_index]['id'];
      }

      $add[$i]['field_openstack_image_id'] = $image_id;

      if ($openstack_ec2_api) {
        $vpc_index = array_rand($vpcs);
        $add[$i]['field_openstack_vpc'] = $vpcs[$vpc_index]['VpcId'];
        $subnet_index = array_rand($subnets);
        $add[$i]['field_openstack_subnet'] = $subnets[$subnet_index]['SubnetId'];
      }

      $this->drupalGet("/clouds/design/server_template/$cloud_context/openstack/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());

      // Access copy page.
      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/copy");
      $this->assertSession()->pageTextContains('Copy launch template');
      $this->assertSession()->fieldValueEquals('name[0][value]', "copy_of_{$add[$i]['name[0][value]']}");

      // Submit copy.
      $copy[$i]['field_openstack_image_id'] = $image_id;
      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/copy");
      $this->submitForm(
        $copy[$i],
        $this->t('Copy')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $copy[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
      $this->assertSession()->pageTextNotContains($add[$i]['name[0][value]']);
      // Make sure uid.
      $this->assertSession()->pageTextContains($this->webUser->getAccountName());

      // Access edit page.
      $num++;
      $this->drupalGet("/clouds/design/server_template/$cloud_context/$num/edit");

      foreach ($copy[$i] ?: [] as $key => $value) {
        if (strpos($key, 'field_tags') === FALSE) {
          $this->assertSession()->fieldValueEquals($key, $value);
        }
      }

      $num++;

    }
  }

}
