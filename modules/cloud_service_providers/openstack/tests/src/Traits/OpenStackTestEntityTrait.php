<?php

namespace Drupal\Tests\openstack\Traits;

use Drupal\Tests\aws_cloud\Traits\AwsCloudTestEntityTrait;
use Drupal\Tests\cloud\Functional\Utils;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Entity\CloudLaunchTemplate;
use Drupal\openstack\Entity\OpenStackFloatingIp;
use Drupal\openstack\Entity\OpenStackImage;

/**
 * The trait creating test entity for openstack testing.
 */
trait OpenStackTestEntityTrait {

  // Most of the functions depends on OpenStackTestEntityTrait.
  use AwsCloudTestEntityTrait;

  /**
   * Create an AWS Cloud Config test entity.
   *
   * @param string $cloud_context
   *   The Cloud context.
   * @param bool $ec2_api
   *   Whether EC2 API or not.
   *
   * @return object
   *   The Cloud Config entity.
   *
   * @throws \Exception
   */
  protected function createCloudConfigTestEntity($cloud_context, $ec2_api = TRUE): object {
    $random = $this->random;
    $num = random_int(1, 3);

    return $this->createTestEntity(CloudConfig::class, [
      'type'                   => 'openstack',
      'cloud_context'          => $cloud_context,
      'label'                  => "Amazon EC2 US West ($num) - {$random->name(8, TRUE)}",
      'field_use_openstack_ec2_api' => $ec2_api,
      'field_os_region'        => 'RegionOne',
      'field_access_key'       => $random->name(20, TRUE),
      'field_secret_key'       => $random->name(40, TRUE),
      'field_api_endpoint'     => "https://ec2.us-west-{$num}.amazonaws.com",
      'field_image_upload_url' => "https://ec2.us-west-{$num}.amazonaws.com",
    ]);
  }

  /**
   * Create cloud launch template.
   *
   * @param array $iam_roles
   *   The IAM roles.
   * @param \Drupal\openstack\Entity\OpenStackImage $image
   *   The image Entity.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The CloudLaunchTemplate entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createOpenStackLaunchTemplateTestEntity(
    array $iam_roles,
    OpenStackImage $image,
    $cloud_context,
  ): CloudContentEntityBase {
    // Create template.
    $template = $this->createTestEntity(CloudLaunchTemplate::class, [
      'cloud_context' => $cloud_context,
      'type' => 'openstack',
      'name' => 'test_template1',
    ]);

    $template->field_test_only->value = '1';
    $template->field_max_count->value = 1;
    $template->field_min_count->value = 1;
    $template->field_monitoring->value = '0';
    $template->field_instance_type->value = 'm1.nano';
    $template->field_openstack_image_id->value = $image->getImageId()
      ?: "ami-{$this->getRandomId()}";

    $template->save();
    return $template;
  }

  /**
   * Create an OpenStack floating IP test entity.
   *
   * @param int $index
   *   The Floating IP index.
   * @param string $floating_ip_name
   *   The Floating IP name.
   * @param string $public_ip
   *   The public IP address.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The Floating IP entity.
   *
   * @throws \Exception
   */
  protected function createFloatingIpTestEntity($index = 0, $floating_ip_name = '', $public_ip = '', $cloud_context = ''): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity(OpenStackFloatingIp::class, [
      'cloud_context' => $cloud_context ?: $this->cloudContext,
      'name' => $floating_ip_name ?: sprintf('eip-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'public_ip' => $public_ip ?: Utils::getRandomPublicIp(),
      'instance_id' => NULL,
      'network_id' => NULL,
      'private_ip_address' => NULL,
      'network_owner' => NULL,
      'allocation_id' => NULL,
      'association_id' => NULL,
      'domain' => 'standard',
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
    ]);
  }

  /**
   * Create an AWS Cloud instance test entity.
   *
   * @param string $class
   *   The instance class.
   * @param int $num
   *   The index.
   * @param array $regions
   *   The regions.
   * @param string $public_ip
   *   The public IP.
   * @param string $instance_name
   *   The instance name.
   * @param string $instance_id
   *   The instance ID.
   * @param string $instance_state
   *   The instance state.
   *
   * @return object
   *   The instance entity.
   *
   * @throws \Exception
   */
  protected function createInstanceTestEntity($class, $num = 0, array $regions = [], $public_ip = NULL, $instance_name = '', $instance_id = '', $instance_state = 'running') {
    if (empty($public_ip)) {
      $public_ip = Utils::getRandomPublicIp();
    }
    $private_ip = Utils::getRandomPrivateIp();
    $region = $regions[array_rand($regions)];

    return $this->createTestEntity($class, [
      'cloud_context' => $this->cloudContext,
      'name' => $instance_name ?: sprintf('instance-entity #%d - %s - %s', $num + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'image_id' => 'ami-' . $this->getRandomId(),
      'key_pair_name' => "key_pair-{$this->random->name(8, TRUE)}",
      'is_monitoring' => 0,
      'availability_zone' => "us-west-$num",
      'security_groups' => "security_group-{$this->random->name(8, TRUE)}",
      'instance_type' => "t$num.small",
      'kernel_id' => 'aki-' . $this->getRandomId(),
      'ramdisk_id' => 'ari-' . $this->getRandomId(),
      'user_data' => "User data #$num: {$this->random->string(64, TRUE)}",
      'account_id' => random_int(100000000000, 999999999999),
      'reservation_id' => 'r-' . $this->getRandomId(),
      'group_name' => $this->random->name(8, TRUE),
      'host_id' => $this->random->name(8, TRUE),
      'affinity' => $this->random->name(8, TRUE),
      'launch_time' => date('c'),
      'security_group_id' => "sg-{$this->getRandomId()}",
      'security_group_name' => $this->random->name(10, TRUE),
      'public_dns_name' => Utils::getPublicDns($region, $public_ip),
      'public_ip_address' => $public_ip,
      'private_dns_name' => Utils::getPrivateDns($region, $private_ip),
      'private_ip_address' => $private_ip,
      'vpc_id' => 'vpc-' . $this->getRandomId(),
      'subnet_id' => 'subnet-' . $this->getRandomId(),
      'reason' => $this->random->string(16, TRUE),
      'instance_id' => $instance_id ?: "i-{$this->getRandomId()}",
      'instance_state' => $instance_state,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an OpenStack network test entity.
   *
   * @param string $class
   *   The network class.
   * @param int $index
   *   The Index.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The network name.
   * @param string $network_id
   *   The network ID.
   * @param string $project_id
   *   The project ID.
   * @param string $admin_state_up
   *   The admin state up.
   * @param string $shared
   *   The shared.
   * @param string $external
   *   The external.
   * @param array $availability_zones
   *   The availability_zones.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The network entity.
   *
   * @throws \Exception
   */
  protected function createNetworkTestEntity(
    string $class,
    int $index = 0,
    string $cloud_context = '',
    string $name = '',
    string $network_id = '',
    string $project_id = '',
    string $admin_state_up = '',
    string $shared = '',
    string $external = '',
    array $availability_zones = [],
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'name' => $name ?: sprintf('eni-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'network_id' => $network_id ?: 'network-' . $this->getRandomId(),
      'project_id' => $project_id ?: 'project-' . $this->getRandomId(),
      'status' => 'available',
      'admin_state_up' => $admin_state_up ?: TRUE,
      'shared' => $shared ?: FALSE,
      'external' => $external ?: TRUE,
      'mtu' => 1,
      'network_type' => 'vlan',
      'physical_network' => 'physnet1',
      'segmentation_id' => 1002,
      'subnets' => NULL,
      'availability_zones' => $availability_zones,
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an OpenStack subnet test entity.
   *
   * @param string $class
   *   The subnet class.
   * @param int $index
   *   The Index.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The subnet name.
   * @param string $subnet_id
   *   The subnet ID.
   * @param string $network_id
   *   The network ID.
   * @param string $project_id
   *   The project ID.
   * @param string $subnet_pool_id
   *   The subnet pool ID.
   * @param string $ip_version
   *   The ip version.
   * @param string $cidr
   *   The cidr.
   * @param string $allocation_pools
   *   The allocation pools.
   * @param string $gateway_ip
   *   The gateway ip.
   * @param string $enable_dhcp
   *   The enable DHCP.
   * @param string $host_routes
   *   The host routes.
   * @param string $dns_name_servers
   *   The dns name servers.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The subnet entity.
   *
   * @throws \Exception
   */
  protected function createOpenStackSubnetTestEntity(
    string $class,
    int $index = 0,
    string $cloud_context = '',
    string $name = '',
    string $subnet_id = '',
    string $network_id = '',
    string $project_id = '',
    string $subnet_pool_id = '',
    string $ip_version = '',
    string $cidr = '',
    string $allocation_pools = '',
    string $gateway_ip = '',
    string $enable_dhcp = '',
    string $host_routes = '',
    string $dns_name_servers = '',
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'name' => $name ?: sprintf('eni-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'subnet_id' => $subnet_id ?: 'network-' . $this->getRandomId(),
      'network_id' => $network_id ?: 'network-' . $this->getRandomId(),
      'project_id' => $project_id ?: 'project-' . $this->getRandomId(),
      'subnet_pool_id' => $subnet_pool_id,
      'ip_version' => $ip_version,
      'cidr' => $cidr,
      'allocation_pools' => $allocation_pools,
      'gateway_ip' => $gateway_ip,
      'enable_dhcp' => $enable_dhcp,
      'host_routes' => $host_routes,
      'dns_name_servers' => $dns_name_servers,
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an OpenStack port test entity.
   *
   * @param string $class
   *   The network class.
   * @param int $index
   *   The Index.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The network name.
   * @param string $port_id
   *   The port ID.
   * @param string $network_id
   *   The network ID.
   * @param string $project_id
   *   The project ID.
   * @param string $mac_address
   *   The mac address.
   * @param string $admin_state_up
   *   The admin state up.
   * @param string $port_security_enabled
   *   The port security enabled.
   * @param string $dns_name
   *   The dns name.
   * @param string $dns_assignment
   *   The dns assignment.
   * @param array $fixed_ips
   *   The fixed ips.
   * @param string $device_owner
   *   The device owner.
   * @param string $device_id
   *   The device ID.
   * @param array $security_groups
   *   The security groups.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The network entity.
   *
   * @throws \Exception
   */
  protected function createPortTestEntity(
    string $class,
    int $index = 0,
    string $cloud_context = '',
    string $name = '',
    string $port_id = '',
    string $network_id = '',
    string $project_id = '',
    string $mac_address = '',
    string $admin_state_up = '',
    string $port_security_enabled = '',
    string $dns_name = '',
    string $dns_assignment = '',
    array $fixed_ips = [],
    string $device_owner = '',
    string $device_id = '',
    array $security_groups = [],
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'name' => $name ?: sprintf('port-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'port_id' => $port_id ?: 'port-' . $this->getRandomId(),
      'network_id' => $network_id ?: 'network-' . $this->getRandomId(),
      'project_id' => $project_id ?: 'project-' . $this->getRandomId(),
      'mac_address' => $mac_address,
      'admin_state_up' => $admin_state_up,
      'port_security_enabled' => $port_security_enabled,
      'dns_name' => $dns_name,
      'dns_assignment' => $dns_assignment,
      'fixed_ips' => $fixed_ips,
      'device_owner' => $device_owner,
      'device_id' => $device_id,
      'security_groups' => $security_groups,
      'binding_vnic_type' => '',
      'binding_host_id' => '',
      'binding_profile' => '',
      'binding_vif_type' => '',
      'binding_vif_details' => '',
      'allowed_address_pairs' => '',
      'status' => 'ACTIVE',
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an OpenStack router test entity.
   *
   * @param string $class
   *   The router class.
   * @param int $index
   *   The Index.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The network name.
   * @param string $network_id
   *   The network ID.
   * @param string $project_id
   *   The project ID.
   * @param string $admin_state_up
   *   The admin state up.
   * @param string $shared
   *   The shared.
   * @param string $external
   *   The external.
   * @param array $availability_zones
   *   The availability_zones.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The network entity.
   *
   * @throws \Exception
   */
  protected function createRouterTestEntity(
    string $class,
    int $index = 0,
    string $cloud_context = '',
    string $name = '',
    string $network_id = '',
    string $project_id = '',
    string $admin_state_up = '',
    string $shared = '',
    string $external = '',
    array $availability_zones = [],
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'name' => $name ?: sprintf('eni-entity #%d - %s - %s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      'network_id' => $network_id ?: 'network-' . $this->getRandomId(),
      'project_id' => $project_id ?: 'project-' . $this->getRandomId(),
      'status' => 'ACTIVE',
      'admin_state_up' => $admin_state_up ?: TRUE,
      'shared' => $shared ?: FALSE,
      'external' => $external ?: FALSE,
      'mtu' => 1,
      'network_type' => 'vlan',
      'physical_network' => 'physnet1',
      'segmentation_id' => 1002,
      'subnets' => NULL,
      'availability_zones' => $availability_zones,
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an OpenStack quota test entity.
   *
   * @param string $class
   *   The quota class.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The subnet entity.
   *
   * @throws \Exception
   */
  protected function createQuotaTestEntity(
    string $class,
    string $cloud_context = '',
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an OpenStack server group test entity.
   *
   * @param string $class
   *   The network interface class.
   * @param int $index
   *   The Index.
   * @param string $server_group_id
   *   The server group ID.
   * @param string $server_group_name
   *   The server group name.
   * @param string $project_id
   *   The project ID.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The network interface entity.
   *
   * @throws \Exception
   */
  protected function createServerGroupTestEntity(
    string $class,
    int $index = 0,
    string $server_group_id = '',
    string $server_group_name = '',
    string $project_id = '',
    string $cloud_context = '',
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context ?: $this->cloudContext,
      'name' => $server_group_name ?: $this->random->name(32, TRUE),
      'server_group_id' => $server_group_id ?: 'server-group-' . $this->getRandomId(),
      'project_id' => $project_id ?: 'project-' . $this->getRandomId(),
      'policy' => 'affinity',
      'members' => [],
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an OpenStack stack test entity.
   *
   * @param string $class
   *   The stack class.
   * @param int $index
   *   The Index.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The stack name.
   * @param string $stack_id
   *   The stack ID.
   * @param string $project_id
   *   The project ID.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The stack entity.
   *
   * @throws \Exception
   */
  protected function createStackTestEntity(
    string $class,
    int $index = 0,
    string $cloud_context = '',
    string $name = '',
    string $stack_id = '',
    string $project_id = '',
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'name' => $name ?: sprintf('eni-entity-%d-%s', $index + 1, $this->random->name(32, TRUE)),
      'stack_id' => $stack_id ?: 'stack-' . $this->getRandomId(),
      'project_id' => $project_id ?: 'project-' . $this->getRandomId(),
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an OpenStack stack resource test entity.
   *
   * @param string $class
   *   The stack class.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The stack name.
   * @param string $stack_id
   *   The stack ID.
   * @param string $stack_entity_id
   *   The stack entity ID.
   * @param string $project_id
   *   The project ID.
   * @param string $resource_id
   *   The resource ID.
   * @param string $resource_status
   *   The resource status.
   * @param string $resource_status_reason
   *   The resource status reason.
   * @param string $resource_type
   *   The resource type.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The stack entity.
   *
   * @throws \Exception
   */
  protected function createStackResourceTestEntity(
    string $class,
    string $cloud_context = '',
    string $name = '',
    string $stack_id = '',
    string $stack_entity_id = '',
    string $project_id = '',
    string $resource_id = '',
    string $resource_status = '',
    string $resource_status_reason = '',
    string $resource_type = '',
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'name' => $name ?: sprintf('eni-entity-%d-%s', $stack_entity_id, $this->random->name(32, TRUE)),
      'stack_id' => $stack_id ?: 'stack-' . $this->getRandomId(),
      'stack_entity_id' => $stack_entity_id,
      'project_id' => $project_id ?: 'project-' . $this->getRandomId(),
      'resource_id' => $resource_id ?: 'project-' . $this->getRandomId(),
      'resource_status' => $resource_status,
      'resource_status_reason' => $resource_status_reason,
      'resource_type' => $resource_type,
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an OpenStack stack event test entity.
   *
   * @param string $class
   *   The stack class.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The stack name.
   * @param string $stack_id
   *   The stack ID.
   * @param string $stack_entity_id
   *   The stack entity ID.
   * @param string $project_id
   *   The project ID.
   * @param string $resource_id
   *   The resource ID.
   * @param string $resource_name
   *   The resource name.
   * @param string $resource_status
   *   The resource status.
   * @param string $resource_status_reason
   *   The resource status reason.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The stack entity.
   *
   * @throws \Exception
   */
  protected function createStackEventTestEntity(
    string $class,
    string $cloud_context = '',
    string $name = '',
    string $stack_id = '',
    string $stack_entity_id = '',
    string $project_id = '',
    string $resource_id = '',
    string $resource_name = '',
    string $resource_status = '',
    string $resource_status_reason = '',
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'name' => $name ?: sprintf('eni-entity-%d-%s', $stack_entity_id, $this->random->name(32, TRUE)),
      'stack_id' => $stack_id ?: 'stack-' . $this->getRandomId(),
      'stack_entity_id' => $stack_entity_id,
      'project_id' => $project_id ?: 'project-' . $this->getRandomId(),
      'event_id' => $resource_id ?: 'project-' . $this->getRandomId(),
      'resource_id' => $resource_id ?: 'project-' . $this->getRandomId(),
      'resource_name' => $resource_name,
      'resource_status' => $resource_status,
      'resource_status_reason' => $resource_status_reason,
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
      'uid' => $this->loggedInUser->id(),
    ]);
  }

  /**
   * Create an OpenStack availability zones entity.
   *
   * @param string $class
   *   The key pair class.
   * @param int $index
   *   The index.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $network_border_group
   *   The network border group.
   * @param string $zone_name
   *   The zone name.
   * @param string $component_name
   *   The component_name.
   * @param string $zone_resource
   *   The zone resource.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The array of availability zone entity.
   */
  protected function createAvailabilityZoneTestEntity($class, $index, $cloud_context = '', $network_border_group = '', $zone_name = '', string $component_name = '', string $zone_resource = ''): CloudContentEntityBase {
    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'group_name' => sprintf('group-#%d-%s-%s', $index + 1, date('Y/m/d H:i:s'), $this->random->name(4, TRUE)),
      'network_border_group' => $network_border_group,
      'zone_id' => 'zone-id-' . $this->random->name(8, TRUE),
      'zone_name' => empty($zone_name) ? 'zone-name-' . $this->random->name(8, TRUE) : $zone_name,
      'created' => time(),
      'component_name' => $component_name,
      'zone_resource' => $zone_resource,
    ]);
  }

  /**
   * Create an Open Stack project test entity.
   *
   * @param string $class
   *   The stack class.
   * @param int $index
   *   The Index.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The stack name.
   * @param string $project_id
   *   The project ID.
   * @param string $description
   *   The description.
   * @param bool $enabled
   *   The enabled.
   * @param string $domain_id
   *   The domain ID.
   * @param string $is_domain
   *   Whether or not the domain.
   * @param array $user_roles
   *   The user roles.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The stack entity.
   *
   * @throws \Exception
   */
  protected function createProjectTestEntity(
    string $class,
    int $index,
    string $cloud_context = '',
    string $name = '',
    string $project_id = '',
    string $description = '',
    bool $enabled = TRUE,
    string $domain_id = '',
    string $is_domain = '',
    array $user_roles = [],
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'name' => $name ?: sprintf('project-entity-%d-%s', $index + 1, $this->random->name(32, TRUE)),
      'project_id' => $project_id ?: 'project-' . $this->getRandomId(),
      'description' => $description,
      'enabled' => $enabled,
      'domain_id' => $domain_id,
      'is_domain' => $is_domain,
      'user_roles' => $user_roles,
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
    ]);
  }

  /**
   * Create an Open Stack role test entity.
   *
   * @param string $class
   *   The stack class.
   * @param int $index
   *   The Index.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The stack name.
   * @param string $role_id
   *   The role ID.
   * @param string $description
   *   The description.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The stack entity.
   *
   * @throws \Exception
   */
  protected function createRoleTestEntity(
    string $class,
    int $index,
    string $cloud_context = '',
    string $name = '',
    string $role_id = '',
    string $description = '',
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'name' => $name ?: sprintf('role-entity-%d-%s', $index + 1, $this->random->name(32, TRUE)),
      'role_id' => $role_id ?: 'role-' . $this->getRandomId(),
      'description' => $description,
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
    ]);
  }

  /**
   * Create an Open Stack user test entity.
   *
   * @param string $class
   *   The stack class.
   * @param int $index
   *   The Index.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $name
   *   The stack name.
   * @param string $user_id
   *   The user ID.
   * @param string $description
   *   The description.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The stack entity.
   *
   * @throws \Exception
   */
  protected function createUserTestEntity(
    string $class,
    int $index,
    string $cloud_context = '',
    string $name = '',
    string $user_id = '',
    string $description = '',
  ): CloudContentEntityBase {
    $timestamp = time();

    return $this->createTestEntity($class, [
      'cloud_context' => $cloud_context,
      'name' => $name ?: sprintf('user-entity-%d-%s', $index + 1, $this->random->name(32, TRUE)),
      'user_id' => $user_id ?: 'user-' . $this->getRandomId(),
      'description' => $description,
      'created' => $timestamp,
      'changed' => $timestamp,
      'refreshed' => $timestamp,
    ]);
  }

}
