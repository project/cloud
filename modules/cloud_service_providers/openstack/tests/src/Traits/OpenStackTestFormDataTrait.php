<?php

namespace Drupal\Tests\openstack\Traits;

use Drupal\Component\Utility\Random;
use Drupal\Tests\aws_cloud\Traits\AwsCloudTestFormDataTrait;
use Drupal\Tests\cloud\Functional\Utils;

/**
 * The trait creating form data for openstack testing.
 */
trait OpenStackTestFormDataTrait {

  use AwsCloudTestFormDataTrait;

  /**
   * OPENSTACK_SECURITY_GROUP_REPEAT_COUNT.
   *
   * @var int
   */
  public static $openStackSecurityGroupRepeatCount = 2;

  /**
   * OPENSTACK_SECURITY_GROUP_RULES_REPEAT_COUNT.
   *
   * @var int
   */
  public static $openStackSecurityGroupRulesRepeatCount = 10;

  /**
   * OPENSTACK_SECURITY_GROUP_RULES_INBOUND.
   *
   * @var int
   */
  public static $openStackSecurityGroupRulesInbound = 0;

  /**
   * OPENSTACK_SECURITY_GROUP_RULES_OUTBOUND.
   *
   * @var int
   */
  public static $openStackSecurityGroupRulesOutbound = 1;

  /**
   * OPENSTACK_SECURITY_GROUP_RULES_MIX.
   *
   * @var int
   */
  public static $openStackSecurityGroupRulesMix = 2;

  /**
   * Create test data for cloud service provider (CloudConfig).
   *
   * @param int $max_count
   *   The max repeat count.
   * @param bool $ec2_api
   *   Whether EC2 API or not.
   *
   * @return array
   *   Test data.
   */
  protected function createOpenStackCloudConfigTestFormData($max_count = 1, $ec2_api = TRUE): array {
    $this->random = new Random();
    $cloud_context = $this->random->name(8, TRUE);

    // Input Fields.
    $data = [];
    for ($i = 0, $num = 1; $i < $max_count; $i++, $num++) {

      $data[$i] = [
        'name[0][value]'            => sprintf('config-entity-#%d-%s', $num, $cloud_context),
        'field_os_region[0][value]' => 'RegionOne',
      ];

      if ($ec2_api) {
        $data[$i]['field_use_openstack_ec2_api[value]'] = $ec2_api;
        $data[$i]['field_api_endpoint[0][value]'] = 'https://openstack.endpoint:8788';
        $data[$i]['field_access_key[0][value]'] = $this->random->name(40, TRUE);
        $data[$i]['field_secret_key[0][value]'] = $this->random->name(40, TRUE);
        $data[$i]['field_account_id[0][value]'] = '';
      }
      else {
        $data[$i]['field_api_endpoint[0][value]'] = 'https://openstack.endpoint';
        $data[$i]['field_username[0][value]'] = $this->random->name(8, TRUE);
        $data[$i]['field_password_wrapper'] = $this->random->name(16, TRUE);
        $data[$i]['field_domain_id[0][value]'] = $this->random->name(8, TRUE);
        $data[$i]['field_project_id[0][value]'] = $this->random->name(32, TRUE);
        $data[$i]['field_api_endpoint_port[0][value]'] = rand(1000, 9999);
        $data[$i]['field_identity_api_endpoint[0][value]'] = 'https://example.com:5000';
        $data[$i]['field_identity_version[0][value]'] = $this->random->name(2, TRUE);
        $data[$i]['field_compute_api_endpoint[0][value]'] = 'https://example.com:8775';
        $data[$i]['field_compute_version[0][value]'] = $this->random->name(2, TRUE);
        $data[$i]['field_image_api_endpoint[0][value]'] = 'https://example.com:9292';
        $data[$i]['field_image_version[0][value]'] = $this->random->name(2, TRUE);
        $data[$i]['field_network_api_endpoint[0][value]'] = 'https://example.com:9696';
        $data[$i]['field_network_version[0][value]'] = $this->random->name(2, TRUE);
        $data[$i]['field_volume_api_endpoint[0][value]'] = 'https://example.com:8776';
        $data[$i]['field_volume_version[0][value]'] = $this->random->name(2, TRUE);
        $data[$i]['field_orchestration_api_endpoint[0][value]'] = 'https://example.com:8004';
        $data[$i]['field_orchestration_version[0][value]'] = $this->random->name(2, TRUE);
      }
    }

    return $data;
  }

  /**
   * Create instance test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createOpenStackInstanceTestFormData($repeat_count = 1): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      $instance_name = sprintf('instance-entity #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(8, TRUE));

      // Input Fields.
      $data[$i] = [
        'name'                => $instance_name,
        'image_id'            => "ami-{$this->getRandomId()}",
        'min_count'           => $num,
        'max_count'           => $num * 2,
        'key_pair_name'       => "key_pair-{$num}-{$this->random->name(8, TRUE)}",
        'is_monitoring'       => 0,
        'availability_zone'   => 'RegionOne',
        'security_groups[]'   => "security_group-$num-{$this->random->name(8, TRUE)}",
        'instance_type'       => 'm1.nano',
        'kernel_id'           => "aki-{$this->getRandomId()}",
        'ramdisk_id'          => "ari-{$this->getRandomId()}",
        'user_data'           => "User data #{$num}: {$this->random->name(64, TRUE)}",
        'tags[0][item_key]'   => 'Name',
        'tags[0][item_value]' => $instance_name,
      ];
    }

    return $data;
  }

  /**
   * Create cloud launch template test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createOpenStackLaunchTemplateTestFormData($repeat_count = 1): array {
    $data = [];
    $random = $this->random;

    for ($i = 0, $num = 1, $instance_family = 3; $i < $repeat_count; $i++, $num++, $instance_family++) {

      // Input Fields.
      $data[] = [
        'cloud_context[0][value]' => $this->cloudContext,
        'name[0][value]' => "LaunchTemplate-{$random->name(16, TRUE)}",
        'field_description[0][value]' => "#$num: " . date('Y/m/d H:i:s - D M j G:i:s T Y')
        . ' - SimpleTest Launch Template Description - '
        . $random->name(32, TRUE),
        'field_test_only[value]' => '1',
        'field_os_availability_zone' => 'RegionOne',
        'field_monitoring[value]' => '1',
        'field_openstack_image_id' => "ami-{$this->getRandomId()}",
        'field_min_count[0][value]' => 1,
        'field_max_count[0][value]' => 1,
        'field_kernel_id[0][value]' => "aki-{$this->getRandomId()}",
        'field_ram[0][value]' => "ari-{$this->getRandomId()}",
        'field_openstack_security_group[1]' => '1',
        'field_openstack_ssh_key' => 1,
        'field_openstack_network' => 1,
        'field_openstack_server_group' => 1,
        'field_tags[0][item_key]' => "key_{$random->name(8, TRUE)}",
        'field_tags[0][item_value]' => "value_{$random->name(8, TRUE)}",
      ];
    }
    return $data;
  }

  /**
   * Create Floating IP test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param array $network_interfaces
   *   Network interfaces array.
   *
   * @return string[][]
   *   Floating IP array.
   */
  protected function createElasticIpTestFormData($repeat_count, array $network_interfaces = []): array {
    $data = [];
    // 3 times.
    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      // Input Fields.
      $data[$i] = [
        'name'   => sprintf('eip-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
        'domain' => 'standard',
        'network_border_group' => 'us-west-2',
        'floating_network_id' => !empty($network_interfaces[$i]) ? $network_interfaces[$i] : 'eni-' . $this->random->name(32, TRUE),
      ];
    }
    return $data;
  }

  /**
   * Create rules.
   *
   * @param int $rules_type
   *   The type of rules. Inbound | Outbound | Mixed.
   * @param string $edit_url
   *   The URL of security group edit form.
   * @param string $self_group_id
   *   The security group ID for rules.
   * @param int $security_group_rules_repeat_count
   *   The security group rules repeat count.
   * @param bool $openstack_ec2_api
   *   Whether EC2 service or REST service.
   *
   * @return array
   *   The rules created.
   *
   * @throws \Exception
   */
  protected function createRulesTestFormData($rules_type, $edit_url, $self_group_id = NULL, $security_group_rules_repeat_count = 1, $openstack_ec2_api = TRUE): array {
    $rules = [];
    $count = random_int(1, $security_group_rules_repeat_count);
    for ($i = 0; $i < $count; $i++) {
      $permissions = [
        [
          'source' => 'ip4',
          'cidr_ip' => Utils::getRandomCidr(),
          'from_port' => Utils::getRandomFromPort(),
          'to_port' => Utils::getRandomToPort(),
          'description' => $this->getRandomSecurityGroupDescription(),
        ],
        [
          'source' => 'ip4',
          'cidr_ip' => Utils::getRandomCidr(),
          'from_port' => Utils::getRandomFromPort(0, 0),
          'to_port' => Utils::getRandomToPort(),
          'description' => $this->getRandomSecurityGroupDescription(),
        ],
        [
          'source' => 'ip6',
          'cidr_ip_v6' => Utils::getRandomCidrV6(),
          'from_port' => Utils::getRandomFromPort(),
          'to_port' => Utils::getRandomToPort(),
          'description' => $this->getRandomSecurityGroupDescription(),
        ],
      ];

      if (!($openstack_ec2_api)) {
        foreach ($permissions as &$permission) {
          $permission['rule_id'] = $this->getRandomId();
        }
      }

      if ($rules_type === self::$openStackSecurityGroupRulesInbound
        || $rules_type === self::$openStackSecurityGroupRulesOutbound) {
        $type = $rules_type;
      }
      else {
        $types = [
          self::$openStackSecurityGroupRulesInbound,
          self::$openStackSecurityGroupRulesOutbound,
        ];
        $type = $types[array_rand($types)];
      }
      $rules[] = $permissions[array_rand($permissions)] + ['type' => $type];
    }

    // Post to form.
    $params = [];
    $inbound_index = 0;
    $outbound_index = 0;
    $rules_added = [];
    foreach ($rules ?: [] as $rule) {
      if ($rule['type'] === self::$openStackSecurityGroupRulesInbound) {
        $index = $inbound_index++;
        $prefix = 'ip_permission';
      }
      else {
        $index = $outbound_index++;
        $prefix = 'outbound_permission';
      }

      foreach ($rule ?: [] as $key => $value) {
        if ($key === 'type') {
          continue;
        }
        $params["{$prefix}[{$index}][{$key}]"] = $value;
      }

      $rules_added[] = $rule;
      $this->updateRulesMockData($rules_added, self::$openStackSecurityGroupRulesOutbound);

      $this->drupalGet($edit_url);
      $this->submitForm(
        $params,
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($rule['from_port']);
    }

    // Confirm the values of edit form.
    $this->confirmRulesFormData($rules, $edit_url);

    return $rules;
  }

  /**
   * Create random instances data.
   *
   * @return array
   *   Random instances data.
   *
   * @throws \Exception
   */
  protected function createInstancesRandomTestFormData(): array {
    $instances = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $instances[] = [
        'InstanceId' => "i-{$this->getRandomId()}",
        'Name' => sprintf('instance-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $instances;
  }

  /**
   * Create random flavors.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @throws \Exception
   */
  protected function createFlavorsRandomTestFormData($repeat_count = 1): array {
    $flavors = [];
    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $flavors[] = [
        'id' => $num,
        'name' => $this->random->name(8, TRUE),
      ];
    }

    return $flavors;
  }

  /**
   * Get the unused volume criteria options on OpenStack Settings.
   *
   * @return array
   *   Array of Unused volume criteria.
   */
  protected function getUnusedVolumeCriteriaOptions(): array {
    return [30, 60, 90, 180, 365];
  }

  /**
   * Create an array of random input data for Settings.
   *
   * @return array
   *   The array including random input data.
   */
  public function createOpenStackSettingsFormData($repeat_count = 1): array {
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('openstack.settings');

    $unused_volume_criteria = $this->getUnusedVolumeCriteriaOptions();

    $edit = [];
    for ($i = 0; $i < $repeat_count; $i++) {
      $edit[] = [
        'openstack_test_mode' => !$config->get('openstack_test_mode'),
        'openstack_unused_volume_criteria' => $unused_volume_criteria[array_rand($unused_volume_criteria)],
        'openstack_instance_terminate' => !$config->get('openstack_instance_terminate'),
        'openstack_update_resources_queue_cron_time' => random_int(1, 9999),
        'openstack_queue_limit' => random_int(1, 50),
      ];
    }

    return $edit;
  }

  /**
   * Create random network data.
   *
   * @return array
   *   Random network data.
   *
   * @throws \Exception
   */
  protected function createNetworksRandomTestFormData(): array {
    $networks = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $networks[] = [
        'NetworkId' => "network-{$this->getRandomId()}",
        'Name' => sprintf('network-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $networks;
  }

  /**
   * Create network test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param bool $is_edit
   *   Whether edit mode or not.
   *
   * @return array
   *   test data array.
   */
  protected function createNetworkTestFormData(int $repeat_count, bool $is_edit = FALSE): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $data[$i] = [
        'name' => "network-name #$num - {$this->random->name(32, TRUE)}",
        'admin_state_up' => TRUE,
        'shared' => FALSE,
        'external' => FALSE,
      ];

      if (!$is_edit) {
        $data[$i]['availability_zones[]'] = 'RegionOne';
      }
    }
    return $data;
  }

  /**
   * Create subnet test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param bool $is_edit
   *   Whether edit mode or not.
   *
   * @return array
   *   test data array.
   */
  protected function createOpenStackSubnetTestFormData(int $repeat_count, bool $is_edit = FALSE): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $data[$i] = [
        'name' => "subnet-name #$num - {$this->random->name(32, TRUE)}",
        'network_id' => "network-{$this->getRandomId()}",
        'gateway_ip' => Utils::getRandomPublicIp(),
        'disable_gateway' => FALSE,
        'enable_dhcp' => TRUE,
        'allocation_pools' => '',
        'dns_name_servers' => '',
        'host_routes' => '',
      ];

      if (!$is_edit) {
        $data[$i]['cidr'] = Utils::getRandomCidr();
        $data[$i]['ip_version'] = 'IPv4';
      }
    }
    return $data;
  }

  /**
   * Create random subnet data.
   *
   * @return array
   *   Random subnet data.
   *
   * @throws \Exception
   */
  protected function createOpenStackSubnetsRandomTestFormData(): array {
    $subnets = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $subnets[] = [
        'SubnetId' => "subnet-{$this->getRandomId()}",
        'Name' => sprintf('subnet-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $subnets;
  }

  /**
   * Create port test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createPortTestFormData(int $repeat_count): array {
    $data = [];
    $ip_address_or_subnets = [
      'subnet',
      'fixed_ip',
    ];
    $ip_address_or_subnet = $ip_address_or_subnets[random_int(0, 1)];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $ip_address_or_subnet = $ip_address_or_subnets[$i];
      $data[$i] = [
        'name' => "port-name #$num - {$this->random->name(32, TRUE)}",
        'network_id' => "network-{$this->getRandomId()}",
        'device_id' => "device-{$this->getRandomId()}",
        'device_owner' => 'network:{$this->getRandomId()}',
        'ip_address_or_subnet' => $ip_address_or_subnet,
        'subnet' => "subnet-{$this->getRandomId()}",
        'fixed_ips' => Utils::getRandomPublicIp(),
      ];
    }
    return $data;
  }

  /**
   * Create random port data.
   *
   * @return array
   *   Random port data.
   *
   * @throws \Exception
   */
  protected function createPortsRandomTestFormData(): array {
    $ports = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $ports[] = [
        'PortId' => "port-{$this->getRandomId()}",
        'Name' => sprintf('port-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $ports;
  }

  /**
   * Create router test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param bool $is_edit
   *   Whether edit mode or not.
   *
   * @return array
   *   test data array.
   */
  protected function createRouterTestFormData(int $repeat_count, bool $is_edit = FALSE): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $data[$i] = [
        'name' => "router-name #$num - {$this->random->name(32, TRUE)}",
        'admin_state_up' => TRUE,
        'external_gateway_network_id' => '',
        'external_gateway_enable_snat' => TRUE,
      ];

      if (!$is_edit) {
        $data[$i]['availability_zones[]'] = 'RegionOne';
      }
    }
    return $data;
  }

  /**
   * Create random router data.
   *
   * @return array
   *   Random router data.
   *
   * @throws \Exception
   */
  protected function createRoutersRandomTestFormData(): array {
    $routers = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $routers[] = [
        'RouterId' => "router-{$this->getRandomId()}",
        'Name' => sprintf('router-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $routers;
  }

  /**
   * Create quota test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createQuotaTestFormData(int $repeat_count): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $data[$i] = [
        'name' => 'quota',
        'instances' => -1,
        'cores' => -1,
        'ram' => -1,
        'metadata_items' => -1,
        'key_pairs' => -1,
        'server_groups' => -1,
        'server_group_members' => -1,
        'injected_files' => -1,
        'injected_file_content_bytes' => -1,
        'injected_file_path_bytes' => -1,
        'volumes' => -1,
        'snapshots' => -1,
        'gigabytes' => -1,
        'network' => -1,
        'subnet' => -1,
        'port' => -1,
        'router' => -1,
        'floatingip' => -1,
        'security_group' => -1,
        'security_group_rule' => -1,
      ];
    }
    return $data;
  }

  /**
   * Create server group test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createServerGroupTestFormData(int $repeat_count): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $data[$i] = [
        'name' => "server-group-name #$num - {$this->random->name(32, TRUE)}",
        'policy' => 'affinity',
      ];
    }
    return $data;
  }

  /**
   * Create stack test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createStackTestFormData(int $repeat_count): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $template_data = <<<EOS
description: sample heat for neutron
heat_template_version: '2013-05-23'
outputs:
  status:
    description: status of sample_net
    value:
      get_attr:
      - sample_net
      - status
parameters:
  sample_net_name:
    default: sample_network
    description: Name of Sample network to be created
    type: string
  sample_net_name1:
    default: 1
    description: Name of Sample network to be created
    type: number
  sample_net_name2:
    default: ['one', "two"]
    description: Name of Sample network to be created
    type: comma_delimited_list
  sample_net_name3:
    default: {"key": "value"}
    description: Name of Sample network to be created
    type: json
  sample_net_name4:
    default: "on"
    description: Name of Sample network to be created
    type: boolean
resources:
  sample_net:
    properties:
      name:
        get_param: sample_net_name
    type: OS::Neutron::Net
EOS;

      $data[] = [
        'template_source' => 'raw',
        'template_data' => $template_data,
      ];
    }
    return $data;
  }

  /**
   * Create stack extra test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createStackTestExtraFormData(int $repeat_count): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $data[$i] = [
        'stack_id' => "stack-{$this->getRandomId()}",
        'name' => "stack-name-$num-{$this->random->name(8, TRUE)}",
        'timeout_mins' => 60,
      ];
    }
    return $data;
  }

  /**
   * Create stack resource test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createStackResourceTestFormData(int $repeat_count): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $data[$i] = [
        'resource_id' => "stack-resource-{$this->getRandomId()}",
        'resource_name' => 'sample_net',
        'resource_status' => 'CREATE_COMPLETE',
        'resource_status_reason' => 'state changed',
        'resource_type' => 'OS::Neutron::Net',
      ];
    }
    return $data;
  }

  /**
   * Create stack event test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createStackEventTestFormData(int $repeat_count): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $data[$i] = [
        'name' => "stack-event-$num-{$this->random->name(8, TRUE)}",
        'resource_id' => "stack-resource-{$this->getRandomId()}",
        'resource_name' => 'sample_net',
        'resource_status' => 'CREATE_COMPLETE',
        'resource_status_reason' => 'state changed',
        'resource_type' => 'OS::Neutron::Net',
      ];
    }
    return $data;
  }

  /**
   * Preview stack test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   * @param string $project_id
   *   The project ID.
   *
   * @return array
   *   test data array.
   */
  protected function previewStackTestFormData(int $repeat_count, string $project_id): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $stack_name = "stack-event-$num-{$this->random->name(8, TRUE)}";
      $data[$i]['parameters'] = [
        'sample_net_name' => 'sample_network',
        'sample_net_name1' => '1',
        'sample_net_name4' => 'on',
      ];
      $data[$i]['resources'] = [
        'name' => $stack_name,
        'resource_id' => "stack-resource-{$this->getRandomId()}",
        'resource_name' => 'sample_net',
        'resource_status' => 'CREATE_COMPLETE',
        'resource_status_reason' => 'state changed',
        'resource_type' => 'OS::Neutron::Net',
      ];
      $data[$i]['links'] = [
        'href' => "https://example.com/v1/$project_id/stacks/$stack_name/None",
        'rel' => "self",
      ];
    }
    return $data;
  }

  /**
   * Create random stack data.
   *
   * @return array
   *   Random stack data.
   *
   * @throws \Exception
   */
  protected function createStacksRandomTestFormData(): array {
    $stacks = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $stacks[] = [
        'StackId' => "stack-{$this->getRandomId()}",
        'Name' => sprintf('stack-random-data-%d-%s-%s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $stacks;
  }

  /**
   * Create template version test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createTemplateVersionTestFormData(int $repeat_count): array {
    $data = [];

    for ($i = 0; $i < $repeat_count; $i++) {
      $count = random_int(1, 10);
      $functions = [];
      for ($num = 0; $num < $count; $num++) {
        $functions += [
          'Name' => sprintf('function-%d-%s', $num, date('Y/m/d H:i:s')),
          'Description' => sprintf('description %d %s %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
        ];
      }
      $data[$i] = [
        'name' => sprintf('heat_template_version.%s', date('Y-m-d')),
        'type' => 'hot',
        'functions' => $functions,
      ];
    }
    return $data;
  }

  /**
   * Create project test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createProjectTestFormData(int $repeat_count): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $data[$i] = [
        'name' => "project-name #$num - {$this->random->name(32, TRUE)}",
        'description' => "project-description #$num - {$this->random->name(32, TRUE)}",
        'enabled' => TRUE,
      ];
    }
    return $data;
  }

  /**
   * Create random project data.
   *
   * @return array
   *   Random project data.
   *
   * @throws \Exception
   */
  protected function createProjectsRandomTestFormData(): array {
    $projects = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $projects[] = [
        'Id' => "project-{$this->getRandomId()}",
        'Name' => sprintf('project-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $projects;
  }

  /**
   * Create role test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createRoleTestFormData(int $repeat_count): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $data[$i] = [
        'name' => "role-name #$num - {$this->random->name(32, TRUE)}",
        'description' => "role-description #$num - {$this->random->name(32, TRUE)}",
      ];
    }
    return $data;
  }

  /**
   * Create random role data.
   *
   * @return array
   *   Random role data.
   *
   * @throws \Exception
   */
  protected function createRolesRandomTestFormData(): array {
    $roles = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $roles[] = [
        'RoleId' => "role-{$this->getRandomId()}",
        'Name' => sprintf('role-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $roles;
  }

  /**
   * Create user test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   test data array.
   */
  protected function createUserTestFormData(int $repeat_count): array {
    $data = [];

    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {
      $password = $this->random->name(16, TRUE);
      $data[$i] = [
        'name' => "user-name #$num - {$this->random->name(32, TRUE)}",
        'description' => "user-description #$num - {$this->random->name(32, TRUE)}",
        'email' => "user-email-{$this->random->name(32, TRUE)}@example.com",
        'password[pass1]' => $password,
        'password[pass2]' => $password,
        'default_project_id' => "project-{$this->getRandomId()}",
        'role' => "role-{$this->getRandomId()}",
        'enabled' => TRUE,

      ];
    }
    return $data;
  }

  /**
   * Create random user data.
   *
   * @return array
   *   Random user data.
   *
   * @throws \Exception
   */
  protected function createUsersRandomTestFormData(): array {
    $users = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $users[] = [
        'RoleId' => "role-{$this->getRandomId()}",
        'Name' => sprintf('role-random-data #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(32, TRUE)),
      ];
    }

    return $users;
  }

}
