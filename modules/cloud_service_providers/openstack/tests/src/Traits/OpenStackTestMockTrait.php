<?php

namespace Drupal\Tests\openstack\Traits;

use Drupal\Component\Serialization\Yaml;
use Drupal\Tests\aws_cloud\Traits\AwsCloudTestMockTrait;
use Drupal\Tests\cloud\Functional\Utils;

/**
 * The trait creating mock data for openstack testing.
 */
trait OpenStackTestMockTrait {

  // Most of the functions depends on AwsCloudTestMockTrait.
  use AwsCloudTestMockTrait;

  /**
   * Update describe regions in mock data.
   *
   * @param array $regions
   *   Regions array.
   */
  protected function updateDescribeRegionsMockData(array $regions): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeRegions']['Regions'][] = $regions;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describe security groups in mock data.
   *
   * @param array $security_groups
   *   Security groups array.
   */
  protected function updateDescribeSecurityGroupsMockData(array $security_groups): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeSecurityGroups']['SecurityGroups'] = $security_groups;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update the CreateSecurityGroup mock data.
   */
  protected function updateCreateSecurityGroupMockData() {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();
    $mock_data['CreateSecurityGroup']['GroupId'] = $vars['group_id'];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update the snapshot mock data.
   *
   * @param int $snapshot_index
   *   The index of snapshot.
   * @param string $name
   *   The snapshot's name.
   * @param bool $is_ec2_service
   *   Whether EC2 service or REST.
   */
  protected function updateSnapshotMockData($snapshot_index, $name, $is_ec2_service = TRUE): void {
    $mock_data = $this->getMockDataFromConfig();

    $is_ec2_service
      ? $mock_data['DescribeSnapshots']['Snapshots'][$snapshot_index]['Tags'][0]
        = ['Key' => 'Name', 'Value' => $name]
      : $mock_data['DescribeSnapshots']['Snapshots'][$snapshot_index]['name']
        = $name;

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describe snapshot in mock data.
   *
   * @param array $test_cases
   *   Test cases array.
   * @param bool $is_ec2_service
   *   Whether EC2 service or REST.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws \Exception
   */
  protected function updateDescribeSnapshotsMockData(
    array $test_cases,
    bool $is_ec2_service = FALSE,
    string $cloud_context = '',
    int $uid = 0,
  ): void {
    $random = $this->random;

    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeSnapshots'] = ['Snapshots' => []];
    foreach ($test_cases ?: [] as $test_case) {
      $snapshot = [
        'name' => $test_case['name'] ?? $test_case['id'],
        'SnapshotId' => $test_case['id'],
        'VolumeSize' => 10,
        'Description' => $random->string(32, TRUE),
        'State' => 'completed',
        'VolumeId' => 'vol-' . $this->getRandomId(),
        'Progress' => '100%',
        'Encrypted' => TRUE,
        'KmsKeyId' => 'arn:aws:kms:us-east-1:123456789012:key/6876fb1b-example',
        'OwnerId' => (string) random_int(100000000000, 999999999999),
        'OwnerAlias' => 'amazon',
        'StateMessage' => $random->string(32, TRUE),
        'StartTime' => date('c'),
      ];

      if (!empty($is_ec2_service) && !empty($test_case['name'])) {
        $snapshot['Tags'] = [
          ['Key' => 'Name', 'Value' => $test_case['name']],
        ];
      }
      if (empty($is_ec2_service)) {
        $snapshot['Tags'] = [
          \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
        ];
      }
      $mock_data['DescribeSnapshots']['Snapshots'][] = $snapshot;
    }

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update mock data related to security group rules.
   *
   * @param array $rules
   *   The security group rules.
   * @param int $rule_type
   *   The security group rule type.
   */
  protected function updateRulesMockData(array $rules, $rule_type): void {
    $mock_data = $this->getMockDataFromConfig();

    $security_group = &$mock_data['DescribeSecurityGroups']['SecurityGroups'][0];
    $security_group['IpPermissions'] = [];
    $security_group['IpPermissionsEgress'] = [];
    foreach ($rules ?: [] as $rule) {
      $permission_name = 'IpPermissions';
      if ($rule['type'] === $rule_type) {
        $permission_name = 'IpPermissionsEgress';
      }

      $permission = [
        'IpProtocol' => 'tcp',
        'FromPort' => $rule['from_port'],
        'ToPort' => $rule['to_port'],
        'RuleId' => $rule['rule_id'] ?? NULL,
      ];

      if ($rule['source'] === 'ip4') {
        $permission['IpRanges'] = [
          [
            'CidrIp' => $rule['cidr_ip'],
            'Description' => $rule['description'],
          ],
        ];
      }
      elseif ($rule['source'] === 'ip6') {
        $permission['Ipv6Ranges'] = [
          [
            'CidrIpv6' => $rule['cidr_ip_v6'],
            'Description' => $rule['description'],
          ],
        ];
      }
      elseif ($rule['source'] === 'group') {
        $permission['UserIdGroupPairs'] = [
          [
            'UserId' => $rule['user_id'],
            'GroupId' => $rule['group_id'],
            'VpcId' => $rule['vpc_id'],
            'VpcPeeringConnectionId' => $rule['peering_connection_id'],
            'PeeringStatus' => $rule['peering_status'],
            'Description' => $rule['description'],
          ],
        ];
      }

      $security_group[$permission_name][] = $permission;
    }

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add describe flavors in mock data.
   *
   * @param int $id
   *   Flavor ID.
   * @param string $name
   *   Flavor name.
   */
  protected function addDescribeFlavorsMockData($id, $name): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeFlavors'] = [
      [
        'id' => $id,
        'name' => $name,
      ],
    ];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add auth token in mock data.
   */
  protected function addAuthTokenMockData($status_code = ''): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['GenerateAuthToken'] = [
      'status_code' => $status_code ?? 200,
      'auth_token' => $this->random->name(32, TRUE),
    ];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update auth token in mock data.
   *
   * @param string $status_code
   *   The status code of auth token.
   */
  protected function updateAuthTokenMockData($status_code = ''): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['GenerateAuthToken']['status_code'] = !empty($status_code) ? $status_code : 200;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add a volume mock data.
   *
   * @param array $data
   *   Array of volume data.
   * @param string $tag_created_uid
   *   The tag created by uid in mock data.
   * @param bool $is_ec2_service
   *   Whether EC2 service or REST.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws \Exception
   */
  protected function addVolumeMockData(
    array $data,
    string $tag_created_uid,
    bool $is_ec2_service = FALSE,
    string $cloud_context = '',
    int $uid = 0,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $volume = [
      'VolumeId' => $data['VolumeId'] ?? $data['name'],
      'Attachments' => [
        ['InstanceId' => NULL],
        ['Device' => ''],
      ],
      'State' => 'available',
      'SnapshotId' => $data['snapshot_id'],
      'Size' => $data['size'],
      'VirtualizationType' => NULL,
      'VolumeType' => $data['volume_type'],
      'Iops' => $data['iops'] ?? '',
      'AvailabilityZone' => $data['availability_zone'],
      'Encrypted' => $data['encrypted'] ?? '',
      'KmsKeyId' => NULL,
      'CreateTime' => $vars['create_time'],
    ];

    $volume['Tags'] = !empty($is_ec2_service)
      ? [
          [
            'Key' => $tag_created_uid,
            'Value' => $uid,
          ],
      ]
      : [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ];

    $mock_data['DescribeVolumes']['Volumes'][] = $volume;
    $this->updateMockDataToConfig($mock_data);

    $snapshot_name = 'snapshot-name' . $this->random->name(10, TRUE);
    $this->updateDescribeSnapshotsMockData(
      [
        [
          'id' => $data['snapshot_id'],
          'name' => $snapshot_name,
        ],
      ],
      $is_ec2_service,
      $cloud_context,
      $uid
    );
  }

  /**
   * Add instance mock data.
   *
   * @param string $test_class
   *   The InstanceTest class.  It can be InstanceTest::class or
   *   OpenStackInstanceTest::class.
   * @param string $name
   *   Instance name.
   * @param string $key_pair_name
   *   Key pair name.
   * @param array $regions
   *   The regions.
   * @param string $state
   *   Instance state.
   * @param string $schedule_value
   *   Schedule value.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $vpc_id
   *   The VPC ID.
   *
   * @return string
   *   The instance ID.
   *
   * @throws \Exception
   */
  protected function addOpenStackInstanceMockData(
    $test_class,
    $name = '',
    $key_pair_name = '',
    array $regions = [],
    $state = 'running',
    $schedule_value = '',
    $cloud_context = '',
    $vpc_id = '',
  ): string {
    // Prepare the mock data for an instance.
    $public_ip = Utils::getRandomPublicIp();
    $private_ip = Utils::getRandomPrivateIp();

    $region = $regions[array_rand($regions)];

    $uid_tag_key = \Drupal::service('openstack.rest')->getTagKeyCreatedByUid(
      'openstack',
      $cloud_context,
      $this->loggedInUser->id()
    );
    $vars = [
      'name' => $name,
      'key_name' => $key_pair_name,
      'account_id' => random_int(100000000000, 999999999999),
      'instance_id' => "i-{$this->getRandomId()}",
      'reservation_id' => "r-{$this->getRandomId()}",
      'group_name' => $this->random->name(8, TRUE),
      'host_id' => $this->random->name(8, TRUE),
      'affinity' => $this->random->name(8, TRUE),
      'launch_time' => date('c'),
      'security_group_id' => 'sg-' . $this->getRandomId(),
      'security_group_name' => $this->random->name(10, TRUE),
      'public_dns_name' => Utils::getPublicDns($region, $public_ip),
      'public_ip_address' => $public_ip,
      'private_dns_name' => Utils::getPrivateDns($region, $private_ip),
      'private_ip_address' => $private_ip,
      'subnet_id' => "subnet-{$this->getRandomId()}",
      'image_id' => "ami-{$this->getRandomId()}",
      'reason' => $this->random->string(16, TRUE),
      'state' => $state,
      'uid' => $this->loggedInUser->id(),
      'uid_tag_key' => $uid_tag_key,
    ];

    $vars['vpc_id'] = empty($vpc_id) ? "vpc-{$this->getRandomId()}" : $vpc_id;

    $vars = array_merge($this->getMockDataTemplateVars(), $vars);

    $instance_mock_data_content = $this->getMockDataFileContent($test_class, $vars, '_instance');

    $instance_mock_data = Yaml::decode($instance_mock_data_content);
    // OwnerId and ReservationId need to be set.
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeInstances']['Reservations'][0]['OwnerId'] = $this->random->name(8, TRUE);
    $mock_data['DescribeInstances']['Reservations'][0]['ReservationId'] = $this->random->name(8, TRUE);

    // Add Schedule information if available.
    if (!empty($schedule_value)) {
      $instance_mock_data['Tags'][] = [
        'Key' => 'Schedule',
        'Value' => $schedule_value,
      ];
    }

    $mock_data['DescribeInstances']['Reservations'][0]['Instances'][] = $instance_mock_data;
    $this->updateMockDataToConfig($mock_data);
    return $vars['instance_id'];
  }

  /**
   * Add Network mock data.
   *
   * @param array $data
   *   Array of network data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws \Exception
   */
  protected function addNetworkMockData(
    array $data,
    string $cloud_context = '',
    int $uid = 0,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $mock_data['CreateNetwork']['Network']['NetworkId'] = $vars['network_id'];

    $network = [
      'NetworkId' => $vars['network_id'],
      'Name' => $data['name'],
      'ProjectId' => $vars['project_id'],
      'Status' => 'available',
      'AdminStateUp' => TRUE,
      'Shared' => FALSE,
      'External' => FALSE,
      'Mtu' => 1,
      'NetworkType' => 'vlan',
      'PhysicalNetwork' => 'physnet1',
      'SegmentationId' => '1002',
      'Subnets' => [],
      'AvailabilityZones' => ['RegionOne'],
      'CreateTime' => $vars['create_time'],
      'Tags' => [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ],
    ];

    $mock_data['DescribeNetworks']['Networks'][] = $network;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Network mock data.
   *
   * @param int $network_index
   *   The index of Network.
   * @param string $name
   *   The network's name.
   */
  protected function updateNetworkMockData(int $network_index, string $name): void {
    $mock_data = $this->getMockDataFromConfig();

    $mock_data['DescribeNetworks']['Networks'][$network_index]['Name'] = $name;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the network of mock data.
   *
   * @param int $index
   *   The index of network.
   */
  protected function deleteNetworkMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeNetworks']['Networks'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first network in mock data.
   */
  protected function deleteFirstNetworkMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $networks = $mock_data['DescribeNetworks']['Networks'];
    array_shift($networks);
    $mock_data['DescribeNetworks']['Networks'] = $networks;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describe networks in mock data.
   *
   * @param array $networks
   *   Volumes array.
   */
  protected function updateDescribeNetworksMockData(array $networks): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeNetworks']['Networks'] = $networks;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add Subnet mock data.
   *
   * @param array $data
   *   Array of subnet data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws \Exception
   */
  protected function addOpenStackSubnetMockData(
    array $data,
    string $cloud_context = '',
    int $uid = 0,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $mock_data['CreateSubnet']['Subnet']['SubnetId'] = $vars['subnet_id'];

    $subnet = [
      'Name' => $data['name'],
      'SubnetId' => $vars['subnet_id'],
      'ProjectId' => $vars['project_id'],
      'NetworkId' => $data['network_id'],
      'SubnetPoolId' => NULL,
      'IpVersion' => $data['ip_version'] ?? 'IPv4',
      'Cidr' => $data['cidr'] ?? Utils::getRandomCidr(),
      'AllocationPools' => [$data['allocation_pools']],
      'GatewayIp' => $data['gateway_ip'],
      'EnableDhcp' => $data['enable_dhcp'],
      'HostRoutes' => [$data['host_routes']],
      'DnsNameServers' => [$data['dns_name_servers']],
      'CreateTime' => $vars['create_time'],
      'Tags' => [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ],
    ];

    $mock_data['DescribeSubnets']['Subnets'][] = $subnet;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Subnet mock data.
   *
   * @param int $subnet_index
   *   The index of Subnet.
   * @param string $name
   *   The subnet's name.
   */
  protected function updateOpenStackSubnetMockData(int $subnet_index, string $name): void {
    $mock_data = $this->getMockDataFromConfig();

    $mock_data['DescribeSubnets']['Subnets'][$subnet_index]['Name'] = $name;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the subnet of mock data.
   *
   * @param int $index
   *   The index of subnet.
   */
  protected function deleteOpenStackSubnetMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeSubnets']['Subnets'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first subnet in mock data.
   */
  protected function deleteFirstOpenStackSubnetMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $subnets = $mock_data['DescribeSubnets']['Subnets'];
    array_shift($subnets);
    $mock_data['DescribeSubnets']['Subnets'] = $subnets;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describe subnets in mock data.
   *
   * @param array $subnets
   *   Volumes array.
   */
  protected function updateDescribeSubnetsMockData(array $subnets): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeSubnets']['Subnets'] = $subnets;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add Port mock data.
   *
   * @param array $data
   *   Array of port data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws \Exception
   */
  protected function addPortMockData(
    array $data,
    string $cloud_context = '',
    int $uid = 0,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $mock_data['CreatePort']['Port']['PortId'] = $vars['port_id'];

    $fixed_ips = !empty($data['ip_address_or_subnet']) && $data['ip_address_or_subnet'] === 'fixed_ip'
      ? ['ip_address' => $data['fixed_ips']]
      : ['subnet_id' => $data['subnet']];

    $port = [
      'PortId' => $vars['port_id'],
      'Name' => $data['name'],
      'NetworkId' => $vars['network_id'],
      'ProjectId' => $vars['project_id'],
      'SubnetId' => $data['subnet'],
      'DeviceId' => $data['device_id'],
      'DeviceOwner' => $data['device_owner'],
      'MacAddress' => '',
      'Status' => 'ACTIVE',
      'AdminStateUp' => '',
      'PortSecurityEnabled' => '',
      'DnsName' => '',
      'DnsAssignment' => [],
      'FixedIps' => $fixed_ips,
      'SecurityGroups' => [],
      'BindingVnicType' => '',
      'BindingHostId' => '',
      'BindingVifType' => '',
      'BindingProfile' => '',
      'BindingVifDetails' => [],
      'AllowedAddressPairs' => [],
      'CreateTime' => $vars['create_time'],
      'Tags' => [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ],
    ];

    $mock_data['DescribePorts']['Ports'][] = $port;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Port mock data.
   *
   * @param int $port_index
   *   The index of Port.
   * @param string $name
   *   The port's name.
   */
  protected function updatePortMockData(int $port_index, string $name): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribePorts']['Ports'][$port_index]['Name'] = $name;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the port of mock data.
   *
   * @param int $index
   *   The index of port.
   */
  protected function deletePortMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribePorts']['Ports'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first port in mock data.
   */
  protected function deleteFirstPortMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $ports = $mock_data['DescribePorts']['Ports'];
    array_shift($ports);
    $mock_data['DescribePorts']['Ports'] = $ports;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describe ports in mock data.
   *
   * @param array $ports
   *   Volumes array.
   */
  protected function updateDescribePortsMockData(array $ports): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribePorts']['Ports'] = $ports;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add Router mock data.
   *
   * @param array $data
   *   Array of router data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws \Exception
   */
  protected function addRouterMockData(
    array $data,
    string $cloud_context = '',
    int $uid = 0,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $mock_data['CreateRouter']['Router']['RouterId'] = $vars['router_id'];

    $router = [
      'RouterId' => $vars['router_id'],
      'Name' => $data['name'],
      'ProjectId' => $vars['project_id'],
      'Status' => 'ACTIVE',
      'AdminStateUp' => TRUE,
      'ExternalGatewayNetworkId' => '',
      'ExternalGatewayEnableSnat' => TRUE,
      'Routes' => '',
      'AvailabilityZones' => ['RegionOne'],
      'CreateTime' => $vars['create_time'],
      'Tags' => [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ],
    ];

    $mock_data['DescribeRouters']['Routers'][] = $router;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Router mock data.
   *
   * @param int $router_index
   *   The index of Router.
   * @param string $name
   *   The router's name.
   */
  protected function updateRouterMockData(int $router_index, string $name): void {
    $mock_data = $this->getMockDataFromConfig();

    $mock_data['DescribeRouters']['Routers'][$router_index]['Name'] = $name;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the router of mock data.
   *
   * @param int $index
   *   The index of router.
   */
  protected function deleteRouterMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeRouters']['Routers'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first router in mock data.
   */
  protected function deleteFirstRouterMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $routers = $mock_data['DescribeRouters']['Routers'];
    array_shift($routers);
    $mock_data['DescribeRouters']['Routers'] = $routers;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describe routers in mock data.
   *
   * @param array $routers
   *   Volumes array.
   */
  protected function updateDescribeRoutersMockData(array $routers): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeRouters']['Routers'] = $routers;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add Quota mock data.
   *
   * @param array $data
   *   Array of quota data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws \Exception
   */
  protected function addQuotaMockData(
    array $data,
    string $cloud_context = '',
    int $uid = 0,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $quota = [
      'Name' => $data['name'],
      'ProjectId' => $vars['project_id'],
      'Instances' => -1,
      'InstancesUsage' => random_int(0, 1000),
      'Cores' => -1,
      'CoresUsage' => random_int(0, 1000),
      'Ram' => -1,
      'RamUsage' => random_int(0, 1000),
      'MetadataItems' => -1,
      'MetadataItemsUsage' => random_int(0, 1000),
      'KeyPairs' => -1,
      'KeyPairsUsage' => random_int(0, 1000),
      'ServerGroups' => -1,
      'ServerGroupsUsage' => random_int(0, 1000),
      'ServerGroupMembers' => -1,
      'ServerGroupMembersUsage' => random_int(0, 1000),
      'InjectedFiles' => -1,
      'InjectedFilesUsage' => random_int(0, 1000),
      'InjectedFileContentBytes' => -1,
      'InjectedFileContentBytesUsage' => random_int(0, 1000),
      'InjectedFilePathBytes' => -1,
      'InjectedFilePathBytesUsage' => random_int(0, 1000),
      'Volumes' => -1,
      'VolumesUsage' => random_int(0, 1000),
      'Snapshots' => -1,
      'SnapshotsUsage' => random_int(0, 1000),
      'GigaBytes' => -1,
      'GigaBytesUsage' => random_int(0, 1000),
      'Network' => -1,
      'NetworkUsage' => random_int(0, 1000),
      'Subnet' => -1,
      'SubnetUsage' => random_int(0, 1000),
      'Port' => -1,
      'PortUsage' => random_int(0, 1000),
      'Router' => -1,
      'RouterUsage' => random_int(0, 1000),
      'FloatingIp' => -1,
      'FloatingIpUsage' => random_int(0, 1000),
      'SecurityGroup' => -1,
      'SecurityGroupUsage' => random_int(0, 1000),
      'SecurityGroupRule' => -1,
      'SecurityGroupRuleUsage' => random_int(0, 1000),
    ];

    $mock_data['DescribeQuotas']['Quotas'][] = $quota;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Quota mock data.
   *
   * @param int $quota_index
   *   The index of Quota.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $instances
   *   The instances.
   */
  protected function updateQuotaMockData(int $quota_index, string $cloud_context = '', int $instances = 0): void {
    $mock_data = $this->getMockDataFromConfig();

    $mock_data['DescribeQuotas']['Quotas'][$quota_index]['Instances'] = $instances;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describe quotas in mock data.
   *
   * @param array $quotas
   *   Volumes array.
   */
  protected function updateDescribeQuotasMockData(array $quotas): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['DescribeQuotas']['Quotas'] = $quotas;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add Server group mock data.
   *
   * @param array $data
   *   Array of server group data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws \Exception
   */
  protected function addServerGroupMockData(
    array $data,
    string $cloud_context = '',
    int $uid = 0,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $mock_data['CreateServerGroup']['ServerGroup']['ServerGroupId'] = $vars['server_group_id'];

    $server_group = [
      'ServerGroupId' => $vars['server_group_id'],
      'Name' => $data['name'],
      'ProjectId' => $vars['project_id'],
      'Policy' => $data['policy'],
      'Members' => [],
    ];

    $mock_data['DescribeServerGroups']['ServerGroups'][] = $server_group;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first server group in mock data.
   */
  protected function deleteFirstServerGroupMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $server_groups = $mock_data['DescribeServerGroups']['ServerGroups'];
    array_shift($server_groups);
    $mock_data['DescribeServerGroups']['ServerGroups'] = $server_groups;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add Stack mock data.
   *
   * @param array $data
   *   Array of stack data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws \Exception
   */
  protected function addStackMockData(
    array $data,
    string $cloud_context = '',
    int $uid = 0,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $mock_data['CreateStack']['Stack']['StackId'] = $data['stack_id'];

    $stack = [
      'StackId' => $data['stack_id'],
      'Name' => $data['name'],
      'ProjectId' => $vars['project_id'],
      'TimeoutMins' => 60,
      'DisableRollback' => FALSE,
      'Template' => $data['template_data'],
      'Description' => '',
      'StackStatus' => 'CREATE_COMPLETE',
      'StackStatusReason' => '',
      'Environment' => [],
      'CreateTime' => $vars['create_time'],
      'Tags' => [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ],
    ];

    $mock_data['DescribeStacks']['Stacks'][] = $stack;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add Stack resource mock data.
   *
   * @param array $data
   *   Array of stack data.
   * @param int $uid
   *   The uid.
   * @param int $stack_entity_id
   *   The stack entity id.
   *
   * @throws \Exception
   */
  protected function addStackResourceMockData(
    array $data,
    int $uid = 0,
    int $stack_entity_id = 0,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $resource = [
      'ResourceId' => 'resource-' . $this->getRandomId(),
      'Name' => 'sample_net',
      'ProjectId' => $vars['project_id'],
      'StackId' => $data['stack_id'],
      'StackName' => $data['name'],
      'StackEntityId' => $stack_entity_id,
      'ResourceStatus' => 'CREATE_COMPLETE',
      'ResourceStatusReason' => 'state changed',
      'ResourceType' => 'OS::Neutron::Net',
      'CreationTime' => $vars['create_time'],
      'UpdatedTime' => $vars['create_time'],
      'Uid' => $uid,
    ];

    $mock_data['describeStackResources']['Resources'][] = $resource;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Stack mock data.
   *
   * @param int $stack_index
   *   The index of Stack.
   */
  protected function updateStackMockData(int $stack_index): void {
    $mock_data = $this->getMockDataFromConfig();

    $mock_data['DescribeStacks']['Stacks'][$stack_index]['StackStatus'] = 'UPDATE_COMPLETE';
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Show Stack mock data.
   *
   * @param array $data
   *   Array of stack data.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws \Exception
   */
  protected function showStackMockData(
    array $data,
    string $cloud_context = '',
    int $uid = 0,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $stack = [
      'StackId' => $vars['stack_id'],
      'Name' => $data['name'],
      'ProjectId' => $vars['project_id'],
      'TimeoutMins' => 60,
      'Template' => $data['template_data'],
      'Parameters' => [],
      'Outputs' => [],
      'Description' => '',
      'Rollback' => FALSE,
      'Tags' => [
        \Drupal::service('openstack.rest')->getTagValueCreatedByUid('openstack', $cloud_context, $uid),
      ],
    ];

    $mock_data['ShowStack'] = $stack;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Preview Stack mock data.
   *
   * @param array $data
   *   Array of stack data.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @throws \Exception
   */
  protected function previewStackMockData(
    array $data,
    string $cloud_context = '',
  ): void {
    $mock_data = $this->getMockDataFromConfig();

    $mock_data['PreviewStack']['parameters'] = $data['parameters'];
    $mock_data['PreviewStack']['resources'][] = $data['resources'];
    $mock_data['PreviewStack']['links'][] = $data['links'];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Check Stack in mock data.
   */
  protected function checkStackMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['CheckStack'] = ['Success' => TRUE];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Suspend Stack in mock data.
   */
  protected function suspendStackMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['SuspendStack'] = ['Success' => TRUE];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Resume Stack in mock data.
   */
  protected function resumeStackMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $mock_data['ResumeStack'] = ['Success' => TRUE];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first stack in mock data.
   */
  protected function deleteFirstStackMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $stacks = $mock_data['DescribeStacks']['Stacks'];
    array_shift($stacks);
    $mock_data['DescribeStacks']['Stacks'] = $stacks;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Add Template version mock data.
   *
   * @param array $data
   *   Array of template version data.
   *
   * @throws \Exception
   */
  protected function addTemplateVersionMockData(
    array $data,
  ): void {
    $mock_data = $this->getMockDataFromConfig();
    $vars = $this->getMockDataTemplateVars();

    $template_version = [
      'Name' => $data['name'],
      'ProjectId' => $vars['project_id'],
      'Type' => $data['type'],
      'CreationTime' => $vars['create_time'],
      'UpdatedTime' => $vars['create_time'],
    ];

    $mock_data['DescribeTemplateVersions']['TemplateVersions'][] = $template_version;
    $mock_data['DescribeTemplateVersionFunctions']['TemplateFunctions'][] = $data['functions'];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Create Project mock data.
   *
   * @param array $data
   *   Array of project data.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @throws \Exception
   */
  protected function addProjectMockData(
    array $data,
    string $cloud_context = '',
  ): void {
    $mock_data = $this->getMockDataFromConfig();

    $project_id = 'project-' . $this->getRandomId();
    $mock_data['CreateProject']['Project']['Id'] = $project_id;

    $project = [
      'Id' => $project_id,
      'Name' => $data['name'],
      'Description' => $data['description'],
      'Enabled' => $data['enabled'],
      'IsDomain' => TRUE,
      'DomainId' => 'domain-' . $this->getRandomId(),
      'UserRoles' => [],
    ];

    $mock_data['DescribeProjects']['Projects'][] = $project;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Create Project user role mock data.
   *
   * @param array $data
   *   Array of project data.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @throws \Exception
   */
  protected function addProjectUserRoleMockData(
    array $data,
    string $cloud_context = '',
  ): void {
    $mock_data = $this->getMockDataFromConfig();

    $roles['UserRoles']['user_id'] = [];

    $mock_data['DescribeProjectUserRoles']['UserRoles'][] = $roles;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Project mock data.
   *
   * @param int $project_index
   *   The index of Project.
   * @param string $name
   *   The project's name.
   */
  protected function updateProjectMockData(int $project_index, string $name): void {
    $mock_data = $this->getMockDataFromConfig();

    $mock_data['DescribeProjects']['Projects'][$project_index]['Name'] = $name;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the project of mock data.
   *
   * @param int $index
   *   The index of project.
   */
  protected function deleteProjectMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeProjects']['Projects'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first project in mock data.
   */
  protected function deleteFirstProjectMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $projects = $mock_data['DescribeProjects']['Projects'];
    array_shift($projects);
    $mock_data['DescribeProjects']['Projects'] = $projects;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Create Role mock data.
   *
   * @param array $data
   *   Array of role data.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @throws \Exception
   */
  protected function addRoleMockData(
    array $data,
    string $cloud_context = '',
  ): void {
    $mock_data = $this->getMockDataFromConfig();

    $role_id = 'role-' . $this->getRandomId();
    $mock_data['CreateRole']['Role']['Id'] = $role_id;

    $role = [
      'Id' => $role_id,
      'Name' => $data['name'],
      'Description' => $data['description'],
    ];

    $mock_data['DescribeRoles']['Roles'][] = $role;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update Role mock data.
   *
   * @param int $role_index
   *   The index of Role.
   * @param string $name
   *   The role's name.
   */
  protected function updateRoleMockData(int $role_index, string $name): void {
    $mock_data = $this->getMockDataFromConfig();

    $mock_data['DescribeRoles']['Roles'][$role_index]['Name'] = $name;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the role of mock data.
   *
   * @param int $index
   *   The index of role.
   */
  protected function deleteRoleMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeRoles']['Roles'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first role in mock data.
   */
  protected function deleteFirstRoleMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $roles = $mock_data['DescribeRoles']['Roles'];
    array_shift($roles);
    $mock_data['DescribeRoles']['Roles'] = $roles;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Create User mock data.
   *
   * @param array $data
   *   Array of user data.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @throws \Exception
   */
  protected function addUserMockData(
    array $data,
    string $cloud_context = '',
  ): void {
    $mock_data = $this->getMockDataFromConfig();

    $user_id = 'user-' . $this->getRandomId();
    $mock_data['CreateUser']['User']['Id'] = $user_id;

    $user = [
      'Id' => $user_id,
      'Name' => $data['name'],
      'Description' => $data['description'],
      'Email' => $data['email'],
      'DefaultProjectId' => $data['default_project_id'],
      'DomainId' => 'domain-' . $this->getRandomId(),
      'Enabled' => $data['enabled'],
      'PasswordExpiresAt' => date('c'),
    ];

    $mock_data['DescribeUsers']['Users'][] = $user;
    $mock_data['AssignRoleToUserOnProject'] = ['Success' => TRUE];
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update User mock data.
   *
   * @param int $user_index
   *   The index of User.
   * @param string $name
   *   The user's name.
   */
  protected function updateUserMockData(int $user_index, string $name): void {
    $mock_data = $this->getMockDataFromConfig();

    $mock_data['DescribeUsers']['Users'][$user_index]['Name'] = $name;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete the user of mock data.
   *
   * @param int $index
   *   The index of user.
   */
  protected function deleteUserMockData(int $index): void {
    $mock_data = $this->getMockDataFromConfig();
    unset($mock_data['DescribeUsers']['Users'][$index]);
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Delete first user in mock data.
   */
  protected function deleteFirstUserMockData(): void {
    $mock_data = $this->getMockDataFromConfig();
    $users = $mock_data['DescribeUsers']['Users'];
    array_shift($users);
    $mock_data['DescribeUsers']['Users'] = $users;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Create Project shared image mock data.
   *
   * @param array $data
   *   Array of user data.
   *
   * @throws \Exception
   */
  protected function addProjectSharedImageMockData(
    array $data,
  ): void {
    $mock_data = $this->getMockDataFromConfig();

    $member = [
      'ProjectId' => $data['project_id'] ?? NULL,
      'Status' => $data['status'] ?? NULL,
      'CloudContext' => $data['cloud_context'] ?? NULL,
    ];

    $mock_data['describeProjectSharedImage']['Members'][] = $member;
    $this->updateMockDataToConfig($mock_data);
  }

}
