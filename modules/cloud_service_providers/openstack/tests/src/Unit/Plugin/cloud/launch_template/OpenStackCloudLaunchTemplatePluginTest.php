<?php

namespace Drupal\Tests\openstack\Unit\Plugin\cloud\launch_template;

use Drupal\Component\Utility\Random;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\aws_cloud\Traits\AwsCloudTestFormDataTrait;
use Drupal\Tests\cloud\Traits\CloudTestEntityTrait;
use Drupal\aws_cloud\Entity\Ec2\ImageInterface;
use Drupal\aws_cloud\Entity\Ec2\KeyPairInterface;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\openstack\Entity\OpenStackNetworkInterface;
use Drupal\openstack\Plugin\cloud\launch_template\OpenStackLaunchTemplatePlugin;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Drupal\openstack\Service\Rest\OpenStackService as OpenStackRestService;
use Drupal\openstack\Service\Rest\OpenStackServiceMock;

/**
 * Tests OpenStack Cloud Template plugin.
 *
 * @group Cloud
 */
class OpenStackCloudLaunchTemplatePluginTest extends UnitTestCase {

  use AwsCloudTestFormDataTrait;
  use CloudTestEntityTrait;

  // Amazon VPC Limits - Security groups per VPC (per region).
  public const MAX_SECURITY_GROUPS_COUNT = 500;

  /**
   * Plugin.
   *
   * @var \Drupal\openstack\Plugin\cloud\launch_template\OpenStackLaunchTemplatePlugin
   */
  private $plugin;

  /**
   * AWS Cloud EC2 service mock.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  private $ec2ServiceMock;

  /**
   * Creating random data utility.
   *
   * @var \Drupal\Component\Utility\Random
   */
  private $random;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    // Create messenger, logger.factory and string_translation container.
    $container = new ContainerBuilder();

    // Messenger.
    $mock_messenger = $this->createMock(Messenger::class);

    // Logger.
    $mock_logger = $this->createMock(LoggerChannelInterface::class);
    $mock_logger_factory = $this->createMock(LoggerChannelFactoryInterface::class);
    $mock_logger_factory
      ->method('get')
      ->willReturn($mock_logger);
    $mock_openstack_rest_service = $this->createMock(OpenStackRestService::class);

    // Set containers.
    $container->set('messenger', $mock_messenger);
    $container->set('logger.factory', $mock_logger_factory);
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    $this->ec2ServiceMock = $this->createMock(Ec2ServiceInterface::class);

    $mock_query = $this->createMock(QueryInterface::class);
    $mock_query
      ->method('condition')
      ->willReturn($mock_query);

    $mock_query
      ->method('execute')
      ->willReturn([]);

    $mock_storage = $this->createMock(EntityStorageInterface::class);
    $mock_storage
      ->method('getQuery')
      ->willReturn($mock_query);

    $mock_storage
      ->method('loadMultiple')
      ->willReturn([]);

    $mock_entity_type_manager = $this->createMock(EntityTypeManagerInterface::class);
    $mock_entity_type_manager
      ->method('getStorage')
      ->willReturn($mock_storage);

    $mock_uuid = $this->createMock(UuidInterface::class);
    $mock_uuid
      ->method('generate')
      ->willReturn('');

    $mock_user = $this->createMock(AccountProxyInterface::class);
    $mock_config_factory = $this->createMock(ConfigFactoryInterface::class);
    $mock_cloud_config_plugin_manager = $this->createMock(CloudConfigPluginManagerInterface::class);
    $mock_entity_link_renderer = $this->createMock(EntityLinkRendererInterface::class);
    $mock_openstack_rest_mock = $this->createMock(OpenStackServiceMock::class);

    $mock_openstack_service_factory = $this->createMock(OpenStackServiceFactoryInterface::class);
    $mock_openstack_service_factory
      ->method('isEc2ServiceType')
      ->willReturn((bool) random_int(0, 1));
    $mock_cloud_service = $this->createMock(CloudServiceInterface::class);

    $this->plugin = new OpenStackLaunchTemplatePlugin(
      [], '', [],
      $this->ec2ServiceMock,
      $mock_entity_type_manager,
      $mock_uuid,
      $mock_user,
      $mock_config_factory,
      $mock_cloud_config_plugin_manager,
      $mock_entity_link_renderer,
      $mock_openstack_rest_service,
      $mock_openstack_rest_mock,
      $mock_openstack_service_factory,
      $mock_cloud_service
    );

    $this->random = new Random();
  }

  /**
   * Tests launching an instance.
   *
   * @throws \Exception
   */
  public function testLaunch(): void {
    $random = $this->random;

    // Mock object of image.
    $mock_image = $this->createMock(ImageInterface::class);
    $image_value_map = [
      ['image_id', (object) ['value' => 'ami-' . $this->getRandomId()]],
      ['root_device_type', 'ebs'],
    ];
    $mock_image
      ->method('get')
      ->willReturnMap($image_value_map);

    // Mock object of KeyPair.
    $mock_ssh_key = $this->createMock(KeyPairInterface::class);
    $ssh_key_value_map = [
      ['key_pair_name', (object) ['value' => $random->name(8, TRUE)]],
    ];
    $mock_ssh_key
      ->method('get')
      ->willReturnMap($ssh_key_value_map);

    // Mock object of Network.
    $mock_network = $this->createMock(OpenStackNetworkInterface::class);
    $network_id = 'network-' . $this->getRandomId();
    $mock_network
      ->method('getNetworkId')
      ->willReturn($network_id);

    $vpc_id = 'vpc-' . $this->getRandomId();
    $field_security_groups_test_cases = $this->createFieldSecurityGroupsTestCases($vpc_id);
    $subnet_id = 'subnet-' . $this->getRandomId();

    // Create random tags.
    $mock_tags = $this->createRandomTags();
    $openstack_tags = $this->convertToTags($mock_tags);

    // Add name tag.
    $template_name = $random->name(8, TRUE);
    $openstack_tags[] = [
      'Key' => 'Name',
      'Value' => $template_name,
    ];

    // Run test cases.
    foreach ($field_security_groups_test_cases ?: [] as $field_security_groups_test_case) {
      $mock_template = $this->createMock(CloudLaunchTemplateInterface::class);
      $template_value_map = [
        ['field_test_only', (object) ['value' => '1']], [
          'field_openstack_image_id', (object) [

            'value' => $mock_image->getImageId(),
          ],
        ],
        ['field_max_count', (object) ['value' => 1]],
        ['field_min_count', (object) ['value' => 1]],
        ['field_monitoring', (object) ['value' => '1']],
        ['field_instance_type', (object) ['value' => 'm1.nano']],
        ['field_openstack_ssh_key', (object) ['entity' => $mock_ssh_key]], [
          'field_kernel_id',
          (object) ['value' => 'aki-' . $this->getRandomId()],
        ],
        ['field_ram', (object) ['value' => 'ari-' . $this->getRandomId()]],
        ['field_user_data', (object) ['value' => $random->string(32, TRUE)]], [
          'field_os_availability_zone',
          (object) ['value' => $random->name(7, TRUE)],
        ], [
          'field_openstack_security_group',
          $this->extractArrayItem($field_security_groups_test_case, 0),
        ],
        ['field_openstack_network', (object) ['entity' => $mock_network]],
        ['field_openstack_subnet', (object) ['value' => $subnet_id]],
        ['field_instance_shutdown_behavior', (object) ['value' => '1']],
        ['field_openstack_vpc', (object) ['value' => $vpc_id]],
        ['field_iam_role', (object) ['value' => '']],
        ['field_tags', $mock_tags],
        ['field_flavor', (object) ['value' => 1]],
      ];
      $mock_template
        ->method('get')
        ->willReturnMap($template_value_map);

      $mock_template
        ->method('getName')
        ->willReturn($template_name);

      $mock_entity_type = $this->createMock(EntityTypeInterface::class);
      $mock_entity_type
        ->method('getProvider')
        ->willReturn('cloud');

      $mock_template
        ->method('getEntityType')
        ->willReturn($mock_entity_type);

      // Assert followings for the first argument of method runInstances,
      // 1. Has key SecurityGroups,
      // 2. Does not have key SecurityGroup,
      // 3. Contains the array of group1 and group2,
      // 4. Has key SubnetId,
      // 5. Contains subnet_id.
      $this->ec2ServiceMock
        ->method('runInstances')
        ->with(
          self::logicalAnd(
            self::arrayHasKey('SecurityGroupIds'),
            self::logicalNot(self::arrayHasKey('SecurityGroups')),
            // @fixme
            // self::containsEqual($this->extractArrayItem(
            // $field_security_groups_test_case, 1)),
            // .
            self::arrayHasKey('SubnetId'),
            self::containsEqual($subnet_id)
          ),
          self::equalTo($openstack_tags)
        )
        ->willReturn(TRUE);

      $return = $this->plugin->launch(
        $mock_template,
        $this->createMock(FormStateInterface::class)
      );
      self::assertNotNull($return);
    }
  }

}
