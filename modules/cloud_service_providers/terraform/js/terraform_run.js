(function () {
  'use strict';

  const updateLogs = function () {
    // Check status.
    const field_status = document.querySelector('.field--name-status .field--item');
    if (!field_status) {
      return;
    }

    if (field_status.innerHTML === 'applied' || field_status.innerHTML === 'canceled') {
      return;
    }

    const xhr = new XMLHttpRequest();
    xhr.open('GET', window.location.pathname + '/logs');
    xhr.onreadystatechange = function () {
      if (xhr.readyState !== XMLHttpRequest.DONE || xhr.status !== 200) {
        return;
      }

      const response = JSON.parse(xhr.responseText);
      const field_plan_log = document.querySelector('.field--name-plan-log');
      if (field_plan_log) {
        field_plan_log.outerHTML = response.planLog;
      }

      const field_apply_log = document.querySelector('.field--name-apply-log');
      if (field_apply_log) {
        field_apply_log.outerHTML = response.applyLog;
      }
    };

    xhr.send();
  };

  const interval = drupalSettings.terraform.terraform_js_refresh_interval || 10;
  setInterval(function () {
    updateLogs();
  }, interval * 1000);

}());
