<?php

namespace Drupal\terraform\Controller;

use Drupal\terraform\Entity\TerraformRunInterface;
use Drupal\terraform\Entity\TerraformWorkspaceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * {@inheritdoc}
 */
interface ApiControllerInterface {

  /**
   * Update all workspaces.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateAllWorkspaceList(): RedirectResponse;

  /**
   * Update all workspaces.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateWorkspaceList($cloud_context): RedirectResponse;

  /**
   * Update all run.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\terraform\Entity\TerraformWorkspaceInterface $terraform_workspace
   *   The Terraform workspace entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateRunList($cloud_context, TerraformWorkspaceInterface $terraform_workspace): RedirectResponse;

  /**
   * Update all state.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\terraform\Entity\TerraformWorkspaceInterface $terraform_workspace
   *   The Terraform workspace entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateStateList($cloud_context, TerraformWorkspaceInterface $terraform_workspace): RedirectResponse;

  /**
   * Update all variable.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\terraform\Entity\TerraformWorkspaceInterface $terraform_workspace
   *   The Terraform workspace entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateVariableList($cloud_context, TerraformWorkspaceInterface $terraform_workspace): RedirectResponse;

  /**
   * Update a run.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\terraform\Entity\TerraformWorkspaceInterface $terraform_workspace
   *   The Terraform workspace entity.
   * @param \Drupal\terraform\Entity\TerraformRunInterface $terraform_run
   *   The Terraform run entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateRun($cloud_context, TerraformWorkspaceInterface $terraform_workspace, TerraformRunInterface $terraform_run): RedirectResponse;

  /**
   * Get logs of a run.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param \Drupal\terraform\Entity\TerraformWorkspaceInterface $terraform_workspace
   *   The Terraform workspace entity.
   * @param \Drupal\terraform\Entity\TerraformRunInterface $terraform_run
   *   The Terraform run entity.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getRunLogs($cloud_context, TerraformWorkspaceInterface $terraform_workspace, TerraformRunInterface $terraform_run): JsonResponse;

}
