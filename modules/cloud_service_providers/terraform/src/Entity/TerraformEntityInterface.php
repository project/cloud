<?php

namespace Drupal\terraform\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Terraform Cloud entity.
 *
 * @ingroup terraform
 */
interface TerraformEntityInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): string;

  /**
   * {@inheritdoc}
   */
  public function setName($name): TerraformEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): TerraformEntityInterface;

}
