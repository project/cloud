<?php

namespace Drupal\terraform\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Run entity.
 *
 * @ingroup terraform
 */
interface TerraformRunInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getTerraformWorkspaceId(): string;

  /**
   * {@inheritdoc}
   */
  public function setTerraformWorkspaceId($terraform_workspace_id): TerraformRunInterface;

  /**
   * {@inheritdoc}
   */
  public function getRunId(): string;

  /**
   * {@inheritdoc}
   */
  public function setRunId($run_id): TerraformRunInterface;

  /**
   * {@inheritdoc}
   */
  public function getMessage(): string;

  /**
   * {@inheritdoc}
   */
  public function setMessage($message): TerraformRunInterface;

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): TerraformRunInterface;

  /**
   * {@inheritdoc}
   */
  public function getSource(): string;

  /**
   * {@inheritdoc}
   */
  public function setSource($source): TerraformRunInterface;

  /**
   * {@inheritdoc}
   */
  public function getTriggerReason(): string;

  /**
   * {@inheritdoc}
   */
  public function setTriggerReason($trigger_reason): TerraformRunInterface;

  /**
   * {@inheritdoc}
   */
  public function setPlanId($plan_id): TerraformRunInterface;

  /**
   * {@inheritdoc}
   */
  public function getPlanLog(): string;

  /**
   * {@inheritdoc}
   */
  public function setPlanLog($plan_log): TerraformRunInterface;

  /**
   * {@inheritdoc}
   */
  public function getApplyId(): string;

  /**
   * {@inheritdoc}
   */
  public function setApplyId($apply_id): TerraformRunInterface;

  /**
   * {@inheritdoc}
   */
  public function getApplyLog(): string;

  /**
   * {@inheritdoc}
   */
  public function setApplyLog($apply_log): TerraformRunInterface;

}
