<?php

namespace Drupal\terraform\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a State entity.
 *
 * @ingroup terraform
 */
interface TerraformStateInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getTerraformWorkspaceId(): string;

  /**
   * {@inheritdoc}
   */
  public function setTerraformWorkspaceId($terraform_workspace_id): TerraformStateInterface;

  /**
   * {@inheritdoc}
   */
  public function getStateId(): string;

  /**
   * {@inheritdoc}
   */
  public function setStateId($state_id): TerraformStateInterface;

  /**
   * {@inheritdoc}
   */
  public function getRunId(): string;

  /**
   * {@inheritdoc}
   */
  public function setRunId($run_id): TerraformStateInterface;

  /**
   * {@inheritdoc}
   */
  public function getSerialNo(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setSerialNo($serial_no): TerraformStateInterface;

  /**
   * {@inheritdoc}
   */
  public function getDetail(): string;

  /**
   * {@inheritdoc}
   */
  public function setDetail($detail): TerraformStateInterface;

}
