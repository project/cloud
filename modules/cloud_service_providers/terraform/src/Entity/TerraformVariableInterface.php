<?php

namespace Drupal\terraform\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Variable entity.
 *
 * @ingroup terraform
 */
interface TerraformVariableInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getTerraformWorkspaceId(): string;

  /**
   * {@inheritdoc}
   */
  public function setTerraformWorkspaceId($terraform_workspace_id): TerraformVariableInterface;

  /**
   * {@inheritdoc}
   */
  public function getVariableId(): string;

  /**
   * {@inheritdoc}
   */
  public function setVariableId($variable_id): TerraformVariableInterface;

  /**
   * {@inheritdoc}
   */
  public function getAttributeKey(): string;

  /**
   * {@inheritdoc}
   */
  public function setAttributeKey($attribute_key): TerraformVariableInterface;

  /**
   * {@inheritdoc}
   */
  public function getAttributeValue(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setAttributeValue($attribute_value): TerraformVariableInterface;

  /**
   * {@inheritdoc}
   */
  public function getCategory(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setCategory($category): TerraformVariableInterface;

  /**
   * {@inheritdoc}
   */
  public function getSensitive(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setSensitive($sensitive): TerraformVariableInterface;

  /**
   * {@inheritdoc}
   */
  public function getHcl(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setHcl($hcl): TerraformVariableInterface;

}
