<?php

namespace Drupal\terraform\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Workspace entity.
 *
 * @ingroup terraform
 */
interface TerraformWorkspaceInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getWorkspaceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setWorkspaceId($workspace_id): TerraformWorkspaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getAutoApply(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setAutoApply($auto_apply): TerraformWorkspaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getTerraformVersion(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setTerraformVersion($terraform_version): TerraformWorkspaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getWorkingDirectory(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setWorkingDirectory($working_directory): TerraformWorkspaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getLocked(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setLocked($locked): TerraformWorkspaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getCurrentRunId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setCurrentRunId($current_run_id): TerraformWorkspaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getCurrentRunStatus(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setCurrentRunStatus($current_run_status): TerraformWorkspaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getVcsRepoIdentifier(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setVcsRepoIdentifier($vcs_repo_identifier): TerraformWorkspaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getOauthTokenId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setOauthTokenId($oauth_token_id): TerraformWorkspaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getVcsRepoBranch(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setVcsRepoBranch($vcs_repo_branch): TerraformWorkspaceInterface;

  /**
   * {@inheritdoc}
   */
  public function getAwsCloud(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setAwsCloud($aws_cloud): TerraformWorkspaceInterface;

}
