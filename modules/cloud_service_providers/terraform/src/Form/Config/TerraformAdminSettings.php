<?php

namespace Drupal\terraform\Form\Config;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Traits\CloudFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Terraform Cloud Admin Settings.
 */
class TerraformAdminSettings extends ConfigFormBase {

  use CloudFormTrait;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * The cloud service provider plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a TerraformAdminSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityTypeManagerInterface $entity_type_manager,
    TypedConfigManagerInterface $typed_config_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->fileSystem = $file_system;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity_type.manager'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'terraform_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['terraform.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('terraform.settings');

    $form['views'] = [
      '#type' => 'details',
      '#title' => $this->t('Views'),
      '#open' => TRUE,
      '#description' => $this->t("Note that selecting the default option will overwrite View's settings."),
    ];

    $form['views']['terraform_js_refresh_interval'] = [
      '#type' => 'number',
      '#title' => 'View refresh interval',
      '#description' => $this->t('Refresh UI (Logs and etc) at periodical intervals.'),
      '#default_value' => $config->get('terraform_js_refresh_interval'),
      '#min' => 1,
      '#max' => 9999,
      '#field_suffix' => 'seconds',
    ];

    $form['views']['pager_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Pager options'),
      '#open' => TRUE,
    ];

    $form['views']['pager_options']['terraform_view_expose_items_per_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow user to control the number of items displayed in views.'),
      '#default_value' => $config->get('terraform_view_expose_items_per_page'),
      '#description' => $this->t('When enabled, an "Items per page" dropdown listbox is shown.'),
    ];

    $form['views']['pager_options']['terraform_view_items_per_page'] = [
      '#type' => 'select',
      '#options' => cloud_get_views_items_options(),
      '#title' => $this->t('Items per page'),
      '#description' => $this->t('Number of items to display on each page in views.'),
      '#default_value' => $config->get('terraform_view_items_per_page'),
    ];

    $form['cron'] = [
      '#type' => 'details',
      '#title' => $this->t('Cron'),
      '#open' => TRUE,
    ];

    $form['cron']['terraform_update_resources_queue_cron_time'] = [
      '#type' => 'number',
      '#title' => 'Update resources every',
      '#description' => $this->t('Run cron to process queue to update resources.'),
      '#default_value' => $config->get('terraform_update_resources_queue_cron_time'),
      '#min' => 1,
      '#max' => 9999,
      '#field_suffix' => 'seconds',
    ];
    $form['cron']['terraform_queue_limit'] = [
      '#type' => 'number',
      '#title' => 'Queue Limit',
      '#description' => $this->t('Number of items allowed in the update queue. New items are added when the queue count falls below this threshold.'),
      '#default_value' => $config->get('terraform_queue_limit'),
    ];
    $form['icon'] = [
      '#type' => 'details',
      '#title' => $this->t('Icon'),
      '#open' => TRUE,
    ];

    $form['icon']['terraform_cloud_config_icon'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Terraform cloud service provider icon'),
      '#default_value' => [
        'fids' => $config->get('terraform_cloud_config_icon'),
      ],
      '#description' => $this->t('Upload an image to represent Terraform.'),
      '#upload_location' => 'public://images/cloud/icons',
      '#upload_validators' => [
        'file_validate_is_image' => [],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('terraform.settings');
    $config->set('terraform_js_refresh_interval', $form_state->getValue('terraform_js_refresh_interval'));
    $config->set('terraform_update_resources_queue_cron_time', $form_state->getValue('terraform_update_resources_queue_cron_time'));
    $config->set('terraform_queue_limit', $form_state->getValue('terraform_queue_limit'));
    $config->set('terraform_view_expose_items_per_page', $form_state->getValue('terraform_view_expose_items_per_page'));
    $config->set('terraform_view_items_per_page', $form_state->getValue('terraform_view_items_per_page'));

    $icon = $form_state->getValue('terraform_cloud_config_icon');
    $fileStorage = $this->entityTypeManager->getStorage('file');
    $file = $fileStorage->load($icon[0]);
    // Save the icon.
    if (!empty($file)) {
      $file->setPermanent();
      $file->save();
      $config->set('terraform_cloud_config_icon', $icon[0]);
    }
    else {
      $config->set('terraform_cloud_config_icon', '');
    }

    $config->save();

    $views_settings = [];
    $views_settings['terraform_view_items_per_page'] = (int) $form_state->getValue('terraform_view_items_per_page');
    $views_settings['terraform_view_expose_items_per_page'] = (boolean) $form_state->getValue('terraform_view_expose_items_per_page');
    $this->updateViewsPagerOptions('terraform', $views_settings);

    parent::submitForm($form, $form_state);
  }

}
