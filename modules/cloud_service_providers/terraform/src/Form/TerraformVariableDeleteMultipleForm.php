<?php

namespace Drupal\terraform\Form;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;

/**
 * Provides an entities deletion confirmation form.
 */
class TerraformVariableDeleteMultipleForm extends TerraformDeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): TrustedCallbackInterface {
    return new Url(
      'entity.' . $this->entityTypeId . '.collection', [
        'cloud_context' => $this->routeMatch->getParameter('cloud_context'),
        'terraform_workspace' => $this->routeMatch->getParameter('terraform_workspace'),
      ]
    );
  }

}
