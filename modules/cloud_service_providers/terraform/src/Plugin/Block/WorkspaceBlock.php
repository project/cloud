<?php

namespace Drupal\terraform\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\cloud\Traits\CloudResourceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block displaying Terraform workspaces.
 *
 * @Block(
 *   id = "terraform_workspace_block",
 *   admin_label = @Translation("Terraform workspaces"),
 *   category = @Translation("Terraform")
 * )
 */
class WorkspaceBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use CloudContentEntityTrait;
  use CloudResourceTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The cloud config plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  protected $entityLinkRenderer;

  /**
   * Creates a ResourcesBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud config plugin manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    DateFormatterInterface $date_formatter,
    EntityLinkRendererInterface $entity_link_renderer,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->dateFormatter = $date_formatter;
    $this->entityLinkRenderer = $entity_link_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('date.formatter'),
      $container->get('entity.link_renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'cloud_context' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['cloud_context'] = [
      '#type' => 'select',
      '#title' => $this->t('Cloud service provider'),
      '#description' => $this->t('Select cloud service provider.'),
      '#options' => $this->cloudConfigPluginManager->getCloudConfigs($this->t('All Terraform regions'), 'terraform'),
      '#default_value' => $config['cloud_context'] ?? '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['cloud_context'] = $form_state->getValue('cloud_context');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];

    $build['workspaces'] = [
      '#type' => 'details',
      '#title' => $this->t('Workspaces'),
      '#open' => TRUE,
    ];

    $build['workspaces']['table'] = $this->buildTable();
    $build['workspaces']['#attached'] = [
      'library' => [
        'cloud/datatables',
      ],
    ];
    $build['workspaces']['#attributes']['class'][] = 'simple-datatable';

    return $build;
  }

  /**
   * Build workspace table.
   *
   * @return array
   *   Table array ready for rendering.
   */
  private function buildTable(): array {
    return [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Repository Identifier'),
        $this->t('Run'),
        $this->t('State'),
        $this->t('Last Change'),
      ],
      '#empty' => $this->t('No workspaces found.'),
      '#attributes' => [
        'class' => [
          'simple-datatable',
        ],
      ],
      '#rows' => $this->getWorkspaceRows(),
    ];
  }

  /**
   * Get workspaces from database and format as table row.
   *
   * @return array
   *   Array of workspaces.
   */
  private function getWorkspaceRows(): array {
    $rows = [];
    $entities = $this->runEntityQuery('terraform_workspace', []);
    /** @var \Drupal\terraform\Entity\TerraformWorkspace $entity */
    foreach ($entities ?: [] as $entity) {
      if (!$this->currentUser->hasPermission('view terraform workspace') ||
        !$this->currentUser->hasPermission('view ' . $entity->getCloudContext())) {
        continue;
      }
      $link = $entity->getName();
      try {
        $link = $entity->toLink($entity->getName());
      }
      catch (\Exception $e) {
        $this->messenger()->addError("An error occurred: {$e->getMessage()}");
      }

      $rows[] = [
        'data' => [
          $link,
          $entity->getVcsRepoIdentifier(),
          [
            'data' => $this->entityLinkRenderer->renderViewElement(
              $entity->getCurrentRunId(),
              'terraform_run',
              'run_id',
              [],
              '',
              '',
              '',
              $entity->getCloudContext(),
              'terraform_workspace',
              'id'
            ),
          ],
          $entity->getCurrentRunStatus() ?? '',
          !empty($entity->getCurrentRunId()) ?
          $this->getFormattedRunDate($entity->getCurrentRunId(), $entity->getCloudContext())
            : '',
        ],
      ];
    }
    return $rows;
  }

  /**
   * Get a formatted run date from the terraform_run entity.
   *
   * @param string $run_id
   *   Run id.
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return string
   *   Formatted date string.
   */
  private function getFormattedRunDate(string $run_id, string $cloud_context): string {
    // Get the Terraform run entity.
    $runs = $this->runEntityQuery('terraform_run', [
      'cloud_context' => $cloud_context,
      'run_id' => $run_id,
    ]);

    if (empty($runs)) {
      return '';
    }

    /** @var \Drupal\terraform\Entity\TerraformRun $run */
    $run = reset($runs);
    return $this->dateFormatter->format($run->created(), 'short');
  }

}
