<?php

namespace Drupal\terraform\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a workspace operations bulk form element.
 *
 * @ViewsField("workspace_bulk_form")
 */
class TerraformWorkspaceBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No Workspace selected.');
  }

}
