<?php

namespace Drupal\Tests\terraform\Functional\Config;

use Drupal\Tests\terraform\Functional\TerraformTestBase;
use Drupal\Tests\terraform\Traits\TerraformTestFormDataTrait;

/**
 * Test Case class for Terraform Cloud Admin Setting forms.
 *
 * @group Terraform
 */
class TerraformAdminSettingsTest extends TerraformTestBase {

  use TerraformTestFormDataTrait;

  public const TERRAFORM_ADMIN_SETTINGS_REPEAT_COUNT = 2;

  /**
   * Get permissions of login user.
   *
   * @return array
   *   permissions of login user.
   */
  protected function getPermissions(): array {
    return [
      'administer terraform',
      'administer site configuration',
    ];
  }

  /**
   * Test for TerraformAdminSettings.
   *
   * @param string $setting_type
   *   The setting type id.
   * @param array $edit
   *   The array of input data.
   */
  protected function runTerraformAdminSettings($setting_type, array $edit): void {
    $this->drupalGet("/admin/config/services/cloud/terraform/{$setting_type}");
    $this->assertNoErrorMessage();

    $this->submitForm(
      $edit,
      $this->t('Save')->render()
    );
    $this->assertNoErrorMessage();
  }

  /**
   * Test Terraform Admin Setting forms on Settings.
   */
  public function testTerraformAdminSettings(): void {
    $edit = $this->createTerraformSettingsTestFormData(self::TERRAFORM_ADMIN_SETTINGS_REPEAT_COUNT);

    for ($i = 0; $i < self::TERRAFORM_ADMIN_SETTINGS_REPEAT_COUNT; $i++) {
      $edit[$i] = array_intersect_key($edit[$i], array_flip(
        (array) array_rand($edit[$i], random_int(1, count($edit[$i])))
      ));
      $this->runTerraformAdminSettings('settings', $edit[$i]);
    }
  }

}
