<?php

namespace Drupal\Tests\terraform\Functional;

use Drupal\Tests\cloud\Traits\CloudConfigTestEntityTrait;

/**
 * Tests Terraform Cloud workspace.
 *
 * @group Terraform
 */
class TerraformWorkspaceTest extends TerraformTestBase {

  use CloudConfigTestEntityTrait;

  public const TERRAFORM_WORKSPACE_REPEAT_COUNT = 1;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list terraform workspace',
      'add terraform workspace',
      'view terraform workspace',
      'edit terraform workspace',
      'delete terraform workspace',
    ];
  }

  /**
   * Tests CRUD for Workspace.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testWorkspace(): void {

    $cloud_context = $this->cloudContext;
    // List Workspace for Terraform Cloud.
    $this->drupalGet("/clouds/terraform/$cloud_context/workspace");
    $this->assertNoErrorMessage();

    // Add a new Workspace.
    $add = $this->createWorkspaceTestFormData(self::TERRAFORM_WORKSPACE_REPEAT_COUNT);
    for ($i = 0; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT; $i++) {
      $this->reloadMockData();

      $this->addWorkspaceMockData($add[$i]);

      $this->drupalGet("/clouds/terraform/$cloud_context/workspace/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Workspace', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/terraform/$cloud_context/workspace");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all workspace listing exists.
      $this->drupalGet('/clouds/terraform/workspace');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($add[$j]['name']);
      }
    }

    // Delete Workspace.
    for ($i = 0, $num = 1; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT; $i++, $num++) {

      $this->deleteWorkspaceMockData($add[$i]);

      $this->drupalGet("/clouds/terraform/$cloud_context/workspace/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'Workspace', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/terraform/$cloud_context/workspace");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting workspaces with bulk operation.
   *
   * @throws \Exception
   */
  public function testWorkspaceBulk(): void {

    for ($i = 0; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT; $i++) {
      // Create Workspaces.
      $workspaces = $this->createWorkspacesRandomTestFormData();
      $entities = [];
      foreach ($workspaces ?: [] as $workspace) {
        $entities[] = $this->createWorkspaceTestEntity($workspace);
      }
      $this->deleteWorkspaceMockData($workspaces[0]);
      $this->runTestEntityBulk('workspace', $entities);
    }
  }

  /**
   * Test updating all workspace list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testUpdateAllWorkspaceList(): void {
    $cloud_configs = [];

    // List Workspace for Terraform Cloud.
    $this->drupalGet('/clouds/terraform/workspace');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createTerraformCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new Workspace.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createWorkspaceTestFormData(self::TERRAFORM_WORKSPACE_REPEAT_COUNT);
      for ($i = 0; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT; $i++) {
        // $this->reloadMockData();
        $this->addWorkspaceMockData($add[$i]);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/terraform/workspace');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm Workspaces are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Workspaces',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    $num = 1;
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/terraform/{$cloud_context}/workspace/{$num}");
        $this->assertSession()->linkExists($this->t('Edit'));
        $this->assertSession()->linkByHrefExists("/clouds/terraform/{$cloud_context}/workspace/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/terraform/{$cloud_context}/workspace/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List Terraform Cloud Workspace'));
        // Click 'List Terraform Cloud Workspace'.
        $this->clickLink($this->t('List Terraform Cloud Workspace'));
        $this->assertNoErrorMessage();

        // Confirm the edit view.
        $this->drupalGet("/clouds/terraform/{$cloud_context}/workspace/{$num}/edit");
        $this->assertSession()->linkNotExists($this->t('Edit'));
      }
    }

    // Add a new Workspace.
    $num++;
    $data = [
      'name' => "Workspace-{$this->random->name(8, TRUE)}- " . date('Y/m/d H:i:s'),
    ];
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->addWorkspaceMockData($data);
    }

    // Make sure listing.
    $this->drupalGet('/clouds/terraform/workspace');
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextNotContains($data['name']);

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm Workspaces are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Workspaces',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Delete Workspace in mock data.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT + 1; $i++) {
        $this->deleteWorkspaceMockData($data);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/terraform/workspace');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::TERRAFORM_WORKSPACE_REPEAT_COUNT + 1; $i++) {
      $this->assertSession()->pageTextContains($data['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm Workspaces are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Workspaces',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }
  }

}
