<?php

namespace Drupal\Tests\terraform\Traits;

use Drupal\Tests\cloud\Traits\CloudTestEntityTrait;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\terraform\Entity\TerraformVariable;
use Drupal\terraform\Entity\TerraformWorkspace;

/**
 * The trait creating test entity for terraform testing.
 */
trait TerraformTestEntityTrait {

  use CloudTestEntityTrait;

  /**
   * Create a Terraform Config test entity.
   *
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return object
   *   The Cloud Config entity.
   *
   * @throws \Exception
   */
  protected function createTerraformCloudConfigTestEntity($cloud_context): CloudConfig {
    $random = $this->random;
    return $this->createTestEntity(CloudConfig::class, [
      'type'                      => 'terraform',
      'cloud_context'             => $cloud_context,
      'name'                      => sprintf('Terraform Cloud - %s', $random->name(8, TRUE)),
      'field_organization'        => $random->name(32, TRUE),
      'field_terraform_api_token' => $random->name(128, TRUE),
    ]);
  }

  /**
   * Create a Terraform Cloud Workspace test entity.
   *
   * @param array $workspace
   *   The workspace data.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The role binding entity.
   */
  protected function createWorkspaceTestEntity(array $workspace): CloudContentEntityBase {
    return $this->createTestEntity(TerraformWorkspace::class, $workspace);
  }

  /**
   * Create a Terraform Cloud Variable test entity.
   *
   * @param array $variable
   *   The variable data.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The role binding entity.
   */
  protected function createVariableTestEntity(array $variable): CloudContentEntityBase {
    return $this->createTestEntity(TerraformVariable::class, $variable);
  }

}
