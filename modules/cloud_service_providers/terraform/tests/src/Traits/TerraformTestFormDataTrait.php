<?php

namespace Drupal\Tests\terraform\Traits;

use Drupal\Component\Utility\Random;

/**
 * The trait creating form data for terraform testing.
 */
trait TerraformTestFormDataTrait {

  /**
   * Create test data for cloud service provider (CloudConfig).
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createCloudConfigTestFormData($repeat_count): array {
    $random = new Random();

    // Input Fields.
    $data = [];
    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      $data[] = [
        'name[0][value]'                      => sprintf('conf#%d', $num),
        'cloud_context'                       => strtolower($random->name(16, TRUE)),
        'field_organization[0][value]'        => $random->name(32, TRUE),
        'field_terraform_api_token[0][value]' => $random->name(128, TRUE),
      ];
    }

    return $data;
  }

  /**
   * Create test data for terraform workspace.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createWorkspaceTestFormData($repeat_count): array {
    $random = new Random();

    // Input Fields.
    $data = [];
    for ($i = 0; $i < $repeat_count; $i++) {
      $data[] = [
        'name' => sprintf('Workspace-%s - %s', $random->name(8, TRUE), date('Y/m/d H:i:s')),
        'vcs_repo_identifier' => $random->name(16, TRUE),
        'oauth_token_id' => $random->name(8, TRUE),
      ];
    }

    return $data;
  }

  /**
   * Create random workspaces data.
   *
   * @return array
   *   Random workspaces.
   *
   * @throws \Exception
   */
  protected function createWorkspacesRandomTestFormData(): array {
    $workspaces = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $workspaces[] = [
        // 'cloud_context' needs to associate a fixed $cloud_context since the
        // workspace entities belong to a specific $cloud_context.
        // The Terraform Cloud organization is equal to a cloud service provider
        // (CloudConfig), which is $cloud_context here.
        'cloud_context' => $this->cloudContext,
        'name' => sprintf('workspace #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(4, TRUE)),
      ];
    }

    return $workspaces;
  }

  /**
   * Create test data for terraform variable.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createVariableTestFormData($repeat_count): array {
    $random = new Random();

    // Input Fields.
    $data = [];
    for ($i = 0; $i < $repeat_count; $i++) {
      $data[] = [
        'attribute_key' => sprintf('Variable-%s - %s', $random->name(8, TRUE), date('Y/m/d H:i:s')),
        'attribute_value' => $random->name(8, TRUE),
      ];
    }

    return $data;
  }

  /**
   * Create random variable data.
   *
   * @return array
   *   Random variables.
   *
   * @throws \Exception
   */
  protected function createVariablesRandomTestFormData(): array {
    $variables = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $variables[] = [
        // 'cloud_context' needs to associate a fixed $cloud_context since the
        // variable entities belong to a specific $cloud_context.
        // The Terraform Cloud organization is equal to a cloud service provider
        // (CloudConfig), which is $cloud_context here.
        'cloud_context' => $this->cloudContext,
        'name' => sprintf('workspace #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(4, TRUE)),
        'terraform_workspace_id' => 1,
      ];
    }

    return $variables;
  }

  /**
   * Get the items per page on Pager options, Views, VMware Settings.
   *
   * @return array
   *   Array of items per page.
   */
  protected function getItemsPerPage(): array {
    return [10, 15, 20, 25, 50, 100];
  }

  /**
   * Create an array of random input data for Settings.
   *
   * @return array
   *   The array including random input data.
   */
  protected function createTerraformSettingsTestFormData($repeat_count = 1): array {
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('terraform.settings');

    $items_per_page = $this->getItemsPerPage();

    $edit = [];
    for ($i = 0; $i < $repeat_count; $i++) {
      $edit[] = [
        'terraform_js_refresh_interval' => random_int(1, 9999),
        'terraform_view_expose_items_per_page' => !$config->get('terraform_view_expose_items_per_page'),
        'terraform_view_items_per_page' => $items_per_page[array_rand($items_per_page)],
        'terraform_update_resources_queue_cron_time' => random_int(1, 9999),
        'terraform_queue_limit' => random_int(1, 50),
      ];
    }

    return $edit;
  }

}
