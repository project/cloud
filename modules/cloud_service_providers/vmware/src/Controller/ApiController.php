<?php

namespace Drupal\vmware\Controller;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\cloud\Controller\CloudApiControllerBase;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Traits\CloudResourceTrait;
use Drupal\vmware\Service\VmwareOperationsService;
use Drupal\vmware\Service\VmwareServiceException;
use Drupal\vmware\Service\VmwareServiceInterface;
use Drupal\vmware\Service\VmwareVmGuestOsDataProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller responsible for "update" URLs.
 *
 * This class is mainly responsible for
 * updating the vmware entities from urls.
 */
class ApiController extends CloudApiControllerBase implements ApiControllerInterface {

  use CloudResourceTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The VMware Service.
   *
   * @var \Drupal\vmware\Service\VmwareServiceInterface
   */
  private $vmwareService;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * Cloud service interface.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The configuration data.
   *
   * @var array
   */
  protected $configuration = ['cloud_context' => ''];

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * ApiController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\vmware\Service\VmwareServiceInterface $vmware_service
   *   Object for interfacing with VMware API.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger Object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   Cloud service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    VmwareServiceInterface $vmware_service,
    Messenger $messenger,
    RequestStack $request_stack,
    RendererInterface $renderer,
    Connection $database,
    CloudServiceInterface $cloud_service,
    AccountInterface $current_user,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->vmwareService = $vmware_service;
    $this->messenger = $messenger;
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->database = $database;
    $this->cloudService = $cloud_service;
    $this->currentUser = $current_user;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return ApiController
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('vmware'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('database'),
      $container->get('cloud'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateVmList($cloud_context): RedirectResponse {
    return $this->updateEntityList('vm', 'vms', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAllVmList(): RedirectResponse {
    return $this->updateAllEntityList('vm', 'vms');
  }

  /**
   * {@inheritdoc}
   */
  public function updateHostList($cloud_context): RedirectResponse {
    return $this->updateEntityList('host', 'hosts', $cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAllHostList(): RedirectResponse {
    return $this->updateAllEntityList('host', 'hosts');
  }

  /**
   * Helper method to update entities.
   *
   * @param string $entity_type_name
   *   The entity type name.
   * @param string $entity_type_name_plural
   *   The plural format of entity type name.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return array
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   */
  private function updateEntityList($entity_type_name, $entity_type_name_plural, $cloud_context): RedirectResponse {
    $entity_type_name_capital_plural = self::getCamelCaseWithoutWhitespace($entity_type_name_plural);

    $this->vmwareService->setCloudContext($cloud_context);
    try {
      $this->vmwareService->login();
      $update_method_name = 'update' . $entity_type_name_capital_plural;
      $updated = $this->vmwareService->$update_method_name();
    }
    catch (VmwareServiceException $e) {
      $updated = FALSE;
    }

    $entity_type = $this->entityTypeManager->getDefinition('vmware_' . $entity_type_name);

    if ($updated !== FALSE) {
      $this->messageUser($this->t('Updated @name.', ['@name' => $entity_type->getPluralLabel()]));
    }
    else {
      $this->messageUser($this->t('Unable to update @name.', ['@name' => $entity_type->getPluralLabel()]), 'error');
    }

    // Update the cache.
    $this->cloudService->invalidateCacheTags();

    return $this->redirect("view.vmware_$entity_type_name.list", [
      'cloud_context' => $cloud_context,
    ]);
  }

  /**
   * Helper method to update all entities.
   *
   * @param string $short_entity_type_name
   *   The short entity type name.
   * @param string $short_entity_type_name_plural
   *   The plural format of short entity type name.
   *
   * @return array
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   */
  private function updateAllEntityList($short_entity_type_name, $short_entity_type_name_plural): RedirectResponse {
    $short_entity_type_name_capital_plural = self::getCamelCaseWithoutWhitespace($short_entity_type_name_plural);
    $entity_type_name = "vmware_$short_entity_type_name";
    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => 'vmware',
      ]);
    $labels = $this->getDisplayLabels($entity_type_name);
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $this->vmwareService->setCloudContext($cloud_context);
      $this->vmwareService->login();
      $update_method_name = 'update' . $short_entity_type_name_capital_plural;
      $updated = $this->vmwareService->$update_method_name();
      $resource_link = Link::fromTextAndUrl(
        $labels['plural'],
        Url::fromRoute(
          "entity.$entity_type_name.collection",
          ['cloud_context' => $cloud_context]
        )
      )->toString();
      $cloud_config_link = $cloud_config->getCloudConfigLink();

      if ($updated === TRUE) {
        $this->messageUser($this->t('Updated %resource_link of %cloud_config_link cloud service provider.', [
          '%resource_link' => $resource_link,
          '%cloud_config_link' => $cloud_config_link ?? $cloud_config->getName(),
        ]));
      }
      else {
        $this->messageUser($this->t('Unable to update %resource_link of %cloud_config_link cloud service provider.', [
          '%resource_link' => $resource_link,
          '%cloud_config_link' => $cloud_config_link ?? $cloud_config->getName(),
        ]), 'error'
        );
      }
    }

    $this->cloudService->invalidateCacheTags();
    return $this->redirect("view.$entity_type_name.all");
  }

  /**
   * Helper method to add messages for the end user.
   *
   * @param string $message
   *   The message.
   * @param string $type
   *   The message type: error or message.
   */
  private function messageUser($message, $type = 'message'): void {
    switch ($type) {
      case 'error':
        $this->messenger->addError($message);
        break;

      case 'message':
        $this->messenger->addStatus($message);
        break;

      default:
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityCount(string $cloud_context, string $entity_type_id): JsonResponse {
    $params = !empty($cloud_context)
      ? ['cloud_context' => $cloud_context] : [];
    if (!empty($cloud_context)) {
      $this->configuration['cloud_context'] = $cloud_context;
    }
    $count = $this->getResourceCount(
      $entity_type_id,
      // Use a static trait method through CloudService.
      'list ' . CloudService::convertUnderscoreToWhitespace($entity_type_id),
      $params
    );

    return new JsonResponse(['count' => $count]);
  }

  /**
   * {@inheritdoc}
   */
  public function operateEntity(Request $request, string $cloud_context, string $entity_type_id, string $entity_id, string $command): JsonResponse {
    $entity = $command !== 'create'
      ? $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->load($entity_id)
      : $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->create([
          'cloud_context' => $cloud_context,
        ]);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The instance has already been deleted.',
      ], 404);
    }

    $method_name = '';
    $parameter = [];
    switch ($command) {
      case 'create':
        switch ($entity_type_id) {
          case 'vmware_vm':
            $method_name = 'createVm';
            $parameter = [
              'cloud_context' => $cloud_context,
              'datastore' => $request->get('datastore', ''),
              'host' => $request->get('host', ''),
              'folder' => $request->get('folder', ''),
            ];
            /** @var \Drupal\vmware\Entity\VmwareVm $entity */
            $entity->setName($request->get('name', ''));
            $entity->setGuestOs($request->get('guest_os', ''));
            $entity->setCpuCount($request->get('cpu_count', 1));
            $entity->setMemorySize($request->get('memory_size', 4096));
            $entity->setDiskSize(json_decode($request->get('disk_size', '[]'), TRUE, 512, JSON_THROW_ON_ERROR));
            break;
        }
        break;

      case 'edit':
        switch ($entity_type_id) {
          case 'vmware_vm':
            $method_name = 'editVm';
            $parameter = [
              'cpu_count' => $request->get('cpu_count', 1),
              'memory_size' => $request->get('memory_size', 4096),
              'disk_size' => json_decode($request->get('disk_size', '[]'), TRUE, 512, JSON_THROW_ON_ERROR),
            ];
            /** @var \Drupal\vmware\Entity\VmwareVm $entity */
            $entity->setCpuCount($parameter['cpu_count']);
            $entity->setMemorySize($parameter['memory_size']);
            $entity->setDiskSize($parameter['disk_size']);
            break;
        }
        break;

      case 'reboot':
        switch ($entity_type_id) {
          case 'vmware_vm':
            $method_name = 'rebootVm';
            break;
        }
        break;

      case 'delete':
        switch ($entity_type_id) {
          case 'vmware_vm':
            $method_name = 'deleteVm';
            break;
        }
        break;

      case 'start':
        switch ($entity_type_id) {
          case 'vmware_vm':
            $method_name = 'startVm';
            break;
        }
        break;

      case 'stop':
        switch ($entity_type_id) {
          case 'vmware_vm':
            $method_name = 'stopVm';
            break;
        }
        break;

      case 'suspend':
        switch ($entity_type_id) {
          case 'vmware_vm':
            $method_name = 'suspendVm';
            break;
        }
        break;
    }

    /** @var /Drupal\Core\Entity\ContentEntityBase $entity */
    $violations = $entity->validate();
    if ($violations->count() > 0) {
      $violationList = [];
      foreach ($violations as $violation) {
        $fieldName = $violation->getParameters()['@field_name'];
        $violationList[$fieldName] = $violation->getMessage()->render();
      }
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The value entered in the form is invalid.',
        'violationList' => $violationList,
      ], 400);
    }

    $result = FALSE;
    if (!empty($method_name)) {
      $service = new VmwareOperationsService(
        $this->vmwareService
      );
      if (method_exists($service, $method_name)) {
        $result = $service->$method_name($entity, $parameter);
      }
    }
    $this->messenger()->deleteAll();

    return !empty($result)
      ? new JsonResponse([
        'result' => 'OK',
        'id' => $entity->id(),
      ], 200)
      : new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
      ], 500);
  }

  /**
   * {@inheritdoc}
   */
  public function getFolders(string $cloud_context): JsonResponse {
    $this->vmwareService->setCloudContext($cloud_context);
    $this->vmwareService->login();
    $folders = $this->vmwareService->describeFolders();

    $filtered_folders = [];
    foreach ($folders['value'] ?: [] as $folder) {
      if ($folder['type'] !== 'VIRTUAL_MACHINE') {
        continue;
      }
      $filtered_folders[] = [
        'value' => $folder['folder'],
        'label' => $folder['name'],
      ];
    }

    return new JsonResponse($filtered_folders, 200);
  }

  /**
   * Convert the unit.
   *
   * @param int $value
   *   The Value.
   *
   * @return string
   *   Converted value string.
   */
  private function convert(int $value): string {
    if ($value < 1024) {
      return '' . $value;
    }
    if ($value < 1024 * 1024) {
      return round($value / 1024, 2) . 'Ki';
    }
    if ($value < 1024 * 1024 * 1024) {
      return round($value / 1024 / 1024, 2) . 'Mi';
    }
    if ($value < 1024 * 1024 * 1024 * 1024) {
      return round($value / 1024 / 1024 / 1024, 2) . 'Gi';
    }
    return round($value / 1024 / 1024 / 1024 / 1024, 2) . 'Ti';
  }

  /**
   * {@inheritdoc}
   */
  public function getDatastores(string $cloud_context): JsonResponse {
    $this->vmwareService->setCloudContext($cloud_context);
    $this->vmwareService->login();
    $folders = $this->vmwareService->describeDatastores();

    $filtered_folders = [];
    foreach ($folders['value'] ?: [] as $folder) {
      $filtered_folders[] = [
        'value' => $folder['datastore'],
        'label' => $folder['name'] . ' (Free: ' . $this->convert($folder['free_space']) . ' / Capacity: ' . $this->convert($folder['capacity']) . ')',
      ];
    }

    return new JsonResponse($filtered_folders, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getGuestOses(): JsonResponse {
    $service = new VmwareVmGuestOsDataProvider();
    $options = $service->getOptions();
    $filtered_options = [];
    foreach ($options ?: [] as $group => $oses) {
      foreach ($oses ?: [] as $value => $label) {
        $filtered_options[] = [
          'value' => $value,
          'label' => $label,
          'group' => $group,
        ];
      }
    }
    return new JsonResponse($filtered_options, 200);
  }

}
