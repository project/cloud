<?php

namespace Drupal\vmware\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * {@inheritdoc}
 */
interface ApiControllerInterface {

  /**
   * Update all VMs.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateVmList($cloud_context): RedirectResponse;

  /**
   * Update all VMs.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateAllVmList(): RedirectResponse;

  /**
   * Update all Hosts.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateHostList($cloud_context): RedirectResponse;

  /**
   * Update all Hosts.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function updateAllHostList(): RedirectResponse;

  /**
   * Get the count of VMware entities.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getEntityCount(string $cloud_context, string $entity_type_id): JsonResponse;

  /**
   * Operate VMware entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $entity_id
   *   The entity ID.
   * @param string $command
   *   Operation command.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function operateEntity(Request $request, string $cloud_context, string $entity_type_id, string $entity_id, string $command): JsonResponse;

  /**
   * Get folders' information.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getFolders(string $cloud_context): JsonResponse;

  /**
   * Get data stores' information.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getDatastores(string $cloud_context): JsonResponse;

  /**
   * Get Guest OSes' information.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getGuestOses(): JsonResponse;

}
