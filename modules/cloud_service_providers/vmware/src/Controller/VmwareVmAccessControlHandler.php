<?php

namespace Drupal\vmware\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Traits\AccessCheckTrait;

/**
 * Access controller for the VmwareVm entity.
 *
 * @see \Drupal\vmware\Entity\VmwareVm.
 */
class VmwareVmAccessControlHandler extends EntityAccessControlHandler {

  use AccessCheckTrait;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {

    switch ($operation) {
      case 'start':
        if ($entity->getPowerState() === 'POWERED_OFF' || $entity->getPowerState() === 'SUSPENDED') {
          return $this->allowedIfCanAccessCloudConfigWithOwner(
            $entity,
            $account,
            'edit own vmware vm',
            'edit any vmware vm'
          );
        }
        break;

      case 'stop':
        if ($entity->getPowerState() === 'POWERED_ON' || $entity->getPowerState() === 'SUSPENDED') {
          return $this->allowedIfCanAccessCloudConfigWithOwner(
            $entity,
            $account,
            'edit own vmware vm',
            'edit any vmware vm'
          );
        }
        break;

      case 'reboot':
      case 'suspend':
        if ($entity->getPowerState() === 'POWERED_ON') {
          return $this->allowedIfCanAccessCloudConfigWithOwner(
            $entity,
            $account,
            'edit own vmware vm',
            'edit any vmware vm'
          );
        }
        break;

      case 'view':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'view own vmware vm',
          'view any vmware vm'
        );

      case 'update':
      case 'edit':
        return $this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $account,
          'edit own vmware vm',
          'edit any vmware vm'
        );

      case 'delete':
        if ($entity->getPowerState() !== 'POWERED_ON') {
          return $this->allowedIfCanAccessCloudConfigWithOwner(
            $entity,
            $account,
            'delete own vmware vm',
            'delete any vmware vm'
          );
        }
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    return $this->allowedIfCanAccessCloudConfig(
      NULL,
      $account,
      'add vmware vm'
    );
  }

}
