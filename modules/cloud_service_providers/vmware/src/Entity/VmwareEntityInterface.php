<?php

namespace Drupal\vmware\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a VMware entity.
 *
 * @ingroup vmware
 */
interface VmwareEntityInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getName(): string;

  /**
   * {@inheritdoc}
   */
  public function setName($name): VmwareEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getTagCategories(): array;

  /**
   * {@inheritdoc}
   */
  public function setTagCategories(array $tag_categories): VmwareEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function getTags(): array;

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): VmwareEntityInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

  /**
   * {@inheritdoc}
   */
  public function getRefreshed(): string;

  /**
   * {@inheritdoc}
   */
  public function setRefreshed($time): VmwareEntityInterface;

}
