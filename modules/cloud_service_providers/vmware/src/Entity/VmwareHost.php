<?php

namespace Drupal\vmware\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the VMware Host entity.
 *
 * @ingroup vmware
 *
 * @ContentEntityType(
 *   id = "vmware_host",
 *   id_plural = "vmware_hosts",
 *   label = @Translation("Host"),
 *   label_collection = @Translation("Hosts"),
 *   label_singular = @Translation("Host"),
 *   label_plural = @Translation("Hosts"),
 *   handlers = {
 *     "view_builder" = "Drupal\vmware\Entity\VmwareHostViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\vmware\Entity\VmwareHostViewsData",
 *     "access"       = "Drupal\vmware\Controller\VmwareHostAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "vmware_host",
 *   admin_permission = "administer vmware host",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical"            = "/clouds/vmware/{cloud_context}/host/{vmware_host}",
 *     "collection"           = "/clouds/vmware/{cloud_context}/host",
 *   },
 *   field_ui_base_route = "vmware_host.settings",
 *   resource_type = "HostSystem"
 * )
 */
class VmwareHost extends VmwareEntityBase implements VmwareHostInterface {

  /**
   * {@inheritdoc}
   */
  public function getHost(): string {
    return $this->get('host')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHost($host): VmwareHostInterface {
    return $this->set('host', $host);
  }

  /**
   * {@inheritdoc}
   */
  public function getPowerState(): string {
    return $this->get('power_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPowerState($power_state): VmwareHostInterface {
    return $this->set('power_state', $power_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectionState(): string {
    return $this->get('connection_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setConnectionState($connection_state): VmwareHostInterface {
    return $this->set('connection_state', $connection_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = VmwareEntityBase::baseFieldDefinitions($entity_type);

    $fields['host'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Host'))
      ->setDescription(t('Identifier of the host.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['power_state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Power State'))
      ->setDescription(t('Power state of the virtual machine.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['connection_state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Connection State'))
      ->setDescription(t('Connection status of the host.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    return $fields;
  }

}
