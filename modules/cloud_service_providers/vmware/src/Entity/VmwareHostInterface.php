<?php

namespace Drupal\vmware\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a VMware Host entity.
 *
 * @ingroup vmware
 */
interface VmwareHostInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getHost(): string;

  /**
   * {@inheritdoc}
   */
  public function setHost($host): VmwareHostInterface;

  /**
   * {@inheritdoc}
   */
  public function getPowerState(): string;

  /**
   * {@inheritdoc}
   */
  public function setPowerState($power_state): VmwareHostInterface;

  /**
   * {@inheritdoc}
   */
  public function getConnectionState(): string;

  /**
   * {@inheritdoc}
   */
  public function setConnectionState($connection_state): VmwareHostInterface;

}
