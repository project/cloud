<?php

namespace Drupal\vmware\Entity;

use Drupal\cloud\Entity\CloudViewBuilder;

/**
 * Provides the Host view builders.
 */
class VmwareHostViewBuilder extends CloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'group_host',
        'title' => $this->t('Host'),
        'open' => TRUE,
        'fields' => [
          'name',
          'host',
          'power_state',
          'connection_state',
          'created',
        ],
      ],
      [
        'name' => 'fieldset_tags',
        'title' => $this->t('Tag Information'),
        'open' => TRUE,
        'fields' => [
          'tag_categories',
          'tags',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
