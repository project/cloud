<?php

namespace Drupal\vmware\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the VMware Host entity type.
 */
class VmwareHostViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();
    $base_table = $this->entityType->getBaseTable() ?: $this->entityType->id();
    // Additional information for Views integration, such as table joins, can be
    // put here.
    // Add access check query tag.
    $data[$base_table]['table']['base']['access query tag'] = 'vmware_entity_views_access';
    return $data;
  }

}
