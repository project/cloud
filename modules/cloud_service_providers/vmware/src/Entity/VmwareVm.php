<?php

namespace Drupal\vmware\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the VMware VM entity.
 *
 * @ingroup vmware
 *
 * @ContentEntityType(
 *   id = "vmware_vm",
 *   id_plural = "vmware_vms",
 *   label = @Translation("VM"),
 *   label_collection = @Translation("VMs"),
 *   label_singular = @Translation("VM"),
 *   label_plural = @Translation("VMs"),
 *   handlers = {
 *     "view_builder" = "Drupal\vmware\Entity\VmwareVmViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudContentListBuilder",
 *     "views_data"   = "Drupal\vmware\Entity\VmwareVmViewsData",
 *     "access"       = "Drupal\vmware\Controller\VmwareVmAccessControlHandler",
 *     "form" = {
 *       "add"        = "Drupal\vmware\Form\VmwareVmCreateForm",
 *       "edit"       = "Drupal\vmware\Form\VmwareVmEditForm",
 *       "start"      = "Drupal\vmware\Form\VmwareVmStartForm",
 *       "stop"       = "Drupal\vmware\Form\VmwareVmStopForm",
 *       "reboot"     = "Drupal\vmware\Form\VmwareVmRebootForm",
 *       "suspend"    = "Drupal\vmware\Form\VmwareVmSuspendForm",
 *       "delete"     = "Drupal\vmware\Form\VmwareVmDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\vmware\Form\VmwareDeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "vmware_vm",
 *   admin_permission = "administer vmware vm",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical"            = "/clouds/vmware/{cloud_context}/vm/{vmware_vm}",
 *     "collection"           = "/clouds/vmware/{cloud_context}/vm",
 *     "add-form"             = "/clouds/vmware/{cloud_context}/vm/add",
 *     "edit-form"            = "/clouds/vmware/{cloud_context}/vm/{vmware_vm}/edit",
 *     "start-form"           = "/clouds/vmware/{cloud_context}/vm/{vmware_vm}/start",
 *     "stop-form"            = "/clouds/vmware/{cloud_context}/vm/{vmware_vm}/stop",
 *     "reboot-form"          = "/clouds/vmware/{cloud_context}/vm/{vmware_vm}/reboot",
 *     "suspend-form"         = "/clouds/vmware/{cloud_context}/vm/{vmware_vm}/suspend",
 *     "delete-form"          = "/clouds/vmware/{cloud_context}/vm/{vmware_vm}/delete",
 *     "delete-multiple-form" = "/clouds/vmware/{cloud_context}/vm/delete_multiple",
 *   },
 *   field_ui_base_route = "vmware_vm.settings",
 *   resource_type = "VirtualMachine"
 * )
 */
class VmwareVm extends VmwareEntityBase implements VmwareVmInterface {

  /**
   * {@inheritdoc}
   */
  public function getVmId(): ?string {
    return $this->get('vm_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVmId($vm_id): VmwareVmInterface {
    return $this->set('vm_id', $vm_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getPowerState(): ?string {
    return $this->get('power_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPowerState($power_state): VmwareVmInterface {
    return $this->set('power_state', $power_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getCpuCount(): ?int {
    return $this->get('cpu_count')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCpuCount($cpu_count): VmwareVmInterface {
    return $this->set('cpu_count', $cpu_count);
  }

  /**
   * {@inheritdoc}
   */
  public function getMemorySize(): ?int {
    return $this->get('memory_size')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMemorySize($memory_size): VmwareVmInterface {
    return $this->set('memory_size', $memory_size);
  }

  /**
   * {@inheritdoc}
   */
  public function getGuestOs(): ?string {
    return $this->get('guest_os')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGuestOs($guest_os): VmwareVmInterface {
    return $this->set('guest_os', $guest_os);
  }

  /**
   * {@inheritdoc}
   */
  public function getDiskSize(): ?FieldItemListInterface {
    return $this->get('disk_size');
  }

  /**
   * {@inheritdoc}
   */
  public function setDiskSize($disk_size): VmwareVmInterface {
    return $this->set('disk_size', $disk_size);
  }

  /**
   * {@inheritdoc}
   */
  public function getCdroms(): ?FieldItemListInterface {
    return $this->get('cdroms');
  }

  /**
   * {@inheritdoc}
   */
  public function setCdroms($cdroms): VmwareVmInterface {
    return $this->set('cdroms', $cdroms);
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworks(): ?FieldItemListInterface {
    return $this->get('networks');
  }

  /**
   * {@inheritdoc}
   */
  public function setNetworks($networks): VmwareVmInterface {
    return $this->set('networks', $networks);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = VmwareEntityBase::baseFieldDefinitions($entity_type);

    $fields['vm_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('VM ID'))
      ->setDescription(t('Identifier of the virtual machine.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['power_state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Power State'))
      ->setDescription(t('Power state of the virtual machine.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['cpu_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('CPU Count'))
      ->setDescription(t('Number of CPU cores.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['memory_size'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Memory Size (MiB)'))
      ->setDescription(t('Memory size in mebibytes.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['disk_size'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Disk Size (GiB)'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('Memory size in gibibytes.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string',
        'weight' => 5,
      ]);

    $fields['guest_os'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Guest OS'))
      ->setDescription(t('Defines the valid guest operating system types used for configuring a virtual machine.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'vmware_guest_os_formatter',
        'weight' => -5,
      ]);

    $fields['cdroms'] = BaseFieldDefinition::create('key_value')
      ->setLabel(t('CD-ROMs'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('List of CD-ROMs.'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'weight' => -5,
      ]);

    $fields['networks'] = BaseFieldDefinition::create('key_value')
      ->setLabel(t('Networks'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('List of networks.'))
      ->setDisplayOptions('view', [
        'type' => 'key_value_formatter',
        'weight' => -5,
      ]);

    return $fields;
  }

}
