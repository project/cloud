<?php

namespace Drupal\vmware\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a VMware VM entity.
 *
 * @ingroup vmware
 */
interface VmwareVmInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getVmId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setVmId($vm_id): VmwareVmInterface;

  /**
   * {@inheritdoc}
   */
  public function getPowerState(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setPowerState($power_state): VmwareVmInterface;

  /**
   * {@inheritdoc}
   */
  public function getCpuCount(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setCpuCount($cpu_count): VmwareVmInterface;

  /**
   * {@inheritdoc}
   */
  public function getMemorySize(): ?int;

  /**
   * {@inheritdoc}
   */
  public function setMemorySize($memory_size): VmwareVmInterface;

  /**
   * {@inheritdoc}
   */
  public function getGuestOs(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setGuestOs($guest_os): VmwareVmInterface;

  /**
   * {@inheritdoc}
   */
  public function getDiskSize(): ?FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setDiskSize($disk_size): VmwareVmInterface;

  /**
   * {@inheritdoc}
   */
  public function getCdroms(): ?FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setCdroms($cdroms): VmwareVmInterface;

  /**
   * {@inheritdoc}
   */
  public function getNetworks(): ?FieldItemListInterface;

  /**
   * {@inheritdoc}
   */
  public function setNetworks($networks): VmwareVmInterface;

}
