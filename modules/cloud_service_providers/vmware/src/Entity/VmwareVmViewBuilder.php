<?php

namespace Drupal\vmware\Entity;

use Drupal\cloud\Entity\CloudViewBuilder;

/**
 * Provides the VM view builders.
 */
class VmwareVmViewBuilder extends CloudViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getFieldsetDefs(): array {
    return [
      [
        'name' => 'vm',
        'title' => $this->t('VM'),
        'open' => TRUE,
        'fields' => [
          'name',
          'power_state',
          'cpu_count',
          'memory_size',
          'disk_size',
          'guest_os',
          'cdroms',
          'networks',
          'created',
        ],
      ],
      [
        'name' => 'fieldset_tags',
        'title' => $this->t('Tag Information'),
        'open' => TRUE,
        'fields' => [
          'tag_categories',
          'tags',
        ],
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'cloud_context',
          'uid',
        ],
      ],
    ];
  }

}
