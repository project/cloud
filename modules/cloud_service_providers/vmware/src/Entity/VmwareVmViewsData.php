<?php

namespace Drupal\vmware\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the VMware VM entity type.
 */
class VmwareVmViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();
    $base_table = $this->entityType->getBaseTable() ?: $this->entityType->id();
    // Additional information for Views integration, such as table joins, can be
    // put here.
    $data['vmware_vm']['vm_bulk_form'] = [
      'title' => $this->t('VM operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple VMs.'),
      'field' => [
        'id' => 'vm_bulk_form',
      ],
    ];
    // Add access check query tag.
    $data[$base_table]['table']['base']['access query tag'] = 'vmware_entity_views_access';
    return $data;
  }

}
