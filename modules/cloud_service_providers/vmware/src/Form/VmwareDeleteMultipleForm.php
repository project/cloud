<?php

namespace Drupal\vmware\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Traits\CloudDeleteMultipleFormTrait;
use Drupal\vmware\Service\VmwareServiceException;
use Drupal\vmware\Service\VmwareServiceInterface;

/**
 * Provides an entities deletion confirmation form.
 */
class VmwareDeleteMultipleForm extends VmwareProcessMultipleForm {

  use CloudDeleteMultipleFormTrait;

  /**
   * Constructs a new VmwareDeleteMultipleForm object.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\vmware\Service\VmwareServiceInterface $vmware_service
   *   The VMware Service.
   */
  public function __construct(
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    PrivateTempStoreFactory $temp_store_factory,
    MessengerInterface $messenger,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    RouteMatchInterface $route_match,
    VmwareServiceInterface $vmware_service,
  ) {

    parent::__construct(
      $current_user,
      $entity_type_manager,
      $temp_store_factory,
      $messenger,
      $cloud_config_plugin_manager,
      $route_match,
      $vmware_service
    );
    $this->tempStore = $temp_store_factory->get('entity_delete_multiple_confirm');
  }

  /**
   * {@inheritdoc}
   */
  protected function processCloudResource(CloudContentEntityBase $entity): bool {
    $this->vmwareService->setCloudContext($entity->getCloudContext());
    try {
      $this->vmwareService->login();
      $this->vmwareService->deleteVm(
        ['VmId' => $entity->getVmId()]
      );
      return TRUE;
    }
    catch (VmwareServiceException $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function process(CloudContentEntityBase $entity): bool {

    try {
      $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
      if ($cloud_config->isRemote()) {
        $this->processCloudResource($entity);
        $this->processOperationStatus($entity, 'deleted remotely');
        return TRUE;
      }

      if ($this->processCloudResource($entity)) {

        $this->processEntity($entity);

        // Since we do not use EntityDeleteFormTrait since it is an abstract
        // class and requires to implement a getEntity() method, so we use our
        // own method here instead.
        $this->processOperationStatus($entity, 'deleted');

        return TRUE;
      }

      // @todo Possibly refactor processOperationErrorStatus() to add the third parameter for the reason.
      if ($entity->getPowerState() === 'POWERED_ON') {
        $entity_type = $entity->getEntityType();
        $label = $entity->label();
        $message = $this->t('The @type %label could not be @passive_operation since the VM is currently running.', [
          '@type' => $entity_type->getSingularLabel(),
          '%label' => $entity->toLink($label)->toString(),
          '@passive_operation' => 'deleted',
        ]);
        $this->messenger()->addError($message);
        $this->logger('vmware')->error($message);
      }
      else {
        $this->processOperationErrorStatus($entity, 'deleted');
      }

      return FALSE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    // Exception return type handling.
    // Return unable to process.
    return FALSE;
  }

}
