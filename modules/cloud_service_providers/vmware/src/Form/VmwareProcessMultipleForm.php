<?php

namespace Drupal\vmware\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\cloud\Form\CloudProcessMultipleForm;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\vmware\Service\VmwareServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an entities deletion confirmation form.
 */
abstract class VmwareProcessMultipleForm extends CloudProcessMultipleForm {

  /**
   * The VMware Service.
   *
   * @var \Drupal\vmware\Service\VmwareServiceInterface
   */
  protected $vmwareService;

  /**
   * Constructs a new VmwareProcessMultipleForm object.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\vmware\Service\VmwareServiceInterface $vmware_service
   *   The vmware Service.
   */
  public function __construct(
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    PrivateTempStoreFactory $temp_store_factory,
    MessengerInterface $messenger,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    RouteMatchInterface $route_match,
    VmwareServiceInterface $vmware_service,
  ) {

    parent::__construct(
      $current_user,
      $entity_type_manager,
      $temp_store_factory,
      $messenger,
      $cloud_config_plugin_manager,
      $route_match
    );

    $this->vmwareService = $vmware_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('tempstore.private'),
      $container->get('messenger'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_route_match'),
      $container->get('vmware')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId(): string {
    return 'vmware_process_multiple_confirm_form';
  }

}
