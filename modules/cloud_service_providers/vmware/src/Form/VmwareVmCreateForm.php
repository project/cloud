<?php

namespace Drupal\vmware\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Service\CloudService;

/**
 * Form controller for the VMware VM entity create form.
 *
 * @ingroup vmware
 */
class VmwareVmCreateForm extends VmwareContentForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state);
    $this->vmwareService->setCloudContext($cloud_context);
    $this->vmwareService->login();

    $weight = -50;

    $form['vm'] = [
      '#type' => 'details',
      '#title' => $this->t('VM'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['vm']['name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Name'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#required'      => TRUE,
    ];

    $hosts = $this->vmwareService->describeHosts();
    $host_options = [];
    foreach ($hosts['value'] as $host) {
      $host_options[$host['host']] = $host['name'];
    }
    $form['vm']['host'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Host'),
      '#options'       => $host_options,
      '#required'      => TRUE,
    ];

    $folders = $this->vmwareService->describeFolders();
    $folder_options = [];
    foreach ($folders['value'] as $folder) {
      if ($folder['type'] !== 'VIRTUAL_MACHINE') {
        continue;
      }
      $folder_options[$folder['folder']] = $folder['name'];
    }
    $form['vm']['folder'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Folder'),
      '#options'       => $folder_options,
      '#required'      => TRUE,
    ];

    $datastores = $this->vmwareService->describeDatastores();
    $datastore_options = [];
    foreach ($datastores['value'] as $datastore) {
      $name = sprintf(
        '%s (Free: %s / Capacity: %s)', $datastore['name'],
        CloudService::formatMemory($datastore['free_space']),
        CloudService::formatMemory($datastore['capacity'])
      );
      $datastore_options[$datastore['datastore']] = $name;
    }
    $form['vm']['datastore'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Datastore'),
      '#options'       => $datastore_options,
      '#required'      => TRUE,
    ];

    $form['vm']['guest_os'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Guest OS'),
      '#options'       => $this->vmwareVmGuestOsDataProvider->getOptions(),
      '#required'      => TRUE,
    ];

    $form['vm']['cpu_count'] = [
      '#type'          => 'number',
      '#title'         => $this->t('CPU Count'),
      '#default_value' => 1,
      '#required'      => TRUE,
    ];

    $form['vm']['memory_size'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Memory Size (MiB)'),
      '#default_value' => 4096,
      '#required'      => TRUE,
    ];

    $form['vm']['disk_size'] = $form['disk_size'];
    unset($form['disk_size']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->trimTextfields($form, $form_state);

    $entity = $this->entity;
    $params = [
      'cloud_context' => $this->routeMatch->getParameter('cloud_context'),
      'datastore' => $form_state->getValue('datastore'),
      'host' => $form_state->getValue('host'),
      'folder' => $form_state->getValue('folder'),
    ];

    if ($this->vmwareOperationsService->createVm(
      $entity,
      $params
    )) {
      $form_state->setRedirect('view.vmware_vm.list', ['cloud_context' => $entity->getCloudContext()]);
    }
  }

}
