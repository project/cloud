<?php

namespace Drupal\vmware\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a form for deleting a VM entity.
 *
 * @ingroup vmware
 */
class VmwareVmDeleteForm extends VmwareDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    $entity = $this->entity;

    return $this->t('Are you sure you want to delete the "@name" VM?', [
      '@name' => $entity->getName(),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\vmware\Service\VmwareServiceException
   *    Thrown when unable to delete entity.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entity = $this->entity;
    $this->vmwareOperationsService->deleteVm($entity, []);
    $form_state->setRedirect('view.vmware_vm.list', ['cloud_context' => $entity->getCloudContext()]);
  }

}
