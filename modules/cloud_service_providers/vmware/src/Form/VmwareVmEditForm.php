<?php

namespace Drupal\vmware\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the VMware VM entity edit form.
 *
 * @ingroup vmware
 */
class VmwareVmEditForm extends VmwareContentForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;
    $weight = -50;

    $form['vm'] = [
      '#type' => 'details',
      '#title' => $this->t('VM'),
      '#open' => TRUE,
      '#weight' => $weight++,
    ];

    $form['vm']['name'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Name')),
      '#markup'        => $entity->getName(),
      '#weight'        => $weight++,
    ];

    $form['vm']['power_state'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Power State')),
      '#markup'        => $entity->getPowerState(),
      '#weight'        => $weight++,
    ];

    $form['vm']['cpu_count'] = [
      '#type'          => 'number',
      '#title'         => $this->t('CPU Count'),
      '#description'   => $this->t('The CPU count can be modified when virtual machine is powered off.'),
      '#default_value' => $entity->getCpuCount(),
      '#required'      => TRUE,
      '#weight'        => $weight++,
      '#disabled'      => $entity->getPowerState() !== 'POWERED_OFF',
    ];

    $form['vm']['memory_size'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Memory Size (MiB)'),
      '#description'   => $this->t('The memory size can be modified when virtual machine is powered off.'),
      '#default_value' => $entity->getMemorySize(),
      '#weight'        => $weight++,
      '#disabled'      => $entity->getPowerState() !== 'POWERED_OFF',
    ];

    $form['vm']['guest_os'] = [
      '#type'          => 'item',
      '#title'         => $this->getItemTitle($this->t('Guest OS')),
      '#markup'        => $this->vmwareVmGuestOsDataProvider->getLabel($entity->getGuestOs()),
      '#weight'        => $weight++,
    ];

    $form['vm']['disk_size'] = $form['disk_size'];
    $form['vm']['disk_size']['#disabled'] = TRUE;
    unset($form['disk_size']);

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $this->copyFormItemValues($form);

    $this->trimTextfields($form, $form_state);

    $entity = $this->entity;
    if ($this->vmwareOperationsService->editVm($entity, [])) {
      $form_state->setRedirect('view.vmware_vm.list', ['cloud_context' => $entity->getCloudContext()]);
    }
  }

}
