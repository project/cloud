<?php

namespace Drupal\vmware\Form;

use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Provides an entities start confirmation form.
 */
class VmwareVmStartMultipleForm extends VmwareProcessMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {

    return $this->formatPlural(count($this->selection),
      'Are you sure you want to start this @item?',
      'Are you sure you want to start these @items?', [
        '@item' => $this->entityType->getSingularLabel(),
        '@items' => $this->entityType->getPluralLabel(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Start');
  }

  /**
   * {@inheritdoc}
   */
  protected function processCloudResource(CloudContentEntityBase $entity): bool {
    $this->vmwareService->setCloudContext($entity->getCloudContext());
    $this->vmwareService->login();
    $this->vmwareService->startVm([
      'VmId' => $entity->getVmId(),
    ]);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processEntity(CloudContentEntityBase $entity): void {}

  /**
   * Process an entity and related VMware resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   *
   * @return bool
   *   Succeeded or failed.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function process(CloudContentEntityBase $entity): bool {
    if (empty($entity)) {
      return FALSE;
    }

    try {
      $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
      if ($cloud_config->isRemote()) {
        $this->processCloudResource($entity);
        $this->processOperationStatus($entity, 'started remotely');
        return TRUE;
      }

      $this->processCloudResource($entity);
      $this->processEntity($entity);
      $this->processOperationStatus($entity, 'started');

      return TRUE;
    }
    catch (\Exception $e) {
      $this->processOperationErrorStatus($entity, 'started');
      return FALSE;
    }
  }

  /**
   * Returns the message to show the user after an item was processed.
   *
   * @param int $count
   *   Count of processed translations.
   *
   * @return \Drupal\Core\StringTranslation\PluralTranslatableMarkup
   *   The item processed message.
   */
  protected function getProcessedMessage($count): PluralTranslatableMarkup {
    $this->vmwareService->updateVms();
    return $this->formatPlural($count, 'Started @count item.', 'Started @count items.');
  }

}
