<?php

namespace Drupal\vmware\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a VM form.
 *
 * @Action(
 *   id = "entity:delete_action:vmware_vm",
 *   label = @Translation("Delete VMware"),
 *   type = "vmware_vm"
 * )
 */
class DeleteVm extends DeleteAction {

}
