<?php

namespace Drupal\vmware\Plugin\Action;

use Drupal\cloud\Plugin\Action\OperateAction;

/**
 * Reboot selected VM(s).
 *
 * @Action(
 *   id = "vmware_vm_reboot_action",
 *   label = @Translation("Reboot VM"),
 *   type = "vmware_vm",
 *   confirm_form_route_name
 *     = "entity.vmware_vm.reboot_multiple_form"
 * )
 */
class RebootVm extends OperateAction {

  /**
   * {@inheritdoc}
   */
  protected function getOperation(): string {
    return 'reboot';
  }

}
