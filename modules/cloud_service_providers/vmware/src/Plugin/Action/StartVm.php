<?php

namespace Drupal\vmware\Plugin\Action;

use Drupal\cloud\Plugin\Action\OperateAction;

/**
 * Start selected VM(s).
 *
 * @Action(
 *   id = "vmware_vm_start_action",
 *   label = @Translation("Start VM"),
 *   type = "vmware_vm",
 *   confirm_form_route_name
 *     = "entity.vmware_vm.start_multiple_form"
 * )
 */
class StartVm extends OperateAction {

  /**
   * {@inheritdoc}
   */
  protected function getOperation(): string {
    return 'start';
  }

}
