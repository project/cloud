<?php

namespace Drupal\vmware\Plugin\Action;

use Drupal\cloud\Plugin\Action\OperateAction;

/**
 * Stop selected VM(s).
 *
 * @Action(
 *   id = "vmware_vm_stop_action",
 *   label = @Translation("Stop VM"),
 *   type = "vmware_vm",
 *   confirm_form_route_name
 *     = "entity.vmware_vm.stop_multiple_form"
 * )
 */
class StopVm extends OperateAction {

  /**
   * {@inheritdoc}
   */
  protected function getOperation(): string {
    return 'stop';
  }

}
