<?php

namespace Drupal\vmware\Plugin\Action;

use Drupal\cloud\Plugin\Action\OperateAction;

/**
 * Suspend selected VM(s).
 *
 * @Action(
 *   id = "vmware_vm_suspend_action",
 *   label = @Translation("Suspend VM"),
 *   type = "vmware_vm",
 *   confirm_form_route_name
 *     = "entity.vmware_vm.suspend_multiple_form"
 * )
 */
class SuspendVm extends OperateAction {

  /**
   * {@inheritdoc}
   */
  protected function getOperation(): string {
    return 'suspend';
  }

}
