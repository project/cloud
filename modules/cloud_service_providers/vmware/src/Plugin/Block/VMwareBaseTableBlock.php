<?php

namespace Drupal\vmware\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\AccessCheckTrait;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\cloud\Traits\CloudResourceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for VMware blocks that builds a resource table.
 */
abstract class VMwareBaseTableBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use CloudContentEntityTrait;
  use CloudResourceTrait;
  use AccessCheckTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The cloud config plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  protected $entityLinkRenderer;

  /**
   * Creates a ResourcesBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud config plugin manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    DateFormatterInterface $date_formatter,
    EntityLinkRendererInterface $entity_link_renderer,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->dateFormatter = $date_formatter;
    $this->entityLinkRenderer = $entity_link_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('date.formatter'),
      $container->get('entity.link_renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'cloud_context' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['cloud_context'] = [
      '#type' => 'select',
      '#title' => $this->t('Cloud service provider'),
      '#description' => $this->t('Select cloud service provider.'),
      '#options' => $this->cloudConfigPluginManager->getCloudConfigs($this->t('All VMware regions'), 'vmware'),
      '#default_value' => $config['cloud_context'] ?? '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['cloud_context'] = $form_state->getValue('cloud_context');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];

    $build['hosts'] = [
      '#type' => 'details',
      '#title' => $this->getTitle(),
      '#open' => TRUE,
    ];

    $build['hosts']['table'] = $this->buildTable();
    $build['hosts']['#attached'] = [
      'library' => [
        'cloud/datatables',
      ],
    ];
    $build['hosts']['#attributes']['class'][] = 'simple-datatable';

    return $build;
  }

  /**
   * Get region name.
   *
   * @return string|null
   *   Region string or NULL
   */
  protected function getRegionName(): ?string {
    if (empty($this->configuration['cloud_context'])) {
      return $this->t('all VMware regions');
    }
    $this->cloudConfigPluginManager->setCloudContext($this->configuration['cloud_context']);
    $region = $this->cloudConfigPluginManager->loadConfigEntity();
    return empty($region) === FALSE ? $region->getName() : NULL;
  }

  /**
   * Helper function to format a timestamp.
   *
   * @param int $timestamp
   *   Timestamp to format.
   * @param string $format
   *   Date format.
   *
   * @return string
   *   Formatted date string.
   */
  protected function getFormattedDate(int $timestamp, string $format = 'short'): string {
    return $this->dateFormatter->format($timestamp, $format);
  }

  /**
   * Build VMware resource table.
   *
   * @return array
   *   Table array.
   */
  abstract protected function buildTable(): array;

  /**
   * Build the table title.
   *
   * @return string
   *   Title string.
   */
  abstract protected function getTitle(): string;

}
