<?php

namespace Drupal\vmware\Plugin\Block;

/**
 * Provides a block displaying VMware hosts.
 *
 * @Block(
 *   id = "vmware_host_block",
 *   admin_label = @Translation("VMware Hosts"),
 *   category = @Translation("VMware")
 * )
 */
class VMwareHostBlock extends VMwareBaseTableBlock {

  /**
   * {@inheritdoc}
   */
  protected function getTitle(): string {
    return $this->t('Hosts for @region', [
      '@region' => $this->getRegionName(),
    ]);
  }

  /**
   * Build VMware table.
   *
   * @return array
   *   Table array.
   */
  protected function buildTable(): array {
    return [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Host'),
        $this->t('Power State'),
        $this->t('Connection State'),
        $this->t('Created'),
      ],
      '#empty' => $this->t('No hosts found.'),
      '#attributes' => [
        'class' => [
          'simple-datatable',
        ],
      ],
      '#rows' => $this->getHostRows(),
    ];
  }

  /**
   * Get VMware hosts from database.
   *
   * @return array
   *   Array of hosts.
   */
  private function getHostRows(): array {
    $rows = [];
    $entities = $this->runEntityQuery('vmware_host', []);
    /** @var \Drupal\Vmware\Entity\VmwareHost $entity */
    foreach ($entities ?: [] as $entity) {
      if ($this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $this->currentUser,
          'view own vmware host',
          'view any vmware host'
        )->isAllowed() === FALSE) {
        continue;
      }

      $link = $entity->getName();
      try {
        $link = $entity->toLink($entity->getName());
      }
      catch (\Exception $e) {
        $this->messenger()->addError("An error occurred: {$e->getMessage()}");
      }
      $rows[] = [
        'data' => [
          $link,
          $entity->getHost(),
          $entity->getPowerState(),
          $entity->getConnectionState(),
          $this->dateFormatter->format($entity->created(), 'short'),
        ],
      ];
    }

    return $rows;
  }

}
