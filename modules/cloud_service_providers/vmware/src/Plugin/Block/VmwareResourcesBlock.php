<?php

namespace Drupal\vmware\Plugin\Block;

use Drupal\cloud\Plugin\Block\CloudBaseResourcesBlock;

/**
 * Provides a block displaying system resources.
 *
 * @Block(
 *   id = "vmware_resources_block",
 *   admin_label = @Translation("VMware resources"),
 *   category = @Translation("VMware")
 * )
 */
class VmwareResourcesBlock extends CloudBaseResourcesBlock {

  /**
   * {@inheritdoc}
   */
  protected function getEntityBundleType(): string {
    return 'vmware';
  }

  /**
   * Build a resource HTML table.
   *
   * @return array
   *   Table array.
   */
  protected function buildResourceTable(): array {
    $resources = [
      'vmware_host' => [
        'view any vmware host',
        [],
      ],
      'vmware_vm' => [
        'view any vmware vm',
        [],
      ],
    ];

    $rows = $this->buildResourceTableRows($resources);

    return [
      '#type' => 'table',
      '#rows' => $rows,
    ];
  }

}
