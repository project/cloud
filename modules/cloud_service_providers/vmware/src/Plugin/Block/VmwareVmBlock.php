<?php

namespace Drupal\vmware\Plugin\Block;

/**
 * Provides a block displaying VMware VMs.
 *
 * @Block(
 *   id = "vmware_vm_block",
 *   admin_label = @Translation("VMware VMs"),
 *   category = @Translation("VMware")
 * )
 */
class VmwareVmBlock extends VMwareBaseTableBlock {

  /**
   * {@inheritdoc}
   */
  protected function getTitle(): string {
    return $this->t('VMs for @region', [
      '@region' => $this->getRegionName(),
    ]);
  }

  /**
   * Build VMware table.
   *
   * @return array
   *   Table array.
   */
  protected function buildTable(): array {
    return [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('VM ID'),
        $this->t('State'),
        $this->t('Created'),
      ],
      '#empty' => $this->t('No VMs found.'),
      '#attributes' => [
        'class' => [
          'simple-datatable',
        ],
      ],
      '#rows' => $this->getVmRows(),
    ];
  }

  /**
   * Get VMs from database.
   *
   * @return array
   *   Array of VMs
   */
  private function getVmRows(): array {
    $rows = [];
    $entities = $this->runEntityQuery('vmware_vm', []);
    /** @var \Drupal\Vmware\Entity\VmwareVm $entity */
    foreach ($entities ?: [] as $entity) {
      if ($this->allowedIfCanAccessCloudConfigWithOwner(
          $entity,
          $this->currentUser,
          'view own vmware vm',
          'view any vmware vm'
        )->isAllowed() === FALSE) {
        continue;
      }

      $link = $entity->getName();
      try {
        $link = $entity->toLink($entity->getName());
      }
      catch (\Exception $e) {
        $this->messenger()->addError("An error occurred: {$e->getMessage()}");
      }
      $rows[] = [
        'data' => [
          $link,
          $entity->getVmId(),
          $entity->getPowerState(),
          $this->dateFormatter->format($entity->created(), 'short'),
        ],
      ];
    }

    return $rows;
  }

}
