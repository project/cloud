<?php

namespace Drupal\vmware\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'tags_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "tags_formatter",
 *   label = @Translation("Tags formatter"),
 *   field_types = {
 *     "key_value",
 *   }
 * )
 */
class TagsFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $rows = [];
    $header = [
      $this->t('ID'),
    ];

    foreach ($items ?: [] as $item) {
      if ($item->isEmpty()) {
        continue;
      }
      $item_value = json_decode($item->item_value, TRUE, 512, JSON_THROW_ON_ERROR);

      $row = [
        $item->item_key,
      ];
      foreach ($item_value ?: [] as $key => $value) {
        if ($key === 'id') {
          continue;
        }
        $label = ucwords(str_replace('_', ' ', $key));
        if (!in_array($label, $header, TRUE)) {
          $header[] = $label;
        }

        $value = is_array($value)
          ? [
            'data' => [
              '#markup' => implode('<br>', $value),
              '#allowed_tags' => ['br'],
            ],
          ]
          : $value;
        $row[] = $value;
      }
      if (!empty($row)) {
        $rows[] = $row;
      }

    }

    if (count($rows)) {
      $elements[0] = [
        '#theme' => 'table__vmware_tags',
        '#header' => $header,
        '#rows' => $rows,
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    return $field_definition->getName() === 'tags'
      || $field_definition->getName() === 'tag_categories';
  }

}
