<?php

namespace Drupal\vmware\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\vmware\Service\VmwareVmGuestOsDataProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'vmware_guest_os_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "vmware_guest_os_formatter",
 *   label = @Translation("VMware Guest OS formatter"),
 *   field_types = {
 *     "string",
 *   }
 * )
 */
class VmwareGuestOsFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The vmware vm guest OS data provider.
   *
   * @var \Drupal\vmware\Service\VmwareVmGuestOsDataProvider
   */
  protected $vmwareVmGuestOsDataProvider;

  /**
   * Constructs an EntityLinkFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\vmware\Service\VmwareVmGuestOsDataProvider $vmware_vm_guest_os_data_provider
   *   The vmware vm guest OS data provider.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    VmwareVmGuestOsDataProvider $vmware_vm_guest_os_data_provider,
  ) {

    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );

    $this->vmwareVmGuestOsDataProvider = $vmware_vm_guest_os_data_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('vmware.vm_guest_os_data_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    foreach ($items ?: [] as $delta => $item) {
      if (!$item->isEmpty()) {
        $elements[$delta] = [
          '#markup' => $this->vmwareVmGuestOsDataProvider->getLabel($item->value),
        ];
      }
    }

    return $elements;
  }

}
