<?php

namespace Drupal\vmware\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\cloud\Plugin\QueueWorker\CloudBaseUpdateResourcesQueueWorker;
use Drupal\vmware\Service\VmwareServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes for Vmware Update Resources Queue.
 *
 * @QueueWorker(
 *   id = "vmware_update_resources_queue",
 *   title = @Translation("Vmware Update Resources Queue"),
 *   deriver = "Drupal\cloud\Plugin\Derivative\CloudUpdateQueueDerivative",
 * )
 */
class VmwareUpdateResourcesQueueWorker extends CloudBaseUpdateResourcesQueueWorker implements ContainerFactoryPluginInterface {

  /**
   * The vmware service.
   *
   * @var \Drupal\vmware\Service\VmwareServiceInterface
   */
  private $vmwareService;

  /**
   * Constructs a new LocaleTranslation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\vmware\Service\VmwareServiceInterface $vmware_service
   *   The vmware service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, VmwareServiceInterface $vmware_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->vmwareService = $vmware_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('vmware')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $cloud_context = $data['cloud_context'];
    $vmware_method_name = $data['vmware_method_name'];

    try {
      $this->setCloudContext($this->vmwareService, $cloud_context, $vmware_method_name);
      $this->checkServiceMethod($this->vmwareService, $vmware_method_name);

      if (empty($this->vmwareService->getSessionId())) {
        $this->vmwareService->login();
      }

      if (empty($this->vmwareService->$vmware_method_name())) {
        $msg = $this->t('Unable to update resource.');
        $this->logger('vmware_service')
          ->error($msg);
        return;
      }

      $this->logger('vmware_service')
        ->info($this->t('Updated resource in @context.', [
          '@context' => $cloud_context,
        ]));
    }
    catch (\Exception $e) {
      // Catch all exceptions to bump it out of the queue.
      $this->handleException($e);
    }
  }

}
