<?php

namespace Drupal\vmware\Plugin\cloud\launch_template;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\vmware\Service\VmwareServiceException;
use Drupal\vmware\Service\VmwareServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * VMware cloud launch template plugin.
 */
class VmwareCloudLaunchTemplatePlugin extends CloudPluginBase implements CloudLaunchTemplatePluginInterface, ContainerFactoryPluginInterface {

  use CloudContentEntityTrait;

  /**
   * The Vmware Service.
   *
   * @var \Drupal\vmware\Service\VmwareServiceInterface
   */
  protected $vmwareService;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Current login user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  protected $entityLinkRenderer;

  /**
   * File system object.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * VmwareCloudLaunchTemplatePlugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The uuid service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current login user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \Drupal\vmware\Service\VmwareServiceInterface $vmware_service
   *   The VMware Service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    UuidInterface $uuid_service,
    AccountProxyInterface $current_user,
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityLinkRendererInterface $entity_link_renderer,
    FileSystemInterface $file_system,
    CloudServiceInterface $cloud_service,
    VmwareServiceInterface $vmware_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->uuidService = $uuid_service;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityLinkRenderer = $entity_link_renderer;
    $this->fileSystem = $file_system;
    $this->cloudService = $cloud_service;
    $this->vmwareService = $vmware_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uuid'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity.link_renderer'),
      $container->get('file_system'),
      $container->get('cloud'),
      $container->get('vmware')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityBundleName(): string {
    return $this->pluginDefinition['entity_bundle'];
  }

  /**
   * Build the launch form for K8s.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state if launch is called from a form.
   */
  public function buildLaunchForm(array &$form, FormStateInterface $form_state): void {
    $form_object = $form_state->getFormObject();
    $entity = $form_object->getEntity();

    $form['#parents'] = [];

    $this->vmwareService->setCloudContext($entity->getCloudContext());
    $this->vmwareService->login();
    $hosts = $this->vmwareService->describeHosts();
    $host_options = [];
    foreach ($hosts['value'] as $host) {
      $host_options[$host['host']] = $host['name'];
    }
    $form['host'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Host'),
      '#options'       => $host_options,
      '#required'      => TRUE,
    ];

    $folders = $this->vmwareService->describeFolders();
    $folder_options = [];
    foreach ($folders['value'] as $folder) {
      if ($folder['type'] !== 'VIRTUAL_MACHINE') {
        continue;
      }
      $folder_options[$folder['folder']] = $folder['name'];
    }
    $form['folder'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Folder'),
      '#options'       => $folder_options,
      '#required'      => TRUE,
    ];

    $datastores = $this->vmwareService->describeDatastores();
    $datastore_options = [];
    foreach ($datastores['value'] as $datastore) {
      $name = sprintf(
        '%s (Free: %s / Capacity: %s)', $datastore['name'],
        CloudService::formatMemory($datastore['free_space']),
        CloudService::formatMemory($datastore['capacity'])
      );
      $datastore_options[$datastore['datastore']] = $name;
    }
    $form['datastore'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Datastore'),
      '#options'       => $datastore_options,
      '#required'      => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function launch(CloudLaunchTemplateInterface $cloud_launch_template, ?FormStateInterface $form_state = NULL): array {
    $disk_size = $cloud_launch_template->field_disk_size;
    $disks = [];
    foreach ($disk_size as $item) {
      $disks[] = [
        'new_vmdk' => [
          'capacity' => $item->value * 1024 * 1024 * 1024,
        ],
      ];
    }

    $this->vmwareService->setCloudContext($cloud_launch_template->getCloudContext());
    $params = [
      'name' => $cloud_launch_template->getName() . $this->uuidService->generate(),
      'guest_OS' => $cloud_launch_template->field_guest_os->value,
      'placement' => [
        'datastore' => $form_state->getValue('datastore'),
        'host' => $form_state->getValue('host'),
        'folder' => $form_state->getValue('folder'),
      ],
      'cpu' => [
        'count' => $cloud_launch_template->field_cpu_count->value,
      ],
      'memory' => [
        'size_MiB' => $cloud_launch_template->field_memory_size->value,
      ],
    ];
    if (!empty($disks)) {
      $params['disks'] = $disks;
    }

    try {
      $this->vmwareService->login();
      $result = $this->vmwareService->createVm($params);
      $this->vmwareService->updateVms([
        'VmId' => $result['value'],
      ], FALSE);

      $this->processOperationStatus($cloud_launch_template, 'launched');

      $this->cloudService->invalidateCacheTags($cloud_launch_template->getCacheTags());

      return [
        'route_name' => 'view.vmware_vm.list',
        'params' => ['cloud_context' => $cloud_launch_template->getCloudContext()],
      ];
    }
    catch (VmwareServiceException
    | EntityStorageException
    | EntityMalformedException $e) {

      $this->processOperationErrorStatus($cloud_launch_template, 'launched');

      return [
        'route_name' => 'entity.cloud_launch_template.canonical',
        'params' => [
          'cloud_launch_template' => $cloud_launch_template->id(),
          'cloud_context' => $cloud_launch_template->getCloudContext(),
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildListHeader(): array {
    return [
      [
        'data' => $this->t('Guest OS'),
        'specifier' => 'field_guest_os',
        'field' => 'field_guest_os',
      ],
      [
        'data' => $this->t('CPU Count'),
        'specifier' => 'field_cpu_count',
        'field' => 'field_cpu_count',
      ],
      [
        'data' => $this->t('Memory Size'),
        'specifier' => 'field_memory_size',
        'field' => 'field_memory_size',
      ],
      [
        'data' => $this->t('Disk Size'),
        'specifier' => 'field_disk_size',
        'field' => 'field_disk_size',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRow(CloudLaunchTemplateInterface $entity): array {
    $row['field_guest_os'] = [
      'data' => $this->renderField($entity, 'field_guest_os'),
    ];
    $row['field_cpu_count'] = [
      'data' => $this->renderField($entity, 'field_cpu_count'),
    ];
    $row['field_memory_size'] = [
      'data' => $this->renderField($entity, 'field_memory_size'),
    ];
    $row['field_disk_size'] = [
      'data' => $this->renderField($entity, 'field_disk_size'),
    ];
    return $row;
  }

  /**
   * Render a launch template entity field.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The launch template entity.
   * @param string $field_name
   *   The field to render.
   * @param string $view
   *   The view to render.
   *
   * @return array
   *   A fully loaded render array for that field or empty array.
   */
  private function renderField(CloudLaunchTemplateInterface $entity, $field_name, $view = 'default'): array {
    $field = [];
    if ($entity->hasField($field_name)) {
      $field = $entity->get($field_name)->view($view);
      // Hide the label.
      $field['#label_display'] = 'hidden';
    }
    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function validateGit(
    CloudLaunchTemplateInterface $entity,
    array &$files_arr,
    string &$tmp_dir_name,
    bool $delete_tmp_dir = FALSE,
  ): string {
    return '';
  }

}
