<?php

namespace Drupal\vmware\Plugin\cloud\launch_template;

use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginUninstallValidator;

/**
 * Validates module uninstall readiness based on existing content entities.
 */
class VmwareCloudLaunchTemplatePluginUninstallValidator extends CloudLaunchTemplatePluginUninstallValidator {

}
