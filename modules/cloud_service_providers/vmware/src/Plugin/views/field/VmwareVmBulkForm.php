<?php

namespace Drupal\vmware\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines an VM operations bulk form element.
 *
 * @ViewsField("vm_bulk_form")
 */
class VmwareVmBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): TranslatableMarkup {
    return $this->t('No VM selected.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getBulkOptions($filtered = TRUE): array {
    $options = parent::getBulkOptions($filtered);

    $action_orders = [
      'vmware_vm_stop_action',
      'vmware_vm_reboot_action',
      'vmware_vm_delete_action',
      'vmware_vm_start_action',
      'vmware_vm_suspend_action',
    ];

    uksort($options, static function ($a, $b) use ($action_orders) {
      return array_search($a, $action_orders) - array_search($b, $action_orders);
    });

    return $options;
  }

}
