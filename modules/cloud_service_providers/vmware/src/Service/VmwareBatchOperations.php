<?php

namespace Drupal\vmware\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\vmware\Entity\VmwareHost;
use Drupal\vmware\Entity\VmwareVm;

/**
 * Entity update methods for Batch API processing.
 */
class VmwareBatchOperations {

  use CloudContentEntityTrait;
  use StringTranslationTrait;

  /**
   * The finish callback function.
   *
   * Deletes stale entities from the database.
   *
   * @param string $entity_type
   *   The entity type.
   * @param array $stale
   *   The stale entities to delete.
   * @param bool $clear
   *   TRUE to clear entities, FALSE keep them.
   */
  public static function finished($entity_type, array $stale, $clear = TRUE): void {
    $entity_type_manager = \Drupal::entityTypeManager();
    if (count($stale) && $clear === TRUE) {
      $entity_type_manager->getStorage($entity_type)->delete($stale);
    }
  }

  /**
   * Update or create a vmware vm entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $vm
   *   The VM array.
   *
   * @throws \Drupal\vmware\Service\VmwareServiceException
   *   Thrown when unable to get VMs.
   */
  public static function updateVm(string $cloud_context, array $vm): void {
    $vmware_service = \Drupal::service('vmware');
    $vmware_service->setCloudContext($cloud_context);
    $vmware_service->login();
    $vm_detail = $vmware_service->showVm($vm['vm']);

    $timestamp = time();
    $name = $vm['name'];
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'vmware_vm', 'name', $name);

    if (!empty($entity_id)) {
      $entity = VmwareVm::load($entity_id);
    }
    else {
      $entity = VmwareVm::create([
        'cloud_context' => $cloud_context,
        'name' => $name,
        'created' => $timestamp,
        'changed' => $timestamp,
      ]);

      // Store the owner information in VMware tags.
      $vmware_service->updateTagValueCreatedByUid($entity, $vm['vm'] ?? '');
    }

    // Disk size.
    $disk_size = [];
    foreach ($vm_detail['value']['disks'] as $disk) {
      $disk_size[] = $disk['value']['capacity'] / (1024 * 1024 * 1024);
    }
    $entity->setDiskSize($disk_size);

    // CD-ROMs.
    $cdroms = [];
    foreach ($vm_detail['value']['cdroms'] as $cdrom) {
      $cdroms[] = [
        'item_key' => $cdrom['value']['label'],
        'item_value' => $cdrom['value']['state'] === 'CONNECTED'
          ? sprintf('%s (%s)', $cdrom['value']['backing']['iso_file'], $cdrom['value']['state'])
          : $cdrom['value']['state'],
      ];
    }
    $entity->setCdroms($cdroms);

    // Networks.
    $networks = [];
    foreach ($vm_detail['value']['nics'] as $nic) {
      $networks[] = [
        'item_key' => $nic['value']['label'],
        'item_value' => sprintf('%s (%s)', $nic['value']['backing']['network_name'], $nic['value']['state']),
      ];
    }
    $entity->setNetworks($networks);

    $entity->setVmId($vm['vm']);
    $entity->setPowerState($vm['power_state']);
    $entity->setCpuCount($vm['cpu_count']);
    $entity->setMemorySize($vm['memory_size_MiB']);
    $entity->setGuestOs($vm_detail['value']['guest_OS']);
    $entity->setRefreshed($timestamp);

    // Set tag information.
    $vmware_service->setTagInformation($entity, $vm['vm']);

    $entity->save();
  }

  /**
   * Update or create a vmware host entity.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $host
   *   The VM array.
   *
   * @throws \Drupal\vmware\Service\VmwareServiceException|\Drupal\Core\Entity\EntityStorageException
   *   Thrown when unable to get Hosts.
   */
  public static function updateHost(string $cloud_context, array $host): void {
    $vmware_service = \Drupal::service('vmware');

    $timestamp = time();
    $name = $host['name'];
    $entity_id = \Drupal::service('cloud')->getEntityId($cloud_context, 'vmware_host', 'name', $name);

    if (!empty($entity_id)) {
      $entity = VmwareHost::load($entity_id);
    }
    else {
      $entity = VmwareHost::create([
        'cloud_context' => $cloud_context,
        'name' => $name,
        'created' => $timestamp,
        'changed' => $timestamp,
      ]);

      // Store the owner information in VMware tags.
      $vmware_service->updateTagValueCreatedByUid($entity, $host['host'] ?? '');
    }

    $entity->setHost($host['host']);
    $entity->setPowerState($host['power_state']);
    $entity->setConnectionState($host['connection_state']);
    $entity->setRefreshed($timestamp);

    // Set tag information.
    $vmware_service->setTagInformation($entity, $host['host']);

    $entity->save();
  }

}
