<?php

namespace Drupal\vmware\Service;

use Drupal\Core\Entity\EntityDeleteFormTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\cloud\Service\CloudServiceBase;
use Drupal\vmware\Entity\VmwareEntityInterface;
use Drupal\vmware\Entity\VmwareVmInterface;

/**
 * Provides VMware operations service.
 */
class VmwareOperationsService extends CloudServiceBase implements VmwareOperationsServiceInterface {

  use EntityDeleteFormTrait;

  /**
   * The VMware Service.
   *
   * @var \Drupal\vmware\Service\VmwareServiceInterface
   */
  private $vmwareService;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The K8sOperationsService constructor.
   *
   * @param \Drupal\vmware\Service\VmwareServiceInterface $vmware_service
   *   The VMware Service.
   */
  public function __construct(
    VmwareServiceInterface $vmware_service,
  ) {
    parent::__construct();

    $this->vmwareService = $vmware_service;
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context): void {
    $this->vmwareService->setCloudContext($cloud_context);
  }

  /**
   * Gets the entity of this method.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function createVm(VmwareVmInterface $entity, array $params): bool {
    $cloud_context = $params['cloud_context'];
    $entity->setCloudContext($cloud_context);

    $disk_size = $entity->getDiskSize();
    $disks = [];
    foreach ($disk_size ?: [] as $item) {
      $disks[] = [
        'new_vmdk' => [
          'capacity' => $item->value * 1024 * 1024 * 1024,
        ],
      ];
    }

    $this->vmwareService->setCloudContext($cloud_context);
    $parameters = [
      'name' => $entity->getName(),
      'guest_OS' => $entity->getGuestOs(),
      'placement' => [
        'datastore' => $params['datastore'],
        'host' => $params['host'],
        'folder' => $params['folder'],
      ],
      'cpu' => [
        'count' => $entity->getCpuCount(),
      ],
      'memory' => [
        'size_MiB' => $entity->getMemorySize(),
      ],
    ];
    if (!empty($disks)) {
      $parameters['disks'] = $disks;
    }

    try {
      $this->vmwareService->login();
      $result = $this->vmwareService->createVm($parameters);
      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'created remotely');
        return TRUE;
      }

      $entity->setVmId($result['value']);
      $entity->save();

      // Store the owner information in VMware tags.
      $cast = fn($orig): VmwareEntityInterface => $orig;
      $this->vmwareService->updateTagValueCreatedByUid($cast($entity), $result['value'] ?? '');

      $this->vmwareService->updateVms([
        'VmId' => $result['value'],
      ], FALSE);

      $this->processOperationStatus($entity, 'created');

      $this->clearCacheValues($entity->getCacheTags());

      return TRUE;
    }
    catch (VmwareServiceException
    | EntityStorageException
    | EntityMalformedException $e) {
      try {
        $this->processOperationErrorStatus($entity, 'created');
      }
      catch (EntityMalformedException $e) {
        $this->handleException($e);
      }
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function editVm(VmwareVmInterface $entity, array $params): bool {
    try {
      $this->vmwareService->setCloudContext($entity->getCloudContext());
      $this->vmwareService->login();

      if ($entity->getPowerState() === 'POWERED_OFF') {
        $update_cpu_result = $this->vmwareService->updateVmCpu(
          $entity->getVmId(),
          $entity->getCpuCount()
        );

        $update_memory_result = $this->vmwareService->updateVmMemory(
          $entity->getVmId(),
          $entity->getMemorySize()
        );

        if (!empty($update_cpu_result['SendToWorker'])
          || !empty($update_memory_result['SendToWorker'])) {
          $this->processOperationStatus($entity, 'updated remotely');
          return TRUE;
        }
      }

      // Save author to entity.
      $entity->save();

      // Store the owner information in VMware tags.
      $cast = fn($orig): VmwareEntityInterface => $orig;
      $this->vmwareService->updateTagValueCreatedByUid($cast($entity), $entity->getVmId() ?? '');

      $this->vmwareService->updateVms([
        'VmId' => $entity->getVmId(),
      ], FALSE);

      $this->processOperationStatus($entity, 'updated');

      $this->clearCacheValues($entity->getCacheTags());

      return TRUE;
    }
    catch (VmwareServiceException
    | EntityStorageException
    | EntityMalformedException $e) {

      try {
        $this->processOperationErrorStatus($entity, 'updated');
      }
      catch (EntityMalformedException $e) {
        $this->handleException($e);
      }

      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function rebootVm(VmwareVmInterface $entity, array $params): bool {
    try {
      $this->vmwareService->setCloudContext($entity->getCloudContext());
      $this->vmwareService->login();
      $result = $this->vmwareService->rebootVm([
        'VmId' => $entity->getVmId(),
      ]);

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'rebooted remotely');
        return TRUE;
      }

      $this->vmwareService->updateVms([
        'name' => $entity->getName(),
      ], FALSE);

      $this->processOperationStatus($entity, 'rebooted');
      $this->clearCacheValues($entity->getCacheTags());
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVm(VmwareVmInterface $entity, array $params): bool {
    $this->entity = $entity;

    try {
      $this->vmwareService->setCloudContext($entity->getCloudContext());
      $this->vmwareService->login();
      $result = $this->vmwareService->deleteVm([
        'VmId' => $entity->getVmId(),
      ]);

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'deleted remotely');
        return TRUE;
      }

      $entity->delete();

      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
      return TRUE;
    }
    catch (VmwareServiceException $e) {
      // @todo Possibly refactor processOperationErrorStatus() to add the third parameter for the reason.
      if ($entity->getPowerState() === 'POWERED_ON') {
        $entity_type = $entity->getEntityType();
        $label = $entity->label();
        $message = $this->t('The @type %label could not be @passive_operation since the VM is currently running.', [
          '@type' => $entity_type->getSingularLabel(),
          '%label' => $entity->toLink($label)->toString(),
          '@passive_operation' => 'deleted',
        ]);
        $this->messenger->addError($message);
        $this->logger('vmware')->error($message);
      }
      else {
        $this->processOperationErrorStatus($entity, 'deleted');
      }
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function startVm(VmwareVmInterface $entity, array $params): bool {
    try {
      $this->vmwareService->setCloudContext($entity->getCloudContext());
      $this->vmwareService->login();
      $result = $this->vmwareService->startVm([
        'VmId' => $entity->getVmId(),
      ]);

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'started remotely');
        return TRUE;
      }

      $this->vmwareService->updateVms([
        'name' => $entity->getName(),
      ], FALSE);

      $this->processOperationStatus($entity, 'started');
      $this->clearCacheValues($entity->getCacheTags());
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function stopVm(VmwareVmInterface $entity, array $params): bool {
    try {
      $this->vmwareService->setCloudContext($entity->getCloudContext());
      $this->vmwareService->login();
      $result = $this->vmwareService->stopVm([
        'VmId' => $entity->getVmId(),
      ]);

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'stopped remotely');
        return TRUE;
      }

      $this->vmwareService->updateVms([
        'name' => $entity->getName(),
      ], FALSE);

      $this->processOperationStatus($entity, 'stopped');
      $this->clearCacheValues($entity->getCacheTags());
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function suspendVm(VmwareVmInterface $entity, array $params): bool {
    try {
      $this->vmwareService->setCloudContext($entity->getCloudContext());
      $this->vmwareService->login();
      $result = $this->vmwareService->suspendVm([
        'VmId' => $entity->getVmId(),
      ]);

      if (!empty($result['SendToWorker'])) {
        $this->processOperationStatus($entity, 'suspended remotely');
        return TRUE;
      }

      $this->vmwareService->updateVms([
        'name' => $entity->getName(),
      ], FALSE);

      $this->processOperationStatus($entity, 'suspended');
      $this->clearCacheValues($entity->getCacheTags());
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

}
