<?php

namespace Drupal\vmware\Service;

use Drupal\vmware\Entity\VmwareVmInterface;

/**
 * The Interface for VmwareOperationsService.
 */
interface VmwareOperationsServiceInterface {

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function setCloudContext($cloud_context): void;

  /**
   * Create VMware VM.
   *
   * @param \Drupal\vmware\Entity\VmwareVmInterface $entity
   *   The VMware VM.
   * @param array $params
   *   Optional parameters array.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function createVm(VmwareVmInterface $entity, array $params): bool;

  /**
   * Edit VMware VM.
   *
   * @param \Drupal\vmware\Entity\VmwareVmInterface $entity
   *   The VMware VM.
   * @param array $params
   *   Optional parameters array.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function editVm(VmwareVmInterface $entity, array $params): bool;

  /**
   * Reboot VMware VM.
   *
   * @param \Drupal\vmware\Entity\VmwareVmInterface $entity
   *   The VMware VM.
   * @param array $params
   *   Optional parameters array.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function rebootVm(VmwareVmInterface $entity, array $params): bool;

  /**
   * Delete VMware VM.
   *
   * @param \Drupal\vmware\Entity\VmwareVmInterface $entity
   *   The VMware VM.
   * @param array $params
   *   Optional parameters array.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteVm(VmwareVmInterface $entity, array $params): bool;

  /**
   * Start VMware VM.
   *
   * @param \Drupal\vmware\Entity\VmwareVmInterface $entity
   *   The VMware VM.
   * @param array $params
   *   Optional parameters array.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function startVm(VmwareVmInterface $entity, array $params): bool;

  /**
   * Stop VMware VM.
   *
   * @param \Drupal\vmware\Entity\VmwareVmInterface $entity
   *   The VMware VM.
   * @param array $params
   *   Optional parameters array.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function stopVm(VmwareVmInterface $entity, array $params): bool;

  /**
   * Suspend VMware VM.
   *
   * @param \Drupal\vmware\Entity\VmwareVmInterface $entity
   *   The VMware VM.
   * @param array $params
   *   Optional parameters array.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function suspendVm(VmwareVmInterface $entity, array $params): bool;

}
