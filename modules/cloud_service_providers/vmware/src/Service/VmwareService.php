<?php

namespace Drupal\vmware\Service;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Utility\Error;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceBase;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\user\Entity\User;
use Drupal\vmware\Entity\VmwareEntityInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * Provides VMware service.
 */
class VmwareService extends CloudServiceBase implements VmwareServiceInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The lock interface.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The credentials from set method.
   *
   * @var array
   */
  private $credentials;

  /**
   * The session ID.
   *
   * @var string
   */
  private $sessionId;

  /**
   * Constructs a new VmwareService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock interface.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    LockBackendInterface $lock,
    QueueFactory $queue_factory,
    CloudServiceInterface $cloud_service,
    ClientInterface $http_client,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->lock = $lock;
    $this->queueFactory = $queue_factory;
    $this->cloudService = $cloud_service;
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context): void {
    $this->cloudContext = $cloud_context;
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
  }

  /**
   * {@inheritdoc}
   */
  public function setCredentials(array $credentials): void {
    $this->credentials = $credentials;
  }

  /**
   * {@inheritdoc}
   */
  public function login(): void {
    $this->loadCredentials();
    $result = $this->request('post', 'rest/com/vmware/cis/session', [
      'headers' => [
        'Authorization' => 'Basic ' . base64_encode("{$this->credentials['vcenter_username']}:{$this->credentials['vcenter_password']}" ?? ''),
      ],
    ]);

    $this->sessionId = $result['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function updateVms(array $params = [], $clear = TRUE): bool {
    $this->logger('vmware_service')->info($this->t('Updating VMs for @cloud_context.', [
      '@cloud_context' => $this->cloudContext,
    ]));
    return $this->updateEntities(
      'vmware_vm',
      'Vm',
      'describeVms',
      'updateVm',
      $params,
      $clear
    );
  }

  /**
   * {@inheritdoc}
   */
  public function describeVms(array $params = []): array {
    $url = 'rest/vcenter/vm';
    if (!empty($params) && !empty($params['VmId'])) {
      $url .= '?filter.vms.1=' . $params['VmId'];
    }
    return $this->callApi('get', $url);
  }

  /**
   * {@inheritdoc}
   */
  public function showVm($vm_id): array {
    return $this->callApi('get', "rest/vcenter/vm/$vm_id");
  }

  /**
   * {@inheritdoc}
   */
  public function createVm(array $params = []): array {
    $url = 'rest/vcenter/vm';
    $body = [
      'spec' => $params,
    ];
    return $this->callApi('post', $url, ['json' => $body]);
  }

  /**
   * {@inheritdoc}
   */
  public function startVm(array $params = []): array {
    return $this->callApi('post', "rest/vcenter/vm/{$params['VmId']}/power/start");
  }

  /**
   * {@inheritdoc}
   */
  public function stopVm(array $params = []): array {
    return $this->callApi('post', "rest/vcenter/vm/{$params['VmId']}/power/stop");
  }

  /**
   * {@inheritdoc}
   */
  public function rebootVm(array $params = []): array {
    return $this->callApi('post', "rest/vcenter/vm/{$params['VmId']}/power/reset");
  }

  /**
   * {@inheritdoc}
   */
  public function suspendVm(array $params = []): array {
    return $this->callApi('post', "rest/vcenter/vm/{$params['VmId']}/power/suspend");
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVm(array $params = []): array {
    return $this->callApi('delete', "rest/vcenter/vm/{$params['VmId']}");
  }

  /**
   * {@inheritdoc}
   */
  public function updateVmMemory($vm_id, $memory_size): array {
    $body = [
      'spec' => [
        'size_MiB' => $memory_size,
      ],
    ];
    return $this->callApi(
      'patch',
      "rest/vcenter/vm/$vm_id/hardware/memory",
      ['json' => $body]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateVmCpu($vm_id, $cpu_count): array {
    $body = [
      'spec' => [
        'count' => $cpu_count,
      ],
    ];
    return $this->callApi(
      'patch',
      "rest/vcenter/vm/$vm_id/hardware/cpu",
      ['json' => $body]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function describeFolders(array $params = []): array {
    $url = 'rest/vcenter/folder';
    return $this->callApi('get', $url);
  }

  /**
   * {@inheritdoc}
   */
  public function describeDatastores(array $params = []): array {
    $url = 'rest/vcenter/datastore';
    return $this->callApi('get', $url);
  }

  /**
   * {@inheritdoc}
   */
  public function updateHosts(array $params = [], $clear = TRUE): bool {
    $this->logger('vmware_service')->info($this->t('Updating Hosts for @cloud_context.', [
      '@cloud_context' => $this->cloudContext,
    ]));
    return $this->updateEntities(
      'vmware_host',
      'Host',
      'describeHosts',
      'updateHost',
      $params,
      $clear
    );
  }

  /**
   * {@inheritdoc}
   */
  public function describeHosts(array $params = []): array {
    $url = 'rest/vcenter/host';
    return $this->callApi('get', $url);
  }

  /**
   * {@inheritdoc}
   */
  public function describeResourcePools(array $params = []): array {
    $url = 'rest/vcenter/resource_pool';
    return $this->callApi('get', $url);
  }

  /**
   * Load credentials.
   */
  private function loadCredentials(): void {
    if (empty($this->credentials)) {
      $this->credentials = $this->cloudConfigPluginManager->loadCredentials();
    }
  }

  /**
   * Setup any default parameters for the Guzzle request.
   *
   * @return array
   *   Array of parameters.
   */
  private function getDefaultParams(): array {
    // Add a slash in the end of vcenter_url if it does not exist.
    if (substr($this->credentials['vcenter_url'], -1) !== '/') {
      $this->credentials['vcenter_url'] .= '/';
    }

    $params = [
      'verify' => FALSE,
    ];
    return $params;
  }

  /**
   * Helper method to call API.
   *
   * @param string $http_method
   *   The http method in lower case.
   * @param string $endpoint
   *   The endpoint to access.
   * @param array $params
   *   Parameters to pass to the request.
   *
   * @return array
   *   Array interpretation of the response body.
   */
  private function callApi($http_method, $endpoint, array $params = []): array {
    $this->loadCredentials();
    $params['headers']['vmware-api-session-id'] = $this->sessionId;
    return $this->request($http_method, $endpoint, $params);
  }

  /**
   * Helper method to do a Guzzle HTTP Request.
   *
   * @param string $http_method
   *   The http method in lower case.
   * @param string $endpoint
   *   The endpoint to access.
   * @param array $params
   *   Parameters to pass to the request.
   *
   * @return array
   *   Array interpretation of the response body.
   */
  private function request($http_method, $endpoint, array $params = []): array {
    $output = NULL;
    try {
      $params = array_merge_recursive($this->getDefaultParams(), $params);
      $response = $this->httpClient->$http_method(
        $this->credentials['vcenter_url'] . $endpoint,
        $params
      );
      if (!empty($response)) {
        $output = $response->getBody()->getContents();
      }
    }
    catch (ClientException $error) {
      $response = $error->getResponse();
      $content = '';
      if (!empty($response)) {
        $content = $response->getBody()->getContents();
      }
      $message = new FormattableMarkup(
        'VMware API error. Error details are as follows:<pre>@response</pre>', [
          '@response' => !empty($content)
            ? $content
            : $error->getMessage(),
        ]
      );
      $this->logError('Remote API Connection', $error, $message);
    }
    catch (\Exception $error) {
      $this->logError('Remote API Connection', $error, $this->t('An unknown error
      occurred while trying to connect to the remote API. This is not a Guzzle
      error, nor an error in the remote API, rather a generic local error
      occurred. The reported error was @error',
        ['@error' => $error->getMessage()]
      ));
    }

    $result = empty($output) ? [] : json_decode($output, TRUE, 512, JSON_THROW_ON_ERROR);
    return is_array($result) ? $result : [$result];
  }

  /**
   * Log errors to watchdog and throw an exception.
   *
   * @param string $type
   *   The error type.
   * @param \Exception $exception
   *   The exception object.
   * @param string $message
   *   The message to log.
   * @param bool $throw
   *   TRUE to throw exception.
   *
   * @throws \Drupal\vmware\Service\VmwareServiceException
   */
  private function logError($type, \Exception $exception, $message, $throw = TRUE): void {
    // See also: https://www.drupal.org/node/2932520.
    Error::logException($this->logger($type), $exception, $message);
    if ($throw === TRUE) {
      throw new VmwareServiceException($message);
    }
  }

  /**
   * Update entities.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $entity_type_label
   *   The entity type label.
   * @param string $get_entities_method
   *   The method name of get entities.
   * @param string $update_entity_method
   *   The method name of update entity.
   * @param array $params
   *   The params for API Call.
   * @param bool $clear
   *   TRUE to clear stale entities.
   * @param bool $batch_mode
   *   Whether updating entities in batch.
   *
   * @return bool
   *   True or false depending on lock name.
   *
   * @throws \Drupal\vmware\Service\VmwareServiceException
   *   Thrown when unable to get get_entities_method.
   */
  private function updateEntities(
    $entity_type,
    $entity_type_label,
    $get_entities_method,
    $update_entity_method,
    array $params = [],
    $clear = TRUE,
    $batch_mode = TRUE,
  ): bool {
    $updated = FALSE;
    $lock_name = $this->getLockKey($entity_type);

    if (!$this->lock->acquire($lock_name)) {
      return FALSE;
    }

    $result = NULL;
    try {
      $result = $this->$get_entities_method($params);
    }
    catch (VmwareServiceException $e) {
      $this->logger('vmware_service')->error($e->getMessage());
    }

    if ($result !== NULL) {
      $conditions = [];
      $all_entities = $this->cloudService->loadAllEntities($this->cloudContext, $entity_type, $conditions);
      $stale = [];
      foreach ($all_entities ?: [] as $entity) {
        $key = $entity->getName();
        if (empty($stale[$key])) {
          $stale[$key] = $entity;
          continue;
        }

        $entity->delete();
      }

      if ($batch_mode) {
        /** @var \Drupal\Core\Batch\BatchBuilder $batch_builder */
        $batch_builder = $this->initBatch("$entity_type_label Update");
      }

      foreach ($result['value'] ?: [] as $entity) {
        $key = $entity['name'];

        if (isset($stale[$key])) {
          unset($stale[$key]);
        }

        if ($batch_mode) {
          $batch_params = [$this->cloudContext, $entity];
          $batch_builder->addOperation([
            VmwareBatchOperations::class,
            $update_entity_method,
          ], $batch_params);
        }
        else {
          VmwareBatchOperations::$update_entity_method($this->cloudContext, $entity);
        }
      }

      if ($batch_mode) {
        $batch_builder->addOperation([
          VmwareBatchOperations::class,
          'finished',
        ], [$entity_type, $stale, $clear]);
        $this->runBatch($batch_builder);
      }
      else {
        if (count($stale) && $clear === TRUE) {
          $this->entityTypeManager->getStorage($entity_type)->delete($stale);
        }
      }

      $updated = TRUE;
    }

    $this->lock->release($lock_name);
    return $updated;
  }

  /**
   * Initialize a new batch builder.
   *
   * @param string $batch_name
   *   The batch name.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   The initialized batch object.
   */
  protected function initBatch($batch_name): BatchBuilder {
    return (new BatchBuilder())
      ->setTitle($batch_name);
  }

  /**
   * Run the batch job to process entities.
   *
   * @param \Drupal\Core\Batch\BatchBuilder $batch_builder
   *   The batch builder object.
   */
  protected function runBatch(BatchBuilder $batch_builder): void {
    try {
      // Log the start time.
      $start = time();
      $batch_array = $batch_builder->toArray();
      batch_set($batch_array);

      // Reset the progressive so batch works w/o a web head.
      $batch = &batch_get();
      $batch['progressive'] = FALSE;
      if (PHP_SAPI === 'cli' && function_exists('drush_backend_batch_process')) {
        drush_backend_batch_process();
      }
      else {
        batch_process();
      }

      // Log the end time.
      $end = time();
      $this->logger('vmware_service')->info($this->t('@updater - @cloud_context: Batch operation took @time seconds.', [
        '@cloud_context' => $this->cloudContext,
        '@updater' => $batch_array['title'],
        '@time' => $end - $start,
      ]));
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    finally {
      // Reset the batch otherwise this operation hangs when using Drush.
      // https://www.drupal.org/project/drupal/issues/3166042
      $batch = [];
    }
  }

  /**
   * Helper static method to clear cache.
   */
  public static function clearCacheValue(): void {
    \Drupal::cache('menu')->invalidateAll();
    \Drupal::service('cache.render')->deleteAll();
    \Drupal::service('router.builder')->rebuild();
    \Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();
  }

  /**
   * Create queue items for update resources queue.
   */
  public function createResourceQueueItems(): void {
    $queue_limit = $this->configFactory->get('vmware.settings')->get('vmware_queue_limit');
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    if (empty($cloud_config)) {
      return;
    }
    $method_names = [
      'updateVms',
      'updateHosts',
    ];
    self::updateResourceQueue($cloud_config, $method_names, $queue_limit);
  }

  /**
   * {@inheritdoc}
   */
  public function getSessionId(): ?string {
    return $this->sessionId;
  }

  /**
   * {@inheritdoc}
   */
  public function createTaggingCategory(array $params = []): array {
    return $this->callApi(
      'post',
      "api/cis/tagging/category",
      ['json' => $params]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateTaggingCategory($category_id, array $params = []): array {
    return $this->callApi(
      'patch',
      "api/cis/tagging/category/{$category_id}",
      ['json' => $params]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTaggingCategory($category_id): array {
    return $this->callApi('delete', "api/cis/tagging/category/{$category_id}");
  }

  /**
   * {@inheritdoc}
   */
  public function getTaggingCategory($category_id): array {
    return $this->callApi('get', "api/cis/tagging/category/{$category_id}");
  }

  /**
   * {@inheritdoc}
   */
  public function listTaggingCategory(): array {
    return $this->callApi('get', "api/cis/tagging/category");
  }

  /**
   * {@inheritdoc}
   */
  public function createTag(array $params = []): array {
    return $this->callApi(
      'post',
      "api/cis/tagging/tag",
      ['json' => $params]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateTag($tag_id, array $params = []): array {
    return $this->callApi(
      'patch',
      "api/cis/tagging/tag/{$tag_id}",
      ['json' => $params]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function listTags(array $params = []): array {
    return $this->callApi(
      'post',
      "api/cis/tagging/tag",
      [
        'json' => $params,
        'query' => ['action' => 'list-tags-for-category'],
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTag($tag_id): array {
    return $this->callApi('delete', "api/cis/tagging/tag/{$tag_id}");
  }

  /**
   * {@inheritdoc}
   */
  public function getTag($tag_id): array {
    return $this->callApi('get', "api/cis/tagging/tag/{$tag_id}");
  }

  /**
   * {@inheritdoc}
   */
  public function attachTag($tag_id, array $params = []): array {
    return $this->callApi(
      'post',
      "api/cis/tagging/tag-association/{$tag_id}",
      [
        'json' => $params,
        'query' => ['action' => 'attach'],
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function detachTag($tag_id, array $params = []): array {
    return $this->callApi(
      'post',
      "api/cis/tagging/tag-association/{$tag_id}",
      [
        'json' => $params,
        'query' => ['action' => 'detach'],
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function listAttachedTags($params = []): array {
    return $this->callApi(
      'post',
      "api/cis/tagging/tag-association",
      [
        'json' => $params,
        'query' => ['action' => 'list-attached-tags-on-objects'],
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setTagInformation(VmwareEntityInterface $entity, $resource_id): void {
    $bundle = $entity->bundle();
    $entity_type = $entity->getEntityType();
    $entity_id = $entity_type->id();
    $resource_type = $entity_type->get('resource_type');

    $category_name = $this->cloudService->getTagKeyCreatedByUid($bundle, $entity->getCloudContext());
    $category_id = $this->getTaggingCategoryId($category_name, $resource_type);

    $tag_key_name = "{$category_name}__{$entity_id}__{$resource_id}__";

    // Get currently attached tags.
    $params = ['object_ids' => []];
    $params['object_ids'][] = [
      'id' => $resource_id,
      'type' => $resource_type,
    ];
    $attached_tags = $this->listAttachedTags($params);

    if (empty($attached_tags[0]['tag_ids'])) {
      // If there is no tag, set category and tag empty.
      $entity->setTagCategories([]);
      $entity->setTags([]);
      return;
    }

    $categories = [];
    $tags = [];
    foreach ($attached_tags[0]['tag_ids'] ?: [] as $tag_id) {
      // Get the tag information.
      $attached_tag = $this->getTag($tag_id);

      if (empty($attached_tag)) {
        continue;
      }

      if (strpos($attached_tag['name'], $tag_key_name) !== FALSE
        && $attached_tag['category_id'] === $category_id) {
        // Set the owner id.
        $uid = str_replace($tag_key_name, '', $attached_tag['name']);
        $uid = (is_numeric($uid) && !empty(User::load($uid))) ? $uid : 0;
        $entity->setOwnerId($uid);
      }

      $tags[$tag_id] = json_encode($attached_tag, JSON_THROW_ON_ERROR);

      $tag_category_id = $attached_tag['category_id'];
      if (!empty($categories[$tag_category_id])) {
        continue;
      }
      $category = $this->getTaggingCategory($tag_category_id);

      if (empty($category)) {
        continue;
      }

      $categories[$tag_category_id] = json_encode($category, JSON_THROW_ON_ERROR);
    }

    if (!empty($categories)) {
      self::setKeyValueTypeFieldValue($entity, 'tag_categories', $categories);
    }

    if (!empty($tags)) {
      self::setKeyValueTypeFieldValue($entity, 'tags', $tags);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateTagValueCreatedByUid(VmwareEntityInterface $entity, string $resource_id): void {
    $bundle = $entity->bundle();
    $entity_type = $entity->getEntityType();
    $entity_id = $entity_type->id();
    $resource_type = $entity_type->get('resource_type');

    $category_name = $this->cloudService->getTagKeyCreatedByUid($bundle, $entity->getCloudContext());

    // Tags in VMware can't have key-value.
    // So including the resource id and owner id in tag name to make it unique.
    $tag_key_name = "{$category_name}__{$entity_id}__{$resource_id}__";
    $tag_name = $tag_key_name . $entity->getOwnerId();

    // Prepare the category.
    $category_id = $this->getTaggingCategoryId($category_name, $resource_type, TRUE);
    if (empty($category_id)) {
      $entity->setTags([]);
      return;
    }

    // Get currently attached tags.
    $params = ['object_ids' => []];
    $params['object_ids'][] = [
      'id' => $resource_id,
      'type' => $resource_type,
    ];
    $attached_tags = $this->listAttachedTags($params);

    $tag_id = NULL;
    if (!empty($attached_tags[0]['tag_ids'])) {
      // Confirm currently attached tags.
      foreach ($attached_tags[0]['tag_ids'] as $attached_tag_id) {
        // Get the tag information.
        $attached_tag = $this->getTag($attached_tag_id);

        if (empty($attached_tag)) {
          continue;
        }

        if (strpos($attached_tag['name'], $tag_key_name) === FALSE) {
          continue;
        }

        $tag_id = $attached_tag_id;
        if ($attached_tag['name'] === $tag_name) {
          // If the tag name and the category name are same, do nothing.
          break;
        }

        $params = [
          'name' => $tag_name,
          'description' => $this->t('The tag for storing the owner id.'),
        ];
        // Update the tag information.
        $this->updateTag($tag_id, $params);

        break;
      }
    }

    if (empty($tag_id)) {
      // If $tag_id is empty, create a new tag and attach it.
      $tag_id = $this->getTagId($category_id, $tag_name);
      if (empty($tag_id)) {
        $entity->setTags([]);
        return;
      }
      $params = [
        'object_id' => [
          'id' => $resource_id,
          'type' => $resource_type,
        ],
      ];
      $this->attachTag($tag_id, $params);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function deleteTagValueCreatedByUid(VmwareEntityInterface $entity, string $resource_id): void {
    $bundle = $entity->bundle();
    $entity_type = $entity->getEntityType();
    $entity_id = $entity_type->id();

    $category_name = $this->cloudService->getTagKeyCreatedByUid($bundle, $entity->getCloudContext());

    // Tags in VMware can't have key-value.
    // So including the resource id and owner id in tag name to make it unique.
    $tag_key_name = "{$category_name}__{$entity_id}__{$resource_id}__";
    $tag_name = $tag_key_name . $entity->getOwnerId();

    $categories = $entity->getTagCategories();
    $tags = $entity->getTags();

    foreach ($tags ?: [] as $tag_id => $value) {
      $tag = json_decode($value, TRUE, 512, JSON_THROW_ON_ERROR);
      if ($tag['name'] !== $tag_name) {
        continue;
      }
      // If the name is same, delete the tag.
      $this->deleteTag($tag_id);

    }

    foreach ($categories ?: [] as $category_id => $value) {
      $category = json_decode($value, TRUE, 512, JSON_THROW_ON_ERROR);
      if ($category['name'] !== $category_name) {
        continue;
      }
      $tags = $this->listTags(['category_id' => $category_id]);

      if (!empty($tags)) {
        continue;
      }

      // If there is no tag for the category, delete it.
      $this->deleteTaggingCategory($category_id);
    }

  }

  /**
   * Get the category id and create a new category if not exists.
   *
   * @param string $category_name
   *   The category name.
   * @param string $resource_type
   *   The resource type.
   * @param bool $create
   *   The flag whether create a new category if not exists.
   *
   * @return string
   *   The category id.
   */
  private function getTaggingCategoryId($category_name, $resource_type, $create = FALSE): string {
    // Get all tag categories.
    $categories = $this->listTaggingCategory();
    if (!empty($categories)) {
      foreach ($categories ?: [] as $category_id) {
        $category = $this->getTaggingCategory($category_id);
        if (empty($category)) {
          continue;
        }
        if ($category['name'] !== $category_name) {
          continue;
        }
        if (empty($category['associable_types'])
          || !in_array($resource_type, $category['associable_types'], TRUE)) {
          // If the resource type isn't in associable types, add it.
          $params = [
            'name' => $category['name'],
            'description' => $category['description'],
            'cardinality' => $category['cardinality'],
            'associable_types' => $category['associable_types'],
          ];
          $params['associable_types'][] = $resource_type;
          $this->updateTaggingCategory($category_id, $params);
        }
        return $category_id;
      }
    }

    if ($create) {
      // Create a new category.
      $params = [
        'associable_types' => [$resource_type],
        'cardinality' => 'SINGLE',
        'description' => $this->t('The category for storing the owner id.'),
        'name' => $category_name,
      ];
      $result = $this->createTaggingCategory($params);
      return !empty($result) ? $result[0] : '';
    }

    return '';

  }

  /**
   * Get the tag id and create a new tag if not exists.
   *
   * @param string $category_id
   *   The category id.
   * @param string $tag_name
   *   The tag name.
   *
   * @return string
   *   The tag id.
   */
  private function getTagId($category_id, $tag_name): string {
    // Get all tags with category id.
    $tags = $this->listTags(['category_id' => $category_id]);

    if (!empty($tags)) {

      foreach ($tags ?: [] as $tag_id) {
        $tag = $this->getTag($tag_id);
        if (empty($tag)) {
          continue;
        }
        if ($tag['name'] !== $tag_name) {
          continue;
        }
        return $tag_id;
      }
    }

    // Create a new tag.
    $params = [
      'category_id' => $category_id,
      'description' => $this->t('The tag for storing the owner id.'),
      'name' => $tag_name,
    ];
    $result = $this->createTag($params);

    return !empty($result) ? $result[0] : '';

  }

  /**
   * Set key_value type field value.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   * @param string $field_name
   *   The field name.
   * @param array $value_map
   *   The value of map type.
   */
  private static function setKeyValueTypeFieldValue(EntityInterface $entity, $field_name, array $value_map): void {
    $key_values = [];
    if (empty($value_map)) {
      $value_map = [];
    }
    foreach ($value_map ?: [] as $key => $value) {
      $key_values[] = ['item_key' => $key, 'item_value' => $value ?: ''];
    }

    usort($key_values, static function ($a, $b) {
      return strcmp($a['item_key'], $b['item_key']);
    });

    $entity->set($field_name, $key_values);
  }

}
