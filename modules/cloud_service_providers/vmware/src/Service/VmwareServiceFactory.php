<?php

namespace Drupal\vmware\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceBase;
use Drupal\cloud\Service\CloudServiceInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * VmwareService factory.
 */
class VmwareServiceFactory extends CloudServiceBase {

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The lock interface.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * Guzzle Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new VmwareService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock interface.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request Object.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    LockBackendInterface $lock,
    QueueFactory $queue_factory,
    RequestStack $request_stack,
    CloudServiceInterface $cloud_service,
    ClientInterface $http_client,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->lock = $lock;
    $this->queueFactory = $queue_factory;
    $this->request = $request_stack->getCurrentRequest();
    $this->cloudService = $cloud_service;
    $this->httpClient = $http_client;
  }

  /**
   * Create service.
   *
   * @return \Drupal\vmware\Service\VmwareServiceInterface
   *   VMware service.
   */
  public function create(): VmwareServiceInterface {
    $test_mode = (bool) $this->configFactory->get('vmware.settings')->get('vmware_test_mode');
    $class_name = VmwareService::class;
    if ($test_mode) {
      $class_name = VmwareServiceMock::class;
    }
    else {
      $cloud_context = $this->request->get('cloud_context');
      if (!empty($cloud_context)) {
        $this->cloudConfigPluginManager->setCloudContext($cloud_context);
        $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
        if ($cloud_config->isRemote()) {
          $class_name = VmwareServiceRemote::class;
        }
      }
    }

    return new $class_name(
      $this->entityTypeManager,
      $this->configFactory,
      $this->cloudConfigPluginManager,
      $this->lock,
      $this->queueFactory,
      $this->cloudService,
      $this->httpClient
    );
  }

}
