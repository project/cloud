<?php

namespace Drupal\vmware\Service;

use Drupal\vmware\Entity\VmwareEntityInterface;

/**
 * VMware service interface.
 */
interface VmwareServiceInterface {

  /**
   * Set the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function setCloudContext($cloud_context): void;

  /**
   * Set credentials.
   *
   * @param array $credentials
   *   Credentials.
   */
  public function setCredentials(array $credentials): void;

  /**
   * Login to an VMware server.
   */
  public function login(): void;

  /**
   * Describe VMs.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function describeVms(array $params = []): array;

  /**
   * Start VM.
   *
   * @param array $params
   *   Parameters array to send to API.
   */
  public function startVm(array $params = []): array;

  /**
   * Stop VM.
   *
   * @param array $params
   *   Parameters array to send to API.
   */
  public function stopVm(array $params = []): array;

  /**
   * Reboot VM.
   *
   * @param array $params
   *   Parameters array to send to API.
   */
  public function rebootVm(array $params = []): array;

  /**
   * Suspend VM.
   *
   * @param array $params
   *   Parameters array to send to API.
   */
  public function suspendVm(array $params = []): array;

  /**
   * Delete VM.
   *
   * @param array $params
   *   Parameters array to send to API.
   */
  public function deleteVm(array $params = []): array;

  /**
   * Create VM.
   *
   * @param array $params
   *   Parameters array to send to API.
   */
  public function createVm(array $params = []): array;

  /**
   * Update the VMs.
   *
   * Delete old VM entities, query the api for updated entities and store
   * them as VM entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale entities.
   *
   * @return array
   *   The response of API.
   */
  public function updateVms(array $params = [], $clear = TRUE): bool;

  /**
   * Update the VM memory.
   *
   * @param string $vm_id
   *   The VM's ID.
   * @param int $memory_size
   *   The memory size.
   *
   * @return array
   *   The response of API.
   */
  public function updateVmMemory($vm_id, $memory_size): array;

  /**
   * Describe the folders.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function describeFolders(array $params = []): array;

  /**
   * Describe the datastores.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function describeDatastores(array $params = []): array;

  /**
   * Update the Hosts.
   *
   * Delete old VM entities, query the api for updated entities and store
   * them as Host entities.
   *
   * @param array $params
   *   Optional parameters array.
   * @param bool $clear
   *   TRUE to clear stale entities.
   *
   * @return array
   *   The response of API.
   */
  public function updateHosts(array $params = [], $clear = TRUE): bool;

  /**
   * Describe the hosts.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function describeHosts(array $params = []): array;

  /**
   * Describe the resource pools.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function describeResourcePools(array $params = []): array;

  /**
   * Create queue items for update resources queue.
   */
  public function createResourceQueueItems(): void;

  /**
   * Get the current session id.
   *
   * @return null|string
   *   Either NULL or the session string.
   */
  public function getSessionId(): ?string;

  /**
   * Create the category for tag.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   The response of API.
   */
  public function createTaggingCategory(array $params = []): array;

  /**
   * Update the category for tag.
   *
   * @param string $category_id
   *   The category id.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   The response of API.
   */
  public function updateTaggingCategory($category_id, array $params = []): array;

  /**
   * Delete the category for tag.
   *
   * @param string $category_id
   *   The category id.
   *
   * @return array
   *   The response of API.
   */
  public function deleteTaggingCategory($category_id): array;

  /**
   * Get the category for tag.
   *
   * @param string $category_id
   *   The category id.
   *
   * @return array
   *   The response of API.
   */
  public function getTaggingCategory($category_id): array;

  /**
   * Get the list of categories for tag.
   *
   * @return array
   *   The response of API.
   */
  public function listTaggingCategory(): array;

  /**
   * Create the tag.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   The response of API.
   */
  public function createTag(array $params = []): array;

  /**
   * Update the tag.
   *
   * @param string $tag_id
   *   The tag id.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   The response of API.
   */
  public function updateTag($tag_id, array $params = []): array;

  /**
   * Delete the tag.
   *
   * @param string $tag_id
   *   The tag id.
   *
   * @return array
   *   The response of API.
   */
  public function deleteTag($tag_id): array;

  /**
   * List the tags.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   The response of API.
   */
  public function listTags(array $params = []): array;

  /**
   * Get the tag.
   *
   * @param string $tag_id
   *   The tag id.
   *
   * @return array
   *   The response of API.
   */
  public function getTag($tag_id): array;

  /**
   * Attach the tag.
   *
   * @param string $tag_id
   *   The tag id.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   The response of API.
   */
  public function attachTag($tag_id, array $params = []): array;

  /**
   * Detach the tag.
   *
   * @param string $tag_id
   *   The tag id.
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   The response of API.
   */
  public function detachTag($tag_id, array $params = []): array;

  /**
   * Get the list of attached tags.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return array
   *   The response of API.
   */
  public function listAttachedTags(array $params = []): array;

  /**
   * Update created by tags.
   *
   * @param \Drupal\vmware\Entity\VmwareEntityInterface $entity
   *   The entity.
   * @param string $resource_id
   *   The resource id.
   */
  public function updateTagValueCreatedByUid(VmwareEntityInterface $entity, string $resource_id): void;

  /**
   * Delete created by tags.
   *
   * @param \Drupal\vmware\Entity\VmwareEntityInterface $entity
   *   The entity.
   * @param string $resource_id
   *   The resource id.
   */
  public function deleteTagValueCreatedByUid(VmwareEntityInterface $entity, string $resource_id): void;

}
