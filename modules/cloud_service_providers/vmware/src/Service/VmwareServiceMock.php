<?php

namespace Drupal\vmware\Service;

use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\vmware\Entity\VmwareEntityInterface;

/**
 * VmwareServiceMock service interacts with the VMware API.
 */
class VmwareServiceMock extends VmwareService {

  use ConfigFormBaseTrait;

  /**
   * Get mock data for a method.
   *
   * @param string $method_name
   *   The method name.
   *
   * @return array
   *   An array of the mock data for a method.
   */
  private function getMockData($method_name): array {
    return json_decode($this->config('vmware.settings')->get('vmware_mock_data'), TRUE)[$method_name] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['vmware.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function login(): void {
    $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function describeVms(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function showVm($vm_id): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function createVm(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVm(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function startVm(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function stopVm(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function rebootVm(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function describeFolders(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function describeDatastores(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function describeHosts(array $params = []): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function updateVmMemory($vm_id, $memory_size): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function updateVmCpu($vm_id, $cpu_count): array {
    return $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function setTagInformation(VmwareEntityInterface $entity, $resource_id): void {
    $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function updateTagValueCreatedByUid(VmwareEntityInterface $entity, string $resource_id): void {
    $this->getMockData(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTagValueCreatedByUid(VmwareEntityInterface $entity, string $resource_id): void {
    $this->getMockData(__FUNCTION__);
  }

}
