<?php

namespace Drupal\vmware\Service;

/**
 * VmwareServiceRemote service interacts with the VMware API.
 */
class VmwareServiceRemote extends VmwareService {

  /**
   * {@inheritdoc}
   */
  public function login(): void {
    $this->saveOperation(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function describeVms(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function showVm($vm_id): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createVm(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVm(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function startVm(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function stopVm(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function rebootVm(array $params = []): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function describeFolders(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function describeDatastores(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function describeHosts(array $params = []): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function updateVmMemory($vm_id, $memory_size): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function updateVmCpu($vm_id, $cpu_count): array {
    $this->saveOperation(__FUNCTION__, func_get_args());
    return ['SendToWorker' => TRUE];
  }

  /**
   * Save operation to queue.
   *
   * @param string $operation
   *   The operation to perform.
   * @param array $params
   *   An array of parameters.
   */
  private function saveOperation(string $operation, array $params): void {
    if (empty($this->cloudContext)) {
      return;
    }

    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    if (!$cloud_config->isRemote()) {
      return;
    }

    $entity_type_name = 'vmware_' . $this->getSnakeCase(preg_replace('/^[[:lower:]]+/', '', $operation));
    $entity_type_name_plural_camel = $this->getShortEntityTypeNamePluralCamelByTypeName($entity_type_name);
    if (empty($entity_type_name_plural_camel)) {
      $update_entities_method = NULL;
      $update_entities_method_params = [];
    }
    else {
      $update_entities_method = 'update' . $entity_type_name_plural_camel;
      $update_entities_method_params = [$params, FALSE];
    }

    $queue = $this->queueFactory->get('operation_queue_' . $cloud_config->get('cloud_cluster_name')->value . $cloud_config->get('cloud_cluster_worker_name')->value);
    $queue->createItem([
      'cloud_context' => $this->cloudContext,
      'operation' => $operation,
      'params' => $params,
      'service_name' => 'vmware',
      'update_entities_method' => $update_entities_method,
      'update_entities_method_params' => $update_entities_method_params,
    ]);
  }

}
