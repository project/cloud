@minimal @ci_job @setup
Feature: Create a cloud service provider for VMware as "Cloud administrator"
  In order to start BDD testing
  We need to create a cloud service provider

  @api
  Scenario: Add a VMware Cloud service provider
    Given I am logged in as a user with the "Cloud administrator" role
    When I visit "/admin/structure/cloud_config/add/vmware"
    And I enter "{{ cloud_service_provider_name_entered }}" for "Name"
    And I enter "{{ vcenter_url }}" for "vCenter URL"
    And I enter "{{ vcenter_username }}" for "vCenter Username"
    And I enter "{{ vcenter_password }}" for "vCenter Password"
    And I press "Save"
    And I wait {{ wait }} milliseconds
    Then I should be on "/admin/structure/cloud_config"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ cloud_service_provider_name }}" in the table
