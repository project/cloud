@minimal @ci_job
Feature: Reading and refreshing Hosts for VMware as "Authenticated User"

  @api @javascript
  Scenario: Refreshing Hosts a VMs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/vmware/{{ cloud_context }}/host"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    Then I should see the link "{{ host_name }}"
    Then I should see "{{ host_id }}" in the "{{ host_name }}" row

  @api @javascript
  Scenario: Reading Hosts a VMs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/vmware/{{ cloud_context }}/host"
    And I wait {{ wait }} milliseconds
    And I click "Refresh"
    And I should see the link "{{ host_name }}"
    And I click "{{ host_name }}"
    Then the url should match "/clouds/vmware/{{ cloud_context }}/host/"
    And I should see "{{ host_name }}"
    And I should see "{{ host_id }}"
    And I should see "{{ host_power_state }}"
    And I should see "{{ host_connection_state }}"
