@minimal @ci_job
Feature: Create, read, start, suspend, reboot, stop and delete a VMware VM
  as "Authenticated User"

  @api @javascript
  Scenario: Create a VMs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/vmware/{{ cloud_context }}/vm/add"
    And I should see the heading "Add VM"
    And I enter "{{ vm_name }}" for "Name"
    And I select "{{ host_name }}" from "Host"
    And I select "{{ vm_folder }}" from "Folder"
    And I select "{{ vm_datastore }}" from "Datastore"
    And I select "{{ vm_guest_os }}" from "Guest OS"
    And I enter "{{ vm_disk_size }}" for "Disk Size (GiB)"
    And I press "Save"
    And I wait for AJAX to finish
    Then the url should match "/clouds/vmware/{{ cloud_context }}/vm"
    And I should see "{{ vm_name }}" in the "POWERED_OFF" row
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I should see the link "{{ vm_name }}" in the table

  @api @javascript
  Scenario: Read the VMs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/vmware/{{ cloud_context }}/vm"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ vm_name }}"
    And I click "{{ vm_name }}"
    Then the url should match "/clouds/vmware/{{ cloud_context }}/vm"
    And I should see "{{ vm_name }}"
    And I should see "POWERED_OFF"
    And I should see "{{ vm_disk_size }}"

  @api @javascript
  Scenario: Start the VMs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/vmware/{{ cloud_context }}/vm"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ vm_name }}"
    And I click "{{ vm_name }}"
    And I click "Start"
    And the url should match "/start"
    And I press "Start"
    And I wait for AJAX to finish
    Then the url should match "/clouds/vmware/{{ cloud_context }}/vm"
    And I should see "{{ vm_name }}" in the "POWERED_ON" row
    And I should see the success message "has been started"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Suspend the VMs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/vmware/{{ cloud_context }}/vm"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ vm_name }}"
    And I click "{{ vm_name }}"
    And I click "Suspend"
    And the url should match "/suspend"
    And I press "Suspend"
    And I wait for AJAX to finish
    Then the url should match "/clouds/vmware/{{ cloud_context }}/vm"
    And I should see "{{ vm_name }}" in the "SUSPENDED" row
    And I should see the success message "has been suspended"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Start the VMs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/vmware/{{ cloud_context }}/vm"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ vm_name }}"
    And I click "{{ vm_name }}"
    And I click "Start"
    And the url should match "/start"
    And I press "Start"
    And I wait for AJAX to finish
    Then the url should match "/clouds/vmware/{{ cloud_context }}/vm"
    And I should see "{{ vm_name }}" in the "POWERED_ON" row
    And I should see the success message "has been started"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Reboot the VMs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/vmware/{{ cloud_context }}/vm"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ vm_name }}"
    And I click "{{ vm_name }}"
    And I click "Reboot"
    And the url should match "/reboot"
    And I press "Reboot"
    And I wait for AJAX to finish
    Then the url should match "/clouds/vmware/{{ cloud_context }}/vm"
    And I should see "{{ vm_name }}" in the "POWERED_ON" row
    And I should see the success message "has been rebooted"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Stop the VMs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/vmware/{{ cloud_context }}/vm"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ vm_name }}"
    And I click "{{ vm_name }}"
    And I click "Stop"
    And the url should match "/stop"
    And I press "Stop"
    And I wait for AJAX to finish
    Then the url should match "/clouds/vmware/{{ cloud_context }}/vm"
    And I should see "{{ vm_name }}" in the "POWERED_OFF" row
    And I should see the success message "has been stopped"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Delete the VMs
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/vmware/{{ cloud_context }}/vm"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ vm_name }}"
    And I click "{{ vm_name }}"
    And the url should match "/clouds/vmware/{{ cloud_context }}/vm/"
    And I click "Delete" in the "actions"
    And the url should match "/delete"
    And I press "Delete"
    And I wait for AJAX to finish
    Then I should be on "/clouds/vmware/{{ cloud_context }}/vm"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ vm_name }}"
