@minimal @ci_job
Feature: Create, read, launch, copy and delete a Launch template for VMware
  as "Authenticated User"

  @api @javascript
  Scenario: Create a Launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}/vmware/add"
    And I should see the heading "Add VMware"
    And I enter "{{ launch_template_name }}" for "Name"
    And I select "{{ vm_guest_os }}" from "Guest OS"
    And I enter {{ vm_disk_size }} for "Disk Size (GiB)"
    And I press "Save"
    And I wait for AJAX to finish
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}/"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Read the launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ launch_template_name }}"
    And I click "{{ launch_template_name }}"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see "{{ vm_guest_os }}"
    And I should see "{{ vm_disk_size }}"

  @api @javascript
  Scenario: Launch a launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ launch_template_name }}"
    And I click "{{ launch_template_name }}"
    And I click "Launch"
    And the url should match "/launch"
    And I select "{{ host_name }}" from "Host"
    And I select "{{ vm_folder }}" from "Folder"
    And I select "{{ vm_datastore }}" from "Datastore"
    And I press "Launch"
    Then the url should match "/clouds/vmware/{{ cloud_context }}/vm"
    And I should see "{{ launch_template_name }}" in the "POWERED_OFF" row
    And I should see the success message "has been launched"
    And I should see neither error nor warning messages

  @api @javascript
  Scenario: Copy the launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ launch_template_name }}"
    And I click "{{ launch_template_name }}"
    And the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Copy"
    And the url should match "/copy"
    And I enter "{{ launch_template_name_copy }}" for "Name"
    And I press "Copy"
    Then the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been created"
    And I should see neither error nor warning messages
    And I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the link "{{ launch_template_name_copy }}" in the table

  @api @javascript
  Scenario: Delete the copied launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ launch_template_name_copy }}"
    And I click "{{ launch_template_name_copy }}"
    And the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    And I wait for AJAX to finish
    Then I should be on "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ launch_template_name_copy }}"

  @api @javascript
  Scenario: Delete the launch template
    Given I am logged in as user "{{ drupal_user_name }}"
    When I visit "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Refresh"
    And I wait {{ wait }} milliseconds
    And I should see the link "{{ launch_template_name }}"
    And I click "{{ launch_template_name }}"
    And the url should match "/clouds/design/server_template/{{ cloud_context }}"
    And I click "Delete"
    And the url should match "/delete"
    And I press "Delete"
    And I wait for AJAX to finish
    Then I should be on "/clouds/design/server_template/{{ cloud_context }}"
    And I should see the success message "has been deleted"
    And I should see neither error nor warning messages
    And I click "Refresh"
    And I should not see the link "{{ launch_template_name }}"
