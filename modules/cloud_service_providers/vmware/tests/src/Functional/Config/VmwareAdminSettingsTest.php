<?php

namespace Drupal\Tests\vmware\Functional\Config;

use Drupal\Tests\vmware\Functional\VmwareTestBase;
use Drupal\Tests\vmware\Traits\VmwareTestFormDataTrait;

/**
 * Test Case class for VMware Cloud Admin Setting forms.
 *
 * @group VMware
 */
class VmwareAdminSettingsTest extends VmwareTestBase {

  use VmwareTestFormDataTrait;

  public const VMWARE_ADMIN_SETTINGS_REPEAT_COUNT = 2;

  /**
   * Get permissions of login user.
   *
   * @return array
   *   permissions of login user.
   */
  protected function getPermissions(): array {
    return [
      'administer vmware',
      'administer site configuration',
    ];
  }

  /**
   * Test for VMwareAdminSettings.
   *
   * @param string $setting_type
   *   The setting type id.
   * @param array $edit
   *   The array of input data.
   */
  protected function runVmwareAdminSettings($setting_type, array $edit): void {
    $this->drupalGet("/admin/config/services/cloud/vmware/{$setting_type}");
    $this->assertNoErrorMessage();

    $this->submitForm(
      $edit,
      $this->t('Save')->render()
    );
    $this->assertNoErrorMessage();
  }

  /**
   * Test VMware Admin Setting forms on Settings.
   */
  public function testVmwareAdminSettings(): void {
    $edit = $this->createVmwareSettingsTestFormData(self::VMWARE_ADMIN_SETTINGS_REPEAT_COUNT);

    for ($i = 0; $i < self::VMWARE_ADMIN_SETTINGS_REPEAT_COUNT; $i++) {
      $edit[$i] = array_intersect_key($edit[$i], array_flip(
        (array) array_rand($edit[$i], random_int(1, count($edit[$i])))
      ));
      $this->runVmwareAdminSettings('settings', $edit[$i]);
    }
  }

}
