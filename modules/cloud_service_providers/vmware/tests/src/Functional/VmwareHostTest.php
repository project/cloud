<?php

namespace Drupal\Tests\vmware\Functional;

use Drupal\Tests\cloud\Traits\CloudConfigTestEntityTrait;

/**
 * Tests VMware host.
 *
 * @group VMware
 */
class VmwareHostTest extends VmwareTestBase {

  use CloudConfigTestEntityTrait;

  public const VMWARE_HOST_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list vmware host',
      'view any vmware host',
    ];
  }

  /**
   * Tests CRUD for VMware host.
   *
   * @throws \Exception
   */
  public function testHost(): void {
    $cloud_context = $this->cloudContext;

    $data = $this->createHostTestFormData(self::VMWARE_HOST_REPEAT_COUNT);
    $this->updateHostsMockData($data);

    // Update vmware hosts.
    $this->drupalGet("/clouds/vmware/$cloud_context/host/update");
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::VMWARE_HOST_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($data[$i]['name']);
    }

    // Confirm detail page.
    for ($i = 0; $i < self::VMWARE_HOST_REPEAT_COUNT; $i++) {
      $num = $i + 1;
      $this->drupalGet("/clouds/vmware/$cloud_context/host/$num");
      $this->assertSession()->pageTextContains($data[$i]['name']);
    }

    for ($i = 0, $num = 1; $i < self::VMWARE_HOST_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all host listing exists.
      $this->drupalGet('/clouds/vmware/host');
      $this->assertNoErrorMessage();

      for ($j = 0; $j < $num; $j++) {
        $this->assertSession()->pageTextContains($data[$j]['name']);
        $this->assertSession()->pageTextContains($data[$j]['host']);
        $this->assertSession()->pageTextContains($data[$j]['power_state']);
        $this->assertSession()->pageTextContains($data[$j]['connection_state']);
      }
    }
  }

  /**
   * Test updating all host list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUpdateAllHostList(): void {
    $cloud_configs = [];

    // List Host for VMware.
    $this->drupalGet('/clouds/vmware/host');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::VMWARE_HOST_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createVmwareCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      // Add a new Host.
      $data = $this->createHostTestFormData(self::VMWARE_HOST_REPEAT_COUNT);
      $this->updateHostsMockData($data);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'Hosts',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    $this->drupalGet('/clouds/vmware/host');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::VMWARE_HOST_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($data[$i]['name']);
    }
  }

}
