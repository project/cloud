<?php

namespace Drupal\Tests\vmware\Functional;

use Drupal\Tests\cloud\Functional\CloudTestBase;
use Drupal\Tests\vmware\Traits\VmwareTestEntityTrait;
use Drupal\Tests\vmware\Traits\VmwareTestFormDataTrait;
use Drupal\Tests\vmware\Traits\VmwareTestMockTrait;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Base Test Case class for VMware.
 */
abstract class VmwareTestBase extends CloudTestBase {

  use VmwareTestEntityTrait;
  use VmwareTestFormDataTrait;
  use VmwareTestMockTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'cloud',
    'vmware',
  ];

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {

    parent::setUp();

    $this->init(__CLASS__, $this);
  }

  /**
   * Create cloud context.
   *
   * @param string $bundle
   *   The CloudConfig Bundle Type ('vmware').
   *
   * @return \Drupal\cloud\Entity\CloudConfig
   *   The cloud service provider (CloudConfig) entity.
   */
  protected function createCloudContext($bundle = 'vmware'): CloudContentEntityBase {
    $random = $this->random;
    $this->cloudContext = $random->name(8);
    return $this->createTestEntity(CloudConfig::class, [
      'type'                    => $bundle,
      'cloud_context'           => $this->cloudContext,
      'name'                    => sprintf('Vmware - %s', $random->name(8, TRUE)),
      'field_vcenter_url'       => $random->name(32, TRUE),
      'field_vcenter_username'  => $random->name(32, TRUE),
      'field_vcenter_password'  => $random->name(32, TRUE),
    ]);
  }

  /**
   * Test bulk operation for entities.
   *
   * @param string $type
   *   The name of the entity type. For example, instance.
   * @param array $entities
   *   The data of entities.
   * @param string $operation
   *   The operation.
   * @param string $passive_operation
   *   The passive voice of operation.
   * @param string $path_prefix
   *   The URL path of prefix.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function runTestEntityBulk(
    $type,
    array $entities,
    $operation = 'delete',
    $passive_operation = 'deleted',
    $path_prefix = '/clouds/vmware',
  ): void {

    $this->runTestEntityBulkImpl(
      $type,
      $entities,
      $operation,
      $passive_operation,
      $path_prefix
    );
  }

}
