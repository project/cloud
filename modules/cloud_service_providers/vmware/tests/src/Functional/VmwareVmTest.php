<?php

namespace Drupal\Tests\vmware\Functional;

use Drupal\Tests\cloud\Functional\Utils;
use Drupal\Tests\cloud\Traits\CloudConfigTestEntityTrait;

/**
 * Tests VMware VM.
 *
 * @group VMware
 */
class VmwareVmTest extends VmwareTestBase {

  use CloudConfigTestEntityTrait;

  public const VMWARE_VM_REPEAT_COUNT = 1;

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    return [
      'view all cloud service providers',
      'list vmware vm',
      'add vmware vm',
      'view any vmware vm',
      'edit any vmware vm',
      'delete any vmware vm',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'host_name1' => Utils::getRandomPrivateIp(),
      'host_name2' => Utils::getRandomPrivateIp(),
      'host1' => $this->random->name(8, TRUE),
      'host2' => $this->random->name(8, TRUE),
      'folder_name1' => 'Folder-' . $this->random->name(8, TRUE),
      'folder_name2' => 'Folder-' . $this->random->name(8, TRUE),
      'folder1' => $this->random->name(8, TRUE),
      'folder2' => $this->random->name(8, TRUE),
      'datastore_name1' => 'Datastore-' . $this->random->name(8, TRUE),
      'datastore_name2' => 'Datastore-' . $this->random->name(8, TRUE),
      'datastore1' => $this->random->name(8, TRUE),
      'datastore2' => $this->random->name(8, TRUE),
      'free_space1' => rand(1024 * 1024, 1024 * 1024 * 100),
      'free_space2' => rand(1024 * 1024, 1024 * 1024 * 100),
      'capacity1' => rand(1024 * 1024, 1024 * 1024 * 100),
      'capacity2' => rand(1024 * 1024, 1024 * 1024 * 100),
    ];
  }

  /**
   * Tests CRUD for VM.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testVm(): void {
    $cloud_context = $this->cloudContext;

    // List VM for VMware.
    $this->drupalGet("/clouds/vmware/$cloud_context/vm");
    $this->assertNoErrorMessage();

    // Add a new VM.
    $add = $this->createVmTestFormData(self::VMWARE_VM_REPEAT_COUNT);
    for ($i = 0; $i < self::VMWARE_VM_REPEAT_COUNT; $i++) {
      $this->addVmMockData($add[$i]);

      $this->drupalGet("/clouds/vmware/$cloud_context/vm/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'VM', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/vmware/$cloud_context/vm");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($add[$i]['name']);

      // Confirm power state.
      $num = $i + 1;
      $this->assertSession()->elementContains('css', "tbody tr:nth-child($num)", 'POWERED_OFF');
      $this->assertSession()->linkByHrefExists("/clouds/vmware/$cloud_context/vm/$num/start");

      // Start VM.
      $this->updateVmAttributeInMockData('power_state', 'POWERED_ON');
      $this->drupalGet("/clouds/vmware/$cloud_context/vm/$num/start");
      $this->submitForm(
        [],
        $this->t('Start')->render()
      );
      $this->assertNoErrorMessage();

      // Confirm power state.
      $num = $i + 1;
      $this->assertSession()->elementContains('css', "tbody tr:nth-child($num)", 'POWERED_ON');
      $this->assertSession()->linkByHrefExists("/clouds/vmware/$cloud_context/vm/$num/stop");

      // Reboot VM.
      $this->updateVmAttributeInMockData('power_state', 'POWERED_ON');
      $this->drupalGet("/clouds/vmware/$cloud_context/vm/$num/reboot");
      $this->submitForm(
        [],
        $this->t('Reboot')->render()
      );
      $this->assertNoErrorMessage();

      // Confirm power state.
      $num = $i + 1;
      $this->assertSession()->elementContains('css', "tbody tr:nth-child($num)", 'POWERED_ON');
      $this->assertSession()->linkByHrefExists("/clouds/vmware/$cloud_context/vm/$num/stop");

      // Stop VM.
      $this->updateVmAttributeInMockData('power_state', 'POWERED_OFF');
      $this->drupalGet("/clouds/vmware/$cloud_context/vm/$num/stop");
      $this->submitForm(
        [],
        $this->t('Stop')->render()
      );
    }
    $this->assertNoErrorMessage();

    for ($i = 0, $num = 1; $i < self::VMWARE_VM_REPEAT_COUNT; $i++, $num++) {
      // Make sure the all VM listing exists.
      $this->drupalGet("/clouds/vmware/$cloud_context/vm");
      $this->assertNoErrorMessage();

      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Edit VM.
    $edit = $this->createVmTestEditFormData(self::VMWARE_VM_REPEAT_COUNT);
    for ($i = 0, $num = 1; $i < self::VMWARE_VM_REPEAT_COUNT; $i++, $num++) {
      $this->updateVmAttributeInMockData('cpu_count', $edit[$i]['cpu_count']);
      $this->updateVmAttributeInMockData('memory_size_MiB', $edit[$i]['memory_size']);

      $this->drupalGet("/clouds/vmware/$cloud_context/vm/$num/edit");
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'VM', '%label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));

      // Make sure attributes.
      $this->drupalGet("/clouds/vmware/$cloud_context/vm/$num");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextContains($edit[$i]['cpu_count']);
      $this->assertSession()->pageTextContains($edit[$i]['memory_size']);
    }

    // Delete VM.
    for ($i = 0, $num = 1; $i < self::VMWARE_VM_REPEAT_COUNT; $i++, $num++) {

      $this->deleteVmMockData($add[$i]);

      $this->drupalGet("/clouds/vmware/$cloud_context/vm/$num/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = ['@type' => 'VM', '@label' => $add[$i]['name']];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type @label has been deleted.', $t_args)));

      // Make sure listing.
      $this->drupalGet("/clouds/vmware/$cloud_context/vm");
      $this->assertNoErrorMessage();
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }
  }

  /**
   * Tests deleting VMs with bulk operation.
   *
   * @throws \Exception
   */
  public function testVmBulk(): void {
    for ($i = 0; $i < self::VMWARE_VM_REPEAT_COUNT; $i++) {
      // Create Vms.
      $vms = $this->createVmsRandomTestFormData();
      $entities = [];
      foreach ($vms ?: [] as $vm) {
        $entities[] = $this->createVmTestEntity($vm);
      }
      $this->deleteVmMockData($vms[0]);
      $this->runTestEntityBulk('vm', $entities);
    }
  }

  /**
   * Test updating all vm list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testUpdateAllVmList(): void {
    $cloud_configs = [];

    // List VM for VMware.
    $this->drupalGet('/clouds/vmware/vm');
    $this->assertNoErrorMessage();

    // Create Cloud Config.
    for ($i = 0; $i < self::VMWARE_VM_REPEAT_COUNT; $i++) {
      $this->cloudContext = $this->random->name(8);
      $cloud_config = $this->createVmwareCloudConfigTestEntity($this->cloudContext);
      $cloud_configs[] = $cloud_config;
    }

    // Add a new VM.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $add = $this->createVmTestFormData(self::VMWARE_VM_REPEAT_COUNT);
      for ($i = 0; $i < self::VMWARE_VM_REPEAT_COUNT; $i++) {
        // $this->reloadMockData();
        $this->addVmMockData($add[$i]);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/vmware/vm');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::VMWARE_VM_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextNotContains($add[$i]['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm VMs are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'VMs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Make sure listing.
    for ($i = 0; $i < self::VMWARE_VM_REPEAT_COUNT; $i++) {
      $this->assertSession()->pageTextContains($add[$i]['name']);
    }

    // Make sure detailed and edit view.
    $num = 1;
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      for ($i = 0; $i < self::VMWARE_VM_REPEAT_COUNT; $i++, $num++) {

        // Confirm the detailed view.
        $this->drupalGet("/clouds/vmware/{$cloud_context}/vm/{$num}");

        // The edit button will be added later.
        // $this->assertSession()->linkExists($this->t('Edit'));
        // $this->assertSession()->linkByHrefExists("/clouds/vmware/{$cloud_context}/vm/{$num}/edit");
        $this->assertSession()->linkExists($this->t('Delete'));
        $this->assertSession()->linkByHrefExists("/clouds/vmware/{$cloud_context}/vm/{$num}/delete");
        $this->assertSession()->linkExists($this->t('List VMware VMs'));

        // Click 'List VMware VMs'.
        $this->clickLink($this->t('List VMware VMs'));
        $this->assertNoErrorMessage();

        // Confirm the edit view.
        $this->drupalGet("/clouds/vmware/{$cloud_context}/vm/{$num}/edit");
        $this->assertSession()->linkNotExists($this->t('Edit'));
      }
    }

    // Add a new VM.
    $num++;
    $data = [
      'name' => "VM-{$this->random->name(8, TRUE)}- " . date('Y/m/d H:i:s'),
    ];
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->addVmMockData($data);
    }

    // Make sure listing.
    $this->drupalGet('/clouds/vmware/vm');
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextNotContains($data['name']);

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm Workspaces are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'VMs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }

    // Delete Workspace in mock data.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      for ($i = 0; $i < self::VMWARE_VM_REPEAT_COUNT + 1; $i++) {
        $this->deleteVmMockData($data);
      }
    }

    // Make sure listing.
    $this->drupalGet('/clouds/vmware/vm');
    $this->assertNoErrorMessage();

    for ($i = 0; $i < self::VMWARE_VM_REPEAT_COUNT + 1; $i++) {
      $this->assertSession()->pageTextContains($data['name']);
    }

    // Click 'Refresh'.
    $this->clickLink($this->t('Refresh'));
    // Confirm VMs are successfully updated or not.
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $this->assertSession()->pageTextContains($this->t('Updated @resources of @cloud_config cloud service provider.', [
        '@resources' => 'VMs',
        '@cloud_config' => $cloud_config->getName(),
      ]));
    }
  }

}
