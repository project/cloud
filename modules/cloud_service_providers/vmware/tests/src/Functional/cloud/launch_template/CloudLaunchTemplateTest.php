<?php

namespace Drupal\Tests\vmware\Functional\cloud\launch_template;

use Drupal\Tests\cloud\Functional\Utils;
use Drupal\Tests\vmware\Functional\VmwareTestBase;

/**
 * Test VMware cloud launch templates (CloudLaunchTemplate).
 *
 * @group Cloud
 */
class CloudLaunchTemplateTest extends VmwareTestBase {

  public const CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT = 2;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getPermissions(): array {
    return [
      // Cloud service provider.
      "view {$this->cloudContext}",
      'edit cloud service providers',

      // Launch template.
      'add cloud server templates',
      'list cloud server template',
      'view any published cloud server templates',
      'launch cloud server template',
      'edit any cloud server templates',
      'delete any cloud server templates',

      // Launch template revisions.
      'access cloud server template revisions',
      'revert all cloud server template revisions',
      'delete all cloud server template revisions',

      // VM.
      'list vmware vm',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function getMockDataTemplateVars(): array {
    return [
      'host_name1' => Utils::getRandomPrivateIp(),
      'host_name2' => Utils::getRandomPrivateIp(),
      'host1' => $this->random->name(8, TRUE),
      'host2' => $this->random->name(8, TRUE),
      'folder_name1' => 'Folder-' . $this->random->name(8, TRUE),
      'folder_name2' => 'Folder-' . $this->random->name(8, TRUE),
      'folder1' => $this->random->name(8, TRUE),
      'folder2' => $this->random->name(8, TRUE),
      'datastore_name1' => 'Datastore-' . $this->random->name(8, TRUE),
      'datastore_name2' => 'Datastore-' . $this->random->name(8, TRUE),
      'datastore1' => $this->random->name(8, TRUE),
      'datastore2' => $this->random->name(8, TRUE),
      'free_space1' => rand(1024 * 1024, 1024 * 1024 * 100),
      'free_space2' => rand(1024 * 1024, 1024 * 1024 * 100),
      'capacity1' => rand(1024 * 1024, 1024 * 1024 * 100),
      'capacity2' => rand(1024 * 1024, 1024 * 1024 * 100),
    ];
  }

  /**
   * CRUD test for VMware launch template.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testVmwareLaunchTemplate(): void {

    $cloud_context = $this->cloudContext;

    // Create test.
    $add = $this->createLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {
      $this->drupalGet("/clouds/design/server_template/$cloud_context/vmware/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
    }

    // List test.
    $this->drupalGet("/clouds/design/server_template/$cloud_context");
    $this->assertNoErrorMessage();

    // Edit test.
    $edit = $this->createLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {
      // Go to listing page.
      $this->drupalGet("/clouds/design/server_template/$cloud_context");
      $this->clickLink('Edit', $i);

      $this->drupalGet($this->getUrl());
      $this->submitForm(
        $edit[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $edit[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been updated.', $t_args)));
    }

    // Delete test.
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {

      // Go to listing page.
      $this->drupalGet("/clouds/design/server_template/$cloud_context");

      $this->clickLink('Delete', 0);
      $this->drupalGet($this->getUrl());
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertNoErrorMessage();
    }

    // Make sure the deleted templates are not listed anymore.
    $this->drupalGet("/clouds/design/server_template/$cloud_context");
    $this->assertNoErrorMessage();

    for ($j = 0; $j < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $j++) {
      $this->assertSession()->pageTextNotContains($edit[$j]['name[0][value]']);
    }
  }

  /**
   * CRUD test for VMware launch template launch.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testVmwareLaunchTemplateLaunch(): void {
    $cloud_context = $this->cloudContext;

    $data = $this->createVmTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);
    $add = $this->createLaunchTemplateTestFormData(self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT);

    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {
      // field_object is automatically calculated.
      unset($add[$i]['field_object']);
      $this->drupalGet("/clouds/design/server_template/$cloud_context/vmware/add");
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '@type' => 'launch template',
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @type %label has been created.', $t_args)));
    }

    // Launch the templates.
    for ($i = 0; $i < self::CLOUD_LAUNCH_TEMPLATES_REPEAT_COUNT; $i++) {
      $this->drupalGet("/clouds/design/server_template/$cloud_context");

      $this->addVmMockData($data[$i]);

      // Navigate to the launch template, and launch it.
      $this->clickLink($add[$i]['name[0][value]']);
      $this->clickLink('Launch');

      unset($data[$i]['name']);
      unset($data[$i]['guest_os']);
      $this->drupalGet($this->getUrl());
      $this->submitForm(
        $data[$i],
        $this->t('Launch')->render()
      );
      $this->assertNoErrorMessage();

      $t_args = [
        '%label' => $add[$i]['name[0][value]'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The launch template %label has been launched.', $t_args)));
    }
  }

}
