<?php

namespace Drupal\Tests\vmware\Traits;

use Drupal\Tests\cloud\Traits\CloudTestEntityTrait;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\vmware\Entity\VmwareVm;

/**
 * The trait creating test entity for vmware testing.
 */
trait VmwareTestEntityTrait {

  use CloudTestEntityTrait;

  /**
   * Create a Terraform Config test entity.
   *
   * @param string $cloud_context
   *   The Cloud context.
   *
   * @return object
   *   The Cloud Config entity.
   *
   * @throws \Exception
   */
  protected function createVmwareCloudConfigTestEntity($cloud_context): CloudContentEntityBase {
    $random = $this->random;
    return $this->createTestEntity(CloudConfig::class, [
      'type'                    => 'vmware',
      'cloud_context'           => $cloud_context,
      'name'                    => sprintf('VMware - %s', $random->name(8, TRUE)),
      'field_vcenter_url'       => 'https://example.com/',
      'field_vcenter_username'  => $random->name(16, TRUE),
      'field_vcenter_password'  => $random->name(16, TRUE),
    ]);
  }

  /**
   * Create a Terraform VM test entity.
   *
   * @param array $vm
   *   The VM data.
   *
   * @return \Drupal\cloud\Entity\CloudContentEntityBase
   *   The role binding entity.
   */
  protected function createVmTestEntity(array $vm): CloudContentEntityBase {
    return $this->createTestEntity(VmwareVm::class, $vm);
  }

}
