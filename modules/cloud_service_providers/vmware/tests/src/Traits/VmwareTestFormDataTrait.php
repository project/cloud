<?php

namespace Drupal\Tests\vmware\Traits;

use Drupal\Component\Utility\Random;

/**
 * The trait creating form data for vmware testing.
 */
trait VmwareTestFormDataTrait {

  /**
   * Create test data for cloud service provider (CloudConfig).
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createCloudConfigTestFormData($repeat_count): array {
    $random = new Random();

    // Input Fields.
    $data = [];
    for ($i = 0, $num = 1; $i < $repeat_count; $i++, $num++) {

      $data[] = [
        'name[0][value]'                   => sprintf('conf#%d', $num),
        'cloud_context'                    => strtolower($random->name(16, TRUE)),
        'field_vcenter_url[0][value]'      => 'https://example.com/',
        'field_vcenter_username[0][value]' => $random->name(16, TRUE),
        'field_vcenter_password[0][value]' => $random->name(16, TRUE),
      ];
    }

    return $data;
  }

  /**
   * Create test data for VMware VM.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createVmTestFormData($repeat_count): array {
    $random = new Random();

    // Input Fields.
    $data = [];
    for ($i = 1; $i <= $repeat_count; $i++) {
      $data[] = [
        'name' => sprintf('VM-%s - %s', $random->name(8, TRUE), date('Y/m/d H:i:s')),
        'host' => $this->latestTemplateVars["host$i"],
        'folder' => $this->latestTemplateVars["folder$i"],
        'datastore' => $this->latestTemplateVars["datastore$i"],
        'guest_os' => 'DOS',
      ];
    }

    return $data;
  }

  /**
   * Create test data for VMware VM edit form.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createVmTestEditFormData($repeat_count): array {
    // Input Fields.
    $data = [];
    for ($i = 1; $i <= $repeat_count; $i++) {
      $data[] = [
        'memory_size' => rand(1024, 4096),
        'cpu_count' => rand(1, 5),
      ];
    }

    return $data;
  }

  /**
   * Create random VMs data.
   *
   * @return array
   *   Random VMs.
   *
   * @throws \Exception
   */
  protected function createVmsRandomTestFormData(): array {
    $vms = [];
    $count = random_int(1, 10);
    for ($i = 0, $num = 1; $i < $count; $i++, $num++) {
      $vms[] = [
        // 'cloud_context' needs to associate a fixed $cloud_context since the
        // workspace entities belong to a specific $cloud_context.
        // The Terraform organization is equal to a cloud service provider
        // (CloudConfig), which is $cloud_context here.
        'cloud_context' => $this->cloudContext,
        'name' => sprintf('vm #%d - %s - %s', $num, date('Y/m/d H:i:s'), $this->random->name(4, TRUE)),
      ];
    }

    return $vms;
  }

  /**
   * Create test data for VMware Host.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createHostTestFormData($repeat_count): array {
    $random = new Random();

    $power_states = ['POWERED_ON', 'POWERED_OFF'];
    $connection_states = ['CONNECTED', 'DISCONNECTED'];

    // Input Fields.
    $data = [];
    for ($i = 1; $i <= $repeat_count; $i++) {
      $data[] = [
        'name' => sprintf('Host-%s - %s', $random->name(8, TRUE), date('Y/m/d H:i:s')),
        'host' => 'host-' . rand(),
        'power_state' => $power_states[array_rand($power_states)],
        'connection_state' => $connection_states[array_rand($connection_states)],
      ];
    }

    return $data;
  }

  /**
   * Create cloud launch template test data.
   *
   * @param int $repeat_count
   *   Repeat count.
   *
   * @return array
   *   Test data.
   */
  protected function createLaunchTemplateTestFormData($repeat_count = 1): array {
    $random = new Random();
    $data = [];

    for ($i = 0; $i < $repeat_count; $i++) {
      $data[] = [
        'cloud_context[0][value]' => $this->cloudContext,
        'name[0][value]' => 'name-' . $random->name(8, TRUE),
        'field_guest_os' => 'DOS',
        'field_cpu_count[0][value]' => 1,
        'field_memory_size[0][value]' => 4096,
        'field_disk_size[0][value]' => 40,
      ];
    }
    return $data;
  }

  /**
   * Get the items per page on Pager options, Views, VMware Settings.
   *
   * @return array
   *   Array of items per page.
   */
  protected function getItemsPerPage(): array {
    return [10, 15, 20, 25, 50, 100];
  }

  /**
   * Create an array of random input data for Settings.
   *
   * @return array
   *   The array including random input data.
   */
  public function createVmwareSettingsTestFormData($repeat_count = 1): array {
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('vmware.settings');

    $items_per_page = $this->getItemsPerPage();

    $edit = [];
    for ($i = 0; $i < $repeat_count; $i++) {
      $edit[] = [
        'vmware_view_expose_items_per_page' => !$config->get('vmware_view_expose_items_per_page'),
        'vmware_view_items_per_page' => $items_per_page[array_rand($items_per_page)],
        'vmware_update_resources_queue_cron_time' => random_int(1, 9999),
        'vmware_queue_limit' => random_int(1, 50),
      ];
    }

    return $edit;
  }

}
