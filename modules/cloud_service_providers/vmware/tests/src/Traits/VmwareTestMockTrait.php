<?php

namespace Drupal\Tests\vmware\Traits;

use Drupal\Component\Utility\Random;

/**
 * The trait creating mock data for vmware testing.
 */
trait VmwareTestMockTrait {

  /**
   * Update createVm in mock data.
   *
   * @param array $vm
   *   The VM mock data.
   */
  protected function addVmMockData(array $vm): void {
    $mock_data = $this->getMockDataFromConfig();
    $random = new Random();
    $vm_id = 'vm-' . rand(1, 100);
    $create_vm = [
      'value' => $vm_id,
    ];

    $mock_data['createVm'] = $create_vm;
    $mock_data['describeVms'] = [
      'value' => [
        [
          'vm' => $vm_id,
          'name' => $vm['name'],
          'power_state' => 'POWERED_OFF',
          'cpu_count' => 1,
          'memory_size_MiB' => 1024,
          'cdroms' => [
            [
              'value' => [
                'state' => 'CONNECTED',
                'backing' => [
                  'iso_file' => 'isofile',
                ],
                'label' => 'cdrom - ' . $random->name(8, TRUE),
              ],
            ],
          ],
          'networks' => [
            'value' => [
              'state' => 'CONNECTED',
              'label' => 'network - ' . $random->name(8, TRUE),
            ],
          ],
        ],
      ],
    ];

    $mock_data['showVm'] = [
      'value' => [
        'guest_OS' => 'UBUNTU_64',
        'disks' => [],
        'cdroms' => [
          [
            'value' => [
              'state' => 'CONNECTED',
              'backing' => [
                'iso_file' => 'isofile',
              ],
              'label' => 'cdrom - ' . $random->name(8, TRUE),
            ],
          ],
        ],
        'nics' => [
          [
            'value' => [
              'state' => 'CONNECTED',
              'label' => 'network - ' . $random->name(8, TRUE),
              'backing' => [
                'network_name' => 'VM NETWORK - ' . $random->name(8, TRUE),
              ],
            ],
          ],
        ],
      ],
    ];

    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update deleteVm in mock data.
   *
   * @param array $vm
   *   The VM mock data.
   */
  protected function deleteVmMockData(array $vm): void {
    $mock_data = $this->getMockDataFromConfig();

    $delete_vm = [];
    $mock_data['deleteVm'] = $delete_vm;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update the attribute of VM in mock data.
   *
   * @param string $name
   *   The attribute name.
   * @param string $value
   *   The attribute $value.
   */
  protected function updateVmAttributeInMockData($name, $value): void {
    $mock_data = $this->getMockDataFromConfig();

    $mock_data['describeVms']['value'][0][$name] = $value;
    $this->updateMockDataToConfig($mock_data);
  }

  /**
   * Update describeHosts in mock data.
   *
   * @param array $data
   *   The mock data.
   *
   * @throws \Exception
   */
  protected function updateHostsMockData(array $data): void {
    $mock_data = $this->getMockDataFromConfig();

    $hosts = [];
    foreach ($data ?: [] as $host_data) {
      $hosts[] = [
        'name' => $host_data['name'],
        'connection_state' => $host_data['connection_state'],
        'host' => $host_data['host'],
        'power_state' => $host_data['power_state'],
      ];
    }
    $mock_data['describeHosts'] = [
      'value' => $hosts,
    ];
    $this->updateMockDataToConfig($mock_data);
  }

}
