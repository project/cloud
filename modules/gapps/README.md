INTRODUCTION
============

- GApps (Google Applications) is a supplemental module of AWS Cloud module,
  therefore this module does NOT work without AWS Cloud module.

- See also [Cloud](https://drupal.org/project/cloud/) module.

REQUIREMENTS
============

- Drupal `10.x` or higher (The latest version of Drupal `10.x`)
- Cloud `8.x` or higher

INSTALLATION
============

- `composer require drupal/cloud`
