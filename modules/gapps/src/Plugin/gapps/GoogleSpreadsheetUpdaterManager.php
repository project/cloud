<?php

namespace Drupal\gapps\Plugin\gapps;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginManager;
use Drupal\gapps\Annotation\GoogleSpreadsheetUpdater;

/**
 * Provides a Google spreadsheet updater manager.
 *
 * @see \Drupal\gapps\Annotation\GoogleSpreadsheetUpdater
 * @see \Drupal\gapps\Plugin\gapps\GoogleSpreadsheetUpdaterBase
 * @see \Drupal\gapps\Plugin\gapps\GoogleSpreadsheetUpdaterInterface
 * @see plugin_api
 */
class GoogleSpreadsheetUpdaterManager extends CloudPluginManager {

  /**
   * Constructs a GoogleSpreadsheetUpdaterManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/gapps', $namespaces, $module_handler, GoogleSpreadsheetUpdaterInterface::class, GoogleSpreadsheetUpdater::class);
    $this->alterInfo('google_spreadsheet_updater');
    $this->setCacheBackend($cache_backend, 'gapps:google_spreadsheet_updater');
  }

  /**
   * Delete Google spreadsheets.
   *
   * @return array
   *   The cloud configs changed.
   */
  public function deleteAllSpreadsheets(): array {
    $cloud_configs = [];
    foreach ($this->getDefinitions() ?: [] as $id => $definition) {
      $plugin = $this->createInstance($id);
      foreach ($plugin->deleteSpreadsheets() ?: [] as $spreadsheet) {
        $cloud_configs[] = $spreadsheet;
      }
    }
    return $cloud_configs;
  }

}
