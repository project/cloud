INTRODUCTION
============

- K8s to S3 enables to transfer manifests (YAML files such as deployments,
  pods) files in a K8s cluster to an S3 bucket.
- This is a supplemental module of Kubernetes (K8s) module, therefore the module
  does NOT work without K8s module.

- See also [Cloud](https://drupal.org/project/cloud/) module.

REQUIREMENTS
============

- Drupal `10.x` or higher (The latest version of Drupal `10.x`)
- Cloud `8.x` or higher

INSTALLATION
============

- `composer require drupal/cloud`
