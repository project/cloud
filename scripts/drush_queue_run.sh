#!/bin/bash
MODULES=( 'vmware' 'terraform' 'openstack' 'k8s' 'aws_cloud' )
for MODULE in ${MODULES[@]}; do
  export QUEUES="$(drush queue-list --fields=queue | grep "${MODULE}" | sort -r)"
  if [ "${QUEUES}" ]; then
    for QUEUE in ${QUEUES}; do
      drush queue-run "${QUEUE}" --time-limit=300
    done
  fi
done
