<?php

namespace Drupal\cloud\Commands;

use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Utility\Random;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\cloud\Service\CloudServiceInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush Behat commands file.
 */
class BehatCommands extends DrushCommands implements BehatCommandsInterface {

  /**
   * Cloud Service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * BehatCommands constructor.
   *
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   Cloud Service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   */
  public function __construct(CloudServiceInterface $cloud_service, ExtensionPathResolver $extension_path_resolver) {
    $this->cloudService = $cloud_service;
    $this->extensionPathResolver = $extension_path_resolver;
    parent::__construct();
  }

  /**
   * Preprocess Behat test feature files.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, config, etc.
   *
   * @option string $behat_path
   *   The Behat relative path from the module root directory. Other files are
   *   relative to this path.
   * @option string $params_files
   *   The comma-separated files defining parameters for templates. If any same
   *   keys exist across files, the values are not overwritten.
   * @option string $templates_dir
   *   The directory where the feature file templates (.feature or .twig) exist.
   * @option string $output_dir
   *   The directory where this preprocessor creates files.
   * @option string $random_str
   *   The random string to be used in templates.
   *   If not set or the length is <6, automatically set.
   *
   * @return int
   *   Return the exit code.
   *
   * @command cloud:behat:preprocess
   * @aliases cloud-behat-pre
   */
  public function preprocess(
    array $options = [
      self::OPT_BEHAT_PATH => 'tests/src/Behat',
      self::OPT_PARAMS_FILES => 'templates_vars/common.yml',
      self::OPT_TEMPLATES_DIR => 'features/templates',
      self::OPT_OUTPUT_DIR => 'features/tmp',
      self::OPT_RANDOM_STR => '',
    ],
  ): int {

    try {
      $opt_base_path = $options[self::OPT_BEHAT_PATH];
      $base_path = $this->extensionPathResolver->getPath('module', 'cloud') . '/' . $opt_base_path . '/';
      $this->validatePath(self::OPT_BEHAT_PATH, $base_path, $opt_base_path, TRUE, FALSE);

      $opt_templates_dir = $options[self::OPT_TEMPLATES_DIR];
      $templates_dir = $base_path . $opt_templates_dir;
      $this->validatePath(self::OPT_TEMPLATES_DIR, $templates_dir, $opt_templates_dir, TRUE, FALSE);

      $opt_params_files = explode(',', $options[self::OPT_PARAMS_FILES] ?: '');
      $params = [];
      foreach ($opt_params_files ?: [] as $opt_params_file) {
        $params_file = $base_path . trim($opt_params_file ?: '');
        $this->validatePath(self::OPT_PARAMS_FILES, $params_file, $opt_params_file, FALSE, FALSE);
        $file_contents = Yaml::decode(file_get_contents($params_file));
        if (empty($file_contents) || !is_array($file_contents)) {
          throw new \RuntimeException("No valid parameters are found in PARAMS_FILE: {$opt_params_file}");
        }
        $params += $file_contents;
      }
      $opt_output_dir = $options[self::OPT_OUTPUT_DIR];
      $output_dir = $base_path . $opt_output_dir;
      if (!file_exists($output_dir)) {
        if (!mkdir($tmp_dir = $output_dir, 0755, TRUE) && !is_dir($tmp_dir)) {
          throw new \RuntimeException("Failed to create a directory of OUTPUT_DIR: {$opt_output_dir}");
        }
        $this->logger()->notice(dt('Created: @opt', ['@opt' => $output_dir]));
      }
      $this->validatePath(self::OPT_OUTPUT_DIR, $output_dir, $opt_output_dir, TRUE, TRUE);

      $random = strlen($options[self::OPT_RANDOM_STR]) < self::RANDOM_NAME_LENGTH ? (new Random())->name(self::RANDOM_NAME_LENGTH) : $options[self::OPT_RANDOM_STR];
      $this->setRandomizeName($random, $params);

      $template_file_pattern = '~\.(twig|feature)$~i';
      if ($handle = opendir($templates_dir)) {
        $file_count = count(preg_grep($template_file_pattern, scandir($templates_dir)));
        $this->logger()->notice(dt('Processing @file_count file(s) under @opt_templates_dir...', [
          '@file_count' => $file_count,
          '@opt_templates_dir' => $opt_templates_dir,
        ]));
        $i = 0;
        while ($file = readdir($handle)) {
          if (preg_match($template_file_pattern, $file)) {
            $output = $output_dir . '/' . $file;
            $input = $templates_dir . '/' . $file;
            file_put_contents($output, $this->cloudService->renderTemplate(file_get_contents($input), $params));
            $i++;
            $this->logger()->notice(dt(' [@index/@file_count] Created: @opt_output_dir/@file', [
              '@file' => $file,
              '@file_count' => $file_count,
              '@index' => $i,
              '@opt_output_dir' => $opt_output_dir,
            ]));
          }
        }
        closedir($handle);
        $this->logger()->success(dt('Finished processing the templates.'));
      }
    }
    catch (\RuntimeException $e) {
      $this->logger()->error($e->getMessage());
      return DrushCommands::EXIT_FAILURE;
    }
    return DrushCommands::EXIT_SUCCESS;
  }

  /**
   * Replace a specified random string with a randomized name. Case-sensitive.
   *
   * @param string $random
   *   A random string.
   * @param array $params
   *   An associative array of params passed by reference.
   */
  private function setRandomizeName(string $random, array &$params): void {
    foreach ($params as $key => $value) {
      preg_match_all('/(@random)/i', $value, $matches_all);
      $match_count = count($matches_all[0]);
      for ($i = 0; $i < $match_count; $i++) {
        preg_match('/(^.*)(@random)(.*$)/i', $value, $matches);
        if ($matches[2] === '@random') {
          $rand = strtolower($random);
        }
        elseif ($matches[2] === '@RANDOM') {
          $rand = strtoupper($random);
        }
        else {
          $rand = $random;
        }
        $value = $matches[1] . $rand . $matches[3];
        $params[$key] = $value;
      }
    }
    $this->logger()->notice(dt('Replace @Random in templates with a random string: @random_str', [
      '@random_str' => $random,
    ]));
  }

  /**
   * Validate a directory or file path.
   *
   * @param string $path_name
   *   The path name shown in the exception message.
   * @param string $path
   *   The path to validate.
   * @param string $opt_path
   *   The path from the input or set by default.
   * @param bool $isDir
   *   TRUE for a directory. FALSE for a file.
   * @param bool $isWritable
   *   TRUE for writable check. FALSE for readable check.
   *
   * @throws RuntimeException
   *   Exception.
   */
  private function validatePath(string $path_name, string $path, string $opt_path, bool $isDir, bool $isWritable): void {
    $permission = $isWritable ? 'writable' : 'readable';
    $operation = 'is_' . $permission;
    if (!$operation($path) || ($isDir && !is_dir($path))) {
      throw new \RuntimeException("No such directory/file exists or is {$permission} for {$path_name}: {$opt_path} (path: {$path})");
    }
  }

}
