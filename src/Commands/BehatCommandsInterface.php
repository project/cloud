<?php

namespace Drupal\cloud\Commands;

/**
 * A Drush Behat commands file interface.
 */
interface BehatCommandsInterface {

  public const OPT_BEHAT_PATH = 'behat_path';
  public const OPT_PARAMS_FILES = 'params_files';
  public const OPT_TEMPLATES_DIR = 'templates_dir';
  public const OPT_OUTPUT_DIR = 'output_dir';
  public const OPT_RANDOM_STR = 'random_str';
  public const RANDOM_NAME_LENGTH = 6;

  /**
   * Preprocess Behat test feature files.
   *
   * Note: The PHPDoc of a class method is shown in the 'drush list' and 'drush
   *  help' commands. Therefore, we cannot use {@inheritdoc} in the class.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, config, etc.
   *
   * @return int
   *   Return the exit code.
   *
   * @command cloud:behat:preprocess
   * @aliases cloud-behat-pre
   */
  public function preprocess(array $options): int;

}
