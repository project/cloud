<?php

namespace Drupal\cloud\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueGarbageCollectionInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

/**
 * Drush command for queue cleanup.
 */
class QueueCommands extends DrushCommands implements QueueCommandsInterface {

  use StringTranslationTrait;

  /**
   * The active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Cloud config plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigManger;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Queue command constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager interface.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_manager
   *   Cloud config manager interface.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_worker
   *   Queue worker manager interface.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory.
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager, CloudConfigPluginManagerInterface $cloud_config_manager, QueueWorkerManagerInterface $queue_worker, QueueFactory $queue_factory) {
    parent::__construct();
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->cloudConfigManger = $cloud_config_manager;
    $this->queueWorkerManager = $queue_worker;
    $this->queueFactory = $queue_factory;
  }

  /**
   * Delete old queue items.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, config, etc.
   *
   * @option string $hours
   *   Delete items older N number of hours. Defaults to 1 hour.
   * @option string $queue-names
   *   Optional comma delimited list of queue ids.
   * @usage drush cqc
   *   Delete all cloud service provider queue items older than 1 hour.
   * @usage drush cloud:queue:cleanup --hours=5 --queue_names=queue1,queue2
   *   Delete queue items older than 5 hours with queue name of "queue1"
   *   or "queue2".
   *
   * @return int
   *   Return the exit code.
   *
   * @command cloud:queue:cleanup
   * @aliases cqc
   */
  public function cleanup(array $options = ['hours' => 1, 'queue-names' => NULL]): int {
    $queue_names = [];
    // Clean up a comma delimited list of queues.
    if (!empty($options['queue-names'])) {
      $names = StringUtils::csvToArray($options['queue-names']);
      foreach ($names as $name) {
        if ($this->isGarbageCollectible($name)) {
          $queue_names[] = $name;
        }
      }
    }
    else {
      // If no queue_names are passed, load all queue definitions, and only
      // clean up the ones created by cloud service providers.
      $providers = $this->getProviders();
      $queue_definitions = $this->queueWorkerManager->getDefinitions();
      foreach ($queue_definitions ?: [] as $key => $queue_definition) {
        $provider = $queue_definition['provider'];
        // Clean up the queue.
        if (in_array($provider, $providers, FALSE) && $this->isGarbageCollectible($key)) {
          $queue_names[] = $key;
        }
      }
    }

    if (!empty($queue_names)) {
      $count = $this->connection
        ->delete('queue')
        ->condition('created', time() - ($options['hours'] * 3600), '<')
        ->condition('name', $queue_names, 'IN')
        ->execute();
      $this->logger()->success(dt('Deleted @num from queue', [
        '@num' => $this->formatPlural($count, '1 item', '@count items'),
      ]));
    }

    return DrushCommands::EXIT_SUCCESS;
  }

  /**
   * Helper to check if queue is garbage collectible.
   *
   * @param string $queue_name
   *   Queue to check.
   *
   * @return bool
   *   TRUE if garbage collectible, FALSE otherwise.
   */
  private function isGarbageCollectible(string $queue_name): bool {
    $queue = $this->queueFactory->get($queue_name);
    return $queue instanceof QueueGarbageCollectionInterface;
  }

  /**
   * Get the provider(module name) from the cloud config plugin definition.
   *
   * @return array
   *   Array of unique providers.
   */
  private function getProviders(): array {
    $definitions = $this->cloudConfigManger->getDefinitions();
    $providers = [];
    array_walk($definitions, static function ($group) use (&$providers) {
      if (!in_array($group['provider'], $providers, TRUE)) {
        $providers[$group['provider']] = $group['provider'];
      }
    });
    return $providers;
  }

}
