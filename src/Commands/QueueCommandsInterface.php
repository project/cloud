<?php

namespace Drupal\cloud\Commands;

/**
 * Interface for queue cleanup class.
 */
interface QueueCommandsInterface {

  /**
   * Delete old queue items.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, config, etc.
   *
   * @option string $hours
   *   Delete items older N number of hours.
   * @option string $queue-names
   *   Optional comma delimited list of queue ids.
   * @usage drush cqc
   *   Delete all cloud service provider queue items older than 1 day.
   * @usage drush cloud:queue:cleanup --hours=5 --queue_names=queue1,queue2
   *   Delete queue items older than 5 hours with queue name of "queue1"
   *   or "queue2".
   *
   * @return int
   *   Return the exit code.
   *
   * @command cloud:queue:cleanup
   * @aliases cqc
   */
  public function cleanup(array $options): int;

}
