<?php

namespace Drupal\cloud\Controller;

use Drupal\Core\Entity\EntityDeleteFormTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Url;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Entity\IntermediateFormState;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller responsible for "update" URLs.
 *
 * This class is mainly responsible for updating the entities from URLs.
 */
class ApiController extends CloudApiControllerBase implements ApiControllerInterface {

  use EntityDeleteFormTrait {
    logDeletionMessage as traitLogDeletionMessage;
    getDeletionMessage as traitGetDeletionMessage;
  }

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * The entity used by this form.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface|\Drupal\Core\Entity\RevisionLogInterface
   */
  private $entity;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The CloudLaunchTemplatePluginManager.
   *
   * @var \Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManager
   */
  protected $launchTemplatePluginManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ApiController constructor.
   *
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManagerInterface $server_template_plugin_manager
   *   The cloud launch template plugin manager.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger Object.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityTypeManagerInterface $entity_type_manager,
    CloudLaunchTemplatePluginManagerInterface $server_template_plugin_manager,
    Messenger $messenger,
    ModuleHandlerInterface $module_handler,
  ) {
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->launchTemplatePluginManager = $server_template_plugin_manager;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Instance of ContainerInterface.
   *
   * @return ApiController
   *   return created object.
   */
  public static function create(ContainerInterface $container): ApiController {
    return new static(
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.cloud_launch_template_plugin'),
      $container->get('messenger'),
      $container->get('module_handler')
    );
  }

  /**
   * Gets the entity of this form.
   *
   * Provided by \Drupal\Core\Entity\EntityForm.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  private function getDeletionMessage() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();

    if (!$entity->isDefaultTranslation()) {
      return $this->t('The @entity-type %label @language translation has been deleted.', [
        '@entity-type' => $entity->getEntityType()->getSingularLabel(),
        '%label'       => $entity->label(),
        '@language'    => $entity->language()->getName(),
      ]);
    }

    return $this->traitGetDeletionMessage();
  }

  /**
   * {@inheritdoc}
   */
  private function logDeletionMessage(): void {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();
    if (!$entity->isDefaultTranslation()) {
      $this->logger($entity->getEntityType()->getProvider())->info('The @entity-type %label @language translation has been deleted.', [
        '@entity-type' => $entity->getEntityType()->getSingularLabel(),
        '%label'       => $entity->label(),
        '@language'    => $entity->language()->getName(),
      ]);
    }
    else {
      $this->traitLogDeletionMessage();
    }
  }

  /**
   * Delete a Cloud Launch Template.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The Cloud Launch Template's entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  protected function deleteCloudLaunchTemplate(CloudLaunchTemplateInterface $entity): bool {
    try {
      // Make sure that deleting a translation does not delete the whole entity.
      if (!$entity->isDefaultTranslation()) {
        $untranslated_entity = $entity->getUntranslated();
        $untranslated_entity->removeTranslation($entity->language()->getId());
        $untranslated_entity->save();
      }
      else {
        $entity->delete();
      }

      $this->entity = $entity;
      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
      $this->clearCacheValues($this->entity->getCacheTags());

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * Launch a Cloud Launch Template.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The Cloud Launch Template's entity.
   * @param array $form
   *   Array of form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  protected function launchCloudLaunchTemplate(CloudLaunchTemplateInterface $entity, array $form, FormStateInterface $form_state): bool {
    try {
      $redirect_route = $this->launchTemplatePluginManager->launch($entity, $form_state);
      // Let other modules alter the redirect after a cloud launch template has
      // been launched.
      $this->moduleHandler->invokeAll('cloud_launch_template_post_launch_redirect_alter', [
        &$redirect_route,
        $entity,
      ]);
      $this->clearCacheValues($entity->getCacheTags());

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function operateEntity(Request $request, string $cloud_service_provider, string $cloud_context, string $entity_type_id, string $entity_id, string $command): JsonResponse {
    // Create an instance of the entity.
    $entity = $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'The entity has already been deleted.',
      ], 404);
    }

    // Depending on the content of the process,
    // branching is performed with a switch statement.
    $form = [];
    $form_state = new IntermediateFormState();
    $method_name = '';
    switch ($command . '_' . $entity_type_id) {
      case 'delete_cloud_launch_template':
        $method_name = 'deleteCloudLaunchTemplate';
        break;

      case 'launch_cloud_launch_template':
        $method_name = 'launchCloudLaunchTemplate';
        break;
    }

    // Execute the process.
    $result = NULL;
    if (method_exists($this, $method_name)) {
      $result = $this->$method_name(
        $entity,
        $form,
        $form_state
      );
    }
    $this->messenger()->deleteAll();

    return !empty($result)
      ? new JsonResponse([
        'result' => 'OK',
        'id' => $entity->id(),
      ], 200)
      : new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
      ], 500);

  }

  /**
   * {@inheritdoc}
   */
  public function deleteCloudLaunchTemplatesImpl(CloudLaunchTemplateInterface $entity): bool {
    try {
      // Make sure that deleting a translation does not delete the whole entity.
      if (!$entity->isDefaultTranslation()) {
        $untranslated_entity = $entity->getUntranslated();
        $untranslated_entity->removeTranslation($entity->language()->getId());
        $untranslated_entity->save();
      }
      else {
        $entity->delete();
      }

      $this->entity = $entity;
      $this->messenger->addStatus($this->getDeletionMessage());
      $this->logDeletionMessage();
      $this->clearCacheValues($this->entity->getCacheTags());

      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCloudLaunchTemplates(string $cloud_context, array $id_list): bool {
    try {
      $this->processMultiAction('cloud_launch_template', $id_list, function ($entity) {
        return $this->deleteCloudLaunchTemplatesImpl($entity);
      });
      return TRUE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function operateEntities(Request $request, string $cloud_context, string $entity_type_id, string $command): JsonResponse {

    // Depending on the content of the process,
    // branching is performed with a switch statement.
    $id_list = json_decode($request->get('idList', '[]'), TRUE);
    $method_name = '';
    switch ($command . '_' . $entity_type_id) {
      case 'delete_cloud_launch_template':
        $method_name = 'deleteCloudLaunchTemplates';
        break;
    }

    // Execute the process.
    $result = NULL;
    if (method_exists($this, $method_name)) {
      $result = $this->$method_name(
        $cloud_context,
        $id_list
      );
    }
    // Store all messages before removing all in the next code.
    $messages = $this->messenger->all();
    // Remove all messages for cleanup before returning JSON object.
    $this->messenger->deleteAll();

    return !empty($result)
      ? new JsonResponse([
        'result' => 'OK',
        'messages' => $messages,
      ], 200)
      : new JsonResponse([
        'result' => 'NG',
        'reason' => 'Internal Server Error',
        'messages' => $messages,
      ], 500);

  }

  /**
   * {@inheritdoc}
   */
  public function getImageOptions($cloud_service_provider, $cloud_context): JsonResponse {
    /** @var \Drupal\aws_cloud\Entity\Ec2\ImageInterface[] $images */
    $images = $this->entityTypeManager
      ->getStorage("{$cloud_service_provider}_image")
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);

    $options = [];
    foreach ($images ?: [] as $image) {
      $options[$image->getImageId()] = sprintf('%s (%s)',
        $image->getName(),
        $image->getImageId()
      );
    }

    natcasesort($options);

    $output = [];
    foreach ($options ?: [] as $value => $label) {
      $output[] = [
        'value' => $value,
        'label' => $label,
      ];
    }
    return new JsonResponse($output, 200);
  }

  /**
   * {@inheritdoc}
   */
  public function getVisibleManageMenuLinks(): JsonResponse {
    $currentUser = $this->currentUser();
    $visibleMenuItemList = [];

    if (!$currentUser->isAuthenticated()) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'Invalid request.',
      ], 400);
    }

    if ($currentUser->hasPermission('view cloud service provider admin list')) {
      $visibleMenuItemList[] = [
        'label' => 'Administration',
        'url' => Url::fromRoute('entity.cloud_config.collection')->toString(),
      ];
    }

    if ($this->moduleHandler->moduleExists('roleassign') && $currentUser->hasPermission('administer users')) {
      $visibleMenuItemList[] = [
        'label' => 'Users',
        'url' => Url::fromRoute('entity.user.collection')->toString(),
      ];
    }

    if ($this->moduleHandler->moduleExists('module_permissions') && $currentUser->hasPermission('administer managed modules permissions')) {
      $visibleMenuItemList[] = [
        'label' => 'Permissions',
        'url' => Url::fromRoute('user.admin_permissions')->toString(),
      ];
    }

    return new JsonResponse($visibleMenuItemList);
  }

  /**
   * {@inheritdoc}
   */
  public function getFlavorOptions(string $cloud_service_provider, string $cloud_context, string $entity_id): JsonResponse {
    // Create an instance of the entity.
    $entity = $this->entityTypeManager
      ->getStorage('cloud_launch_template')
      ->load($entity_id);
    if (empty($entity)) {
      return new JsonResponse([], 200);
    }

    // Load flavor data and convert to output.
    /** @var \Drupal\openstack\Entity\OpenStackFlavorInterface[] $flavors */
    $flavors = $this->entityTypeManager
      ->getStorage("{$cloud_service_provider}_flavor")
      ->loadByProperties([
        'cloud_context' => $cloud_context,
      ]);
    $output = [];
    foreach ($flavors ?: [] as $flavor) {
      $output[] = [
        'value' => $flavor->getFlavorId(),
        'label' => sprintf('%s:%s:%s',
          $flavor->getName(),
          $flavor->getVcpus(),
          $flavor->getRam(),
        ),
      ];
    }
    return new JsonResponse($output, 200);
  }

}
