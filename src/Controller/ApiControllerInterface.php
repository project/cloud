<?php

namespace Drupal\cloud\Controller;

use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * {@inheritdoc}
 */
interface ApiControllerInterface {

  /**
   * Operate entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   * @param string $cloud_service_provider
   *   The cloud service provider.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $entity_id
   *   The entity ID.
   * @param string $command
   *   Operation command.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function operateEntity(Request $request, string $cloud_service_provider, string $cloud_context, string $entity_type_id, string $entity_id, string $command): JsonResponse;

  /**
   * The implementation function to Delete an OpenStack key pairs.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The Cloud Launch Template entity.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteCloudLaunchTemplatesImpl(CloudLaunchTemplateInterface $entity): bool;

  /**
   * Delete Cloud Launch Templates.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $id_list
   *   The list of entity IDs.
   *
   * @return bool
   *   TRUE when the process succeeds.
   */
  public function deleteCloudLaunchTemplates(string $cloud_context, array $id_list): bool;

  /**
   * Operate entities.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $command
   *   Operation command.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function operateEntities(Request $request, string $cloud_context, string $entity_type_id, string $command): JsonResponse;

  /**
   * Get select options of image.
   *
   * @param string $cloud_service_provider
   *   The cloud service provider.
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getImageOptions($cloud_service_provider, $cloud_context): JsonResponse;

  /**
   * Get list of visible manage menu links.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getVisibleManageMenuLinks(): JsonResponse;

  /**
   * Get select options of flavor.
   *
   * @param string $cloud_service_provider
   *   The cloud service provider.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_id
   *   The entity ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getFlavorOptions(string $cloud_service_provider, string $cloud_context, string $entity_id): JsonResponse;

}
