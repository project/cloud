<?php

namespace Drupal\cloud\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * {@inheritdoc}
 */
class CloudApiControllerBase extends ControllerBase implements CloudApiControllerBaseInterface {

  use CloudContentEntityTrait;

  /**
   * {@inheritdoc}
   */
  public function updateResourceList(string $cloud_context, string $entity_type, bool $all = FALSE): JsonResponse {
    $method_name = 'update'
      . ($cloud_context === '' && $all ? 'All' : '')
      . $this->getCamelCaseWithoutWhitespace($entity_type)
      . 'List';

    if (!method_exists($this, $method_name)) {
      return new JsonResponse([
        'result' => 'NG',
        'reason' => 'This method is not implemented.',
        'messages' => [
          'error' => [
            'This method is not implemented.',
          ],
        ],
      ], 404);
    }

    $this->$method_name($cloud_context);
    return new JsonResponse([
      'result' => 'OK',
      'messages' => $this->messenger->all(),
    ]);
  }

  /**
   * Returns the message to show the user after an item was processed.
   *
   * @param int $count
   *   Count of processed translations.
   *
   * @return \Drupal\Core\StringTranslation\PluralTranslatableMarkup
   *   The item processed message.
   */
  private function getProcessedMessage($count): PluralTranslatableMarkup {

    return $this->formatPlural($count, 'Processed @count item.', 'Processed @count items.');
  }

  /**
   * The wrapper function that operates to multiple entities.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $id_list
   *   The list of entity IDs.
   * @param callable $impl_function
   *   The name of the implementation function.
   */
  protected function processMultiAction(string $entity_type_id, array $id_list, callable $impl_function): void {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $entities = $storage->loadMultiple($id_list);
    $total_count = 0;
    $first_entity = NULL;

    foreach ($entities ?: [] as $entity) {
      if ($impl_function($entity)) {
        $total_count++;
      }
      if ($total_count === 1) {
        $first_entity = $entity;
      }
    }

    if ($total_count) {
      $this->messenger->addStatus($this->getProcessedMessage($total_count));
    }

    // Fire the dispatch event. Send the first entity along.
    if (!empty($first_entity)) {
      $this->dispatchSubmitEvent($first_entity);
    }

    $this->clearCacheValues();
  }

}
