<?php

namespace Drupal\cloud\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * {@inheritdoc}
 */
interface CloudApiControllerBaseInterface {

  /**
   * Update all entities in particular cloud region.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $entity_type
   *   The entity type.
   * @param bool $all
   *   If TRUE, execute updateAllXxxList method for all cloud contexts.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function updateResourceList(string $cloud_context, string $entity_type, bool $all = FALSE): JsonResponse;

}
