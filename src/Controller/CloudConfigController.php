<?php

namespace Drupal\cloud\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudService;
use Drupal\cloud\Traits\AccessCheckTrait;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Class CloudConfigController.
 *
 *  Returns responses for cloud service provider (CloudConfig) routes.
 */
class CloudConfigController extends ControllerBase implements ContainerInjectionInterface {

  use CloudContentEntityTrait;
  use AccessCheckTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cloud service provider (CloudConfig) storage.
   *
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  protected $cloudConfigStorage;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Render\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The cloud config plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManager
   */
  protected $cloudConfigPluginManager;

  /**
   * Constructs an OperationsController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\RevisionableStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud config plugin manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RevisionableStorageInterface $entity_storage,
    RouteMatchInterface $route_match,
    RendererInterface $renderer,
    DateFormatterInterface $date_formatter,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
  ) {

    $this->entityTypeManager = $entity_type_manager;
    $this->cloudConfigStorage = $entity_storage;
    $this->routeMatch = $route_match;
    $this->renderer = $renderer;
    $this->dateFormatter = $date_formatter;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager,
      $entity_type_manager->getStorage('cloud_config'),
      $container->get('current_route_match'),
      $container->get('renderer'),
      $container->get('date.formatter'),
      $container->get('plugin.manager.cloud_config_plugin')
    );
  }

  /**
   * Displays a cloud service provider (CloudConfig) revision.
   *
   * @param int $cloud_config_revision
   *   The cloud service provider (CloudConfig) revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($cloud_config_revision): array {

    /** @var \Drupal\cloud\Entity\CloudConfigInterface $cloud_config */
    $cloud_config = $this->cloudConfigStorage->loadRevision($cloud_config_revision);

    $view_builder = $this->entityTypeManager
      ->getViewBuilder('cloud_config');

    try {
      return $view_builder->view($cloud_config);
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }

    return [];
  }

  /**
   * Page title callback for a cloud service provider (CloudConfig) revision.
   *
   * @param int $cloud_config_revision
   *   The cloud service provider (CloudConfig) revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($cloud_config_revision): TranslatableMarkup {

    /** @var \Drupal\cloud\Entity\CloudConfigInterface $cloud_config */
    $cloud_config = $this->cloudConfigStorage->loadRevision($cloud_config_revision);

    return $this->t('Revision of %title from %date', [
      '%title' => !empty($cloud_config) ? $cloud_config->label() : 'N/A',
      '%date' => $this->dateFormatter->format(!empty($cloud_config) ? $cloud_config->getRevisionCreationTime() : 'N/A'),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Cloud config.
   *
   * @param \Drupal\cloud\Entity\CloudConfigInterface $cloud_config
   *   The cloud service provider (CloudConfig) entity.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(CloudConfigInterface $cloud_config): array {
    $account = $this->currentUser();
    $langcode = $cloud_config->language()->getId();
    $langname = $cloud_config->language()->getName();
    $languages = $cloud_config->getTranslationLanguages();
    $has_translations = (count($languages) > 1);

    $build['#title'] = $has_translations
      ? $this->t('@langname revisions for %title', [
        '@langname' => $langname,
        '%title' => $cloud_config->label(),
      ])
      : $this->t('Revisions for %title', [
        '%title' => $cloud_config->label(),
      ]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission('revert all cloud service provider revisions') || $account->hasPermission('administer cloud service providers')));
    $delete_permission = (($account->hasPermission('delete all cloud service provider revisions') || $account->hasPermission('administer cloud service providers')));

    $rows = [];

    $vids = $this->cloudConfigStorage->revisionIds($cloud_config);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) ?: [] as $vid) {

      /** @var \Drupal\cloud\CloudConfigInterface $revision */
      $revision = $this->cloudConfigStorage->loadRevision($vid);

      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid !== $cloud_config->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.cloud_config.revision', [
            'cloud_config' => $cloud_config->id(),
            'cloud_config_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $cloud_config->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderInIsolation($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row ?: [] as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.cloud_config.translation_revert', [
                'cloud_config' => $cloud_config->id(),
                'cloud_config_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.cloud_config.revision_revert', [
                'cloud_config' => $cloud_config->id(),
                'cloud_config_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.cloud_config.revision_delete', [
                'cloud_config' => $cloud_config->id(),
                'cloud_config_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['cloud_config_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

  /**
   * Checks user access for a specific request based on the cloud context.
   *
   * Supports 'AND' and 'OR' access checks, similar to _permission definition in
   * *.routing.yml.
   *
   * @param string $cloud_context
   *   Cloud context to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   * @param string $entity_type_id
   *   The entity type.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route, string $entity_type_id = ''): AccessResultInterface {
    // Replace perm option if entity_type_id string is included.
    $perm = $route->getOption('perm');
    if (strpos($perm, '{entity_type_id}') !== FALSE && !empty($entity_type_id)) {
      $route->setOption('perm', str_replace('{entity_type_id}', str_replace('_', ' ', $entity_type_id), $perm));
    }

    // @FIXME: Change the validation order to be local Drupal permission first.
    // Validating the API permissions of a cloud service provider, e.g., AWS IAM
    // permissions.
    $access_result = !empty($cloud_context)
      ? $this->cloudConfigPluginManager->access($cloud_context, $account, $route)
      : AccessResult::allowed();

    // Validation of the local Drupal permissions.
    return $this->checkPermissions(
      $route->getOption('perm'),
      $access_result,
      $account,
      $cloud_context
    );
  }

  /**
   * Strict access check for entity.*.list_update.all routes.
   *
   * This method checks if a user has access to a particular
   * cloud service provider in addition to any permissions defined the in
   * *.routing.yml files.
   *
   * To use this access check, the route must pass the `entity_type` as an
   * option.  The access check will use that parameter to check access.
   *
   * Optionally, pass the `cloud_config_bundle` parameter to load a specific
   * cloud config bundle.  If not passed, the bundle will be derived from
   * the entity definition.  This is helpful if a module provides more than
   * one cloud_config_bundle
   *
   * Optionally, pass the `bypass_own_any_check=true` parameter to skip any/own
   * access check.
   *
   * Example route declaration:
   * entity.aws_cloud_transit_gateway.list_update.all:
   *   path: '/clouds/aws_cloud/transit_gateway/update'
   *   defaults:
   *     _controller: '\Drupal\aws_cloud\Controller\Vpc\ApiController::updateAllTransitGatewayList'
   *   requirements:
   *     _custom_access: '\Drupal\cloud\Controller\CloudConfigController::checkAccessForUpdateAll'
   *   options:
   *     entity_type: 'aws_cloud_transit_gateway'
   *     cloud_config_bundle: 'bundle_name'
   *     bypass_any_own_check: true
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function checkAccessForUpdateAll(AccountInterface $account, Route $route): AccessResultInterface {
    $cloud_configs = [];
    // If no entity type passed, return.
    $entity_type = $route->getOption('entity_type');
    if (empty($entity_type)) {
      return AccessResult::neutral();
    }
    try {
      // Load cloud_configs using `cloud_config_bundle` parameter if defined in
      // *.routing.yml.  Useful if a cloud service provider provides multiple
      // cloud_config bundles.
      // Derive bundle from the provider property of the entity definition
      // if cloud_config_bundle not passed.
      $entity_definition = $this->entityTypeManager->getDefinition($entity_type);
      $cloud_configs = $this->entityTypeManager
        ->getStorage('cloud_config')
        ->loadByProperties([
          'type' =>
          empty($route->getOption('cloud_config_bundle')) ?
          $entity_definition->get('provider') :
          $route->getOption('cloud_config_bundle'),
        ]);
      // Return neutral if cloud_configs is empty.
      if (empty($cloud_configs)) {
        return AccessResult::neutral();
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return AccessResult::forbidden();
    }

    foreach ($cloud_configs ?: [] as $cloud_config) {
      // If bypass_any_own_check option is passed and TRUE, skip any/own access
      // check.
      if (!empty($route->getOption('bypass_any_own_check'))
        && $route->getOption('bypass_any_own_check') === TRUE) {
        $access_result = AccessResult::allowed();
      }
      else {
        // Convert underscore to whitespace in entity name.
        // Use a static trait method through CloudService.
        $entity_name = CloudService::convertUnderscoreToWhitespace($entity_type);
        $access_result = $this->allowedIfCanAccessCloudConfigWithOwner(
          $cloud_config,
          $account,
          "edit own {$entity_name}",
          "edit any {$entity_name}"
        );
        if ($access_result->isNeutral()) {
          return AccessResult::neutral();
        }
      }
      // If additional permissions are passed, check those.
      if (empty($route->getOption('perm'))) {
        continue;
      }
      // Check remaining permissions.
      $results = $this->checkPermissions(
        $route->getOption('perm'),
        $access_result,
        $account,
        $cloud_config->getCloudContext());
      if ($results->isAllowed() === FALSE) {
        return AccessResult::neutral();
      }
    }
    return AccessResult::allowed();
  }

}
