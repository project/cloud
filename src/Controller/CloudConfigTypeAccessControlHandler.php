<?php

namespace Drupal\cloud\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Traits\AccessCheckTrait;

/**
 * Access controller for the cloud service provider (CloudConfigType) entity.
 *
 * @see \Drupal\cloud\Entity\CloudConfig.
 */
class CloudConfigTypeAccessControlHandler extends EntityAccessControlHandler {

  use AccessCheckTrait;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    /** @var \Drupal\cloud\Entity\CloudConfigTypeInterface $entity */
    switch ($operation) {
      case 'view':
        // Add the permissions to allow to display
        // in the cloud service provider admin list.
        return AccessResult::allowedIfHasPermissions(
          $account,
          [
            'view cloud service provider admin list',
            'administer site configuration',
          ],
          'OR'
        );

      case 'edit':
      case 'update':
      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['administer site configuration']);
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
