<?php

namespace Drupal\cloud\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\user\UserAuthInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller responsible for "health check" URLs.
 */
class CloudHealthCheckController extends ControllerBase implements CloudHealthCheckControllerInterface {
  use CloudContentEntityTrait;

  /**
   * UserAuth object.
   *
   * @var \Drupal\user\UserAuthInterface
   */
  protected $userAuth;

  /**
   * Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * CloudHealthCheckController constructor.
   *
   * @param \Drupal\user\UserAuthInterface $user_auth
   *   The user authentication service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(
    UserAuthInterface $user_auth,
    RequestStack $request_stack,
  ) {
    $this->userAuth = $user_auth;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) : CloudHealthCheckController {
    return new static(
      $container->get('user.auth'),
      $container->get('request_stack')
    );
  }

  /**
   * Returns the current date and time formatted according to ISO-8601.
   *
   * This function retrieves the current date and time and formats it as per the
   * ISO-8601 standard (e.g., "Y-m-d\TH:i:sP").
   *
   * @return string
   *   The formatted date and time string in ISO-8601 format.
   */
  private static function getCurrentDateTime(): string {
    // Use system's timezone.
    return (new \DateTimeImmutable('now', new \DateTimeZone(date_default_timezone_get())))->format(\DateTimeInterface::ATOM);
  }

  /**
   * Creates JsonResponse from an associative array.
   *
   * @param array $json
   *   An associative array to be converted to JSON response.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response object.
   */
  private function createJsonResponse(array $json): JsonResponse {
    $json['status'] ??= self::STATUS_FAIL;
    $json['time'] = self::getCurrentDateTime();
    $status_code = $json['status'] === self::STATUS_SUCCESS ? 200 : 503;
    $response = new JsonResponse($json, $status_code);
    $response->setCache(['no_cache' => TRUE]);

    $this->logger('health_check')->info('Health check status is @status: @message', [
      '@status' => $json['status'],
      '@message' => $json['message'] ?? '',
    ]);

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function healthCheck(): JsonResponse {
    $username = $this->request->get('username');
    $password = $this->request->get('password');

    if (empty($username) || empty($password)) {
      return $this->createJsonResponse([
        'status' => self::STATUS_FAIL,
        'message' => 'Username and password are required',
      ]);
    }
    $uid = $this->userAuth->authenticate($username, $password);
    $auth_result = empty($uid) ? [
      'status' => self::STATUS_FAIL,
      'message' => 'User authentication failed',
    ] : [
      'status' => self::STATUS_SUCCESS,
    ];

    return $this->createJsonResponse($auth_result);
  }

}
