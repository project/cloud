<?php

namespace Drupal\cloud\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Interface for the health check controller.
 */
interface CloudHealthCheckControllerInterface {

  /**
   * The value of the JSON property 'status'.
   *
   * This value means that the health check was successful.
   *
   * @var int
   */
  public const STATUS_SUCCESS = 1;

  /**
   * The value of the JSON property 'status'.
   *
   * This value means that the health check was failed.
   *
   * @var int
   */
  public const STATUS_FAIL = 0;

  /**
   * Perform a health check.
   *
   * This method checks the health status by authenticating
   * the user with a provided username and password.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response containing the health status and time.
   */
  public function healthCheck(): JsonResponse;

}
