<?php

namespace Drupal\cloud\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManagerInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Route;

/**
 * Class CloudLaunchTemplateController.
 *
 *  Returns responses for cloud launch template routes.
 */
class CloudLaunchTemplateController extends ControllerBase implements ContainerInjectionInterface, CloudLaunchTemplateControllerInterface {

  use CloudContentEntityTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cloud launch template (CloudLaunchTemplate) storage.
   *
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  protected $cloudLaunchTemplateStorage;

  /**
   * The CloudLaunchTemplatePluginManager.
   *
   * @var \Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManagerPluginManager
   */
  protected $launchTemplatePluginManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Render\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs an OperationsController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\RevisionableStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManagerInterface $server_template_plugin_manager
   *   The cloud launch template plugin manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler interface.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RevisionableStorageInterface $entity_storage,
    RouteMatchInterface $route_match,
    CloudLaunchTemplatePluginManagerInterface $server_template_plugin_manager,
    RendererInterface $renderer,
    DateFormatterInterface $date_formatter,
    ModuleHandlerInterface $module_handler,
  ) {

    $this->entityTypeManager = $entity_type_manager;
    $this->cloudLaunchTemplateStorage = $entity_storage;
    $this->routeMatch = $route_match;
    $this->launchTemplatePluginManager = $server_template_plugin_manager;
    $this->renderer = $renderer;
    $this->dateFormatter = $date_formatter;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager,
      $entity_type_manager->getStorage('cloud_launch_template'),
      $container->get('current_route_match'),
      $container->get('plugin.manager.cloud_launch_template_plugin'),
      $container->get('renderer'),
      $container->get('date.formatter'),
      $container->get('module_handler')
    );
  }

  /**
   * Displays a cloud launch template  revision.
   *
   * @param int $cloud_launch_template_revision
   *   The cloud launch template  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($cloud_launch_template_revision): array {

    /** @var \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template */
    $cloud_launch_template = $this->cloudLaunchTemplateStorage->loadRevision($cloud_launch_template_revision);

    $view_builder = $this->entityTypeManager->getViewBuilder('cloud_launch_template');

    try {
      return $view_builder->view($cloud_launch_template);
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }

    return [];
  }

  /**
   * Page title callback for a cloud launch template  revision.
   *
   * @param int $cloud_launch_template_revision
   *   The cloud launch template  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($cloud_launch_template_revision): TranslatableMarkup {

    /** @var \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template */
    $cloud_launch_template = $this->cloudLaunchTemplateStorage->loadRevision($cloud_launch_template_revision);

    return $this->t('Revision of %title from %date', [
      '%title' => !empty($cloud_launch_template) ? $cloud_launch_template->label() : 'N/A',
      '%date' => !empty($cloud_launch_template) ? $this->dateFormatter->format($cloud_launch_template->getRevisionCreationTime()) : 'N/A',
    ]);
  }

  /**
   * Generates an overview table of older revisions of a cloud launch template .
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template
   *   The cloud launch template entity.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(CloudLaunchTemplateInterface $cloud_launch_template): array {
    $account = $this->currentUser();
    $langcode = $cloud_launch_template->language()->getId();
    $langname = $cloud_launch_template->language()->getName();
    $languages = $cloud_launch_template->getTranslationLanguages();
    $has_translations = (count($languages) > 1);

    $build['#title'] = $has_translations
    ? $this->t('@langname revisions for %title', [
      '@langname' => $langname,
      '%title' => $cloud_launch_template->label(),
    ])
    : $this->t('Revisions for %title', [
      '%title' => $cloud_launch_template->label(),
    ]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission('revert all cloud server template revisions') || $account->hasPermission('administer cloud server templates')));
    $delete_permission = (($account->hasPermission('delete all cloud server template revisions') || $account->hasPermission('administer cloud server templates')));

    $rows = [];

    $vids = $this->cloudLaunchTemplateStorage->revisionIds($cloud_launch_template);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) ?: [] as $vid) {

      /** @var \Drupal\cloud\Entity\CloudLaunchTemplateInterface $revision */
      $revision = $this->cloudLaunchTemplateStorage->loadRevision($vid);

      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid !== $cloud_launch_template->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.cloud_launch_template.revision', [
            'cloud_context' => $cloud_launch_template->getCloudContext(),
            'cloud_launch_template' => $cloud_launch_template->id(),
            'cloud_launch_template_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $cloud_launch_template->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderInIsolation($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row ?: [] as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations
                ? Url::fromRoute('entity.cloud_launch_template.translation_revert', [
                  'cloud_context' => $cloud_launch_template->getCloudContext(),
                  'cloud_launch_template' => $cloud_launch_template->id(),
                  'cloud_launch_template_revision' => $vid,
                  'langcode' => $langcode,
                ])
                : Url::fromRoute('entity.cloud_launch_template.revision_revert', [
                  'cloud_context' => $cloud_launch_template->getCloudContext(),
                  'cloud_launch_template' => $cloud_launch_template->id(),
                  'cloud_launch_template_revision' => $vid,
                ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.cloud_launch_template.revision_delete', [
                'cloud_context' => $cloud_launch_template->getCloudContext(),
                'cloud_launch_template' => $cloud_launch_template->id(),
                'cloud_launch_template_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['cloud_launch_template_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#sticky' => TRUE,
    ];

    return $build;
  }

  /**
   * Checks user access to launch for cloud launch template.
   *
   * @param string $cloud_context
   *   Cloud context to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   * @param int $cloud_launch_template
   *   Cloud launch template entity ID.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function launchAccess($cloud_context, AccountInterface $account, Route $route, $cloud_launch_template = NULL): AccessResultInterface {
    // The template does not need to be approved for
    // users with 'launch cloud server template' permission
    // unlike in the case of 'launch approved cloud server template'.
    $check_workflow_status = $account->hasPermission('launch cloud server template') ? NULL : CloudLaunchTemplateInterface::APPROVED;
    return $this->workflowStatusAccess($cloud_context, $account, $route, $cloud_launch_template, $check_workflow_status);
  }

  /**
   * Checks user access to approve for cloud launch template.
   *
   * @param string $cloud_context
   *   Cloud context to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   * @param int $cloud_launch_template
   *   Cloud launch template entity ID.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function approveAccess($cloud_context, AccountInterface $account, Route $route, $cloud_launch_template = NULL): AccessResultInterface {
    $entity = $this->cloudLaunchTemplateStorage->load($cloud_launch_template);

    foreach (CloudLaunchTemplateInterface::PERMISSIONS_TO_APPROVE ?: [] as $bundle => $permission) {
      if ($entity->bundle() === $bundle
      && $account->hasPermission($permission)) {
        return $this->workflowStatusAccess(
          $cloud_context,
          $account,
          $route,
          $cloud_launch_template,
          CloudLaunchTemplateInterface::REVIEW
        );
      }
    }

    return AccessResult::neutral();
  }

  /**
   * Checks user access to review for cloud launch template.
   *
   * @param string $cloud_context
   *   Cloud context to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   * @param int $cloud_launch_template
   *   Cloud launch template entity ID.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function reviewAccess($cloud_context, AccountInterface $account, Route $route, $cloud_launch_template = NULL): AccessResult {
    $entity = $this->cloudLaunchTemplateStorage->load($cloud_launch_template);
    if (empty($entity)
      || empty($entity->hasField('field_workflow_status'))
      || empty($entity->field_workflow_status)) {
      return AccessResult::neutral();
    }

    return $this->workflowStatusAccess($cloud_context, $account, $route, $cloud_launch_template, CloudLaunchTemplateInterface::DRAFT);
  }

  /**
   * Checks user access for cloud launch template.
   *
   * @param string $cloud_context
   *   Cloud context to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   * @param int $cloud_launch_template
   *   Cloud launch template entity ID.
   * @param string $check_workflow_status
   *   Cloud launch template workflow status.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function workflowStatusAccess($cloud_context, AccountInterface $account, Route $route, $cloud_launch_template = NULL, $check_workflow_status = NULL): AccessResultInterface {
    if (empty($cloud_launch_template) || empty($check_workflow_status)) {
      return $this->access($cloud_context, $account, $route);
    }

    $entity = $this->cloudLaunchTemplateStorage->load($cloud_launch_template);
    if (empty($entity) || empty($entity->hasField('field_workflow_status')) || empty($entity->field_workflow_status)) {
      // Exit entities that are not Workflow targets or entity get failed.
      return $this->access($cloud_context, $account, $route);
    }

    $workflow_status = $entity->get('field_workflow_status')->value;
    return $workflow_status === $check_workflow_status
      ? $this->access($cloud_context, $account, $route)
      : AccessResult::neutral();

  }

  /**
   * Checks user access.
   *
   * @param string $cloud_context
   *   Cloud context to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface {
    // @FIXME: Change the validation order to be local Drupal permission first.
    // Validating the API permissions of a cloud service provider, e.g., AWS IAM
    // permissions.
    $access_result = $this->launchTemplatePluginManager->access($cloud_context, $account, $route);

    // Validation of the local Drupal permissions.
    $perm = $route->getOption('perm');
    if (!$account->hasPermission('view all cloud service providers')
      && !$account->hasPermission('view ' . $cloud_context)) {
      return AccessResult::neutral()
        ->andIf($access_result);
    }

    // Allow to conjunct the permissions with OR ('+') or AND (',').
    $split = explode(',', $perm);
    // Support AND permission check.
    if (count($split) > 1) {
      return AccessResult::allowedIfHasPermissions($account, $split, 'AND')
        ->andIf($access_result);
    }
    else {
      $split = explode('+', $perm);
      return AccessResult::allowedIfHasPermissions($account, $split, 'OR')
        ->andIf($access_result);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function launch(CloudLaunchTemplateInterface $cloud_launch_template): RedirectResponse {
    $redirect_route = $this->launchTemplatePluginManager->launch($cloud_launch_template);
    // Let modules alter the redirect after a cloud launch template has been
    // launched.
    $this->moduleHandler->invokeAll('cloud_launch_template_post_launch_redirect_alter', [
      &$redirect_route,
      $cloud_launch_template,
    ]);
    return $this->redirect($redirect_route['route_name'], $redirect_route['params'] ?? []);
  }

}
