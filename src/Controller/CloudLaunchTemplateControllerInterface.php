<?php

namespace Drupal\cloud\Controller;

use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Common interfaces for the CloudLaunchTemplateControllerInterface.
 */
interface CloudLaunchTemplateControllerInterface {

  /**
   * Launch Operation.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template
   *   The instance of CloudLaunchTemplateInterface.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  public function launch(CloudLaunchTemplateInterface $cloud_launch_template): RedirectResponse;

}
