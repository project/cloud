<?php

namespace Drupal\cloud\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManager;
use Drupal\cloud\Service\EntityLinkRendererInterface;
use Drupal\system\ActionConfigEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a list controller for CloudLaunchTemplate entity.
 *
 * @ingroup cloud_launch_template
 */
class CloudLaunchTemplateListBuilder extends CloudContentListBuilder implements FormInterface {

  /**
   * Cloud launch template plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManager
   */
  private $cloudLaunchTemplatePluginManager;

  /**
   * The key to use for the form element containing the entities.
   *
   * @var string
   */
  protected $entitiesKey = 'entities';

  /**
   * The entities being listed.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $entities = [];

  /**
   * The bulk operations.
   *
   * @var \Drupal\system\Entity\Action[]
   */
  protected $actions;

  /**
   * The action storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $actionStorage;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The name of workflow status.
   *
   * @var string
   */
  protected $workflowStatusFilter;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Entity link renderer object.
   *
   * @var \Drupal\cloud\Service\EntityLinkRendererInterface
   */
  protected $entityLinkRenderer;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_route_match'),
      $container->get('current_user'),
      $container->get('plugin.manager.cloud_launch_template_plugin'),
      $container->get('entity_type.manager')->getStorage('action'),
      $container->get('form_builder'),
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('entity.link_renderer')
    );
  }

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The currently active route match object.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\cloud\Plugin\cloud\launch_template\CloudLaunchTemplatePluginManager $cloud_server_template_plugin_manager
   *   Cloud launch template plugin manager.
   * @param \Drupal\Core\Entity\EntityStorageInterface $action_storage
   *   The action storage.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   * @param \Drupal\cloud\Service\EntityLinkRendererInterface $entity_link_renderer
   *   The entity link render service.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    RouteMatchInterface $route_match,
    AccountProxyInterface $current_user,
    CloudLaunchTemplatePluginManager $cloud_server_template_plugin_manager,
    EntityStorageInterface $action_storage,
    FormBuilderInterface $form_builder,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    EntityLinkRendererInterface $entity_link_renderer,
  ) {

    parent::__construct($entity_type, $storage, $route_match, $current_user);

    $this->entityTypeId = $entity_type->id();
    $this->entityType = $entity_type;
    $this->cloudLaunchTemplatePluginManager = $cloud_server_template_plugin_manager;
    $this->actionStorage = $action_storage;
    $this->formBuilder = $form_builder;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $this->entityLinkRenderer = $entity_link_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    // Filter the actions to only include those for this entity type.
    $entity_type_id = $this->entityTypeId;
    $this->actions = array_filter($this->actionStorage->loadMultiple(), static function (ActionConfigEntityInterface $action) use ($entity_type_id) {
      return $action->getType() === $entity_type_id;
    });
    $this->entities = $this->load();
    if ($this->entities) {
      return $this->formBuilder->getForm($this);
    }
    return parent::render();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return $this->entityTypeId . '_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form[$this->entitiesKey] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
      '#attached' => [
        'library' => ['core/drupal.tableselect'],
      ],
    ];

    $cloud_server_template_type = $this->routeMatch->getParameter('cloud_server_template_type');
    // Ensure a consistent container for filters/operations in view header.
    $form['header'] = [
      '#type' => 'container',
      '#weight' => -100,
    ];
    if (empty($cloud_server_template_type)) {
      $form[$this->entitiesKey]['#tableselect'] = TRUE;
      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Apply to selected items'),
        '#button_type' => 'primary',
      ];

      $action_options = [];
      foreach ($this->actions ?: [] as $id => $action) {
        $action_options[$id] = $action->label();
      }
      $form['header']['action'] = [
        '#type' => 'select',
        '#title' => $this->t('Action'),
        '#options' => $action_options,
      ];
      // Duplicate the form actions into the action container in the header.
      $form['header']['actions']['actions'] = $form['actions'];
    }

    $is_workflow_status = FALSE;
    foreach ($this->entities ?: [] as $entity) {
      if ($entity->hasField('field_workflow_status')
      && !empty($entity->field_workflow_status)) {
        $is_workflow_status = TRUE;
        break;
      }
    }

    $this->workflowStatusFilter = $this->requestStack->getCurrentRequest()->query->get('workflow_status');
    $this->entities = $this->load($is_workflow_status);
    foreach ($this->entities ?: [] as $entity) {
      $form[$this->entitiesKey][$entity->id()] = $this->buildRow($entity);
    }
    if ($is_workflow_status) {
      $options = [
        CloudLaunchTemplateInterface::DRAFT => CloudLaunchTemplateInterface::DRAFT,
        CloudLaunchTemplateInterface::REVIEW => CloudLaunchTemplateInterface::REVIEW,
        CloudLaunchTemplateInterface::APPROVED => CloudLaunchTemplateInterface::APPROVED,
      ];
      $form['header']['filters']['workflow_status'] = [
        '#type' => 'select',
        '#title' => $this->t('Workflow status'),
        '#options' => $options,
        '#default_value' => $this->workflowStatusFilter,
        '#empty_option' => $this->t('- Any -'),
      ];
      $form['header']['filters']['actions']['#type'] = 'actions';
      $form['header']['filters']['actions']['filter'] = [
        '#type' => 'submit',
        '#value' => $this->t('Filter'),
        '#button_type' => 'primary',
        '#name' => 'filter',
      ];
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $form['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [
      // The header gives the table the information it needs in order to make
      // the query calls for ordering. TableSort uses the field information
      // to know what database column to sort by.
      ['data' => $this->t('Name'), 'specifier' => 'name', 'field' => 'name'],
    ];

    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $cloud_server_template_type = $this->routeMatch->getParameter('cloud_server_template_type');

    if (!empty($cloud_context)) {
      // Call the plugin to build the header rows.
      $header = array_merge(
        $header,
        $this->cloudLaunchTemplatePluginManager->buildListHeader(
          $cloud_context
        ));
    }

    if ($cloud_server_template_type) {
      $header = [
        // The header gives the table the information it needs in order to make
        // the query calls for ordering. TableSort uses the field information
        // to know what database column to sort by.
        [
          'data' => $this->t('Cloud service provider ID'),
          'specifier' => 'cloud_context',
          'field' => 'cloud_context',
          'sort' => 'asc',
        ],
        ['data' => $this->t('Name'), 'specifier' => 'name', 'field' => 'name'],
      ];

      $cloud_configs = $this->entityTypeManager
        ->getStorage('cloud_config')
        ->loadByProperties([
          'type' => [$cloud_server_template_type],
        ]);

      $cloud_context = [];

      foreach ($cloud_configs ?: [] as $cloud_config) {
        $cloud_context[] = $cloud_config->getCloudContext();
      }
      // Call the plugin to build the header rows.
      $header = array_merge(
        $header,
        $this->cloudLaunchTemplatePluginManager->buildListHeader($cloud_context)
      );
    }

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @param bool $is_workflow_status
   *   Workflow status exists in Entity.
   */
  public function load($is_workflow_status = FALSE): array {
    $header = $this->buildHeader();
    $query = $this->getStorage()
      ->getQuery()
      ->accessCheck(TRUE);

    // Get cloud_context from a path.
    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $cloud_server_template_type = $this->routeMatch->getParameter('cloud_server_template_type');

    if (!empty($cloud_context)) {
      $query->tableSort($header)
        ->condition('cloud_context', $cloud_context);
    }
    else {
      $query->tableSort($header)
        ->condition('type', $cloud_server_template_type);
    }

    // Only return templates the current user owns.
    if (!$this->currentUser->hasPermission('view any published cloud server templates')) {
      if ($this->currentUser->hasPermission('view own published cloud server templates')) {
        $query->condition('uid', $this->currentUser->id());
      }
      else {
        // Do not return any results if the user does not have any of
        // the above conditions.
        return [];
      }
    }

    if ($is_workflow_status && $this->workflowStatusFilter) {
      $query->condition('field_workflow_status', $this->workflowStatusFilter);
    }

    $keys = $query->execute();
    return $this->storage->loadMultiple($keys);
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row = [];
    // Build our own select box, so it fits in the theme css.
    $cloud_server_template_type = $this->routeMatch->getParameter('cloud_server_template_type');
    if (empty($cloud_server_template_type)) {
      $row['select'] = [
        '#id' => 'edit-entities-' . $entity->id(),
        '#type' => 'checkbox',
        '#title' => $this->t('Update this item'),
        '#title_display' => 'invisible',
        '#return_value' => $entity->id(),
        '#wrapper_attributes' => [
          'class' => ['table-select'],
        ],
        '#attributes' => [
          'data-drupal-selector' => 'edit-entities',
        ],
        '#parents' => [
          'entities',
          $entity->id(),
        ],
      ];
    }

    if ($cloud_server_template_type) {
      $cloud_config_entity = $this->entityTypeManager
        ->getStorage('cloud_config')
        ->loadByProperties([
          'type' => [$cloud_server_template_type],
          'cloud_context' => [$entity->getCloudContext()],
        ]);
      $cloud_config = array_shift($cloud_config_entity);

      if ($cloud_config) {
        $row['cloud_service_provider_id']['data'] = Link::createFromRoute(
          $cloud_config->getName(),
          'view.aws_cloud_instance.list',
          [
            'cloud_context' => $entity->getCloudContext(),
          ]
        )->toRenderable();
      }
    }

    $row['name']['data'] = Link::createFromRoute(
      $entity->label(),
      'entity.cloud_launch_template.canonical',
      [
        'cloud_launch_template' => $entity->id(),
        'cloud_context' => $entity->getCloudContext(),
      ]
    )->toRenderable();

    // Call the plugin to build each row.
    $row += $this->cloudLaunchTemplatePluginManager->buildListRow($entity);
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    $operations = parent::getDefaultOperations($entity);
    if ($entity->hasLinkTemplate('launch')) {
      if ($this->currentUser->hasPermission('launch cloud server template')
        || $this->currentUser->hasPermission('launch approved cloud server template')
          && $entity->hasField('field_workflow_status')
          && !empty($entity->field_workflow_status)
          && $entity->get('field_workflow_status')->value === CloudLaunchTemplateInterface::APPROVED) {
        $operations['launch'] = [
          'title' => $this->t('Launch'),
          'url' => $entity->toUrl('launch'),
          'weight' => 100,
        ];
      }
    }
    if ($entity->hasLinkTemplate('copy')) {
      if ($entity->access('update')) {
        $operations['copy'] = [
          'title' => $this->t('Copy'),
          'url' => $entity->toUrl('copy'),
          'weight' => 100,
        ];
      }
    }
    if ($entity->hasLinkTemplate('approve')) {
      foreach (CloudLaunchTemplateInterface::PERMISSIONS_TO_APPROVE ?: [] as $bundle => $permission) {
        if ($entity->bundle() === $bundle
        && $this->currentUser->hasPermission($permission)
        && ($entity->hasField('field_workflow_status')
          && !empty($entity->field_workflow_status)
          && $entity->get('field_workflow_status')->value === CloudLaunchTemplateInterface::REVIEW)) {
          $operations['approve'] = [
            'title' => $this->t('Approve'),
            'url' => $entity->toUrl('approve'),
            'weight' => 100,
          ];
          break;
        }
      }
    }
    if ($entity->hasLinkTemplate('review')) {
      if ($entity->access('update')
        && ($entity->hasField('field_workflow_status')
          && !empty($entity->field_workflow_status)
          && $entity->get('field_workflow_status')->value === CloudLaunchTemplateInterface::DRAFT)) {
        $operations['review'] = [
          'title' => $this->t('Review'),
          'url' => $entity->toUrl('review'),
          'weight' => 100,
        ];
      }
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (!empty($form_state->getTriggeringElement())
    && $form_state->getTriggeringElement()['#name'] === 'filter') {
      return;
    }
    $selected = array_filter($form_state->getValue($this->entitiesKey));
    if (empty($selected)) {
      $form_state->setErrorByName($this->entitiesKey, $this->t('No items selected.'));
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $cloud_context = $this->routeMatch->getParameter('cloud_context');
    $button_clicked = empty($form_state->getTriggeringElement()) ? '' : $form_state->getTriggeringElement()['#name'];

    if ($button_clicked === 'filter') {
      $form_state->setRedirect($this->routeMatch->getRouteName(), [
        'cloud_context' => $cloud_context,
        'cloud_server_template_type' => $this->routeMatch->getParameter('cloud_server_template_type'),
        'workflow_status' => $form['header']['filters']['workflow_status']['#value'],
      ]);
      return;
    }

    $selected = array_filter($form_state->getValue($this->entitiesKey));
    $entities = [];
    $action = $this->actions[$form_state->getValue('action')];
    $count = 0;

    foreach ($selected ?: [] as $id) {
      $entity = $this->entities[$id];
      // Skip execution if the user did not have access.
      if (!$action->getPlugin()->access($entity)) {
        // NOTE: $this->messenger() is correct.
        // cf. MessengerTrait::messenger() MessengerInterface.
        $this->messenger()->addError($this->t('No access to execute %action on the @type %label.', [
          '%action' => $action->label(),
          '@type' => $entity->getEntityType()->getLabel(),
          '%label' => $entity->toLink($this->t('View'))->toString(),
        ]));
        continue;
      }

      $count++;
      $entities[$id] = $entity;
    }

    // Do not perform any action unless there are some elements affected.
    // @see https://www.drupal.org/project/drupal/issues/3018148
    if (!$count) {
      return;
    }

    $action->execute($entities);

    $operation_definition = $action->getPluginDefinition();
    if (!empty($operation_definition['confirm_form_route_name'])) {
      $options = [
        'query' => $this->getDestinationArray(),
      ];
      $form_state->setRedirect($operation_definition['confirm_form_route_name'], ['cloud_context' => $cloud_context], $options);
    }
    else {
      // NOTE: $this->messenger() is correct.
      // cf. MessengerTrait::messenger() MessengerInterface.
      $this->messenger()->addStatus($this->formatPlural($count, '%action was applied to @count item.', '%action was applied to @count items.', [
        '%action' => $action->label(),
      ]));
    }
  }

}
