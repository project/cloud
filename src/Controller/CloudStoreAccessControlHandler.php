<?php

namespace Drupal\cloud\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the cloud store entity.
 *
 * @see \Drupal\cloud\Entity\CloudStore.
 */
class CloudStoreAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    // First check for cloud_context access.
    if (!$account->hasPermission('view all cloud service providers')
    ) {
      return AccessResult::neutral();
    }

    // Determine if the user is the entity owner ID.
    $is_entity_owner = !empty($entity->getOwner()) && $account->id() === $entity->getOwner()->id();

    /** @var \Drupal\cloud\Entity\CloudStoreInterface $entity */
    switch ($operation) {
      case 'view':
        if ($account->hasPermission('view any published cloud stores')) {
          return AccessResult::allowed();
        }
        return AccessResult::allowedIf($account->hasPermission('view own published cloud stores') && $is_entity_owner);

      case 'update':
        if ($account->hasPermission('edit any cloud stores')) {
          return AccessResult::allowed();
        }
        return AccessResult::allowedIf($account->hasPermission('edit own cloud stores') && $is_entity_owner);

      case 'delete':
        if ($account->hasPermission('delete any cloud stores')) {
          return AccessResult::allowed();
        }
        return AccessResult::allowedIf($account->hasPermission('delete own cloud stores') && $is_entity_owner);
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
