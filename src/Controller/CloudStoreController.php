<?php

namespace Drupal\cloud\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\cloud\Entity\CloudStoreInterface;
use Drupal\cloud\Plugin\cloud\store\CloudStorePluginManagerInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Route;

/**
 * Class CloudStoreController.
 *
 *  Returns responses for cloud store routes.
 */
class CloudStoreController extends ControllerBase implements ContainerInjectionInterface, CloudStoreControllerInterface {

  use CloudContentEntityTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cloud store (CloudStore) storage.
   *
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  protected $cloudStoreStorage;

  /**
   * The CloudStorePluginManager.
   *
   * @var \Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager
   */
  protected $cloudStorePluginManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs an OperationsController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\RevisionableStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\cloud\Plugin\cloud\store\CloudStorePluginManagerInterface $store_plugin_manager
   *   The cloud store plugin manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler interface.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RevisionableStorageInterface $entity_storage,
    RouteMatchInterface $route_match,
    CloudStorePluginManagerInterface $store_plugin_manager,
    RendererInterface $renderer,
    DateFormatterInterface $date_formatter,
    ModuleHandlerInterface $module_handler,
  ) {

    $this->entityTypeManager = $entity_type_manager;
    $this->cloudStoreStorage = $entity_storage;
    $this->routeMatch = $route_match;
    $this->cloudStorePluginManager = $store_plugin_manager;
    $this->renderer = $renderer;
    $this->dateFormatter = $date_formatter;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager,
      $entity_type_manager->getStorage('cloud_store'),
      $container->get('current_route_match'),
      $container->get('plugin.manager.cloud_store_plugin'),
      $container->get('renderer'),
      $container->get('date.formatter'),
      $container->get('module_handler')
    );
  }

  /**
   * Displays a cloud store  revision.
   *
   * @param int $cloud_store_revision
   *   The cloud store revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($cloud_store_revision): array {

    /** @var \Drupal\cloud\Entity\CloudStoreInterface $cloud_store */
    $cloud_store = $this->cloudStoreStorage->loadRevision($cloud_store_revision);

    $view_builder = $this->entityTypeManager()->getViewBuilder('cloud_store');

    try {
      return $view_builder->view($cloud_store);
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }

    return [];
  }

  /**
   * Page title callback for a cloud store  revision.
   *
   * @param int $cloud_store_revision
   *   The cloud store  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($cloud_store_revision): string {

    /** @var \Drupal\cloud\Entity\CloudStoreInterface $cloud_store */
    $cloud_store = $this->cloudStoreStorage->loadRevision($cloud_store_revision);

    return $this->t('Revision of %title from %date', [
      '%title' => !empty($cloud_store) ? $cloud_store->label() : 'N/A',
      '%date' => !empty($cloud_store) ? $this->dateFormatter->format($cloud_store->getRevisionCreationTime()) : 'N/A',
    ]);
  }

  /**
   * Generates an overview table of older revisions of a cloud store .
   *
   * @param \Drupal\cloud\Entity\CloudStoreInterface $cloud_store
   *   The cloud store entity.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(CloudStoreInterface $cloud_store): array {
    $account = $this->currentUser();
    $langcode = $cloud_store->language()->getId();
    $langname = $cloud_store->language()->getName();
    $languages = $cloud_store->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $cloud_store_type = $this->routeMatch->getParameter('cloud_store_type');

    $build['#title'] = $has_translations
      ? $this->t('@langname revisions for %title', [
        '@langname' => $langname,
        '%title' => $cloud_store->label(),
      ])
      : $this->t('Revisions for %title', ['%title' => $cloud_store->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission('revert all cloud store revisions') || $account->hasPermission('administer cloud stores')));
    $delete_permission = (($account->hasPermission('delete all cloud store revisions') || $account->hasPermission('administer cloud stores')));
    $rows = [];

    $vids = $this->cloudStoreStorage->revisionIds($cloud_store);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {

      /** @var \Drupal\cloud\Entity\CloudStoreInterface $revision */
      $revision = $this->cloudStoreStorage->loadRevision($vid);

      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];
        // Use revision link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid !== $cloud_store->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.cloud_store.revision', [
            'cloud_store' => $cloud_store->id(),
            'cloud_store_type' => $cloud_store_type,
            'cloud_store_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $cloud_store->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderInIsolation($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations
                ? Url::fromRoute('entity.cloud_store.translation_revert', [
                  'cloud_store' => $cloud_store->id(),
                  'cloud_store_type' => $cloud_store_type,
                  'cloud_store_revision' => $vid,
                  'langcode' => $langcode,
                ])
                : Url::fromRoute('entity.cloud_store.revision_revert', [
                  'cloud_store' => $cloud_store->id(),
                  'cloud_store_type' => $cloud_store_type,
                  'cloud_store_revision' => $vid,
                ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.cloud_store.revision_delete', [
                'cloud_store' => $cloud_store->id(),
                'cloud_store_type' => $cloud_store_type,
                'cloud_store_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['cloud_store_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#sticky' => TRUE,
    ];

    return $build;
  }

  /**
   * Checks user access for a specific request based on the cloud context.
   *
   * Supports 'AND' and 'OR' access checks, similar to _permission definition in
   * *.routing.yml.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route): AccessResultInterface {
    $perm = $route->getOption('perm');
    if (strpos($perm, '+') === FALSE && strpos($perm, ',') === FALSE) {
      if ($account->hasPermission('view all cloud service providers')) {
        return AccessResult::allowedIfHasPermissions($account, [
          $perm,
        ]);
      }
    }
    else {
      if (!$account->hasPermission('view all cloud service providers')) {
        return AccessResult::neutral();
      }

      // Allow to conjunct the permissions with OR ('+') or AND (',').
      $split = explode(',', $perm);
      // Support AND permission check.
      if (count($split) > 1) {
        return AccessResult::allowedIfHasPermissions($account, $split, 'AND');
      }
      else {
        $split = explode('+', $perm);
        return AccessResult::allowedIfHasPermissions($account, $split, 'OR');
      }
    }

    // Unknown permission.
    return AccessResult::neutral();
  }

  /**
   * Get the count of Cloud Store Resources.
   *
   * @param string $bundle
   *   The bundle ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getEntityCount(string $bundle): JsonResponse {
    $ids = $this->entityTypeManager()
      ->getStorage('cloud_store')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', $bundle)
      ->execute();

    return new JsonResponse(['count' => count($ids)]);
  }

}
