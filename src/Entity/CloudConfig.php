<?php

namespace Drupal\cloud\Entity;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Defines the cloud service provider (CloudConfig) entity.
 *
 * @ingroup cloud
 *
 * @ContentEntityType(
 *   id = "cloud_config",
 *   id_plural = "cloud_configs",
 *   label = @Translation("Cloud service provider"),
 *   label_collection = @Translation("Cloud service providers"),
 *   label_singular = @Translation("cloud service provider"),
 *   label_plural = @Translation("cloud service providers"),
 *   bundle_label = @Translation("cloud service provide type"),
 *   handlers = {
 *     "storage"      = "Drupal\cloud\Entity\CloudConfigStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudConfigListBuilder",
 *     "views_data"   = "Drupal\cloud\Entity\CloudConfigViewsData",
 *     "translation"  = "Drupal\cloud\Entity\Drupal\cloud\Entity\CloudConfigStorage",
 *
 *     "form" = {
 *       "default"                 = "Drupal\cloud\Form\CloudConfigForm",
 *       "add"                     = "Drupal\cloud\Form\CloudConfigForm",
 *       "edit"                    = "Drupal\cloud\Form\CloudConfigForm",
 *       "delete"                  = "Drupal\cloud\Form\CloudConfigDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\cloud\Form\CloudConfigDeleteMultipleForm",
 *     },
 *     "access" = "Drupal\cloud\Controller\CloudConfigAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\cloud\Routing\CloudConfigHtmlRouteProvider",
 *     },
 *   },
 *   base_table          = "cloud_config",
 *   data_table          = "cloud_config_field_data",
 *   revision_table      = "cloud_config_revision",
 *   revision_data_table = "cloud_config_field_revision",
 *   translatable        = TRUE,
 *   admin_permission    = "administer cloud service providers",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle"   = "type",
 *     "label"    = "name",
 *     "uuid"     = "uuid",
 *     "uid"      = "uid",
 *     "langcode" = "langcode",
 *     "status"   = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user"        = "revision_user",
 *     "revision_created"     = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical"            = "/admin/structure/cloud_config/{cloud_config}",
 *     "add-page"             = "/admin/structure/cloud_config/add",
 *     "add-form"             = "/admin/structure/cloud_config/add/{cloud_config_type}",
 *     "edit-form"            = "/admin/structure/cloud_config/{cloud_config}/edit",
 *     "delete-form"          = "/admin/structure/cloud_config/{cloud_config}/delete",
 *     "version-history"      = "/admin/structure/cloud_config/{cloud_config}/revisions",
 *     "revision"             = "/admin/structure/cloud_config/{cloud_config}/revisions/{cloud_config_revision}/view",
 *     "revision_revert"      = "/admin/structure/cloud_config/{cloud_config}/revisions/{cloud_config_revision}/revert",
 *     "revision_delete"      = "/admin/structure/cloud_config/{cloud_config}/revisions/{cloud_config_revision}/delete",
 *     "translation_revert"   = "/admin/structure/cloud_config/{cloud_config}/revisions/{cloud_config_revision}/revert/{langcode}",
 *     "collection"           = "/admin/structure/cloud_config",
 *     "delete-multiple-form" = "/admin/structure/cloud_config/delete_multiple",
 *   },
 *   bundle_entity_type  = "cloud_config_type",
 *   field_ui_base_route = "entity.cloud_config_type.edit_form"
 * )
 */
class CloudConfig extends CloudContentEntityBase implements CloudConfigInterface, CloudContextInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values): void {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) ?: [] as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the cloud_config owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name): CloudConfigInterface {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): CloudConfigInterface {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): int {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): CloudConfigInterface {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished(): bool {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published): CloudConfigInterface {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string {
    return $this->get('cloud_context')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context): CloudContextInterface {
    $this->set('cloud_context', $cloud_context);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIconFid(): ?int {
    $fid = $this->get('image')->target_id;
    if (!isset($fid)) {
      $fids = \Drupal::moduleHandler()->invokeAll(
        'default_cloud_config_icon', [
          $this,
        ]
      );

      if (count($fids) === 1 && !empty($fids[0])) {
        $fid = $fids[0];
      }
    }
    return $fid;
  }

  /**
   * {@inheritdoc}
   */
  public function isRemote(): bool {
    if (!$this->hasField('cloud_cluster_name') || !$this->hasField('cloud_cluster_worker_name')) {
      return FALSE;
    }
    return !empty($this->get('cloud_cluster_name')->value) && !empty($this->get('cloud_cluster_worker_name')->value);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(): void {
    parent::delete();
    $this->deleteLaunchTemplate();

    // Note: In case of multiple entity deletion, if $this->updateCache() is
    // called, the first entity deletion clears the entire cache, so the
    // subsequent entities' cache will be gone, which the Drupal Core system
    // still requires to use during the multiple deletion process. Therefore,
    // it causes an error as follows:
    //
    // Uncaught PHP Exception Drupal\\Core\\Entity\\EntityStorageException:
    // "Cannot load cloud service provider (CloudConfig) plugin for
    // <cloud_context>"
    //
    // at core/lib/Drupal/Core/Entity/Sql/SqlContentEntityStorage.php line 7xx.
    //
    // Therefore, $this->updateCache() has been moved to
    //
    // CloudConfigDeleteForm::submitForm and
    // CloudConfigProcessMultipleForm.
  }

  /**
   * {@inheritdoc}
   */
  public function save(): int {
    $return = parent::save();
    \Drupal::service('cloud')->clearAllCacheValues();
    return $return;
  }

  /**
   * Delete cloud launch templates after deleting cloud service provider.
   */
  private function deleteLaunchTemplate(): void {
    $ids = \Drupal::entityQuery('cloud_launch_template')
      ->accessCheck(FALSE)
      ->condition('cloud_context', $this->getCloudContext())
      ->execute();
    if (count($ids)) {
      /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
      $entity_type_manager = \Drupal::entityTypeManager();
      $entities = $entity_type_manager->getStorage('cloud_launch_template')
        ->loadMultiple($ids);
      $entity_type_manager->getStorage('cloud_launch_template')->delete($entities);
    }
  }

  /**
   * Check if a specific cloud context is valid.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $bundle
   *   The cloud provider type.
   *
   * @return bool
   *   If the cloud context is valid or not.
   */
  public static function checkCloudContext($cloud_context, $bundle = NULL): bool {
    if (empty($cloud_context)) {
      return FALSE;
    }

    $cloud_configs = \Drupal::entityTypeManager()
      ->getStorage('cloud_config')
      ->loadByProperties(['cloud_context' => [$cloud_context]]);
    if (empty($cloud_configs)) {
      return FALSE;
    }

    if (empty($bundle)) {
      return TRUE;
    }

    $cloud_config = reset($cloud_configs);
    return $cloud_config->bundle() === $bundle;
  }

  /**
   * Create link to cloud config of cloud id.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The Cloud Config Link Array.
   */
  public function getCloudConfigLink(): MarkupInterface {
    return Link::fromTextAndUrl(
      $this->getName(),
      Url::fromRoute(
        'entity.cloud_config.canonical',
        ['cloud_config' => $this->id()]
      )
    )->toString();
  }

  /**
   * Get resource link.
   *
   * @param string $plural_label
   *   The plural label.
   * @param string $entity_type
   *   The entity type.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The Resource Link Array.
   */
  public function getResourceLink($plural_label, $entity_type): MarkupInterface {
    return Link::fromTextAndUrl(
      $plural_label,
      Url::fromRoute(
        "entity.{$entity_type}.collection",
        ['cloud_context' => $this->getCloudContext()]
      )
    )->toString();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of author of the cloud service provider.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 99,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 99,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the cloud service provider. The maximum number of characters allowed in the name is @max_length.', [
        '@max_length' => CloudConfigInterface::MAX_NAME_LENGTH,
      ]))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => CloudConfigInterface::MAX_NAME_LENGTH,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Icon'))
      ->setDescription(t('Upload an icon representing the cloud service provider.'))
      ->setSettings([
        'file_directory' => 'images/cloud/icons',
        'alt_field_required' => FALSE,
        'file_extensions' => 'png jpg jpeg',
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'cloud_image',
        'settings' => [
          'image_style' => 'icon_32x32',
        ],
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cloud service provider ID'))
      ->setRequired(TRUE)
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setRevisionable(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the cloud service provider (CloudConfig) is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
