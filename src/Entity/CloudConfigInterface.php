<?php

namespace Drupal\cloud\Entity;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining cloud service providers (CloudConfig).
 *
 * @ingroup cloud
 */
interface CloudConfigInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * The maximum number of regions that can be added at one time.
   *
   * @var int
   */
  public const MAX_REGION_COUNT = 5;

  /**
   * The maximum length of CloudConfig::$name.
   *
   * The maximum length of the name (CloudConfig::$name) limited up to
   * 31 characters due to the consideration of a suffix by adding a UUID to
   * deploy multiple Cloud Orchestrator instances.
   *
   * @var int
   */
  public const MAX_NAME_LENGTH = 31;

  /**
   * Gets the cloud service provider (CloudConfig) name.
   *
   * @return string
   *   The name of the cloud service provider (CloudConfig).
   */
  public function getName(): ?string;

  /**
   * Sets the cloud service provider (CloudConfig) name.
   *
   * @param string $name
   *   The cloud service provider (CloudConfig) name.
   *
   * @return \Drupal\cloud\Entity\CloudConfigInterface
   *   The called cloud service provider (CloudConfig) entity.
   */
  public function setName($name): CloudConfigInterface;

  /**
   * Gets the cloud service provider (CloudConfig) creation timestamp.
   *
   * @return int
   *   Creation timestamp of the cloud service provider (CloudConfig).
   */
  public function getCreatedTime();

  /**
   * Sets the cloud service provider (CloudConfig) creation timestamp.
   *
   * @param int $timestamp
   *   The cloud service provider (CloudConfig) creation timestamp.
   *
   * @return \Drupal\cloud\Entity\CloudConfigInterface
   *   The called cloud service provider (CloudConfig) entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the cloud service provider published status indicator.
   *
   * Unpublished cloud service providers are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the cloud service provider (CloudConfig) is published.
   */
  public function isPublished(): bool;

  /**
   * Sets the published status of a cloud service provider (CloudConfig).
   *
   * @param bool $published
   *   TRUE to set this cloud service provider (CloudConfig) to published,
   *   FALSE to set it to unpublished.
   *
   * @return \Drupal\cloud\Entity\CloudConfigInterface
   *   The called cloud service provider (CloudConfig) entity.
   */
  public function setPublished($published): CloudConfigInterface;

  /**
   * Gets the cloud service provider (CloudConfig) revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the cloud service provider (CloudConfig) revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\cloud\Entity\CloudConfigInterface
   *   The called cloud service provider (CloudConfig) entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the cloud service provider (CloudConfig) revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the cloud service provider (CloudConfig) revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\cloud\Entity\CloudConfigInterface
   *   The called cloud service provider (CloudConfig) entity.
   */
  public function setRevisionUserId($uid);

  /**
   * Helper method that returns fid of the image.
   *
   * If no image is uploaded, it will return the default icon.
   *
   * @return int|null
   *   The file ID (int) or NULL if not found.
   */
  public function getIconFid(): ?int;

  /**
   * Create link to cloud config of cloud id.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The Cloud Config Link Array.
   */
  public function getCloudConfigLink(): MarkupInterface;

  /**
   * Get resource link.
   *
   * @param string $plural_label
   *   The plural label.
   * @param string $entity_type
   *   The entity type.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The Resource Link Array.
   */
  public function getResourceLink($plural_label, $entity_type): MarkupInterface;

  /**
   * Check whether the cloud config is remote.
   *
   * @return bool
   *   Whether the cloud config is remote or not.
   */
  public function isRemote(): bool;

  /**
   * Get cloud_context.
   *
   * @return string|null
   *   The cloud context.
   */
  public function getCloudContext(): ?string;

  /**
   * Set cloud_context.
   *
   * @param string $cloud_context
   *   The cloud context.
   *
   * @return CloudContextInterface
   *   The cloud context interface.
   */
  public function setCloudContext(string $cloud_context): CloudContextInterface;

}
