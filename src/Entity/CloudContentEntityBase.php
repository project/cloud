<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Base class for all cloud based entities.
 *
 * The main purpose of this class is to provide the
 * cloud_context as a parameter in urlRouteParameters method.
 */
class CloudContentEntityBase extends RevisionableContentEntityBase implements EntityOwnerInterface, CloudContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values): void {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string {
    return $this->get('cloud_context')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context): CloudContextInterface {
    $this->set('cloud_context', $cloud_context);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    return $this->set('uid', $uid);
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    return $this->set('uid', $account->id());
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerById($id) {
    return $this->set('uid', $id);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function created() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function changed() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = [];

    if (!in_array($rel, ['collection', 'add-page', 'add-form'], TRUE)) {
      // The entity ID is needed as a route parameter.
      $uri_route_parameters[$this->getEntityTypeId()] = $this->id();
    }
    if ($rel === 'add-form'
    && (!empty($this->getEntityType())
    && !empty($this->getEntityType()->hasKey('bundle')))) {
      $parameter_name = !empty($this->getEntityType()->getBundleEntityType()) ?: $this->getEntityType()->getKey('bundle');
      $uri_route_parameters[$parameter_name] = $this->bundle();
    }
    if ($rel === 'revision') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    // Add in cloud context.
    $uri_route_parameters['cloud_context'] = $this->getCloudContext();

    return $uri_route_parameters;
  }

  /**
   * Helper method to update caches.
   *
   * This method has been refactored from a full cache clear to
   * only invalidating the render, block and entity cache tags.
   *
   * This provides faster performance when updating entities.
   *
   * @param array $tags
   *   Additional array of tags to invalidate.
   */
  public static function updateCache(array $tags = []): void {
    \Drupal::service('cloud')->invalidateCacheTags($tags);
  }

}
