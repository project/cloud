<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the cloud launch template entity.
 *
 * @ingroup cloud_launch_template
 *
 * @ContentEntityType(
 *   id = "cloud_launch_template",
 *   id_plural = "cloud_launch_templates",
 *   label = @Translation("Launch Template"),
 *   label_collection = @Translation("Launch Templates"),
 *   label_singular = @Translation("launch template"),
 *   label_plural = @Translation("launch templates"),
 *   bundle_label = @Translation("launch template type"),
 *   handlers = {
 *     "storage"      = "Drupal\cloud\Entity\CloudLaunchTemplateStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudLaunchTemplateListBuilder",
 *     "views_data"   = "Drupal\cloud\Entity\CloudLaunchTemplateViewsData",
 *     "translation"  = "Drupal\cloud\Entity\CloudLaunchTemplateTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\cloud\Form\CloudLaunchTemplateForm",
 *       "add"     = "Drupal\cloud\Form\CloudLaunchTemplateForm",
 *       "edit"    = "Drupal\cloud\Form\CloudLaunchTemplateForm",
 *       "delete"  = "Drupal\cloud\Form\CloudLaunchTemplateDeleteForm",
 *       "launch"  = "Drupal\cloud\Form\CloudLaunchTemplateLaunchConfirm",
 *       "copy"    = "Drupal\cloud\Form\CloudLaunchTemplateForm",
 *       "delete-multiple-confirm" = "Drupal\cloud\Form\CloudLaunchTemplateDeleteMultipleForm",
 *       "approve" = "Drupal\cloud\Form\CloudLaunchTemplateApproveConfirm",
 *       "review" = "Drupal\cloud\Form\CloudLaunchTemplateReviewConfirm",
 *     },
 *     "access" = "Drupal\cloud\Controller\CloudLaunchTemplateAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\cloud\Routing\CloudLaunchTemplateHtmlRouteProvider",
 *     },
 *   },
 *   base_table          = "cloud_launch_template",
 *   data_table          = "cloud_launch_template_field_data",
 *   revision_table      = "cloud_launch_template_revision",
 *   revision_data_table = "cloud_launch_template_field_revision",
 *   translatable        = TRUE,
 *   admin_permission    = "administer cloud server templates",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle"   = "type",
 *     "label"    = "name",
 *     "uuid"     = "uuid",
 *     "uid"      = "uid",
 *     "langcode" = "langcode",
 *     "status"   = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user"        = "revision_user",
 *     "revision_created"     = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical"          = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}",
 *     "add-form"           = "/clouds/design/server_template/{cloud_context}/{cloud_server_template_type}/add",
 *     "edit-form"          = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/edit",
 *     "delete-form"        = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/delete",
 *     "version-history"    = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/revisions",
 *     "revision"           = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/revisions/{cloud_launch_template_revision}/view",
 *     "revision_revert"    = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/revisions/{cloud_launch_template_revision}/revert",
 *     "revision_delete"    = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/revisions/{cloud_launch_template_revision}/delete",
 *     "translation_revert" = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/revisions/{cloud_launch_template_revision}/revert/{langcode}",
 *     "collection"         = "/clouds/design/server_template/{cloud_context}",
 *     "launch"             = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/launch",
 *     "copy"               = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/copy",
 *     "delete-multiple-form" = "/clouds/design/server_template/{cloud_context}/delete_multiple",
 *     "approve"            = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/approve",
 *     "review"             = "/clouds/design/server_template/{cloud_context}/{cloud_launch_template}/review",
 *   },
 *   bundle_entity_type = "cloud_server_template_type",
 *   field_ui_base_route = "entity.cloud_server_template_type.edit_form"
 * )
 */
class CloudLaunchTemplate extends CloudContentEntityBase implements CloudLaunchTemplateInterface {

  use EntityChangedTrait;

  public const TAG_MIN_COUNT = 'cloud_server_template_min_count';

  public const TAG_MAX_COUNT = 'cloud_server_template_max_count';

  public const TAG_TEST_ONLY = 'cloud_server_template_test_only';

  public const TAG_AVAILABILITY_ZONE = 'cloud_server_template_availability_zone';

  public const TAG_VPC = 'cloud_server_template_vpc';

  public const TAG_SUBNET = 'cloud_server_template_subnet';

  public const TAG_SSH_KEY = 'cloud_server_template_ssh_key';

  public const TAG_SCHEDULE = 'cloud_server_template_schedule';

  public const TAG_DESCRIPTION = 'cloud_server_template_description';

  public const TAG_WORKFLOW_STATUS = 'launch_template_workflow_status';

  public const TAG_SERVER_GROUP = 'cloud_server_template_server_group';

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values): void {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    // Add in cloud context.
    $uri_route_parameters['cloud_context'] = $this->getCloudContext();

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) ?: [] as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the
    // cloud_server_template owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCloudContext(): ?string {
    return $this->get('cloud_context')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setCloudContext($cloud_context): CloudContextInterface {
    $this->set('cloud_context', $cloud_context);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of author of the launch template.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 11,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Enter the name of the launch template.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // @todo make this an entity reference to config entity?  For now, leave as string
    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cloud service provider ID'))
      ->setRequired(TRUE)
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the launch template is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(): void {
    parent::delete();
    self::updateCache($this->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function save(): int {
    $return = parent::save();
    self::updateCache($this->getCacheTags());
    return $return;
  }

}
