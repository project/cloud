<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining cloud launch template entities.
 *
 * @ingroup cloud_launch_template
 */
interface CloudLaunchTemplateInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface, CloudContextInterface {

  public const DRAFT = 'Draft';
  public const REVIEW = 'Review';
  public const APPROVED = 'Approved';
  public const PERMISSIONS_TO_APPROVE = [
    'aws_cloud'     => 'approve launch aws cloud instance',
    'k8s'           => 'approve launch k8s resources',
    'cloud_cluster' => 'approve launch cloud orchestrator resources',
  ];
  public const OPERATION_ADD = 'add';
  public const OPERATION_EDIT = 'edit';

  /**
   * Gets the cloud launch template name.
   *
   * @return string
   *   Name of the cloud launch template.
   */
  public function getName(): ?string;

  /**
   * Sets the cloud launch template name.
   *
   * @param string $name
   *   The cloud launch template name.
   *
   * @return \Drupal\cloud\Entity\CloudLaunchTemplateInterface
   *   The called cloud launch template entity.
   */
  public function setName($name);

  /**
   * Gets the cloud launch template creation timestamp.
   *
   * @return int
   *   Creation timestamp of the cloud launch template.
   */
  public function getCreatedTime();

  /**
   * Sets the cloud launch template creation timestamp.
   *
   * @param int $timestamp
   *   The cloud launch template creation timestamp.
   *
   * @return \Drupal\cloud\Entity\CloudLaunchTemplateInterface
   *   The called cloud launch template entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the cloud launch template published status indicator.
   *
   * Unpublished cloud launch templates are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the cloud launch template is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a cloud launch template.
   *
   * @param bool $published
   *   TRUE to set this cloud launch template to published, FALSE to set it to
   *   unpublished.
   *
   * @return \Drupal\cloud\Entity\CloudLaunchTemplateInterface
   *   The called cloud launch template entity.
   */
  public function setPublished($published);

  /**
   * Gets the cloud launch template revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the cloud launch template revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\cloud\Entity\CloudLaunchTemplateInterface
   *   The called cloud launch template entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the cloud launch template revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the cloud launch template revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\cloud\Entity\CloudLaunchTemplateInterface
   *   The called cloud launch template entity.
   */
  public function setRevisionUserId($uid);

}
