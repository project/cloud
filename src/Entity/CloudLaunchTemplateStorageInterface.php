<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for cloud launch template entities.
 *
 * This extends the base storage class, adding required special handling for
 * The cloud launch template entities.
 *
 * @ingroup cloud_launch_template
 */
interface CloudLaunchTemplateStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of cloud launch template revision IDs.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The cloud launch template entity.
   *
   * @return int[]
   *   Cloud launch template revision IDs (in ascending order).
   */
  public function revisionIds(CloudLaunchTemplateInterface $entity): array;

  /**
   * Gets a list of revision IDs given a cloud launch template author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Cloud launch template revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account): array;

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The cloud launch template entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(CloudLaunchTemplateInterface $entity): int;

  /**
   * Unsets the language for all cloud launch template with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
