<?php

namespace Drupal\cloud\Entity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for cloud_launch_template.
 */
class CloudLaunchTemplateTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
