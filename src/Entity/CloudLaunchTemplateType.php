<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the cloud launch template type entity.
 *
 * @ConfigEntityType(
 *   id = "cloud_server_template_type",
 *   id_plural = "cloud_server_template_types",
 *   label = @Translation("Launch Template Type"),
 *   label_collection = @Translation("Launch Template Types"),
 *   label_singular = @Translation("launch template type"),
 *   label_plural = @Translation("launch template types"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cloud\Entity\CloudLaunchTemplateTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cloud\Form\CloudLaunchTemplateTypeForm",
 *       "edit" = "Drupal\cloud\Form\CloudLaunchTemplateTypeForm",
 *       "delete" = "Drupal\cloud\Form\CloudEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\cloud\Routing\CloudLaunchTemplateTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "cloud_server_template_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "cloud_launch_template",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/cloud_server_template_type/{cloud_server_template_type}",
 *     "add-form" = "/admin/structure/cloud_server_template_type/add",
 *     "edit-form" = "/admin/structure/cloud_server_template_type/{cloud_server_template_type}/edit",
 *     "delete-form" = "/admin/structure/cloud_server_template_type/{cloud_server_template_type}/delete",
 *     "collection" = "/admin/structure/cloud_server_template_type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class CloudLaunchTemplateType extends ConfigEntityBundleBase implements CloudLaunchTemplateTypeInterface {

  /**
   * The cloud launch template type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The cloud launch template type label.
   *
   * @var string
   */
  protected $label;

}
