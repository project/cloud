<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining cloud launch template type entities.
 */
interface CloudLaunchTemplateTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
