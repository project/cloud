<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for Cloud.
 */
class CloudPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Constructs new CloudPermissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module handler service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleExtensionList $module_extension_list) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleExtensionList = $module_extension_list;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Returns an array of cloud service provider (CloudConfig) permissions.
   */
  public function configPermissions(): array {
    $permissions = [];
    $cloud_configs = $this->entityTypeManager->getStorage('cloud_config')->loadMultiple();
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $label = $cloud_config->label();
      $permissions['view ' . $cloud_config->getCloudContext()] = [
        'title' => $this->t('Access %label', ['%label' => $label]),
        'description' => $this->t('Allows access to entities in %label. Entity permissions such as e.g. <em>List %module_name &lt;entity name&gt;</em> and <em>View own %module_name &lt;entity name&gt;</em> (or <em>View any %module_name &lt;entity name&gt;</em>) need to be granted in conjunction. Otherwise, no entities will be shown to the user.', [
          '%module_name' => $this->moduleExtensionList->getName($cloud_config->bundle()),
          '%label' => $label,
        ]),
      ];
    }
    return $permissions;
  }

}
