<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Defines the cloud store entity.
 *
 * @ingroup cloud_store
 *
 * @ContentEntityType(
 *   id = "cloud_store",
 *   id_plural = "cloud_stores",
 *   label = @Translation("Cloud store"),
 *   label_collection = @Translation("Cloud stores"),
 *   label_singular = @Translation("cloud store"),
 *   label_plural = @Translation("cloud stores"),
 *   bundle_label = @Translation("cloud store type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cloud\Controller\CloudStoreListBuilder",
 *     "views_data"   = "Drupal\cloud\Entity\CloudStoreViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\cloud\Form\CloudStoreForm",
 *       "add"     = "Drupal\cloud\Form\CloudStoreForm",
 *       "edit"    = "Drupal\cloud\Form\CloudStoreForm",
 *       "delete"  = "Drupal\cloud\Form\CloudStoreDeleteForm",
 *       "copy"    = "Drupal\cloud\Form\CloudStoreForm",
 *       "delete-multiple-confirm" = "Drupal\cloud\Form\CloudStoreDeleteMultipleForm",
 *     },
 *     "access" = "Drupal\cloud\Controller\CloudStoreAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\cloud\Routing\CloudStoreHtmlRouteProvider",
 *     },
 *   },
 *   base_table          = "cloud_store",
 *   admin_permission    = "administer cloud stores",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle"   = "type",
 *     "label"    = "name",
 *     "uuid"     = "uuid",
 *     "uid"      = "uid",
 *     "status"   = "status",
 *   },
 *   links = {
 *     "canonical"          = "/clouds/design/store/{cloud_store_type}/{cloud_store}",
 *     "add-form"           = "/clouds/design/store/{cloud_store_type}/add",
 *     "edit-form"          = "/clouds/design/store/{cloud_store_type}/{cloud_store}/edit",
 *     "delete-form"        = "/clouds/design/store/{cloud_store_type}/{cloud_store}/delete",
 *     "collection"         = "/clouds/design/store/{cloud_store_type}",
 *     "copy"               = "/clouds/design/store/{cloud_store_type}/{cloud_store}/copy",
 *     "delete-multiple-form" = "/clouds/design/store/{cloud_store_type}/delete_multiple",
 *   },
 *   bundle_entity_type = "cloud_store_type",
 *   field_ui_base_route = "entity.cloud_store_type.edit_form"
 * )
 */
class CloudStore extends CloudContentEntityBase implements CloudStoreInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = [];

    if ($rel === 'add-form'
      && (!empty($this->getEntityType())
        && !empty($this->getEntityType()->hasKey('bundle')))) {
      $parameter_name = !empty($this->getEntityType()->getBundleEntityType()) ?: $this->getEntityType()->getKey('bundle');
      $uri_route_parameters[$parameter_name] = $this->bundle();
    }
    else {
      // The entity ID is needed as a route parameter.
      $uri_route_parameters = [
        $this->getEntityTypeId() => $this->id(),
        $this->getEntityType()->getBundleEntityType() => $this->bundle(),
      ];
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name): CloudStoreInterface {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): string {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): CloudStoreInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): User {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): string {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): CloudStoreInterface {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): CloudStoreInterface {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished(): bool {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published): CloudStoreInterface {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    // Bypassing parent::baseFieldDefinition to avoid the revision tables.
    $fields = ContentEntityBase::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of author of the cloud store.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 11,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Enter the name of the cloud store.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the cloud store is published.'))
      ->setDefaultValue(TRUE)
      ->setReadOnly(TRUE);

    $fields['cloud_context'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cloud service provider ID'))
      ->setDescription(t('A unique ID for the cloud service provider.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ])
      ->setReadOnly(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(): void {
    parent::delete();
    self::updateCache($this->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function save(): int {
    $return = parent::save();
    self::updateCache($this->getCacheTags());
    return $return;
  }

}
