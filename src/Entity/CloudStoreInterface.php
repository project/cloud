<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining cloud store entities.
 *
 * @ingroup cloud_store
 */
interface CloudStoreInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the cloud store name.
   *
   * @return string
   *   Name of the cloud store.
   */
  public function getName();

  /**
   * Sets the cloud store name.
   *
   * @param string $name
   *   The cloud store name.
   *
   * @return \Drupal\cloud\Entity\CloudStoreInterface
   *   The called cloud store entity.
   */
  public function setName($name);

  /**
   * Gets the cloud store creation timestamp.
   *
   * @return int
   *   Creation timestamp of the cloud store.
   */
  public function getCreatedTime();

  /**
   * Sets the cloud store creation timestamp.
   *
   * @param int $timestamp
   *   The cloud store creation timestamp.
   *
   * @return \Drupal\cloud\Entity\CloudStoreInterface
   *   The called cloud store entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the cloud store published status indicator.
   *
   * Unpublished cloud stores are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the cloud store is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a cloud store.
   *
   * @param bool $published
   *   TRUE to set this cloud store to published, FALSE to set it to
   *   unpublished.
   *
   * @return \Drupal\cloud\Entity\CloudStoreInterface
   *   The called cloud store entity.
   */
  public function setPublished($published);

}
