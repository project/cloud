<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for cloud Storage entities.
 *
 * This extends the base storage class, adding required special handling for
 * The cloud store entities.
 *
 * @ingroup cloud_store
 */
interface CloudStoreStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of cloud store revision IDs.
   *
   * @param \Drupal\cloud\Entity\CloudStoreInterface $entity
   *   The cloud store entity.
   *
   * @return int[]
   *   Cloud store revision IDs (in ascending order).
   */
  public function revisionIds(CloudStoreInterface $entity): array;

  /**
   * Gets a list of revision IDs given a cloud store author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Cloud store revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account): array;

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\cloud\Entity\CloudStoreInterface $entity
   *   The cloud store entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(CloudStoreInterface $entity): int;

  /**
   * Unsets the language for all cloud store with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
