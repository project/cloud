<?php

namespace Drupal\cloud\Entity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for cloud_store.
 */
class CloudStoreTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
