<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the cloud store type entity.
 *
 * @ConfigEntityType(
 *   id = "cloud_store_type",
 *   id_plural = "cloud_store_types",
 *   label = @Translation("Cloud store type"),
 *   label_collection = @Translation("Cloud store types"),
 *   label_singular = @Translation("cloud store type"),
 *   label_plural = @Translation("cloud store types"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cloud\Entity\CloudStoreTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cloud\Form\CloudStoreTypeForm",
 *       "edit" = "Drupal\cloud\Form\CloudStoreTypeForm",
 *       "delete" = "Drupal\cloud\Form\CloudEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\cloud\Routing\CloudStoreTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "cloud_store_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "cloud_store",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/cloud_store_type/{cloud_store_type}",
 *     "add-form" = "/admin/structure/cloud_store_type/add",
 *     "edit-form" = "/admin/structure/cloud_store_type/{cloud_store_type}/edit",
 *     "delete-form" = "/admin/structure/cloud_store_type/{cloud_store_type}/delete",
 *     "collection" = "/admin/structure/cloud_store_type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class CloudStoreType extends ConfigEntityBundleBase implements CloudStoreTypeInterface {

  /**
   * The cloud store type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The cloud store type label.
   *
   * @var string
   */
  protected $label;

}
