<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining cloud store type entities.
 */
interface CloudStoreTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
