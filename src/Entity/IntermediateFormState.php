<?php

namespace Drupal\cloud\Entity;

use Drupal\Core\Form\FormState;

/**
 * Stores information about the dummy state of a form.
 */
class IntermediateFormState extends FormState {

  /**
   * {@inheritdoc}
   */
  public function setRedirect($route_name, array $route_parameters = [], array $options = []) {
    return $this;
  }

}
