<?php

namespace Drupal\cloud\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Cloud entity event class.
 */
class CloudEntityEvent extends Event {

  /**
   * The Entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  private $entity;

  /**
   * The event type.
   *
   * @var string
   */
  private $eventType;

  /**
   * Construct a new entity event.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity which caused the event.
   * @param string $event_type
   *   The event type.
   */
  public function __construct(EntityInterface $entity, string $event_type) {
    $this->entity = $entity;
    $this->eventType = $event_type;
  }

  /**
   * Method to get the entity from the event.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Method to get the event type.
   *
   * @return string
   *   Event type
   */
  public function getEventType(): string {
    return $this->eventType;
  }

}
