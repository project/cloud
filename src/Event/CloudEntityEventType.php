<?php

namespace Drupal\cloud\Event;

/**
 * Enumeration of cloud entity event types.
 */
class CloudEntityEventType {

  public const FORM_SUBMIT = 'cloud.entity.form.submit';
  public const FORM_SAVE = 'cloud.entity.form.save';
  public const FORM_MULTI_SUBMIT = 'cloud.entity.form_multiple.submit';
  public const TEMPLATE_LAUNCH = 'cloud.entity.server_template.launch';
  public const LAUNCH_TEMPLATE_DELETE = 'cloud.entity.server_template.delete';

}
