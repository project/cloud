<?php

namespace Drupal\cloud\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Messenger\Messenger;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the Cloud entity edit forms.
 *
 * @ingroup cloud
 */
class CloudContentForm extends ContentEntityForm {

  use CloudContentEntityTrait;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Constructs a CloudContentForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->has('datetime.time') ? $container->get('datetime.time') : NULL,
      $container->get('messenger'),
      $container->get('plugin.manager.cloud_config_plugin')
    );
  }

  /**
   * Override actions()
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    $entity = $this->entity;
    foreach ($actions ?: [] as $key => $action) {
      if (isset($actions[$key]['#url'])
      && method_exists($this->entity, 'cloud_context')) {
        $actions[$key]['#url']->setRouteParameter('cloud_context', $entity->getCloudContext());
      }
    }
    return $actions;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->entity;

    if (!($entity instanceof CloudConfigInterface)) {
      $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    }

    if (!$entity->isNew() && !($entity instanceof CloudConfigInterface) && $cloud_config->isRemote()) {
      $status = SAVED_UPDATED;
      $passive_operation = 'updated remotely';
      $this->processOperationStatus($entity, $passive_operation);
      return $status;
    }

    $status = $entity->save();

    // Add an updated status message and the log.
    $status_map = [
      SAVED_NEW => 'created',
      SAVED_DELETED => 'deleted',
      SAVED_UPDATED => 'updated',
    ];

    $passive_operation = $status_map[$status] ?? $status_map[SAVED_UPDATED];

    if (!($entity instanceof CloudConfigInterface) && $cloud_config->isRemote()) {
      $passive_operation .= ' remotely';
    }

    $this->processOperationStatus($entity, $passive_operation);

    // Clear block and menu cache.
    $this->clearCacheValues($entity->getCacheTags());

    return $status;
  }

  /**
   * Add the build array of fieldset others.
   *
   * @param array &$form
   *   The form array.
   * @param int $weight
   *   The weight of the fieldset.  The parameter's default value is 1
   *   to put the "Others" fieldset in between the main items and button(s)
   *   (e.g. "Save") if the parameter is omitted since 0 is the default value
   *   of the #weight attribute.
   * @param string $cloud_context
   *   The cloud context.
   */
  protected function addOthersFieldset(array &$form, $weight = 1, $cloud_context = ''): void {

    $form['others'] = [
      '#type'          => 'details',
      '#title'         => $this->t('Others'),
      '#open'          => FALSE,
      '#weight'       => $weight,
    ];

    $form['others']['cloud_context'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Cloud service provider ID'),
      '#maxlength'     => 255,
      '#size'          => 60,
      '#default_value' => !$this->entity->isNew()
        ? $this->entity->getCloudContext()
        : $cloud_context,
      '#required'      => TRUE,
      '#disabled'      => TRUE,
    ];

    $form['others']['langcode'] = [
      '#title'         => $this->t('Language'),
      '#type'          => 'language_select',
      '#default_value' => $this->entity->getUntranslated()->language()->getId(),
      '#languages'     => Language::STATE_ALL,
      '#attributes'    => ['readonly' => 'readonly'],
      '#disabled'      => FALSE,
    ];

    $form['others']['uid'] = $form['uid'];
  }

}
