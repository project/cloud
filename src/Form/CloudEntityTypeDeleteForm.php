<?php

namespace Drupal\cloud\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * Delete form for <CloudResource>Type entities.
 *
 * This is a generic delete form that can be overridden if needed.  This
 * class is currently used by CloudConfigType, CloudLaunchTemplateType,
 * CloudProjectType and CloudStoreType.
 */
class CloudEntityTypeDeleteForm extends EntityConfirmFormBase {

  use CloudContentEntityTrait;

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): TrustedCallbackInterface {
    return new Url("entity.{$this->entity->getEntityTypeId()}.collection");
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->entity->delete();
    $this->processOperationStatus($this->entity, 'deleted');
    $form_state->setRedirectUrl($this->getCancelUrl());
    // Clear block and menu cache.
    $this->clearCacheValues($this->entity->getCacheTags());
  }

}
