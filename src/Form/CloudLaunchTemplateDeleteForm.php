<?php

namespace Drupal\cloud\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting cloud launch template entities.
 *
 * @ingroup cloud_launch_template
 */
class CloudLaunchTemplateDeleteForm extends ContentEntityDeleteForm {

}
