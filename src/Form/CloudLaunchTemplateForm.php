<?php

namespace Drupal\cloud\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for cloud launch template edit forms.
 *
 * @ingroup cloud_launch_template
 */
class CloudLaunchTemplateForm extends CloudContentForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    /** @var \Drupal\cloud\Entity\CloudLaunchTemplate $entity */
    $form = parent::buildForm($form, $form_state);

    $weight = -50;

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => $weight++,
      ];
    }

    $entity = $this->entity;

    // Set up the cloud_context based on value passed in the path.
    $form['cloud_context']['#disabled'] = TRUE;
    if ($entity->isNew()) {
      $form['cloud_context']['widget'][0]['value']['#default_value'] = $cloud_context;
    }

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') !== FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);
    $this->messenger->deleteAll();

    $status_map = [
      SAVED_NEW => 'created',
      SAVED_UPDATED => 'updated',
    ];

    $this->processOperationStatus($entity, $status_map[$status] ?? $status_map[SAVED_UPDATED]);

    $form_state->setRedirect('entity.cloud_launch_template.canonical', [
      'cloud_launch_template' => $entity->id(),
      'cloud_context' => $entity->getCloudContext(),
    ]);

    // Clear block and menu cache.
    $this->clearCacheValues($entity->getCacheTags());
  }

}
