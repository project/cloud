<?php

namespace Drupal\cloud\Form;

/**
 * Provide a confirmation form of bulk operation for cloud launch templates.
 */
abstract class CloudLaunchTemplateProcessMultipleForm extends CloudProcessMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId(): string {

    return 'cloud_launch_template_process_multiple_confirm_form';
  }

}
