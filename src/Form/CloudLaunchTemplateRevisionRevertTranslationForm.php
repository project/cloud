<?php

namespace Drupal\cloud\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;

/**
 * Provides a form for reverting a cloud launch template revision.
 *
 * @ingroup cloud_launch_template
 */
class CloudLaunchTemplateRevisionRevertTranslationForm extends CloudLaunchTemplateRevisionRevertForm {

  /**
   * The language to be reverted.
   *
   * @var string
   */
  protected $langcode;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cloud_launch_template_revision_revert_translation_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to revert @language translation to the revision from %revision-date?', [
      '@language' => $this->languageManager->getLanguageName($this->langcode),
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_launch_template_revision = NULL, $langcode = NULL): array {
    $this->langcode = $langcode;
    $form = parent::buildForm($form, $form_state, $cloud_launch_template_revision);

    $form['revert_untranslated_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Revert content shared among translations'),
      '#default_value' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareRevertedRevision(CloudLaunchTemplateInterface $revision, FormStateInterface $form_state): CloudLaunchTemplateInterface {
    $revert_untranslated_fields = $form_state->getValue('revert_untranslated_fields');

    /** @var \Drupal\cloud\Entity\CloudLaunchTemplateInterface $default_revision */
    $latest_revision = $this->cloudLaunchTemplateStorage->load($revision->id());
    $latest_revision_translation = $latest_revision->getTranslation($this->langcode);

    $revision_translation = $revision->getTranslation($this->langcode);

    foreach ($latest_revision_translation->getFieldDefinitions() ?: [] as $field_name => $definition) {
      if ($definition->isTranslatable() || $revert_untranslated_fields) {
        $latest_revision_translation->set($field_name, $revision_translation->get($field_name)->getValue());
      }
    }

    $latest_revision_translation->setNewRevision();
    $latest_revision_translation->isDefaultRevision(TRUE);
    $revision->setRevisionCreationTime($this->time->getRequestTime());

    return $latest_revision_translation;
  }

}
