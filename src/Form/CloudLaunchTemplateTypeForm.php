<?php

namespace Drupal\cloud\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides LaunchTemplateType form.
 */
class CloudLaunchTemplateTypeForm extends EntityForm {

  use CloudContentEntityTrait;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Constructs a new CloudLaunchTemplateTypeForm.
   *
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   */
  public function __construct(Messenger $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $cloud_server_template_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $cloud_server_template_type->label(),
      '#description' => $this->t('Label for the launch template type.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $cloud_server_template_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\cloud\Entity\CloudLaunchTemplateType::load',
      ],
      '#disabled' => !$cloud_server_template_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $cloud_server_template_type = $this->entity;
    $status = $cloud_server_template_type->save();

    $status_map = [
      SAVED_NEW => 'created',
      SAVED_UPDATED => 'updated',
    ];

    $this->processOperationStatus($cloud_server_template_type, $status_map[$status] ?? $status_map[SAVED_UPDATED]);

    $form_state->setRedirectUrl($cloud_server_template_type->toUrl('collection'));

    // Clear block and menu cache.
    $this->clearCacheValues($cloud_server_template_type->getCacheTags());
  }

}
