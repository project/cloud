<?php

namespace Drupal\cloud\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Entity\CloudProjectInterface;

/**
 * Form controller for cloud project edit forms.
 *
 * @ingroup cloud_Project
 */
class CloudProjectForm extends CloudContentForm {

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $name = $form_state->getValue('name')[0]['value'] ?? '';

    if (strlen($name) > CloudProjectInterface::MAX_RESOURCE_NAME_LENGTH) {
      $form_state->setError($form, $this->t('The project name %name must be within 63 characters.'));
    }

    // @see https://kubernetes.io/docs/concepts/overview/working-with-objects/names/
    if (!preg_match('/^[a-z0-9][a-z0-9-]+$/', $name)) {
      $form_state->setError($form, $this->t("The @type %name is invalid: a lowercase RFC 1123 label must consist of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character (e.g. 'my-name', or '123-abc').", [
        '@type' => $this->getShortEntityTypeNameWhitespace($this->entity),
        '%name' => $name,
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_context = ''): array {
    /** @var \Drupal\cloud\Entity\CloudProject $entity */
    $form = parent::buildForm($form, $form_state);

    $weight = -50;

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => $weight++,
      ];
    }

    $entity = $this->entity;

    // Set up the cloud_context based on value passed in the path.
    $form['cloud_context']['#disabled'] = TRUE;
    if ($entity->isNew()) {
      $form['cloud_context']['widget'][0]['value']['#default_value'] = $cloud_context;
    }

    $this->addOthersFieldset($form, $weight++, $cloud_context);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') !== FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);
    $this->messenger->deleteAll();

    $status_map = [
      SAVED_NEW => 'created',
      SAVED_UPDATED => 'updated',
    ];

    $this->processOperationStatus($entity, $status_map[$status] ?? $status_map[SAVED_UPDATED]);

    $form_state->setRedirect('entity.cloud_project.canonical', [
      'cloud_project' => $entity->id(),
      'cloud_context' => $entity->getCloudContext(),
    ]);

    // Clear block and menu cache.
    $this->clearCacheValues($entity->getCacheTags());
  }

}
