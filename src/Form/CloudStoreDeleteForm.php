<?php

namespace Drupal\cloud\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting cloud store entities.
 *
 * @ingroup cloud_store
 */
class CloudStoreDeleteForm extends ContentEntityDeleteForm {

}
