<?php

namespace Drupal\cloud\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for cloud store edit forms.
 *
 * @ingroup cloud_Project
 */
class CloudStoreForm extends CloudContentForm {

  /**
   * Entity field manager interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloud;

  /**
   * Constructs a CloudContentForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud
   *   The cloud service.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    Messenger $messenger,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    EntityFieldManagerInterface $entity_field_manager,
    CloudServiceInterface $cloud,
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time, $messenger, $cloud_config_plugin_manager);
    $this->entityFieldManager = $entity_field_manager;
    $this->cloud = $cloud;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('entity_field.manager'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\cloud\Entity\CloudStore $entity */
    $entity = $this->entity;
    $cloud_store_type = $entity->bundle();
    $form = parent::buildForm($form, $form_state);

    $form = $this->reorderFieldset($form, $cloud_store_type);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $entity = $this->entity;

    // Save entity.
    if ($entity->save()) {

      $this->processOperationStatus($entity, 'created');

      $form_state->setRedirect('entity.cloud_store.collection', [
        'cloud_store_type' => $entity->bundle(),
      ]);
    }
    else {
      $this->processOperationErrorStatus($entity, 'created');
    }

    // Clear block and menu cache.
    $this->clearCacheValues($entity->getCacheTags());
  }

  /**
   * Add the build array of fieldset others.
   *
   * @param array &$form
   *   The form array.
   * @param string $cloud_store_type
   *   Cloud store type.
   *
   * @return array
   *   The formulated form array.
   */
  protected function reorderFieldset(array &$form, $cloud_store_type = ''): array {
    $entity_type = $this->entity->getEntityType()->id();
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $cloud_store_type);
    $fields = [];
    foreach ($field_definitions ?: [] as $key => $value) {
      if (preg_match('/field_(\w+)/', $key)) {
        $fields[] = $key;
      }
    }

    $fieldsets_def = [
      [
        'name' => 'store',
        'title' => $this->t('Store name'),
        'open' => TRUE,
        'fields' => [
          'name',
          'cloud_context',
        ],
      ],
      [
        'name' => 'parameter',
        'title' => $this->t('Parameter'),
        'open' => TRUE,
        'fields' => $fields,
      ],
      [
        'name' => 'others',
        'title' => $this->t('Others'),
        'open' => FALSE,
        'fields' => [
          'revision_log_message',
          'uid',
        ],
      ],
    ];

    $this->cloud->reorderForm($form, $fieldsets_def);
    return $form;
  }

}
