<?php

namespace Drupal\cloud\Form;

/**
 * Provide a confirmation form of bulk operation for cloud stores.
 */
abstract class CloudStoreProcessMultipleForm extends CloudProcessMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId(): string {
    return 'cloud_store_process_multiple_confirm_form';
  }

}
