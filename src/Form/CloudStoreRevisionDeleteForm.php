<?php

namespace Drupal\cloud\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a cloud store revision.
 *
 * @ingroup cloud_store
 */
class CloudStoreRevisionDeleteForm extends ConfirmFormBase {

  use CloudContentEntityTrait;

  /**
   * The cloud store revision.
   *
   * @var \Drupal\cloud\Entity\CloudStoreInterface
   */
  protected $revision;

  /**
   * The cloud store storage.
   *
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  protected $cloudStoreStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The mocked date formatter class.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructs a new CloudStoreRevisionDeleteForm.
   *
   * @param \Drupal\Core\Entity\RevisionableStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(
    RevisionableStorageInterface $entity_storage,
    Connection $connection,
    Messenger $messenger,
    DateFormatterInterface $date_formatter,
  ) {
    $this->cloudStoreStorage = $entity_storage;
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager->getStorage('cloud_store'),
      $container->get('database'),
      $container->get('messenger'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cloud_store_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', ['%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime())]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): TrustedCallbackInterface {
    return new Url('entity.cloud_store.version_history', [
      'cloud_store_type' => $this->revision->bundle(),
      'cloud_store' => $this->revision->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cloud_store_revision = NULL): array {
    $this->revision = $this->cloudStoreStorage->loadRevision($cloud_store_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->cloudStoreStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('cloud_store')->info($this->t('Cloud store: deleted %title revision %revision.', [
      '%title' => $this->revision->label(),
      '%revision' => $this->revision->getRevisionId(),
    ]));
    $this->messenger->addStatus($this->t('Revision from %revision-date of cloud store %title has been deleted.', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
      '%title' => $this->revision->label(),
    ]));
    $form_state->setRedirect(
      'entity.cloud_store.canonical', [
        'cloud_store' => $this->revision->id(),
        'cloud_store_type' => $this->revision->bundle(),
      ]
    );
    try {
      if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {cloud_store_field_revision} WHERE id = :id', [':id' => $this->revision->id()])
        ->fetchField() > 1) {
        $form_state->setRedirect(
          'entity.cloud_store.version_history', [
            'cloud_store_type' => $this->revision->bundle(),
            'cloud_store' => $this->revision->id(),
          ]
        );
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }

    // Clear block and menu cache.
    $this->clearCacheValues($this->revision->getCacheTags());
  }

}
