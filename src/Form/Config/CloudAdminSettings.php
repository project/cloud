<?php

namespace Drupal\cloud\Form\Config;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ExtensionDiscovery;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\cloud\Traits\CloudFormTrait;
use Drupal\geocoder\ProviderPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Cloud Admin Settings.
 */
class CloudAdminSettings extends ConfigFormBase {

  use CloudFormTrait;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The geocoder provider plugin manager.
   *
   * @var \Drupal\geocoder\ProviderPluginManager
   */
  protected $geocoderProviderPluginManager;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * App root string.
   *
   * @var string
   */
  protected $appRoot;

  /**
   * Constructs a CloudAdminSettings instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   * @param string $app_root
   *   The application root.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\geocoder\ProviderPluginManager $geocoder_provider_plugin_manager
   *   The geocoder provider plugin manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler,
    UrlGeneratorInterface $url_generator,
    string $app_root,
    TypedConfigManagerInterface $typed_config_manager,
    ?ProviderPluginManager $geocoder_provider_plugin_manager = NULL,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->moduleHandler = $module_handler;
    $this->urlGenerator = $url_generator;
    $this->geocoderProviderPluginManager = $geocoder_provider_plugin_manager;
    $this->appRoot = $app_root;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('url_generator'),
      $container->getParameter('app.root'),
      $container->get('config.typed'),
      $container->has('plugin.manager.geocoder.provider') ? $container->get('plugin.manager.geocoder.provider') : NULL
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cloud_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['cloud.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('cloud.settings');

    $form['custom_urls'] = [
      '#type' => 'details',
      '#title' => $this->t('Custom URLs'),
      '#open' => TRUE,
    ];

    $form['custom_urls']['cloud_use_default_urls'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Default Settings'),
      '#default_value' => $config->get('cloud_use_default_urls'),
      '#description' => $this->t('Uncheck if you want to customize the default JavaScript URLs to your own.'),
    ];

    $form['custom_urls']['cloud_custom_location_map_json_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Location map JSON URL'),
      '#default_value' => $config->get('cloud_custom_location_map_json_url'),
      '#description' => $this->t('The default URL is <em>https://enjalot.github.io/wwsd/data/world/ne_50m_admin_0_countries.geojson</em>.'),
      '#states' => [
        'visible' => [
          'input[name="cloud_use_default_urls"]' => ['checked' => FALSE],
        ],
      ],
    ];

    if ($this->moduleHandler->moduleExists('geocoder')) {
      $form['geocoder'] = [
        '#type' => 'details',
        '#title' => $this->t('Geocoder'),
        '#open' => TRUE,
      ];

      $providers = $this->geocoderProviderPluginManager->getPlugins();
      $options = [];
      foreach ($providers ?: [] as $key => $values) {
        $options[$key] = $values['name'];
      }

      $form['geocoder']['cloud_location_geocoder_plugin'] = [
        '#type' => 'select',
        '#title' => $this->t('Geocoder Plugin'),
        '#options' => $options,
        '#default_value' => $config->get('cloud_location_geocoder_plugin'),
        '#description' => $this->t('The Geocoder plugin to get latitude and longitude of cloud service provider location. Confirm the options / arguments of selected Geocoder plugin are set at <a href=":url">Geocoder configuration</a>.',
          [':url' => $this->urlGenerator->generate('geocoder.settings')]),
      ];
    }

    $form['custom_urls']['cloud_custom_d3_js_url'] = [
      '#type' => 'url',
      '#title' => $this->t('D3.js JavaScript URL'),
      '#default_value' => $config->get('cloud_custom_d3_js_url'),
      '#description' => $this->t('The default URL is <em>https://d3js.org/d3.v5.min.js</em>.'),
      '#states' => [
        'visible' => [
          'input[name="cloud_use_default_urls"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['custom_urls']['cloud_custom_d3_horizon_chart_js_url'] = [
      '#type' => 'url',
      '#title' => $this->t('D3 Horizon Chart JavaScript URL'),
      '#default_value' => $config->get('cloud_custom_d3_horizon_chart_js_url'),
      '#description' => $this->t('The default URL is <em>https://unpkg.com/@docomoinnovations/horizon-chart@1.1.0/dist/d3-horizon-chart.js</em>.'),
      '#states' => [
        'visible' => [
          'input[name="cloud_use_default_urls"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['custom_urls']['cloud_custom_c3_js_url'] = [
      '#type' => 'url',
      '#title' => $this->t('C3.js JavaScript URL'),
      '#default_value' => $config->get('cloud_custom_c3_js_url'),
      '#description' => $this->t('The default URL is <em>https://cdnjs.cloudflare.com/ajax/libs/c3/0.7.15/c3.min.js</em>.'),
      '#states' => [
        'visible' => [
          'input[name="cloud_use_default_urls"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['custom_urls']['cloud_custom_c3_css_url'] = [
      '#type' => 'url',
      '#title' => $this->t('C3.js CSS URL'),
      '#default_value' => $config->get('cloud_custom_c3_css_url'),
      '#description' => $this->t('The default URL is <em>https://cdnjs.cloudflare.com/ajax/libs/c3/0.7.15/c3.min.css</em>.'),
      '#states' => [
        'visible' => [
          'input[name="cloud_use_default_urls"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['custom_urls']['cloud_custom_chart_js_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Chart.js JavaScript URL'),
      '#default_value' => $config->get('cloud_custom_chart_js_url'),
      '#description' => $this->t('The default URL is <em>https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js</em>.'),
      '#states' => [
        'visible' => [
          'input[name="cloud_use_default_urls"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['custom_urls']['cloud_custom_select2_css_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Select2 CSS URL'),
      '#default_value' => $config->get('cloud_custom_select2_css_url'),
      '#description' => $this->t('The default URL is <em>https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css</em>.'),
      '#states' => [
        'visible' => [
          'input[name="cloud_use_default_urls"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['custom_urls']['cloud_custom_select2_js_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Select2 JavaScript URL'),
      '#default_value' => $config->get('cloud_custom_select2_js_url'),
      '#description' => $this->t('The default URL is <em>https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js</em>.'),
      '#states' => [
        'visible' => [
          'input[name="cloud_use_default_urls"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['custom_urls']['cloud_custom_datatables_js_url'] = [
      '#type' => 'url',
      '#title' => $this->t('DataTables JavaScript URL'),
      '#default_value' => $config->get('cloud_custom_datatables_js_url'),
      '#description' => $this->t('The default URL is <em>https://cdn.jsdelivr.net/npm/simple-datatables@3.1.2/dist/umd/simple-datatables.min.js</em>.'),
      '#states' => [
        'visible' => [
          'input[name="cloud_use_default_urls"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['views'] = [
      '#type' => 'details',
      '#title' => $this->t('Views'),
      '#open' => TRUE,
      '#description' => $this->t("Note that selecting the default option will overwrite View's settings."),
    ];

    $form['views']['pager_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Pager options'),
      '#open' => TRUE,
    ];

    $form['views']['pager_options']['cloud_view_expose_items_per_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow user to control the number of items displayed in views.'),
      '#default_value' => $config->get('cloud_view_expose_items_per_page'),
      '#description' => $this->t('When enabled, an "Items per page" dropdown listbox is shown.'),
    ];

    $form['views']['pager_options']['cloud_view_items_per_page'] = [
      '#type' => 'select',
      '#options' => cloud_get_views_items_options(),
      '#title' => $this->t('Items per page'),
      '#description' => $this->t('Number of items to display on each page in views.'),
      '#default_value' => $config->get('cloud_view_items_per_page'),
    ];

    $form['queue'] = [
      '#type' => 'details',
      '#title' => $this->t('Queue'),
      '#open' => TRUE,
    ];
    $form['queue']['cloud_enable_queue_cleanup'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable queue cleanup'),
      '#description' => $this->t('Clean up orphaned and failed batch items in the queue table.  Items older than one day are deleted.'),
      '#default_value' => $config->get('cloud_enable_queue_cleanup'),
    ];

    $form['spa'] = [
      '#type' => 'details',
      '#title' => $this->t('SPA'),
      '#open' => TRUE,
    ];

    $form['spa']['cloud_use_spa'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use React single page application (SPA)'),
      '#description' => $this->t('The React SPA is a javascript UI that interacts with Cloud Orchestrator.'),
      '#default_value' => $config->get('cloud_use_spa'),
    ];

    // Since drupal:simple_oauth is not part cloud's composer.json anymore,
    // users can't enable cloud_dashboard unless they installed it, or use
    // the Cloud Orchestrator distribution, which includes simple_oauth.
    $listing = new ExtensionDiscovery($this->appRoot, FALSE);
    $modules = $listing->scan('module');
    if (empty($modules['simple_oauth'])) {
      $form['spa']['cloud_use_spa']['#default_value'] = FALSE;
      $form['spa']['cloud_use_spa']['#disabled'] = TRUE;
      $form['spa']['message'] = [
        '#type' => 'item',
        '#title' => $this->t('Missing modules.'),
        '#description' => $this->t('The Simple Oauth module is missing.  The SPA application cannot be enabled.'),
      ];
    }

    $form['spa']['spa_settings'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => 'SPA settings',
      '#states' => [
        'visible' => [
          ':input[id=edit-cloud-use-spa]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['spa']['spa_settings']['cloud_spa_key_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Directory for keys'),
      '#description' => $this->t('This is the directory where the public and private keys will be stored. This <strong>SHOULD</strong> be located outside of your webroot to avoid making them public unintentionally.  The directory should be writeable by the webserver.'),
      '#default_value' => $config->get('cloud_spa_key_directory') ?? '',
    ];

    global $base_url;
    $form['spa']['spa_settings']['cloud_spa_callback_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URI'),
      '#default_value' => empty($config->get('cloud_spa_callback_url')) ? $base_url . '/clouds/dashboard/callback' : $config->get('cloud_spa_callback_url'),
      '#description' => $this->t('The URI the SPA client will redirect to when needed.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $d3_url = $form_state->getValue('cloud_custom_d3_js_url');
    $d3_horizon_chart_url = $form_state->getValue('cloud_custom_d3_horizon_chart_js_url');
    $c3_js_url = $form_state->getValue('cloud_custom_c3_js_url');
    $c3_css_url = $form_state->getValue('cloud_custom_c3_css_url');
    $chartjs_url = $form_state->getValue('cloud_custom_chart_js_url');
    $select2_css_url = $form_state->getValue('cloud_custom_select2_css_url');
    $select2_js_url = $form_state->getValue('cloud_custom_select2_js_url');
    $datatables_js_url = $form_state->getValue('cloud_custom_datatables_js_url');

    if (empty($d3_url)
    || empty($d3_horizon_chart_url)
    || empty($c3_js_url)
    || empty($c3_css_url)
    || empty($chartjs_url)
    || empty($select2_css_url)
    || empty($select2_js_url)
    || empty($datatables_js_url)) {
      $form_state->setErrorByName('cloud_custom_d3_js_url', $this->t('Enter D3.js JavaScript URL.'));
      $form_state->setErrorByName('cloud_custom_d3_horizon_chart_js_url', $this->t('Enter D3 Horizon Chart JavaScript URL.'));
      $form_state->setErrorByName('cloud_custom_c3_js_url', $this->t('Enter C3.js JavaScript URL.'));
      $form_state->setErrorByName('cloud_custom_c3_css_url', $this->t('Enter C3.js CSS URL.'));
      $form_state->setErrorByName('cloud_custom_chart_js_url', $this->t('Enter Chart.js JavaScript URL.'));
      $form_state->setErrorByName('cloud_custom_select2_js_url', $this->t('Enter Select2 JavaScript URL.'));
      $form_state->setErrorByName('cloud_custom_select2_css_url', $this->t('Enter Select2 CSS URL.'));
      $form_state->setErrorByName('cloud_custom_datatables_js_url', $this->t('Enter DataTables JavaScript URL.'));
    }

    if ((bool) $form_state->getValue('cloud_use_spa') === FALSE) {
      return;
    }

    // Check if the callback URL exists.
    if (empty($form_state->getValue('cloud_spa_callback_url'))) {
      $form_state->setError($form['spa']['spa_settings']['cloud_spa_callback_url'], $this->t('Callback url is empty.'));
    }

    // Validate the key directory by using `isDirectory()`.
    if (is_dir($form_state->getValue('cloud_spa_key_directory')) === FALSE) {
      $form_state->setError($form['spa']['spa_settings']['cloud_spa_key_directory'], $this->t('Private key directory is not valid. The directory should be the absolute path to a directory outside the webserver document root.'));
      return;
    }

    // Check if the directory is writeable.
    if (is_writable($form_state->getValue('cloud_spa_key_directory')) === FALSE) {
      $form_state->setError($form['spa']['spa_settings']['cloud_spa_key_directory'], $this->t('Private key directory is not writeable. The directory should be writeable by the webserver.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $config = $this->configFactory()->getEditable('cloud.settings');

    $config->set('cloud_use_default_urls', $form_state->getValue('cloud_use_default_urls'));
    $config->set('cloud_custom_location_map_json_url', $form_state->getValue('cloud_custom_location_map_json_url'));
    $config->set('cloud_custom_d3_js_url', $form_state->getValue('cloud_custom_d3_js_url'));
    $config->set('cloud_custom_d3_horizon_chart_js_url', $form_state->getValue('cloud_custom_d3_horizon_chart_js_url'));
    $config->set('cloud_custom_c3_js_url', $form_state->getValue('cloud_custom_c3_js_url'));
    $config->set('cloud_custom_c3_css_url', $form_state->getValue('cloud_custom_c3_css_url'));
    $config->set('cloud_custom_chart_js_url', $form_state->getValue('cloud_custom_chart_js_url'));
    $config->set('cloud_custom_select2_js_url', $form_state->getValue('cloud_custom_select2_js_url'));
    $config->set('cloud_custom_select2_css_url', $form_state->getValue('cloud_custom_select2_css_url'));
    $config->set('cloud_custom_datatables_js_url', $form_state->getValue('cloud_custom_datatables_js_url'));
    $config->set('cloud_view_items_per_page', $form_state->getValue('cloud_view_items_per_page'));
    $config->set('cloud_view_expose_items_per_page', $form_state->getValue('cloud_view_expose_items_per_page'));

    if ($this->moduleHandler->moduleExists('geocoder')) {
      $config->set('cloud_location_geocoder_plugin', $form_state->getValue('cloud_location_geocoder_plugin'));
    }
    $config->set('cloud_enable_queue_cleanup', $form_state->getValue('cloud_enable_queue_cleanup'));
    $config->save();

    $views_settings = [];
    $views_settings['cloud_view_items_per_page'] = (int) $form_state->getValue('cloud_view_items_per_page');
    $views_settings['cloud_view_expose_items_per_page'] = (boolean) $form_state->getValue('cloud_view_expose_items_per_page');
    $this->updateViewsPagerOptions('cloud', $views_settings);

    $use_spa = !empty($form_state->getValue('cloud_use_spa'));

    if ($use_spa) {
      $this->enableSpaApp(
        $form_state->getValue('cloud_spa_key_directory'),
        $form_state->getValue('cloud_spa_callback_url')
      );
    }
    else {
      $this->disableSpaApp();
    }

    parent::submitForm($form, $form_state);
  }

}
