<?php

namespace Drupal\cloud\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RenderableInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\cloud\Traits\CloudResourceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for blocks that build resource tables.
 */
abstract class CloudBaseResourcesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use CloudContentEntityTrait;
  use CloudResourceTrait;

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The cloud config plugin manager.
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Creates a ResourcesBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud config plugin manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    RouteMatchInterface $route_match,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['cloud_context'] = [
      '#type' => 'select',
      '#title' => $this->t('Cloud service provider'),
      '#description' => $this->t('Select cloud service provider.'),
      '#options' => $this->cloudConfigPluginManager->getCloudConfigs($this->t('All @region regions', [
        '@region' => $this->getBundleName(),
      ]), $this->getEntityBundleType()),
      '#default_value' => $config['cloud_context'] ?? '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['cloud_context'] = $form_state->getValue('cloud_context');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'cloud_context' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $region = $this->getBundleName();
    $cloud_configs = $this->cloudConfigPluginManager->getCloudConfigs($this->t('All %region regions', [
      '%region' => $region,
    ]), $this->getEntityBundleType());
    $cloud_context = $this->configuration['cloud_context'];
    $cloud_context_name = empty($cloud_context)
      ? $this->t('All %region regions', [
        '%region' => $region,
      ])
      : $cloud_configs[$cloud_context];
    $build = [];
    $build['resources'] = [
      '#type' => 'details',
      '#title' => $this->t('Resources'),
      '#open' => TRUE,
    ];

    $build['resources']['description'] = [
      '#markup' => $this->t('You are using the following %region resources in %cloud_context_name:',
        [
          '%region' => $region,
          '%cloud_context_name' => $cloud_context_name,
        ]
      ),
    ];
    $build['resources']['resource_table'] = $this->buildResourceTable();

    return $build;
  }

  /**
   * Get cloud_config_type bundle name.
   *
   * @return string
   *   Return the bundle name.
   */
  private function getBundleName(): string {
    try {
      $entities = $this->entityTypeManager->getStorage('cloud_config_type')
        ->loadByProperties(
          [
            'id' => $this->getEntityBundleType(),
          ]
        );
      if (empty($entities)) {
        return '';
      }
      /** @var \Drupal\cloud\Entity\CloudConfig $entity */
      $entity = reset($entities);
      return !empty($entity) ? $entity->label() : '';
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return '';
    }
  }

  /**
   * Build a resource table row.
   *
   * @param array $resources
   *   Entity array to build the row.
   *
   * @return array
   *   Fully built rows.
   */
  protected function buildResourceTableRows(array $resources): array {
    $rows = [];
    $data = [];
    $i = 0;
    $index = 0;

    foreach ($resources ?: [] as $key => $values) {
      $data[] = $this->getResourceLink($key, $values[0], $values[1]);

      // Skip if $data does not have any value.
      if (empty($data[count($data) - 1])) {
        continue;
      }

      $rows[$index] = [
        'no_striping' => TRUE,
        'data' => $data,
      ];
      if ($i++ % 2) {
        $index++;
        $data = [];
      }
    }
    return $rows;
  }

  /**
   * Generate OpenStack resource link.
   *
   * @param string $resource_type
   *   The resource type.
   * @param string $permission
   *   The getResourceCount permission.
   * @param array $params
   *   The getResourceCount params.
   *
   * @return \Drupal\Core\Render\RenderableInterface
   *   The resource link object.
   */
  protected function getResourceLink($resource_type, $permission, array $params = []): RenderableInterface {
    // Fetch the labels.
    $labels = $this->getDisplayLabels($resource_type);
    $cloud_context = $this->configuration['cloud_context'];

    if (!empty($cloud_context)) {
      $route_name = "view.{$resource_type}.list";
      $params = [
        'cloud_context' => $cloud_context,
      ];
    }
    else {
      $route_name = "view.{$resource_type}.all";
    }

    return Link::createFromRoute(
      $this->formatPlural(
        $this->getResourceCount($resource_type, $permission, $params),
        '1 @label',
        '@count @plural_label',
        [
          '@label' => $labels['singular'] ?? $resource_type,
          '@plural_label' => $labels['plural'] ?? $resource_type,
        ]
      ),
      $route_name,
      $params
    );
  }

  /**
   * Build resource table.
   *
   * @return array
   *   Table render array.
   */
  abstract protected function buildResourceTable(): array;

  /**
   * Return cloud_config bundle type.
   *
   * @return string
   *   The cloud_config bundle string.
   */
  abstract protected function getEntityBundleType(): string;

}
