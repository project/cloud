<?php

namespace Drupal\cloud\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides plugin definitions for custom local menu.
 *
 * @see \Drupal\cloud\Plugin\Derivative\CloudLocalTasks
 */
class CloudMenuLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs new CloudLocalTasks.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {

    // Get cloud config.
    $cloud_config = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();

    $cloud_service_provider = array_filter($cloud_config);
    $cloud_service_count = !empty($cloud_service_provider) ? count($cloud_service_provider) : '';

    $cloud_service_provider = array_filter($cloud_config);
    $cloud_service_count = !empty($cloud_service_provider) ? count($cloud_service_provider) : '';

    $links = [];

    if ($cloud_service_count <= 0) {
      $id = "cloud_service_provider.local_tasks";
      $links[$id] = $base_plugin_definition;
      $links[$id]['title'] = $this->t('Add cloud service provider');
      $links[$id]['parent'] = 'cloud.service_providers.menu';
      $links[$id]['route_parameters'] = [];
      $links[$id]['weight'] = -110;
      $links[$id]['route_name'] = 'entity.cloud_config.add_page';
    }
    else {
      // Add dropdown menu for all.
      $title = "All";
      $id = "cloud.service_providers.menu.all";
      $links[$id] = $base_plugin_definition;
      $links[$id]['title'] = $this->t('@title', ['@title' => $title]);
      $links[$id]['route_name'] = "view.cloud_config.list";
      $links[$id]['menu_name'] = 'cloud.service_providers.menu.all';
      $links[$id]['parent'] = 'cloud.service_providers.menu';
      $links[$id]['weight'] = -100;
      $links[$id]['expanded'] = TRUE;

      // Add dropdown menu for all under design menu.
      $title = "All";
      $id = "cloud.design.menu.all";
      $links[$id] = $base_plugin_definition;
      $links[$id]['title'] = $this->t('@title', ['@title' => $title]);
      $links[$id]['route_name'] = "view.cloud_config.list";
      $links[$id]['menu_name'] = 'cloud.design.menu.all';
      $links[$id]['parent'] = 'cloud.design.menu';
      $links[$id]['weight'] = -100;
      $links[$id]['expanded'] = TRUE;

      // Add dropdown menu for cloud service provider.
      $title = "Cloud service providers";
      $id = "list.service_providers.menu";
      $links[$id] = $base_plugin_definition;
      $links[$id]['title'] = $this->t('@title', ['@title' => $title]);
      $links[$id]['route_name'] = "view.cloud_config.list";
      $links[$id]['menu_name'] = 'list.service_providers.menu';
      $links[$id]['parent'] = 'cloud.menu.cloud_links:cloud.service_providers.menu.all';
      $links[$id]['weight'] = -120;
    }

    // Add users link under manage menu if the "RoleAssign" module is enabled.
    if ($this->moduleHandler->moduleExists('roleassign')) {
      $title = "Users";
      $id = "cloud.admin.menu.users";
      $links[$id] = $base_plugin_definition;
      $links[$id]['title'] = $this->t('@title', ['@title' => $title]);
      $links[$id]['route_name'] = 'entity.user.collection';
      $links[$id]['menu_name'] = 'cloud.admin.menu.users';
      $links[$id]['parent'] = 'cloud.admin.menu';
      $links[$id]['weight'] = 20;
    }

    // Add permissions link under manage menu,
    // if the "Module Permissions" module is enabled.
    if ($this->moduleHandler->moduleExists('module_permissions')) {
      $title = "Permissions";
      $id = "cloud.admin.menu.permissions";
      $links[$id] = $base_plugin_definition;
      $links[$id]['title'] = $this->t('@title', ['@title' => $title]);
      $links[$id]['route_name'] = 'user.admin_permissions';
      $links[$id]['menu_name'] = 'cloud.admin.menu.permissions';
      $links[$id]['parent'] = 'cloud.admin.menu';
      $links[$id]['weight'] = 30;
    }

    return $links;
  }

}
