<?php

namespace Drupal\cloud\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides cloud context bundles and cloud stores.
 */
class CloudStoreCloudStoreTypeBundleDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a CloudStoreCloudStoreTypeBundleDeriver instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): ?array {
    // Keep a copy of the original and flag it as the original.
    $cloud_store_types = $this->entityTypeManager->getStorage('cloud_store_type')->loadByProperties();
    foreach ($cloud_store_types ?: [] as $key => $cloud_store) {
      if ($key === $base_plugin_definition['entity_bundle']) {
        $this->derivatives[$cloud_store->id()] = $base_plugin_definition;
        $this->derivatives[$cloud_store->id()]['entity_bundle'] = $base_plugin_definition['entity_bundle'];
      }
    }
    return $this->derivatives;
  }

}
