<?php

namespace Drupal\cloud\Plugin\Derivative;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\cloud\Plugin\cloud\store\CloudStorePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides plugin definitions for local action.
 */
class CloudStoreLocalAction extends LocalActionDefault {

  /**
   * The CloudStorePluginManager.
   *
   * @var \Drupal\cloud\Plugin\cloud\store\CloudStorePluginManager
   */
  protected $storagePluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteProviderInterface $route_provider, CloudStorePluginManagerInterface $store_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);

    $this->routeProvider = $route_provider;
    $this->storagePluginManager = $store_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('router.route_provider'),
      $container->get('plugin.manager.cloud_store_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match): array {
    $cloud_store_type = basename($route_match->getRouteObject()->getPath());
    $parameters['cloud_store_type'] = $cloud_store_type;
    return $parameters;
  }

}
