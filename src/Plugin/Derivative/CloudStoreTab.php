<?php

namespace Drupal\cloud\Plugin\Derivative;

use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides plugin definitions for custom local task.
 */
class CloudStoreTab extends LocalTaskDefault {

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match): array {
    $parameters = parent::getRouteParameters($route_match);
    $cloud_store_type = $route_match->getParameter('cloud_store_type');
    $parameters['cloud_store_type'] = !empty($cloud_store_type) ? $cloud_store_type : '';
    return $parameters;
  }

}
