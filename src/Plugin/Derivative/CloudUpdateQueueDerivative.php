<?php

namespace Drupal\cloud\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver class to set up queues for each cloud service provider.
 */
class CloudUpdateQueueDerivative extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * Constructs new CloudUpdateQueueDerivative.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Cloud\Service\CloudServiceInterface $cloud_service
   *   The Cloud service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    CloudServiceInterface $cloud_service,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->cloudService = $cloud_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $ids = $this->entityTypeManager->getStorage('cloud_config')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', $base_plugin_definition['provider'])
      ->execute();
    if (empty($ids)) {
      return [];
    }
    $entities = $this->entityTypeManager->getStorage('cloud_config')
      ->loadMultiple($ids);
    foreach ($entities ?: [] as $entity) {
      $this->derivatives[$entity->getCloudContext()] = $base_plugin_definition;
      $this->derivatives[$entity->getCloudContext()]['title'] = $this->cloudService->getResourceQueueName($entity);
      // Set the cron time here instead of hook_queue_info_alter().
      $config = $this->configFactory->get("{$base_plugin_definition['provider']}.settings");
      $cron_interval = $config->get("{$base_plugin_definition['provider']}_update_resources_queue_cron_time");
      if (!empty($cron_interval)) {
        $this->derivatives[$entity->getCloudContext()]['cron']['time'] = $cron_interval;
      }
    }
    return $this->derivatives;
  }

}
