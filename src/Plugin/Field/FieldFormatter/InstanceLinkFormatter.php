<?php

namespace Drupal\cloud\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'instance_link_formatter' formatter.
 *
 * This formatter links a cloud service provider name to the list of server
 * templates.
 *
 * @FieldFormatter(
 *   id = "instance_link_formatter",
 *   label = @Translation("Instance link"),
 *   field_types = {
 *     "string",
 *     "uri",
 *   }
 * )
 */
class InstanceLinkFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The cloud service.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloud;

  /**
   * Constructs a StringFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud
   *   The Cloud service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    EntityTypeManagerInterface $entity_type_manager,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    RouteMatchInterface $route_match,
    CloudServiceInterface $cloud,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
    $this->cloud = $cloud;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_route_match'),
      $container->get('cloud')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): ?array {
    $entity = $items->getEntity();
    $elements = [];
    foreach ($items ?: [] as $delta => $item) {
      if (!$item->isEmpty()) {
        $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
        $route = $this->cloudConfigPluginManager->getInstanceCollectionTemplateName();

        // Prepend the icon if it is set.
        $image_url = $this->prepareIcon($entity);
        if (!empty($image_url)) {
          $elements[$delta][] = [
            '#type' => 'link',
            '#title' => [
              '#type' => 'markup',
              // Manually building the image URL.  If using the image_style
              // theme function, it adds a white space that shows up as
              // underline on hover.
              '#markup' => '<img src=' . $image_url . ' class="image-style-icon-32x32">',
              '#attached' => [
                'library' => [
                  'cloud/cloud_icon',
                ],
              ],
            ],
            '#url' => Url::fromRoute($route, ['cloud_context' => $entity->getCloudContext()]),
          ];
        }

        $url = Url::fromRoute($route, ['cloud_context' => $entity->getCloudContext()]);
        $elements[$delta][] = [
          '#type' => 'link',
          '#url' => $url,
          '#title' => $item->value,
        ];
      }
    }
    return $elements;
  }

  /**
   * Prepare the cloud config icon.
   *
   * @param \Drupal\cloud\Entity\CloudConfig $entity
   *   Cloud config entity.
   *
   * @return string
   *   Generated image URL or empty if not found.
   */
  private function prepareIcon(CloudConfig $entity): string {
    $image_url = '';
    $fid = $entity->getIconFid();

    // Generate icon URL only have file_target has been retrieved.
    if (!empty($fid)) {
      $fileStorage = $this->entityTypeManager->getStorage('file');
      $image = $fileStorage->load($fid);
      try {
        $imageStyleStorage = $this->entityTypeManager->getStorage('image_style');
        $image_url = $imageStyleStorage->load('icon_32x32')->buildUrl($image->uri->value);
      }
      catch (\Exception $e) {
        $this->cloud->handleException($e);
      }
    }
    return $image_url;
  }

}
