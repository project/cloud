<?php

namespace Drupal\cloud\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'server_template_link_formatter' formatter.
 *
 * This formatter links a cloud service provider name to the list of server
 * templates.
 *
 * @FieldFormatter(
 *   id = "server_template_link_formatter",
 *   label = @Translation("Launch Template Link"),
 *   field_types = {
 *     "string",
 *     "uri",
 *   }
 * )
 */
class LaunchTemplateLinkFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Constructs a StringFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, CloudConfigPluginManagerInterface $cloud_config_plugin_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.cloud_config_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): ?array {
    $entity = $items->getEntity();
    $elements = [];
    foreach ($items ?: [] as $delta => $item) {
      if (!$item->isEmpty()) {
        $this->cloudConfigPluginManager->setCloudContext($entity->getCloudContext());
        $route = $this->cloudConfigPluginManager->getLaunchTemplateCollectionName();
        $elements[$delta] = [
          '#type' => 'link',
          '#url' => Url::fromRoute($route, ['cloud_context' => $entity->getCloudContext()]),
          '#title' => $item->value,
        ];
      }
    }
    return $elements;
  }

}
