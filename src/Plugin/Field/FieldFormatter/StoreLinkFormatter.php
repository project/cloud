<?php

namespace Drupal\cloud\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'storage_link_formatter' formatter.
 *
 * This formatter links a cloud service provider name to the list of server
 * templates.
 *
 * @FieldFormatter(
 *   id = "store_link_formatter",
 *   label = @Translation("Cloud store Link"),
 *   field_types = {
 *     "string",
 *     "uri",
 *   }
 * )
 */
class StoreLinkFormatter extends LaunchTemplateLinkFormatter {

}
