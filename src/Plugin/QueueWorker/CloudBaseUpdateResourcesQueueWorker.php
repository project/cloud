<?php

namespace Drupal\cloud\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginException;
use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * Contains common methods used during queue processing.
 */
abstract class CloudBaseUpdateResourcesQueueWorker extends QueueWorkerBase {

  use CloudContentEntityTrait;

  /**
   * Method to check if a method exists in a class.
   *
   * @param object $class
   *   Service object to check.
   * @param string $method_name
   *   Name of method to check.
   *
   * @throws \Exception
   *   Throw exception regarding error.
   */
  protected function checkServiceMethod(object $class, string $method_name):void {
    if (!method_exists($class, $method_name)) {
      $msg = $this->t('The method @method_name does not exist in class @class.', [
        '@class' => get_class($class),
        '@method_name' => $method_name,
      ]);
      $this->logger($this->pluginDefinition['provider'])->error($msg);
      throw new \Exception($msg);
    }
  }

  /**
   * Set cloud context and check for CloudConfigPluginException.
   *
   * @param object $class
   *   Service object to set and check.
   * @param string $cloud_context
   *   Cloud context.
   * @param string $method_name
   *   Method to process.
   *
   * @throws \Exception
   *   Throw exception regarding error.
   */
  protected function setCloudContext(object &$class, string $cloud_context, string $method_name):void {
    try {
      if (!method_exists($class, 'setCloudContext')) {
        $class = get_class($class);
        throw new \Exception("{$class}::setCloudContext() not found.");
      }
      $class->setCloudContext($cloud_context);
    }
    catch (CloudConfigPluginException $e) {
      $msg = $this->t(
        'Attempted to process a queue for @class::@method_name; however the queue has been simply deleted since it could not be processed due to a missing $cloud_context @cloud_context. The @provider cloud service provider @cloud_context might be deleted before the queue is processed.', [
          '@class' => get_class($class),
          '@provider' => $this->pluginDefinition['provider'],
          '@method_name' => "K8sServiceInterface::$method_name()",
          '@cloud_context' => $cloud_context,
        ]
      );
      $this->logger($this->pluginDefinition['provider'])->info($msg);
      throw new \Exception($msg);
    }
  }

}
