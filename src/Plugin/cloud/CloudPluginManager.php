<?php

namespace Drupal\cloud\Plugin\cloud;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * Provides a base class for Cloud plugin.
 */
class CloudPluginManager extends DefaultPluginManager implements CloudPluginManagerInterface {

  use CloudContentEntityTrait;

  /**
   * Creates the discovery object.
   *
   * @param string|bool $subdir
   *   The plugin's subdirectory, for example Plugin/views/filter.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param string|null $plugin_interface
   *   (optional) The interface each plugin should implement.
   * @param string $plugin_definition_annotation_name
   *   (optional) The name of the annotation containing the plugin definition.
   *   Defaults to 'Drupal\Component\Annotation\Plugin'.
   * @param string[] $additional_annotation_namespaces
   *   (optional) Additional namespaces to scan for annotation definitions.
   */
  public function __construct(
    $subdir,
    \Traversable $namespaces,
    ModuleHandlerInterface $module_handler,
    $plugin_interface = NULL,
    $plugin_definition_annotation_name = 'Drupal\Component\Annotation\Plugin',
    array $additional_annotation_namespaces = [],
  ) {

    $this->subdir = $subdir;
    $this->namespaces = $namespaces;
    $this->pluginDefinitionAnnotationName = $plugin_definition_annotation_name;
    $this->pluginInterface = $plugin_interface;
    $this->moduleHandler = $module_handler;
    $this->additionalAnnotationNamespaces = $additional_annotation_namespaces;

    $this->messenger();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery(): DiscoveryInterface {
    $file_name_suffix = $this->getFileNameSuffix();
    if (!isset($this->discovery) && !empty($file_name_suffix)) {
      $this->discovery = new YamlDiscovery($file_name_suffix, $this->moduleHandler->getModuleDirectories());
      $this->discovery->addTranslatableProperty('label', 'label_context');
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($this->discovery);
    }
    return parent::getDiscovery();
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileNameSuffix(): string {
    return '';
  }

}
