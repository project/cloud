<?php

namespace Drupal\cloud\Plugin\cloud\config;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Entity\CloudConfigInterface;
use Symfony\Component\Routing\Route;

/**
 * Common interfaces for cloud service provider (CloudConfig) plugins.
 *
 * @package Drupal\cloud\Plugin
 */
interface CloudConfigPluginInterface {

  /**
   * Load all config entities.
   *
   * @return array
   *   An array of cloud_config entities
   */
  public function loadConfigEntities(): iterable;

  /**
   * Load a single cloud_config entity.
   *
   * @param string $cloud_context
   *   The cloud_context to load the entity from.
   *
   * @return mixed
   *   The cloud_config entity.
   */
  public function loadConfigEntity(string $cloud_context): ?CloudConfigInterface;

  /**
   * Load credentials for a given cloud context.
   *
   * @param string $cloud_context
   *   The cloud_context to load the credentials from.
   *
   * @return mixed
   *   Array of credentials.
   */
  public function loadCredentials($cloud_context): ?array;

  /**
   * Return the name of the aws_cloud_instance collection template name.
   *
   * @return string
   *   The instance collection template name.
   */
  public function getInstanceCollectionTemplateName(): string;

  /**
   * Return the name of the aws_cloud_instance collection template name.
   *
   * @return string
   *   The instance collection template name.
   */
  public function getPricingPageRoute(): string;

  /**
   * Check access privilege.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface;

}
