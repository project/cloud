<?php

namespace Drupal\cloud\Plugin\cloud\config;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides the default cloud_config_plugin manager.
 */
class CloudConfigPluginManager extends CloudPluginManager implements CloudConfigPluginManagerInterface {

  /**
   * Provides default values for all cloud_config_plugin plugins.
   *
   * @var array
   */
  protected $defaults = [
    'id' => 'cloud_config',
    'entity_type' => 'cloud_config',
  ];

  /**
   * The cloud context.
   *
   * @var string
   */
  private $cloudContext;

  /**
   * The cloud service provider plugin (CloudConfigPlugin).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginInterface
   */
  private $plugin;

  /**
   * Constructs a new CloudConfigPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(
    \Traversable $namespaces,
    ModuleHandlerInterface $module_handler,
    CacheBackendInterface $cache_backend,
  ) {

    parent::__construct('Plugin\cloud\config', $namespaces, $module_handler);

    // Add more services as required.
    $this->moduleHandler = $module_handler;
    $this->setCacheBackend($cache_backend, 'cloud_config_plugin', ['cloud_config_plugin']);
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The instance of ContainerInterface.
   *
   * @return \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManager
   *   return created object.
   */
  public static function create(ContainerInterface $container): CloudConfigPluginManager {
    return new static(
      $container->get('container.namespaces'),
      $container->get('module_handler'),
      $container->get('cache.discovery')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileNameSuffix(): string {
    return 'cloud.config.plugin';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function processDefinition(&$definition, $plugin_id): void {
    parent::processDefinition($definition, $plugin_id);

    if (empty($definition['id'])) {
      throw new PluginException(sprintf('Example plugin property (%s) definition "is" is required.', $plugin_id));
    }

    if (empty($definition['entity_bundle'])) {
      throw new PluginException(sprintf('entity_bundle property is required for (%s)', $plugin_id));
    }

    if (!isset($definition['base_plugin']) && empty($definition['cloud_context'])) {
      throw new PluginException(sprintf('cloud_context property is required for (%s)', $plugin_id));
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginException
   */
  public function setCloudContext($cloud_context): void {
    $this->cloudContext = $cloud_context;
    // Load the plugin variant since we know the cloud_context.
    $this->plugin = $this->loadPluginVariant();
    if (empty($this->plugin)) {
      // DO NOT translate the message.
      // @see https://www.drupal.org/docs/develop/coding-standards/php-exceptions
      $message = $this->t('Cannot load cloud service provider plugin: %cloud_context ($cloud_context) at %method', [
        '%cloud_context' => !empty($cloud_context) ? $cloud_context : '<Empty>',
        '%method' => __METHOD__ . '()',
      ]);
      $this->logger('cloud')->error($message);
      throw new CloudConfigPluginException($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadPluginVariant() {
    $plugin = FALSE;
    foreach ($this->getDefinitions() ?: [] as $key => $definition) {
      if (isset($definition['cloud_context'])
        && $definition['cloud_context'] === $this->cloudContext) {
        $plugin = $this->createInstance($key);
        break;
      }
    }
    return $plugin;
  }

  /**
   * Load a config entity.
   *
   * @return \Drupal\cloud\Entity\CloudConfig
   *   The cloud service provider (CloudConfig) entity.
   *
   * @throws \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginException
   */
  public function loadConfigEntity(): ?CloudConfigInterface {

    $config_entity = !empty($this->plugin)
      ? $this->plugin->loadConfigEntity($this->cloudContext) : NULL;

    if (empty($config_entity)) {
      $message = $this->t('Cannot load cloud service provider plugin: %cloud_context ($cloud_context) at %method', [
        '%cloud_context' => !empty($this->cloud_context) ? $this->cloud_context : '<Empty>',
        '%method' => __METHOD__ . '()',
      ]);
      $this->logger('cloud')->error($message);
      throw new CloudConfigPluginException($message);
    }
    return $config_entity;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginException
   */
  public function loadConfigEntities($entity_bundle): iterable {
    /** @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginInterface $plugin */
    $plugin = $this->loadBasePluginDefinition($entity_bundle);
    if (empty($plugin)) {
      $message = $this->t('Cannot load cloud service provider entity (CloudConfig) for %bundle at %method', [
        '%bundle' => !empty($entity_bundle) ? $entity_bundle : '<Empty>',
        '%method' => __METHOD__ . '()',
      ]);
      $this->logger('cloud')->error($message);
      throw new CloudConfigPluginException($message);
    }
    return $plugin->loadConfigEntities();
  }

  /**
   * Helper method to load the base plugin definition.
   *
   * Useful when there is no cloud_context.
   *
   * @param string $entity_bundle
   *   The entity bundle.
   *
   * @return bool|object
   *   The base plugin definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Throws the PluginException.
   */
  private function loadBasePluginDefinition($entity_bundle): ?CloudConfigPluginInterface {
    $plugin = NULL;
    foreach ($this->getDefinitions() ?: [] as $key => $definition) {
      if (isset($definition['base_plugin']) && $definition['entity_bundle'] === $entity_bundle) {
        $plugin = $this->createInstance($key);
        break;
      }
    }
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function loadCredentials(): ?array {
    return $this->plugin->loadCredentials($this->cloudContext);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceCollectionTemplateName(): string {
    return $this->plugin->getInstanceCollectionTemplateName();
  }

  /**
   * {@inheritdoc}
   */
  public function getPricingPageRoute(): string {
    return $this->plugin->getPricingPageRoute();
  }

  /**
   * {@inheritdoc}
   */
  public function getLaunchTemplateCollectionName(): string {
    return 'entity.cloud_launch_template.collection';
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectCollectionName(): string {
    return 'entity.cloud_project.collection';
  }

  /**
   * {@inheritdoc}
   */
  public function getStoreCollectionName(): string {
    return 'entity.cloud_store.collection';
  }

  /**
   * {@inheritdoc}
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface {
    $this->setCloudContext($cloud_context);
    return $this->plugin->access($cloud_context, $account, $route);
  }

  /**
   * {@inheritdoc}
   */
  public function getCloudConfigs($default_text, $entity_type): array {
    $cloud_configs = ['' => $default_text];
    $configs = $this->loadConfigEntities($entity_type);
    foreach ($configs ?: [] as $config) {
      $cloud_configs[$config->getCloudContext()] = $config->getName();
    }
    return $cloud_configs;
  }

}
