<?php

namespace Drupal\cloud\Plugin\cloud\config;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Entity\CloudConfigInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines an interface for cloud_config_plugin managers.
 */
interface CloudConfigPluginManagerInterface extends PluginManagerInterface {

  /**
   * Set cloud_context.
   *
   * @param string $cloud_context
   *   The cloud context.
   */
  public function setCloudContext($cloud_context): void;

  /**
   * Load all configuration entities for a given bundle.
   *
   * @param string $entity_bundle
   *   The bundle to load.
   *
   * @return mixed
   *   An array of cloud_config entities.
   */
  public function loadConfigEntities($entity_bundle): iterable;

  /**
   * Load a plugin using the cloud_context.
   *
   * @return \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginInterface
   *   loaded The cloud service provider plugin (CloudConfigPlugin).
   */
  public function loadPluginVariant();

  /**
   * Load a config entity.
   *
   * @return \Drupal\cloud\Entity\CloudConfig
   *   The cloud service provider (CloudConfig) entity.
   */
  public function loadConfigEntity(): ?CloudConfigInterface;

  /**
   * Load credentials.
   *
   * @return mixed
   *   Array of credentials
   */
  public function loadCredentials(): ?array;

  /**
   * Load routes for implementing class's instances.
   *
   * @return string
   *   The instance collection template name.
   */
  public function getInstanceCollectionTemplateName(): string;

  /**
   * Load routes for implementing class's instances.
   *
   * @return string
   *   The instance collection template name.
   */
  public function getPricingPageRoute(): string;

  /**
   * Load route for cloud launch templates.
   *
   * @return string
   *   The cloud launch template collection name.
   */
  public function getLaunchTemplateCollectionName(): string;

  /**
   * Check access privilege.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface;

  /**
   * Load the cloud configs as an array for use in a select dropdown.
   *
   * @param string $default_text
   *   The default dropdown option.
   * @param string $entity_type
   *   The entity type to load.
   *
   * @return array
   *   An array of cloud configs.
   */
  public function getCloudConfigs($default_text, $entity_type): array;

}
