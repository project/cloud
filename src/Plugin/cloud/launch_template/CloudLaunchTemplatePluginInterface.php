<?php

namespace Drupal\cloud\Plugin\cloud\launch_template;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Symfony\Component\Routing\Route;

/**
 * Common interfaces for a cloud launch template.
 *
 * @package Drupal\cloud\Plugin
 */
interface CloudLaunchTemplatePluginInterface {

  /**
   * Get the entity bundle defined for a particular plugin.
   *
   * @return string
   *   The entity bundle used to store and interact with a particular cloud
   */
  public function getEntityBundleName(): string;

  /**
   * Method interacts with the implementing cloud's launch functionality.
   *
   * The cloud launch template contains all the information needed for that
   * particular cloud.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template
   *   The cloud launch template entity.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state if launch is called from a form.
   *
   * @return array
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   */
  public function launch(CloudLaunchTemplateInterface $cloud_launch_template, ?FormStateInterface $form_state = NULL): array;

  /**
   * Check access privilege.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface;

  /**
   * Method validates the input Git information.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The cloud launch template entity.
   * @param array $files_arr
   *   Path of the cloned files.
   * @param string $tmp_dir_name
   *   Temporary directory to be created by validation.
   * @param bool $delete_tmp_dir
   *   Whether to remove the temporary directory.
   *
   * @return string
   *   The error message.
   */
  public function validateGit(
    CloudLaunchTemplateInterface $entity,
    array &$files_arr,
    string &$tmp_dir_name,
    bool $delete_tmp_dir = FALSE,
  ): string;

}
