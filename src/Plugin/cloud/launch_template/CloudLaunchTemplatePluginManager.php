<?php

namespace Drupal\cloud\Plugin\cloud\launch_template;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Route;

/**
 * Provides the default cloud_launch_template_plugin manager.
 */
class CloudLaunchTemplatePluginManager extends CloudPluginManager implements CloudLaunchTemplatePluginManagerInterface, ContainerInjectionInterface {

  /**
   * Provides default values for all cloud_launch_template_plugin plugins.
   *
   * @var array
   */
  protected $defaults = [
    'id' => 'cloud_launch_template',
    'entity_type' => 'cloud_launch_template',
  ];

  /**
   * Request service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CloudLaunchTemplatePluginManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   */
  public function __construct(
    EntityTypeManager $entity_type_manager,
    \Traversable $namespaces,
    ModuleHandlerInterface $module_handler,
    CacheBackendInterface $cache_backend,
    RequestStack $request_stack,
  ) {

    parent::__construct('Plugin\cloud\launch_template', $namespaces, $module_handler);

    // Add more services as required.
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->setCacheBackend($cache_backend, 'cloud_launch_template_plugin', ['cloud_launch_template_plugin']);
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The instance of ContainerInterface.
   *
   * @return CloudLaunchTemplatePluginManager
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('entity_type.manager'),
    $container->get('container.namespaces'),
    $container->get('module_handler'),
    $container->get('cache.discovery'),
    $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileNameSuffix(): string {
    return 'cloud.launch.template.plugin';
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id): void {
    parent::processDefinition($definition, $plugin_id);

    if (empty($definition['id'])) {
      throw new PluginException(sprintf('CloudLaunchTemplatePlugin plugin property (%s) definition "is" is required.', $plugin_id));
    }

    if (empty($definition['entity_bundle'])) {
      throw new PluginException(sprintf('entity_bundle property is required for (%s)', $plugin_id));
    }

    if (empty($definition['cloud_context'])) {
      throw new PluginException(sprintf('cloud_context property is required for (%s)', $plugin_id));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadPluginVariant($cloud_context): ?CloudLaunchTemplatePluginInterface {
    $plugin = NULL;
    foreach ($this->getDefinitions() ?: [] as $key => $definition) {
      if ($definition['cloud_context'] === $cloud_context) {
        $plugin = $this->createInstance($key);
        break;
      }
    }

    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function launch(CloudLaunchTemplateInterface $cloud_launch_template, ?FormStateInterface $form_state = NULL): array {
    $plugin = $this->loadPluginVariant($cloud_launch_template->getCloudContext());
    if ($plugin === FALSE) {
      $this->messenger->addStatus($this->t('Cannot load launch template plugin: %cloud_context', [
        '%cloud_context' => $cloud_launch_template->getCloudContext(),
      ]));

      return [
        'route_name' => 'entity.cloud_launch_template.canonical',
        'params' => [
          'cloud_launch_template' => $cloud_launch_template->id(),
          'cloud_context' => $cloud_launch_template->getCloudContext(),
        ],
      ];
    }

    return $plugin->launch($cloud_launch_template, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildListHeader($cloud_context): array {
    if (is_array($cloud_context)) {
      foreach ($cloud_context ?: [] as $cloud_context_value) {
        $plugin = $this->loadPluginVariant($cloud_context_value);
      }
    }
    else {
      $plugin = $this->loadPluginVariant($cloud_context);
    }

    if (empty($plugin)) {
      $this->messenger->addStatus($this->t('Cannot load launch template plugin: %cloud_context', [
        '%cloud_context' => $cloud_context,
      ]));
      return [];
    }

    return $plugin->buildListHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRow(CloudLaunchTemplateInterface $entity): array {
    $plugin = $this->loadPluginVariant($entity->getCloudContext());
    if (empty($plugin)) {
      $this->messenger->addStatus($this->t('Cannot load launch template plugin: %cloud_context', [
        '%cloud_context' => $entity->getCloudContext(),
      ]));
      return [];
    }

    return $plugin->buildListRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function updateCloudLaunchTemplateList($cloud_context = '', $cloud_server_template_type = ''): Response {
    if (empty($cloud_context)) {
      return $this->updateAllCloudLaunchTemplateList($cloud_server_template_type);
    }

    $referer = $this->request->headers->get('referer');

    $plugin = $this->loadPluginVariant($cloud_context);

    if ($plugin === FALSE) {
      $this->messenger->addStatus($this->t('Cannot load launch template plugin: %cloud_context', ['%cloud_context' => $cloud_context]));
    }
    elseif (!method_exists($plugin, 'updateCloudLaunchTemplateList')) {
      $this->messenger->addStatus($this->t('Unnecessary to update launch templates.'));
    }
    else {
      $updated = $plugin->updateCloudLaunchTemplateList($cloud_context);

      if ($updated !== FALSE) {
        $this->messenger->addStatus($this->t('Updated launch templates.'));
      }
      else {
        $this->messenger->addError($this->t('Unable to update launch templates.'));
      }
    }

    if ($plugin && method_exists($plugin, 'postUpdateCloudLaunchTemplateList')) {
      $plugin->postUpdateCloudLaunchTemplateList($cloud_context);
    }

    if (!$this->request->isXmlHttpRequest()) {
      return new RedirectResponse($referer);
    }
    // Store all messages before removing all in the next code.
    $messages = $this->messenger->all();
    // Remove all messages for cleanup before returning JSON object.
    $this->messenger->deleteAll();
    return new JsonResponse([
      'result' => 'OK',
      'messages' => $messages,
    ], 200);
  }

  /**
   * Method validates the input Git information.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The cloud launch template entity.
   * @param array $files_arr
   *   Path of the cloned files.
   * @param string $tmp_dir_name
   *   Temporary directory to be created by validation.
   * @param bool $delete_tmp_dir
   *   Whether to remove the temporary directory.
   *
   * @return string
   *   The error message.
   */
  public function validateGit(
    CloudLaunchTemplateInterface $entity,
    array &$files_arr,
    string &$tmp_dir_name,
    bool $delete_tmp_dir = FALSE,
  ): string {
    $plugin = $this->loadPluginVariant($entity->getCloudContext());
    if ($plugin === FALSE) {
      return $this->t('Cannot load launch template plugin: %cloud_context', [
        '%cloud_context' => $entity->getCloudContext(),
      ]);
    }
    if (!method_exists($plugin, 'validateGit')) {
      return $this->t('CloudLaunchTemplateInterface::validateGit not found: %cloud_context', [
        '%cloud_context' => $entity->getCloudContext(),
      ]);
    }
    return $plugin->validateGit($entity, $files_arr, $tmp_dir_name, $delete_tmp_dir);
  }

  /**
   * Update all cloud launch template list.
   *
   * @param string $cloud_server_template_type
   *   The cloud launch template type.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An array of created object.
   */
  public function updateAllCloudLaunchTemplateList($cloud_server_template_type): Response {

    $referer = $this->request->headers->get('referer');

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => $cloud_server_template_type,
      ]);

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $plugin = $this->loadPluginVariant($cloud_context);

      if ($plugin === FALSE) {
        $this->messenger->addStatus($this->t('Cannot load launch template plugin: %cloud_context', ['%cloud_context' => $cloud_context]));
      }
      elseif (!method_exists($plugin, 'updateAllCloudLaunchTemplateList')) {
        $this->messenger->addStatus($this->t('Unnecessary to update launch templates.'));
      }
      else {
        $updated = $plugin->updateAllCloudLaunchTemplateList($cloud_context);

        if ($updated !== FALSE) {
          $this->messenger->addStatus($this->t('Updated launch templates.'));
        }
        else {
          $this->messenger->addError($this->t('Unable to update launch templates.'));
        }
      }
    }

    if (!$this->request->isXmlHttpRequest()) {
      return new RedirectResponse($referer);
    }
    // Store all messages before removing all in the next code.
    $messages = $this->messenger->all();
    // Remove all messages for cleanup before returning JSON object.
    $this->messenger->deleteAll();
    return new JsonResponse([
      'result' => 'OK',
      'messages' => $messages,
    ], 200);
  }

  /**
   * {@inheritdoc}
   */
  public function buildLaunchForm(CloudLaunchTemplateInterface $cloud_launch_template, array &$form, FormStateInterface $form_state): void {
    $plugin = $this->loadPluginVariant($cloud_launch_template->getCloudContext());
    if ($plugin === FALSE) {
      $this->messenger->addStatus($this->t('Cannot load launch template plugin: %cloud_context', [
        '%cloud_context' => $cloud_launch_template->getCloudContext(),
      ]));
      return;
    }

    if (method_exists($plugin, 'buildLaunchForm')) {
      $plugin->buildLaunchForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface {
    return $this->loadPluginVariant($cloud_context)->access($cloud_context, $account, $route);
  }

}
