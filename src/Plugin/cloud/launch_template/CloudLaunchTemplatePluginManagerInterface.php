<?php

namespace Drupal\cloud\Plugin\cloud\launch_template;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Route;

/**
 * Defines an interface for cloud_launch_template_plugin managers.
 */
interface CloudLaunchTemplatePluginManagerInterface extends PluginManagerInterface {

  /**
   * Load a plugin using the cloud_context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   *
   * @return CloudLaunchTemplatePluginInterface
   *   loaded CloudLaunchTemplatePlugin.
   */
  public function loadPluginVariant($cloud_context): ?CloudLaunchTemplatePluginInterface;

  /**
   * Launch a cloud launch template.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template
   *   The cloud launch template entity.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state if launch is called from a form.
   *
   * @return mixed
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   */
  public function launch(CloudLaunchTemplateInterface $cloud_launch_template, ?FormStateInterface $form_state = NULL): array;

  /**
   * Build the header array for CloudLaunchTemplateListBuilder.
   *
   * @param string $cloud_context
   *   Cloud context.
   *
   * @return array
   *   An array of headers.
   */
  public function buildListHeader($cloud_context): array;

  /**
   * Build the row for CloudLaunchTemplateListBuilder.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   A loaded cloud launch template entity.
   *
   * @return array
   *   An array of values for the row.
   */
  public function buildListRow(CloudLaunchTemplateInterface $entity): array;

  /**
   * Update cloud launch template list.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $cloud_server_template_type
   *   The cloud launch template type.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An array of created object.
   */
  public function updateCloudLaunchTemplateList($cloud_context = '', $cloud_server_template_type = ''): Response;

  /**
   * Build the launch form for K8s.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $cloud_launch_template
   *   The cloud launch template entity.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state if launch is called from a form.
   */
  public function buildLaunchForm(CloudLaunchTemplateInterface $cloud_launch_template, array &$form, FormStateInterface $form_state): void;

  /**
   * Check operation privilege by the API permissions of a cloud service.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(string $cloud_context, AccountInterface $account, Route $route): AccessResultInterface;

  /**
   * Method validates the input Git information.
   *
   * @param \Drupal\cloud\Entity\CloudLaunchTemplateInterface $entity
   *   The cloud launch template entity.
   * @param array $files_arr
   *   Path of the cloned files.
   * @param string $tmp_dir_name
   *   Temporary directory to be created by validation.
   * @param bool $delete_tmp_dir
   *   Whether to remove the temporary directory.
   *
   * @return string
   *   The error message.
   */
  public function validateGit(
    CloudLaunchTemplateInterface $entity,
    array &$files_arr,
    string &$tmp_dir_name,
    bool $delete_tmp_dir = FALSE,
  ): string;

}
