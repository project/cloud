<?php

namespace Drupal\cloud\Plugin\cloud\project;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cloud\Entity\CloudProjectInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the default cloud_project_plugin manager.
 */
class CloudProjectPluginManager extends CloudPluginManager implements CloudProjectPluginManagerInterface, ContainerInjectionInterface {

  /**
   * Provides default values for all cloud_project_plugin plugins.
   *
   * @var array
   */
  protected $defaults = [
    'id' => 'cloud_project',
    'entity_type' => 'cloud_project',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new CloudProjectPluginManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    \Traversable $namespaces,
    ModuleHandlerInterface $module_handler,
    CacheBackendInterface $cache_backend,
    RequestStack $request_stack,
  ) {

    parent::__construct('Plugin\cloud\project', $namespaces, $module_handler);

    // Add more services as required.
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->setCacheBackend($cache_backend, 'cloud_project_plugin', ['cloud_project_plugin']);
    $this->requestStack = $request_stack;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The instance of ContainerInterface.
   *
   * @return CloudProjectPluginManager
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('container.namespaces'),
      $container->get('module_handler'),
      $container->get('cache.discovery'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileNameSuffix(): string {
    return 'cloud.project.plugin';
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id): void {
    parent::processDefinition($definition, $plugin_id);

    if (empty($definition['id'])) {
      throw new PluginException(sprintf('CloudProjectPlugin plugin property (%s) definition "is" is required.', $plugin_id));
    }

    if (empty($definition['entity_bundle'])) {
      throw new PluginException(sprintf('entity_bundle property is required for (%s)', $plugin_id));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadPluginVariant($cloud_context) {
    $plugin = FALSE;
    foreach ($this->getDefinitions() as $key => $definition) {
      if ($definition['cloud_context'] === $cloud_context) {
        $plugin = $this->createInstance($key);
        break;
      }
    }

    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function launch(CloudProjectInterface $cloud_project, ?FormStateInterface $form_state = NULL): array {
    $plugin = $this->loadPluginVariant($cloud_project->getCloudContext());
    if ($plugin === FALSE) {
      $this->messenger->addStatus($this->t('Cannot load cloud project plugin: %cloud_context', [
        '%cloud_context' => $cloud_project->getCloudContext(),
      ]));

      return [
        'route_name' => 'entity.cloud_project.canonical',
        'params' => [
          'cloud_project' => $cloud_project->id(),
          'cloud_context' => $cloud_project->getCloudContext(),
        ],
      ];
    }

    return $plugin->launch($cloud_project, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildListHeader($cloud_context): array {
    if (is_array($cloud_context)) {
      foreach ($cloud_context ?: [] as $cloud_context_value) {
        $plugin = $this->loadPluginVariant($cloud_context_value);
      }
    }
    else {
      $plugin = $this->loadPluginVariant($cloud_context);
    }

    if ($plugin === FALSE) {
      $this->messenger->addStatus($this->t('Cannot load cloud project plugin: %cloud_context', [
        '%cloud_context' => $cloud_context,
      ]));
      return [];
    }

    return $plugin->buildListHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRow(CloudProjectInterface $entity): array {
    $plugin = $this->loadPluginVariant($entity->getCloudContext());
    if ($plugin === FALSE) {
      $this->messenger->addStatus($this->t('Cannot load cloud project plugin: %cloud_context', [
        '%cloud_context' => $entity->getCloudContext(),
      ]));
      return [];
    }

    return $plugin->buildListRow($entity);
  }

  /**
   * Update cloud project list.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param string $cloud_server_template_type
   *   The cloud launch template type.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   An array of created object.
   */
  public function updateCloudProjectList($cloud_context = '', $cloud_server_template_type = ''): RedirectResponse {
    if (empty($cloud_context)) {
      return $this->updateAllCloudProjectList($cloud_server_template_type);
    }

    $referer = $this->requestStack->getCurrentRequest()->headers->get('referer');

    $plugin = $this->loadPluginVariant($cloud_context);

    if ($plugin === FALSE) {
      $this->messenger->addStatus($this->t('Cannot load cloud project plugin: %cloud_context', ['%cloud_context' => $cloud_context]));
    }
    elseif (!method_exists($plugin, 'updateCloudProjectList')) {
      $this->messenger->addStatus($this->t('Unnecessary to update cloud projects.'));
    }
    else {
      $updated = $plugin->updateCloudProjectList($cloud_context);

      if ($updated !== FALSE) {
        $this->messenger->addStatus($this->t('Updated cloud projects.'));
      }
      else {
        $this->messenger->addError($this->t('Unable to update cloud projects.'));
      }
    }
    return new RedirectResponse($referer);
  }

  /**
   * Update all cloud project list.
   *
   * @param string $cloud_server_template_type
   *   The cloud launch template type.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   An array of created object.
   */
  public function updateAllCloudProjectList($cloud_server_template_type): RedirectResponse {

    $referer = $this->requestStack->getCurrentRequest()->headers->get('referer');

    $cloud_configs = $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadByProperties([
        'type' => $cloud_server_template_type,
      ]);

    foreach ($cloud_configs ?: [] as $cloud_config) {
      $cloud_context = $cloud_config->getCloudContext();
      $plugin = $this->loadPluginVariant($cloud_context);

      if ($plugin === FALSE) {
        $this->messenger->addStatus($this->t('Cannot load cloud project plugin: %cloud_context', ['%cloud_context' => $cloud_context]));
      }
      elseif (!method_exists($plugin, 'updateCloudProjectList')) {
        $this->messenger->addStatus($this->t('Unnecessary to update cloud projects.'));
      }
      else {
        $updated = $plugin->updateCloudProjectList($cloud_context);

        if ($updated !== FALSE) {
          $this->messenger->addStatus($this->t('Updated cloud projects.'));
        }
        else {
          $this->messenger->addError($this->t('Unable to update cloud projects.'));
        }
      }
    }
    return new RedirectResponse($referer);
  }

}
