<?php

namespace Drupal\cloud\Plugin\cloud\store;

/**
 * The cloud store (CloudStore) plugin exception.
 */
class CloudStorePluginException extends \Exception {

}
