<?php

namespace Drupal\cloud\Plugin\cloud\store;

use Drupal\cloud\Entity\CloudConfigInterface;

/**
 * Common interfaces for a cloud store.
 *
 * @package Drupal\cloud\Plugin
 */
interface CloudStorePluginInterface {

  /**
   * Load all config entities.
   *
   * @return array
   *   An array of cloud_config entities
   */
  public function loadConfigEntities(): iterable;

  /**
   * Load a single cloud_config entity.
   *
   * @param string $entity_bundle
   *   The entity_bundle to load the entity from.
   *
   * @return mixed
   *   The entity_bundle entity.
   */
  public function loadConfigEntity(string $entity_bundle): ?CloudConfigInterface;

  /**
   * Get the entity bundle defined for a particular plugin.
   *
   * @return string
   *   The entity bundle used to store and interact with a particular cloud
   */
  public function getEntityBundleName(): string;

}
