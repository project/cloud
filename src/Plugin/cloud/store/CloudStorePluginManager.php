<?php

namespace Drupal\cloud\Plugin\cloud\store;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\cloud\Entity\CloudStoreInterface;
use Drupal\cloud\Plugin\cloud\CloudPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the default cloud_store_plugin manager.
 */
class CloudStorePluginManager extends CloudPluginManager implements CloudStorePluginManagerInterface, ContainerInjectionInterface {

  /**
   * Provides default values for all cloud_store_plugin plugins.
   *
   * @var array
   */
  protected $defaults = [
    'id' => 'cloud_store',
    'entity_type' => 'cloud_store',
  ];

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * The cloud store type plugin (CloudStorePluginManagerInterface).
   *
   * @var \Drupal\cloud\Plugin\cloud\store\CloudStorePluginManagerInterface
   */
  private $plugin;

  /**
   * The entity bundle.
   *
   * @var string
   */
  private $entityBundle;

  /**
   * Constructs a new CloudStorePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack object.
   */
  public function __construct(
    \Traversable $namespaces,
    ModuleHandlerInterface $module_handler,
    CacheBackendInterface $cache_backend,
    RequestStack $request_stack,
  ) {

    parent::__construct('Plugin\cloud\store', $namespaces, $module_handler);

    // Add more services as required.
    $this->moduleHandler = $module_handler;
    $this->setCacheBackend($cache_backend, 'cloud_store_plugin', ['cloud_store_plugin']);
    $this->requestStack = $request_stack;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The instance of ContainerInterface.
   *
   * @return CloudStorePluginManager
   *   return created object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('container.namespaces'),
      $container->get('module_handler'),
      $container->get('cache.discovery'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileNameSuffix(): string {
    return 'cloud.store.plugin';
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id): void {
    parent::processDefinition($definition, $plugin_id);

    if (empty($definition['id'])) {
      throw new PluginException(sprintf('CloudStorePlugin plugin property (%s) definition "is" is required.', $plugin_id));
    }

    if (empty($definition['entity_bundle'])) {
      throw new PluginException(sprintf('entity_bundle property is required for (%s)', $plugin_id));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildListHeader($cloud_store_type): array {
    $plugin = $this->loadPluginVariant($cloud_store_type);
    if ($plugin === FALSE) {
      $this->messenger->addStatus($this->t('Cannot load cloud store plugin: %cloud_store_type', [
        '%cloud_store_type' => $cloud_store_type,
      ]));
      return [];
    }
    return $plugin->buildListHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRow(CloudStoreInterface $entity): array {
    $plugin = $this->loadPluginVariant($entity->bundle());
    if ($plugin === FALSE) {
      $this->messenger->addStatus($this->t('Cannot load cloud store plugin: %cloud_store_type', [
        '%cloud_store_type' => $entity->bundle(),
      ]));
      return [];
    }

    return $plugin->buildListRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function loadPluginVariant($cloud_store_type) {
    $plugin = FALSE;
    foreach ($this->getDefinitions() as $key => $definition) {
      if ($definition['entity_bundle'] === $cloud_store_type) {
        $plugin = $this->createInstance($key);
        break;
      }
    }
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function updateCloudStoreList($cloud_store_type): RedirectResponse {
    $referer = $this->requestStack->getCurrentRequest()->headers->get('referer');

    $plugin = $this->loadPluginVariant($cloud_store_type->id());

    if ($plugin === FALSE) {
      $this->messenger->addStatus($this->t('Cannot load cloud store plugin: %cloud_store_type', ['%cloud_store_type' => $cloud_store_type]));
    }
    elseif (!method_exists($plugin, 'updateCloudStoreList')) {
      $this->messenger->addStatus($this->t('Unnecessary to update cloud store.'));
    }
    else {
      $updated = $plugin->updateCloudStoreList($cloud_store_type->id());

      if ($updated !== FALSE) {
        $this->messenger->addStatus($this->t('Updated cloud store.'));
      }
      else {
        $this->messenger->addError($this->t('Unable to update cloud store.'));
      }
    }
    return new RedirectResponse($referer);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\cloud\Plugin\cloud\store\CloudStorePluginException
   */
  public function loadConfigEntity(): ?CloudStoreInterface {
    $config_entity = !empty($this->plugin) ? $this->plugin->loadConfigEntities($this->entityBundle) : NULL;
    if (empty($config_entity)) {
      $message = $this->t(
        'Cannot load cloud store plugin: %bundle (CloudCloudStore::%bundle)', [
          '%bundle' => $this->entityBundle,
        ]
      );
      throw new CloudStorePluginException($message);
    }
    return $config_entity;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\cloud\Plugin\cloud\store\CloudStorePluginException
   */
  public function loadConfigEntities($entity_bundle): ?iterable {
    /** @var \Drupal\cloud\Plugin\cloud\store\CloudStorePluginInterface $plugin */
    $plugin = $this->loadBasePluginDefinition($entity_bundle);
    if (empty($plugin)) {
      // NOTE: According to Drupal coding standards,
      // exceptions should not be translated.
      throw new CloudStorePluginException(sprintf(
        'Cannot load cloud store type (CloudStore) entity for %s',
        $entity_bundle
      ));
    }
    return $plugin->loadConfigEntities();
  }

  /**
   * Helper method to load the base plugin definition.
   *
   * Useful when there is no cloud_context.
   *
   * @param string $entity_bundle
   *   The entity bundle.
   *
   * @return bool|object
   *   The base plugin definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Throws the PluginException.
   */
  private function loadBasePluginDefinition($entity_bundle): CloudStorePluginInterface {
    $plugin = FALSE;
    foreach ($this->getDefinitions() ?: [] as $key => $definition) {
      if (isset($definition['base_plugin']) && $definition['entity_bundle'] === $entity_bundle) {
        $plugin = $this->createInstance($key);
        break;
      }
    }
    return $plugin;
  }

}
