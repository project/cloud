<?php

namespace Drupal\cloud\Plugin\cloud\store;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\cloud\Entity\CloudStoreInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines an interface for cloud_store_plugin managers.
 */
interface CloudStorePluginManagerInterface extends PluginManagerInterface {

  /**
   * Build the header array for CloudStore.
   *
   * @param string $cloud_store_type
   *   Cloud store type.
   *
   * @return array
   *   An array of headers.
   */
  public function buildListHeader($cloud_store_type): array;

  /**
   * Build the row for CloudStoreListBuilder.
   *
   * @param \Drupal\cloud\Entity\CloudStoreInterface $entity
   *   A loaded cloud launch template entity.
   *
   * @return array
   *   An array of values for the row.
   */
  public function buildListRow(CloudStoreInterface $entity): array;

  /**
   * Load a plugin using the cloud_context.
   *
   * @param string $cloud_store_type
   *   Cloud store type (Bundle).
   *
   * @return CloudStorePluginInterface
   *   loaded CloudStorePlugin.
   */
  public function loadPluginVariant($cloud_store_type);

  /**
   * Update cloud store list.
   *
   * @param string $cloud_store_type
   *   Cloud store type (Bundle).
   *
   * @return mixed
   *   An associative array with a redirect route and any parameters to build
   *   the route.
   */
  public function updateCloudStoreList($cloud_store_type): RedirectResponse;

  /**
   * Load all configuration entities for a given bundle.
   *
   * @param string $entity_bundle
   *   The bundle to load.
   *
   * @return mixed
   *   An array of cloud_store entities.
   */
  public function loadConfigEntities($entity_bundle): ?iterable;

  /**
   * Load a config entity.
   *
   * @return \Drupal\cloud\Entity\CloudStore
   *   The cloud service provider (CloudStore) entity.
   */
  public function loadConfigEntity(): ?CloudStoreInterface;

}
