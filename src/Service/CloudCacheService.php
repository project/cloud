<?php

namespace Drupal\cloud\Service;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;

/**
 * Provides Cloud cache service.
 */
class CloudCacheService extends CloudServiceBase implements CloudCacheServiceInterface {

  /**
   * Cache object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cacheBackend;

  /**
   * The error codes list.
   *
   * @var array
   */
  private $errorCodes = [];

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  private $cloudConfigPluginManager;

  /**
   * Constructs a new IamService object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The Cache object.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager.
   */
  public function __construct(
    CacheBackendInterface $cache_backend,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
  ) {

    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    $this->cacheBackend = $cache_backend;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function setErrorCodes(array $error_codes): void {
    $this->errorCodes = $error_codes;
  }

  /**
   * {@inheritdoc}
   */
  public function getErrorCodes(): array {
    return $this->errorCodes;
  }

  /**
   * {@inheritdoc}
   */
  public function setCachedOperationAccess($cloud_context, $operation, $error_code = NULL): void {
    if (empty($cloud_context)) {
      return;
    }
    try {
      $this->cloudConfigPluginManager->setCloudContext($cloud_context);
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    }
    catch (\Exception $e) {
      return;
    }
    $access = empty($error_code) || empty($this->errorCodes) || !in_array($error_code, $this->errorCodes)
      ? AccessResult::allowed()
      : AccessResult::forbidden();
    $this->cacheSet(
      [$cloud_context, $operation],
      $access,
      $cloud_config->getCacheTags()
    );

    $item = $this->cacheGet([
      $cloud_context,
      self::CACHE_KEY_OPERATION_ACCESS,
    ]);
    $data = !empty($item) && !empty($item->data) ? $item->data : [];
    if (in_array($operation, $data)) {
      return;
    }
    $data[] = $operation;
    $this->cacheSet([
      $cloud_context,
      self::CACHE_KEY_OPERATION_ACCESS,
    ], $data, $cloud_config->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function getCachedOperationAccess($cloud_context, $operation): ?object {
    $item = $this->cacheGet([$cloud_context, $operation]);
    if (empty($item) || empty($item->data)) {
      return NULL;
    }
    return $item->data;
  }

  /**
   * {@inheritdoc}
   */
  public function clearOperationAccessCache($cloud_context): void {
    $item = $this->cacheGet([
      $cloud_context,
      self::CACHE_KEY_OPERATION_ACCESS,
    ]);
    if (empty($item) || empty($item->data)) {
      return;
    }

    foreach ($item->data ?: [] as $data) {
      $this->cacheClear([$cloud_context, $data]);
    }

    $this->cacheClear([$cloud_context, self::CACHE_KEY_OPERATION_ACCESS]);
  }

  /**
   * {@inheritdoc}
   */
  public function setLatestCreatedTimeCache($cloud_context, $type, $name, $latest_created): void {
    if (empty($cloud_context)) {
      return;
    }
    try {
      $this->cloudConfigPluginManager->setCloudContext($cloud_context);
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    }
    catch (\Exception $e) {
      return;
    }
    $this->cacheSet([
      $cloud_context,
      $type,
      $name,
      self::CACHE_KEY_LATEST_CREATED,
    ], $latest_created, $cloud_config->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function getLatestCreatedTimeCache($cloud_context, $type, $name): ?int {
    $item = $this->cacheGet([$cloud_context, $type, $name]);
    if (empty($item) || empty($item->data)) {
      return NULL;
    }
    return $item->data;
  }

  /**
   * {@inheritdoc}
   */
  public function clearLatestCreatedTimeCache($cloud_context, $type, $name): void {
    $this->cacheClear([
      $cloud_context,
      $type,
      $name,
      self::CACHE_KEY_LATEST_CREATED,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceDataCache($cloud_context, $type, $name, $sub_name = NULL, array $resource_data = []): void {
    if (empty($cloud_context)) {
      return;
    }
    try {
      $this->cloudConfigPluginManager->setCloudContext($cloud_context);
      $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();
    }
    catch (\Exception $e) {
      return;
    }

    $item = $this->cacheGet([
      $cloud_context,
      $type,
      self::CACHE_KEY_RESOURCE_DATA,
    ]);
    $data = !empty($item) && !empty($item->data) ? $item->data : [];

    if (!empty($sub_name)) {
      if (empty($data[$name])) {
        $data[$name] = [];
      }
      $data[$name][$sub_name] = $resource_data;
    }

    if (empty($sub_name)) {
      $data[$name] = $resource_data;
    }

    $this->cacheSet([
      $cloud_context,
      $type,
      self::CACHE_KEY_RESOURCE_DATA,
    ], $data, $cloud_config->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceDataCache($cloud_context, $type, $name = NULL, $sub_name = NULL): ?array {
    $item = $this->cacheGet([
      $cloud_context,
      $type,
      self::CACHE_KEY_RESOURCE_DATA,
    ]);
    if (empty($item) || empty($item->data)) {
      return NULL;
    }

    $data = $item->data;
    if (empty($name) && empty($sub_name)) {
      return $data;
    }

    if (!empty($sub_name)) {
      if (empty($data[$name]) || empty($data[$name][$sub_name])) {
        return NULL;
      }
      return $data[$name][$sub_name];
    }

    if (empty($data[$name])) {
      return NULL;
    }
    return $data[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteResourceDataCache($cloud_context, $type, $name, $sub_name = NULL): void {
    $item = $this->cacheGet([
      $cloud_context,
      $type,
      self::CACHE_KEY_RESOURCE_DATA,
    ]);
    if (empty($item) || empty($item->data)) {
      return;
    }

    if (empty($item->data[$name])) {
      return;
    }

    $data = $item->data;
    if (!empty($sub_name)) {
      if (empty($data[$name][$sub_name])) {
        return;
      }
      unset($data[$name][$sub_name]);
      if (empty($data[$name])) {
        unset($data[$name]);
      }
    }

    if (empty($sub_name)) {
      if (empty($data[$name])) {
        return;
      }
      unset($data[$name]);
    }

    if (empty($data)) {
      $this->clearResourceDataCache($cloud_context, $type);
      return;
    }

    $this->cacheSet([
      $cloud_context,
      $type,
      self::CACHE_KEY_RESOURCE_DATA,
    ], $data);
  }

  /**
   * {@inheritdoc}
   */
  public function clearResourceDataCache($cloud_context, $type): void {
    $this->cacheClear([
      $cloud_context,
      $type,
      self::CACHE_KEY_RESOURCE_DATA,
    ]);
  }

  /**
   * Sets data to the cache backend.
   *
   * @param array $cache_keys
   *   The keys to generate cache ID.
   * @param mixed $data
   *   The data that will be cached.
   * @param array $tags
   *   The cache tag list.
   */
  protected function cacheSet(array $cache_keys, $data, array $tags = []): void {
    $this->cacheBackend->set(
      $this->prepareCid($cache_keys),
      $data,
      CacheBackendInterface::CACHE_PERMANENT,
      $tags);
  }

  /**
   * Gets data to the cache backend.
   *
   * @param array $cache_keys
   *   The keys to generate cache ID.
   *
   * @return object|false
   *   The cache item or FALSE on failure.
   */
  protected function cacheGet(array $cache_keys): ?object {
    $item = $this->cacheBackend->get($this->prepareCid($cache_keys));
    return !empty($item) ? $item : NULL;
  }

  /**
   * Deletes an item from the cache.
   *
   * @param array $cache_keys
   *   The keys to generate cache ID.
   */
  protected function cacheClear(array $cache_keys): void {
    $this->cacheBackend->delete($this->prepareCid($cache_keys));
  }

  /**
   * Prepares the cache ID.
   *
   * We use md5 to get a hash value since it may be most lightweight.
   *
   * @param array $cache_keys
   *   The keys to generate cache ID.
   *
   * @return string
   *   The prepared cache ID.
   */
  protected function prepareCid(array $cache_keys): string {
    $key = implode(':', $cache_keys);
    return hash('md5', $key);
  }

}
