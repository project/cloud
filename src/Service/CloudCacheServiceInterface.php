<?php

namespace Drupal\cloud\Service;

/**
 * Class CloudCacheService.
 */
interface CloudCacheServiceInterface {

  /**
   * The cache key for the operation access.
   *
   * @var string
   */
  public const CACHE_KEY_OPERATION_ACCESS = 'operation_access';

  /**
   * The cache key for the latest created.
   *
   * @var string
   */
  public const CACHE_KEY_LATEST_CREATED = 'latest_created';

  /**
   * The cache key for resource data.
   *
   * @var string
   */
  public const CACHE_KEY_RESOURCE_DATA = 'resource_data';

  /**
   * Set error codes.
   *
   * @param array $error_codes
   *   The error codes list.
   */
  public function setErrorCodes(array $error_codes): void;

  /**
   * Get error codes.
   *
   * @return array
   *   The error codes list.
   */
  public function getErrorCodes(): array;

  /**
   * Set permission to the cache backend.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param string $operation
   *   The operation.
   * @param string $error_code
   *   The error code.
   */
  public function setCachedOperationAccess($cloud_context, $operation, $error_code = NULL): void;

  /**
   * Get permission to the cache backend.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param string $operation
   *   The operation.
   */
  public function getCachedOperationAccess($cloud_context, $operation): ?object;

  /**
   * Delete cached operation data for the cloud context.
   *
   * @param string $cloud_context
   *   Cloud context string.
   */
  public function clearOperationAccessCache($cloud_context): void;

  /**
   * Set the latest created time to a created resource.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param string $type
   *   The type of resource.
   * @param string $name
   *   The name of data.
   * @param int $latest_created
   *   The latest created time.
   */
  public function setLatestCreatedTimeCache($cloud_context, $type, $name, $latest_created): void;

  /**
   * Get the latest creation time for the data.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param string $type
   *   The type of resource.
   * @param string $name
   *   The name of data.
   */
  public function getLatestCreatedTimeCache($cloud_context, $type, $name): ?int;

  /**
   * Delete the latest cached creation time for the data.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param string $type
   *   The type of resource.
   * @param string $name
   *   The name of data.
   */
  public function clearLatestCreatedTimeCache($cloud_context, $type, $name): void;

  /**
   * Set resource data.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param string $type
   *   The type of resource.
   * @param string $name
   *   The name of data.
   * @param string $sub_name
   *   The second key name of data array.
   * @param array $resource_data
   *   The array of resource data.
   */
  public function setResourceDataCache($cloud_context, $type, $name, $sub_name = NULL, array $resource_data = []): void;

  /**
   * Get resource data.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param string $type
   *   The type of resource.
   * @param string $name
   *   The name of data.
   * @param string $sub_name
   *   The second key name of data array.
   */
  public function getResourceDataCache($cloud_context, $type, $name, $sub_name = NULL): ?array;

  /**
   * Delete resource data.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param string $type
   *   The type of resource.
   * @param string $name
   *   The name of data.
   * @param string $sub_name
   *   The second key name of data array.
   */
  public function deleteResourceDataCache($cloud_context, $type, $name, $sub_name = NULL): void;

  /**
   * Delete resource data.
   *
   * @param string $cloud_context
   *   Cloud context string.
   * @param string $type
   *   The type of resource.
   */
  public function clearResourceDataCache($cloud_context, $type): void;

}
