<?php

namespace Drupal\cloud\Service;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides CloudOrchestratorManagerInterface interface.
 */
interface CloudOrchestratorManagerInterface {

  /**
   * Deploy.
   *
   * @param string $cloud_context
   *   The target cloud service provider.
   * @param string $manifest
   *   The manifest content to deploy orchestrator.
   * @param array $parameters
   *   The parameters.
   *
   * @return array
   *   Cloud resources created.
   */
  public function deploy(string $cloud_context, string $manifest, array $parameters): array;

  /**
   * Delete resources related to the deployment.
   *
   * @param array $entities
   *   The entities related to the deployment.
   */
  public function undeploy(array $entities): void;

  /**
   * Get the endpoint of deployment.
   *
   * @param array $entities
   *   The entities related to the deployment.
   *
   * @return string
   *   The endpoint.
   */
  public function getEndpoint(array $entities): string;

  /**
   * Validate parameters of deployment.
   *
   * @param array $form
   *   The form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $parameters
   *   The parameters of deployment.
   */
  public function validateParameters(array $form, FormStateInterface $form_state, array $parameters): void;

}
