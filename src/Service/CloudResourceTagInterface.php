<?php

namespace Drupal\cloud\Service;

/**
 * Interface CloudResourceTagInterface for creating resource tags.
 */
interface CloudResourceTagInterface {

  /**
   * Mapping from Bundle to Tag name prefix.
   *
   * @var string[]
   */
  public const PREFIX_UID_TAG_NAME = [
    'aws_cloud' => 'drupal-aws_cloud',
    'k8s' => 'drupal-k8s',
    'openstack' => 'drupal-openstack',
  ];

  /**
   * Get the key name of the UID tag.
   *
   * @param string $bundle
   *   The bundle the entity type, such as aws_cloud.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @throws Drupal\cloud\Service\CloudServiceException
   *   Throws CloudServiceException.
   *
   * @return string
   *   The string of tag.
   */
  public function getTagKeyCreatedByUid(
    string $bundle,
    string $cloud_context,
    int $uid = 0,
  ): string;

  /**
   * Get the key name of the UID tag.
   *
   * @param string $bundle
   *   The bundle the entity type, such as aws_cloud.
   * @param string $cloud_context
   *   The cloud context.
   * @param int $uid
   *   The uid.
   *
   * @return string
   *   The string of tag.
   */
  public function getTagValueCreatedByUid(
    string $bundle,
    string $cloud_context,
    int $uid,
  ): string;

}
