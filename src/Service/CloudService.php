<?php

namespace Drupal\cloud\Service;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Utility\Html;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Mail\Plugin\Mail\PhpMail;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueGarbageCollectionInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\Core\Utility\Token;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\cloud\Entity\CloudLaunchTemplateInterface;
use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\FileRepositoryInterface;
use Drupal\geocoder\Entity\GeocoderProvider;
use Drupal\geocoder\ProviderPluginManager;
use Drupal\user\Entity\User;
use Drupal\user\PermissionHandlerInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides Cloud service.
 */
class CloudService extends CloudServiceBase implements CloudServiceInterface, CloudResourceTagInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * Guzzle Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * Twig.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twig;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * The menu cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cachedMenu;

  /**
   * The cache renderer.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRenderer;

  /**
   * The router builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routerBuilder;

  /**
   * The cached discovery clearer.
   *
   * @var \Drupal\Core\Plugin\CachedDiscoveryClearerInterface
   */
  protected $cachedDiscoveryClearer;

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The geocoder provider plugin manager.
   *
   * @var \Drupal\geocoder\ProviderPluginManager
   */
  protected $geocoderProviderPluginManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * User permission service.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * Constructs a new K8sService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entity_definition_update_manager
   *   The entity definition update manager.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The configuration manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config
   *   The typed configuration manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler interface.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request Object.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Template\TwigEnvironment $twig
   *   The Twig handler.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cached_menu
   *   The cached menu.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_renderer
   *   The cache renderer.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $router_builder
   *   The router builder.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $cached_discovery_clearer
   *   The cached discovery clearer.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current account.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route.
   * @param \Drupal\user\PermissionHandlerInterface $permissionHandler
   *   The permission handler.
   * @param \Drupal\Core\Queue\QueueFactory|null $queue_factory
   *   The queue factory service.
   * @param \Drupal\file\FileRepositoryInterface|null $file_repository
   *   The file repository service.
   * @param \Drupal\geocoder\ProviderPluginManager|null $geocoder_provider_plugin_manager
   *   The geocoder provider plugin manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityDefinitionUpdateManagerInterface $entity_definition_update_manager,
    ConfigManagerInterface $config_manager,
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config,
    FileSystemInterface $file_system,
    ExtensionPathResolver $extension_path_resolver,
    ModuleHandlerInterface $module_handler,
    RequestStack $request_stack,
    ClientInterface $http_client,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    TwigEnvironment $twig,
    Token $token,
    MailManagerInterface $mail_manager,
    LanguageManagerInterface $language_manager,
    CacheTagsInvalidatorInterface $cache_tags_invalidator,
    CacheBackendInterface $cached_menu,
    CacheBackendInterface $cache_renderer,
    RouteBuilderInterface $router_builder,
    CachedDiscoveryClearerInterface $cached_discovery_clearer,
    AccountInterface $account,
    RouteMatchInterface $routeMatch,
    PermissionHandlerInterface $permissionHandler,
    ?QueueFactory $queue_factory = NULL,
    ?FileRepositoryInterface $file_repository = NULL,
    ?ProviderPluginManager $geocoder_provider_plugin_manager = NULL,
  ) {
    // The parent constructor takes care of $this->messenger object.
    parent::__construct();

    // Set up the entity type manager for querying entities.
    $this->entityTypeManager = $entity_type_manager;

    // Set up the entity definition update manager.
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;

    // Set  up the config manager.
    $this->configManager = $config_manager;

    // Set up the configuration factory.
    $this->configFactory = $config_factory;

    // Set up the typed config manager.
    $this->typedConfigManager = $typed_config;

    // Set up the file system.
    $this->fileSystem = $file_system;

    // Set up the extension path resolver.
    $this->extensionPathResolver = $extension_path_resolver;

    // Set up the module handler.
    $this->moduleHandler = $module_handler;

    // Set up the request.
    $this->request = $request_stack->getCurrentRequest();

    // Set up the HTTP client.
    $this->httpClient = $http_client;

    // Set up the cloud config plugin.
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;

    // Set up the twig handler.
    $this->twig = $twig;

    // Set up the token service.
    $this->tokenService = $token;

    // Set up the mail manager.
    $this->mailManager = $mail_manager;

    // Set up the language manager.
    $this->languageManager = $language_manager;

    // Set up the cache tags invalidator.
    $this->cacheTagsInvalidator = $cache_tags_invalidator;

    // Set up the cached menu.
    $this->cachedMenu = $cached_menu;

    // Set up the cache renderer.
    $this->cacheRenderer = $cache_renderer;

    // Set up the router builder.
    $this->routerBuilder = $router_builder;

    // Set up the cached discovery clearer.
    $this->cachedDiscoveryClearer = $cached_discovery_clearer;

    // Set up the queue factory service.
    $this->queueFactory = $queue_factory;

    // Set up the file repository service.
    $this->fileRepository = !empty($file_repository)
      ? $file_repository
      // phpcs:ignore
      : \Drupal::service('file.repository'); // @phpstan-ignore-line

    // Set up the geocoder provider plugin manager.
    $this->geocoderProviderPluginManager = $geocoder_provider_plugin_manager;

    // Setup current user.
    $this->account = $account;

    // Setup current route.
    $this->routeMatch = $routeMatch;

    // Setup permission handler.
    $this->permissionHandler = $permissionHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.definition_update_manager'),
      $container->get('config.manager'),
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('file_system'),
      $container->get('extension.path.resolver'),
      $container->get('module_handler'),
      $container->get('request_stack'),
      $container->get('http_client'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('twig'),
      $container->get('token'),
      $container->get('plugin.manager.mail'),
      $container->get('language_manager'),
      $container->get('cache_tags.invalidator'),
      $container->get('cache.menu'),
      $container->get('cache.render'),
      $container->get('router.builder'),
      $container->get('plugin.cache_clearer'),
      $container->get('queue'),
      $container->has('file.repository') ? $container->get('file.repository') : NULL,
      $container->has('plugin.manager.geocoder.provider') ? $container->get('plugin.manager.geocoder.provider') : NULL
    );
  }

  /**
   * Get the version number of the module.
   *
   * @return string
   *   The version signature string.
   */
  public function getVersion(): string {
    return $this->t('Version: @version', ['@version' => self::VERSION]);
  }

  /**
   * Create cloud context according to name.
   *
   * @param string $name
   *   The name of the cloud service provider (CloudConfig) entity.
   *
   * @return string
   *   The cloud context
   */
  public function generateCloudContext($name): string {

    // Convert ' ' or '-' to '_'.
    $cloud_context = preg_replace('/[ \-]/', '_', strtolower($name));

    // Remove special characters.
    $cloud_context = preg_replace('/[^a-z0-9_]/', '', $cloud_context);

    return $cloud_context;
  }

  /**
   * Check if cloud_context exists regardless of the cloud service provider.
   *
   * @param string $name
   *   Name passed from Cloud Config add form.
   *
   * @return bool
   *   TRUE or FALSE if the cloud_context exists.
   */
  public function cloudContextExists($name): bool {
    $exists = FALSE;
    $cloud_context = $this->generateCloudContext($name);
    try {
      $entities = $this->entityTypeManager->getStorage('cloud_config')
        ->loadByProperties([
          'cloud_context' => $cloud_context,
        ]);
      if (!empty($entities)) {
        $exists = TRUE;
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $exists;
  }

  /**
   * Function to reorder form values.
   *
   * @param array $form
   *   Form elements.
   * @param array $fieldset_defs
   *   Field set definitions.
   */
  public function reorderForm(array &$form, array $fieldset_defs): void {
    $weight = 0;
    foreach ($fieldset_defs ?: [] as $fieldset_def) {
      $fieldset_name = $fieldset_def['name'];
      $form[$fieldset_name] = [
        '#type' => 'details',
        '#title' => $fieldset_def['title'],
        '#weight' => $weight++,
        '#open' => $fieldset_def['open'],
        '#group' => array_key_exists('group', $fieldset_def) && !empty($fieldset_def['group'])
          ? $fieldset_def['group'] : '',
      ];

      foreach ($fieldset_def['fields'] ?: [] as $field_name) {
        if (!isset($form[$field_name])) {
          continue;
        }

        $form[$fieldset_name][$field_name] = $form[$field_name];
        $form[$fieldset_name][$field_name]['#weight'] = $weight++;
        unset($form[$field_name]);
      }

      // Support second level fieldset.
      if (isset($fieldset_def['subfieldsets'])) {
        foreach ($fieldset_def['subfieldsets'] ?: [] as $subfieldset_def) {
          $subfieldset_name = $subfieldset_def['name'];
          $form[$fieldset_name][$subfieldset_name] = [
            '#type' => 'details',
            '#title' => $subfieldset_def['title'],
            '#weight' => $weight++,
            '#open' => $subfieldset_def['open'],
          ];
          foreach ($subfieldset_def['fields'] ?: [] as $subfield) {
            if (!isset($form[$subfield])) {
              continue;
            }
            $form[$fieldset_name][$subfieldset_name][$subfield] = $form[$subfield];
            $form[$fieldset_name][$subfieldset_name][$subfield]['#weight'] = $weight++;
            unset($form[$subfield]);
          }

          // Third level field set.
          if (isset($subfieldset_def['subfieldsets'])) {
            foreach ($subfieldset_def['subfieldsets'] ?: [] as $third_fieldset_def) {
              $third_fieldset_name = $third_fieldset_def['name'];
              $form[$fieldset_name][$subfieldset_name][$third_fieldset_name] = [
                '#type' => 'details',
                '#title' => $third_fieldset_def['title'],
                '#weight' => $weight++,
                '#open' => $third_fieldset_def['open'],
              ];
              foreach ($third_fieldset_def['fields'] ?: [] as $third_field) {
                if (!isset($form[$third_field])) {
                  continue;
                }
                $form[$fieldset_name][$subfieldset_name][$third_fieldset_name][$third_field] = $form[$third_field];
                $form[$fieldset_name][$subfieldset_name][$third_fieldset_name][$third_field]['#weight'] = $weight++;
                unset($form[$third_field]);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Helper function to install or update configuration for a set of yml files.
   *
   * @param array $files
   *   An array of yml file names.
   * @param string $module_name
   *   Module where the files are found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function updateYmlDefinitions(array $files, $module_name = 'cloud'): void {
    $config_path = realpath($this->extensionPathResolver->getPath('module', $module_name)) . '/config/install';

    foreach ($files ?: [] as $file) {
      $filename = $config_path . '/' . $file;
      $file = file_get_contents($filename);
      if (!$file) {
        continue;
      }
      $value = Yaml::decode($file);
      $type = $this->configManager->getEntityTypeIdByName(basename($filename));

      $definition = $this->entityTypeManager->getDefinition($type);
      $id_key = $definition->getKey('id');

      $id = $value[$id_key];
      $entity_storage = $this->entityTypeManager->getStorage($type);
      $entity = $entity_storage->load($id);
      if ($entity) {
        $entity = $entity_storage->updateFromStorageRecord($entity, $value);
        $entity->save();
      }
      else {
        $entity = $entity_storage->createFromStorageRecord($value);
        $entity->save();
      }
    }
  }

  /**
   * Update plural and singular labels in entity annotations.
   *
   * @param string $module
   *   The entities the module provides.
   */
  public function updateEntityPluralLabels($module): void {
    $types = $this->entityDefinitionUpdateManager->getEntityTypes();
    /** @var \Drupal\Core\Entity\EntityTypeInterface $type */
    foreach ($types ?: [] as $type) {
      $provider = $type->getProvider();
      if ($provider === $module) {
        $prefix = "{$provider}_";
        $label = $type->getSingularLabel();
        $plural_label = "{$label}s";
        if (preg_match('/(.*)[sS]$/', $label)) {
          $plural_label = "{$label}es";
        }
        elseif (preg_match('/(.*)[yY]$/', $label)) {
          $plural_label = substr($label, 0, -1) . 'ies';
        }
        $type->set('label_collection', ucwords($plural_label));
        $type->set('label_singular', ucwords($label));
        $type->set('label_plural', ucwords($plural_label));
        $type->set('id_plural', $prefix . $plural_label);
        $this->entityDefinitionUpdateManager->updateEntityType($type);
      }
    }
    $this->typedConfigManager->clearCachedDefinitions();
    drupal_flush_all_caches();
  }

  /**
   * Helper function to rename permissions.
   *
   * @param array $permission_map
   *   An array map of ['current_permission_name' => 'new_permission_name'].
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updatePermissions(array $permission_map): void {

    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    foreach ($roles ?: [] as $role) {
      $permissions = $role->getPermissions();
      foreach ($permissions ?: [] as $permission) {
        if (array_key_exists($permission, $permission_map)) {
          $role->revokePermission($permission);
          $role->grantPermission($permission_map[$permission]);
          $role->save();
        }
      }
    }
  }

  /**
   * Delete views by IDs.
   *
   * @param array $ids
   *   An array of view IDs.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteViews(array $ids): void {

    foreach ($ids ?: [] as $id) {
      $view_storage = $this->entityTypeManager->getStorage('view')
        ->load($id);
      if ($view_storage) {
        $view_storage->delete();
      }
    }
  }

  /**
   * Delete actions by IDs.
   *
   * @param array $ids
   *   An array of action IDs.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteActions(array $ids): void {

    foreach ($ids ?: [] as $id) {
      $action_storage = $this->entityTypeManager->getStorage('action')
        ->load($id);
      if ($action_storage) {
        $action_storage->delete();
      }
    }
  }

  /**
   * Helper function to add default icon.
   *
   * @param string $module
   *   The module name.
   */
  public function addDefaultIcon($module): void {

    try {
      // Add default icon.
      $icon = $this->extensionPathResolver->getPath('module', $module) . "/images/{$module}.png";
      $handle = fopen($icon, 'rb');
      $icon_content = fread($handle, filesize($icon));
      fclose($handle);

      $destination = $this->configFactory->get('system.file')->get('default_scheme') . '://images/cloud/icons';
      $this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY);

      // @todo The optional parameter was FileSystemInterface::EXISTS_RENAME.
      // DEPRECATED COMMENT: It is default option, which appends _{incrementing
      // number} until the filename is unique. Now we use
      // FileSystemInterface::EXISTS_REPLACE however consider to revert to
      // FileSystemInterface::EXISTS_RENAME if it doesn't work.
      $file = $this->fileRepository->writeData($icon_content, "{$destination}/{$module}.png", FileExists::Replace);
      if (!empty($file)) {
        $file->setPermanent();
        $file->save();
        $config = $this->configFactory->getEditable("{$module}.settings");
        $config->set("{$module}_cloud_config_icon", $file->id());
        $config->save();
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
  }

  /**
   * Helper function to delete default icon.
   *
   * @param string $module
   *   The module name.
   */
  public function deleteDefaultIcon($module): void {

    try {
      // Delete default icon.
      $config = $this->configFactory->getEditable("{$module}.settings");
      $fid = !empty($config) ? $config->get("{$module}_cloud_config_icon") : NULL;

      // Delete file from disk and from database.
      if (!empty($fid)) {
        $storage = $this->entityTypeManager->getStorage('file');
        $files = !empty($storage) ? $storage->loadMultiple([$fid]) : [];
        $storage->delete($files);
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
  }

  /**
   * Helper function to add fields to the entity type.
   *
   * @param string $entity_type
   *   The entity type, such as cloud_server_template.
   * @param string $bundle
   *   The bundle of the entity type, such as aws_cloud, k8s, openstack or etc.
   * @param mixed $fields
   *   The fields to be added, such as 'field_a' or ['field_a', 'field_b'].
   * @param string $module_name
   *   Optional module name to use for path resolution.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addFields($entity_type, $bundle, $fields, $module_name = ''): void {
    // Allow modules to use this function where the module_name is not the same
    // as the bundle.
    if (empty($module_name)) {
      $module_name = $bundle;
    }
    $config_path = $this->extensionPathResolver->getPath('module', $module_name) . '/config/install';
    $source = new FileStorage($config_path);

    foreach ((array) $fields ?: [] as $field) {
      // Obtain the storage manager for field_storage_config entity type, then
      // create a new field from the yaml configuration and save.
      $this->entityTypeManager->getStorage('field_storage_config')
        ->create($source->read("field.storage.$entity_type.$field"))
        ->save();

      // Obtain the storage manager for field_config entity type, then
      // create a new field from the yaml configuration and save.
      $this->entityTypeManager->getStorage('field_config')
        ->create($source->read("field.field.$entity_type.$bundle.$field"))
        ->save();
    }

    // Reload the default field display and form.
    // Reference https://drupal.stackexchange.com/questions/164713/how-do-i-update-the-configuration-of-a-module
    $files = [
      "core.entity_view_display.$entity_type.$bundle.default.yml",
      "core.entity_form_display.$entity_type.$bundle.default.yml",
    ];
    $this->updateYmlDefinitions($files, $module_name);
  }

  /**
   * Helper function to delete fields from the entity type.
   *
   * @param string $entity_type
   *   The entity type, such as cloud_server_template.
   * @param string $bundle
   *   The bundle of the entity type, such as aws_cloud, k8s, openstack or etc.
   * @param mixed $fields
   *   The names of fields to be added, such as 'field_a',
   *   or ['field_a', 'field_b'].
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteFields($entity_type, $bundle, $fields): void {

    // Delete field in bundles.
    foreach ((array) $fields ?: [] as $field) {
      $field = FieldConfig::loadByName($entity_type, $bundle, $field);
      if (!empty($field)) {
        $field->delete();
      }
    }

    // Delete field storage.
    foreach ((array) $fields ?: [] as $field) {
      $field_storage = FieldStorageConfig::loadByName($entity_type, $field);
      if (!empty($field_storage)) {
        $field_storage->delete();
      }
    }
  }

  /**
   * Helper function to install location-related fields.
   *
   * @param string $bundle
   *   The CloudConfig bundle name (e.g. 'aws_cloud', 'k8s' or 'openstack').
   */
  public function installLocationFields($bundle): void {

    $location_fields = [
      'field_location_country',
      'field_location_city',
      'field_location_latitude',
      'field_location_longitude',
    ];

    $config_path = realpath($this->extensionPathResolver->getPath('module', 'cloud')) . '/config/install';
    $cloud_source = new FileStorage($config_path);

    $config_path = realpath($this->extensionPathResolver->getPath('module', $bundle)) . '/config/install';
    $bundle_source = new FileStorage($config_path);

    try {
      foreach ($location_fields ?: [] as $location_field) {

        $field_storage = FieldStorageConfig::loadByName('cloud_config', $location_field);
        if (!empty($field_storage)) {
          continue;
        }

        // Obtain the storage manager for field_storage_config entity type, then
        // create a new field from the yaml configuration and save.
        $this->entityTypeManager->getStorage('field_storage_config')
          ->create($cloud_source->read("field.storage.cloud_config.{$location_field}"))
          ->save();

        // Obtain the storage manager for field_config entity type, then
        // create a new field from the yaml configuration and save.
        $this->entityTypeManager->getStorage('field_config')
          ->create($bundle_source->read("field.field.cloud_config.{$bundle}.{$location_field}"))
          ->save();
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
  }

  /**
   * Helper function to uninstall location-related fields.
   *
   * @param string $module_name
   *   The module name (e.g. 'aws_cloud', 'k8s' or 'openstack').
   * @param string $bundle
   *   The CloudConfig bundle name (e.g. 'aws_cloud', 'k8s' or 'openstack').
   */
  public function uninstallLocationFields($module_name, $bundle = NULL): void {

    $bundle = $bundle ?? $module_name;

    $fields = [
      "field.field.cloud_config.{$bundle}.field_location_country",
      "field.field.cloud_config.{$bundle}.field_location_city",
      "field.field.cloud_config.{$bundle}.field_location_latitude",
      "field.field.cloud_config.{$bundle}.field_location_longitude",
    ];

    // Delete field in bundles.
    foreach ($fields ?: [] as $field) {
      $this->configFactory->getEditable($field)->delete();
    }
  }

  /**
   * Helper function to uninstall a submodule for cloud service provider.
   *
   * @param string $cloud_config
   *   The CloudConfig bundle name (e.g. 'aws_cloud', 'k8s' or 'openstack').
   */
  public function uninstallServiceProvider($cloud_config): void {

    try {
      // Delete all cloud_config entities.
      $entities = $this->entityTypeManager
        ->getStorage('cloud_config')
        ->loadByProperties([
          'type' => $cloud_config,
        ]);

      if (!empty($entities)) {
        $this->entityTypeManager->getStorage('cloud_config')->delete($entities);
      }

      // Delete the entity type.
      $entity_type = $this->entityTypeManager->getStorage('cloud_config_type')
        ->load($cloud_config);

      if (!empty($entity_type)) {
        $entity_type->delete();
      }
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }

    // Rebuild caches.
    drupal_flush_all_caches();
  }

  /**
   * Initialize the geocoder provider.
   */
  public function initGeocoder(): void {
    if (!$this->moduleHandler->moduleExists('geocoder')) {
      return;
    }
    if (empty($this->geocoderProviderPluginManager)) {
      // Do not use Dependency Injection during module installation.
      // phpcs:ignore
      $plugins = \Drupal::service('plugin.manager.geocoder.provider')->getDefinitions(); // @phpstan-ignore-line
    }
    else {
      $plugins = $this->geocoderProviderPluginManager->getDefinitions();
    }

    if (empty($plugins)) {
      return;
    }

    $config = $this->configFactory->getEditable('cloud.settings');
    $provider_id = $config->get('cloud_location_geocoder_plugin');

    if (empty($provider_id) || !isset($plugins[$provider_id])) {
      $config_path = realpath($this->extensionPathResolver->getPath('module', 'cloud')) . '/config/install';
      $filename = $config_path . '/cloud.settings.yml';
      $file = file_get_contents($filename);
      if ($file) {
        $values = Yaml::decode($file);
        if (!empty($values) && is_array($values) && isset($values['cloud_location_geocoder_plugin']) && isset($plugins[$values['cloud_location_geocoder_plugin']])) {
          $provider_id = $values['cloud_location_geocoder_plugin'];
          $config->set('cloud_location_geocoder_plugin', $provider_id);
          $config->save();
        }
        else {
          return;
        }
      }
    }
    $storage = $this->entityTypeManager->getStorage('geocoder_provider');
    $entity = $storage->load($provider_id);
    if (!empty($entity)) {
      return;
    }
    $entity = GeocoderProvider::create([
      'id' => $provider_id,
      'label' => $plugins[$provider_id]['name'],
      'plugin' => $provider_id,
    ]);

    $geocoder_config = $this->configFactory->getEditable('geocoder.settings');
    $configuration = $geocoder_config->get('plugins_options') ? $geocoder_config->get('plugins_options')[$provider_id] : [];
    $host = $this->request->getHost();
    $configuration['userAgent'] = $configuration['referer'] = $host;

    $entity->set('configuration', $configuration);
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function isGeocoderAvailable(): bool {
    $available = FALSE;

    if ($this->moduleHandler->moduleExists('geocoder')) {
      if (empty($this->geocoderProviderPluginManager)) {
        // Do not use Dependency Injection during module installation.
        // phpcs:ignore
        $plugins = \Drupal::service('plugin.manager.geocoder.provider')->getDefinitions(); // @phpstan-ignore-line
      }
      else {
        $plugins = $this->geocoderProviderPluginManager->getDefinitions();
      }

      $config = $this->configFactory->getEditable('cloud.settings');
      $provider_id = $config->get('cloud_location_geocoder_plugin');

      if (!empty($provider_id) || isset($plugins[$provider_id])) {
        $root_url = !empty($plugins[$provider_id]['arguments']['rootUrl']) ? $plugins[$provider_id]['arguments']['rootUrl'] : '';
        if (!empty($root_url)) {
          // Test using Drupal client.
          try {
            $results = $this->httpClient
              ->head($root_url, [
                'connect_timeout' => 1,
              ]);
            if ($results->getStatusCode() === 200) {
              $available = TRUE;
            }
          }
          catch (\Exception $e) {
            // No internet connection.
            $available = FALSE;
          }
        }
      }
    }
    return $available;
  }

  /**
   * Format memory string.
   *
   * @param float $number
   *   The number of memory size.
   *
   * @return string
   *   The formatted memory string.
   */
  public static function formatMemory($number): string {
    $binary_units = [
      'Ki',
      'Mi',
      'Gi',
      'Ti',
      'Pi',
      'Ei',
    ];

    $exp = 1;
    $memory_unit = '';
    $memory_number = sprintf('%.2f', $number);
    foreach ($binary_units ?: [] as $unit) {
      $base_number = pow(1024, $exp++);
      if ($number < $base_number) {
        break;
      }

      $memory_number = sprintf('%.2f', $number / $base_number);
      $memory_unit = $unit;
    }

    return $memory_number . $memory_unit;
  }

  /**
   * Prepare the mail message for a given entity type.
   *
   * @param array $entities
   *   An array of entities.
   * @param string $entity_message
   *   The entity message to be transformed by tokens.
   * @param string $entity_token_name
   *   The entity token name used in token definition.
   * @param string $mail_message
   *   The email message.
   * @param string $mail_token_name
   *   The mail token name used in token definition.
   *
   * @return string
   *   A fully transformed email message.
   */
  public function getEntityNotificationMessage(array $entities, $entity_message, $entity_token_name, $mail_message, $mail_token_name): string {
    $message = '';
    $entity_messages = [];

    foreach ($entities ?: [] as $entity) {
      $entity_messages[] = $this->tokenService->replace(
        $entity_message,
        [
          $entity_token_name => $entity,
        ]
      );
    }
    if (count($entity_messages)) {
      $message = $this->tokenService->replace(
        $mail_message,
        [
          $mail_token_name => implode("\n\n", $entity_messages),
        ]
      );
    }
    return $message;
  }

  /**
   * Notifies launch template workflow status changed.
   *
   * @param string $module_name
   *   The module name.
   * @param object $server_template
   *   The cloud server template entity.
   * @param string $before_status
   *   The workflow status before change.
   */
  public function notifyLaunchTemplateWorkflowStatusChanged($module_name, $server_template, $before_status): void {
    if (empty($server_template)
    || empty($server_template->get('field_workflow_status'))) {
      return;
    }
    $config = $this->configFactory->get($module_name . '.settings');

    $account = $server_template->getOwner();
    $owner_email = '';
    if ($account !== NULL && !empty($account->getEmail())) {
      $owner_email = $account->getEmail();
    }
    $request_emails = $config->get($module_name . '_launch_template_notification_request_emails');
    $after_status = $server_template->get('field_workflow_status')->value;

    if ($before_status !== CloudLaunchTemplateInterface::REVIEW
    && $after_status === CloudLaunchTemplateInterface::REVIEW
    && !empty($request_emails)) {
      // Notification of review to IT Admin.
      $this->notifyLaunchTemplateRequest(
        $module_name,
        $server_template,
        $module_name . '_launch_template_notification_request_subject',
        $module_name . '_launch_template_notification_request_msg',
        $request_emails);
    }
    elseif ($before_status === CloudLaunchTemplateInterface::REVIEW
    && $after_status === CloudLaunchTemplateInterface::APPROVED
    && !empty($owner_email)) {
      // Notification of approved to Owner.
      $this->notifyLaunchTemplate(
        $module_name,
        $server_template,
        $module_name . '_launch_template_notification_approved_subject',
        $module_name . '_launch_template_notification_approved_msg',
        $owner_email);
    }
    elseif ($before_status === CloudLaunchTemplateInterface::REVIEW
    && $after_status === CloudLaunchTemplateInterface::DRAFT
    && !empty($owner_email)) {
      // Notification of restore to draft to Owner.
      $this->notifyLaunchTemplate(
        $module_name,
        $server_template,
        $module_name . '_launch_template_notification_restore_subject',
        $module_name . '_launch_template_notification_restore_msg',
        $owner_email);
    }

  }

  /**
   * Notify launch template owner for approved launch template.
   *
   * @param string $module_name
   *   The module name.
   * @param object $server_template
   *   The server_template.
   * @param string $subject
   *   String of the Subject defined in $module_name.settings.
   * @param string $message
   *   String of the Subject defined in $module_name.settings.
   * @param string $emails
   *   The list of emails.
   */
  public function notifyLaunchTemplateRequest($module_name, $server_template, $subject, $message, $emails): void {
    $config = $this->configFactory->get($module_name . '.settings');

    $server_templates[] = $server_template;
    $cloud_template_message = $this->getEntityNotificationMessage(
      $server_templates,
      $config->get($module_name . '_launch_template_notification_launch_template_request_info'),
      $module_name . '_launch_template_request',
      $config->get($message),
      'launch_templates_request'
    );

    $cloud_template_message = Html::decodeEntities($cloud_template_message);
    $approve_url = $server_template->toUrl('approve', ['absolute' => TRUE])->toString();
    $button_html = $this->getHtmlEmailButton($approve_url, 'Approve This Template');
    $cloud_template_message = str_replace($approve_url, $button_html, $cloud_template_message);

    $this->sendNotificationHtmlEmail(
      $config->get($subject),
      $cloud_template_message,
      $emails,
      'launch_templates_request'
    );
  }

  /**
   * Notify launch template owner for approved launch template.
   *
   * @param string $module_name
   *   The module name.
   * @param string $server_template
   *   The server_template.
   * @param string $subject
   *   String of the Subject defined in $module_name.settings.
   * @param string $message
   *   String of the Subject defined in $module_name.settings.
   * @param string $emails
   *   The list of emails.
   */
  public function notifyLaunchTemplate($module_name, $server_template, $subject, $message, $emails): void {
    $config = $this->configFactory->get($module_name . '.settings');

    $server_templates[] = $server_template;
    $cloud_template_message = $this->getEntityNotificationMessage(
      $server_templates,
      $config->get($module_name . '_launch_template_notification_launch_template_info'),
      $module_name . '_launch_template',
      $config->get($message),
      'launch_templates'
    );

    $this->sendNotificationEmail(
      $config->get($subject),
      $cloud_template_message,
      $emails,
      'notify_entity_admins_users',
      'launch_templates'
    );

  }

  /**
   * Send email to users. Log the email transaction in Drupal logger.
   *
   * @param string $subject
   *   The message subject.
   * @param string $message
   *   The email message.
   * @param string $emails
   *   The list of emails.
   * @param string $mail_hook
   *   The mail hook.
   * @param string $message_type
   *   The message type.
   */
  public function sendNotificationEmail($subject, $message, $emails, $mail_hook, $message_type): void {
    $result = $this->mailManager->mail(
      'cloud',
      $mail_hook,
      $emails,
      $this->languageManager->getDefaultLanguage(),
      [
        'message' => $message,
        'subject' => $subject,
      ]
    );
    if (isset($result['result'])) {
      $this->logger('cloud')->info($this->t('An @type notification has been sent to @email for @type with the subject @subject.', [
        '@type' => $message_type,
        '@email' => $emails,
        '@subject' => $subject,
      ]));
    }
  }

  /**
   * Send html email to users. Log the email transaction in Drupal logger.
   *
   * @param string $subject
   *   The message subject.
   * @param string $message
   *   The email message.
   * @param string $emails
   *   The list of emails.
   * @param string $message_type
   *   The message type.
   */
  public function sendNotificationHtmlEmail($subject, $message, $emails, $message_type): void {
    $send_mail = new PhpMail();
    $send_message['headers'] = [
      'content-type' => 'text/html',
      'MIME-Version' => '1.0',
    ];
    $send_message['to'] = $emails;
    $send_message['subject'] = $subject;
    $send_message['body'] = str_replace("\n", '<br />', $message);

    $send_mail->mail($send_message);

    $this->logger('cloud')->info($this->t('An @type notification has been sent to @email for @type with the subject @subject.', [
      '@type' => $message_type,
      '@email' => $emails,
      '@subject' => $subject,
    ]));
  }

  /**
   * Create an HTML of a button for email.
   *
   * @param string $url
   *   The url to go to when you click on the button.
   * @param string $text
   *   The text in the button.
   *
   * @return string
   *   The HTML text of the button.
   */
  public function getHtmlEmailButton($url, $text): string {
    $html = '<div style="display: inline-block; _display: inline;">';
    $html .= '<!--[if mso]>';
    $html .= '<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"';
    $html .= 'xmlns:w="urn:schemas-microsoft-com:office:word"';
    $html .= ' href="' . $url . '" ';
    $html .= 'style="height:50px;v-text-anchor:middle;width:200px;"';
    $html .= 'arcsize="8%" stroke="f" fillcolor="#326de6">';
    $html .= '<w:anchorlock/>';
    $html .= '<center>';
    $html .= '<![endif]-->';
    $html .= '<a href="' . $url . '" ';
    $html .= 'style="background-color:#326de6;border-radius:4px;padding:10px;';
    $html .= 'color:#ffffff;display:inline-block;font-family:sans-serif;';
    $html .= 'font-size:13px;font-weight:bold;line-height:px;text-align:center;';
    $html .= 'text-decoration:none;width:px;-webkit-text-size-adjust:none;">';
    $html .= $text;
    $html .= '</a>';
    $html .= '<!--[if mso]>';
    $html .= '</center>';
    $html .= '</v:roundrect>';
    $html .= '<![endif]--></div>';

    return $html;
  }

  /**
   * Get a file descriptor of a default icon for cloud service provider.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The CloudConfig (cloud service provider) entity.
   * @param string $module
   *   The module name.
   *
   * @return int
   *   The file ID (int) or NULL if not found.
   */
  public function getDefaultCloudConfigIcon(EntityInterface $entity, $module): ?int {
    return $entity->bundle() === $module
      ? $this->configFactory->get("{$module}.settings")->get("{$module}_cloud_config_icon")
      : NULL;
  }

  /**
   * Update the configuration of view.
   *
   * @param string $view_name
   *   The name of view.
   * @param array $options
   *   The key and value array of view configuration.
   */
  public function updateViewsConfiguration($view_name, array $options): void {
    $view = $this->configFactory->getEditable($view_name);

    foreach ($options ?: [] as $key => $value) {
      $view->set($key, $value);
    }
    $view->save(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getTagKeyCreatedByUid($bundle, $cloud_context, $uid = 0): string {
    $tag_format = '{module_name}.{hash_value}';
    $hash_value_format = '{cloud_context}.{site_uuid}';

    $module_name = '';
    foreach (CloudResourceTagInterface::PREFIX_UID_TAG_NAME ?: [] as $key => $value) {
      $module_name = $bundle === $key ? $value : $module_name;
    }

    $site_uuid = $this->configFactory->get('system.site')->get('uuid');
    $context = [
      '{cloud_context}' => $cloud_context,
      '{site_uuid}' => $site_uuid,
    ];
    $hash_value = md5(strtr($hash_value_format, $context));

    $context = [
      '{module_name}' => $module_name,
      '{hash_value}' => $hash_value,
    ];
    return strtr($tag_format, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function getTagValueCreatedByUid(
    string $bundle,
    string $cloud_context,
    int $uid,
  ): string {
    $tag_format = '{tag}.{uid}';

    $context = [
      '{tag}' => $this->getTagKeyCreatedByUid($bundle, $cloud_context),
      '{uid}' => $uid,
    ];

    return strtr($tag_format, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function clearAllCacheValues(array $tags = []): void {
    if (!empty($tags)) {
      $this->cacheTagsInvalidator->invalidateTags($tags);
    }
    $this->cachedMenu->invalidateAll();
    $this->cacheRenderer->deleteAll();
    $this->routerBuilder->rebuild();
    $this->cachedDiscoveryClearer->clearCachedDefinitions();
  }

  /**
   * Helper method to load an entity using parameters.
   *
   * @param string $cloud_context
   *   Cloud context.
   * @param string $entity_type
   *   Entity Type.
   * @param string $id_field
   *   Entity ID field.
   * @param string|null $id_value
   *   Entity ID value.
   * @param array $extra_conditions
   *   Extra conditions.
   *
   * @return int|null
   *   Entity ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEntityId(string $cloud_context, string $entity_type, string $id_field, ?string $id_value, array $extra_conditions = []): ?int {
    $query = $this->entityTypeManager
      ->getStorage($entity_type)
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition($id_field, $id_value)
      ->condition('cloud_context', $cloud_context);

    foreach ($extra_conditions ?: [] as $key => $value) {
      $query->condition($key, $value);
    }

    $entities = $query->execute();
    return array_shift($entities);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateCacheTags(array $tags = []): void {
    $tags += [
      'rendered',
      'block_view',
    ];
    $this->cacheTagsInvalidator->invalidateTags($tags);
  }

  /**
   * {@inheritdoc}
   */
  public function renderTemplate(string $file_contents, array $vars): string {
    $result = $this->twig->renderInline($file_contents, $vars);
    return (empty($result) || is_string($result)) ? $result : $result->__toString();
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllEntities(string $cloud_context, string $entity_type, array $conditions = []): array {
    $conditions['cloud_context'] = $cloud_context;
    try {
      return $this->entityTypeManager
        ->getStorage($entity_type)
        ->loadByProperties($conditions);
    }
    catch (InvalidPluginDefinitionException
    | PluginNotFoundException $e) {
      $this->handleException($e);
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function clearAllEntities(string $module, string $cloud_context): void {
    $timestamp = $this->getTimestamp();
    $entity_types = $this->entityTypeManager->getDefinitions();
    foreach ($entity_types as $entity_name => $entity_type) {
      if ($entity_type->getProvider() === $module) {
        try {
          $this->clearEntities($cloud_context, $entity_name, $timestamp);
        }
        catch (InvalidPluginDefinitionException
          | PluginNotFoundException
          | EntityStorageException $e) {
          $this->handleException($e);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function clearEntities(string $cloud_context, string $entity_type, int $timestamp): void {
    $entity_storage = $this->entityTypeManager->getStorage($entity_type);
    $entity_ids = $entity_storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('refreshed', $timestamp, '<')
      ->condition('cloud_context', $cloud_context)
      ->execute();
    if (count($entity_ids)) {
      $entities = $entity_storage->loadMultiple($entity_ids);
      try {
        $entity_storage->delete($entities);
      }
      catch (\Exception $e) {
        $this->handleException($e);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceQueueName(CloudContentEntityBase $entity): string {
    return $entity->bundle() . '_update_resources_queue:' . $entity->getCloudContext();
  }

  /**
   * {@inheritdoc}
   */
  public function createQueue(string $queue_name): QueueInterface {
    return $this->queueFactory->get($queue_name);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteQueue(string $queue_name): void {
    $queue = $this->queueFactory->get($queue_name);
    if ($queue instanceof QueueGarbageCollectionInterface) {
      $queue->deleteQueue();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildOwnerQueryCondition(AlterableInterface $query): void {
    if (!$account = $query->getMetaData('account')) {
      $account = $this->account;
    }

    $route_name = $this->routeMatch->getRouteName();

    // Regex extracts entity_type from view.*.list and view.*.all.
    if (preg_match('@^view.(.*).(list|all)$@', $route_name, $matches) !== FALSE && !empty($matches)) {
      $entity_type = $matches[1];

      // Use a static trait method through CloudService.
      $entity_type_name = CloudService::convertUnderscoreToWhitespace($entity_type);

      // Check if the permission exists.
      $permission_name = "view any $entity_type_name";
      $permissions = $this->permissionHandler->getPermissions();
      if (empty($permissions[$permission_name])) {
        return;
      }

      if ($account->hasPermission($permission_name)) {
        return;
      }

      // Add uid condition.
      $query->condition("$entity_type.uid", $account->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderEntityTypes(string $provider): array {
    $entity_types = $this->entityDefinitionUpdateManager
      ->getEntityTypes();
    foreach ($entity_types ?: [] as $key => $entity) {
      if (!empty($entity)
        && str_starts_with($entity->id(), $provider) === FALSE) {
        unset($entity_types[$key]);
      }
    }
    return $entity_types;
  }

  /**
   * {@inheritdoc}
   */
  public function reassignUids(array $ids, array $update, string $entity_type, bool $revision, User $account, array $operations = [], ?callable $custom_callback = NULL): void {
    // Exit the method if both $ids and $operations are empty.  There
    // can be cases where $ids is empty, but a module wants to do
    // additional processing and has passed additional $operations.
    if (empty($ids) && empty($operations)) {
      return;
    }

    $batch_builder = new BatchBuilder();

    if (!empty($ids)) {
      // If a custom callback is set, use that instead of the
      // default CloudService::batchProcessEntities.  Give
      // implementing modules a chance to do custom processing.
      $batch_builder
        ->addOperation($custom_callback ?? [
          self::class,
          'batchProcessEntities',
        ], [$ids, $update, $entity_type, $revision, $account]);
    }

    // Add additional operations if passed into this method.  Let
    // implementing modules perform additional processing on the entity ids.
    foreach ($operations ?: [] as $operation) {
      $batch_builder->addOperation(
        $operation, [$ids, $update, $entity_type, $revision, $account]);
    }

    $batch_builder
      ->setFinishCallback([
        self::class,
        'completeBatchProcess',
      ])
      ->setTitle($this->t('Processing'))
      ->setErrorMessage($this->t('Error occurred.'))
      ->setProgressMessage('');
    batch_set($batch_builder->toArray());
  }

  /**
   * Performs batch processing on a set of entities.
   *
   * This method is static so the Batch api can find it.
   *
   * @param array $entities
   *   Array of entity ids.
   * @param array $updates
   *   Array of key/values to update.
   * @param string $entity_type
   *   The entity type.
   * @param bool $revision
   *   TRUE if the entities support revisions.
   * @param Drupal\user\Entity\User $account
   *   User entity.
   * @param array|\ArrayAccess $context
   *   An array of contextual key/values.
   */
  public static function batchProcessEntities(array $entities, array $updates, string $entity_type, bool $revision, User $account, &$context): void {
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($entities);
      $context['sandbox']['entities'] = $entities;
    }

    try {

      /** @var \Drupal\Core\Entity\RevisionableStorageInterface $storage */
      $storage = \Drupal::entityTypeManager()->getStorage($entity_type);

      $count = min(5, count($context['sandbox']['entities']));
      for ($i = 1; $i <= $count; $i++) {
        $entity = array_shift($context['sandbox']['entities']);
        $entity = $revision ? $storage->loadRevision($entity) : $storage->load($entity);

        // Do the actual entity update.
        self::updateEntityValues($entity, $updates);

        // Update our progress information.
        $context['sandbox']['progress']++;
      }
    }
    catch (\Exception $e) {
      \Drupal::service('cloud')->handleException($e);
      // Set progress value to max so batch processing completes.
      $context['sandbox']['progress'] = $context['sandbox']['max'];
    }

    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] !== $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Implements callback_batch_finished().
   *
   * Reports the 'finished' status of batch operation for
   * batchProcessEntities().
   *
   * @param bool $success
   *   A boolean indicating whether the batch mass update operation successfully
   *   concluded.
   * @param array $results
   *   An array of rendered links to nodes updated via the batch mode process.
   * @param array $operations
   *   An array of function calls (not used in this function).
   *
   * @see batchProcessEntities()
   */
  public static function completeBatchProcess(bool $success, array $results, array $operations): void {
    if ($success) {
      \Drupal::messenger()->addStatus(\Drupal::translation()->translate('The update has been performed.'));
    }
    else {
      \Drupal::messenger()->addError(\Drupal::translation()->translate('An error occurred and processing did not complete.'));
    }
  }

  /**
   * Static helper to update entity values.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to save.
   * @param array $updates
   *   Array of key/values to update.
   */
  public static function updateEntityValues(EntityInterface $entity, array $updates): void {
    try {
      $entity->original = clone $entity;
      foreach ($updates ?: [] as $name => $value) {
        $entity->$name = $value;
      }
      $entity->save();
    }
    catch (\Exception $e) {
      \Drupal::service('cloud')->handleException($e);
    }
  }

}
