<?php

namespace Drupal\cloud\Service;

use Drupal\cloud\Traits\CloudContentEntityTrait;

/**
 * Provides a base class for Cloud plugin.
 */
class CloudServiceBase implements CloudServiceBaseInterface {

  use CloudContentEntityTrait;

  /**
   * Set an API timeout to speed up API return.
   *
   * @var int
   */
  protected $connectTimeout = 2;

  /**
   * Cloud context string.
   *
   * @var string
   */
  protected $cloudContext;

  /**
   * The CloudService constructor.
   */
  public function __construct() {

    $this->messenger();
  }

  /**
   * Generate a lock key based on entity name.
   *
   * @param string $name
   *   The entity name.
   *
   * @return string
   *   The lock key.
   */
  protected function getLockKey($name): string {
    return $this->cloudContext . '_' . $name;
  }

}
