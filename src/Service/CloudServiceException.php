<?php

namespace Drupal\cloud\Service;

/**
 * The cloud service (CloudService) exception.
 */
class CloudServiceException extends \Exception {

}
