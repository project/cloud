<?php

namespace Drupal\cloud\Service;

use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;
use Drupal\user\Entity\User;

/**
 * Class CloudService.
 */
interface CloudServiceInterface {

  /**
   * The version signature string.
   */
  public const VERSION = '8.x-dev';

  /**
   * Get the version number of the module.
   *
   * @return string
   *   The version signature string.
   */
  public function getVersion(): string;

  /**
   * Create cloud context according to name.
   *
   * @param string $name
   *   The name of the cloud service provider (CloudConfig) entity.
   *
   * @return string
   *   The cloud context
   */
  public function generateCloudContext($name): string;

  /**
   * Check if cloud_context exists regardless of the cloud service provider.
   *
   * @param string $name
   *   Name passed from Cloud Config add form.
   *
   * @return bool
   *   TRUE or FALSE if the cloud_context exists.
   */
  public function cloudContextExists($name): bool;

  /**
   * Function to reorder form values.
   *
   * @param array $form
   *   Form elements.
   * @param array $fieldset_defs
   *   Field set definitions.
   */
  public function reorderForm(array &$form, array $fieldset_defs): void;

  /**
   * Helper function to install or update configuration for a set of yml files.
   *
   * @param array $files
   *   An array of yml file names.
   * @param string $module_name
   *   Module where the files are found.
   */
  public function updateYmlDefinitions(array $files, $module_name = 'cloud'): void;

  /**
   * Update plural and singular labels in entity annotations.
   *
   * @param string $module
   *   The entities the module provides.
   */
  public function updateEntityPluralLabels($module): void;

  /**
   * Helper function to rename permissions.
   *
   * @param array $permission_map
   *   An array map of ['current_permission_name' => 'new_permission_name'].
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updatePermissions(array $permission_map): void;

  /**
   * Delete views by IDs.
   *
   * @param array $ids
   *   An array of view IDs.
   */
  public function deleteViews(array $ids): void;

  /**
   * Helper function to add default icon.
   *
   * @param string $module
   *   The module name.
   */
  public function addDefaultIcon($module): void;

  /**
   * Helper function to delete default icon.
   *
   * @param string $module
   *   The module name.
   */
  public function deleteDefaultIcon($module): void;

  /**
   * Helper function to add fields to the entity type.
   *
   * @param string $entity_type
   *   The entity type, such as cloud_server_template.
   * @param string $bundle
   *   The bundle the entity type, such as aws_cloud.
   * @param mixed $fields
   *   The fields to be added, such as 'field_a' or ['field_a', 'field_b'].
   * @param string $module_name
   *   Optional module name to use for path resolution.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addFields($entity_type, $bundle, $fields, $module_name = ''): void;

  /**
   * Helper function to delete fields from the entity type.
   *
   * @param string $entity_type
   *   The entity type, such as cloud_server_template.
   * @param string $bundle
   *   The bundle the entity type, such as aws_cloud.
   * @param mixed $field_names
   *   The names of fields to be added, such as 'field_a',
   *   or ['field_a', 'field_b'].
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteFields($entity_type, $bundle, $field_names): void;

  /**
   * Helper function to install location-related fields.
   *
   * @param string $bundle
   *   The CloudConfig bundle name (e.g. 'aws_cloud', 'k8s' or 'openstack').
   */
  public function installLocationFields($bundle): void;

  /**
   * Helper function to uninstall location-related fields.
   *
   * @param string $module_name
   *   The module name (e.g. 'aws_cloud', 'k8s' or 'openstack').
   * @param string $bundle
   *   The CloudConfig bundle name (e.g. 'aws_cloud', 'k8s' or 'openstack').
   */
  public function uninstallLocationFields($module_name, $bundle = NULL): void;

  /**
   * Helper function to uninstall a submodule for cloud service provider.
   *
   * @param string $cloud_config
   *   The CloudConfig bundle name (e.g. 'aws_cloud', 'k8s' or 'openstack').
   */
  public function uninstallServiceProvider($cloud_config): void;

  /**
   * Initialize the geocoder provider.
   */
  public function initGeocoder(): void;

  /**
   * Checks if geocoder url is available.
   *
   * This is used in a closed off server that does not have internet
   * access.
   *
   * @return bool
   *   TRUE is available.
   */
  public function isGeocoderAvailable(): bool;

  /**
   * Format memory string.
   *
   * @param float $number
   *   The number of memory size.
   *
   * @return string
   *   The formatted memory string.
   */
  public static function formatMemory($number): string;

  /**
   * Get a file descriptor of a default icon for cloud service provider.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The CloudConfig (cloud service provider) entity.
   * @param string $module
   *   The module name.
   *
   * @return int
   *   The file ID (int) or NULL if not found.
   */
  public function getDefaultCloudConfigIcon(EntityInterface $entity, $module): ?int;

  /**
   * Helper method to update caches.
   *
   * Be careful using this function especially during batch processing
   * or entity imports.  Call this method only when necessary.
   *
   * @param array $tags
   *   Additional array of tags to invalidate.
   */
  public function clearAllCacheValues(array $tags = []): void;

  /**
   * Helper method to update caches.
   *
   * This method has been refactored from a full cache clear to
   * only invalidating the render, block and entity cache tags.
   *
   * This provides faster performance when updating entities.
   *
   * @param array $tags
   *   Additional array of tags to invalidate.
   */
  public function invalidateCacheTags(array $tags = []): void;

  /**
   * Replaces {{ values }} with KVP values.
   *
   * @param string $file_contents
   *   Result of file_get_contents on a Twig template-like file.
   * @param array $vars
   *   The array of KVP's to replace in the file.
   *
   * @return string
   *   The rendered yaml string to be decoded.
   */
  public function renderTemplate(string $file_contents, array $vars): string;

  /**
   * Helper method to load all entities of a given type.
   *
   * @param string $cloud_context
   *   Cloud context.
   * @param string $entity_type
   *   Entity type.
   * @param array $conditions
   *   Query conditions.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of entities.
   */
  public function loadAllEntities(string $cloud_context, string $entity_type, array $conditions = []): array;

  /**
   * Method to clear all entities out of the system.
   *
   * @param string $module
   *   The module's name.
   * @param string $cloud_context
   *   The cloud context.
   */
  public function clearAllEntities(string $module, string $cloud_context): void;

  /**
   * Clear entities.
   *
   * @param string $cloud_context
   *   Cloud context.
   * @param string $entity_type
   *   Entity Type.
   * @param int $timestamp
   *   The timestamp for condition of refreshed time to clear entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function clearEntities(string $cloud_context, string $entity_type, int $timestamp): void;

  /**
   * Helper to generate resource queue name.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   Entity used to build queue name.
   *
   * @return string
   *   Queue resource name.
   */
  public function getResourceQueueName(CloudContentEntityBase $entity): string;

  /**
   * Helper function to create a queue.
   *
   * @param string $queue_name
   *   Queue to create.
   */
  public function createQueue(string $queue_name): QueueInterface;

  /**
   * Helper function to delete a queue.
   *
   * @param string $queue_name
   *   Queue to delete.
   */
  public function deleteQueue(string $queue_name): void;

  /**
   * Build owner query condition parameter for views queries.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The query to build the condition for.
   */
  public function buildOwnerQueryCondition(AlterableInterface $query): void;

  /**
   * Reassign UIDs for a set of entities.
   *
   * Method uses the batch api to perform the reassignment.  Additional
   * processing can be performed by passing an array of `$operations`.  The
   * `$operations` array can contain an array method names such as:
   * [
   *   '\Drupal\cloud\Service\CloudService::operation_function_callback_1',
   *   '\Drupal\cloud\Service\CloudService::operation_function_callback_2',
   * ]
   * where the method is located in another class.
   *
   * The callback function is passed to the following arguments:
   *   operation_function_callback(
   *     array $entities,
   *     array $updates,
   *     bool $revision,
   *     string $entity_type,
   *     AccountInterface $account,
   *     &$context
   *   );
   *
   * @param array $ids
   *   Array of entity ids to reassign.
   * @param array $update
   *   An array of field/values to update.
   * @param string $entity_type
   *   The entity type to query.
   * @param bool $revision
   *   Whether entity type uses revisions or not.
   * @param \Drupal\user\Entity\User $account
   *   Account being deleted.
   * @param array $operations
   *   Optional operations to pass in for batch processing.
   * @param callable|null $custom_callback
   *   Optional custom processing, instead of `batchProcessEntities`.
   *
   * @see batchProcessEntities()
   */
  public function reassignUids(array $ids, array $update, string $entity_type, bool $revision, User $account, array $operations = [], ?callable $custom_callback = NULL): void;

  /**
   * Return an array of entities provided by a cloud service provider.
   *
   * @param string $provider
   *   Provider to return.
   *
   * @return array
   *   Any array of entity names.
   */
  public function getProviderEntityTypes(string $provider): array;

}
