<?php

namespace Drupal\cloud\Service\Util;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default entity link query implementation.
 */
class EntityLinkQuery implements EntityLinkQueryInterface {

  /**
   * An entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager instance.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query(string $target_type, string $cloud_context, string $field_name, string $item, ?EntityInterface $entity = NULL): array {
    return $this->entityTypeManager
      ->getStorage($target_type)
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('cloud_context', $cloud_context)
      ->condition($field_name, $item)
      ->execute();
  }

}
