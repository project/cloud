<?php

namespace Drupal\cloud\Service\Util;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Query generator interface for entity link.
 */
interface EntityLinkQueryInterface extends ContainerInjectionInterface {

  /**
   * Run entity query with given parameters.
   *
   * @param string $target_type
   *   The type of target entity.
   * @param string $cloud_context
   *   The cloud context.
   * @param string $field_name
   *   The field name of target entity.
   * @param string $item
   *   The value of the field to query.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity interface or NULL.
   *
   * @return array
   *   An array of entity ids.
   */
  public function query(string $target_type, string $cloud_context, string $field_name, string $item, ?EntityInterface $entity = NULL): array;

}
