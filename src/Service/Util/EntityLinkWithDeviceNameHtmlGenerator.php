<?php

namespace Drupal\cloud\Service\Util;

use Drupal\Core\Url;

/**
 * Html generator utility using name for Entity link.
 */
class EntityLinkWithDeviceNameHtmlGenerator extends EntityLinkHtmlGenerator {

  /**
   * {@inheritdoc}
   */
  public function generate(Url $url, $id, $name = '', $alt_text = ''): string {
    if (!empty($alt_text)) {
      $html = $this->linkGenerator->generate($name, $url) . sprintf(" on %s", $alt_text);
    }
    else {
      $html = $this->linkGenerator->generate($name, $url) . sprintf(" on %s", $id);
    }
    return $html;
  }

}
