<?php

namespace Drupal\cloud\Traits;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * The trait checking permission.
 */
trait AccessCheckTrait {

  /**
   * Checks if a user can access the cloud service provider as an owner.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase|null $entity
   *   The entity object.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param mixed $own_permissions
   *   The permissions for the user who is owner.
   * @param mixed $any_permissions
   *   The permissions for the user who is not owner.
   * @param bool $access_anonymous_entity
   *   TRUE means permit access anonymous owned entities.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Allowed or forbidden, neutral if tempstore is empty.
   */
  protected function allowedIfCanAccessCloudConfigWithOwner(
    ?CloudContentEntityBase $entity,
    AccountInterface $account,
    $own_permissions,
    $any_permissions,
    bool $access_anonymous_entity = FALSE,
  ): AccessResultInterface {

    if ($entity !== NULL
      && !$account->hasPermission('view all cloud service providers')
      && !$account->hasPermission('view ' . $entity->getCloudContext())) {
      return AccessResult::neutral();
    }

    // If the account has any permissions.
    $result = AccessResult::allowedIfHasPermissions($account, (array) $any_permissions);
    if ($result->isAllowed()) {
      return $result;
    }

    if (empty($entity->getOwner())) {
      return AccessResult::neutral();
    }

    // If the account has own permissions.
    if (($access_anonymous_entity && $entity->getOwner()->isAnonymous())
      || ($account->id() === $entity->getOwner()->id())) {
      return AccessResult::allowedIfHasPermissions($account, (array) $own_permissions);
    }

    return AccessResult::neutral();
  }

  /**
   * Checks if a user can access the cloud service provider (CloudConfig).
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase|null $entity
   *   The entity object.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param mixed $permissions
   *   The permissions for the user.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Allowed or forbidden, neutral if tempstore is empty.
   */
  protected function allowedIfCanAccessCloudConfig(
    ?CloudContentEntityBase $entity,
    AccountInterface $account,
    $permissions,
  ): AccessResultInterface {

    $cloud_context = $entity === NULL
      ? \Drupal::routeMatch()->getParameter('cloud_context')
      : $entity->getCloudContext();

    if (!$account->hasPermission('view all cloud service providers')
      && !$account->hasPermission('view ' . $cloud_context)) {
      return AccessResult::neutral();
    }

    return AccessResult::allowedIfHasPermissions($account, (array) $permissions);
  }

  /**
   * Check user access for a specific cloud context.
   *
   * Supports 'AND' and 'OR' access checks, similar to _permission definition in
   * *.routing.yml.
   *
   * @param string $perm
   *   Permission string.
   * @param \Drupal\Core\Access\AccessResultInterface $access_result
   *   Additional access results to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account to check access.
   * @param string $cloud_context
   *   The cloud context to check for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function checkPermissions(
    string $perm,
    AccessResultInterface $access_result,
    AccountInterface $account,
    string $cloud_context,
  ): AccessResultInterface {
    if (str_contains($perm, '+') === FALSE && str_contains($perm, ',') === FALSE) {
      return $account->hasPermission('view all cloud service providers')
        ? AccessResult::allowedIfHasPermissions($account, [
          $perm,
        ])->andIf($access_result)
        : AccessResult::allowedIfHasPermissions($account, [
          $perm,
          "view {$cloud_context}",
        ])->andIf($access_result);
    }
    else {
      if (!$account->hasPermission('view all cloud service providers')
        && !$account->hasPermission("view {$cloud_context}")) {
        return AccessResult::neutral();
      }

      // Allow to conjunct the permissions with OR ('+') or AND (',').
      $split = explode(',', $perm);
      // Support AND permission check.
      return count($split) > 1
        ? AccessResult::allowedIfHasPermissions($account, $split)
          ->andIf($access_result)
        : AccessResult::allowedIfHasPermissions($account, explode('+', $perm), 'OR')
          ->andIf($access_result);
    }
  }

}
