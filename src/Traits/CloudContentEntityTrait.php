<?php

namespace Drupal\cloud\Traits;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\cloud\Event\CloudEntityEvent;
use Drupal\cloud\Event\CloudEntityEventType;
use Psr\Log\LoggerInterface;

/**
 * The cloud content entity trait.
 *
 * Handles various functions such as database operations (e.g. clear entities),
 * string manipulations, and standard status message constructions.
 */
trait CloudContentEntityTrait {

  use DependencySerializationTrait;
  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Helper method to get the current timestamp.
   *
   * @return int
   *   The current timestamp.
   */
  protected function getTimestamp(): int {
    return time();
  }

  /**
   * {@inheritdoc}
   */
  public function clearCacheValues(array $tags = []): void {
    \Drupal::service('cloud')->invalidateCacheTags($tags);
  }

  /**
   * Get the entity type name with the format underscore.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The name with the format underscore.
   */
  protected function getShortEntityTypeNameUnderscore(EntityInterface $entity): string {
    $entity_type_id = $entity->getEntityTypeId();
    $provider = $this->getProviderWithUnderscore($entity->getEntityType()->getProvider()) ?? '';
    return substr($entity_type_id, strlen($provider));
  }

  /**
   * Get the entity type name with the format whitespace.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The name with the format whitespace.
   */
  protected function getShortEntityTypeNameWhitespace(EntityInterface $entity): string {
    $entity_type_id = $entity->getEntityTypeId();
    $provider = $this->getProviderWithUnderscore($entity->getEntityType()->getProvider()) ?? '';
    $short_name = substr($entity_type_id, strlen($provider));
    return self::getCamelCaseWithWhitespace($short_name);
  }

  /**
   * Get the entity type name with the format camel.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The name with the format camel.
   */
  public function getShortEntityTypeNameCamel(EntityInterface $entity): string {
    return self::getCamelCaseWithoutWhitespace($this->getShortEntityTypeNameWhitespace($entity));
  }

  /**
   * Get the entity type name plural with the format camel.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The name plural with the format camel.
   */
  public function getShortEntityTypeNamePluralCamel(EntityInterface $entity): string {
    return $this->getShortEntityTypeNamePluralCamelByType($entity->getEntityType());
  }

  /**
   * Get the entity type name plural with the format camel by entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity.
   * @param bool $whitespace
   *   Whether to include whitespace.
   *
   * @return string
   *   The name plural with the format camel.
   *   e.g. function('FooBar' Type) returns 'FooBars',
   *        function('FooBar' Type, TRUE) returns 'Foo bars'.
   */
  public function getShortEntityTypeNamePluralCamelByType(EntityTypeInterface $entity_type, bool $whitespace = FALSE): string {
    $entity_type_id_plural = !empty($entity_type->get('id_plural')) ? $entity_type->get('id_plural') : '';
    $provider = $this->getProviderWithUnderscore($entity_type->getProvider()) ?? '';
    $short_name = '';
    if (!empty($entity_type_id_plural)) {
      $short_name = substr($entity_type_id_plural, strlen($provider));
    }
    return $whitespace
      ? self::getCamelCaseWithWhitespace($short_name, TRUE)
      : self::getCamelCaseWithoutWhitespace($short_name);
  }

  /**
   * Get the entity type name plural with the format camel by entity type name.
   *
   * @param string $entity_type_name
   *   The entity type name.
   * @param bool $whitespace
   *   Whether to include whitespace.
   *
   * @return string|null
   *   The name plural with the format camel.
   */
  public function getShortEntityTypeNamePluralCamelByTypeName(string $entity_type_name, bool $whitespace = FALSE): ?string {
    $entity_type_manager = \Drupal::entityTypeManager();
    if (!$entity_type_manager->hasDefinition($entity_type_name)) {
      return NULL;
    }
    $entity_type = $entity_type_manager
      ->getStorage($entity_type_name)
      ->getEntityType();
    return self::getShortEntityTypeNamePluralCamelByType($entity_type, $whitespace);
  }

  /**
   * Take an entity_type and get the plural form.
   *
   * @param string $entity_type
   *   Entity type.
   *
   * @return array
   *   Array with singular and plural labels.
   */
  protected function getDisplayLabels($entity_type): array {
    $labels = [];
    try {
      $type = \Drupal::entityTypeManager()
        ->getStorage($entity_type)
        ->getEntityType();
      $labels['plural'] = $type->getPluralLabel();
      $labels['singular'] = $type->getSingularLabel();
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    return $labels;
  }

  /**
   * Get a module name of the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The module name. e.g. 'aws_cloud'.
   */
  protected function getModuleName(EntityInterface $entity): string {
    return $entity->getEntityType()->getProvider();
  }

  /**
   * Get a module name of the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The entity's cloud name. e.g. 'aws_cloud' -> returns 'aws cloud'.
   */
  protected function getModuleNameWithWhitespace(EntityInterface $entity): string {
    return self::convertUnderscoreToWhitespace($this->getModuleName($entity));
  }

  /**
   * Add an underscore to the provider string.
   *
   * @param string $provider
   *   The provider (i.e. module name).
   *
   * @return string
   *   Provider with underscore.
   */
  private function getProviderWithUnderscore($provider): string {
    return $provider . '_';
  }

  /**
   * Add 'created' | 'updated' status message and log its notice.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to handle.
   * @param string $passive_operation
   *   The passive voice e.g. 'created' | 'updated' | 'deleted'.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The status message for the operation.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getOperationMessage(EntityInterface $entity, $passive_operation): TranslatableMarkup {

    if ($entity === NULL) {
      return $this->t('The entity is empty.');
    }

    $entity_type = $entity->getEntityType();
    if ($entity_type === NULL) {
      return $this->t('The entity type is empty.');
    }

    if (empty($passive_operation)) {
      return $this->t('The operation is empty.');
    }

    if ($passive_operation === 'deleted') {
      return $this->t('The @type %label has been @passive_operation.', [
        '@type' => $entity_type->getSingularLabel(),
        '%label' => $entity->label(),
        '@passive_operation' => $passive_operation,
      ]);
    }

    $label = $entity->label() ?? $entity_type->getSingularLabel();
    if ($entity->hasLinkTemplate('canonical')) {
      $label = $entity->id()
        ? $entity->toLink($label)->toString() : $label;
    }
    elseif ($entity->hasLinkTemplate('edit-form')) {
      $label = $entity->toLink($label, 'edit-form')->toString();
    }

    return $this->t('The @type %label has been @passive_operation.', [
      '@type' => $entity_type->getSingularLabel(),
      '%label' => $label,
      '@passive_operation' => $passive_operation,
    ]);
  }

  /**
   * Add a status error message and log its notice.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to handle.
   * @param string $passive_operation
   *   The passive voice e.g. 'created' | 'updated' | 'deleted'.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The status message for the operation.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getOperationErrorMessage(EntityInterface $entity, $passive_operation): TranslatableMarkup {

    if ($entity === NULL) {
      return $this->t('The entity is empty.');
    }

    $entity_type = $entity->getEntityType();
    if ($entity_type === NULL) {
      return $this->t('The entity type is empty.');
    }

    if (empty($passive_operation)) {
      return $this->t('The operation is empty.');
    }

    if ($passive_operation === 'created') {
      return $this->t('The @type @label could not be @passive_operation.', [
        '@type' => $entity_type->getSingularLabel(),
        '@label' => $entity->label(),
        '@passive_operation' => $passive_operation,
      ]);
    }

    $label = $entity->label();
    if ($entity->hasLinkTemplate('canonical')) {
      $label = $entity->id() ? $entity->toLink($entity->label())->toString() : $entity->label();
    }
    elseif ($entity->hasLinkTemplate('edit-form')) {
      $label = $entity->toLink($entity->label(), 'edit-form')->toString();
    }

    return $this->t('The @type %label could not be @passive_operation.', [
      '@type' => $entity_type->getSingularLabel(),
      '%label' => $label,
      '@passive_operation' => $passive_operation,
    ]);
  }

  /**
   * Add a status message and log its notice.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to handle.
   * @param string $passive_operation
   *   The passive voice e.g. 'created' | 'updated' | 'deleted'.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function logOperationMessage(EntityInterface $entity, $passive_operation): void {

    $channel = $this->getModuleName($entity);

    if (empty($channel)) {
      $this->logger('cloud')->error($this->t('The module name is empty.'));
      return;
    }

    if ($entity === NULL) {
      $this->logger($channel)->error($this->t('The entity is empty.'));
      return;
    }

    $entity_type = $entity->getEntityType();
    $label = $entity->label() ?? $entity_type->getSingularLabel();
    if ($entity_type === NULL) {
      $this->logger($channel)->error($this->t('The entity type of @label is empty.', [
        '@label' => $label,
      ]));
      return;
    }

    if (empty($passive_operation)) {
      $this->logger($channel)->error($this->t('The operation of @label is empty.', [
        '@label' => $label,
      ]));
      return;
    }

    if ($passive_operation === 'deleted') {

      // @label does not have any link since it is already deleted.
      $this->logger($channel)->info($this->t('@type: @passive_operation @label.', [
        '@type' => $entity_type->getLabel(),
        '@passive_operation' => $passive_operation,
        '@label' => $label,
      ]));

      // Skip the following code if $passive_operation is 'deleted'.
      return;
    }

    $link = [];
    if ($entity->hasLinkTemplate('canonical')) {
      $link = $entity->id()
        ? $entity->toLink($this->t('View'))->toString() : $label;
    }
    elseif ($entity->hasLinkTemplate('edit-form')) {
      $link = $entity->toLink('View', 'edit-form')->toString();
    }

    $this->logger($channel)->info($this->t('@type: @passive_operation %label.', [
      '@type' => $entity_type->getLabel(),
      '@passive_operation' => $passive_operation,
      '%label' => $label,
    ]), [
      'link' => $link,
    ]);
  }

  /**
   * Add an error message and log its error.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to handle.
   * @param string $passive_operation
   *   The passive voice e.g. 'created' | 'updated' | 'deleted'.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function logOperationErrorMessage(EntityInterface $entity, $passive_operation): void {

    $channel = $this->getModuleName($entity);

    if (empty($channel)) {
      $this->logger('cloud')->error($this->t('The module name is empty.'));
      return;
    }

    if ($entity === NULL) {
      $this->logger($channel)->error($this->t('The entity is empty.'));
      return;
    }

    $entity_type = $entity->getEntityType();
    if ($entity_type === NULL) {
      $this->logger($channel)->error($this->t('The entity type of @label is empty.', [
        '@label' => $entity->label(),
      ]));
      return;
    }

    if (empty($passive_operation)) {
      $this->logger($channel)->error($this->t('The operation of @label is empty.', [
        '@label' => $entity->label(),
      ]));
      return;
    }

    if ($passive_operation === 'created') {

      $this->logger($channel)->error($this->t('@type: @label could not be @passive_operation.', [
        '@type' => $entity_type->getLabel(),
        '@label' => $entity->label(),
        '@passive_operation' => $passive_operation,
      ]));

      // Skip the following code if $present_operation is 'create'.
      return;
    }

    $link = [];
    if ($entity->hasLinkTemplate('canonical')) {
      $link = $entity->id()
        ? $entity->toLink($this->t('View'))->toString() : $entity->label();
    }
    elseif ($entity->hasLinkTemplate('edit-form')) {
      $link = $entity->toLink('View', 'edit-form')->toString();
    }

    $this->logger($channel)->error($this->t('@type: %label could not be @passive_operation.', [
      '@type' => $entity_type->getLabel(),
      '%label' => $entity->label(),
      '@passive_operation' => $passive_operation,
    ]), [
      'link' => $link,
    ]);
  }

  /**
   * Add a status message and log its notice.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to handle.
   * @param string $passive_operation
   *   The passive voice e.g. 'created' | 'updated' | 'deleted'.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function processOperationStatus(EntityInterface $entity, $passive_operation): void {

    // Using MessengerTrait::messenger since $this->messenger might not be
    // available at the caller of this method.
    $this->messenger()->addStatus($this->getOperationMessage($entity, $passive_operation));
    $this->logOperationMessage($entity, $passive_operation);
  }

  /**
   * Add a status error message and log its error.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to handle.
   * @param string $passive_operation
   *   The passive voice e.g. 'created' | 'updated' | 'deleted'.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function processOperationErrorStatus(EntityInterface $entity, $passive_operation): void {

    // Using MessengerTrait::messenger since $this->messenger might not be
    // available at the caller of this method.
    $this->messenger()->addError($this->getOperationErrorMessage($entity, $passive_operation));
    $this->logOperationErrorMessage($entity, $passive_operation);
  }

  /**
   * Add a status error message for an exception.
   *
   * @param \Exception $e
   *   An exception object.
   */
  public function handleException(\Exception $e): void {

    // Using MessengerTrait::messenger since $this->messenger might not be
    // available at the caller of this method.
    $this->messenger()->addError($this->t('An error occurred: @exception', [
      '@exception' => $e->getMessage(),
    ]));
  }

  /**
   * Gets the logger for a specific channel.
   *
   * This method exists for backward-compatibility between FormBase and
   * LoggerChannelTrait. Use LoggerChannelTrait::getLogger() instead.
   *
   * @param string $channel
   *   The name of the channel. Can be any string, but the general practice is
   *   to use the name of the subsystem calling this.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger for the given channel.
   */
  public function logger($channel): LoggerInterface {
    return $this->getLogger($channel);
  }

  /**
   * Dispatches a form save event.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to fire for.
   */
  protected function dispatchSaveEvent(EntityInterface $entity): void {
    $this->dispatchEvent($entity, CloudEntityEventType::FORM_SAVE);
  }

  /**
   * Dispatches a form submit event.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Event to fire for.
   */
  protected function dispatchSubmitEvent(EntityInterface $entity): void {
    $this->dispatchEvent($entity, CloudEntityEventType::FORM_SUBMIT);
  }

  /**
   * Helper method that dispatches an event.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to include in the event.
   * @param string $event_type
   *   Event type to fire.
   *
   * @return \Drupal\cloud\Event\CloudEntityEvent
   *   Event object.
   */
  private function dispatchEvent(EntityInterface $entity, string $event_type): CloudEntityEvent {
    $event = new CloudEntityEvent($entity, $event_type);
    \Drupal::service('event_dispatcher')->dispatch($event, $event_type);

    return $event;
  }

  /**
   * Helper method to format words. Accessible from other static functions.
   *
   * @param string $words
   *   Words separated with spaces or _.
   * @param bool $ucfirst
   *   If TRUE, capitalize only the first letter of the sentence;
   *   if FALSE, capitalize the first letter of each word.
   *
   * @return string
   *   The formatted string.
   *   e.g. function('hello_world', TRUE) returns 'Hello world',
   *        function('hello_world') returns 'Hello World'.
   */
  public static function getCamelCaseWithWhitespace($words, bool $ucfirst = FALSE): string {
    if ($ucfirst) {
      return ucfirst(str_replace('_', ' ', $words));
    }
    return ucwords(str_replace('_', ' ', $words));
  }

  /**
   * Helper method to format words. Accessible from other static functions.
   *
   * @param string $words
   *   Words separated with spaces or _.
   *
   * @return string
   *   Formatted
   */
  public static function convertUnderscoreToWhitespace($words): string {
    return str_replace('_', ' ', $words);
  }

  /**
   * Helper method to format words. Accessible from other static functions.
   *
   * @param string $words
   *   Words separated with spaces or _.
   * @param bool $lower_case_first
   *   TRUE to make first letter lower.
   *
   * @return string
   *   Formatted
   */
  public static function getCamelCaseWithoutWhitespace($words, $lower_case_first = FALSE): string {
    $words = str_replace(' ', '', ucwords(str_replace('_', ' ', $words)));
    return $lower_case_first ? lcfirst($words) : $words;
  }

  /**
   * Helper method to convert camel case to snake case.
   *
   * @param string $words
   *   Word in camel case.
   *
   * @return string
   *   Formatted
   */
  public static function getSnakeCase($words): string {
    return strtolower(ltrim(preg_replace('/([A-Z]+)([A-Z][a-z])/', '$1_$2', $words), '_'));
  }

  /**
   * Helper method to convert camel/snake case to kebab case.
   *
   * @param string $words
   *   Word in camel/snake case.
   *
   * @return string
   *   Formatted
   */
  public static function getKebabCase($words): string {
    return strtolower(ltrim(preg_replace('/[A-Z]/', '-\0', $words), '-'));
  }

  /**
   * Helper to generate iam permissions queue name.
   *
   * @param \Drupal\cloud\Entity\CloudConfigInterface $entity
   *   Entity used to build queue name.
   *
   * @return string
   *   Queue resource name.
   */
  public static function getIamPermissionsQueueName(CloudConfigInterface $entity): string {
    return $entity->bundle() . '_update_iam_permissions_queue:' . $entity->getCloudContext();
  }

  /**
   * Helper to generate cloud formation queue name.
   *
   * @param \Drupal\cloud\Entity\CloudConfigInterface $entity
   *   Entity used to build queue name.
   *
   * @return string
   *   Queue resource name.
   */
  public static function getCloudFormationQueueName(CloudConfigInterface $entity): string {
    return $entity->bundle() . '_update_cloud_formation_resources_queue:' . $entity->getCloudContext();
  }

  /**
   * Add items to the resource queue.
   *
   * @param \Drupal\cloud\Entity\CloudConfigInterface $cloud_config
   *   Cloud config object.
   * @param array $method_names
   *   Array of method names.
   * @param int $queue_limit
   *   Queue limit.
   * @param string $queue_name
   *   If passed, get the queue from the optional queue name.
   */
  public static function updateResourceQueue(CloudConfigInterface $cloud_config, array $method_names, $queue_limit, $queue_name = ''): void {
    $update_resources_queue = \Drupal::queue(empty($queue_name) ? \Drupal::service('cloud')->getResourceQueueName($cloud_config) : $queue_name);
    $queue_count = $update_resources_queue->numberOfItems();

    if ($queue_count >= $queue_limit
      || $queue_count > count($method_names)) {
      return;
    }

    // Calculate how many queue items have been processed.
    $queue_count = count($method_names) - $queue_count;
    for ($i = 0; $i < $queue_count; $i++) {
      $update_resources_queue->createItem([
        'cloud_context' => $cloud_config->getCloudContext(),
        "{$cloud_config->bundle()}_method_name" => $method_names[$i],
      ]);
    }
  }

}
