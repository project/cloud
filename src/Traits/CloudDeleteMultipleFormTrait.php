<?php

namespace Drupal\cloud\Traits;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * The trait for CloudDeleteMultipleForm.
 *
 * See also Drupal\Core\Entity\EntityDeleteFormTrait.
 */
trait CloudDeleteMultipleFormTrait {

  // Do not use EntityDeleteFormTrait since it is an abstract class and requires
  // to implement a getEntity() method.
  use CloudContentEntityTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {

    // Get entity type ID from the route because ::buildForm has not yet been
    // called.
    $entity_type_id = $this->getRouteMatch()->getParameter('entity_type_id');
    return $entity_type_id . '_delete_multiple_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {

    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {

    return $this->formatPlural(count($this->selection),
      'Are you sure you want to delete this @item?',
      'Are you sure you want to delete these @items?', [
        '@item' => $this->entityType->getSingularLabel(),
        '@items' => $this->entityType->getPluralLabel(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): TrustedCallbackInterface {
    $route = \Drupal::routeMatch();
    $entity_type_id = $route->getParameter('entity_type_id');
    if (empty($entity_type_id)) {
      $this->logger('cloud')->error($this->t('No entity type ID.'));
      return new Url('<front>');
    }
    return new Url(
      "entity.{$entity_type_id}.collection",
      // @todo Add some comment for this logic here.
      $entity_type_id !== 'cloud_store'
        ? ['cloud_context' => $route->getParameter('cloud_context')]
        : ['cloud_store_type' => $route->getParameter('cloud_store_type')]
    );
  }

  /**
   * Process entity.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   */
  protected function processEntity(CloudContentEntityBase $entity): void {

    $entity->delete();
  }

  /**
   * Process an entity and related AWS resource.
   *
   * @param \Drupal\cloud\Entity\CloudContentEntityBase $entity
   *   An entity object.
   *
   * @return bool
   *   Succeeded or failed.
   */
  protected function process(CloudContentEntityBase $entity): bool {

    try {
      $cloud_config_plugin_manager = \Drupal::service('plugin.manager.cloud_config_plugin');
      $cloud_config_plugin_manager->setCloudContext($entity->getCloudContext());
      $cloud_config = $cloud_config_plugin_manager->loadConfigEntity();

      if ($cloud_config->isRemote()) {
        $this->processCloudResource($entity);
        $this->processOperationStatus($entity, 'deleted remotely');
        return TRUE;
      }

      if ($this->processCloudResource($entity)) {

        $this->processEntity($entity);

        // Since we do not use EntityDeleteFormTrait since it is an abstract
        // class and requires to implement a getEntity() method, so we use our
        // own method here instead.
        $this->processOperationStatus($entity, 'deleted');

        return TRUE;
      }

      // Ditto.
      $this->processOperationErrorStatus($entity, 'deleted');

      return FALSE;
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }
    // Exception return type handling.
    // Return unable to process.
    return FALSE;
  }

  /**
   * Returns the message to show the user after an item was processed.
   *
   * @param int $count
   *   Count of processed translations.
   *
   * @return \Drupal\Core\StringTranslation\PluralTranslatableMarkup
   *   The item processed message.
   */
  protected function getProcessedMessage($count): PluralTranslatableMarkup {

    return $this->formatPlural($count, 'Deleted @count item.', 'Deleted @count items.');
  }

  /**
   * Returns the message to show the user when an item has not been processed.
   *
   * @param int $count
   *   Count of processed translations.
   *
   * @return \Drupal\Core\StringTranslation\PluralTranslatableMarkup
   *   The item inaccessible message.
   */
  protected function getInaccessibleMessage($count): PluralTranslatableMarkup {

    return $this->formatPlural($count,
      '@count item has not been deleted because you do not have the necessary permissions.',
      '@count items have not been deleted because you do not have the necessary permissions.'
    );
  }

}
