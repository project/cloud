<?php

namespace Drupal\cloud\Traits;

use Drupal\consumers\Entity\Consumer;

/**
 * The trait for Cloud Form.
 */
trait CloudFormTrait {

  /**
   * Update views pager options.
   *
   * @param string $module
   *   The module name.
   * @param array $views_settings
   *   The key and value array of views pager options.
   */
  protected function updateViewsPagerOptions($module, array $views_settings): void {
    // This is a trait, so we cannot use DI (dependency injection) here, so that
    // we use \Drupal::service('extension.path.resolver').
    $config_path = \Drupal::service('extension.path.resolver')->getPath('module', $module) . '/config/install';

    $views = [];
    $files = scandir($config_path);
    foreach ($files as $file) {
      if (preg_match('/^(views\.view\..+)\.yml$/', $file, $matches)) {
        $views[] = $matches[1];
      }
    }

    $options = [];
    foreach ($views_settings ?: [] as $key => $value) {
      $view_key = str_replace("{$module}_view_", '', $key);
      if (strpos($view_key, 'expose_') !== FALSE) {
        $view_key = str_replace('expose_', 'expose.', $view_key);
        if ($value) {
          $items_per_page = cloud_get_views_items_options();
          $options['display.default.display_options.pager.options.expose.items_per_page_options'] = implode(',', $items_per_page);
          $options['display.default.display_options.pager.options.expose.items_per_page_options_all'] = TRUE;
        }
      }
      $options["display.default.display_options.pager.options.$view_key"] = $value;

    }
    foreach ($views ?: [] as $view_name) {
      \Drupal::service('cloud')->updateViewsConfiguration($view_name, $options);
    }
  }

  /**
   * Method to enable SPA app.
   *
   * @param string $key_directory
   *   Key directory.
   * @param string $callback_url
   *   SPA callback url.
   */
  protected function enableSpaApp(string $key_directory, string $callback_url): void {
    try {
      // If cloud_dashboard is not enabled, enable it first.
      if (\Drupal::service('module_handler')->moduleExists('cloud_dashboard') === FALSE) {
        \Drupal::service('module_installer')->install([
          'cloud_dashboard',
        ]);
      }

      // Generate the private and public keys, then save the configurations.
      \Drupal::service('simple_oauth.key.generator')->generateKeys($key_directory);
      $config = \Drupal::configFactory()->getEditable('simple_oauth.settings');
      $config->set('public_key', $key_directory . '/public.key');
      $config->set('private_key', $key_directory . '/private.key');
      $config->save();

      // Create the Consumer entity and make it the default.
      $results = \Drupal::entityTypeManager()->getStorage('consumer')
        ->loadByProperties([
          'client_id' => 'cloud_dashboard',
        ]);
      if (!empty($results)) {
        // Reuse the consumer if possible.
        /** @var \Drupal\consumers\Entity\Consumer $consumer */
        $consumer = array_shift($results);
        $consumer->set('is_default', TRUE);
        $consumer->set('redirect', $callback_url);
        $consumer->save();
      }
      else {
        // @phpstan-ignore-next-line
        Consumer::create([
          'client_id' => 'cloud_dashboard',
          'label' => 'Cloud Dashboard',
          'is_default' => TRUE,
          'secret' => 'cloud_dashboard',
          'roles' => [
            'cloud_admin',
          ],
          'redirect' => $callback_url,
          'pkce' => FALSE,
          'confidential' => FALSE,
        ])->save();
      }

      \Drupal::configFactory()
        ->getEditable('system.site')
        ->set('page.front', '/clouds/dashboard/providers')
        ->save();

      \Drupal::configFactory()
        ->getEditable('cloud.settings')
        ->set('cloud_use_spa', TRUE)
        ->set('cloud_spa_key_directory', $key_directory)
        ->set('cloud_spa_callback_url', $callback_url)
        ->save();

    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

  /**
   * Turn off SPA application and set homepage to regular dashboard.
   */
  protected function disableSpaApp(): void {
    \Drupal::configFactory()
      ->getEditable('cloud.settings')
      ->set('cloud_use_spa', FALSE)
      ->save();

    \Drupal::configFactory()
      ->getEditable('system.site')
      ->set('page.front', '/dashboard')
      ->save();
  }

}
