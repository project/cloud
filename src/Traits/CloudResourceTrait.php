<?php

namespace Drupal\cloud\Traits;

/**
 * The trait with common functions for Resource Blocks.
 */
trait CloudResourceTrait {

  /**
   * Get a count of all aws resources.
   *
   * @param string $resource_name
   *   The resource name.
   * @param string $permission
   *   The permission.
   * @param array $params
   *   The params.
   *
   * @return int|void
   *   Entity count.
   */
  protected function getResourceCount($resource_name, $permission, array $params): int {
    if (empty($this->currentUser->hasPermission($permission))) {
      $params['uid'] = $this->currentUser->id();
    }
    $entities = $this->runEntityQuery($resource_name, $params);
    return count($entities ?: []);
  }

  /**
   * Get the count of resources.
   *
   * @param string $resource_name
   *   The resource name.
   * @param array $params
   *   The params.
   *
   * @return int
   *   Entity count.
   */
  protected function getResourceCountWithAccessCheck($resource_name, array $params): int {
    $storage = $this->entityTypeManager
      ->getStorage($resource_name);

    $query = $storage
      ->getQuery()
      ->accessCheck(TRUE);

    foreach ($params ?: [] as $key => $value) {
      $query->condition($key, $value);
    }

    $result = $query->execute();
    $entities = $storage->loadMultiple($result);
    return count(array_filter($entities, function ($entity) {
      return $entity->access('view', $this->currentUser);
    }));
  }

  /**
   * Execute an entity query.
   *
   * @param string $entity_name
   *   The entity name.
   * @param array $params
   *   Array of parameters.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of loaded entities.
   */
  protected function runEntityQuery(string $entity_name, array $params): array {
    $cloud_context = $this->configuration['cloud_context'];
    /** @var \Drupal\Core\Routing\RouteMatchInterface $route_match */
    $cloud_project = \Drupal::routeMatch()->getParameter('cloud_project');
    // Added $params['cloud_context'] check so a cloud_context value can be
    // passed in the $params array.
    if (empty($cloud_project) && !empty($cloud_context) && empty($params['cloud_context'])) {
      $params['cloud_context'] = $cloud_context;
    }
    return $this->entityTypeManager->getStorage($entity_name)
      ->loadByProperties($params);
  }

}
