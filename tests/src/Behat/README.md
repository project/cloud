Introduction
============

[Behat](https://docs.behat.org/en/latest/) is an open source **Behavioral-Driven
Development** (or **BDD**) framework for PHP. It is a tool to support you in
delivering software that matters through continuous communication, deliberate
discovery and test-automation.

Features and Scenarios in Behat
-------------------------------
Every `*.feature` file conventionally consists of a single feature. A feature
usually contains a list of scenarios.
Every scenario consists of a list of steps, which must start with one of
the keywords `Given`, `When`, `Then`, `And`, or `But`, defined in
[Gherkin](https://cucumber.io/docs/gherkin/)

Locations of feature files in our project
------------------------------------------
We have multiple Behat directories, one for each module.
```
- `tests/src/Behat`
   + behat.yml
   + features
      + bootstrap/FeatureContext.php
      + templates
         + params
   + scirpts/run_behat.sh

- `modules/cloud_service_providers/aws_cloud/tests/src/Behat`
   + behat.yml
   + features
      + bootstrap/FeatureContext.php
      + templates
         + params
   + scirpts/leanup_aws_resources.sh

- `modules/cloud_service_providers/k8s/tests/src/Behat`
   + behat.yml
   + features
      + bootstrap/FeatureContext.php
      + templates
         + params
   + scirpts/cleanup_k8s_resources.sh
```

Something we should know about Behat testing
-------------------------------------------
- Requirements for a container

If you build a Docker container, it requires more than 256 MB of shared memory,
`shm-size >= 256m`. (run `df -h /dev/shm` to display your shared memory size),

An example of a script:
   ```
   #!/bin/bash

   IMAGE_TAG=docomoinnovations/cloud_orchestrator
   CONTAINER_NAME=cloud-orchestrator-qa

   docker start cloud-orchestrator-memcache
   docker start mariadb

   CO_DIR="$HOME/cloud_orchestrator"
   DOCKER_DIR="/var/www/cloud_orchestrator"

   sudo docker run --name $CONTAINER_NAME --link mariadb:mysql \
                   --link cloud-orchestrator-memcache:memcache \
                   --shm-size=256m  \
                   -p 8080:80 -d -v $CO_DIR:$DOCKER_DIR $IMAGE_TAG
   ```

- A patch for `chrome` extension, `dmore/behat-chrome-extension`

This extension is for controlling Chrome without the overhead of Selenium.
This works at least twice as fast as Chrome with selenium, but
connection timeout in `DevToolsConnections` class sometimes happens.

To mitigate this timeout, we need to wait longer by modifying the function.
Please refer to the patch at `./tests/src/Behat/DevToolsConnection.patch` and
replace `DevToolsConnection.php` with it.

This patch is also taken care of by the script `run_behat.sh`.


How to start Behat testing
============================

A. Prepare test scenarios under `features/templates` directory
--------------------------------------------------------------

1. Create a `feature` file with `Twig` template.

2. Define key-value pairs of the Twig template in `yaml` files.

We recommend the definitions of parameters set into two files: public and
private. The private file should include privacy-sensitive information such as
an account ID and credentials.

If a random string is expected in a value, reserved keywords, `@Random`,
`@random` or `@RANDOM` ( depending on required cases ) can be used.

B. Set up the running environment of Behat
--------------------------------------------------------------

1. Install the following packages.
- Behat tools via `composer require --dev`
    - Drupal extension: `drupal/drupal-extension`
      or `docomoinnovations/drupal-extension`
    - Chrome extension:`dmore/behat-chrome-extension`

- a web driver via `apt-get install`
  e.g., `chromium-driver`

- a web browser supporting a headless mode via `apt-get install`
  e.g., `chromium`

Since the Drupal API requires the testing server is the same host of the site
under the test, these packages are to be installed the same host.

2. Set up Behat configurations in `behat.yml` and/or `${BEHAT_PARAMS}`

The configuration file, `behat.yml` should exist at the same directory where
`features` directory exists.
The file should include extensions and configurations for them.

However, some configurations that highly depends on the running environment are
better to set as the environment variables, instead of setting in the file.
The examples of such configurations are for MinkExtension, configure `base_url`,
and for DrupalExtension, `drupal_root` for drupal API and `root` for drush API.

E.g.,
```
export BEHAT_PARAMS=$(cat << EOF
{
    "extensions": {
        "Drupal\\\MinkExtension": {"base_url":"http://localhost"},
        "Drupal\\\DrupalExtension": {
            "drupal": {"drupal_root":"${DOCROOT}"},
            "drush": {"root":"${DOCROOT}"}
        }
    }
}
EOF
```

C. Start to run Behat
--------------------------------------------------------------

This part of steps are taken care of by the script, `run_behat.sh`.
This script also deletes the temporary generated files and directories
and stop the browser process once the test ends.

   E.g.,
   ```
   run_behat.sh -b modules/cloud_service_providers/aws_cloud/tests/src/Behat \
   -p features/templates/params/private.yml,\
   features/templates/params/common.yml, \
   features/templates/params/ap-northeast-1.yml --tags '@minimal&&~@ignore'
   ```

   ```
   run_behat.sh -b modules/cloud_service_providers/k8s/tests/src/Behat \
   -p features/templates/params/private.yml,\
   features/templates/params/k8s_params.yml,\
   features/templates/params/nginx.yml
   ```

Instead of using the script, you can follow the steps below:

1. Start a browser in a headless mode

    ```
    % chromium --disable-extensions --disable-gpu --headless --no-sandbox \
    --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222 2> \
   /dev/null &
   ```

2. Generate temporary test feature files based on Twig template and parameters

   Run `drush` subcommand, `cloud:behat:preprocess` with the parameter files
   prepared in the step above:

   ```
   % drush cloud:behat:preprocess \
   --behat_path=modules/cloud_service_providers/aws_cloud/tests/src/Behat \
   --params_files=features/templates/params/private.yml, \
   features/templates/params/common.yml, \
   features/templates/params/ap-northeast-1.yml \
   --templates_dir=features/templates \
   --output_dir=features/tmp.20220507_203832.oAuSWq \
   --random_str=oAuSWq
   ```

   Note: The random string can be automatically generated without `--random_str`
   option.
   However, we want to specify the string since the string is needed in the
   cleanup script after testing.

3. Run Behat command with the directory of the generated test feature files in
   the previous step:
   ```
   % behat features/tmp.20220507_203832.oAuSWq
   ```

   You can run the Behat command with various options, for example:

   - `--stop-on-failure`: Once a test scenario fails, the subsequent test steps
     and scenarios are skipped.

   - `--tags '@minimal&&~@ignore'`: Filter test scenarios by the specified tag
     conditions.

D. After testing
--------------------------------------------------------------
When the test scenario includes resource creation at a cloud service, testing,
specially upon a test failure, might leave unused resources there. To clean up
such garbage resources,
we should run a cleanup script to delete the resources created for testing
purpose only.

To clean up AWS resources, run the following script, for example,
```
% scripts/cleanup_aws_resources.sh -n BDD*_${RANDOM_STRING}* --profile \
${AWS_PROFILE} -r ${AWS_REGION} --no-dry-run
```
To clean up K8s resources, run the following script, for example,
```
% scripts/cleanup_k8s_resources.sh -n bdd.*-${RANDOM_STRING}.* \
--namespace bdd-namespace-${RANDOM_STRING} --no-dry-run
```
