Feature: Login, Check the Clean State, and Logout
  In order to list resources on the clean state
  As a user
  I need to have both Drupal and AWS IAM permissions

  Scenario: Log in as "Anonymous user"
    Given I am not logged in
    When I visit "/clouds"
    Then I should be on "/user/login"
    And I should see the heading "Log in"

  @api
  Scenario Outline: See links in the navigation bar
    Given I am logged in as a user with the "<role>" role
    When I should see the link "Cloud service providers" in the "nav_bar"
    And I should see the link "Design" in the "nav_bar"
    # DO NOT change <status> to "<status>" since <status> is not an :arg1.
    Then I should <status> the link "Manage" in the "nav_bar"

    Examples:
      | role                | status  |
      | Authenticated user  | not see |
      | Cloud administrator | see     |
      | Administrator       | see     |

  @api
  Scenario Outline: Access cloud service providers
    Given I am logged in as a user with the "<role>" role
    # Make sure the clean state where no cloud service providers exist
    And I should not see the link "All" in the "nav_bar"
    # DO NOT change <status> to "<status>" since <status> is not an :arg1.
    And I should <status> "Add cloud service provider" in the "nav_bar"
    When I go to "/clouds"
    Then I should be on "/admin/structure/cloud_config/add"
    And I should get a "<code>" HTTP response
    And I should see the heading "<heading>"

    Examples:
      |  role               | status  | code | heading                    |
      | Authenticated user  | not see | 403  | Access denied              |
      | Cloud administrator | see     | 200  | Add cloud service provider |
      | Administrator       | see     | 200  | Add cloud service provider |

  @api
  Scenario: Manage cloud service providers as "Cloud administrator"
    Given I am logged in as a user with the "Cloud administrator" role
    And I should see the link "Manage" in the "nav_bar"
    When I click "Manage" in the "nav_bar"
    Then I should be on "/admin/structure/cloud_config"
    And I should see "Cloud service providers"

  @api
  Scenario: View the list of cloud service providers as "Cloud administrator"
    Given I am logged in as a user with the "Cloud administrator" role
    And I visit "/clouds"
    Then I should be on "/admin/structure/cloud_config/add"
    # Make sure the clean state where no cloud service providers exist
    And I should see the success message "There is no cloud service provider."
    And I should see the link "AWS Cloud"
    And I should see the link "K8s"
    And I should see the link "VMware"

  @api
  Scenario Outline: Log out
    Given I am logged in as a user with the "<role>" role
    And I visit "/"
    And I should see the link "Log out"
    When I click "Log out"
    Then I should be on "/user/login"
    And I should see the link "Log in"

    Examples:
      | role                |
      | Authenticated user  |
      | Cloud administrator |
      | Administrator       |
