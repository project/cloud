<?php

namespace Drupal\Tests\cloud\Behat\features\bootstrap;

use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Mink\Element\NodeElement;
use Behat\Mink\Exception\DriverException;
use Behat\Mink\Exception\ElementNotFoundException;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Behat\Testwork\Tester\Result\TestResult;
use Drupal\DrupalExtension\Context\RawDrupalContext;
use Drupal\DrupalExtension\FeatureTrait;
use Drupal\DrupalExtension\MinkAwareTrait;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext {

  use MinkAwareTrait;
  use FeatureTrait;

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /**
   * Fill in the hour/minute based on the elapsed minute from the current time.
   *
   * @var hour_id
   *   The ID of the hour list.
   * @var minute_id
   *   The ID of the minute list.
   * @var elapsed_minute
   *   The elapsed minute.
   *
   * @throws \Exception
   *   If no hour or minute cannot be selected.
   */
  private function selectTimeByElapsedTime(string $hour_id, string $minute_id, string $elapsed_minute): void {
    $element = $this->getSession()->getPage();
    $hour_select = $element->findById($hour_id);
    $minute_select = $element->findById($minute_id);

    if (empty($hour_select)) {
      throw new \Exception(sprintf('The select with "%s" was not found on the page %s', $hour_id, $this->getSession()->getCurrentUrl()));
    }
    if (empty($minute_select)) {
      throw new \Exception(sprintf('The select with "%s" was not found on the page %s', $minute_id, $this->getSession()->getCurrentUrl()));
    }

    $time = new \DateTime('now', new \DateTimeZone(date_default_timezone_get()));
    $time->add(\DateInterval::createFromDateString("$elapsed_minute minute"));
    $hour = $time->format('H');
    $minute = $time->format('i');

    $hour_select->selectOption($hour, FALSE);
    $minute_select->selectOption($minute, FALSE);
  }

  /**
   * Wait until the specified schedule time.
   *
   * @var hour_id
   *   The ID of the hour list.
   * @var minute_id
   *   The ID of the minute list.
   *
   * @throws \Exception
   *   If the selector for hour_id or minute_id is not found.
   */
  private function waitUntilScheduleTime(string $hour_id, string $minute_id): void {
    $resource_startup_wait_minute = 1;
    $element = $this->getSession()->getPage();
    $hour_select = $element->findById($hour_id);
    $minute_select = $element->findById($minute_id);

    if (empty($hour_select)) {
      throw new \Exception(sprintf('The select with "%s" was not found on the page %s', $hour_id, $this->getSession()->getCurrentUrl()));
    }
    if (empty($minute_select)) {
      throw new \Exception(sprintf('The select with "%s" was not found on the page %s', $minute_id, $this->getSession()->getCurrentUrl()));
    }

    $hour = $hour_select->getValue();
    $minute = $minute_select->getValue();

    if (!is_numeric($hour) || !(0 <= $hour && $hour <= 23)) {
      throw new \Exception(sprintf('Failed to get the valid value of %s from the hour select.', $hour_id));
    }
    if (!is_numeric($minute) || !(0 <= $minute && $minute <= 59)) {
      throw new \Exception(sprintf('Failed to get the valid value of %s from the minute select.', $minute_id));
    }

    $now_time = new \DateTime('now', new \DateTimeZone(date_default_timezone_get()));
    $wait_until_time = new \DateTime('', new \DateTimeZone(date_default_timezone_get()));
    $wait_until_time->setTime($hour, $minute);
    $wait_until_time->add(\DateInterval::createFromDateString("$resource_startup_wait_minute Minutes"));
    $wait_time = $wait_until_time->diff($now_time);
    // Converts hours, minutes, and seconds to seconds.
    $wait_second = $wait_time->h * 60 * 60 + $wait_time->i * 60 + $wait_time->s;
    sleep($wait_second);
  }

  /**
   * Select the start time in elapsed minute from the current time.
   *
   * @When I select :elapsed_minutes minutes later now from the start-up time
   */
  public function selectScheduleStartupTimeByElapsedTime($elapsed_minutes) {
    $this->selectTimeByElapsedTime('edit-start-hour', 'edit-start-minute', $elapsed_minutes);
  }

  /**
   * Select the stop time in elapsed minute from the current time.
   *
   * @When I select :elapsed_minutes minutes later now from the stop time
   */
  public function selectScheduleStopTimeByElapsedTime($elapsed_minutes) {
    $this->selectTimeByElapsedTime('edit-stop-hour', 'edit-stop-minute', $elapsed_minutes);
  }

  /**
   * Wait until the stop time for Schedule.
   *
   * @When I wait until start-up time for schedule
   */
  public function waitUntilStartupTimeForSchedule(): void {
    $this->waitUntilScheduleTime('edit-start-hour', 'edit-start-minute');
  }

  /**
   * Wait until the stop time for Schedule.
   *
   * @When I wait until stop time for schedule
   */
  public function waitUntilStopTimeForSchedule(): void {
    $this->waitUntilScheduleTime('edit-stop-hour', 'edit-stop-minute');
  }

  /**
   * Get a row by the rowIndex in the specified region.
   *
   * @param int $rowIndex
   *   The row index.
   * @param string $region
   *   The search region.
   *
   * @return \Behat\Mink\Element\NodeElement
   *   The NodeElement in the row.
   *
   * @throws \Exception
   *   If no rowObj is found.
   */
  private function getRowByIndexInRegion(int $rowIndex, string $region): NodeElement {
    $regionObj = $this->getRegion($region);
    $tableRowSelector = sprintf('.row-%d', (int) $rowIndex);
    $rowObj = $regionObj->find('css', $tableRowSelector);
    if (empty($rowObj)) {
      throw new \Exception(sprintf('No field "%s" found in the region %s of %s.', $tableRowSelector, $region, $this->getSession()->getCurrentUrl()));
    }
    return $rowObj;
  }

  /**
   * Fills in a form field with id|name|title|alt|value in the specified row.
   *
   * The row is specified by the index in the region.
   *
   * @When I fill in :field with :value in the row :rowIndex in the :region (region)
   */
  public function fillFieldInTableRowInRegion($value, $field, $rowIndex, $region): void {
    $this->getRowByIndexInRegion($rowIndex, $region)->fillField($field, $value);
  }

  /**
   * Selects an option in select field with specified id|name|title|alt|value.
   *
   * The select field is also specified by the row index in the region.
   *
   * @When I select :option from :field in the row :rowIndex in the :region (region)
   */
  public function selectOptionInTableRowInRegion($option, $field, $rowIndex, $region): void {
    $this->getRowByIndexInRegion($rowIndex, $region)->selectFieldOption($field, $option);
  }

  /**
   * Presses the button with the specified attribute and value.
   *
   * This function searches for a button element that matches the given
   * attribute and value, and clicks on it. If the button is not found,
   * a \RuntimeException is thrown with an error message.
   *
   * @param string $attribute
   *   The attribute name (e.g., 'id', 'name', 'value', 'class') to search for.
   * @param string $value
   *   The attribute value of the button to match.
   *
   * @throws \RuntimeException
   *   If the button with the specified attribute and value is not found.
   *
   * @When I click the button with the :attribute :value
   */
  public function pressButtonWithAttribute($attribute, $value): void {
    $button = $this->getSession()->getPage()->find($attribute, $value);

    if (empty($button)) {
      throw new \RuntimeException("Button with $attribute '$value' not found.");
    }

    $button->click();
  }

  /**
   * Clicks the link in a table row with specified text in specified column.
   *
   * @param string $text
   *   The text to search for in the table row.
   * @param string $column_name
   *   The name of the column to search in.
   * @param string $table
   *   The CSS selector of the table.
   *
   * @When I click the link in the row with :text in the :column_name column in the :table table
   */
  public function iClickLinkInRowWithTextInColumnInTable($text, $column_name, $table): void {
    $table_node = $this->getSession()->getPage()->find('css', $table);

    if (empty($table_node)) {
      throw new ElementNotFoundException($this->getSession(), 'table', 'css', $table);
    }

    $headers = $table_node->findAll('css', 'thead th');
    $column_index = $this->getColumnIndexByText($headers, $column_name);

    if (empty($column_index)) {
      throw new ElementNotFoundException($this->getSession(), 'table column', 'text', $column_name);
    }

    $this->clickLinkInRowWithTextInColumn($table_node, $text, $column_index);
  }

  /**
   * Gets index of column with specified text in headers.
   *
   * @param \Behat\Mink\Element\NodeElement[] $headers
   *   The header elements of the table.
   * @param string $column_name
   *   The text to search for in the headers.
   *
   * @return int|null
   *   The index of the column, or null if not found.
   */
  private function getColumnIndexByText(array $headers, string $column_name): ?int {
    foreach ($headers as $index => $header) {
      if ($header->getText() === $column_name) {
        return $index + 1;
      }
    }
    return NULL;
  }

  /**
   * Clicks link in row with text in specified column.
   *
   * @param \Behat\Mink\Element\NodeElement $table_node
   *   The table element containing the rows.
   * @param string $text
   *   The text to search for in the column.
   * @param int $column_index
   *   The index of the column to search in.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   *   If the link is not found.
   */
  private function clickLinkInRowWithTextInColumn(NodeElement $table_node, string $text, int $column_index): void {
    $rows = $table_node->findAll('css', 'tbody tr');

    foreach ($rows as $row) {
      $cell = $row->find('css', "td:nth-child($column_index)");

      if (empty($cell) || strpos($cell->getText(), $text) === FALSE) {
        continue;
      }

      $link = $row->find('css', 'a');

      if (!empty($link) && $link->isVisible()) {
        $link->click();
        return;
      }
    }
  }

  /**
   * Custom step definition to check a radio button with a specific label.
   *
   * @param string $labelText
   *   The text of the label associated with the radio button.
   *
   * @throws \Exception
   *   If the radio button with the specified label is not found.
   *
   * @When /^I check the "([^"]*)" radio button$/
   */
  public function iCheckRadioButton($labelText) {
    $page = $this->getSession()->getPage();
    foreach ($page->findAll('css', 'label') as $label) {
      if ($labelText === $label->getText()) {
        $radioButton = $page->find('css', '#' . $label->getAttribute('for'));
        $radioButton->click();
        return;
      }
    }
    throw new \Exception("Radio button with label {$labelText} not found");
  }

  /**
   * Takes a screenshot of the current form and saves it to an output directory.
   *
   * @When I take screenshot
   * @When I take screenshot with suffix :suffix
   */
  public function takeScreenshot(string $suffix = ''): void {
    $url = parse_url($this->getSession()->getCurrentUrl());
    $url_path = ltrim($url['path'] ?: '', '/');
    $feature_file_index = preg_replace('/^(.*?-\d+-\d+)-.*$/', '$1', $this->getFeatureFileName());

    $path = rtrim($this->getScreenshotParameter('path') ?: '', '/') . '/form_screenshot';
    $filepath = $this->getOrCreateDirectory($path);
    $filename = sprintf(
      '%s-%s%s',
      $feature_file_index,
      str_replace(['/'], '-', $url_path),
      $suffix ? "_{$suffix}" : ''
    );

    $this->handleScreenshot($filename, $filepath);
  }

  /**
   * Get the filename of the current feature file without the extension.
   *
   * @return string
   *   The filename without the extension.
   */
  private function getFeatureFileName(): string {
    $feature_file = $this->getFeature()->getFile();
    return pathinfo($feature_file, PATHINFO_FILENAME);
  }

  /**
   * Handle the process of taking a screenshot and displaying the result.
   *
   * @param string $filename
   *   The base filename for the screenshot.
   * @param string $filepath
   *   The directory path where the screenshot will be saved.
   */
  private function handleScreenshot(string $filename, string $filepath): void {
    try {
      $extension = 'png';

      // If width or height not specified, the width can be 1020 or height can
      // be 900 pixels.
      $width = $this->getScreenshotParameter('width') ?? 1020;
      $height = $this->getScreenshotParameter('height') ?? 900;
      if (!empty($width) || !empty($height)) {
        $this->getSession()->resizeWindow(
          (int) $width,
          (int) $height,
          'current'
        );
      }
      $this->saveScreenshot("{$filename}.{$extension}", $filepath);
      echo "Screenshot at: {$filepath}/{$filename}.{$extension}";
    }
    catch (UnsupportedDriverActionException | DriverException $e) {
      $data = $this->getSession()->getDriver()->getContent();
      $extension = 'html';
      file_put_contents("{$filepath}/{$filename}.{$extension}", $data);
      echo "Screenshot at: {$filepath}/{$filename}.{$extension}";
    }
  }

  /**
   * Get a screenshot parameter based on the provided name.
   *
   * @param string $name
   *   The name of the screenshot parameter.
   *
   * @return string
   *   The screenshot parameter value or null if not found.
   */
  private function getScreenshotParameter(?string $name = NULL): ?string {
    $parameters = $this->getDrupalParameter('screenshot');

    return $parameters[$name] ?? NULL;
  }

  /**
   * Gets or creates a directory.
   *
   * @param string $path
   *   The path to the directory.
   *
   * @return string
   *   The path to the existing or newly created directory.
   */
  private function getOrCreateDirectory(string $path): string {
    // If the path is empty, or it doesn't exist and cannot be created,
    // return the temporary directory.
    if (empty($path) || !is_dir($path) && !mkdir($path, 0777, TRUE)) {
      return sys_get_temp_dir();
    }

    return $path;
  }

  /**
   * Wait specified milliseconds.
   *
   * @When I wait :msec milliseconds
   */
  public function waitMilliSeconds(int $msec): void {
    usleep($msec * 1000);
  }

  /**
   * Find text in a table row containing given text.
   *
   * @Then I should see (the text ):text1 or :text2 in the :rowText row
   */
  public function assertTextOrInTableRow(string $text1, string $text2, string $rowText): void {
    $row = $this->getTableRow($this->getSession()->getPage(), $rowText);
    if (strpos($row->getText(), $text1) === FALSE && strpos($row->getText(), $text2) === FALSE) {
      throw new \RuntimeException(sprintf('Found a row containing "%s", but it contained neither the text "%s" nor "%s".', $rowText, $text1, $text2));
    }
  }

  /**
   * Check a box in a table row containing given text.
   *
   * @When I check the box in the :rowText row
   */
  public function checkBoxInTableRow(string $rowText): void {
    $row = $this->getTableRow($this->getSession()->getPage(), $rowText);
    $input = $row->find('css', 'input');
    $input->check();
  }

  /**
   * Follows link in a table row with given value set in specified field.
   *
   * @When I follow/click :link in a row with :value set in :field in the :region( region)
   */
  public function followLinkInTableRowWithValueInRegion(string $link, string $value, string $field, string $region): void {
    $regionObj = $this->getRegion($region);
    $rows = $regionObj->findAll('css', 'table > tbody > tr');
    foreach ($rows as $row) {
      $fieldObj = $row->findField($field);
      if (strpos($fieldObj->getValue(), $value) === FALSE) {
        continue;
      }
      $row->clickLink($link);
      return;
    }
  }

  /**
   * Find text in a table row containing given text in specified region.
   *
   * @Then I should see (the text ):text in the :rowText row in the :region( region)
   */
  public function assertTextInTableRowInRegion(string $text, string $rowText, string $region): void {
    $regionObj = $this->getRegion($region);
    $row = $this->getTableRow($regionObj, $rowText);
    if (strpos($row->getText(), $text) === FALSE) {
      throw new \Exception(sprintf("The text '%s' was not found in the row with '%s' in the region '%s' on the page %s", $text, $rowText, $region, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * Take a screenshot on failure.
   *
   * @AfterStep
   */
  public function takeScreenshotOnFailure(AfterStepScope $scope): void {
    // We cannot use !isPassed() as the scenario outline returns
    // a non-PASSED code.
    if ($scope->getTestResult()->getResultCode() === TestResult::FAILED) {
      $filepath = $this->getOrCreateDirectory($this->getScreenshotParameter('path'));
      $filename = sprintf(
        '%s_%s_%s%s',
        date('mdy-His'),
        $this->getFeatureFileName(),
        $this->getStep()->getLine(),
        $this->getScreenshotParameter('failure_suffix')
      );

      $this->handleScreenshot($filename, $filepath);
    }
  }

  /**
   * Log in as the existing user.
   *
   * @Given I am logged in as user :name
   */
  public function iAmLoggedInAsUser(string $name): void {
    // Another solution using user_pass_reset_url() is independent from drush,
    // but it works only once.
    $base_url = $this->getMinkParameter('base_url');
    $user_login = $this->getDriver('drush')->drush('user:login', [
      "--name=" . $name,
      "--no-browser",
      "--uri=" . $base_url,
    ]);
    $this->getSession()->visit(trim($user_login ?: ''));
  }

  /**
   * Visits the current URL with the specified path appended.
   *
   * @param string $path
   *   The path to append to the current URL.
   *
   * @When /^I visit the current URL with "([^"]*)" append$/
   */
  public function iVisitTheCurrentUrlWithAppended(string $path): void {
    $currentUrl = $this->getSession()->getCurrentUrl();
    $newUrl = rtrim($currentUrl, '/') . '/' . ltrim($path ?: '', '/');
    $this->getSession()->visit($newUrl);
  }

  /**
   * Select a value in a TomSelect dropdown.
   *
   * @param string $option
   *   The option to set.
   * @param string $element
   *   The element to set the option for.
   *
   * @When I select the option :option in the TomSelect element :element
   */
  public function iSelectTheOptionInTheInitializedTomSelect($option, $element): void {
    $script = <<< EOF
    let select_element = document.getElementById('$element').tomselect;
    if (!select_element) {
      return;
    }
    select_element.addItem('$option');
EOF;
    $this->getSession()->evaluateScript($script);
  }

}
