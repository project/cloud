#!/bin/bash
#
# Run Behat after check the environment and preprocess feature files.
#
source "$(dirname $0)/.env"

function usage() {
  readonly cmd=$(echo $0 | sed -e 's/.*\///')
  cat <<EOF
Usage: ${cmd} [OPTION]

Examples
  AWS: ${cmd} -b ${AWS_BEHAT_PATH} -p ${AWS_PARAMS_FILES} --tags ${AWS_TAGS}

  K8s: ${cmd} -b ${K8S_BEHAT_PATH} -p ${K8S_PARAMS_FILES} --tags '@setup,@cleanup,@your_test'

  OpenStack: ${cmd} -b ${OPENSTACK_BEHAT_PATH} -p ${OPENSTACK_PARAMS_FILES} --tags '~@wip'

  VMware: ${cmd} -b ${VMWARE_BEHAT_PATH} -p ${VMWARE_PARAMS_FILES} --tags '~@wip'

OPTION:
  -b, --behat_path:    Behat path, relative to ${DOCROOT}, where this runs Behat command.
                       All other options to be relative to this path. [default: ${DEFAULT_BEHAT_PATH}]
  -f, --feature_file:  The feature file or directory. If set, no preprocessing: -o, -p, -s, -t options are ignored.
  -h, --help:          Show this usage.
  -m, --run_mode:      Behat run mode: multiple feature files at_once, or for each file.
                       [default: at_once with suites/tags options, otherwise for each]
  --no-js:             No javascript and no browser process are required.
  -o, --output_dir:    The parent directory where this preprocessing outputs.
                       [default: ${FEATURES}]
  -p, --params_files:  The comma-separated names of files defining parameters
                       for templates. [default: ${DEFAULT_PARAMS_FILES}]
  -r, --repeat:        Repeat Behat run n times [default: 1]
  -s, --random_str:    The string to replace @random in feature files. If unset, dynamically generated.
  -t, --templates_dir: The directory where the feature file templates exist.
                       [default: ${FEATURES}/${TEMPLATES}]

OPTION to be passed to `behat` command:
  --profile=PROFILE
  --role=ROLE
  --stop-on-failure
  --suite=SUITE
  --tags=TAGS
EOF
  exit 1
}

function main() {
  while [[ "$1" != "" ]]; do
    case $1 in
      -b | --behat_path ) shift; behat_path="$1" ;;
      -f | --feature_file ) shift; feature_file="$1"; require_preprocess='false' ;;
      -h | --help ) usage ;;
      -m | --run_mode) shift; run_mode="$1" ;;
      --no-js) require_javascript='false' ;;
      -o | --output_dir ) shift; preprocess_output_dir="$1" ;;
      -p | --params_files ) shift; preprocess_params_files="$1" ;;
      -r | --repeat ) shift; repeat="$1" ;;
      -s | --random_str) shift; random_str_opt="$1" ;;
      -t | --templates_dir ) shift; preprocess_templates_dir="$1" ;;
      --*) behat_options="${behat_options} $1"
        if [[ $1 =~ 'profile' || $1 =~ 'tags' ]]; then run_mode='at_once'; fi
        if [[ ! $1 =~ '=' ]] && [[ $1 != '--stop-on-failure' ]] ; then shift; behat_options="${behat_options} $1"; fi ;;
      * ) usage
    esac
    shift
  done

}

function err() {
  echo "[Error] $*" >&2
}

function echo_count() {
  echo
  echo "$(( ++i ))) $*..."
}

function run_cmd() {
  local cmd="$@"
  echo_count "Executing ${cmd}"
  if ! ${cmd}; then
    err 'Failed.'
    exit 1
  fi
}

function run_cmd_recent_log() {
  local cmd="$@"
  echo_count "Executing ${cmd}"
  if ! ${cmd}; then
    err 'Failed.'
    check_recent_log
    exit 1
  fi
}

function run_cmd_ignoring_failure() {
  local cmd="$@"
  echo_count "Executing ${cmd}"
  if ! ${cmd}; then
    err 'Failed.'
  fi
}

function run_pushd() {
  local target="$1"
  pushd "${target}" > /dev/null || exit 1
  echo "Current directory: $(pwd)"
}

function run_popd() {
  popd > /dev/null || exit 1
}

function require_module() {
  local module="$1"
  echo_count "Checking if ${module} is installed"
  if (composer show "${module}" > /dev/null); then
    echo 'Found.'
  else
    run_cmd composer require --dev "${module}"
  fi
}

function setup() {
  export BEHAT_PARAMS=$(cat << EOF
    {
      "extensions": {
        "Drupal\\\MinkExtension": {"base_url":"http://localhost"},
        "Drupal\\\DrupalExtension": {
          "drupal": {"drupal_root":"${DOCROOT}"},
          "drush": {"root":"${DOCROOT}"}
        }
      }
    }
EOF
  )
  run_pushd "${CO_DIR}"
  require_module docomoinnovations/drupal-extension
  if [[ "${require_javascript}" == 'false' ]]; then
    return
  fi
  require_module dmore/behat-chrome-extension
  # Remove once this patch is unneeded.
  local dest='DevToolsConnection.php'
  local patch_dest="${CO_DIR}/vendor/dmore/chrome-mink-driver/src/${dest}"
  local patch_src="${CLOUD_HOME}/${DEFAULT_BEHAT_PATH}/DevToolsConnection.patch"
  if [[ -f "${patch_src}" && ! -f "${patch_dest}.back" ]]; then
      mv "${patch_dest}" "${patch_dest}.back"
      echo_count "Replacing ${dest}"
      cp "${patch_src}" "${patch_dest}"
  fi

  echo_count "Starting ${CHROMIUM}"
  if ! (command -v "${CHROMIUM}" &>/dev/null); then
    run_cmd apt-get update && apt-get -y install chromium-driver
  else
    pkill "${CHROMIUM}"
  fi

  # See https://gitlab.com/DMore/chrome-mink-driver/
  "${CHROMIUM}" --disable-extensions --disable-gpu --headless --no-sandbox \
  --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222 2> /dev/null &
  readonly CHROMIUM_PID=$(pgrep "${CHROMIUM}")
  echo "PID: ${CHROMIUM_PID}"
  sleep 10s
  run_popd
}

function create_output_dir() {
  run_pushd "${BEHAT_FULL_PATH}"
  echo_count 'Creating a tmp directory'
  readonly OUTPUT_DIR_FROM_BEHAT_PATH=$(mktemp -d "${preprocess_output_dir:-${FEATURES}}"/tmp."${START_TIMESTAMP}".XXXXXX)
  readonly random_str_generated=$(echo "${OUTPUT_DIR_FROM_BEHAT_PATH}"| sed -r 's/.*\.([^\.]*)$/\1/')
  run_popd

  if [[ "${OUTPUT_DIR_FROM_BEHAT_PATH}" ]]; then
    echo "${OUTPUT_DIR_FROM_BEHAT_PATH} is created."
    readonly PREPROCESS_OUTPUT_FULL_PATH="${BEHAT_FULL_PATH}/${OUTPUT_DIR_FROM_BEHAT_PATH}"
  else
    exit 1
  fi
}

function before_test() {
  run_pushd "${DOCROOT}"
  export PATH="${PATH}:${CO_DIR}/vendor/bin"
  run_cmd drush updatedb -y
  run_cmd drush cache:rebuild
  run_cmd "drush cloud:behat:preprocess \
    --behat_path=${behat_path} \
    --params_files=${preprocess_params_files:-${DEFAULT_PARAMS_FILES}} \
    --templates_dir=${preprocess_templates_dir} \
    --output_dir=${OUTPUT_DIR_FROM_BEHAT_PATH} --random_str=${random_str_opt:-${random_str_generated}}"
  run_popd
}

function check_recent_log() {
  local errors=$(drush watchdog:show | grep Error)
  if [[ -z "${errors}" ]]; then
    echo "No errors by drush watchdog:show"
  else
    local error_ids=$(echo "${errors}" | awk '{print $1}')
    local i=0
    for id in ${error_ids}; do
      echo '---------------------------------'
      echo " Error #$((++i)) by drush watchdog:show "
      echo '---------------------------------'
      drush watchdog:show-one "${id}"
    done
  fi
}

function run_test() {
  local feature_file_or_dir="$@"
  run_pushd "${BEHAT_FULL_PATH}"
  local behat_options="--colors ${behat_options}"
  export PATH="${PATH}:${CO_DIR}/vendor/bin"
  run_cmd drush updatedb -y
  run_cmd drush cache:rebuild
  if [[ "${run_mode}" == 'at_once' ]]; then
    run_cmd_recent_log behat "${behat_options}" "${feature_file_or_dir}"
  else
    for file in "${feature_file_or_dir}"/*; do
      run_cmd_recent_log behat "${behat_options}" "${file}"
    done
  fi
  run_popd
}

function repeat_test() {
  local feature_file_or_dir="$1"
  for ((j=1; j <= repeat; j++)); do
    echo "** [${j}/${repeat}] Starting to test on $(date +'%Y%m%d_%H%M%S')"
    run_test "${feature_file_or_dir}"
  done
}

function after_test() {
  if [[ -d "${PREPROCESS_OUTPUT_FULL_PATH}" ]]; then
    echo "Deleting ${PREPROCESS_OUTPUT_FULL_PATH}"
    rm -rf "${PREPROCESS_OUTPUT_FULL_PATH}"
  fi
  if [[ "${CHROMIUM_PID}" ]]; then
    run_cmd kill "${CHROMIUM_PID}"
  fi
}

function print_env {
  run_pushd "${BEHAT_FULL_PATH}"
  echo_count 'Printing test environments'
  cat << EOF
behat_path:       ${BEHAT_FULL_PATH}
behat version:    $(behat --version)
EOF
  if [[ "${CHROMIUM_PID}" ]]; then
    echo "${CHROMIUM} version: $(${CHROMIUM} --version)"
  fi
  if [[ "${require_preprocess}" == 'false' ]]; then
    echo "feature_file: ${feature_file}"
  else
    echo "parameter_files:  ${preprocess_params_files:=${DEFAULT_PARAMS_FILES}}"
    echo "templates_dir:    ${preprocess_templates_dir:=${FEATURES}/${TEMPLATES}}"
    echo "feature_file_dir: ${OUTPUT_DIR_FROM_BEHAT_PATH:-TBD}"
  fi
  run_popd
}

main "$@"
i=0
readonly CHROMIUM='chromium'
readonly BEHAT_FULL_PATH="${CLOUD_HOME}/${behat_path:=${DEFAULT_BEHAT_PATH}}"

readonly START_TIMESTAMP=$(date +'%Y%m%d_%H%M%S')
readonly START_EPOCH=$(date +'%s')
echo "*** Starting tests ${repeat:=1} time(s) using $0 on ${START_TIMESTAMP}..."
setup
print_env
if [[ "${require_preprocess}" == 'false' ]]; then
  run_mode='at_once'
  repeat_test "${feature_file}"
else
  create_output_dir || exit 1
  before_test
  repeat_test "${OUTPUT_DIR_FROM_BEHAT_PATH}"
fi
after_test
readonly END_TIMESTAMP=$(date +'%Y%m%d_%H%M%S')
echo "*** Finished tests using $0 on ${END_TIMESTAMP}: duration (sec) $(( $(date +'%s') - START_EPOCH))"
