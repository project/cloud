<?php

namespace Drupal\Tests\cloud\Functional;

use Drupal\Component\Utility\Random;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Tests 404 Not Found redirect.
 *
 * @group Cloud
 */
class CloudErrorPageTest extends CloudTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'aws_cloud',
    'cloud_budget',
    'cloud',
    'cloud_cluster',
    'cloud_cluster_worker',
    'docker',
    'gapps',
    'k8s',
    'k8s_to_s3',
    'openstack',
    'gapps',
    's3_to_k8s',
    'terraform',
    'vmware',
  ];

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->random = new Random();
    $perms = $this->getPermissions();
    $web_user = $this->drupalCreateUser($perms);
    $this->drupalLogin($web_user);
  }

  /**
   * {@inheritdoc}
   */
  protected function createCloudContext($bundle = NULL): CloudContentEntityBase {
    // This is a stub method, which is unnecessary to implement.
    return CloudContentEntityBase::create();
  }

  /**
   * {@inheritdoc}
   */
  protected function getPermissions(): array {
    // This is a stub method, which is unnecessary to implement.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function runTestEntityBulk($type, array $entities, $operation = 'delete', $passive_operation = 'deleted', $path_prefix = '/admin/structure'): void {
    // This is a stub method, which is unnecessary to implement.
  }

  /**
   * Test 403 Access denied.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function test403AccessDenied(): void {

    // 1. Login as a normal user w/o any permissions.
    // Access to /admin to try to see 403 Access denied.
    $this->drupalGet('/admin');
    $this->assertAccessDenied();

    // 2. Login as an admin user w/ administer site configuration.
    $admin_user = $this->drupalCreateUser([
      'administer site configuration',
      'access administration pages',
    ]);
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/config');
    // The error message should be shown: "One or more problems were detected
    // with your Drupal installation.Check the status report for more
    // information.".
    $this->assertErrorMessage();
  }

  /**
   * Test 404 Not found.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Exception
   */
  public function test404NotFound(): void {

    // Access to /{random_string} to try to see 404 Not found.
    $url_path = "/{$this->random->word(random_int(1, 255))}";
    $this->drupalGet($url_path);
    $this->assertNotFound();
  }

}
