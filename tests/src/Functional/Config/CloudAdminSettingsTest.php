<?php

namespace Drupal\Tests\cloud\Functional\Config;

use Drupal\Component\Utility\Random;
use Drupal\Tests\cloud\Functional\CloudTestBase;
use Drupal\Tests\cloud\Traits\CloudTestFormDataTrait;
use Drupal\cloud\Entity\CloudContentEntityBase;

/**
 * Test Case class for Cloud Admin Settings forms.
 *
 * @group Cloud
 */
class CloudAdminSettingsTest extends CloudTestBase {

  use CloudTestFormDataTrait;

  public const CLOUD_ADMIN_SETTINGS_REPEAT_COUNT = 2;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'cloud',
    'geocoder',
  ];

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->random = new Random();
    $perms = $this->getPermissions();
    $web_user = $this->drupalCreateUser($perms);
    $this->drupalLogin($web_user);
  }

  /**
   * Get permissions of login user.
   *
   * @return array
   *   permissions of login user.
   */
  protected function getPermissions(): array {
    return [
      'administer cloud',
      'administer site configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function createCloudContext($bundle = NULL): CloudContentEntityBase {
    // This is a stub method, which is unnecessary to implement.
    return CloudContentEntityBase::create();
  }

  /**
   * {@inheritdoc}
   */
  protected function runTestEntityBulk($type, array $entities, $operation = 'delete', $passive_operation = 'deleted', $path_prefix = '/admin/config'): void {
    // This is a stub method, which is unnecessary to implement.
  }

  /**
   * Test for CloudAdminSettings.
   *
   * @param string $setting_type
   *   The setting type id.
   * @param array $edit
   *   The array of input data.
   * @param bool $flag
   *   The value of flag.
   */
  protected function runCloudAdminSettings($setting_type, array $edit, bool $flag): void {
    $this->drupalGet("/admin/config/services/cloud/{$setting_type}");
    $this->assertWarningMessage();

    $this->submitForm(
      $edit,
      $this->t('Save')->render()
    );

    if (empty($flag)) {
      $this->assertWarningAndErrorMessage();
    }
    else {
      $this->assertWarningMessage();
    }
  }

  /**
   * Test for CloudAdminSettings.
   */
  public function testCloudAdminSettings(): void {
    $flag = 1;
    $edit = $this->createCloudAdminSettingsFormData(self::CLOUD_ADMIN_SETTINGS_REPEAT_COUNT, $flag);

    for ($i = 0; $i < self::CLOUD_ADMIN_SETTINGS_REPEAT_COUNT; $i++) {
      $edit[$i] = array_intersect_key($edit[$i], array_flip(
        (array) array_rand($edit[$i], random_int(1, count($edit[$i])))
      ));

      $this->runCloudAdminSettings('settings', $edit[$i], $flag);
    }
  }

}
