<?php

namespace Drupal\Tests\cloud\Functional\Module;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\cloud\Traits\CloudAssertionTrait;
use Drupal\Tests\system\Functional\Module\ModuleTestBase;

/**
 * Install/uninstall module(s) and confirm table creation/deletion.
 */
abstract class CloudModuleTestBase extends ModuleTestBase {

  use CloudAssertionTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * Repeat install / uninstall for a test case.
   */
  public const CLOUD_MODULE_INSTALL_UNINSTALL_REPEAT_COUNT = 1;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dblog',
    // To use a field_purge_batch() function.
    'field',
  ];

  /**
   * {@inheritdoc}
   */
  public static $excludedModules = ['filter'];

  /**
   * Tests that a fixed set of modules can be installed and uninstalled.
   */
  abstract public function testInstallUninstall(): void;

  /**
   * Tests that a fixed set of modules can be installed and uninstalled.
   *
   * @param string $module_name
   *   The module names to test install and uninstall.
   */
  protected function runInstallUninstall($module_name): void {
    try {
      $this->repeatAssertInstallUninstallModules([$module_name]);
    }
    catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
  }

  /**
   * Tests that a fixed set of modules can be installed and uninstalled.
   *
   * @param array $modules
   *   The module names to test install and uninstall.
   * @param int $max_count
   *   Max test repeating count.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  protected function repeatAssertInstallUninstallModules(array $modules = ['cloud'], $max_count = self::CLOUD_MODULE_INSTALL_UNINSTALL_REPEAT_COUNT): void {
    for ($i = 0; $i < $max_count; $i++) {
      // Install and uninstall $module.
      $this->assertInstallUninstallModulesTest($modules);
    }
  }

  /**
   * Repeating test cloud service provider redirect.
   *
   * @param array $modules
   *   The module names to test install and uninstall.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  protected function assertInstallUninstallModulesTest(array $modules): void {

    // Get a list of modules to enable.
    $all_modules = $this->container->get('extension.list.module')->getList();
    $all_modules = array_filter($all_modules, static function ($module) use ($modules) {

      // Only return TRUE for $modules.
      foreach ($modules ?: [] as $module_name) {
        if ($module->getName() === $module_name) {
          return TRUE;
        }
      }
      return FALSE;
    });

    // Install every module possible.
    \Drupal::service('module_installer')->install(array_keys($all_modules));

    $this->assertModules(array_keys($all_modules), TRUE);
    foreach ($all_modules ?: [] as $module => $info) {
      $this->assertModuleConfig($module);
      $this->assertModuleTablesExist($module);
    }

    // Resets all data structures after having enabled new modules.
    $this->resetAll();

    // Delete all entities provided by modules that prevent uninstallation. For
    // example, if any content entity exists its provider cannot be uninstalled.
    // So deleting all taxonomy terms allows the Taxonomy to be uninstalled.
    // Additionally, every field is deleted so modules can be uninstalled. For
    // example, if a comment field exists then Comment cannot be uninstalled.
    $entity_type_manager = \Drupal::entityTypeManager();

    foreach ($entity_type_manager->getDefinitions() ?: [] as $entity_type) {

      if (($entity_type instanceof ContentEntityTypeInterface
        || in_array($entity_type->id(), ['field_storage_config', 'filter_format'], TRUE))
        && !in_array($entity_type->getProvider(), ['system', 'user'], TRUE)) {

        $storage = $entity_type_manager->getStorage($entity_type->id());
        $storage->delete($storage->loadMultiple());
      }
    }

    // Purge the field data.
    field_purge_batch(1000);

    $all_modules = \Drupal::service('extension.list.module')->getList();
    $database_module = \Drupal::service('database')->getProvider();

    $expected_modules = ['path_alias', 'system', 'user', $database_module];

    // Ensure that only core required modules and the installation profile can
    // not be uninstalled.
    $validation_reasons = \Drupal::service('module_installer')->validateUninstall(array_keys($all_modules));
    $validation_modules = array_keys($validation_reasons);
    $this->assertEqualsCanonicalizing($expected_modules, $validation_modules);

    $modules_to_uninstall = array_filter($all_modules, static function ($module) {
      // Filter required and not enabled modules.
      if (!empty($module->info['required']) || $module->status == FALSE) {
        return FALSE;
      }
      return TRUE;
    });

    // Can not uninstall the database module.
    unset($modules_to_uninstall[$database_module]);

    // Uninstall all modules that can be uninstalled.
    \Drupal::service('module_installer')->uninstall(array_keys($modules_to_uninstall));

    $this->assertModules(array_keys($modules_to_uninstall), FALSE);
    foreach ($modules_to_uninstall ?: [] as $module => $info) {
      $this->assertNoModuleConfig($module);
      $this->assertModuleTablesDoNotExist($module);
    }
  }

  /**
   * Asserts that a module is not yet installed.
   *
   * @param string $name
   *   Name of the module to check.
   */
  protected function assertModuleNotInstalled($name): void {
    $this->assertModules([$name], FALSE);
    // $this->assertModuleTablesDoNotExist($name);
  }

  /**
   * Asserts that a module was successfully installed.
   *
   * @param string $name
   *   Name of the module to check.
   */
  protected function assertModuleSuccessfullyInstalled($name): void {
    $this->assertModules([$name], TRUE);
    $this->assertModuleTablesExist($name);
    $this->assertModuleConfig($name);
  }

  /**
   * Uninstalls a module and asserts that it was done correctly.
   *
   * @param string $module
   *   The name of the module to uninstall.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function assertSuccessfulUninstall($module): void {
    $edit = [];
    $edit["uninstall[{$module}]"] = TRUE;
    $this->drupalGet('admin/modules/uninstall');
    $this->submitForm(
      $edit,
      $this->t('Uninstall')->render()
    );
    $this->submitForm(
      [],
      $this->t('Uninstall')->render()
    );
    $this->assertSession()->pageTextContains($this->t('The selected modules have been uninstalled.'));
    $this->assertModules([$module], FALSE);

    // Check that the appropriate hook was fired and the appropriate log
    // message appears. (But do not check for the log message if the dblog
    // module was just uninstalled, since the {watchdog} table won't be there
    // anymore.)
    $this->assertLogMessage('system', '%module module uninstalled.', ['%module' => $module], RfcLogLevel::INFO);

    // Check that the module's database tables no longer exist.
    $this->assertModuleTablesDoNotExist($module);
    // Check that the module's config files no longer exist.
    $this->assertNoModuleConfig($module);
  }

  /**
   * Verifies a module's help.
   *
   * Verifies that the module help page from hook_help() exists and can be
   * displayed, and that it contains the phrase "Foo Bar module", where "Foo
   * Bar" is the name of the module from the .info.yml file.
   *
   * @param string $module
   *   Machine name of the module to verify.
   * @param string $name
   *   Human-readable name of the module to verify.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  protected function assertHelp($module, $name): void {
    $this->drupalGet('admin/help/' . $module);
    // DO NOT change to $this->assertNoErrorMessage(), keep this as it is since
    // the page text may contain a warning message such as "There are no Cloud
    // Service provider modules enabled. Enable...".
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextNotContains($this->t('Error message'));
    $this->assertSession()->pageTextContains($name . ' module');
    $this->assertSession()->linkExists("online documentation for the {$name} module");
  }

  /**
   * Get a module name from a class name.
   *
   * @param string $class_name
   *   The class name.
   *
   * @return string
   *   The module name.
   */
  protected function getModuleName($class_name = ''): string {

    $class_name = $class_name ?: get_class($this);
    $prefix = 'Drupal\\Tests\\';
    $module_name_start_pos = strpos($class_name, $prefix) + strlen($prefix);
    $module_name_end_pos = strpos($class_name, '\\', $module_name_start_pos);
    return substr($class_name, $module_name_start_pos,
      $module_name_end_pos - $module_name_start_pos);
  }

}
