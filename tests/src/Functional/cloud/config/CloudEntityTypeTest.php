<?php

namespace Drupal\Tests\cloud\Functional\cloud\config;

use Drupal\Component\Utility\Random;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\cloud\Traits\CloudAssertionTrait;

/**
 * Testing for <CloudResource>Type entities.
 *
 * These entities include CloudConfig, CloudLaunchTemplate, CloudProject
 * and CloudStore.
 *
 * @group Cloud
 */
class CloudEntityTypeTest extends BrowserTestBase {

  use CloudAssertionTrait;
  use StringTranslationTrait;

  public const CLOUD_ENTITY_TYPE_REPEAT_COUNT = 2;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'cloud',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * The profile to install as a basis for testing.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * The random object.
   *
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;

  /**
   * Get permissions of login user.
   *
   * @return array
   *   permissions of login user.
   */
  protected function getPermissions(): array {
    return [
      'administer cloud service providers',
      'administer site configuration',
    ];
  }

  /**
   * Set up test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->random = new Random();
    $perms = $this->getPermissions();
    $web_user = $this->drupalCreateUser($perms);
    $this->drupalLogin($web_user);
  }

  /**
   * Create the form data for a <CloudResource>Type entity.
   *
   * @param int $repeat_count
   *   The number of times to repeat.
   *
   * @return array
   *   Array of form data.
   */
  protected function createCloudEntityTypeFormData($repeat_count = 1): array {
    $data = [];
    for ($i = 0; $i < $repeat_count; $i++) {
      $name = $this->random->word(8);
      $data[] = [
        'label' => $name,
        'id' => strtolower($name),
      ];
    }
    return $data;
  }

  /**
   * Generic crud test for <CloudResource>Types.
   *
   * @param string $entity_type
   *   The entity type ID.
   * @param string $entity_type_label
   *   The entity label.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function runCloudEntityTypeCrud($entity_type, $entity_type_label): void {
    $this->drupalGet("/admin/structure/{$entity_type}");

    // Only asserting error message.  Since we are not enabling any of the
    // cloud service provider modules, we will see the warning message
    // asking users to enable AWS/K8s/OpenStack..etc...
    $this->assertWarningMessage();

    for ($i = 0; $i < self::CLOUD_ENTITY_TYPE_REPEAT_COUNT; $i++) {
      // Add the cloud config type.
      $this->drupalGet("admin/structure/{$entity_type}/add");
      $add = $this->createCloudEntityTypeFormData(self::CLOUD_ENTITY_TYPE_REPEAT_COUNT);
      $this->submitForm(
        $add[$i],
        $this->t('Save')->render()
      );
      $this->assertWarningMessage();

      // Assert the cloud config type was added.
      $t_args = [
        '@name' => $entity_type_label,
        '%label' => $add[$i]['label'],
      ];
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @name type %label has been created.', $t_args)));

      // Delete the cloud config type.
      $this->drupalGet("admin/structure/{$entity_type}/{$add[$i]['id']}/delete");
      $this->submitForm(
        [],
        $this->t('Delete')->render()
      );
      $this->assertWarningMessage();
      $this->assertSession()->pageTextContains(strip_tags($this->t('The @name type %label has been deleted.', $t_args)));
    }
  }

  /**
   * Test add and delete for CloudConfigType entity.
   */
  public function testCloudConfigTypeCrud(): void {
    $this->runCloudEntityTypeCrud('cloud_config_type', 'cloud service provider');
  }

  /**
   * Test add and delete for CloudLaunchTemplateType entity.
   */
  public function testCloudLaunchTemplateTypeCrud(): void {
    $this->runCloudEntityTypeCrud('cloud_server_template_type', 'launch template');
  }

  /**
   * Test add and delete for CloudProjectType entity.
   */
  public function testCloudProjectTypeCrud(): void {
    $this->runCloudEntityTypeCrud('cloud_project_type', 'cloud project');
  }

  /**
   * Test add and delete for CloudStoreType entity.
   */
  public function testCloudStoreTypeCrud(): void {
    $this->runCloudEntityTypeCrud('cloud_store_type', 'cloud store');
  }

}
