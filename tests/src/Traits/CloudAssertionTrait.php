<?php

namespace Drupal\Tests\cloud\Traits;

use Symfony\Component\HttpFoundation\Response;

/**
 * The assertion trait for common usage.
 */
trait CloudAssertionTrait {

  /**
   * Assert successful/non-error responses.
   */
  protected function assertNoErrorMessage(): void {
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->pageTextNotContains($this->t('Error message'));
    $this->assertSession()->pageTextNotContains($this->t('Warning message'));
  }

  /**
   * Assert warning responses.
   */
  protected function assertWarningMessage(): void {
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->pageTextNotContains($this->t('Error message'));
    $this->assertSession()->pageTextContains($this->t('Warning message'));
  }

  /**
   * Assert error response.
   */
  protected function assertErrorMessage(): void {
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->pageTextNotContains($this->t('Warning message'));
    $this->assertSession()->pageTextContains($this->t('Error message'));
  }

  /**
   * Assert error response.
   */
  protected function assertWarningAndErrorMessage(): void {
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->pageTextContains($this->t('Warning message'));
    $this->assertSession()->pageTextContains($this->t('Error message'));
  }

  /**
   * Assert error message only with invalid Google Apps.
   */
  protected function assertGoogleAppErrorOnly(): void {
    $errors = $this->xpath('//div[contains(@class,"messages--error")]/div/div[@class="messages__content"]');
    $expected = $this->t('The module Google Applications is invalid. Enable the module.');
    $this->assertEquals($errors[0]->getText(), $expected);
  }

  /**
   * Assert 403 Access denied.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  protected function assertAccessDenied(): void {
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);
    $this->assertSession()->pageTextContains(strip_tags($this->t('Access Denied')));
    $this->assertSession()->pageTextContains(strip_tags($this->t('You are not authorized to access this page.')));
    $this->assertSession()->pageTextNotContains(strip_tags($this->t('The website encountered an unexpected error. Try again later.')));
  }

  /**
   * Assert 404 Not found.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  protected function assertNotFound(): void {
    $this->assertSession()->statusCodeEquals(Response::HTTP_NOT_FOUND);
    $this->assertSession()->pageTextContains(strip_tags($this->t('Page not found')));
    $this->assertSession()->pageTextContains(strip_tags($this->t('The requested page could not be found.')));
    $this->assertSession()->pageTextNotContains(strip_tags($this->t('The website encountered an unexpected error. Try again later.')));
  }

}
