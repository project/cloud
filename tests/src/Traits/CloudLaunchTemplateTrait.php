<?php

namespace Drupal\Tests\cloud\Traits;

use Drupal\cloud\Entity\CloudLaunchTemplateInterface;

/**
 * The trait creating form data for cloud server template testing.
 */
trait CloudLaunchTemplateTrait {

  /**
   * Check the status for List and canonical view.
   *
   * @param string $list_url
   *   The URL of the cloud launch template list.
   * @param string $view_url
   *   The URL of the cloud launch template canonical view.
   * @param string $workflow_status
   *   The Value of workflow status to be checked.
   * @param bool $is_admin
   *   Whether the logged-in user is admin or not.
   */
  private function checkPageTextContains(
    string $list_url,
    string $view_url,
    string $workflow_status,
    bool $is_admin = FALSE,
  ): void {
    $actions = ['Review', 'Approve', 'Launch'];

    // Warning will occur if `$this->t()` is applied to non-literal strings.
    $t = [
      'Review' => $this->t('Review'),
      'Approve' => $this->t('Approve'),
      'Launch' => $this->t('Launch'),
    ];

    $allowed_actions = match ($workflow_status) {
      CloudLaunchTemplateInterface::DRAFT => match ($is_admin) {
        TRUE => ['Review', 'Launch'],
        FALSE => ['Review'],
      },
      CloudLaunchTemplateInterface::REVIEW => match ($is_admin) {
        TRUE => ['Approve', 'Launch'],
        FALSE => [],
      },
      CloudLaunchTemplateInterface::APPROVED => ['Launch'],
      default => throw new \InvalidArgumentException("Invalid workflow status: {$workflow_status}"),
    };

    $seq = static fn(...$_) => NULL;
    $check_page = fn() => $seq(
      $this->assertSession()->pageTextContains($workflow_status),
      array_walk(
        $actions,
        fn(string $action) => in_array($action, $allowed_actions)
        ? $seq(
          $this->assertSession()->linkExistsExact($t[$action]),
          $this->assertSession()->linkByHrefExists("{$view_url}/" . strtolower($action)),
        )
        : $seq(
          $this->assertSession()->linkNotExistsExact($t[$action]),
          $this->assertSession()->linkByHrefNotExists("{$view_url}/" . strtolower($action)),
        )
      ),
    );

    $this->drupalGet($list_url);
    $check_page();
    $this->assertNoErrorMessage();

    $this->drupalGet($view_url);
    $check_page();
    $this->assertNoErrorMessage();

    foreach ($allowed_actions as $action) {
      $this->drupalGet($view_url);
      $this->clickLink($t[$action]);
      $this->assertNoErrorMessage();
    }
  }

  /**
   * Update the cloud launch template to make sure there are no errors.
   *
   * @param string $cloud_context
   *   The cloud context.
   * @param array $test_data
   *   The cloud server template.
   * @param string $type
   *   The type is 'add' or 'edit'.
   * @param string $module_name
   *   The module name.
   * @param int $num
   *   The num is sequential numbering of cloud server template.
   */
  private function updateLaunchTemplate(
    string $cloud_context,
    array $test_data,
    string $type,
    string $module_name,
    int $num = 1,
  ): void {
    if (empty($cloud_context) || empty($test_data)) {
      $this->assertTrue(FALSE, 'Either of the two arguments is empty.');
      return;
    }

    $url = '';
    $text_contains = '';
    $t_args = [
      '@type' => 'launch template',
      '%label' => $test_data['name[0][value]'],
    ];

    switch ($type) {
      case CloudLaunchTemplateInterface::OPERATION_ADD:
        $url = "/clouds/design/server_template/$cloud_context/$module_name/add";
        $text_contains = strip_tags($this->t('The @type %label has been created.', $t_args));
        break;

      case CloudLaunchTemplateInterface::OPERATION_EDIT:
        $url = "/clouds/design/server_template/$cloud_context/$num/edit";
        $text_contains = strip_tags($this->t('The @type %label has been updated.', $t_args));
        break;

      default:
        $this->assertTrue(FALSE, 'There is no corresponding type.');
        return;
    }

    if ($module_name === 'k8s') {
      unset($test_data['field_object']);
    }
    $this->drupalGet($url);
    $this->submitForm(
      $test_data,
      $this->t('Save')->render()
    );
    $this->assertNoErrorMessage();
    $this->assertSession()->pageTextContains($text_contains);
  }

}
