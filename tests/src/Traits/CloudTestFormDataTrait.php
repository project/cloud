<?php

namespace Drupal\Tests\cloud\Traits;

/**
 * The trait creating form data for CloudAdminSettings.
 */
trait CloudTestFormDataTrait {

  /**
   * Get the items per page on Pager options, Views, Cloud Settings.
   *
   * @return array
   *   Array of items per page.
   */
  private function getItemsPerPage(): array {
    return [10, 15, 20, 25, 50, 100];
  }

  /**
   * Get the Geocoder Plugin on Cloud Settings.
   *
   * @return array
   *   Array of items per page.
   */
  private function getGeocoderPluginOptions(): array {
    return ['random', 'openstreetmap', 'nominatim'];
  }

  /**
   * Create an array of random input data for Cloud Admin Settings.
   *
   * @param int $repeat_count
   *   Number of time to repeat the test.
   * @param bool $flag
   *   Value of the flag.
   *
   * @return array
   *   The array including random input data.
   */
  public function createCloudAdminSettingsFormData($repeat_count = 1, $flag = TRUE): array {
    $config_factory = \Drupal::ConfigFactory();
    $config = $config_factory->getEditable('cloud.settings');

    $geocoder_plugin_options = $this->getGeocoderPluginOptions();
    $items_per_page = $this->getItemsPerPage();

    $edit = [];
    for ($i = 0; $i < $repeat_count; $i++) {
      if (!empty($flag)) {
        $edit[] = [
          'cloud_use_default_urls' => 1,
          'cloud_location_geocoder_plugin' => $geocoder_plugin_options[array_rand($geocoder_plugin_options)],
          'cloud_view_expose_items_per_page' => !$config->get('cloud_view_expose_items_per_page'),
          'cloud_view_items_per_page' => $items_per_page[array_rand($items_per_page)],
        ];
      }
      else {
        $edit[] = [
          'cloud_use_default_urls' => 0,
          'cloud_custom_location_map_json_url' => 'https://' . $this->random->name(10, TRUE),
          'cloud_custom_d3_js_url' => 'https://' . $this->random->name(10, TRUE),
          'cloud_custom_d3_horizon_chart_js_url' => 'https://' . $this->random->name(10, TRUE),
          'cloud_custom_c3_js_url' => 'https://' . $this->random->name(10, TRUE),
          'cloud_custom_c3_css_url' => 'https://' . $this->random->name(10, TRUE),
          'cloud_custom_chart_js_url' => 'https://' . $this->random->name(10, TRUE),
          'cloud_custom_select2_css_url' => 'https://' . $this->random->name(10, TRUE),
          'cloud_custom_select2_js_url' => 'https://' . $this->random->name(10, TRUE),
          'cloud_custom_datatables_js_url' => 'https://' . $this->random->name(10, TRUE),
          'cloud_location_geocoder_plugin' => $geocoder_plugin_options[array_rand($geocoder_plugin_options)],
          'cloud_view_expose_items_per_page' => !$config->get('cloud_view_expose_items_per_page'),
          'cloud_view_items_per_page' => $items_per_page[array_rand($items_per_page)],
        ];
      }
    }

    return $edit;
  }

}
